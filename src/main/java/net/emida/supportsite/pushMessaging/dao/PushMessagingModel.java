
package net.emida.supportsite.pushMessaging.dao;

import com.debisys.utils.DateUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author dgarzon
 */
public class PushMessagingModel {
    private BigDecimal rwn;
    private String id;
    private Timestamp notificationDate;
    private String title;
    private boolean active;
    private boolean androidMethod;
    private boolean iosMethod;

    public BigDecimal getRwn() {
        return rwn;
    }

    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotificationDate() {
        return DateUtil.formatDateTime_DB(notificationDate.toString());
    }

    public void setNotificationDate(Timestamp notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isAndroidMethod() {
        return androidMethod;
    }

    public void setAndroidMethod(boolean androidMethod) {
        this.androidMethod = androidMethod;
    }

    public boolean isIosMethod() {
        return iosMethod;
    }

    public void setIosMethod(boolean iosMethod) {
        this.iosMethod = iosMethod;
    }
    
    

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
