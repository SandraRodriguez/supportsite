package net.emida.supportsite.pushMessaging.dao;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.pushMessaging.IPushMessagingDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dgarzon
 */
@Repository
public class PushMessagingJdbcDao implements IPushMessagingDao {

    private static final Logger log = LoggerFactory.getLogger(PushMessagingJdbcDao.class);

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll() {
        try {
            return 0L;
        } catch (DataAccessException ex) {
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<PushMessagingModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {

        try {
            PushMessagingQueryBuilder queryBuilder = new PushMessagingQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();
            log.debug("Query : {}", SQL_QUERY);
            if (queryData != null) {
                queryData.clear();
            }
            queryData = null;
            return jdbcTemplate.query(SQL_QUERY, new PushMessagingMapper());

        } catch (DataAccessException ex) {
            log.error("Data access error in list PushMessaging");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in list PushMessaging");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {
            PushMessagingQueryBuilder queryBuilder = new PushMessagingQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();
            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            if (queryData != null) {
                queryData.clear();
            }
            queryData = null;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count PushMessaging");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count PushMessaging");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

}
