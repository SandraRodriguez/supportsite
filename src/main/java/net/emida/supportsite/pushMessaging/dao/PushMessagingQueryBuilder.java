
package net.emida.supportsite.pushMessaging.dao;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dgarzon
 */
public class PushMessagingQueryBuilder {
    private static final Logger log = LoggerFactory.getLogger(PushMessagingQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    
    private String startDate;
    private String endDate;
    private String title;

    /**
     *
     * @param queryData
     */
    public PushMessagingQueryBuilder(Map<String, Object> queryData) {
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.title = (String) queryData.get("title");
        log.debug("Query Map = {}", queryData);
    }

    public PushMessagingQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.title = (String) queryData.get("title");
        
    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery());
        sb0.append(") AS tbl1 ");
        return sb0.toString();
    }

    public String mainQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append(" SELECT id as Id, notificationDate as NotificationDate, title as Title, active as Active, androidMethod as AndroidMethod, iosMethod as IosMethod ");
        mainQuery.append(" FROM pushMessaging WITH(nolock) ");
        
        boolean hasStartDate = false;
        boolean hasEndDate = false;
        
        if(startDate != null && !startDate.trim().isEmpty()){
            mainQuery.append(" WHERE notificationDate >= '"+startDate+"' ");
            hasStartDate = true;
        }
        if(endDate != null && !endDate.trim().isEmpty()){
            if(hasStartDate){
                mainQuery.append(" AND notificationDate <= '"+endDate+"' ");
            }
            else{
                mainQuery.append(" WHERE notificationDate <= '"+endDate+"' ");
            }
            hasEndDate = true;
        }
        if(title != null && !title.trim().isEmpty()){
            if(hasStartDate || hasEndDate){
                mainQuery.append(" AND title like '%"+title+"%' ");
            }
            else{
                mainQuery.append(" WHERE title like '%"+title+"%' ");
            }
            
        }
        System.out.println("Main Query push Messaging : "+mainQuery.toString());
        return mainQuery.toString();
    }


    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "title";
            sortOrder = " ASC ";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS rwn, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE rwn BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }


}

