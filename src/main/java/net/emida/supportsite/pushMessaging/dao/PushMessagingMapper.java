package net.emida.supportsite.pushMessaging.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author dgarzon
 */
public class PushMessagingMapper implements RowMapper<PushMessagingModel>{

    @Override
    public PushMessagingModel mapRow(ResultSet rs, int i) throws SQLException {
        PushMessagingModel model = new PushMessagingModel();
        model.setRwn(rs.getBigDecimal("rwn"));
        model.setId(rs.getString("id"));
        model.setNotificationDate(rs.getTimestamp("notificationDate"));
        model.setTitle(rs.getString("title"));
        model.setActive(rs.getBoolean("active"));
        model.setAndroidMethod(rs.getBoolean("androidMethod"));
        model.setIosMethod(rs.getBoolean("iosMethod"));
        
        return model;
    }
    
}
