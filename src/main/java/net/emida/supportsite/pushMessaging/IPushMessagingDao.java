
package net.emida.supportsite.pushMessaging;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.pushMessaging.dao.PushMessagingModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author dgarzon
 */
public interface IPushMessagingDao {
     public List<PushMessagingModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    public Long count(Map<String, Object> queryData) throws TechnicalException;
}
