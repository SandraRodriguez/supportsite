
package net.emida.supportsite.pushMessaging;

/**
 *
 * @author dgarzon
 */
public class PushMessagingForm {
    private String startDate;
    private String endDate;
    private String title;    

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
 
    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    

    @Override
    public String toString() {
        return "PushMessagingForm[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", title=" + title +                
                ']';
    }
}
