
package net.emida.supportsite.pushMessaging;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.pushMessaging.dao.PushMessagingModel;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dgarzon
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class PushMessagingController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(PushMessagingController.class);

    @Autowired
    private IPushMessagingDao iPushMessagingDao;

    @RequestMapping(value = UrlPaths.PUSH_MESSAGING_SETUP_REPORT_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute PushMessagingForm reportModel) {

        fillProgram(model);
        return "reports/pushmessaging/pushMessagingForm";
    }

    @RequestMapping(value = UrlPaths.PUSH_MESSAGING_SETUP_REPORT_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid PushMessagingForm reportModel, BindingResult result) throws TechnicalException, ApplicationException, ParseException {

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillProgram(model.asMap());
            return "reports/pushmessaging/pushMessagingForm";
        }

        log.debug("reportBuild {}", result);
        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        String formattedStartDate = reportModel.getStartDate();
        String formattedEndDate = reportModel.getStartDate();

        model.addAttribute("startDate", reportModel.getStartDate());
        model.addAttribute("endDate", reportModel.getEndDate());
        model.addAttribute("title", reportModel.getTitle());
        Map<String, Object> queryData = fillQueryData(formattedStartDate, formattedEndDate, reportModel.getTitle());

        Long totalRecords = iPushMessagingDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;
        
        return "reports/pushmessaging/pushMessagingForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = UrlPaths.PUSH_MESSAGING_SETUP_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<PushMessagingModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String startDate, @RequestParam String endDate, @RequestParam String title) throws ParseException {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. StartDate={}. EndDate={}. ", startDate, endDate);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);
        
        Map<String, Object> queryData = fillQueryData(startDate, endDate, title);
        List<PushMessagingModel> list = iPushMessagingDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;
        
        return list;
    }

    private Map<String, Object> fillQueryData(String startDate, String endDate, String name) throws ParseException {       
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put("title", name);
        return queryData;
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 32;
    }

    /**
     * Fill the selects type inputs
     *
     * @param model
     */
    private void fillProgram(Map model) {
        fillFilter(model);
    }
    
    private void fillFilter (Map model){
        try {
            model.put("strAccessLevel", getAccessLevel());
            model.put("strRefId", getRefId());
            model.put("strDistChainType", getDistChainType());
        } catch (TechnicalException e) {
            log.error("Error setting merchant variables. Ex = {}", e);
        }
    }


}

