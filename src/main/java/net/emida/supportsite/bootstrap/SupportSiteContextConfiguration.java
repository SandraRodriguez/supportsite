package net.emida.supportsite.bootstrap;

import net.emida.supportsite.util.SupportSiteMessageSource;
import net.emida.supportsite.util.security.AESCipher;
import net.emida.supportsite.util.security.DefaultAESCipher;
import net.emida.supportsite.util.security.DefaultSecretKeyProvider;
import net.emida.supportsite.util.security.SecretKeyProvider;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * @author fvilladiego
 */
@Configuration
//Scanning annotated components. Replaces for <context:component-scan>
//@ComponentScan(basePackages = "net.emida.supportsite")
public class SupportSiteContextConfiguration{



    @Bean(name = "messageSource")
    public MessageSource messageSource() {
        SupportSiteMessageSource bean = new SupportSiteMessageSource();
        bean.setUseCodeAsDefaultMessage(true);
        return bean;
    }


    //If we omit this name, Spring configure this validator and its mvcValidator
    @Bean(name = "validator")
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }


    @Bean
    public SecretKeyProvider secretKeyProvider(){
        DefaultSecretKeyProvider bean = new DefaultSecretKeyProvider();
        return bean;
    }

    @Bean
    public AESCipher aesCipher(){
        DefaultAESCipher bean = new DefaultAESCipher(secretKeyProvider());
        return bean;
    }

}
