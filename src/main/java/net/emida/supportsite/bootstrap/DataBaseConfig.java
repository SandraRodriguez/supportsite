package net.emida.supportsite.bootstrap;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import net.emida.supportsite.util.security.AESCipher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.beans.PropertyVetoException;

/**
 * @author fvilladiego
 */

//@Configuration
@PropertySources(value = {@PropertySource("/WEB-INF/webapp.properties")})
public class DataBaseConfig {

    private static final Logger log = LoggerFactory.getLogger(DataBaseConfig.class);

    @Resource
    Environment env;

    @Autowired
    private AESCipher aesCipher;

    @Bean(name = "dsMaster", destroyMethod = "close")
    public DataSource masterDataSource() {
        //org.apache.commons.dbcp.BasicDataSource
        log.info("Initializing Master DataSource bean...");
        log.info("URL={}", env.getRequiredProperty("torque.dsfactory.masterDb.pool.jdbcUrl"));
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        try {
            dataSource.setDriverClass(env.getRequiredProperty("torque.dsfactory.masterDb.pool.driverClass"));
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        dataSource.setJdbcUrl(env.getRequiredProperty("torque.dsfactory.masterDb.pool.jdbcUrl"));
        dataSource.setUser(env.getRequiredProperty("torque.dsfactory.masterDb.pool.user"));

        String masterPassword = env.getRequiredProperty("torque.dsfactory.masterDb.pool.password");
        if (masterPassword.contains("ENC(")) {
            masterPassword = masterPassword.substring(4, masterPassword.length() - 1);
            masterPassword = aesCipher.decrypt(masterPassword);
        }
        dataSource.setPassword(masterPassword);
        
        try {
            String checkoutTimeout = env.getRequiredProperty("torque.dsfactory.masterDb.pool.checkoutTimeout");
            if(checkoutTimeout != null && !checkoutTimeout.trim().isEmpty()){
                dataSource.setCheckoutTimeout(Integer.parseInt(checkoutTimeout));
            } 
        } catch (IllegalStateException e) {
            log.warn("The [torque.dsfactory.masterDb.pool.checkoutTimeout] does not exist, The Support Site will work without this property.");
        }   
        return dataSource;
    }

    @Bean(name = "dsReplica", destroyMethod = "close")
    public DataSource replicaDataSource() {

        log.info("Initializing Replica DataSource bean...");
        log.info("URL={}", env.getRequiredProperty("torque.dsfactory.replicaDb.pool.jdbcUrl"));
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        try {
            dataSource.setDriverClass(env.getRequiredProperty("torque.dsfactory.replicaDb.pool.driverClass"));
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        dataSource.setJdbcUrl(env.getRequiredProperty("torque.dsfactory.replicaDb.pool.jdbcUrl"));
        dataSource.setUser(env.getRequiredProperty("torque.dsfactory.replicaDb.pool.user"));

        String replicaPassword = env.getRequiredProperty("torque.dsfactory.replicaDb.pool.password");
        if (replicaPassword.contains("ENC(")) {
            replicaPassword = replicaPassword.substring(4, replicaPassword.length() - 1);
            replicaPassword = aesCipher.decrypt(replicaPassword);
        }
        dataSource.setPassword(replicaPassword);
        
        try {
            String checkoutTimeout = env.getRequiredProperty("torque.dsfactory.replicaDb.pool.checkoutTimeout");
            if(checkoutTimeout != null && !checkoutTimeout.trim().isEmpty()){
                dataSource.setCheckoutTimeout(Integer.parseInt(checkoutTimeout));
            } 
        } catch (IllegalStateException e) {
            log.warn("The [torque.dsfactory.replicaDb.pool.checkoutTimeout] does not exist, The Support Site will work without this property.");
        }
        return dataSource;
    }

    @Bean(name = "masterJdbcTemplate")
    public JdbcTemplate masterJdbcTemplate(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(masterDataSource());
        return jdbcTemplate;
    }

    @Bean(name = "masterNamedParamJdbcTemplate")
    public NamedParameterJdbcTemplate masterNamedParamJdbcTemplate(){
        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(masterDataSource());
        return template;
    }

    @Bean(name = "replicaJdbcTemplate")
    public JdbcTemplate replicaJdbcTemplate(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(replicaDataSource());
        return jdbcTemplate;
    }

    @Bean(name = "replicaNamedParamJdbcTemplate")
    public NamedParameterJdbcTemplate replicaNamedParamJdbcTemplate(){
        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(replicaDataSource());
        return template;
    }

}
