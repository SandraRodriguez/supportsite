package net.emida.supportsite.bootstrap;

import net.emida.supportsite.mvc.ServiceInterceptor;
import net.emida.supportsite.mvc.security.SecurityInterceptor;
import net.emida.supportsite.util.SupportSiteLocaleResolver;
import net.emida.supportsite.util.SupportSiteMessageSource;
import net.emida.supportsite.util.exceptions.SupportSiteExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.annotation.Resource;

import static net.emida.supportsite.mvc.UrlPaths.CHANGE_LANGUAGE_PATH;
import static net.emida.supportsite.mvc.UrlPaths.MESSAGE_PATH;

/**
 * @author fvilladiego
 */
@Configuration

//Enables annotation-based request handling components(Mapping, Adapter)
@EnableWebMvc //replaces for <mvc:annotation-driven/>

//Scanning annotated components. Replaces for <context:component-scan>
@ComponentScan(basePackages = "net.emida.supportsite")
@Import({DataBaseConfig.class})
public class SupportSiteMvcContextConfiguration extends WebMvcConfigurerAdapter {

    @Resource
    Environment environment;

    @Autowired
    private LocalValidatorFactoryBean localValidatorFactoryBean;

    @Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/pages/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SecurityInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/login.jsp", MESSAGE_PATH + "/**" ,  CHANGE_LANGUAGE_PATH + "/**", "/login/ng", "/login/ng/intranet", "/login/ng/multi-iso");
        registry.addInterceptor(new ServiceInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/login.jsp",  CHANGE_LANGUAGE_PATH + "/**", "/login/ng", "/login/ng/intranet", "/login/ng/multi-iso");

    }

    /*    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions("/WEB-INF/tiles/tiles-definitions.xml");
        return tilesConfigurer;
    }*/
    @Bean
    public HandlerExceptionResolver getHandlerExceptionResolver() {

        SupportSiteExceptionHandler exceptionResolver = new SupportSiteExceptionHandler();

        return exceptionResolver;
    }

/*    @Bean(name = "messageSource")
    public MessageSource messageSource() {
        SupportSiteMessageSource bean = new SupportSiteMessageSource();
        bean.setUseCodeAsDefaultMessage(true);
        return bean;
    }*/

    @Bean(name = "localeResolver")
    public LocaleResolver localeResolver() {
        SupportSiteLocaleResolver bean = new SupportSiteLocaleResolver();
        return bean;
    }

    //If we omit this name, Spring configure this validator and its mvcValidator
/*    @Bean(name = "validator")
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }*/

    /**
     * <strong>DTU-3582 / Support Site Bulk Merchant Payment Feature</strong>
     *
     * @author janez@emida.net
     * @return <code>CommonsMultipartResolver</code>
     * @since 21-02-2017
     * @version 1.0
     */
    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setDefaultEncoding("UTF-8");
        multipartResolver.setMaxUploadSize(268435456);
        return multipartResolver;
    }

    @Override
    public Validator getValidator() {
        return this.localValidatorFactoryBean;
    }
}
