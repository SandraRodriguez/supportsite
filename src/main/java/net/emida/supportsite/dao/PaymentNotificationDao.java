/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dao;

import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import net.emida.supportsite.dto.PaymentNotification;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author fernandob
 */
public class PaymentNotificationDao {
    private static final Logger logger = Logger.getLogger(PaymentNotificationDao.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("net.emida.supportsite.dao.sql");
    
    
        private static PaymentNotification getPaymentNotificationFromRs(ResultSet rs) {
        PaymentNotification retValue = null;

        try {
            if (rs != null) {
                retValue = new PaymentNotification();
                if ((hasColumn(rs, "id")) && (rs.getString("id") != null)) {
                    retValue.setId(rs.getLong("id"));
                }
                if ((hasColumn(rs, "actor_id")) && (rs.getString("actor_id") != null)) {
                    retValue.setActorId(rs.getLong("actor_id"));
                }
                if ((hasColumn(rs, "logon_id")) && (rs.getString("logon_id") != null)) {
                    retValue.setLogonId(rs.getString("logon_id"));
                }
                if ((hasColumn(rs, "description")) && (rs.getString("description") != null)) {
                    retValue.setDescription(rs.getString("description"));
                }
                if ((hasColumn(rs, "amount")) && (rs.getString("amount") != null)) {
                    retValue.setAmount(rs.getBigDecimal("amount"));
                }
                if ((hasColumn(rs, "credit_limit")) && (rs.getString("credit_limit") != null)) {
                    retValue.setCreditLimit(rs.getBigDecimal("credit_limit"));
                }
                if ((hasColumn(rs, "prior_credit_limit")) && (rs.getString("prior_credit_limit") != null)) {
                    retValue.setPriorCreditLimit(rs.getBigDecimal("prior_credit_limit"));
                }
                if ((hasColumn(rs, "available_credit")) && (rs.getString("available_credit") != null)) {
                    retValue.setAvailableCredit(rs.getBigDecimal("available_credit"));
                }
                if ((hasColumn(rs, "prior_available_credit")) && (rs.getString("prior_available_credit") != null)) {
                    retValue.setPriorAvailableCredit(rs.getBigDecimal("prior_available_credit"));
                }
                if ((hasColumn(rs, "commission")) && (rs.getString("commission") != null)) {
                    retValue.setCommission(rs.getBigDecimal("commission"));
                }
                if ((hasColumn(rs, "datetime")) && (rs.getString("datetime") != null)) {
                    retValue.setDatetime(rs.getString("datetime"));
                }
                if ((hasColumn(rs, "entityAccountType")) && (rs.getString("entityAccountType") != null)) {
                    retValue.setEntityAccountType(rs.getLong("entityAccountType"));
                }
                if ((hasColumn(rs, "actor_name")) && (rs.getString("actor_name") != null)) {
                    retValue.setActorName(rs.getString("actor_name"));
                }
                if ((hasColumn(rs, "payment_notification_type")) && (rs.getString("payment_notification_type") != null)) {
                    retValue.setPaymentNotificationType(rs.getLong("payment_notification_type"));
                }
                if ((hasColumn(rs, "sms_notification_phone")) && (rs.getString("sms_notification_phone") != null)) {
                    retValue.setSmsNotificationPhone(rs.getString("sms_notification_phone"));
                }
                if ((hasColumn(rs, "mail_notification_address")) && (rs.getString("mail_notification_address") != null)) {
                    retValue.setMailNotificationAddress(rs.getString("mail_notification_address"));
                }
                if ((hasColumn(rs, "notification_sent")) && (rs.getString("notification_sent") != null)) {
                    retValue.setNotificationSent(rs.getBoolean("notification_sent"));
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getDepositTypeFromRs. Could not get deposit type information : " + localException.toString());
        }

        return retValue;
    }

    public static boolean insertPaymentNotification(PaymentNotification paymentNotification) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        String sqlInsertNotification = sql_bundle.getString("insertPaymentNotification");
        
        List<String> paramValues = new ArrayList<String>();
        paramValues.add(String.valueOf(paymentNotification.getActorId()));
        paramValues.add(paymentNotification.getLogonId());
        paramValues.add(paymentNotification.getDescription());
        paramValues.add(paymentNotification.getAmount().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(paymentNotification.getCreditLimit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(paymentNotification.getPriorCreditLimit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(paymentNotification.getAvailableCredit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(paymentNotification.getPriorAvailableCredit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(paymentNotification.getCommission().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(paymentNotification.getDatetime());
        paramValues.add(String.valueOf(paymentNotification.getEntityAccountType()));
        paramValues.add(paymentNotification.getActorName());
        paramValues.add(String.valueOf(paymentNotification.getPaymentNotificationType()));
        paramValues.add(paymentNotification.getSmsNotificationPhone());
        paramValues.add(paymentNotification.getMailNotificationAddress());
        paramValues.add(String.valueOf(paymentNotification.isNotificationSent()));
        DbUtil.printSqlString(sqlInsertNotification, paramValues);
        try {
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlInsertNotification);
            pstmt.setLong(1, paymentNotification.getActorId());
            pstmt.setString(2, paymentNotification.getLogonId());
            pstmt.setString(3, paymentNotification.getDescription());
            pstmt.setBigDecimal(4, paymentNotification.getAmount());
            pstmt.setBigDecimal(5, paymentNotification.getCreditLimit());
            pstmt.setBigDecimal(6, paymentNotification.getPriorCreditLimit());
            pstmt.setBigDecimal(7, paymentNotification.getAvailableCredit());
            pstmt.setBigDecimal(8, paymentNotification.getPriorAvailableCredit());
            pstmt.setBigDecimal(9, paymentNotification.getCommission());
            pstmt.setString(10, paymentNotification.getDatetime());
            pstmt.setLong(11, paymentNotification.getEntityAccountType());
            pstmt.setString(12, paymentNotification.getActorName());
            pstmt.setLong(13, paymentNotification.getPaymentNotificationType());
            pstmt.setString(14, paymentNotification.getSmsNotificationPhone());
            pstmt.setString(15, paymentNotification.getMailNotificationAddress());
            pstmt.setBoolean(16, paymentNotification.isNotificationSent());
            retValue = (pstmt.executeUpdate() == 1);
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("Error while inserting a new payment notification", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
        }
        return retValue;
    }
    
}
