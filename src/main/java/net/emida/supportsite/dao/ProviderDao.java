/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dao;

import java.util.List;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import java.util.ResourceBundle;
import org.apache.torque.Torque;
import com.debisys.utils.DbUtil;
import java.sql.PreparedStatement;
import com.debisys.utils.StringUtil;
import net.emida.supportsite.dto.Provider;
import static com.debisys.utils.DbUtil.hasColumn;

/**
 *
 * @author fernandob
 */
public class ProviderDao {

    private static final Logger logger = Logger.getLogger(ProviderDao.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("net.emida.supportsite.dao.sql");

    public static Provider getProviderFromRs(ResultSet rs) {
        Provider provider = null;

        try {
            if (rs != null) {
                provider = new Provider();
                if ((hasColumn(rs, "provider_id")) && (rs.getString("provider_id") != null)){
                    provider.setProviderId(rs.getLong("provider_id"));
                }
                if ((hasColumn(rs, "name")) && (rs.getString("name") != null)) {
                    provider.setName(rs.getString("name").trim());
                }
                if ((hasColumn(rs, "classname")) && (rs.getString("classname") != null)) {
                    provider.setClassName(rs.getString("classname").trim());
                }
                if ((hasColumn(rs, "address")) && (rs.getString("address") != null)){
                    provider.setAddress(rs.getString("address").trim());
                }
                if ((hasColumn(rs, "port")) && (rs.getString("port") != null)){
                    provider.setPort(rs.getInt("port"));
                }
                if ((hasColumn(rs, "timeout")) && (rs.getString("timeout") != null)){
                    provider.setTimeout(rs.getInt("timeout"));
                }
                if ((hasColumn(rs, "disabled")) && (rs.getString("disabled") != null)){
                    provider.setDisabled(rs.getBoolean("disabled"));
                }
                if ((hasColumn(rs, "disabled_response")) && (rs.getString("disabled_response") != null)){
                    provider.setDisabledResponse(rs.getString("disabled_response").trim());
                }
                if ((hasColumn(rs, "ref_id")) && (rs.getString("ref_id") != null)){
                    provider.setRefId(rs.getLong("ref_id"));
                }
                if ((hasColumn(rs, "H2HXml")) && (rs.getString("H2HXml") != null)) {
                    provider.setH2hXml(rs.getBoolean("H2HXml"));
                }
                if ((hasColumn(rs, "Check_Duplicate")) && (rs.getString("Check_Duplicate") != null)){
                    provider.setCheckDuplicate(rs.getBoolean("Check_Duplicate"));
                }
                if ((hasColumn(rs, "always_check")) && (rs.getString("always_check") != null)){
                    provider.setAlwaysCheck(rs.getBoolean("always_check"));
                }
                if ((hasColumn(rs, "UsesMapping")) && (rs.getString("UsesMapping") != null)){
                    provider.setUsesMapping(rs.getBoolean("UsesMapping"));
                }
                if ((hasColumn(rs, "TimeOutAsyncCalls")) && (rs.getString("TimeOutAsyncCalls") != null)){
                    provider.setTimeoutAsyncCalls(rs.getLong("TimeOutAsyncCalls"));
                }
                if ((hasColumn(rs, "display_name")) && (rs.getString("display_name") != null)){
                    provider.setDisplayName(rs.getString("display_name").trim());
                }
            }// else : result already null
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getProviderFromRs.Error while getting one provider from the resultset.", localException);
        }

        return provider;
    }

    public static List<Provider> getProviderListFromRs(ResultSet rs) {
        List<Provider> providerList = null;

        try {
            if (rs != null) {
                providerList = new ArrayList<Provider>();
                while (rs.next()) {
                    Provider newProvider = getProviderFromRs(rs);
                    if (newProvider != null) {
                        providerList.add(newProvider);
                    }
                }
                if (providerList.isEmpty()) {
                    providerList = null;
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getSimTypeList. Error while getting sim types for filling a list.", localException);
        }
        return providerList;
    }

    public static Provider getProviderById(long providerId) {
        Provider provider = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String sqlGetProviderById = sql_bundle.getString("getProviderById");
        logger.info(String.format("SQL getProviderById string :  [%s]", sqlGetProviderById));
        try {
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlGetProviderById);
            pstmt.setLong(1, providerId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                provider = getProviderFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("Error while getting provider from database", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return provider;
    }

    public static List<Provider> getProvidersByIdList(List<Long> providerIdList) {
        List<Provider> providerList = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            if ((providerIdList != null) && (!providerIdList.isEmpty())) {
                String sqlGetProviderByIdBase = sql_bundle.getString("getProvidersByIdList");
                String sqlGetProviderById = String.format("%s)",
                        StringUtil.addSqlParamWhildCards(providerIdList.size(), sqlGetProviderByIdBase));
                logger.info(String.format("SQL getProvidersByIdList string :  [%s]", sqlGetProviderById));
                dbConn = Torque.getConnection();
                pstmt = dbConn.prepareStatement(sqlGetProviderById);
                for (int i = 0; i < providerIdList.size(); i++) {
                    pstmt.setLong((i + 1), providerIdList.get(i));
                }
                rs = pstmt.executeQuery();
                providerList = getProviderListFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getProvidersByIdList. Error while getting provider list from database", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return providerList;
    }

    public static List<Provider> getAllProviders() {
        List<Provider> providerList = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sqlGetAllProviders = sql_bundle.getString("getAllProviders");
            logger.info(String.format("SQL getProvidersByIdList string :  [%s]", sqlGetAllProviders));
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlGetAllProviders);
            rs = pstmt.executeQuery();
            providerList = getProviderListFromRs(rs);
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getAllProviders. Error while getting provider list from database", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return providerList;
    }
}
