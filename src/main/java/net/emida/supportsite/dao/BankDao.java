/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dao;

import java.util.List;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import java.util.ResourceBundle;
import org.apache.torque.Torque;
import java.sql.PreparedStatement;
import net.emida.supportsite.dto.Bank;

/**
 *
 * @author fernandob
 */
public class BankDao {

    private static final Logger logger = Logger.getLogger(BankDao.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("net.emida.supportsite.dao.sql");

    private static Bank getBankFromRs(ResultSet rs) {
        Bank retValue = null;

        try {
            if (rs != null) {
                retValue = new Bank();

                if ((hasColumn(rs, "ID")) && (rs.getString("ID") != null)) {
                    retValue.setId(rs.getLong("ID"));
                }
                if ((hasColumn(rs, "Code")) && (rs.getString("Code") != null)) {
                    retValue.setCode(rs.getString("Code"));
                }
                if ((hasColumn(rs, "Name")) && (rs.getString("Name") != null)) {
                    retValue.setName(rs.getString("Name"));
                }
                if ((hasColumn(rs, "Status")) && (rs.getString("Status") != null)) {
                    retValue.setStatus(rs.getBoolean("Status"));
                }
                if ((hasColumn(rs, "idNew")) && (rs.getString("idNew") != null)) {
                    retValue.setIdNew(rs.getString("idNew"));
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getBankFromRs. Could not get bank information : " + localException.toString());
        }

        return retValue;
    }

    private static List<Bank> getBankList(String resourceId) {
        List<Bank> retValue = new ArrayList<Bank>();

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString(resourceId);
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Bank newBank = getBankFromRs(rs);
                    retValue.add(newBank);
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getAllBanks.Error while getting all banks", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static List<Bank> getAllBanks() {
        return getBankList("getAllBanks");
    }

    public static List<Bank> getAllEnabledBanks() {
        return getBankList("getAllEnabledBanks");
    }

}
