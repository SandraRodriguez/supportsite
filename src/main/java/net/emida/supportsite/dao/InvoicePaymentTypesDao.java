/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dao;

import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import net.emida.supportsite.dto.InvoicePaymentType;
import net.emida.supportsite.dto.InvoiceType;
import net.emida.supportsite.dto.RepInvoicePaymentType;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author fernandob
 */
public class InvoicePaymentTypesDao {

    private static final Logger logger = Logger.getLogger(InvoicePaymentTypesDao.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("net.emida.supportsite.dao.sql");

    private static InvoiceType getInvoiceTypeFromRs(ResultSet rs) {
        InvoiceType retValue = null;

        try {
            if (rs != null) {
                retValue = new InvoiceType();
                if ((hasColumn(rs, "id")) && (rs.getString("id") != null)) {
                    retValue.setId(rs.getLong("id"));
                }
                if ((hasColumn(rs, "description")) && (rs.getString("description") != null)) {
                    retValue.setDescription(rs.getString("description"));
                }
                if ((hasColumn(rs, "Status")) && (rs.getString("Status") != null)) {
                    retValue.setStatus(rs.getBoolean("Status"));
                }
                if ((hasColumn(rs, "Code")) && (rs.getString("Code") != null)) {
                    retValue.setCode(rs.getString("Code"));
                }
                if ((hasColumn(rs, "idNew")) && (rs.getString("idNew") != null)) {
                    retValue.setIdNew(rs.getString("idNew"));
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getInvoiceTypeFromRs. Could not get invoice type information : " + localException.toString());
        }

        return retValue;
    }

    public static List<InvoiceType> getAllInvoiceTypes() {
        List<InvoiceType> retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getAllInvoiceTypes");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs != null) {
                retValue = new ArrayList<InvoiceType>();
                while (rs.next()) {
                    InvoiceType newInvoiceType = getInvoiceTypeFromRs(rs);
                    retValue.add(newInvoiceType);
                }
                if (retValue.isEmpty()) {
                    retValue = null;
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getAllInvoiceTypes.Error while getting all invoice types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    public static List<InvoiceType> getEnabledInvoiceTypes() {
        List<InvoiceType> retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getEnabledInvoiceTypes");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs != null) {
                retValue = new ArrayList<InvoiceType>();
                while (rs.next()) {
                    InvoiceType newInvoiceType = getInvoiceTypeFromRs(rs);
                    retValue.add(newInvoiceType);
                }
                if (retValue.isEmpty()) {
                    retValue = null;
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getAllInvoiceTypes.Error while getting all invoice types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    public static InvoiceType getInvoiceTypeById(long invoiceTypeId) {
        InvoiceType retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getInvoiceTypeById");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setLong(1, invoiceTypeId);
            rs = pstmt.executeQuery();
            if ((rs != null) && (rs.next())) {
                retValue = getInvoiceTypeFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getInvoiceTypeById.Error while getting invoice type", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    private static InvoicePaymentType getInvoicePaymentTypeFromRs(ResultSet rs) {
        InvoicePaymentType retValue = null;

        try {
            if (rs != null) {
                retValue = new InvoicePaymentType();
                retValue.setId(rs.getLong("id"));
                retValue.setCode(rs.getString("code"));
                retValue.setDescription(rs.getString("description"));
                retValue.setStatus(rs.getBoolean("Status"));
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getInvoicePaymentTypeFromRs. Could not get invoice payment type information : " + localException.toString());
        }

        return retValue;
    }

    public static List<InvoicePaymentType> getAllInvoicePaymentTypes() {
        List<InvoicePaymentType> retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getAllInvoicePaymentTypes");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs != null) {
                retValue = new ArrayList<InvoicePaymentType>();
                while (rs.next()) {
                    InvoicePaymentType newInvoiceType = getInvoicePaymentTypeFromRs(rs);
                    retValue.add(newInvoiceType);
                }
                if (retValue.isEmpty()) {
                    retValue = null;
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getAllInvoicePaymentTypes.Error while getting all invoice payment types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    private static RepInvoicePaymentType getRepInvoicePaymentTypeFromRs(ResultSet rs) {
        RepInvoicePaymentType retValue = null;

        try {
            if (rs != null) {
                retValue = new RepInvoicePaymentType();
                retValue.setId(rs.getString("id"));
                retValue.setRepId(rs.getLong("repId"));
                retValue.setTypeId(rs.getLong("paymentTypeId"));
                retValue.setTypeDescription(rs.getString("typeDescription"));
                retValue.setAccountNumber(rs.getString("accountNumber"));
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getRepInvoicePaymentTypeFromRs. Could not get rep invoice payment type information : " + localException.toString());
        }

        return retValue;
    }

    public static List<RepInvoicePaymentType> getAllRepInvoicePaymentTypes(long repId) {
        List<RepInvoicePaymentType> retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getRepInvoicePaymentTypesByRepId");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setLong(1, repId);
            rs = pstmt.executeQuery();
            if (rs != null) {
                retValue = new ArrayList<RepInvoicePaymentType>();
                while (rs.next()) {
                    RepInvoicePaymentType newPaymentType = getRepInvoicePaymentTypeFromRs(rs);
                    retValue.add(newPaymentType);
                }
                if (retValue.isEmpty()) {
                    retValue = null;
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getAllRepInvoicePaymentTypes.Error while getting rep invoice payment types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    public static boolean insertRepInvoicePaymentType(RepInvoicePaymentType invoicePamentType) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("insertRepInvoicePaymentType");
            pstmt = dbConn.prepareStatement(sql); //dbConn.prepareCall(sql);
            pstmt.setLong(1, invoicePamentType.getRepId());
            pstmt.setLong(2, invoicePamentType.getTypeId());
            logger.debug(invoicePamentType.getAccountNumber());
            pstmt.setString(3, invoicePamentType.getAccountNumber());
            pstmt.executeUpdate();
            retValue = true;
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("insertRepInvoicePaymentTpe.Error while getting rep invoice payment types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
        }
        return retValue;
    }

    public static boolean updateRepInvoicePaymentType(RepInvoicePaymentType invoicePamentType) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("updateRepInvoicePaymentType");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setLong(1, invoicePamentType.getTypeId());
            pstmt.setString(2, invoicePamentType.getAccountNumber());
            pstmt.setString(3, invoicePamentType.getId());
            pstmt.setLong(4, invoicePamentType.getRepId());
            pstmt.executeUpdate();
            retValue = true;
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("updateRepInvoicePaymentTpe.Error while getting rep invoice payment types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
        }
        return retValue;
    }

    public static boolean deleteRepInvoicePaymentTypesByRepId(long repId) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("deleteRepInvoicePaymentTypesByRep");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setLong(1, repId);
            pstmt.executeUpdate();
            retValue = true;
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("deleteRepInvoicePaymentTypesByRepId.Error while getting rep invoice payment types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
        }
        return retValue;
    }

    public static boolean deleteRepInvoicePaymentTypeById(String repPaymentTypeId) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("deleteRepInvoicePaymentTypeById");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, repPaymentTypeId);
            pstmt.executeUpdate();
            retValue = true;
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("deleteRepInvoicePaymentTypeById.Error while getting rep invoice payment types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
        }
        return retValue;
    }

}
