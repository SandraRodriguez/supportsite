/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dao;

import java.util.List;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.ArrayList;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import java.util.ResourceBundle;
import org.apache.torque.Torque;
import java.sql.PreparedStatement;
import net.emida.supportsite.dto.DepositType;

/**
 *
 * @author fernandob
 */
public class DepositTypeDao {

    private static final Logger logger = Logger.getLogger(DepositTypeDao.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("net.emida.supportsite.dao.sql");

    private static DepositType getDepositTypeFromRs(ResultSet rs) {
        DepositType retValue = null;

        try {
            if (rs != null) {
                retValue = new DepositType();
                if ((hasColumn(rs, "id")) && (rs.getString("id") != null)) {
                    retValue.setId(rs.getString("id"));
                }
                if ((hasColumn(rs, "descriptionEnglish")) && (rs.getString("descriptionEnglish") != null)) {
                    retValue.setDescriptionEnglish(rs.getString("descriptionEnglish"));
                }
                if ((hasColumn(rs, "descriptionSpanish")) && (rs.getString("descriptionSpanish") != null)) {
                    retValue.setDescriptionSpanish(rs.getString("descriptionSpanish"));
                }
                if ((hasColumn(rs, "externalCode")) && (rs.getString("externalCode") != null)) {
                    retValue.setExternalCode(rs.getString("externalCode"));
                }
                if ((hasColumn(rs, "depositCode")) && (rs.getString("depositCode") != null)) {
                    retValue.setDepositCode(rs.getString("depositCode"));
                }
                if ((hasColumn(rs, "transferAccount")) && (rs.getString("transferAccount") != null)) {
                    retValue.setTransferAccount(rs.getBoolean("transferAccount"));
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getDepositTypeFromRs. Could not get deposit type information : " + localException.toString());
        }

        return retValue;
    }

    public static List<DepositType> getAllDepositTypes() {
        List<DepositType> retValue = new ArrayList<DepositType>();

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getAllDepositTypes");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    DepositType newDepositType = getDepositTypeFromRs(rs);
                    retValue.add(newDepositType);
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getAllDepositTypes.Error while getting all deposit types", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static List<DepositType> getDepositTypesByBankId(long bankId) {
        List<DepositType> retValue = new ArrayList<DepositType>();

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getDepositTypesByBankId");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setLong(1, bankId);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    DepositType newDepositType = getDepositTypeFromRs(rs);
                    retValue.add(newDepositType);
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getDepositTypesByBankId.Error while getting deposit types by bank id", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }

}
