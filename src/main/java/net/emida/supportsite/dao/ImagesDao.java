/*
 * EMIDA all rights reserved 1999-2025.
 */
package net.emida.supportsite.dao;

import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import net.emida.supportsite.dto.ImageType;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author fernandob
 */
public class ImagesDao {

    private static final Logger LOG = Logger.getLogger(ImagesDao.class);

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("net.emida.supportsite.dao.sql");

    private static ImageType getImageTypeFromRs(ResultSet rs) {
        ImageType retValue = null;

        try {
            if (rs != null) {
                retValue = new ImageType();
                if ((hasColumn(rs, "id")) && (rs.getString("id") != null)) {
                    retValue.setId(rs.getString("id"));
                }
                if ((hasColumn(rs, "code")) && (rs.getString("code") != null)) {
                    retValue.setCode(rs.getString("code"));
                }
                if ((hasColumn(rs, "typeDescription")) && (rs.getString("typeDescription") != null)) {
                    retValue.setDescription(rs.getString("typeDescription"));
                }
                if ((hasColumn(rs, "suggestedSize")) && (rs.getString("suggestedSize") != null)) {
                    retValue.setSuggestedSize(rs.getString("suggestedSize"));
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            LOG.error("getImageTypeFromRs. Could not get image type information : " + localException.toString());
        }

        return retValue;
    }

    public static ImageType getImageTypeById(String imageTypeId) {
        ImageType retValue = null;

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String sqlGetProviderById = sql_bundle.getString("getImageTypeById");
        LOG.info(String.format("SQL getImageTypeById string :  [%s] | %s", sqlGetProviderById, imageTypeId));
        try {
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlGetProviderById);
            pstmt.setString(1, imageTypeId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = getImageTypeFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            LOG.error("Error while getting image type from database", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    public static ImageType getImageTypeByCode(String code) {
        ImageType retValue = null;

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String sqlGetProviderById = sql_bundle.getString("getImageTypeByCode");
        LOG.info(String.format("SQL getImageTypeByCode string :  [%s] | %s", sqlGetProviderById, code));
        try {
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlGetProviderById);
            pstmt.setString(1, code);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = getImageTypeFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            LOG.error("Error while getting image type from database", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    public static Map<String, ImageType> getAllImageTypes() {
        Map<String, ImageType> retValue = new LinkedHashMap<>();

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getAllImageTypes");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    ImageType newImageType = getImageTypeFromRs(rs);
                    retValue.put(newImageType.getId(), newImageType);
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            LOG.error("getAllImageTypes.Error while getting all image types set", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }
}
