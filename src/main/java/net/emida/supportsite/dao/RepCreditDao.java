/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dao;

import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import com.debisys.utils.NumberUtil;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import net.emida.supportsite.dto.RepCredit;
import net.emida.supportsite.dto.RepCreditPaymentData;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author fernandob
 */
public class RepCreditDao {

    private static final Logger logger = Logger.getLogger(RepCreditDao.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("net.emida.supportsite.dao.sql");

    private static RepCredit getRepCreditFromRs(ResultSet rs) {
        RepCredit retValue = null;

        try {
            if (rs != null) {
                retValue = new RepCredit();
                if ((hasColumn(rs, "id")) && (rs.getString("id") != null)) {
                    retValue.setId(rs.getLong("id"));
                }
                if ((hasColumn(rs, "rep_id")) && (rs.getString("rep_id") != null)) {
                    retValue.setRepId(rs.getLong("rep_id"));
                }
                if ((hasColumn(rs, "logon_id")) && (rs.getString("logon_id") != null)) {
                    retValue.setLogonId(rs.getString("logon_id"));
                }
                if ((hasColumn(rs, "description")) && (rs.getString("description") != null)) {
                    retValue.setDescription(rs.getString("description"));
                }
                if ((hasColumn(rs, "amount")) && (rs.getString("amount") != null)) {
                    retValue.setAmount(rs.getBigDecimal("amount"));
                }
                if ((hasColumn(rs, "credit_limit")) && (rs.getString("credit_limit") != null)) {
                    retValue.setCreditLimit(rs.getBigDecimal("credit_limit"));
                }
                if ((hasColumn(rs, "prior_credit_limit")) && (rs.getString("prior_credit_limit") != null)) {
                    retValue.setPriorCreditLimit(rs.getBigDecimal("prior_credit_limit"));
                }
                if ((hasColumn(rs, "available_credit")) && (rs.getString("available_credit") != null)) {
                    retValue.setAvailableCredit(rs.getBigDecimal("available_credit"));
                }
                if ((hasColumn(rs, "prior_available_credit")) && (rs.getString("prior_available_credit") != null)) {
                    retValue.setPriorAvailableCredit(rs.getBigDecimal("prior_available_credit"));
                }
                if ((hasColumn(rs, "commission")) && (rs.getString("commission") != null)) {
                    retValue.setCommission(rs.getBigDecimal("commission"));
                }
                if ((hasColumn(rs, "datetime")) && (rs.getString("datetime") != null)) {
                    retValue.getDatetime().setTime(rs.getTimestamp("datetime"));
                }
                if ((hasColumn(rs, "entityAccountType")) && (rs.getString("entityAccountType") != null)) {
                    retValue.setEntityAccountType(rs.getLong("entityAccountType"));
                }
                if ((hasColumn(rs, "idNew")) && (rs.getString("idNew") != null)) {
                    retValue.setIdNew(rs.getString("idNew"));
                }
                if ((hasColumn(rs, "depositDate")) && (rs.getString("depositDate") != null)) {
                    retValue.getDepositDate().setTime(rs.getTimestamp("depositDate"));
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            logger.error("getRepCreditFromRs. Could not get rep credit information : " + localException.toString());
        }

        return retValue;
    }

    public static RepCredit getRepCreditById(long id) {
        RepCredit provider = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String sqlGetProviderById = sql_bundle.getString("getRepCreditById");
        logger.info(String.format("SQL getRepCreditById string :  [%s] | %d", sqlGetProviderById, id));
        try {
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlGetProviderById);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                provider = getRepCreditFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("Error while getting rep credit from database", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return provider;
    }

    public static RepCredit getRepCreditByIdNew(String idNew) {
        RepCredit provider = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String sqlGetProviderById = sql_bundle.getString("getRepCreditByIdNew");
        logger.info(String.format("SQL getRepCreditById string :  [%s] | %s", sqlGetProviderById, idNew));
        try {
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlGetProviderById);
            pstmt.setString(1, idNew);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                provider = getRepCreditFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("Error while getting rep credit from database", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return provider;
    }

    public static List<RepCredit> getRepCreditByDateAndRepId(long repId,
            String startDate, String endDate) {
        List<RepCredit> retValue = null;

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection();
            String sql = sql_bundle.getString("getRepCreditByDateAndRepId");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setLong(1, repId);
            pstmt.setString(2, startDate);
            pstmt.setString(3, endDate);
            rs = pstmt.executeQuery();
            if (rs != null) {
                retValue = new ArrayList<RepCredit>();
                while (rs.next()) {
                    RepCredit newDepositType = getRepCreditFromRs(rs);
                    retValue.add(newDepositType);
                }
                if (retValue.isEmpty()) {
                    retValue = null;
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("getRepCreditByDateAndRepId.Error while getting rep credit set", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }

    /**
     * For some reason there are direct insertions into the rep_credits table.
     * The safe way should be using the applyRepPayment stored procedure
     *
     * @param repCredit
     * @return
     */
    public static boolean insertRepCredit(RepCredit repCredit) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        String sqlInsertRepCredit = sql_bundle.getString("insertRepCredit");
        List<String> paramValues = new ArrayList<String>();
        paramValues.add(String.valueOf(repCredit.getRepId()));
        paramValues.add(repCredit.getLogonId());
        paramValues.add(repCredit.getDescription());
        paramValues.add(repCredit.getAmount().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(repCredit.getCreditLimit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(repCredit.getPriorCreditLimit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(repCredit.getAvailableCredit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(repCredit.getPriorAvailableCredit().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(repCredit.getCommission().setScale(4, RoundingMode.DOWN).toPlainString());
        paramValues.add(DateUtil.formatCalendarDate(repCredit.getDatetime(), null));
        paramValues.add(String.valueOf(repCredit.getEntityAccountType()));
        paramValues.add(DateUtil.formatCalendarDate(repCredit.getDepositDate(), null));
        DbUtil.printSqlString(sqlInsertRepCredit, paramValues);
        try {
            dbConn = Torque.getConnection();
            pstmt = dbConn.prepareStatement(sqlInsertRepCredit);
            pstmt.setLong(1, repCredit.getRepId());
            pstmt.setString(2, repCredit.getLogonId());
            pstmt.setString(3, repCredit.getDescription());
            pstmt.setBigDecimal(4, repCredit.getAmount());
            pstmt.setBigDecimal(5, repCredit.getCreditLimit());
            pstmt.setBigDecimal(6, repCredit.getPriorCreditLimit());
            pstmt.setBigDecimal(7, repCredit.getAvailableCredit());
            pstmt.setBigDecimal(8, repCredit.getPriorAvailableCredit());
            pstmt.setBigDecimal(9, repCredit.getCommission());
            pstmt.setString(10, DateUtil.formatCalendarDate(repCredit.getDatetime(), null));
            pstmt.setLong(11, repCredit.getEntityAccountType());
            pstmt.setString(12, DateUtil.formatCalendarDate(repCredit.getDepositDate(), null));
            retValue = (pstmt.executeUpdate() == 1);
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("Error while inserting a new rep payment record", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
        }
        return retValue;

    }

    public static long applyRepPayment(long repId, BigDecimal paymentAmount,
            BigDecimal commission, String paymentDescription, String userName,
            int deploymentType, int customConfigType, String depositDate) {
        long retValue = -1;
        Connection dbConn = null;
        CallableStatement cstmt = null;

        String sqlInsertSim = sql_bundle.getString("applyRepPayment");
        try {
            dbConn = Torque.getConnection();
            cstmt = dbConn.prepareCall(sqlInsertSim);
            cstmt.setLong("RepID", repId);
            cstmt.setBigDecimal("PaymentAmount", paymentAmount);
            cstmt.setBigDecimal("Commission", commission);
            cstmt.setString("PaymentDescription", paymentDescription);
            cstmt.setString("UserName", userName);
            cstmt.setInt("DeploymentType", deploymentType);
            cstmt.setInt("CustomConfigType", customConfigType);
            cstmt.setString("DepositDate", depositDate);
            // Register output parameters
            cstmt.registerOutParameter("ErrorCode", java.sql.Types.INTEGER);
            cstmt.setInt("ErrorCode", -1);
            cstmt.registerOutParameter("ErrorDesc", java.sql.Types.VARCHAR);
            cstmt.setString("ErrorDesc", "");

            cstmt.executeUpdate();

            // Read output parámeters
            int errorCode = cstmt.getInt("ErrorCode");
            String errorDescription = cstmt.getString("ErrorDesc");
            logger.debug(String.format("Add rep payment result. Code = [%d] | Description = [%s]",
                    errorCode, errorDescription));
            if ((errorCode == 0) && (errorDescription.contains("-"))) {
                String[] errorDescriptionComponents = errorDescription.split("-");
                if ((errorDescriptionComponents.length == 2)
                        && (NumberUtil.isNumeric(errorDescriptionComponents[1]))) {
                    retValue = Long.valueOf(errorDescriptionComponents[1]);
                }
            }

        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            logger.error("Error while inserting a new rep payment", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, cstmt, null);
        }
        return retValue;
    }

    public static boolean insertRepCreditPaymentData(RepCreditPaymentData paymentData) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        if ((paymentData != null) && (paymentData.isValid())) {

            String sqlInsertSim = sql_bundle.getString("insertRepCreditPaymentData");
            try {
                dbConn = Torque.getConnection();
                pstmt = dbConn.prepareStatement(sqlInsertSim);
                pstmt.setString(1, paymentData.getRepCreditId());
                pstmt.setString(2, paymentData.getBankId());
                pstmt.setString(3, paymentData.getDepositTypeId());
                pstmt.setString(4, paymentData.getInvoiceTypeId());
                pstmt.setString(5, paymentData.getAccountNumber());
                pstmt.setString(6, paymentData.getDocumentNumber());

                retValue = (pstmt.executeUpdate() == 1);

            } catch (SQLException localSqlException) {
                DbUtil.processSqlException(localSqlException);
            } catch (Exception e) {
                logger.error("Error while inserting a new rep payment data", e);
            } finally {
                DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
            }
        }else{
            logger.error("Payment data is incomplete, no insertion could be done");
        }
        return retValue;
    }

}
