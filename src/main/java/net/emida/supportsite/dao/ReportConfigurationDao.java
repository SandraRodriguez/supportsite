/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dao;

import net.emida.supportsite.dto.ReportConfiguration;
import net.emida.supportsite.dto.ReportConfigurationByResponseType;
import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author fernandob
 */
public class ReportConfigurationDao {

    private static final Logger logger = Logger.getLogger(ReportConfigurationDao.class);

    private static ReportConfiguration getReportConfigurationFromRs(ResultSet rs) {
        ReportConfiguration retValue = null;

        try {
            if (rs != null) {
                retValue = new ReportConfiguration();
                if (hasColumn(rs, "reportId")) {
                    retValue.setReportId(rs.getString("reportId"));
                }
                if (hasColumn(rs, "reportCode")) {
                    retValue.setReportCode(rs.getString("reportCode"));
                }
                if (hasColumn(rs, "reportBackwardDateLimit")) {
                    retValue.setReportBackwardDateLimit(rs.getInt("reportBackwardDateLimit"));
                }
            }
        } catch (Exception localException) {
            logger.error("getReportConfigurationFromRs. Error while getting report configuration info", localException);
        }

        return retValue;
    }

    public static ReportConfiguration getReportConfigurationByReportCode(String reportCode) {
        ReportConfiguration retValue = null;

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection();
            StringBuilder sqlStringB = new StringBuilder();

            sqlStringB.append(" SELECT * FROM [dbo].[reportConfiguration] WITH(NOLOCK) ");
            sqlStringB.append(" WHERE [reportCode] = ? ");
            String sqlString = sqlStringB.toString();

            pstmt = dbConn.prepareStatement(sqlString);
            pstmt.setString(1, reportCode);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = getReportConfigurationFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (TorqueException localTorqueException) {
            logger.error("General torque error", localTorqueException);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    public static ReportConfigurationByResponseType getReportConfigurationByTerminalResponseType(String reportConfigurationId,
            long terminalResponseTypeId) {
        ReportConfigurationByResponseType retValue = null;

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection();
            StringBuilder sqlStringB = new StringBuilder();

            sqlStringB.append(" SELECT * ");
            sqlStringB.append(" FROM   [dbo].[ReportConfigurationByResponseType] WITH(NOLOCK) ");
            sqlStringB.append(" WHERE  [reportConfigurationId] = ? AND [terminalResponseTypeId] = ?");
            String sqlString = sqlStringB.toString();

            pstmt = dbConn.prepareStatement(sqlString);
            pstmt.setString(1, reportConfigurationId);
            pstmt.setLong(2, terminalResponseTypeId);
            
            logger.info(String.format("%s . Params : Report Configuration Id = %s | Terminal Response Type Id = %d", sqlString, reportConfigurationId, terminalResponseTypeId));

            rs = pstmt.executeQuery();
            if(rs.next()){
                retValue = getReportConfigurationByTerminalResponseTypeFromRs(rs);
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (TorqueException localTorqueException) {
            logger.error("General torque error", localTorqueException);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }

        return retValue;
    }

    private static ReportConfigurationByResponseType getReportConfigurationByTerminalResponseTypeFromRs(ResultSet rs) {
        ReportConfigurationByResponseType retValue = null;

        try {
            if (rs != null) {
                retValue = new ReportConfigurationByResponseType();
                if (hasColumn(rs, "id")) {
                    retValue.setId(rs.getString("id"));
                }
                if (hasColumn(rs, "reportConfigurationId")) {
                    retValue.setReportConfigurationId(rs.getString("reportConfigurationId"));
                }
                if (hasColumn(rs, "terminalTypeId")) {
                    retValue.setTerminalResponseTypeId(rs.getLong("terminalResponseTypeId"));
                }
                if (hasColumn(rs, "retrieverClassName")) {
                    retValue.setRetrieverClassName(rs.getString("retrieverClassName"));
                }
                if (hasColumn(rs, "formatterClassName")) {
                    retValue.setFormatterClassName(rs.getString("formatterClassName"));
                }
            }
        } catch (SQLException localException) {
            logger.error("getReportConfigurationByTerminalResponseTypeFromRs. Error while getting report configuration by terminal info", localException);
            DbUtil.processSqlException(localException);
        }

        return retValue;
    }

}
