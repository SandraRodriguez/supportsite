package net.emida.supportsite.loyaltyPointsProgram;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.loyaltyPointsProgram.dao.LoyaltyProgramModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author dgarzon
 */
public interface ILoyaltyProgramDao {
    public List<LoyaltyProgramModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    public Long count(Map<String, Object> queryData) throws TechnicalException;
}
