/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.loyaltyPointsProgram;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import net.emida.supportsite.loyaltyPointsProgram.dao.LoyaltyProgramModel;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dgarzon
 */

@Controller
@RequestMapping(value = UrlPaths.LOYALTY_PATH)
public class LoyaltyProgramController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(LoyaltyProgramController.class);

    @Autowired
    private ILoyaltyProgramDao iLoyaltyProgramDao;

    @RequestMapping(value = UrlPaths.LOYALTY_PROGRAM_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute LoyaltyProgramForm reportModel) {

        fillLoyaltyProgram(model);
        return "reports/loyaltyProgram/loyaltyProgram";
    }

    @RequestMapping(value = UrlPaths.LOYALTY_PROGRAM_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid LoyaltyProgramForm reportModel, BindingResult result) throws TechnicalException, ApplicationException, ParseException {

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillLoyaltyProgram(model.asMap());
            return "reports/loyaltyProgram/loyaltyProgram";
        }

        log.debug("reportBuild {}", result);
        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        String formattedStartDate = reportModel.getStartDate();
        String formattedEndDate = reportModel.getStartDate();

        model.addAttribute("startDate", reportModel.getStartDate());
        model.addAttribute("endDate", reportModel.getEndDate());
        model.addAttribute("name", reportModel.getName());
        Map<String, Object> queryData = fillQueryData(formattedStartDate, formattedEndDate, reportModel.getName());

        Long totalRecords = iLoyaltyProgramDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);
        
        return "reports/loyaltyProgram/loyaltyProgram";
    }

    @RequestMapping(method = RequestMethod.POST, value = UrlPaths.LOYALTY_PROGRAM_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<LoyaltyProgramModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String startDate, @RequestParam String endDate, @RequestParam String name) throws ParseException {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. StartDate={}. EndDate={}. ", startDate, endDate);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);
        
        Map<String, Object> queryData = fillQueryData(startDate, endDate, name);
        List<LoyaltyProgramModel> list = iLoyaltyProgramDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);
        
        return list;
    }

    private Map<String, Object> fillQueryData(String startDate, String endDate, String name) throws ParseException {       
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put("name", name);
        return queryData;
    }

    @Override
    public int getSection() {
        return 17;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

    /**
     * Fill the selects type inputs
     *
     * @param model
     */
    private void fillLoyaltyProgram(Map model) {
        fillMerchantsFilter(model);
    }
    
    private void fillMerchantsFilter(Map model){
        try {
            model.put("strAccessLevel", getAccessLevel());
            model.put("strRefId", getRefId());
            model.put("strDistChainType", getDistChainType());
        } catch (TechnicalException e) {
            log.error("Error setting merchant variables. Ex = {}", e);
        }
    }


}
