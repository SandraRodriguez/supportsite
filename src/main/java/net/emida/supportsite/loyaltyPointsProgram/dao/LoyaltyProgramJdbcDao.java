
package net.emida.supportsite.loyaltyPointsProgram.dao;

import net.emida.supportsite.loyaltyPointsProgram.ILoyaltyProgramDao;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dgarzon
 */
@Repository
public class LoyaltyProgramJdbcDao implements ILoyaltyProgramDao {

    private static final Logger log = LoggerFactory.getLogger(LoyaltyProgramJdbcDao.class);

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll() {
        try {
            return 0L;
        } catch (DataAccessException ex) {
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<LoyaltyProgramModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {

        try {
            LoyaltyProgramQueryBuilder queryBuilder = new LoyaltyProgramQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();
            log.debug("Query : {}", SQL_QUERY);
            return jdbcTemplate.query(SQL_QUERY, new LoyaltyProgramMapper());

        } catch (DataAccessException ex) {
            log.error("Data access error in list loyalty program");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in list loyalty program");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {
            LoyaltyProgramQueryBuilder queryBuilder = new LoyaltyProgramQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();
            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            //return 7L;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count loyalty program");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count loyalty program");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }


    


}
