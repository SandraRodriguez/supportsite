package net.emida.supportsite.loyaltyPointsProgram.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author dgarzon
 */
public class LoyaltyProgramMapper implements RowMapper<LoyaltyProgramModel>{

    @Override
    public LoyaltyProgramModel mapRow(ResultSet rs, int i) throws SQLException {
        LoyaltyProgramModel model = new LoyaltyProgramModel();
        model.setRwn(rs.getBigDecimal("rwn"));
        model.setId(rs.getString("id"));

        model.setStartDate(rs.getTimestamp("startDate"));
        model.setEndDate(rs.getTimestamp("endDate"));
        model.setName(rs.getString("name"));
        model.setActive(rs.getBoolean("active"));
        
        return model;
    }
    
}
