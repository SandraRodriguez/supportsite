package net.emida.supportsite.loyaltyPointsProgram.dao;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dgarzon
 */
public class LoyaltyProgramQueryBuilder {
    private static final Logger log = LoggerFactory.getLogger(LoyaltyProgramQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    
    private String startDate;
    private String endDate;
    private String name;
    private String isoId;

    /**
     *
     * @param queryData
     */
    public LoyaltyProgramQueryBuilder(Map<String, Object> queryData) {
        this.isoId = (String) queryData.get("refId");
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.name = (String) queryData.get("name");
        log.debug("Query Map = {}", queryData);
    }

    public LoyaltyProgramQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.isoId = (String) queryData.get("refId");
        this.name = (String) queryData.get("name");
        
    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery());
        sb0.append(") AS tbl1 ");
        return sb0.toString();
    }

    public String mainQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append(" SELECT id as Id, startDate as StartDate, endDate as endDate, name as Name, active as Active");
        mainQuery.append(" FROM loyaltyPointsProgram WITH(nolock) WHERE carrierId = "+isoId+" ");
        if(startDate != null && !startDate.trim().isEmpty()){
            mainQuery.append(" AND startDate >= '"+startDate+"' ");
        }
        if(endDate != null && !endDate.trim().isEmpty()){
            mainQuery.append(" AND endDate <= '"+endDate+"' ");
        }
        if(name != null && !name.trim().isEmpty()){
            mainQuery.append(" AND name like '%"+name+"%' ");
        }
        System.out.println("Main Query loyalty : "+mainQuery.toString());
        return mainQuery.toString();
    }


    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "name";
            sortOrder = " ASC ";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS rwn, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE rwn BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }


}
