package net.emida.supportsite.loyaltyPointsProgram.dao;

import com.debisys.utils.DateUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author dgarzon
 */
public class LoyaltyProgramModel {

    private BigDecimal rwn;
    private String id;
    private Timestamp startDate;
    private Timestamp endDate;
    private String name;
    private boolean active;    

    public BigDecimal getRwn() {
        return rwn;
    }

    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDate() {
        return DateUtil.formatDateTime_DB(startDate.toString());
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return DateUtil.formatDateTime_DB(endDate.toString());
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
