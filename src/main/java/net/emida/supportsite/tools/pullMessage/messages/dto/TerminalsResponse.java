/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.dto;

/**
 *
 * @author emida
 */
public class TerminalsResponse {
    
    private String terminalTypeId;
    private String terminalName;

    public TerminalsResponse(String terminalTypeId, String terminalName) {
        this.terminalTypeId = terminalTypeId;
        this.terminalName = terminalName;
    }

    public String getTerminalTypeId() {
        return terminalTypeId;
    }

    public void setTerminalTypeId(String terminalTypeId) {
        this.terminalTypeId = terminalTypeId;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }
}
