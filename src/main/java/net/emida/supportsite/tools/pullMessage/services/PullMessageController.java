package net.emida.supportsite.tools.pullMessage.services;

import com.debisys.utils.JSONException;
import com.debisys.utils.JSONObject;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.pullMessage.dto.PullMessageDto;
import net.emida.supportsite.tools.pullMessage.repository.IPullMessageDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez@emida.net
 * @since 20/03/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4395
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class PullMessageController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(PullMessageController.class);

    @Autowired
    private IPullMessageDao dao;

    @RequestMapping(value = UrlPaths.PULL_MESSAGE_NOTIFY, method = RequestMethod.GET)
    public String __init__() {
        return "tools/pullMessage/pullMessage";
    }

    @RequestMapping(value = UrlPaths.PULL_MESSAGE_NOTIFY + "/getLast", method = RequestMethod.GET)
    @ResponseBody
    public List<PullMessageDto> getLast(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return dao.getLast();
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.PULL_MESSAGE_NOTIFY + "/getByRange", method = RequestMethod.GET)
    @ResponseBody
    public List<PullMessageDto> getByRange(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return dao.getByRange(startDt, endDt);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.PULL_MESSAGE_NOTIFY + "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> add(@RequestParam("subject") String subject, @RequestParam("content") String content, @RequestParam("startDt") String startDt, @RequestParam("endDt") String endDt,
            @RequestParam("status") boolean status, @RequestParam("businessUnit") String businessUnit, @RequestParam("channel") String channel,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            PullMessageDto pullMessageDto = new PullMessageDto(subject, content, getTimeL(startDt), getTimeL(endDt), status, businessUnit, channel);
            JSONObject object = new JSONObject();
            object.put("response", dao.add(pullMessageDto));
            return new ResponseEntity<String>(object.toString(), HttpStatus.OK);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (JSONException ex) {
            LOG.error(ex.getLocalizedMessage());
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = UrlPaths.PULL_MESSAGE_NOTIFY + "/update", method = RequestMethod.POST)
    public ResponseEntity<String> update(@RequestParam("subject") String subject, @RequestParam("content") String content, @RequestParam("startDt") String startDt, @RequestParam("endDt") String endDt,
            @RequestParam("status") boolean status, @RequestParam("id") String id, @RequestParam("businessUnit") String businessUnit, @RequestParam("channel") String channel,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            PullMessageDto pullMessageDto = new PullMessageDto(id, subject, content, getTimeL(startDt), getTimeL(endDt), status, businessUnit, channel);
            JSONObject object = new JSONObject();
            object.put("response", dao.update(pullMessageDto));
            return new ResponseEntity<String>(object.toString(), HttpStatus.OK);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (JSONException ex) {
            LOG.error(ex.getLocalizedMessage());
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = UrlPaths.PULL_MESSAGE_NOTIFY + "/remove", method = RequestMethod.POST)
    public ResponseEntity<String> remove(@RequestParam("id") String id, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            JSONObject object = new JSONObject();
            object.put("response", dao.remove(id));
            return new ResponseEntity<String>(object.toString(), HttpStatus.OK);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (JSONException ex) {
            LOG.error(ex.getLocalizedMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Timestamp getTimeL(String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(date.concat(" 00:00:00.000"));

            return new java.sql.Timestamp(parsedDate.getTime());
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage());
        }
        return null;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
