package net.emida.supportsite.tools.pullMessage.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import net.emida.supportsite.tools.pullMessage.dto.PullMessageDto;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez@emida.net
 * @since 20/03/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4395
 */
@Repository("IPullMessageDao")
public class PullMessageRepository implements IPullMessageDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(PullMessageRepository.class);

    @Override
    public Integer add(PullMessageDto pullMessage) {
        String endDt = pullMessage.getEndDt().toString().substring(0, 10).concat(" 23:59:59");
        final String insert = "INSERT INTO PullMessage(subject, content, startDt, endDt, status, businessUnit, channel) "
                + "VALUES('" + pullMessage.getSubject() + "','"
                + pullMessage.getContent() + "','"
                + pullMessage.getStartDt() + "','"
                + endDt + "',"
                + (pullMessage.isStatus() ? 1 : 0) + ", '"
                + pullMessage.getBusinessUnit() + "','"
                + pullMessage.getChannel() + "')";
        LOG.info("insert :: " + insert);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(insert);
        } catch (Exception e) {
            LOG.error("Error on add PullMessage. Ex = {}", e);
        }
        return rows;
    }

    @Override
    public Integer update(PullMessageDto pullMessage) {
        String endDt = pullMessage.getEndDt().toString().substring(0, 10).concat(" 23:59:59");
        final String update = "UPDATE PullMessage SET "
                + " subject = '" + pullMessage.getSubject() + "',"
                + " content = '" + pullMessage.getContent() + "',"
                + " startDt = '" + pullMessage.getStartDt() + "',"
                + " endDt   = '" + endDt + "',"
                + " status  = " + (pullMessage.isStatus() ? 1 : 0) + ", "
                + " businessUnit = '" + pullMessage.getBusinessUnit() + "',"
                + " channel = '" + pullMessage.getChannel()+ "'"
                + " WHERE id = '" + pullMessage.getId() + "'";
        LOG.info("update :: " + update);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(update);
        } catch (Exception e) {
            LOG.error("Error on update PullMessage. Ex = {}", e);
        }
        return rows;
    }

    @Override
    public Integer remove(String id) {
        int rowsAfected = 0;
        String remove = "DELETE PullMessagePulled WHERE id_pullMessage = '" + id + "'";
        LOG.info("remove :: " + remove);
        rowsAfected = this.jdbcTemplate.update(remove);

        remove = "DELETE PullMessage WHERE id = '" + id + "'";
        LOG.info("remove :: " + remove);
        rowsAfected += this.jdbcTemplate.update(remove);

        try {
            rowsAfected += this.jdbcTemplate.update(remove);
        } catch (Exception e) {
            LOG.error("Error on remove PullMessage. Ex = {}", e);
        }
        return rowsAfected;
    }

    @Override
    public List<PullMessageDto> getLast() {
        final String query = "SELECT TOP 15 * FROM PullMessage ORDER BY createDt DESC";
        List<PullMessageDto> list = null;
        LOG.info("query :: " + query);
        try {
            list = jdbcTemplate.query(query, new RowMapper<PullMessageDto>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public PullMessageDto mapRow(ResultSet rs, int index) throws SQLException {
                    return new PullMessageDto(
                            rs.getString("id"),
                            rs.getString("subject"),
                            rs.getString("content"),
                            rs.getTimestamp("createDt"),
                            rs.getTimestamp("lastUpdateDt"),
                            rs.getTimestamp("startDt"),
                            rs.getTimestamp("endDt"),
                            rs.getBoolean("status"),
                            rs.getString("businessUnit"),
                            rs.getString("channel"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on getLast PullMessage. Ex = {}", e);
        }
        return list;
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public PullMessageDto getById(String id) {
        final String query = "SELECT * FROM PullMessage WHERE id = '" + id + "'";
        LOG.info("query :: " + query);
        PullMessageDto dto = null;
        try {
            dto = jdbcTemplate.queryForObject(query, new RowMapper<PullMessageDto>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public PullMessageDto mapRow(ResultSet rs, int index) throws SQLException {
                    return new PullMessageDto(
                            rs.getString("id"),
                            rs.getString("subject"),
                            rs.getString("content"),
                            rs.getTimestamp("createDt"),
                            rs.getTimestamp("lastUpdateDt"),
                            rs.getTimestamp("startDt"),
                            rs.getTimestamp("endDt"),
                            rs.getBoolean("status"),
                            rs.getString("businessUnit"),
                            rs.getString("channel"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on getById PullMessage. Ex = {}", e);
        }
        return dto;
    }

    /**
     *
     * @param startDt
     * @param endDt
     * @return
     */
    @Override
    public List<PullMessageDto> getByRange(String startDt, String endDt) {
        final String query = "SELECT * FROM PullMessage WHERE createDt BETWEEN '" + startDt + "' AND DATEADD(DAY, 1, '" + endDt + "')";
        LOG.info("query :: " + query);
        List<PullMessageDto> list = null;
        try {
            list = jdbcTemplate.query(query, new RowMapper<PullMessageDto>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public PullMessageDto mapRow(ResultSet rs, int index) throws SQLException {
                    return new PullMessageDto(
                            rs.getString("id"),
                            rs.getString("subject"),
                            rs.getString("content"),
                            rs.getTimestamp("createDt"),
                            rs.getTimestamp("lastUpdateDt"),
                            rs.getTimestamp("startDt"),
                            rs.getTimestamp("endDt"),
                            rs.getBoolean("status"),
                            rs.getString("businessUnit"),
                            rs.getString("channel"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on getByRange PullMessage. Ex = {}", e);
        }
        return list;
    }

}
