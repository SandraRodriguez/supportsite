/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.util;

/**
 *
 * @author emida
 */
public interface PullMessagesConstants {
    
    String UNIQUE_SYMBOL_IDENTIFIER                                             = "@";
    String MILLENNIUM_NO_PARAMETER                                              = "terminalType";
    String START_DATE_PARAMETER                                                 = "startDate";
    String END_DATE_PARAMETER                                                   = "endDate";
    String STATUS_MESSAGE_PARAMETER                                             = "statusMessage";
    String INITIAL_PAGE_PARAMETER                                               = "initialPage";
    String FINAL_PAGE_PARAMETER                                                 = "finalPage";
    String TERMINALS_PARAMETER                                                  = "terminals";
    
    /**
     * Terminal types
     */
    String TERMINAL_TYPES_AVAILABLE                                             = "23, 26, 104, 105";
    
    String LIST_OF_TERMINALS                                                    = "list_terminals";
    String MAP_OF_TERMINALS                                                     = "map_terminals";
    
    /**
     * Names of session
     */
    String SEARCH_PARAMETERS                                                    = "search_parameters";
    
    /**
     * Additional data to download file
     */
    String LANGUAGE_DOWNLOAD_DATA                                               = "language";
    String START_DATE_DOWNLOAD_DATA                                             = "startDate";
    String END_DATE_DOWNLOAD_DATA                                               = "endDate";
    String TERMINAL_TYPE_DOWNLOAD_DATA                                          = "terminalType";
    String STATUS_MESSAGE_DOWNLOAD_DATA                                         = "statusMessage";
    
    String FILE_WITH_DATA                                                       = "full_data_file";
    String LAST_PARAMETERS_SEARCHED                                             = "last_parameters_searched";
    
    
}
