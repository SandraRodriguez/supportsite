/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.services;

import com.debisys.users.SessionData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.tools.pullMessage.messages.dto.PullMessagesDataSearch;
import net.emida.supportsite.tools.pullMessage.messages.dto.PullMessagesResponse;
import net.emida.supportsite.tools.pullMessage.messages.dto.TerminalsResponse;
import net.emida.supportsite.tools.pullMessage.messages.util.PullMessagesConstants;
import net.emida.supportsite.util.ValidationParameters;
import net.emida.supportsite.util.ValidationParametersResponse;
import net.emida.supportsite.util.ValidationParametersResponseData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author emida
 */
@Repository("IRetrievePullMessagesDao")
public class RetrievePullMessagesDaoImpl implements IRetrievePullMessagesDao, ValidationParametersResponseData, PullMessagesConstants {
    
    private static final int RECORDS_NUMBER_BY_PAGE = 50; 
    private ValidationParametersResponse providerResponse = new ValidationParametersResponse();
    private ValidationParameters validateUtil =  new ValidationParameters(providerResponse);
    private static final Logger _logger = Logger.getLogger(RetrievePullMessagesDaoImpl.class);

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    
    private final String GET_MESSAGES_BY_TERMINAL_QUERY = 
                            "SELECT * FROM "
            + "                     (SELECT ROW_NUMBER() "
            + "                         OVER (ORDER BY t.millennium_no) AS rowNum, "
            + "                         pm.id AS idMessage, "
            + "                         t.millennium_no AS siteId, "
            + "                         pm.content AS message, "
            + "                         pm.status, "
            + "                         (SELECT CASE WHEN COUNT(pmp.id_pullMessage) > 0 THEN 1 ELSE 0 END FROM dbo.PullMessagePulled pmp WITH (NOLOCK) WHERE pmp.id_pullMessage = pm.id AND pmp.millennium_no = t.millennium_no) As messageRead, " 
            + "                         (SELECT CASE WHEN COUNT(pmp.id_pullMessage) > 0 THEN pmp.pulledDt ELSE '' END FROM dbo.PullMessagePulled pmp WITH (NOLOCK) WHERE pmp.id_pullMessage = pm.id AND pmp.millennium_no = t.millennium_no GROUP BY pmp.pulledDt) As dateOnMessageWasRead "
            + "                     FROM terminals t WITH(NOLOCK) "
            + "                     INNER JOIN PullMessage pm WITH (NOLOCK) "
            + "                             ON pm.startDt >= @startDate AND pm.endDt <= @endDate "
            + "                     WHERE pm.status = @statusMessage"
            + "                             AND t.terminal_type = @terminalType) AS RowConstrainedResult "
            + "              WHERE rowNum >= @initialPage "
            + "                 AND rowNum <= @finalPage ORDER BY rowNum";
    
    private final String GET_ALL_MESSAGES_QUERY = 
                            "SELECT "
            + "                 pm.id AS idMessage, "
            + "                 t.millennium_no AS siteId, "
            + "                 pm.content AS message, "
            + "                 pm.status, "
            + "                 (SELECT CASE WHEN COUNT(pmp.id_pullMessage) > 0 THEN 1 ELSE 0 END FROM dbo.PullMessagePulled pmp WITH (NOLOCK) WHERE pmp.id_pullMessage = pm.id AND pmp.millennium_no = t.millennium_no) As messageRead, " 
            + "                 (SELECT CASE WHEN COUNT(pmp.id_pullMessage) > 0 THEN pmp.pulledDt ELSE '' END FROM dbo.PullMessagePulled pmp WITH (NOLOCK) WHERE pmp.id_pullMessage = pm.id AND pmp.millennium_no = t.millennium_no GROUP BY pmp.pulledDt) As dateOnMessageWasRead "
            + "             FROM terminals t WITH(NOLOCK) "
            + "             INNER JOIN PullMessage pm WITH (NOLOCK) "
            + "                         ON pm.startDt >= @startDate AND pm.endDt <= @endDate "
            + "             WHERE pm.status = @statusMessage "
            + "                         AND t.terminal_type = @terminalType";
    
    
    private final String GET_PRIORITY_TERMINALS =
                                "SELECT t.terminal_type AS terminalType, t.description AS terminalName "
            + "                  FROM terminal_types t WITH(NOLOCK) " 
            + "                  WHERE t.terminal_type IN (@terminals) "
            + "                  ORDER BY t.terminal_type";
    
    
    @Override
    public ValidationParametersResponse getPullMessagesByTerminal(PullMessagesDataSearch request) {
            List<PullMessagesResponse> lsResponse = null;
            String terminalDescription = null;
            
        try {
            ServletRequestAttributes contextRequest = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            HttpSession session = contextRequest.getRequest().getSession(); // Get session
            Map mapTerminals = (Map) session.getAttribute(MAP_OF_TERMINALS);
            SessionData sessionData = (SessionData)session.getAttribute("SessionData");
            final String language = sessionData.getLanguage();

            if (mapTerminals != null) {
                terminalDescription = (String) mapTerminals.get(request.getTerminalTypeId());
            } else {
                this.getSomeTerminals(request.getTerminalTypeId());
                List<TerminalsResponse> lsTerminals = this.providerResponse.getLsResult();
                terminalDescription = lsTerminals.get(0).getTerminalName();
            }
            
            final String terminalTypeDescription = terminalDescription;
            final boolean isDownload = request.isDownload();
            this.validateFields(request);
            if (this.providerResponse.getResultStatus() == SUCCESS_PARAMETER) {
                String sqlQuery = this.buildRequest(request);
                if (sqlQuery != null) {
                    _logger.info("\n\n                      RetrievePullMessagesDaoImpl.getPullMessagesByTerminal - QUERY : \n      " + sqlQuery+"\n\n");
                    long initTime = System.currentTimeMillis();
                    lsResponse = jdbcTemplate.query(sqlQuery, new RowMapper<PullMessagesResponse>() {
                        @Override
                        public PullMessagesResponse mapRow(ResultSet rs, int index) throws SQLException {
                            if (isDownload) {
                                return new PullMessagesResponse(
                                        rs.getInt("status"),
                                        rs.getInt("messageRead"),
                                        rs.getString("idMessage"),
                                        terminalTypeDescription,
                                        rs.getString("siteId"),
                                        rs.getString("message"),
                                        rs.getString("dateOnMessageWasRead"),
                                        language);
                            } else {
                                return new PullMessagesResponse(
                                        rs.getInt("rowNum"),
                                        rs.getInt("status"),
                                        rs.getInt("messageRead"),
                                        rs.getString("idMessage"),
                                        terminalTypeDescription,
                                        rs.getString("siteId"),
                                        rs.getString("message"),
                                        rs.getString("dateOnMessageWasRead"),
                                        language);
                            }
                        }
                    });
                    long finalTime = System.currentTimeMillis();
                    _logger.info("\n                            RetrievePullMessagesDaoImpl.getPullMessagesByTerminal - TIME IT TOOK TO DO QUERY : " + ((finalTime - initTime) / 1000) + " seconds \n");
                } else {
                    _logger.info("RetrievePullMessagesDaoImpl.getPullMessagesByTerminal - ERROR BUILDING SQL " );
                }
                this.providerResponse.setLsResult(lsResponse);
            } 
            return this.providerResponse;
        } catch (Exception e) {
            _logger.error("RetrievePullMessagesDaoImpl.getPullMessagesByTerminal - ERROR RETRIEVING MESSAGES FROM DB " + e.getMessage());
            return null;
        }
    }
    
    @Override
    public ValidationParametersResponse getSomeTerminals(String terminals) {
        List<TerminalsResponse> lsTerminals = null;
        try {
            String query = this.buildTerminalsRequest(terminals);
            _logger.info("\n\n                      RetrievePullMessagesDaoImpl.getSomeTerminals - QUERY : \n      " + query +"\n\n");
            lsTerminals = jdbcTemplate.query(query, new RowMapper<TerminalsResponse>() {
                        @Override
                        public TerminalsResponse mapRow(ResultSet rs, int index) throws SQLException {
                            return new TerminalsResponse(
                                    rs.getString("terminalType"),
                                    rs.getString("terminalName"));
                        }
            });
   
            if (lsTerminals != null && lsTerminals.size() > 0) {
                this.providerResponse.setResultStatus(SUCCESS_PARAMETER);
                this.providerResponse.setLsResult(lsTerminals);                
            } else {
                this.providerResponse.setResultStatus(INVALID_PARAMETER);
            }
            return this.providerResponse;
        } catch (Exception e) {
            _logger.error("RetrievePullMessagesDaoImpl.getSomeTerminals - ERROR RETRIEVING TERMINALS FROM DB " + e.getMessage());
            return null;
        }
    }
    
    /**
     * Validates front end fields.
     * 
     * @param request 
     */
    private void validateFields(PullMessagesDataSearch request) {
        try {
            if (this.validateUtil.checkField(INTEGER_ONLY, TERMINAL_TYPE_ID_TAG, request.getTerminalTypeId(), 0, 0) &&
                this.validateUtil.checkField(INTEGER_WITH_MIN_AND_MAX_LENGTH_TYPE, STATUS_TAG, request.getStatusMessage(), 1, 1) &&
                this.validateUtil.checkField(INTEGER_ONLY, PAGE_TAG, request.getPage(), 0, 0) &&
                this.validateUtil.checkField(DATE_FORMAT_YYYY_MM_DD, INITIAL_DATE_TAG, request.getStartDate(), 0, 0) &&
                this.validateUtil.checkField(DATE_FORMAT_YYYY_MM_DD, FINAL_DATE_TAG, request.getEndDate(), 0, 0)) {
                this.providerResponse.setResultStatus(SUCCESS_PARAMETER);
            } else {
                this.providerResponse.setResultStatus(INVALID_PARAMETER);
            }
        } catch (Exception e) {
            _logger.error("RetrievePullMessagesDaoImpl.validateFields - ERROR VALIDATING FIELDS " + e.getMessage());
            this.providerResponse.setResultStatus(INVALID_PARAMETER);
        }
    }
    
    /**
     * Take string query to replace data with user values, to build a new query.
     * 
     * @param request
     * @return 
     */
    private String buildRequest(PullMessagesDataSearch request) {
        String response = null;
        try {
            int page = Integer.parseInt(request.getPage());
            String query = (request.isDownload() ? GET_ALL_MESSAGES_QUERY : GET_MESSAGES_BY_TERMINAL_QUERY);
            Map params = new HashMap();
            params.put(MILLENNIUM_NO_PARAMETER, request.getTerminalTypeId());
            params.put(START_DATE_PARAMETER, request.getStartDate() + " 00:00:00");
            params.put(END_DATE_PARAMETER, request.getEndDate() + " 23:59:59");
            params.put(STATUS_MESSAGE_PARAMETER, request.getStatusMessage());
            if (!request.isDownload()) {
                params.put(INITIAL_PAGE_PARAMETER, ((page == 1) ? 1 : ((RECORDS_NUMBER_BY_PAGE * (page - 1)) + 1)));
                params.put(FINAL_PAGE_PARAMETER, (RECORDS_NUMBER_BY_PAGE * page));
            }
            
            Iterator itParams = params.entrySet().iterator();
            while (itParams.hasNext()) {
                Map.Entry param = (Map.Entry)itParams.next();
                String value = (((param.getKey().equals(START_DATE_PARAMETER)) || (param.getKey().equals(END_DATE_PARAMETER))) ? ("\'" + param.getValue() + "\'") : (param.getValue() + ""));
                query = query.replaceAll(UNIQUE_SYMBOL_IDENTIFIER + param.getKey(), value);
                itParams.remove(); // avoids a ConcurrentModificationException
            }
            return response = query;
        } catch (Exception e) {
            _logger.error("RetrievePullMessagesDaoImpl.buildRequest - ERROR BUILDING REQUEST :  " + e.getLocalizedMessage());
            return null;
        }
    }
    
    private String buildTerminalsRequest(String terminals) {
        try {
            String query = GET_PRIORITY_TERMINALS;
            return query.replaceAll(UNIQUE_SYMBOL_IDENTIFIER + TERMINALS_PARAMETER, terminals);
        } catch (Exception e) {
            _logger.error("RetrievePullMessagesDaoImpl.buildTerminalsRequest - ERROR BUILDING TERMINALS REQUEST :  " + e.getLocalizedMessage());
            return null;
        }
    }
}
