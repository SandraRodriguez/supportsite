package net.emida.supportsite.tools.pullMessage.repository;

import java.util.List;
import net.emida.supportsite.tools.pullMessage.dto.PullMessageDto;

/**
 *
 * @author janez@emida.net
 * @since 20/03/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4395
 */
public interface IPullMessageDao {

    /**
     *
     * @param pullMessage
     * @return
     */
    public Integer add(PullMessageDto pullMessage);

    /**
     *
     * @param pullMessage
     * @return
     */
    public Integer update(PullMessageDto pullMessage);

    /**
     * 
     * @param id
     * @return 
     */
    public Integer remove(String id);

    /**
     *
     * @return
     */
    public List<PullMessageDto> getLast();

    /**
     *
     * @param id
     * @return
     */
    public PullMessageDto getById(String id);

    /**
     *
     * @param startDt
     * @param endDt
     * @return
     */
    public List<PullMessageDto> getByRange(String startDt, String endDt);

}
