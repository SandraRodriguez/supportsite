/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.dto;

import com.debisys.languages.Languages;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author emida
 */
public class PullMessagesResponse implements Serializable, CsvRow {
    
    private int rowNum;
    private int status;
    private int messageRead;
    private String idMessage;
    private String terminalType;
    private String siteId;
    private String message;
    private String dateWhenMessageWasRead;
    private String language;

    public PullMessagesResponse() {
    }

    public PullMessagesResponse(int status, int messageRead, String idMessage, String terminalType, 
            String siteId, String message, String dateWhenMessageWasRead, String language) {
        this.status = status;
        this.messageRead = messageRead;
        this.idMessage = idMessage;
        this.terminalType = terminalType;
        this.siteId = siteId;
        this.message = message;
        this.dateWhenMessageWasRead = dateWhenMessageWasRead;
        this.language = language;
    }
    
    public PullMessagesResponse(int rowNum, int status, int messageRead, String idMessage, String terminalType,
            String siteId, String message, String dateWhenMessageWasRead, String language) {
        this.rowNum = rowNum;
        this.status = status;
        this.messageRead = messageRead;
        this.idMessage = idMessage;
        this.terminalType = terminalType;
        this.siteId = siteId;
        this.message = message;
        this.dateWhenMessageWasRead = dateWhenMessageWasRead;
        this.language = language;
    }
    
    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMessageRead() {
        return messageRead;
    }

    public void setMessageRead(int messageRead) {
        this.messageRead = messageRead;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateWhenMessageWasRead() {
        return dateWhenMessageWasRead;
    }

    public void setDateWhenMessageWasRead(String dateWhenMessageWasRead) {
        this.dateWhenMessageWasRead = dateWhenMessageWasRead;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    @Override
    public List<CsvCell> cells() {
        List<CsvCell> retValue = new ArrayList<CsvCell>();

        CsvCell terminalTypeCell = new CsvCell(this.terminalType);
        retValue.add(terminalTypeCell);
        
        CsvCell siteIdCell = new CsvCell(this.siteId);
        retValue.add(siteIdCell);
        
        CsvCell messageCell = new CsvCell(this.message);
        retValue.add(messageCell);
        
        CsvCell messageDeliveryCell = new CsvCell(((this.messageRead == 0) 
                                            ? Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.column.message.notRead", this.language)
                                            : Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.column.message.read", this.language)) );
        retValue.add(messageDeliveryCell);
        
        CsvCell whenWasReadCell = new CsvCell(this.dateWhenMessageWasRead);
        retValue.add(whenWasReadCell);
        return retValue;
    }
}
