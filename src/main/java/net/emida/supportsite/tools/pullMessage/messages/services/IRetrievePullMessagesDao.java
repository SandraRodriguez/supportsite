/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.services;

import net.emida.supportsite.tools.pullMessage.messages.dto.PullMessagesDataSearch;
import net.emida.supportsite.util.ValidationParametersResponse;

/**
 *
 * @author emida
 */
public interface IRetrievePullMessagesDao {
    
    ValidationParametersResponse getPullMessagesByTerminal(PullMessagesDataSearch request);
    
    ValidationParametersResponse getSomeTerminals(String terminals);
    
}
