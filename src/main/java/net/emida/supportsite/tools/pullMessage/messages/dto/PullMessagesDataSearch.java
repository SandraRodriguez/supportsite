/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.dto;

import java.io.Serializable;

/**
 *
 * @author emida
 */
public class PullMessagesDataSearch implements Serializable {
    
    private String startDate;
    private String endDate;
    private String page;
    private String statusMessage;
    private String terminalTypeId;
    private boolean download;
    
    public PullMessagesDataSearch() {}
    
    public PullMessagesDataSearch(PullMessagesDataSearch pullSearch) {
        this.startDate = pullSearch.getStartDate();
        this.endDate = pullSearch.getEndDate();
        this.page = pullSearch.getPage();
        this.statusMessage = pullSearch.getStatusMessage();
        this.terminalTypeId = pullSearch.getTerminalTypeId();
        this.download = pullSearch.isDownload();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getTerminalTypeId() {
        return terminalTypeId;
    }

    public void setTerminalTypeId(String terminalTypeId) {
        this.terminalTypeId = terminalTypeId;
    }

    public boolean isDownload() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download = download;
    }
}