/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.pullMessage.messages.dto.PullMessagesDataSearch;
import net.emida.supportsite.tools.pullMessage.messages.dto.PullMessagesResponse;
import net.emida.supportsite.tools.pullMessage.messages.dto.TerminalsResponse;
import net.emida.supportsite.tools.pullMessage.messages.services.IRetrievePullMessagesDao;
import net.emida.supportsite.tools.pullMessage.messages.util.PullMessagesConstants;
import net.emida.supportsite.util.ValidationParametersResponse;
import static net.emida.supportsite.util.ValidationParametersResponseData.SUCCESS_PARAMETER;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author emida
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PULL_MESSAGE_NOTIFY + UrlPaths.PULL_QUEUE_MESSAGE)
public class PullMessagesController extends BaseController implements PullMessagesConstants {
    
    private static final Logger _logger = Logger.getLogger(PullMessagesController.class);
    
    @Autowired
    private IRetrievePullMessagesDao pullMessagesService;
    
    @Autowired
    @Qualifier("pullMessagesCsvBuilderService")
    private CsvBuilderService csvBuilderService;
    
    @RequestMapping(value = UrlPaths.PULL_QUEUE_VIEW, method = RequestMethod.GET)
    public String __init__() {
        return "tools/pullMessage/messages";
    }
    
    @RequestMapping(value = UrlPaths.PULL_QUEUE_GET_ALL_MESSAGES, method = RequestMethod.POST)
    @ResponseBody
    public List getAllMessagesByTerminal(HttpSession session, PullMessagesDataSearch request) {
        List<PullMessagesResponse> messages = new ArrayList<PullMessagesResponse>();

        try {
            request.setStartDate(((request.getStartDate() != null && !request.getStartDate().isEmpty()) ? request.getStartDate().replaceAll("/", "-") : null));
            request.setEndDate(((request.getEndDate() != null && !request.getEndDate().isEmpty()) ? request.getEndDate().replaceAll("/", "-") : null));
            request.setDownload(false);
            ValidationParametersResponse response = this.pullMessagesService.getPullMessagesByTerminal(request);
            if (response != null) {
                if (response.getResultStatus() == SUCCESS_PARAMETER) {
                    session.removeAttribute(SEARCH_PARAMETERS);
                    if  (response.getLsResult() != null && response.getLsResult().size() > 0) {
                        session.setAttribute(SEARCH_PARAMETERS, request);
                        messages = response.getLsResult();
                    } else {
                        _logger.info("PullMessagesController.getAllMessagesByTerminal - NO DATA TO SHOW");
                    }
                } else {
                    _logger.info("\nPullMessagesController.getAllMessagesByTerminal - ERROR :\n "
                            + "                                         ERROR CODE : " + (response.getResultCode() != null ? response.getResultCode() : "") 
                            + "                                         ERROR MESSAGE : " + (response.getResultMessage() != null ? response.getResultMessage() : ""));
                }
            } else {
                _logger.info("PullMessagesController.getAllMessagesByTerminal - ERROR GETTING MESSAGES FROM DB");
            }
            return messages;
        } catch (TechnicalException err) {
            _logger.error("PullMessagesController.getAllMessagesByTerminal - ERROR : " + err.getLocalizedMessage());
            return null;
        } catch (Exception e) {
            _logger.error("PullMessagesController.getAllMessagesByTerminal - ERROR : " + e.getLocalizedMessage());
            return null;
        }
    }
    
    @RequestMapping(value = UrlPaths.GET_AVAILABLE_TERMINALS, method = RequestMethod.POST)
    @ResponseBody
    public List getTerminals(HttpSession session) {
        List<TerminalsResponse> terminals = null;
        Map<String, String> mapTerminals = new HashMap<String, String>();
        
        try {
            if (session.getAttribute(LIST_OF_TERMINALS) == null) {
                ValidationParametersResponse response = this.pullMessagesService.getSomeTerminals(TERMINAL_TYPES_AVAILABLE);
                if (response != null) {
                    if (response.getResultStatus() == SUCCESS_PARAMETER) {
                        if  (response.getLsResult() != null && response.getLsResult().size() > 0) {
                            terminals = response.getLsResult();
                            session.setAttribute(LIST_OF_TERMINALS, terminals);
                        } else {
                            _logger.info("PullMessagesController.getTerminals - NO DATA TO SHOW");
                        }
                    } else {
                        _logger.info("\nPullMessagesController.getTerminals - ERROR :\n "
                            + "                                         ERROR CODE : " + (response.getResultCode() != null ? response.getResultCode() : "") 
                            + "                                         ERROR MESSAGE : " + (response.getResultMessage() != null ? response.getResultMessage() : ""));
                    }
                } else {
                    _logger.info("PullMessagesController.getTerminals - ERROR GETTING MESSAGES FROM DB");
                }            
            } else {
                terminals = (List) session.getAttribute(LIST_OF_TERMINALS);
            }
            if (terminals != null && terminals.size() > 0 && session.getAttribute(MAP_OF_TERMINALS) == null) {
                for (TerminalsResponse terminal : terminals) {
                    mapTerminals.put(terminal.getTerminalTypeId(), terminal.getTerminalName());
                }
                session.setAttribute(MAP_OF_TERMINALS, mapTerminals);                
            }
            return terminals;
        } catch (Exception e) {
            _logger.error("PullMessagesController.getTerminals - ERROR : " + e.getLocalizedMessage());
            return null;
        }
    }
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.PULL_QUEUE_DOWNLOAD, 
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse httpResponse, HttpSession session) {
        FileSystemResource zipFileResource = null;
        try {
            PullMessagesDataSearch searchParameters = (PullMessagesDataSearch) session.getAttribute(SEARCH_PARAMETERS);
            if (searchParameters != null) {
                _logger.info("PullMessagesController.downloadFile - DOWNLOADING FILE!");
                String fileName = generateRandomFileName();
                searchParameters.setDownload(true);
                ValidationParametersResponse response = this.pullMessagesService.getPullMessagesByTerminal(searchParameters);
                if (response != null) {
                    if (response.getResultStatus() == SUCCESS_PARAMETER) {
                        if (response.getLsResult() != null && response.getLsResult().size() > 0) {
                            try {
                                String filePath = generateDestinationFilePath(fileName, ".csv");
                                _logger.info("PullMessagesController.downloadFile - GENERARED PATH : " + filePath);
                                csvBuilderService.buildCsv(filePath, response.getLsResult(), this.getAddionalDataDownloadFile(searchParameters));
                                _logger.info("PullMessagesController.downloadFile - FILE CSV HAS BEEN BUILT!");

                                long initTime = System.currentTimeMillis();
                                _logger.info("PullMessagesController.downloadFile - GENERATING ZIP FILE...");
                                FileSystemResource csvFileResource = new FileSystemResource(filePath);
                                String generateZipFile = generateZipFile(fileName, csvFileResource);
                                _logger.info("PullMessagesController.downloadFile - ZIP FILE HAS BEEN CREATED : " + generateZipFile);
                                zipFileResource = new FileSystemResource(generateZipFile);
                                session.removeAttribute(FILE_WITH_DATA);
                                session.setAttribute(FILE_WITH_DATA, zipFileResource);
                                csvFileResource.getFile().delete(); //Delete csv file
                                long finalTime = System.currentTimeMillis();
                                _logger.info("\n                            PullMessagesController.downloadFile - TIME IT TOOK TO GENERATE ZIP : " + ((finalTime - initTime) / 1000) + " seconds \n");

                                httpResponse.setContentType("application/zip");
                                httpResponse.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + fileName + ".zip");
                                PullMessagesDataSearch lastParametersSearched = new PullMessagesDataSearch(searchParameters);
                                session.setAttribute(LAST_PARAMETERS_SEARCHED, lastParametersSearched);

                            } catch (Exception e) {
                                _logger.error("PullMessagesController.downloadFile - ERROR BUILDING FILE " + e.getMessage());
                                return null;
                            }
                        } else {
                            _logger.info("PullMessagesController.downloadFile - NO DATA TO SHOW");
                        }
                    } else {
                        _logger.info("\nPullMessagesController.downloadFile - ERROR :\n "
                                + "                                         ERROR CODE : " + (response.getResultCode() != null ? response.getResultCode() : "")
                                + "                                         ERROR MESSAGE : " + (response.getResultMessage() != null ? response.getResultMessage() : ""));
                    }
                } else {
                    _logger.info("PullMessagesController.downloadFile - ERROR GETTING MESSAGES FROM DB");
                }
            } else {
                _logger.info("PullMessagesController.downloadFile - THERE'S NO FOUND DATA TO QUERY");
            }
        } catch (Exception e) {
            _logger.error("PullMessagesController.downloadFile - ERROR DOWNLOADING FILE " + e.getMessage());
            return null;
        }
        return zipFileResource;
    } 

    private Map getAddionalDataDownloadFile(PullMessagesDataSearch searchParameters) {
        Map<String, Object> additionalData = new HashMap<String, Object>();
        try {
            additionalData.put(LANGUAGE_DOWNLOAD_DATA, getSessionData().getLanguage());
            additionalData.put(START_DATE_DOWNLOAD_DATA, searchParameters.getStartDate());
            additionalData.put(END_DATE_DOWNLOAD_DATA, searchParameters.getEndDate());
            additionalData.put(TERMINAL_TYPE_DOWNLOAD_DATA, searchParameters.getTerminalTypeId());
            additionalData.put(STATUS_MESSAGE_DOWNLOAD_DATA, searchParameters.getStatusMessage());
            
            return additionalData;
        } catch (Exception e) {
            _logger.error("PullMessagesController.downloadFile - ERROR DOWNLOADING FILE " + e.getMessage());
            return null;
        }
    }
    
    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 40;
    }
}
