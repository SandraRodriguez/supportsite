/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.pullMessage.messages.services;

import com.debisys.languages.Languages;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.tools.pullMessage.messages.util.PullMessagesConstants;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 *
 * @author emida
 */
@Service
public class PullMessagesCsvBuilderService implements CsvBuilderService, PullMessagesConstants {

    private static final Logger _logger = Logger.getLogger(PullMessagesCsvBuilderService.class);
    
    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        long initTime = System.currentTimeMillis();
        try {
            writer = this.getFileWriter(filePath);
            
            if (writer != null) {
                _logger.info("MessagesController.downloadFile - CREATING TEMP FILE " + filePath);
                writer.flush();
                
                String language = (String) additionalData.get(LANGUAGE_DOWNLOAD_DATA);
                writer.write(Languages.getString("jsp.admin.tools.notification.messagesTitle", language));  // Report Title
                
                // Add report filter values
                writer.write(String.format("\n\n%s : %s\n\n",
                                    Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.startDate.column.title", language),
                                    additionalData.get(START_DATE_DOWNLOAD_DATA).toString()));
                writer.write(String.format("\n\n%s : %s\n\n",
                                    Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.endDate.column.title", language),
                                    additionalData.get(END_DATE_DOWNLOAD_DATA).toString()));
                writer.write(String.format("\n\n%s : %s\n\n",
                                    Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.terminalId.column.title", language),
                                    additionalData.get(TERMINAL_TYPE_DOWNLOAD_DATA).toString()));
                writer.write(String.format("\n\n%s : %s\n\n",
                                    Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.messageRead.column.title", language),
                                    ((Integer.parseInt((String) additionalData.get(STATUS_MESSAGE_DOWNLOAD_DATA)) == 1) ? Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.messageStatus.option.enable", language) : Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.messageStatus.option.disable", language))));
                
                // Add report column headers
                List<String> reportHeaders = this.generateReportHeaders(language);
                String reportHeaderRow = CsvUtil.generateRow(reportHeaders, true);            
                writer.write(reportHeaderRow);
                
                // Write report content
                List<CsvRow> reportRows = (List<CsvRow>) list;
                this.fillWriter(reportRows, writer);
            }
        } catch (Exception e) {
            _logger.error("PullMessagesCsvBuilderService.buildCsv - ERROR  " + e.getMessage());
            throw new TechnicalException(e.getMessage(), e);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {
                    _logger.error("PullMessagesCsvBuilderService.buildCsv - Error closing csv file : {}" + filePath);
                    throw new TechnicalException(ex.getMessage(), ex);
                }
            }
            long finalTime = System.currentTimeMillis();
            _logger.info("\n                            PullMessagesCsvBuilderService.buildCsv - TIME IT TOOK TO PASS DATA TO FILE : " + ((finalTime - initTime) / 1000) + " seconds \n");
        }
    }
    
    private Writer getFileWriter(String filePath) {
        try {
            FileWriter fileWriter = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            return bufferedWriter;
        } catch (IOException ioErr) {
            _logger.error("PullMessagesCsvBuilderService.buildCsv - ERROR  " + ioErr.getMessage());
            return null;
        }
    }
    
    private List<String> generateReportHeaders(String language) {
        List<String> headers = new ArrayList<String>();
        headers.add(Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.terminalType.column.title", language));
        headers.add(Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.terminalId.column.title", language));
        headers.add(Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.message.column.title", language));
        headers.add(Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.messageRead.column.title", language));
        headers.add(Languages.getString("jsp.tools.conciliationExporter.pullMessages.form.table.dateOnMessageWasRead.column.title", language));
        return headers;
    }
    
    private void fillWriter(List<CsvRow> data, Writer writer) {
        try {
            for (CsvRow row : data) {
                List<CsvCell> cells = row.cells();
                String stringifyRow = CsvUtil.stringifyRow(cells, true);
                writer.write(stringifyRow);
            }
        } catch (IOException ioErr) {
            _logger.error("PullMessagesCsvBuilderService.buildCsv - ERROR  " + ioErr.getMessage());
        }
    }
}
