package net.emida.supportsite.tools.pullMessage.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author janez@emida.net
 * @since 20/03/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4395
 */
public class PullMessageDto implements Serializable {

    private static final long serialVersionUID = 4771451429570165077L;
    private String id;
    private String subject;
    private String content;
    private Timestamp createDt;
    private Timestamp lastUpdateDt;
    private Timestamp startDt;
    private Timestamp endDt;
    private boolean status;
    private String businessUnit;
    private String channel;

    /**
     * Add Object.
     * 
     * @param subject
     * @param content
     * @param startDt
     * @param endDt
     * @param status
     * @param businessUnit
     * @param channel 
     */
    public PullMessageDto(String subject, String content, Timestamp startDt, Timestamp endDt, boolean status, String businessUnit, String channel) {
        this.subject = subject;
        this.content = content;
        this.startDt = startDt;
        this.endDt = endDt;
        this.status = status;
        this.businessUnit = businessUnit;
        this.channel = channel;
    }

    /**
     * Update Object.
     * 
     * @param id
     * @param subject
     * @param content
     * @param startDt
     * @param endDt
     * @param status
     * @param businessUnit
     * @param channel 
     */
    public PullMessageDto(String id, String subject, String content, Timestamp startDt, Timestamp endDt, boolean status, String businessUnit, String channel) {
        this.id = id;
        this.subject = subject;
        this.content = content;
        this.startDt = startDt;
        this.endDt = endDt;
        this.status = status;
        this.businessUnit = businessUnit;
        this.channel = channel;
    }

    /**
     *
     * @param id
     * @param subject
     * @param content
     * @param createDt
     * @param lastUpdateDt
     * @param startDt
     * @param endDt
     * @param status
     * @param businessUnit
     * @param channel
     */
    public PullMessageDto(String id, String subject, String content, Timestamp createDt, Timestamp lastUpdateDt, Timestamp startDt, Timestamp endDt, boolean status, String businessUnit, String channel) {
        this.id = id;
        this.subject = subject;
        this.content = content;
        this.createDt = createDt;
        this.lastUpdateDt = lastUpdateDt;
        this.startDt = startDt;
        this.endDt = endDt;
        this.status = status;
        this.businessUnit = businessUnit;
        this.channel = channel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getCreateDt() {
        return createDt;
    }

    public void setCreateDt(Timestamp createDt) {
        this.createDt = createDt;
    }

    public Timestamp getLastUpdateDt() {
        return lastUpdateDt;
    }

    public void setLastUpdateDt(Timestamp lastUpdateDt) {
        this.lastUpdateDt = lastUpdateDt;
    }

    public Timestamp getStartDt() {
        return startDt;
    }

    public void setStartDt(Timestamp startDt) {
        this.startDt = startDt;
    }

    public Timestamp getEndDt() {
        return endDt;
    }

    public void setEndDt(Timestamp endDt) {
        this.endDt = endDt;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

}
