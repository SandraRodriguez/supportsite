package net.emida.supportsite.tools.noAckPinManagement.repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.tools.noAckPinManagement.dto.NoAckPin;
import net.emida.supportsite.tools.noAckPinManagement.dto.PinIsolatedHistory;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-3957
 * @since 2018-09-10
 */
public interface NoAckPinDAO {

    public List<NoAckPin> getLastAckTransactions();

    public List<NoAckPin> getAckTransactions(Timestamp startDt, Timestamp endDt);

    public List<PinIsolatedHistory> getPinIsolatedHistory(Long pinIsolatedId);

    public Integer autitPinView(Map<String, String> parameters);

    public Integer returnPin(String pin) throws Exception;

}
