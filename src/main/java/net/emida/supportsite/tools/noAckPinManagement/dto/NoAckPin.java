package net.emida.supportsite.tools.noAckPinManagement.dto;

import java.io.Serializable;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-3957
 * @since 2018-09-10
 */
public class NoAckPin implements Serializable {

    private Long controlNumber;
    private Long id;
    private Long recId;
    private Long merchantId;
    private Long millenniumNo;
    private String pin;
    private String date;
    private String productDescription;
    private String providerName;
    private String status;

    public NoAckPin() {
    }

    public NoAckPin(Long controlNumber, Long id, Long recId, Long merchantId, Long millenniumNo, String pin, String date, String productDescription, String providerName, String status) {
        this.controlNumber = controlNumber;
        this.id = id;
        this.recId = recId;
        this.merchantId = merchantId;
        this.millenniumNo = millenniumNo;
        this.pin = pin;
        this.date = date;
        this.productDescription = productDescription;
        this.providerName = providerName;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRecId() {
        return recId;
    }

    public void setRecId(Long recId) {
        this.recId = recId;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getMillenniumNo() {
        return millenniumNo;
    }

    public void setMillenniumNo(Long millenniumNo) {
        this.millenniumNo = millenniumNo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getControlNumber() {
        return controlNumber;
    }

    public void setControlNumber(Long controlNumber) {
        this.controlNumber = controlNumber;
    }

}
