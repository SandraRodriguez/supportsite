package net.emida.supportsite.tools.noAckPinManagement.repository;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.tools.noAckPinManagement.dto.NoAckPin;
import net.emida.supportsite.tools.noAckPinManagement.dto.PinIsolatedHistory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-3957
 * @since 2018-09-10
 */
@Repository(value = "ACKTransactionDAO")
public class NoAckPinRepository implements NoAckPinDAO {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NoAckPinRepository.class);

    @Override
    public List<NoAckPin> getLastAckTransactions() {
        final String query = "SELECT TOP 15 pi.Id, pi.ControlNo, pi.Rec_id AS recId, t.merchant_id, t.millennium_no, pi.Pin, pi.[DateTime], p.description AS productDescription, c.Name AS carrierName, pis.Code AS status"
                + " FROM PinsIsolated pi WITH(NOLOCK) "
                + " INNER JOIN products p WITH(NOLOCK) ON p.id = pi.ProductId "
                + " INNER JOIN terminals t WITH(NOLOCK) ON t.millennium_no = pi.Millennium_no "
                + " INNER JOIN Carriers c WITH(NOLOCK) ON c.ID = pi.ProviderId"
                + " INNER JOIN PinsIsolatedStatus pis WITH(NOLOCK) ON pis.Id = pi.StatusId "
                + " WHERE pis.Code = 'PROCESSED' OR pis.Code = 'CHECK_PROVIDER' OR pis.Code = 'CHARGED'"
                + " ORDER BY pi.Id DESC ";
        List<NoAckPin> list = null;
        LOG.info("query :: " + query);
        try {
            list = jdbcTemplate.query(query, new RowMapper<NoAckPin>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public NoAckPin mapRow(ResultSet rs, int index) throws SQLException {
                    return new NoAckPin(
                            rs.getLong("ControlNo"), rs.getLong("Id"), rs.getLong("recId"), rs.getLong("merchant_id"), rs.getLong("millennium_no"),
                            rs.getString("Pin"), rs.getString("DateTime"), rs.getString("productDescription").trim(), rs.getString("carrierName"), rs.getString("status"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on get NoAckPins. Ex = {}", e);
        }
        return list;
    }

    @Override
    public List<NoAckPin> getAckTransactions(Timestamp startDt, Timestamp endDt) {
        final String query = "SELECT pi.Id, pi.ControlNo, pi.Rec_id AS recId, t.merchant_id, t.millennium_no, pi.Pin, pi.[DateTime], p.description AS productDescription, c.Name AS carrierName, pis.Code AS status"
                + " FROM PinsIsolated pi WITH(NOLOCK) "
                + " INNER JOIN products p WITH(NOLOCK) ON p.id = pi.ProductId "
                + " INNER JOIN terminals t WITH(NOLOCK) ON t.millennium_no = pi.Millennium_no "
                + " INNER JOIN Carriers c WITH(NOLOCK) ON c.ID = pi.ProviderId"
                + " INNER JOIN PinsIsolatedStatus pis WITH(NOLOCK) ON pis.Id = pi.StatusId "
                + " WHERE pi.[DateTime] BETWEEN ? AND DATEADD(day, 1, ?)"
                + " AND (pis.Code = 'PROCESSED' OR pis.Code = 'CHECK_PROVIDER' OR pis.Code = 'CHARGED')";
        List<NoAckPin> list = null;
        LOG.info("query :: " + query);
        try {
            list = jdbcTemplate.query(query, new Object[]{
                startDt, endDt
            }, new RowMapper<NoAckPin>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public NoAckPin mapRow(ResultSet rs, int index) throws SQLException {
                    return new NoAckPin(
                            rs.getLong("ControlNo"), rs.getLong("Id"), rs.getLong("recId"), rs.getLong("merchant_id"), rs.getLong("millennium_no"),
                            rs.getString("Pin"), rs.getString("DateTime"), rs.getString("productDescription").trim(), rs.getString("carrierName"), rs.getString("status"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on get NoAckPins. Ex = {}", e);
        }
        return list;
    }

    @Override
    public List<PinIsolatedHistory> getPinIsolatedHistory(Long pinIsolatedId) {
        final String query = "SELECT pih.Comments AS comment, pih.[DateTime] AS date FROM PinsIsolatedHistory pih WITH(NOLOCK) WHERE pih.PinsIsolatedId = ?";
        List<PinIsolatedHistory> list = null;
        LOG.info("query :: " + query);
        try {
            list = jdbcTemplate.query(query, new Object[]{
                pinIsolatedId
            }, new RowMapper<PinIsolatedHistory>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public PinIsolatedHistory mapRow(ResultSet rs, int index) throws SQLException {
                    return new PinIsolatedHistory(rs.getString("comment"), rs.getString("date"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on get NoAckPins. Ex = {}", e);
        }
        return list;
    }

    @Override
    public Integer autitPinView(Map<String, String> parameters) {
        int rows = 0;

        final String FIND_LOG_CHANGE_TYPE = "SELECT LOG_CHANGE_TYPE_ID FROM LOGCHANGESTYPES (NOLOCK) WHERE [NAME] = 'No Ack Pins Management' AND table_name = 'PinsIsolated'";
        final String AUDIT_LOG = "INSERT INTO LogChanges "
                + "(DATE_TIME,"
                + "LOG_CHANGE_TYPE_ID,"
                + "KEY_VALUE,"
                + "OLD_VALUE,"
                + "NEW_VALUE,"
                + "APPLICATION_NAME,"
                + "USER_NAME,"
                + "INSTANCE,"
                + "REASON) "
                + "VALUES (GETDATE(),?,?,?,?,?,?,?,?)";

        Map<String, Object> type_id = jdbcTemplate.queryForMap(FIND_LOG_CHANGE_TYPE);

        rows += jdbcTemplate.update(AUDIT_LOG,
                type_id.get("LOG_CHANGE_TYPE_ID"),
                parameters.get("controlNumber"),
                "--",
                "--",
                "support",
                parameters.get("user_name"),
                parameters.get("instance"),
                parameters.get("reason"));

        return rows;
    }

    @Override
    public Integer returnPin(String pin) throws Exception {
        int rows = 0;
        CallableStatement callableStatement = null;
        try {
            callableStatement = jdbcTemplate.getDataSource().getConnection().prepareCall("EXEC pinStatusManagement ?, ?, NULL, 'UPDATE', NULL, NULL");
            callableStatement.setString(1, pin);
            callableStatement.setInt(2, 1);
            rows += callableStatement.executeUpdate();
            if (rows > 0) {
                rows += jdbcTemplate.update("UPDATE PinsIsolated SET StatusId = " + 6 + " WHERE Pin = '" + pin + "'");
            }
        } catch (SQLException ex) {
            LOG.error(ex.getMessage(), ex);
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
        }
        return rows;
    }

}
