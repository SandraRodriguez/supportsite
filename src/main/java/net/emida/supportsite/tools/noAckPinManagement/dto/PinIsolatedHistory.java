package net.emida.supportsite.tools.noAckPinManagement.dto;

import java.io.Serializable;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-3957
 * @since 2018-09-10
 */
public class PinIsolatedHistory implements Serializable {

    private String comment;
    private String date;

    public PinIsolatedHistory() {
    }

    public PinIsolatedHistory(String comment, String date) {
        this.comment = comment;
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
