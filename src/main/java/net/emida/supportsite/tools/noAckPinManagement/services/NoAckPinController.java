package net.emida.supportsite.tools.noAckPinManagement.services;

import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.noAckPinManagement.dto.NoAckPin;
import net.emida.supportsite.tools.noAckPinManagement.dto.PinIsolatedHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import net.emida.supportsite.tools.noAckPinManagement.repository.NoAckPinDAO;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.emida.supportsite.util.languageUtil.LanguageIOService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-3957
 * @since 2018-09-10
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class NoAckPinController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(NoAckPinController.class);

    @Autowired
    private NoAckPinDAO dao;

    @Autowired
    private LanguageIOService language;

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT, method = RequestMethod.GET)
    public String __init__() {
        return "tools/noAckPinManagement/noAckPinManagement";
    }

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT + "/getLastAckTransactions", method = RequestMethod.GET)
    @ResponseBody
    public List<NoAckPin> getLastAckTransactions(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return dao.getLastAckTransactions();
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage(), err);
            return new ArrayList<NoAckPin>();
        }
    }

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT + "/getAckPins", method = RequestMethod.GET)
    @ResponseBody
    public List<NoAckPin> getNoAckPins(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return dao.getAckTransactions(new Timestamp(strToDate(startDt).getTime()), new Timestamp(strToDate(endDt).getTime()));
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage(), err);
            return new ArrayList<NoAckPin>();
        }
    }

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT + "/getPinIsolatedHistory/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<PinIsolatedHistory> getPinIsolatedHistory(@PathVariable(value = "id") Long id, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return dao.getPinIsolatedHistory(id);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage(), err);
            return new ArrayList<PinIsolatedHistory>();
        }
    }

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT + "/chargePinAmount", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> chargePinAmount(@RequestParam(value = "transactionId") Integer transactionId, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        try {
            String username = sessionData.getUser().getUsername();
            com.debisys.tools.noack.NoAckPinsManagement.chargePinAmount(username, transactionId);
            return new ResponseEntity<String>("Ok", HttpStatus.OK);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage(), err);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT + "/returnPin", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> returnPin(@RequestParam(value = "pin") String pin, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {        
        try {
            dao.returnPin(pin);
            return new ResponseEntity<String>("Ok", HttpStatus.OK);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT + "/autitPinView", method = RequestMethod.POST)
    @ResponseBody
    public int autitPinView(@RequestParam(value = "controlNumber") Long controlNumber, Model model, Locale locale, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Map<String, String> param = new HashMap<String, String>();
            param.put("controlNumber", controlNumber.toString());
            param.put("user_name", sessionData.getUser().getUsername());
            param.put("instance", DebisysConfigListener.getInstance(request.getSession().getServletContext()));
            param.put("reason", "PIN with control number [" + controlNumber + "] has view by " + sessionData.getUser().getUsername());
            return dao.autitPinView(param);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.NO_ACK_PIN_MANAGEMENT + "/langs", method = RequestMethod.GET)
    @ResponseBody
    public String getLangs(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            List<String> list = new ArrayList<String>();
            list.add("jsp.admin.genericLabel.search");
            list.add("jsp.admin.genericLabel.next");
            list.add("jsp.admin.genericLabel.back");

            list.add("jsp.admin.genericLabel.search.empty");
            list.add("jsp.admin.genericLabel.table.showing");
            list.add("jsp.admin.genericLabel.table.of");
            list.add("jsp.admin.genericLabel.table.entries");

            list.add("reports.apply");
            list.add("reports.cancel");
            list.add("reports.from");
            list.add("reports.to");
            list.add("reports.custom");

            list.add("reports.day.Su");
            list.add("reports.day.Mo");
            list.add("reports.day.Tu");
            list.add("reports.day.We");
            list.add("reports.day.Th");
            list.add("reports.day.Fr");
            list.add("reports.day.Sa");

            list.add("reports.monthName.Jan");
            list.add("reports.monthName.Feb");
            list.add("reports.monthName.Mar");
            list.add("reports.monthName.Apr");
            list.add("reports.monthName.May");
            list.add("reports.monthName.Jun");
            list.add("reports.monthName.Jul");
            list.add("reports.monthName.Aug");
            list.add("reports.monthName.Sep");
            list.add("reports.monthName.Oct");
            list.add("reports.monthName.Nov");
            list.add("reports.monthName.Dec");

            list.add("ack.management.main.err1");
            list.add("ack.management.main.warn1");

            return language.mapperLanguage(sessionData, list);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage(), err);
            return new String();
        }
    }

    private Date strToDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException ex) {
            LOG.error(ex.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
