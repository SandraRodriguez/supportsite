package net.emida.supportsite.tools.bulkPayment.repository;

import java.util.Date;
import java.util.List;
import net.emida.supportsite.tools.bulkPayment.dto.BulkPaymentDto;
import net.emida.supportsite.tools.bulkPayment.services.StorageException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.http.ResponseEntity;

/**
 * <strong>DTU-3582 / Support Site Bulk Merchant Payment Feature</strong>
 *
 * @author janez@emida.net
 * @since 21-02-2017
 * @version 1.0
 */
public interface IBulkPaymentDao {
    
    /**
     * 
     * @param startDt
     * @param endDate
     * @param repId
     * @return
     * @throws TechnicalException 
     */
    public List<BulkPaymentDto> getAllBulkPayments(Date startDt, Date endDate, long repId) throws TechnicalException;

    /**
     *
     * @param bulkPaymentDto
     * @return
     * @throws StorageException
     */
    public ResponseEntity<String> storeBulkPayment(BulkPaymentDto bulkPaymentDto) throws StorageException;       
    
    /**
     * 
     * @param repId
     * @param fileName
     * @return
     * @throws TechnicalException 
     */
    public boolean isValidFileName(long repId, String fileName) throws TechnicalException;
    
    /**
     * 
     * @param repId
     * @return
     * @throws TechnicalException 
     */
    public int getLevelType(long repId) throws TechnicalException;
}
