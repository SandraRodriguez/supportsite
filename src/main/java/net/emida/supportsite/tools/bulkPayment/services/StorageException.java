package net.emida.supportsite.tools.bulkPayment.services;

/**
 * <strong>DTU-3582 / Support Site Bulk Merchant Payment Feature</strong>
 *
 * @author janez@emida.net
 * @since 21-02-2017
 * @version 1.0
 */
public class StorageException extends RuntimeException {

    /**
     *
     * @param message
     */
    public StorageException(String message) {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
