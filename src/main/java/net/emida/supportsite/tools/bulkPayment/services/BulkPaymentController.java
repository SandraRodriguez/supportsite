package net.emida.supportsite.tools.bulkPayment.services;

import au.com.bytecode.opencsv.CSVWriter;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.tools.bulkPayment.dto.BulkPaymentDto;
import net.emida.supportsite.tools.bulkPayment.repository.IBulkPaymentDao;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <strong>DTU-3582 / Support Site Bulk Merchant Payment Feature</strong>
 *
 * @author janez@emida.net
 * @since 21-02-2017
 * @version 1.0
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class BulkPaymentController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(BulkPaymentController.class);
    private static final String FORM_PATH = "bulkPayment/bulkPayment";
    private static final String ADD_BULK_PAYMENT_FORM_PATH = "bulkPayment/newBulkPayment";

    @Autowired
    private StorageService storageService;

    @Autowired
    private IBulkPaymentDao bulkPaymentDao;

    /**
     *
     * @param message
     * @param modelMap
     * @return
     */
    @RequestMapping(value = UrlPaths.BULK_PAYMENT, method = RequestMethod.GET)
    public String showBulkPaymentForm(@RequestParam("message") String message, ModelMap modelMap) {
        modelMap.addAttribute("message", message);
        return FORM_PATH;
    }

    @RequestMapping(value = "/findPaymentsByRange", method = RequestMethod.GET)
    @ResponseBody
    public List<BulkPaymentDto> onFindHistoryBalance(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        log.debug("executed::onFindHistoryBalance::" + startDt + "\t" + endDt);
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            List<BulkPaymentDto> balanceHistoryDto = storageService.getAllBulkPayments(
                    parseStringToDate(startDt),
                    parseStringToDate(endDt),
                    new Long(sessionData.getUser().getRefId())
            );
            return balanceHistoryDto;
        } catch (TechnicalException e) {
            return null;
        }
    }

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping(value = UrlPaths.BULK_PAYMENT + UrlPaths.RUN_NEW_BULK_PAYMENT, method = RequestMethod.GET)
    public String showNewBulkPayment(Map model) {
        return ADD_BULK_PAYMENT_FORM_PATH;
    }

    /**
     *
     * @param model
     * @param locale
     * @param req
     * @param httpSession
     * @param response
     */
    @RequestMapping(value = UrlPaths.BULK_PAYMENT + "/downloadTemplate", method = RequestMethod.GET)
    public void downloadTemplate(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession, HttpServletResponse response) {
        CSVWriter writer = null;
        try {
            writer = new CSVWriter(new OutputStreamWriter(response.getOutputStream()));
            buildCSVReport(writer);
            response.addHeader("Content-Disposition", "attachment;filename=bulkPayment.csv");
            response.setContentType("text/csv");
            response.flushBuffer();            
        } catch (Exception e) {
            log.info(e.getMessage());
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                log.info(ex.getMessage());
            }
        }
    }

    private void buildCSVReport(CSVWriter writer) {
        List<String[]> data = new ArrayList<String[]>();
        data.add(new String[]{
            "EntityType",
            "EntityId",
            "Amount",
            "Description",
            "FlexPay"
        });
        writer.writeAll(data);
    }

    /**
     *
     * @param xls_filename
     * @param modelMap
     * @param attr
     * @param httpSession
     * @return
     */
    @RequestMapping(value = UrlPaths.BULK_PAYMENT + UrlPaths.RUN_NEW_BULK_PAYMENT, method = RequestMethod.POST)
    public String runNewBulkPayment(@RequestParam("xls_filename") MultipartFile xls_filename, ModelMap modelMap, RedirectAttributes attr, HttpSession httpSession) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        try {
            log.debug("so_call::runNewBulkPayment::name" + xls_filename.getOriginalFilename() + "\tgetContentType::" + xls_filename.getContentType() + "\tgetSize::" + xls_filename.getSize());
            boolean isValidFileName = bulkPaymentDao.isValidFileName(Long.parseLong(sessionData.getProperty("ref_id")), xls_filename.getOriginalFilename());
            if (isValidFileName) {
                long repId = Long.parseLong(sessionData.getProperty("ref_id"));
                storageService.store(
                        bulkPaymentDao.getLevelType(repId),
                        repId,
                        sessionData.getProperty("username").toString(),
                        xls_filename);
                modelMap.addAttribute("message", "add sucessful");
                return "redirect:/tools/bulkPayment?message=SUCCESS : " + Languages.getString("jsp.tools.bulkpayment.upload.succ", sessionData.getLanguage());
            } else {
                return "redirect:/tools/bulkPayment?message=ERROR : " + Languages.getString("jsp.tools.bulkpayment.upload.invalid", sessionData.getLanguage());
            }
        } catch (Exception e) {
            log.debug("ERROR::" + e.getLocalizedMessage());
            log.debug("ERROR::" + e.getMessage());
            String returnMessage = Languages.getString("jsp.tools.bulkpayment.not.allowed", sessionData.getLanguage());
            modelMap.addAttribute("message", returnMessage);
            return "redirect:/tools/bulkPayment?message=ERROR : " + returnMessage;
        }
    }

    private static Date parseStringToDate(String currentDt) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return simpleDateFormat.parse(currentDt);
        } catch (ParseException ex) {
            return null;
        }
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 15;
    }

}
