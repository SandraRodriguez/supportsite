package net.emida.supportsite.tools.bulkPayment.services;

import java.io.IOException;
import java.util.Date;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.emida.supportsite.tools.bulkPayment.dto.BulkPaymentDto;
import net.emida.supportsite.tools.bulkPayment.repository.IBulkPaymentDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <strong>DTU-3582 / Support Site Bulk Merchant Payment Feature</strong>
 *
 * @author janez@emida.net
 * @since 21-02-2017
 * @version 1.0
 */
@Service
public class FileSystemStorageService implements StorageService {

    @Autowired
    IBulkPaymentDao bulkPaymentDao;

    /**
     * 
     * @param entityType
     * @param entityID
     * @param logonId
     * @param file 
     */
    @Override
    public void store(int entityType, long entityID, String logonId, MultipartFile file) {
        if (file.isEmpty()) {
            throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
        } else {
            try {
                byte[] bytes = file.getBytes();
                if (file.getContentType().equalsIgnoreCase("text/csv") || file.getContentType().equalsIgnoreCase("application/vnd.ms-excel")) {

                    /**
                     *
                     * @param bulkPaymentDto
                     * @return
                     * @throws StorageException
                     */
                    bulkPaymentDao.storeBulkPayment(new BulkPaymentDto(entityType, entityID, logonId, file.getOriginalFilename(), bytes));
                } else {
                    throw new IllegalArgumentException("Content Type not allowed.!");
                }
            } catch (IOException ex) {
                Logger.getLogger(FileSystemStorageService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @return @throws TechnicalException
     */
    @Override
    public List<BulkPaymentDto> getAllBulkPayments(Date startDt, Date endDate, long repId) throws TechnicalException {
        return bulkPaymentDao.getAllBulkPayments(startDt, endDate, repId);
    }
}
