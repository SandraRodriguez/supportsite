package net.emida.supportsite.tools.bulkPayment.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.emida.supportsite.tools.bulkPayment.dto.BulkPaymentDto;
import net.emida.supportsite.tools.bulkPayment.services.StorageException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * <strong>DTU-3582 / Support Site Bulk Merchant Payment Feature</strong>
 *
 * @author janez@emida.net
 * @since 21-02-2017
 * @version 1.0
 */
@Repository(value = "IBulkPaymentDao")
public class BulkPaymentImpl implements IBulkPaymentDao {

    private static final String INSERT_INTO_BULK_PAYMENT_TABLE = "INSERT INTO BulkPayment (id, entityType, entityID, logonId, fileName, streamByte, loadDt, status) "
            + "VALUES (NEWID(), ?, ?, ?, ?, ?, getdate(), 'LOADED')";

    private static final String FIND_ALL_BULK_PAYMENT_PROCESS = "SELECT * FROM BulkPayment WHERE loadDt >= ? AND loadDt <= CAST(CONVERT(VARCHAR(10), ?, 110) + ' 23:59:59' AS DATETIME) "
            + "AND entityId = ? ORDER BY loadDt DESC";
    private static final String FIND_EXIST_FILE_NAME = "SELECT fileName FROM dbo.BulkPayment WHERE entityID = ? AND fileName = ?";

    private static final String FIND_LEVEL_LOADER = "select r.type from reps r where r.rep_id = ?";

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * @param repId
     * @param fileName
     * @return
     * @throws TechnicalException
     */
    @Override
    public boolean isValidFileName(long repId, String fileName) throws TechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = this.jdbcTemplate.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(FIND_EXIST_FILE_NAME);
            preparedStatement.setLong(1, repId);
            preparedStatement.setString(2, fileName);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString("fileName").equalsIgnoreCase(fileName)) {
                    return false;
                }
            }

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @param bulkPaymentDto
     * @return
     * @throws StorageException
     */
    @Override
    public ResponseEntity<String> storeBulkPayment(BulkPaymentDto bulkPaymentDto) throws StorageException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = this.jdbcTemplate.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(INSERT_INTO_BULK_PAYMENT_TABLE);
            preparedStatement.setInt(1, bulkPaymentDto.getEntityType());
            preparedStatement.setLong(2, bulkPaymentDto.getEntityID());
            preparedStatement.setString(3, bulkPaymentDto.getLogonId());
            preparedStatement.setString(4, bulkPaymentDto.getFileName());
            preparedStatement.setBytes(5, bulkPaymentDto.getOutputStream());

            preparedStatement.execute();
            return ResponseEntity.status(HttpStatus.OK).body("success");
        } catch (SQLException ex) {
            Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("fail::" + ex.getMessage());
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @param startDt
     * @param endDate
     * @param repId
     * @return
     * @throws TechnicalException
     */
    @Override
    public List<BulkPaymentDto> getAllBulkPayments(Date startDt, Date endDate, long repId) throws TechnicalException {
        List<BulkPaymentDto> bulkPaymentDtos = this.jdbcTemplate.query(
                FIND_ALL_BULK_PAYMENT_PROCESS, new Object[]{
                    startDt,
                    endDate,
                    repId
                },
                new RowMapper<BulkPaymentDto>() {
            /**
             *
             * @param rs
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public BulkPaymentDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                BulkPaymentDto bulkPaymentDto = new BulkPaymentDto(
                        resultSet.getString("fileName"),
                        resultSet.getDate("loadDt"),
                        resultSet.getDate("endProcess"),
                        resultSet.getInt("rowsProcessed"),
                        resultSet.getString("status"));
                return bulkPaymentDto;
            }
        });
        return bulkPaymentDtos;
    }

    /**
     *
     * @param repId
     * @return
     * @throws TechnicalException
     */
    @Override
    public int getLevelType(long repId) throws TechnicalException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = this.jdbcTemplate.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(FIND_LEVEL_LOADER);
            preparedStatement.setLong(1, repId);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();
            if (!resultSet.wasNull()) {
                return resultSet.getInt("type");
            }
        } catch (SQLException ex) {
            Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        } finally {
            try {
                resultSet.close();
                preparedStatement.close();
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return -1;
    }

}
