package net.emida.supportsite.tools.bulkPayment.dto;

import java.io.Serializable;

/**
 * <strong>Map BulkPaymentDetailDto table atributes</strong>
 *
 * @author janez@emida.net
 * @since 20/02/2017
 * @version 1.0
 */
public class BulkPaymentDetailDto implements Serializable {

    private String uid;
    private String uidBulkPayment;
    private int entityType;
    private long entityId;
    private double amount;
    private double balance;
    private String description;
    private String status;
    private String logMessage;

    public BulkPaymentDetailDto() {
    }

    /**
     *
     * @param uid
     * @param uidBulkPayment
     * @param entityType
     * @param entityId
     * @param amount
     * @param balance
     * @param description
     * @param status
     * @param logMessage
     */
    public BulkPaymentDetailDto(String uid, String uidBulkPayment, int entityType, long entityId, double amount, double balance, String description, String status, String logMessage) {
        this.uid = uid;
        this.uidBulkPayment = uidBulkPayment;
        this.entityType = entityType;
        this.entityId = entityId;
        this.amount = amount;
        this.balance = balance;
        this.description = description;
        this.status = status;
        this.logMessage = logMessage;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUidBulkPayment() {
        return uidBulkPayment;
    }

    public void setUidBulkPayment(String uidBulkPayment) {
        this.uidBulkPayment = uidBulkPayment;
    }

    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(String logMessage) {
        this.logMessage = logMessage;
    }

}
