package net.emida.supportsite.tools.bulkPayment.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * <strong>Map BulkPaymen table atributes</strong>
 *
 * @author janez@emida.net
 * @since 20/02/2017
 * @version 1.0
 */
public class BulkPaymentDto implements Serializable {

    private String uid;
    private int entityType;
    private long entityID;
    private String fileName;
    private byte[] outputStream;
    private int rowsProcessed;
    private Date loadDt;
    private Date endProcess;
    private String status;
    private String logonId;

    public BulkPaymentDto() {
    }

    /**
     * 
     * @param entityType
     * @param entityID
     * @param logonId
     * @param fileName
     * @param outputStream 
     */
    public BulkPaymentDto(int entityType, long entityID, String logonId, String fileName, byte[] outputStream) {
        this.entityType = entityType;
        this.entityID = entityID;
        this.logonId = logonId;
        this.fileName = fileName;
        this.outputStream = outputStream;
    }

    /**
     *
     * @param fileName
     * @param loadDt
     * @param endProcess
     * @param rowsProcessed
     * @param status
     */
    public BulkPaymentDto(String fileName, Date loadDt, Date endProcess, int rowsProcessed, String status) {
        this.fileName = fileName;
        this.endProcess = endProcess;
        this.loadDt = loadDt;
        this.rowsProcessed = rowsProcessed;
        this.status = status;
    }

    /**
     *
     * @param uid
     * @param fileName
     * @param outputStream
     * @param rowsProcessed
     * @param loadDt
     * @param endProcess
     * @param status
     */
    public BulkPaymentDto(String uid, String fileName, byte[] outputStream, int rowsProcessed, Date loadDt, Date endProcess, String status) {
        this.uid = uid;
        this.fileName = fileName;
        this.outputStream = outputStream;
        this.rowsProcessed = rowsProcessed;
        this.loadDt = loadDt;
        this.endProcess = endProcess;
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }

    public long getEntityID() {
        return entityID;
    }

    public void setEntityID(long entityID) {
        this.entityID = entityID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(byte[] outputStream) {
        this.outputStream = outputStream;
    }

    public int getRowsProcessed() {
        return rowsProcessed;
    }

    public void setRowsProcessed(int rowsProcessed) {
        this.rowsProcessed = rowsProcessed;
    }

    public Date getLoadDt() {
        return loadDt;
    }

    public void setLoadDt(Date loadDt) {
        this.loadDt = loadDt;
    }

    public Date getEndProcess() {
        return endProcess;
    }

    public void setEndProcess(Date endProcess) {
        this.endProcess = endProcess;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLogonId() {
        return logonId;
    }

    public void setLogonId(String logonId) {
        this.logonId = logonId;
    }

}
