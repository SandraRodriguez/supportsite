package net.emida.supportsite.tools.bulkPayment.services;

import java.util.Date;
import java.util.List;
import net.emida.supportsite.tools.bulkPayment.dto.BulkPaymentDto;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.web.multipart.MultipartFile;

/**
 * <strong>DTU-3582 / Support Site Bulk Merchant Payment Feature</strong>
 *
 * @author janez@emida.net
 * @since 21-02-2017
 * @version 1.0
 */
public interface StorageService {

    /**
     *
     * @param startDt
     * @param endDate
     * @param repId
     * @return
     * @throws TechnicalException
     */
    public List<BulkPaymentDto> getAllBulkPayments(Date startDt, Date endDate, long repId) throws TechnicalException;

    /**
     * 
     * @param entityType
     * @param entityID
     * @param logonId
     * @param file
     * @throws StorageException 
     */
    public void store(int entityType, long entityID, String logonId, MultipartFile file) throws StorageException;
}
