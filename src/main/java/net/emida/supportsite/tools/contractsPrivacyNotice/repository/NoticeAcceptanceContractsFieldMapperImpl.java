package net.emida.supportsite.tools.contractsPrivacyNotice.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.emida.supportsite.tools.bulkPayment.repository.BulkPaymentImpl;
import net.emida.supportsite.tools.bulkPayment.services.StorageException;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.AcceptanceContractsDto;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.NoticeAcceptanceContractsDto;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.NoticeAcceptanceContractsFieldMapperDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * <strong>DTU-3905 / Support Site PM - MX - Creation of notice and acceptance
 * of contracts Feature</strong>
 *
 * @author janez@emida.net
 * @since 17-04-2017
 * @version 1.0
 */
@Repository(value = "INoticeAcceptanceContractsFieldMapperDao")
public class NoticeAcceptanceContractsFieldMapperImpl implements INoticeAcceptanceContractsFieldMapperDao {

    private static final String INSERT_NEW_CONFIG = "INSERT INTO NoticeAcceptanceContracts (id, configName, entityId, createDt, modifyDt, content, status, description) "
            + "VALUES (NEWID(), ?, ?, getdate(), getdate(), ?, ?, ?)";
    private static final String FIND_ALL_FIELD_MAPPER = "select * from NoticeAcceptanceContracts_FieldMapper WITH(NOLOCK)";
    private static final String FIND_ALL_TEMPLATES = "SELECT * FROM NoticeAcceptanceContracts WITH(NOLOCK)";
    private static final String FIND_TEMPLATE_BY_ID = "SELECT * FROM NoticeAcceptanceContracts WITH(NOLOCK) WHERE id = ?";
    private static final String UPDATE_TEMPLATE = "UPDATE NoticeAcceptanceContracts SET configName = ?, entityId = ?, modifyDt = GETDATE(), status = ?, content = ?, description = ? WHERE id = ?";
    private static final String VALIDATE_STATUS_TEMPLATES = "select top 1 id from dbo.NoticeAcceptanceContracts WHERE status = 'enable'";
    private static final String UPDATE_DISABLE_TEMPALTE = "UPDATE NoticeAcceptanceContracts SET status = 'disable' WHERE id = ?";
    private static final String FIND_ACCEPTANCE_CONTRACTS_BY_RANGE = "SELECT ac.*, m.legal_businessname FROM AcceptanceContracts ac WITH(NOLOCK) INNER JOIN merchants m ON m.merchant_id = ac.entityId WHERE acceptanceDt >= ? AND acceptanceDt <= CAST(CONVERT(VARCHAR(10), ?, 110) + ' 23:59:59' AS DATETIME)";    
    private static final String FIND_LAST_ACCEPTANCE_CONTRACTSS = "SELECT  TOP 15 ac.*, m.legal_businessname FROM AcceptanceContracts ac WITH(NOLOCK) INNER JOIN merchants m ON m.merchant_id = ac.entityId ORDER BY acceptanceDt DESC";            
    private static final String FIND_CONTRACT_BY_MERCHANT_ID = "SELECT ac.*, m.legal_businessname FROM AcceptanceContracts ac WITH(NOLOCK) INNER JOIN merchants m ON m.merchant_id = ac.entityId WHERE id = ?";

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * @return @throws StorageException
     */
    @Override
    public List<NoticeAcceptanceContractsFieldMapperDto> findAllNoticeAcceptanceContractsFieldMapper() throws StorageException {
        List<NoticeAcceptanceContractsFieldMapperDto> contractsFieldMappers = this.jdbcTemplate.query(FIND_ALL_FIELD_MAPPER, new Object[]{},
                new RowMapper<NoticeAcceptanceContractsFieldMapperDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public NoticeAcceptanceContractsFieldMapperDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                NoticeAcceptanceContractsFieldMapperDto contractsFieldMapper = new NoticeAcceptanceContractsFieldMapperDto(
                        resultSet.getString("id"),
                        resultSet.getString("field"),
                        resultSet.getInt("numberParameters"),
                        resultSet.getString("queryMap"));
                return contractsFieldMapper;
            }
        });
        return contractsFieldMappers;
    }

    /**
     *
     * @param noticeAcceptanceContractsDto
     * @return
     * @throws StorageException
     */
    @Override
    public ResponseEntity<String> addNoticeAcceptanceContracts(NoticeAcceptanceContractsDto noticeAcceptanceContractsDto) throws StorageException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            connection = this.jdbcTemplate.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(INSERT_NEW_CONFIG);
            preparedStatement.setString(1, noticeAcceptanceContractsDto.getConfigName());
            preparedStatement.setLong(2, noticeAcceptanceContractsDto.getEntityId());
            preparedStatement.setBytes(3, null);
            preparedStatement.setString(4, "disable");
            preparedStatement.setString(5, noticeAcceptanceContractsDto.getDescription());

            preparedStatement.execute();
            return ResponseEntity.status(HttpStatus.OK).body("success");
        } catch (SQLException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("failure::" + ex.getMessage());
        } finally {
            try {
                connection.close();
                preparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<NoticeAcceptanceContractsDto> findConfigTemplateNoticeAcceptanceContracts() throws StorageException {
        List<NoticeAcceptanceContractsDto> templates = this.jdbcTemplate.query(FIND_ALL_TEMPLATES, new Object[]{},
                new RowMapper<NoticeAcceptanceContractsDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public NoticeAcceptanceContractsDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                NoticeAcceptanceContractsDto noticeAcceptanceContractsDto = new NoticeAcceptanceContractsDto(
                        resultSet.getString("id"),
                        resultSet.getString("configName"),
                        resultSet.getLong("entityId"),
                        resultSet.getDate("createDt"),
                        resultSet.getDate("modifyDt"),
                        resultSet.getBytes("content"),
                        resultSet.getString("status"),
                        resultSet.getString("description"),
                        null);

                return noticeAcceptanceContractsDto;
            }
        });
        return templates;
    }

    @Override
    public NoticeAcceptanceContractsDto findTemplateNoticeAcceptanceContractsById(String id) throws StorageException {
        NoticeAcceptanceContractsDto template = this.jdbcTemplate.queryForObject(FIND_TEMPLATE_BY_ID, new Object[]{
            id
        },
                new RowMapper<NoticeAcceptanceContractsDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public NoticeAcceptanceContractsDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                NoticeAcceptanceContractsDto noticeAcceptanceContractsDto = new NoticeAcceptanceContractsDto(
                        resultSet.getString("id"),
                        resultSet.getString("configName"),
                        resultSet.getLong("entityId"),
                        resultSet.getDate("createDt"),
                        resultSet.getDate("modifyDt"),
                        (resultSet.getBytes("content") == null ? "".getBytes() : new String(resultSet.getBytes("content")).getBytes()),
                        resultSet.getString("status"),
                        resultSet.getString("description"),
                        (resultSet.getString("content") == null ? "" : new String(resultSet.getBytes("content"))));

                return noticeAcceptanceContractsDto;
            }
        });
        return template;
    }

    @Override
    public ResponseEntity<String> updateNoticeAcceptanceContracts(NoticeAcceptanceContractsDto noticeAcceptanceContractsDto) throws StorageException {
        PreparedStatement preparedStatement = null;
        Connection connection = null;
        try {
            String hline = noticeAcceptanceContractsDto.getTempContent().replaceAll("<hr />", "<hr style='border: 1 !important; height: 1px; background-color: #dadada'/>");
            connection = this.jdbcTemplate.getDataSource().getConnection();
            preparedStatement = connection.prepareStatement(UPDATE_TEMPLATE);
            preparedStatement.setString(1, noticeAcceptanceContractsDto.getConfigName());
            preparedStatement.setLong(2, noticeAcceptanceContractsDto.getEntityId());
            preparedStatement.setString(3, noticeAcceptanceContractsDto.getStatus());
//            preparedStatement.setBytes(4, org.apache.commons.codec.binary.StringUtils.getBytesUtf8(noticeAcceptanceContractsDto.getTempContent()));
            preparedStatement.setBytes(4, hline.getBytes());
            preparedStatement.setString(5, noticeAcceptanceContractsDto.getDescription());
            preparedStatement.setString(6, noticeAcceptanceContractsDto.getId());

            if (noticeAcceptanceContractsDto.getStatus().equals("enable")) {
                /**
                 * Solo debe haber una plantilla habilitada, en caso que exista
                 * una habilitada la deshabilita y solo deja esta en caso que se
                 * quiera habilitar.
                 */
                PreparedStatement preparedStatementUpdater = null;
                ResultSet resultSet = null;
                try {
                    preparedStatementUpdater = connection.prepareStatement(VALIDATE_STATUS_TEMPLATES);
                    resultSet = preparedStatementUpdater.executeQuery();
                    resultSet.next();
                    if (resultSet.getString("id") != null) {
                        PreparedStatement preparedStatementAction = connection.prepareStatement(UPDATE_DISABLE_TEMPALTE);
                        preparedStatementAction.setString(1, resultSet.getString("id"));

                        preparedStatementAction.executeUpdate();
                        preparedStatementAction.close();
                    }
                } catch (SQLException exception) {
                    System.out.println("ERROR::" + exception.getLocalizedMessage());
                } finally {
                    resultSet.close();
                    preparedStatementUpdater.close();
                }
            }

            preparedStatement.executeUpdate();
            return ResponseEntity.status(HttpStatus.OK).body("success");
        } catch (SQLException ex) {
            System.out.println("ERROR::" + ex.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("failure::" + ex.getMessage());
        } finally {
            try {
                preparedStatement.close();
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(BulkPaymentImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public List<AcceptanceContractsDto> findLastAcceptanceContractsByQueryRangue() throws StorageException {
        List<AcceptanceContractsDto> templates = this.jdbcTemplate.query(FIND_LAST_ACCEPTANCE_CONTRACTSS, new Object[]{},
                new RowMapper<AcceptanceContractsDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public AcceptanceContractsDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                AcceptanceContractsDto noticeAcceptanceContractsDto = new AcceptanceContractsDto(
                        resultSet.getString("id"),
                        resultSet.getLong("entityId"),
                        resultSet.getString("legal_businessname"),
                        resultSet.getString("logonId"),
                        resultSet.getInt("isAccept"),
                        resultSet.getDate("acceptanceDt")
                );

                return noticeAcceptanceContractsDto;
            }
        });
        return templates;
    }

    /**
     * 
     * @param startDt
     * @param endDt
     * @return
     * @throws StorageException 
     */
    @Override
    public List<AcceptanceContractsDto> findAcceptanceContractsByQueryRangue(Date startDt, Date endDt) throws StorageException {
        List<AcceptanceContractsDto> templates = this.jdbcTemplate.query(FIND_ACCEPTANCE_CONTRACTS_BY_RANGE, new Object[]{
            new java.sql.Date(startDt.getTime()),
            new java.sql.Date(endDt.getTime())
        },
                new RowMapper<AcceptanceContractsDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public AcceptanceContractsDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                AcceptanceContractsDto noticeAcceptanceContractsDto = new AcceptanceContractsDto(
                        resultSet.getString("id"),
                        resultSet.getLong("entityId"),
                        resultSet.getString("legal_businessname"),
                        resultSet.getString("logonId"),
                        resultSet.getInt("isAccept"),
                        resultSet.getDate("acceptanceDt")
                );

                return noticeAcceptanceContractsDto;
            }
        });
        return templates;
    }

    @Override
    public AcceptanceContractsDto findAcceptanceContractsById(String id) throws StorageException {
        AcceptanceContractsDto template = this.jdbcTemplate.queryForObject(FIND_CONTRACT_BY_MERCHANT_ID, new Object[]{
            id
        },
                new RowMapper<AcceptanceContractsDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public AcceptanceContractsDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                AcceptanceContractsDto acceptanceContract = new AcceptanceContractsDto(
                        resultSet.getString("id"),
                        resultSet.getLong("entityId"),
                        resultSet.getString("legal_businessname"),
                        resultSet.getString("logonId"),
                        resultSet.getInt("isAccept"),
                        resultSet.getDate("acceptanceDt"),
                        (resultSet.getString("content") == null ? "" : new String(resultSet.getBytes("content"))));

                return acceptanceContract;
            }
        });
        return template;
    }        

}
