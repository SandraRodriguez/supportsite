package net.emida.supportsite.tools.contractsPrivacyNotice.dto;

import java.io.Serializable;

/**
 * <strong>DTU-3905 / Support Site PM - MX - Creation of notice and acceptance
 * of contracts Feature</strong>
 *
 * @author janez@emida.net
 * @since 17-04-2017
 * @version 1.0
 */
public class NoticeAcceptanceContractsFieldMapperDto implements Serializable {

    private String id;
    private String field;
    private int numberParameters;
    private String queryMap;

    public NoticeAcceptanceContractsFieldMapperDto() {
    }

    /**
     *
     * @param id
     * @param field
     * @param numberParameters
     * @param queryMap
     */
    public NoticeAcceptanceContractsFieldMapperDto(String id, String field, int numberParameters, String queryMap) {
        this.id = id;
        this.field = field;
        this.numberParameters = numberParameters;
        this.queryMap = queryMap;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public int getNumberParameters() {
        return numberParameters;
    }

    public void setNumberParameters(int numberParameters) {
        this.numberParameters = numberParameters;
    }

    public String getQueryMap() {
        return queryMap;
    }

    public void setQueryMap(String queryMap) {
        this.queryMap = queryMap;
    }

}
