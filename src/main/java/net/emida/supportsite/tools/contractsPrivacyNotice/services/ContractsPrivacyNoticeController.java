package net.emida.supportsite.tools.contractsPrivacyNotice.services;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.tools.bulkPayment.services.StorageException;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.AcceptanceContractsDto;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.NoticeAcceptanceContractsDto;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.NoticeAcceptanceContractsFieldMapperDto;
import net.emida.supportsite.tools.contractsPrivacyNotice.repository.INoticeAcceptanceContractsFieldMapperDao;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <strong>DTU-3905 / Support Site PM - MX - Creation of notice and acceptance
 * of contracts Feature</strong>
 *
 * @author janez@emida.net
 * @since 17-04-2017
 * @version 1.0
 */
@Controller
public class ContractsPrivacyNoticeController extends BaseController {

    private static final String FORM_PATH = "tools/contractsPrivacyNotice/contractsPrivacyNotice";
    private static final String MAIN_PAGE_ACCEPTANCE_CONTRACT = "tools/contractsPrivacyNotice/acceptanceContracts";
    private static final Logger log = LoggerFactory.getLogger(ContractsPrivacyNoticeController.class);

    @Autowired
    private INoticeAcceptanceContractsFieldMapperDao fieldMapperDao;

    /**
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.CONTRACTS_PRIVACY_NOTICER, method = RequestMethod.GET)
    public String showContractsPrivacyNoticeControllerWebPage__HTTP_GET(ModelMap modelMap) {
        return FORM_PATH;
    }

    /**
     *
     * @param modelMap
     * @return
     */
    @RequestMapping(value = UrlPaths.REPORT_PATH + UrlPaths.ACCEPTANCE_CONTRACTS, method = RequestMethod.GET)
    public String showAcceptanceContracts__HTTP_GET(ModelMap modelMap) {
        return MAIN_PAGE_ACCEPTANCE_CONTRACT;
    }

    @RequestMapping(value = UrlPaths.REPORT_PATH + UrlPaths.ACCEPTANCE_CONTRACTS + "/queryLast", method = RequestMethod.GET)
    @ResponseBody
    public List<AcceptanceContractsDto> findLastAcceptanceContractsByQueryRangue_HTTP_GET(
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return fieldMapperDao.findLastAcceptanceContractsByQueryRangue();
        } catch (TechnicalException err) {
            log.error("err::" + err);
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.REPORT_PATH + UrlPaths.ACCEPTANCE_CONTRACTS + "/queryRangue", method = RequestMethod.GET)
    @ResponseBody
    public List<AcceptanceContractsDto> findAcceptanceContractsByQueryRangue_HTTP_GET(
            @RequestParam(value = "startDt") String startDt,
            @RequestParam(value = "endDt") String endDt,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        System.out.println("executed::findAcceptanceContractsByQueryRangue_HTTP_GET::" + startDt + "\t" + endDt);
        try {
            return fieldMapperDao.findAcceptanceContractsByQueryRangue(
                    parseStringToDate(startDt),
                    parseStringToDate(endDt));
        } catch (TechnicalException err) {
            log.error("err::" + err);
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.REPORT_PATH + UrlPaths.ACCEPTANCE_CONTRACTS + "/queryById", method = RequestMethod.GET)
    @ResponseBody
    public AcceptanceContractsDto findAcceptanceContractsByMerchant_HTTP_GET(
            @RequestParam(value = "id") String id,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) throws JRException {
        System.out.println("executed::findAcceptanceContractsByMerchant_HTTP_GET::" + id);
        try {
            return fieldMapperDao.findAcceptanceContractsById(id);
        } catch (TechnicalException err) {
            log.error("err::" + err);
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.REPORT_PATH + UrlPaths.ACCEPTANCE_CONTRACTS + "/pdf", method = RequestMethod.GET)
    public void getPdfReport_HTTP_GET(@RequestParam(value = "id") String id, HttpServletResponse response,
            Locale locale, HttpSession httpSession) throws IOException {
        com.itextpdf.text.Document document = new com.itextpdf.text.Document(com.itextpdf.text.PageSize.LETTER);
        try {
            com.itextpdf.text.pdf.PdfWriter.getInstance(document, response.getOutputStream());
            document.open();

            com.itextpdf.text.html.simpleparser.HTMLWorker htmlWorker = new com.itextpdf.text.html.simpleparser.HTMLWorker(document);
            htmlWorker.parse(new StringReader(buildReport(id)));

            document.close();
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (Exception ex) {
            System.out.println("pailas");
        }
    }

    private String buildReport(String id) {
        String content = "<font size='1'>";
        content += fieldMapperDao.findAcceptanceContractsById(id).getContent().replace("hr", "p");
        content += "</font>";
        return content;
    }

    /**
     *
     * @param model
     * @param locale
     * @param req
     * @return
     */
    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.CONTRACTS_PRIVACY_NOTICER + "/findConfigTemplate", method = RequestMethod.GET)
    @ResponseBody
    public List<NoticeAcceptanceContractsFieldMapperDto> onFindConfigTemplate__HTTP_GET(Model model, Locale locale, HttpServletRequest req) {
        System.out.println("so__call::onFindConfigTemplate__HTTP_GET");
        try {
            return fieldMapperDao.findAllNoticeAcceptanceContractsFieldMapper();
        } catch (TechnicalException e) {
            log.debug("ERROR::" + e.getLocalizedMessage());
            log.debug("ERROR::" + e.getMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.CONTRACTS_PRIVACY_NOTICER + "/findAllTemplates", method = RequestMethod.GET)
    @ResponseBody
    public List<NoticeAcceptanceContractsDto> onFindConfigTemplateById__HTTP_GET(Model model, Locale locale, HttpServletRequest req) {
        System.out.println("so__call::onFindConfigTemplateById__HTTP_GET");
        try {
            return fieldMapperDao.findConfigTemplateNoticeAcceptanceContracts();
        } catch (TechnicalException e) {
            log.debug("ERROR::" + e.getLocalizedMessage());
            log.debug("ERROR::" + e.getMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.CONTRACTS_PRIVACY_NOTICER + "/findTemplateById", method = RequestMethod.GET)
    @ResponseBody
    public NoticeAcceptanceContractsDto onfindTemplateById__HTTP_GET(@RequestParam(value = "id") String id, Model model, Locale locale, HttpServletRequest req) {
        System.out.println("so__call::onfindTemplateById__HTTP_GET");
        try {
            return fieldMapperDao.findTemplateNoticeAcceptanceContractsById(id);
        } catch (TechnicalException e) {
            log.debug("ERROR::TechnicalException::" + e.getLocalizedMessage());
            log.debug("ERROR::TechnicalException::" + e.getMessage());
            return null;
        } catch (StorageException exception) {
            log.debug("ERROR::StorageException::" + exception.getLocalizedMessage());
            log.debug("ERROR::StorageException::" + exception.getMessage());
            return null;
        } catch (Exception exception) {
            log.debug("ERROR::Exception::" + exception.getLocalizedMessage());
            log.debug("ERROR::Exception::" + exception.getMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.CONTRACTS_PRIVACY_NOTICER + "/add", method = RequestMethod.POST)
    public String addNoticeAcceptanceContracts__HTTP_POST(NoticeAcceptanceContractsDto content, ModelMap modelMap, RedirectAttributes attr, HttpSession httpSession) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        try {
            content.setEntityId(new Long(sessionData.getUser().getRefId()));
            fieldMapperDao.addNoticeAcceptanceContracts(content);
            modelMap.addAttribute("message", "add sucessful");
            return "redirect:/tools/" + UrlPaths.CONTRACTS_PRIVACY_NOTICER;
        } catch (Exception e) {
            log.debug("ERROR::" + e.getLocalizedMessage());
            log.debug("ERROR::" + e.getMessage());
            String returnMessage = Languages.getString("jsp.tools.bulkpayment.not.allowed", sessionData.getLanguage());
            modelMap.addAttribute("message", returnMessage);
            return "redirect:/tools/contractsPrivacyNotice?message=ERROR : " + returnMessage;
        }
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.CONTRACTS_PRIVACY_NOTICER + "/update", method = RequestMethod.POST)
    public String updateNoticeAcceptanceContracts__HTTP_POST(NoticeAcceptanceContractsDto content, ModelMap modelMap, RedirectAttributes attr, HttpSession httpSession) {
        System.out.println("so__call::updateNoticeAcceptanceContracts__HTTP_POST" + content.toString());
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        try {
            content.setEntityId(new Long(sessionData.getUser().getRefId()));
            fieldMapperDao.updateNoticeAcceptanceContracts(content);
            modelMap.addAttribute("message", "update sucessful");
            return "redirect:/tools/" + UrlPaths.CONTRACTS_PRIVACY_NOTICER;
        } catch (Exception e) {
            log.debug("ERROR::" + e.getLocalizedMessage());
            log.debug("ERROR::" + e.getMessage());
            String returnMessage = Languages.getString("jsp.tools.bulkpayment.not.allowed", sessionData.getLanguage());
            modelMap.addAttribute("message", returnMessage);
            return "redirect:/tools/contractsPrivacyNotice?message=ERROR : " + returnMessage;
        }
    }

    private Date parseStringToDate(String currentDt) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return simpleDateFormat.parse(currentDt);
        } catch (ParseException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 16;
    }

}
