package net.emida.supportsite.tools.contractsPrivacyNotice.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * <strong>DTU-3905 / Support Site PM - MX - Creation of notice and acceptance
 * of contracts Feature</strong>
 *
 * @author janez@emida.net
 * @since 17-04-2017
 * @version 1.0
 */
public class NoticeAcceptanceContractsDto implements Serializable {

    private String id;
    private String configName;
    private long entityId;
    private Date createDt;
    private Date modifyDt;
    private byte[] content;
    private String status;
    private String description;
    private String tempContent;

    public NoticeAcceptanceContractsDto() {
    }

    /**
     *
     * @param id
     * @param configName
     * @param entityId
     * @param createDt
     * @param modifyDt
     * @param content
     * @param status
     * @param description
     * @param tempContent
     */
    public NoticeAcceptanceContractsDto(String id, String configName, long entityId, Date createDt, Date modifyDt, byte[] content, String status, String description, String tempContent) {
        this.id = id;
        this.configName = configName;
        this.entityId = entityId;
        this.createDt = createDt;
        this.modifyDt = modifyDt;
        this.content = content;
        this.status = status;
        this.description = description;
        this.tempContent = tempContent;
    }

    public String getTempContent() {
        return tempContent;
    }

    public void setTempContent(String tempContent) {
        this.tempContent = tempContent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public Date getCreateDt() {
        return createDt;
    }

    public void setCreateDt(Date createDt) {
        this.createDt = createDt;
    }

    public Date getModifyDt() {
        return modifyDt;
    }

    public void setModifyDt(Date modifyDt) {
        this.modifyDt = modifyDt;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "id::" + getId() + "\tgetConfigName::" + getConfigName() + "\tgetStatus::" + getStatus();
    }

}
