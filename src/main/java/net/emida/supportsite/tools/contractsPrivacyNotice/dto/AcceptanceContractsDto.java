package net.emida.supportsite.tools.contractsPrivacyNotice.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * <strong>DTU-3905 / Support Site PM - MX - Creation of notice and acceptance
 * of contracts Feature</strong>
 *
 * @author janez@emida.net
 * @since 17-04-2017
 * @version 1.0
 */
public class AcceptanceContractsDto implements Serializable {

    private String id;
    private long entityId;
    private String legal_businessname;
    private String logonId;
    private int isAccept;
    private Date acceptanceDt;
//    private byte[] content;
    private String content;

    public AcceptanceContractsDto() {
    }

    /**
     * 
     * @param id
     * @param entityId
     * @param legal_businessname
     * @param logonId
     * @param isAccept
     * @param acceptanceDt 
     */
    public AcceptanceContractsDto(String id, long entityId, String legal_businessname, String logonId, int isAccept, Date acceptanceDt) {
        this.id = id;
        this.entityId = entityId;
        this.legal_businessname = legal_businessname;
        this.logonId = logonId;
        this.isAccept = isAccept;
        this.acceptanceDt = acceptanceDt;
    }

    /**
     *
     * @param id
     * @param entityId
     * @param legal_businessname
     * @param logonId
     * @param isAccept
     * @param acceptanceDt
     * @param content
     */
    public AcceptanceContractsDto(String id, long entityId, String legal_businessname, String logonId, int isAccept, Date acceptanceDt, String content) {
        this.id = id;
        this.entityId = entityId;
        this.legal_businessname = legal_businessname;
        this.logonId = logonId;
        this.isAccept = isAccept;
        this.acceptanceDt = acceptanceDt;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public String getLegal_businessname() {
        return legal_businessname;
    }

    public void setLegal_businessname(String legal_businessname) {
        this.legal_businessname = legal_businessname;
    }

    public String getLogonId() {
        return logonId;
    }

    public void setLogonId(String logonId) {
        this.logonId = logonId;
    }

    public int getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(int isAccept) {
        this.isAccept = isAccept;
    }

    public Date getAcceptanceDt() {
        return acceptanceDt;
    }

    public void setAcceptanceDt(Date acceptanceDt) {
        this.acceptanceDt = acceptanceDt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
