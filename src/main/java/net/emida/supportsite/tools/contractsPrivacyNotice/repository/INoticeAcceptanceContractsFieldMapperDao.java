package net.emida.supportsite.tools.contractsPrivacyNotice.repository;

import java.util.Date;
import java.util.List;
import net.emida.supportsite.tools.bulkPayment.services.StorageException;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.AcceptanceContractsDto;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.NoticeAcceptanceContractsDto;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.NoticeAcceptanceContractsFieldMapperDto;
import org.springframework.http.ResponseEntity;

/**
 * <strong>DTU-3905 / Support Site PM - MX - Creation of notice and acceptance
 * of contracts Feature</strong>
 *
 * @author janez@emida.net
 * @since 17-04-2017
 * @version 1.0
 */
public interface INoticeAcceptanceContractsFieldMapperDao {

    /**
     *
     * @return @throws StorageException
     */
    public List<NoticeAcceptanceContractsFieldMapperDto> findAllNoticeAcceptanceContractsFieldMapper() throws StorageException;

    /**
     *
     * @param noticeAcceptanceContractsDto
     * @return
     * @throws StorageException
     */
    public ResponseEntity<String> addNoticeAcceptanceContracts(NoticeAcceptanceContractsDto noticeAcceptanceContractsDto) throws StorageException;

    /**
     *
     * @return @throws StorageException
     */
    public List<NoticeAcceptanceContractsDto> findConfigTemplateNoticeAcceptanceContracts() throws StorageException;

    /**
     *
     * @param id
     * @return
     * @throws StorageException
     */
    public NoticeAcceptanceContractsDto findTemplateNoticeAcceptanceContractsById(String id) throws StorageException;

    /**
     *
     * @param noticeAcceptanceContractsDto
     * @return
     * @throws StorageException
     */
    public ResponseEntity<String> updateNoticeAcceptanceContracts(NoticeAcceptanceContractsDto noticeAcceptanceContractsDto) throws StorageException;

    /**
     *
     * @return @throws StorageException
     */
    public List<AcceptanceContractsDto> findLastAcceptanceContractsByQueryRangue() throws StorageException;

    /**
     *
     * @param startDt
     * @param endDt
     * @return
     * @throws StorageException
     */
    public List<AcceptanceContractsDto> findAcceptanceContractsByQueryRangue(Date startDt, Date endDt) throws StorageException;

    /**
     * 
     * @param id
     * @return
     * @throws StorageException 
     */
    public AcceptanceContractsDto findAcceptanceContractsById(String id) throws StorageException;

}
