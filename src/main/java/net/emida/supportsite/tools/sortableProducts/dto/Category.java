package net.emida.supportsite.tools.sortableProducts.dto;

import java.io.Serializable;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-4627
 * @since 2018-07-23
 */
public class Category implements Serializable {

    private Integer id;
    private String description;
    private String code;

    public Category() {
    }

    /**
     *
     * @param id
     * @param description
     * @param code
     */
    public Category(Integer id, String description, String code) {
        this.id = id;
        this.description = description;
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
