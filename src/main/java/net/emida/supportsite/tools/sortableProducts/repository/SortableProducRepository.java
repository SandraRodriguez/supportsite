package net.emida.supportsite.tools.sortableProducts.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import net.emida.supportsite.tools.sortableProducts.dto.Carrier;
import net.emida.supportsite.tools.sortableProducts.dto.Category;
import net.emida.supportsite.tools.sortableProducts.dto.Product;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-4627
 * @since 2018-07-23
 */
@Repository
public class SortableProducRepository implements ISortableProductDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SortableProducRepository.class);

    @Override
    public List<Category> getCategories(Integer language) {
        final String query = "SELECT a.id, ptc.description, a.code  "
                + " FROM MBProductTypes a WITH(NOLOCK)  "
                + " INNER JOIN MBTransactionTypes b WITH(NOLOCK) ON a.MBTransactionType=b.id "
                + " INNER JOIN mbproducttypescodes ptc WITH(NOLOCK) ON a.Code = ptc.Code "
                + " WHERE ptc.Language = " + language
                + " ORDER BY a.PCTerminalViewOrder";
        List<Category> list = null;
        LOG.info("query :: " + query);
        try {
            list = jdbcTemplate.query(query, new RowMapper<Category>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public Category mapRow(ResultSet rs, int index) throws SQLException {
                    return new Category(
                            rs.getInt("id"),
                            rs.getString("description"),
                            rs.getString("code"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on get categories. Ex = {}", e);
        }
        return list;
    }

    @Override
    public List<Carrier> getCarriers(Integer productType) {
        final String query = "SELECT carriers.id, carriers.name "
                + " FROM products_mb WITH (NOLOCK) "
                + " INNER JOIN products WITH (NOLOCK) ON products_mb.product_id = products.id "
                + " INNER JOIN carriers WITH (NOLOCK) ON carriers.id = products.carrier_id "
                + " WHERE products_mb.MBProductType = " + productType + " "
                + " GROUP BY carriers.id, carriers.name "
                + " ORDER BY carriers.name";
        List<Carrier> list = null;
        LOG.info("query :: " + query);
        try {
            list = jdbcTemplate.query(query, new RowMapper<Carrier>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public Carrier mapRow(ResultSet rs, int index) throws SQLException {
                    return new Carrier(
                            rs.getInt("id"),
                            rs.getString("name"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on get carriers. Ex = {}", e);
        }
        return list;
    }

    @Override
    public List<Product> getProducts(Integer productType, Integer carrierId, Integer language) {
        final String query = "SELECT products.Description as description,products.id, products_mb.PCTerminalOrder as productorder "
                + " FROM products_mb WITH (NOLOCK) "
                + " INNER JOIN languages WITH (NOLOCK) ON  languages.languageid = products_mb.language"
                + " INNER JOIN products WITH (NOLOCK) ON  products_mb.product_id = products.id  "
                + " LEFT JOIN PCTerminalProducts pp WITH (NOLOCK) ON  pp.ProductId = products.id  "
                + " WHERE products.carrier_id = " + carrierId + " "
                + " AND  products_mb.MBProductType = " + productType + " "
                + " AND products_mb.language = " + language + " "
                + " GROUP BY products.Description, products.id, products_mb.PCTerminalOrder, products_mb.language , languages.languagecode "
                + " ORDER BY products_mb.PCTerminalOrder,products.Description ";
        List<Product> list = null;
        LOG.info("query :: " + query);
        try {
            list = jdbcTemplate.query(query, new RowMapper<Product>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public Product mapRow(ResultSet rs, int index) throws SQLException {
                    return new Product(
                            rs.getLong("id"),
                            rs.getString("description"),
                            rs.getInt("productorder"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on get carriers. Ex = {}", e);
        }
        return list;
    }

    @Override
    public List<Product> getProductsOrderedBy(Integer productType, Integer carrierId, Integer language, String orderBy) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT products.Description as description,products.id, products_mb.PCTerminalOrder as productorder "
                + " FROM products_mb WITH (NOLOCK) "
                + " INNER JOIN languages WITH (NOLOCK) ON  languages.languageid = products_mb.language"
                + " INNER JOIN products WITH (NOLOCK) ON  products_mb.product_id = products.id  "
                + " LEFT JOIN PCTerminalProducts pp WITH (NOLOCK) ON  pp.ProductId = products.id  "
                + " WHERE products.carrier_id = " + carrierId + " "
                + " AND  products_mb.MBProductType = " + productType + " "
                + " AND products_mb.language = " + language + " ");                
        
        if (orderBy.equalsIgnoreCase("amount")) {
            builder.append(" GROUP BY products.Description, products.id, products_mb.PCTerminalOrder, products_mb.language , languages.languagecode, products.amount");
            builder.append(" ORDER BY products.amount");
        } else if (orderBy.equalsIgnoreCase("name")) {
            builder.append(" GROUP BY products.Description, products.id, products_mb.PCTerminalOrder, products_mb.language , languages.languagecode");
            builder.append(" ORDER BY products.Description");
        } else if (orderBy.equalsIgnoreCase("sku")) {
            builder.append(" GROUP BY products.Description, products.id, products_mb.PCTerminalOrder, products_mb.language , languages.languagecode");
            builder.append(" ORDER BY products.id");
        }

        List<Product> list = null;
        LOG.info("query :: " + builder.toString());
        try {
            list = jdbcTemplate.query(builder.toString(), new RowMapper<Product>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public Product mapRow(ResultSet rs, int index) throws SQLException {
                    return new Product(
                            rs.getLong("id"),
                            rs.getString("description"),
                            rs.getInt("productorder"));
                }
            });
        } catch (Exception e) {
            LOG.error("Error on get carriers. Ex = {}", e);
        }
        return list;
    }

    @Override
    public Integer updateProductPosition(Product product) {
        final String updateProducts_mb = "UPDATE products_mb SET PCTerminalOrder = " + product.getProductOrder()
                + " WHERE product_id = " + product.getId();
        LOG.info("update products_mb :: " + updateProducts_mb);
        
        final String updatePcterminalProduct = "UPDATE PCTerminalProducts SET PCTerminalOrder = " + product.getProductOrder()
                + " WHERE ProductId = " + product.getId();
        LOG.info("update PCTerminalProducts :: " + updatePcterminalProduct);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(updateProducts_mb);
            rows += this.jdbcTemplate.update(updatePcterminalProduct);
        } catch (Exception e) {
            LOG.error("Error on update Product Position. Ex = {}", e);
        }
        return rows;
    }

}
