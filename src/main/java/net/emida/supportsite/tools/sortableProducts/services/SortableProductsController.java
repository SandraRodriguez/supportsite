package net.emida.supportsite.tools.sortableProducts.services;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.sortableProducts.dto.Carrier;
import net.emida.supportsite.tools.sortableProducts.dto.Category;
import net.emida.supportsite.tools.sortableProducts.dto.Product;
import net.emida.supportsite.tools.sortableProducts.repository.ISortableProductDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-4627
 * @since 2018-07-23
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class SortableProductsController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(SortableProductsController.class);

    @Autowired
    private ISortableProductDao dao;

    @RequestMapping(value = UrlPaths.SORTABLE_PRODUCTS, method = RequestMethod.GET)
    public String __init__() {
        return "tools/sortableProducts/sortableProducts";
    }

    @RequestMapping(value = UrlPaths.SORTABLE_PRODUCTS + "/categories", method = RequestMethod.GET)
    @ResponseBody
    public List<Category> getCategories(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            int lang = 2;
            if (sessionData.getLanguage().equalsIgnoreCase("english")) {
                lang = 1;
            }
            return dao.getCategories(lang);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.SORTABLE_PRODUCTS + "/carriers", method = RequestMethod.GET)
    @ResponseBody
    public List<Carrier> getCarriers(@RequestParam("productType") Integer productType, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return dao.getCarriers(productType);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.SORTABLE_PRODUCTS + "/products", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> getProducts(@RequestParam("productType") Integer productType, @RequestParam("carrierId") Integer carrierId, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            int lang = 2;
            if (sessionData.getLanguage().equalsIgnoreCase("english")) {
                lang = 1;
            }
            return dao.getProducts(productType, carrierId, lang);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.SORTABLE_PRODUCTS + "/productsOrdered", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> getProductsOrderedBy(@RequestParam("productType") Integer productType, @RequestParam("carrierId") Integer carrierId, @RequestParam("orderBy") String orderBy,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        int lang = 2;
        if (sessionData.getLanguage().equalsIgnoreCase("english")) {
            lang = 1;
        }
        try {
            return dao.getProductsOrderedBy(productType, carrierId, lang, orderBy);
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.SORTABLE_PRODUCTS + "/updateProductPosition", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<String> updateProductPosition(@RequestBody List<Product> products, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        try {
            int rows = 0;
            int index = 0;
            for (Product product : products) {
                index++;
                product.setProductOrder(index);

                rows += dao.updateProductPosition(product);
            }
            String responseMessage;
            if (rows > 0) {
                responseMessage = Languages.getString("sort.products.products.lbl.l1", sessionData.getLanguage());
                return new ResponseEntity<String>(responseMessage, HttpStatus.OK);
            } else {
                responseMessage = Languages.getString("sort.products.products.lbl.l2", sessionData.getLanguage());
                return new ResponseEntity<String>(responseMessage, HttpStatus.OK);
            }
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage());
            return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
