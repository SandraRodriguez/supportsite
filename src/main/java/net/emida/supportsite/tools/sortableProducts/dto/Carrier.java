package net.emida.supportsite.tools.sortableProducts.dto;

import java.io.Serializable;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-4627
 * @since 2018-07-23
 */
public class Carrier implements Serializable {

    private Integer id;
    private String name;

    public Carrier() {
    }

    /**
     *
     * @param id
     * @param name
     */
    public Carrier(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
