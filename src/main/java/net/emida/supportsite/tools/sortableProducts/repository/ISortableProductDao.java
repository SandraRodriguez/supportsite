package net.emida.supportsite.tools.sortableProducts.repository;

import java.util.List;
import net.emida.supportsite.tools.sortableProducts.dto.Carrier;
import net.emida.supportsite.tools.sortableProducts.dto.Category;
import net.emida.supportsite.tools.sortableProducts.dto.Product;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-4627
 * @since 2018-07-23
 */
public interface ISortableProductDao {

    /**
     *
     * @param language
     * @return
     */
    public List<Category> getCategories(Integer language);

    /**
     *
     * @param productType - The <code>MBProductType</code> field
     * @return
     */
    public List<Carrier> getCarriers(Integer productType);

    /**
     *
     * @param carrierId
     * @param productType - The <code>MBProductType</code> field
     * @param language
     * @return
     */
    public List<Product> getProducts(Integer productType, Integer carrierId, Integer language);

    /**
     *
     * @param carrierId
     * @param productType
     * @param language
     * @param orderBy
     * @return
     */
    public List<Product> getProductsOrderedBy(Integer productType, Integer carrierId, Integer language, String orderBy);

    /**
     *
     * @param product
     * @return
     */
    public Integer updateProductPosition(Product product);

}
