package net.emida.supportsite.tools.sortableProducts.dto;

import java.io.Serializable;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/DTU-4627
 * @since 2018-07-23
 */
public class Product implements Serializable {

    private Long id;
    private String description;
    private Integer productOrder;

    public Product() {
    }

    /**
     *
     * @param id
     * @param description
     * @param productOrder
     */
    public Product(Long id, String description, Integer productOrder) {
        this.id = id;
        this.description = description;
        this.productOrder = productOrder;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getProductOrder() {
        return productOrder;
    }

    public void setProductOrder(Integer productOrder) {
        this.productOrder = productOrder;
    }

}
