package net.emida.supportsite.tools.pinManagementStatus.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/25/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3911
 */
public class PinDto implements Serializable {

    private String ani;
    private Long pinCount;
    private Long productId;
    private String pin;
    private Timestamp insertDt;
    private String pinStatus;
    private int pinStatusId;
    private int transactionCount;
    private Timestamp activationDt;

    public PinDto() {
    }

    public PinDto(String pin, int pinStatusId) {
        this.pin = pin;
        this.pinStatusId = pinStatusId;
    }   

    /**
     * 
     * @param productId
     * @param pinCount 
     */
    public PinDto(Long productId, Long pinCount) {
        this.productId = productId;
        this.pinCount = pinCount;
    }

    /**
     * 
     * @param productId
     * @param pin
     * @param pinStatus
     * @param pinStatusId
     * @param transactionCount
     * @param activationDt 
     */
    public PinDto(Long productId, String pin, String pinStatus, int pinStatusId, int transactionCount, Timestamp activationDt) {        
        this.productId = productId;
        this.pin = pin;
        this.pinStatus = pinStatus;
        this.pinStatusId = pinStatusId;
        this.transactionCount = transactionCount;
        this.activationDt = activationDt;
    }

    /**
     *
     * @param ani
     * @param pinCount
     * @param productId
     * @param pin
     * @param insertDt
     * @param pinStatus
     * @param transactionCount
     * @param activationDt
     */
    public PinDto(String ani, Long pinCount, Long productId, String pin, Timestamp insertDt, String pinStatus, int transactionCount, Timestamp activationDt) {
        this.ani = ani;
        this.pinCount = pinCount;
        this.productId = productId;
        this.pin = pin;
        this.insertDt = insertDt;
        this.pinStatus = pinStatus;
        this.transactionCount = transactionCount;
        this.activationDt = activationDt;
    }

    public String getAni() {
        return ani;
    }

    public void setAni(String ani) {
        this.ani = ani;
    }

    public Long getPinCount() {
        return pinCount;
    }

    public void setPinCount(Long pinCount) {
        this.pinCount = pinCount;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Timestamp getInsertDt() {
        return insertDt;
    }

    public void setInsertDt(Timestamp insertDt) {
        this.insertDt = insertDt;
    }

    public String getPinStatus() {
        return pinStatus;
    }

    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }

    public int getPinStatusId() {
        return pinStatusId;
    }

    public void setPinStatusId(int pinStatusId) {
        this.pinStatusId = pinStatusId;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

    public Timestamp getActivationDt() {
        return activationDt;
    }

    public void setActivationDt(Timestamp activationDt) {
        this.activationDt = activationDt;
    }

}
