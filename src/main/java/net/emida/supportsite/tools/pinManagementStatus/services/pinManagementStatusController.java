package net.emida.supportsite.tools.pinManagementStatus.services;

import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.pinManagementStatus.dto.PinDto;
import net.emida.supportsite.tools.pinManagementStatus.repository.IPinManagementStatusDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/25/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3911
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class pinManagementStatusController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(pinManagementStatusController.class);

    @Autowired
    private IPinManagementStatusDao pinManagementStatusDao;

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS, method = RequestMethod.GET)
    public String __init__() {
        return "tools/pinManagementStatus/pinManagementStatus";
    }

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS + "/findBatchesByInsertDate", method = RequestMethod.GET)
    @ResponseBody
    public List<PinDto> findBatchesByInsertDate(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            @RequestParam(value = "pageFrom") String pageFrom, @RequestParam(value = "pageTo") String pageTo,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return pinManagementStatusDao.findBatchesByInsertDate(startDt, endDt, Integer.valueOf(pageFrom), Integer.valueOf(pageTo));
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS + "/findPinsByProductId", method = RequestMethod.GET)
    @ResponseBody
    public List<PinDto> findPinsByProductId(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            @RequestParam(value = "ani") String ani, @RequestParam(value = "pageFrom") String pageFrom, @RequestParam(value = "pageTo") String pageTo,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return pinManagementStatusDao.findPinsByProductId(ani, Integer.valueOf(pageFrom), Integer.valueOf(pageTo), startDt, endDt);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS + "/findPinByMatch", method = RequestMethod.GET)
    @ResponseBody
    public List<PinDto> findPinByMatch(@RequestParam(value = "pin") String pin, Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return pinManagementStatusDao.findPinByMatch(pin);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS + "/updatePin", method = RequestMethod.POST)
    @ResponseBody
    public int updatePin(@RequestParam(value = "pin") String pin, @RequestParam("oldPinStatusId") int oldPinStatusId, @RequestParam("pinStatusId") int pinStatusId, @RequestParam(value = "reason") String reason,
            Model model, Locale locale, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Map<String, String> param = new HashMap<String, String>();
            param.put("user_name", sessionData.getUser().getUsername());
            param.put("instance", DebisysConfigListener.getInstance(request.getSession().getServletContext()));
            param.put("reason", reason);
            param.put("pin", pin);
            param.put("oldPinStatusId", String.valueOf(oldPinStatusId));
            param.put("pinStatusId", String.valueOf(pinStatusId));
            return pinManagementStatusDao.updatePin(param);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS + "/updateBulkPin", method = RequestMethod.POST)
    @ResponseBody
    public int updateBulkPin(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            @RequestParam(value = "product_id") String product_id, @RequestParam("pinStatusId") int pinStatusId, @RequestParam(value = "reason") String reason,
            Model model, Locale locale, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Map<String, String> param = new HashMap<String, String>();
            param.put("user_name", sessionData.getUser().getUsername());
            param.put("instance", DebisysConfigListener.getInstance(request.getSession().getServletContext()));
            param.put("reason", reason);
            param.put("pinStatusId", String.valueOf(pinStatusId));
            param.put("product_id", product_id);
            param.put("startDt", startDt);
            param.put("endDt", endDt);
            return pinManagementStatusDao.updateBulkPin(param);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS + "/removePin", method = RequestMethod.POST)
    @ResponseBody
    public int removePin(@RequestParam(value = "pin") String pin, @RequestParam(value = "reason") String reason, @RequestParam("oldPinStatusId") int oldPinStatusId,
            Model model, Locale locale, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Map<String, String> param = new HashMap<String, String>();
            param.put("user_name", sessionData.getUser().getUsername());
            param.put("instance", DebisysConfigListener.getInstance(request.getSession().getServletContext()));
            param.put("reason", reason);
            param.put("pin", pin);
            param.put("oldPinStatusId", String.valueOf(oldPinStatusId));
            return pinManagementStatusDao.removePin(param);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.PIN_MANAGEMENT_STATUS + "/removeBulkPin", method = RequestMethod.POST)
    @ResponseBody
    public int removeBulkPin(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            @RequestParam(value = "product_id") String product_id, @RequestParam(value = "reason") String reason,
            Model model, Locale locale, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Map<String, String> param = new HashMap<String, String>();
            param.put("user_name", sessionData.getUser().getUsername());
            param.put("instance", DebisysConfigListener.getInstance(request.getSession().getServletContext()));
            param.put("reason", reason);
            param.put("product_id", product_id);
            param.put("startDt", startDt);
            param.put("endDt", endDt);
            return pinManagementStatusDao.removeBulkPin(param);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
