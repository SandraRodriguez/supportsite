package net.emida.supportsite.tools.pinManagementStatus.repository;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.tools.pinManagementStatus.dto.PinDto;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/25/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3911
 */
@Repository("IPinManagementStatusDao")
public class PinManagementStatusRepository implements IPinManagementStatusDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final Logger LOG = Logger.getLogger(PinManagementStatusRepository.class);

    private static final String FIND_LOG_CHANGE_TYPE = "SELECT LOG_CHANGE_TYPE_ID FROM LOGCHANGESTYPES (NOLOCK) WHERE [NAME] = 'Pin Status Management' AND table_name = 'Pins'";

    private static final String AUDIT_LOG = "INSERT INTO LogChanges "
            + "(DATE_TIME,"
            + "LOG_CHANGE_TYPE_ID,"
            + "KEY_VALUE,"
            + "OLD_VALUE,"
            + "NEW_VALUE,"
            + "APPLICATION_NAME,"
            + "USER_NAME,"
            + "INSTANCE,"
            + "REASON) "
            + "VALUES (GETDATE(),?,?,?,?,?,?,?,?)";

    @Override
    public List<PinDto> findBatchesByInsertDate(String startDt, String endDt, int pageFrom, int pageTo) {
        String query = "SELECT * FROM "
                + " (SELECT ROW_NUMBER() OVER (ORDER BY p1.product_id) AS RowNum, p1.product_id, (select COUNT(1) from pins WITH(nolock) WHERE product_id = p1.product_id "
                + " AND insert_date BETWEEN '" + startDt + "' AND DATEADD(DAY, 1, '" + endDt + "') GROUP BY product_id) as pin_count "
                + " FROM pins p1 WITH(nolock) WHERE p1.insert_date BETWEEN '" + startDt + "' "
                + " AND DATEADD(DAY, 1, '" + endDt + "') "
                + " GROUP BY p1.product_id) AS RowConstrainedResult "
                + " WHERE RowNum >= " + pageFrom + " AND RowNum <= " + pageTo + ""
                + " ORDER BY RowNum";
        return jdbcTemplate.query(query, new RowMapper<PinDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public PinDto mapRow(ResultSet rs, int index) throws SQLException {
                return new PinDto(rs.getLong("product_id"), rs.getLong("pin_count"));
            }
        });
    }

    @Override
    public List<PinDto> findPinsByProductId(String product_id, int pageFrom, int pageTo, String startDt, String endDt) {
        String query = "SELECT * FROM "
                + " (select ROW_NUMBER() OVER (ORDER BY product_id) AS RowNum, product_id, pin, (select code from PINStatus WITH(nolock) where id = pinStatusId) as pinStatus, "
                + " transaction_count, activation_date, pinStatusId "
                + " FROM pins WITH(nolock) WHERE product_id = '" + product_id + "' AND insert_date BETWEEN '" + startDt + "' AND DATEADD(DAY, 1, '" + endDt + "')) AS RowConstrainedResult "
                + " WHERE RowNum >= " + pageFrom + " AND RowNum <= " + pageTo + ""
                + " ORDER BY RowNum";

        return jdbcTemplate.query(query, new RowMapper<PinDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public PinDto mapRow(ResultSet rs, int index) throws SQLException {
                return new PinDto(
                        rs.getLong("product_id"),
                        rs.getString("pin"),
                        rs.getString("pinStatus"),
                        rs.getInt("pinStatusId"),
                        rs.getInt("transaction_count"),
                        (rs.getDate("activation_date") != null ? new Timestamp(rs.getDate("activation_date").getTime()) : null));
            }
        });
    }

    @Override
    public List<PinDto> findPinByMatch(String pin) {
        String query = "SELECT product_id, pin, (select code from PINStatus WITH(nolock) where id = pinStatusId) as pinStatus,transaction_count, activation_date, pinStatusId "
                + "FROM dbo.pins p WHERE p.pin like '" + pin + "%'";

        return jdbcTemplate.query(query, new RowMapper<PinDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public PinDto mapRow(ResultSet rs, int index) throws SQLException {
                return new PinDto(
                        rs.getLong("product_id"),
                        rs.getString("pin"),
                        rs.getString("pinStatus"),
                        rs.getInt("pinStatusId"),
                        rs.getInt("transaction_count"),
                        (rs.getDate("activation_date") != null ? new Timestamp(rs.getDate("activation_date").getTime()) : null));
            }
        });
    }

    @Override
    public int updatePin(Map<String, String> param) {
        CallableStatement callableStatement = null;
        int rows = 0;
        try {
            callableStatement = this.jdbcTemplate.getDataSource().getConnection().prepareCall("EXEC dbo.pinStatusManagement ?, ?, NULL, 'UPDATE', NULL, NULL");
            callableStatement.setString(1, param.get("pin").trim());
            callableStatement.setInt(2, Integer.valueOf(param.get("pinStatusId")));
            rows = callableStatement.executeUpdate();
            if (rows > 0) {
                Map<String, Object> type_id = this.jdbcTemplate.queryForMap(FIND_LOG_CHANGE_TYPE);
                this.jdbcTemplate.update(AUDIT_LOG,
                        type_id.get("LOG_CHANGE_TYPE_ID"),
                        param.get("pin"),
                        param.get("oldPinStatusId"),
                        param.get("pinStatusId"),
                        "support",
                        param.get("user_name"),
                        param.get("instance"),
                        param.get("reason"));
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException ex) {
                    LOG.error(ex);
                }
            }
        }
        return rows;
    }

    @Override
    public int updateBulkPin(Map<String, String> param) {
        CallableStatement callableStatement = null;
        int rows = 0;
        try {
            String query = "SELECT pin, pinStatusId FROM pins WITH(nolock) WHERE product_id = '" + param.get("product_id").trim() + "' "
                    + " AND insert_date BETWEEN '" + param.get("startDt").trim() + "' AND DATEADD(DAY, 1, '" + param.get("endDt").trim() + "')";

            List<PinDto> list = jdbcTemplate.query(query, new RowMapper<PinDto>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public PinDto mapRow(ResultSet rs, int index) throws SQLException {
                    return new PinDto(rs.getString("pin"), rs.getInt("pinStatusId"));
                }
            });

            callableStatement = this.jdbcTemplate.getDataSource().getConnection().prepareCall("EXEC dbo.pinStatusManagement NULL, " + Integer.valueOf(param.get("pinStatusId")) + ", '" + param.get("product_id").trim() + "', 'BULK_UPDATE', '" + param.get("startDt").trim() + "', '" + param.get("endDt").trim() + "'");
            rows = callableStatement.executeUpdate();
            if (rows > 0) {
                int val = Integer.valueOf(param.get("pinStatusId"));
                boolean isAvailable = Boolean.FALSE;
                if (val == 1) {
                    isAvailable = Boolean.TRUE;
                }
                onAuditProcess(list, param, Boolean.FALSE, isAvailable);
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException ex) {
                    LOG.error(ex);
                }
            }
        }
        return rows;
    }

    @Override
    public int removePin(Map<String, String> param) {
        CallableStatement callableStatement = null;
        int rows = 0;
        try {
            callableStatement = this.jdbcTemplate.getDataSource().getConnection().prepareCall("EXEC dbo.pinStatusManagement ?, ?, NULL, 'REMOVE', NULL, NULL");
            callableStatement.setString(1, param.get("pin").trim());
            callableStatement.setInt(2, 0);
            rows = callableStatement.executeUpdate();
            if (rows > 0) {
                Map<String, Object> type_id = this.jdbcTemplate.queryForMap(FIND_LOG_CHANGE_TYPE);
                this.jdbcTemplate.update(AUDIT_LOG,
                        type_id.get("LOG_CHANGE_TYPE_ID"),
                        param.get("pin"),
                        param.get("oldPinStatusId"),
                        param.get("pinStatusId"),
                        "support",
                        param.get("user_name"),
                        param.get("instance"),
                        param.get("reason"));
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException ex) {
                    LOG.error(ex);
                }
            }
        }
        return rows;
    }

    @Override
    public int removeBulkPin(Map<String, String> param) {
        CallableStatement callableStatement = null;
        int rows = 0;
        try {
            String query = "SELECT pin, pinStatusId FROM pins WITH(nolock) WHERE product_id = '" + param.get("product_id").trim() + "' "
                    + " AND insert_date BETWEEN '" + param.get("startDt").trim() + "' AND DATEADD(DAY, 1, '" + param.get("endDt").trim() + "') AND activation_date IS NULL AND transaction_count = 0";

            List<PinDto> list = jdbcTemplate.query(query, new RowMapper<PinDto>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public PinDto mapRow(ResultSet rs, int index) throws SQLException {
                    return new PinDto(rs.getString("pin"), rs.getInt("pinStatusId"));
                }
            });

            callableStatement = this.jdbcTemplate.getDataSource().getConnection().prepareCall("EXEC dbo.pinStatusManagement NULL, 0, '" + param.get("product_id").trim() + "', 'BULK_REMOVE', '" + param.get("startDt").trim() + "', '" + param.get("endDt").trim() + "'");
            rows = callableStatement.executeUpdate();
            if (rows > 0) {
                onAuditProcess(list, param, Boolean.TRUE, Boolean.FALSE);
            }
        } catch (SQLException ex) {
            LOG.error(ex);
        } finally {
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (SQLException ex) {
                    LOG.error(ex);
                }
            }
        }
        return rows;
    }

    private void onAuditProcess(List<PinDto> list, Map<String, String> param, boolean isRemove, boolean isAvailable) {
        String newStatus = "5";
        if (isAvailable) {
            newStatus = "1";
        }
        for (PinDto element : list) {
            Map<String, Object> type_id = this.jdbcTemplate.queryForMap(FIND_LOG_CHANGE_TYPE);
            this.jdbcTemplate.update(AUDIT_LOG,
                    type_id.get("LOG_CHANGE_TYPE_ID"),
                    element.getPin().trim(),
                    element.getPinStatusId(),
                    (isRemove ? "-1" : newStatus),
                    "support",
                    param.get("user_name"),
                    param.get("instance"),
                    param.get("reason"));
        }
    }

}
