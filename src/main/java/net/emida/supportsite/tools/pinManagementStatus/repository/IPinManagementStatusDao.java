package net.emida.supportsite.tools.pinManagementStatus.repository;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.tools.pinManagementStatus.dto.PinDto;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/25/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3911
 */
public interface IPinManagementStatusDao {

    /**
     *
     * @param startDt
     * @param endDt
     * @param pageFrom
     * @param pageTo
     * @return
     */
    public List<PinDto> findBatchesByInsertDate(String startDt, String endDt, int pageFrom, int pageTo);

    /**
     *
     * @param ani
     * @param pageFrom
     * @param pageTo
     * @param startDt
     * @param endDt
     * @return
     */
    public List<PinDto> findPinsByProductId(String ani, int pageFrom, int pageTo, String startDt, String endDt);

    /**
     *
     * @param pin
     * @return
     */
    public List<PinDto> findPinByMatch(String pin);

    /**
     *
     * @param param
     * @return
     */
    public int updatePin(Map<String, String> param);

    /**
     *
     * @param param
     * @return
     */
    public int removePin(Map<String, String> param);

    /**
     *
     * @param param
     * @return
     */
    public int updateBulkPin(Map<String, String> param);

    /**
     *
     * @param param
     * @return
     */
    public int removeBulkPin(Map<String, String> param);

}
