/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.dto.fileTransfer.FileTransferTaskType;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IFileTransferTaskTypeRepositoryDao")
public class FileTransferTaskTypeRepository implements IFileTransferTaskTypeRepositoryDao {
    
    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FileTransferTaskTypeRepository.class);
    
    private class FileTransferTaskTypeRowMapper implements RowMapper<FileTransferTaskType> {
        
        @Override
        public FileTransferTaskType mapRow(ResultSet rs, int i) throws SQLException {
            return new FileTransferTaskType(
                    rs.getString("id"),
                    rs.getString("code"),
                    rs.getString("name")
            );
            
        }
        
    }
    
    @Override
    public FileTransferTaskType getFileTransferTaskTypeById(String fileTransferTaskTypeId) {
        final String query = String.format("SELECT * FROM [dbo].[FileTransferTaskTypes] WITH(NOLOCK) WHERE [id] = '%s'",
                fileTransferTaskTypeId);
        LOG.info("query :: " + query);
        FileTransferTaskType dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileTransferTaskTypeRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }
    
    @Override
    public FileTransferTaskType getFileTransferTaskTypeByCode(String fileTransferTaskTypeCode) {
        final String query = String.format("SELECT * FROM [dbo].[FileTransferTaskTypes] WITH(NOLOCK) WHERE [code] = '%s'",
                fileTransferTaskTypeCode);
        LOG.info("query :: " + query);
        FileTransferTaskType dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileTransferTaskTypeRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }
    
    @Override
    public List<FileTransferTaskType> getAllFileTransferTaskTypes() {
        final String query = "SELECT * FROM [dbo].[FileTransferTaskTypes] WITH(NOLOCK)";
        LOG.info("query :: " + query);
        List<FileTransferTaskType> list = new ArrayList<FileTransferTaskType>();
        try {
            list = jdbcTemplate.query(query, new FileTransferTaskTypeRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getFileTransferTaskTypeById . Ex = {}", e);
        }
        return list;
    }
    
}
