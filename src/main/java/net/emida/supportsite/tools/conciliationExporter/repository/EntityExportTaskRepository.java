/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.dto.fileExporter.EntityExportTask;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IEntityExportTaskRepositoryDao")
public class EntityExportTaskRepository implements IEntityExportTaskRepositoryDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(EntityExportTaskRepository.class);

    private class EntityExportTaskRowMapper implements RowMapper<EntityExportTask> {

        @Override
        public EntityExportTask mapRow(ResultSet rs, int i) throws SQLException {
            return new EntityExportTask(
                    rs.getString("id"),
                    rs.getString("description"),
                    rs.getLong("entityId"),
                    rs.getString("entityName"),
                    rs.getInt("entityTypeId"),
                    rs.getString("retrieverId"),
                    rs.getString("fileExporterId"),
                    rs.getString("queryProcessorClass"),
                    rs.getBoolean("taskEnabled")
            );
        }

    }

    @Override
    public int insert(EntityExportTask entityExportTask) {
        int affectedRecords = 0;

        final StringBuilder sbInsert = new StringBuilder();
        sbInsert.append(" INSERT INTO [dbo].[EntityExportTasks] ");
        sbInsert.append(" ([description], [entityId], [entityName], ");
        sbInsert.append(" [entityTypeId], [retrieverId], [fileExporterId], ");
        sbInsert.append(" [queryProcessorClass], [taskEnabled]) ");
        sbInsert.append(" VALUES ( ");
        sbInsert.append(String.format(" '%s', ", entityExportTask.getDescription()));
        sbInsert.append(String.format(" %d, ", entityExportTask.getEntityId()));
        sbInsert.append(String.format(" '%s', ", entityExportTask.getEntityName()));
        sbInsert.append(String.format(" %d, ", entityExportTask.getEntityTypeId()));
        sbInsert.append(String.format(" '%s', ", entityExportTask.getRetrieverId()));
        sbInsert.append(String.format(" '%s', ", entityExportTask.getFileExporterId()));
        sbInsert.append(String.format(" '%s', ", entityExportTask.getQueryPreprocessorClass()));
        sbInsert.append(String.format(" %d) ", (entityExportTask.isTaskEnabled()) ? 1 : 0));

        String insert = sbInsert.toString();
        LOG.info("insert :: " + insert);
        try {
            affectedRecords = this.jdbcTemplate.update(insert);
        } catch (DataAccessException e) {
            LOG.error("Error on insert EntityExportTask. Ex = {}", e);
        }

        return affectedRecords;
    }

    @Override
    public int update(EntityExportTask entityExportTask) {
        final StringBuilder sbUpdate = new StringBuilder();
        sbUpdate.append(" UPDATE [dbo].[EntityExportTasks] ");
        sbUpdate.append(String.format(" SET [description] = '%s', ", entityExportTask.getDescription()));
        sbUpdate.append(String.format(" [entityId] = %d, ", entityExportTask.getEntityId()));
        sbUpdate.append(String.format(" [entityName] = '%s', ", entityExportTask.getEntityName()));
        sbUpdate.append(String.format(" [entityTypeId] = %d, ", entityExportTask.getEntityTypeId()));
        sbUpdate.append(String.format(" [retrieverId] = '%s', ", entityExportTask.getRetrieverId()));
        sbUpdate.append(String.format(" [fileExporterId] = '%s', ", entityExportTask.getFileExporterId()));
        sbUpdate.append(String.format(" [queryProcessorClass] = '%s', ", entityExportTask.getQueryPreprocessorClass()));
        sbUpdate.append(String.format(" [taskEnabled] = %d ", (entityExportTask.isTaskEnabled()) ? 1 : 0));
        sbUpdate.append(String.format(" WHERE [id] = '%s' ", entityExportTask.getId()));
 
        String update = sbUpdate.toString();
        LOG.info("update :: " + update);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(update);
        } catch (DataAccessException e) {
            LOG.error("Error on update EntityExportTask. Ex = {}", e);
        }
        return rows;
    }

    @Override
    public int delete(String entityExportTaskId) {
        int rowsAfected = 0;
        final String query = String.format("DELETE FROM [dbo].[EntityExportTasks] WHERE [id] = '%s'",
                entityExportTaskId);
        LOG.info("delete :: " + query);
        try {
            rowsAfected += jdbcTemplate.update(query);
        } catch (DataAccessException e) {
            LOG.error("Error on delete EntityExportTask. Ex = {}", e);
        }
        return rowsAfected;
    }

    @Override
    public EntityExportTask getEntityExportTaskById(String entityExportTaskId) {
        final String query = String.format("SELECT * FROM [dbo].[EntityExportTasks] WITH(NOLOCK) WHERE [id] = '%s' ORDER BY [description]",
                entityExportTaskId);
        LOG.info("query :: " + query);
        EntityExportTask dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new EntityExportTaskRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }

    @Override               
    public EntityExportTask getEntityExportTaskByRetrieverId(String retrieverId) {
        final String query = String.format("SELECT * FROM [dbo].[EntityExportTasks] WITH(NOLOCK) WHERE [retrieverId] = '%s'",
                retrieverId);
        LOG.info("query :: " + query);
        EntityExportTask dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new EntityExportTaskRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }

    @Override
    public List<EntityExportTask> getEntityExportTasksByExporterId(String exporterId) {
        final String query = String.format("SELECT * FROM [dbo].[EntityExportTasks] WITH(NOLOCK) WHERE [fileExporterId] = '%s'",
                exporterId);
        LOG.info("query :: " + query);
        List<EntityExportTask> dtoList = new ArrayList<EntityExportTask>();
        try {
            dtoList = jdbcTemplate.query(query, new EntityExportTaskRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getEntityExportTaskByExporterId . Ex = {}", e);
        }
        return dtoList;
    }

    
    @Override
    public List<EntityExportTask> getEntityExportTasksByRetrieverId(String retrieverId) {
        String query = "SELECT * FROM [dbo].[EntityExportTasks] WITH(NOLOCK) WHERE [retrieverId] = ?'";
        Object[] param = new Object[1];
        param[0] = retrieverId;
        LOG.info("query :: " + query + ": " + retrieverId);
        List<EntityExportTask> dtoList = new ArrayList<EntityExportTask>();
        try {
            dtoList = jdbcTemplate.query(query, param, new EntityExportTaskRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getEntityExportTasksByRetrieverId . Ex = {}", e);
        }
        return dtoList;
    }
    
    
    @Override
    public List<EntityExportTask> getEntityExportTasksByDescription(String description) {
        String query = "SELECT * FROM [dbo].[EntityExportTasks] WITH(NOLOCK) WHERE [description] = ?";
        Object[] param = new Object[1];
        param[0] = description;
        LOG.info("query :: " + query + ": " + description);
        List<EntityExportTask> dtoList = new ArrayList<EntityExportTask>();
        try {
            dtoList = jdbcTemplate.query(query, param, new EntityExportTaskRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getEntityExportTaskByDescription . Ex = {}", e);
        }
        return dtoList;
    }

    /**
     * No duplicate tasks are allowed on the same entity, this method
     * brings entity based on the table index so duplication can be checked
     * before insertion
     * @param entityExportTask
     * @return Entity task found or null
     */
    @Override
    public List<EntityExportTask> getEntityExportTaskByIndexFields(EntityExportTask entityExportTask) {
        String query = "SELECT * FROM [dbo].[EntityExportTasks] WITH(NOLOCK) WHERE [entityId] = ? AND [entityTypeId] = ? AND [retrieverId] = ? AND [fileExporterId] = ?";
        Object[] param = new Object[4];
        param[0] = entityExportTask.getEntityId();
        param[1] = entityExportTask.getEntityTypeId();
        param[2] = entityExportTask.getRetrieverId();
        param[3] = entityExportTask.getFileExporterId();
        LOG.info("query :: " + query + ": " + param);
        List<EntityExportTask> dtoList = new ArrayList<EntityExportTask>();
        try {
            dtoList = jdbcTemplate.query(query, param, new EntityExportTaskRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getEntityExportTaskByIndexFields . Ex = {}", e);
        }
        return dtoList;
    }
    
    
    
    @Override
    public List<EntityExportTask> getAllEntityExportTasks() {
        final String query = "SELECT * FROM [dbo].[EntityExportTasks] WITH(NOLOCK)";
        LOG.info("query :: " + query);
        List<EntityExportTask> list = new ArrayList<EntityExportTask>();
        try {
            list = jdbcTemplate.query(query, new EntityExportTaskRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllEntityExportTasks. Ex = {}", e);
        }
        return list;
    }

}
