/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.util.List;
import net.emida.supportsite.dto.fileExporter.ExportRetriever;

/**
 *
 * @author fernandob
 */
public interface IExportRetrieverRepositoryDao {

    int delete(String exportRetrieverId);

    List<ExportRetriever> getAllExportRetrievers();

    ExportRetriever getExportRetrieverById(String exportRetrieverId);
    
    public ExportRetriever getExportRetrieverByClassNDesc(ExportRetriever retriever);

    int insert(ExportRetriever exportRetriever);

    int update(ExportRetriever exportRetriever);
    
    public List<ExportRetriever> getExportRetrieverByDescription(String exportRetrieverDescription);
    
}
