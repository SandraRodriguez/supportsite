/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.dto.fileTransfer.FileTransferTask;
import net.emida.supportsite.dto.fileTransfer.FileTransferTaskConnection;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IFileTransferTaskRepostoryDao")
public class FileTransferTaskRespository implements IFileTransferTaskRepostoryDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FileTransferTaskRespository.class);

    private class FileTransferTaskRowMapper implements RowMapper<FileTransferTask> {

        @Override
        public FileTransferTask mapRow(ResultSet rs, int i) throws SQLException {
            return new FileTransferTask(
                    
                    rs.getString("id"),
                    rs.getString("fileTransferTaskTypeId"),
                    rs.getString("fileTransferConnectionId"),
                    rs.getString("description"),
                    rs.getString("fileNamePattertn"),
                    rs.getString("sourceFolder"),
                    rs.getString("targetFolder"),
                    rs.getString("processedFolder"),
                    rs.getBoolean("checkTransferredFiles"),
                    rs.getInt("retriesOnTransferFail"),
                    rs.getBoolean("taskEnabled")
            );
        }
    }
    
    private class FileTransferTaskTypeConnectionRowMapper implements RowMapper<FileTransferTaskConnection> {

        @Override
        public FileTransferTaskConnection mapRow(ResultSet rs, int i) throws SQLException {
            return new FileTransferTaskConnection(
                    
                    rs.getString("id"),
                    rs.getString("fileTransferTaskTypeId"),
                    rs.getString("fileTransferTaskTypeName"),
                    rs.getString("fileTransferConnectionId"),
                    rs.getString("fileTransferConnectionName"),
                    rs.getString("description"),
                    rs.getString("fileNamePattertn"),
                    rs.getString("sourceFolder"),
                    rs.getString("targetFolder"),
                    rs.getString("processedFolder"),
                    rs.getBoolean("checkTransferredFiles"),
                    rs.getInt("retriesOnTransferFail"),
                    rs.getBoolean("taskEnabled")
            );
        }
    }

    @Override
    public int insert(FileTransferTask fileTransferTask) {
        int affectedRecords = 0;

        final StringBuilder sbInsert = new StringBuilder();
        sbInsert.append(" INSERT INTO [dbo].[FileTransferTask] ");
        sbInsert.append(" ([fileTransferTaskTypeId], [fileTransferConnectionId], ");
        sbInsert.append(" [description], [fileNamePattertn], [sourceFolder], ");
        sbInsert.append(" [targetFolder], [processedFolder], [checkTransferredFiles], ");
        sbInsert.append(" [retriesOnTransferFail], [taskEnabled]) ");
        sbInsert.append(" VALUES ( ");
        sbInsert.append(String.format(" '%s', ", fileTransferTask.getFileTransferTaskTypeId()));
        sbInsert.append(String.format(" '%s', ", fileTransferTask.getFileTransferConnectionId()));
        sbInsert.append(String.format(" '%s', ", fileTransferTask.getDescription()));
        sbInsert.append(String.format(" '%s', ", fileTransferTask.getFileNamePattern()));
        sbInsert.append(String.format(" '%s', ", fileTransferTask.getSourceFolder()));
        sbInsert.append(String.format(" '%s', ", fileTransferTask.getTargetFolder()));
        sbInsert.append(String.format(" '%s', ", fileTransferTask.getProcessedFolder()));
        sbInsert.append(String.format(" %d ,", (fileTransferTask.isCheckTransferredFiles()) ? 1 : 0));
        sbInsert.append(String.format(" %d ,", fileTransferTask.getRetriesOnTransferFail()));
        sbInsert.append(String.format(" %d )", (fileTransferTask.isTaskEnabled()) ? 1 : 0));

        String insert = sbInsert.toString();
        LOG.info("insert :: " + insert);
        try {
            affectedRecords = this.jdbcTemplate.update(insert);
        } catch (DataAccessException e) {
            LOG.error("Error on insert FileTransferTask. Ex = {}", e);
        }

        return affectedRecords;
    }

    @Override
    public boolean update(FileTransferTask fileTransferTask) {
        boolean response = false;
        final StringBuilder sbUpdate = new StringBuilder();
        sbUpdate.append(" UPDATE [dbo].[FileTransferTask] ");
        sbUpdate.append(String.format(" SET [fileTransferTaskTypeId] = '%s', ", fileTransferTask.getFileTransferTaskTypeId()));
        sbUpdate.append(String.format(" [fileTransferConnectionId] = '%s', ", fileTransferTask.getFileTransferConnectionId()));
        sbUpdate.append(String.format(" [description] = '%s', ", fileTransferTask.getDescription()));
        sbUpdate.append(String.format(" [fileNamePattertn] ='%s', ", fileTransferTask.getFileNamePattern()));
        sbUpdate.append(String.format(" [sourceFolder] = '%s', ", fileTransferTask.getSourceFolder()));
        sbUpdate.append(String.format(" [targetFolder] = '%s', ", fileTransferTask.getTargetFolder()));
        sbUpdate.append(String.format(" [processedFolder] = '%s', ", fileTransferTask.getProcessedFolder()));
        sbUpdate.append(String.format(" [checkTransferredFiles] = %d, ", (fileTransferTask.isCheckTransferredFiles()) ? 1 : 0));
        sbUpdate.append(String.format(" [retriesOnTransferFail] = %d, ", fileTransferTask.getRetriesOnTransferFail()));
        sbUpdate.append(String.format(" [taskEnabled] = %d ", (fileTransferTask.isTaskEnabled()) ? 1 : 0));
        sbUpdate.append(String.format(" WHERE [id] = '%s' ", fileTransferTask.getId()));

        String update = sbUpdate.toString();
        LOG.info("update :: " + update);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(update);
            if (rows > 0) response = true;
        } catch (DataAccessException e) {
            LOG.error("Error on update FileTransferTask. Ex = {}", e);
        }
        return response;
    }

    @Override
    public boolean delete(String fileTransferTaskId) {
        int rowsAfected = 0;
        boolean response = false;
        final String query = String.format("DELETE FROM [dbo].[FileTransferTask] WHERE [id] = '%s'",
                fileTransferTaskId);
        LOG.info("delete :: " + query);
        try {
            rowsAfected += jdbcTemplate.update(query);
            if (rowsAfected > 0) response = true;
        } catch (DataAccessException e) {
            LOG.error("Error on delete FileTransferTask. Ex = {}", e);
        }
        return response;
    }

    @Override
    public FileTransferTask getFileTransferTaskById(String fileExporterId) {
        final String query = String.format("SELECT * FROM [dbo].[FileTransferTask] WITH(NOLOCK) WHERE [id] = '%s'",
                fileExporterId);
        LOG.info("query :: " + query);
        FileTransferTask dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileTransferTaskRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }
    
    @Override
    public boolean getFileTransferTaskByDescription(String description) {
        boolean response = false;
        final String query = String.format("SELECT * FROM [dbo].[FileTransferTask] WITH(NOLOCK) WHERE [description] = '%s'",
                description);
        LOG.info("query :: " + query);
        try {
            List<FileTransferTask> lsTransferTask = jdbcTemplate.query(query, new FileTransferTaskRowMapper());
            if (lsTransferTask.isEmpty()) response = true;
        } catch (DataAccessException e) {
            LOG.error("Error on getFileTransferTaskByDescription . Ex = {}", e);
        }
        return response;
    }

    @Override
    public List<FileTransferTask> getAllFileTransferTasks() {
        final String query = "SELECT * FROM [dbo].[FileTransferTask] WITH(NOLOCK)";
        LOG.info("query :: " + query);
        List<FileTransferTask> list = new ArrayList<FileTransferTask>();
        try {
            list = jdbcTemplate.query(query, new FileTransferTaskRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllFileTransferTasks. Ex = {}", e);
        }
        return list;
    }
    
    @Override
    public List<FileTransferTaskConnection> getAllTransferTypeConnection() {
        final StringBuilder sbQuery = new StringBuilder();
        sbQuery.append(" SELECT");
        sbQuery.append("        FTT.id As id, ");
        sbQuery.append("        FTTY.id As fileTransferTaskTypeId, ");
        sbQuery.append("        FTTY.name As fileTransferTaskTypeName, ");
        sbQuery.append("        FTC.[id] As fileTransferConnectionId, ");
        sbQuery.append("        FTC.[description] As fileTransferConnectionName, ");
        sbQuery.append("        FTT.[description] As description, ");
        sbQuery.append("        FTT.fileNamePattertn As fileNamePattertn, ");
        sbQuery.append("        FTT.sourceFolder As sourceFolder, ");
        sbQuery.append("        FTT.targetFolder As targetFolder, ");
        sbQuery.append("        FTT.processedFolder As processedFolder, ");
        sbQuery.append("        FTT.checkTransferredFiles As checkTransferredFiles, ");
        sbQuery.append("        FTT.retriesOnTransferFail As retriesOnTransferFail, ");
        sbQuery.append("        FTT.taskEnabled As taskEnabled ");
        sbQuery.append("  FROM [dbo].[fileTransferTask] FTT WITH(NOLOCK) ");
        sbQuery.append("        INNER JOIN ");
        sbQuery.append("            [dbo].[fileTransferTaskTypes] FTTY WITH(NOLOCK) ");
        sbQuery.append("            ON FTT.[fileTransferTaskTypeId] = FTTY.[id] ");
        sbQuery.append("        INNER JOIN ");
        sbQuery.append("            [dbo].[fileTransferConnection] FTC WITH(NOLOCK) ");
        sbQuery.append("            ON FTT.fileTransferConnectionId = FTC.id");
        
        LOG.info("FileTransferTaskRespository.getAllTransferTypeConnection  :: " + sbQuery.toString());
        List<FileTransferTaskConnection> list = new ArrayList<FileTransferTaskConnection>();
        
        try {
            list = jdbcTemplate.query(sbQuery.toString(), new FileTransferTaskTypeConnectionRowMapper());
        } catch (DataAccessException e) {
            LOG.error("FileTransferTaskRespository.getAllTransferTypeConnection - ERROR GETTING TRANSFER FILES = {}", e);
        }
        return list;
    }

}
