/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.debisys.utils.DateUtil;

import net.emida.supportsite.dto.fileExporter.ExportRetriever;

/**
 *
 * @author fernandob
 */
@Repository("IExportRetrieverRepositoryDao")
public class ExportRetrieverRepository implements IExportRetrieverRepositoryDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ExportRetrieverRepository.class);

    private class ExportRetrieverRowMapper implements RowMapper<ExportRetriever> {

        @Override
        public ExportRetriever mapRow(ResultSet rs, int i) throws SQLException {
        	ExportRetriever retriever = new ExportRetriever(
                    rs.getString("id"),
                    rs.getString("retrieverClass"),
                    rs.getString("description"),
                    rs.getString("columnHeaders"),
                    rs.getString("dataSourceName"),
                    rs.getString("startDate"),
                    rs.getString("endDate"),
                    rs.getBoolean("useFixedDates"),
                    rs.getBoolean("useDateRange"),
                    rs.getInt("rangeDays"),
                    rs.getBoolean("isPartialQuery"),
                    rs.getString("detailQuery"),
                    rs.getString("fileNameDateFormat"));
        	retriever.setUseStartDate(rs.getBoolean("useStartDate"));
        	return retriever;
        }
    }

    @Override
    public int insert(final ExportRetriever exportRetriever) {
        String sbInsert = "INSERT INTO [ExportRetrievers] ([retrieverClass], [description], [columnHeaders], [dataSourceName], [startDate], "
        		+ " [endDate], [useFixedDates], [useDateRange], [rangeDays], [isPartialQuery], [detailQuery], [fileNameDateFormat], [useStartDate]) "
        		+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        LOG.info("insert :: " + sbInsert + "::" + exportRetriever.toString());
        int rows = jdbcTemplate.update(sbInsert, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				int i = 1;
				ps.setString(i++, exportRetriever.getRetrieverClass());
				ps.setString(i++, exportRetriever.getDescription());
				ps.setString(i++, exportRetriever.getColumnHeaders());
				ps.setString(i++, exportRetriever.getDataSourceName());
				ps.setDate(i++, convertDate(exportRetriever.getStartDate()));
				ps.setDate(i++, convertDate(exportRetriever.getEndDate()));
				ps.setBoolean(i++, exportRetriever.isUseFixedDates() ? true : false);
				ps.setBoolean(i++, exportRetriever.isUseDateRange() ? true : false);
				ps.setInt(i++, exportRetriever.getRangeDays());
				ps.setBoolean(i++, exportRetriever.isPartialQuery() ? true : false);
				ps.setString(i++, exportRetriever.getDetailQuery());
				ps.setString(i++, exportRetriever.getFileNameDateFormat());
				ps.setBoolean(i, exportRetriever.isUseStartDate());
			}
        });
        
        return rows;
    }

    @Override
    public int update(final ExportRetriever exportRetriever) {
    	String sql = "update ExportRetrievers set retrieverClass = ?, description = ?, columnHeaders = ?, dataSourceName = ?, startDate = ?, " 
    			+ "endDate = ?, useFixedDates = ?, useDateRange = ?, rangeDays = ?, isPartialQuery = ?, detailQuery = ?, fileNameDateFormat = ?,"
    			+ " useStartDate = ? where id = ?";
    	
    	LOG.info("update :: " + sql + "::" + exportRetriever.toString());
    	int rows = jdbcTemplate.update(sql, new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				int i = 1;
				ps.setString(i++, exportRetriever.getRetrieverClass());
				ps.setString(i++, exportRetriever.getDescription());
				ps.setString(i++, exportRetriever.getColumnHeaders());
				ps.setString(i++, exportRetriever.getDataSourceName());
				ps.setDate(i++, convertDate(exportRetriever.getStartDate()));
				ps.setDate(i++, convertDate(exportRetriever.getEndDate()));
				ps.setBoolean(i++, exportRetriever.isUseFixedDates() ? true : false);
				ps.setBoolean(i++, exportRetriever.isUseDateRange() ? true : false);
				ps.setInt(i++, exportRetriever.getRangeDays());
				ps.setBoolean(i++, exportRetriever.isPartialQuery() ? true : false);
				ps.setString(i++, exportRetriever.getDetailQuery());
				ps.setString(i++, exportRetriever.getFileNameDateFormat());
				ps.setBoolean(i++, exportRetriever.isUseStartDate());
				ps.setString(i, exportRetriever.getId());
			}
    	});
    	
        return rows;
    }
    
    private java.sql.Date convertDate(String dateStr){
    	java.sql.Date date = null;
    	if(StringUtils.isNotBlank(dateStr)) {
    		Date parsedDate = DateUtil.parseStringToDate(dateStr, "MM/dd/yyyy");
    		if(parsedDate != null) {
    			date = new java.sql.Date(parsedDate.getTime());
    		}
    	} else {
    		date = new java.sql.Date(0);
    	}
    	return date;
    }

    @Override
    public int delete(String exportRetrieverId) {
        int rowsAfected = 0;
        final String query = String.format("DELETE FROM [dbo].[ExportRetrievers] WHERE [id] = '%s'",
                exportRetrieverId);
        LOG.info("delete :: " + query);
        try {
            rowsAfected += jdbcTemplate.update(query);
        } catch (DataAccessException e) {
            LOG.error("Error on delete ExportRetriever. Ex = {}", e);
        }
        return rowsAfected;
    }

    @Override
    public ExportRetriever getExportRetrieverById(String exportRetrieverId) {
        final String query = "SELECT * FROM [dbo].[ExportRetrievers] WITH(NOLOCK) WHERE [id] = ?";
        LOG.info("query :: " + query + ": " + exportRetrieverId);
        ExportRetriever dto = null;
        Object[] param = new Object[1];
        param[0] = exportRetrieverId;
        try {
            dto = jdbcTemplate.queryForObject(query, param, new ExportRetrieverRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Export retriever not found. " + e.getMessage());
        }
        return dto;
    }

    @Override
    public List<ExportRetriever> getExportRetrieverByDescription(String exportRetrieverDescription) {
        String query = "SELECT * FROM [dbo].[ExportRetrievers] WITH(NOLOCK) WHERE [description] = ?";
        Object[] param = new Object[1];
        param[0] = exportRetrieverDescription;
        LOG.info("query :: " + query + ": " + exportRetrieverDescription);
        List<ExportRetriever> dto = null;
        try {
            dto = jdbcTemplate.query(query, param, new ExportRetrieverRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getExportRetrieverByDescription . Ex = {}", e);
        }
        return dto;
    }
    
    
    @Override
    public ExportRetriever getExportRetrieverByClassNDesc(ExportRetriever retriever) {
        String query = "SELECT * FROM [dbo].[ExportRetrievers] WITH(NOLOCK) WHERE [retrieverClass] = ? AND [description] = ?";
        Object[] param = new Object[2];
        param[0] = retriever.getRetrieverClass();
        param[1] = retriever.getDescription();
        LOG.info("query :: " + query);
        ExportRetriever dto;
        try {
            dto = jdbcTemplate.queryForObject(query, param, new ExportRetrieverRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }

    @Override
    public List<ExportRetriever> getAllExportRetrievers() {
        final String query = "SELECT * FROM [dbo].[ExportRetrievers] WITH(NOLOCK) ORDER BY [description]";
        LOG.info("query :: " + query);
        List<ExportRetriever> list = new ArrayList<ExportRetriever>();
        try {
            list = jdbcTemplate.query(query, new ExportRetrieverRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllExportRetrievers. Ex = {}", e);
        }
        return list;
    }

}
