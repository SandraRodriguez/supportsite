/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.dto.fileTransfer.FileTransferType;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IFileTransferTypeRepositoryDao")
public class FileTransferTypeRepository implements IFileTransferTypeRepositoryDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FileTransferTypeRepository.class);

    private class FileTransferTypeRowMapper implements RowMapper<FileTransferType> {

        @Override
        public FileTransferType mapRow(ResultSet rs, int i) throws SQLException {
            return new FileTransferType(
                    rs.getString("id"),
                    rs.getString("code"),
                    rs.getString("name")
            );
        }

    }

    @Override
    public FileTransferType getFileTransferTypeById(String fileTransferTypeId) {
        final String query = String.format("SELECT * FROM [dbo].[FileTransferTypes] WITH(NOLOCK) WHERE [id] = '%s'",
                fileTransferTypeId);
        LOG.info("query :: " + query);
        FileTransferType dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileTransferTypeRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }

    @Override
    public FileTransferType getFileTransferTypeByCode(String fileTransferTypeCode) {
        final String query = String.format("SELECT * FROM [dbo].[FileTransferTypes] WITH(NOLOCK) WHERE [code] = '%s'",
                fileTransferTypeCode);
        LOG.info("query :: " + query);
        FileTransferType dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileTransferTypeRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }

    @Override
    public List<FileTransferType> getAllFileTransferTypes() {
        final String query = "SELECT * FROM [dbo].[FileTransferTypes] WITH(NOLOCK)";
        LOG.info("query :: " + query);
        List<FileTransferType> list = new ArrayList<FileTransferType>();
        try {
            list = jdbcTemplate.query(query, new FileTransferTypeRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllFileTransferTypes . Ex = {}", e);
        }
        return list;
    }
    
    @Override
    public List<FileTransferType> getAllFileTransferTaskTypes() {
        final String query = "SELECT * FROM [dbo].[FileTransferTaskTypes] WITH(NOLOCK)";
        LOG.info("query :: " + query);
        List<FileTransferType> list = new ArrayList<FileTransferType>();
        try {
            list = jdbcTemplate.query(query, new FileTransferTypeRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllFileTransferTypes . Ex = {}", e);
        }
        return list;
    } 

}
