/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.services;

import com.debisys.languages.Languages;
import com.debisys.utils.OperationResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.dto.RepType;
import net.emida.supportsite.dto.fileExporter.EntityExportTask;
import net.emida.supportsite.dto.fileExporter.ExportRetriever;
import net.emida.supportsite.dto.fileExporter.FileExporter;
import net.emida.supportsite.dto.fileTransfer.FileTransferConnection;
import net.emida.supportsite.dto.fileTransfer.FileTransferTask;
import net.emida.supportsite.dto.fileTransfer.FileTransferTaskConnection;
import net.emida.supportsite.dto.fileTransfer.FileTransferTaskType;
import net.emida.supportsite.dto.fileTransfer.FileTransferType;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.conciliationExporter.repository.IEntityExportTaskRepositoryDao;
import net.emida.supportsite.tools.conciliationExporter.repository.IExportRetrieverRepositoryDao;
import net.emida.supportsite.tools.conciliationExporter.repository.IFileExporterRepositoryDao;
import net.emida.supportsite.tools.conciliationExporter.repository.IFileTransferConnectionRepositoryDao;
import net.emida.supportsite.tools.conciliationExporter.repository.IFileTransferTaskRepostoryDao;
import net.emida.supportsite.tools.conciliationExporter.repository.IFileTransferTaskTypeRepositoryDao;
import net.emida.supportsite.tools.conciliationExporter.repository.IFileTransferTypeRepositoryDao;
import net.emida.supportsite.tools.conciliationExporter.repository.IRepTypeRepositoryDao;
import net.emida.supportsite.tools.conciliationExporter.util.ConciliationExporterFormCodes;
import net.emida.supportsite.tools.conciliationExporter.util.ConciliationExporterUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fernandob
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class ConciliationExporterController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConciliationExporterController.class);

    private static final String TOOL_PATH = "tools/conciliationExporter/conciliationExporter";

    private static final String INVALID_FILE_EXPORTER = "001";
    private static final String FILE_EXPORTER_ALREADY_EXISTS = "002";
    private static final String FILE_EXPORTER_IS_REFERENCED = "003";
    private static final String FILE_EXPORTER_EDIT_ONDELETED = "004";
    private static final String FILE_EXPORTER_DELETE_ONDELETED = "005";
    private static final String RETRIEVER_ALREADY_EXISTS = "006";
    private static final String INVALID_RETRIEVER = "007";
    private static final String RETRIEVER_EDIT_ONDELETED = "008";
    private static final String INVALID_FILE_RETRIEVER = "009";
    private static final String RETRIEVER_IS_REFERENCED = "010";
    private static final String RETRIEVER_DELETE_ONDELETED = "011";
    private static final String EXPORT_TASK_ALREADY_EXISTS_BY_INDEX_FIELDS = "012";
    private static final String EXPORT_TASK_ALREADY_EXISTS_BY_DESCRIPTION = "013";
    private static final String INVALID_EXPORT_TASK = "014";
    private static final String EXPORT_TASK_EDIT_ONDELETED = "015";
    private static final String EXPORT_TASK_DELETE_ONDELETED = "016";
    
    @Autowired
    private IEntityExportTaskRepositoryDao entityExportTaskDao;

    @Autowired
    private IExportRetrieverRepositoryDao exportRetrieverDao;

    @Autowired
    private IFileExporterRepositoryDao fileExporterDao;

    @Autowired
    private IFileTransferConnectionRepositoryDao fileTransferConnectionDao;

    @Autowired
    private IFileTransferTaskRepostoryDao fileTransferTaskDao;

    @Autowired
    private IFileTransferTaskTypeRepositoryDao fileTransferTaskTypeDao;

    @Autowired
    private IFileTransferTypeRepositoryDao fileTransferTypeDao;

    @Autowired
    private IRepTypeRepositoryDao repTypesDao;

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT, method = RequestMethod.GET)
    public String showConciliationExportForm(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        return TOOL_PATH;

    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getExportTasks", method = RequestMethod.GET)
    @ResponseBody
    public List<EntityExportTask> getExportTasks() {
        try {
            return this.entityExportTaskDao.getAllEntityExportTasks();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/insertEntityExportTask", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult insertEntityExportTask(EntityExportTask entityExportTask) {
        OperationResult retValue = new OperationResult();
        try {
            if (entityExportTask.isValid()) {
                if (this.entityExportTaskDao.getEntityExportTasksByDescription(entityExportTask.getDescription()).isEmpty()) {
                    if (this.entityExportTaskDao.getEntityExportTaskByIndexFields(entityExportTask).isEmpty()) {
                        this.entityExportTaskDao.insert(entityExportTask);
                        retValue.setError(false);
                        retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                        retValue.setResultMessage("Successful");
                    } else {
                        retValue.setError(true);
                        retValue.setResultCode(EXPORT_TASK_ALREADY_EXISTS_BY_INDEX_FIELDS);
                        retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.exportTask.error.alreadyExistsByIndexFields", getSessionData().getLanguage()));
                    }
                } else {
                    retValue.setError(true);
                    retValue.setResultCode(EXPORT_TASK_ALREADY_EXISTS_BY_DESCRIPTION);
                    retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.exportTask.error.alreadyExistsByDescription", getSessionData().getLanguage()));
                }
            } else {
                retValue.setError(true);
                retValue.setResultCode(INVALID_EXPORT_TASK);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.exportTask.error.incomplete", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/editEntityExportTask", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult editEntityExportTask(EntityExportTask entityExportTask) {
        OperationResult retValue = new OperationResult();
        try {
            if (entityExportTask.isValid()) {
                if (this.entityExportTaskDao.getEntityExportTaskById(entityExportTask.getId()) != null) {
                    if (!isExportTaskDuplicateByDescription(entityExportTask)) {
                        if (!isExportTaskDuplicateByIndexFields(entityExportTask)) {
                            this.entityExportTaskDao.update(entityExportTask);
                            retValue.setError(false);
                            retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                            retValue.setResultMessage("Successful");
                        } else {
                            retValue.setError(true);
                            retValue.setResultCode(EXPORT_TASK_ALREADY_EXISTS_BY_INDEX_FIELDS);
                            retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.exportTask.error.alreadyExistsByIndexFields", getSessionData().getLanguage()));
                        }
                    } else {
                        retValue.setError(true);
                        retValue.setResultCode(EXPORT_TASK_ALREADY_EXISTS_BY_DESCRIPTION);
                        retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.exportTask.error.alreadyExistsByDescription", getSessionData().getLanguage()));
                    }
                } else {
                    retValue.setError(true);
                    retValue.setResultCode(EXPORT_TASK_EDIT_ONDELETED);
                    retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.retriever.error.editOnDeletedRecord", getSessionData().getLanguage()));
                }
            } else {
                retValue.setError(true);
                retValue.setResultCode(INVALID_EXPORT_TASK);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.exportTask.error.incomplete", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/deleteExportTask/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public OperationResult deleteExportTask(@PathVariable("id") String exportTaskId) {
        OperationResult retValue = new OperationResult();
        try {
            if (this.entityExportTaskDao.getEntityExportTaskById(exportTaskId) != null) {
                this.entityExportTaskDao.delete(exportTaskId);
                retValue.setError(false);
                retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                retValue.setResultMessage("Successful");
            } else {
                retValue.setError(true);
                retValue.setResultCode(EXPORT_TASK_DELETE_ONDELETED);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.exportTask.error.deleteOnNonexistent", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    private boolean isExportTaskDuplicateByDescription(EntityExportTask currentExportTask) {
        boolean retValue = false;

        List<EntityExportTask> foundTasks = this.entityExportTaskDao.getEntityExportTasksByDescription(currentExportTask.getDescription());

        if (!foundTasks.isEmpty()) {
            for (EntityExportTask currentFound : foundTasks) {
                if (!currentFound.getId().equals(currentExportTask.getId())) {
                    retValue = true;
                }
            }
        }

        return retValue;
    }

    private boolean isExportTaskDuplicateByIndexFields(EntityExportTask currentExportTask) {
        boolean retValue = false;

        List<EntityExportTask> foundTasks = this.entityExportTaskDao.getEntityExportTaskByIndexFields(currentExportTask);

        if (!foundTasks.isEmpty()) {
            for (EntityExportTask currentFound : foundTasks) {
                if (!currentFound.getId().equals(currentExportTask.getId())) {
                    retValue = true;
                }
            }
        }

        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllExportRetrievers", method = RequestMethod.GET)
    @ResponseBody
    public List<ExportRetriever> getAllExportRetrievers(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return this.exportRetrieverDao.getAllExportRetrievers();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/insertRetriever", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult insertRetriever(ExportRetriever retriever) {
        OperationResult retValue = new OperationResult();
        try {
            if (retriever.isValid()) {
                if (this.exportRetrieverDao.getExportRetrieverByDescription(retriever.getDescription()).isEmpty()) {
                    this.exportRetrieverDao.insert(retriever);
                    retValue.setError(false);
                    retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                    retValue.setResultMessage("Successful");
                } else {
                    retValue.setError(true);
                    retValue.setResultCode(RETRIEVER_ALREADY_EXISTS);
                    retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.retriever.error.alreadyExists", getSessionData().getLanguage()));
                }
            } else {
                retValue.setError(true);
                retValue.setResultCode(INVALID_RETRIEVER);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.retriever.error.incomplete", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/editRetriever", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult editRetriever(ExportRetriever retriever) {
        OperationResult retValue = new OperationResult();
        try {
            if (retriever.isValid()) {
                if (this.exportRetrieverDao.getExportRetrieverById(retriever.getId()) != null) {
                    this.exportRetrieverDao.update(retriever);
                    retValue.setError(false);
                    retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                    retValue.setResultMessage("Successful");
                } else {
                    retValue.setError(true);
                    retValue.setResultCode(RETRIEVER_EDIT_ONDELETED);
                    retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.retriever.error.editOnDeletedRecord", getSessionData().getLanguage()));
                }
            } else {
                retValue.setError(true);
                retValue.setResultCode(INVALID_FILE_RETRIEVER);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.retriever.error.incomplete", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/deleteRetriever/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public OperationResult deleteRetriever(@PathVariable("id") String retrieverId) {
        OperationResult retValue = new OperationResult();
        try {
            if (this.entityExportTaskDao.getEntityExportTasksByRetrieverId(retrieverId).isEmpty()) {
                this.exportRetrieverDao.delete(retrieverId);
                retValue.setError(false);
                retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                retValue.setResultMessage("Successful");
            } else {
                retValue.setError(true);
                retValue.setResultCode(RETRIEVER_IS_REFERENCED);                
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.retriever.error.isReferenced", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllFileExporters", method = RequestMethod.GET)
    @ResponseBody
    public List<FileExporter> getAllFileExporters(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return this.fileExporterDao.getAllFileExporters();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return new ArrayList<FileExporter>();
        }
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/insertFileExporter", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult insertFileExporter(FileExporter fileExporter) {
        OperationResult retValue = new OperationResult();
        try {
            if (fileExporter.isValid()) {
                if (this.fileExporterDao.getFileExporterByClassDesc(fileExporter) == null) {
                    this.fileExporterDao.insert(fileExporter);
                    retValue.setError(false);
                    retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                    retValue.setResultMessage("Successful");
                } else {
                    retValue.setError(true);
                    retValue.setResultCode(FILE_EXPORTER_ALREADY_EXISTS);
                    retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileExporter.error.alreadyExists", getSessionData().getLanguage()));
                }
            } else {
                retValue.setError(true);
                retValue.setResultCode(INVALID_FILE_EXPORTER);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileExporter.error.incomplete", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/editFileExporter", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult editFileExporter(FileExporter fileExporter) {
        OperationResult retValue = new OperationResult();
        try {
            if (fileExporter.isValid()) {
                if (this.fileExporterDao.getFileExporterById(fileExporter.getId()) != null) {
                    this.fileExporterDao.update(fileExporter);
                    retValue.setError(false);
                    retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                    retValue.setResultMessage("Successful");
                } else {
                    retValue.setError(true);
                    retValue.setResultCode(FILE_EXPORTER_EDIT_ONDELETED);
                    retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileExporter.error.editOnDeletedRecord", getSessionData().getLanguage()));
                }
            } else {
                retValue.setError(true);
                retValue.setResultCode(INVALID_FILE_EXPORTER);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileExporter.error.incomplete", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/deleteFileExporter/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public OperationResult deleteFileExporter(@PathVariable("id") String fileExporterId) {
        OperationResult retValue = new OperationResult();
        try {
            if (this.entityExportTaskDao.getEntityExportTasksByExporterId(fileExporterId).isEmpty()) {
                this.fileExporterDao.delete(fileExporterId);
                retValue.setError(false);
                retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                retValue.setResultMessage("Successful");
            } else {
                retValue.setError(true);
                retValue.setResultCode(FILE_EXPORTER_DELETE_ONDELETED);
                retValue.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileExporter.error.isReferenced", getSessionData().getLanguage()));
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllFileTransferConnections", method = RequestMethod.GET)
    @ResponseBody
    public List<FileTransferConnection> getAllFileTransferConnections(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return this.fileTransferConnectionDao.getAllFileTransferConnections();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllFileTransferTypes", method = RequestMethod.GET)
    @ResponseBody
    public List<FileTransferType> getAllFileTransferTypes(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return this.fileTransferTypeDao.getAllFileTransferTypes();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }
    
    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllFileTransferTasks", method = RequestMethod.GET)
    @ResponseBody
    public List<FileTransferTask> getAllFileTransferTasks(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return this.fileTransferTaskDao.getAllFileTransferTasks();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllFileTransferTaskTypes", method = RequestMethod.GET)
    @ResponseBody
    public List<FileTransferTaskType> getAllFileTransferTaskTypes(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return this.fileTransferTaskTypeDao.getAllFileTransferTaskTypes();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }
    
    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllTransferTypesConnection", method = RequestMethod.GET)
    @ResponseBody
    public List<FileTransferTaskConnection> getAllTransferTypesConnection() {
        try {
            return this.fileTransferTaskDao.getAllTransferTypeConnection();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        } catch (Exception e) {
            LOGGER.error("ConciliationExporterController.getAllTransferTypesConnection - ERROR GETTING FILE TRANSFER {} ",e.getLocalizedMessage());
            return null;
        }
    }
    
    
    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/insertFileTransferTaskTypeConnection", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult insertFileTransferTaskTypeConnection(FileTransferTask fileTransferTask) {
        
        OperationResult responseApi = new OperationResult();

        try {
            if (fileTransferTask != null) {
                if (ConciliationExporterUtil.validateTransferTaskFields(fileTransferTask)) {
                    if (this.fileTransferTaskDao.getFileTransferTaskByDescription(fileTransferTask.getDescription())) { // If exists a description with the same name, can't add new record
                        FileTransferTaskType objFileTransferTaskType = this.fileTransferTaskTypeDao.getFileTransferTaskTypeById(fileTransferTask.getFileTransferTaskTypeId()); // Searching fileTransferTaskType by id
                        if (objFileTransferTaskType != null) {
                            FileTransferConnection objFileTransferConnection = this.fileTransferConnectionDao.getFileTransferConnectionById(fileTransferTask.getFileTransferConnectionId()); // Searching fileTransferConnection by id
                            if (objFileTransferConnection != null) {
                                if (this.fileTransferTaskDao.insert(fileTransferTask) == 1) {
                                    responseApi.setError(false);
                                    responseApi.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                                    responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.success.newTransferTask", getSessionData().getLanguage()));
                                    LOGGER.info("ConciliationExporterController.insertFileTransferTaskTypeConnection - FILE TRANSFER ADDED AS ROW SUCCESFUL");
                                } else {
                                    responseApi.setError(true);
                                    responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_FILETRANSFER_NEW_TRANSFER);
                                    responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.newTransferTask", getSessionData().getLanguage()));
                                    LOGGER.info("ConciliationExporterController.insertFileTransferTaskTypeConnection - IT HAS NOT BEEN POSSIBLE TO ADD THE NEW TRANSFER TASK");
                                }
                            } else {
                                responseApi.setError(true);
                                responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_FILETRANSFER_INVALID_CONNECTION_TYPE);
                                responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.connectionType", getSessionData().getLanguage()));
                                LOGGER.info("ConciliationExporterController.insertFileTransferTaskTypeConnection - CONNECTION TYPE WITH ID [{0}] NOT FOUND", fileTransferTask.getFileTransferConnectionId());
                            }
                        } else {
                            responseApi.setError(true);
                            responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_FILETRANSFER_INVALID_TASK_TYPE);
                            responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.taskType", getSessionData().getLanguage()));
                            LOGGER.info("ConciliationExporterController.insertFileTransferTaskTypeConnection - TASK TYPE WITH ID [{0}] NOT FOUND", fileTransferTask.getFileTransferTaskTypeId());
                        }
                    } else {
                        responseApi.setError(true);
                        responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_FILETRANSFER_INVALID_DESCRIPTION);
                        responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.description", getSessionData().getLanguage()));
                        LOGGER.info("ConciliationExporterController.insertFileTransferTaskTypeConnection - DESCRIPTION [{0}] EXISTS ALREADY", fileTransferTask.getDescription());
                    }
                } else {
                    responseApi.setError(true);
                    responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_FILETRANSFER_ALL_FIELDS_REQUIRED);
                    responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.nullFields", getSessionData().getLanguage()));
                }
            } else {
                responseApi.setError(true);
                responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_FILETRANSFER_NULL_FIELDS);
                responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.nullFields", getSessionData().getLanguage()));
                LOGGER.info("ConciliationExporterController.insertFileTransferTaskTypeConnection - OBJECT FILE TRANSFER ARRIVED NULL ");
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return responseApi;
    }
    
    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/editFileTransferTaskTypeConnection", method = RequestMethod.POST)
    @ResponseBody
    public OperationResult editFileTransferTaskTypeConnection(FileTransferTask fileTransferTask) {
        OperationResult responseApi = new OperationResult();
        try {
            if (fileTransferTask != null) {
                if (ConciliationExporterUtil.validateEditFormTransferTask(fileTransferTask)) {
                    if (this.fileTransferTaskDao.update(fileTransferTask)) {
                        responseApi.setError(false);
                        responseApi.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                        responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.success.updatedTransferTask", getSessionData().getLanguage()));
                        LOGGER.info("ConciliationExporterController.editFileTransferTaskTypeConnection - FILE TRANSFER ROW HAS BEEN UPDATED SUCCESFULLY");
                    } else {
                        responseApi.setError(true);
                        responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_UPDATE_RECORD_FAILED_TRANSFER);
                        responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.updateTransferTask", getSessionData().getLanguage()));
                        LOGGER.info("ConciliationExporterController.editFileTransferTaskTypeConnection - IT HAS NOT BEEN POSSIBLE TO UPDATE THE NEW ROW");
                    }
                } else {
                    responseApi.setError(true);
                    responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_UPDATE_ALL_FIELDS_REQUIRED);
                    responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.nullFields", getSessionData().getLanguage()));
                }
            } else {
                responseApi.setError(true);
                responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_UPDATE_THERES_NO_DATA);
                responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.nullFields", getSessionData().getLanguage()));
                LOGGER.info("ConciliationExporterController.editFileTransferTaskTypeConnection - OBJECT FILE TRANSFER ARRIVED NULL");
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return responseApi;
    }
    
    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/deleteFileTransferTaskTypeConnection/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public OperationResult deleteFileTransferTaskTypeConnection(@PathVariable("id") String retrieverId) {
        OperationResult responseApi = new OperationResult();
        try {
            if (retrieverId != null && !retrieverId.isEmpty()) {
                FileTransferTask objFileTransferTask = this.fileTransferTaskDao.getFileTransferTaskById(retrieverId);
                if (objFileTransferTask != null && objFileTransferTask.getId() != null && !objFileTransferTask.getId().isEmpty()) {
                    if (this.fileTransferTaskDao.delete(retrieverId)) {
                        responseApi.setError(false);
                        responseApi.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                        responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.success.deleteTransferTask", getSessionData().getLanguage()));
                        LOGGER.info("ConciliationExporterController.deleteFileTransferTaskTypeConnection - FILE TRANSFER ROW HAS BEEN DELETED SUCCESFULLY");
                    } else {
                        responseApi.setError(true);
                        responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_DELETE_RECORD_FAILED_TRANSFER);
                        responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.delete.failed", getSessionData().getLanguage()));
                        LOGGER.info("ConciliationExporterController.deleteFileTransferTaskTypeConnection - IT HAS NOT BEEN POSSIBLE TO DELETE THE ROW");
                    }
                } else {
                    responseApi.setError(true);
                    responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_DELETE_THERES_NO_EXISTS);
                    responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.delete.notExists", getSessionData().getLanguage()));
                    LOGGER.info("ConciliationExporterController.deleteFileTransferTaskTypeConnection - ROW ID [{0}] NOT FOUND ", retrieverId);
                }
            } else {
                responseApi.setError(true);
                responseApi.setResultCode(ConciliationExporterFormCodes.CODE_ERROR_DELETE_ID_RECORD_NULL);
                responseApi.setResultMessage(Languages.getString("jsp.tools.conciliationExporter.fileTransferTask.form.notifications.error.delete.idNull", getSessionData().getLanguage()));
                LOGGER.info("ConciliationExporterController.deleteFileTransferTaskTypeConnection - ID ROW IS NULL ");
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
        return responseApi;
    }
    

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getRepTypeList", method = RequestMethod.GET)
    @ResponseBody
    public List<RepType> getRepTypes(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        try {
            return this.repTypesDao.getAllRepTypes();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONCILIATION_EXPORT + "/getAllRepTypes", method = RequestMethod.GET)
    @ResponseBody
    public Map<Long, RepType> getAllRepTypes(Model model, Locale locale, HttpServletRequest req, HttpSession httpSession) {
        Map<Long, RepType> retValue = new HashMap<Long, RepType>();
        try {
            List<RepType> allRepTypes = this.repTypesDao.getAllRepTypes();
            for (RepType currentType : allRepTypes) {
                retValue.put(currentType.getTypeId(), currentType);
            }
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
        }
        return retValue;
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 37;
    }

}
