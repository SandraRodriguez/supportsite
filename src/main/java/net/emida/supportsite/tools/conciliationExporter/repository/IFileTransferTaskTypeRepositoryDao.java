/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.util.List;
import net.emida.supportsite.dto.fileTransfer.FileTransferTaskType;

/**
 *
 * @author fernandob
 */
public interface IFileTransferTaskTypeRepositoryDao {

    List<FileTransferTaskType> getAllFileTransferTaskTypes();

    FileTransferTaskType getFileTransferTaskTypeByCode(String fileTransferTaskTypeCode);

    FileTransferTaskType getFileTransferTaskTypeById(String fileTransferTaskTypeId);
    
}
