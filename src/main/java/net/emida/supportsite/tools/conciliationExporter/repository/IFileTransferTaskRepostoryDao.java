/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.util.List;
import net.emida.supportsite.dto.fileTransfer.FileTransferTask;
import net.emida.supportsite.dto.fileTransfer.FileTransferTaskConnection;

/**
 *
 * @author fernandob
 */
public interface IFileTransferTaskRepostoryDao {

    boolean delete(String fileTransferTaskId);

    List<FileTransferTask> getAllFileTransferTasks();

    FileTransferTask getFileTransferTaskById(String fileExporterId);
    
    boolean getFileTransferTaskByDescription(String description);

    int insert(FileTransferTask fileTransferTask);

    boolean update(FileTransferTask fileTransferTask);
    
    List<FileTransferTaskConnection> getAllTransferTypeConnection();
    
}
