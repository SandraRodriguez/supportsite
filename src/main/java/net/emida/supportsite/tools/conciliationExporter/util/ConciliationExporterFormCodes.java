/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.tools.conciliationExporter.util;

/**
 *
 * @author emida
 */
public interface ConciliationExporterFormCodes {
    
    /**
     * Success codes file transfer task form
     */
    String CODE_SUCCESS_FILETRANSFER_ADDED                                      =  "00"; 
    
    /**
     * Error codes file transfer task form
     */
    String CODE_ERROR_FILETRANSFER_NEW_TRANSFER                                 =  "017"; 
    String CODE_ERROR_FILETRANSFER_INVALID_TASK_TYPE                            =  "018"; 
    String CODE_ERROR_FILETRANSFER_INVALID_CONNECTION_TYPE                      =  "019"; 
    String CODE_ERROR_FILETRANSFER_INVALID_DESCRIPTION                          =  "020"; 
    String CODE_ERROR_FILETRANSFER_NULL_FIELDS                                  =  "022"; 
    String CODE_ERROR_FILETRANSFER_ALL_FIELDS_REQUIRED                          =  "023"; 
    
    /**
     * Error codes update record file transfer task form
     */
    String CODE_ERROR_UPDATE_RECORD_FAILED_TRANSFER                             =  "024"; 
    String CODE_ERROR_UPDATE_ALL_FIELDS_REQUIRED                                =  "025"; 
    String CODE_ERROR_UPDATE_THERES_NO_DATA                                     =  "026"; 
    
    /**
     * Error codes delete record file transfer task form
     */
    String CODE_ERROR_DELETE_THERES_NO_EXISTS                                   =  "027"; 
    String CODE_ERROR_DELETE_RECORD_FAILED_TRANSFER                             =  "028"; 
    String CODE_ERROR_DELETE_ID_RECORD_NULL                                     =  "029"; 
}
