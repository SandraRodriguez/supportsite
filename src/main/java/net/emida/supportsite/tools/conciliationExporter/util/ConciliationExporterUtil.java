package net.emida.supportsite.tools.conciliationExporter.util;

import net.emida.supportsite.dto.fileTransfer.FileTransferTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author emida
 */
public class ConciliationExporterUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConciliationExporterUtil.class);
    
    /**
     * Validate fiels transfer task fields of form.
     * 
     * @param fileTransferTask
     * @return 
     */
    public static boolean validateTransferTaskFields(FileTransferTask fileTransferTask) {
        boolean response = false;
        if (fileTransferTask != null && 
            fileTransferTask.getDescription()              != null && !fileTransferTask.getDescription().isEmpty() &&
            fileTransferTask.getSourceFolder()             != null && !fileTransferTask.getSourceFolder().isEmpty() &&
            fileTransferTask.getTargetFolder()             != null && !fileTransferTask.getTargetFolder().isEmpty() &&
            fileTransferTask.getFileNamePattern()          != null && !fileTransferTask.getFileNamePattern().isEmpty() &&
            fileTransferTask.getProcessedFolder()          != null && !fileTransferTask.getProcessedFolder().isEmpty() &&
            fileTransferTask.getFileTransferTaskTypeId()   != null && !fileTransferTask.getFileTransferTaskTypeId().isEmpty() && 
            fileTransferTask.getFileTransferConnectionId() != null && !fileTransferTask.getFileTransferConnectionId().isEmpty() &&
            (fileTransferTask.getRetriesOnTransferFail()  >= 0  &&  fileTransferTask.getRetriesOnTransferFail() <= 10 )) {
            response = true;
        } else {
            LOGGER.info("ConciliationExporterUtil.validateTransferTaskFields - ALL FIELDS ARE REQUIRED : \n"
                    + "     DESCRIPTION : "+ ((fileTransferTask.getDescription() != null &&  !fileTransferTask.getDescription().isEmpty()) ? fileTransferTask.getDescription() : "") + "\n"
                    + "     SOURCE FOLDER : "+ ((fileTransferTask.getSourceFolder() != null &&  !fileTransferTask.getSourceFolder().isEmpty()) ? fileTransferTask.getSourceFolder() : "") + "\n"
                    + "     TARGET FOLDER : "+ ((fileTransferTask.getTargetFolder() != null &&  !fileTransferTask.getTargetFolder().isEmpty()) ? fileTransferTask.getTargetFolder() : "") + "\n"
                    + "     FILE NAME PATTERN : "+ ((fileTransferTask.getFileNamePattern() != null &&  !fileTransferTask.getFileNamePattern().isEmpty()) ? fileTransferTask.getFileNamePattern() : "") + "\n"
                    + "     PROCESSED FOLDER : "+ ((fileTransferTask.getProcessedFolder() != null &&  !fileTransferTask.getProcessedFolder().isEmpty()) ? fileTransferTask.getProcessedFolder() : "") + "\n"
                    + "     TASK TYPE ID : "+ ((fileTransferTask.getFileTransferTaskTypeId() != null &&  !fileTransferTask.getFileTransferTaskTypeId().isEmpty()) ? fileTransferTask.getFileTransferTaskTypeId() : "") + "\n"
                    + "     CONNECTION TYPE ID : "+ ((fileTransferTask.getFileTransferConnectionId() != null &&  !fileTransferTask.getFileTransferConnectionId().isEmpty()) ? fileTransferTask.getFileTransferConnectionId() : "") + "\n"
                    + "     RETRIES ON TRANSFER FAIL : "+  fileTransferTask.getRetriesOnTransferFail() + "\n"
            );
        }
        return response;
    }
    
    /**
     * Validate fiels transfer task fields of form.
     * 
     * @param fileTransferTask
     * @return 
     */
    public static boolean validateEditFormTransferTask (FileTransferTask fileTransferTask) {
        boolean response = false;
        if (fileTransferTask != null && 
            fileTransferTask.getId()                       != null && !fileTransferTask.getId().isEmpty() &&
            fileTransferTask.getDescription()              != null && !fileTransferTask.getDescription().isEmpty() &&
            fileTransferTask.getSourceFolder()             != null && !fileTransferTask.getSourceFolder().isEmpty() &&
            fileTransferTask.getTargetFolder()             != null && !fileTransferTask.getTargetFolder().isEmpty() &&
            fileTransferTask.getFileNamePattern()          != null && !fileTransferTask.getFileNamePattern().isEmpty() &&
            fileTransferTask.getProcessedFolder()          != null && !fileTransferTask.getProcessedFolder().isEmpty() &&
            fileTransferTask.getFileTransferTaskTypeId()   != null && !fileTransferTask.getFileTransferTaskTypeId().isEmpty() && 
            fileTransferTask.getFileTransferConnectionId() != null && !fileTransferTask.getFileTransferConnectionId().isEmpty() && 
            (fileTransferTask.getRetriesOnTransferFail()  >= 0  &&  fileTransferTask.getRetriesOnTransferFail() <= 10 )) {
            response = true;
        } else {
            LOGGER.info("ConciliationExporterUtil.validateTransferTaskFields - ALL FIELDS ARE REQUIRED : \n"
                    + "     ID : "+ ((fileTransferTask.getId() != null &&  !fileTransferTask.getId().isEmpty()) ? fileTransferTask.getId() : "") + "\n"
                    + "     DESCRIPTION : "+ ((fileTransferTask.getDescription() != null &&  !fileTransferTask.getDescription().isEmpty()) ? fileTransferTask.getDescription() : "") + "\n"
                    + "     SOURCE FOLDER : "+ ((fileTransferTask.getSourceFolder() != null &&  !fileTransferTask.getSourceFolder().isEmpty()) ? fileTransferTask.getSourceFolder() : "") + "\n"
                    + "     TARGET FOLDER : "+ ((fileTransferTask.getTargetFolder() != null &&  !fileTransferTask.getTargetFolder().isEmpty()) ? fileTransferTask.getTargetFolder() : "") + "\n"
                    + "     FILE NAME PATTERN : "+ ((fileTransferTask.getFileNamePattern() != null &&  !fileTransferTask.getFileNamePattern().isEmpty()) ? fileTransferTask.getFileNamePattern() : "") + "\n"
                    + "     PROCESSED FOLDER : "+ ((fileTransferTask.getProcessedFolder() != null &&  !fileTransferTask.getProcessedFolder().isEmpty()) ? fileTransferTask.getProcessedFolder() : "") + "\n"
                    + "     TASK TYPE ID : "+ ((fileTransferTask.getFileTransferTaskTypeId() != null &&  !fileTransferTask.getFileTransferTaskTypeId().isEmpty()) ? fileTransferTask.getFileTransferTaskTypeId() : "") + "\n"
                    + "     CONNECTION TYPE ID : "+ ((fileTransferTask.getFileTransferConnectionId() != null &&  !fileTransferTask.getFileTransferConnectionId().isEmpty()) ? fileTransferTask.getFileTransferConnectionId() : "") + "\n"
                    + "     RETRIES ON TRANSFER FAIL : "+  fileTransferTask.getRetriesOnTransferFail() + "\n"
            );
        }
        return response;
    }
    
}
