/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.dto.fileTransfer.FileTransferConnection;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IFileTransferConnectionRepositoryDao")
public class FileTransferConnectionRepository implements IFileTransferConnectionRepositoryDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FileTransferConnectionRepository.class);

    private class FileTransferConnectionRowMapper implements RowMapper<FileTransferConnection> {

        @Override
        public FileTransferConnection mapRow(ResultSet rs, int i) throws SQLException {
            
            // File transfer connections are managed by production
            // then no security data should be shown.
            
            return new FileTransferConnection(
                    rs.getString("id"),
                    rs.getString("description"),
                    "", // Host
                    0, // Port
                    rs.getString("fileTransferTypeId"),
                    rs.getBoolean("connectionByOperation"),
                    rs.getInt("connectionTimeout"),
                    "", // User
                    "", // Password
                    rs.getBoolean("activeMode"),
                    rs.getBoolean("binaryTransfer"),
                    rs.getString("sshConfigFilePath"),
                    rs.getBoolean("connectionEnabled")
            );
        }
    }

    @Override
    public FileTransferConnection getFileTransferConnectionById(String fileTransferConnectionId) {
        final String query = String.format("SELECT * FROM [dbo].[FileTransferConnection] WITH(NOLOCK) WHERE [id] = '%s'",
                fileTransferConnectionId);
        LOG.info("query :: " + query);
        FileTransferConnection dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileTransferConnectionRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }

    @Override
    public List<FileTransferConnection> getAllFileTransferConnections() {
        final String query = "SELECT * FROM [dbo].[FileTransferConnection] WITH(NOLOCK)";
        LOG.info("query :: " + query);
        List<FileTransferConnection> list = new ArrayList<FileTransferConnection>();
        try {
            list = jdbcTemplate.query(query, new FileTransferConnectionRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllFileTransferConnections . Ex = {}", e);
        }
        return list;
    }

}
