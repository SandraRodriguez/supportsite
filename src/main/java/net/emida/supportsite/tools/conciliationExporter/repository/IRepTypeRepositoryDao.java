/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.util.List;
import net.emida.supportsite.dto.RepType;

/**
 *
 * @author fernandob
 */
public interface IRepTypeRepositoryDao {

    List<RepType> getAllRepTypes();

    RepType getRepTypeById(long repTypeId);
    
}
