/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.util.List;
import net.emida.supportsite.dto.fileTransfer.FileTransferConnection;

/**
 *
 * @author fernandob
 */
public interface IFileTransferConnectionRepositoryDao {

    List<FileTransferConnection> getAllFileTransferConnections();

    FileTransferConnection getFileTransferConnectionById(String fileTransferConnectionId);
    
}
