/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.util.List;
import net.emida.supportsite.dto.fileExporter.EntityExportTask;

/**
 *
 * @author fernandob
 */
public interface IEntityExportTaskRepositoryDao {

    public int delete(String entityExportTaskId);

    public List<EntityExportTask> getAllEntityExportTasks();

    public EntityExportTask getEntityExportTaskById(String entityExportTaskId);

    public int insert(EntityExportTask entityExportTask);

    public int update(EntityExportTask entityExportTask);
    
    public EntityExportTask getEntityExportTaskByRetrieverId(String retrieverId);
    
    public List<EntityExportTask> getEntityExportTasksByRetrieverId(String retrieverId);
    
    public List<EntityExportTask> getEntityExportTasksByExporterId(String exporterId);
    
    public List<EntityExportTask> getEntityExportTasksByDescription(String description);
    
    public List<EntityExportTask> getEntityExportTaskByIndexFields(EntityExportTask entityExportTask);
    
}
