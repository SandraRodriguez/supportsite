/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.util.List;
import net.emida.supportsite.dto.fileExporter.FileExporter;

/**
 *
 * @author fernandob
 */
public interface IFileExporterRepositoryDao {

    public int delete(String fileExporterId);

    public List<FileExporter> getAllFileExporters();

    public FileExporter getFileExporterById(String fileExporterId);
    
    public FileExporter getFileExporterByClassDesc(FileExporter fileExporter);

    public int insert(FileExporter fileExporter);

    public int update(FileExporter fileExporter);
    
}
