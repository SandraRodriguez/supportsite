/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.dto.fileExporter.FileExporter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IFileExporterRepositoryDao")
public class FileExporterRepository implements IFileExporterRepositoryDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(FileExporterRepository.class);

    private class FileExporterRowMapper implements RowMapper<FileExporter> {

        @Override
        public FileExporter mapRow(ResultSet rs, int i) throws SQLException {
            return new FileExporter(
                    rs.getString("id"),
                    rs.getString("fileExporterClass"),
                    rs.getString("description"),
                    rs.getString("fileNameFormat"),
                    rs.getString("targetFilePath"),
                    rs.getString("headerFormat"),
                    rs.getString("recordFormat"),
                    rs.getString("recordSeparator"),
                    rs.getString("footerFormat")
            );
        }
    }

    @Override
    public int insert(FileExporter fileExporter) {
        int affectedRecords = 0;

        final StringBuilder sbInsert = new StringBuilder();
        sbInsert.append(" INSERT INTO [dbo].[FileExporters] ");
        sbInsert.append(" ([fileExporterClass], [description], ");
        sbInsert.append(" [fileNameFormat], [targetFilePath], [headerFormat], ");
        sbInsert.append(" [recordFormat], [recordSeparator], [footerFormat]) ");
        sbInsert.append(" VALUES ( ");
        sbInsert.append(String.format(" '%s', ", fileExporter.getFileExporterClass()));
        sbInsert.append(String.format(" '%s', ", fileExporter.getDescription()));
        sbInsert.append(String.format(" '%s', ", fileExporter.getFileNameFormat()));
        sbInsert.append(String.format(" '%s', ", fileExporter.getTargetFilePath()));
        sbInsert.append(String.format(" '%s', ", fileExporter.getHeaderFormat()));
        sbInsert.append(String.format(" '%s', ", fileExporter.getRecordFormat()));
        sbInsert.append(String.format(" '%s', ", fileExporter.getRecordSeparator()));
        sbInsert.append(String.format(" '%s') ", fileExporter.getFooterFormat()));

        String insert = sbInsert.toString();
        LOG.info("insert :: " + insert);
        try {
            affectedRecords = this.jdbcTemplate.update(insert);
        } catch (DataAccessException e) {
            LOG.error("Error on insert FileExporter. Ex = {}", e);
        }

        return affectedRecords;
    }

    @Override
    public int update(FileExporter fileExporter) {
        final StringBuilder sbUpdate = new StringBuilder();
        sbUpdate.append(" UPDATE [dbo].[FileExporters] ");
        sbUpdate.append(String.format(" SET [fileExporterClass] = '%s', ", fileExporter.getFileExporterClass()));
        sbUpdate.append(String.format(" [description] = '%s', ", fileExporter.getDescription()));
        sbUpdate.append(String.format(" [fileNameFormat] = '%s', ", fileExporter.getFileNameFormat()));
        sbUpdate.append(String.format(" [targetFilePath] ='%s', ", fileExporter.getTargetFilePath()));
        sbUpdate.append(String.format(" [headerFormat] = '%s', ", fileExporter.getHeaderFormat()));
        sbUpdate.append(String.format(" [recordFormat] = '%s', ", fileExporter.getRecordFormat()));
        sbUpdate.append(String.format(" [recordSeparator] = '%s', ", fileExporter.getRecordSeparator()));
        sbUpdate.append(String.format(" [footerFormat] = '%s' ", fileExporter.getFooterFormat()));
        sbUpdate.append(String.format(" WHERE [id] = '%s' ", fileExporter.getId()));

        String update = sbUpdate.toString();
        LOG.info("update :: " + update);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(update);
        } catch (DataAccessException e) {
            LOG.error("Error on update FileExporter. Ex = {}", e);
        }
        return rows;
    }

    @Override
    public int delete(String fileExporterId) {
        int rowsAfected = 0;
        final String query = String.format("DELETE FROM [dbo].[FileExporters] WHERE [id] = '%s'",
                fileExporterId);
        LOG.info("delete :: " + query);
        try {
            rowsAfected += jdbcTemplate.update(query);
        } catch (DataAccessException e) {
            LOG.error("Error on delete FileExporter. Ex = {}", e);
        }
        return rowsAfected;
    }

    @Override
    public FileExporter getFileExporterById(String fileExporterId) {
        final String query = String.format("SELECT * FROM [dbo].[FileExporters] WITH(NOLOCK) WHERE [id] = '%s'",
                fileExporterId);
        LOG.info("query :: " + query);
        FileExporter dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileExporterRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }
    
    @Override
    public FileExporter getFileExporterByClassDesc(FileExporter fileExporter) {
        final String query = String.format("SELECT * FROM [dbo].[FileExporters] WITH(NOLOCK) WHERE [fileExporterClass] = '%s' AND [description] = '%s'",
                fileExporter.getFileExporterClass(), fileExporter.getDescription());
        LOG.info("query :: " + query);
        FileExporter dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new FileExporterRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }
    

    @Override
    public List<FileExporter> getAllFileExporters() {
        final String query = "SELECT * FROM [dbo].[FileExporters] WITH(NOLOCK) ORDER BY [description]";
        LOG.info("query :: " + query);
        List<FileExporter> list = new ArrayList<FileExporter>();
        try {
            list = jdbcTemplate.query(query, new FileExporterRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllFileExporters. Ex = {}", e);
        }
        return list;
    }

}
