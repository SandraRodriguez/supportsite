/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.tools.conciliationExporter.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.dto.RepType;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IRepTypeRepositoryDao")
public class RepTypeRepository implements IRepTypeRepositoryDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(RepTypeRepository.class);

    private class RepTypeRowMapper implements RowMapper<RepType> {

        @Override
        public RepType mapRow(ResultSet rs, int i) throws SQLException {
            return new RepType(
                    rs.getLong("type_id"),
                    rs.getString("description")
            );
        }
    }

    @Override
    public RepType getRepTypeById(long repTypeId) {
        final String query = String.format("SELECT * FROM [dbo].[rep_type] WITH(NOLOCK) WHERE [id] = %d",
                repTypeId);
        LOG.info("query :: " + query);
        RepType dto;
        try {
            dto = jdbcTemplate.queryForObject(query, new RepTypeRowMapper());
        } catch (DataAccessException e) {
            dto = null;
        }
        return dto;
    }

    @Override
    public List<RepType> getAllRepTypes() {
        final String query = "SELECT * FROM [dbo].[rep_type] WITH(NOLOCK)";
        LOG.info("query :: " + query);
        List<RepType> list = new ArrayList<RepType>();
        try {
            list = jdbcTemplate.query(query, new RepTypeRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getAllRepTypes . Ex = {}", e);
        }
        return list;
    }

}
