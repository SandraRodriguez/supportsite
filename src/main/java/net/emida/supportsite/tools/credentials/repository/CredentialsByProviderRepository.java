package net.emida.supportsite.tools.credentials.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import net.emida.supportsite.tools.credentials.dao.CredentialDetail;
import net.emida.supportsite.tools.credentials.dao.Provider;
import org.simpleflatmapper.jdbc.spring.JdbcTemplateMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Emida
 */
@Repository
public class CredentialsByProviderRepository implements ICredentialsByProviderDao {

    private static final Logger LOG = LoggerFactory.getLogger(CredentialsByProviderRepository.class);

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private final ResultSetExtractor<List<Provider>> resultSetExtractor = JdbcTemplateMapperFactory
            .newInstance()
            .addKeys("idNewProvider", "details_idNewCredentialDetail") // the column name you expect the user id to be on
            .newResultSetExtractor(Provider.class);

    @Override
    public List<Provider> getAllProviders() {

        String query = "SELECT p.idNewProvider, p.providerId, p.status, a.name AS name, \n"
                + "c.idNewCredentialDetail    AS details_idNewCredentialDetail, \n"
                + "c.idNewProviderCredentials AS details_idNewProvider, \n"
                + "c.region                   AS details_region , \n"
                + "c.userCredential           AS details_user , \n"
                + "c.passwordCredential       AS details_password, \n"
                + "c.status                   AS details_statusDetail \n"
                + "FROM       [ProviderCredentials]       p WITH(NOLOCK) \n"
                + "LEFT OUTER JOIN [ProviderCredentialsDetail] c WITH(NOLOCK) ON p.idNewProvider = c.idNewProviderCredentials \n"
                + "LEFT OUTER JOIN [provider]                  a WITH(NOLOCK) ON a.provider_id   = p.providerId \n"
                + "ORDER BY a.name, c.region ";

        List<Provider> list = null;
        LOG.info("query :: " + query);
        //
        try {
            list = jdbcTemplate.query(query, resultSetExtractor);
        } catch (Exception e) {
            LOG.error("Error on get All Providers. Ex = {}", e);
        }
        return list;

    }

    @Override
    public Integer updateCredentialDetail(final CredentialDetail credentialDetail) {
        String sql = "UPDATE ProviderCredentialsDetail SET userCredential=?, passwordCredential=? "
                + " WHERE idNewCredentialDetail =?  ";
        LOG.info("update ProviderCredentialsDetail " + sql);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(sql, new PreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps) throws SQLException {
                    int index = 1;                    
                    ps.setString(index++, credentialDetail.getUser());
                    ps.setString(index++, credentialDetail.getPassword());
                    ps.setString(index++, credentialDetail.getIdNewCredentialDetail());
                }
            });
        } catch (DuplicateKeyException e) {
            LOG.error("Error on insertCredentialDetail. Ex = {}", e);
            rows = -1;
        } catch (Exception e) {
            LOG.error("Error on updateCredentialDetail. Ex = {}", e);
        }
        return rows;
    }

    @Override
    public Integer insertCredentialDetail(final Integer providerId, final CredentialDetail credentialDetail) {
        String sql = "DECLARE @ID UNIQUEIDENTIFIER\n"
                + "DECLARE @providerId INT\n"
                + "DECLARE @statusDetail INT\n"
                + "DECLARE @region VARCHAR(5)\n"
                + "DECLARE @user VARCHAR(25)\n"
                + "DECLARE @pass VARCHAR(25)\n"
                + "\n"
                + "SELECT @providerId = ?, @ID = NULL, @region=1, @user=?, @pass=?, @statusDetail=1 \n"
                + "\n"
                + "SELECT @ID = p.idNewProvider FROM ProviderCredentials p WITH(NOLOCK) WHERE p.providerId = @providerId \n"
                + "IF @ID IS NULL BEGIN \n"
                + "	SELECT @ID=NEWID() \n"
                + "	INSERT INTO [ProviderCredentials] ([idNewProvider],[providerId],[status]) VALUES (@ID,@providerId,1) \n"
                + "	INSERT INTO [ProviderCredentialsDetail] ([idNewProviderCredentials],[region],[userCredential],[passwordCredential],[status]) \n"
                + "    VALUES (@ID,@region,@user,@pass,@statusDetail) \n"
                + "END ELSE BEGIN	\n"
                + "             	\n"
                + "   SELECT @region = MAX(region)+1 FROM [ProviderCredentialsDetail] WITH(NOLOCK)  WHERE idNewProviderCredentials=@ID  \n"
                + "   IF @region <=9 BEGIN \n"
                + "	  INSERT INTO [ProviderCredentialsDetail] ([idNewProviderCredentials],[region],[userCredential],[passwordCredential],[status])\n"
                + "       VALUES (@ID,@region,@user,@pass,@statusDetail)\n"
                + "   END \n"
                + "END";
        LOG.info("update insertCredentialDetail " + sql);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(sql, new PreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps) throws SQLException {
                    int index = 1;
                    ps.setInt(index++, providerId);                    
                    ps.setString(index++, credentialDetail.getUser());
                    ps.setString(index++, credentialDetail.getPassword());                    
                }
            });
        } catch (DuplicateKeyException e) {
            LOG.error("Error on insertCredentialDetail. Ex = {}", e);
            rows = -1;
        } catch (Exception e) {
            LOG.error("Error on insertCredentialDetail. Ex = {}", e);
        }
        return rows;
    }

    /*
    @Override
    public Integer deleteCredentialDetail(final String id) {
        final String sql = "DECLARE @idNewProvider UNIQUEIDENTIFIER\n"
                + "DECLARE @idNewCredentialDetail UNIQUEIDENTIFIER\n"
                + "SELECT @idNewCredentialDetail = ? \n"
                + "SELECT @idNewProvider=idNewProviderCredentials FROM ProviderCredentialsDetail WHERE idNewCredentialDetail = @idNewCredentialDetail\n"
                + "IF (SELECT COUNT(*) FROM ProviderCredentialsDetail WHERE idNewProviderCredentials = @idNewProvider)>1\n"
                + "BEGIN\n"
                + "	DELETE FROM ProviderCredentialsDetail WHERE idNewCredentialDetail = @idNewCredentialDetail	\n"
                + "END ELSE BEGIN\n"
                + "	DELETE FROM ProviderCredentialsDetail WHERE idNewCredentialDetail = @idNewCredentialDetail\n"
                + "	DELETE FROM ProviderCredentials WHERE idNewProvider=@idNewProvider	\n"
                + "END  ";
        LOG.info("update deleteCredentialDetail " + sql);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(sql, new PreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps) throws SQLException {
                    int index = 1;
                    ps.setString(index++, id);
                }
            });
        } catch (Exception e) {
            LOG.error("Error on deleteCredentialDetail. Ex = {}", e);
        }
        return rows;
    }*/

    @Override
    public Integer updateStatusProvider(final Provider provider) {
        final String sql = "UPDATE ProviderCredentials SET status =? WHERE idNewProvider =?  ";
        LOG.info("update updateStatusProvider " + sql);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(sql, new PreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps) throws SQLException {
                    int index = 1;
                    ps.setInt(index++, provider.getStatus());
                    ps.setString(index++, provider.getIdNewProvider());
                }
            });
        } catch (Exception e) {
            LOG.error("Error on updateStatusProvider. Ex = {}", e);
        }
        return rows;
    }

    @Override
    public Integer updateStatusDetail(final CredentialDetail credentialDetail) {
        final String sql = "UPDATE ProviderCredentialsDetail SET status =? WHERE idNewCredentialDetail =?  ";
        LOG.info("update updateStatusProvider " + sql);
        int rows = 0;
        try {
            rows = this.jdbcTemplate.update(sql, new PreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps) throws SQLException {
                    int index = 1;
                    ps.setInt(index++, credentialDetail.getStatusDetail());
                    ps.setString(index++, credentialDetail.getIdNewCredentialDetail());
                }
            });
        } catch (Exception e) {
            LOG.error("Error on updateStatusDetail. Ex = {}", e);
        }
        return rows;
    }    

}
