package net.emida.supportsite.tools.credentials.services;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.dao.ProviderDao;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.credentials.dao.CredentialDetail;
import net.emida.supportsite.tools.credentials.dao.Provider;
import net.emida.supportsite.tools.credentials.repository.ICredentialsByProviderDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Emida
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH_CREDENTIAL)
public class CredentialsByProviderController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(CredentialsByProviderController.class);

    @Autowired
    private ICredentialsByProviderDao credentialsByProviderDao;

    @RequestMapping(value = UrlPaths.CREDENTIALS_PROVIDER, method = RequestMethod.GET)
    public String __init__() {
        return "tools/credentials/credentialsByProvider";
    }

    @RequestMapping(value = UrlPaths.CREDENTIALS_PROVIDER + "/list", method = RequestMethod.GET)
    @ResponseBody
    public Map list(HttpServletRequest req, HttpSession httpSession) {
        Map<String, Object> response = new LinkedHashMap<>();
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            List<Provider> list = credentialsByProviderDao.getAllProviders();
            List<net.emida.supportsite.dto.Provider> allProviders = ProviderDao.getAllProviders();
            response.put("providers", list);
            response.put("allProviders", allProviders);
            response.put("success", true);

            String provider = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnProvider", sessionData.getLanguage());
            String user = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnUser", sessionData.getLanguage());
            String password = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.columnPassword", sessionData.getLanguage());
            String contrlReq = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.contrlReq", sessionData.getLanguage());

            String providerMesValidator = String.format(contrlReq, provider);
            String providerMesUser = String.format(contrlReq, user);
            String providerMesPass = String.format(contrlReq, password);

            String lengthSingle = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.LenghtSingleMessage", sessionData.getLanguage());
            String lengthRange = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.LenghtRangeMessage", sessionData.getLanguage());

            String lengthMessSingle = String.format(lengthSingle, "3");
            String lengthMessRange = String.format(lengthRange, "1", "40");

            response.put("providerMesValidator", providerMesValidator);
            response.put("providerMesUser", providerMesUser);
            response.put("providerMesPass", providerMesPass);
            response.put("lengthMessSingle", lengthMessSingle);
            response.put("lengthMessRange", lengthMessRange);

        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            response.put("errMessage", ex.getMessage());
        }
        return response;
    }

    @RequestMapping(value = UrlPaths.CREDENTIALS_PROVIDER + "/statusProvider", method = RequestMethod.POST)
    public ResponseEntity<String> statusProvider(@RequestBody Map<String, String> parameters, Model model,
            Locale locale, HttpServletRequest req, HttpSession httpSession) {
        LOG.info("statusProvider action");
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        String responseMessage = Languages.getString("sort.products.products.lbl.l1", sessionData.getLanguage());
        try {
            Provider provider = new Provider();
            if (parameters.get("id") != null) {
                provider.setIdNewProvider(parameters.get("id"));
                if (parameters.get("status") != null) {
                    provider.setStatus(Integer.parseInt(parameters.get("status")));
                    credentialsByProviderDao.updateStatusProvider(provider);
                }
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        } finally {
            return new ResponseEntity<String>(responseMessage, HttpStatus.OK);
        }
    }

    @RequestMapping(value = UrlPaths.CREDENTIALS_PROVIDER + "/statusCredential", method = RequestMethod.POST)
    public ResponseEntity<String> statusDetail(@RequestBody Map<String, String> parameters, Model model,
            Locale locale, HttpServletRequest req, HttpSession httpSession) {
        LOG.info("statusProvider action");
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        String responseMessage = Languages.getString("sort.products.products.lbl.l1", sessionData.getLanguage());
        try {
            CredentialDetail credential = new CredentialDetail();
            String id = parameters.get("id");
            if (id != null && parameters.get("status") != null) {
                Integer status = Integer.parseInt(parameters.get("status"));
                credential.setIdNewCredentialDetail(id);
                credential.setStatusDetail(status);
                credentialsByProviderDao.updateStatusDetail(credential);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        } finally {
            return new ResponseEntity<String>(responseMessage, HttpStatus.OK);
        }
    }

    /*
    @RequestMapping(value = UrlPaths.CREDENTIALS_PROVIDER + "/dataCredential", method = RequestMethod.POST)
    public ResponseEntity<String> dataCredential(@RequestBody Map<String, String> parameters, Model model,
            Locale locale, HttpServletRequest req, HttpSession httpSession) {
        LOG.info("statusProvider action");
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        String responseMessage = Languages.getString("sort.products.products.lbl.l1", sessionData.getLanguage());
        try {
            CredentialDetail credential = new CredentialDetail();
            String id = parameters.get("id");
            String user = parameters.get("user");
            String password = parameters.get("password");
            if (id != null && user != null && password != null) {
                credential.setIdNewCredentialDetail(parameters.get("id"));
                credential.setUser(user);
                credential.setPassword(password);
                credentialsByProviderDao.updateCredentialDetail(credential);
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        } finally {
            return new ResponseEntity<String>(responseMessage, HttpStatus.OK);
        }
    }*/
    @RequestMapping(value = UrlPaths.CREDENTIALS_PROVIDER + "/save", method = RequestMethod.POST)
    public ResponseEntity<String> save(@RequestBody Map<String, String> parameters, Model model,
            Locale locale, HttpServletRequest req, HttpSession httpSession) {
        LOG.info("save action");
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        String responseMessage = "";
        int rows = 0;
        try {
            CredentialDetail detail = new CredentialDetail();
            String user = parameters.get("user");
            String password = parameters.get("password");
            String id = parameters.get("id");
            detail.setUser(user);
            detail.setPassword(password);
            if (user != null && password != null && "-1".equals(id)) {
                Integer providerId = Integer.parseInt(parameters.get("providerId"));
                rows = credentialsByProviderDao.insertCredentialDetail(providerId, detail);
            } else {
                detail.setIdNewCredentialDetail(id);
                rows = credentialsByProviderDao.updateCredentialDetail(detail);
            }
            if (rows == -1) {
                responseMessage = Languages.getString("jsp.includes.menu.tools.h2hFeatures.credentials.duplicate", sessionData.getLanguage());
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        } finally {
            return new ResponseEntity<String>(responseMessage, HttpStatus.OK);
        }

    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
