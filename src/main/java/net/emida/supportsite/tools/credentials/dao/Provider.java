package net.emida.supportsite.tools.credentials.dao;

import java.util.List;

/**
 *
 * @author Emida
 */
public class Provider {

    private String idNewProvider;
    private Integer providerId;
    private String name;
    private Integer status;
    private List<CredentialDetail> details;

    public Provider() {

    }

    /**
     * 
     * @param id
     * @param providerId
     * @param name
     * @param status
     * @param details 
     */
    public Provider(String id, Integer providerId, String name, Integer status, List<CredentialDetail> details) {
        this.idNewProvider = id;
        this.providerId = providerId;
        this.name = name;
        this.status = status;   
        this.details = details;
    }

    /**
     * @return the providerId
     */
    public Integer getProviderId() {
        return providerId;
    }

    /**
     * @param providerId the providerId to set
     */
    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }     

    

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the details
     */
    public List<CredentialDetail> getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(List<CredentialDetail> details) {
        this.details = details;
    }

    /**
     * @return the idNewProvider
     */
    public String getIdNewProvider() {
        return idNewProvider;
    }

    /**
     * @param idNewProvider the idNewProvider to set
     */
    public void setIdNewProvider(String idNewProvider) {
        this.idNewProvider = idNewProvider;
    }

}
