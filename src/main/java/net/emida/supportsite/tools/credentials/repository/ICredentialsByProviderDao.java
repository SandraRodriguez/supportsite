package net.emida.supportsite.tools.credentials.repository;

import java.util.List;
import net.emida.supportsite.tools.credentials.dao.CredentialDetail;
import net.emida.supportsite.tools.credentials.dao.Provider;

/**
 *
 * @author Emida
 */
public interface ICredentialsByProviderDao {
    
    public List<Provider> getAllProviders();
    
    public Integer updateCredentialDetail(CredentialDetail credentialDetail);
    
    public Integer insertCredentialDetail(Integer providerId, CredentialDetail credentialDetail);
        
    public Integer updateStatusProvider(Provider provider);
    
    public Integer updateStatusDetail(CredentialDetail credentialDetail);
}
