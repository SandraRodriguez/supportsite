package net.emida.supportsite.tools.credentials.dao;

/**
 *
 * @author Emida
 */
public class CredentialDetail {

    private String idNewCredentialDetail;
    private String idNewProvider;
    private Integer region;
    private String user;
    private String password;
    private Integer statusDetail;

    public CredentialDetail() {
    }

    /**
     * 
     * @param id
     * @param idNewProvider
     * @param region
     * @param user
     * @param password
     * @param status 
     */
    public CredentialDetail(String id, String idNewProvider, Integer region, String user,
            String password, Integer status) {
        this.idNewCredentialDetail = id;
        this.idNewProvider = idNewProvider;
        this.region = region;
        this.user = user;
        this.password = password;
        this.statusDetail = status;
    }

    /**
     * @return the idNewCredentialDetail
     */
    public String getIdNewCredentialDetail() {
        return idNewCredentialDetail;
    }

    /**
     * @param idNewCredentialDetail the idNewCredentialDetail to set
     */
    public void setIdNewCredentialDetail(String idNewCredentialDetail) {
        this.idNewCredentialDetail = idNewCredentialDetail;
    }

    /**
     * @return the idNewProvider
     */
    public String getIdNewProvider() {
        return idNewProvider;
    }

    /**
     * @param idNewProvider the idNewProvider to set
     */
    public void setIdNewProvider(String idNewProvider) {
        this.idNewProvider = idNewProvider;
    }

    /**
     * @return the region
     */
    public Integer getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(Integer region) {
        this.region = region;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the statusDetail
     */
    public Integer getStatusDetail() {
        return statusDetail;
    }

    /**
     * @param statusDetail the statusDetail to set
     */
    public void setStatusDetail(Integer statusDetail) {
        this.statusDetail = statusDetail;
    }
    
}
