package net.emida.supportsite.tools.instantSpiffSetting.repository;

import java.util.List;

/**
 *
 * @author janez@emida.net
 * @param <T>
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
public interface ISCommonsSettingDAO<T> {

    Integer add(T model);

    List<T> filterBy(T model);

    T getById(String id);

    Integer update(T model);

    Integer remove(String id);
}
