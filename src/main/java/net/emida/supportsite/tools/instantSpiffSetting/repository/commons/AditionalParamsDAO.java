package net.emida.supportsite.tools.instantSpiffSetting.repository.commons;

import java.util.List;
import net.emida.supportsite.tools.instantSpiffSetting.model.AditionalParams;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffProductBase;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
public interface AditionalParamsDAO {

    /**
     *
     * @param transType
     * @return
     */
    List<InstantSpiffProductBase> getProductsBy(Integer transType);

    /**
     * 
     * @param isEdit
     * @param iso_id
     * @return 
     */
    List<AditionalParams> getIsos(Boolean isEdit, Long iso_id);

    /**
     *
     * @param idSuper
     * @return
     */
    List<InstantSpiffProductBase> filter(String idSuper);

}
