package net.emida.supportsite.tools.instantSpiffSetting.repository.setting;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffRtr;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffSetting;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import net.emida.supportsite.tools.instantSpiffSetting.repository.rtr.InstantSpiffRtrDAO;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
@Repository
public class InstantSpiffSettingRepository implements InstantSpiffSettingDAO {

    private static final Logger LOG = Logger.getLogger(InstantSpiffSettingRepository.class);

    private static final String ALL = "SELECT s.*, r.businessname AS iso_name FROM dbo.IS_Setting s WITH(NOLOCK) INNER JOIN dbo.reps r WITH(NOLOCK) ON r.rep_id = s.iso_id";
    private static final String GET_BY_ID = "SELECT * FROM IS_Setting s WITH(NOLOCK) WHERE s.id = :id";
    private static final String ADD = "INSERT INTO IS_Setting(id, iso_id, status, update_dt) "
            + "                               VALUES(:id, :iso_id, :status, GETDATE())";
    private static final String UPDATE = "UPDATE IS_Setting SET iso_id = :iso_id, status = :status WHERE id = :id";
    private static final String REMOVE = "DELETE FROM IS_Setting WHERE id = :id";

    @Autowired
    @Qualifier("masterNamedParamJdbcTemplate")
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    @Autowired
    private InstantSpiffRtrDAO rtrRepository;

    @Override
    public Integer add(InstantSpiffSetting model) {
        try {
            String uid = UUID.randomUUID().toString().toUpperCase();
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", uid)
                    .addValue("iso_id", model.getIso_id())
                    .addValue("status", model.getStatus());

            List<InstantSpiffRtr> rtrs = model.getRtrs();
            for (InstantSpiffRtr rtr : rtrs) {
                rtr.setId_super(uid);
                rtrRepository.add(rtr);
            }

            return parameterJdbcTemplate.update(ADD, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public List<InstantSpiffSetting> filterBy(InstantSpiffSetting model) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource();
            return parameterJdbcTemplate.query(ALL, parameters, new BeanPropertyRowMapper(InstantSpiffSetting.class));
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new ArrayList<>();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new ArrayList<>();
        } catch (Exception ex) {
            LOG.error(ex);
            return new ArrayList<>();
        }
    }

    @Override
    public InstantSpiffSetting getById(String id) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id);
            return parameterJdbcTemplate.queryForObject(GET_BY_ID, parameters, new RowMapper<InstantSpiffSetting>() {
                @Override
                public InstantSpiffSetting mapRow(ResultSet rs, int index) throws SQLException {
                    InstantSpiffSetting model = new InstantSpiffSetting();
                    model.setId(rs.getString("id"));
                    model.setIso_id(rs.getLong("iso_id"));
                    model.setStatus(rs.getBoolean("status"));

                    InstantSpiffRtr filter = new InstantSpiffRtr();
                    filter.setId_super(model.getId());

                    model.setRtrs(rtrRepository.filterBy(filter));
                    return model;
                }
            });
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new InstantSpiffSetting();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new InstantSpiffSetting();
        } catch (Exception ex) {
            LOG.error(ex);
            return new InstantSpiffSetting();
        }
    }

    @Override
    public Integer update(InstantSpiffSetting model) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", model.getId())
                    .addValue("iso_id", model.getIso_id())
                    .addValue("status", model.getStatus());

            return parameterJdbcTemplate.update(UPDATE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public Integer remove(String id) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id);
            rtrRepository.removeBySuper(id);
            return parameterJdbcTemplate.update(REMOVE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

}
