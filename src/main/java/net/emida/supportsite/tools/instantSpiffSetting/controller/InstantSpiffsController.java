package net.emida.supportsite.tools.instantSpiffSetting.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiff;
import net.emida.supportsite.tools.instantSpiffSetting.repository.spiff.InstanSpiffDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.INSTANT_SPIFF_SETTING_SPIFFS)
public class InstantSpiffsController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(InstantSpiffsController.class);
    @Autowired
    InstanSpiffDAO repository;

    @RequestMapping(value = "/filterBy", method = RequestMethod.GET)
    @ResponseBody
    List<InstantSpiff> filterBy(@RequestParam("id_super") String id_super, HttpServletRequest request, HttpSession httpSession) {
        try {
            InstantSpiff model = new InstantSpiff();
            model.setId_super(id_super);
            return repository.filterBy(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new ArrayList<>();
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    @ResponseBody
    InstantSpiff getById(@RequestParam("id") String id, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.getById(id);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new InstantSpiff();
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    Integer add(@RequestBody InstantSpiff model, HttpServletRequest request, HttpSession httpSession) {
        try {            
            return repository.add(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    Integer update(@RequestBody InstantSpiff model, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.update(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    @ResponseBody
    Integer remove(@RequestParam("id") String id, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.remove(id);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 47;
    }

}
