package net.emida.supportsite.tools.instantSpiffSetting.model;

import java.sql.Timestamp;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
public class InstantSpiffModel {

    private String id;
    private Long iso_id;
    private Boolean status;
    private Timestamp update_dt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIso_id() {
        return iso_id;
    }

    public void setIso_id(Long iso_id) {
        this.iso_id = iso_id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getUpdate_dt() {
        return update_dt;
    }

    public void setUpdate_dt(Timestamp update_dt) {
        this.update_dt = update_dt;
    }

}
