package net.emida.supportsite.tools.instantSpiffSetting.model;

import java.util.List;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
public class InstantSpiffRtr extends InstantSpiffProductBase {

    private List<InstantSpiff> spiffsProducts;
    private List<InstantSpiffActivate> activateProducts;

    public List<InstantSpiff> getSpiffsProducts() {
        return spiffsProducts;
    }

    public void setSpiffsProducts(List<InstantSpiff> spiffsProducts) {
        this.spiffsProducts = spiffsProducts;
    }

    public List<InstantSpiffActivate> getActivateProducts() {
        return activateProducts;
    }

    public void setActivateProducts(List<InstantSpiffActivate> activateProducts) {
        this.activateProducts = activateProducts;
    }

}
