package net.emida.supportsite.tools.instantSpiffSetting.repository.rtr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiff;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffActivate;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffRtr;
import net.emida.supportsite.tools.instantSpiffSetting.repository.spiff.InstanSpiffDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import net.emida.supportsite.tools.instantSpiffSetting.repository.activate.InstantSpiffActivateDAO;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
@Repository
public class InstantSpiffRtrRepository implements InstantSpiffRtrDAO {

    private static final Logger LOG = Logger.getLogger(InstantSpiffRtrRepository.class);

    private static final String ALL = "SELECT * FROM IS_Setting_RTR b WITH(NOLOCK) WHERE b.entity_id = :entity_id";
    private static final String GET_BY_ID = "SELECT * FROM IS_Setting_RTR b WITH(NOLOCK) WHERE b.id = :id";
    private static final String ADD = "INSERT INTO IS_Setting_RTR(id, id_super, product_id, name, status, update_dt) "
            + "                               VALUES(:id, :id_super, :product_id, :name, :status, GETDATE())";
    private static final String UPDATE = "UPDATE IS_Setting_RTR SET status = :status WHERE id = :id";
    private static final String REMOVE = "DELETE FROM IS_Setting_RTR WHERE id = :id";
    private static final String REMOVE_BY_SUPER = "DELETE FROM IS_Setting_RTR WHERE id_super = :id_super";
    private static final String GET_BY_SUPER = "SELECT * FROM IS_Setting_RTR WHERE id_super = :id_super";

    @Autowired
    @Qualifier("masterNamedParamJdbcTemplate")
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    @Autowired
    private InstantSpiffActivateDAO activateRepository;

    @Autowired
    private InstanSpiffDAO instanSpiffRepository;

    @Override
    public Integer add(InstantSpiffRtr model) {
        try {
            String id = UUID.randomUUID().toString().toUpperCase();
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id)
                    .addValue("id_super", model.getId_super())
                    .addValue("product_id", model.getProduct_id())
                    .addValue("name", model.getName())
                    .addValue("status", model.getStatus());

            List<InstantSpiffActivate> activateProducts = model.getActivateProducts();
            for (InstantSpiffActivate activateProduct : activateProducts) {
                activateProduct.setId_super(id);
                activateRepository.add(activateProduct);
            }

            List<InstantSpiff> spiffsProducts = model.getSpiffsProducts();
            for (InstantSpiff spiffsProduct : spiffsProducts) {
                spiffsProduct.setId_super(id);
                instanSpiffRepository.add(spiffsProduct);
            }

            return parameterJdbcTemplate.update(ADD, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public List<InstantSpiffRtr> filterBy(InstantSpiffRtr model) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id_super", model.getId_super());
            return parameterJdbcTemplate.query(GET_BY_SUPER, parameters, new BeanPropertyRowMapper(InstantSpiffRtr.class));
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new ArrayList<>();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new ArrayList<>();
        } catch (Exception ex) {
            LOG.error(ex);
            return new ArrayList<>();
        }
    }

    @Override
    public InstantSpiffRtr getById(String id) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id);
            return parameterJdbcTemplate.queryForObject(GET_BY_ID, parameters, new RowMapper<InstantSpiffRtr>() {
                @Override
                public InstantSpiffRtr mapRow(ResultSet resultSet, int index) throws SQLException {
                    InstantSpiffRtr model = new InstantSpiffRtr();
                    /*
                    model.setId(RSUtil.getString(resultSet, "id"));               
                     */
                    return model;
                }
            });
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new InstantSpiffRtr();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new InstantSpiffRtr();
        } catch (Exception ex) {
            LOG.error(ex);
            return new InstantSpiffRtr();
        }
    }

    @Override
    public Integer update(InstantSpiffRtr model) {
        try {
            if (model.getStatus()) {
                model.setStatus(Boolean.FALSE);
            } else {
                model.setStatus(Boolean.TRUE);
            }
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", model.getId())
                    .addValue("status", model.getStatus());
            return parameterJdbcTemplate.update(UPDATE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public Integer remove(String id) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id);
            activateRepository.removeBySuper(id, Boolean.FALSE);
            instanSpiffRepository.removeBySuper(id, Boolean.FALSE);
            return parameterJdbcTemplate.update(REMOVE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public Integer removeBySuper(String id_super) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id_super", id_super);
            activateRepository.removeBySuper(id_super, Boolean.TRUE);
            instanSpiffRepository.removeBySuper(id_super, Boolean.TRUE);
            return parameterJdbcTemplate.update(REMOVE_BY_SUPER, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

}
