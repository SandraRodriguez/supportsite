package net.emida.supportsite.tools.instantSpiffSetting.repository.spiff;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiff;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
@Repository
public class InstanSpiffRepository implements InstanSpiffDAO {

    private static final Logger LOG = Logger.getLogger(InstanSpiffRepository.class);

    private static final String ALL = "SELECT * FROM BillingStatementBrandInfo b WITH(NOLOCK) WHERE b.entity_id = :entity_id";
    private static final String GET_BY_ID = "SELECT * FROM IS_Setting_Spiff b WITH(NOLOCK) WHERE b.id = :id";
    private static final String ADD = "INSERT INTO IS_Setting_Spiff(id_super, product_id, name, day_to_start_pay, day_to_end_pay) "
            + "                               VALUES(:id_super, :product_id, :name, :day_to_start_pay, :day_to_end_pay)";
    private static final String UPDATE = "UPDATE BillingStatementBrandInfo SET entity_id = :entity_id, entity_type_id = :entity_type_id, header_image_id = :header_image_id, "
            + "                               title = :title, footer_image_id = :footer_image_id,"
            + "                               footer_line1 = :footer_line1, footer_line2 = :footer_line2, image_line = :image_line WHERE id = :id";
    private static final String REMOVE = "DELETE FROM IS_Setting_Spiff WHERE id = :id";
    private static final String GET_BY_SUPER_ID = "SELECT * FROM IS_Setting_Spiff a WITH(NOLOCK) WHERE a.id_super = :id_super";
    private static final String REMOVE_BY_SUPER = "DELETE FROM IS_Setting_Spiff WHERE id_super = :id";
    private static final String REMOVE_BY_SUPER_FULL = "DELETE FROM IS_Setting_Spiff WHERE id IN(SELECT s.id FROM IS_Setting_Spiff s WITH (NOLOCK) INNER JOIN IS_Setting_RTR r WITH (NOLOCK) ON r.id = s.id_super WHERE r.id_super = :id)";

    @Autowired
    @Qualifier("masterNamedParamJdbcTemplate")
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    @Override
    public Integer add(InstantSpiff model) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id_super", model.getId_super())
                    .addValue("product_id", model.getProduct_id())
                    .addValue("name", model.getName())
                    .addValue("day_to_start_pay", model.getDay_to_start_pay())
                    .addValue("day_to_end_pay", model.getDay_to_end_pay());
            return parameterJdbcTemplate.update(ADD, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public List<InstantSpiff> filterBy(InstantSpiff model) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id_super", model.getId_super());
            return parameterJdbcTemplate.query(GET_BY_SUPER_ID, parameters, new BeanPropertyRowMapper(InstantSpiff.class));
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new ArrayList<>();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new ArrayList<>();
        } catch (Exception ex) {
            LOG.error(ex);
            return new ArrayList<>();
        }
    }

    @Override
    public InstantSpiff getById(String id) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id);
            return parameterJdbcTemplate.queryForObject(GET_BY_ID, parameters, new RowMapper<InstantSpiff>() {
                @Override
                public InstantSpiff mapRow(ResultSet rs, int index) throws SQLException {
                    InstantSpiff model = new InstantSpiff();
                    model.setId(rs.getString("id"));
                    model.setId_super(rs.getString("id_super"));
                    model.setProduct_id(rs.getLong("product_id"));
                    model.setName(rs.getString("name"));
                    model.setDay_to_start_pay(rs.getInt("day_to_start_pay"));
                    model.setDay_to_end_pay(rs.getInt("day_to_end_pay"));
                    return model;
                }
            });
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new InstantSpiff();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new InstantSpiff();
        } catch (Exception ex) {
            LOG.error(ex);
            return new InstantSpiff();
        }
    }

    @Override
    public Integer update(InstantSpiff model) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", model.getId())
                    .addValue("product_id", model.getProduct_id())
                    .addValue("name", model.getName())
                    .addValue("day_to_start_pay", model.getDay_to_start_pay())
                    .addValue("day_to_end_pay", model.getDay_to_end_pay());
            return parameterJdbcTemplate.update(UPDATE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public Integer remove(String id) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id);
            return parameterJdbcTemplate.update(REMOVE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

    @Override
    public Integer removeBySuper(String id_super, Boolean fullCascade) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("id", id_super);
            String query = REMOVE_BY_SUPER;
            if (fullCascade) {
                query = REMOVE_BY_SUPER_FULL;
            }
            return parameterJdbcTemplate.update(query, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }

}
