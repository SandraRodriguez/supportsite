package net.emida.supportsite.tools.instantSpiffSetting.model;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
public class AditionalParams {

    private Long key;
    private String value;

    public Long getKey() {
        return key;
    }

    public void setKey(Long key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
