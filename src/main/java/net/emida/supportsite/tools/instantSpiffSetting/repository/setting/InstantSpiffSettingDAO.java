package net.emida.supportsite.tools.instantSpiffSetting.repository.setting;

import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffSetting;
import net.emida.supportsite.tools.instantSpiffSetting.repository.ISCommonsSettingDAO;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
public interface InstantSpiffSettingDAO extends ISCommonsSettingDAO<InstantSpiffSetting> {

}
