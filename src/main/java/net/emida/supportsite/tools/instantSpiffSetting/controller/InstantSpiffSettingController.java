package net.emida.supportsite.tools.instantSpiffSetting.controller;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.instantSpiffSetting.model.AditionalParams;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffProductBase;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import net.emida.supportsite.tools.instantSpiffSetting.repository.commons.AditionalParamsDAO;
import net.emida.supportsite.tools.instantSpiffSetting.repository.setting.InstantSpiffSettingDAO;
import org.springframework.http.MediaType;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.INSTANT_SPIFF_SETTING)
public class InstantSpiffSettingController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(InstantSpiffSettingController.class);

    @Autowired
    InstantSpiffSettingDAO repository;

    @Autowired
    AditionalParamsDAO aditionalParamsRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String __init__() {
        return "tools/instantSpiffSetting/index";
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    List<InstantSpiffSetting> getAll(HttpServletRequest request, HttpSession httpSession) {
        try {
            InstantSpiffSetting model = new InstantSpiffSetting();
            return repository.filterBy(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new ArrayList<>();
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getById", method = RequestMethod.GET)
    @ResponseBody
    InstantSpiffSetting getById(@RequestParam("id") String id, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.getById(id);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new InstantSpiffSetting();
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    Integer add(@RequestBody InstantSpiffSetting model, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.add(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseBody
    Integer update(@RequestBody InstantSpiffSetting model, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.update(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    @ResponseBody
    Integer remove(@RequestParam("id") String id, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.remove(id);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @RequestMapping(value = "/getIsos", method = RequestMethod.GET)
    @ResponseBody
    List<AditionalParams> getIsos(
            @RequestParam("isoId") Long isoId,
            @RequestParam("isEdit") Boolean isEdit,
            HttpServletRequest request, HttpSession httpSession) {
        try {
            return aditionalParamsRepository.getIsos(isEdit, isoId);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new ArrayList<>();
        }
    }

    @RequestMapping(value = "/findProductsBy", method = RequestMethod.GET)
    @ResponseBody
    List<InstantSpiffProductBase> findProductsBy(@RequestParam("transType") Integer transType, HttpServletRequest request, HttpSession httpSession) {
        try {
            return aditionalParamsRepository.getProductsBy(transType);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new ArrayList<>();
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/labels", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    String labels(Locale locale, HttpServletRequest request, HttpSession httpSession) {
        JsonObject object = new JsonObject();
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");

        object.add("lblNext", new JsonPrimitive(Languages.getString("jsp.admin.genericLabel.next", sessionData.getLanguage())));
        object.add("lblSave", new JsonPrimitive(Languages.getString("jsp.admin.genericLabel.Save", sessionData.getLanguage())));
        object.add("okMessage", new JsonPrimitive(Languages.getString("instant.spiff.main.lbl.add.ok", sessionData.getLanguage())));        
        object.add("rmWarning", new JsonPrimitive(Languages.getString("instant.spiff.main.lbl.warn.rm", sessionData.getLanguage())));
        object.add("rmWarningSubtitle", new JsonPrimitive(Languages.getString("instant.spiff.main.lbl.warn.rm.subtitle", sessionData.getLanguage())));

        return object.toString();
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 47;
    }

}
