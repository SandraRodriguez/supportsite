package net.emida.supportsite.tools.instantSpiffSetting.repository.commons;

import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.tools.instantSpiffSetting.model.AditionalParams;
import net.emida.supportsite.tools.instantSpiffSetting.model.InstantSpiffProductBase;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5628
 * @since 2019-23-07
 * @version 2019-I
 */
@Repository
public class AditionalParamsRepository implements AditionalParamsDAO {

    private static final Logger LOG = Logger.getLogger(AditionalParamsRepository.class);
    private static final String FIND_PRODUCT_BY = "SELECT p.id AS 'product_id', p.description AS 'name', 1 AS 'status' FROM dbo.products p WITH(nolock) WHERE p.Trans_Type = :trans_type and disabled = 0";
    private static final String FIND_ACTIVATIONS_PRODUCTS = "SELECT p.id AS 'product_id', p.description AS 'name', 1 AS 'status' FROM dbo.products p WITH(nolock) WHERE p.Trans_Type = 20 OR p.Trans_Type = 25 and disabled = 0";
    private static final String FIND_RTR = "SELECT * FROM dbo.IS_Setting_RTR WHERE id_super = id_super";

    private static final String ISOS_EXCLUDE = "SELECT r.rep_id AS 'key', r.businessname AS 'value' FROM dbo.reps r WITH(nolock) WHERE r.type = 2 OR r.type = 3 AND r.rep_id NOT IN (SELECT iso_id FROM IS_Setting)";
    private static final String ISOS_INCLUDE = "SELECT r.rep_id AS 'key', r.businessname AS 'value' FROM dbo.reps r WITH(nolock) WHERE r.type = 2 OR r.type = 3 AND r.rep_id NOT IN (SELECT iso_id FROM IS_Setting WHERE iso_id <> :iso_id)";

    @Autowired
    @Qualifier("masterNamedParamJdbcTemplate")
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    @Override
    public List<InstantSpiffProductBase> getProductsBy(Integer transType) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("trans_type", transType);
            if (transType == 20) {
                return parameterJdbcTemplate.query(FIND_ACTIVATIONS_PRODUCTS, parameters, new BeanPropertyRowMapper(InstantSpiffProductBase.class));
            }
            return parameterJdbcTemplate.query(FIND_PRODUCT_BY, parameters, new BeanPropertyRowMapper(InstantSpiffProductBase.class));
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new ArrayList<>();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new ArrayList<>();
        } catch (Exception ex) {
            LOG.error(ex);
            return new ArrayList<>();
        }
    }

    @Override
    public List<AditionalParams> getIsos(Boolean isEdit, Long iso_id) {
        try {
            String query = ISOS_EXCLUDE;
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("iso_id", iso_id);
            if (isEdit) {
                query = ISOS_INCLUDE;
            }
            return parameterJdbcTemplate.query(query, parameters, new BeanPropertyRowMapper(AditionalParams.class));
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new ArrayList<>();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new ArrayList<>();
        } catch (Exception ex) {
            LOG.error(ex);
            return new ArrayList<>();
        }
    }

    @Override
    public List<InstantSpiffProductBase> filter(String idSuper) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource();
            return parameterJdbcTemplate.query(FIND_RTR, parameters, new BeanPropertyRowMapper(InstantSpiffProductBase.class));
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new ArrayList<>();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new ArrayList<>();
        } catch (Exception ex) {
            LOG.error(ex);
            return new ArrayList<>();
        }
    }

}
