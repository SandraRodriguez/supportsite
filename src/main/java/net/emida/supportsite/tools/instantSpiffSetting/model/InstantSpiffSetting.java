package net.emida.supportsite.tools.instantSpiffSetting.model;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
public class InstantSpiffSetting {

    private String id;
    private Long iso_id;
    private String iso_name;
    private Boolean status;
    private Timestamp update_dt;
    private List<InstantSpiffRtr> rtrs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIso_id() {
        return iso_id;
    }

    public void setIso_id(Long iso_id) {
        this.iso_id = iso_id;
    }

    public String getIso_name() {
        return iso_name;
    }

    public void setIso_name(String iso_name) {
        this.iso_name = iso_name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getUpdate_dt() {
        return update_dt;
    }

    public void setUpdate_dt(Timestamp update_dt) {
        this.update_dt = update_dt;
    }

    public List<InstantSpiffRtr> getRtrs() {
        return rtrs;
    }

    public void setRtrs(List<InstantSpiffRtr> rtrs) {
        this.rtrs = rtrs;
    }

}
