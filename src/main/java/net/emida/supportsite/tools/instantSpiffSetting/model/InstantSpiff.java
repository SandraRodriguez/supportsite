package net.emida.supportsite.tools.instantSpiffSetting.model;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
public class InstantSpiff extends InstantSpiffProductBase {

    private String id;
    private String id_super;
    private Integer day_to_start_pay;
    private Integer day_to_end_pay;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_super() {
        return id_super;
    }

    public void setId_super(String id_super) {
        this.id_super = id_super;
    }

    public Integer getDay_to_start_pay() {
        return day_to_start_pay;
    }

    public void setDay_to_start_pay(Integer day_to_start_pay) {
        this.day_to_start_pay = day_to_start_pay;
    }

    public Integer getDay_to_end_pay() {
        return day_to_end_pay;
    }

    public void setDay_to_end_pay(Integer day_to_end_pay) {
        this.day_to_end_pay = day_to_end_pay;
    }

}
