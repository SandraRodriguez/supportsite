package net.emida.supportsite.tools.paymentProcessor.dto;

/**
 *
 * @author janez
 */
public class PaymentProcessorParameter {

    private String id;
    private String platformId;
    private String name;
    private String description;
    private String value;

    public PaymentProcessorParameter() {
    }

    /**
     *
     * @param platformId
     * @param name
     * @param description
     * @param value
     */
    public PaymentProcessorParameter(String platformId, String name, String description, String value) {
        this.platformId = platformId;
        this.name = name;
        this.description = description;
        this.value = value;
    }

    /**
     *
     * @param id
     * @param platformId
     * @param name
     * @param description
     * @param value
     */
    public PaymentProcessorParameter(String id, String platformId, String name, String description, String value) {
        this.id = id;
        this.platformId = platformId;
        this.name = name;
        this.description = description;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlatformId() {
        return platformId;
    }

    public void setPlatformId(String platformId) {
        this.platformId = platformId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
