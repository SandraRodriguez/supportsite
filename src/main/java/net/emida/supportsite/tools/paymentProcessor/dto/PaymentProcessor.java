package net.emida.supportsite.tools.paymentProcessor.dto;

import java.util.List;

/**
 *
 * @author janez
 */
public class PaymentProcessor {

    private String id;
    private String platform;
    private String fee;
    private String feeType;
    private String amountsAllowed;
    private String status;
    private boolean isDefault;
    private Long entityId;
    private List<PaymentProcessorParameter> params;

    public PaymentProcessor() {
    }

    public PaymentProcessor(String id, String platform, String fee, String feeType, String amountsAllowed, String status, boolean isDefault, Long entityId) {
        this.id = id;
        this.platform = platform;
        this.fee = fee;
        this.feeType = feeType;
        this.amountsAllowed = amountsAllowed;
        this.status = status;
        this.isDefault = isDefault;
        this.entityId = entityId;
    }

    /**
     *
     * @param platform
     * @param fee
     * @param feeType
     * @param amountsAllowed
     * @param status
     * @param isDefault
     */
    public PaymentProcessor(String platform, String fee, String feeType, String amountsAllowed, String status, boolean isDefault, boolean process) {
        this.platform = platform;
        this.fee = fee;
        this.feeType = feeType;
        this.amountsAllowed = amountsAllowed;
        this.status = status;
        this.isDefault = isDefault;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getAmountsAllowed() {
        return amountsAllowed;
    }

    public void setAmountsAllowed(String amountsAllowed) {
        this.amountsAllowed = amountsAllowed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public List<PaymentProcessorParameter> getParams() {
        return params;
    }

    public void setParams(List<PaymentProcessorParameter> params) {
        this.params = params;
    }

}
