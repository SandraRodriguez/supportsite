package net.emida.supportsite.tools.paymentProcessor.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.emida.supportsite.tools.paymentProcessor.dto.PaymentProcessorParameter;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez
 */
@Repository
public class PaymentProcessorParameterImpl implements PaymentProcessorParameterDAO {

    private static final String FIND_PARAMETERS_BY = "SELECT id, platformid, name, description, value  FROM PaymentProcessorParameter WITH(NOLOCK) WHERE platformid = ?";
    private static final String INSERT = "INSERT INTO PaymentProcessorParameter (id, platformId, Name, Description, Value) VALUES (?, ?, ?, ?, ?)";
    private static final String DELETE = "DELETE FROM PaymentProcessorParameter WHERE id = ?";
    private static final String UPDATE = "UPDATE PaymentProcessorParameter SET name = ?, description = ?, value = ?  WHERE id = ?";
    private static final Logger LOG = Logger.getLogger(PaymentProcessorParameterImpl.class.getName());

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<PaymentProcessorParameter> findBy(String id) throws TechnicalException {
        LOG.log(Level.INFO, "platform::id::{0}", id);
        List<PaymentProcessorParameter> paymentProcessorParameters = this.jdbcTemplate.query(FIND_PARAMETERS_BY, new Object[]{
            id
        }, new RowMapper<PaymentProcessorParameter>() {
            @Override
            public PaymentProcessorParameter mapRow(ResultSet rs, int i) throws SQLException {
                PaymentProcessorParameter paymentProcessor = new PaymentProcessorParameter(
                        rs.getString("id"),
                        rs.getString("platformid"),
                        rs.getString("name"),
                        rs.getString("description"),
                        rs.getString("value"));
                return paymentProcessor;
            }
        });

        return paymentProcessorParameters;
    }

    @Override
    public ResponseEntity<PaymentProcessorParameter> add(PaymentProcessorParameter model) throws TechnicalException {
        if (model != null) {
            this.jdbcTemplate.update(INSERT, new Object[]{
                UUID.randomUUID().toString().toUpperCase(),
                model.getPlatformId(),
                model.getName(),
                model.getDescription(),
                model.getValue()
            });
            return ResponseEntity.status(HttpStatus.OK).body(model);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(model);
    }

    @Override
    public ResponseEntity<Integer> update(PaymentProcessorParameter model) throws TechnicalException {
        int rowUpdate = 0;
        if (model != null) {
            rowUpdate = this.jdbcTemplate.update(UPDATE, new Object[]{
                model.getName(),
                model.getDescription(),
                model.getValue(),
                model.getId()
            });
            if (rowUpdate != 0) {
                return ResponseEntity.status(HttpStatus.OK).body(rowUpdate);
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rowUpdate);
    }

    @Override
    public ResponseEntity<Integer> delete(String id) throws TechnicalException {
        int rowDeleted = 0;
        if (id != null) {
            rowDeleted = this.jdbcTemplate.update(DELETE, new Object[]{
                id
            });
            if (rowDeleted != 0) {
                return ResponseEntity.status(HttpStatus.OK).body(rowDeleted);
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(rowDeleted);
    }

}
