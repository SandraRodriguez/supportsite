package net.emida.supportsite.tools.paymentProcessor.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import net.emida.supportsite.tools.paymentProcessor.dto.PaymentProcessor;
import net.emida.supportsite.tools.paymentProcessor.dto.PaymentProcessorParameter;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez
 */
@Repository
public class PaymentProcessorImpl implements PaymentProcessorDAO {

    private static final String FIND_PROCESSOR_BY = "SELECT * FROM PaymentProcessor WITH(NOLOCK) WHERE entityId = ?";
    private static final String ADD = "INSERT INTO PaymentProcessor (id, Platform, Fee, FeeType, amountsAllowed, State, isDefault, entityId) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String FIND_DEFAULT = "SELECT * FROM PaymentProcessor WITH(NOLOCK) WHERE isDefault = 1";
    private static final String UPDATE_DEFAULT = "UPDATE PaymentProcessor set isDefault = 0 where id = ?";
    private static final String FIND_BY_ID = "SELECT * FROM PaymentProcessor WITH(NOLOCK) WHERE id = ?";
    private static final String DELETE_PARAMETERS = "DELETE FROM PaymentProcessorParameter WHERE platformid = ?";
    private static final String DELETE_PROCESSOR = "DELETE FROM PaymentProcessor WHERE id = ?";
    private static final String UPDATE_PROCESSOR = "UPDATE PaymentProcessor set fee = ?, feeType = ?, amountsAllowed = ?, state = ?, isDefault = ? where id = ?";

    private static final String ERROR_MESSAGE_NOT_FOUNT_DEFAULT_PAYMENT_PROCESSOR = "ERROR:: Not found default payment processor.";
    private static final Logger LOG = Logger.getLogger(PaymentProcessorImpl.class.getName());

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    PaymentProcessorParameterDAO paramsDAO;

    @Override
    public List<PaymentProcessor> getPaymentsProcessors(Long entityId) throws TechnicalException {
        List<PaymentProcessor> paymentProcessor = this.jdbcTemplate.query(
                FIND_PROCESSOR_BY, new Object[]{
                    entityId
                },
                new RowMapper<PaymentProcessor>() {
            @Override
            public PaymentProcessor mapRow(ResultSet rs, int rowNum) throws SQLException {
                PaymentProcessor paymentProcessor = new PaymentProcessor(
                        rs.getString("id"),
                        rs.getString("platform"),
                        rs.getString("fee"),
                        rs.getString("feetype"),
                        rs.getString("amountsAllowed"),
                        rs.getString("state"),
                        (rs.getByte("isDefault") == 1),
                        rs.getLong("entityId"));
                return paymentProcessor;
            }
        });
        return paymentProcessor;
    }

    @Override
    public ResponseEntity<PaymentProcessor> addPaymentProcessor(PaymentProcessor model) throws TechnicalException {
        if (model != null) {
            if (model.isIsDefault()) {
                PaymentProcessor defaultPprocessor;
                try {
                    defaultPprocessor = this.getDefaultPaymentProcessor();
                    if (defaultPprocessor != null) {
                        String id = defaultPprocessor.getId();
                        this.jdbcTemplate.update(UPDATE_DEFAULT, new Object[]{
                            id
                        });
                    }
                } catch (org.springframework.dao.EmptyResultDataAccessException emptyResultDataAccessException) {
                    LOG.info(ERROR_MESSAGE_NOT_FOUNT_DEFAULT_PAYMENT_PROCESSOR);
                }
            }

            String id = UUID.randomUUID().toString().toUpperCase();

            this.jdbcTemplate.update(ADD, new Object[]{
                id,
                model.getPlatform(),
                model.getFee(),
                model.getFeeType(),
                (model.getAmountsAllowed() != null ? model.getAmountsAllowed() : "0"),
                (model.getStatus() == null ? "INACTIVE" : "ACTIVE"),
                model.isIsDefault(),
                model.getEntityId()

            });

            List<PaymentProcessorParameter> params = model.getParams();
            for (PaymentProcessorParameter param : params) {
                param.setPlatformId(id);
                paramsDAO.add(param);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(model);
    }

    @Override
    public PaymentProcessor getDefaultPaymentProcessor() throws org.springframework.dao.EmptyResultDataAccessException {
        PaymentProcessor paymentProcessor = (PaymentProcessor) this.jdbcTemplate.queryForObject(FIND_DEFAULT, new RowMapper<PaymentProcessor>() {
            @Override
            public PaymentProcessor mapRow(ResultSet rs, int i) throws SQLException {
                PaymentProcessor paymentProcessor = new PaymentProcessor();
                paymentProcessor.setId(rs.getString("id"));
                return paymentProcessor;
            }
        });
        return paymentProcessor;
    }

    @Override
    public PaymentProcessor getPaymentProcessorById(String id) throws EmptyResultDataAccessException {
        PaymentProcessor paymentProcessor = (PaymentProcessor) this.jdbcTemplate.queryForObject(FIND_BY_ID, new Object[]{
            id
        }, new RowMapper<PaymentProcessor>() {
            @Override
            public PaymentProcessor mapRow(ResultSet rs, int i) throws SQLException {
                PaymentProcessor paymentProcessor = new PaymentProcessor(
                        rs.getString("id"),
                        rs.getString("platform"),
                        rs.getString("fee"),
                        rs.getString("feetype"),
                        rs.getString("amountsAllowed"),
                        rs.getString("state"),
                        (rs.getByte("isDefault") == 1),
                        rs.getLong("entityId"));
                return paymentProcessor;
            }
        });
        return paymentProcessor;
    }

    @Override
    public ResponseEntity<String> deletePaymentProcessorByPlatform(String platform) throws TechnicalException {
        int rowDeleted;
        if (platform != null) {
            rowDeleted = this.jdbcTemplate.update(DELETE_PARAMETERS, new Object[]{
                platform
            });
            rowDeleted = this.jdbcTemplate.update(DELETE_PROCESSOR, new Object[]{
                platform
            });
            if (rowDeleted != 0) {
                return ResponseEntity.status(HttpStatus.OK).body(platform);
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(platform);
    }

    @Override
    public ResponseEntity<Integer> updatePaymentProcessorByPlatform(PaymentProcessor paymentProcessor) throws TechnicalException {
        int rowUpdated = 0;
        if (paymentProcessor != null) {
            if (paymentProcessor.isIsDefault()) {
                PaymentProcessor defaultPprocessor;
                try {
                    defaultPprocessor = this.getDefaultPaymentProcessor();
                    if (defaultPprocessor != null) {
                        String id = defaultPprocessor.getId();
                        this.jdbcTemplate.update(UPDATE_DEFAULT, new Object[]{
                            id
                        });
                    }
                } catch (org.springframework.dao.EmptyResultDataAccessException emptyResultDataAccessException) {
                    LOG.info(ERROR_MESSAGE_NOT_FOUNT_DEFAULT_PAYMENT_PROCESSOR);
                }
            }

            rowUpdated = this.jdbcTemplate.update(UPDATE_PROCESSOR, new Object[]{
                paymentProcessor.getFee(),
                paymentProcessor.getFeeType(),
                paymentProcessor.getAmountsAllowed(),
                (paymentProcessor.getStatus() == null ? "INACTIVE" : "ACTIVE"),
                paymentProcessor.isIsDefault(),
                paymentProcessor.getId()
            });
        }
        return ResponseEntity.status(HttpStatus.OK).body(rowUpdated);

    }
}
