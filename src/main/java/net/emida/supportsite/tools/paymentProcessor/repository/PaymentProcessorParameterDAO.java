package net.emida.supportsite.tools.paymentProcessor.repository;

import java.util.List;
import net.emida.supportsite.tools.paymentProcessor.dto.PaymentProcessorParameter;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author janez
 */
public interface PaymentProcessorParameterDAO {

    /**
     *
     * @param id
     * @return
     * @throws TechnicalException
     */
    List<PaymentProcessorParameter> findBy(String id) throws TechnicalException;

    /**
     *
     * @param model
     * @return
     * @throws TechnicalException
     */
    ResponseEntity<PaymentProcessorParameter> add(PaymentProcessorParameter model) throws TechnicalException;

    /**
     *
     * @param model
     * @return
     * @throws TechnicalException
     */
    ResponseEntity<Integer> update(PaymentProcessorParameter model) throws TechnicalException;

    /**
     *
     * @param id
     * @return
     * @throws TechnicalException
     */
    ResponseEntity<Integer> delete(String id) throws TechnicalException;

}
