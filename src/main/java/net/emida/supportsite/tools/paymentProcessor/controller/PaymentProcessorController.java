package net.emida.supportsite.tools.paymentProcessor.controller;

import net.emida.supportsite.tools.paymentProcessor.dto.ParameterListWrapper;
import com.debisys.users.SessionData;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.paymentProcessor.dto.PaymentProcessor;
import net.emida.supportsite.tools.paymentProcessor.dto.PaymentProcessorParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import net.emida.supportsite.tools.paymentProcessor.repository.PaymentProcessorDAO;
import net.emida.supportsite.tools.paymentProcessor.repository.PaymentProcessorParameterDAO;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez
 */
@Controller
@RequestMapping(value = UrlPaths.ADMIN_PATH)
public class PaymentProcessorController extends BaseController {

    private static final Logger LOG = Logger.getLogger(PaymentProcessorController.class.getName());
    private static final String FORM_PATH = "admin/tools/paymentOption/paymentOptions";
    private static final String ADD_PAYMENTS_FORM_PATH = "admin/tools/paymentOption/addPaymentProcessor";
    private static final String EDIT_PAYMENTS_FORM_PATH = "admin/tools/paymentOption/editPaymentProcessor";

    @Autowired
    PaymentProcessorDAO dao;

    @Autowired
    PaymentProcessorParameterDAO parametersDAO;

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS, method = RequestMethod.GET)
    String getAllPaymentsProcessors(Map model, HttpSession httpSession) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        Long entityId = new Long(sessionData.getProperty("ref_id"));
        model.put("processorList", dao.getPaymentsProcessors(entityId));
        return FORM_PATH;
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS + UrlPaths.ADD_PAYMENTS_OPTIONS, method = RequestMethod.GET)
    String showAddPaymentProcessor(@RequestParam("message") String message, ModelMap modelMap) {
        modelMap.addAttribute("message", message);
        return ADD_PAYMENTS_FORM_PATH;
    }

    @RequestMapping(value = UrlPaths.PAYMENTS_OPTIONS + UrlPaths.ADD_PAYMENTS_OPTIONS, method = RequestMethod.POST)
    @ResponseBody
    Integer addPaymentProcessor(@RequestBody PaymentProcessor model, ModelMap modelMap,
            RedirectAttributes attr, HttpSession httpSession) {

        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        Long entityId = new Long(sessionData.getProperty("ref_id"));

        List<PaymentProcessor> paymentProcessors = dao.getPaymentsProcessors(entityId);
        boolean confirm = matcherPlatformName(paymentProcessors, model.getPlatform());
        if (!confirm) {
            model.setEntityId(entityId);

            dao.addPaymentProcessor(model);
            return 1;
        }
        return 0;
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS + UrlPaths.EDIT_PAYMENTS_PROCESSOR + "/{id}", method = RequestMethod.GET)
    String showPaymentProcessorById(@PathVariable String id, Model model) {
        LOG.log(Level.INFO, "GET::Identity::{0}", id);
        PaymentProcessor paymentProcessor = dao.getPaymentProcessorById(id);

        ParameterListWrapper parameterListWrapper = new ParameterListWrapper();
        parameterListWrapper.setParametersList(parametersDAO.findBy(paymentProcessor.getId()));

        model.addAttribute("paymentProcessor", paymentProcessor);
        model.addAttribute("parameterListWrapper", parameterListWrapper);
        model.addAttribute("parametersList", parameterListWrapper.getParametersList());

        return EDIT_PAYMENTS_FORM_PATH;
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS + UrlPaths.EDIT_PAYMENTS_PROCESSOR + "/{id}", method = RequestMethod.POST)
    String editPaymentProcessorParameters(@PathVariable String id, @ModelAttribute("parameterListWrapper") ParameterListWrapper parameterListWrapper,
            @ModelAttribute("paymentProcessor") PaymentProcessor paymentProcessor, Map model) {
        LOG.info("executed::editPaymentProcessorParameters");
        PaymentProcessor paymentProcessr = dao.getPaymentProcessorById(id);

        if (paymentProcessor != null) {
            paymentProcessor.setId(id);
            dao.updatePaymentProcessorByPlatform(paymentProcessor);
        }

        model.put("paymentProcessor", paymentProcessr);
        model.put("paymentProcessorParameters", parametersDAO.findBy(paymentProcessr.getId()));

        try {
            for (PaymentProcessorParameter paymentProcessorParametr : parameterListWrapper.getParametersList()) {
                paymentProcessorParametr.setPlatformId(paymentProcessr.getId());
                if (!(paymentProcessorParametr.getName().length() <= 0 || paymentProcessorParametr.getValue().length() <= 0)) {
                    parametersDAO.add(paymentProcessorParametr);
                }
            }
        } catch (NullPointerException nullPointerException) {
            LOG.info(nullPointerException.getLocalizedMessage());
        }
        return "redirect:/admin/tools/paymentOption/editPaymentProcessor/" + id;
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS + UrlPaths.DELETE_PAYMENTS_PROCESSOR, method = RequestMethod.POST)
    String deletePaymentProcessorByPlatform(@RequestParam("platform") String platform, Map model) {
        LOG.log(Level.INFO, "DELETE::deletePaymentProcessorById::{0}", platform);

        dao.deletePaymentProcessorByPlatform(platform);

        return EDIT_PAYMENTS_FORM_PATH;
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS + UrlPaths.DELETE_PAYMENTS_PROCESSOR_PARAMETER, method = RequestMethod.POST)
    String deletePaymentProcessorParameterById(@RequestParam("id") String id, @RequestParam("idplatform") String idplatform, Map model) {
        LOG.log(Level.INFO, "DELETE::deletePaymentProcessorParameterById::{0}", id);

        parametersDAO.delete(id);

        PaymentProcessor paymentProcessor = dao.getPaymentProcessorById(idplatform);

        model.put("paymentProcessor", paymentProcessor);
        model.put("paymentProcessorParameters", parametersDAO.findBy(paymentProcessor.getId()));

        return "redirect:/admin/tools/paymentOption/editPaymentProcessor/" + idplatform;
    }

    @RequestMapping(value = UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS + UrlPaths.UPDATE_PAYMENT_PROCESSOR_PARAMETER, method = RequestMethod.POST)
    String updatePaymentProcessorParameter(@ModelAttribute("paymentProcessorParameter") PaymentProcessorParameter paymentProcessorParameter, ModelMap modelMap, RedirectAttributes attr, HttpSession session) {
        if (paymentProcessorParameter != null) {
            LOG.log(Level.INFO, "namecc::{0}\tvalue::{1}\tdescription::{2}", new Object[]{paymentProcessorParameter.getName(), paymentProcessorParameter.getValue(), paymentProcessorParameter.getDescription()});
            parametersDAO.update(paymentProcessorParameter);
        }
        return "redirect:/admin/tools/paymentOption";
    }

    private boolean matcherPlatformName(List<PaymentProcessor> paymentProcessors, String newPlatformName) {
        String content = "( ";
        String found = "";
        for (PaymentProcessor processor : paymentProcessors) {
            content += processor.getPlatform().toUpperCase() + " | ";
        }
        content = content.substring(0, (content.length() - 2)) + " )";

        if (!paymentProcessors.isEmpty()) {
            Pattern pattern = Pattern.compile(content);
            Matcher matcher = pattern.matcher(newPlatformName.toUpperCase());

            while (matcher.find()) {
                String matched = matcher.group(1);
                found = matched;
            }
            if (found.length() <= 0) {
                content = "(";
                for (PaymentProcessor processor : paymentProcessors) {
                    content += processor.getPlatform().toUpperCase() + "|";
                }
                content = content.substring(0, (content.length() - 1)) + ")";

                Pattern pattern2 = Pattern.compile(content);
                Matcher matcher2 = pattern2.matcher(newPlatformName.toUpperCase());

                while (matcher2.find()) {
                    String matched = matcher2.group(1);
                    found = matched;
                }
            }
        }
        if (found.length() > 0) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 35;
    }
}
