package net.emida.supportsite.tools.paymentProcessor.repository;

import java.util.List;
import net.emida.supportsite.tools.paymentProcessor.dto.PaymentProcessor;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author janez
 */
public interface PaymentProcessorDAO {

    /**
     * 
     * @param entityId
     * @return
     * @throws TechnicalException 
     */
    List<PaymentProcessor> getPaymentsProcessors(Long entityId) throws TechnicalException;

    /**
     *
     * @param model
     * @return
     * @throws TechnicalException
     */
    ResponseEntity<PaymentProcessor> addPaymentProcessor(PaymentProcessor model) throws TechnicalException;

    /**
     * 
     * @return
     * @throws TechnicalException 
     */
    PaymentProcessor getDefaultPaymentProcessor() throws TechnicalException;

    /**
     * 
     * @param id
     * @return
     * @throws TechnicalException 
     */
    PaymentProcessor getPaymentProcessorById(String id) throws TechnicalException;

    /**
     *
     * @param id
     * @return
     * @throws TechnicalException
     */
    ResponseEntity<String> deletePaymentProcessorByPlatform(String id) throws TechnicalException;

    /**
     *
     * @param model
     * @return
     * @throws TechnicalException
     */
    ResponseEntity<Integer> updatePaymentProcessorByPlatform(PaymentProcessor model) throws TechnicalException;

}
