package net.emida.supportsite.tools.paymentProcessor.dto;

import java.util.List;

/**
 *
 * @author janez
 */
public class ParameterListWrapper {

    public List<PaymentProcessorParameter> parametersList;

    /**
     *
     * @return
     */
    public List<PaymentProcessorParameter> getParametersList() {
        return parametersList;
    }

    /**
     *
     * @param parametersList
     */
    public void setParametersList(List<PaymentProcessorParameter> parametersList) {
        this.parametersList = parametersList;
    }

}
