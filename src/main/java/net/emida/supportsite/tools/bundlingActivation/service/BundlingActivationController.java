package net.emida.supportsite.tools.bundlingActivation.service;

import au.com.bytecode.opencsv.CSVWriter;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.bundlingActivation.dto.HoldBackRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import net.emida.supportsite.tools.bundlingActivation.repository.IBundlingActivationDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ResponseBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * <strong>DTU-4013 / Support Site Report of queued second recharges
 * Feature</strong>
 *
 * @author janez@emida.net
 * @since 07-11-2017
 * @version 1.0
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class BundlingActivationController extends BaseController {

    private static final String FORM_PATH = "tools/bundlingActivation/bundlingActivation";
    private static final Logger log = LoggerFactory.getLogger(BundlingActivationController.class);

    @Autowired
    IBundlingActivationDao bundlingActivationDao;

    @RequestMapping(value = UrlPaths.BUNDLING_ACTIVATION_INIT, method = RequestMethod.GET)
    public String showBundlingActivationForm(ModelMap modelMap) {
        return FORM_PATH;
    }

    @RequestMapping(value = UrlPaths.BUNDLING_ACTIVATION_INIT + "/findLastHoldBackRequest", method = RequestMethod.GET)
    @ResponseBody
    public List<HoldBackRequestDto> findLastCompletedHoldBackRequest_HTTP_GET(
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        System.out.println("executed::findLastCompletedHoldBackRequest::");
        try {
            return bundlingActivationDao.findLastCompletedHoldBackRequest();
        } catch (TechnicalException err) {
            log.error("err::" + err);
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.BUNDLING_ACTIVATION_INIT + "/findHoldBackRequestByStatus", method = RequestMethod.GET)
    @ResponseBody
    public List<HoldBackRequestDto> findHoldBackRequestByStatus_HTTP_GET(
            @RequestParam(value = "startDt") String startDt,
            @RequestParam(value = "endDt") String endDt,
            @RequestParam(value = "statusCode") String statusCode,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        System.out.println("executed::findHoldBackRequestByStatus::" + startDt + "\t" + endDt);
        try {
            return bundlingActivationDao.findHoldBackRequestByStatus(
                    parseStringToDate(startDt),
                    parseStringToDate(endDt),
                    statusCode);
        } catch (TechnicalException err) {
            log.error("err::" + err);
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.BUNDLING_ACTIVATION_INIT + "/getHistoryTransaction", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> findLogTransaction_HTTP_GET(
            @RequestParam(value = "holdBackTrxId") String holdBackTrxId,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        System.out.println("executed::findLogTransaction_HTTP_GET::" + holdBackTrxId);
        try {
            return bundlingActivationDao.findHistoryTransaction(holdBackTrxId);
        } catch (TechnicalException err) {
            log.error("err::" + err);
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.BUNDLING_ACTIVATION_INIT + "/updateHoldBackRequestStatus", method = RequestMethod.POST)
    public String updateHoldBackRequestStatus_HTTP_POST(
            @RequestParam(value = "id") String id,
            @RequestParam(value = "statusCode") String statusCode,
            @RequestParam(value = "reason") String reason,
            ModelMap modelMap, RedirectAttributes attr, HttpSession httpSession, HttpServletRequest request) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        Map<String, String> auditLog = new HashMap<String, String>();
        auditLog.put("user_name", sessionData.getUser().getUsername());
        auditLog.put("instance", DebisysConfigListener.getInstance(request.getSession().getServletContext()));
        auditLog.put("reason", reason);
        try {
            return bundlingActivationDao.updateHoldBackRequestStatus(id, statusCode, auditLog);
        } catch (TechnicalException err) {
            log.error("err::" + err);
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.BUNDLING_ACTIVATION_INIT + "/downloadReport", method = RequestMethod.GET)
    public void downloadCSVReport_HTTP_GET(
            @RequestParam(value = "startDt") String startDt,
            @RequestParam(value = "endDt") String endDt,
            @RequestParam(value = "statusCode") String statusCode,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession, HttpServletResponse response) {
        System.out.println("executed::downloadCSVReport_HTTP_GET::" + startDt + "\t" + endDt);
        CSVWriter writer = null;
        try {
            writer = new CSVWriter(new OutputStreamWriter(response.getOutputStream()));
            buildCSVReport(writer, parseStringToDate(startDt), parseStringToDate(endDt), statusCode);

            response.addHeader("Content-Disposition", "attachment;filename=BunlingReport.csv");
            response.setContentType("txt/plain");
            response.flushBuffer();
        } catch (Exception e) {
            log.info(e.getMessage());
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(BundlingActivationController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @param writer
     * @param startDt
     * @param endDate
     * @param statusCode
     */
    private void buildCSVReport(CSVWriter writer, Date startDt, Date endDate, String statusCode) {
        List<HoldBackRequestDto> list = bundlingActivationDao.findHoldBackRequestByStatus(startDt, endDate, statusCode);
        List<String[]> data = new ArrayList<String[]>();
        data.add(new String[]{
            "Creation Date",
            "Schedule Date",
            "Last Execution Date",
            "Activation Transaction ID",
            "Hold BackTrx ID",
            "Status"
        });
        for (HoldBackRequestDto holdbackRequest : list) {
            data.add(new String[]{
                String.valueOf(holdbackRequest.getCreationDate()),
                String.valueOf(holdbackRequest.getScheduleDate()),
                String.valueOf((holdbackRequest.getLastExecutionDate() == null ? "--" : holdbackRequest.getLastExecutionDate())),
                String.valueOf(holdbackRequest.getActivationTransactionId()),
                String.valueOf(holdbackRequest.getHoldBackTrxId()),
                String.valueOf(statusCode)
            });
        }
        writer.writeAll(data);
    }

    private static Date parseStringToDate(String currentDt) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return simpleDateFormat.parse(currentDt);
        } catch (ParseException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 16;
    }

}
