package net.emida.supportsite.tools.bundlingActivation.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * <strong>DTU-4013 / Support Site Report of queued second recharges
 * Feature</strong>
 *
 * @author janez@emida.net
 * @since 07-11-2017
 * @version 1.0
 */
public class HoldBackRequestDto implements Serializable {

    private String id;
    private Date creationDate;
    private Date scheduleDate;
    private Date lastExecutionDate;
    private int activationTransactionId;
    private int holdBackTrxId;
    private String status;

    public HoldBackRequestDto() {
    }

    /**
     *
     * @param id
     * @param creationDate
     * @param scheduleDate
     * @param lastExecutionDate
     * @param activationTransactionId
     * @param holdBackTrxId
     * @param status
     */
    public HoldBackRequestDto(String id, Date creationDate, Date scheduleDate, Date lastExecutionDate, int activationTransactionId, int holdBackTrxId, String status) {
        this.id = id;
        this.creationDate = creationDate;
        this.scheduleDate = scheduleDate;
        this.lastExecutionDate = lastExecutionDate;
        this.activationTransactionId = activationTransactionId;
        this.holdBackTrxId = holdBackTrxId;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public Date getLastExecutionDate() {
        return lastExecutionDate;
    }

    public void setLastExecutionDate(Date lastExecutionDate) {
        this.lastExecutionDate = lastExecutionDate;
    }

    public int getActivationTransactionId() {
        return activationTransactionId;
    }

    public void setActivationTransactionId(int activationTransactionId) {
        this.activationTransactionId = activationTransactionId;
    }

    public int getHoldBackTrxId() {
        return holdBackTrxId;
    }

    public void setHoldBackTrxId(int holdBackTrxId) {
        this.holdBackTrxId = holdBackTrxId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
