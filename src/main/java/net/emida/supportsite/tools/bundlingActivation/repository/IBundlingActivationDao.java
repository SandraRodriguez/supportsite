package net.emida.supportsite.tools.bundlingActivation.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.tools.contractsPrivacyNotice.dto.AcceptanceContractsDto;
import net.emida.supportsite.tools.bundlingActivation.dto.HoldBackRequestDto;

/**
 * <strong>DTU-4013 / Support Site Report of queued second recharges
 * Feature</strong>
 *
 * @author janez@emida.net
 * @since 07-11-2017
 * @version 1.0
 */
public interface IBundlingActivationDao {

    /**
     *
     * @return
     */
    public List<HoldBackRequestDto> findLastCompletedHoldBackRequest();

    /**
     *
     * @param startDt
     * @param endDate
     * @param statusCode
     * @return
     */
    public List<HoldBackRequestDto> findHoldBackRequestByStatus(Date startDt, Date endDate, String statusCode);

    /**
     * 
     * @param id
     * @param statusCode
     * @param auditLog
     * @return 
     */
    public String updateHoldBackRequestStatus(String id, String statusCode, Map<String, String> auditLog);

    /**
     * 
     * @param holdBackTrxId
     * @return 
     */
    public Map<String, Object> findHistoryTransaction(String holdBackTrxId);       
}
