package net.emida.supportsite.tools.bundlingActivation.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.tools.bundlingActivation.dto.HoldBackRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 * <strong>DTU-4013 / Support Site Report of queued second recharges
 * Feature</strong>
 *
 * @author janez@emida.net
 * @since 07-11-2017
 * @version 1.0
 */
@Repository(value = "IBundlingActivationDao")
public class BundlingActivationRepository implements IBundlingActivationDao {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static final String FIND_HOLDBACKREQUEST_STATUS = "SELECT HBR.id, HBR.creationDate, HBR.scheduleDate, HBR.lastExecutionDate, HBR.activationTransactionId, HBR.holdBackTrxId, HTR.provider_response "
            + "FROM HoldBackRequest HBR WITH(NOLOCK) "
            + "INNER JOIN dbo.HoldBackRequestStatus HBRS WITH(NOLOCK) ON HBRS.id = HBR.status "
            + "LEFT JOIN dbo.hostTransactionsrecord HTR WITH(NOLOCK) ON (HBR.holdBackTrxId = HTR.transaction_id AND HBR.holdBackTrxSiteId = HTR.millennium_no) OR (HTR.transaction_id IS NULL) "
            + "WHERE HBRS.code = ? AND HBR.creationDate >= ? AND HBR.creationDate <= CAST(CONVERT(VARCHAR(10), ?, 110) + ' 23:59:59' AS DATETIME)";

    private static final String FIND_LAST_TRANSACTIONS = "SELECT  TOP 15 HBR.id, HBR.creationDate, HBR.scheduleDate, HBR.lastExecutionDate, HBR.activationTransactionId, HBR.holdBackTrxId "
            + " FROM HoldBackRequest HBR WITH(NOLOCK) "
            + " INNER JOIN dbo.HoldBackRequestStatus HBRS WITH(NOLOCK) "
            + " ON HBR.status = HBRS.id "
            + " WHERE HBRS.code = 'COMPLETED' ORDER BY HBR.creationDate DESC";

    private static final String FIND_LOG_HISTORY = "select provider_response, transText from hosttransactions where transaction_id = ?";
    
    private static final String FIND_LOG_CHANGE_TYPE = "SELECT LOG_CHANGE_TYPE_ID FROM LOGCHANGESTYPES (NOLOCK) WHERE [NAME] = 'Bundling Activation' AND table_name = 'HoldBackRequest'";
    
    private static final String FIND_HOLDBACK_REQUEST_BY_ID = "SELECT HBR.id, HBR.activationTransactionId, HBRS.code FROM HoldBackRequest HBR WITH(NOLOCK) "
            + "INNER JOIN dbo.HoldBackRequestStatus HBRS WITH(NOLOCK) ON HBRS.id = HBR.status "
            + "WHERE HBR.id = ?";

    private static final String AUDIT_LOG = "INSERT INTO LogChanges "
            + "(DATE_TIME,"
            + "LOG_CHANGE_TYPE_ID,"
            + "KEY_VALUE,"
            + "OLD_VALUE,"
            + "NEW_VALUE,"
            + "APPLICATION_NAME,"
            + "USER_NAME,"
            + "INSTANCE,"
            + "REASON) "
            + "VALUES (GETDATE(),?,?,?,?,?,?,?,?)";   

    /**
     * *
     *
     * @return
     */
    @Override
    public List<HoldBackRequestDto> findLastCompletedHoldBackRequest() {                        
        List<HoldBackRequestDto> listHoldBackRequest = this.jdbcTemplate.query(FIND_LAST_TRANSACTIONS, new Object[]{},
                new RowMapper<HoldBackRequestDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public HoldBackRequestDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                HoldBackRequestDto holdBackRequestDto = new HoldBackRequestDto(
                        resultSet.getString("id"),
                        resultSet.getDate("creationDate"),
                        resultSet.getDate("scheduleDate"),
                        resultSet.getDate("lastExecutionDate"),
                        resultSet.getInt("activationTransactionId"),
                        resultSet.getInt("holdBackTrxId"),
                        "COMPLETED");

                return holdBackRequestDto;
            }
        });
        return listHoldBackRequest;
    }

    /**
     *
     * @param startDt
     * @param endDate
     * @param statusCode
     * @return
     */
    @Override
    public List<HoldBackRequestDto> findHoldBackRequestByStatus(Date startDt, Date endDate, final String statusCode) {
        System.out.println("BundlingActivationRepository::" + new java.sql.Date(startDt.getTime()) + "\t" + new java.sql.Date(endDate.getTime()) + "\tstatus::" + statusCode);

        List<HoldBackRequestDto> listHoldBackRequest = this.jdbcTemplate.query(FIND_HOLDBACKREQUEST_STATUS, new Object[]{
            statusCode,
            new java.sql.Date(startDt.getTime()),
            new java.sql.Date(endDate.getTime())
        }, new RowMapper<HoldBackRequestDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public HoldBackRequestDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                HoldBackRequestDto holdBackRequestDto = new HoldBackRequestDto(
                        resultSet.getString("id"),
                        resultSet.getDate("creationDate"),
                        resultSet.getDate("scheduleDate"),
                        resultSet.getDate("lastExecutionDate"),
                        resultSet.getInt("activationTransactionId"),
                        resultSet.getInt("holdBackTrxId"),
                        statusCode);

                return holdBackRequestDto;
            }
        });
        return listHoldBackRequest;
    }

    @Override
    public String updateHoldBackRequestStatus(String id, String statusCode, Map<String, String> auditLog) {
        Map<String, Object> map = this.jdbcTemplate.queryForMap(FIND_HOLDBACK_REQUEST_BY_ID, id);        
        int rows = this.jdbcTemplate.update("UPDATE HoldBackRequest SET status = (SELECT id FROM dbo.holdBackRequestStatus hbrs WHERE code = ?) WHERE id = ?", new Object[]{
            statusCode,
            id
        });
        if (rows > 0) {      
            Map<String, Object> type_id = this.jdbcTemplate.queryForMap(FIND_LOG_CHANGE_TYPE);            
            
            this.jdbcTemplate.update(AUDIT_LOG, 
                    type_id.get("LOG_CHANGE_TYPE_ID"),
                    map.get("activationTransactionId"),
                    map.get("code"),
                    statusCode,                    
                    "support",
                    auditLog.get("user_name"),
                    auditLog.get("instance"),
                    auditLog.get("reason"));
            
            return "success";
        } else {
            return "failure";
        }
    }

    /**
     *
     * @param holdBackTrxId
     * @return
     */
    @Override
    public Map<String, Object> findHistoryTransaction(String holdBackTrxId) {
        try {

            return this.jdbcTemplate.queryForMap(FIND_LOG_HISTORY, new Object[]{
                holdBackTrxId
            });
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
