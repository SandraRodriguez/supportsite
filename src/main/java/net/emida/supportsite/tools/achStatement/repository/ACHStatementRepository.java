package net.emida.supportsite.tools.achStatement.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import net.emida.supportsite.tools.achStatement.model.BillingStatementBrandInfoModel;
import net.emida.supportsite.util.RSUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez
 * @see https://emidadev.atlassian.net/browse/ETP-1039
 * @since 2018-11-22
 * @version 2018-M
 */
@Repository
public class ACHStatementRepository implements ACHStatementDAO {
    
    private static final Logger LOG = Logger.getLogger(ACHStatementRepository.class);
    
    private static final String ALL = "SELECT * FROM BillingStatementBrandInfo b WITH(NOLOCK) WHERE b.entity_id = :entity_id";
    private static final String GET_BY_ID = "SELECT * FROM BillingStatementBrandInfo b WITH(NOLOCK) WHERE b.id = :id";
    private static final String ADD = "INSERT INTO BillingStatementBrandInfo(entity_id, entity_type_id, header_image_id, title, footer_image_id, footer_line1, footer_line2, image_line, email_from, email_subject, email_body) "
            + "                               VALUES(:entity_id, :entity_type_id, :header_image_id, :title, :footer_image_id, :footer_line1, :footer_line2, :image_line, :email_from, :email_subject, :email_body)";
    private static final String UPDATE = "UPDATE BillingStatementBrandInfo SET entity_id = :entity_id, entity_type_id = :entity_type_id, header_image_id = :header_image_id, "
            + "                               title = :title, footer_image_id = :footer_image_id,"
            + "                               footer_line1 = :footer_line1, footer_line2 = :footer_line2, image_line = :image_line, email_from = :email_from, email_subject = :email_subject, email_body = :email_body"
            + "                               WHERE id = :id";
    private static final String REMOVE = "DELETE FROM BillingStatementBrandInfo WHERE id = :id";
    
    @Autowired
    @Qualifier("masterNamedParamJdbcTemplate")
    private NamedParameterJdbcTemplate parameterJdbcTemplate;
    
    @Override
    public List<BillingStatementBrandInfoModel> getAll(Long iso) {
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("entity_id", iso);
            return parameterJdbcTemplate.query(ALL, parameters, new BeanPropertyRowMapper(BillingStatementBrandInfoModel.class));
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new ArrayList<>();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new ArrayList<>();
        } catch (Exception ex) {
            LOG.error(ex);
            return new ArrayList<>();
        }
    }
    
    @Override
    public BillingStatementBrandInfoModel getById(String id) {
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("id", id);
            return parameterJdbcTemplate.queryForObject(GET_BY_ID, parameters, new RowMapper<BillingStatementBrandInfoModel>() {
                @Override
                public BillingStatementBrandInfoModel mapRow(ResultSet resultSet, int index) throws SQLException {
                    BillingStatementBrandInfoModel model = new BillingStatementBrandInfoModel();
                    model.setId(RSUtil.getString(resultSet, "id"));
                    model.setEntity_id(RSUtil.getLong(resultSet, "entity_id"));
                    model.setEntity_type_id(RSUtil.getInteger(resultSet, "entity_type_id"));
                    model.setHeader_image_id(RSUtil.getString(resultSet, "header_image_id"));
                    model.setTitle(RSUtil.getString(resultSet, "title"));
                    model.setFooter_image_id(RSUtil.getString(resultSet, "footer_image_id"));
                    model.setFooter_line1(RSUtil.getString(resultSet, "footer_line1"));
                    model.setFooter_line2(RSUtil.getString(resultSet, "footer_line2"));
                    model.setImage_line(RSUtil.getString(resultSet, "image_line"));
                    model.setEmail_from(RSUtil.getString(resultSet, "email_from"));
                    model.setEmail_subject(RSUtil.getString(resultSet, "email_subject"));
                    model.setEmail_body(new String(resultSet.getBytes("email_body")));                    
                    
                    return model;
                }
            });
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new BillingStatementBrandInfoModel();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new BillingStatementBrandInfoModel();
        } catch (Exception ex) {
            LOG.error(ex);
            return new BillingStatementBrandInfoModel();
        }
    }
    
    @Override
    public BillingStatementBrandInfoModel getRepType(Long entityId) {
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("rep_id", entityId);
            return parameterJdbcTemplate.queryForObject("select r.type as entity_type_id from dbo.reps r where r.rep_id = :rep_id", parameters, new RowMapper<BillingStatementBrandInfoModel>() {
                @Override
                public BillingStatementBrandInfoModel mapRow(ResultSet resultSet, int index) throws SQLException {
                    BillingStatementBrandInfoModel model = new BillingStatementBrandInfoModel();
                    model.setEntity_type_id(RSUtil.getInteger(resultSet, "entity_type_id"));
                    return model;
                }
            });
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new BillingStatementBrandInfoModel();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new BillingStatementBrandInfoModel();
        } catch (Exception ex) {
            LOG.error(ex);
            return new BillingStatementBrandInfoModel();
        }
    }
    
    @Override
    public Integer add(BillingStatementBrandInfoModel model) {
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("id", model.getId());
            parameters.addValue("entity_id", model.getEntity_id());
            parameters.addValue("entity_type_id", getRepType(model.getEntity_id()).getEntity_type_id());
            parameters.addValue("header_image_id", model.getHeader_image_id());
            parameters.addValue("title", model.getTitle());
            parameters.addValue("footer_image_id", model.getFooter_image_id());
            parameters.addValue("footer_line1", model.getFooter_line1());
            parameters.addValue("footer_line2", model.getFooter_line2());
            parameters.addValue("image_line", model.getImage_line());
            parameters.addValue("email_from", model.getEmail_from());
            parameters.addValue("email_subject", model.getEmail_subject());
            parameters.addValue("email_body", model.getEmail_body().getBytes());
            
            return parameterJdbcTemplate.update(ADD, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }
    
    @Override
    public Integer update(BillingStatementBrandInfoModel model) {
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("id", model.getId());
            parameters.addValue("entity_id", model.getEntity_id());
            parameters.addValue("entity_type_id", getRepType(model.getEntity_id()).getEntity_type_id());
            parameters.addValue("header_image_id", model.getHeader_image_id());
            parameters.addValue("title", model.getTitle());
            parameters.addValue("footer_image_id", model.getFooter_image_id());
            parameters.addValue("footer_line1", model.getFooter_line1());
            parameters.addValue("footer_line2", model.getFooter_line2());
            parameters.addValue("image_line", model.getImage_line());
            parameters.addValue("email_from", model.getEmail_from());
            parameters.addValue("email_subject", model.getEmail_subject());
            parameters.addValue("email_body", model.getEmail_body().getBytes());
            return parameterJdbcTemplate.update(UPDATE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }
    
    @Override
    public Integer remove(String id) {
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("id", id);
            return parameterJdbcTemplate.update(REMOVE, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return 0;
        } catch (Exception ex) {
            LOG.error(ex);
            return 0;
        }
    }
    
    @Override
    public String loadImage(byte[] image) {
        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("uuid", uuid);
            parameters.addValue("image", image);
            
            int rows = parameterJdbcTemplate.update("INSERT INTO Images(Name, Type, IdNew, imageTypeId) VALUES(concat('ach-statement-', :uuid, '.png'), 'STATEMENT', :uuid, (SELECT id FROM dbo.ImageTypes WHERE code = 'STATEMENT'))", parameters);
            if (rows > 0) {
                parameterJdbcTemplate.update("INSERT INTO ImagesData(Id, LastUpdate, Data) VALUES((SELECT Id FROM dbo.Images WHERE IdNew = :uuid), (SELECT GETDATE()), :image)", parameters);
                return uuid;
            } else {
                return "";
            }
        } catch (Exception ex) {
            LOG.error(ex);
            return "";
        }
    }
    
}
