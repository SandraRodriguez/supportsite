package net.emida.supportsite.tools.achStatement.model;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
public class BillingStatementBrandInfoModel {

    private String id;
    private Long entity_id;
    private Integer entity_type_id;
    private String header_image_id;
    private String title;
    private String footer_image_id;
    private String footer_line1;
    private String footer_line2;
    private String image_line;
    private byte[] header_image;
    private byte[] footer_image;
    private String aux_uuid;
    private String email_from;
    private String email_subject;
    private String email_body;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(Long entity_id) {
        this.entity_id = entity_id;
    }

    public Integer getEntity_type_id() {
        return entity_type_id;
    }

    public void setEntity_type_id(Integer entity_type_id) {
        this.entity_type_id = entity_type_id;
    }

    public String getHeader_image_id() {
        return header_image_id;
    }

    public void setHeader_image_id(String header_image_id) {
        this.header_image_id = header_image_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFooter_image_id() {
        return footer_image_id;
    }

    public void setFooter_image_id(String footer_image_id) {
        this.footer_image_id = footer_image_id;
    }

    public String getFooter_line1() {
        return footer_line1;
    }

    public void setFooter_line1(String footer_line1) {
        this.footer_line1 = footer_line1;
    }

    public String getFooter_line2() {
        return footer_line2;
    }

    public void setFooter_line2(String footer_line2) {
        this.footer_line2 = footer_line2;
    }

    public String getImage_line() {
        return image_line;
    }

    public void setImage_line(String image_line) {
        this.image_line = image_line;
    }

    public byte[] getHeader_image() {
        return header_image;
    }

    public void setHeader_image(byte[] header_image) {
        this.header_image = header_image;
    }

    public byte[] getFooter_image() {
        return footer_image;
    }

    public void setFooter_image(byte[] footer_image) {
        this.footer_image = footer_image;
    }

    public String getAux_uuid() {
        return aux_uuid;
    }

    public void setAux_uuid(String aux_uuid) {
        this.aux_uuid = aux_uuid;
    }

    public String getEmail_from() {
        return email_from;
    }

    public void setEmail_from(String email_from) {
        this.email_from = email_from;
    }

    public String getEmail_subject() {
        return email_subject;
    }

    public void setEmail_subject(String email_subject) {
        this.email_subject = email_subject;
    }

    public String getEmail_body() {
        return email_body;
    }

    public void setEmail_body(String email_body) {
        this.email_body = email_body;
    }

}
