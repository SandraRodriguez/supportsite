package net.emida.supportsite.tools.achStatement.repository;

import java.util.List;
import net.emida.supportsite.tools.achStatement.model.BillingStatementBrandInfoModel;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
public interface ACHStatementDAO {

    List<BillingStatementBrandInfoModel> getAll(Long iso);

    BillingStatementBrandInfoModel getById(String id);

    Integer add(BillingStatementBrandInfoModel model);

    Integer update(BillingStatementBrandInfoModel model);

    Integer remove(String id);

    String loadImage(byte[] image);

    BillingStatementBrandInfoModel getRepType(Long entityId);

}
