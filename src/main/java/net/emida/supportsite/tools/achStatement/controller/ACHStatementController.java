package net.emida.supportsite.tools.achStatement.controller;

import com.debisys.users.SessionData;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.tools.achStatement.model.BillingStatementBrandInfoModel;
import net.emida.supportsite.tools.achStatement.repository.ACHStatementDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class ACHStatementController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(ACHStatementController.class);
    private static final String PATH = UrlPaths.ACH_STATEMENT;

    @Autowired
    ACHStatementDAO repository;

    @RequestMapping(value = PATH, method = RequestMethod.GET)
    public String __init__() {
        return "tools/achStatement/achStatement";
    }

    @RequestMapping(value = PATH + "/all", method = RequestMethod.GET)
    @ResponseBody
    List<BillingStatementBrandInfoModel> getAll(HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Long strRefId = new Long(sessionData.getProperty("ref_id"));
            return repository.getAll(strRefId);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new ArrayList<>();
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = PATH + "/getById", method = RequestMethod.GET)
    @ResponseBody
    BillingStatementBrandInfoModel getById(@RequestParam("id") String id, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.getById(id);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return new BillingStatementBrandInfoModel();
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = PATH + "/add", method = RequestMethod.POST)
    @ResponseBody
    Integer add(@RequestBody BillingStatementBrandInfoModel model, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Long strRefId = new Long(sessionData.getProperty("ref_id"));
            model.setEntity_id(strRefId);
            return repository.add(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = PATH + "/update", method = RequestMethod.POST)
    @ResponseBody
    Integer update(@RequestBody BillingStatementBrandInfoModel model, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Long strRefId = new Long(sessionData.getProperty("ref_id"));
            model.setEntity_id(strRefId);
            return repository.update(model);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = PATH + "/remove", method = RequestMethod.DELETE)
    @ResponseBody
    Integer remove(@RequestParam("id") String id, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.remove(id);
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return 0;
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = PATH + "/labels", method = RequestMethod.GET)
    @ResponseBody
    String labels(HttpServletRequest request, HttpSession httpSession) {
        return null;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = PATH + "/loadImage", method = RequestMethod.POST)
    @ResponseBody
    String loadImage(MultipartFile file, HttpServletRequest request, HttpSession httpSession) {
        try {
            return repository.loadImage(file.getBytes());
        } catch (Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
            return "";
        }
    }

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 47;
    }

}
