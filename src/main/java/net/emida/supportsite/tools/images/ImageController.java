/*
 * EMIDA all rights reserved 1999-2025.
 */
package net.emida.supportsite.tools.images;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.dao.ImagesDao;
import net.emida.supportsite.dto.ImageType;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fernandob
 */
@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class ImageController extends BaseController{
    
    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 18;
    }
    
    
    @RequestMapping(value = UrlPaths.IMAGES_GET_SUGGESTED_SIZE, method = RequestMethod.GET)
    @ResponseBody
    public String getImageSuggestedSize(@RequestParam(required = true) String imageTypeCode, HttpSession httpSession){
        String retValue = "";
        
        ImageType imageType = ImagesDao.getImageTypeByCode(imageTypeCode);
        if((imageType != null) && (imageType.getSuggestedSize()!= null) && (!imageType.getSuggestedSize().isEmpty())){
            SessionData sessionData = (SessionData)httpSession.getAttribute("SessionData");
            retValue = String.format("%s %s", Languages.getString("jsp.admin.images.recommendedSize", sessionData.getLanguage()),
                    imageType.getSuggestedSize());
        }
        return retValue;        
    }
    
}
