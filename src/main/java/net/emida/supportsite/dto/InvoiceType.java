/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class InvoiceType implements Comparable<InvoiceType> {

    private long id;
    private String description;
    private boolean status;
    private String code;
    private String idNew;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIdNew() {
        return idNew;
    }

    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    public InvoiceType() {
        this.id = 0L;
        this.description = "";
        this.status = false;
        this.code = "";
        this.idNew = "";
    }

    public InvoiceType(long id, String description, boolean status, String code,
            String idNew) {
        this.id = id;
        this.description = description;
        this.status = status;
        this.code = code;
        this.idNew = idNew;
    }

    @Override
    public String toString() {
        return "InvoiceType{" + "id=" + id + ", description=" + description + ", status=" + status + ", code=" + code + ", idNew=" + idNew + '}';
    }

    @Override
    public int compareTo(InvoiceType other) {
        int last = description.compareTo(other.description);
        if (last == 0) {
            return description.compareTo(other.description);
        }
        return last;
    }
}
