/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class Bank {

    private long id;
    private String code;
    private String name;
    private boolean status;
    private String idNew;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getIdNew() {
        return idNew;
    }

    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    public Bank() {
        this.id = 0L;
        this.code = "";
        this.name = "";
        this.status = false;
        this.idNew = "";
    }

    public Bank(long id, String code, String name, boolean status, String idNew) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.status = status;
        this.idNew = idNew;
    }

    @Override
    public String toString() {
        return "Bank{" + "id=" + id + ", code=" + code + ", name=" + name + ", status=" + status + ", idNew=" + idNew + '}';
    }

}
