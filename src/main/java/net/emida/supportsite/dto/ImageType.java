/*
 * EMIDA all rights reserved 1999-2025.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class ImageType {

    private String id;
    private String code;
    private String description;
    private String suggestedSize;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSuggestedSize() {
        return suggestedSize;
    }

    public void setSuggestedSize(String suggestedSize) {
        this.suggestedSize = suggestedSize;
    }

    public ImageType() {
        this.id = "";
        this.code = "";
        this.description = "";
        this.suggestedSize = "";
    }

    public ImageType(String id, String code, String description, String suggestedSize) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.suggestedSize = suggestedSize;
    }

    @Override
    public String toString() {
        return "ImageType{" + "id=" + id + ", code=" + code + ", description=" + description + ", suggestedSize=" + suggestedSize + '}';
    }

}
