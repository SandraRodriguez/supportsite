/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public interface IReportConfigurationByResponseType {

    String getFormatterClassName();

    String getId();

    String getReportConfigurationId();

    String getRetrieverClassName();

    long getTerminalResponseTypeId();

    void setFormatterClassName(String formatterClassName);

    void setId(String id);

    void setReportConfigurationId(String reportConfigurationId);

    void setRetrieverClassName(String retrieverClassName);

    void setTerminalResponseTypeId(long terminalTypeId);
    
}
