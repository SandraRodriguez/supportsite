/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

/**
 *
 * @author fernandob
 */
public class RepInvoicePaymentType implements Comparable<RepInvoicePaymentType>{

    private String id;
    private long repId;
    private long typeId;
    private String typeDescription;
    private String accountNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getRepId() {
        return repId;
    }

    public void setRepId(long repId) {
        this.repId = repId;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public RepInvoicePaymentType() {
        this.id = "";
        this.repId = 0L;
        this.typeId = 0L;
        this.typeDescription = "";
        this.accountNumber = "";
    }

    public RepInvoicePaymentType(String id, long repId, long typeId, String typeDescription, String accountNumber) {
        this.id = id;
        this.repId = repId;
        this.typeId = typeId;
        this.typeDescription = typeDescription;
        this.accountNumber = accountNumber;
    }

    @Override
    public String toString() {
        return "RepInvoicePaymentType{" + "id=" + id + ", repId=" + repId + ", typeId=" + typeId + ", typeDescription=" + typeDescription + ", accountNumber=" + accountNumber + '}';
    }

    @Override
    public int compareTo(RepInvoicePaymentType other) {
        int last = typeDescription.compareTo(other.typeDescription);
        if(last == 0)
            return typeDescription.compareTo(other.typeDescription);
        return last;
    }





    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RepInvoicePaymentType other = (RepInvoicePaymentType) obj;
        if (this.repId != other.repId) {
            return false;
        }
        if (this.typeId != other.typeId) {
            return false;
        }
        if ((this.accountNumber == null) ? (other.accountNumber != null) : !this.accountNumber.equals(other.accountNumber)) {
            return false;
        }
        return true;
    }
    
    
    
    public String toJsonString() {
        String retValue = "";
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<RepInvoicePaymentType>() {
            }.getType();
            retValue = gson.toJson(this, type);

        } catch (Exception localException) {

        }
        return retValue;
    }
    
    public void copy(RepInvoicePaymentType otherPaymentType){
        if((otherPaymentType != null) &&
                (this.getId().equals(otherPaymentType.getId()))){
            this.setRepId(otherPaymentType.getRepId());
            this.setTypeId(otherPaymentType.getTypeId());
            this.setTypeDescription(otherPaymentType.getTypeDescription());
            this.setAccountNumber(otherPaymentType.getAccountNumber());
        }        
    }

}
