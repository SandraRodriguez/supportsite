/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class Provider {
    private long providerId;
    private String name;
    private String className;
    private String address;
    private int port;
    private int timeout;
    private boolean disabled;
    private String disabledResponse;
    private long refId;
    private boolean h2hXml;
    private boolean checkDuplicate;
    private boolean alwaysCheck;
    private boolean usesMapping;
    private long timeoutAsyncCalls;
    private String displayName;

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getDisabledResponse() {
        return disabledResponse;
    }

    public void setDisabledResponse(String disabledResponse) {
        this.disabledResponse = disabledResponse;
    }

    public long getRefId() {
        return refId;
    }

    public void setRefId(long refId) {
        this.refId = refId;
    }

    public boolean isH2hXml() {
        return h2hXml;
    }

    public void setH2hXml(boolean h2hXml) {
        this.h2hXml = h2hXml;
    }

    public boolean isCheckDuplicate() {
        return checkDuplicate;
    }

    public void setCheckDuplicate(boolean checkDuplicate) {
        this.checkDuplicate = checkDuplicate;
    }

    public boolean isAlwaysCheck() {
        return alwaysCheck;
    }

    public void setAlwaysCheck(boolean alwaysCheck) {
        this.alwaysCheck = alwaysCheck;
    }

    public boolean isUsesMapping() {
        return usesMapping;
    }

    public void setUsesMapping(boolean usesMapping) {
        this.usesMapping = usesMapping;
    }

    public long getTimeoutAsyncCalls() {
        return timeoutAsyncCalls;
    }

    public void setTimeoutAsyncCalls(long timeoutAsyncCalls) {
        this.timeoutAsyncCalls = timeoutAsyncCalls;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "Provider{" + "providerId=" + providerId + ", name=" + name + ", className=" + className + ", address=" + address + ", port=" + port + ", timeout=" + timeout + ", disabled=" + disabled + ", disabledResponse=" + disabledResponse + ", refId=" + refId + ", h2hXml=" + h2hXml + ", checkDuplicate=" + checkDuplicate + ", alwaysCheck=" + alwaysCheck + ", usesMapping=" + usesMapping + ", timeoutAsyncCalls=" + timeoutAsyncCalls + ", displayName=" + displayName + '}';
    }
    
    
    
}
