/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

import com.google.gson.Gson;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author fernandob
 */
public class InvoicePaymentType implements Comparable<InvoicePaymentType> {

    private long id;
    private String code;
    private String description;
    private boolean status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public InvoicePaymentType() {
        this.id = 0L;
        this.code = "";
        this.description = "";
        this.status = false;
    }

    public InvoicePaymentType(long id, String code, String description, boolean status) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return "InvoicePaymentType{" + "id=" + id + ", code=" + code + ", description=" + description + ", status=" + status + '}';
    }

    @Override
    public int compareTo(InvoicePaymentType other) {
        int last = description.compareTo(other.description);
        if (last == 0) {
            return description.compareTo(other.description);
        }
        return last;
    }

    public String toJsonString() {
        String retValue = "";
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<InvoicePaymentType>() {
            }.getType();
            retValue = gson.toJson(this, type);

        } catch (Exception localException) {

        }
        return retValue;
    }

}
