/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class ReportConfigurationByResponseType implements IReportConfigurationByResponseType {

    private String id;
    private String reportConfigurationId;
    private long terminalResponseTypeId;
    private String retrieverClassName;
    private String formatterClassName;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getReportConfigurationId() {
        return reportConfigurationId;
    }

    @Override
    public void setReportConfigurationId(String reportConfigurationId) {
        this.reportConfigurationId = reportConfigurationId;
    }

    @Override
    public long getTerminalResponseTypeId() {
        return terminalResponseTypeId;
    }

    @Override
    public void setTerminalResponseTypeId(long terminalResponseTypeId) {
        this.terminalResponseTypeId = terminalResponseTypeId;
    }

    @Override
    public String getRetrieverClassName() {
        return retrieverClassName;
    }

    @Override
    public void setRetrieverClassName(String retrieverClassName) {
        this.retrieverClassName = retrieverClassName;
    }

    @Override
    public String getFormatterClassName() {
        return formatterClassName;
    }

    @Override
    public void setFormatterClassName(String formatterClassName) {
        this.formatterClassName = formatterClassName;
    }

    public ReportConfigurationByResponseType() {
    }

    public ReportConfigurationByResponseType(String id, String reportConfigurationId,
            long terminalTypeId, String retrieverClassName, String formatterClassName) {
        this.id = id;
        this.reportConfigurationId = reportConfigurationId;
        this.terminalResponseTypeId = terminalTypeId;
        this.retrieverClassName = retrieverClassName;
        this.formatterClassName = formatterClassName;
    }

    @Override
    public String toString() {
        return "ReportConfigurationByTerminal{" + "id=" + id + ", reportConfigurationId=" + 
                reportConfigurationId + ", terminalTypeId=" + terminalResponseTypeId + ", retrieverClassName=" + 
                retrieverClassName + ", formatterClassName=" + formatterClassName + '}';
    }
    
}
