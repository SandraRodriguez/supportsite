/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class ReportConfiguration implements IReportConfiguration {

    private String reportId;
    private String reportCode;
    private int reportBackwardDateLimit;

    @Override
    public String getReportId() {
        return reportId;
    }

    @Override
    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    @Override
    public String getReportCode() {
        return reportCode;
    }

    @Override
    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    @Override
    public int getReportBackwardDateLimit() {
        return reportBackwardDateLimit;
    }

    @Override
    public void setReportBackwardDateLimit(int reportBackwardDateLimit) {
        this.reportBackwardDateLimit = reportBackwardDateLimit;
    }

    public ReportConfiguration() {
        this.reportId = "";
        this.reportCode = "";
        this.reportBackwardDateLimit = 0;
    }

    public ReportConfiguration(String reportId, String reportCode,
            int reportBackwardDateLimit) {
        this.reportId = reportId;
        this.reportCode = reportCode;
        this.reportBackwardDateLimit = reportBackwardDateLimit;
    }

    @Override
    public String toString() {
        return "ReportConfiguration{" + "reportId=" + reportId + ", reportCode=" + reportCode + ", reportBackwardDateLimit=" + reportBackwardDateLimit + '}';
    }

}
