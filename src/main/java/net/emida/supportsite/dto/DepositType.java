/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class DepositType {
    private String id;
    private String descriptionEnglish;
    private String descriptionSpanish;
    private String externalCode;
    private String depositCode;
    private boolean transferAccount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescriptionEnglish() {
        return descriptionEnglish;
    }

    public void setDescriptionEnglish(String descriptionEnglish) {
        this.descriptionEnglish = descriptionEnglish;
    }

    public String getDescriptionSpanish() {
        return descriptionSpanish;
    }

    public void setDescriptionSpanish(String descriptionSpanish) {
        this.descriptionSpanish = descriptionSpanish;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public String getDepositCode() {
        return depositCode;
    }

    public void setDepositCode(String depositCode) {
        this.depositCode = depositCode;
    }

    public boolean isTransferAccount() {
        return transferAccount;
    }

    public void setTransferAccount(boolean transferAccount) {
        this.transferAccount = transferAccount;
    }


    public DepositType() {
        this.id = "";
        this.descriptionEnglish = "";
        this.descriptionSpanish = "";
        this.externalCode = "";
        this.depositCode = "";
        this.transferAccount = false;
    }
    
    public DepositType(String id, String descriptionEnglish, 
            String descriptionSpanish, String externalCode, String depositCode, 
            boolean transferAccount) {
        this.id = id;
        this.descriptionEnglish = descriptionEnglish;
        this.descriptionSpanish = descriptionSpanish;
        this.externalCode = externalCode;
        this.depositCode = depositCode;
        this.transferAccount = transferAccount;
    }

    @Override
    public String toString() {
        return "DepositType{" + "id=" + id + ", descriptionEnglish=" + descriptionEnglish + ", descriptionSpanish=" + descriptionSpanish + ", externalCode=" + externalCode + ", depositCode=" + depositCode + ", transferAccount=" + transferAccount + '}';
    }
    
    
    
}
