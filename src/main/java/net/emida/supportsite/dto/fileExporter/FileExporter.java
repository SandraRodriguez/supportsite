/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto.fileExporter;

/**
 *
 * @author fernandob
 */
public class FileExporter {

    protected String id;
    protected String fileExporterClass;
    protected String description;
    protected String fileNameFormat;
    protected String targetFilePath;
    protected String headerFormat;
    protected String recordFormat;
    protected String recordSeparator;
    protected String footerFormat;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileExporterClass() {
        return fileExporterClass;
    }

    public void setFileExporterClass(String fileExporterClass) {
        this.fileExporterClass = fileExporterClass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileNameFormat() {
        return fileNameFormat;
    }

    public void setFileNameFormat(String fileNameFormat) {
        this.fileNameFormat = fileNameFormat;
    }

    public String getTargetFilePath() {
        return targetFilePath;
    }

    public void setTargetFilePath(String targetFilePath) {
        this.targetFilePath = targetFilePath;
    }

    public String getHeaderFormat() {
        return headerFormat;
    }

    public void setHeaderFormat(String headerFormat) {
        this.headerFormat = headerFormat;
    }

    public String getRecordFormat() {
        return recordFormat;
    }

    public void setRecordFormat(String recordFormat) {
        this.recordFormat = recordFormat;
    }

    public String getRecordSeparator() {
        return recordSeparator;
    }

    public void setRecordSeparator(String recordSeparator) {
        this.recordSeparator = recordSeparator;
    }

    public String getFooterFormat() {
        return footerFormat;
    }

    public void setFooterFormat(String footerFormat) {
        this.footerFormat = footerFormat;
    }

    public FileExporter() {
        this.id = "";
        this.fileExporterClass = "";
        this.description = "";
        this.fileNameFormat = "";
        this.targetFilePath = "";
        this.headerFormat = "";
        this.recordFormat = "";
        this.recordSeparator = "";
        this.footerFormat = "";
    }

    public FileExporter(String id, String fileExporterClass,
            String description, String fileNameFormat, String targetFilePath,
            String headerFormat, String recordFormat, String recordSeparator,
            String footerFormat) {
        this.id = id;
        this.fileExporterClass = fileExporterClass;
        this.description = description;
        this.fileNameFormat = fileNameFormat;
        this.targetFilePath = targetFilePath;
        this.headerFormat = headerFormat;
        this.recordFormat = recordFormat;
        this.recordSeparator = recordSeparator;
        this.footerFormat = footerFormat;
    }

    @Override
    public String toString() {
        return "FileExporterProperties{" + "id=" + id + ", fileExporterClass=" + fileExporterClass + ", description=" + description + ", fileNameFormat=" + fileNameFormat + ", targetFilePath=" + targetFilePath + ", headerFormat=" + headerFormat + ", recordFormat=" + recordFormat + ", recordSeparator=" + recordSeparator + ", footerFormat=" + footerFormat + '}';
    }
    
    public boolean isValid(){
        return (!this.fileExporterClass.isEmpty() &&
                !this.description.isEmpty() &&
                !this.fileNameFormat.isEmpty() &&
                !this.targetFilePath.isEmpty() &&
                !this.recordFormat.isEmpty() &&
                !this.recordSeparator.isEmpty());        
    }

}
