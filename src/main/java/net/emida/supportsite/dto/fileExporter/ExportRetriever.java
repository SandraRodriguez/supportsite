/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto.fileExporter;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author fernandob
 */
public class ExportRetriever {

	private String id;
	private String retrieverClass;
	private String description;
	private String columnHeaders;
	private String dataSourceName;
	private String startDate;
	private String endDate;
    private boolean useFixedDates;
    private boolean useDateRange;
    private int rangeDays;
    private boolean isPartialQuery;
    private String detailQuery;
    private String fileNameDateFormat;
    private boolean useStartDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRetrieverClass() {
        return retrieverClass;
    }

    public void setRetrieverClass(String retrieverClass) {
        this.retrieverClass = retrieverClass;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColumnHeaders() {
        return columnHeaders;
    }

    public void setColumnHeaders(String columnHeaders) {
        this.columnHeaders = columnHeaders;
    }

    public String getDataSourceName() {
        return dataSourceName;
    }

    public void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isUseFixedDates() {
        return useFixedDates;
    }

    public void setUseFixedDates(boolean useFixedDates) {
        this.useFixedDates = useFixedDates;
    }

    public boolean isUseDateRange() {
        return useDateRange;
    }

    public void setUseDateRange(boolean useDateRange) {
        this.useDateRange = useDateRange;
    }

    public int getRangeDays() {
        return rangeDays;
    }

    public void setRangeDays(int rangeDays) {
        this.rangeDays = rangeDays;
    }

    public boolean isPartialQuery() {
        return isPartialQuery;
    }

    public void setIsPartialQuery(boolean isPartialQuery) {
        this.isPartialQuery = isPartialQuery;
    }

    public String getDetailQuery() {
        return detailQuery;
    }

    public void setDetailQuery(String detailQuery) {
        this.detailQuery = detailQuery;
    }

    public String getFileNameDateFormat() {
        return fileNameDateFormat;
    }

    public void setFileNameDateFormat(String fileNameDateFormat) {
        this.fileNameDateFormat = fileNameDateFormat;
    }
    
    public boolean isUseStartDate() {
		return useStartDate;
	}

	public void setUseStartDate(boolean useStartDate) {
		this.useStartDate = useStartDate;
	}

    public ExportRetriever() {
        this.id = "";
        this.retrieverClass = "";
        this.description = "";
        this.columnHeaders = "";
        this.dataSourceName = "";
        this.startDate = "";
        this.endDate = "";
        this.useFixedDates = false;
        this.useDateRange = false;
        this.rangeDays = 0;
        this.isPartialQuery = false;
        this.detailQuery = "";
        this.fileNameDateFormat = "";
        this.useStartDate = false;
    }

    public ExportRetriever(String id, String retrieverClass,
            String description, String columnHeaders, String dataSourceName,
            String startDate, String endDate, boolean useFixedDates,
            boolean useDateRange, int rangeDays, boolean partialQuery,
            String detailQuery, String fileNameDateFormat) {
        this.id = id;
        this.retrieverClass = retrieverClass;
        this.description = description;
        this.columnHeaders = columnHeaders;
        this.dataSourceName = dataSourceName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.useFixedDates = useFixedDates;
        this.useDateRange = useDateRange;
        this.rangeDays = rangeDays;
        this.isPartialQuery = partialQuery;
        this.detailQuery = detailQuery;
        this.fileNameDateFormat = fileNameDateFormat;
    }

    @Override
	public String toString() {
		return "ExportRetriever [id=" + id + ", retrieverClass=" + retrieverClass + ", description=" + description
				+ ", columnHeaders=" + columnHeaders + ", dataSourceName=" + dataSourceName + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", useFixedDates=" + useFixedDates + ", useDateRange=" + useDateRange
				+ ", rangeDays=" + rangeDays + ", isPartialQuery=" + isPartialQuery + ", detailQuery=" + detailQuery
				+ ", fileNameDateFormat=" + fileNameDateFormat + ", useStartDate=" + useStartDate + "]";
	}

	public boolean isValid() {
		return StringUtils.isNotEmpty(retrieverClass) 
				&& StringUtils.isNotEmpty(description)
				&& StringUtils.isNotEmpty(dataSourceName)
				&& (!useFixedDates || (useFixedDates && StringUtils.isNotEmpty(startDate) && StringUtils.isNotEmpty(endDate)))
				&& (!useDateRange || (useDateRange && rangeDays > 0))
				&& StringUtils.isNotEmpty(detailQuery)
				&& StringUtils.isNotEmpty(fileNameDateFormat);
    }

}
