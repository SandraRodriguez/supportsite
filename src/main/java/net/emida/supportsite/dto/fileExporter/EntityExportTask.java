/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto.fileExporter;

/**
 *
 * @author fernandob
 */
public class EntityExportTask {

    protected String id;
    protected String description;
    protected long entityId;
    protected String entityName;
    protected int entityTypeId;
    protected String retrieverId;
    protected String fileExporterId;
    protected String queryPreprocessorClass;
    protected boolean taskEnabled;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public int getEntityTypeId() {
        return entityTypeId;
    }

    public void setEntityTypeId(int entityTypeId) {
        this.entityTypeId = entityTypeId;
    }

    public String getRetrieverId() {
        return retrieverId;
    }

    public void setRetrieverId(String retrieverId) {
        this.retrieverId = retrieverId;
    }

    public String getFileExporterId() {
        return fileExporterId;
    }

    public void setFileExporterId(String fileExporterId) {
        this.fileExporterId = fileExporterId;
    }

    public String getQueryPreprocessorClass() {
        return queryPreprocessorClass;
    }

    public void setQueryPreprocessorClass(String queryPreprocessorClass) {
        this.queryPreprocessorClass = queryPreprocessorClass;
    }

    public boolean isTaskEnabled() {
        return taskEnabled;
    }

    public void setTaskEnabled(boolean taskEnabled) {
        this.taskEnabled = taskEnabled;
    }

    public EntityExportTask() {
        this.id = "";
        this.description = "";
        this.entityId = 0L;
        this.entityName = "";
        this.entityTypeId = 0;
        this.retrieverId = "";
        this.fileExporterId = "";
        this.queryPreprocessorClass = "";
        this.taskEnabled = false;
    }

    public EntityExportTask(String id, String description, long entityId,
            String entityName, int entityTypeId, String retrieverId,
            String fileExporterId, String queryPreprocessorClass, boolean taskEnabled) {
        this.id = id;
        this.description = description;
        this.entityId = entityId;
        this.entityName = entityName;
        this.entityTypeId = entityTypeId;
        this.retrieverId = retrieverId;
        this.fileExporterId = fileExporterId;
        this.queryPreprocessorClass = queryPreprocessorClass;
        this.taskEnabled = taskEnabled;
    }

    @Override
    public String toString() {
        return "EntityExportTask{" + "id=" + id + ", description=" + description + ", entityId=" + entityId + ", entityName=" + entityName + ", entityTypeId=" + entityTypeId + ", retrieverId=" + retrieverId + ", fileExporterId=" + fileExporterId + ", queryPreprocessorClass=" + queryPreprocessorClass + ", taskEnabled=" + taskEnabled + '}';
    }

    public boolean isValid() {
        return (!this.description.isEmpty()
                && (this.entityId > 0L)
                && !this.entityName.isEmpty()
                && (entityTypeId > 0)
                && !this.retrieverId.isEmpty()
                && !this.fileExporterId.isEmpty()
                && !this.queryPreprocessorClass.isEmpty());
    }

}
