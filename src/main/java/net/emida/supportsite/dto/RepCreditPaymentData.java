/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

import com.debisys.utils.StringUtil;

/**
 *
 * @author fernandob
 */
public class RepCreditPaymentData {

    private String id;
    private String repCreditId;
    private String bankId;
    private String depositTypeId;
    private String invoiceTypeId;
    private String accountNumber;
    private String documentNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRepCreditId() {
        return repCreditId;
    }

    public void setRepCreditId(String repCreditId) {
        this.repCreditId = repCreditId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getDepositTypeId() {
        return depositTypeId;
    }

    public void setDepositTypeId(String depositTypeId) {
        this.depositTypeId = depositTypeId;
    }

    public String getInvoiceTypeId() {
        return invoiceTypeId;
    }

    public void setInvoiceTypeId(String invoiceTypeId) {
        this.invoiceTypeId = invoiceTypeId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public RepCreditPaymentData() {
        this.id = "";
        this.repCreditId = "";
        this.bankId = "";
        this.depositTypeId = "";
        this.invoiceTypeId = "";
        this.accountNumber = "";
        this.documentNumber = "";
    }

    public RepCreditPaymentData(String id, String repCreditId, String bankId,
            String depositTypeId, String invoiceTypeId, String accountNumber,
            String documentNumber) {
        this.id = id;
        this.repCreditId = repCreditId;
        this.bankId = bankId;
        this.depositTypeId = depositTypeId;
        this.invoiceTypeId = invoiceTypeId;
        this.accountNumber = accountNumber;
        this.documentNumber = documentNumber;
    }

    public boolean isValid() {
        return ((StringUtil.isNotEmptyStr(this.repCreditId))
                && (StringUtil.isNotEmptyStr(this.bankId))
                && (StringUtil.isNotEmptyStr(this.depositTypeId))
                && (StringUtil.isNotEmptyStr(this.invoiceTypeId))
                && (StringUtil.isNotEmptyStr(this.documentNumber)));
    }

    @Override
    public String toString() {
        return "RepCreditsPaymentData{" + "id=" + id + ", repCreditId=" + repCreditId + ", bankId=" + bankId + ", depositTypeId=" + depositTypeId + ", invoiceTypeId=" + invoiceTypeId + ", accountNumber=" + accountNumber + ", documentNumber=" + documentNumber + '}';
    }

}
