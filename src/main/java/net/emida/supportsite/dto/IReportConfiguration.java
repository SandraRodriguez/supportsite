/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public interface IReportConfiguration {

    int getReportBackwardDateLimit();

    String getReportCode();

    String getReportId();

    void setReportBackwardDateLimit(int reportBackwardDateLimit);

    void setReportCode(String reportCode);

    void setReportId(String reportId);
    
}
