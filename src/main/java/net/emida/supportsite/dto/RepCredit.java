/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

import java.util.Calendar;
import java.math.BigDecimal;

/**
 *
 * @author fernandob
 */
public class RepCredit {

    private long id;
    private long repId;
    private String logonId;
    private String description;
    private BigDecimal amount;
    private BigDecimal creditLimit;
    private BigDecimal priorCreditLimit;
    private BigDecimal availableCredit;
    private BigDecimal priorAvailableCredit;
    private BigDecimal commission;
    private Calendar datetime;
    private long entityAccountType;
    private String idNew;
    private Calendar depositDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRepId() {
        return repId;
    }

    public void setRepId(long repId) {
        this.repId = repId;
    }

    public String getLogonId() {
        return logonId;
    }

    public void setLogonId(String logonId) {
        this.logonId = logonId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public BigDecimal getPriorCreditLimit() {
        return priorCreditLimit;
    }

    public void setPriorCreditLimit(BigDecimal priorCreditLimit) {
        this.priorCreditLimit = priorCreditLimit;
    }

    public BigDecimal getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(BigDecimal availableCredit) {
        this.availableCredit = availableCredit;
    }

    public BigDecimal getPriorAvailableCredit() {
        return priorAvailableCredit;
    }

    public void setPriorAvailableCredit(BigDecimal priorAvailableCredit) {
        this.priorAvailableCredit = priorAvailableCredit;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public Calendar getDatetime() {
        return datetime;
    }

    public void setDatetime(Calendar datetime) {
        this.datetime = datetime;
    }

    public long getEntityAccountType() {
        return entityAccountType;
    }

    public void setEntityAccountType(long entityAccountType) {
        this.entityAccountType = entityAccountType;
    }

    public String getIdNew() {
        return idNew;
    }

    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    public Calendar getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Calendar depositDate) {
        this.depositDate = depositDate;
    }

    public RepCredit() {
        this.id = 0L;
        this.repId = 0L;
        this.logonId = "";
        this.description = "";
        this.amount = BigDecimal.ZERO;
        this.creditLimit = BigDecimal.ZERO;
        this.priorCreditLimit = BigDecimal.ZERO;
        this.availableCredit = BigDecimal.ZERO;
        this.priorAvailableCredit = BigDecimal.ZERO;
        this.commission = BigDecimal.ZERO;
        this.datetime = Calendar.getInstance();
        this.entityAccountType = 0L;
        this.idNew = "";
        this.depositDate = Calendar.getInstance();
    }

    public RepCredit(long id, long repId, String logonId, String description,
            BigDecimal amount, BigDecimal creditLimit, BigDecimal priorCreditLimit,
            BigDecimal availableCredit, BigDecimal priorAvailableCredit,
            BigDecimal commission, Calendar datetime, long entityAccountType,
            String idNew, Calendar depositDate) {
        this.id = id;
        this.repId = repId;
        this.logonId = logonId;
        this.description = description;
        this.amount = amount;
        this.creditLimit = creditLimit;
        this.priorCreditLimit = priorCreditLimit;
        this.availableCredit = availableCredit;
        this.priorAvailableCredit = priorAvailableCredit;
        this.commission = commission;
        this.datetime = datetime;
        this.entityAccountType = entityAccountType;
        this.idNew = idNew;
        this.depositDate = depositDate;
    }

    @Override
    public String toString() {
        return "RepCredit{" + "id=" + id + ", repId=" + repId + ", logonId=" + logonId + ", description=" + description + ", amount=" + amount + ", creditLimit=" + creditLimit + ", priorCreditLimit=" + priorCreditLimit + ", availableCredit=" + availableCredit + ", priorAvailableCredit=" + priorAvailableCredit + ", commission=" + commission + ", datetime=" + datetime + ", entityAccountType=" + entityAccountType + ", idNew=" + idNew + ", depositDate=" + depositDate + '}';
    }

}
