/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.dto.fileTransfer;

/**
 *
 * @author emida
 */
public class FileTransferTaskConnection {
    
    private String id;
    private String fileTransferTaskTypeId;
    private String fileTransferTaskTypeName;
    private String fileTransferConnectionId;
    private String fileTransferConnectionName;
    private String description;
    private String fileNamePattern;
    private String sourceFolder;
    private String targetFolder;
    private String processedFolder;
    private boolean checkTransferredFiles;
    private int retriesOnTransferFail;
    private boolean taskEnabled;

    public FileTransferTaskConnection() {
        this.id = "";
        this.fileTransferTaskTypeId = "";
        this.fileTransferConnectionId = "";
        this.description = "";
        this.fileNamePattern = "";
        this.sourceFolder = "";
        this.targetFolder = "";
        this.processedFolder = "";
        this.checkTransferredFiles = false;
        this.retriesOnTransferFail = 0;
        this.taskEnabled = false;
    }

    public FileTransferTaskConnection(String id, String fileTransferTaskTypeId,
            String fileTransferTaskTypeName, String fileTransferConnectionId, 
            String fileTransferConnectionName, String description, String fileNamePattern, 
            String sourceFolder, String targetFolder, String processedFolder, 
            boolean checkTransferredFiles, int retriesOnTransferFail, boolean taskEnabled) {
        this.id = id;
        this.fileTransferTaskTypeId = fileTransferTaskTypeId;
        this.fileTransferTaskTypeName = fileTransferTaskTypeName;
        this.fileTransferConnectionId = fileTransferConnectionId;
        this.fileTransferConnectionName = fileTransferConnectionName;
        this.description = description;
        this.fileNamePattern = fileNamePattern;
        this.sourceFolder = sourceFolder;
        this.targetFolder = targetFolder;
        this.processedFolder = processedFolder;
        this.checkTransferredFiles = checkTransferredFiles;
        this.retriesOnTransferFail = retriesOnTransferFail;
        this.taskEnabled = taskEnabled;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileTransferTaskTypeId() {
        return fileTransferTaskTypeId;
    }

    public void setFileTransferTaskTypeId(String fileTransferTaskTypeId) {
        this.fileTransferTaskTypeId = fileTransferTaskTypeId;
    }

    public String getFileTransferConnectionId() {
        return fileTransferConnectionId;
    }

    public void setFileTransferConnectionId(String fileTransferConnectionId) {
        this.fileTransferConnectionId = fileTransferConnectionId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileNamePattern() {
        return fileNamePattern;
    }

    public void setFileNamePattern(String fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    public String getSourceFolder() {
        return sourceFolder;
    }

    public void setSourceFolder(String sourceFolder) {
        this.sourceFolder = sourceFolder;
    }

    public String getTargetFolder() {
        return targetFolder;
    }

    public void setTargetFolder(String targetFolder) {
        this.targetFolder = targetFolder;
    }

    public String getProcessedFolder() {
        return processedFolder;
    }

    public void setProcessedFolder(String processedFolder) {
        this.processedFolder = processedFolder;
    }

    public boolean isCheckTransferredFiles() {
        return checkTransferredFiles;
    }

    public void setCheckTransferredFiles(boolean checkTransferredFiles) {
        this.checkTransferredFiles = checkTransferredFiles;
    }

    public int getRetriesOnTransferFail() {
        return retriesOnTransferFail;
    }

    public void setRetriesOnTransferFail(int retriesOnTransferFail) {
        this.retriesOnTransferFail = retriesOnTransferFail;
    }

    public boolean isTaskEnabled() {
        return taskEnabled;
    }

    public void setTaskEnabled(boolean taskEnabled) {
        this.taskEnabled = taskEnabled;
    }    

    public String getFileTransferTaskTypeName() {
        return fileTransferTaskTypeName;
    }

    public void setFileTransferTaskTypeName(String fileTransferTaskTypeName) {
        this.fileTransferTaskTypeName = fileTransferTaskTypeName;
    }

    public String getFileTransferConnectionName() {
        return fileTransferConnectionName;
    }

    public void setFileTransferConnectionName(String fileTransferConnectionName) {
        this.fileTransferConnectionName = fileTransferConnectionName;
    }
}
