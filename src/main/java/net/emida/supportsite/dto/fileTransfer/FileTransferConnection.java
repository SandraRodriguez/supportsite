/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto.fileTransfer;

/**
 *
 * @author fernandob
 */
public class FileTransferConnection {

    private String id;
    private String description;
    private String host;
    private int port;
    private String fileTransferTypeId;
    private boolean connectionByOperation;
    private int connectionTimeout;
    private String connectionUser;
    private String connectionPassword;
    private boolean activeMode; // FTP Only property
    private boolean binaryTransfer; // FTP Only property
    private String sshConfigFilePath; // SFTP Only property
    private boolean connectionEnabled;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getFileTransferTypeId() {
        return fileTransferTypeId;
    }

    public void setFileTransferTypeId(String fileTransferTypeId) {
        this.fileTransferTypeId = fileTransferTypeId;
    }

    public boolean isConnectionByOperation() {
        return connectionByOperation;
    }

    public void setConnectionByOperation(boolean connectionByOperation) {
        this.connectionByOperation = connectionByOperation;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public String getConnectionUser() {
        return connectionUser;
    }

    public void setConnectionUser(String connectionUser) {
        this.connectionUser = connectionUser;
    }

    public String getConnectionPassword() {
        return connectionPassword;
    }

    public void setConnectionPassword(String connectionPassword) {
        this.connectionPassword = connectionPassword;
    }

    public boolean isActiveMode() {
        return activeMode;
    }

    public void setActiveMode(boolean activeMode) {
        this.activeMode = activeMode;
    }

    public boolean isBinaryTransfer() {
        return binaryTransfer;
    }

    public void setBinaryTransfer(boolean binaryTransfer) {
        this.binaryTransfer = binaryTransfer;
    }

    public String getSshConfigFilePath() {
        return sshConfigFilePath;
    }

    public void setSshConfigFilePath(String sshConfigFilePath) {
        this.sshConfigFilePath = sshConfigFilePath;
    }

    public boolean isConnectionEnabled() {
        return connectionEnabled;
    }

    public void setConnectionEnabled(boolean connectionEnabled) {
        this.connectionEnabled = connectionEnabled;
    }

    public FileTransferConnection() {
        this.id = "";
        this.description = "";
        this.host = "";
        this.port = 0;
        this.fileTransferTypeId = "";
        this.connectionByOperation = true;
        this.connectionTimeout = 4000;
        this.connectionUser = "";
        this.connectionPassword = "";
        this.activeMode = false;
        this.binaryTransfer = false;
        this.sshConfigFilePath = "";
        this.connectionEnabled = false;
    }

    public FileTransferConnection(String id, String description, String host,
            int port, String fileTransferTypeId, boolean connectionByOperation,
            int connectionTimeout, String connectionUser, String connectionPassword,
            boolean activeMode, boolean binaryTransfer, String sshConfigFilePath,
            boolean connectionEnabled) {
        this.id = id;
        this.description = description;
        this.host = host;
        this.port = port;
        this.fileTransferTypeId = fileTransferTypeId;
        this.connectionByOperation = connectionByOperation;
        this.connectionTimeout = connectionTimeout;
        this.connectionUser = connectionUser;
        this.connectionPassword = connectionPassword;
        this.activeMode = activeMode;
        this.binaryTransfer = binaryTransfer;
        this.sshConfigFilePath = sshConfigFilePath;
        this.connectionEnabled = connectionEnabled;
    }

    @Override
    public String toString() {
        return "FileTransferConnection{" + "id=" + id + ", description=" + description + ", host=" + host + ", port=" + port + ", fileTransferTypeId=" + fileTransferTypeId + ", connectionByOperation=" + connectionByOperation + ", connectionTimeout=" + connectionTimeout + ", connectionUser=" + connectionUser + ", connectionPassword=" + connectionPassword + ", activeMode=" + activeMode + ", binaryTransfer=" + binaryTransfer + ", sshConfigFilePath=" + sshConfigFilePath + ", connectionEnabled=" + connectionEnabled + '}';
    }

}
