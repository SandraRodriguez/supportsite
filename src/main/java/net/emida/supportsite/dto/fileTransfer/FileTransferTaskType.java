/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto.fileTransfer;

/**
 *
 * @author fernandob
 */
public class FileTransferTaskType {

    private String id;
    private String code;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FileTransferTaskType() {
        this.id = "";
        this.code = "";
        this.name = "";
    }

    public FileTransferTaskType(String id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    @Override
    public String toString() {
        return "FileTransferTaskType{" + "id=" + id + ", code=" + code + ", name=" + name + '}';
    }
}
