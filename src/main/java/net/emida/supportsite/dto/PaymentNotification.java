/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

import java.math.BigDecimal;

/**
 *
 * @author fernandob
 */
public class PaymentNotification {

    private long id;
    private long actorId;
    private String logonId;
    private String description;
    private BigDecimal amount;
    private BigDecimal creditLimit;
    private BigDecimal priorCreditLimit;
    private BigDecimal availableCredit;
    private BigDecimal priorAvailableCredit;
    private BigDecimal commission;
    private String datetime;
    private long entityAccountType;
    private String actorName;
    private long paymentNotificationType;
    private String smsNotificationPhone;
    private String mailNotificationAddress;
    private boolean notificationSent;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getActorId() {
        return actorId;
    }

    public void setActorId(long actorId) {
        this.actorId = actorId;
    }

    public String getLogonId() {
        return logonId;
    }

    public void setLogonId(String logonId) {
        this.logonId = logonId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }

    public BigDecimal getPriorCreditLimit() {
        return priorCreditLimit;
    }

    public void setPriorCreditLimit(BigDecimal priorCreditLimit) {
        this.priorCreditLimit = priorCreditLimit;
    }

    public BigDecimal getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(BigDecimal availableCredit) {
        this.availableCredit = availableCredit;
    }

    public BigDecimal getPriorAvailableCredit() {
        return priorAvailableCredit;
    }

    public void setPriorAvailableCredit(BigDecimal priorAvailableCredit) {
        this.priorAvailableCredit = priorAvailableCredit;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public long getEntityAccountType() {
        return entityAccountType;
    }

    public void setEntityAccountType(long entityAccountType) {
        this.entityAccountType = entityAccountType;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public long getPaymentNotificationType() {
        return paymentNotificationType;
    }

    public void setPaymentNotificationType(long paymentNotificationType) {
        this.paymentNotificationType = paymentNotificationType;
    }

    public String getSmsNotificationPhone() {
        return smsNotificationPhone;
    }

    public void setSmsNotificationPhone(String smsNotificationPhone) {
        this.smsNotificationPhone = smsNotificationPhone;
    }

    public String getMailNotificationAddress() {
        return mailNotificationAddress;
    }

    public void setMailNotificationAddress(String mailNotificationAddress) {
        this.mailNotificationAddress = mailNotificationAddress;
    }

    public boolean isNotificationSent() {
        return notificationSent;
    }

    public void setNotificationSent(boolean notificationSent) {
        this.notificationSent = notificationSent;
    }

    public PaymentNotification() {
        this.id = 0L;
        this.actorId = 0L;
        this.logonId = "";
        this.description = "";
        this.amount = BigDecimal.ZERO;
        this.creditLimit = BigDecimal.ZERO;
        this.priorCreditLimit = BigDecimal.ZERO;
        this.availableCredit = BigDecimal.ZERO;
        this.priorAvailableCredit = BigDecimal.ZERO;
        this.commission = BigDecimal.ZERO;
        this.datetime = "";
        this.entityAccountType = 0L;
        this.actorName = "";
        this.paymentNotificationType = 0L;
        this.smsNotificationPhone = "";
        this.mailNotificationAddress = "";
        this.notificationSent = false;
    }

    public PaymentNotification(long id, long actorId, String logonId,
            String description, BigDecimal amount, BigDecimal creditLimit,
            BigDecimal priorCreditLimit, BigDecimal availableCredit,
            BigDecimal priorAvailableCredit, BigDecimal commission,
            String datetime, long entityAccountType, String actorName,
            long paymentNotificationType, String smsNotificationPhone,
            String mailNotificationAddress, boolean notificationSent) {
        this.id = id;
        this.actorId = actorId;
        this.logonId = logonId;
        this.description = description;
        this.amount = amount;
        this.creditLimit = creditLimit;
        this.priorCreditLimit = priorCreditLimit;
        this.availableCredit = availableCredit;
        this.priorAvailableCredit = priorAvailableCredit;
        this.commission = commission;
        this.datetime = datetime;
        this.entityAccountType = entityAccountType;
        this.actorName = actorName;
        this.paymentNotificationType = paymentNotificationType;
        this.smsNotificationPhone = smsNotificationPhone;
        this.mailNotificationAddress = mailNotificationAddress;
        this.notificationSent = notificationSent;
    }

    @Override
    public String toString() {
        return "PaymentNotification{" + "id=" + id + ", actorId=" + actorId + ", logonId=" + logonId + ", description=" + description + ", amount=" + amount + ", credit_limit=" + creditLimit + ", priorCreditLimit=" + priorCreditLimit + ", availableCredit=" + availableCredit + ", priorAvailableCredit=" + priorAvailableCredit + ", commission=" + commission + ", datetime=" + datetime + ", entityAccountType=" + entityAccountType + ", actorName=" + actorName + ", paymentNotificationType=" + paymentNotificationType + ", smsNotificationPhone=" + smsNotificationPhone + ", mailNotificationAddress=" + mailNotificationAddress + ", notificationSent=" + notificationSent + '}';
    }

    
}
