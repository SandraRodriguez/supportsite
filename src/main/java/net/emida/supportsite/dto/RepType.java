/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.dto;

/**
 *
 * @author fernandob
 */
public class RepType {

    private long typeId;
    private String description;

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RepType() {
    }

    public RepType(long typeId, String description) {
        this.typeId = typeId;
        this.description = description;
    }

    @Override
    public String toString() {
        return "RepType{" + "typeId=" + typeId + ", description=" + description + '}';
    }

}
