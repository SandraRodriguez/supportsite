package net.emida.supportsite.mvc.dao;

import net.emida.supportsite.util.exceptions.TechnicalException;

import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
public interface BasicDao<E> {

    List<E> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    Long count(Map<String, Object> queryData) throws TechnicalException;

    Long countAll() throws TechnicalException;
}
