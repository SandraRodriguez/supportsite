package net.emida.supportsite.mvc.taxes.engine;


import net.emida.supportsite.mvc.taxes.TaxDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author nmartinez
 * @author Franky Villadiego
 *
 */
@Repository
public class TaxJdbcDao implements TaxDao {

    private static final Logger LOG = LoggerFactory.getLogger(TaxJdbcDao.class);
    
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate parameterJdbcTemplate;
    
     
    @Override
    public Long findTaxByIsoId(BigDecimal isoId) throws TechnicalException {
        StringBuilder query = new StringBuilder();
        LOG.debug("Finding current tax for iso {}", isoId);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("isoId", isoId);

        //TODO : Wrong return type. taxPercent is money type in database which is mapped into a BigDecimal.
        // Why return a Long type??? Is Decimal fraction not important??

        try {
            query.append("SELECT v.taxPercent ");
            query.append("FROM TAXES t WITH (NOLOCK) INNER JOIN taxvalues v WITH (NOLOCK) ON t.id = v.refid ");
            query.append("WHERE v.startDate <= getdate() AND v.endDate >= getdate() AND t.ISO_ID = :isoId ");
            Long tax = parameterJdbcTemplate.queryForObject(query.toString(), params, Long.class);
            return tax;            
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn("Caught exception {}. no tax for iso {}.", ex.getClass(), isoId);
            return null;
        } catch (DataAccessException ex) {
            LOG.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }


    @Override
    public BigDecimal findIsoTaxInfo(BigDecimal isoId, Date startDate, Date endDate) throws TechnicalException {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("isoId", isoId);
        return getTaxInfo(ISO_TAX_INFO, params, startDate, endDate);
    }

    @Override
    public BigDecimal findAgentTaxInfo(BigDecimal repId, Date startDate, Date endDate) throws TechnicalException {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("repId", repId);
        return getTaxInfo(AGENT_TAX_INFO, params, startDate, endDate);
    }

    @Override
    public BigDecimal findSubAgentTaxInfo(BigDecimal repId, Date startDate, Date endDate) throws TechnicalException {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("repId", repId);
        return getTaxInfo(SUB_AGENT_TAX_INFO, params, startDate, endDate);
    }

    @Override
    public BigDecimal findRepTaxInfo(BigDecimal repId, Date startDate, Date endDate) throws TechnicalException {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("repId", repId);
        return getTaxInfo(REP_TAX_INFO, params, startDate, endDate);
    }

    @Override
    public BigDecimal findMerchantTaxInfo(BigDecimal merchantId, Date startDate, Date endDate) throws TechnicalException {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("merchantId", merchantId);
        return getTaxInfo(MERCHANT_TAX_INFO, params, startDate, endDate);
    }

    @Override
    public BigDecimal findMerchant3TaxInfo(BigDecimal merchantId, Date startDate, Date endDate) throws TechnicalException {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("merchantId", merchantId);
        return getTaxInfo(MERCHANT_3_TAX_INFO, params, startDate, endDate);
    }


    private BigDecimal getTaxInfo(String sql, MapSqlParameterSource params, Date startDate, Date endDate) throws TechnicalException {
        try {
            params.addValue("startDate", startDate, Types.TIMESTAMP);
            params.addValue("endDate", endDate, Types.TIMESTAMP);
            return parameterJdbcTemplate.queryForObject(sql, params, BigDecimal.class);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn("Caught exception {}.", ex.getClass());
            return null;
        } catch (DataAccessException ex) {
            LOG.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }


    @Autowired @Qualifier("masterJdbcTemplate")
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired @Qualifier("masterNamedParamJdbcTemplate")
    public void setParameterJdbcTemplate(NamedParameterJdbcTemplate parameterJdbcTemplate) {
        this.parameterJdbcTemplate = parameterJdbcTemplate;
    }


    ////// QUERIES  \\\\\\\\

    private static String ISO_TAX_INFO = "SELECT TOP 1 v.taxPercent " +
            "FROM TAXES t WITH (NOLOCK) " +
            "INNER JOIN taxvalues v WITH (NOLOCK) ON t.id = v.refid " +
            "WHERE v.startDate <= :startDate AND v.endDate >= :endDate AND t.iso_id = :isoId " +
            "ORDER BY v.endDate DESC";

    private static String AGENT_TAX_INFO = "SELECT TOP 1 v.taxPercent " +
            "FROM TAXES t WITH (NOLOCK) " +
            "INNER JOIN taxvalues v WITH (NOLOCK) ON t.id = v.refid " +
            "INNER JOIN reps r WITH (NOLOCK) ON r.iso_id = t.iso_id " +
            "WHERE v.startDate <= :startDate AND v.endDate >= :endDate AND rep_id = :repId " +
            "ORDER BY v.endDate DESC";

    private static String SUB_AGENT_TAX_INFO = "select TOP 1 v.taxPercent " +
            "FROM taxes t WITH (NOLOCK) " +
            "INNER JOIN taxvalues v WITH (NOLOCK) ON t.id = v.refid " +
            "INNER JOIN reps r1 WITH (NOLOCK) ON r1.iso_id = t.iso_id " +
            "INNER JOIN reps r2 WITH (NOLOCK) ON r1.rep_id = r2.iso_id " +
            "WHERE v.startDate <= :startDate and v.endDate >= :endDate AND r2.rep_id = :repId " +
            "ORDER BY v.endDate DESC";

    private static String REP_TAX_INFO = "SELECT TOP 1 v.taxPercent " +
            "FROM TAXES t WITH (NOLOCK) " +
            "INNER JOIN taxvalues v WITH (NOLOCK) ON t.id = v.refid " +
            "INNER JOIN reps r1 WITH (NOLOCK) ON r1.iso_id = t.iso_id " +
            "INNER JOIN reps r2 WITH (NOLOCK) ON r1.rep_id = r2.iso_id " +
            "INNER JOIN reps r3 WITH (NOLOCK) ON r2.rep_id = r3.iso_id " +
            "WHERE v.startDate <= :startDate and v.endDate >= :endDate AND r3.rep_id = :repId " +
            "ORDER BY v.endDate DESC";

    private static String MERCHANT_TAX_INFO = "SELECT TOP 1 v.taxPercent " +
            "FROM TAXES t WITH (NOLOCK) " +
            "INNER JOIN taxvalues v WITH (NOLOCK) ON t.id = v.refid " +
            "INNER JOIN reps r1 WITH (NOLOCK) ON r1.iso_id = t.iso_id " +
            "INNER JOIN reps r2 WITH (NOLOCK) ON r1.rep_id = r2.iso_id " +
            "INNER JOIN reps r3 WITH (NOLOCK) ON r2.rep_id = r3.iso_id " +
            "INNER JOIN merchants m WITH (NOLOCK) ON r3.rep_id = m.rep_id " +
            "WHERE v.startDate <= :startDate and v.endDate >= :endDate AND m.merchant_id = :merchantId " +
            "ORDER BY v.endDate DESC";

    private static String MERCHANT_3_TAX_INFO = "SELECT TOP 1 v.taxPercent " +
            "FROM TAXES t WITH (NOLOCK) " +
            "INNER JOIN taxvalues v WITH (NOLOCK) ON t.id = v.refid " +
            "INNER JOIN reps r WITH (NOLOCK) ON r.iso_id = t.iso_id " +
            "INNER JOIN merchants m WITH (NOLOCK) ON r.rep_id = m.rep_id " +
            "WHERE v.startDate <= :startDate AND v.endDate >= :endDate AND m.merchant_id = :merchantId " +
            "ORDER BY v.endDate DESC";

    
}
