package net.emida.supportsite.mvc.taxes;


import net.emida.supportsite.util.exceptions.TechnicalException;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author nmartinez
 * @author Franky Villadiego
 */
public interface TaxDao {
    
    Long findTaxByIsoId(BigDecimal isoId) throws TechnicalException;;

    BigDecimal findIsoTaxInfo(BigDecimal isoId, Date startDate, Date endDate) throws TechnicalException;

    BigDecimal findAgentTaxInfo(BigDecimal repId, Date startDate, Date endDate) throws TechnicalException;

    BigDecimal findSubAgentTaxInfo(BigDecimal repId, Date startDate, Date endDate) throws TechnicalException;

    BigDecimal findRepTaxInfo(BigDecimal repId, Date startDate, Date endDate) throws TechnicalException;

    BigDecimal findMerchantTaxInfo(BigDecimal merchantId, Date startDate, Date endDate) throws TechnicalException;

    BigDecimal findMerchant3TaxInfo(BigDecimal merchantId, Date startDate, Date endDate) throws TechnicalException;

}
