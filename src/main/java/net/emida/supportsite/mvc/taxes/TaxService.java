package net.emida.supportsite.mvc.taxes;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Franky Villadiego
 */

public interface TaxService {


    BigDecimal getTax(String mxValueAddedTax);

    BigDecimal getTax(long refId, int accessLevel, int distChainType, Date theDate);



}
