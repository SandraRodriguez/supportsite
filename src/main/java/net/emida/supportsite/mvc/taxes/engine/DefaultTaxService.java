package net.emida.supportsite.mvc.taxes.engine;


import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.mvc.taxes.TaxDao;
import net.emida.supportsite.mvc.taxes.TaxService;
import net.emida.supportsite.mvc.timezones.TimeZoneDao;
import net.emida.supportsite.mvc.timezones.TimeZoneDates;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Franky Villadiego
 */

@Service
public class DefaultTaxService implements TaxService {


    private static final Logger log = LoggerFactory.getLogger(DefaultTaxService.class);

    private TaxDao taxDao;
    private TimeZoneDao timeZoneDao;


    @Override
    public BigDecimal getTax(String mxValueAddedTax) {
        return buildResult(new BigDecimal(mxValueAddedTax));
    }

    @Override
    public BigDecimal getTax(long refId, int accessLevel, int distChainType, Date date) {

        Date theDate = getDate(refId, accessLevel, date);
        String strAccessLevel = String.valueOf(accessLevel);
        String strDistChain = String.valueOf(distChainType);
        BigDecimal result = null;
        if(strAccessLevel.equals(DebisysConstants.CARRIER)){
            result = new BigDecimal(0.0);
        }else if(strAccessLevel.equals(DebisysConstants.ISO)){
            result = taxDao.findIsoTaxInfo(new BigDecimal(refId), theDate, theDate);
        }else if(strAccessLevel.equals(DebisysConstants.AGENT) ||
                (strAccessLevel.equals(DebisysConstants.REP) && strDistChain.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))){
            result = taxDao.findAgentTaxInfo(new BigDecimal(refId), theDate, theDate);
        }else if(strAccessLevel.equals(DebisysConstants.SUBAGENT)){
            result = taxDao.findSubAgentTaxInfo(new BigDecimal(refId), theDate, theDate);
        }else if(strAccessLevel.equals(DebisysConstants.REP) && strDistChain.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
            result = taxDao.findRepTaxInfo(new BigDecimal(refId), theDate, theDate);
        }else if(strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChain.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
            result = taxDao.findMerchantTaxInfo(new BigDecimal(refId), theDate, theDate);
        }else if(strAccessLevel.equals(DebisysConstants.MERCHANT) && strDistChain.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
            result = taxDao.findMerchant3TaxInfo(new BigDecimal(refId), theDate, theDate);
        }

        return buildResult(result);
    }


    private Date getDate(long refId, int accesLevel, Date theDate) {
        try {
            TimeZoneDates tsd = timeZoneDao.getTimeZoneFilterDates(accesLevel, new BigDecimal(refId), theDate, theDate, false);
            Timestamp startDate = tsd.getStartDate();
            return new Date(startDate.getTime());
        }catch (Exception ex){
            log.error("Caught error {} getting time zones. Ex: {}", ex.getClass(), ex);
        }
        return DateUtil.atStartOfDay(theDate);
    }


    private BigDecimal buildResult(BigDecimal result){
        BigDecimal one = new BigDecimal("1.0");
        if(result == null) result = new BigDecimal(0.0);
        result = result.divide(new BigDecimal(100));
        return one.add(result);
    }


    @Autowired
    public void setTaxDao(TaxDao taxDao) {
        this.taxDao = taxDao;
    }

    @Autowired
    public void setTimeZoneDao(TimeZoneDao timeZoneDao) {
        this.timeZoneDao = timeZoneDao;
    }
}
