package net.emida.supportsite.mvc.security.filter;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import net.emida.supportsite.mvc.security.TokenHolder;

/**
 * 
 * @author cmercado
 *
 */
public class LoginTokenSessionListener implements HttpSessionListener {
	
	private static final Logger logger = Logger.getLogger(LoginTokenSessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		logger.debug("Session created. " + se.getSession().getId());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSession httpSession = se.getSession();
		String token = (String) httpSession.getAttribute(TokenHolder.TOKEN_ATTR);
		
		if(StringUtils.isNotEmpty(token)) {
			logger.info(String.format("Session being destroyed sessionId, token [%s,%s]", se.getSession().getId(), token));
			TokenHolder tokenHolder = (TokenHolder) httpSession.getServletContext().getAttribute(TokenHolder.TOKEN_HOLDER_ATTR);
			if(tokenHolder != null){
				tokenHolder.removeTokenData(token);
			}else{
				logger.warn("TokenHolder missing!!");
			}
		} else {
			logger.debug("Session destroyed " + se.getSession().getId());
		}
	}

}
