package net.emida.supportsite.mvc.security;

/**
 * @author Franky Villadiego
 */

public class NgMultiISOLoginRequest {

    private String isoId;
    private String userName;
    private String email;
    private boolean intranetUser;
    private String language;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIsoId() {
        return isoId;
    }

    public void setIsoId(String isoId) {
        this.isoId = isoId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isIntranetUser() {
        return intranetUser;
    }

    public void setIntranetUser(boolean intranetUser) {
        this.intranetUser = intranetUser;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "NgMultiISOLoginRequest{" +
                "isoId='" + isoId + '\'' +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", intranetUser=" + intranetUser +
                ", language='" + language + '\'' +
                '}';
    }
}
