package net.emida.supportsite.mvc.security;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Franky Villadiego
 */
public class NGLoginResponse {


    private Boolean logged;
    private List<String> messages = new ArrayList<>();
    private String token;

    public NGLoginResponse() {
    }

    public NGLoginResponse(Boolean logged) {
        this.logged = logged;
    }

    public Boolean getLogged() {
        return logged;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public void addMessage(String message){
        messages.add(message);
    }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
