package net.emida.supportsite.mvc.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import net.emida.supportsite.mvc.security.TokenData;
import net.emida.supportsite.mvc.security.TokenHolder;

/**
 * 
 * @author cmercado
 *
 */
public class LoginTokenFilter implements Filter {
	
	private static final Logger logger = Logger.getLogger(LoginTokenFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.debug("Initializing filter");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String token = request.getParameter(TokenHolder.TOKEN_NG_PARAMETER);
		HttpServletRequest httpReq = (HttpServletRequest) request;
		logger.debug(String.format("Session Id: %s ssToken: %s", httpReq.getSession().getId(), token));
		
		if(StringUtils.isNotEmpty(token)) {
			HttpSession httpSession = httpReq.getSession();
			TokenHolder tokenHolder = (TokenHolder) httpSession.getServletContext().getAttribute(TokenHolder.TOKEN_HOLDER_ATTR);
			//TokenHolder tokenHolder = TokenHolder.getInstance();
			TokenData data = tokenHolder.getTokenData(token);
			httpSession.setAttribute("SessionData", data.getSessionData());
		}
		
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		logger.debug("destroy");
	}

}
