package net.emida.supportsite.mvc.security;

import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

/**
 * 
 * @author cmercado
 *
 */
public class TokenHolder {
	
	private static final Logger logger = Logger.getLogger(TokenHolder.class);

	public static final String TOKEN_HOLDER_ATTR = "NG_TOKEN_HOLDER";
	public static final String TOKEN_ATTR = "NG_TOKEN";
	public static final String TOKEN_NG_PARAMETER = "ssToken";

	//private static TokenHolder instance;

	private final ConcurrentHashMap<String, TokenData> holder = new ConcurrentHashMap<>();
	
	public TokenHolder() {

	}
	
/*	public static TokenHolder getInstance() {
		if(instance == null) {
			synchronized(TokenHolder.class) {
				instance = new TokenHolder();
			}
		}
		return instance;
	}*/
	
	public void addTokenData(TokenData data) {
		if(!holder.containsKey(data.getToken())) {
			holder.put(data.getToken(), data);
			logger.info("Adding token data in session token holder. " + data.toString());
		} else {
			logger.info("Token data already exists in session token holder. " + data.toString());
		}
	}
	
	public TokenData getTokenData(String token) {
		TokenData data = new TokenData();
		if(holder.containsKey(token)) {
			data = holder.get(token);
		}
		return data;
	}
	
	public void removeTokenData(String token) {
		TokenData data = holder.remove(token);
		if(data != null) {
			logger.info("Token data removed from session. " + data.toString());
		} else {
			logger.info("Token data not found");
		}
	}
}
