package net.emida.supportsite.mvc.security;

import net.emida.supportsite.mvc.security.dao.PasswordPojo;

/**
 * @author Franky Villadiego
 */
public interface PasswordDao {


    PasswordPojo findByUsername(String userName);

}
