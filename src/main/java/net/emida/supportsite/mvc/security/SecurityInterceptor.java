package net.emida.supportsite.mvc.security;

import net.emida.supportsite.mvc.SupportSiteController;
import net.emida.supportsite.mvc.UrlPaths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(SecurityInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long startTime = System.currentTimeMillis();
        log.debug("PreHandle Checking security starting");

        HandlerMethod handlerMethod = (HandlerMethod)handler;
        SupportSiteController supportSiteController =(SupportSiteController) handlerMethod.getBean();

        int section = supportSiteController.getSection();
        int sectionPage = supportSiteController.getSectionPage();
        request.setAttribute("section", section);
        request.setAttribute("section_page", sectionPage);
        log.debug("##### section = {} ####", section);
        log.debug("##### section_page = {} ####", sectionPage);
        SecurityChecker securityChecker =  new SecurityChecker();

        Map model = new HashMap();
        securityChecker.check(request, request.getSession(), request.getSession().getServletContext(), section, sectionPage, model);
        log.debug("Model = {}", model);
        String messageId =(String) model.get("message");
        long finishTime = System.currentTimeMillis();
        log.debug("PreHandle finishing at {} milliseconds", finishTime - startTime);
        if(messageId != null){
            //response.sendRedirect("/support/message.jsp?message=" + messageId);
            response.sendRedirect(UrlPaths.CONTEXT_PATH + UrlPaths.MESSAGE_PATH  + "?message=" + messageId);
            return false;
        }

        return true;

    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.debug("PostHandle");
        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.debug("After Completion");
        super.afterCompletion(request, response, handler, ex);
    }
}
