package net.emida.supportsite.mvc.security;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.debisys.exceptions.UserException;
import com.debisys.users.DBUser;
import com.debisys.users.IntranetUser;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.DebisysConstants;

import net.emida.supportsite.mvc.security.dao.PasswordPojo;
import net.emida.supportsite.util.security.HmacUtil;

/**
 * @author Franky Villadiego
 */

@RestController
public class NgLoginController {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(NgLoginController.class);

    private static final String SESSION_DATA = "SessionData";
    private PasswordDao passwordDao;


    @RequestMapping(value = "/login/ng", method = RequestMethod.POST)
    public NGLoginResponse loginNg(@RequestBody NgLoginRequest loginRequest, HttpServletRequest request,
                           HttpServletResponse response, HttpSession httpSession){

        logger.info("Doing login from SupportSite-NG");
        logger.info("LoginRequest:" + loginRequest);

        NGLoginResponse ngLoginResponse = new NGLoginResponse(false);
        ngLoginResponse.addMessage("Init login...");

        SessionData sessionSupportSite = new SessionData();
        httpSession.setAttribute("SessionData", sessionSupportSite);

        User user = new User();
        user.setSupportSiteNgUser(true);
        user.setUsername(loginRequest.getUsername());
        user.setPassword(loginRequest.getPassword());
        String language = sessionSupportSite.getLanguage();
        user.setLanguage(language);
        
        createTokenInHttpSession(httpSession, sessionSupportSite, ngLoginResponse);

        boolean result = validateUserSupportSite(request, httpSession, sessionSupportSite, user, false, ngLoginResponse);

        ngLoginResponse.setLogged(result);

        return ngLoginResponse;

    }

	private void createTokenInHttpSession(HttpSession httpSession, SessionData sessionSupportSite, NGLoginResponse ngLoginResponse) {
		String hmacToken = createHmacToken(httpSession);
		ngLoginResponse.setToken(hmacToken);
		TokenHolder tokenHolder = getTokenHolder(httpSession.getServletContext());
        createTokenDataInSession(hmacToken, sessionSupportSite, tokenHolder);
        httpSession.setAttribute(TokenHolder.TOKEN_ATTR, hmacToken);
	}

    private TokenHolder getTokenHolder(ServletContext servletContext) {
        TokenHolder tokenHolder = (TokenHolder) servletContext.getAttribute(TokenHolder.TOKEN_HOLDER_ATTR);
        if(tokenHolder == null){
            tokenHolder = new TokenHolder();
            servletContext.setAttribute(TokenHolder.TOKEN_HOLDER_ATTR, tokenHolder);
        }
        return tokenHolder;
    }

    private String createHmacToken(HttpSession httpSession) {
    	Date now = new Date();
        String token = httpSession.getId() + ":" + now.getTime();
        String hmac = null;
        try {
			hmac = HmacUtil.calculateHMAC(token, "ngtoken");
		} catch (GeneralSecurityException e) {
			logger.error(e.getMessage(), e);
		}
        return hmac;
    }
    
    private void createTokenDataInSession(String hmacToken, SessionData sessionSupportSite, TokenHolder tokenHolder) {
    	TokenData data = new TokenData();
		data.setToken(hmacToken);
		data.setTimestamp(new Date());
		data.setSessionData(sessionSupportSite);
		//TokenHolder holder = TokenHolder.getInstance();
		tokenHolder.addTokenData(data);
    }

    @RequestMapping(value = "/login/ng/multi-iso", method = RequestMethod.POST)
    public NGLoginResponse loginISO(@RequestBody NgMultiISOLoginRequest loginRequest, HttpServletRequest request,
                                    HttpServletResponse response, HttpSession httpSession){

        logger.info("Doing login for MultiISO from SupportSite-NG .");

        NGLoginResponse ngLoginResponse = new NGLoginResponse(false);
        ngLoginResponse.addMessage("Init login for MultiISO...");

        ngLoginResponse.addMessage("Looking for password..");
        PasswordPojo passPojo = passwordDao.findByUsername(loginRequest.getUserName());
        if(passPojo == null){
            ngLoginResponse.addMessage("No password data for user " + loginRequest.getUserName());
            logger.error("No user login for MultiISO user " + loginRequest.getUserName());
            ngLoginResponse.setLogged(false);
            return ngLoginResponse;
        }

        SessionData sessionSupportSite = new SessionData();
        httpSession.setAttribute(SESSION_DATA, sessionSupportSite);

        User user = new User();
        user.setSupportSiteNgUser(true);
        user.setUsername(loginRequest.getUserName());
        user.setPassword(passPojo.getLogonPassword());
        String language = sessionSupportSite.getLanguage();
        user.setLanguage(language);
        
        createTokenInHttpSession(httpSession, sessionSupportSite, ngLoginResponse);

        boolean result = validateUserSupportSite(request, httpSession, sessionSupportSite, user, true, ngLoginResponse);
        ngLoginResponse.setLogged(result);
        return ngLoginResponse;
    }

    @RequestMapping(value = "/login/ng/intranet", method = RequestMethod.GET)
    public NGLoginResponse loginNgIntranet(@RequestParam String userId, @RequestParam(required = false) String drep, HttpServletRequest request,
                                           HttpServletResponse response, HttpSession httpSession){

        logger.info("Doing login from SupportSite-NG and Intranet...");
        logger.info("Doing login from SupportSite-NG and Intranet..");
        logger.info("UserId :" + userId);

        int userIdInt = Integer.parseInt(userId);

        NGLoginResponse ngLoginResponse = new NGLoginResponse(false);
        ngLoginResponse.addMessage("Init login for Intranet...");

        boolean result = false;

        ngLoginResponse.addMessage("Validating info for intranet user id " + userId);
        IntranetUser intranetUser = IntranetUser.validateInfoByUserIdIntranet(userIdInt, "");
        if(intranetUser.hasError()){
            ngLoginResponse.addMessage("Intranet info validation failed");
            ngLoginResponse.setLogged(false);
            return ngLoginResponse;
        }else{

            ngLoginResponse.addMessage("Building user info...");
            User user = new User();

            String prefixUserName = (String)httpSession.getServletContext().getAttribute("prefix_user_intranet");
            user.setUsername(prefixUserName + intranetUser.getUserName());
            user.setIntranetUser(true);
            user.setIntranetUserId(userIdInt);
            user.setIdNew(intranetUser.getIdNew());
            user.setSupportSiteNgUser(true);

            SessionData sessionData = (SessionData) httpSession.getAttribute(SESSION_DATA);
            if(sessionData != null && sessionData.isLoggedIn()){
                ngLoginResponse.addMessage("User is logged..");
                sessionData.setProperty("drep", userId);
                httpSession.setAttribute("expiryDiffDays", 0);
                ngLoginResponse.setLogged(true);
                createTokenInHttpSession(httpSession, sessionData, ngLoginResponse);
                return ngLoginResponse;
            }

            result = validateIntranetUserSupportSite(httpSession, user, userId, ngLoginResponse);
            ngLoginResponse.setLogged(result);
        }

        return ngLoginResponse;

    }


    private boolean validateUserSupportSite(HttpServletRequest request, HttpSession httpSession, SessionData ssSessionData, User user,
                                            boolean multiIso, NGLoginResponse ngLoginResponse) {

        logger.info("Trying lo log in user in old SupportSite...");
        try {

            ngLoginResponse.addMessage("Validating login...");
            ServletContext context = request.getSession().getServletContext();
            boolean loginSuccess = user.validateLogin(context, ssSessionData, multiIso);

            if(loginSuccess){
                ngLoginResponse.addMessage("Login validation OK.");
                httpSession.setAttribute("loginAttempts", 0);
                httpSession.setAttribute("showCaptcha", false);

                setAsLoggedIn(request, httpSession, ssSessionData, user, ngLoginResponse);

                return true;
            }

        }catch (Exception ex){
            logger.error("Error trying to log in user in old SupportSite.", ex);
            ngLoginResponse.addMessage("Login validation Exception. Ex: " + ex.getMessage());
            return false;
        }
        ngLoginResponse.addMessage("Login validation Failed.");
        return false;
    }

/*    private boolean validateMultiIsoUserSupperSite(NGLoginResponse ngLoginResponse){
        logger.info("Trying lo log in Multi Iso user in old SupportSite...");
        try{

            ngLoginResponse.addMessage("Validating MultiIso login...");

        }catch (Exception ex){
            logger.error("Error trying to log in MultiIso user in old SupportSite.", ex);
            ngLoginResponse.addMessage("Login validation Exception. Ex: " + ex.getMessage());
            return false;
        }
        ngLoginResponse.addMessage("MultiIso Login validation Failed.");
        return false;
    }*/


    private boolean validateIntranetUserSupportSite(HttpSession httpSession, User user, String drep, NGLoginResponse ngLoginResponse) {

        logger.info("Trying lo log in user in old SupportSite...");
        try {

            ngLoginResponse.addMessage("Building Intranet user info...");

            SessionData sessionData = new SessionData();
            String defaultISO = (String) httpSession.getServletContext().getAttribute("iso_default");

            user.setRefId(defaultISO);
            user.setRefType(DebisysConstants.PW_REF_TYPE_REP_ISO);

            fillSessionBasicData(sessionData, user, drep, defaultISO, ngLoginResponse);

            fillPermissions(httpSession, sessionData, ngLoginResponse);

            sessionData.setIsLoggedIn(true);
            sessionData.setUser(user);
            httpSession.setAttribute("SessionData", sessionData);
            createTokenInHttpSession(httpSession, sessionData, ngLoginResponse);
            ngLoginResponse.setLogged(true);

            return true;

        }catch (Exception ex){
            logger.error("Error trying to log in user in old SupportSite.", ex);
            ngLoginResponse.addMessage("Failed intranet build information..");
            ngLoginResponse.setLogged(false);
            return false;
        }
    }


    private void fillSessionBasicData(SessionData sessionData, User user, String drep, String defaultIso, NGLoginResponse ngLoginResponse) {

        ngLoginResponse.addMessage("Setting basic properties for user...");
        sessionData.setProperty("drep", drep);
        sessionData.setProperty("username", user.getUsername());
        sessionData.setProperty("password", user.getPassword());
        sessionData.setProperty("ref_id", defaultIso);
        sessionData.setProperty("ref_type", user.getRefType());
        //********************************************************************
        //This means that if you enter by this point, all permission will added.
        sessionData.setProperty("access_level", DebisysConstants.ISO);
        //********************************************************************

        //determines always like ISO level
        sessionData.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_3_LEVEL);
        sessionData.setProperty("iso_id", defaultIso);
    }

    private void fillPermissions(HttpSession httpSession, SessionData sessionData, NGLoginResponse ngLoginResponse){

        ngLoginResponse.addMessage("Loading and setting permission info..");

        String[] blackListPermissions = null;
        if (httpSession.getServletContext().getAttribute("blackListPermissions") != null) {
            blackListPermissions = ((String) httpSession.getServletContext().getAttribute("blackListPermissions")).split("\\|");
        }

        ngLoginResponse.addMessage("Searching permission info..");

        ArrayList<String> permissionsId = DBUser.getAllPermissionsId(blackListPermissions);
        for (String idPermission : permissionsId) {
            sessionData.setPermission(idPermission);
        }

        ngLoginResponse.addMessage("Permission info done!!");
    }

    private void setAsLoggedIn(HttpServletRequest request, HttpSession httpSession, SessionData ssSessionData, User user, NGLoginResponse ngLoginResponse) throws UserException, IOException {

        ngLoginResponse.addMessage("Setting properties AS LOGGED IN...");

        ssSessionData.clear();

		/*R34 Support Site Revamp*/
        ssSessionData.setProperty("name", user.getName());

		/*End R34 Support Site Revamp*/
        ssSessionData.setProperty("username", user.getUsername());
        ssSessionData.setProperty("password", user.getPassword());
        ssSessionData.setProperty("ref_id", user.getRefId());
        ssSessionData.setProperty("ref_type", user.getRefType());
        ssSessionData.setProperty("access_level", user.getAccessLevel());

        //determines 3 level or 5 level layout
        ssSessionData.setProperty("dist_chain_type", user.getDistChainType());
        ssSessionData.setProperty("company_name", user.getCompanyName());
        ssSessionData.setProperty("contact_name", user.getContactName());
        ssSessionData.setProperty("ip_address", request.getRemoteAddr());

        ssSessionData.setProperty("iso_id", user.getISOId(user.getAccessLevel(),user.getRefId()));
        ssSessionData.setProperty("datasource", user.getDatasource());

        //set permissions for this user
        ngLoginResponse.addMessage("Loading and setting permissions AS LOGGED IN...");
        user.setPermissions(ssSessionData);

        if(ssSessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            user.setCarrierUserProducts(ssSessionData);
        }
        ssSessionData.setIsLoggedIn(true);
        ssSessionData.setUser(user);

        httpSession.setAttribute("expiryDiffDays", 0);

        ngLoginResponse.addMessage("Process for AS LOGGED IN was correctly executed!!");

    }

    @Autowired
    public void setPasswordDao(PasswordDao passwordDao) {
        this.passwordDao = passwordDao;
    }

}
