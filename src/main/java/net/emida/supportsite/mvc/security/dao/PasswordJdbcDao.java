package net.emida.supportsite.mvc.security.dao;


import net.emida.supportsite.mvc.security.PasswordDao;
import net.emida.supportsite.util.RSUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Franky Villadiego
 */
@Repository
public class PasswordJdbcDao implements PasswordDao {

    private static final Logger log = LoggerFactory.getLogger(PasswordJdbcDao.class);



    private static String FIND_BY_USERNAME = "SELECT TOP 1 p.* FROM password p WITH(NOLOCK) WHERE p.logon_id=:login";



    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate parameterJdbcTemplate;



    public PasswordPojo findByUsername(String username) throws TechnicalException {
        try {
            log.debug("Finding Password by username {}.", username);
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("login", username);

            return parameterJdbcTemplate.queryForObject(FIND_BY_USERNAME, params, new RowMapper<PasswordPojo>() {
                @Override
                public PasswordPojo mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return buildPassword(rs);
                }
            });
        }catch (EmptyResultDataAccessException ex){
            log.warn("Caught exception {}. Password by username {} not found.", ex.getClass(), username);
            return null;
        } catch (DataAccessException ex){
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }




    public PasswordPojo buildPassword(ResultSet rs){
        PasswordPojo ppi = new PasswordPojoImpl(rs);
        return ppi;
    }


    @Autowired @Qualifier("masterJdbcTemplate")
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired @Qualifier("masterNamedParamJdbcTemplate")
    public void setParameterJdbcTemplate(NamedParameterJdbcTemplate parameterJdbcTemplate) {
        this.parameterJdbcTemplate = parameterJdbcTemplate;
    }
}

class PasswordPojoImpl extends PasswordPojo{

    private transient ResultSet rs;

    public PasswordPojoImpl(ResultSet rs) {
        this.rs = rs;
        this.id = RSUtil.getInteger(rs, "Password_id").toString();
        this.refId = RSUtil.getBigDecimal(rs, "Ref_id");
        this.refType = RSUtil.getInteger(rs, "Ref_type");
        this.logonId = RSUtil.getString(rs, "Logon_id");
        this.logonPassword = RSUtil.getString(rs, "Logon_password");
        this.admin = RSUtil.getBoolean(rs, "Admin");
        this.status = RSUtil.getInteger(rs, "Status");
        this.statusLastChange = RSUtil.getTimestamp(rs, "StatusLastChange");
        this.name = RSUtil.getString(rs, "Name");
        this.email = RSUtil.getString(rs, "Email");
        this.encrypted = RSUtil.getBoolean(rs, "isEncrypted");
        this.lastSuccessfulLogin = RSUtil.getTimestamp(rs, "lastSuccessfulLogin");
        this.lastPasswordChange = RSUtil.getTimestamp(rs, "lastPasswordChange");
    }

}