package net.emida.supportsite.mvc.security;

import net.emida.supportsite.util.exceptions.TechnicalException;

import java.util.List;

/**
 * @author Franky Villadiego
 */

public interface PermissionsDao {


    List<AssignablePermission> findAssignablePermissions(Long isoId, String accessLevel) throws TechnicalException;
}
