package net.emida.supportsite.mvc.security.dao;

import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.mvc.security.AssignablePermission;
import net.emida.supportsite.mvc.security.PermissionsDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Franky Villadiego
 */

@Repository
public class PermissionsJdbcDao implements PermissionsDao {

    private static final Logger log = LoggerFactory.getLogger(PermissionsJdbcDao.class);
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate parameterJdbcTemplate;


    public List<AssignablePermission> findAssignablePermissions(Long isoId, String accessLevel) throws TechnicalException {
        try {
            log.debug("Finding assignable web iso permissions for iso id {} and access level {}.", isoId, accessLevel);

            StringBuilder sb = new StringBuilder(GET_ASSIGNABLE_PERMS);

            if(DebisysConstants.ISO.equalsIgnoreCase(accessLevel)){
                sb.append("AND wip.iso='Y' ");
            }else if(DebisysConstants.AGENT.equalsIgnoreCase(accessLevel)){
                sb.append("AND wip.agent='Y' ");
            }else if(DebisysConstants.SUBAGENT.equalsIgnoreCase(accessLevel)){
                sb.append("AND wip.subagent='Y' ");
            }else if(DebisysConstants.REP.equalsIgnoreCase(accessLevel)){
                sb.append("AND wip.rep='Y' ");
            }else if(DebisysConstants.REFERRAL.equalsIgnoreCase(accessLevel)){
                sb.append("AND wip.ref='Y' ");
            }else if(DebisysConstants.CARRIER.equalsIgnoreCase(accessLevel)){
                sb.append("AND wip.carrier='Y' ");
            }else{
                sb.append("AND wip.merchant='Y' ");
            }
            sb.append("ORDER BY wip.web_permission_type_id");

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("isoId", isoId);
            return parameterJdbcTemplate.query(sb.toString(), params, new BeanPropertyRowMapper(AssignablePermission.class));
        }catch (EmptyResultDataAccessException ex){
            log.warn("Caught exception {}. Iso id {} not found.", ex.getClass(), isoId);
            return new ArrayList<AssignablePermission>();
        } catch (DataAccessException ex){
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }


    @Autowired @Qualifier("masterJdbcTemplate")
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired @Qualifier("masterNamedParamJdbcTemplate")
    public void setParameterJdbcTemplate(NamedParameterJdbcTemplate parameterJdbcTemplate) {
        this.parameterJdbcTemplate = parameterJdbcTemplate;
    }


    private static String GET_ASSIGNABLE_PERMS = "SELECT wip.web_permission_type_id, wpt.description " +
            "FROM web_iso_permissions AS wip WITH(NOLOCK) " +
            "INNER JOIN web_permission_types AS wpt WITH(NOLOCK) ON wip.web_permission_type_id = wpt.web_permission_type_id " +
            "WHERE wip.iso_id=:isoId ";

}
