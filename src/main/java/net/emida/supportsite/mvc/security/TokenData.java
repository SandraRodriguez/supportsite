package net.emida.supportsite.mvc.security;

import java.util.Date;

import com.debisys.users.SessionData;

public class TokenData {

	private String token;
	private Date timestamp;
	private SessionData sessionData;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public SessionData getSessionData() {
		return sessionData;
	}
	public void setSessionData(SessionData sessionData) {
		this.sessionData = sessionData;
	}
	
	@Override
	public String toString() {
		return "TokenData [token=" + token + ", timestamp=" + timestamp + "]";
	}
}
