package net.emida.supportsite.mvc.security;

/**
 * @author Franky Villadiego
 */

public class NgLoginRequest {

    private String username;
    private String password;
    private String language;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "NgLoginRequest{" +
                "username='" + username + '\'' +
                ", password='****"  + '\'' +
                ", language='" + language + '\'' +
                '}';
    }
}
