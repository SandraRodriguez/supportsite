package net.emida.supportsite.mvc.security;

/**
 * @author Franky Villadiego
 */

public class AssignablePermission {

    protected Integer webPermissionTypeId;
    protected String description;


    public AssignablePermission() {
    }

    public AssignablePermission(Integer webPermissionTypeId, String description) {
        this.webPermissionTypeId = webPermissionTypeId;
        this.description = description;
    }

    public Integer getWebPermissionTypeId() {
        return webPermissionTypeId;
    }

    public void setWebPermissionTypeId(Integer webPermissionTypeId) {
        this.webPermissionTypeId = webPermissionTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
