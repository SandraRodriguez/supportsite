package net.emida.supportsite.mvc.timezones;

import java.sql.Timestamp;

/**
 * @author Franky Villadiego
 */

public class TimeZoneDates {

    private Timestamp startDate;
    private Timestamp endDate;


    public TimeZoneDates() {
    }

    public TimeZoneDates(Timestamp startDate, Timestamp endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }


    @Override
    public String toString() {
        return "TimeZoneDates[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ']';
    }
}
