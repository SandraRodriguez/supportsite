package net.emida.supportsite.mvc.timezones.dao;


import net.emida.supportsite.mvc.timezones.TimeZoneDao;
import net.emida.supportsite.mvc.timezones.TimeZoneDates;
import net.emida.supportsite.mvc.timezones.TimeZoneInfoPojo;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;


/**
 * @author Franky Villadiego
 */

@Repository
public class TimeZoneJdbcDao implements TimeZoneDao {


    private static final Logger log = LoggerFactory.getLogger(TimeZoneJdbcDao.class);


    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate parameterJdbcTemplate;


    @Override
    public TimeZoneDates getTimeZoneFilterDates(int accessLevel, BigDecimal refId, Date startDate, Date endDate, boolean fromServerToClient) throws TechnicalException {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("accessLevel", accessLevel);
            params.addValue("refId", refId);
            params.addValue("startDate", startDate, Types.TIMESTAMP);
            params.addValue("endDate", endDate, Types.TIMESTAMP);
            params.addValue("serverToClient", fromServerToClient);
            return parameterJdbcTemplate.queryForObject(TIME_ZONE_FILTER_DATES, params, new BeanPropertyRowMapper<TimeZoneDates>(TimeZoneDates.class));
        } catch (EmptyResultDataAccessException ex) {
            log.warn("Caught exception {}.", ex.getClass());
            return null;
        } catch (DataAccessException ex) {
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }


    @Override
    public TimeZoneInfoPojo findDefault(String instanceName) throws TechnicalException {
        try{
            TimeZoneInfoPojo tzi = getTimeZoneInfoFromProperty(instanceName, "global", "global", "DefaultTimeZone");
            if(tzi == null){
                tzi = getTimeZoneInfoByCodeName("Coordinated Universal Time");
            }
            return tzi;
        }catch (DataAccessException ex){
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }

    @Override
    public TimeZoneInfoPojo findByMerchantId(long merchantId) throws TechnicalException{
        try {
            log.debug("Finding TimeZoneInfo by Merchant Id {}.", merchantId);
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("merchantId", merchantId);
            return parameterJdbcTemplate.queryForObject(FIND_MERCHANT_BY_ID, params, new BeanPropertyRowMapper<TimeZoneInfoPojo>(TimeZoneInfoPojo.class));
        }catch (EmptyResultDataAccessException ex){
            log.warn("Caught exception. No timeZoneInfo for merchant id {}. ex:{}", merchantId, ex.getClass());
            return null;
        }catch (DataAccessException ex){
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }

    }

    @Override
    public TimeZoneInfoPojo findByRepsId(long repId) throws TechnicalException{
        try {
            log.debug("Finding TimeZoneInfo by repId Id {}.", repId);
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("repId", repId);
            return parameterJdbcTemplate.queryForObject(FIND_REPS_BY_ID, params, new BeanPropertyRowMapper<TimeZoneInfoPojo>(TimeZoneInfoPojo.class));
        }catch (EmptyResultDataAccessException ex){
            log.warn("Caught exception. No timeZoneInfo for rep id {}. ex:{}", repId, ex.getClass());
            return null;
        }catch (DataAccessException ex){
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }


    private TimeZoneInfoPojo getTimeZoneInfoFromProperty(String instanceName, String project, String module, String property) throws TechnicalException{
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("instanceName", instanceName);
            params.addValue("project", project);
            params.addValue("module", module);
            params.addValue("property", property);
            return parameterJdbcTemplate.queryForObject(TIME_ZONE_FROM_PROPERTY, params, new BeanPropertyRowMapper<TimeZoneInfoPojo>(TimeZoneInfoPojo.class));
        } catch (EmptyResultDataAccessException ex) {
            log.error("Caught exception {}. Data EmptyResultDataAccessException {}", ex.getClass(), ex.getMessage());
            return null;
        } catch (DataAccessException ex) {
            log.error("Caught exception in searchMultiLine {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }

    private TimeZoneInfoPojo getTimeZoneInfoByCodeName(String codeName) throws TechnicalException{
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("codeName", codeName);
            return parameterJdbcTemplate.queryForObject(TIME_ZONE_FROM_CODE_NAME, params, new BeanPropertyRowMapper<TimeZoneInfoPojo>(TimeZoneInfoPojo.class));
        } catch (EmptyResultDataAccessException ex) {
            log.error("Caught exception {}. Data EmptyResultDataAccessException {}", ex.getClass(), ex.getMessage());
            return null;
        } catch (DataAccessException ex) {
            log.error("Caught exception in searchMultiLine {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }



    @Autowired @Qualifier("masterJdbcTemplate")
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired @Qualifier("masterNamedParamJdbcTemplate")
    public void setParameterJdbcTemplate(NamedParameterJdbcTemplate parameterJdbcTemplate) {
        this.parameterJdbcTemplate = parameterJdbcTemplate;
    }



    private static String TIME_ZONE_FILTER_DATES = "SELECT " +
            "dbo.GetLocalTimeByAccessLevel(:accessLevel, :refId, :startDate, :serverToClient) AS StartDate, " +
            "dbo.GetLocalTimeByAccessLevel(:accessLevel, :refId, :endDate, :serverToClient) AS EndDate ";


    private static String FIND_MERCHANT_BY_ID = "SELECT tzi.timeZoneId, tzi.CodeName, tzi.DisplayName, tzi.Bias, tzi.standardBias," +
            "tzi.daylightBias, tzi.standardMonth, tzi.standardDayOfWeek, tzi.standardWeek, tzi.standardHour, tzi.daylightMonth," +
            "tzi.daylightDayOfWeek, tzi.daylightWeek, tzi.daylightHour, tzi.visible," +
            "tzi.JavaTimeZoneId, jtz.DisplayName as gmtInfo " +
            "FROM merchants m WITH(NOLOCK) " +
            "INNER JOIN TimeZoneInfo tzi WITH(NOLOCK) ON tzi.TimeZoneID = m.TimeZoneID " +
            "LEFT OUTER JOIN JavaTimeZones jtz WITH(NOLOCK) ON jtz.Id = tzi.JavaTimeZoneId " +
            "WHERE m.merchant_id=:merchantId";



    private static String FIND_REPS_BY_ID = "SELECT tzi.timeZoneId, tzi.CodeName, tzi.DisplayName, tzi.Bias, tzi.standardBias," +
            "tzi.daylightBias, tzi.standardMonth, tzi.standardDayOfWeek, tzi.standardWeek, tzi.standardHour, tzi.daylightMonth," +
            "tzi.daylightDayOfWeek, tzi.daylightWeek, tzi.daylightHour, tzi.visible," +
            "tzi.JavaTimeZoneId, jtz.DisplayName as gmtInfo " +
            "FROM reps r WITH(NOLOCK) " +
            "INNER JOIN TimeZoneInfo tzi WITH(NOLOCK) ON tzi.TimeZoneID = r.TimeZoneID " +
            "LEFT OUTER JOIN JavaTimeZones jtz WITH(NOLOCK) ON jtz.Id = tzi.JavaTimeZoneId " +
            "WHERE r.rep_id=:repId";


    private static String TIME_ZONE_FROM_PROPERTY = "SELECT tzi.timeZoneId, tzi.CodeName, tzi.DisplayName, tzi.Bias, tzi.standardBias, " +
            "tzi.daylightBias, tzi.standardMonth, tzi.standardDayOfWeek, tzi.standardWeek, tzi.standardHour, tzi.daylightMonth, " +
            "tzi.daylightDayOfWeek, tzi.daylightWeek, tzi.daylightHour, tzi.visible, " +
            "tzi.JavaTimeZoneId, jtz.DisplayName as gmtInfo " +
            "FROM TimeZoneInfo AS tzi WITH (NOLOCK) " +
            "INNER JOIN properties AS p WITH (NOLOCK) ON tzi.CodeName = p.value " +
            "LEFT OUTER JOIN JavaTimeZones jtz WITH(NOLOCK) On jtz.Id = tzi.JavaTimeZoneId " +
            "WHERE p.project = :project AND p.module = :module AND p.property = :property AND p.instance = :instanceName";



    private static String TIME_ZONE_FROM_CODE_NAME = "SELECT tzi.timeZoneId, tzi.CodeName, tzi.DisplayName, tzi.Bias, tzi.standardBias, " +
            "tzi.daylightBias, tzi.standardMonth, tzi.standardDayOfWeek, tzi.standardWeek, tzi.standardHour, tzi.daylightMonth, " +
            "tzi.daylightDayOfWeek, tzi.daylightWeek, tzi.daylightHour, tzi.visible, " +
            "tzi.JavaTimeZoneId, jtz.DisplayName as gmtInfo " +
            "FROM TimeZoneInfo AS tzi WITH (NOLOCK) " +
            "LEFT OUTER JOIN JavaTimeZones jtz WITH(NOLOCK) On jtz.Id = tzi.JavaTimeZoneId " +
            "WHERE tzi.CodeName = :codeName";


}
