package net.emida.supportsite.mvc.timezones;


import net.emida.supportsite.util.exceptions.TechnicalException;

import java.math.BigDecimal;
import java.util.Date;

public interface TimeZoneDao {


    TimeZoneInfoPojo findDefault(String instanceName) throws TechnicalException;

    TimeZoneInfoPojo findByMerchantId(long merchantId) throws TechnicalException;

    TimeZoneInfoPojo findByRepsId(long repId) throws TechnicalException;

    TimeZoneDates getTimeZoneFilterDates(int accessLevel, BigDecimal refId, Date startDate, Date endDate, boolean fromServerToClient) throws TechnicalException;


}
