package net.emida.supportsite.mvc.timezones;

/**
 * @author Franky Villadiego
 */
public class TimeZoneInfoPojo {


    private Integer timeZoneID;
    private String codeName;
    private String displayName;
    private Short bias;
    private Short standardBias;
    private Short daylightBias;
    private Short standardMonth;
    private Short standardDayOfWeek;
    private Short standardWeek;
    private Short standardHour;
    private Short daylightMonth;
    private Short daylightDayOfWeek;
    private Short daylightWeek;
    private Short daylightHour;
    private Boolean visible;
    private String javaTimeZoneId;
    private String gmtInfo;


    public Integer getTimeZoneID() {
        return timeZoneID;
    }

    public void setTimeZoneID(Integer timeZoneID) {
        this.timeZoneID = timeZoneID;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Short getBias() {
        return bias;
    }

    public void setBias(Short bias) {
        this.bias = bias;
    }

    public Short getStandardBias() {
        return standardBias;
    }

    public void setStandardBias(Short standardBias) {
        this.standardBias = standardBias;
    }

    public Short getDaylightBias() {
        return daylightBias;
    }

    public void setDaylightBias(Short daylightBias) {
        this.daylightBias = daylightBias;
    }

    public Short getStandardMonth() {
        return standardMonth;
    }

    public void setStandardMonth(Short standardMonth) {
        this.standardMonth = standardMonth;
    }

    public Short getStandardDayOfWeek() {
        return standardDayOfWeek;
    }

    public void setStandardDayOfWeek(Short standardDayOfWeek) {
        this.standardDayOfWeek = standardDayOfWeek;
    }

    public Short getStandardWeek() {
        return standardWeek;
    }

    public void setStandardWeek(Short standardWeek) {
        this.standardWeek = standardWeek;
    }

    public Short getStandardHour() {
        return standardHour;
    }

    public void setStandardHour(Short standardHour) {
        this.standardHour = standardHour;
    }

    public Short getDaylightMonth() {
        return daylightMonth;
    }

    public void setDaylightMonth(Short daylightMonth) {
        this.daylightMonth = daylightMonth;
    }

    public Short getDaylightDayOfWeek() {
        return daylightDayOfWeek;
    }

    public void setDaylightDayOfWeek(Short daylightDayOfWeek) {
        this.daylightDayOfWeek = daylightDayOfWeek;
    }

    public Short getDaylightWeek() {
        return daylightWeek;
    }

    public void setDaylightWeek(Short daylightWeek) {
        this.daylightWeek = daylightWeek;
    }

    public Short getDaylightHour() {
        return daylightHour;
    }

    public void setDaylightHour(Short daylightHour) {
        this.daylightHour = daylightHour;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getJavaTimeZoneId() {
        return javaTimeZoneId;
    }

    public void setJavaTimeZoneId(String javaTimeZoneId) {
        this.javaTimeZoneId = javaTimeZoneId;
    }

    public String getGmtInfo() {
        return gmtInfo;
    }

    public void setGmtInfo(String gmtInfo) {
        this.gmtInfo = gmtInfo;
    }
}
