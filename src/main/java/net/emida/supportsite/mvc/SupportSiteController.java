package net.emida.supportsite.mvc;

/**
 * @author Franky Villadiego
 */
public interface SupportSiteController {

    int getSection();

    int getSectionPage();
}
