package net.emida.supportsite.mvc;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.mvc.security.AssignablePermission;
import net.emida.supportsite.mvc.security.PermissionsDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.text.ParseException;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Franky Villadiego
 */

public abstract class BaseController implements SupportSiteController {

    private static final Logger log = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    protected ServletContext servletContext;

    @Autowired
    private PermissionsDao permissionsDao;

    @ModelAttribute("customConfiguration")
    private String customConfiguration(){
        //ServletContext servletContext = httpServletRequest.getServletContext();
        String result = DebisysConfigListener.getCustomConfigType(servletContext);
        return result;
    }

    @ModelAttribute("isInternational")
    protected boolean isInternational(){
        //ServletContext servletContext = httpServletRequest.getServletContext();
        boolean result = DebisysConfigListener.getDeploymentType(servletContext).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                DebisysConfigListener.getCustomConfigType(servletContext).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
        return result;
    }

    @ModelAttribute("isMexico")
    protected boolean isMexico(){
        //ServletContext servletContext = httpServletRequest.getServletContext();
        boolean result = DebisysConfigListener.getDeploymentType(servletContext).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                DebisysConfigListener.getCustomConfigType(servletContext).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
        return result;
    }

    @ModelAttribute("isDomestic")
    protected boolean isDomestic(){
        //ServletContext servletContext = httpServletRequest.getServletContext();
        boolean result = DebisysConfigListener.getDeploymentType(servletContext).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
        return result;
    }

    @ModelAttribute("access_level")
    protected String getAccessLevel(){
        SessionData sessionData = getSessionData(httpServletRequest.getSession());
        String strAccessLevel = sessionData.getProperty("access_level");
        return strAccessLevel;
    }

    @ModelAttribute("dist_chain_type")
    protected String getDistChainType(){
        SessionData sessionData = getSessionData(httpServletRequest.getSession());
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        return strDistChainType;
    }

    @ModelAttribute("ref_id")
    protected String getRefId(){
        SessionData sessionData = getSessionData(httpServletRequest.getSession());
        String strRefId = sessionData.getProperty("ref_id");
        return strRefId;
    }

    @ModelAttribute("company_name")
    protected String getCompanyName(){
        SessionData sessionData = getSessionData(httpServletRequest.getSession());
        String strCompanyName = sessionData.getProperty("company_name");
        return strCompanyName;
    }

    @ModelAttribute("instanceName")
    protected String getInstanceName(){
        return DebisysConfigListener.getInstance(servletContext);
    }
    
    @ModelAttribute("isIntranetUser")
    protected boolean isIntranetUser(){
        SessionData sessionData = getSessionData(httpServletRequest.getSession());
        boolean isIntranetUser = sessionData.getUser().isIntranetUser();
        return isIntranetUser;
    }


    protected int getProperty(String propertyName, int defaultValue){
        Object propertyValue = servletContext.getAttribute(propertyName);
        if(propertyValue != null){
            try{
                return Integer.parseInt(propertyValue.toString());
            }catch (Exception ex){
                log.error("Error parsing INT property :" + propertyName + ", with value :" + propertyValue);
            }
        }
        return defaultValue;
    }
    protected double getProperty(String propertyName, double defaultValue){
        Object propertyValue = servletContext.getAttribute(propertyName);
        if(propertyValue != null){
            try{
                return Double.parseDouble(propertyValue.toString());
            }catch (Exception ex){
                log.error("Error parsing DOUBLE property :" + propertyName + ", with value :" + propertyValue);
            }
        }
        return defaultValue;
    }
    protected String getProperty(String propertyName, String defaultValue){
        Object propertyValue = servletContext.getAttribute(propertyName);
        if(propertyValue != null){
            return propertyValue.toString();
        }
        return defaultValue;
    }

    protected boolean isLevelCarrier(){
        return getAccessLevel().equalsIgnoreCase(DebisysConstants.CARRIER);
    }
    protected boolean isLevelIso3(){
        return getAccessLevel().equalsIgnoreCase(DebisysConstants.ISO) && getDistChainType().equals(DebisysConstants.DIST_CHAIN_3_LEVEL);
    }
    protected boolean isLevelIso5(){
        return getAccessLevel().equalsIgnoreCase(DebisysConstants.ISO) && getDistChainType().equals(DebisysConstants.DIST_CHAIN_5_LEVEL);
    }
    protected boolean isLevelAgent(){
        return getAccessLevel().equalsIgnoreCase(DebisysConstants.AGENT);
    }
    protected boolean isLevelSubAgent(){
        return getAccessLevel().equalsIgnoreCase(DebisysConstants.SUBAGENT);
    }
    protected boolean isLevelRep(){
        return getAccessLevel().equalsIgnoreCase(DebisysConstants.REP);
    }
    protected boolean isLevelMerchant(){
        return getAccessLevel().equalsIgnoreCase(DebisysConstants.MERCHANT);
    }

    private SessionData getSessionData(HttpSession session){
        SessionData sessionData = (SessionData)session.getAttribute("SessionData");
        if(sessionData == null){
            sessionData = new SessionData();
        }
        return sessionData;
    }

    protected SessionData getSessionData(){
        return getSessionData(httpServletRequest.getSession());
    }

    protected String getString(String key){
        SessionData sessionData = getSessionData();
        return Languages.getString(key, sessionData.getLanguage());
    }

    protected String getWorkingDir(){
        return DebisysConfigListener.getWorkingDir(servletContext);
    }

    protected String getFilePrefix(){
        return DebisysConfigListener.getFilePrefix(servletContext);
    }

    protected String getDownloadPath(){
        return DebisysConfigListener.getDownloadPath(servletContext);
    }

    protected boolean allowPermission(String permissionConstant){
        return getSessionData().checkPermission(permissionConstant);
    }


    //NOTA : OJO. Este metodo fue creado para emular varios metodos "is" que estan en com.debisys.users.User
    //User.isInvoiceNumberEnabled
    //User.isWebServiceAutherticationTerminalsEnabled
    //User.isRepRatePlanforTerminalCreationEnabled
    //User.isExternalRepAllowedEnabled
    //User.isManagePromoDetailsEnabled
    //El metodo allowPermission() es la forma de ver si un permiso esta activo para un usuario
    //Y es un simil con com.debisys.users.SessionData.checkPermission()
    protected boolean hasPermissionEnabled(String permission){
        String strIsoId = getSessionData().getProperty("iso_id");
        Long isoId = Long.valueOf(strIsoId);
        List<AssignablePermission> assignablePermissions = permissionsDao.findAssignablePermissions(isoId, DebisysConstants.ISO);
        for (AssignablePermission ap : assignablePermissions) {
            int permInt = Integer.parseInt(permission);
            if(ap.getWebPermissionTypeId().equals(permInt)){
                return true;
            }
        }
        return false;
    }

    protected String generateDestinationFilePath(String fileName, String extension){
        String workingDir = getWorkingDir();
        String filePrefix = getFilePrefix();
        extension = extension.indexOf('.') != -1 ? extension : "." + extension;
        String finalFileName = workingDir + filePrefix + fileName + extension;
        return finalFileName;
    }

    protected String generateRandomFileName(){
        boolean isUnique = false;
        String strFileName = "";
        while (!isUnique){
            strFileName = generateRandomNumber();
            isUnique = checkFileName(strFileName, servletContext);
        }
        return strFileName;
    }

    private String generateRandomNumber(){
        StringBuffer s = new StringBuffer();
        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);
        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private boolean checkFileName(String strFileName, ServletContext context){
        boolean isUnique = true;
        String downloadPath = getDownloadPath();
        String workingDir = getWorkingDir();
        String filePrefix = getFilePrefix();

        File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
        if (f.exists()){
            // duplicate file found
            log.error("Duplicate file found: {}{}{}{}.csv",workingDir, File.separator, filePrefix, strFileName);
            return false;
        }

        f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
        if (f.exists()){
            // duplicate file found
            log.error("Duplicate file found: {}{}{}{}.zip", downloadPath, File.separator, filePrefix, strFileName);
            return false;
        }
        return isUnique;
    }

    protected String generateZipFile(String generatedFileName, Resource resource) throws IOException {
        String downloadPath = getDownloadPath();
        String filePrefix = getFilePrefix();

        String zipFileName = downloadPath + filePrefix + generatedFileName + ".zip";
        log.debug("File zip : {}", zipFileName);
        ZipOutputStream out = null;
        InputStream in = null;
        try {

            out = new ZipOutputStream(new FileOutputStream(zipFileName));

            // Set the compression ratio
            out.setLevel(Deflater.DEFAULT_COMPRESSION);

            // Add ZIP entry to output stream.
            String filename = resource.getFilename();
            int lastIndex = filename.lastIndexOf('.');
            String fileExtension = "";
            if(lastIndex != -1){
                fileExtension = filename.substring(lastIndex);
            }
            String entryFileName = filePrefix + generatedFileName + fileExtension;

            out.putNextEntry(new ZipEntry(entryFileName));

            long size = resource.contentLength();
            byte[] buffer = new byte[(int) size];

            // Transfer bytes from the current file to the ZIP file
            // out.write(buffer, 0, in.read(buffer));
            in = resource.getInputStream();
            int len;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
        }finally {
            if(out != null){
                out.closeEntry();
            }
            if(in != null){
                in.close();
            }
            if(out != null){
                out.close();
            }
        }

        return zipFileName;
    }



}
