package net.emida.supportsite.mvc;

import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author fvilladiego
 */

@Controller
@RequestMapping(value = UrlPaths.MESSAGE_PATH)
public class SecurityMessageController  extends BaseController{

    private static final Logger log = LoggerFactory.getLogger(SecurityMessageController.class);


    @RequestMapping(method = RequestMethod.GET)
    public String login(Map model, @RequestParam("message") String msgId, HttpSession httpSession){

        log.info("@@@@@@11111 MESSAGE @@@@@@@");
        log.info("MesgaeID={}", msgId);
        SessionData sessionData = (SessionData)httpSession.getAttribute("SessionData");
        String messageToPrint = "";
        if(msgId != null && !msgId.isEmpty()){
            int errorNumber = 0;
            try {
                errorNumber = Integer.parseInt(msgId);
            }catch (Exception ex){
                errorNumber = 0;
            }

            switch (errorNumber){
                case 0: break;
                case 1:
                    messageToPrint = Languages.getString("jsp.message.msg1", sessionData.getLanguage());
                    break;
                case 2:
                    messageToPrint = Languages.getString("jsp.message.msg2", sessionData.getLanguage());
                    break;
                case 3:
                    messageToPrint = Languages.getString("jsp.message.msg3", sessionData.getLanguage());
                    break;
                case 4:
                    messageToPrint = Languages.getString("jsp.message.msg4", sessionData.getLanguage());
                    break;
                case 5:
                    messageToPrint = Languages.getString("jsp.message.msg5", sessionData.getLanguage());
                    break;
                case 6:
                    messageToPrint = Languages.getString("jsp.message.msg6", sessionData.getLanguage());
                    break;
                case 7:
                    messageToPrint = Languages.getString("jsp.message.msg7", sessionData.getLanguage());
                    break;
                default:
                    break;
            }
        }
        model.put("messageToPrint", messageToPrint);
        return "message";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String authenticate(Map model, @RequestParam("message") String msgId) throws TechnicalException, ApplicationException {
        log.info("@@@@@@222222 MESSAGE @@@@@@");
        log.info("MesgaeID={}", msgId);
        return "message";
    }




    @Override
    public int getSection() {
        return 1;
    }

    @Override
    public int getSectionPage() {
        return 2;
    }
}
