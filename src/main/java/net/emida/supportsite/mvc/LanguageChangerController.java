package net.emida.supportsite.mvc;

import com.debisys.users.SessionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author fvilladiego
 */

@Controller
@RequestMapping(value = UrlPaths.CHANGE_LANGUAGE_PATH)
public class LanguageChangerController {

    private static final Logger log = LoggerFactory.getLogger(LanguageChangerController.class);


    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void change(HttpServletRequest request, @RequestParam("lan") String language){
        log.info("Changing language to {}", language);
        SessionData sessionData = getSessionData(request.getSession());
        if(language != null){
            sessionData.setLanguage(language);
        }
        log.debug("Language has changed");
    }


    private SessionData getSessionData(HttpSession session){
        SessionData sessionData = (SessionData)session.getAttribute("SessionData");
        if(sessionData == null){
            sessionData = new SessionData();
        }
        return sessionData;
    }

}
