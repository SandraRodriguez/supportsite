package net.emida.supportsite.mvc.rateplans;

import java.util.Objects;

/**
 * @author Franky Villadiego
 */
public class MerchantRatePlanPojo {


    private Long ratePlanId;
    private String name;
    private String description;
    private Boolean isTemplate;
    private Boolean isNewRatePlanModel;


    public Long getRatePlanId() {
        return ratePlanId;
    }

    public void setRatePlanId(Long ratePlanId) {
        this.ratePlanId = ratePlanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsTemplate() {
        return isTemplate;
    }

    public void setIsTemplate(Boolean template) {
        isTemplate = template;
    }

    public Boolean getIsNewRatePlanModel() {
        return isNewRatePlanModel;
    }

    public void setIsNewRatePlanModel(Boolean newRatePlanModel) {
        isNewRatePlanModel = newRatePlanModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MerchantRatePlanPojo that = (MerchantRatePlanPojo) o;
        return Objects.equals(ratePlanId, that.ratePlanId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(ratePlanId);
    }
}
