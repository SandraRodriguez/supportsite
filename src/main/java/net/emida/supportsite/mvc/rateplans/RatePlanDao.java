package net.emida.supportsite.mvc.rateplans;

import net.emida.supportsite.util.exceptions.TechnicalException;

import java.util.List;

/**
 * @author Franky Villadiego
 */

public interface RatePlanDao {


    List<MerchantRatePlanPojo> getAllRatePlansForAllMerchants(long rootId, Long carrierId) throws TechnicalException;

    List<MerchantRatePlanPojo> getNewRatePlansForAllMerchants(long rootId, Long carrierId) throws TechnicalException;

}
