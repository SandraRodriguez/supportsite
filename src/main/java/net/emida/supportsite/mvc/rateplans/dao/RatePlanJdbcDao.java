package net.emida.supportsite.mvc.rateplans.dao;


import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.mvc.rateplans.MerchantRatePlanPojo;
import net.emida.supportsite.mvc.rateplans.RatePlanDao;
import net.emida.supportsite.mvc.timezones.TimeZoneDao;
import net.emida.supportsite.mvc.timezones.TimeZoneDates;
import net.emida.supportsite.mvc.timezones.TimeZoneInfoPojo;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * @author Franky Villadiego
 */

@Repository
public class RatePlanJdbcDao implements RatePlanDao {


    private static final Logger log = LoggerFactory.getLogger(RatePlanJdbcDao.class);


    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    @Override
    public List<MerchantRatePlanPojo> getAllRatePlansForAllMerchants(long rootId, Long carrierId) throws TechnicalException {
        List<MerchantRatePlanPojo> list = getRatePlansForAllMerchants(rootId, carrierId);
        if(!list.isEmpty()){
            Iterator<MerchantRatePlanPojo> it = list.iterator();
            while (it.hasNext()){
                MerchantRatePlanPojo rp = it.next();
                boolean isTemplate = rp.getIsTemplate() != null && rp.getIsTemplate();
                if(isTemplate){
                    it.remove();
                }
            }
        }
        return list;
    }

    @Override
    public List<MerchantRatePlanPojo> getNewRatePlansForAllMerchants(long rootId, Long carrierId) throws TechnicalException {
        List<MerchantRatePlanPojo> list = getRatePlansForAllMerchants(rootId, carrierId);
        if(!list.isEmpty()){
            Iterator<MerchantRatePlanPojo> it = list.iterator();
            while (it.hasNext()){
                MerchantRatePlanPojo rp = it.next();
                boolean isTemplate = rp.getIsTemplate() != null && rp.getIsTemplate();
                boolean notNewRatePlan = rp.getIsNewRatePlanModel() == null || !rp.getIsNewRatePlanModel();
                if(notNewRatePlan || isTemplate){
                    it.remove();
                }
            }
        }
        return list;
    }

    private List<MerchantRatePlanPojo> getRatePlansForAllMerchants(long rootId, Long carrierId) throws TechnicalException {
        try {
            String sql = buildRatePlansQueryForAllMerchants(carrierId);

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("rootId", rootId);
            params.addValue("repType", DebisysConstants.REP_TYPE_REP);
            if(carrierId != null) {
                params.addValue("carrierId", carrierId);
            }
            return parameterJdbcTemplate.query(sql, params, new BeanPropertyRowMapper<MerchantRatePlanPojo>(MerchantRatePlanPojo.class));
        } catch (EmptyResultDataAccessException ex) {
            log.warn("Caught exception {}.", ex.getClass());
            return new ArrayList();
        } catch (DataAccessException ex) {
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }


    private String buildRatePlansQueryForAllMerchants(Long carrierId){

        StringBuilder actorHierarchy = buildMerchantActorHierarchy();

        actorHierarchy
        .append(" SELECT DISTINCT rp.ratePlanId, rp.name, rp.description, rp.isTemplate, rpg.isNewRatePlanModel ")
        .append(" FROM Merchants m WITH (NoLock) ");

        if(carrierId != null){
            actorHierarchy
            .append(" INNER JOIN MerchantCarriers mc WITH (NoLock) ON mc.merchantId = m.Merchant_ID ")
            .append(" INNER JOIN RepCarriers rc WITH (NoLock) ON rc.carrierId = mc.carrierId AND rc.RepID=:carrierId ")
            .append(" INNER JOIN Products p WITH (NoLock) ON mc.carrierId = p.carrier_id ");
        }

        actorHierarchy
        .append(" INNER JOIN terminals t ON m.merchant_id = t.merchant_id ")
        .append(" INNER JOIN RatePlan rp ON t.RatePlanId = rp.RatePlanID ")
        .append(" INNER JOIN rep_iso_rate_plan_glue rpg ON rp.RatePlanID = rpg.iso_rate_plan_id ")

        .append(" WHERE (m.Rep_Id IN (")
        .append("                      SELECT RepId FROM ActorHierarchy ah WHERE ah.Type =:repType")
        .append("                    ) OR m.Merchant_ID =:rootId ")
        .append("        ) ")
        //.append(" AND rpg.isNewRatePlanModel=1 ") esto necesita indice, mejor filtramos despues del query
        .append(" ORDER BY rp.name ");

        return actorHierarchy.toString();
    }

    private StringBuilder buildMerchantActorHierarchy(){
        StringBuilder sb = new StringBuilder();
        sb.append(" WITH ActorHierarchy AS (")
                .append("SELECT r1.Rep_ID as RepId, r1.Type FROM Reps r1 WITH (NOLOCK) ")
                .append("WHERE r1.Rep_ID =:rootId ")

                .append(" UNION ALL ")

                .append("SELECT r2.Rep_ID as RepId, r2.Type FROM Reps r2 WITH (NOLOCK) ")
                .append("INNER JOIN ActorHierarchy ah ON ah.RepId = r2.ISO_ID ")
                .append("WHERE r2.Type NOT IN (2, 3, 11) ")
                .append(") ");
        return sb;
    }

    private Integer getRepType(long repId){
        try {
            log.debug("Finding rep type by repId:{}", repId);
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("repId", repId);
            return parameterJdbcTemplate.queryForObject(REP_TYPE_BY_ID, params, Integer.class);
        } catch (EmptyResultDataAccessException ex) {
            log.error("Caught exception {}. Data EmptyResultDataAccessException {}", ex.getClass(), ex.getMessage());
            return null;
        } catch (DataAccessException ex) {
            log.error("Caught exception {}. Data access exception {}", ex.getClass(), ex);
            throw new TechnicalException("There was a TechnicalException.", ex);
        }
    }


    @Autowired @Qualifier("masterJdbcTemplate")
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired @Qualifier("masterNamedParamJdbcTemplate")
    public void setParameterJdbcTemplate(NamedParameterJdbcTemplate parameterJdbcTemplate) {
        this.parameterJdbcTemplate = parameterJdbcTemplate;
    }



    private static String REP_TYPE_BY_ID = "SELECT r.Type FROM Reps r WHERE rep_id=repId";



}
