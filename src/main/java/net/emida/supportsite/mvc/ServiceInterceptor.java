package net.emida.supportsite.mvc;

import com.debisys.languages.Languages;
import com.debisys.presentation.Menu;
import com.debisys.users.SessionData;
import com.debisys.utils.ConfigHelper;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.util.EmidaLanguage;
import net.emida.supportsite.util.LanguageBridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Franky Villadiego
 */
public class ServiceInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(ServiceInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        long startTime = System.currentTimeMillis();
        log.debug("PreHandle Service starting");
        Integer section = (Integer)request.getAttribute("section");
        Integer sectionPage = (Integer)request.getAttribute("section_page");
        if(section == null && sectionPage == null){
            HandlerMethod handlerMethod = (HandlerMethod)handler;
            SupportSiteController supportSiteController =(SupportSiteController) handlerMethod.getBean();

            section = supportSiteController.getSection();
            sectionPage = supportSiteController.getSectionPage();
            request.setAttribute("section", section);
            request.setAttribute("section_page", sectionPage);

        }

        HttpSession httpSession = request.getSession();
        SessionData sessionData = getSessionData(httpSession);
        String language = sessionData.getLanguage();
        log.debug("##### SessionData language = {}", language);

        String languageAttribute = request.getSession().getServletContext().getAttribute("language").toString();
        log.debug("##### languageAttribute = {} ####", languageAttribute);

/*        if(sessionData.getUser() == null){
            if(sessionData.getLanguage().equals("1")){
                sessionData.setLanguage(languageAttribute);
            }
        }*/



        LanguageBridge languageBridge = new LanguageBridge(sessionData);
        request.setAttribute("Languages", languageBridge);

        String strProtocol = strProtocol(request);
        request.setAttribute("strProtocol", strProtocol);
        log.debug("##### strProtocol = {} ####", strProtocol);

        String baseHref = baseHref(request);
        request.setAttribute("baseHref", baseHref);
        log.debug("##### baseHref = {} ####", baseHref);

        String strBrandedCompanyName = strBrandedCompanyName(request);
        request.setAttribute("strBrandedCompanyName", strBrandedCompanyName);
        log.debug("##### strBrandedCompanyName = {} ####", strBrandedCompanyName);

        boolean permViewCarrierReports = sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS);
        request.setAttribute("PERM_VIEW_CARRIER_REPORTS", permViewCarrierReports);
        log.debug("##### permViewCarrierReports = {} ####", permViewCarrierReports);

        String headerBackground = headerBackground(request);
        request.setAttribute("headerBackground", headerBackground);
        log.debug("##### headerBackground = {} ####", headerBackground);

        String serverName = serverName(request);
        request.setAttribute("serverName", serverName);
        log.debug("##### serverName = {} ####", serverName);

        String serverPort = serverPort(request);
        request.setAttribute("serverPort", serverPort);
        log.debug("##### strProtocol = {} ####", strProtocol);

        String imageName = imageName(request);
        request.setAttribute("imageName", imageName);
        log.debug("##### imageName = {} ####", imageName);

        String codeLanguage = codeLanguage(request);
        request.setAttribute("codeLanguage", codeLanguage);
        log.debug("##### codeLanguage = {} ####", codeLanguage);

        List<EmidaLanguage> languages = languages(request);
        request.setAttribute("languages", languages);
        log.debug("##### languages = {} ####", languages);

        String loginLabel = loginLabel(request);
        request.setAttribute("loginLabel", loginLabel);
        log.debug("##### loginLabel = {} ####", loginLabel);

        String isoId = isoId(request);
        request.setAttribute("isoId", isoId);
        log.debug("##### isoId = {} ####", isoId);

        String intranetSite = intranetSite(request);
        request.setAttribute("intranetSite", intranetSite);
        log.debug("##### intranetSite = {} ####", intranetSite);

        String intranetPortSite = intranetPortSite(request);
        request.setAttribute("intranetPortSite", intranetPortSite);
        log.debug("##### intranetPortSite = {} ####", intranetPortSite);

        boolean intranetUser = sessionData.getUser().isIntranetUser();
        request.setAttribute("isIntranetUser", intranetUser);
        log.debug("##### intranetUser = {} ####", intranetUser);

        Menu menu = menu(request);
        request.setAttribute("menu", menu);
        log.debug("##### menu = {} ####", menu);

        boolean domestic = isDomestic(request);
        request.setAttribute("isDomestic", domestic);
        log.debug("##### domestic = {} ####", domestic);

        setStringTypeDatesRange(request);
        setDateTypeDatesRange(request);

        long finishTime = System.currentTimeMillis();
        log.debug("Finishing PreHandle Service at {} milliseconds", finishTime - startTime);
        return true;
    }

    //@ModelAttribute("strProtocol")
    private String strProtocol(HttpServletRequest request) {
        ConfigHelper ch = new ConfigHelper(request);
        String requestUrl = ch.getRequestUrl();
        String strProtocol = "http";
        if(requestUrl.indexOf(':') >= 0){
            strProtocol = requestUrl.substring(0, requestUrl.indexOf(':')).toLowerCase();
        }
        return strProtocol;
    }

    //@ModelAttribute("baseHref")
    private String baseHref(HttpServletRequest request) {
        ConfigHelper ch = new ConfigHelper(request);
        String baseHref = ch.getBaseHref();
        return baseHref;
    }


    //@ModelAttribute("strBrandedCompanyName")
    private String strBrandedCompanyName(HttpServletRequest request) {
        ConfigHelper ch = new ConfigHelper(request);
        String strBrandedCompanyName = ch.getBrandedCompanyName();
        return strBrandedCompanyName;
    }


    //@ModelAttribute("headerBackground")
    private String headerBackground(HttpServletRequest request) {
        ConfigHelper ch = new ConfigHelper(request);
        String headerBackground = ch.getHeaderBackground();
        return headerBackground;
    }

    //@ModelAttribute("serverName")
    private String serverName(HttpServletRequest request) {
        ConfigHelper ch = new ConfigHelper(request);
        String serverName = ch.getServerName();
        return serverName;
    }

    //@ModelAttribute("serverPort")
    private String serverPort(HttpServletRequest request) {
        ConfigHelper ch = new ConfigHelper(request);
        String serverPort = ch.getServerPort();
        return serverPort;
    }

    //@ModelAttribute("imageName")
    private String imageName(HttpServletRequest request) {
        ConfigHelper ch = new ConfigHelper(request);
        String imageName = ch.getImageName();
        return imageName;
    }

    //@ModelAttribute("SessionData")
    private SessionData getSessionData(HttpSession session){
        SessionData sessionData = (SessionData)session.getAttribute("SessionData");
        if(sessionData == null){
            sessionData = new SessionData();
        }
        return sessionData;
    }

    private String codeLanguage(HttpServletRequest request){
        //String pathCont = request.getContextPath();
        //String codeLanguage = "";
        SessionData sessionData = getSessionData(request.getSession());
        String lang =  sessionData.getLanguage();
        if(lang.equals("spanish")){
            return "1";
        }else{
            return "0";
        }
    }


    private List<EmidaLanguage> languages(HttpServletRequest request){
        Vector<Vector<String>> languages = Languages.searchLanguages();
        List<EmidaLanguage> languageList = new ArrayList<EmidaLanguage>();
        for (Vector<String> language : languages) {
            EmidaLanguage languageBean = new EmidaLanguage();
            languageBean.setId(Integer.valueOf(language.get(0)));
            languageBean.setEnglishLabel(language.get(1));
            languageBean.setSpanishLabel(language.get(2));
            languageList.add(languageBean);
        }
        return languageList;
        //Iterator it_ = languages.iterator();
    }


    private String loginLabel(HttpServletRequest request){
        SessionData sessionData = getSessionData(request.getSession());
        String loginLabel = sessionData.getProperty("username");
        return loginLabel;
    }

    private String isoId(HttpServletRequest request){
        SessionData sessionData = getSessionData(request.getSession());
        String isoId = sessionData.getProperty("iso_id");
        return isoId;
    }

    private String intranetSite(HttpServletRequest request){
        String intranetSite = (String)request.getSession().getServletContext().getAttribute("intranet-site");
        return intranetSite;
    }

    private String intranetPortSite(HttpServletRequest request){
        String intranetPortSite = (String)request.getSession().getServletContext().getAttribute("intranet-port-site");
        return intranetPortSite;
    }


    private Menu menu(HttpServletRequest request){
        ServletContext application = request.getSession().getServletContext();
        SessionData sessionData = getSessionData(request.getSession());
        Integer section = (Integer)request.getAttribute("section");
        Integer sectionPage = (Integer)request.getAttribute("section_page");
        //String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String deploymentType = DebisysConfigListener.getDeploymentType(application);
        String serverName = serverName(request);
        StringBuffer optionTools= new StringBuffer("");
        Menu menu = new Menu(application, sessionData, section, strDistChainType, sectionPage, deploymentType, serverName);
        return menu;
    }



    //@ModelAttribute("isInternational")
    private boolean isInternational(HttpServletRequest req){
        ServletContext servletContext = req.getSession().getServletContext();
        boolean result = DebisysConfigListener.getDeploymentType(servletContext).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                DebisysConfigListener.getCustomConfigType(servletContext).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
        return result;
    }

    //@ModelAttribute("isMexico")
    private boolean isMexico(HttpServletRequest req){
        ServletContext servletContext = req.getSession().getServletContext();
        boolean result = DebisysConfigListener.getDeploymentType(servletContext).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                DebisysConfigListener.getCustomConfigType(servletContext).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
        return result;
    }

    //@ModelAttribute("isDomestic")
    private boolean isDomestic(HttpServletRequest req){
        ServletContext servletContext = req.getSession().getServletContext();
        boolean result = DebisysConfigListener.getDeploymentType(servletContext).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
        return result;
    }

    //@ModelAttribute("customConfiguration")
    private String customConfiguration(HttpServletRequest req){
        log.info("********** model attribute customConfiguration ************");
        ServletContext servletContext = req.getSession().getServletContext();
        String result = DebisysConfigListener.getCustomConfigType(servletContext);
        return result;
    }


    private void setStringTypeDatesRange(HttpServletRequest request){
        SessionData sessionData = getSessionData(request.getSession());
        sessionData.setProperty("start_date", request.getParameter("startDate"));
        sessionData.setProperty("end_date", request.getParameter("endDate"));
    }

    private void setDateTypeDatesRange(HttpServletRequest request){
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try {
            String startDateStr = request.getParameter("startDate");
            if(startDateStr != null && !startDateStr.trim().isEmpty()){
                Date startDate = sdf.parse(startDateStr);
                request.setAttribute("startDate", startDate);
            }
            String endDateStr = request.getParameter("endDate");
            if(endDateStr != null && !endDateStr.trim().isEmpty()){
                Date endDate = sdf.parse(endDateStr);
                request.setAttribute("endDate", endDate);
            }
        } catch (ParseException e) {
            log.error("Error parsing dates. {}", e);
        }


    }

}
