package net.emida.supportsite.mvc;

/**
 * @author Franky Villadiego
 */
public interface UrlPaths {

    String CONTEXT_PATH = "/support";

    String MESSAGE_PATH = "/message";
    String CHANGE_LANGUAGE_PATH = "/changeLanguage";

    String CUSTOMER_PATH = "/customers";

    String AGENTS_PATH = "/agents";
    String AGENTS_GRID_PATH = "/agents/grid";
    String AGENTS_SEARCH_PATH = "/agents/search";
    String PATH_MERCHANT_FIND_BY = "/findMerchantBy";
    String PATH_ADD_FP_NOTIFICATION = "/addForgotPasswordNotification";
    
    String REPS_PATH = "/reps";

    String REPORT_PATH = "/reports";

    String TOOLS_PATH = "/tools";
    String TOOLS_PATH_CREDENTIAL = "/toolsCredentials";
    String ADMIN_PATH = "/admin";

    String REPORT_SUPPORT_PATH = "support/reports";

    String LOYALTY_PATH = "/loyalty";
    
    String UTIL_PATH = "/util";

    //Multisource transaction report
    String MULTISOURCE_TRANSACTION_REPORT_FILTERS = "/transactions/multisource/filters";

    String PAYMENTS_OPTIONS = "/paymentOption";
    String ADD_PAYMENTS_OPTIONS = "/addPaymentProcessor";
    String EDIT_PAYMENTS_PROCESSOR = "/editPaymentProcessor";
    String DELETE_PAYMENTS_PROCESSOR = "/deletePaymentProcessor";
    String DELETE_PAYMENTS_PROCESSOR_PARAMETER = "/deletePaymentProcessorParameter";
    String UPDATE_PAYMENT_PROCESSOR_PARAMETER = "/updatePaymentProcessorParameter";

    //Support Site Bulk Merchant Payment Feature
    String BULK_PAYMENT = "bulkPayment";
    String RUN_NEW_BULK_PAYMENT = "/newBulkPayment";

    String CONTRACTS_PRIVACY_NOTICER = "/contractsPrivacyNotice";
    String ACCEPTANCE_CONTRACTS = "/acceptanceContracts";

    String TRX_BY_REGION_PATH = "/trx/region";
    //Spiff Report
    String SPIFF_DETAIL_REPORT_PATH = "/spiff/detail";
    String ACTIVATION_VERIZON_REPORT_FILTER = "/activationVerizon/reportFilter";
    String ACTIVATION_VERIZON_REPORT_GET_IDS_ENTITY = "/activationVerizon/get/entity";
    String ACTIVATION_DETAIL_REPORT_PATH = "/activation/detail";
    String SPIFF_DETAIL_REPORT_GRID_PATH = "/spiff/detail/grid";
    String ACTIVATION_DETAIL_REPORT_GRID_PATH = "/activation/detail/grid";
    String ACTIVATION_VERIZON_DETAIL_REPORT_GRID_PATH = "/activation/verizon/detail/grid";
    String SPIFF_DETAIL_REPORT_DOWNLOAD_PATH = "/spiff/detail/download";
    String ACTIVATION_DETAIL_REPORT_DOWNLOAD_PATH = "/activation/detail/download";
    String ACTIVATION_VERIZON_REPORT_DOWNLOAD_PATH = "/activationVerizon/download";

    //Available Final Balance Report by Reps
    String REPS_AFBR_REPORT_PATH = "/final-balance/reps";
    String REPS_AFBR_REPORT_GRID_PATH = "/final-balance/reps/grid";
    String REPS_AFBR_REPORT_DOWNLOAD_PATH = "/final-balance/reps/download";

    //Spread By Provider and Carrier Report
    String SPREAD_BY_PROVIDER_CARRIER_REPORT_PATH = "/commissions/spreadByProviderNCarrier";
    String SPREAD_BY_PROVIDER_CARRIER_REPORT_GRID_PATH = "/commissions/spreadByProviderNCarrier/grid";
    String SPREAD_BY_PROVIDER_CARRIER_REPORT_DOWNLOAD_PATH = "/commissions/spreadByProviderNCarrier/download";

    //Spiff processing history report
    String SPIFF_PROCESSING_HISTORY_REPORT_PATH = "/spiff/processHistory";
    String SPIFF_PROCESSING_HISTORY_REPORT_GRID_PATH = "/spiff/processHistory/grid";
    String SPIFF_PROCESSING_HISTORY_REPORT_DOWNLOAD_PATH = "/spiff/processHistory/download";

    //Balance Audit history report
    String BALANCE_AUDIT_HISTORICAL_REPORT_PATH = "/balanceaudit/historical";
    String BALANCE_AUDIT_DETAIL_REPORT_GRID_PATH = "/balanceaudit/historical/grid";
    String BALANCE_AUDIT_DETAIL_REPORT_DOWNLOAD_PATH = "/balanceaudit/historical/download";

    String SCHEDULER_HISTORIAL_BALANCE = "/scheduleHistorialBalanceController";

    //Balance Audit history report
    String BALANCE_HISTORY_HISTORICAL_REPORT_PATH = "/balancehistory/historical";
    String BALANCE_HISTORY_DETAIL_REPORT_GRID_PATH = "/balancehistory/historical/grid";
    String BALANCE_HISTORY_DETAIL_REPORT_DOWNLOAD_PATH = "/balancehistory/historical/download";

    //Balance Audit history report
    String BALANCE_HISTORY_BY_REPS_HISTORICAL_REPORT_PATH = "/balancehistory_byreps/historical";
    String BALANCE_HISTORY_BY_REPS_DETAIL_REPORT_GRID_PATH = "/balancehistory_byreps/historical/grid";
    String BALANCE_HISTORY_BY_REPS_DETAIL_REPORT_DOWNLOAD_PATH = "/balancehistory_byreps/historical/download";

    //Balance Audit history report
    //Merchant Remaining Balance Mapping
    String MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_PATH = "/balancemapping/historical";
    String MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_GRID_PATH = "/balancemapping/historical/grid";
    String MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_DOWNLOAD_PATH = "/balancemapping/historical/download";

    //Loyalty Points Program
    String LOYALTY_PROGRAM_PATH = "/loyaltyProgram/historical";
    String LOYALTY_PROGRAM_GRID_PATH = "/loyaltyProgram/historical/grid";
    String LOYALTY_PROGRAM_DOWNLOAD_PATH = "/loyaltyProgram/historical/download";

    //Merchant Remaining Balance Mapping
    String PUSH_MESSAGING_SETUP_REPORT_PATH = "/pushmessaging/historical";
    String PUSH_MESSAGING_SETUP_REPORT_GRID_PATH = "/pushmessaging/historical/grid";
    /*String PUSH_MESSAGING_SETUP_REPORT_UPDATE = "/pushmessaging/historical/update";*/
    String PUSH_MESSAGING_SETUP_REPORT_DOWNLOAD_PATH = "/pushmessaging/historical/download";
    
    //FlowBusinessIntelligence
    String STREETLIGHT_SNAPSHOT_REPORT_PATH = "/streetlightSnapshot/historical";
    String STREETLIGHT_SNAPSHOT_REPORT_GRID_PATH = "/streetlightSnapshot/historical/grid";
    String STREETLIGHT_SNAPSHOT_REPORT_DOWNLOAD_PATH = "/streetlightSnapshot/historical/download";
    
    String STREETLIGHT_TREND_ELEMENTS_PATH = "/streetLightTrendElements/historical";
    String STREETLIGHT_TREND_ELEMENTS_GRID_PATH = "/streetLightTrendElements/historical/grid";
    String STREETLIGHT_TREND_ELEMENTS_DOWNLOAD_PATH = "/streetLightTrendElements/historical/download";
    
    String STREETLIGHT_TREND_DETAIL_PATH = "/streetLightTrendDetail/historical";
    String STREETLIGHT_TREND_DETAIL_GRID_PATH = "/streetLightTrendDetail/historical/grid";
    String STREETLIGHT_TREND_DETAIL_DOWNLOAD_PATH = "/streetLightTrendDetail/historical/download";
  	
	String BUNDLING_ACTIVATION_INIT = "/bundlingActivation";
    
    // Emida Balance on Provider report
    String EMIDA_BALANCE_ON_PROVIDER = "/balance/emidaBalanceOnProvider";
    String EMIDA_BALANCE_ON_PROVIDER_FORM = "/balance/emidaBalanceOnProvider/form";
    String EMIDA_BALANCE_ON_PROVIDER_CONTENT = "/balance/emidaBalanceOnProvider/grid";
    String EMIDA_BALANCE_ON_PROVIDER_DOWNLOAD = "/balance/emidaBalanceOnProvider/download";
    
    String CONSOLIDATE_MONTHLY_SALES_REPORT = "/consolidateMonthlySales";
    
    String TERMINAL_REGISTRATION = "/AllTerminalRegistration";
    String LOGIN_ACTIVITY = "/loginActivity";
    
    String PIN_MANAGEMENT_STATUS = "/pinManagementStatus";
    String PAYMENT_PROCESSOR_INFORMATION = "/paymentProcessorInformation";
    String PULL_MESSAGE_NOTIFY = "/pullMessage";
    String PULL_QUEUE_MESSAGE = "/messages";
    String PULL_QUEUE_VIEW = "/view";
    String PULL_QUEUE_GET_ALL_MESSAGES = "/getAllMessages";
    String GET_AVAILABLE_TERMINALS = "/getAvailableTerminals";
    String PULL_QUEUE_DOWNLOAD = "/downloadReportMessages";
    String CONCILIATION_EXPORTER = "/conciliationExporter";
    
    String CONCILIATION_EXPORT="/conciliationExporter";
    String CONCILIATION_EXPORT_FORM="/conciliationExporter/form";
    
    String EMIDA_ENTITY_LOCATOR="/entityLocator";
    String SORTABLE_PRODUCTS = "/sortableProducts";
    String NO_ACK_PIN_MANAGEMENT = "/noAckPinManagement";
    String CREDENTIALS_PROVIDER = "/credentialsByProvider";
    
    String REP_MERCHANT_BALANCE_SUM = "reps/repMerchantBalanceSum";
    
    // Switch Account
    String REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT = "/switchAccount";
    String REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT_VIEW = "/view";
    String REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT_GET_ALL_TERMINALS = "/getAllTerminals";
    String REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT_DOWNLOAD = "/downloadReport";
    
    String IMAGES_GET_SUGGESTED_SIZE = "/images/getSuggestedSize";
    String ACH_STATEMENT = "/ach-statement";
    String INSTANT_SPIFF_SETTING = "/instant-spiff-setting";
    String INSTANT_SPIFF_SETTING_RTR = "/instant-spiff-setting/rtr";
    String INSTANT_SPIFF_SETTING_ACTIVATE = "/instant-spiff-setting/activate";
    String INSTANT_SPIFF_SETTING_SPIFFS = "/instant-spiff-setting/spiff";
}
