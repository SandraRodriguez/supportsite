package net.emida.supportsite.customers.merchantsNotifier.repository;

import com.debisys.customers.Merchant;
import java.sql.ResultSet;
import java.sql.SQLException;
import net.emida.supportsite.customers.merchantsNotifier.model.SECResetPassword;
import net.emida.supportsite.util.RSUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
@Repository
public class MerchantNotifierRepository implements MerchantNotifierDAO {

    private static final Logger LOG = Logger.getLogger(MerchantNotifierRepository.class);

    private static final String GET_BY_ID = "select m.sms_notification_phone, m.mail_notification_address "
            + " from dbo.merchants m where m.merchant_id = :merchant_id";
    private static final String ADD = "INSERT INTO SEC_ResetPassword (entity_id, entity_reset, clerkName, millennium_no, channel, subscriber, status, create_dt) "
            + "VALUES (:entity_id, :entity_reset, :clerkName, :millennium_no, :channel, :subscriber, 'PENDING', GETDATE())";

    @Autowired
    @Qualifier("masterNamedParamJdbcTemplate")
    private NamedParameterJdbcTemplate parameterJdbcTemplate;

    @Override
    public Merchant findBy(Long merchantId) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("merchant_id", merchantId);
            return parameterJdbcTemplate.queryForObject(GET_BY_ID, parameters, new RowMapper<Merchant>() {
                @Override
                public Merchant mapRow(ResultSet resultSet, int index) throws SQLException {
                    Merchant model = new Merchant();
                    model.setSmsNotificationPhone(RSUtil.getString(resultSet, "sms_notification_phone"));
                    model.setMailNotificationAddress(RSUtil.getString(resultSet, "mail_notification_address"));
                    return model;
                }
            });
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn(ex);
            return new Merchant();
        } catch (DataAccessException ex) {
            LOG.error(ex);
            return new Merchant();
        } catch (Exception ex) {
            LOG.error(ex);
            return new Merchant();
        }
    }

    @Override
    public Integer addForgotPasswordNotification(SECResetPassword model) {
        try {
            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("entity_id", model.getEntity_id())
                    .addValue("entity_reset", model.getEntity_reset())
                    .addValue("clerkName", model.getClerkName())
                    .addValue("millennium_no", model.getMillennium_no())
                    .addValue("channel", model.getChannel())
                    .addValue("subscriber", model.getSubscriber().trim());
            return parameterJdbcTemplate.update(ADD, parameters);
        } catch (EmptyResultDataAccessException ex) {
            LOG.warn("Caught exception {}. Data access exception {}", ex);
            return 0;
        } catch (DataAccessException ex) {
            LOG.warn("Caught exception {}. Data access exception {}", ex);
            return 0;
        } catch (Exception ex) {
            LOG.warn("Caught exception {}. Data access exception {}", ex);
            return 0;
        }
    }

}
