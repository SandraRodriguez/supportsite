package net.emida.supportsite.customers.merchantsNotifier.model;

/**
 *
 * @author janez
 */
public class SECResetPassword {

    private Long entity_id;
    private Long entity_reset;
    private Long millennium_no;
    private String channel;
    private String subscriber;
    private String status;
    private String clerkName;

    public Long getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(Long entity_id) {
        this.entity_id = entity_id;
    }

    public Long getEntity_reset() {
        return entity_reset;
    }

    public void setEntity_reset(Long entity_reset) {
        this.entity_reset = entity_reset;
    }

    public Long getMillennium_no() {
        return millennium_no;
    }

    public void setMillennium_no(Long millennium_no) {
        this.millennium_no = millennium_no;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(String subscriber) {
        this.subscriber = subscriber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClerkName() {
        return clerkName;
    }

    public void setClerkName(String clerkName) {
        this.clerkName = clerkName;
    }

}
