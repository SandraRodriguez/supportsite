package net.emida.supportsite.customers.merchantsNotifier.repository;

import com.debisys.customers.Merchant;
import net.emida.supportsite.customers.merchantsNotifier.model.SECResetPassword;

/**
 *
 * @author janez@emida.net
 * @see https://emidadev.atlassian.net/browse/DTU-5482
 * @since 2019-13-06
 * @version 2018-M
 */
public interface MerchantNotifierDAO {

    Merchant findBy(Long merchantId);
    
    Integer addForgotPasswordNotification(SECResetPassword model);
}
