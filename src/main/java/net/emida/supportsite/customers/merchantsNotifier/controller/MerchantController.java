package net.emida.supportsite.customers.merchantsNotifier.controller;

import com.debisys.customers.Merchant;
import com.debisys.users.SessionData;
import com.debisys.utils.LogChanges;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.customers.merchantsNotifier.model.SECResetPassword;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import net.emida.supportsite.customers.merchantsNotifier.repository.MerchantNotifierDAO;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author janez@emida.net
 */
@RestController
@RequestMapping(UrlPaths.ADMIN_PATH)
public class MerchantController extends BaseController {
	
	private static final Logger logger = Logger.getLogger(MerchantController.class);

    @Autowired
    MerchantNotifierDAO merchantNotifierDAO;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = UrlPaths.PATH_MERCHANT_FIND_BY, method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<?> findClerksBy(@RequestParam("merchantId") Long merchantId) {
        try {
            Merchant merchant = merchantNotifierDAO.findBy(merchantId);
            return ResponseEntity.status(HttpStatus.OK).body(merchant);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex);
        }
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = UrlPaths.PATH_ADD_FP_NOTIFICATION, method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity<?> addForgotPasswordNotification(@RequestBody SECResetPassword model, HttpServletRequest request, HttpSession httpSession) {
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            Long strRefId = new Long(sessionData.getProperty("ref_id"));
            model.setEntity_id(strRefId);
            audit(sessionData, model);
            Integer response = merchantNotifierDAO.addForgotPasswordNotification(model);
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex);
        }
    }

    private void audit(SessionData sessionData, SECResetPassword model) {
    	LogChanges logChanges = new LogChanges(sessionData);
    	try {
			int logType = logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE);
			logChanges.log_changed(logType, String.valueOf(model.getEntity_id()), "", "", "Passwordreset request", sessionData.getUser().getName(), "SupportSite");
		} catch (Exception e) {
			logger.error("Could not store password change auditory", e);
		}
	}

	@Override
    public int getSection() {
        return 2;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
