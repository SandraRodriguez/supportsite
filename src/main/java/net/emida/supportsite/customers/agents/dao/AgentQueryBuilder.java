package net.emida.supportsite.customers.agents.dao;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Franky Villadiego
 */
public class AgentQueryBuilder {

    private static final Logger log = LoggerFactory.getLogger(AgentQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;

    private String accessLevel;
    private String distChainType;
    private String refId;

    private String searchCriteria;

    public AgentQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.accessLevel = (String)queryData.get("accessLevel");
        this.distChainType = (String)queryData.get("distChainType");
        this.refId = (String)queryData.get("refId");
        this.searchCriteria = (String) queryData.get("searchCriteria");
    }

    public AgentQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";

        this.accessLevel = (String)queryData.get("accessLevel");
        this.distChainType = (String)queryData.get("distChainType");
        this.refId = (String)queryData.get("refId");

        this.searchCriteria = (String) queryData.get("searchCriteria");
    }


    public String buildListQuery(){

        if(sortFieldName == null || sortFieldName.isEmpty()){
            sortFieldName = "businessName";
            sortOrder = " ASC ";
        }
        if("contact".equalsIgnoreCase(sortFieldName.trim())){
            sortFieldName = "firstName, lastName";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName).append(" ").append(sortOrder).append( ") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();



    }

    private String mainQuery(){
        StringBuilder sb = new StringBuilder()
        .append(listFields())
        .append(listFrom())
        .append(listWhere());
        //.append(" ORDER BY ").append(sortFieldName) .append(" ").append(sortOrder);
        return sb.toString();
    }

    public String buildCountQuery(){

        StringBuilder sb0 = new StringBuilder()
        .append(countFields())
        .append(listFrom())
        .append(listWhere());

        return sb0.toString();
    }

    private String listFields(){
        StringBuilder sb = new StringBuilder()
        .append("SELECT DISTINCT businessname AS businessName,")
        .append(" rep_id AS agentId, ")
        .append(" city, state, firstname, lastname, phone ");
        return sb.toString();
    }

    private String listFrom(){
        return " FROM reps WITH(NOLOCK) ";
    }

    private String listWhere(){
        StringBuilder sb = new StringBuilder()
        .append(" WHERE ")
        .append(" type = 4 ")
        .append(" AND iso_id = ").append(refId).append(" ")
        .append(" AND rep_id <> ").append(refId).append(" ");
        if(!StringUtils.isEmpty(searchCriteria)){
            sb.append(" AND (")
            .append(" rep_id like '%").append(searchCriteria).append("%' ")
            .append(" OR businessname like '%").append(searchCriteria).append("%' ")
            .append(" OR phone like '%").append(searchCriteria).append("%' ")
            .append(")");
        }
        return sb.toString();
    }

    private String countFields(){
        return " SELECT COUNT(*) ";
    }

}
