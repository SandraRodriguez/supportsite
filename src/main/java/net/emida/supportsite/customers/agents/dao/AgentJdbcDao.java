package net.emida.supportsite.customers.agents.dao;

import net.emida.supportsite.customers.agents.AgentDao;
import net.emida.supportsite.reports.spiff.deatil.SpiffDetailDao;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailQueryBuilder;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportMapper;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportModel;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */

@Repository
public class AgentJdbcDao implements AgentDao {

    private static final Logger log = LoggerFactory.getLogger(AgentJdbcDao.class);

    private static String SQL_COUNT_ALL= "SELECT COUNT(*) from reps";

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll(){
        try {
            return jdbcTemplate.queryForObject(SQL_COUNT_ALL, Long.class);
        }catch (DataAccessException ex){
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    public List<AgentModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData){
        try {
            AgentQueryBuilder queryBuilder = new AgentQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();

            log.debug("Query : {}", SQL_QUERY);

            return jdbcTemplate.query(SQL_QUERY, new AgentMapper());
        }catch (DataAccessException ex){
            log.error("Data access error in list agents");
            throw new TechnicalException(ex.getMessage(), ex);
        }catch (Exception ex){
            log.error("Exception in list agents");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {

            AgentQueryBuilder queryBuilder = new AgentQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();

            log.debug("Query Count : {}", SQL_QUERY_COUNT);

            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        }catch (DataAccessException ex){
            log.error("Data access error in count agents");
            throw new TechnicalException(ex.getMessage(), ex);
        }catch (Exception ex){
            log.error("Exception in count agents");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }



}
