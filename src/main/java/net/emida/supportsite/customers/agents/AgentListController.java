package net.emida.supportsite.customers.agents;

import com.debisys.customers.Merchant;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.customers.agents.dao.AgentModel;
import net.emida.supportsite.customers.agents.dao.AgentResponse;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.customers.merchantsNotifier.repository.MerchantNotifierDAO;

/**
 * @author Franky Villadiego
 */
@Controller
@RequestMapping(value = UrlPaths.CUSTOMER_PATH)
public class AgentListController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(AgentListController.class);

    @Autowired
    AgentDao agentDao;   

    @RequestMapping(value = UrlPaths.AGENTS_PATH, method = RequestMethod.GET)
    public String listAgents(Model model, @RequestParam(required = false) String searchCriteria) throws TechnicalException, ApplicationException {

        log.debug("Get agents searchCriteria : {}", searchCriteria);
        fillPerms(model);
        Map<String, Object> queryData = fillQueryData();
        queryData.put("searchCriteria", searchCriteria);

        model.addAttribute("searchCriteria", searchCriteria);
        return "customers/agents/agents";
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.AGENTS_GRID_PATH, produces = "application/json")
    @ResponseBody
    public AgentResponse listGrid(Model model, @RequestParam Integer start, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam Integer rows, @RequestParam(required = false) String searchCriteria) {
        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", start, sortField, sortOrder, rows);

        fillPerms(model);

        Map<String, Object> queryData = fillQueryData();
        queryData.put("searchCriteria", searchCriteria);

        Long totalRecords = agentDao.count(queryData);
        log.debug("Total records = {}", totalRecords);
        model.addAttribute("totalRecords", totalRecords);

        List<AgentModel> list = agentDao.list(start, rows + start - 1, sortField, sortOrder, queryData);
        AgentResponse agentResponse = new AgentResponse();
        agentResponse.setAgents(list);
        agentResponse.setTotalRecords(totalRecords);
        agentResponse.setPermAgents((Boolean) model.asMap().get("PERM_AGENTS"));
        agentResponse.setPermLogins((Boolean) model.asMap().get("PERM_LOGINS"));
        agentResponse.setRefTypeAgent(Integer.valueOf(DebisysConstants.PW_REF_TYPE_AGENT));
        return agentResponse;
    }

    private void fillPerms(Model model) {
        boolean PERM_AGENTS = allowPermission(DebisysConstants.PERM_AGENTS);
        model.addAttribute("PERM_AGENTS", PERM_AGENTS);
        boolean PERM_LOGINS = allowPermission(DebisysConstants.PERM_LOGINS);
        model.addAttribute("PERM_LOGINS", PERM_LOGINS);
        model.addAttribute("PW_REF_TYPE_AGENT", DebisysConstants.PW_REF_TYPE_AGENT);
    }

    private Map<String, Object> fillQueryData() {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        return queryData;
    }    

    @Override
    public int getSection() {
        return 2;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }
}
