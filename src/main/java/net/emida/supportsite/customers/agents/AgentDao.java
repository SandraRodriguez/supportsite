package net.emida.supportsite.customers.agents;

import net.emida.supportsite.customers.agents.dao.AgentModel;
import net.emida.supportsite.mvc.dao.BasicDao;

/**
 * @author Franky Villadiego
 */
public interface AgentDao extends BasicDao<AgentModel> {
}
