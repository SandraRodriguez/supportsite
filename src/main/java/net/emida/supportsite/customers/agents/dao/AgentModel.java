package net.emida.supportsite.customers.agents.dao;

import com.debisys.utils.StringUtil;
import org.apache.commons.lang.StringUtils;

/**
 * @author Franky Villadiego
 */
public class AgentModel {

    private String businessName;
    private Long agentId;
    private String city;
    private String state;
    private String firstName;
    private String lastName;
    private String contact;
    private String phone;


    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getContact() {
        contact = (StringUtils.isEmpty(firstName) ? "" : firstName) + " " + (StringUtils.isEmpty(lastName) ? "" : lastName);
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
