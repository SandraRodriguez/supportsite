package net.emida.supportsite.customers.agents.dao;

import java.util.List;

/**
 * @author Franky Villadiego
 */
public class AgentResponse {

    private Long totalRecords;
    private List<AgentModel> agents;
    private boolean permAgents;
    private boolean permLogins;
    private Integer refTypeAgent;

    public boolean isPermAgents() {
        return permAgents;
    }

    public void setPermAgents(boolean permAgents) {
        this.permAgents = permAgents;
    }

    public boolean isPermLogins() {
        return permLogins;
    }

    public void setPermLogins(boolean permLogins) {
        this.permLogins = permLogins;
    }

    public Integer getRefTypeAgent() {
        return refTypeAgent;
    }

    public void setRefTypeAgent(Integer refTypeAgent) {
        this.refTypeAgent = refTypeAgent;
    }

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<AgentModel> getAgents() {
        return agents;
    }

    public void setAgents(List<AgentModel> agents) {
        this.agents = agents;
    }
}
