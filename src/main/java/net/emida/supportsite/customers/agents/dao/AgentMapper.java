package net.emida.supportsite.customers.agents.dao;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Franky Villadiego
 */
public class AgentMapper implements RowMapper<AgentModel>{

    @Override
    public AgentModel mapRow(ResultSet rs, int i) throws SQLException {
        AgentModel model = new AgentModel();
        model.setBusinessName(rs.getString("businessName"));
        model.setAgentId(rs.getLong("agentId"));
        model.setCity(rs.getString("city"));
        model.setState(rs.getString("state"));
        model.setFirstName(rs.getString("firstname"));
        model.setLastName(rs.getString("lastname"));
        model.setPhone(rs.getString("phone"));
        return model;
    }
}
