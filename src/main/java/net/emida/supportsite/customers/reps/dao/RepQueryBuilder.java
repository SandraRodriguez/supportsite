package net.emida.supportsite.customers.reps.dao;

import com.debisys.utils.DebisysConstants;

import java.util.Map;

/**
 * @author Franky Villadiego
 */
public class RepQueryBuilder {

    private final String accessLevel;
    private final String distChainType;
    private final String refId;
    private final boolean sharedBalance;


    public RepQueryBuilder(Map<String, Object> queryData) {

        this.accessLevel = (String)queryData.get("accessLevel");
        this.distChainType = (String)queryData.get("distChainType");
        this.refId = (String)queryData.get("refId");
        this.sharedBalance = (queryData.get("sharedBalance") != null) ?(Boolean)queryData.get("sharedBalance"):false;
    }


    public String buildListQuery(){

        StringBuilder query = new StringBuilder();
        query.append("SELECT r.rep_id AS repId, r.businessname AS repName ");
        query.append("FROM reps AS r WITH (NOLOCK) ");
        query.append("WHERE 1=1 ");
        switch (accessLevel) {
            case DebisysConstants.ISO:
                if (distChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    query.append("AND r.iso_id=").append(refId).append(" AND r.type=").append(DebisysConstants.REP_TYPE_REP).append(" ");
                }
                else if (distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    query.append("AND r.rep_id IN (SELECT r1.rep_id FROM reps AS r1 WITH (NOLOCK) WHERE r1.type=").append(DebisysConstants.REP_TYPE_REP);
                    query.append(" AND r1.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH (NOLOCK) WHERE r2.type=").append(DebisysConstants.REP_TYPE_SUBAGENT);
                    query.append(" AND r2.iso_id IN (SELECT r3.rep_id FROM reps AS r3 WITH (NOLOCK) WHERE r3.type=").append(DebisysConstants.REP_TYPE_AGENT);
                    query.append(" AND r3.iso_id =").append(refId).append("))) ");
                }   break;
            case DebisysConstants.AGENT:
                query.append("AND r.rep_id IN (SELECT r1.rep_id FROM reps AS r1 WITH (NOLOCK) WHERE r1.type=").append(DebisysConstants.REP_TYPE_REP);
                query.append(" AND r1.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH (NOLOCK) WHERE r2.type=").append(DebisysConstants.REP_TYPE_SUBAGENT);
                query.append(" AND r2.iso_id =").append(refId).append(")) ");
                break;
            case DebisysConstants.SUBAGENT:
                query.append("AND r.iso_id =").append(refId).append(" AND r.type =").append(DebisysConstants.REP_TYPE_REP);
                break;
            default:
                break;
        }
        if(sharedBalance) {
            query.append(" AND r.sharedbalance=1 ");
        }

        query.append("ORDER BY r.businessname");

        return query.toString();

    }
    
    public String getRepMerchantsBalanceSumQuery(long repId){
        StringBuilder query = new StringBuilder();
        
        query.append("SELECT    ISNULL(SUM(LiabilityLimit - RunningLiability), 0) AS MerchantBalanceSum ");
        query.append("FROM	dbo.merchants M WITH(NOLOCK) ");
        query.append("		INNER JOIN dbo.CreditTypes CT WITH(NOLOCK) ");
        query.append("              ON M.CreditTypeId = CT.CreditTypeID ");
        query.append("		    AND CT.CreditTypeCode = 'PREPAID' ");
        query.append("WHERE	rep_id = ").append(String.valueOf(repId));
        query.append("          AND M.cancelled = 0");
                
        return query.toString();
    }
}
