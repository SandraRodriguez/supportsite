package net.emida.supportsite.customers.reps;

import java.math.BigDecimal;
import net.emida.supportsite.util.exceptions.TechnicalException;

import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
public interface RepDao {


    List<Map<String,Object>> list(Map<String, Object> queryData, boolean sharedBalance) throws TechnicalException;
    
    BigDecimal getMerchantBalanceSum(Map<String, Object> queryData);
}
