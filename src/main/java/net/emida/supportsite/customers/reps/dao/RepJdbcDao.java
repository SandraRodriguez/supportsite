package net.emida.supportsite.customers.reps.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.customers.reps.RepDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author Franky Villadiego
 */
@Repository
public class RepJdbcDao implements RepDao {

    private static final Logger LOG = LoggerFactory.getLogger(RepJdbcDao.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> list(Map<String, Object> queryData, boolean sharedBalance) {
        try {
            queryData.put("sharedBalance", sharedBalance);
            RepQueryBuilder queryBuilder = new RepQueryBuilder(queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();

            LOG.debug("Query : {}", SQL_QUERY);

            return jdbcTemplate.queryForList(SQL_QUERY);
        } catch (DataAccessException ex) {
            LOG.error("Data access error in list agents");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            LOG.error("Exception in list agents");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public BigDecimal getMerchantBalanceSum(Map<String, Object> queryData) {

        try {
            RepQueryBuilder queryBuilder = new RepQueryBuilder(queryData);
            long repId = Long.valueOf(queryData.get("repId").toString());
            String SQL_QUERY = queryBuilder.getRepMerchantsBalanceSumQuery(repId);

            LOG.debug("Query : {}", SQL_QUERY);
            BigDecimal retValue = jdbcTemplate.queryForObject(SQL_QUERY, BigDecimal.class);
            LOG.info(String.format("Balance sum of merchants from rep %d is %f ", repId, retValue));
            return retValue;
        } catch (DataAccessException ex) {
            LOG.error("Data access error in getMerchantBalanceSum");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            LOG.error("Exception in getMerchantBalanceSum");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

}
