/*
 * EMIDA all rights reserved 1999-2025.
 */
package net.emida.supportsite.customers.reps;

import com.debisys.util.NumberUtil;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fernandob
 */
@Controller
@RequestMapping(value = UrlPaths.CUSTOMER_PATH)
public class RepController  extends BaseController{
    
    @Autowired
    private RepDao repDao;    

    @Override
    public int getSection() {
        return 2;
    }

    @Override
    public int getSectionPage() {
        return 22;
    }
    
    @RequestMapping(value = UrlPaths.REP_MERCHANT_BALANCE_SUM, method = RequestMethod.GET)
    @ResponseBody
    public String getRepMerchantBalanceSum(@RequestParam long repId){
        Map<String, Object> queryData = new HashMap<>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("repId", repId);
        BigDecimal merchantBalanceSum = repDao.getMerchantBalanceSum(queryData);
        return NumberUtil.formatCurrency(merchantBalanceSum.toEngineeringString());
    }
    
}
