package net.emida.supportsite.util.security;

import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author Franky Villadiego
 */

//@Component
public class DefaultAESCipher implements AESCipher {

    private static final Logger log = LoggerFactory.getLogger(DefaultAESCipher.class);

    //private final Cipher ecipher;
    //private final Cipher dcipher;
    private SecretKeyProvider secretKeyProvider;
    private static final String base = "BOEVNAASDRVVQOPL";
    private SecretKey secretKey;


    //@Autowired
    public DefaultAESCipher(SecretKeyProvider secretKeyProvider) {
        this.secretKeyProvider = secretKeyProvider;
        byte[] secretKeyBytes = secretKeyProvider.getSecretKeyBytes();
        secretKey = new SecretKeySpec(secretKeyBytes, "AES");
    }

    public String encrypt(String encrypt) throws TechnicalException {
        try {
            Cipher ecipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(base.getBytes("UTF-8"));
            ecipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
            byte[] bytes = encrypt.getBytes("UTF8");
            byte[] encrypted = ecipher.doFinal(bytes);
            return DatatypeConverter.printBase64Binary(encrypted);
        }catch (Exception ex){
            log.error("Error encrypting. Ex:{}", ex);
        }
        return null;
    }

    public String decrypt(String encrypt) throws TechnicalException {
        try {
            Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv2 = new IvParameterSpec(base.getBytes("UTF-8"));
            dcipher.init(Cipher.DECRYPT_MODE, secretKey, iv2);
            byte[] bytes = DatatypeConverter.parseBase64Binary(encrypt);
            byte[] decrypted = dcipher.doFinal(bytes);
            return new String(decrypted, "UTF8");
        }catch (Exception ex){
            log.error("Error decrypting. Ex:{}", ex);
        }
        return null;
    }


/*    @Autowired
    public void setSecretKeyProvider(SecretKeyProvider secretKeyProvider) {
        this.secretKeyProvider = secretKeyProvider;
    }*/
}
