package net.emida.supportsite.util.security;

import net.emida.supportsite.util.exceptions.TechnicalException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.UnsupportedEncodingException;

/**
 * @author Franky Villadiego
 */
public interface AESCipher {


    String encrypt(String encrypt) throws TechnicalException;


    String decrypt(String encrypt) throws TechnicalException;

}
