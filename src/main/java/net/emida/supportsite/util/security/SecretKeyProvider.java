package net.emida.supportsite.util.security;

/**
 * @author Franky Villadiego
 */


public interface SecretKeyProvider {


    String getSecretKey();

    byte[] getSecretKeyBytes();

}
