package net.emida.supportsite.util.security;

/**
 *
 * @author janez
 */
public class SecurityCipherService {

    private static SecurityCipherService instance;
    private net.emida.crypto.AESCipher cipher;
    private net.emida.crypto.SecretKeyProvider keyProvider;

    public SecurityCipherService(String seeder) {
        try {
            byte[] secretKey = seeder.getBytes();

            keyProvider = new net.emida.crypto.DefaultSecretKeyProvider();
            keyProvider.setSecretKeyFile(secretKey);

            cipher = new net.emida.crypto.DefaultAESCipher();
            cipher.setSecretProvider(keyProvider);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static SecurityCipherService getInstance(String seeder) {
        if (instance == null) {
            instance = new SecurityCipherService(seeder);
        }
        return instance;
    }

    public String encrypt(String content) {
        return cipher.encrypt(content);
    }

    public String decrypt(String content) {
        return cipher.decrypt(content);
    }
    
    public String decryptClerk(String content) {
        return cipher.decryptClerk(content);
    }

    public String generateRandomPassword() {
        return cipher.generatePassword();
    }
    
    public boolean isOldAlgorithm(String loginPassword, String encryptedPassword) {
        return cipher.isOldAlgorithm(loginPassword, encryptedPassword);
    }
    
    public String encryptWithOldAlgorithm(String encrypt) {
        return cipher.encryptWithOldAlgorithm(encrypt);
    }
    

}
