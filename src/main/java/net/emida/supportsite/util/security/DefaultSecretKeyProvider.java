package net.emida.supportsite.util.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import javax.annotation.PostConstruct;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Franky Villadiego
 */

//@Component
public class DefaultSecretKeyProvider implements SecretKeyProvider, ResourceLoaderAware{

    private static final Logger log = LoggerFactory.getLogger(DefaultSecretKeyProvider.class);


    byte[] SECRET_KEY = {
            (byte) 0x77, (byte) 0x72, (byte) 0x65, (byte) 0x36,
            (byte) 0x48, (byte) 0x32, (byte) 0x35, (byte) 0x68,
            (byte) 0x45, (byte) 0x73, (byte) 0x75, (byte) 0x66,
            (byte) 0x72, (byte) 0x75, (byte) 0x47, (byte) 0x75
    };


    String RAW_SECRET_KEY = "wre6H25hEsufruGu";

    private File file;
    private ResourceLoader resourceLoader;


    @PostConstruct
    private void init(){
        try {
            file = loadFile();
            if(file == null){
                log.error("Error building key provider. Resource not found.");
            }else {
                byte[] fileBytes = FileCopyUtils.copyToByteArray(file);
                String fileHash = getHash(fileBytes);
                RAW_SECRET_KEY = fileHash;
                SECRET_KEY = RAW_SECRET_KEY.getBytes();
            }
        }catch (Exception ex){
            log.error("!!!!!! ERROR BUILDING KEY PROVIDER !!!!!!!!!!.  Ex:", ex);
        }
    }

    private File loadFile() throws IOException {
        Resource resource = resourceLoader.getResource("WEB-INF/webapp.properties.temp");
        boolean exists = resource.exists();
        if(!exists) {
            log.error("Error loading resource. File not found.");
        }else{
            return resource.getFile();
        }
        return null;
    }

    private String getHash(byte[] bytes) throws NoSuchAlgorithmException {
        MessageDigest msgDigest = MessageDigest.getInstance("SHA-1");
        byte[] digest = msgDigest.digest(bytes);
        String hexBinary = DatatypeConverter.printHexBinary(digest);
        hexBinary = hexBinary.substring(0, 16);
        return hexBinary;
    }


    @Override
    public String getSecretKey() {
        return RAW_SECRET_KEY;
    }

    @Override
    public byte[] getSecretKeyBytes() {
        return SECRET_KEY;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
