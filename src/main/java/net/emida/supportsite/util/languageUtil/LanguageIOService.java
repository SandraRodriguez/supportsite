package net.emida.supportsite.util.languageUtil;

import com.debisys.users.SessionData;
import java.util.List;

/**
 *
 * @author janez
 */
public interface LanguageIOService {

    /**
     *
     * @param sessionData
     * @param labels
     * @return
     */
    public String mapperLanguage(SessionData sessionData, List<String> labels);
}
