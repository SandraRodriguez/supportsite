package net.emida.supportsite.util.languageUtil;

import java.io.Serializable;

/**
 *
 * @author janez
 */
public class JSONContent implements Serializable {

    private String key;
    private String value;

    public JSONContent() {
    }

    public JSONContent(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
