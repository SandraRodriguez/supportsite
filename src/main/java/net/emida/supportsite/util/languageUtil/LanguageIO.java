package net.emida.supportsite.util.languageUtil;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author janez
 */
@Service
public class LanguageIO implements LanguageIOService {

    private static final Logger LOG = LoggerFactory.getLogger(LanguageIO.class);

    @Override
    public String mapperLanguage(SessionData sessionData, List<String> labels) {
        Map<String, String> map = new HashMap<String, String>();
        Gson gson = new Gson();        
        try {                        
            for (String label : labels) {                               
                map.put(label.replace(".", ""), Languages.getString(label, sessionData.getLanguage()));
            }
            String r = gson.toJson(map);
            return r;
        } catch (TechnicalException err) {
            LOG.error(err.getLocalizedMessage(), err);
            return new String();
        }
    }

}
