package net.emida.supportsite.util;


import com.debisys.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Franky Villadiego
 */


public class TimeZoneUtils {

    private static final Logger log = LoggerFactory.getLogger(TimeZoneUtils.class);



    public static Date getLocalDateTime(Date utcDate, TimeZoneInfo timeZoneInfo) {
        //Este codigo emula un poco al SP GetLocalDateTime. Especificamente el codigo la
        //instruccion SELECT que multiplica algunos valores por -1 (Bias, DaylightBias) y a otros
        // le suma 1(StandardDayOfWeek, DaylightDayOfWeek).
        //Por lo anterior, se hace una copia para no modificar el timeZoneInfo del parametro ya que puede ser una instancia compartida
        TZParam tz = new TZParam(timeZoneInfo);

        return getAdjustedDateTime(utcDate, tz, false);
    }

    public static Date getUTCDateTime(Date sourceDate, TimeZoneInfo timeZoneInfo) {
        //Este codigo emula un poco al SP GetUTCFromLocalDateTime. Especificamente el codigo la
        //instruccion SELECT que multiplica algunos valores por -1 (Bias, DaylightBias) y a otros
        // le suma 1(StandardDayOfWeek, DaylightDayOfWeek).
        //Por lo anterior, se hace una copia para no modificar el timeZoneInfo del parametro ya que puede ser una instancia compartida
        TZParam tz = new TZParam(timeZoneInfo);

        return getAdjustedDateTime(sourceDate, tz, true);
    }


    /**
     * El origen del TimeZoneInfo puede ser : </p>
     *  1. El servidor (propiedad 'DefaultTimeZone' o TimeZoneInfo 'Coordinated Universal Time') </p>
     *  2. El cliente (columna timeZoneId de la tabla merchants o reps) </p>
     *
     * @param sourceDate
     * @param tz
     * @param toUTC
     * @return
     */
    private static Date getAdjustedDateTime(Date sourceDate, TZParam tz, boolean toUTC) {

        int daylightBiasFactor = 0;
        if (tz == null) {
            throw new IllegalArgumentException("Invalid TimeZoneInfo!!");
        }

        if (tz.getStandardMonth() == 0) {
            int valueBias = tz.getBias();
            if (toUTC) {
                valueBias = -tz.getBias();
            }
            return DateUtil.plusMinutes(sourceDate, valueBias);
        }

        Calendar calendar = DateUtil.toCalendar(sourceDate);
        int year = calendar.get(Calendar.YEAR);

        Date standardDate = getDaylightStandardDateTime(year, tz.getStandardMonth(), tz.getStandardDayOfWeek(), tz.getStandardWeek(), tz.getStandardHour());

        Date daylightDate = getDaylightStandardDateTime(year, tz.getDaylightMonth(), tz.getDaylightDayOfWeek(), tz.getDaylightWeek(), tz.getDaylightHour());

        if (standardDate.getTime() > daylightDate.getTime()) {
            Date temp = DateUtil.plusMinutes(sourceDate, tz.getBias());
            if (daylightDate.getTime() < temp.getTime() && temp.getTime() < standardDate.getTime()) {
                daylightBiasFactor = 1;
            }
            else if (standardDate.getTime() < temp.getTime() && temp.getTime() < daylightDate.getTime()) {
                daylightBiasFactor = 0;
            }
        }
        int operation = (tz.getBias() + (daylightBiasFactor * tz.getDaylightBias()));
        if (toUTC) {
            operation = -(tz.getBias() + (daylightBiasFactor * tz.getDaylightBias()));
        }
        return DateUtil.plusMinutes(sourceDate, operation);
    }

    private static Date getDaylightStandardDateTime(int year, short month, short dayOfWeek, short week, short hour) {
        Date ret = null;
        try {
            //Find day of the week of the first day of a given month
            Date firstOfMonth = DateUtil.parseStringToDate(year + "/" + (month) + "/01", "yyyy/MM/dd");

            //5th week means the last week of the month, so go one month forth, then one week back
            if (week == 5) {
                firstOfMonth = DateUtil.plusMonths(firstOfMonth, 1);
            }
            Calendar calendar = DateUtil.toCalendar(firstOfMonth);
            int doW = calendar.get(Calendar.DAY_OF_WEEK);

            //Find first given day of the week of the given month:
            if (doW > dayOfWeek) {
                ret = DateUtil.plusDays(firstOfMonth, 7 + dayOfWeek - doW);
            }
            else {
                ret = DateUtil.plusMonths(firstOfMonth, dayOfWeek - doW);
            }

            //Advance to the given week ( 5th week means the last one of the month )
            if (week < 5) {
                ret = DateUtil.plusMonths(ret, week - 1);
            }
            else {
                //The last week of the previous month; go one week backward
                ret = DateUtil.plusMonths(ret, -1);
            }
            ret = DateUtil.plusHours(ret, hour);
        }
        catch (Exception ex) {
            log.error("Error in getDaylightStandardDateTime. Ex :{}", ex);
        }
        return ret;
    }


}

class TZParam{

    private short bias;
    private short daylightBias;

    private short standardMonth;
    private short daylightMonth;

    private short standardDayOfWeek;
    private short daylightDayOfWeek;

    private short standardWeek;
    private short daylightWeek;

    private short standardHour;
    private short daylightHour;


    public TZParam(TimeZoneInfo tzi) {
        this.bias = (short)(tzi.getBias() * -1);
        this.daylightBias = (short) (tzi.getDaylightBias() * -1);
        this.standardMonth = tzi.getStandardMonth();
        this.daylightMonth = tzi.getDaylightMonth();
        this.standardDayOfWeek = (short) (tzi.getStandardDayOfWeek() + 1);
        this.daylightDayOfWeek = (short) (tzi.getDaylightDayOfWeek() + 1);
        this.standardWeek = tzi.getStandardWeek();
        this.daylightWeek = tzi.getDaylightWeek();
        this.standardHour = tzi.getStandardHour();
        this.daylightHour = tzi.getDaylightHour();
    }

    public short getBias() {
        return bias;
    }

    public short getStandardMonth() {
        return standardMonth;
    }

    public short getStandardDayOfWeek() {
        return standardDayOfWeek;
    }

    public short getStandardWeek() {
        return standardWeek;
    }

    public short getStandardHour() {
        return standardHour;
    }

    public short getDaylightBias() {
        return daylightBias;
    }

    public short getDaylightMonth() {
        return daylightMonth;
    }

    public short getDaylightDayOfWeek() {
        return daylightDayOfWeek;
    }

    public short getDaylightWeek() {
        return daylightWeek;
    }

    public short getDaylightHour() {
        return daylightHour;
    }
}


