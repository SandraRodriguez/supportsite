/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.util;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author emida
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationParametersResponse {
    
    private int resultStatus;
    @XmlElement(name="ResponseCode")
    private String resultCode;
    @XmlElement(name="ResponseMessage")
    private String resultMessage;
    private List lsResult;

    public int getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(int resultStatus) {
        this.resultStatus = resultStatus;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public List getLsResult() {
        return lsResult;
    }

    public void setLsResult(List lsResult) {
        this.lsResult = lsResult;
    }
}
