package net.emida.supportsite.util;

/**
 * @author Franky Villadiego
 */

public class TimeZoneInfo {


/*    private Integer timeZoneID;
    private String codeName;
    private String displayName;*/

    private short bias;

    private short standardBias;
    private short standardMonth;
    private short standardDayOfWeek;
    private short standardWeek;
    private short standardHour;

    private short daylightBias;
    private short daylightMonth;
    private short daylightDayOfWeek;
    private short daylightWeek;
    private short daylightHour;


    public short getBias() {
        return bias;
    }

    public void setBias(short bias) {
        this.bias = bias;
    }

    public short getStandardBias() {
        return standardBias;
    }

    public void setStandardBias(short standardBias) {
        this.standardBias = standardBias;
    }

    public short getStandardMonth() {
        return standardMonth;
    }

    public void setStandardMonth(short standardMonth) {
        this.standardMonth = standardMonth;
    }

    public short getStandardDayOfWeek() {
        return standardDayOfWeek;
    }

    public void setStandardDayOfWeek(short standardDayOfWeek) {
        this.standardDayOfWeek = standardDayOfWeek;
    }

    public short getStandardWeek() {
        return standardWeek;
    }

    public void setStandardWeek(short standardWeek) {
        this.standardWeek = standardWeek;
    }

    public short getStandardHour() {
        return standardHour;
    }

    public void setStandardHour(short standardHour) {
        this.standardHour = standardHour;
    }

    public short getDaylightBias() {
        return daylightBias;
    }

    public void setDaylightBias(short daylightBias) {
        this.daylightBias = daylightBias;
    }

    public short getDaylightMonth() {
        return daylightMonth;
    }

    public void setDaylightMonth(short daylightMonth) {
        this.daylightMonth = daylightMonth;
    }

    public short getDaylightDayOfWeek() {
        return daylightDayOfWeek;
    }

    public void setDaylightDayOfWeek(short daylightDayOfWeek) {
        this.daylightDayOfWeek = daylightDayOfWeek;
    }

    public short getDaylightWeek() {
        return daylightWeek;
    }

    public void setDaylightWeek(short daylightWeek) {
        this.daylightWeek = daylightWeek;
    }

    public short getDaylightHour() {
        return daylightHour;
    }

    public void setDaylightHour(short daylightHour) {
        this.daylightHour = daylightHour;
    }
}
