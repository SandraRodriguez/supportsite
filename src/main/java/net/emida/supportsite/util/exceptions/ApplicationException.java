package net.emida.supportsite.util.exceptions;

/**
 * @author fvilladiego
 */
public class ApplicationException extends RuntimeException {


    private static final long serialVersionUID = 4750305284256907830L;

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

}
