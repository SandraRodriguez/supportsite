package net.emida.supportsite.util.exceptions;

/**
 * @author fvilladiego
 */
public class TechnicalException extends RuntimeException {

    public TechnicalException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
