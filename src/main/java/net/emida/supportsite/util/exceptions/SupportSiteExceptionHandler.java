package net.emida.supportsite.util.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author fvilladiego
 */

public class SupportSiteExceptionHandler implements HandlerExceptionResolver {


    private static Logger log = LoggerFactory.getLogger(SupportSiteExceptionHandler.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        if(ex instanceof TechnicalException) {
            log.error("TechnicalException {}", ex.getMessage());
        }
        if(ex instanceof ApplicationException) {
            log.error("ApplicationException {}", ex.getMessage());
        }
        return null;
    }

    private ModelAndView handleTechnicalAndApplicationException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        ModelAndView modelAndView = new ModelAndView();

        return modelAndView;
    }

}
