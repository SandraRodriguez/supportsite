package net.emida.supportsite.util.pagination;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

public class PaginationHelper<E> {
	
	
	  public Page<E> fetchPage(
	            final JdbcTemplate jt,
	            final String sqlCountRows,
	            final String sqlFetchRows,
	            final Object args[],
	            final int rowNumberStart,
	            final int pageSize,
	            final RowMapper<E> rowMapper,
	            final String sortFieldName,
	            final String sortOrder) {
		  
		  	
		    StringBuffer sqlQueryWhitPaginationBuffer= new StringBuffer();
		    
	        int rowCount = (jt.queryForObject(sqlCountRows, Long.class)).intValue();
	        int starRow = rowNumberStart;
	        int  numberRowsinThisPage= rowCount -starRow;
	        int endRow;
	        
	        if(numberRowsinThisPage <pageSize  ){
	        	endRow= starRow+numberRowsinThisPage;
	        }
	        else{
	        	endRow =starRow+pageSize;
	        }

		    sqlQueryWhitPaginationBuffer.append("SELECT * FROM ( \n");
		    sqlQueryWhitPaginationBuffer.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + " " + sortOrder).append(") AS RWN, * FROM (");
		    sqlQueryWhitPaginationBuffer.append(sqlFetchRows);
		    sqlQueryWhitPaginationBuffer.append(") AS tbl1 ");
		    sqlQueryWhitPaginationBuffer.append(") AS tbl2 ");
		    sqlQueryWhitPaginationBuffer.append(" WHERE RWN BETWEEN ").append(starRow).append(" AND ").append(endRow);



	      

	        // create the page object
	        final Page<E> page = new Page<E>();
	        page.setPagesAvailable(endRow);

	        jt.query(sqlQueryWhitPaginationBuffer.toString(),args,new ResultSetExtractor() {
	        				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
	                        final List pageItems = page.getPageItems();
	                        int rowNumber= 1;
	                        while (rs.next() ) {
	                                pageItems.add(rowMapper.mapRow(rs, rowNumber++));
	                        }
	                        return page;
	                    }
	                });
	        return page;
	    }
}




