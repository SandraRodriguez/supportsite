/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.util.emidaEntityLocator.repository;

import java.util.List;
import net.emida.supportsite.util.emidaEntityLocator.dto.EmidaEntity;

/**
 *
 * @author fernandob
 */
public interface IEmidaEntityLocatorRepository {

    List<EmidaEntity> getIsos();

    List<EmidaEntity> getMerchantsByDba(String dba);

    List<EmidaEntity> getMerchantsByRepId(long repId);

    List<EmidaEntity> getRepsByBusinessName(String businessName);

    List<EmidaEntity> getRepsChildsByParentId(long parentId);
    
}
