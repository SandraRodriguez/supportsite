/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.util.emidaEntityLocator.dto;

/**
 *
 * @author fernandob
 */
public class EmidaEntity {

    private long entityId;
    private int entityType;
    private String entityName;

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }

    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public EmidaEntity() {
        this.entityId = 0L;
        this.entityType = 0;
        this.entityName = "";
    }

    public EmidaEntity(long entityId, int entityType, String entityName) {
        this.entityId = entityId;
        this.entityType = entityType;
        this.entityName = (entityName != null)?entityName.trim():"";
    }

    @Override
    public String toString() {
        return "EmidaEntity{" + "entityId=" + entityId + ", entityType=" + entityType + ", entityName=" + entityName + '}';
    }    
}
