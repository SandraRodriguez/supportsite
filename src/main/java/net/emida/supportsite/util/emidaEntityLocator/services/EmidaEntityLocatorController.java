/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.util.emidaEntityLocator.services;

import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.util.emidaEntityLocator.dto.EmidaEntity;
import net.emida.supportsite.util.emidaEntityLocator.repository.IEmidaEntityLocatorRepository;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author fernandob
 */
@Controller
@RequestMapping(value = UrlPaths.UTIL_PATH)
public class EmidaEntityLocatorController extends BaseController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(EmidaEntityLocatorController.class);

    @Autowired
    private IEmidaEntityLocatorRepository entityLocatorDao;

    @RequestMapping(value = UrlPaths.EMIDA_ENTITY_LOCATOR + "/getIsos", method = RequestMethod.GET)
    @ResponseBody
    public List<EmidaEntity> getIsos() {
        List<EmidaEntity> retValue = new ArrayList<EmidaEntity>();
        try {
            retValue = this.entityLocatorDao.getIsos();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.EMIDA_ENTITY_LOCATOR + "/getMerchantsByDba", method = RequestMethod.GET)
    @ResponseBody
    public List<EmidaEntity> getMerchantsByDba(@RequestParam("dba") String dba) {
        List<EmidaEntity> retValue = new ArrayList<EmidaEntity>();
        try {
            retValue = this.entityLocatorDao.getMerchantsByDba(dba);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.EMIDA_ENTITY_LOCATOR + "/getMerchantsByRepId", method = RequestMethod.GET)
    @ResponseBody
    public List<EmidaEntity> getMerchantsByRepId(@RequestParam("repId") long repId) {
        List<EmidaEntity> retValue = new ArrayList<EmidaEntity>();
        try {
            retValue = this.entityLocatorDao.getMerchantsByRepId(repId);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.EMIDA_ENTITY_LOCATOR + "/getRepsByBusinessName", method = RequestMethod.GET)
    @ResponseBody
    public List<EmidaEntity> getRepsByBusinessName(@RequestParam("businessName") String businessName) {
        List<EmidaEntity> retValue = new ArrayList<EmidaEntity>();
        try {
            retValue = this.entityLocatorDao.getRepsByBusinessName(businessName);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
        }
        return retValue;
    }

    @RequestMapping(value = UrlPaths.EMIDA_ENTITY_LOCATOR + "/getRepsChildsByParentId", method = RequestMethod.GET)
    @ResponseBody
    public List<EmidaEntity> getRepsChildsByParentId(@RequestParam("parentId") long parentId) {
        List<EmidaEntity> retValue = new ArrayList<EmidaEntity>();
        try {
            retValue = this.entityLocatorDao.getRepsChildsByParentId(parentId);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
        }
        return retValue;
    }

    @Override
    public int getSection() {
        return 18;

    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
