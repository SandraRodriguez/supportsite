/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.util.emidaEntityLocator.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.emidaEntityLocator.dto.EmidaEntity;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fernandob
 */
@Repository("IEmidaEntityLocatorRepository")
public class EmidaEntityLocatorRepository implements IEmidaEntityLocatorRepository {

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(EmidaEntityLocatorRepository.class);

    private static final int MERCHANT_ENTITY_TYPE = 8;

    private class RepRowMapper implements RowMapper<EmidaEntity> {

        @Override
        public EmidaEntity mapRow(ResultSet rs, int i) throws SQLException {
            return new EmidaEntity(
                    rs.getLong("rep_id"),
                    rs.getInt("type"),
                    rs.getString("businessname")
            );
        }

    }

    private class MerchantRowMapper implements RowMapper<EmidaEntity> {

        @Override
        public EmidaEntity mapRow(ResultSet rs, int i) throws SQLException {
            return new EmidaEntity(
                    rs.getLong("merchant_id"),
                    MERCHANT_ENTITY_TYPE,
                    rs.getString("dba")
            );
        }

    }

    @Override
    public List<EmidaEntity> getIsos() {
        final String query = "SELECT [rep_id], [type], [businessname] FROM [dbo].[reps] WITH(NOLOCK) WHERE [rep_id] = [iso_id] ORDER BY [businessname] ASC";
        LOG.info("query :: " + query);
        List<EmidaEntity> dtoList = new ArrayList<EmidaEntity>();
        try {
            dtoList = jdbcTemplate.query(query, new RepRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getIsos . Ex = {}", e);
        }
        return dtoList;
    }

    @Override
    public List<EmidaEntity> getRepsChildsByParentId(long parentId) {
        final String query = String.format("SELECT [rep_id], [type], [businessname] FROM [dbo].[reps] WITH(NOLOCK) WHERE [rep_id] <> [iso_id] AND [iso_id] = %d ORDER BY [businessname] ASC",
                parentId);
        LOG.info("query :: " + query);
        List<EmidaEntity> dtoList = new ArrayList<EmidaEntity>();
        try {
            dtoList = jdbcTemplate.query(query, new RepRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getRepsChildsByParentId . Ex = {}", e);
        }
        return dtoList;
    }

    @Override
    public List<EmidaEntity> getRepsByBusinessName(String businessName) {
        final String query = String.format("SELECT [rep_id], [type], [businessname] FROM [dbo].[reps] WITH(NOLOCK) WHERE [businessname] like '%%%s%%' ORDER BY [businessname] ASC",
                businessName);
        LOG.info("query :: " + query);
        List<EmidaEntity> dtoList = new ArrayList<EmidaEntity>();
        try {
            dtoList = jdbcTemplate.query(query, new RepRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getRepsByBusinessName . Ex = {}", e);
        }
        return dtoList;
    }

    @Override
    public List<EmidaEntity> getMerchantsByRepId(long repId) {
        final String query = String.format("SELECT [merchant_id], [dba] FROM [dbo].[merchants] WITH(NOLOCK) WHERE [rep_id] = %d ORDER BY [dba] ASC",
                repId);
        LOG.info("query :: " + query);
        List<EmidaEntity> dtoList = new ArrayList<EmidaEntity>();
        try {
            dtoList = jdbcTemplate.query(query, new MerchantRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getMerchantsByRepId . Ex = {}", e);
        }
        return dtoList;
    }

    @Override
    public List<EmidaEntity> getMerchantsByDba(String dba) {
        final String query = String.format("SELECT [merchant_id], [dba] FROM [dbo].[merchants] WITH(NOLOCK) WHERE [dba] like  %%%s%% ORDER BY [dba] ASC",
                dba);
        LOG.info("query :: " + query);
        List<EmidaEntity> dtoList = new ArrayList<EmidaEntity>();
        try {
            dtoList = jdbcTemplate.query(query, new MerchantRowMapper());
        } catch (DataAccessException e) {
            LOG.error("Error on getMerchantsByDba . Ex = {}", e);
        }
        return dtoList;
    }

}
