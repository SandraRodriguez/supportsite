/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.util;

/**
 *
 * @author emida
 */
public interface ValidationParametersResponseData {
    
    /**
     * For regex
     */
    int INTEGER_WITH_MIN_AND_MAX_LENGTH_TYPE                                    = 1;
    int STRING_WITH_MIN_AND_MAX_LENGTH_TYPE                                     = 2;
    int ALPHA_NUMERIC_TYPE                                                      = 3;
    int INTEGER_ONLY                                                            = 4;
    int LETTER_OR_NUMBER                                                        = 5;
    int WORD_WITH_OR_WITHOUT_SPACES_NO_SIZE                                     = 6;
    int INTEGER_WITH_SIZE_DIFFERENT_TO_ZERO                                     = 7;
    int DATE_FORMAT_YYYY_MM_DD                                                  = 8;
    
    /**
     * Status
     */
    int SUCCESS_PARAMETER                                                       = 1;
    int INVALID_PARAMETER                                                       = 2;
    
    /**
     * Attribute's name
     */
    String TERMINAL_TYPE_ID_TAG                                                 = "TERMINAL_TYPE_ID";
    String STATUS_TAG                                                           = "STATUS";
    String INITIAL_PAGE_TAG                                                     = "INITIAL_PAGE";
    String FINAL_PAGE_TAG                                                       = "FINAL_PAGE";
    String PAGE_TAG                                                             = "PAGE";
    String INITIAL_DATE_TAG                                                     = "INITIAL_DATE";
    String FINAL_DATE_TAG                                                       = "FINAL_DATE";
    
    /**
     * Error Codes
     */
    String TAG_ERROR_CODE                                                       = "EC_INVALID_";
    String EC_INVALID_TERMINAL_TYPE_ID                                          = "112";
    String EC_INVALID_STATUS                                                    = "113";
    String EC_INVALID_INITIAL_PAGE                                              = "114";
    String EC_INVALID_FINAL_PAGE                                                = "115";
    String EC_INVALID_PAGE                                                      = "116";
    String EC_INVALID_INITIAL_DATE                                              = "117";
    String EC_INVALID_FINAL_DATE                                                = "118";
    
    /**
     * Error messages
     */
    String TAG_ERROR_MESSAGE                                                    = "EM_INVALID_";
    String EM_INVALID_TERMINAL_TYPE_ID                                          = "Please check terminal type";
    String EM_INVALID_STATUS                                                    = "Please check status";
    String EM_INVALID_INITIAL_PAGE                                              = "Please check initial page";
    String EM_INVALID_FINAL_PAGE                                                = "Please check final page";
    String EM_INVALID_PAGE                                                      = "Please check page";
    String EM_INVALID_INITIAL_DATE                                              = "Please check initial date";
    String EM_INVALID_FINAL_DATE                                                = "Please check final date";
}
