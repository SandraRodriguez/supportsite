package net.emida.supportsite.util;

/**
 * @author Franky Villadiego
 */
public class EmidaLanguage {

    private Integer id;
    private String englishLabel;
    private String spanishLabel;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnglishLabel() {
        return englishLabel;
    }

    public void setEnglishLabel(String englishLabel) {
        this.englishLabel = englishLabel;
    }

    public String getSpanishLabel() {
        return spanishLabel;
    }

    public void setSpanishLabel(String spanishLabel) {
        this.spanishLabel = spanishLabel;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmidaLanguage that = (EmidaLanguage) o;

        return !(id != null ? !id.equals(that.id) : that.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
