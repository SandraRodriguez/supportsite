package net.emida.supportsite.util;

import com.debisys.languages.Languages;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * @author Franky Villadiego
 */
public class ELUtil {

    public static String encodeUrl(String value){
        String encodedUrl = "";
        try {
            encodedUrl= URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodedUrl;
    }

    public static Object get(List list, int index){
        return list.get(index);
    }

    public static Object get(Map map, String key){
        return map.get(key);
    }

    public static int size(List list){
        return list.size();
    }

    public static String languageString(LanguageBridge lb, String key){
        return lb.getString(key);
    }

    public static String trimLower(String text){
        return text.trim().toLowerCase();
    }
}
