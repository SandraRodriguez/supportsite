package net.emida.supportsite.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.*;

/**
 * @author Franky Villadiego
 */
public class SupportSiteMessageSource extends AbstractMessageSource {

    private static final Logger log = LoggerFactory.getLogger(SupportSiteMessageSource.class);

    private Map<String, Map<Locale, String>> messages = new HashMap<String, Map<Locale, String>>();
    private ResourceLoader resourceLoader = new DefaultResourceLoader();

    @PostConstruct
    private void init(){
        log.debug("Loading SupportSiteMessageSource");
        loadMessages();
        log.debug("Messages loaded {}", messages.size());
    }


    private void loadMessages(){
        fillMessages(messages, "classpath:com/debisys/languages/english.properties", "en");
        fillMessages(messages, "classpath:com/debisys/languages/spanish.properties", "es");
    }

    private void fillMessages(Map<String, Map<Locale, String>> messages, String resourceToLoad, String localeLanguage){
        log.debug("Filling messages from {}", resourceToLoad);
        try {

            Resource propertiesResource = resourceLoader.getResource(resourceToLoad);
            InputStream inputStream = propertiesResource.getInputStream();
            Properties properties = new Properties();
            properties.load(inputStream);

            Locale locale = new Locale(localeLanguage);
            for (Object key : properties.keySet()) {
                Map<Locale, String> currentMap = messages.get(key.toString());
                if(currentMap == null){
                    currentMap = new HashMap<Locale, String>();
                    messages.put(key.toString(), currentMap);
                }
                String value = properties.getProperty(key.toString());
                currentMap.put(locale, value);

            }

        }catch (Exception ex){

        }

    }

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        log.debug("Resolving code {} with locale {}", code, locale);
        String msg  = getSupportSiteMessage(code, locale);
        log.debug("Value of message ={}", msg);
        return createMessageFormat(msg, locale);
    }

    private String getSupportSiteMessage(String code, Locale locale) {

        Map<Locale, String> map = messages.get(code);
        log.debug("messageMap={}", map);
        //log.debug("Messages size ={}", map.size());
        boolean useCodeAsDefaultMessage = isUseCodeAsDefaultMessage();
        if(map == null) return code;
        String msg = map.get(locale);
        return msg;
    }

    @Override
    public MessageSource getParentMessageSource() {
        log.debug("getParentMessageSource");
        return super.getParentMessageSource();
    }

    @Override
    protected Properties getCommonMessages() {
        log.debug("getCommonMessages");
        return super.getCommonMessages();
    }

    @Override
    protected String getMessageInternal(String code, Object[] args, Locale locale) {
        log.debug("getMessageInternal. {}. {}. {}", code, args, locale);
        return super.getMessageInternal(code, args, locale);
    }

    @Override
    protected String getMessageFromParent(String code, Object[] args, Locale locale) {
        log.debug("getMessageFromParent. {}. {}. {}", code, args, locale);
        return super.getMessageFromParent(code, args, locale);
    }

    @Override
    protected String getDefaultMessage(String code) {
        log.debug("getDefaultMessage. {}", code);
        return super.getDefaultMessage(code);
    }
}
