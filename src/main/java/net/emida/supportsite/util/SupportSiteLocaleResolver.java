package net.emida.supportsite.util;

import com.debisys.users.SessionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;

/**
 * @author Franky Villadiego
 */
public class SupportSiteLocaleResolver implements LocaleResolver{

    private static final Logger log = LoggerFactory.getLogger(SupportSiteLocaleResolver.class);

    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {
        log.debug("Resolving Locale");
        HttpSession session = httpServletRequest.getSession();
        SessionData sessionData = (SessionData) session.getAttribute("SessionData");
        String language = sessionData.getLanguage();
        log.debug("language from sessionData = {}", language);
        if("english".equals(language)){
            return new Locale("en");
        }else if("spanish".equals(language)){
            return new Locale("es");
        }
        return new Locale("en");
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {
        throw new UnsupportedOperationException("Cannot change locale form here");
    }
}
