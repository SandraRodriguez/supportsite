package net.emida.supportsite.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;

/**
 * @author Franky Villadiego
 */
public class RSUtil {

    private static final Logger log = LoggerFactory.getLogger(RSUtil.class);

    private RSUtil() {
    }

    public static String toString(BigDecimal bd) {
        Long l = (Long) bd.longValue();
        return l.toString();
    }

    public static Long getLong(ResultSet rs, String colName) {
        try {
            return rs.getLong(colName);
        } catch (Exception ex) {
            log.error("ERROR getLong({}):{}", colName, ex.getMessage());
        }
        return null;
    }


    public static String getString(ResultSet rs, String colName) {
        try {
            return rs.getString(colName);
        } catch (Exception ex) {
            log.error("ERROR getString({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static String getString(ResultSet rs, String colName, boolean validateColExist) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            boolean hasColumn = hasColumn(metaData, colName);
            return hasColumn ? getString(rs, colName) : null;
        } catch (Exception ex) {
            log.error("ERROR getString2({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static short getShort(ResultSet rs, String colName) {
        try {
            return rs.getShort(colName);
        } catch (Exception ex) {
            log.error("ERROR getShort({}):{}", colName, ex.getMessage());
        }
        return Short.parseShort(null);
    }

    public static Integer getInteger(ResultSet rs, String colName) {
        try {
            return rs.getInt(colName);
        } catch (Exception ex) {
            log.error("ERROR getInteger({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static Double getDouble(ResultSet rs, String colName) {
        try {
            return rs.getDouble(colName);
        } catch (Exception ex) {
            log.error("ERROR getDouble({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static Boolean getBoolean(ResultSet rs, String colName) {
        try {
            return rs.getBoolean(colName);
        } catch (Exception ex) {
            log.error("ERROR getBoolean({}):{}", colName, ex.getMessage());
        }
        return null;
    }


    public static BigDecimal getBigDecimal(ResultSet rs, String colName) {
        try {
            return rs.getBigDecimal(colName);
        } catch (Exception ex) {
            log.error("ERROR getBiDecimal({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static BigDecimal getBigDecimal(ResultSet rs, String colName, boolean validateColExist) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            boolean hasColumn = hasColumn(metaData, colName);
            return hasColumn ? getBigDecimal(rs, colName) : null;
        } catch (Exception ex) {
            log.error("ERROR getBiDecimal2({}):{}", colName, ex.getMessage());
        }
        return null;
    }


    public static Timestamp getTimestamp(ResultSet rs, String colName) {
        try {
            return rs.getTimestamp(colName);
        } catch (Exception ex) {
            log.error("ERROR getTimestamp({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static Timestamp getTimestamp(ResultSet rs, String colName, boolean validateColExist) {
        try {
            ResultSetMetaData metaData = rs.getMetaData();
            boolean hasColumn = hasColumn(metaData, colName);
            return hasColumn ? getTimestamp(rs, colName) : null;
        } catch (Exception ex) {
            log.error("ERROR getTimestamp2({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static String getTimestamp(ResultSet rs, String colName, String defaultValue) {
        try {
            if (rs.getTimestamp(colName)!=null){
                return rs.getTimestamp(colName).toString();
            } else{
                return defaultValue;
            }
        } catch (Exception ex) {
            log.error("ERROR getTimestamp3({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static Date getDate(ResultSet rs, String colName) {
        try {
            return rs.getDate(colName);
        } catch (Exception ex) {
            log.error("ERROR getDate({}):{}", colName, ex.getMessage());
        }
        return null;
    }

    public static double getDouble(ResultSet rs, String colName, double defaultValue){
        Double value = getDouble(rs, colName);
        return value != null ? value : defaultValue;
    }

    public static BigDecimal getBigDecimal(ResultSet rs, String colName, double defaultValue){
        BigDecimal value = getBigDecimal(rs, colName);
        return value != null ? value : new BigDecimal(defaultValue);
    }


    public static boolean hasColumn(ResultSetMetaData md, String colName){
        try {
            int columns = md.getColumnCount();
            for (int x = 1; x <= columns; x++) {
                if (colName.equalsIgnoreCase(md.getColumnName(x))) {
                    return true;
                }
            }
        }catch (Exception ex){
            log.error("ERROR Metadata({}):{}", colName, ex.getMessage());
        }
        return false;
    }
    
    public static boolean validateStr(String field) {
        return ((field != null) && (!field.isEmpty()));
    }
}
