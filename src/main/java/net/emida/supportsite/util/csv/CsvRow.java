package net.emida.supportsite.util.csv;

import java.util.List;

/**
 * @author Franky Villadiego
 */
public interface CsvRow {

    List<CsvCell> cells();
}
