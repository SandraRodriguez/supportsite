package net.emida.supportsite.util.csv;

import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
public interface CsvBuilderService {

    void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData);

}
