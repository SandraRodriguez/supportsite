package net.emida.supportsite.util.csv;

/**
 * @author Franky Villadiego
 */
public class CsvCell {


    private Object value;
    private Class valueType = String.class;

    public CsvCell(String value) {
        this.value = value;
    }

    public CsvCell(Object value, Class valueType) {
        if(!value.getClass().equals(valueType)){
            throw new IllegalArgumentException("The value is not equal to valueType");
        }
        this.value = value;
        this.valueType = valueType;
    }

    public String getValue(){
        return value != null ? value.toString() : "";
    }

}
