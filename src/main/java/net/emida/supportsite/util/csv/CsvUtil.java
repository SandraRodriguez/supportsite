package net.emida.supportsite.util.csv;

import java.util.List;

/**
 * @author Franky Villadiego
 */
public class CsvUtil {

    private CsvUtil(){}


    public static String generateRow(List<String> row, boolean addReturn){
        StringBuilder sb = new StringBuilder();

        for (String col : row) {
            sb.append("\"").append(col).append("\"").append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        if(addReturn) {
            sb.append("\n");
        }
        return sb.toString();
    }

    public static String stringifyRow(List<CsvCell> cells, boolean addReturn){
        StringBuilder sb = new StringBuilder();
        for (CsvCell cell : cells) {
            sb.append("\"").append(cell.getValue()).append("\"").append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        if(addReturn) {
            sb.append("\n");
        }
        return sb.toString();
    }

}
