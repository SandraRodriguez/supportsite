/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.util;

import java.lang.reflect.Field;
import static net.emida.supportsite.util.ValidationParametersResponseData.ALPHA_NUMERIC_TYPE;
import static net.emida.supportsite.util.ValidationParametersResponseData.INTEGER_ONLY;
import static net.emida.supportsite.util.ValidationParametersResponseData.INTEGER_WITH_MIN_AND_MAX_LENGTH_TYPE;
import static net.emida.supportsite.util.ValidationParametersResponseData.LETTER_OR_NUMBER;
import static net.emida.supportsite.util.ValidationParametersResponseData.STRING_WITH_MIN_AND_MAX_LENGTH_TYPE;
import static net.emida.supportsite.util.ValidationParametersResponseData.WORD_WITH_OR_WITHOUT_SPACES_NO_SIZE;
import org.apache.log4j.Logger;

/**
 *
 * @author emida
 */
public class ValidationParameters implements ValidationParametersResponseData {
    
    private ValidationParametersResponse providerResponse;
    private static final Logger _logger = Logger.getLogger(ValidationParameters.class);
    
    public ValidationParameters(ValidationParametersResponse providerResponse) {
        this.providerResponse = providerResponse;
    }

     /**
     * Validate field, if invalid, get its error code and error message from Constants class.
     * 
     * @param opc
     * @param name
     * @param parameter
     * @param minLength
     * @param maxLength 
     */
    public boolean checkField(int opc, String name, String parameter, int minLength, int maxLength) {
        
        if (!this.validateField(opc, parameter, minLength, maxLength)) {
            this.providerResponse = this.getDataError(name);
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Check parameter according data type.
     * 
     * @param opc
     * @param parameter
     * @param minLength
     * @param maxLength
     * @return 
     */
    private boolean validateField(int opc, String parameter, int minLength, int maxLength) {
        boolean response = false;
        
        if (parameter != null && !parameter.isEmpty() && !parameter.equals("")) {
            switch (opc) {
                case INTEGER_WITH_MIN_AND_MAX_LENGTH_TYPE:
                    response = ((parameter.matches("[-+]?\\d*\\.?\\d+")) && ((parameter.length() >= minLength) && parameter.length() <= maxLength)); 
                break;
                case STRING_WITH_MIN_AND_MAX_LENGTH_TYPE:
                    response = ((parameter.matches("[a-zA-Z]+")) && ((parameter.length() >= minLength) && parameter.length() <= maxLength)); 
                break;
                case ALPHA_NUMERIC_TYPE:
                    response = ((parameter.matches("[A-Za-z0-9]+")) && ((parameter.length() >= minLength) && parameter.length() <= maxLength)); 
                break;
                case INTEGER_ONLY:
                    response = ((parameter.matches("[-+]?\\d*\\.?\\d+"))); 
                break;
                case LETTER_OR_NUMBER:
                    response = (parameter.length() >= minLength) && (parameter.length() <= maxLength); 
                break;
                case WORD_WITH_OR_WITHOUT_SPACES_NO_SIZE:
                    response = parameter.matches("^[\\p{L}\\s'.-]+$"); 
                break;
                case INTEGER_WITH_SIZE_DIFFERENT_TO_ZERO:
                    response = ((parameter.matches("[-+]?\\d*\\.?\\d+")) && ((parameter.length() >= minLength) && parameter.length() <= maxLength) && !parameter.equals("0")); 
                break;
                case DATE_FORMAT_YYYY_MM_DD:
                    response = parameter.matches("^([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$");
                break;
            }           
        }
        return response;
    }
    
    /**
     * Check variable's name from Constants class to match with the parameter, to get it's error code and result message.
     * 
     * @param name      parameter name        
     * @return 
     */
    private ValidationParametersResponse getDataError(String name) {
        
        try {
            String tagErrorCode = TAG_ERROR_CODE + name, tagErrorMessage = TAG_ERROR_MESSAGE + name, errorCode = null, errorMessage = null; 
            Class c = Class.forName(ValidationParametersResponseData.class.getName());
            Field[] fields = c.getFields();

            for (Field f : fields) {
                f.setAccessible(true);
                if (f.getName() != null && !f.getName().isEmpty()) {
                    if (f.getName().equals(tagErrorCode)) {
                        errorCode = (String) f.get(null);
                    } else if(f.getName().equals(tagErrorMessage)) {
                        errorMessage = (String) f.get(null);
                    }
                }
            }
            
            if (errorCode != null) this.providerResponse.setResultCode(errorCode);
            if (errorMessage != null) this.providerResponse.setResultMessage(errorMessage);
            
        } catch (Exception e) {
            _logger.info("ValidationParameters.getDataError - ERROR GETTING DATA FROM CONSTANTS INTERFACE : " + e.toString());
        } finally {
            if (this.providerResponse.getResultCode() != null && !this.providerResponse.getResultCode().isEmpty()) {
                return this.providerResponse;
            } else {
                return null;
            }
        }   
    } 
}
