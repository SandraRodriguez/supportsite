package net.emida.supportsite.util;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;

/**
 * @author Franky Villadiego
 */
public class LanguageBridge {

    private SessionData sessionData;

    public LanguageBridge(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    public String getString(String key){
        return Languages.getString(key, sessionData.getLanguage());
    }

}
