package net.emida.supportsite.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Franky Villadiego
 */

@Component
public class SpringBeanFactory implements ApplicationContextAware{


    private static ApplicationContext applicationContext;


    public static Object getBean(String s){
        return applicationContext.getBean(s);
    }

    public static <T> T getBean(Class<T> clazz){
        return applicationContext.getBean(clazz);
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
