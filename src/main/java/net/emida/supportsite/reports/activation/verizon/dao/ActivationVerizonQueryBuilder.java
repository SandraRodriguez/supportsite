package net.emida.supportsite.reports.activation.verizon.dao;

import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActivationVerizonQueryBuilder {

    Map<String, Object> queryData;
    private StringBuilder sb;

    private static final boolean QUERY_IS_COUNT = true;
    private static final boolean QUERY_NOT_IS_COUNT = false;

    private static final Logger log = LoggerFactory.getLogger(ActivationVerizonQueryBuilder.class);

    public ActivationVerizonQueryBuilder(Map<String, Object> queryData) {
        this.queryData = queryData;
        sb = new StringBuilder();
    }

    public String buildCountQuery() throws Exception {

        queryForTransactionsActiviationVerizon(QUERY_IS_COUNT);
        return sb.toString();
    }

    public String buildMainQuery() throws Exception {
        queryForTransactionsActiviationVerizon(QUERY_NOT_IS_COUNT);
        return sb.toString();
    }

    private void queryForTransactionsActiviationVerizon(boolean isCountQuery) throws Exception {

        if (isCountQuery) {
            sb.append("SELECT COUNT(*) \n");
        }
        else {
            sb.append("SELECT  \n");
            queryHeadFields();
        }

        sb.append("FROM ");
        queryBodyjoins();
        sb.append(buildMerchantIDsWhere());
        sb.append(buildDatesRangeWhere("WB.datetime"));
        sb.append(buildSatusFilter());

        log.debug(sb.toString());

    }

    private Object buildSatusFilter() {
        StringBuilder sb1 = new StringBuilder();
        String statusCodes = (String) this.queryData.get("selectStatus");
        String[] statusCodesArray;
        String inCodesQuery = "";

        statusCodesArray = statusCodes.split(",");

        for (int i = 0; i < statusCodesArray.length; i++) {
            if ((i + 1) == (statusCodesArray.length)) {
                inCodesQuery += "'" + statusCodesArray[i] + "'";
            }
            else {
                inCodesQuery += "'" + statusCodesArray[i] + "',";
            }
        }
        sb1.append("AND ARS.statusCode IN(").append(inCodesQuery).append(")");
        return sb1;
    }

    private void queryBodyjoins() {

        sb.append("	dbo.activationRequest AC  WITH(NOLOCK) \n")
                .append("	JOIN dbo.Web_Transactions WB  WITH(NOLOCK) ON WB.rec_id= AC.transactionId \n")
                .append("	JOIN dbo.States S   WITH(NOLOCK) ON ac.stateIdNew = S.idNew \n")
                .append("	JOIN dbo.merchants M WITH(NOLOCK) ON WB.merchant_id = M.merchant_id \n")
                .append("	JOIN dbo.products P WITH(NOLOCK) ON WB.RecordedProductId = P.id \n")
                .append("	JOIN dbo.activationRequestStatus ARS WITH(NOLOCK) ON ARS.statusId = AC.requestStatus \n");

    }

    private void queryHeadFields() {

        sb.append("		AC.transactionId transactionId, \n")
                .append("		WB.millennium_no terminalNumber, \n")
                .append("		M.dba DBA, \n")
                .append("		WB.datetime transactionDate, \n")
                .append("		M.merchant_id Merchant_id, \n")
                .append("		M.phys_city MerchatCity, \n")
                .append("		M.phys_state MerchantState, \n")
                .append("		M.email MerchantEmail, \n")
                .append("		M.contact_phone MerchantPhone, \n")
                .append("		P.id ProductId, \n")
                .append("		P.description ProductDescription, \n")
                .append("		AC.linesRequested numberLines, \n")
                .append("		AC.subscriberFirstName SubscriberFirstName, \n")
                .append("		AC.subscriberLastName SubscriberLastName, \n")
                .append("		AC.streetNumber StreetNumber, \n")
                .append("		AC.streetName StreetName, \n")
                .append("		AC.city City, \n")
                .append("		s.StateName state, \n")
                .append("		AC.zipCode ZipCode, \n")
                .append("		AC.preferredLanguage Language, \n")
                .append("		AC.desiredAreaCode DesiredAreaCode, \n")
                .append("		AC.contactPhone ContactPhoneNumber, \n")
                .append("		AC.accountPinNumber SubscriberPIN,  \n")
                .append("		AC.sim SIM,  \n")
                .append("		AC.esnImei EsnImei, \n")
                .append("		ARS.description RequestStatus, \n")
                .append("		AC.phoneNumber activationPhoneNumber \n");

    }

    private String buildMerchantIDsWhere() {
        StringBuilder sb1 = new StringBuilder();
        String merchants = (String) queryData.get("merchants");

        sb1.append(" WHERE M.merchant_id IN(\n");
        sb1.append(" SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE merchant_id IN (").append(merchants).append(")\n");
        sb1.append(")\n");

        return sb1.toString();
    }

    /**
     *
     * @param fieldName
     * @param startDate With format dd/mm/yyyy
     * @param endDate With format dd/mm/yyyy
     * @return
     * @throws Exception
     */
    private String buildDatesRangeWhere(String fieldName) throws Exception {

        String accessLevel = (String) queryData.get("strAccessLevel");
        StringBuilder sb = new StringBuilder();
        String startDate = (String) queryData.get("starDate");
        String endDate = (String) queryData.get("endDate");
        String strRefId = (String) queryData.get("strRefId");

        Vector vTimeZoneFilterDates = com.debisys.utils.TimeZone.getTimeZoneFilterDates(accessLevel, strRefId, startDate, endDate + " 23:59:59.999", false);
        sb.append(" AND (").append(fieldName).append(">='").append(vTimeZoneFilterDates.get(0)).append("' AND ").append(fieldName).append("<='").append(vTimeZoneFilterDates.get(1)).append("') \n");
        return sb.toString();
    }

    public String buildSqlQueryNerchabtByRep(String strRefId) {
        StringBuilder sb1 = new StringBuilder();
        sb1.append(" SELECT m.dba merchantDba,m.merchant_id merchantId  FROM merchants m WITH(NOLOCK) WHERE rep_id IN (").append(strRefId).append(")");

        return sb1.toString();
    }

}
