package net.emida.supportsite.reports.activation.verizon.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.debisys.utils.NumberUtil;

import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

public class ActivationVerizonReportModel implements CsvRow{
	
	private long rowNum;
	private long transactionId;
	private long terminalNumber;
	private String dba;
	private Date transactionDate;
	private BigDecimal merchantId;
	private String merchatCity;
	private String merchantState;
	private String merchantEmail;
	private String merchantPhone;
	private long productId;
	private String productDescription;
	private long numersLines;
	private String subcriberFirstName;
	private String subcriberLastName;
	private String streetNumber;
	private String streetName;
	private String city;
	private String stateName;
	private String zipCode;
	private String languange;
	private String desiredAreaCode;
	private String contactPhoneNumber;
	private String subscriberPIN;
	private String phoneNumber;
	private String sim;
	private String esnImei;
	private String requestStatusDescription;
	private String activationPhoneNumber;


	private ActivationRequestVerizonStatus requestStatus;
	

	
	
	
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public long getTerminalNumber() {
		return terminalNumber;
	}
	public void setTerminalNumber(long terminalNumber) {
		this.terminalNumber = terminalNumber;
	}
	public BigDecimal getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(BigDecimal merchantId) {
		this.merchantId = merchantId;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getMerchatCity() {
		return merchatCity;
	}
	public void setMerchatCity(String merchatCity) {
		this.merchatCity = merchatCity;
	}
	public String getMerchantState() {
		return merchantState;
	}
	public void setMerchantState(String merchantState) {
		this.merchantState = merchantState;
	}
	public String getMerchantEmail() {
		return merchantEmail;
	}
	public void setMerchantEmail(String merchantEmail) {
		this.merchantEmail = merchantEmail;
	}
	public String getMerchantPhone() {
		return merchantPhone;
	}
	public void setMerchantPhone(String merchantPhone) {
		this.merchantPhone = merchantPhone;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public long getNumersLines() {
		return numersLines;
	}
	public void setNumersLines(long numersLines) {
		this.numersLines = numersLines;
	}
	public String getSubcriberFirstName() {
		return subcriberFirstName;
	}
	public void setSubcriberFirstName(String subcriberFirstName) {
		this.subcriberFirstName = subcriberFirstName;
	}
	public String getSubcriberLastName() {
		return subcriberLastName;
	}
	public void setSubcriberLastName(String subcriberLastName) {
		this.subcriberLastName = subcriberLastName;
	}
	public String getStreetNumber() {
		return streetNumber;
	}
	public String getRequestStatusDescription() {
		return requestStatusDescription;
	}
	public void setRequestStatusDescription(String requestStatusDescription) {
		this.requestStatusDescription = requestStatusDescription;
	}
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getLanguange() {
		return languange;
	}
	public void setLanguange(String languange) {
		this.languange = languange;
	}
	public String getDesiredAreaCode() {
		return desiredAreaCode;
	}
	public void setDesiredAreaCode(String desiredAreaCode) {
		this.desiredAreaCode = desiredAreaCode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getSim() {
		return sim;
	}
	public void setSim(String sim) {
		this.sim = sim;
	}
	
	public ActivationRequestVerizonStatus getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(ActivationRequestVerizonStatus requestStatus) {
		this.requestStatus = requestStatus;
	}
	
	
	@Override
	public List<CsvCell> cells() {
		  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        List<CsvCell> list = new ArrayList<CsvCell>();
	        
	          
            
   
	        CsvCell cell1 = new CsvCell(Long.toString(this.transactionId));
	        list.add(cell1);
	        CsvCell cell2 = new CsvCell(Long.toString(this.terminalNumber));
	        list.add(cell2);
	        CsvCell cell3 = new CsvCell(this.dba.toString());
	        list.add(cell3);
	        CsvCell cell4 = new CsvCell(this.merchantId.toString());
	        list.add(cell4);
	        CsvCell cell5 = new CsvCell(transactionDate !=null ?sdf.format(transactionDate) : "");
	        list.add(cell5);
	        
	        CsvCell cell6 = new CsvCell(this.merchatCity);
	        list.add(cell6);
	        CsvCell cell7 = new CsvCell(this.merchantState);
	        list.add(cell7);
	        CsvCell cell8 = new CsvCell(this.merchantEmail);
	        list.add(cell8);
	        CsvCell cell9 = new CsvCell(this.merchantPhone);
	        list.add(cell9);
	        CsvCell cell10 = new CsvCell(Long.toString(this.productId));
	        list.add(cell10);
	
	        CsvCell cell11 = new CsvCell(this.productDescription);
	        list.add(cell11);
	        CsvCell cell12 = new CsvCell(Long.toString(this.numersLines));
	        list.add(cell12);
	        CsvCell cell13 = new CsvCell(this.subcriberFirstName);
	        list.add(cell13);
	        CsvCell cell14 = new CsvCell(this.subcriberLastName);
	        list.add(cell14);
	        CsvCell cell15 = new CsvCell(this.streetNumber);
	        list.add(cell15);
	        CsvCell cell16 = new CsvCell(this.streetName);
	        list.add(cell16);
	        CsvCell cell17 = new CsvCell(this.city);
	        list.add(cell17);
	        CsvCell cell18 = new CsvCell(this.stateName);
	        list.add(cell18);
	        CsvCell cell19 = new CsvCell(this.zipCode);
	        list.add(cell19);
	        CsvCell cell20 = new CsvCell(this.languange);
	        list.add(cell20);
	  
	        CsvCell cell21 = new CsvCell(this.desiredAreaCode);
	        list.add(cell21);
	        CsvCell cell22 = new CsvCell(this.contactPhoneNumber);
	        list.add(cell22);
	        CsvCell cell23 = new CsvCell(this.getSubscriberPIN());
	        list.add(cell23);
	        CsvCell cell24 = new CsvCell(this.sim);
	        list.add(cell24);
	        CsvCell cell25 = new CsvCell(this.esnImei);
	        list.add(cell25);
	        CsvCell cell26 = new CsvCell(this.requestStatusDescription);
	        list.add(cell26);
	        CsvCell cell27 = new CsvCell(this.activationPhoneNumber);
	        list.add(cell27);
	        
	        
	        return list;
	}
	
	public String getContactPhoneNumber() {
		return contactPhoneNumber;
	}
	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}
	public String getSubscriberPIN() {
		return subscriberPIN;
	}
	public void setSubscriberPIN(String subscriberPIN) {
		this.subscriberPIN = subscriberPIN;
	}
	public String getEsnImei() {
		return esnImei;
	}
	public void setEsnImei(String esnImei) {
		this.esnImei = esnImei;
	}
	public String getActivationPhoneNumber() {
		return activationPhoneNumber;
	}
	public void setActivationPhoneNumber(String activationPhoneNumber) {
		this.activationPhoneNumber = activationPhoneNumber;
	}
	public String getDba() {
		return dba;
	}
	public void setDba(String dba) {
		this.dba = dba;
	}
	public long getRowNum() {
		return rowNum;
	}
	public void setRowNum(long rowNum) {
		this.rowNum = rowNum;
	}

	
	
}
