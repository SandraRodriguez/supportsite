package net.emida.supportsite.reports.activation.verizon.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import net.emida.supportsite.reports.activation.verizon.model.ActivationRequestVerizonStatus;
import net.emida.supportsite.reports.activation.verizon.model.ActivationVerizonReportModel;
import net.emida.supportsite.reports.spiff.deatil.dao.ActivationDetailReportModel;

public class ActivationDetailVerizonMapper implements RowMapper<ActivationVerizonReportModel>{

	@Override
	public ActivationVerizonReportModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		ActivationVerizonReportModel activationVerizonReportModel = new ActivationVerizonReportModel();
		
		
		activationVerizonReportModel.setRowNum(rowNum);
		activationVerizonReportModel.setTransactionId(rs.getLong("transactionId"));
		activationVerizonReportModel.setTerminalNumber(rs.getLong("terminalNumber"));
		activationVerizonReportModel.setDba(rs.getString("DBA"));
		activationVerizonReportModel.setTransactionDate(rs.getDate("transactionDate"));
		activationVerizonReportModel.setMerchatCity(rs.getString("MerchatCity"));
		activationVerizonReportModel.setMerchantState(rs.getString("MerchantState"));
		activationVerizonReportModel.setMerchantId(rs.getBigDecimal("Merchant_id"));
		activationVerizonReportModel.setStateName(rs.getString("MerchantState"));
		activationVerizonReportModel.setMerchantEmail(rs.getString("MerchantEmail"));
		activationVerizonReportModel.setMerchantPhone(rs.getString("MerchantPhone"));
		activationVerizonReportModel.setProductId(rs.getLong("ProductId"));
		activationVerizonReportModel.setProductDescription(rs.getString("ProductDescription"));
		activationVerizonReportModel.setNumersLines(rs.getLong("numberLines"));
		activationVerizonReportModel.setSubcriberFirstName(rs.getString("SubscriberFirstName"));
		activationVerizonReportModel.setSubcriberLastName(rs.getString("SubscriberLastName"));
		activationVerizonReportModel.setStreetNumber(rs.getString("StreetNumber"));
		activationVerizonReportModel.setStreetName(rs.getString("StreetName"));
		activationVerizonReportModel.setCity(rs.getString("City"));
		activationVerizonReportModel.setStateName(rs.getString("state"));
		activationVerizonReportModel.setZipCode(rs.getString("ZipCode"));
		activationVerizonReportModel.setLanguange(rs.getString("Language"));
		activationVerizonReportModel.setDesiredAreaCode(rs.getString("DesiredAreaCode"));
		activationVerizonReportModel.setContactPhoneNumber(rs.getString("ContactPhoneNumber"));
		activationVerizonReportModel.setSubscriberPIN(rs.getString("SubscriberPIN"));
		activationVerizonReportModel.setSim(rs.getString("SIM"));
		activationVerizonReportModel.setEsnImei(rs.getString("EsnImei"));
		String requesStatus = rs.getString("RequestStatus");
		activationVerizonReportModel.setRequestStatusDescription(requesStatus);
		if(requesStatus.equals("Pending")){
			activationVerizonReportModel.setRequestStatus(ActivationRequestVerizonStatus.PENDING);
		}
		if(requesStatus.equals("Completed")){
			activationVerizonReportModel.setRequestStatus(ActivationRequestVerizonStatus.COMPLETED);
		}
		if(requesStatus.equals("Cancelled")){
			activationVerizonReportModel.setRequestStatus(ActivationRequestVerizonStatus.CANCELLED);
		}
		
	    activationVerizonReportModel.setActivationPhoneNumber(rs.getString("activationPhoneNumber"));

	    return activationVerizonReportModel;
	}
}