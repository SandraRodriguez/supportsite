package net.emida.supportsite.reports.activation.verizon.detail;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.debisys.users.SessionData;
import java.util.Vector;
import javax.validation.Valid;

import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.activation.verizon.dao.ActivationVerizonDao;
import net.emida.supportsite.reports.activation.verizon.model.ActivationRequestVerizonStatus;
import net.emida.supportsite.reports.activation.verizon.model.ActivationVerizonDetailReportForm;
import net.emida.supportsite.reports.activation.verizon.model.ActivationVerizonReportModel;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.TechnicalException;

@Controller
@RequestMapping(value = UrlPaths.TOOLS_PATH)
public class ActivationVerizonDetailReportController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(ActivationVerizonDetailReportController.class);

    @Autowired
    ActivationVerizonDao activationVerizonDao;

    @Autowired
    @Qualifier("activationVerizonReportCsvBuilder")
    private CsvBuilderService csvBuilderService;

    @RequestMapping(value = UrlPaths.ACTIVATION_VERIZON_REPORT_FILTER, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute ActivationVerizonDetailReportForm reportModel) {

        fillFilters(model);

        return "reports/activationVerizon/activationVerizonFilter";
    }

    @RequestMapping(value = UrlPaths.ACTIVATION_VERIZON_REPORT_FILTER, method = RequestMethod.POST)
    public String getentities(Model model, @Valid ActivationVerizonDetailReportForm reportModel, BindingResult result) {

        log.debug("reportBuil {}", result);

        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());
        
        SessionData sessionData = getSessionData();
        //Date dateStart;
        String strDistChainType;
        String strAccessLevel;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String formattedStarDate = "";
        String formattedEndDate = "";
        try {

            formattedStarDate = sdf.format(reportModel.getStartDate());
            formattedEndDate = sdf.format(reportModel.getEndDate());
        } catch (Exception e) {
            model.addAttribute("reportModel", reportModel);
            fillFilters(model.asMap());
            return "reports/activationVerizon/activationVerizonFilter";
        }

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillFilters(model.asMap());
            return "reports/activationVerizon/activationVerizonFilter";
        }

        model.addAttribute("startDate", formattedStarDate);
        model.addAttribute("endDate", formattedEndDate);
        model.addAttribute("merchants", reportModel.getMerchants());
        model.addAttribute("selectStatus", reportModel.getSelectStatus());

        strDistChainType = sessionData.getUser().getDistChainType();
        strAccessLevel = sessionData.getUser().getAccessLevel();

        Map<String, Object> queryData = fillQueryData(formattedStarDate, formattedEndDate, strDistChainType, strAccessLevel, reportModel.getSelectStatus(), reportModel.getMerchants());

        Long totalRecords = activationVerizonDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);
        return "reports/spiff/activationVerizonDetailReportResult";
    }

    @RequestMapping(method = RequestMethod.POST, value = UrlPaths.ACTIVATION_VERIZON_DETAIL_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<ActivationVerizonReportModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String startDate, @RequestParam String endDate, @RequestParam String selectStatus, @RequestParam String merchants) {

        String strDistChainType;
        String strAccessLevel;

        SessionData sessionData;
        sessionData = getSessionData();
        int rowsPerPage = Integer.parseInt(rows);
        int rowNumberStartInt = Integer.parseInt(page);

        strDistChainType = sessionData.getUser().getDistChainType();
        strAccessLevel = sessionData.getUser().getAccessLevel();

        Map<String, Object> queryData = fillQueryData(startDate, endDate, strDistChainType, strAccessLevel, selectStatus, merchants);
        // start, rowsPerPage + start - 1, sortField, sortOrder, queryData
        queryData.put("rowNumberStartInt", rows);

        List<ActivationVerizonReportModel> list = activationVerizonDao.list(rowNumberStartInt, rowsPerPage, sortField, sortOrder, queryData);

        return list;
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.ACTIVATION_VERIZON_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, Model model, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam String selectStatus, @RequestParam String sortOrder, @RequestParam String sortField,
            @RequestParam String totalRecords, @RequestParam String merchants) throws IOException {

        String strDistChainType;
        String strAccessLevel;
        log.info("Downloading file...");
        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Listing grid. StartDate={}. EndDate={}. Sim={}. Pnumber={}. Mids={}. Pids={}", startDate, endDate);
        SessionData sessionData;
        sessionData = getSessionData();
        strDistChainType = sessionData.getUser().getDistChainType();
        strAccessLevel = sessionData.getUser().getAccessLevel();
        
        int rowsPerPage = Integer.parseInt(totalRecords);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, strDistChainType, strAccessLevel, selectStatus, merchants);
        List<ActivationVerizonReportModel> list = activationVerizonDao.list(1, rowsPerPage, sortField, sortOrder, queryData);
        log.debug("Total list={}", list.size());
        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.info("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());

        csvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.info("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();

        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

        return zipFileResource;

    }

    private Map<String, Object> fillQueryData(String formattedStarDate, String formattedEndDate, String strDistChainType, String strAccessLevel, String selectStatus, String merchants) {
        Map<String, Object> queryData = new HashMap<String, Object>();

        queryData.put("starDate", formattedStarDate);
        queryData.put("endDate", formattedEndDate);
        queryData.put("strDistChainType", strDistChainType);
        queryData.put("strAccessLevel", strAccessLevel);
        queryData.put("strRefId", getRefId());
        queryData.put("selectStatus", selectStatus);
        queryData.put("merchants", merchants);

        return queryData;
    }

    private void fillFilters(Map model) {
        try {
            model.put("strAccessLevel", getAccessLevel());
            model.put("strRefId", getRefId());
            model.put("strDistChainType", getDistChainType());

            Vector<Vector<String>> aControlStatus;
            aControlStatus = loadComboStatus();

            model.put("aControlStatus", aControlStatus);
        } catch (TechnicalException e) {
            log.error("Error setting merchant variables. Ex = {}", e);
        }
    }

    private Vector<Vector<String>> loadComboStatus() {
        Vector<Vector<String>> aControlStatus = new Vector<Vector<String>>();
        Vector<String> Satatus;

        Satatus = new Vector<String>();
        Satatus.add(ActivationRequestVerizonStatus.ALL.getSatusCode());
        Satatus.add(ActivationRequestVerizonStatus.ALL.getDecription());
        aControlStatus.add(Satatus);

        Satatus = new Vector<String>();
        Satatus.add(ActivationRequestVerizonStatus.PENDING.getSatusCode());
        Satatus.add(ActivationRequestVerizonStatus.PENDING.getDecription());
        aControlStatus.add(Satatus);

        Satatus = new Vector<String>();
        Satatus.add(ActivationRequestVerizonStatus.CANCELLED.getSatusCode());
        Satatus.add(ActivationRequestVerizonStatus.CANCELLED.getDecription());
        aControlStatus.add(Satatus);

        Satatus = new Vector<String>();
        Satatus.add(ActivationRequestVerizonStatus.COMPLETED.getSatusCode());
        Satatus.add(ActivationRequestVerizonStatus.COMPLETED.getDecription());
        aControlStatus.add(Satatus);

        return aControlStatus;
    }

    @Override
    public int getSectionPage() {
        return 14;
    }

    @Override
    public int getSection() {
        return 4;
    }

}
