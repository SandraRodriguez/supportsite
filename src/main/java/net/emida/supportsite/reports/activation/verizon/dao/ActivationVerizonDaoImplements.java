package net.emida.supportsite.reports.activation.verizon.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import net.emida.supportsite.reports.activation.verizon.model.ActivationVerizonReportModel;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailJdbcDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.emida.supportsite.util.pagination.Page;
import net.emida.supportsite.util.pagination.PaginationHelper;

@Repository
public class ActivationVerizonDaoImplements implements ActivationVerizonDao {

    private static final Logger log = LoggerFactory.getLogger(ActivationVerizonDaoImplements.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private static int PAGE_SIZE = 25;

    private String sqlCountActivationVerizonDetail;
    private String sqlQueryActivationVerizonDetail;

    @Override
    public Vector findMerchantBYRep(String merchant, String strRefId, String strDistChainType, String string,
            String string2, String string3, String string4, ArrayList<String> fieldToRead, Map<String, Object> queryData) {
        String sqlQueryMerchantsbyRep;
        Map<String, Object> mapResult;
        List<Map<String, Object>> listResult = new ArrayList<Map<String, Object>>();
        Vector<Vector<String>> vectorResult = new Vector<Vector<String>>();

        ActivationVerizonQueryBuilder activationVerizonQueryBuilder = new ActivationVerizonQueryBuilder(queryData);

        sqlQueryMerchantsbyRep = activationVerizonQueryBuilder.buildSqlQueryNerchabtByRep(strRefId);
        listResult = jdbcTemplate.queryForList(sqlQueryMerchantsbyRep);
        for (Iterator iterator = listResult.iterator(); iterator.hasNext();) {
            mapResult = (Map<String, Object>) iterator.next();
            Vector<String> data = new Vector<String>();

            for (Entry<String, Object> entry : mapResult.entrySet()) {
                if (entry.getValue() instanceof BigDecimal) {
                    data.add(((BigDecimal) entry.getValue()).toString());
                }
                else {
                    data.add((String) entry.getValue());
                }

            }
            vectorResult.add(data);
        }

        return vectorResult;
    }

    @Override
    public List<ActivationVerizonReportModel> list(Integer startPage, Integer endPage, String sortFieldName,
            String sortOrder, Map<String, Object> queryData) throws TechnicalException {

        try {

            ActivationVerizonQueryBuilder activationVerizonQueryBuilder = new ActivationVerizonQueryBuilder(queryData);
            sqlQueryActivationVerizonDetail = activationVerizonQueryBuilder.buildMainQuery();
//            String strRows =(String) queryData.get("rowNumberStartInt");
//            int rows = Integer.valueOf(strRows);

            log.debug("Query : {}", sqlQueryActivationVerizonDetail);

            //return jdbcTemplate.query(sqlQueryActivationVerizonDetail, new ActivationDetailVerizonMapper());
            return getActivationVerizonDetailReportModelPage(startPage, PAGE_SIZE, sortFieldName, sortOrder).getPageItems();
        } catch (DataAccessException ex) {
            log.error("Data access error in list spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in list spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        ActivationVerizonQueryBuilder activationVerizonQueryBuilder = new ActivationVerizonQueryBuilder(queryData);
        try {
            sqlCountActivationVerizonDetail = activationVerizonQueryBuilder.buildCountQuery();

            log.debug("Query Count : {}", sqlCountActivationVerizonDetail);

            return jdbcTemplate.queryForObject(sqlCountActivationVerizonDetail, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    public Page<ActivationVerizonReportModel> getActivationVerizonDetailReportModelPage(final int rowNumberStartInt, final int pageSize, String sortFieldName, String sortOrder) throws SQLException {
        PaginationHelper<ActivationVerizonReportModel> paginationHelper = new PaginationHelper<ActivationVerizonReportModel>();
        return paginationHelper.fetchPage(
                jdbcTemplate,
                sqlCountActivationVerizonDetail,
                sqlQueryActivationVerizonDetail,
                null,
                rowNumberStartInt,
                pageSize,
                new ActivationDetailVerizonMapper(),
                sortFieldName,
                sortOrder
        );

    }

    @Override
    public Long countAll() throws TechnicalException {
        //String merchantIDsWhere = buildMerchantIDsWhere();
        return null;
    }

    @Override
    public List<Map<String, Object>> listActivationVerizonMerchants() throws TechnicalException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Vector findMerchantBYRep(String merchant, String strRefId, String strDistChainType, String string,
            String string2, String string3, String string4, ArrayList<String> fieldToRead) {
        // TODO Auto-generated method stub
        return null;
    }

}
