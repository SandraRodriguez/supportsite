package net.emida.supportsite.reports.activation.verizon.detail;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.debisys.languages.Languages;
import com.debisys.utils.DebisysConstants;

import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;

@Service
	
public class ActivationVerizonReportCsvBuilder implements CsvBuilderService{
	
	private static final Logger log = LoggerFactory.getLogger(ActivationVerizonReportCsvBuilder.class);

	@Override
	public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
		  Writer writer = null;
	        try {

	            List<CsvRow> data = (List<CsvRow>) list;
	            //File to fill
	            writer = getFileWriter(filePath);

	            log.debug("Creating temp file : {}", filePath);
	            writer.flush(); //Create temp file

	            List<String> headers = generateHeaders(additionalData);
	            String headerRow = CsvUtil.generateRow(headers, true);
	            writer.write(headerRow);

	            fillWriter(data, writer, additionalData);

	        }catch (Exception ex){
	            log.error("Error building csv file : {}", filePath);
	            throw new TechnicalException(ex.getMessage(), ex);
	        }finally {
	            if(writer != null){
	                try {
	                    writer.flush();
	                    writer.close();
	                }catch (Exception ex){

	                }
	            }
	        }
		
	}
	
	
    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    
    
    private List<String> generateHeaders(Map<String, Object> additionalData){
        String language = (String) additionalData.get("language");
        String accessLevel = (String) additionalData.get("accessLevel");
        String distChainType = (String) additionalData.get("distChainType");
        List<String> headers = new ArrayList<String>();
        
        
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.transactionId", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.terminalNumber", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.DBA", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.merchantId", language));        		
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.transactionDate", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.merchantCity", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.merchantState", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.merchantEmail", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.merchantPhone", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.productId", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.productDescription", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.numberLines", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.subscriberFirstName", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.subscriberLastName", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.streetNumber", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.streetName", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.city", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.state", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.zipCode", language));       
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.preferredLanguage", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.desiredAreaCode", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.contactPhone", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.SubscriberPIN", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.SIM", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.ESNIMEI", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.RequestStatus", language));
        headers.add(Languages.getString("jsp.admin.reports.activation.verizon.activationPhoneNumber", language));
        
        return headers;
        
        
        
        
        

    }
    
    
    
    
    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {
        String accessLevel = (String) additionalData.get("accessLevel");
        String distChainType = (String) additionalData.get("distChainType");
        int index = 0;
        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            if(index == 14){
                if (accessLevel.equals(DebisysConstants.REP) || accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
                    writer.write(stringifyRow);
                }
            }else if(index == 15){
                if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT)
                        || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                    writer.write(stringifyRow);
                }
            }else if(index == 16){
                if (accessLevel.equals(DebisysConstants.AGENT)
                        || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                    writer.write(stringifyRow);
                }
            }else if(index == 17){
                if (accessLevel.equals(DebisysConstants.ISO)) {
                    writer.write(stringifyRow);
                }
            }else if(index == 20){
                if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
                    writer.write(stringifyRow);
                }
            }else if(index == 21){
                if (accessLevel.equals(DebisysConstants.AGENT)
                        || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                    writer.write(stringifyRow);
                }
            }else if(index == 22){
                if (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    writer.write(stringifyRow);
                }
            }else {
                writer.write(stringifyRow);
            }

            index++;
        }

    }
	
	
}
