package net.emida.supportsite.reports.activation.verizon.detail;

import java.util.ArrayList;

public class ControlOptionCombo {
	
	String idControl;

	String nextIdControl;
	String entityLoad;
	String titleCombo;
	ArrayList<String> optionsCombo;
	
	
	
	
	public String getIdControl() {
		return idControl;
	}
	public void setIdControl(String idControl) {
		this.idControl = idControl;
	}
	
	public String getNextIdControl() {
		return nextIdControl;
	}
	
	public void setNextIdControl(String nextControl) {
		this.nextIdControl = nextControl;
	}
	
	public String getEntityLoad() {
		return entityLoad;
	}
	
	public void setEntityLoad(String entityLoad) {
		this.entityLoad = entityLoad;
	}
	
	public ArrayList<String> getOptionsCombo() {
		return optionsCombo;
	}
	
	public void setOptionsCombo(ArrayList<String> optionsCombo) {
		this.optionsCombo = optionsCombo;
	}
	
	public String getTitleCombo() {
		return titleCombo;
	}
	
	public void setTitleCombo(String titleCombo) {
		this.titleCombo = titleCombo;
	}
	
	
	
}
