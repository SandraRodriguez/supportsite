package net.emida.supportsite.reports.activation.verizon.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import net.emida.supportsite.reports.activation.verizon.model.ActivationVerizonReportModel;
import net.emida.supportsite.reports.spiff.deatil.dao.ActivationDetailReportModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

public interface ActivationVerizonDao {
	
	List<ActivationVerizonReportModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;
	
    Long count(Map<String, Object> queryData) throws TechnicalException;
    
    Long countAll() throws TechnicalException;

    List<Map<String, Object>> listActivationVerizonMerchants() throws TechnicalException;

	Vector findMerchantBYRep(String merchant, String strRefId, String strDistChainType, String string, String string2,
			String string3, String string4, ArrayList<String> fieldToRead);

	Vector findMerchantBYRep(String merchant, String strRefId, String strDistChainType, String string, String string2,
			String string3, String string4, ArrayList<String> fieldToRead, Map<String, Object> queryData);
}
