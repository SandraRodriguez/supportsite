package net.emida.supportsite.reports.activation.verizon.model;

public enum ActivationRequestVerizonStatus {
	
	ALL("","-1","ALL"),
	PENDING("3815E841-6995-E611-A02C-000C291026C1","PEN","Pending"),
	COMPLETED("3915E841-6995-E611-A02C-000C291026C1","CMP","Completed"),
	CANCELLED("3A15E841-6995-E611-A02C-000C291026C1","CAN","Cancelled");
	
	private String statusId;
	private String satusCode;
	private String decription;
	
	ActivationRequestVerizonStatus(String statusId, String statusCode, String decription  ){
		this.statusId =statusId;
		this.satusCode = statusCode ;
		this.decription=decription;
	}
	
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	
	public String getDecription() {
		return decription;
	}
	public void setDecription(String decription) {
		this.decription = decription;
	}
	
	
	public String getSatusCode() {
		return satusCode;
	}

	public void setSatusCode(String satusCode) {
		this.satusCode = satusCode;
	}

	
	
	
}
