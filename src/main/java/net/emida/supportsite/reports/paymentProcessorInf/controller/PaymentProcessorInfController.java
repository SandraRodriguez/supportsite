/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.paymentProcessorInf.controller;

import au.com.bytecode.opencsv.CSVWriter;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.paymentProcessorInf.dto.LastTransactionsDto;
import net.emida.supportsite.reports.paymentProcessorInf.dto.PaymentProcessorInfDto;
import net.emida.supportsite.reports.paymentProcessorInf.repository.IPaymentProcessorInfDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/26/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4346
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class PaymentProcessorInfController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentProcessorInfController.class);

    @Autowired
    private IPaymentProcessorInfDao paymentProcessorInfDao;

    @RequestMapping(value = UrlPaths.PAYMENT_PROCESSOR_INFORMATION, method = RequestMethod.GET)
    public String __init__() {
        return "reports/paymentProcessorInf/paymentProcessorInfoView";
    }

    @RequestMapping(value = UrlPaths.PAYMENT_PROCESSOR_INFORMATION + "/findByCreateDt", method = RequestMethod.GET)
    @ResponseBody
    public List<PaymentProcessorInfDto> findByCreateDt(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return paymentProcessorInfDao.findByCreateDt(new Timestamp(strToDate(startDt).getTime()), new Timestamp(strToDate(endDt).getTime()));
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.PAYMENT_PROCESSOR_INFORMATION + "/findLastTransactionByMerchantId", method = RequestMethod.GET)
    @ResponseBody
    public List<LastTransactionsDto> findLastTransactionByMerchantId(@RequestParam(value = "merchant_id") Long merchant_id, @RequestParam(value = "authorizationNumber") String authorizationNumber,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return paymentProcessorInfDao.findLastTransactionByMerchantId(merchant_id, authorizationNumber);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    /**
     *
     * @param startDt
     * @param endDt
     * @param model
     * @param locale
     * @param req
     * @param httpSession
     * @param response
     */
    @RequestMapping(value = UrlPaths.PAYMENT_PROCESSOR_INFORMATION + "/csv", method = RequestMethod.GET)
    public void csvDownload(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession, HttpServletResponse response) {
        CSVWriter writer = null;
        try {
            writer = new CSVWriter(new OutputStreamWriter(response.getOutputStream()));
            buildCSVReport(writer, startDt, endDt);
            response.addHeader("Content-Disposition", "attachment;filename=paypal_report.csv");
            response.setContentType("text/csv");
            response.flushBuffer();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                LOGGER.info(ex.getMessage());
            }
        }
    }

    /**
     *
     * @param writer
     * @param startDt
     * @param endDt
     */
    private void buildCSVReport(CSVWriter writer, String startDt, String endDt) {
        SessionData sessionData = getSessionData();
        List<String[]> data = new ArrayList<String[]>();
        data.add(new String[]{
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c1", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c2", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c3", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c4", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c5", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c6", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c7", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c8", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c9", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c10", sessionData.getLanguage()),
            Languages.getString("jsp.includes.menu.report.ppi.tbl1.c11", sessionData.getLanguage())
        });
        List<PaymentProcessorInfDto> list = paymentProcessorInfDao.findByCreateDt(new Timestamp(strToDate(startDt).getTime()), new Timestamp(strToDate(endDt).getTime()));
        for (PaymentProcessorInfDto content : list) {
            data.add(new String[]{
                String.valueOf(content.getCreateDt()),
                String.valueOf(content.getMerchantId()),
                content.getLegal_businessname(),
                content.getAuthorizationNumber(),
                content.getTransactionId(),
                content.getEmail(),
                content.getHost(),
                String.valueOf(content.getPaymentAmount()),
                String.valueOf(content.getFee()),
                String.valueOf(content.getTotalAmount()),
                (content.getState().equalsIgnoreCase("approved") ? "pending" : content.getState())
            });
        }
        writer.writeAll(data);
    }

    private Date strToDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
