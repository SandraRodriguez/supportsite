/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.paymentProcessorInf.repository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import net.emida.supportsite.reports.paymentProcessorInf.dto.LastTransactionsDto;
import net.emida.supportsite.reports.paymentProcessorInf.dto.PaymentProcessorInfDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/26/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4346
 */
@Repository("IPaymentProcessorInfDao")
public class PaymentProcessorInfRepository implements IPaymentProcessorInfDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentProcessorInfRepository.class);

    private static final String FIND_PAYMENT_PROCESSOR_TXS_BY_RANGE = "SELECT ppt.id, ppt.paymentId, ppt.authorizationNumber, pptd.transactionId, pptd.host, pptd.payer_id, ppt.fee, ppt.paymentAmount, ppt.totalAmount, ppt.state, ppt.createDt, ppt.updateDt, ppt.merchantId, m.legal_businessname, pptd.email, pptd.phone, pptd.first_name, pptd.last_name "
            + " FROM dbo.PaymentProcessorTransactions ppt WITH(nolock) "
            + " LEFT JOIN dbo.PaymentProcessorTransactionsDetail pptd WITH(nolock) ON ppt.id = pptd.id_ppt "
            + " INNER JOIN dbo.merchants m WITH(nolock) ON m.merchant_id = ppt.merchantId WHERE ppt.createDt BETWEEN ? AND DATEADD(day, 1, ?) ORDER by ppt.createDt DESC";
    
    private static final String FIND_LAST_TX_BY_MERCHANT_ID = "SELECT top 10 wt.[datetime], wt.millennium_no, wt.merchant_id, wt.rec_id, wt.amount "
            + "FROM dbo.Web_Transactions wt WITH(nolock) WHERE merchant_id =  ? AND wt.[datetime] >= (select updateDt from PaymentProcessorTransactions where authorizationNumber = ?) ";            

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<PaymentProcessorInfDto> findByCreateDt(Timestamp startDt, Timestamp endDt) {
        LOGGER.info("findByCreateDt::query -> " + FIND_PAYMENT_PROCESSOR_TXS_BY_RANGE);
        return this.jdbcTemplate.query(FIND_PAYMENT_PROCESSOR_TXS_BY_RANGE, new Object[]{
            startDt, endDt
        }, new RowMapper<PaymentProcessorInfDto>() {

            /**
             *
             * @param resultSet
             * @param i
             * @return
             * @throws SQLException
             */
            @Override
            public PaymentProcessorInfDto mapRow(ResultSet resultSet, int i) throws SQLException {
                return new PaymentProcessorInfDto(
                        resultSet.getString("id"),
                        resultSet.getString("paymentId"),
                        resultSet.getString("authorizationNumber"),
                        resultSet.getString("transactionId"),
                        resultSet.getString("host"),
                        resultSet.getString("payer_id"),
                        new BigDecimal(resultSet.getString("fee").replaceAll(",", ".")).setScale(2, RoundingMode.HALF_UP),
                        new BigDecimal(resultSet.getString("paymentAmount").replaceAll(",", ".")).setScale(2, RoundingMode.HALF_UP),
                        new BigDecimal(resultSet.getString("totalAmount").replaceAll(",", ".")).setScale(2, RoundingMode.HALF_UP),
                        resultSet.getString("state"),
                        resultSet.getTimestamp("createDt"),
                        resultSet.getTimestamp("updateDt"),
                        resultSet.getLong("merchantId"),
                        resultSet.getString("legal_businessname"),
                        resultSet.getString("email"),
                        resultSet.getString("phone"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"));
            }
        });
    }

    @Override
    public List<LastTransactionsDto> findLastTransactionByMerchantId(Long merchant_id, String authorizationNumber) {
        LOGGER.info("findLastTransactionByMerchantId::query -> " + FIND_LAST_TX_BY_MERCHANT_ID);
        return this.jdbcTemplate.query(FIND_LAST_TX_BY_MERCHANT_ID, new Object[]{
            merchant_id,
            authorizationNumber
        }, new RowMapper<LastTransactionsDto>() {

            /**
             *
             * @param resultSet
             * @param i
             * @return
             * @throws SQLException
             */
            @Override
            public LastTransactionsDto mapRow(ResultSet resultSet, int i) throws SQLException {
                return new LastTransactionsDto(
                        resultSet.getTimestamp("datetime"),
                        resultSet.getLong("millennium_no"),
                        resultSet.getLong("merchant_id"),
                        resultSet.getLong("rec_id"),
                        new BigDecimal(resultSet.getString("amount").replaceAll(",", ".")).setScale(2, RoundingMode.HALF_UP));
            }
        });
    }

}
