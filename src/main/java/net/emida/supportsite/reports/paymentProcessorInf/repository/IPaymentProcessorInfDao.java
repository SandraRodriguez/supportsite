/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.paymentProcessorInf.repository;

import java.sql.Timestamp;
import java.util.List;
import net.emida.supportsite.reports.paymentProcessorInf.dto.LastTransactionsDto;
import net.emida.supportsite.reports.paymentProcessorInf.dto.PaymentProcessorInfDto;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
public interface IPaymentProcessorInfDao {

    /**
     *
     * @param startDt
     * @param endDt
     * @return
     */
    public List<PaymentProcessorInfDto> findByCreateDt(Timestamp startDt, Timestamp endDt);

    /**
     * 
     * @param merchant_id
     * @param authorizationNumber
     * @return 
     */
    public List<LastTransactionsDto> findLastTransactionByMerchantId(Long merchant_id, String authorizationNumber);

}
