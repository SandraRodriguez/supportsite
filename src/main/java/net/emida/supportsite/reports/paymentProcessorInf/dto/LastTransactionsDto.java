package net.emida.supportsite.reports.paymentProcessorInf.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/26/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4346
 */
public class LastTransactionsDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private Timestamp datetime;
    private Long millennium_no;
    private Long merchant_id;
    private Long rec_id;
    private BigDecimal amount;

    public LastTransactionsDto() {
    }

    /**
     *
     * @param datetime
     * @param millennium_no
     * @param merchant_id
     * @param rec_id
     * @param amount
     */
    public LastTransactionsDto(Timestamp datetime, Long millennium_no, Long merchant_id, Long rec_id, BigDecimal amount) {
        this.datetime = datetime;
        this.millennium_no = millennium_no;
        this.merchant_id = merchant_id;
        this.rec_id = rec_id;
        this.amount = amount;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public Long getMillennium_no() {
        return millennium_no;
    }

    public void setMillennium_no(Long millennium_no) {
        this.millennium_no = millennium_no;
    }

    public Long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(Long merchant_id) {
        this.merchant_id = merchant_id;
    }

    public Long getRec_id() {
        return rec_id;
    }

    public void setRec_id(Long rec_id) {
        this.rec_id = rec_id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
