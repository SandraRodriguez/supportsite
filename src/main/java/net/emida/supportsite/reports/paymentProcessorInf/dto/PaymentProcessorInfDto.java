package net.emida.supportsite.reports.paymentProcessorInf.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/26/2018
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4346
 */
public class PaymentProcessorInfDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String paymentId;
    private String authorizationNumber;
    private String transactionId;
    private String host;
    private String payer_id;
    private BigDecimal fee;
    private BigDecimal paymentAmount;
    private BigDecimal totalAmount;
    private String state;
    private Timestamp createDt;
    private Timestamp updateDt;
    private Long merchantId;
    private String legal_businessname;
    private String email;
    private String phone;
    private String first_name;
    private String last_name;

    public PaymentProcessorInfDto() {
    }

    /**
     *
     * @param id
     * @param paymentId
     * @param authorizationNumber
     * @param transactionId
     * @param host
     * @param payer_id
     * @param fee
     * @param paymentAmount
     * @param totalAmount
     * @param state
     * @param createDt
     * @param updateDt
     * @param merchantId
     * @param legal_businessname
     * @param email
     * @param phone
     * @param first_name
     * @param last_name
     */
    public PaymentProcessorInfDto(String id, String paymentId, String authorizationNumber, String transactionId, String host, String payer_id, BigDecimal fee, BigDecimal paymentAmount, BigDecimal totalAmount, String state, Timestamp createDt, Timestamp updateDt, Long merchantId, String legal_businessname, String email, String phone, String first_name, String last_name) {
        this.id = id;
        this.paymentId = paymentId;
        this.authorizationNumber = authorizationNumber;
        this.transactionId = transactionId;
        this.host = host;
        this.payer_id = payer_id;
        this.fee = fee;
        this.paymentAmount = paymentAmount;
        this.totalAmount = totalAmount;
        this.state = state;
        this.createDt = createDt;
        this.updateDt = updateDt;
        this.merchantId = merchantId;
        this.legal_businessname = legal_businessname;
        this.email = email;
        this.phone = phone;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getAuthorizationNumber() {
        return authorizationNumber;
    }

    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(String payer_id) {
        this.payer_id = payer_id;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Timestamp getCreateDt() {
        return createDt;
    }

    public void setCreateDt(Timestamp createDt) {
        this.createDt = createDt;
    }

    public Timestamp getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(Timestamp updateDt) {
        this.updateDt = updateDt;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getLegal_businessname() {
        return legal_businessname;
    }

    public void setLegal_businessname(String legal_businessname) {
        this.legal_businessname = legal_businessname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

}
