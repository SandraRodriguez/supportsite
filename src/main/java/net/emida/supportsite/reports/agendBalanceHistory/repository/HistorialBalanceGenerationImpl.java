/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.agendBalanceHistory.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import net.emida.supportsite.reports.agendBalanceHistory.dto.ScheduleBalanceHistoryDto;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/03/2017
 * @version 1.0
 */
@Repository("historialBalanceGenerationDao")
public class HistorialBalanceGenerationImpl implements HistorialBalanceGenerationDao {
    
    private static final String FIND_SCHEDULE_BALANCE_HISTORY = "SELECT * FROM ScheduleBalanceHistory WHERE startDt >= ? AND startDt <= CAST(CONVERT(VARCHAR(10), ?, 110) + ' 23:59:59' AS DATETIME) "
            + "AND repId = ? ORDER BY createDt ASC";
    
    private static final String INSERT_INTO_SCHEDULE_BALANCE_HISTORY_TABLE = "INSERT INTO ScheduleBalanceHistory(repId, filename, balanceType, startDt, endDt) VALUES(?, ?, ?, ?, ?)";

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    /**
     *
     * @param startDt
     * @param endDate
     * @return
     * @throws TechnicalException
     */
    @Override
    public List<ScheduleBalanceHistoryDto> getHistoryBalanceByDate(Date startDt, Date endDate, long repId) throws TechnicalException {
        List<ScheduleBalanceHistoryDto> list = this.jdbcTemplate.query(
                FIND_SCHEDULE_BALANCE_HISTORY, new Object[]{
                    startDt,
                    endDate,
                    repId
                },
                new RowMapper<ScheduleBalanceHistoryDto>() {
            /**
             *
             * @param resultSet
             * @param rowNum
             * @return
             * @throws SQLException
             */
            @Override
            public ScheduleBalanceHistoryDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                ScheduleBalanceHistoryDto scheduleBalanceHistoryDto = new ScheduleBalanceHistoryDto(
                        resultSet.getString("id"),
                        resultSet.getString("fileName"),
                        resultSet.getString("balanceType"),
                        resultSet.getString("status"),
                        resultSet.getLong("records"),
                        resultSet.getString("sftpPath"),
                        resultSet.getDate("createDt"),
                        resultSet.getDate("startDt"),
                        resultSet.getDate("endDt")
                );
                return scheduleBalanceHistoryDto;
            }
        });
        return list;
    }

    /**
     * 
     * @param repId
     * @param startDt
     * @param endDate
     * @return
     * @throws TechnicalException 
     */
    @Override
    public ResponseEntity<String> onSchedulerBalanceHistory(String repId, Date startDt, Date endDate) throws TechnicalException {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        int rows = this.jdbcTemplate.update(INSERT_INTO_SCHEDULE_BALANCE_HISTORY_TABLE, new Object[]{
            new Long(repId),
            repId + ".zip",
            "SCHEDULED",
            startDt,
            endDate
        });
        if (rows > 0) {
            return new ResponseEntity<String>(HttpStatus.OK.toString(), httpHeaders, HttpStatus.OK);
        }
        return new ResponseEntity<String>(HttpStatus.BAD_REQUEST.toString(), httpHeaders, HttpStatus.OK);
    }

}
