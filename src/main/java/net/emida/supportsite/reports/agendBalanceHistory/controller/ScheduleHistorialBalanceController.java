/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.agendBalanceHistory.controller;

import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.agendBalanceHistory.dto.ScheduleBalanceHistoryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import net.emida.supportsite.reports.agendBalanceHistory.repository.HistorialBalanceGenerationDao;
import net.emida.supportsite.reports.agendBalanceHistory.repository.HistorialBalanceGenerationImpl;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/03/2017
 * @version 1.0
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class ScheduleHistorialBalanceController extends BaseController {

    @Autowired
    HistorialBalanceGenerationDao balanceGenerationDao;

    @Autowired
    ServletContext context;

    private Channel channel;
    private Session session;
    private ChannelSftp channelSftp;
    private static final ResourceBundle CONFG_BUNDLE = ResourceBundle.getBundle("com.debisys.ftpConfg.sftpConfg");

    @RequestMapping(value = UrlPaths.SCHEDULER_HISTORIAL_BALANCE, method = RequestMethod.GET)
    public String reportForm(Map model) {
        return "reports/agendBalanceHistory/agendBalanceHistory";
    }

    @RequestMapping(value = "/findHistoryBalance", method = RequestMethod.GET)
    @ResponseBody
    public List<ScheduleBalanceHistoryDto> onFindHistoryBalance(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {        
        try {
            SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
            List<ScheduleBalanceHistoryDto> balanceHistoryDto = balanceGenerationDao.getHistoryBalanceByDate(
                    parseStringToDate(startDt),
                    parseStringToDate(endDt),
                    new Long(sessionData.getUser().getRefId())
            );
            return balanceHistoryDto;
        } catch (TechnicalException e) {
            return null;
        }
    }

    @RequestMapping(value = "/onScheduleHistoryBalance", method = RequestMethod.POST)
    public ResponseEntity<String> onScheduleHistoryBalance(@RequestParam(value = "startDt") String startDt, @RequestParam(value = "endDt") String endDt,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        ResponseEntity<String> responseEntity = null;        
        try {
            responseEntity = balanceGenerationDao.onSchedulerBalanceHistory(
                    sessionData.getUser().getRefId(),
                    parseStringToDate(startDt),
                    parseStringToDate(endDt));
            return responseEntity;
        } catch (TechnicalException e) {
            return responseEntity;
        }
    }

    /**
     *
     * @param fileName
     * @param request
     * @param response
     * @param httpSession
     * @throws IOException
     */
    @RequestMapping(value = "/doDownloadHistoryBalance", method = RequestMethod.GET)
    public void doDownload(@RequestParam(value = "fileName") String fileName, HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) throws IOException {
        SessionData sessionData = (SessionData) httpSession.getAttribute("SessionData");
        response.setContentType("application/zip");
        response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
        OutputStream os = response.getOutputStream();
        InputStream remoteFile = null;
        try {
            remoteFile = doSftpConection(sessionData.getUser().getRefId()).get(fileName);
            byte[] bytes = org.apache.poi.util.IOUtils.toByteArray(remoteFile);
            os.write(bytes);
            os.flush();
            response.flushBuffer();
        } catch (SftpException ex) {
            Logger.getLogger(HistorialBalanceGenerationImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            os.close();
            remoteFile.close();
            session.disconnect();
            channel.disconnect();
            channelSftp.disconnect();
        }
    }

    /**
     *
     * @param refId
     * @return
     */
    private ChannelSftp doSftpConection(String refId) {
        try {
            JSch secureChannel = new JSch();
            session = secureChannel.getSession(
                    DebisysConfigListener.getFtpUser(context),
                    DebisysConfigListener.getFtpHost(context),
                    Integer.parseInt(DebisysConfigListener.getFtpPort(context)));

            session.setPassword(DebisysConfigListener.getFtpPassword(context));
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            channel = session.openChannel(DebisysConfigListener.getProviderType(context).toLowerCase());
            channel.connect();

            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(DebisysConfigListener.getFtpTargetFolder(context) + refId);            
        } catch (JSchException ex) {
            Logger.getLogger(ScheduleHistorialBalanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SftpException ex) {
            Logger.getLogger(ScheduleHistorialBalanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return channelSftp;
    }

    private static Date parseStringToDate(String currentDt) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return simpleDateFormat.parse(currentDt);
        } catch (ParseException ex) {
            return null;
        }
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 16;
    }

}
