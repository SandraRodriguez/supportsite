/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.agendBalanceHistory.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/03/2017
 * @version 1.0
 */
public class ScheduleBalanceHistoryDto implements Serializable {

    private String id;
    private String fileName;
    private String balanceType;
    private String status;
    private long records;
    private String sftpPath;
    private Date createDt;
    private Date startDt;
    private Date endDt;

    public ScheduleBalanceHistoryDto() {
    }

    /**
     *
     * @param fileName
     * @param balanceType
     * @param startDt
     * @param endDt
     */
    public ScheduleBalanceHistoryDto(String fileName, String balanceType, Date startDt, Date endDt) {
        this.fileName = fileName;
        this.balanceType = balanceType;
        this.startDt = startDt;
        this.endDt = endDt;
    }

    /**
     *
     * @param id
     * @param fileName
     * @param balanceType
     * @param status
     * @param records
     * @param sftpPath
     * @param createDt
     * @param startDt
     * @param endDt
     */
    public ScheduleBalanceHistoryDto(String id, String fileName, String balanceType, String status, long records, String sftpPath, Date createDt, Date startDt, Date endDt) {
        this.id = id;
        this.fileName = fileName;
        this.balanceType = balanceType;
        this.status = status;
        this.records = records;
        this.sftpPath = sftpPath;
        this.createDt = createDt;
        this.startDt = startDt;
        this.endDt = endDt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getBalanceType() {
        return balanceType;
    }

    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getRecords() {
        return records;
    }

    public void setRecords(long records) {
        this.records = records;
    }

    public String getSftpPath() {
        return sftpPath;
    }

    public void setSftpPath(String sftpPath) {
        this.sftpPath = sftpPath;
    }

    public Date getCreateDt() {
        return createDt;
    }

    public void setCreateDt(Date createDt) {
        this.createDt = createDt;
    }

    public Date getStartDt() {
        return startDt;
    }

    public void setStartDt(Date startDt) {
        this.startDt = startDt;
    }

    public Date getEndDt() {
        return endDt;
    }

    public void setEndDt(Date endDt) {
        this.endDt = endDt;
    }

}
