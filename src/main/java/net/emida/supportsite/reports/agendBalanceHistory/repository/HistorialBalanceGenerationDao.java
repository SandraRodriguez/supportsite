/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.agendBalanceHistory.repository;

import java.util.Date;
import java.util.List;
import net.emida.supportsite.reports.agendBalanceHistory.dto.ScheduleBalanceHistoryDto;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author janez - janez@emida.net
 * @since 02/03/2017
 * @version 1.0
 */
public interface HistorialBalanceGenerationDao {

    /**
     * 
     * @param startDt
     * @param endDate
     * @param repId
     * @return
     * @throws TechnicalException 
     */
    public List<ScheduleBalanceHistoryDto> getHistoryBalanceByDate(Date startDt, Date endDate, long repId) throws TechnicalException;

    /**
     *
     * @param repId
     * @param startDt
     * @param endDate
     * @return
     * @throws TechnicalException
     */
    public ResponseEntity<String> onSchedulerBalanceHistory(String repId, Date startDt, Date endDate) throws TechnicalException;
    
}
