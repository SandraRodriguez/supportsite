package net.emida.supportsite.reports.balanceMapping.detail.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


/**
 *
 * @author g.martinez
 */
public class BalanceMappingMapper implements RowMapper<BalanceMappingModel>{

    @Override
    public BalanceMappingModel mapRow(ResultSet rs, int i) throws SQLException {
        BalanceMappingModel model = new BalanceMappingModel();
        model.setRwn(rs.getBigDecimal("rwn"));
        
        /*model.setId(rs.getString("id"));
        model.setDate(rs.getTimestamp("date"));
        model.setUserName(rs.getString("userName"));*/
        model.setMerchant(rs.getString("merchant"));
        model.setMerchantId(rs.getBigDecimal("merchantId"));
        model.setContact(rs.getString("contact"));
        model.setPhone(rs.getString("phone"));
        model.setStatus(rs.getString("status"));       
        return model;
    }
    
}
