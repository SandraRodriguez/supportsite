package net.emida.supportsite.reports.balanceMapping.detail;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.reports.balanceMapping.detail.dao.BalanceMappingModel;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author g.martinez
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class BalanceMappingController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(BalanceMappingController.class);

    @Autowired
    private IBalanceMappingDao iBalanceMappingDao;

    @Autowired
    @Qualifier("balanceMappingCsvBuilder")
    private CsvBuilderService balanceMappingCsvBuilderService;

    @RequestMapping(value = UrlPaths.MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_PATH, method = RequestMethod.GET)
    public String reportBuild(Model model, @Valid BalanceMappingForm reportModel, BindingResult result) throws TechnicalException, ApplicationException {

        Map<String, Object> queryData = fillQueryData();

        Long totalRecords = iBalanceMappingDao.count(queryData);
        Long cancelledRecords = iBalanceMappingDao.countCancelled(queryData);
        Long disabledRecords = iBalanceMappingDao.countDisabled(queryData);
        Long activeRecords = iBalanceMappingDao.countActive(queryData);  
        model.addAttribute("totalRecords", totalRecords);
        model.addAttribute("cancelledRecords", cancelledRecords);
        model.addAttribute("disabledRecords", disabledRecords);
        model.addAttribute("activeRecords", activeRecords);
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;
        return "reports/balancemapping/balanceMappingResult";
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<BalanceMappingModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = fillQueryData();
        List<BalanceMappingModel> list = iBalanceMappingDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;

        return list;
    }

    private Map<String, Object> fillQueryData() {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("strAccessLevel", getAccessLevel());
        queryData.put("strDistChainType", getDistChainType());
        
        return queryData;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 14;
    }

    /**
     * Fill the selects type inputs
     *
     * @param model
     */
    /*private void fillBalanceMapping(Map model) {

    }*/

    
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response,
            @RequestParam Integer totalRecords, @RequestParam String sortField, @RequestParam String sortOrder) throws IOException {

        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file.");

        Map<String, Object> queryData = fillQueryData();
        List<BalanceMappingModel> list = iBalanceMappingDao.list(1, totalRecords, sortField, sortOrder, queryData);

        log.debug("Total list={}", list.size());

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.debug("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());

        balanceMappingCsvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.debug("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");
        return zipFileResource;

    }

}
