package net.emida.supportsite.reports.balanceMapping.detail.dao;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import net.emida.supportsite.reports.balanceMapping.detail.IBalanceMappingDao;
import org.springframework.stereotype.Repository;

@Repository
public class BalanceMappingJdbcDao implements IBalanceMappingDao {

    private static final Logger log = LoggerFactory.getLogger(BalanceMappingJdbcDao.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll() {
        try {
            return 0L;
        } catch (DataAccessException ex) {
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<BalanceMappingModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {

        try {
            BalanceMappingQueryBuilder queryBuilder = new BalanceMappingQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();
            log.debug("Query : {}", SQL_QUERY);
            if (queryData != null) {
                queryData.clear();
            }
            queryData = null;
            return jdbcTemplate.query(SQL_QUERY, new BalanceMappingMapper());

        } catch (DataAccessException ex) {
            log.error("Data access error in list balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in list balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {
            BalanceMappingQueryBuilder queryBuilder = new BalanceMappingQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();
            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            //return 7L;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long countCancelled(Map<String, Object> queryData) throws TechnicalException {
        try {
            BalanceMappingQueryBuilder queryBuilder = new BalanceMappingQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.countCancelledQuery();
            log.debug("Query Canceled Count : {}", SQL_QUERY_COUNT);
            //return 7L;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count cancelled  balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count cancelled balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long countDisabled(Map<String, Object> queryData) throws TechnicalException {
        try {
            BalanceMappingQueryBuilder queryBuilder = new BalanceMappingQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.countDisabledQuery();
            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            //return 7L;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count disabled balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count disabled balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long countActive(Map<String, Object> queryData) throws TechnicalException {
        try {
            BalanceMappingQueryBuilder queryBuilder = new BalanceMappingQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.countActiveQuery();
            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            //return 7L;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count active balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count active balance mapping report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

}
