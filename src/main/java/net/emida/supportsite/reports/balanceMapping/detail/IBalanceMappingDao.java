/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceMapping.detail;

import com.debisys.users.SessionData;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import net.emida.supportsite.reports.balanceMapping.detail.dao.BalanceMappingModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author g.martinez
 */
public interface IBalanceMappingDao {
 
    public List<BalanceMappingModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    public Long count(Map<String, Object> queryData) throws TechnicalException;
    
    public Long countCancelled(Map<String, Object> queryData) throws TechnicalException;
    
    public Long countDisabled(Map<String, Object> queryData) throws TechnicalException;
    
    public Long countActive(Map<String, Object> queryData) throws TechnicalException;   
   
}
