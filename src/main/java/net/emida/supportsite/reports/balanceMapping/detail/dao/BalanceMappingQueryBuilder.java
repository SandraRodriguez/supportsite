
package net.emida.supportsite.reports.balanceMapping.detail.dao;


import com.debisys.utils.DebisysConstants;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author g.martinez
 */
public class BalanceMappingQueryBuilder {

    private static final Logger log = LoggerFactory.getLogger(BalanceMappingQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    private String isoId;
    private String strAccessLevel;
    private String strDistChainType;


    /**
     *
     * @param queryData
     */
    public BalanceMappingQueryBuilder(Map<String, Object> queryData) {
        this.isoId = (String) queryData.get("refId");
        this.strAccessLevel = (String) queryData.get("strAccessLevel");
        this.strDistChainType = (String) queryData.get("strDistChainType");
        log.debug("Query Map = {}", queryData);
    }

    public BalanceMappingQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.isoId = (String) queryData.get("refId");
        this.strAccessLevel = (String) queryData.get("strAccessLevel");
        this.strDistChainType = (String) queryData.get("strDistChainType");
    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery());
        sb0.append(") AS tbl1 ");
        return sb0.toString();
    }

    public String mainQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT m.merchant_id AS MerchantId, m.dba AS Merchant, m.contact AS Contact, m.contact_phone AS Phone, ");
        mainQuery.append("(CASE  ");
        mainQuery.append("WHEN (m.cancelled = 1 AND m.dateCancelled IS NOT NULL) THEN 'Cancelled' ");
        mainQuery.append("WHEN (m.cancelled = 0 AND m.dateCancelled IS NOT NULL) THEN 'Disabled' ");
        mainQuery.append("ELSE 'Active' ");
        mainQuery.append("END) as Status ");
        //mainQuery.append("WHEN ((m.cancelled = 0 AND m.dateCancelled IS NULL) THEN 'Active' ");
        mainQuery.append("FROM merchants m ");
        mainQuery.append(isoInnerJoinQuery());
        mainQuery.append("AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 ) ");
        return mainQuery.toString();
    }

    public String reviewQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT ");
        mainQuery.append("(SELECT count (*) FROM merchants m ").append(isoInnerJoinQuery()).append(" AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 )) AS Total, ");
        mainQuery.append("(SELECT count (*) FROM merchants m ").append(isoInnerJoinQuery()).append(" AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 )) AS Cancelled, ");
        mainQuery.append("(SELECT count (*) FROM merchants m ").append(isoInnerJoinQuery()).append(" AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 )) AS Disable, ");
        mainQuery.append("(SELECT count (*) FROM merchants m ").append(isoInnerJoinQuery()).append(" AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 )) AS Active ");
        mainQuery.append("FROM merchants m ");
        mainQuery.append(isoInnerJoinQuery());
        mainQuery.append(" AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 ) ");
        return mainQuery.toString();
    }

    /*private String isoInnerJoinQuery(){
        StringBuilder builder = new StringBuilder();
        builder.append(" INNER JOIN reps r WITH(NOLOCK) ON  r.rep_id = m.rep_id "); // rep
        builder.append(" INNER JOIN reps s WITH(NOLOCK) ON  s.rep_id = r.iso_id "); // subagent
        builder.append(" INNER JOIN reps a WITH(NOLOCK) ON  a.rep_id = s.iso_id "); // agent
        builder.append(" INNER JOIN reps i WITH(NOLOCK) ON  i.rep_id = a.iso_id "); // iso
        return builder.toString();
    }*/
    
    
    public String isoInnerJoinQuery() {
        StringBuilder sql = new StringBuilder("");
        
        if (strAccessLevel.equals(DebisysConstants.ISO)) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                sql.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
                sql.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id");
                sql.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 5 AND iso_id ");
                sql.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 4 AND iso_id = " + isoId + "))) ");
            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                sql.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
                sql.append(" WHERE r.iso_id = " + isoId + " ");
            }
        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
            sql.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            sql.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id");
            sql.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 5 AND iso_id = " + isoId + ")) ");
        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
            sql.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            sql.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id = " + isoId + ") ");
        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
            sql.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            sql.append(" WHERE r.rep_id = " + isoId + " ");
        }
        return sql.toString();
    }

    public String countCancelledQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT count (*) ");
        mainQuery.append("FROM merchants m ");
        mainQuery.append(isoInnerJoinQuery());
        mainQuery.append("AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 ) AND m.cancelled = 1 AND m.dateCancelled IS NOT NULL   ");
        return mainQuery.toString();
    }

    public String countDisabledQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT count (*) ");
        mainQuery.append("FROM merchants m ");
        mainQuery.append(isoInnerJoinQuery());
        mainQuery.append("AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 ) AND m.cancelled = 0 AND m.dateCancelled IS NOT NULL ");
        return mainQuery.toString();
    }

    public String countActiveQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT count (*) ");
        mainQuery.append("FROM merchants m ");
        mainQuery.append(isoInnerJoinQuery());
        mainQuery.append(" AND (m.latitude IS NULL or m.longitude IS NULL or  m.latitude = 0 or m.longitude = 0 ) AND m.dateCancelled IS NULL ");
        return mainQuery.toString();
    }


    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "Merchant";
            sortOrder = " ASC ";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }



}
