/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceMapping.detail;

import com.debisys.languages.Languages;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author g.martinez
 */
@Service
public class BalanceMappingCsvBuilder implements CsvBuilderService {

    private static final Logger log = LoggerFactory.getLogger(BalanceMappingCsvBuilder.class);

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {
            List<CsvRow> data = (List<CsvRow>) list;
            //File to fill
            writer = getFileWriter(filePath);

            log.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            List<String> headers = generateHeaders(additionalData, writer);
            String headerRow = CsvUtil.generateRow(headers, true);
            writer.write(headerRow);

            fillWriter(data, writer, additionalData);

        } catch (Exception ex) {
            log.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {
            if (additionalData != null) {
                additionalData.clear();
            }
            additionalData = null;
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {

                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {
        double sumTotal = 0;
        double sumActive = 0;
        double sumCancelled = 0;
        double sumDisabled = 0;
        String language = (String) additionalData.get("language");

        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            writer.write(stringifyRow);

            sumTotal++;
            if (cells.get(5).getValue().equalsIgnoreCase("Active")) {
                sumActive++;
            } else if (cells.get(5).getValue().equalsIgnoreCase("Cancelled")) {
                sumCancelled++;
            } else {
                sumDisabled++;
            }
        }
        writer.write("\"\",\"\",\"\",\"\",\"\",\"\"\n");
        writer.write("\"\",\"" + Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.total", language) + "\",\"" + sumTotal + "\",\"\",\"\"\n");
        writer.write("\"\",\"" + Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.active", language) + "\",\"" + sumActive + "\",\"\",\"\"\n");
        writer.write("\"\",\"" + Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.cancelled", language) + "\",\"" + sumCancelled + "\",\"\",\"\"\n");
        writer.write("\"\",\"" + Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.disabled", language) + "\",\"" + sumDisabled + "\",\"\",\"\"\n");
    }

    private List<String> generateHeaders(Map<String, Object> additionalData, Writer writer) {
        String language = (String) additionalData.get("language");
        try {
            writer.write("\"" + Languages.getString("jsp.admin.reports.balanceMapping.merchantRemainingBalanceReport", language) + "\",\"\",\"\",\"\",\"\",\"\"\n");
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(BalanceMappingCsvBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<String> headers = new ArrayList<String>();
        headers.add("#");
        headers.add(Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.merchantId", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.merchant", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.contact", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.phone", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceMapping.merchantRemaining.field.status", language));
        return headers;
    }

}
