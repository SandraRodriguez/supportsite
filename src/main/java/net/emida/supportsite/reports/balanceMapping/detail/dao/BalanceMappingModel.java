/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceMapping.detail.dao;

import com.debisys.utils.DateUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author g.martinez
 */
public class BalanceMappingModel implements CsvRow{
    
    private BigDecimal rwn;
    private BigDecimal merchantId;
    private String merchant;
    private String contact;
    private String phone;
    private String status;

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public BigDecimal getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(BigDecimal merchantId) {
        this.merchantId = merchantId;
    }
    

    @Override
    public List<CsvCell> cells() {
        //SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cella = new CsvCell(String.valueOf(rwn));
        list.add(cella);
        //String formattedDate = date != null ? sdf.format(date) : "";
        CsvCell cell0 = new CsvCell(merchantId.toString());
        list.add(cell0);
        CsvCell cell1 = new CsvCell(merchant);
        list.add(cell1);
        CsvCell cell2 = new CsvCell(contact);
        list.add(cell2);
        CsvCell cell3 = new CsvCell(phone);
        list.add(cell3);
        CsvCell cell4 = new CsvCell(status);
        list.add(cell4);         
        return list;
    }

    /**
     * @return the rwn
     */
    public BigDecimal getRwn() {
        return rwn;
    }

    /**
     * @param rwn the rwn to set
     */
    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
