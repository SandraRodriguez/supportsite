
package net.emida.supportsite.reports.balanceAudit.detail;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.reports.balanceAudit.detail.dao.BalanceAuditModel;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author g.martinez
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class BalanceAuditController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(BalanceAuditController.class);

    @Autowired
    private IBalanceAuditDao iBalanceAuditDao;

    @Autowired
    @Qualifier("balanceAuditCsvBuilder")
    private CsvBuilderService balanceAuditCsvBuilderService;

    @RequestMapping(value = UrlPaths.BALANCE_AUDIT_HISTORICAL_REPORT_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute BalanceAuditForm reportModel) {

        fillBalanceAudit(model);
        return "reports/balanceaudit/balanceAuditForm";
    }

    @RequestMapping(value = UrlPaths.BALANCE_AUDIT_HISTORICAL_REPORT_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid BalanceAuditForm reportModel, BindingResult result) throws TechnicalException, ApplicationException {

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillBalanceAudit(model.asMap());
            return "reports/balanceaudit/balanceAuditForm";
        }

        log.debug("reportBuil {}", result);
        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String formattedStartDate = sdf.format(reportModel.getStartDate());
        String formattedEndDate = sdf.format(reportModel.getEndDate());

        model.addAttribute("startDate", formattedStartDate);
        model.addAttribute("endDate", formattedEndDate);
        model.addAttribute("filterType", reportModel.getFilterType());
        model.addAttribute("filterValue", reportModel.getFilterValue());

        Map<String, Object> queryData = fillQueryData(formattedStartDate, formattedEndDate, reportModel.getFilterType(), reportModel.getFilterValue());

        Long totalRecords = iBalanceAuditDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;
        
        return "reports/balanceaudit/balanceAuditResult";
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.BALANCE_AUDIT_DETAIL_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<BalanceAuditModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String startDate, @RequestParam String endDate, @RequestParam String filterType, @RequestParam String filterValue) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. StartDate={}. EndDate={}. ", startDate, endDate);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, filterType, filterValue);
        List<BalanceAuditModel> list = iBalanceAuditDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;

        return list;
    }

    private Map<String, Object> fillQueryData(String startDate, String endDate, String filterType, String filterValue) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put("filterType", filterType);
        queryData.put("filterValue", filterValue);
        return queryData;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 14;
    }

    /**
     * Fill the selects type inputs
     *
     * @param model
     */
    private void fillBalanceAudit(Map model) {
        fillFilters(model);
        fillChangeTypesFilter(model);
    }

    private void fillFilters(Map model) {
        try {
            Vector<Vector<String>> list = iBalanceAuditDao.listFilters(getSessionData());
            model.put("filterTypes", list);
        } catch (TechnicalException e) {
            log.error("Error listing filters. Ex = {}", e);
        }
    }

    private void fillChangeTypesFilter(Map model) {
        try {
            Vector<Vector<String>> list = iBalanceAuditDao.listChangeTypes(getSessionData());
            model.put("changeTypesList", list);
        } catch (TechnicalException e) {
            log.error("Error listing change types. Ex = {}", e);
        }
    }
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.BALANCE_AUDIT_DETAIL_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String startDate, @RequestParam String endDate, @RequestParam String filterType, @RequestParam String filterValue,
            @RequestParam Integer totalRecords, @RequestParam String sortField, @RequestParam String sortOrder) throws IOException {

        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file. StartDate={}. EndDate={}. FilterType={}. FilterValue={}", startDate, endDate, filterType, filterValue);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, filterType, filterValue);
        List<BalanceAuditModel> list = iBalanceAuditDao.list(1, totalRecords, sortField, sortOrder, queryData);

        log.debug("Total list={}", list.size());
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.debug("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());

        balanceAuditCsvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.debug("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");
        return zipFileResource;

    }

}
