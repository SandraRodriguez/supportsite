
package net.emida.supportsite.reports.balanceAudit.detail.dao;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author g.martinez
 */
public class BalanceAuditQueryBuilder {

    private static final Logger log = LoggerFactory.getLogger(BalanceAuditQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    private String startDate;
    private String endDate;
    private String filterType;
    private String filterValue;


    /**
     *
     * @param queryData
     */
    public BalanceAuditQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.filterType = (String) queryData.get("filterType");
        this.filterValue = (String) queryData.get("filterValue");
    }

    public BalanceAuditQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");        
        this.filterType = (String) queryData.get("filterType");
        this.filterValue = (String) queryData.get("filterValue");  
    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery());
        sb0.append(") AS tbl1 ");
        return sb0.toString();
    }

    public String mainQuery() {       
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT ").append(queryFields()); //SELECT
        mainQuery.append(queryFrom());                     //FROM WITH JOIN
        mainQuery.append(queryWhere());        
        mainQuery.append(queryAndWhere());                    //WHERE
        //mainQuery.append(buildFiltersCondition(this.filterType,this.filterValue));
        return mainQuery.toString();
    }

    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildQuery() {
        return "select ba.id as Id, ba.date as Date, ba.userName as UserName, rt.description as EntityType, ba.entityID as EntityID, ba.changeType as ChangeType, ba.oldValue as OLDValue, ba.newValue as NewValue from BalanceAudit ba inner join Rep_type rt on ba.entityType = rt.type_id";
        //select ba.id as Id, ba.insertDate as InsertDate, ba.date as Date, ba.userName as UserName, ba.entityType as EntityType, ba.entityID as EntityID, ba.changeType as ChangeType, ba.oldValue as OLDValue, ba.newValue as NewValuefrom BalanceAudit ba WITH(NOLOCK);
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "Date";
            sortOrder = " DESC ";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }

    private String queryFields() {
        StringBuilder sb = new StringBuilder();
        sb.append("ba.id as Id,");
        sb.append("ba.date as Date,");
        sb.append("ba.userName as UserName,");
        sb.append("rt.description as EntityType,");
        sb.append("ba.entityID as EntityID,");
        sb.append("ba.changeType as ChangeType, ");

        sb.append("(CASE WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.oldValue = 1 THEN 'Prepaid' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.oldValue = 2 THEN 'Credit' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.oldValue = 3 THEN 'Unlimited' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.oldValue = 4 THEN 'Shared' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.oldValue = 5 THEN 'Flexible' ");
        sb.append("WHEN ba.changeType = 'SHARED_INDIVIDUAL_CHANGE' AND ba.oldValue = 0 THEN 'Disabled' ");
        sb.append("WHEN ba.changeType = 'SHARED_INDIVIDUAL_CHANGE' AND ba.oldValue = 1 THEN 'Enabled'  ");
        sb.append("WHEN ba.changeType = 'MERCHANT_DISABLED' AND ba.oldValue = 0 THEN 'Enable' ");
        sb.append("WHEN ba.changeType = 'MERCHANT_DISABLED' AND ba.oldValue = 1 THEN 'Disabled'  ");
        sb.append("WHEN ba.changeType = 'MERCHANT_CANCELLED' AND ba.oldValue = 0 THEN 'Not Cancelled' ");
        sb.append("WHEN ba.changeType = 'MERCHANT_CANCELLED' AND ba.oldValue = 1 THEN 'Cancelled' ELSE convert(varchar(20),ba.oldValue) END) AS OLDValue, ");

        sb.append("(CASE WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.newValue = 1 THEN 'Prepaid' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.newValue = 2 THEN 'Credit' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.newValue = 3 THEN 'Unlimited' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.newValue = 4 THEN 'Shared' ");
        sb.append("WHEN ba.changeType = 'CREDITTYPE_CHANGE' AND ba.newValue = 5 THEN 'Flexible' ");
        sb.append("WHEN ba.changeType = 'SHARED_INDIVIDUAL_CHANGE' AND ba.newValue = 0 THEN 'Disabled' ");
        sb.append("WHEN ba.changeType = 'SHARED_INDIVIDUAL_CHANGE' AND ba.newValue = 1 THEN 'Enabled'  ");
        sb.append("WHEN ba.changeType = 'MERCHANT_DISABLED' AND ba.newValue = 0 THEN 'Enable' ");
        sb.append("WHEN ba.changeType = 'MERCHANT_DISABLED' AND ba.newValue = 1 THEN 'Disabled'  ");
        sb.append("WHEN ba.changeType = 'MERCHANT_CANCELLED' AND ba.newValue = 0 THEN 'Not Cancelled' ");
        sb.append("WHEN ba.changeType = 'MERCHANT_CANCELLED' AND ba.newValue = 1 THEN 'Cancelled' ELSE convert(varchar(20),ba.newValue) END) AS NewValue ");

        return sb.toString();
    }

    private String queryFrom() {
        StringBuilder sb = new StringBuilder()
                .append("FROM BalanceAudit ba WITH (NOLOCK) ")
                .append("INNER JOIN Rep_type rt on ba.entityType = rt.type_id ");
        return sb.toString();
    }

    private String queryAndWhere() {
        String query =" ";
        if(this.filterType.equals("USERNAME")){
            query = queryWhereUsername();
        } else if(this.filterType.equals("ENTITY_ID")){
            query = queryWhereEntityId();
        }else if(this.filterType.equals("CHANGE_TYPE")){
            query = queryWhereChangeType();
        }else if(this.filterType.equals("OLD_VALUE")){
            query = queryWhereOldValue();
        }
        else if(this.filterType.equals("NEW_VALUE")){
            query = queryWhereNewValue();
        }
        return query;
    }  
    
    private String queryWhereUsername() {
        StringBuilder sb = new StringBuilder()
        .append("AND ba.userName LIKE '%")
        .append(this.filterValue)
        .append("%'");        
        return sb.toString();
    }

    private String queryWhereEntityId() {
        if(!this.filterValue.equals("")){
            StringBuilder sb = new StringBuilder()
            .append("AND ba.entityID = ")
            .append(this.filterValue);
            return sb.toString();
        }
        return "";
    }

    private String queryWhereChangeType() {
        StringBuilder sb = new StringBuilder()
        .append("AND ba.changeType = '")
        .append(this.filterValue)
        .append("'");         
        return sb.toString();
    }

    private String queryWhereOldValue() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        if(!this.filterValue.equals("") ){
            sb.append("AND ba.oldValue ");
            sb.append("= ");
            sb.append(this.filterValue);        
        }
        return sb.toString();
    }

    private String queryWhereNewValue() {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        if(!this.filterValue.trim().equals("")){
            sb.append("AND ba.newValue = ");    
            sb.append(this.filterValue);
        }
        return sb.toString();    
    }

    private String queryWhere() {         
        return setDateQueryRanges(this.startDate, this.endDate, "ba.Date");
    }
    
    /**
     * 
     * @param stringToFormat format "dd/MM/yyyy  HH:mm:ss.S"
     * @return 
     */
    private Date convertToDate(String stringToFormat) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");         
        try {
            Date date = format.parse(stringToFormat);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 
     * @param date
     * @return 
     */
    private String convertToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");         
        String sCertDate = dateFormat.format(date);
        return sCertDate;
    }
    
    
    private String setDateQueryRanges(String startDate, String endDate, String field){
        startDate=startDate.trim().concat(" 00:000:00.000");
        endDate=endDate.trim().concat(" 23:59:59.999");
        String sDate=convertToString((Date)convertToDate(startDate));
        String eDate=convertToString((Date)convertToDate(endDate));
        
        StringBuilder sb = new StringBuilder()
                .append(" WHERE (").append(field).append(">='").append(sDate).append("' AND ").append(field).append("<='").append(eDate).append("') ");
        return sb.toString();
    }
   
}
