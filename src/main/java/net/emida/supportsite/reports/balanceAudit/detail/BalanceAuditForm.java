/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceAudit.detail;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author g.martinez
 */
public class BalanceAuditForm {
 
    private Date startDate;
    private Date endDate;
    private String filterValue;
    private String filterType;
    //private String filterValue;    
    

    /**
     * @return the startDate
     */
    @NotNull(message = "{com.debisys.reports.error1}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    @NotNull(message = "{com.debisys.reports.error3}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")    
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }
    

    @Override
    public String toString() {
        return "BalanceAuditModel[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", filterType=" + filterType +
                ", filterValue=" + filterValue +                
                ']';
    }

    /**
     * @return the filterType
     */
    public String getFilterType() {
        return filterType;
    }

    /**
     * @param textfilterType the filterType to set
     */
    public void setFilterType(String textfilterType) {
        this.filterType = textfilterType;
    }

}
