/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceAudit.detail;

import com.debisys.users.SessionData;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import net.emida.supportsite.reports.balanceAudit.detail.dao.BalanceAuditModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author g.martinez
 */
public interface IBalanceAuditDao {
 
    public List<BalanceAuditModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    public Long count(Map<String, Object> queryData) throws TechnicalException;
    
    Vector<Vector<String>> listChangeTypes(SessionData sessionData) throws TechnicalException;
    
    Vector<Vector<String>> listFilters(SessionData sessionData) throws TechnicalException;    
   
}
