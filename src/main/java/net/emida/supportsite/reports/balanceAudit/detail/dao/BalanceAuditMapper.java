package net.emida.supportsite.reports.balanceAudit.detail.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


/**
 *
 * @author g.martinez
 */
public class BalanceAuditMapper implements RowMapper<BalanceAuditModel>{

    @Override
    public BalanceAuditModel mapRow(ResultSet rs, int i) throws SQLException {
        BalanceAuditModel model = new BalanceAuditModel();
        model.setRwn(rs.getBigDecimal("rwn"));
        model.setId(rs.getString("id"));
        model.setDate(rs.getTimestamp("date"));
        model.setUserName(rs.getString("userName"));
        model.setEntityType(rs.getString("entityType"));
        model.setEntityId(rs.getBigDecimal("entityId"));
        model.setChangeType(rs.getString("changeType"));
        model.setOldValue(rs.getString("oldValue"));
        model.setNewValue(rs.getString("newValue"));        
        return model;
    }
    
}
