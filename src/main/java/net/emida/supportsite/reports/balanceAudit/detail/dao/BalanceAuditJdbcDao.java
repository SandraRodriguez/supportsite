/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceAudit.detail.dao;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import net.emida.supportsite.reports.balanceAudit.detail.BalanceAuditConstants;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import net.emida.supportsite.reports.balanceAudit.detail.IBalanceAuditDao;
import org.springframework.stereotype.Repository;

@Repository
public class BalanceAuditJdbcDao implements IBalanceAuditDao {

    private static final Logger log = LoggerFactory.getLogger(BalanceAuditJdbcDao.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll() {
        try {
            return 0L;
        } catch (DataAccessException ex) {
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<BalanceAuditModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {

        try {
            BalanceAuditQueryBuilder queryBuilder = new BalanceAuditQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();
            log.debug("Query : {}", SQL_QUERY);
            if (queryData != null) {
                queryData.clear();
            }
            queryData = null;
            return jdbcTemplate.query(SQL_QUERY, new BalanceAuditMapper());

        } catch (DataAccessException ex) {
            log.error("Data access error in list balance audit report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in list balance audit report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {
            BalanceAuditQueryBuilder queryBuilder = new BalanceAuditQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();
            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            if (queryData != null) {
                queryData.clear();
            }
            queryData = null;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count balance audit report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count balance audit report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Vector<Vector<String>> listChangeTypes(SessionData sessionData) {
        Vector<Vector<String>> vecMain = new Vector<Vector<String>>();
        Vector<String> vec = new Vector<String>();
        vec.add(BalanceAuditConstants.AUDIT_CREDITTYPE_CHANGE);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.creditType", sessionData.getLanguage()));

        vecMain.add(vec);
        vec = new Vector<String>();
        vec.add(BalanceAuditConstants.AUDIT_MERCHANT_CANCELLED);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.merchantCancelled", sessionData.getLanguage()));
        vecMain.add(vec);
        vec = new Vector<String>();
        vec.add(BalanceAuditConstants.AUDIT_MERCHANT_DISABLED);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.merchantDisabled", sessionData.getLanguage()));
        vecMain.add(vec);
        vec = new Vector<String>();
        vec.add(BalanceAuditConstants.AUDIT_PARENT_CHANGE);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.parentChange", sessionData.getLanguage()));
        vecMain.add(vec);
        vec = new Vector<String>();
        vec.add(BalanceAuditConstants.AUDIT_SHARED_INDIVIDUAL_CHANGE);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.SharedIndividualChange", sessionData.getLanguage()));
        vecMain.add(vec);
        return vecMain;
    }

    @Override
    public Vector<Vector<String>> listFilters(SessionData sessionData) throws TechnicalException {
        Vector<Vector<String>> vecMain = new Vector<Vector<String>>();
        Vector<String> vec = new Vector<String>();
        vec.add(BalanceAuditConstants.USERNAME);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.userName", sessionData.getLanguage()));
        vecMain.add(vec);
        vec = new Vector<String>();
        vec.add(BalanceAuditConstants.ENTITY_ID);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.entityID", sessionData.getLanguage()));
        vecMain.add(vec);
        vec = new Vector<String>();
        vec.add(BalanceAuditConstants.CHANGE_TYPE);
        vec.add(Languages.getString("jsp.admin.reports.balanceAudit.filter.type", sessionData.getLanguage()));
        vecMain.add(vec);
        /*vec = new Vector<String>();
         vec.add(BalanceAuditConstants.OLD_VALUE);
         vec.add("Old value");
         vecMain.add(vec);
         vec = new Vector<String>();
         vec.add(BalanceAuditConstants.NEW_VALUE);
         vec.add("New value");
         vecMain.add(vec);*/
        return vecMain;
    }

}
