/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceAudit.detail.dao;

import com.debisys.utils.DateUtil;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author g.martinez
 */
public class BalanceAuditModel implements CsvRow{
    
    private BigDecimal rwn;
    private String id;
    private Timestamp date;
    private String userName;
    private String entityType;
    private BigDecimal entityId;
    private String changeType;
    private String oldValue;
    private String newValue;
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return DateUtil.formatDateTime_DB(date.toString());
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public BigDecimal getEntityId() {
        return entityId;
    }

    public void setEntityId(BigDecimal entityId) {
        this.entityId = entityId;
    }
    
    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Override
    public List<CsvCell> cells() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cella = new CsvCell(String.valueOf(rwn));
        list.add(cella);
        String formattedDate = date != null ? sdf.format(date) : "";
        CsvCell cell0 = new CsvCell(formattedDate);
        list.add(cell0);
        CsvCell cell1 = new CsvCell(userName);
        list.add(cell1);
        CsvCell cell2 = new CsvCell(String.valueOf(entityType));
        list.add(cell2);
        CsvCell cell3 = new CsvCell(entityId.toString());
        list.add(cell3);
        CsvCell cell4 = new CsvCell(changeType);
        list.add(cell4);
        CsvCell cell5 = new CsvCell(oldValue.toString());
        list.add(cell5);
        CsvCell cell6 = new CsvCell(newValue.toString());
        list.add(cell6);
               
        return list;
    }

    /**
     * @return the rwn
     */
    public BigDecimal getRwn() {
        return rwn;
    }

    /**
     * @param rwn the rwn to set
     */
    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }
}
