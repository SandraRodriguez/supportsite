/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.internalReport.transactionErr;

import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author janez - janez@emida.net
 * @since 22/05/2017
 * @version 1.0
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class TransactionErrController extends BaseController {

    /**
     *
     * @param model
     * @param m
     * @return
     */
    @RequestMapping(value = "/internalReport/transactionErr/transactionErr", method = RequestMethod.GET)
    public String doTransationControllerInit(ModelMap model, Model m) {
        model.addAttribute("filter", "errors");        
        String urlRedirect = "/reports/internalReport/transactionErr/transactionErr";
        return urlRedirect;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
