package net.emida.supportsite.reports.spiff.processHistory;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author fernandob
 */
public class SpiffProcessHistoryReportForm {

    private String pin;


    @NotNull(message = "{NotNull.spiffProcessHistoryReportForm.pin}")
    @Size(min=1, message = "{Size.spiffProcessHistoryReportForm.pin}") 
    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }



}
