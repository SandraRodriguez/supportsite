package net.emida.supportsite.reports.spiff.deatil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.debisys.customers.Merchant;
import com.debisys.exceptions.CustomerException;

import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.spiff.deatil.dao.ActivationDetailReportModel;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;

@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class ActivationDetailController extends BaseController {

	private static final Logger log = LoggerFactory.getLogger(ActivationDetailController.class);

	@Autowired
	private ActivationDetailDao activationDetailDao;

	@Autowired
	@Qualifier("activationDetailReportCsvBuilder")
	private CsvBuilderService csvBuilderService;

	@RequestMapping(value = UrlPaths.ACTIVATION_DETAIL_REPORT_PATH, method = RequestMethod.GET)
	public String reportForm(Map model, @ModelAttribute SpiffDetailReportForm reportModel) {

		fillMerchants(model);

		return "reports/spiff/activationDetailReportForm";
	}

	@RequestMapping(value = UrlPaths.ACTIVATION_DETAIL_REPORT_PATH, method = RequestMethod.POST)
	public String reportBuild(Model model, @Valid SpiffDetailReportForm reportModel, BindingResult result)
			throws TechnicalException, ApplicationException {

		log.debug("reportBuil {}", result);
		log.debug("ReportModel ={}", reportModel);
		log.debug("ReportModel allIsos ={}", reportModel.isFilterAllIsos());
		log.debug("HasError={}", result.hasErrors());
		log.debug("{}", result.getAllErrors());
		log.debug("IsAll = {}", isAlls(reportModel.getPids()));

		if (result.hasErrors()) {
			model.addAttribute("reportModel", reportModel);
			fillMerchants(model.asMap());
			return "reports/spiff/activationDetailReportForm";
		}

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String formattedStarDate = sdf.format(reportModel.getStartDate());
		String formattedEndDate = sdf.format(reportModel.getEndDate());

		model.addAttribute("startDate", formattedStarDate);
		model.addAttribute("endDate", formattedEndDate);
		model.addAttribute("mids", reportModel.toCsvMerchantIds());
		model.addAttribute("pids", isAlls(reportModel.getPids()) ? "" : reportModel.toCsvProviderIds());
		model.addAttribute("filterAllIsos", reportModel.isFilterAllIsos());

		Map<String, Object> queryData = fillQueryData(formattedStarDate, formattedEndDate,
				reportModel.toCsvMerchantIds(),
				reportModel.getPnumber() != null ? reportModel.getPnumber().toString() : "", reportModel.getSim(),
				isAlls(reportModel.getPids()) ? "" : reportModel.toCsvProviderIds(), reportModel.isFilterAllIsos());

		Long totalRecords = activationDetailDao.count(queryData);
		model.addAttribute("totalRecords", totalRecords);
		return "reports/spiff/activationDetailReportResult";
	}

	@RequestMapping(method = RequestMethod.GET, value = UrlPaths.ACTIVATION_DETAIL_REPORT_GRID_PATH, produces = "application/json")
	@ResponseBody
	public List<ActivationDetailReportModel> listGrid(@RequestParam(value = "start") String rowNumberStart,
			@RequestParam String sortField, @RequestParam String sortOrder, @RequestParam String rows,
			@RequestParam String sim, @RequestParam String pnumber, @RequestParam String startDate,
			@RequestParam String endDate, @RequestParam String mids, @RequestParam String pids,
			@RequestParam String filterAllIsos) {

		log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", rowNumberStart, sortField, sortOrder, rows);
		log.debug("Listing grid. StartDate={}. EndDate={}. Sim={}. Pnumber={}. Mids={}. Pids={}", startDate, endDate,
				sim, pnumber, mids, pids);

		int rowsPerPage = Integer.parseInt(rows);
		int rowNumberStartInt = Integer.parseInt(rowNumberStart);

		Map<String, Object> queryData = fillQueryData(startDate, endDate, mids, pnumber, sim, pids, filterAllIsos);

		List<ActivationDetailReportModel> list = activationDetailDao.list(rowNumberStartInt, rowsPerPage, sortField,
				sortOrder, queryData);

		return list;
	}

	@RequestMapping(method = RequestMethod.GET, value = UrlPaths.ACTIVATION_DETAIL_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String sim,
			@RequestParam String pnumber, @RequestParam String startDate, @RequestParam String endDate,
			@RequestParam String mids, @RequestParam String pids, @RequestParam Integer totalRecords,
			@RequestParam String sortField, @RequestParam String sortOrder, @RequestParam String filterAllIsos)
			throws IOException {

		log.info("Downloading file...");
		log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
		log.debug("Download file. StartDate={}. EndDate={}. Sim={}. Pnumber={}. Mids={}. Pids={}", startDate, endDate,
				sim, pnumber, mids, pids);

		Map<String, Object> queryData = fillQueryData(startDate, endDate, mids, pnumber, sim, pids, filterAllIsos);
		List<ActivationDetailReportModel> list = activationDetailDao.listForDownloadFile(1, totalRecords, sortField,
				sortOrder, queryData);
		log.debug("Total list={}", list.size());
		String randomFileName = generateRandomFileName();
		String filePath = generateDestinationFilePath(randomFileName, ".csv");
		log.info("Generated path = {}", filePath);

		Map<String, Object> additionalData = new HashMap<String, Object>();
		additionalData.put("language", getSessionData().getLanguage());
		additionalData.put("accessLevel", getAccessLevel());
		additionalData.put("distChainType", getDistChainType());
		additionalData.put("isIntranetUser", isIntranetUser());
		additionalData.put("filterAllIsos", filterAllIsos);

		csvBuilderService.buildCsv(filePath, list, additionalData);
		log.debug("File csv has been built!");

		FileSystemResource csvFileResource = new FileSystemResource(filePath);
		log.debug("Generating zip file...");
		String generateZipFile = generateZipFile(randomFileName, csvFileResource);
		log.info("Zip file ={}", generateZipFile);
		FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

		// Delete csv file
		csvFileResource.getFile().delete();

		response.setContentType("application/zip");
		response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

		return zipFileResource;

	}

	private void fillMerchants(Map model) {
		try {
			// List<Map<String, Object>> providerList = spiffDetailDao.listSpiffProviders();
			List<Map<String, Object>> providerList = activationDetailDao.listSpiffProviders(model);
			Vector<Vector<String>> merchantList = Merchant.getMerchantListReports(getSessionData());
			model.put("merchantList", merchantList);
			model.put("providerList", providerList);
		} catch (CustomerException e) {
			log.error("Error listing merchants. Ex = {}", e);
			throw new TechnicalException("Error listing merchants", e);
		}
	}

	private Map<String, Object> fillQueryData(String startDate, String endDate, String merchantIds, String paymentRef,
			String sim, String providerIds, String filterAllIsos) {
		Map<String, Object> queryData = new HashMap<String, Object>();
		queryData.put("accessLevel", getAccessLevel());
		queryData.put("distChainType", getDistChainType());
		queryData.put("refId", getRefId());
		queryData.put("startDate", startDate);
		queryData.put("endDate", endDate);
		queryData.put("merchantIds", merchantIds);
		queryData.put("paymentReference", paymentRef);
		queryData.put("sim", sim);
		queryData.put("providerIds", providerIds);
		queryData.put("filterAllIsos", filterAllIsos);
		return queryData;
	}

	private boolean isAlls(String[] pids) {
		if (pids != null && pids.length > 0 && pids[0].trim().length() > 0) {
			return false;
		}
		return true;
	}

	@Override
	public int getSection() {
		return 4;
	}

	@Override
	public int getSectionPage() {
		return 14;
	}

}
