package net.emida.supportsite.reports.spiff.deatil.dao;

import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportModel;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Franky Villadiego
 */
public class SpiffDetailReportMapper implements RowMapper<SpiffDetailReportModel>{


    @Override
    public SpiffDetailReportModel mapRow(ResultSet rs, int i) throws SQLException {

        SpiffDetailReportModel model = new SpiffDetailReportModel();

        model.setRowNum(rs.getLong("RWN"));
        model.setRecId(rs.getLong("RecID"));
        model.setActivationTrxId(rs.getLong("ActivationTrxId"));
        model.setMerchantId(rs.getLong("MerchantID"));
        model.setMerchantDba(rs.getString("MerchantDBA"));
        model.setProductName(rs.getString("ProductName"));
        model.setSpiffType(rs.getString("SpiffType"));
        model.setRepName(rs.getString("RepName"));
        model.setCommissionType(rs.getString("CommissionType"));
        model.setIsoRate(rs.getBigDecimal("IsoRate"));
        model.setAgentRate(rs.getBigDecimal("AgentRate"));
        model.setSubAgentRate(rs.getBigDecimal("SubAgentRate"));
        model.setRepRate(rs.getBigDecimal("RepRate"));
        model.setMerchantRate(rs.getBigDecimal("MerchantRate"));

        Long blRecId = rs.getLong("BlRecId");
        Long cptRecId = rs.getLong("CptRecId");

        if(blRecId != null && blRecId != 0){
            model.setPaymentType("ACH");
            model.setPaymentDate(rs.getDate("BlTransmitDate"));
            model.setPaymentReference(rs.getString("BlTraceNumber"));
        }else if (cptRecId != null && cptRecId != 0){
            model.setPaymentType("CREDIT");
            model.setPaymentDate(rs.getDate("McLogDatetime"));
            model.setPaymentReference(rs.getString("McId"));
        }else{
            model.setPaymentType("");
        }

        model.setPin(rs.getString("PIN"));
        model.setEsn(rs.getString("ESN"));
        model.setSim(rs.getString("SIM"));
        model.setActivationDate(rs.getDate("ActivationDate"));
        model.setActivationProduct(rs.getString("ActivationProduct"));

        BigDecimal merchantCommissionAmount = rs.getBigDecimal("MerchantCommissionAmount");
        if(merchantCommissionAmount != null && merchantCommissionAmount.doubleValue() < 0) {
            //merchantCommissionAmount = merchantCommissionAmount.multiply(new BigDecimal(-1));
        }
        model.setMerchantCommissionAmount(merchantCommissionAmount);

        BigDecimal repCommissionAmount = rs.getBigDecimal("RepCommissionAmount");
        if(repCommissionAmount != null && repCommissionAmount.doubleValue() < 0) {
            //repCommissionAmount = repCommissionAmount.multiply(new BigDecimal(-1));
        }
        model.setRepCommissionAmount(repCommissionAmount);

        BigDecimal subAgentCommissionAmount = rs.getBigDecimal("SubAgentCommissionAmount");
        if(subAgentCommissionAmount != null && subAgentCommissionAmount.doubleValue() < 0) {
            //subAgentCommissionAmount = subAgentCommissionAmount.multiply(new BigDecimal(-1));
        }
        model.setSubAgentCommissionAmount(subAgentCommissionAmount);

        BigDecimal agentCommissionAmount = rs.getBigDecimal("AgentCommissionAmount");
        if(agentCommissionAmount != null && agentCommissionAmount.doubleValue() < 0) {
            //agentCommissionAmount = agentCommissionAmount.multiply(new BigDecimal(-1));
        }
        model.setAgentCommissionAmount(agentCommissionAmount);

        BigDecimal isoCommissionAmount = rs.getBigDecimal("IsoCommissionAmount");
        if(isoCommissionAmount != null && isoCommissionAmount.doubleValue() < 0) {
            //isoCommissionAmount = isoCommissionAmount.multiply(new BigDecimal(-1));
        }
        model.setIsoCommissionAmount(isoCommissionAmount);

        model.setRepName(rs.getString("RepName"));
        model.setSubAgentName(rs.getString("SubAgentName"));
        model.setAgentName(rs.getString("AgentName"));
        model.setProviderName(rs.getString("ProviderName"));

        return model;
    }
}
