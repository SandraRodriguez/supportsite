package net.emida.supportsite.reports.spiff.processHistory.dao;

/**
 *
 * @author fernandob
 */
public class SpiffProcessHistoryReportConstants {

    public static final String ROW_NUMBER = "RWN";

    // Spiff transactions recordset related column names    
    public static final String TRX_DATE = "trxDate";
    public static final String PRODUCT_ID = "productId";
    public static final String PRODUCT_TRANS_TYPE = "productTransType";
    public static final String BILLING_TRANS_TYPE = "billingTransactionType";
    public static final String TRX_ID = "trxId";
    public static final String RELATED_TRX_ID = "relatedTrxId";
    public static final String COTROL_NO = "controlNo";
    public static final String AMOUNT = "amount";
    public static final String REP_ID = "rep";
    public static final String MERCHANT_ID = "merchant";
    public static final String TERMINAL_ID = "terminal";
    public static final String PAID_BY = "paidBy";

    public static final String PAID_BY_JOB = "PAID_BY_JOB";
    public static final String PAID_BY_BILLNG = "PAID_BY_BILLNG";

    // Spiff transactions recordset related column names
    public static final String EMIDA_TRX_ID = "emidaTrxId";
    public static final String RECORD_DATE = "datetime";
    public static final String FILE_DATE = "fileDate";
    public static final String PROCESS_DATE = "processDate";
    public static final String PROCESS_RESULT_CODE = "processResult";
    public static final String TRACFONE_REASON = "tracfoneRejectReason";

}
