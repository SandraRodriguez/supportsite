package net.emida.supportsite.reports.spiff.deatil.dao;

import com.debisys.utils.StringUtil;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ActivationDetailReportMapper implements RowMapper<ActivationDetailReportModel> {

    @Override
    public ActivationDetailReportModel mapRow(ResultSet rs, int rowNum) throws SQLException {

        ActivationDetailReportModel model = new ActivationDetailReportModel();

        model.setRowNum(new Long(rowNum));
        model.setActivationTrxId(rs.getLong("ActivationTrxId"));
        model.setRecId(rs.getString("RecID"));
        model.setMerchantId(rs.getLong("MerchantID"));
        model.setMerchantDba(rs.getString("MerchantDBA"));
        model.setProductName(rs.getString("ProductName"));
        model.setSpiffType(rs.getString("SpiffType"));
        model.setCommissionType(rs.getString("CommissionType"));
        model.setSpiffAmount(rs.getString("SpiffAmount"));
        model.setActivationAmount(rs.getString("ActivationAmount"));
        model.setSim(rs.getString("SIM"));
        model.setEsn(rs.getString("ESN"));
        model.setPin(rs.getString("PIN"));
        model.setActivationDate(rs.getDate("ActivationDate"));
        model.setActivationProduct(rs.getString("ActivationProduct"));
        model.setRepName(rs.getString("RepName"));
        model.setSubAgentName(rs.getString("SubAgentName"));
        model.setAgentName(rs.getString("AgentName"));
        model.setCarName(rs.getString("CarName"));
        model.setIsoName(rs.getString("IsoName"));
        model.setDealerCode((StringUtil.checkStr(rs.getString("DealerCode"))) ? rs.getString("DealerCode") : "");
        if (rs.getInt("MultiLineId")==0)
            model.setMultiLine("NA");
        else
            model.setMultiLine(rs.getString("MultiLineId"));
        return model;
    }

}
