package net.emida.supportsite.reports.spiff.processHistory.dao;

import com.debisys.languages.Languages;
import java.util.Date;
import java.sql.ResultSet;
import java.math.BigDecimal;
import java.sql.SQLException;
import net.emida.supportsite.util.csv.CsvRow;
import org.springframework.jdbc.core.RowMapper;
import static com.debisys.utils.DbUtil.hasColumn;
import static net.emida.supportsite.reports.spiff.processHistory.dao.SpiffProcessHistoryReportConstants.*;

/**
 *
 * @author fernandob
 */
public class SpiffProcessHistoryReportMapperTrx implements RowMapper<CsvRow> {

    @Override
    public CsvRow mapRow(ResultSet rs, int i) throws SQLException {
        SpiffProcessHistoryReportModelTrx newRecord = new SpiffProcessHistoryReportModelTrx();
        if (hasColumn(rs, ROW_NUMBER)) {
            newRecord.setRowNum((rs.getString(ROW_NUMBER) == null) ? 0L : rs.getLong(ROW_NUMBER));
        }
        if (hasColumn(rs, TRX_DATE)) {
            newRecord.setTrxDate((rs.getString(TRX_DATE) == null) ? (new Date()) : new java.util.Date(rs.getTimestamp(TRX_DATE).getTime()));
        }
        if (hasColumn(rs, PRODUCT_ID)) {
            newRecord.setProductId((rs.getString(PRODUCT_ID) == null) ? 0L : rs.getLong(PRODUCT_ID));
        }
        if (hasColumn(rs, PRODUCT_TRANS_TYPE)) {
            newRecord.setProductTransType((rs.getString(PRODUCT_TRANS_TYPE) == null) ? "" : rs.getString(PRODUCT_TRANS_TYPE));
        }
        if (hasColumn(rs, BILLING_TRANS_TYPE)) {
            newRecord.setBillingTransactionType((rs.getString(BILLING_TRANS_TYPE) == null) ? "" : rs.getString(BILLING_TRANS_TYPE));
        }
        if (hasColumn(rs, TRX_ID)) {
            newRecord.setTrxId((rs.getString(TRX_ID) == null) ? 0L : rs.getLong(TRX_ID));
        }
        if (hasColumn(rs, RELATED_TRX_ID)) {
            newRecord.setRelatedTrxId((rs.getString(RELATED_TRX_ID) == null) ? 0L : rs.getLong(RELATED_TRX_ID));
        }
        if (hasColumn(rs, COTROL_NO)) {
            newRecord.setControlNo((rs.getString(COTROL_NO) == null) ? 0L : rs.getLong(COTROL_NO));
        }
        if (hasColumn(rs, AMOUNT)) {
            newRecord.setAmount((rs.getString(AMOUNT) == null) ? BigDecimal.ZERO : rs.getBigDecimal(AMOUNT));
        }
        if (hasColumn(rs, REP_ID)) {
            newRecord.setRep((rs.getString(REP_ID) == null) ? 0L : rs.getLong(REP_ID));
        }
        if (hasColumn(rs, MERCHANT_ID)) {
            newRecord.setMerchant((rs.getString(MERCHANT_ID) == null) ? 0L : rs.getLong(MERCHANT_ID));
        }
        if (hasColumn(rs, TERMINAL_ID)) {
            newRecord.setTerminal((rs.getString(TERMINAL_ID) == null) ? 0L : rs.getLong(TERMINAL_ID));
        }
        if (hasColumn(rs, PAID_BY)) {
            newRecord.setPaidBy((rs.getString(PAID_BY) == null) ? "" : rs.getString(PAID_BY));
        }

        return newRecord;
    }

}
