package net.emida.supportsite.reports.spiff.processHistory.dao;

import java.util.Map;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataAccessException;
import net.emida.supportsite.reports.base.IDetailDao;
import net.emida.supportsite.util.csv.CsvRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import net.emida.supportsite.util.exceptions.TechnicalException;
/**
 *
 * @author fernandob
 */
@Repository
public class SpiffProcessHistoryReportJdbcDaoTrx implements IDetailDao<CsvRow> {

    private static final Logger logger = LoggerFactory.getLogger(SpiffProcessHistoryReportJdbcDaoTrx.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<CsvRow> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {
        try {
            SpiffProcessHistoryReportQueryBuilder queryBuilder = new SpiffProcessHistoryReportQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.generateSpiffTrxSqlString();

            logger.debug("Query : {}", SQL_QUERY);

            return jdbcTemplate.query(SQL_QUERY, new SpiffProcessHistoryReportMapperTrx());
        } catch (DataAccessException ex) {
            logger.error("Data access error in list spiff process history report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            logger.error("Exception in list spiff process history report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {
            SpiffProcessHistoryReportQueryBuilder queryBuilder = new SpiffProcessHistoryReportQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.generateTrxCountQuery();

            logger.debug("Query Count : {}", SQL_QUERY_COUNT);

            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            logger.error("Data access error in count spiff process history report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            logger.error("Exception in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }


}
