package net.emida.supportsite.reports.spiff.processHistory.dao;

import com.debisys.utils.NumberUtil;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author fernandob
 */
public class SpiffProcessHistoryReportModelTrx implements CsvRow {

    private Long rowNum;
    private Date trxDate;
    private Long productId;
    private String productTransType;
    private String billingTransactionType;
    private Long trxId;
    private Long relatedTrxId;
    private Long controlNo;
    private BigDecimal amount;
    private Long rep;
    private Long merchant;
    private Long terminal;
    private String paidBy;

    public long getRowNum() {
        return rowNum;
    }

    public void setRowNum(long rowNum) {
        this.rowNum = rowNum;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductTransType() {
        return productTransType;
    }

    public void setProductTransType(String productTransType) {
        this.productTransType = productTransType;
    }

    public String getBillingTransactionType() {
        return billingTransactionType;
    }

    public void setBillingTransactionType(String billingTransactionType) {
        this.billingTransactionType = billingTransactionType;
    }

    public long getTrxId() {
        return trxId;
    }

    public void setTrxId(long trxId) {
        this.trxId = trxId;
    }

    public long getRelatedTrxId() {
        return relatedTrxId;
    }

    public void setRelatedTrxId(long relatedTrxId) {
        this.relatedTrxId = relatedTrxId;
    }

    public long getControlNo() {
        return controlNo;
    }

    public void setControlNo(long controlNo) {
        this.controlNo = controlNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public long getRep() {
        return rep;
    }

    public void setRep(long rep) {
        this.rep = rep;
    }

    public long getMerchant() {
        return merchant;
    }

    public void setMerchant(long merchant) {
        this.merchant = merchant;
    }

    public long getTerminal() {
        return terminal;
    }

    public void setTerminal(long terminal) {
        this.terminal = terminal;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }
    
    

    public SpiffProcessHistoryReportModelTrx() {
        this.rowNum = 0L;
        this.trxDate = new Date();
        this.productId = 0L;
        this.productTransType = "";
        this.billingTransactionType = "";
        this.trxId = 0L;
        this.relatedTrxId = 0L;
        this.controlNo = 0L;
        this.amount = BigDecimal.ZERO;
        this.rep = 0L;
        this.merchant = 0L;
        this.terminal = 0L;
        this.paidBy = "";
    }

    public SpiffProcessHistoryReportModelTrx(long rowNum, Date trxDate,
            long productId, String productTransType, String billingTransactionType,
            long trxId, long relatedTrxId, long controlNo, BigDecimal amount,
            long rep, long merchant, long terminal, String paidBy) {
        this.rowNum = rowNum;
        this.trxDate = trxDate;
        this.productId = productId;
        this.productTransType = productTransType;
        this.billingTransactionType = billingTransactionType;
        this.trxId = trxId;
        this.relatedTrxId = relatedTrxId;
        this.controlNo = controlNo;
        this.amount = amount;
        this.rep = rep;
        this.merchant = merchant;
        this.terminal = terminal;
        this.paidBy = paidBy;
    }

    @Override
    public String toString() {
        return "SpiffProcessHistoryReportModelTrx{" + "rowNum=" + rowNum + ", trxDate=" + trxDate + ", productId=" + productId + ", productTransType=" + productTransType + ", billingTransactionType=" + billingTransactionType + ", trxId=" + trxId + ", relatedTrxId=" + relatedTrxId + ", controlNo=" + controlNo + ", amount=" + amount + ", rep=" + rep + ", merchant=" + merchant + ", terminal=" + terminal + ", paidBy=" + paidBy + '}';
    }



    @Override
    public List<CsvCell> cells() {
        List<CsvCell> retValue = new ArrayList<CsvCell>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Row number
        CsvCell cell = new CsvCell(this.rowNum.toString());
        retValue.add(cell);
        // Transaction date
        String formattedTrxDate = this.trxDate != null ? sdf.format(this.trxDate) : "";
        cell = new CsvCell(formattedTrxDate);
        retValue.add(cell);
        // Product id
        cell = new CsvCell(this.productId.toString());
        retValue.add(cell);
        // Product transaction type
        cell = new CsvCell(this.productTransType);
        retValue.add(cell);
        // Billing transaction type
        cell = new CsvCell(this.billingTransactionType);
        retValue.add(cell);
        // Transaction id
        cell = new CsvCell(this.trxId.toString());
        retValue.add(cell);
        // Related transaction id
        cell = new CsvCell(this.relatedTrxId.toString());
        retValue.add(cell);
        // Control number
        cell = new CsvCell(this.controlNo.toString());
        retValue.add(cell);
        // Transaction amount
        cell = new CsvCell(NumberUtil.formatCurrency(this.amount.toString()));
        retValue.add(cell);
        // Rep id
        cell = new CsvCell(this.rep.toString());
        retValue.add(cell);
        // Merchant id
        cell = new CsvCell(this.merchant.toString());
        retValue.add(cell);
        // Terminal id (Millennium_no)
        cell = new CsvCell(this.terminal.toString());
        retValue.add(cell);
        // Paid by [Commissions job OR Billing]
        cell = new CsvCell(this.paidBy);
        retValue.add(cell);
        return retValue;
    }

}
