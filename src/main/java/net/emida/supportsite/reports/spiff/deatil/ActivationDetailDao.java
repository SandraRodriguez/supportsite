package net.emida.supportsite.reports.spiff.deatil;

import java.util.List;
import java.util.Map;

import net.emida.supportsite.reports.spiff.deatil.dao.ActivationDetailReportModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

	
public interface ActivationDetailDao {
	
	
	   List<ActivationDetailReportModel> list(Integer rowNumberStartInt, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;
	    
	    public List<ActivationDetailReportModel> listForDownloadFile(Integer rowNumberStartInt, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

	    Long count(Map<String, Object> queryData) throws TechnicalException;

	    Long countAll() throws TechnicalException;

	    List<Map<String, Object>> listSpiffProviders() throws TechnicalException;
	    
	    List<Map<String, Object>> listSpiffProviders(Map model) throws TechnicalException;

}
