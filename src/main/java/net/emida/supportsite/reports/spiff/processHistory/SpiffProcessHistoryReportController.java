package net.emida.supportsite.reports.spiff.processHistory;

import java.util.Map;
import java.util.List;
import org.slf4j.Logger;
import java.util.HashMap;
import java.io.IOException;
import java.util.ArrayList;
import javax.validation.Valid;
import org.slf4j.LoggerFactory;
import java.text.SimpleDateFormat;
import org.springframework.ui.Model;
import org.springframework.http.MediaType;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.util.csv.CsvRow;
import javax.servlet.http.HttpServletResponse;
import net.emida.supportsite.mvc.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.core.io.FileSystemResource;
import net.emida.supportsite.util.csv.CsvBuilderService;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.reports.spiff.processHistory.dao.SpiffProcessHistoryReportJdbcDaoTrx;
import net.emida.supportsite.reports.spiff.processHistory.dao.SpiffProcessHistoryReportJdbcDaoRec;
/**
 *
 * @author fernandob
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class SpiffProcessHistoryReportController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(SpiffProcessHistoryReportController.class);

    private static final String FORM_PATH = "reports/spiff/processHistory/spiffProcessHistoryReportForm";
    private static final String RESULTS_PATH = "reports/spiff/processHistory/spiffProcessHistoryReportResult";

    @Autowired
    private SpiffProcessHistoryReportJdbcDaoTrx detailTrxDao;

    @Autowired
    private SpiffProcessHistoryReportJdbcDaoRec detailRecDao;

    @Autowired
    @Qualifier("spiffProcessHistoryReportCsvBuilder")
    private CsvBuilderService csvBuilderService;

    private Map<String, Object> fillQueryData(String pin) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("pin", pin);
        return queryData;
    }


    @RequestMapping(value = UrlPaths.SPIFF_PROCESSING_HISTORY_REPORT_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute SpiffProcessHistoryReportForm reportModel) {
        return FORM_PATH;
    }

    @RequestMapping(value = UrlPaths.SPIFF_PROCESSING_HISTORY_REPORT_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid SpiffProcessHistoryReportForm reportModel, BindingResult result) throws TechnicalException, ApplicationException {

        log.debug("reportBuil {}", result);

        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            return FORM_PATH;
        }

        model.addAttribute("pin", reportModel.getPin());

        Map<String, Object> queryData = this.fillQueryData(reportModel.getPin());

        Long totalTrxRecords = this.detailTrxDao.count(queryData);
        model.addAttribute("totalTrxRecords", totalTrxRecords);
        Long totalRecRecords = this.detailRecDao.count(queryData);
        model.addAttribute("totalRecRecords", totalRecRecords);
        return RESULTS_PATH;
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.SPIFF_PROCESSING_HISTORY_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public Map<String, List<CsvRow>> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String pin) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. Pin={}. ", pin);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = this.fillQueryData(pin);

        List<CsvRow> trxList =  this.detailTrxDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);
        List<CsvRow> recList =  this.detailRecDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);
        
        Map<String, List<CsvRow>> retValue = new HashMap<String, List<CsvRow>>();
        retValue.put("trxList", trxList);
        retValue.put("recList", recList);

        return retValue;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.SPIFF_PROCESSING_HISTORY_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String pin, 
            @RequestParam Integer totalTrxRecords, @RequestParam Integer totalRecRecords, 
            @RequestParam String sortField, @RequestParam String sortOrder) throws IOException {

        log.info("Downloading file...");
        log.debug("Download file. Field={}. Order={}. TotalTrxRecords={}. TotalRecRecords={}", sortField, sortOrder, totalTrxRecords, totalRecRecords);
        log.debug("Download file. Pin={}", pin);

        Map<String, Object> queryData = fillQueryData(pin);
        
        List<CsvRow> trxList =  this.detailTrxDao.list(1, totalTrxRecords, sortField, sortOrder, queryData);
        log.debug("Total transaction list={}", trxList.size());
        List<CsvRow> recList =  this.detailRecDao.list(1, totalRecRecords, sortField, sortOrder, queryData);
        log.debug("Total spiff history list={}", recList.size());
        List<List<CsvRow>> reportDetail = new ArrayList<List<CsvRow>>();
        reportDetail.add(trxList);
        reportDetail.add(recList);
        
        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.info("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());
        ((SpiffProcessHistoryReportCsvBuilder)this.csvBuilderService).setPin(pin);
        csvBuilderService.buildCsv(filePath, reportDetail, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.info("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();

        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

        return zipFileResource;

    }
    

    @Override
    public int getSection() {
        return 9;
    }

    @Override
    public int getSectionPage() {
        return 9;
    }

}
