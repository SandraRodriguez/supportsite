package net.emida.supportsite.reports.spiff.deatil.dao;

import com.debisys.utils.DebisysConstants;
import org.apache.commons.lang.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Vector;

/**
 * @author Franky Villadiego
 */
public class SpiffDetailQueryBuilder {

    private static final Logger log = LoggerFactory.getLogger(SpiffDetailQueryBuilder.class);

    int startPage;
    int endPage;
    String sortFieldName;
    String sortOrder;

    String accessLevel;
    String distChainType;
    String refId;
    String startDate;
    String endDate;
    String csvMerchantIds;
    String paymentReference;
    String sim;
    String csvProviderIds;



    public SpiffDetailQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.accessLevel = (String)queryData.get("accessLevel");
        this.distChainType = (String)queryData.get("distChainType");
        this.refId = (String)queryData.get("refId");
        this.startDate = (String)queryData.get("startDate");
        this.endDate = (String)queryData.get("endDate");
        this.csvMerchantIds = (String)queryData.get("merchantIds");
        this.paymentReference = (String)queryData.get("paymentReference");
        this.sim = (String)queryData.get("sim");
        this.csvProviderIds = (String)queryData.get("providerIds");
    }

    public SpiffDetailQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";

        this.accessLevel = (String)queryData.get("accessLevel");
        this.distChainType = (String)queryData.get("distChainType");
        this.refId = (String)queryData.get("refId");
        this.startDate = (String)queryData.get("startDate");
        this.endDate = (String)queryData.get("endDate");
        this.csvMerchantIds = (String)queryData.get("merchantIds");
        this.paymentReference = (String)queryData.get("paymentReference");
        this.sim = (String)queryData.get("sim");
        this.csvProviderIds = (String)queryData.get("providerIds");
    }


    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");

        return sb0.toString();
    }

    public String buildQuery() throws Exception {

        if(sortFieldName == null || sortFieldName.isEmpty()){
            sortFieldName = "MerchantId";
            sortOrder = "ASC";
        }

        if("paymentDate".equalsIgnoreCase(sortFieldName.trim())){
            sortFieldName = "BlTransmitDate,McLogDatetime";
        }

        if("paymentReference".equalsIgnoreCase(sortFieldName.trim())){
            sortFieldName = "BlTraceNumber,McId";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + " " + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();

    }

    private String mainQuery() throws Exception {

        String merchantIDsWhere = buildMerchantIDsWhere(csvMerchantIds, accessLevel, distChainType, refId);
        String datesWhere = buildDatesRangeWhere(accessLevel, refId, "spTrx.datetime", startDate, endDate);

        StringBuilder mainQuery = new StringBuilder();

        //SELECT
        mainQuery.append("SELECT ").append(queryOneFields());

        //FROM
        mainQuery.append(queryOneFrom());

        //WHERE
        mainQuery.append(queryOneBasicWhere());                 // 1 = 1
        mainQuery.append(merchantIDsWhere);                     //
        mainQuery.append(datesWhere);                           //
        mainQuery.append(queryOneWhereProviders());
        mainQuery.append(queryOneWhere(paymentReference, sim));

        mainQuery.append(" UNION ");

        //SELECT
        mainQuery.append("SELECT ").append(queryOneFields());

        //FROM
        mainQuery.append(queryTwoFrom());

        //WHERE
        mainQuery.append(queryOneBasicWhere());                 // 1 = 1
        mainQuery.append(queryTwoWhere());
        mainQuery.append(merchantIDsWhere);                     //
        mainQuery.append(datesWhere);                           //
        mainQuery.append(queryOneWhereProviders());
        mainQuery.append(queryOneWhere(paymentReference, sim));


        mainQuery.append(" UNION ");

        //SELECT
        mainQuery.append("SELECT ").append(queryThreeFields());

        //FROM
        mainQuery.append(queryThreeFrom());

        //WHERE
        mainQuery.append(queryOneBasicWhere());                 // 1 = 1
        mainQuery.append(queryThreeWhere());
        mainQuery.append(merchantIDsWhere);                     //
        mainQuery.append(datesWhere);                           //
        mainQuery.append(queryOneWhereProviders());
        mainQuery.append(queryOneWhere(paymentReference, sim));


        return mainQuery.toString();
    }

    /**
     *
     * @param fieldName
     * @param startDate With format dd/mm/yyyy
     * @param endDate With format dd/mm/yyyy
     * @return
     * @throws Exception
     */
    private String buildDatesRangeWhere(String accessLevel, String refId, String fieldName, String startDate, String endDate) throws Exception{
        Vector vTimeZoneFilterDates = com.debisys.utils.TimeZone.getTimeZoneFilterDates(accessLevel, refId, startDate, endDate + " 23:59:59.999", false);
        StringBuilder sb = new StringBuilder();
        sb.append(" AND (").append(fieldName).append(">='").append(vTimeZoneFilterDates.get(0)).append("' AND ").append(fieldName).append("<='").append(vTimeZoneFilterDates.get(1)).append("') ");
        return sb.toString();
    }

    private String buildMerchantIDsWhere(String csvMerchantIds, String accessLevel, String distChainType, String refId){

        StringBuilder sb1 = new StringBuilder();
        if(csvMerchantIds != null && !csvMerchantIds.isEmpty()){
            sb1.append(" AND spTrx.merchant_id IN(").append(csvMerchantIds).append(") ");
        }else {
            sb1.append(" AND spTrx.merchant_id IN(");
            if (accessLevel.equals(DebisysConstants.ISO)) {
                if (distChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
                    sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id =").append(refId).append(")");

                } else if (distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
                    sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(");
                    sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id IN(");
                    sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_AGENT).append(" AND iso_id =").append(refId).append(")))");
                }
            } else if (accessLevel.equals(DebisysConstants.AGENT)) {
                sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
                sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(");
                sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id =").append(refId).append("))");

            } else if (accessLevel.equals(DebisysConstants.SUBAGENT)) {
                sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
                sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(refId).append(")");
            } else if (accessLevel.equals(DebisysConstants.REP)) {
                sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id =").append(refId);

            } else if (accessLevel.equals(DebisysConstants.MERCHANT)) {
                sb1.append(refId);
            }
            sb1.append(") ");
        }
        return sb1.toString();

    }

    private String queryOneFields(){
        StringBuilder sb = new StringBuilder()
        .append("spTrx.rec_id                               AS RecId,")
        .append("spTrx.merchant_id                          AS MerchantID,")
        .append("spTrx.dba                                  AS MerchantDBA,")
        .append("spTrx.description                          AS ProductName,")
        .append("spTypes.spifftypedescription               AS SpiffType,")
        .append("'ACTIVATION SPIFF'                         AS CommissionType,")
        .append("(spTrx.amount * spTrx.merchant_rate / 100) AS MerchantCommissionAmount,")
        .append("(spTrx.amount * spTrx.rep_rate / 100)      AS RepCommissionAmount,")
        .append("(spTrx.amount * spTrx.subagent_rate / 100) AS SubAgentCommissionAmount,")
        .append("(spTrx.amount * spTrx.agent_rate / 100)    AS AgentCommissionAmount,")
        .append("(spTrx.amount * spTrx.iso_rate / 100)      AS IsoCommissionAmount,")
        .append("spTrx.iso_rate                             AS IsoRate,")
        .append("spTrx.agent_rate                           AS AgentRate,")
        .append("spTrx.subagent_rate                        AS SubAgentRate,")
        .append("spTrx.rep_rate                             AS RepRate,")
        .append("spTrx.merchant_rate                        AS MerchantRate,")
        .append("spTrx.datetime                             AS TrxDate,")
        .append("saTrx.sim                                  AS SIM,")
        .append("saTrx.esn                                  AS ESN,")
        .append("pinTrx.pin                                 AS PIN,")
        .append("actTrx.datetime                            AS ActivationDate,")
        .append("actPrd.description                         AS ActivationProduct,")
        .append("rep.businessname                           AS RepName,")
        .append("suba.businessname                          AS SubAgentName,")
        .append("age.businessname                           AS AgentName,")
        .append("actTrx.rec_id                              AS ActivationTrxId,")
        .append("bil.recid                                  AS BlRecId,")
        .append("bil.transmitDate                           AS BlTransmitDate,")
        .append("bil.ACHFileTraceNumber                     AS BlTraceNumber,")
        .append("cpTrx.transactionId                        AS CptRecId,")
        .append("mcre.log_datetime                          AS McLogDatetime,")
        .append("mcre.id                                    AS McId,")
        .append("spProv.carrierName                         AS ProviderName, ")
        .append("saTrx.multiLineId As MultiLineId ");
        return sb.toString();
    }

    private String queryOneFrom(){
        StringBuilder sb = new StringBuilder();
        sb.append("FROM SpiffTypes spTypes WITH (NOLOCK) ");
        sb.append("INNER JOIN spiffsproducts spPrd WITH ( NOLOCK ) ON spPrd.spifftype = spTypes.id ");
        sb.append("INNER JOIN spiff_transactions spTrx WITH ( NOLOCK ) ON spTrx.id = spPrd.productspiff ");

        sb.append("INNER JOIN reps rep WITH ( NOLOCK ) ON rep.rep_id = spTrx.rep_id ");
        sb.append("INNER JOIN reps suba WITH ( NOLOCK ) ON suba.rep_id = rep.iso_id ");
        sb.append("INNER JOIN reps age WITH ( NOLOCK ) ON age.rep_id = suba.iso_id ");

        sb.append("INNER JOIN transactions trxSpiff WITH ( NOLOCK ) ON spTrx.rec_id = trxSpiff.rec_id AND trxSpiff.ach_type IN (7,8) ");
        sb.append("INNER JOIN transactions pinTrx WITH ( NOLOCK ) ON trxSpiff.pin = pinTrx.pin AND pinTrx.type = 1 AND pinTrx.ach_type = 1 ");
        sb.append("INNER JOIN transactions actTrx WITH ( NOLOCK ) ON trxSpiff.related_rec_id = actTrx.rec_id ");

        sb.append("LEFT OUTER JOIN billing bil WITH ( NOLOCK ) ON bil.recid = spTrx.rec_id AND bil.paymentTypeId = 2 AND bil.entityTypeId = 8 ");
        sb.append("LEFT OUTER JOIN CommissionProcessedTransactions cpTrx WITH ( NOLOCK ) ON cpTrx.transactionid = spTrx.rec_id AND cpTrx.entitytype = 'm' ");
        sb.append("LEFT OUTER JOIN merchant_credits mcre WITH ( NOLOCK ) ON mcre.id = cpTrx.paymentid ");

        sb.append("LEFT OUTER JOIN products actPrd WITH ( NOLOCK ) ON actPrd.id = actTrx.recordedproductid ");
        sb.append("LEFT OUTER JOIN SimActivationTransactionsDetail saTrxDet WITH ( NOLOCK ) ON saTrxDet.transactionId = spTrx.rec_id ");
        sb.append("LEFT OUTER JOIN HelperTransactions hpTrx WITH ( NOLOCK ) ON hpTrx.EmidaTransactionId = spTrx.rec_id ");
        sb.append("LEFT OUTER JOIN SimActivationTransactions saTrx WITH ( NOLOCK ) ON (saTrx.idnew = saTrxDet.idsimactivationtransactions OR saTrx.idnew = hpTrx.IdNewSimActivations) ");

        sb.append("LEFT OUTER JOIN SimCarrierMapping scMap WITH (NOLOCK ) ON scMap.emidaCarrierId = actPrd.carrier_id ");
        sb.append("LEFT OUTER JOIN SpiffProvidersH2HMapping spMap WITH (NOLOCK ) ON spMap.externalcarrierId = scMap.externalcarrierid ");
        sb.append("LEFT OUTER JOIN SpiffProviders spProv WITH (NOLOCK ) ON spProv.idNew = spMap.spiffcarrierid ");

        return sb.toString();
    }

    private String queryOneBasicWhere(){
        StrBuilder sb = new StrBuilder();
        sb.append(" WHERE 1 = 1 ");
        return sb.toString();
    }

    private String queryOneWhere(String paymentReference, String sim){
        StrBuilder sb = new StrBuilder();
        if(paymentReference != null && !paymentReference.isEmpty()){
            sb.append(" AND (bil.ACHFileTraceNumber=").append(paymentReference).append(" OR mcre.id=").append(paymentReference).append(") ");
        }
        if(sim != null && !sim.isEmpty()){
            sb.append(" AND saTrx.sim='").append(sim).append("' ");
        }
        return sb.toString();
    }

    private String queryOneWhereProviders(){
        StringBuilder sb = new StringBuilder();
        if(this.csvProviderIds != null && !csvProviderIds.isEmpty()){
            sb.append(" AND spProv.idNew IN (").append(getProvidersIdStringfy(csvProviderIds)).append(") ");
            //sb.append("SELECT h2hmap.providerid ");
            //sb.append("FROM SpiffProvidersH2HMapping h2hmap WITH ( NOLOCK ) ");
            //sb.append("WHERE h2hmap.spiffproviderid IN (").append(getProvidersIdStringfy(csvProviderIds)).append(") ");

/*            sb.append(" AND spp.productspiff IN (");

            sb.append("SELECT DISTINCT prdSpiff.id ");
            sb.append("FROM SpiffProvidersH2HMapping h2hmap WITH ( NOLOCK ) ");
            sb.append("INNER JOIN products prdAct WITH ( NOLOCK ) ON prdAct.program_id=h2hmap.providerid ");
            sb.append("INNER JOIN SimActivation SimAct WITH ( NOLOCK ) ON SimAct.productSim=prdAct.id ");
            sb.append("INNER JOIN products prdSpiff WITH ( NOLOCK ) ON prdSpiff.id=SimAct.spiffsku ");
            sb.append("WHERE h2hmap.spiffproviderid IN (").append(getProvidersIdStringfy(csvProviderIds)).append(")");*/

            //sb.append(") ");
        }

        return sb.toString();
    }

    private String getProvidersIdStringfy(String pids){
        String[] split = pids.split(",");
        StringBuilder sb = new StringBuilder();
        for (String pid : split) {
            sb.append("'").append(pid).append("',");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }


    private String queryTwoFrom(){
        StringBuilder sb = new StringBuilder();
        sb.append("FROM SpiffTypes spTypes WITH (NOLOCK) ");
        sb.append("INNER JOIN spiffsproducts spPrd WITH ( NOLOCK ) ON spPrd.spifftype = spTypes.id ");
        sb.append("INNER JOIN spiff_transactions spTrx WITH ( NOLOCK ) ON spTrx.id = spPrd.productspiff ");

        sb.append("INNER JOIN reps rep WITH ( NOLOCK ) ON rep.rep_id = spTrx.rep_id ");
        sb.append("INNER JOIN reps suba WITH ( NOLOCK ) ON suba.rep_id = rep.iso_id ");
        sb.append("INNER JOIN reps age WITH ( NOLOCK ) ON age.rep_id = suba.iso_id ");

        sb.append("INNER JOIN transactions trxSpiff WITH ( NOLOCK ) ON spTrx.rec_id = trxSpiff.rec_id AND trxSpiff.ach_type IN (7,8) ");
        sb.append("INNER JOIN transactions pinTrx WITH ( NOLOCK ) ON trxSpiff.pin = pinTrx.pin AND pinTrx.type = 1 AND pinTrx.ach_type = 1 ");
        sb.append("LEFT OUTER JOIN transactions actTrx WITH ( NOLOCK ) ON trxSpiff.related_rec_id = actTrx.rec_id ");

        sb.append("LEFT OUTER JOIN billing bil WITH ( NOLOCK ) ON bil.recid = spTrx.rec_id AND bil.paymentTypeId = 2 AND bil.entityTypeId = 8 ");
        sb.append("LEFT OUTER JOIN CommissionProcessedTransactions cpTrx WITH ( NOLOCK ) ON cpTrx.transactionid = spTrx.rec_id AND cpTrx.entitytype = 'm' ");
        sb.append("LEFT OUTER JOIN merchant_credits mcre WITH ( NOLOCK ) ON mcre.id = cpTrx.paymentid ");

        sb.append("LEFT OUTER JOIN products actPrd WITH ( NOLOCK ) ON actPrd.id = actTrx.recordedproductid ");
        sb.append("LEFT OUTER JOIN products pinPrd WITH ( NOLOCK ) ON pinPrd.id = pinTrx.recordedproductid ");
        sb.append("LEFT OUTER JOIN SimActivationTransactionsDetail saTrxDet WITH ( NOLOCK ) ON saTrxDet.transactionId = spTrx.rec_id ");
        sb.append("LEFT OUTER JOIN HelperTransactions hpTrx WITH ( NOLOCK ) ON hpTrx.EmidaTransactionId = spTrx.rec_id ");
        sb.append("LEFT OUTER JOIN SimActivationTransactions saTrx WITH ( NOLOCK ) ON (saTrx.idnew = saTrxDet.idsimactivationtransactions OR saTrx.idnew = hpTrx.IdNewSimActivations) ");

        sb.append("LEFT OUTER JOIN SimCarrierMapping scMap WITH (NOLOCK ) ON scMap.emidaCarrierId = pinPrd.carrier_id ");
        sb.append("LEFT OUTER JOIN SpiffProvidersH2HMapping spMap WITH (NOLOCK ) ON spMap.externalcarrierId = scMap.externalcarrierid ");
        sb.append("LEFT OUTER JOIN SpiffProviders spProv WITH (NOLOCK ) ON spProv.idNew = spMap.spiffcarrierid ");

        return sb.toString();
    }

    private String queryTwoWhere(){
        StringBuilder sb = new StringBuilder();
        sb.append(" AND actTrx.rec_id IS NULL ");
        return sb.toString();
    }


    private String queryThreeFields(){
        StringBuilder sb = new StringBuilder()
                .append("spTrx.rec_id                               AS RecId,")
                .append("spTrx.merchant_id                          AS MerchantID,")
                .append("spTrx.dba                                  AS MerchantDBA,")
                .append("spTrx.description                          AS ProductName,")
                .append("spTypes.spifftypedescription               AS SpiffType,")
                .append("'ACTIVATION SPIFF'                         AS CommissionType,")
                .append("(spTrx.amount * spTrx.merchant_rate / 100) AS MerchantCommissionAmount,")
                .append("(spTrx.amount * spTrx.rep_rate / 100)      AS RepCommissionAmount,")
                .append("(spTrx.amount * spTrx.subagent_rate / 100) AS SubAgentCommissionAmount,")
                .append("(spTrx.amount * spTrx.agent_rate / 100)    AS AgentCommissionAmount,")
                .append("(spTrx.amount * spTrx.iso_rate / 100)      AS IsoCommissionAmount,")
                .append("spTrx.iso_rate                             AS IsoRate,")
                .append("spTrx.agent_rate                           AS AgentRate,")
                .append("spTrx.subagent_rate                        AS SubAgentRate,")
                .append("spTrx.rep_rate                             AS RepRate,")
                .append("spTrx.merchant_rate                        AS MerchantRate,")
                .append("spTrx.datetime                             AS TrxDate,")
                .append("saTrx.sim                                  AS SIM,")
                .append("saTrx.esn                                  AS ESN,")
                .append("actTrx.pin                                 AS PIN,")
                .append("actTrx.datetime                            AS ActivationDate,")
                .append("actPrd.description                         AS ActivationProduct,")
                .append("rep.businessname                           AS RepName,")
                .append("suba.businessname                          AS SubAgentName,")
                .append("age.businessname                           AS AgentName,")
                .append("actTrx.rec_id                              AS ActivationTrxId,")
                .append("bil.recid                                  AS BlRecId,")
                .append("bil.transmitDate                           AS BlTransmitDate,")
                .append("bil.ACHFileTraceNumber                     AS BlTraceNumber,")
                .append("cpTrx.transactionId                        AS CptRecId,")
                .append("mcre.log_datetime                          AS McLogDatetime,")
                .append("mcre.id                                    AS McId,")
                .append("spProv.carrierName                         AS ProviderName, ")
                .append("saTrx.multiLineId As MultiLineId ");

        return sb.toString();
    }

    private String queryThreeFrom(){
        StringBuilder sb = new StringBuilder();
        sb.append("FROM SpiffTypes spTypes WITH (NOLOCK) ");
        sb.append("INNER JOIN spiffsproducts spPrd WITH ( NOLOCK ) ON spPrd.spifftype = spTypes.id ");
        sb.append("INNER JOIN spiff_transactions spTrx WITH ( NOLOCK ) ON spTrx.id = spPrd.productspiff ");

        sb.append("INNER JOIN reps rep WITH ( NOLOCK ) ON rep.rep_id = spTrx.rep_id ");
        sb.append("INNER JOIN reps suba WITH ( NOLOCK ) ON suba.rep_id = rep.iso_id ");
        sb.append("INNER JOIN reps age WITH ( NOLOCK ) ON age.rep_id = suba.iso_id ");

        sb.append("INNER JOIN transactions trxSpiff WITH ( NOLOCK ) ON spTrx.rec_id = trxSpiff.rec_id AND trxSpiff.ach_type IN (7,8) ");
        sb.append("LEFT OUTER JOIN transactions pinTrx WITH ( NOLOCK ) ON trxSpiff.pin = pinTrx.pin AND pinTrx.type = 1 AND pinTrx.ach_type = 1 ");
        sb.append("INNER JOIN transactions actTrx WITH ( NOLOCK ) ON trxSpiff.related_rec_id = actTrx.rec_id ");

        sb.append("LEFT OUTER JOIN billing bil WITH ( NOLOCK ) ON bil.recid = spTrx.rec_id AND bil.paymentTypeId = 2 AND bil.entityTypeId = 8 ");
        sb.append("LEFT OUTER JOIN CommissionProcessedTransactions cpTrx WITH ( NOLOCK ) ON cpTrx.transactionid = spTrx.rec_id AND cpTrx.entitytype = 'm' ");
        sb.append("LEFT OUTER JOIN merchant_credits mcre WITH ( NOLOCK ) ON mcre.id = cpTrx.paymentid ");

        sb.append("LEFT OUTER JOIN products actPrd WITH ( NOLOCK ) ON actPrd.id = actTrx.recordedproductid ");
        sb.append("LEFT OUTER JOIN SimActivationTransactionsDetail saTrxDet WITH ( NOLOCK ) ON saTrxDet.transactionId = spTrx.rec_id ");
        sb.append("LEFT OUTER JOIN HelperTransactions hpTrx WITH ( NOLOCK ) ON hpTrx.EmidaTransactionId = spTrx.rec_id ");
        sb.append("LEFT OUTER JOIN SimActivationTransactions saTrx WITH ( NOLOCK ) ON (saTrx.idnew = saTrxDet.idsimactivationtransactions OR saTrx.idnew = hpTrx.IdNewSimActivations) ");

        sb.append("LEFT OUTER JOIN SimCarrierMapping scMap WITH (NOLOCK ) ON scMap.emidaCarrierId = actPrd.carrier_id ");
        sb.append("LEFT OUTER JOIN SpiffProvidersH2HMapping spMap WITH (NOLOCK ) ON spMap.externalcarrierId = scMap.externalcarrierid ");
        sb.append("LEFT OUTER JOIN SpiffProviders spProv WITH (NOLOCK ) ON spProv.idNew = spMap.spiffcarrierid ");

        return sb.toString();
    }

    private String queryThreeWhere(){
        StringBuilder sb = new StringBuilder();
        sb.append(" AND pinTrx.rec_id IS NULL ");
        return sb.toString();
    }

}
