package net.emida.supportsite.reports.spiff.deatil.dao;

import com.debisys.utils.NumberUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Franky Villadiego
 */
public class SpiffDetailReportModel implements CsvRow{

    private Long rowNum;
    private Long recId; //Spiff trasaction ID
    private Long activationTrxId;
    private Long merchantId;
    private String merchantDba = "";
    private String productName = "";
    private String spiffType = "";
    private Date paymentDate;
    private Date activationDate;
    private String activationProduct = "";
    private String pin = "";
    private String esn = "";
    private String sim = "";
    private String commissionType = "";
    private BigDecimal isoRate = new BigDecimal(0.0);
    private BigDecimal agentRate = new BigDecimal(0.0);
    private BigDecimal subAgentRate = new BigDecimal(0.0);
    private BigDecimal repRate = new BigDecimal(0.0);
    private BigDecimal merchantRate = new BigDecimal(0.0);
    private BigDecimal merchantCommissionAmount = new BigDecimal(0.0);
    private BigDecimal repCommissionAmount = new BigDecimal(0.0);
    private BigDecimal subAgentCommissionAmount = new BigDecimal(0.0);
    private BigDecimal agentCommissionAmount = new BigDecimal(0.0);
    private BigDecimal isoCommissionAmount = new BigDecimal(0.0);
    private String paymentType = "";
    private String paymentReference = "";
    private String repName = "";
    private String subAgentName = "";
    private String agentName = "";
    private String providerName;

    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public Long getRecId() {
        return recId;
    }

    public void setRecId(Long recId) {
        this.recId = recId;
    }

    public Long getActivationTrxId() {
        return activationTrxId;
    }

    public void setActivationTrxId(Long activationTrxId) {
        this.activationTrxId = activationTrxId;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantDba() {
        return merchantDba;
    }

    public void setMerchantDba(String merchantDba) {
        this.merchantDba = merchantDba;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSpiffType() {
        return spiffType;
    }

    public void setSpiffType(String spiffType) {
        this.spiffType = spiffType;
    }

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public String getActivationProduct() {
        return activationProduct;
    }

    public void setActivationProduct(String activationProduct) {
        this.activationProduct = activationProduct;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getEsn() {
        return esn;
    }

    public void setEsn(String esn) {
        this.esn = esn;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(String commissionType) {
        this.commissionType = commissionType;
    }

    public BigDecimal getIsoRate() {
        return isoRate;
    }

    public void setIsoRate(BigDecimal isoRate) {
        this.isoRate = isoRate;
    }

    public BigDecimal getAgentRate() {
        return agentRate;
    }

    public void setAgentRate(BigDecimal agentRate) {
        this.agentRate = agentRate;
    }

    public BigDecimal getSubAgentRate() {
        return subAgentRate;
    }

    public void setSubAgentRate(BigDecimal subAgentRate) {
        this.subAgentRate = subAgentRate;
    }

    public BigDecimal getRepRate() {
        return repRate;
    }

    public void setRepRate(BigDecimal repRate) {
        this.repRate = repRate;
    }

    public BigDecimal getMerchantRate() {
        return merchantRate;
    }

    public void setMerchantRate(BigDecimal merchantRate) {
        this.merchantRate = merchantRate;
    }

    public BigDecimal getMerchantCommissionAmount() {
        return merchantCommissionAmount;
    }

    public void setMerchantCommissionAmount(BigDecimal merchantCommissionAmount) {
        this.merchantCommissionAmount = merchantCommissionAmount;
    }

    public BigDecimal getRepCommissionAmount() {
        return repCommissionAmount;
    }

    public void setRepCommissionAmount(BigDecimal repCommissionAmount) {
        this.repCommissionAmount = repCommissionAmount;
    }

    public BigDecimal getSubAgentCommissionAmount() {
        return subAgentCommissionAmount;
    }

    public void setSubAgentCommissionAmount(BigDecimal subAgentCommissionAmount) {
        this.subAgentCommissionAmount = subAgentCommissionAmount;
    }

    public BigDecimal getAgentCommissionAmount() {
        return agentCommissionAmount;
    }

    public void setAgentCommissionAmount(BigDecimal agentCommissionAmount) {
        this.agentCommissionAmount = agentCommissionAmount;
    }

    public BigDecimal getIsoCommissionAmount() {
        return isoCommissionAmount;
    }

    public void setIsoCommissionAmount(BigDecimal isoCommissionAmount) {
        this.isoCommissionAmount = isoCommissionAmount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }


    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getSubAgentName() {
        return subAgentName;
    }

    public void setSubAgentName(String subAgentName) {
        this.subAgentName = subAgentName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    @Override
    public List<CsvCell> cells() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cell1 = new CsvCell(recId.toString());
        list.add(cell1);
        CsvCell cell2 = new CsvCell(activationTrxId.toString());
        list.add(cell2);
        CsvCell cell3 = new CsvCell(merchantId.toString());
        list.add(cell3);
        CsvCell cell4 = new CsvCell(merchantDba);
        list.add(cell4);
        CsvCell cell5 = new CsvCell(productName != null ? productName.trim() : "");
        list.add(cell5);
        CsvCell cell6 = new CsvCell(spiffType);
        list.add(cell6);

        String formattedPaymentDate = paymentDate != null ? sdf.format(paymentDate) : "";
        CsvCell cell7 = new CsvCell(formattedPaymentDate);
        list.add(cell7);

        String formattedActivationDate = activationDate != null ? sdf.format(activationDate) : "";
        CsvCell cell8 = new CsvCell(formattedActivationDate);
        list.add(cell8);

        CsvCell cell9 = new CsvCell(activationProduct != null ? activationProduct.trim() : "");
        list.add(cell9);
        CsvCell cell10 = new CsvCell(pin != null ? pin : "");
        list.add(cell10);
        CsvCell cell11 = new CsvCell(esn != null ? esn : "");
        list.add(cell11);
        CsvCell cell12 = new CsvCell(sim != null ? sim : "");
        list.add(cell12);
        CsvCell cell13 = new CsvCell(commissionType);
        list.add(cell13);
        CsvCell cell14 = new CsvCell(NumberUtil.formatCurrency(merchantCommissionAmount.toString()));
        list.add(cell14);
        CsvCell cell15 = new CsvCell(NumberUtil.formatCurrency(repCommissionAmount.toString()));
        list.add(cell15);
        CsvCell cell16 = new CsvCell(NumberUtil.formatCurrency(subAgentCommissionAmount.toString()));
        list.add(cell16);
        CsvCell cell17 = new CsvCell(NumberUtil.formatCurrency(agentCommissionAmount.toString()));
        list.add(cell17);
        CsvCell cell18 = new CsvCell(NumberUtil.formatCurrency(isoCommissionAmount.toString()));
        list.add(cell18);
        CsvCell cell19 = new CsvCell(paymentType);
        list.add(cell19);
        CsvCell cell20 = new CsvCell(paymentReference);
        list.add(cell20);
        CsvCell cell21 = new CsvCell(repName);
        list.add(cell21);
        CsvCell cell22 = new CsvCell(subAgentName);
        list.add(cell22);
        CsvCell cell23 = new CsvCell(agentName);
        list.add(cell23);

        CsvCell cell24 = new CsvCell(providerName);
        list.add(cell24);

        return list;
    }
}
