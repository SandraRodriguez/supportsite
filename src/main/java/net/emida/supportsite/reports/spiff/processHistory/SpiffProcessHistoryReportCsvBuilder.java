package net.emida.supportsite.reports.spiff.processHistory;

import java.util.Map;
import java.util.List;
import java.io.Writer;
import org.slf4j.Logger;
import java.io.FileWriter;
import java.util.ArrayList;
import java.io.IOException;
import java.io.BufferedWriter;
import org.slf4j.LoggerFactory;
import com.debisys.languages.Languages;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.csv.CsvCell;
import org.springframework.stereotype.Service;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 * @author Fernando Briceno based on Franky Villadiego work
 */
@Service
public class SpiffProcessHistoryReportCsvBuilder implements CsvBuilderService {

    private static final Logger logger = LoggerFactory.getLogger(SpiffProcessHistoryReportCsvBuilder.class);
    
    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {
            if((list == null) || (list.size() != 2))
            {
                throw new IllegalArgumentException("Two result lists expected");
            }
            
            //File to fill
            writer = getFileWriter(filePath);

            logger.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            // Transaction list results
            List<CsvRow> trxData = (List<CsvRow>) list.get(0);
            
            List<String> trxHeaders = generateTrxListHeaders(additionalData);
            String trxHeaderRow = CsvUtil.generateRow(trxHeaders, true);
            String language = (String) additionalData.get("language");
            writer.write(String.format("\n\n%s : %s\n\n",
                    Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.requestPin", language),
                    this.pin));
            writer.write(trxHeaderRow);
            
            this.fillWriter(trxData, writer, additionalData);

            // Spiff process history records
            List<String> historyHeaders = generateProcessHistoryHeaders(additionalData);
            String historyHeaderRow = CsvUtil.generateRow(historyHeaders, true);
            writer.write('\n');
            writer.write('\n');
            writer.write(historyHeaderRow);

            List<CsvRow> historyData = (List<CsvRow>) list.get(1);
            this.fillWriter(historyData, writer, additionalData);

        } catch (Exception ex) {
            logger.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {

                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {
        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            writer.write(stringifyRow);
        }
    }

    private List<String> generateTrxListHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");
        List<String> headers = new ArrayList<String>();

        headers.add("#");
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.trxDate", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.productId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.productTrxType", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.billingTrxType", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.TrxId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.relTrxId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.controlNo", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.trxAmount", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.repId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.merchantId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.terminal", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.paidBy", language));
        return headers;
    }

    private List<String> generateProcessHistoryHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");
        List<String> headers = new ArrayList<String>();

        headers.add("#");
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.TrxId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.loadDate", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.fileDate", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.processDate", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.processResult", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffProcessHistoryReport.rejectReason", language));
        return headers;
    }
    
    
}
