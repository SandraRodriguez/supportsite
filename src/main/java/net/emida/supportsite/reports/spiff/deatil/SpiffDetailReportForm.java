package net.emida.supportsite.reports.spiff.deatil;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Date;

/**
 * @author Franky Villadiego
 */
public class SpiffDetailReportForm {

    private Date startDate;
    private Date endDate;
    private String sim;
    private Long pnumber;
    private boolean allIsos;
    private String filterAllIsos;
    private String[] mids;
    private String[] pids;




    @NotNull(message = "{com.debisys.reports.error1}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }



    @NotNull(message = "{com.debisys.reports.error3}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public Long getPnumber() {
        return pnumber;
    }

    public void setPnumber(Long pnumber) {
        this.pnumber = pnumber;
    }

    public String[] getMids() {
        return mids;
    }

    public void setMids(String[] mids) {
        this.mids = mids;
    }

    public String[] getPids() {
        return pids;
    }

    public void setPids(String[] pids) {
        this.pids = pids;
    }

    public String isFilterAllIsos() {
        return filterAllIsos;
    }

    public void setFilterAllIsos(String filterAllIsos) {
        this.filterAllIsos = filterAllIsos;
    }

    public boolean isAllIsos() {
        return allIsos;
    }

    public void setAllIsos(boolean allIsos) {
        this.allIsos = allIsos;
    }

    

    
    public String toCsvMerchantIds(){
        if(this.getMids() != null && this.getMids().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getMids()) {
                sb1.append(val).append(",");
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

    public String toCsvProviderIds(){
        if(this.getPids() != null && this.getPids().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getPids()) {
                sb1.append(val).append(",");
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

    @Override
    public String toString() {
        return "SpiffReportFormModel[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", sim='" + sim + '\'' +
                ", pnumber=" + pnumber +
                ", mids=" + Arrays.toString(mids) +
                ", pids=" + Arrays.toString(pids) +
                ']';
    }
}
