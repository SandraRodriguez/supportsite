package net.emida.supportsite.reports.spiff.deatil.dao;

import net.emida.supportsite.reports.spiff.deatil.SpiffDetailDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */

@Repository
public class SpiffDetailJdbcDao implements SpiffDetailDao {

    private static final Logger log = LoggerFactory.getLogger(SpiffDetailJdbcDao.class);

    private static String SQL1= "SELECT car.idnew AS Id, car.id As Code, car.name AS Description  from carriers car";
    private static String SQL_COUNT= "SELECT COUNT(*) from carriers";

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll(){
        try {
            return jdbcTemplate.queryForObject(SQL_COUNT, Long.class);
        }catch (DataAccessException ex){
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    public List<SpiffDetailReportModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData){
        try {

            SpiffDetailQueryBuilder queryBuilder = new SpiffDetailQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildQuery();

            log.debug("Query : {}", SQL_QUERY);

            return jdbcTemplate.query(SQL_QUERY, new SpiffDetailReportMapper());
        }catch (DataAccessException ex){
            log.error("Data access error in list spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }catch (Exception ex){
            log.error("Exception in list spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {

            SpiffDetailQueryBuilder queryBuilder = new SpiffDetailQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();

            log.debug("Query Count : {}", SQL_QUERY_COUNT);

            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        }catch (DataAccessException ex){
            log.error("Data access error in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }catch (Exception ex){
            log.error("Exception in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }


    public List<Map<String, Object>> listSpiffProviders() throws TechnicalException {
     try{
        String sql = "SELECT IdNew AS IdNew, CarrierName AS ProviderName FROM SpiffProviders sp WITH(NOLOCK)";
        return jdbcTemplate.queryForList(sql);
    }catch (DataAccessException ex){
        log.error("Data access error listing spiff providers");
        throw new TechnicalException(ex.getMessage(), ex);
    }catch (Exception ex){
        log.error("Exception ilisting spiff providers");
        throw new TechnicalException(ex.getMessage(), ex);
    }
    }


}
