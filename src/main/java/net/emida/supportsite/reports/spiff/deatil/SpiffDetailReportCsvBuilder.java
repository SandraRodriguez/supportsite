package net.emida.supportsite.reports.spiff.deatil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.debisys.languages.Languages;
import com.debisys.utils.DebisysConstants;

import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 * @author Franky Villadiego
 */
@Service
public class SpiffDetailReportCsvBuilder implements CsvBuilderService {

    private static final Logger log = LoggerFactory.getLogger(SpiffDetailReportCsvBuilder.class);

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {

            List<CsvRow> data = (List<CsvRow>) list;
            //File to fill
            writer = getFileWriter(filePath);

            log.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            List<String> headers = generateHeaders(additionalData);
            String headerRow = CsvUtil.generateRow(headers, true);
            writer.write(headerRow);

            fillWriter(data, writer, additionalData);

        } catch (Exception ex) {
            log.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {

                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {
        String accessLevel = (String) additionalData.get("accessLevel");
        String distChainType = (String) additionalData.get("distChainType");
        
        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            //String stringifyRow = ""; //CsvUtil.stringifyRow(cells, true);
            StringBuilder sb = new StringBuilder();
            int index = 0;
            for (CsvCell cell : cells) {                
                //sb.append("\"").append(cell.getValue()).append("\"").append(",");
                if (index == 14) {
                    if (accessLevel.equals(DebisysConstants.REP) || accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
                        sb.append("\"").append(cell.getValue()).append("\"").append(",");
                    }
                } else if (index == 15) {
                    if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT)
                            || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                        sb.append("\"").append(cell.getValue()).append("\"").append(",");
                    }
                } else if (index == 16) {
                    if (accessLevel.equals(DebisysConstants.AGENT)
                            || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                        sb.append("\"").append(cell.getValue()).append("\"").append(",");
                    }
                } else if (index == 17) {
                    if (accessLevel.equals(DebisysConstants.ISO)) {
                        sb.append("\"").append(cell.getValue()).append("\"").append(",");
                    }
                } else if (index == 20) {
                    if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
                        sb.append("\"").append(cell.getValue()).append("\"").append(",");
                    }
                } else if (index == 21) {
                    if (accessLevel.equals(DebisysConstants.AGENT)
                            || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                        sb.append("\"").append(cell.getValue()).append("\"").append(",");
                    }
                } else if (index == 22) {
                    if (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        sb.append("\"").append(cell.getValue()).append("\"").append(",");
                    }
                } else {
                    sb.append("\"").append(cell.getValue()).append("\"").append(",");
                }
                index++;                
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("\n");
            writer.write(sb.toString());

            /*
             if (index == 14) {
             if (accessLevel.equals(DebisysConstants.REP) || accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
             writer.write(stringifyRow);
             }
             } else if (index == 15) {
             if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT)
             || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
             writer.write(stringifyRow);
             }
             } else if (index == 16) {
             if (accessLevel.equals(DebisysConstants.AGENT)
             || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
             writer.write(stringifyRow);
             }
             } else if (index == 17) {
             if (accessLevel.equals(DebisysConstants.ISO)) {
             writer.write(stringifyRow);
             }
             } else if (index == 20) {
             if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
             writer.write(stringifyRow);
             }
             } else if (index == 21) {
             if (accessLevel.equals(DebisysConstants.AGENT)
             || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
             writer.write(stringifyRow);
             }
             } else if (index == 22) {
             if (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
             writer.write(stringifyRow);
             }
             } else {
             writer.write(stringifyRow);
             }
             */
            //index++;
        }
        log.info("fillWriter end");
    }

    private List<String> generateHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");
        String accessLevel = (String) additionalData.get("accessLevel");
        String distChainType = (String) additionalData.get("distChainType");
        List<String> headers = new ArrayList<String>();

        headers.add(Languages.getString("jsp.admin.reports.spiffReports.recId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.activationId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.merchantId", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.merchantDba", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.productName", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.spiffType", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.commissionPaymentDate", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.activationDate", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.activationProduct", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.pin", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.esn", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.sim", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.commissionType", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.mCommissionAmount", language));

        if (accessLevel.equals(DebisysConstants.REP) || accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
            headers.add(Languages.getString("jsp.admin.reports.spiffReports.rCommissionAmount", language));
        }

        if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT)
                || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
            headers.add(Languages.getString("jsp.admin.reports.spiffReports.saCommissionAmount", language));
        }

        if (accessLevel.equals(DebisysConstants.AGENT)
                || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
            headers.add(Languages.getString("jsp.admin.reports.spiffReports.aCommissionAmount", language));
        }

        if (accessLevel.equals(DebisysConstants.ISO)) {
            headers.add(Languages.getString("jsp.admin.reports.spiffReports.iCommissionAmount", language));
        }

        headers.add(Languages.getString("jsp.admin.reports.spiffReports.paymentType", language));
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.paymentReference", language));

        if (accessLevel.equals(DebisysConstants.SUBAGENT) || accessLevel.equals(DebisysConstants.AGENT) || accessLevel.equals(DebisysConstants.ISO)) {
            headers.add(Languages.getString("jsp.admin.reports.spiffReports.repName", language));
        }

        if (accessLevel.equals(DebisysConstants.AGENT)
                || (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
            headers.add(Languages.getString("jsp.admin.reports.spiffReports.subAgentName", language));
        }

        if (accessLevel.equals(DebisysConstants.ISO) && distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
            headers.add(Languages.getString("jsp.admin.reports.spiffReports.agentName", language));
        }
        headers.add(Languages.getString("jsp.admin.reports.spiffReports.providerName", language));
        return headers;

    }

}
