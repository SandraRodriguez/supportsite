package net.emida.supportsite.reports.spiff.deatil.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.debisys.utils.DebisysConstants;

public class SpiffDetailReportActivationCarriers {

    private static final Logger log = LoggerFactory.getLogger(SpiffDetailQueryBuilder.class);

    private String refId;
    private String accessLevel;
    private String distChainType;
    StringBuilder sb;

    SpiffDetailReportActivationCarriers(String refId, String accessLevel, String distChainType) {
        this.refId = refId;
        this.accessLevel = accessLevel;
        this.distChainType = distChainType;
    }

    public String publicQueryBuilderForFoundActivationTypeProductCarrirer() {
        sb = new StringBuilder();
        this.queryForfoundActivationTypeProductCarriersOldRatePlan();
        sb.append("union \n \n");
        this.queryForfoundActivationTypeProductCarriersNewRatePlan();

        String newQuery = sb.toString();
        log.debug(newQuery);

        return sb.toString();
    }

    private void queryForfoundActivationTypeProductCarriersOldRatePlan() {

        queryForfoundActivationTypeProductCarriersPrincipalQuery();
        sb.append("INNER JOIN  terminal_rates tr with (nolock) ON tr.millennium_no=ter.millennium_no \n");
        sb.append("INNER JOIN products p WITH ( NOLOCK ) ON p.id=tr.product_id \n");
        sb.append("INNER JOIN carriers car WITH ( NOLOCK ) ON car.id = p.carrier_id \n");
        sb.append("WHERE p.ach_type = 11 and m.UseRatePlanModel = 0 and ter.merchant_id \n");

        queryForFoundActivationTypeProductCarriersMerchantQuery();
    }

    private void queryForfoundActivationTypeProductCarriersNewRatePlan() {

        queryForfoundActivationTypeProductCarriersPrincipalQuery();
        sb.append("INNER JOIN  RatePlanDetail rpd with (nolock) ON rpd.RatePlanID=ter.RatePlanId \n");
        sb.append("INNER JOIN products p WITH ( NOLOCK ) ON p.id=rpd.ProductID \n");
        sb.append("INNER JOIN carriers car WITH ( NOLOCK ) ON car.id = p.carrier_id \n");
        sb.append("WHERE p.ach_type = 11 and m.UseRatePlanModel = 1 and ter.merchant_id \n");
        queryForFoundActivationTypeProductCarriersMerchantQuery();
    }

    private void queryForfoundActivationTypeProductCarriersPrincipalQuery() {
        sb.append("SELECT DISTINCT  car.IdNew AS IdNew , car.Name  AS ProviderName  \n");
        sb.append("FROM terminals ter WITH ( NOLOCK ) \n");
        sb.append("INNER JOIN merchants m WITH ( NOLOCK ) ON ter.merchant_id = m.merchant_id \n");
        sb.append("INNER JOIN reps rep WITH ( NOLOCK ) ON rep.rep_id = m.rep_id \n");
        sb.append("INNER JOIN reps suba WITH ( NOLOCK ) ON suba.rep_id = rep.iso_id \n");
        sb.append("INNER JOIN reps age WITH ( NOLOCK ) ON age.rep_id = suba.iso_id \n");

        // queryForFoundActivationTypeProductCarriersMerchantQuery();
    }

    private void queryForFoundActivationTypeProductCarriersMerchantQuery() {

        if (accessLevel.equals(DebisysConstants.ISO)) {
            sb.append("	IN(SELECT merchant_id \n");
            if (distChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                sb.append("		FROM merchants WITH(NOLOCK) \n")
                        .append("		WHERE rep_id \n")
                        .append("		IN (SELECT rep_id \n")
                        .append("			FROM reps WITH(NOLOCK) \n")
                        .append("			WHERE iso_id = ").append(refId).append(")");

            } else if (distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                sb.append("		FROM merchants WITH(NOLOCK) \n")
                        .append("		WHERE rep_id \n")
                        .append("		IN (SELECT rep_id \n")
                        .append("			FROM reps WITH(NOLOCK) \n")
                        .append("			WHERE type = ").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id \n")
                        .append("			IN(SELECT rep_id \n")
                        .append("				FROM reps WITH(NOLOCK) \n")
                        .append("				WHERE type = ").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id \n")
                        .append("				IN( SELECT rep_id \n")
                        .append("					FROM reps WITH(NOLOCK) \n")
                        .append("					WHERE type = ").append(DebisysConstants.REP_TYPE_AGENT).append(" AND iso_id = ").append(refId).append(")))");

            }
            sb.append(") \n");
        } else if (accessLevel.equals(DebisysConstants.AGENT)) {
            sb.append("	IN(SELECT merchant_id \n");

            sb.append("		FROM merchants WITH(NOLOCK) \n")
                    .append("		WHERE rep_id \n")
                    .append("		IN (SELECT rep_id \n")
                    .append("			FROM reps WITH(NOLOCK) \n")
                    .append("			WHERE type = ").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id \n")
                    .append("			IN(SELECT rep_id \n")
                    .append("		   	   FROM reps WITH(NOLOCK) \n")
                    .append("		   	   WHERE type = ").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id = ").append(refId).append("))");
            sb.append(") \n");
        } else if (accessLevel.equals(DebisysConstants.SUBAGENT)) {
            sb.append("	IN(SELECT merchant_id \n");
            sb.append("		FROM merchants WITH(NOLOCK) \n")
                    .append("		WHERE rep_id \n")
                    .append("		IN (SELECT rep_id \n")
                    .append("			FROM reps WITH(NOLOCK) \n")
                    .append("			WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(refId).append(")");
            sb.append(") \n");
        } else if (accessLevel.equals(DebisysConstants.REP)) {
            sb.append("	IN(SELECT merchant_id \n");
            sb.append("		FROM merchants WITH(NOLOCK) \n")
                    .append("		WHERE rep_id = ").append(refId);
            sb.append(") \n");
        } else if (accessLevel.equals(DebisysConstants.MERCHANT)) {
            sb.append("=").append(refId);
        }
    }

}
