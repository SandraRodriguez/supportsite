package net.emida.supportsite.reports.spiff.processHistory.dao;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import static com.debisys.utils.DbUtil.hasColumn;
import static net.emida.supportsite.reports.spiff.processHistory.dao.SpiffProcessHistoryReportConstants.*;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author fernandob
 */
public class SpiffProcessHistoryReportMapperRec implements RowMapper<CsvRow> {

    @Override
    public SpiffProcessHistoryReportModelRec mapRow(ResultSet rs, int i) throws SQLException {
        SpiffProcessHistoryReportModelRec newRecord = new SpiffProcessHistoryReportModelRec();
        if (hasColumn(rs, ROW_NUMBER)) {
            newRecord.setRowNum((rs.getString(ROW_NUMBER) == null) ? 0L : rs.getLong(ROW_NUMBER));
        }
        if (hasColumn(rs, EMIDA_TRX_ID)) {
            newRecord.setEmidaTrxId((rs.getString(EMIDA_TRX_ID) == null) ? 0L : rs.getLong(EMIDA_TRX_ID));
        }
        if (hasColumn(rs, RECORD_DATE)) {
            newRecord.setDatetime((rs.getString(RECORD_DATE) == null) ? (new Date()) : new java.util.Date(rs.getTimestamp(RECORD_DATE).getTime()));
        }
        if (hasColumn(rs, FILE_DATE)) {
            newRecord.setFileDate((rs.getString(FILE_DATE) == null) ? (new Date()) : new java.util.Date(rs.getTimestamp(FILE_DATE).getTime()));
        }
        if (hasColumn(rs, PROCESS_DATE)) {
            newRecord.setProcessDate((rs.getString(PROCESS_DATE) == null) ? (new Date()) : new java.util.Date(rs.getTimestamp(PROCESS_DATE).getTime()));
        }
        if (hasColumn(rs, PROCESS_RESULT_CODE)) {
            newRecord.setProcessResult((rs.getString(PROCESS_RESULT_CODE) == null) ? "" : rs.getString(PROCESS_RESULT_CODE));
        }
        if (hasColumn(rs, TRACFONE_REASON)) {
            newRecord.setTracfoneRejectReason((rs.getString(TRACFONE_REASON) == null) ? "" : rs.getString(TRACFONE_REASON));
        }
        return newRecord;
    }

}
