package net.emida.supportsite.reports.spiff.deatil.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import net.emida.supportsite.reports.spiff.deatil.ActivationDetailDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.emida.supportsite.util.pagination.Page;
import net.emida.supportsite.util.pagination.PaginationHelper;

@Repository
public class ActivationDetailJdbcDao implements ActivationDetailDao {

	private static final Logger log = LoggerFactory.getLogger(ActivationDetailJdbcDao.class);

	private static String SQL1 = "SELECT car.idnew AS Id, car.id As Code, car.name AS Description  from carriers car";
	private static String SQL_COUNT = "SELECT COUNT(*) from carriers";
	private static int PAGE_SIZE = 25;

	private String sqlCountActivationDetail;
	private String sqlQueryActivationDetail;

	@Autowired
	@Qualifier("replicaJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	public Long countAll() {
		try {
			return jdbcTemplate.queryForObject(SQL_COUNT, Long.class);
		} catch (DataAccessException ex) {
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	public List<ActivationDetailReportModel> list(Integer rowNumberStartInt, Integer endPage, String sortFieldName,
			String sortOrder, Map<String, Object> queryData) {
		try {

			ActivationDetailQueryBuilder queryBuilder = new ActivationDetailQueryBuilder(rowNumberStartInt, endPage, sortFieldName, sortOrder, queryData);
			sqlQueryActivationDetail = queryBuilder.buildQuery();

			log.debug("Query : {}", sqlQueryActivationDetail);

			// return jdbcTemplate.query(sqlQuerySpiffDetail, new
			// SpiffDetailReportMapper());
			return getActivationDetailReportModelPage(rowNumberStartInt, PAGE_SIZE, sortFieldName, sortOrder).getPageItems();
		} catch (DataAccessException ex) {
			log.error("Data access error in list spiff detail report");
			throw new TechnicalException(ex.getMessage(), ex);
		} catch (Exception ex) {
			log.error("Exception in list spiff detail report");
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	public List<ActivationDetailReportModel> listForDownloadFile(Integer rowNumberStartInt, Integer endPage,
			String sortFieldName, String sortOrder, Map<String, Object> queryData) {
		try {
			ActivationDetailQueryBuilder queryBuilder = new ActivationDetailQueryBuilder(rowNumberStartInt, endPage, sortFieldName, sortOrder, queryData);
			sqlQueryActivationDetail = queryBuilder.buildQuery();

			log.debug("Query : {}", sqlQueryActivationDetail);

			// return jdbcTemplate.query(sqlQuerySpiffDetail, new
			// SpiffDetailReportMapper());
			return getActivationDetailReportModelPage(rowNumberStartInt, endPage, sortFieldName, sortOrder).getPageItems();
		} catch (DataAccessException ex) {
			log.error("Data access error in list spiff detail report");
			throw new TechnicalException(ex.getMessage(), ex);
		} catch (Exception ex) {
			log.error("Exception in list spiff detail report");
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	@Override
	public Long count(Map<String, Object> queryData) throws TechnicalException {
		try {

			ActivationDetailQueryBuilder queryBuilder = new ActivationDetailQueryBuilder(queryData);
			sqlCountActivationDetail = queryBuilder.buildCountQuery();

			log.debug("Query Count : {}", sqlCountActivationDetail);

			return jdbcTemplate.queryForObject(sqlCountActivationDetail, Long.class);
		} catch (DataAccessException ex) {
			log.error("Data access error in count spiff detail report");
			throw new TechnicalException(ex.getMessage(), ex);
		} catch (Exception ex) {
			log.error("Exception in count spiff detail report");
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	public List<Map<String, Object>> listSpiffProviders() throws TechnicalException {
		try {
			// Map<String, Object> queryData
			// publicqueryBuilderForFoundActivationTypeProductCarrirer
			String sql = "SELECT IdNew AS IdNew, CarrierName AS ProviderName FROM SpiffProviders sp WITH(NOLOCK)";
			return jdbcTemplate.queryForList(sql);
		} catch (DataAccessException ex) {
			log.error("Data access error listing spiff providers");
			throw new TechnicalException(ex.getMessage(), ex);
		} catch (Exception ex) {
			log.error("Exception ilisting spiff providers");
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	public List<Map<String, Object>> listSpiffProviders(Map model) throws TechnicalException {
		try {
			// Map<String, Object> queryData
			// queryBuilderForFoundActivationTypeProductCarrirer pu
			SpiffDetailReportActivationCarriers spiffDetailReportActivationCarriers;
			spiffDetailReportActivationCarriers = new SpiffDetailReportActivationCarriers((String) model.get("ref_id"),
					(String) model.get("access_level"), (String) model.get("dist_chain_type"));
			String sql = spiffDetailReportActivationCarriers.publicQueryBuilderForFoundActivationTypeProductCarrirer();
			return jdbcTemplate.queryForList(sql);
		} catch (DataAccessException ex) {
			log.error("Data access error listing spiff providers");
			throw new TechnicalException(ex.getMessage(), ex);
		} catch (Exception ex) {
			log.error("Exception ilisting spiff providers");
			throw new TechnicalException(ex.getMessage(), ex);
		}
	}

	public Page<ActivationDetailReportModel> getActivationDetailReportModelPage(final int rowNumberStartInt,
			final int pageSize, String sortFieldName, String sortOrder) throws SQLException {
		PaginationHelper<ActivationDetailReportModel> paginationHelper = new PaginationHelper<ActivationDetailReportModel>();
		return paginationHelper.fetchPage(jdbcTemplate, sqlCountActivationDetail, sqlQueryActivationDetail, null,
				rowNumberStartInt, pageSize, new ActivationDetailReportMapper(), sortFieldName, sortOrder);

	}

}
