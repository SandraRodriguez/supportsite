package net.emida.supportsite.reports.spiff.deatil.dao;

import com.debisys.utils.StringUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

public class ActivationDetailReportModel implements CsvRow {

    private Long rowNum;
    private Long activationTrxId;
    private String recId; //Spiff trasaction ID
    private Long merchantId;
    private String merchantDba = "";
    private String productName = "";
    private String spiffType = "";
    private String commissionType = "";
    private String spiffAmount = "";
    private String activationAmount = "";
    private String sim = "";
    private String esn = "";
    private String pin = "";
    private Date activationDate;
    private String activationProduct = "";
    private String repName = "";
    private String subAgentName = "";
    private String agentName = "";
    private String carName = "";
    private String multiLine = "";
    private String filterAllIsos = "";
    private String isoName = "";
    private String dealerCode = "";

    /**
     * @return the multiLine
     */
    public String getMultiLine() {
        return multiLine;
    }

    /**
     * @param multiLine the multiLine to set
     */
    public void setMultiLine(String multiLine) {
        this.multiLine = multiLine;
    }

    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId;
    }

    public Long getActivationTrxId() {
        return activationTrxId;
    }

    public void setActivationTrxId(Long activationTrxId) {
        this.activationTrxId = activationTrxId;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantDba() {
        return merchantDba;
    }

    public void setMerchantDba(String merchantDba) {
        this.merchantDba = merchantDba;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSpiffType() {
        return spiffType;
    }

    public void setSpiffType(String spiffType) {
        this.spiffType = spiffType;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public String getActivationProduct() {
        return activationProduct;
    }

    public void setActivationProduct(String activationProduct) {
        this.activationProduct = activationProduct;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getEsn() {
        return esn;
    }

    public void setEsn(String esn) {
        this.esn = esn;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(String commissionType) {
        this.commissionType = commissionType;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getSubAgentName() {
        return subAgentName;
    }

    public void setSubAgentName(String subAgentName) {
        this.subAgentName = subAgentName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getFilterAllIsos() {
        return filterAllIsos;
    }

    public void setFilterAllIsos(String filterAllIsos) {
        this.filterAllIsos = filterAllIsos;
    }

    public String getIsoName() {
        return isoName;
    }

    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }
    
    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }
    

    @Override
    public List<CsvCell> cells() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cell1 = new CsvCell(activationTrxId.toString());
        list.add(cell1);
        CsvCell cell2 = new CsvCell(recId);
        list.add(cell2);
        CsvCell cellMultiLine = new CsvCell(this.multiLine);
        list.add(cellMultiLine);
        CsvCell cell3 = new CsvCell(merchantId.toString());
        list.add(cell3);
        CsvCell cell4 = new CsvCell((StringUtil.checkStr(dealerCode) ? dealerCode : ""));
        list.add(cell4);
        CsvCell cell5 = new CsvCell(merchantDba);
        list.add(cell5);
        CsvCell cell6 = new CsvCell(productName != null ? productName.trim() : "");
        list.add(cell6);
        CsvCell cell7 = new CsvCell(spiffType);
        list.add(cell7);
        CsvCell cell8 = new CsvCell(commissionType);
        list.add(cell8);
        CsvCell cell9 = new CsvCell(spiffAmount);
        list.add(cell9);
        CsvCell cell10 = new CsvCell(activationAmount);
        list.add(cell10);

        CsvCell cell11 = new CsvCell(sim);
        list.add(cell11);

        CsvCell cell12 = new CsvCell(esn);
        list.add(cell12);

        CsvCell cell13 = new CsvCell(pin);
        list.add(cell13);

        String formattedActivationDate = activationDate != null ? sdf.format(activationDate) : "";
        CsvCell cell14 = new CsvCell(formattedActivationDate);
        list.add(cell14);

        CsvCell cell15 = new CsvCell(activationProduct != null ? activationProduct.trim() : "");

        list.add(cell15);

        CsvCell cell16 = new CsvCell(repName);
        list.add(cell16);
        CsvCell cell17 = new CsvCell(subAgentName);
        list.add(cell17);
        CsvCell cell18 = new CsvCell(agentName);
        list.add(cell18);
        CsvCell cell19 = new CsvCell(carName);
        list.add(cell19);
        
        CsvCell cell20 = new CsvCell(isoName);
        list.add(cell20);
                
        return list;
    }

    public String getSpiffAmount() {
        return spiffAmount;
    }

    public void setSpiffAmount(String spiffAmount) {
        this.spiffAmount = spiffAmount;
    }

    public String getActivationAmount() {
        return activationAmount;
    }

    public void setActivationAmount(String activationAmount) {
        this.activationAmount = activationAmount;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

}
