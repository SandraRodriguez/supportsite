package net.emida.supportsite.reports.spiff.deatil.dao;

import java.util.Map;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.debisys.utils.DebisysConstants;

public class ActivationDetailQueryBuilder {

    private static final boolean QUERY_IS_COUNT = true;
    private static final boolean QUERY_NOT_IS_COUNT = false;

    private static final Logger log = LoggerFactory.getLogger(SpiffDetailQueryBuilder.class);

    private StringBuilder sb;

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;

    private String accessLevel;
    private String distChainType;
    private String refId;
    private String startDate;
    private String endDate;
    private String csvMerchantIds;
    private String paymentReference;
    private String sim;
    private String csvProviderIds;
    private boolean allIsos;

    public ActivationDetailQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.accessLevel = (String) queryData.get("accessLevel");
        this.distChainType = (String) queryData.get("distChainType");
        this.refId = (String) queryData.get("refId");
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.csvMerchantIds = (String) queryData.get("merchantIds");
        this.paymentReference = (String) queryData.get("paymentReference");
        this.sim = (String) queryData.get("sim");
        this.csvProviderIds = (String) queryData.get("providerIds");
        this.allIsos = Boolean.parseBoolean(""+queryData.get("filterAllIsos"));
    }

    public ActivationDetailQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";

        this.accessLevel = (String) queryData.get("accessLevel");
        this.distChainType = (String) queryData.get("distChainType");
        this.refId = (String) queryData.get("refId");
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.csvMerchantIds = (String) queryData.get("merchantIds");
        this.paymentReference = (String) queryData.get("paymentReference");
        this.sim = (String) queryData.get("sim");
        this.csvProviderIds = (String) queryData.get("providerIds");
        this.allIsos = Boolean.parseBoolean(""+queryData.get("filterAllIsos"));
    }

    public String buildCountQuery() throws Exception {
        sb = new StringBuilder();

        String queryStringCount;

        queryForTransactionsActiviationspiff(QUERY_IS_COUNT);

        queryStringCount = sb.toString();

        log.debug(queryStringCount);

        return queryStringCount;
    }

    public String buildQuery() throws Exception {
        String queryspiffTransactionActivation;

        sb = new StringBuilder();

        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "MerchantId";
            sortOrder = "ASC";
        }

        if ("paymentDate".equalsIgnoreCase(sortFieldName.trim())) {
            sortFieldName = "BlTransmitDate,McLogDatetime";
        }

        if ("paymentReference".equalsIgnoreCase(sortFieldName.trim())) {
            sortFieldName = "BlTraceNumber,McId";
        }
        queryForTransactionsActiviationspiff(QUERY_NOT_IS_COUNT);

        queryspiffTransactionActivation = sb.toString();

        return queryspiffTransactionActivation;

    }

    private String getProvidersIdStringfy(String pids) {
        String[] split = pids.split(",");
        StringBuilder sb = new StringBuilder();
        for (String pid : split) {
            sb.append("'").append(pid).append("',");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    private void queryHeadFields() {

        sb.append("		actTrx.rec_id AS ActivationTrxId, \n")
                .append("		isnull(CONVERT(VARCHAR(30),spTrx.rec_id),'NA') AS RecId, \n")
                .append("		isnull(spTrx.merchant_id, actTrx.merchant_id) AS MerchantID, \n")
                .append("		isnull(spTrx.dba, actTrx.dba) AS MerchantDBA, \n")
                .append("		isnull(CONVERT(VARCHAR(30),spTrx.description),'NA')  AS ProductName, \n")
                .append("		isnull(CONVERT(VARCHAR(30),spTypes.spifftypedescription),'NA')  AS SpiffType, \n")
                .append("		CASE \n")
                .append("			WHEN spTrx.rec_id IS NULL THEN 'NA' \n")
                .append("			ELSE 'ACTIVATION SPIFF' \n")
                .append("		END as CommissionType, \n")
                .append("		isnull(CONVERT(VARCHAR(30),spTrx.amount),'NA') AS SpiffAmount, \n")
                .append("		actTrx.amount AS ActivationAmount, \n")
                .append("		saTrx.sim AS SIM, \n")
                .append("		saTrx.esn AS ESN, \n")
                .append("		isnull(saTrx.pinAccount,'NA') AS PIN, \n")
                .append("		actTrx.datetime AS ActivationDate, \n")
                .append("		actPrd.description AS ActivationProduct, \n")
                .append("		rep.businessname AS RepName, \n")
                .append("		suba.businessname AS SubAgentName, \n")
                .append("		age.businessname AS AgentName, \n")
                .append("		car.Name As CarName, \n")
                .append("		ISNULL(saTrx.multiLineId,0) As MultiLineId, \n")
                .append("		iso.businessname AS IsoName, \n")
                .append("		dnapird.dealer_code AS dealerCode \n");
        
    }

    private void queryBodyjoins() {

        sb.append("	transactions actTransactions WITH ( NOLOCK ) \n")
                .append("	INNER JOIN web_transactions actTrx WITH ( NOLOCK ) ON actTransactions.rec_id = actTrx.rec_id \n")
                .append("	LEFT JOIN spiff_transactions spTrx WITH ( NOLOCK ) ON actTrx.rec_id=spTrx.related_rec_id \n")
                .append("	LEFT JOIN spiffsproducts spPrd WITH ( NOLOCK ) ON spPrd.ProductSpiff =  spTrx.id \n")
                .append("	LEFT JOIN SpiffTypes spTypes WITH (NOLOCK) ON spPrd.spifftype = spTypes.id \n")
                .append("	LEFT JOIN reps rep WITH ( NOLOCK ) ON rep.rep_id = actTrx.rep_id \n")
                .append("	INNER JOIN products actProd WITH ( NOLOCK ) ON actProd.id = actTrx.RecordedProductId \n")
                .append("	INNER JOIN carriers car WITH ( NOLOCK ) ON car.id = actProd.carrier_id \n")
                .append("	LEFT JOIN reps suba WITH ( NOLOCK ) ON suba.rep_id = rep.iso_id \n")
                .append("	LEFT JOIN reps age WITH ( NOLOCK ) ON age.rep_id = suba.iso_id \n")
                .append("	LEFT JOIN reps iso WITH ( NOLOCK ) ON iso.rep_id = age.iso_id \n")
                .append("	INNER JOIN terminals ter WITH ( NOLOCK ) ON ter.millennium_no = actTrx.millennium_no \n")
                .append("	INNER  JOIN products actPrd WITH ( NOLOCK ) ON actPrd.id = actTrx.recordedproductid \n")
                .append("	LEFT OUTER JOIN SimActivationTransactionsDetail saTrxDet WITH ( NOLOCK ) ON saTrxDet.transactionId = actTrx.rec_id \n")
                .append("	LEFT OUTER JOIN HelperTransactions hpTrx WITH ( NOLOCK ) ON hpTrx.EmidaTransactionId = actTrx.rec_id \n")
                .append("	LEFT OUTER JOIN SimActivationTransactions saTrx WITH ( NOLOCK ) ON (saTrx.idnew = saTrxDet.idsimactivationtransactions OR saTrx.idnew = hpTrx.IdNewSimActivations) \n")
                .append("	LEFT OUTER JOIN dn_api_registration dnapir WITH ( NOLOCK ) ON (actTrx.merchant_id = dnapir.merchant_id) \n")
                .append("	LEFT OUTER JOIN dn_api_registration_dealer dnapird WITH ( NOLOCK ) ON (dnapir.id = dnapird.api_registration_id) \n");
    }

    private void queryBasicWhere() {

        sb.append("WHERE (actTransactions.type = 1 or actTransactions.type = 2)  \n")
                .append("AND actTransactions.ach_type in (0, 11) \n")
                .append("AND actProd.Trans_Type in (1, 5, 6, 18, 20, 25) \n");
    }

    private void queryWhereMerchantsORISOlevelIfMerchantsIdsIsEmpty() {

        if (csvMerchantIds != null && !csvMerchantIds.isEmpty()) {
            sb.append("	AND actTrx.merchant_id \n")
                    .append("	IN(").append(csvMerchantIds).append(")  \n");
        } else {
            if(!allIsos){
                sb.append("	AND actTrx.merchant_id \n");

                if (accessLevel.equals(DebisysConstants.ISO)) {
                    sb.append("	IN(SELECT merchant_id \n");
                    if (distChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                        sb.append("		FROM merchants WITH(NOLOCK) \n")
                                .append("		WHERE rep_id \n")
                                .append("		IN (SELECT rep_id \n")
                                .append("			FROM reps WITH(NOLOCK) \n")
                                .append("			WHERE iso_id =").append(refId).append(")");

                    } else if (distChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        sb.append("		FROM merchants WITH(NOLOCK) \n")
                                .append("		WHERE rep_id \n")
                                .append("		IN (SELECT rep_id \n")
                                .append("			FROM reps WITH(NOLOCK) \n")
                                .append("			WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id \n")
                                .append("			IN(SELECT rep_id \n")
                                .append("				FROM reps WITH(NOLOCK) \n")
                                .append("				WHERE type =").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id \n")
                                .append("				IN( SELECT rep_id \n")
                                .append("					FROM reps WITH(NOLOCK) \n")
                                .append("					WHERE type =").append(DebisysConstants.REP_TYPE_AGENT).append(" AND iso_id =").append(refId).append(")))");
                    }
                    sb.append(") \n");
                } else if (accessLevel.equals(DebisysConstants.AGENT)) {
                    sb.append("	IN(SELECT merchant_id \n");
                    sb.append("		FROM merchants WITH(NOLOCK) \n")
                            .append("		WHERE rep_id \n")
                            .append("		IN (SELECT rep_id \n")
                            .append("			FROM reps WITH(NOLOCK) \n")
                            .append("			WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id \n")
                            .append("			IN(SELECT rep_id \n")
                            .append("		   	   FROM reps WITH(NOLOCK) \n")
                            .append("		   	   WHERE type =").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id =").append(refId).append("))");
                    sb.append(") \n");
                } else if (accessLevel.equals(DebisysConstants.SUBAGENT)) {
                    sb.append("	IN(SELECT merchant_id \n");
                    sb.append("		FROM merchants WITH(NOLOCK) \n")
                            .append("		WHERE rep_id \n")
                            .append("		IN (SELECT rep_id \n")
                            .append("			FROM reps WITH(NOLOCK) \n")
                            .append("			WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(refId).append(")");
                    sb.append(") \n");
                } else if (accessLevel.equals(DebisysConstants.REP)) {
                    sb.append("	IN(SELECT merchant_id \n");
                    sb.append("		FROM merchants WITH(NOLOCK) \n")
                            .append("		WHERE rep_id =").append(refId);
                    sb.append(") \n");
                } else if (accessLevel.equals(DebisysConstants.MERCHANT)) {
                    sb.append("=").append(refId);
                }

            }
        }

    }

    private void queryDatesRangeWhere() throws Exception {
        Vector vTimeZoneFilterDates = com.debisys.utils.TimeZone.getTimeZoneFilterDates(accessLevel, refId, startDate, endDate + " 23:59:59.999", false);
        sb.append("AND (actTrx.datetime >= '").append(vTimeZoneFilterDates.get(0)).append("' AND ").append("actTrx.datetime <= '").append(vTimeZoneFilterDates.get(1)).append("') \n");
    }

    private void queryProvidersWhere() {
        if (this.csvProviderIds != null && !csvProviderIds.isEmpty()) {
            sb.append(" AND car.IdNew IN (").append(getProvidersIdStringfy(csvProviderIds)).append(") ");
        }
    }

    private void qyerySimAnd() {
        if (!(this.sim.isEmpty() || this.sim == null)) {
            sb.append("AND saTrx.sim = '").append(this.sim).append("'\n");
        }
    }

    private void queryForTransactionsActiviationspiff(boolean isCountQuery) throws Exception {

        if (isCountQuery) {
            sb.append("SELECT COUNT(*) \n");
        } else {
            sb.append("SELECT  \n");
            queryHeadFields();
        }

        sb.append("FROM ");
        queryBodyjoins();
        queryBasicWhere();
        queryWhereMerchantsORISOlevelIfMerchantsIdsIsEmpty();
        queryDatesRangeWhere();
        queryProvidersWhere();
        qyerySimAnd();
    }

}
