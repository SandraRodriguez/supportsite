package net.emida.supportsite.reports.spiff.processHistory.dao;

import java.util.Map;
import net.emida.supportsite.reports.base.QueryBuilder;
import static net.emida.supportsite.reports.spiff.processHistory.dao.SpiffProcessHistoryReportConstants.PAID_BY_BILLNG;
import static net.emida.supportsite.reports.spiff.processHistory.dao.SpiffProcessHistoryReportConstants.PAID_BY_JOB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author fernandob
 */
public final class SpiffProcessHistoryReportQueryBuilder extends QueryBuilder {

    private static final Logger logger = LoggerFactory.getLogger(SpiffProcessHistoryReportQueryBuilder.class);

    // Property Names
    public static final String PIN = "pin";

    // attributes
    private String pin;

    @Override
    protected void getQueryData(Map<String, Object> queryData) {
        if (queryData != null) {
            logger.debug("Query Map = {}", queryData);
            super.getQueryData(queryData);

            if (queryData.containsKey(PIN)) {
                this.pin = (String) queryData.get(PIN);
            }
        }
    }

    public SpiffProcessHistoryReportQueryBuilder(Map<String, Object> queryData) {
        this.getQueryData(queryData);
    }

    public SpiffProcessHistoryReportQueryBuilder(int startPage, int endPage,
            String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.getQueryData(queryData);
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = String.format(" %s ", sortFieldName);
        this.sortOrder = String.format(" %s ", sortOrder);
    }

    private String generateCountQuery(String subQuery) {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");

        sb0.append(subQuery);

        sb0.append(") AS trxCount ");

        return sb0.toString();
    }

    public String generateTrxCountQuery() {
        return this.generateCountQuery(this.generateSpiffTransactionsByPin());
    }

    public String generateSpiffProcessCountQuery() {
        return this.generateCountQuery(this.generateSpiffProcessHistoryByPin());
    }

    private String generateSqlString(String subQuery, String defaultSortField) {
        sortFieldName = defaultSortField;
        sortOrder = "ASC";

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(String.format("%s %s", sortFieldName, sortOrder)).append(") AS RWN, * FROM (");

        sb0.append(subQuery);

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }

    public String generateSpiffTrxSqlString() {
        return this.generateSqlString(this.generateSpiffTransactionsByPin(), "trxDate");
    }

    public String generateSpiffHistorySqlString() {
        return this.generateSqlString(this.generateSpiffProcessHistoryByPin(), "processDate");
    }

    private String generateSpiffTransactionsByPin() {
        StringBuilder sbSpiffTransactions = new StringBuilder();
        sbSpiffTransactions.append(" SELECT WT.[datetime] As trxDate, WT.id As productId, ISNULL(PTT.[description], '') productTransType, ");
        sbSpiffTransactions.append("        BTT.descriptionBillingTransactionType As billingTransactionType, WT.rec_id As trxId, ");
        sbSpiffTransactions.append("        WT.related_rec_id As relatedTrxId, WT.control_no As controlNo, WT.amount, WT.rep_id As rep, ");
        sbSpiffTransactions.append("        WT.merchant_id As merchant, WT.millennium_no As terminal, ");
        sbSpiffTransactions.append(String.format("        CASE WHEN CPT.TransactionId IS NOT NULL THEN '%s' ", PAID_BY_JOB));
        sbSpiffTransactions.append(String.format("             WHEN B.recId IS NOT NULL THEN '%s' ", PAID_BY_BILLNG));
        sbSpiffTransactions.append("                           ELSE '' END  AS paidBy ");
        sbSpiffTransactions.append(" FROM   [dbo].[Allweb_transactions] WT WITH(NOLOCK) ");
        sbSpiffTransactions.append(String.format("        INNER JOIN dbo.Pins P WITH(NOLOCK) ON WT.control_no = P.control_no and P.pin = '%s'", this.pin));
        sbSpiffTransactions.append("        INNER JOIN dbo.BillingTransactionType BTT WITH(NOLOCK) ON WT.billing_typeid = BTT.idBillingTransactionType ");
        sbSpiffTransactions.append("        INNER JOIN  dbo.Products PR WITH(NOLOCK) ON WT.id = PR.id ");
        sbSpiffTransactions.append("        LEFT JOIN dbo.Product_Trans_Type PTT WITH(NOLOCK) ON PR.Trans_Type = PTT.transId ");
        sbSpiffTransactions.append("        LEFT JOIN [dbo].[CommissionProcessedTransactions] CPT WITH(NOLOCK) ON WT.rec_id = CPT.TransactionId ");
        sbSpiffTransactions.append("        LEFT JOIN [dbo].[Billing] B WITH(NOLOCK) ON WT.rec_id = B.recId AND B.entityTypeId = 8 AND B.transmitDate IS NOT NULL ");
        return sbSpiffTransactions.toString();
    }

    private String generateSpiffProcessHistoryByPin() {
        StringBuilder sbSpiffHistory = new StringBuilder();
        sbSpiffHistory.append(" SELECT	LFD.[emidaTrxId], LFD.[datetime], LFD.[OnFileTrxDate] fileDate,  ");
        sbSpiffHistory.append("         LFD.[DateTimeProcess] As processDate, CASE ");
        sbSpiffHistory.append("             WHEN (LFD.OnFileNonCommisionReason IS NULL) OR (LTRIM(RTRIM(LFD.OnFileNonCommisionReason)) = '') THEN ISNULL(LFRR.[rejectReason], '')  ");
        sbSpiffHistory.append("             ELSE LTRIM(RTRIM(LFD.OnFileNonCommisionReason)) END AS processResult, ");
        sbSpiffHistory.append("         ISNULL(RTRIM(LTRIM(LFD.[ReasonsInvalidRecord])), '') tracfoneRejectReason ");
        sbSpiffHistory.append(" FROM	[dbo].[LoadFileDetail] LFD WITH (NOLOCK) ");
        sbSpiffHistory.append("         LEFT JOIN [dbo].[LoadFileDetailRejectReasons] LFRR WITH(NOLOCK) ON LFD.CodeStatus = LFRR.[code] ");
        sbSpiffHistory.append(String.format(" WHERE   LFD.[OnFilePin] = '%s' ", this.pin));
        return sbSpiffHistory.toString();
    }

}
