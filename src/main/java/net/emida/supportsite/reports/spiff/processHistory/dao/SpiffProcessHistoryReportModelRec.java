package net.emida.supportsite.reports.spiff.processHistory.dao;

import java.util.List;
import java.util.Date;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvCell;

/**
 *
 * @author fernandob
 */
public class SpiffProcessHistoryReportModelRec implements CsvRow {

    private Long rowNum;
    private Long emidaTrxId;
    private Date datetime;
    private Date fileDate;
    private Date processDate;
    private String processResult;
    private String tracfoneRejectReason;

    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public Long getEmidaTrxId() {
        return emidaTrxId;
    }

    public void setEmidaTrxId(Long emidaTrxId) {
        this.emidaTrxId = emidaTrxId;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Date getFileDate() {
        return fileDate;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public String getProcessResult() {
        return processResult;
    }

    public void setProcessResult(String processResult) {
        this.processResult = processResult;
    }

    public String getTracfoneRejectReason() {
        return tracfoneRejectReason;
    }

    public void setTracfoneRejectReason(String tracfoneRejectReason) {
        this.tracfoneRejectReason = tracfoneRejectReason;
    }

    public SpiffProcessHistoryReportModelRec() {
        this.rowNum = 0L;
        this.emidaTrxId = 0L;
        this.datetime = new Date();
        this.fileDate = new Date();
        this.processDate = new Date();
        this.processResult = "";
        this.tracfoneRejectReason = "";
    }

    public SpiffProcessHistoryReportModelRec(Long rowNum, Long emidaTrxId, Date datetime,
            Date fileDate, Date dateTimeProcess, String processResult,
            String tracfoneRejectReason) {
        this.rowNum = rowNum;
        this.emidaTrxId = emidaTrxId;
        this.datetime = datetime;
        this.fileDate = fileDate;
        this.processDate = dateTimeProcess;
        this.processResult = processResult;
        this.tracfoneRejectReason = tracfoneRejectReason;
    }

    @Override
    public String toString() {
        return "SpiffProcessHistoryReportModel{" + "rowNum=" + rowNum + ", emidaTrxId=" + emidaTrxId + ", datetime=" + datetime + ", fileDate=" + fileDate + ", dateTimeProcess=" + processDate + ", processResultCode=" + processResult + ", tracfoneRejectReason=" + tracfoneRejectReason + '}';
    }

    @Override
    public List<CsvCell> cells() {
        List<CsvCell> retValue = new ArrayList<CsvCell>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Row number
        CsvCell cell = new CsvCell(this.rowNum.toString());
        retValue.add(cell);
        // Emida transaction id
        cell = new CsvCell(this.emidaTrxId.toString());
        retValue.add(cell);
        // Load file record date 
        String formattedRecordDate = this.datetime != null ? sdf.format(this.datetime) : "";
        cell = new CsvCell(formattedRecordDate);
        retValue.add(cell);
        // File date
        String formattedFileDate = this.fileDate != null ? sdf.format(this.fileDate) : "";
        cell = new CsvCell(formattedFileDate);
        retValue.add(cell);
        // Process date
        String formattedProcessDate = this.processDate != null ? sdf.format(this.processDate) : "";
        cell = new CsvCell(formattedProcessDate);
        retValue.add(cell);
        // Process result
        cell = new CsvCell(this.processResult);
        retValue.add(cell);
        // Tracfone reject reason
        cell = new CsvCell(this.tracfoneRejectReason);
        retValue.add(cell);
        return retValue;
    }

}
