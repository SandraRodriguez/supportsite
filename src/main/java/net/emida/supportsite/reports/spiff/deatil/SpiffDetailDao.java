package net.emida.supportsite.reports.spiff.deatil;

import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
public interface SpiffDetailDao {

    List<SpiffDetailReportModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    Long count(Map<String, Object> queryData) throws TechnicalException;

    Long countAll() throws TechnicalException;

    List<Map<String, Object>> listSpiffProviders() throws TechnicalException;

}
