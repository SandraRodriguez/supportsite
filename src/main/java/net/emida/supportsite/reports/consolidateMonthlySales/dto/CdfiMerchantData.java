/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.consolidateMonthlySales.dto;

import java.io.Serializable;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
public class CdfiMerchantData implements Serializable {

    private long merchantId;
    private String merchantName;
//    private long IsoId;
    private String IsoId;
    private String legalBusinessName;
    private String taxId;
    private String address;
    private String colony;
    private String delegation;
    private String city;
    private String state;
    private String country;
    private String zip;
    private String email;
    private String phone;
    private String contact;
    private String invoiceType;
    private String depositType;
    private String accountNumber;
    private double iniBalance;
    private double finalBalance;

    public CdfiMerchantData() {
    }

    /**
     *
     * @param merchantId
     * @param merchantName
     * @param IsoId
     * @param legalBusinessName
     * @param taxId
     * @param address
     * @param colony
     * @param delegation
     * @param city
     * @param state
     * @param country
     * @param zip
     * @param email
     * @param phone
     * @param contact
     * @param invoiceType
     * @param depositType
     * @param accountNumber
     * @param iniBalance
     * @param finalBalance
     */
    public CdfiMerchantData(long merchantId, String merchantName, String IsoId, String legalBusinessName, String taxId, String address, String colony, String delegation, String city, String state, String country, String zip, String email, String phone, String contact, String invoiceType, String depositType, String accountNumber, double iniBalance, double finalBalance) {
        this.merchantId = merchantId;
        this.merchantName = merchantName;
        this.IsoId = IsoId;
        this.legalBusinessName = legalBusinessName;
        this.taxId = taxId;
        this.address = address;
        this.colony = colony;
        this.delegation = delegation;
        this.city = city;
        this.state = state;
        this.country = country;
        this.zip = zip;
        this.email = email;
        this.phone = phone;
        this.contact = contact;
        this.invoiceType = invoiceType;
        this.depositType = depositType;
        this.accountNumber = accountNumber;
        this.iniBalance = iniBalance;
        this.finalBalance = finalBalance;
    }

    public long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(long merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getIsoId() {
        return IsoId;
    }

    public void setIsoId(String IsoId) {
        this.IsoId = IsoId;
    }

    public String getLegalBusinessName() {
        return legalBusinessName;
    }

    public void setLegalBusinessName(String legalBusinessName) {
        this.legalBusinessName = legalBusinessName;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getColony() {
        return colony;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getIniBalance() {
        return iniBalance;
    }

    public void setIniBalance(double iniBalance) {
        this.iniBalance = iniBalance;
    }

    public double getFinalBalance() {
        return finalBalance;
    }

    public void setFinalBalance(double finalBalance) {
        this.finalBalance = finalBalance;
    }

    public String getDelegation() {
        return delegation;
    }

    public void setDelegation(String delegation) {
        this.delegation = delegation;
    }

}
