/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.consolidateMonthlySales.repository;

import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiCarrierDataConsolidate;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiCarrierMappingDto;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiMerchantConsolidateDto;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiMerchantData;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiProduct;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CollectorProductTransactionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
@Repository("IConsolidateMonthlySalesDao")
public class ConsolidateMonthlySalesRepository implements IConsolidateMonthlySalesDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsolidateMonthlySalesRepository.class);

    private static final String FIND_CARRIER_MAPPING = "select carrier_description from CdfiCarrierMapping with(nolock) where enabled = 1 GROUP by carrier_description";
    private static final String FIND_ALL_PRODUCTS_BY_CARRIERS = "select cm.id, cm.product_id, cm.enabled, p.description as carrier_description from CdfiCarrierMapping cm INNER JOIN products p ON cm.product_id = p.id where cm.carrier_description = ";
    private static final String FIND_CARRIERS_INFORMATION = "SELECT carrier_description, COUNT(product_id) AS count_product FROM CdfiCarrierMapping GROUP BY carrier_description";
    private static final String FIND_EXCLUDES_PRODUCTS = "select p.id, p.description FROM products p LEFT OUTER join CdfiCarrierMapping cm ON p.id = cm.product_id where p.description like ? and cm.product_id is null";
    private static final String INSERT_CARRIER_MAPPING = "INSERT INTO CdfiCarrierMapping(carrier_description, product_id, enabled) VALUES(?,?,1)";
    private static final String FIND_CARRIERS = "select c.NAme FROM Carriers c LEFT join CdfiCarrierMapping cm ON c.NAme = cm.carrier_description where c.Name <> '' and  cm.carrier_description is null order by c.Name";

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ServletContext context;

    private Map<String, CollectorProductTransactionDto> mapFromTotals;

    @Override
    public Integer updateProductStatus(int enabled, String id) {
        return this.jdbcTemplate.update("UPDATE CdfiCarrierMapping SET enabled = ? WHERE id = ?", enabled, id);
    }

    @Override
    public Integer deleteProductStatus(String id) {
        return this.jdbcTemplate.update("DELETE CdfiCarrierMapping WHERE id = ?", id);
    }

    @Override
    public Integer updateCarrierDescription(String carrier_description_old, String carrier_description_new) {
        return this.jdbcTemplate.update("UPDATE CdfiCarrierMapping SET carrier_description = ? WHERE carrier_description = ?", carrier_description_new, carrier_description_old);
    }

    @Override
    public Integer addCarrier(String carrier_description) {
        return this.jdbcTemplate.update("INSERT INTO CdfiCarrierMapping(carrier_description, enabled) VALUES(?, 0)", carrier_description);
    }

    @Override
    public Integer deleteCarrier(String carrier_description) {
        return this.jdbcTemplate.update("DELETE CdfiCarrierMapping WHERE carrier_description = ?", carrier_description);
    }

    @Override
    public Integer addProductMapping(String carrier_description, String product_id) {
        String[] productArray = product_id.split(",");
        for (String productId : productArray) {
            this.jdbcTemplate.update(INSERT_CARRIER_MAPPING, carrier_description, productId);
        }
        return productArray.length;
    }

    /**
     *
     * @param periodDt
     * @param entityType
     * @return
     */
    @Override
    public List<CdfiMerchantConsolidateDto> findDataConsolodateByProvider(Date periodDt, int entityType, int operation) {
        return consolidateResponser(periodDt, entityType, operation);
    }

    /**
     *
     * @param periodDt
     * @param entityType
     * @return
     */
    private List<CdfiMerchantConsolidateDto> consolidateResponser(Date periodDt, int entityType, int operation) {
        List<CdfiMerchantConsolidateDto> responser = new ArrayList<CdfiMerchantConsolidateDto>();
        List<CdfiCarrierDataConsolidate> tmp = null;
        Map<Long, CdfiMerchantData> content = null;

        content = findGeneralInformation(periodDt, entityType);

        if (operation == 2) {
            mapFromTotals = getCollectorCounterProductTransaction(periodDt, entityType);
        }

        List<CdfiCarrierMappingDto> listCarrierMapping = findCarrierMapping();

        if (content != null) {
            for (Map.Entry<Long, CdfiMerchantData> entry : content.entrySet()) {
                CdfiMerchantData element = entry.getValue();
                tmp = new ArrayList<CdfiCarrierDataConsolidate>();
                if (operation == 2) {
                    for (CdfiCarrierMappingDto carrierMap : listCarrierMapping) {
                        try {
                            CdfiCarrierDataConsolidate carrierDataConsolidate = getTotalizerObject(element.getMerchantId(), carrierMap.getCarrier_description());
                            tmp.add((carrierDataConsolidate != null ? carrierDataConsolidate : new CdfiCarrierDataConsolidate()));
                        } catch (org.springframework.dao.EmptyResultDataAccessException e) {
                            LOGGER.error(e.getLocalizedMessage());
                            tmp.add(new CdfiCarrierDataConsolidate());
                        }
                    }
                }
                responser.add(new CdfiMerchantConsolidateDto(element, tmp));
            }
        }
        return responser;
    }

    private CollectorProductTransactionDto getTotalizerObject(long merchantId, String carrierDescription) {
        return mapFromTotals.get(String.valueOf(merchantId) + carrierDescription);
    }

    public Map<String, CollectorProductTransactionDto> getCollectorCounterProductTransaction(Date periodDt, int entityType) {
        Map<String, CollectorProductTransactionDto> map = new HashMap<String, CollectorProductTransactionDto>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            preparedStatement = this.jdbcTemplate.getDataSource().getConnection().prepareCall("EXEC getTotalCollectorProductTransaction ?,?,?,?");
            preparedStatement.setString(1, firstDay(periodDt));
            preparedStatement.setString(2, getLastDay(periodDt));
            preparedStatement.setInt(3, entityType);
            preparedStatement.setDouble(4, getMxValueAddedTax());

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                map.put(resultSet.getString("merchant_id") + resultSet.getString("carrier_description"), new CollectorProductTransactionDto(
                        resultSet.getString("carrier_description"),
                        resultSet.getLong("merchant_id"),
                        resultSet.getLong("total_transactions"),
                        resultSet.getDouble("total_refills"),
                        resultSet.getDouble("merchant_commission"),
                        resultSet.getDouble("total_client_invoice"),
                        resultSet.getDouble("total_client_invoice_not_iva")));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        } finally {
            TorqueHelper.closeConnection(null, preparedStatement, resultSet);
        }
        return map;
    }

    private Map<Long, CdfiMerchantData> findGeneralInformation(Date periodDt, int entityType) {
        Map<Long, CdfiMerchantData> map = new HashMap<Long, CdfiMerchantData>();
        List<CdfiMerchantData> findMerchants = new ArrayList<CdfiMerchantData>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = this.jdbcTemplate.getDataSource().getConnection();
            preparedStatement = connection.prepareCall("EXEC getCollectorProductTransaction ?,?,?");
            preparedStatement.setString(1, firstDay(periodDt));
            preparedStatement.setString(2, getLastDay(periodDt));
            preparedStatement.setInt(3, entityType);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                findMerchants.add(new CdfiMerchantData(resultSet.getLong("merchant_id"), resultSet.getString("dba"),
                        getPrefix(resultSet.getString("dba")), resultSet.getString("legal_businessname"),
                        resultSet.getString("tax_id"), resultSet.getString("phys_address"), resultSet.getString("phys_colony"), resultSet.getString("phys_county"),
                        resultSet.getString("phys_city"), resultSet.getString("phys_state"), resultSet.getString("phys_country"),
                        resultSet.getString("phys_zip"), resultSet.getString("email"), resultSet.getString("contact_phone"), resultSet.getString("contact"),
                        resultSet.getString("invoiceType"), resultSet.getString("depositType"), resultSet.getString("accountNumber"), resultSet.getDouble("iniBalance"), resultSet.getDouble("finalBalance")));
            }
            for (CdfiMerchantData merchant : findMerchants) {
                map.put(merchant.getMerchantId(), merchant);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex.getLocalizedMessage());
        } finally {
            TorqueHelper.closeConnection(connection, preparedStatement, resultSet);
        }
        return map;
    }

    @Override
    public List<CdfiCarrierMappingDto> findCarrierMappingInf() {
        return this.jdbcTemplate.query(FIND_CARRIERS_INFORMATION,
                new RowMapper<CdfiCarrierMappingDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public CdfiCarrierMappingDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new CdfiCarrierMappingDto(resultSet.getString("carrier_description"), resultSet.getInt("count_product"));
            }
        });
    }

    @Override
    public List<CdfiCarrierMappingDto> findProductsByCarrier(String carrierDescription) {
        Map<Integer, CdfiCarrierMappingDto> map = new HashMap<Integer, CdfiCarrierMappingDto>();
        StringBuilder builder = new StringBuilder();
        builder.append(FIND_ALL_PRODUCTS_BY_CARRIERS).append("'").append(carrierDescription).append("'");
        List<CdfiCarrierMappingDto> content = this.jdbcTemplate.query(builder.toString(), new RowMapper<CdfiCarrierMappingDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public CdfiCarrierMappingDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new CdfiCarrierMappingDto(
                        resultSet.getString("id"),
                        resultSet.getString("carrier_description"),
                        resultSet.getInt("product_id"),
                        resultSet.getBoolean("enabled"));
            }
        });
        for (CdfiCarrierMappingDto cdfiCarrierMappingDto : content) {
            map.put(cdfiCarrierMappingDto.getProduct(), cdfiCarrierMappingDto);
        }
        List<CdfiCarrierMappingDto> result = new ArrayList<CdfiCarrierMappingDto>();
        for (Map.Entry<Integer, CdfiCarrierMappingDto> entry : map.entrySet()) {
            CdfiCarrierMappingDto value = entry.getValue();
            result.add(value);
        }
        return result;
    }

    @Override
    public List<CdfiCarrierMappingDto> findCarrierMapping() {
        return this.jdbcTemplate.query(FIND_CARRIER_MAPPING, new RowMapper<CdfiCarrierMappingDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public CdfiCarrierMappingDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new CdfiCarrierMappingDto(resultSet.getString("carrier_description"));
            }
        });
    }

    @Override
    public List<CdfiProduct> findProductsByCarrierDescription(String description) {
        return this.jdbcTemplate.query(FIND_EXCLUDES_PRODUCTS, new Object[]{
            "%" + description + "%"
        },
                new RowMapper<CdfiProduct>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public CdfiProduct mapRow(ResultSet resultSet, int index) throws SQLException {
                return new CdfiProduct(resultSet.getInt("id"), resultSet.getString("description"));
            }
        });
    }

    @Override
    public List<CdfiCarrierMappingDto> findDiffCarriers() {
        return this.jdbcTemplate.query(FIND_CARRIERS, new Object[]{},
                new RowMapper<CdfiCarrierMappingDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public CdfiCarrierMappingDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new CdfiCarrierMappingDto(resultSet.getString("Name"));
            }
        });
    }

    private String getPrefix(String dba) {
        for (int i = 0; i < dba.length(); i++) {
            if (dba.charAt(i) == '-') {
                return dba.substring(0, i);
            }
        }
        return "--";
    }

    private String firstDay(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    private String getLastDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);

        Date lastDayOfMonth = calendar.getTime();

        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return sdf.format(lastDayOfMonth);
    }

    public double getMxValueAddedTax() {
        return 1 + (Double.parseDouble(context.getAttribute("mxValueAddedTax").toString())) / 100;
    }

}
