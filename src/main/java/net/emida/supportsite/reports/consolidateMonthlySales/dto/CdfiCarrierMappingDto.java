/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.consolidateMonthlySales.dto;

import java.io.Serializable;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
public class CdfiCarrierMappingDto implements Serializable {

    private String id;
    private String carrier_description;
    private int product;
    private boolean enabled;

    public CdfiCarrierMappingDto() {
    }

    public CdfiCarrierMappingDto(String carrier_description) {
        this.carrier_description = carrier_description;
    }

    /**
     *
     * @param carrier_description
     * @param product
     */
    public CdfiCarrierMappingDto(String carrier_description, int product) {
        this.carrier_description = carrier_description;
        this.product = product;
    }

    /**
     *
     * @param id
     * @param carrier_description
     * @param product
     * @param enabled
     */
    public CdfiCarrierMappingDto(String id, String carrier_description, int product, boolean enabled) {
        this.id = id;
        this.carrier_description = carrier_description;
        this.product = product;
        this.enabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCarrier_description() {
        return carrier_description;
    }

    public void setCarrier_description(String carrier_description) {
        this.carrier_description = carrier_description;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
