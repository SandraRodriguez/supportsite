/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.consolidateMonthlySales.controller;

import au.com.bytecode.opencsv.CSVWriter;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiCarrierDataConsolidate;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiCarrierMappingDto;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiMerchantConsolidateDto;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiProduct;
import net.emida.supportsite.reports.consolidateMonthlySales.repository.IConsolidateMonthlySalesDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class ConsolidateMonthlySalesController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsolidateMonthlySalesController.class);

    @Autowired
    private IConsolidateMonthlySalesDao consolidateMonthlySalesDao;

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT, method = RequestMethod.GET)
    public String __init__() {
        return "reports/consolidateMonthlySales/consolidateMonthlySales";
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/consolidate", method = RequestMethod.GET)
    @ResponseBody
    public List<CdfiMerchantConsolidateDto> findMerchantDataConsolodateByProvider(
            @RequestParam(value = "periodDt") String periodDt, @RequestParam(value = "entityType") int entityType, @RequestParam(value = "operation") int operation,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.findDataConsolodateByProvider(parseStringToDate(periodDt), entityType, operation);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/findACarrierMappingInf", method = RequestMethod.GET)
    @ResponseBody
    public List<CdfiCarrierMappingDto> findACarrierMappingInf(
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.findCarrierMappingInf();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/findProductsByCarrier", method = RequestMethod.GET)
    @ResponseBody
    public List<CdfiCarrierMappingDto> findProductsByCarrier(@RequestParam(value = "carrierDescription") String carrierDescription,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.findProductsByCarrier(carrierDescription);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/updateProductStatus", method = RequestMethod.POST)
    @ResponseBody
    public Integer updateProductStatus(@RequestParam(value = "enabled") int enabled, @RequestParam(value = "id") String id,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.updateProductStatus(enabled, id);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/addCarrier", method = RequestMethod.POST)
    @ResponseBody
    public Integer addCarrier(@RequestParam(value = "carrier_description") String carrier_description,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.addCarrier(carrier_description);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/deleteCarrier", method = RequestMethod.POST)
    @ResponseBody
    public Integer deleteCarrier(@RequestParam(value = "carrier_description") String carrier_description,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.deleteCarrier(carrier_description);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/findProductsByCarrierDescription", method = RequestMethod.GET)
    @ResponseBody
    public List<CdfiProduct> findProductsByCarrierDescription(@RequestParam(value = "description") String description,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.findProductsByCarrierDescription(description);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/updateCarrierDescription", method = RequestMethod.POST)
    @ResponseBody
    public Integer updateCarrierDescription(@RequestParam(value = "carrier_description_old") String carrier_description_old, @RequestParam(value = "carrier_description_new") String carrier_description_new,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.updateCarrierDescription(carrier_description_old, carrier_description_new);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/deleteProductStatus", method = RequestMethod.POST)
    @ResponseBody
    public Integer deleteProductStatus(@RequestParam(value = "id") String id,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.deleteProductStatus(id);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/addProductMapping", method = RequestMethod.POST)
    @ResponseBody
    public Integer addProductMapping(@RequestParam(value = "carrier_description") String carrier_description, @RequestParam(value = "product_id") String product_id,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.addProductMapping(carrier_description, product_id);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return -1;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/findAllCarrierMapping", method = RequestMethod.GET)
    @ResponseBody
    public List<CdfiCarrierMappingDto> findAllCarrierMapping(
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.findCarrierMapping();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/findDiffCarriers", method = RequestMethod.GET)
    @ResponseBody
    public List<CdfiCarrierMappingDto> findDiffCarriers(
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return consolidateMonthlySalesDao.findDiffCarriers();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT + "/csv", method = RequestMethod.GET)
    @ResponseBody
    public void downloadCSVReport_HTTP_GET(
            @RequestParam(value = "periodDt") String periodDt, @RequestParam(value = "entityType") int entityType, @RequestParam(value = "operation") int operation,
            Model model, Locale locale, HttpServletRequest req, HttpSession httpSession, HttpServletResponse response) {
        CSVWriter writer = null;
        try {
            String encoder = System.getProperty("file.encoding");            
            writer = new CSVWriter(new OutputStreamWriter(response.getOutputStream()));
            buildCSVReport(writer, parseStringToDate(periodDt), entityType, operation);
            String fileName = (entityType == 8 ? "creditoIndividual" : "creditoCompartido");
            response.addHeader("Content-Disposition", "attachment;filename=" + fileName + "_" + periodDt + ".csv");
            response.setCharacterEncoding(encoder);
            response.setHeader("Content-Type", "text/csv;" + encoder);
            response.flushBuffer();
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                LOGGER.error(ex.getLocalizedMessage());
            }
        }
    }

    private void buildCSVReport(CSVWriter writer, Date periodDt, int entityType, int operation) {
        List<CdfiMerchantConsolidateDto> list = consolidateMonthlySalesDao.findDataConsolodateByProvider(periodDt, entityType, operation);
        List<String[]> data = new ArrayList<String[]>();
        buildHeaderReport(data);
        for (int indez = 0; indez < list.size(); indez++) {
            String[] total = new String[(20 + (list.get(indez).getCarrierDataConsolidateList().size() * 5))];

            total[0] = String.valueOf(list.get(indez).getMerchantData().getMerchantId());
            total[1] = list.get(indez).getMerchantData().getMerchantName();
            total[2] = String.valueOf(list.get(indez).getMerchantData().getIsoId());
            total[3] = list.get(indez).getMerchantData().getLegalBusinessName();
            total[4] = list.get(indez).getMerchantData().getTaxId();
            total[5] = list.get(indez).getMerchantData().getAddress();
            total[6] = (list.get(indez).getMerchantData().getColony() == null ? "" : list.get(indez).getMerchantData().getColony());
            total[7] = (list.get(indez).getMerchantData().getCity() == null ? "" : list.get(indez).getMerchantData().getCity());
            total[8] = list.get(indez).getMerchantData().getState();
            total[9] = list.get(indez).getMerchantData().getDelegation();
            total[10] = list.get(indez).getMerchantData().getCountry();
            total[11] = list.get(indez).getMerchantData().getZip();
            total[12] = list.get(indez).getMerchantData().getEmail();
            total[13] = list.get(indez).getMerchantData().getContact();
            total[14] = list.get(indez).getMerchantData().getPhone();
            total[15] = list.get(indez).getMerchantData().getInvoiceType();
            total[16] = String.valueOf(round(list.get(indez).getMerchantData().getIniBalance()));

            List<CdfiCarrierDataConsolidate> dataConsolidates = list.get(indez).getCarrierDataConsolidateList();
            int index = 16;
            for (CdfiCarrierDataConsolidate dataConsolidate : dataConsolidates) {
                for (int indej = 1; indej < 6; indej++) {
                    switch (indej) {
                        case 1:
                            total[index + indej] = String.valueOf(dataConsolidate.getTotalTransaction());
                            break;
                        case 2:
                            total[index + indej] = String.valueOf(round(dataConsolidate.getTotalRefills()));
                            break;
                        case 3:
                            total[index + indej] = String.valueOf(round(dataConsolidate.getTotalComision()));
                            break;
                        case 4:
                            total[index + indej] = String.valueOf(round(dataConsolidate.getTotalClientInvoice()));
                            break;
                        case 5:
                            total[index + indej] = String.valueOf(round(dataConsolidate.getTotalClientInvoiceNotIva()));
                            break;
                    }
                }
                index += 5;
            }
            total[++index] = String.valueOf(round(list.get(indez).getMerchantData().getFinalBalance()));
            total[++index] = list.get(indez).getMerchantData().getDepositType();
            total[++index] = String.valueOf((list.get(indez).getMerchantData().getAccountNumber() != null ? list.get(indez).getMerchantData().getAccountNumber() : "-"));
            data.add(total);
        }
        writer.writeAll(data);
    }

    private void buildHeaderReport(List<String[]> data) {
        SessionData sessionData = getSessionData();        
        List<CdfiCarrierMappingDto> listCarrierMapping = consolidateMonthlySalesDao.findCarrierMapping();
        String[] total = new String[(20 + (listCarrierMapping.size() * 5))];
        total[0] = Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col1", sessionData.getLanguage());
        total[1] = Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col3", sessionData.getLanguage());
        total[2] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col2", sessionData.getLanguage());
        total[3] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col3", sessionData.getLanguage());
        total[4] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col4", sessionData.getLanguage());
        total[5] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col5", sessionData.getLanguage());
        total[6] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col6", sessionData.getLanguage());
        total[7] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col7", sessionData.getLanguage());
        total[8] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col8", sessionData.getLanguage());
        total[9] = Languages.getString("jsp.report.consolidate.monthly.sales.tbl.col13", sessionData.getLanguage());
        total[10] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col9", sessionData.getLanguage());
        total[11] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col10", sessionData.getLanguage());
        total[12] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col11", sessionData.getLanguage());
        total[13] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col12", sessionData.getLanguage());
        total[14] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col13", sessionData.getLanguage());
        total[15] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col14", sessionData.getLanguage());
        total[16] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col15", sessionData.getLanguage());

        int index = 16;
        for (CdfiCarrierMappingDto carrier : listCarrierMapping) {
            for (int indej = 1; indej < 6; indej++) {
                switch (indej) {
                    case 1:
                        total[index + indej] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.cant", sessionData.getLanguage()) + " " + carrier.getCarrier_description();
                        break;
                    case 2:
                        total[index + indej] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.refills", sessionData.getLanguage()) + " " + carrier.getCarrier_description();
                        break;
                    case 3:
                        total[index + indej] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.percent", sessionData.getLanguage()) + " " + carrier.getCarrier_description();
                        break;
                    case 4:
                        total[index + indej] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.afc", sessionData.getLanguage());
                        break;
                    case 5:
                        total[index + indej] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.afcnotiva", sessionData.getLanguage());
                        break;
                }
            }
            index += 5;
        }
        total[++index] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.finalBalance", sessionData.getLanguage());
        total[++index] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.depositType", sessionData.getLanguage());
        total[++index] = Languages.getString("jsp.report.consolidate.monthly.sales.csv.col.accountNumber", sessionData.getLanguage());
        data.add(total);
    }

    private Date parseStringToDate(String currentDt) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(currentDt);
        } catch (ParseException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return null;
        }
    }

    public static double round(double value) {
        long factor = (long) Math.pow(10, 2);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }

}
