/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.consolidateMonthlySales.dto;

import java.io.Serializable;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
public class CdfiCarrierDataConsolidate implements Serializable {

    private long totalTransaction;
    private double totalRefills;
    private double totalComision;
    private double totalClientInvoice;
    private double totalClientInvoiceNotIva;

    public CdfiCarrierDataConsolidate() {
    }

    /**
     *
     * @param totalTransaction
     * @param totalRefills
     * @param totalComision
     * @param totalClientInvoice
     * @param totalClientInvoiceNotIva
     */
    public CdfiCarrierDataConsolidate(long totalTransaction, double totalRefills, double totalComision, double totalClientInvoice, double totalClientInvoiceNotIva) {
        this.totalTransaction = totalTransaction;
        this.totalRefills = totalRefills;
        this.totalComision = totalComision;
        this.totalClientInvoice = totalClientInvoice;
        this.totalClientInvoiceNotIva = totalClientInvoiceNotIva;
    }

    public long getTotalTransaction() {
        return totalTransaction;
    }

    public void setTotalTransaction(long totalTransaction) {
        this.totalTransaction = totalTransaction;
    }

    public double getTotalRefills() {
        return totalRefills;
    }

    public void setTotalRefills(double totalRefills) {
        this.totalRefills = totalRefills;
    }

    public double getTotalComision() {
        return totalComision;
    }

    public void setTotalComision(double totalComision) {
        this.totalComision = totalComision;
    }

    public double getTotalClientInvoice() {
        return totalClientInvoice;
    }

    public void setTotalClientInvoice(double totalClientInvoice) {
        this.totalClientInvoice = totalClientInvoice;
    }

    public double getTotalClientInvoiceNotIva() {
        return totalClientInvoiceNotIva;
    }

    public void setTotalClientInvoiceNotIva(double totalClientInvoiceNotIva) {
        this.totalClientInvoiceNotIva = totalClientInvoiceNotIva;
    }

}
