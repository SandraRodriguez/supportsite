package net.emida.supportsite.reports.consolidateMonthlySales.dto;

import java.io.Serializable;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
public class CollectorProductTransactionDto extends CdfiCarrierDataConsolidate implements Serializable {

    private String carrier_description;
    private long merchant_id;

    public CollectorProductTransactionDto() {
    }

    /**
     *
     * @param carrier_description
     * @param merchant_id
     */
    public CollectorProductTransactionDto(String carrier_description, long merchant_id) {
        this.carrier_description = carrier_description;
        this.merchant_id = merchant_id;
    }

    /**
     *
     * @param carrier_description
     * @param merchant_id
     * @param totalTransaction
     * @param totalRefills
     * @param totalComision
     * @param totalClientInvoice
     * @param totalClientInvoiceNotIva
     */
    public CollectorProductTransactionDto(String carrier_description, long merchant_id, long totalTransaction, double totalRefills, double totalComision, double totalClientInvoice, double totalClientInvoiceNotIva) {
        super(totalTransaction, totalRefills, totalComision, totalClientInvoice, totalClientInvoiceNotIva);
        this.carrier_description = carrier_description;
        this.merchant_id = merchant_id;
    }

    public String getCarrier_description() {
        return carrier_description;
    }

    public void setCarrier_description(String carrier_description) {
        this.carrier_description = carrier_description;
    }

    public long getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(long merchant_id) {
        this.merchant_id = merchant_id;
    }

}
