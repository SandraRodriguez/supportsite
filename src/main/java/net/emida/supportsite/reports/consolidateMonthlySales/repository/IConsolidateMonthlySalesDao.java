/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.consolidateMonthlySales.repository;

import java.util.Date;
import java.util.List;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiCarrierMappingDto;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiMerchantConsolidateDto;
import net.emida.supportsite.reports.consolidateMonthlySales.dto.CdfiProduct;

/**
 *
 * @author janez - janez@emida.net
 * @since 10/18/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-4208
 */
public interface IConsolidateMonthlySalesDao {

    /**
     *
     * @return
     */
    public List<CdfiCarrierMappingDto> findCarrierMappingInf();

    /**
     *
     * @param carrierDescription
     * @return
     */
    public List<CdfiCarrierMappingDto> findProductsByCarrier(String carrierDescription);

    /**
     *
     * @param carrier_description
     * @param product_id
     * @return
     */
    public Integer addProductMapping(String carrier_description, String product_id);

    /**
     *
     * @param enabled
     * @param id
     * @return
     */
    public Integer updateProductStatus(int enabled, String id);

    /**
     *
     * @param id
     * @return
     */
    public Integer deleteProductStatus(String id);

    /**
     *
     * @param carrier_description
     * @return
     */
    public Integer addCarrier(String carrier_description);

    /**
     *
     * @param carrier_description_old
     * @param carrier_description_new
     * @return
     */
    public Integer updateCarrierDescription(String carrier_description_old, String carrier_description_new);

    /**
     *
     * @param carrier_description
     * @return
     */
    public Integer deleteCarrier(String carrier_description);

    /**
     *
     * @return
     */
    public List<CdfiCarrierMappingDto> findCarrierMapping();

    /**
     *
     * @param description
     * @return
     */
    public List<CdfiProduct> findProductsByCarrierDescription(String description);

    /**
     * 
     * @param periodDt
     * @param entityType
     * @param operation 1 normal report - 2 CSV Report
     * @return 
     */
    public List<CdfiMerchantConsolidateDto> findDataConsolodateByProvider(Date periodDt, int entityType, int operation);

    /**
     *
     * @return
     */
    public List<CdfiCarrierMappingDto> findDiffCarriers();

}
