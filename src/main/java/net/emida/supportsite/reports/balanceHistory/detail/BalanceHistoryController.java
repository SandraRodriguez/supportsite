
package net.emida.supportsite.reports.balanceHistory.detail;

import com.debisys.utils.NumberUtil;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.reports.balanceHistory.detail.dao.BalanceHistoryModel;

import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author g.martinez
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class BalanceHistoryController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(BalanceHistoryController.class);

    @Autowired
    private IBalanceHistoryDao iBalanceHistoryDao;

    @Autowired
    @Qualifier("balanceHistoryCsvBuilder")
    private CsvBuilderService balanceHistoryCsvBuilderService;

    @RequestMapping(value = UrlPaths.BALANCE_HISTORY_HISTORICAL_REPORT_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute BalanceHistoryForm reportModel) {

        fillBalanceHistory(model);
        return "reports/balancehistory/balanceHistoryForm";
    }

    @RequestMapping(value = UrlPaths.BALANCE_HISTORY_HISTORICAL_REPORT_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid BalanceHistoryForm reportModel, BindingResult result) throws TechnicalException, ApplicationException {

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillBalanceHistory(model.asMap());
            return "reports/balancehistory/balanceHistoryForm";
        }

        log.debug("reportBuild {}", result);
        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String formattedStartDate = sdf.format(reportModel.getStartDate());
        String formattedEndDate = sdf.format(reportModel.getEndDate());

        model.addAttribute("startDate", formattedStartDate);
        model.addAttribute("endDate", formattedEndDate);
        model.addAttribute("merchants", reportModel.getMerchants());
        Map<String, Object> queryData = fillQueryData(formattedStartDate, formattedEndDate, reportModel.getMerchants());

        Long totalRecords = iBalanceHistoryDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);
        
        double sumOpenBalance = 0;
        double sumTransactions = 0;
        double sumDeposits = 0;
        double sumCloseBalance = 0;
        
        List<BalanceHistoryModel> list = iBalanceHistoryDao.list(1, Integer.parseInt(totalRecords+""),"entityId", "ASC", queryData);
        for (BalanceHistoryModel list1 : list) {
           sumOpenBalance += Double.parseDouble(list1.getOpenBalance().toString());
            sumTransactions += Double.parseDouble(list1.getTransactions().toString());
            sumDeposits += Double.parseDouble(list1.getDeposits().toString());
            sumCloseBalance += Double.parseDouble(list1.getCloseBalance().toString());
        }
        
        model.addAttribute("sumOpenBalance", NumberUtil.formatCurrency(""+sumOpenBalance));
        model.addAttribute("sumTransactions", NumberUtil.formatCurrency(""+sumTransactions));
        model.addAttribute("sumDeposits", NumberUtil.formatCurrency(""+sumDeposits));
        model.addAttribute("sumCloseBalance", NumberUtil.formatCurrency(""+sumCloseBalance));
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;
        
        return "reports/balancehistory/balanceHistoryResult";
    }

    @RequestMapping(method = RequestMethod.POST, value = UrlPaths.BALANCE_HISTORY_DETAIL_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<BalanceHistoryModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String startDate, @RequestParam String endDate, @RequestParam String merchants) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. StartDate={}. EndDate={}. ", startDate, endDate);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);
        
        Map<String, Object> queryData = fillQueryData(startDate, endDate, merchants);
        List<BalanceHistoryModel> list = iBalanceHistoryDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;
        
        return list;
    }

    private Map<String, Object> fillQueryData(String startDate, String endDate, String merchants) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put("merchants", merchants);
        return queryData;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 14;
    }

    /**
     * Fill the selects type inputs
     *
     * @param model
     */
    private void fillBalanceHistory(Map model) {
        fillMerchantsFilter(model);
    }
    
    private void fillMerchantsFilter(Map model){
        try {
            model.put("strAccessLevel", getAccessLevel());
            model.put("strRefId", getRefId());
            model.put("strDistChainType", getDistChainType());
        } catch (TechnicalException e) {
            log.error("Error setting merchant variables. Ex = {}", e);
        }
    }


    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.BALANCE_HISTORY_DETAIL_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String startDate, @RequestParam String endDate, @RequestParam String merchants,
            @RequestParam Integer totalRecords, @RequestParam String sortField, @RequestParam String sortOrder) throws IOException {

        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file. StartDate={}. EndDate={}. Merchants={}", startDate, endDate, merchants);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, merchants);
        List<BalanceHistoryModel> list = iBalanceHistoryDao.list(1, totalRecords, sortField, sortOrder, queryData);

        log.debug("Total list={}", list.size());

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.debug("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());
        additionalData.put("startDate", startDate);
        additionalData.put("endDate", endDate);

        balanceHistoryCsvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.debug("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;
        
        return zipFileResource;

    }

}
