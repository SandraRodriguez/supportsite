/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceHistory.detail;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author g.martinez
 */
public class BalanceHistoryForm {
 
    private Date startDate;
    private Date endDate;
    private String merchants;    

    /**
     * @return the startDate
     */
    @NotNull(message = "{com.debisys.reports.error1}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    @NotNull(message = "{com.debisys.reports.error3}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")    
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @NotNull(message = "{NotNull.BalanceHistoryForm.merchant}")
    public String getMerchants() {
        return merchants;
    }


    public void setMerchants(String merchants) {
        this.merchants = merchants;
    }
    

    @Override
    public String toString() {
        return "BalanceHistoryModel[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", merchants=" + merchants +                
                ']';
    }

}
