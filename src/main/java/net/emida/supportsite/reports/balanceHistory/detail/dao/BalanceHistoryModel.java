
package net.emida.supportsite.reports.balanceHistory.detail.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author g.martinez
 */
public class BalanceHistoryModel implements CsvRow {

    private BigDecimal rwn;
    private BigDecimal openBalance;
    private BigDecimal transactions;
    private BigDecimal deposits;
    private BigDecimal closeBalance;
    
    private String openBalanceFormat;
    private String transactionsFormat;
    private String depositsFormat;
    private String closeBalanceFormat;

    private BigDecimal entityId;
    private String entityName;

    public BigDecimal getEntityId() {
        return entityId;
    }

    public void setEntityId(BigDecimal entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the rwn
     */
    public BigDecimal getRwn() {
        return rwn;
    }

    /**
     * @param rwn the rwn to set
     */
    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }

    public String setBalanceAuditUrl(String entityId) {
        return "<a href=\"#\">".concat(entityId).concat("text</a>");
    }

    @Override
    public List<CsvCell> cells() {
        List<CsvCell> list = new ArrayList<CsvCell>();
        CsvCell cella = new CsvCell(String.valueOf(rwn));
        list.add(cella);
        CsvCell cell1 = new CsvCell(getEntityId().toString());
        list.add(cell1);
        CsvCell cell2 = new CsvCell(getOpenBalance().toString());
        list.add(cell2);
        CsvCell cell3 = new CsvCell(getTransactions().toString());
        list.add(cell3);
        CsvCell cell4 = new CsvCell(getDeposits().toString());
        list.add(cell4);
        CsvCell cell5 = new CsvCell(getCloseBalance().toString());
        list.add(cell5);
        return list;
    }

    /**
     * @return the openBalance
     */
    public BigDecimal getOpenBalance() {
        return openBalance;
    }

    /**
     * @param openBalance the openBalance to set
     */
    public void setOpenBalance(BigDecimal openBalance) {
        this.openBalance = openBalance;
    }

    /**
     * @return the transactions
     */
    public BigDecimal getTransactions() {
        return transactions;
    }

    /**
     * @param transactions the transactions to set
     */
    public void setTransactions(BigDecimal transactions) {
        this.transactions = transactions;
    }

    /**
     * @return the deposits
     */
    public BigDecimal getDeposits() {
        return deposits;
    }

    /**
     * @param deposits the deposits to set
     */
    public void setDeposits(BigDecimal deposits) {
        this.deposits = deposits;
    }

    /**
     * @return the closeBalance
     */
    public BigDecimal getCloseBalance() {
        return closeBalance;
    }

    /**
     * @param closeBalance the closeBalance to set
     */
    public void setCloseBalance(BigDecimal closeBalance) {
        this.closeBalance = closeBalance;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getOpenBalanceFormat() {
        return openBalanceFormat;
    }

    public void setOpenBalanceFormat(String openBalanceFormat) {
        this.openBalanceFormat = openBalanceFormat;
    }

    public String getTransactionsFormat() {
        return transactionsFormat;
    }

    public void setTransactionsFormat(String transactionsFormat) {
        this.transactionsFormat = transactionsFormat;
    }

    public String getDepositsFormat() {
        return depositsFormat;
    }

    public void setDepositsFormat(String depositsFormat) {
        this.depositsFormat = depositsFormat;
    }

    public String getCloseBalanceFormat() {
        return closeBalanceFormat;
    }

    public void setCloseBalanceFormat(String closeBalanceFormat) {
        this.closeBalanceFormat = closeBalanceFormat;
    }

}
