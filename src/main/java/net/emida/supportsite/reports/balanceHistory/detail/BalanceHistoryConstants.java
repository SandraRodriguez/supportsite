/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceHistory.detail;

/**
 *
 * @author dgarzon
 */
public class BalanceHistoryConstants {
    //filters
    public static final String HISTORY_FILTER_SELECT = "SELECT";
    public static final String HISTORY_FILTER_ENTITY_TYPE = "ENTITY TYPE";
    public static final String HISTORY_FILTER_ENTITY_ID = "ENTITY ID";

        
    /*public static final String SELECT = "SELECT";
    public static final String USERNAME = "USERNAME";
    public static final String ENTITY_ID = "ENTITY_ID";
    public static final String CHANGE_TYPE = "CHANGE_TYPE";
    public static final String OLD_VALUE = "OLD_VALUE";
    public static final String NEW_VALUE = "NEW_VALUE";

    //credit type changes
    public static final String HISTORY_PARENT_CHANGE = "PARENT_CHANGE";
    public static final String HISTORY_CREDITTYPE_CHANGE = "CREDITTYPE_CHANGE";
    public static final String HISTORY_SHARED_INDIVIDUAL_CHANGE = "SHARED_INDIVIDUAL_CHANGE";
    public static final String HISTORY_INTERNAL_EXTERNAL_CHANGE = "INTERNAL_EXTERNAL_CHANGE";
    public static final String HISTORY_MERCHANT_CANCELLED="MERCHANT_CANCELLED";
    public static final String HISTORY_MERCHANT_DISABLED="MERCHANT_DISABLED";*/
    
    //entity types
    /*public static final String HISTORY_PARENT_CHANGE = "PARENT_CHANGE";
    public static final String HISTORY_CREDITTYPE_CHANGE = "CREDITTYPE_CHANGE";
    public static final String HISTORY_SHARED_INDIVIDUAL_CHANGE = "SHARED_INDIVIDUAL_CHANGE";
    public static final String HISTORY_INTERNAL_EXTERNAL_CHANGE = "INTERNAL_EXTERNAL_CHANGE";
    public static final String HISTORY_MERCHANT_CANCELLED="MERCHANT_CANCELLED";
    public static final String HISTORY_MERCHANT_DISABLED="MERCHANT_DISABLED"; */   
}
