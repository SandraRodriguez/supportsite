/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceHistory.detail;

import com.debisys.languages.Languages;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author g.martinez
 */
@Service
public class BalanceHistoryCsvBuilder implements CsvBuilderService {

    private static final Logger log = LoggerFactory.getLogger(BalanceHistoryCsvBuilder.class);

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {
            List<CsvRow> data = (List<CsvRow>) list;
            //File to fill
            writer = getFileWriter(filePath);

            log.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            String language = (String) additionalData.get("language");
            String startDate = (String) additionalData.get("startDate");
            String endDate = (String) additionalData.get("endDate");

            writer.write("\"" + Languages.getString("jsp.admin.reports.balancehistory.balanceHistoryReport", language) + "\"\n");
            writer.write("\"" + Languages.getString("jsp.admin.from", language) + "\",\"" + startDate + "\"\n");
            writer.write("\"" + Languages.getString("jsp.admin.to", language) + "\",\"" + endDate + "\"\n\n");

            List<String> headers = generateHeaders(additionalData);
            String headerRow = CsvUtil.generateRow(headers, true);
            writer.write(headerRow);

            fillWriter(data, writer, additionalData);

        } catch (Exception ex) {
            log.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {
            if (additionalData != null) {
                additionalData.clear();
            }
            additionalData = null;
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {

                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {

        double sumOpenBalance = 0;
        double sumTransactions = 0;
        double sumDeposits = 0;
        double sumCloseBalance = 0;

        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            writer.write(stringifyRow);
            sumOpenBalance += Double.parseDouble(cells.get(2).getValue());
            sumTransactions += Double.parseDouble(cells.get(3).getValue());
            sumDeposits += Double.parseDouble(cells.get(4).getValue());
            sumCloseBalance += Double.parseDouble(cells.get(5).getValue());
            cells.clear();
            cells = null;
        }
        writer.write("\"\",\"Total\",\"" + new BigDecimal(sumOpenBalance).setScale(2, RoundingMode.HALF_UP)
                + "\",\"" + new BigDecimal(sumTransactions).setScale(2, RoundingMode.HALF_UP)
                + "\",\"" + new BigDecimal(sumDeposits).setScale(2, RoundingMode.HALF_UP)
                + "\",\"" + new BigDecimal(sumCloseBalance).setScale(2, RoundingMode.HALF_UP) + "\"\n");
    }

    private List<String> generateHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");
        List<String> headers = new ArrayList<String>();
        headers.add("#");
        headers.add(Languages.getString("jsp.admin.reports.balancehistory.field.entityId", language));
        headers.add(Languages.getString("jsp.admin.reports.balancehistory.field.openBalance", language));
        headers.add(Languages.getString("jsp.admin.reports.balancehistory.field.transactionsAdjustments", language));
        headers.add(Languages.getString("jsp.admin.reports.balancehistory.field.deposits", language));
        headers.add(Languages.getString("jsp.admin.reports.balancehistory.field.closeBalance", language));
        return headers;
    }

}
