package net.emida.supportsite.reports.balanceHistory.detail.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author g.martinez
 */
public class BalanceHistoryQueryBuilder {

    private static final Logger log = LoggerFactory.getLogger(BalanceHistoryQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    private String startDate;
    private String endDate;
    private String merchants;

    /**
     *
     * @param queryData
     */
    public BalanceHistoryQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.merchants = (String) queryData.get("merchants");
    }

    public BalanceHistoryQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.merchants = (String) queryData.get("merchants");
    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append(initQuery());
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery());
        sb0.append(") AS tbl1 ");
        return sb0.toString();

    }

    public String initQuery() {
        StringBuilder initQuery = new StringBuilder();
        initQuery.append("with cte as ");
        initQuery.append("(SELECT ROW_NUMBER() OVER (PARTITION BY entityID ORDER BY date asc) row, liabilityLimit, runningLiability, entityID ");
        initQuery.append("FROM BalanceHistory WITH(NOLOCK) ");
        initQuery.append("WHERE entityType=8 ");
        initQuery.append("AND entityID IN (").append(this.merchants).append(") ");
        initQuery.append("AND ").append(queryDates("date"));
        initQuery.append("GROUP BY  liabilityLimit,date , runningLiability, entityID) ");
        return initQuery.toString();

    }

    public String mainQuery() {
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT distinct(a.EntityID) as entityId, ");
        mainQuery.append("ISNULL((select (liabilityLimit-runningLiability) ");
        mainQuery.append("FROM BalanceHistory WITH(NOLOCK) ");
        mainQuery.append("WHERE entityId = a.entityID and date = (select min(a1.date) ");
        mainQuery.append("FROM BalanceHistory a1 WITH(NOLOCK) ");
        mainQuery.append("WHERE a1.entityID = a.entityID AND ").append(queryDates("a1.date")).append(")),0) AS openBalance, ");
        mainQuery.append("ISNULL((SELECT sum(wt.amount) FROM web_transactions wt WITH(NOLOCK) WHERE wt.merchant_id = a.entityID AND ").append(queryDates("wt.datetime")).append("),0) AS transactions, ");
        mainQuery.append("ISNULL((select sum(mc.payment) FROM merchant_credits mc WITH(NOLOCK) ");
        mainQuery.append("WHERE mc.merchant_id = a.entityID AND ").append(queryDates("log_datetime")).append("),0) ");
        mainQuery.append("AS Deposits, ");
        //.append("((OpenBalance + deposits) - transactions) ")
        mainQuery.append("(ISNULL(( ");
        mainQuery.append("select (liabilityLimit-runningLiability) ");
        mainQuery.append("FROM BalanceHistory WITH(NOLOCK) ");
        mainQuery.append("WHERE entityId = a.entityID and date = (select min(a1.date) ");
        mainQuery.append("FROM BalanceHistory a1 WITH(NOLOCK) ");
        mainQuery.append("WHERE a1.entityID = a.entityID AND ").append(queryDates("a1.date")).append(")),0)+ISNULL(( ");
        mainQuery.append("select sum(mc.payment) ");
        mainQuery.append("FROM merchant_credits mc WITH(NOLOCK) ");
        mainQuery.append("WHERE mc.merchant_id = a.entityID and ").append(queryDates("log_datetime")).append("),0) - ");
        mainQuery.append("ISNULL((SELECT sum(wt.amount) FROM web_transactions wt WITH(NOLOCK) WHERE wt.merchant_id = a.entityID AND ").append(queryDates("wt.datetime")).append("),0)) AS closeBalance, ");
        mainQuery.append("m.dba ");
        mainQuery.append("FROM cte a WITH(NOLOCK) ");
        mainQuery.append("LEFT JOIN cte b WITH(NOLOCK) on a.entityID = b.entityID and a.row = b.row+1 ");
        mainQuery.append("INNER JOIN merchants m WITH(NOLOCK) on a.entityID = m.merchant_id ");
        mainQuery.append("group by a.EntityID , m.dba ");

        return mainQuery.toString();

    }

    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "entityID";
            sortOrder = " DESC ";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append(initQuery());

        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }

    /**
     *
     * @param stringToFormat format "dd/MM/yyyy HH:mm:ss.S"
     * @return
     */
    private Date convertToDate(String stringToFormat) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");
        try {
            Date date = format.parse(stringToFormat);
            return date;
        } catch (ParseException e) {
            return null;
        }
    }

    private String queryDates(String dateField) {
        return setDateQueryRanges(this.startDate, this.endDate, dateField);
    }

    /**
     * Set the dates ranges for a query with starting and ending date.
     *
     * @param startDate
     * @param endDate
     * @param field
     * @return
     */
    private String setDateQueryRanges(String startDate, String endDate, String field) {
        startDate = startDate.trim().concat(" 00:000:00.000");
        endDate = endDate.trim().concat(" 23:59:59.999");
        String sDate = convertToString((Date) convertToDate(startDate));
        String eDate = convertToString((Date) convertToDate(endDate));

        StringBuilder sb = new StringBuilder();
        sb.append(" (").append(field).append(">='").append(sDate).append("' AND ").append(field).append("<='").append(eDate).append("') ");
        return sb.toString();
    }

    private String convertToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String sCertDate = dateFormat.format(date);
        return sCertDate;
    }

}
