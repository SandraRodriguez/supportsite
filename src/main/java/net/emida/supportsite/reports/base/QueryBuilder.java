package net.emida.supportsite.reports.base;

import java.util.Map;
import java.util.Vector;

/**
 * General query builder base class
 * @author fernandob
 */
public abstract class QueryBuilder {
    // Report parameter names
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String START_PAGE = "startPage";
    public static final String END_PAGE = "endPage";
    public static final String SORT_FIELD_NAME = "sortFieldName";
    public static final String SORT_ORDER = "sortOrder";
    public static final String ACCESS_LEVEL = "accessLevel";
    public static final String CHAIN_TYPE = "distChainType";
    public static final String REFID = "refId";
    
    protected String startDate;
    protected String endDate;
    protected int startPage;
    protected int endPage;
    protected String sortFieldName;
    protected String sortOrder;
    protected String accessLevel;
    protected String distChainType;
    protected String refId;
    

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public String getSortFieldName() {
        return sortFieldName;
    }

    public void setSortFieldName(String sortFieldName) {
        this.sortFieldName = sortFieldName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getDistChainType() {
        return distChainType;
    }

    public void setDistChainType(String distChainType) {
        this.distChainType = distChainType;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }
    
    
    
    protected void getQueryData(Map<String, Object> queryData)
    {
        if(queryData != null)
        {
            if(queryData.containsKey(START_DATE))
            {
                this.startDate = (String)queryData.get(START_DATE);
            }
            if(queryData.containsKey(END_DATE))
            {
                this.endDate = (String)queryData.get(END_DATE);
            }
            if(queryData.containsKey(START_PAGE))
            {
                this.startPage = Integer.valueOf(String.valueOf(queryData.get(START_PAGE)));
            }
            if(queryData.containsKey(END_PAGE))
            {
                this.endPage = Integer.valueOf(String.valueOf(queryData.get(END_PAGE)));
            }
            if(queryData.containsKey(SORT_FIELD_NAME))
            {
                this.sortFieldName = (String)queryData.get(SORT_FIELD_NAME);
            }
            if(queryData.containsKey(SORT_ORDER))
            {
                this.sortOrder = (String)queryData.get(SORT_ORDER);
            }
            if(queryData.containsKey(ACCESS_LEVEL))
            {
                this.accessLevel = (String)queryData.get(ACCESS_LEVEL);
            }
            if(queryData.containsKey(CHAIN_TYPE))
            {
                this.distChainType = (String)queryData.get(CHAIN_TYPE);
            }
            if(queryData.containsKey(REFID))
            {
                this.refId = (String)queryData.get(REFID);
            }
        }
    }
    
    
    /**
     *
     * @param fieldName     Name of the field that will be used to set queries time interval
     * @return              String containing the SQL code segment to set date interval restriction
     * @throws Exception
     */
    protected String buildDatesRangeWhere(String fieldName) throws Exception{
        Vector vTimeZoneFilterDates = com.debisys.utils.TimeZone.getTimeZoneFilterDates(this.accessLevel, this.refId, this.startDate, this.endDate + " 23:59:59.999", false);
        StringBuilder sb = new StringBuilder();
        sb.append(" AND (").append(fieldName).append(">='").append(vTimeZoneFilterDates.get(0)).append("' AND ").append(fieldName).append("<='").append(vTimeZoneFilterDates.get(1)).append("') ");
        return sb.toString();
    }
    
    
    
    /**
    *
    * @param fieldName     Name of the field that will be used to set queries time interval
    * @return              String containing the SQL code segment to set date interval restriction
    * @throws Exception
    */
   protected String buildDatesRangeWhere(String dateField1, String dateField2) throws Exception{
       Vector vTimeZoneFilterDates = com.debisys.utils.TimeZone.getTimeZoneFilterDates(this.accessLevel, this.refId, this.startDate, this.endDate + " 23:59:59.999", false);
       StringBuilder sb = new StringBuilder();
       sb.append(" AND (isnull(").append(dateField1).append(", ").append(dateField2).append(")>='").append(vTimeZoneFilterDates.get(0)).append("' \n")
         .append(" AND (isnull(").append(dateField1).append(", ").append(dateField2).append(")<='").append(vTimeZoneFilterDates.get(1)).append("' \n");
       return sb.toString();
   }

    
}
