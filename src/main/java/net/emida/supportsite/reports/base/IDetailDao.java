package net.emida.supportsite.reports.base;


import java.util.Map;
import java.util.List;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author fernandob Based on Franky's work
 * @param <T> Detail model class
 */
public interface IDetailDao<T> {
    List<T> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    Long count(Map<String, Object> queryData) throws TechnicalException;
}
