package net.emida.supportsite.reports.transactions.afbr.dao;

import com.debisys.utils.NumberUtil;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;




/**
 * @author Franky Villadiego
 */
public class AvailableBalanceReportModel implements CsvRow{

    private Long rowNum;
    private Long repId;
    private String repName;
    private BigDecimal finalBalance = new BigDecimal(0.0);
    private Date lastTrxDate;
    private String sku;
    private BigDecimal amount = new BigDecimal(0.0);

    public AvailableBalanceReportModel() {
    }

    public AvailableBalanceReportModel(Long repId, String repName, BigDecimal finalBalance, Date lastTrxDate, String sku, BigDecimal amount) {
        this.repId = repId;
        this.repName = repName;
        this.finalBalance = finalBalance;
        this.lastTrxDate = lastTrxDate;
        this.sku = sku;
        this.amount = amount;
    }

    public AvailableBalanceReportModel(Long repId, String repName, double finalBalance, Date lastTrxDate, String sku, double amount) {
        this.repId = repId;
        this.repName = repName;
        this.finalBalance = new BigDecimal(finalBalance);
        this.lastTrxDate = lastTrxDate;
        this.sku = sku;
        this.amount = new BigDecimal(amount);
    }

    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public Long getRepId() {
        return repId;
    }

    public void setRepId(Long repId) {
        this.repId = repId;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public BigDecimal getFinalBalance() {
        return finalBalance;
    }

    public void setFinalBalance(BigDecimal finalBalance) {
        this.finalBalance = finalBalance;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    public Date getLastTrxDate() {
        return lastTrxDate;
    }

    public void setLastTrxDate(Date lastTrxDate) {
        this.lastTrxDate = lastTrxDate;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public List<CsvCell> cells() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cell1 = new CsvCell(repId.toString());
        list.add(cell1);

        CsvCell cell2 = new CsvCell(repName != null ? repName : "");
        list.add(cell2);

        CsvCell cell3 = new CsvCell(NumberUtil.formatCurrency(finalBalance != null ? finalBalance.toString() : null));
        list.add(cell3);

        String formattedLastTrxDate = lastTrxDate != null ? sdf.format(lastTrxDate) : "";
        CsvCell cell4 = new CsvCell(formattedLastTrxDate);
        list.add(cell4);

        CsvCell cell5 = new CsvCell(sku != null ? sku.trim() : "");
        list.add(cell5);

        CsvCell cell6 = new CsvCell(NumberUtil.formatCurrency(amount.toString()));
        list.add(cell6);

        return list;
    }
}
