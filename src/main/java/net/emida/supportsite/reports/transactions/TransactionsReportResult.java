package net.emida.supportsite.reports.transactions;



import java.util.List;

/**
 * @author Franky Villadiego
 */

public class TransactionsReportResult {


    private Long totalRecords;
    private List<TransactionsReportModel> body;


    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<TransactionsReportModel> getBody() {
        return body;
    }

    public void setBody(List<TransactionsReportModel> body) {
        this.body = body;
    }
}
