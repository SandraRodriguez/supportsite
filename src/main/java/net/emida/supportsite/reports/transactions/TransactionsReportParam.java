package net.emida.supportsite.reports.transactions;


import java.util.Date;
import java.util.List;

/**
 * @author Franky Villadiego
 */
public class TransactionsReportParam {

    //Pagination info
    private String sortFieldName;
    private String sortOrder;
    private int startPage;
    private int endPage;

    //Required data
    private String instanceName;
    boolean internationalDeployment;
    boolean domesticDeployment;
    boolean mexicoDeployment;
    private TransactionsReportType reportType;
    private long refId;
    private String accessLevel;
    private String distChainType;
    private Date startDate;
    private Date endDate;

    //Optional data
    private boolean includeTax;
    private String pinNumber;
    private String invoiceNumber;
    private String transactionId;
    private List<Long> repIds;
    private boolean allMerchants;
    private List<Long> merchantIds;
    private List<Long> millenniumNum;
    private List<String> ratePlanIds;
    private List<Long> productIds;
    private String mxValueAddedTax;

    //Permissions
    private boolean permViewRefCard;
    private boolean permEnableTaxCalc;
    private boolean permShowAdditionalData;
    private boolean permViewPin;
    private boolean permViewAchDate;
    private boolean permViewFilterS2KRatePlan;
    private boolean showQrCodeTransactions;
    private boolean showCommissionDetails;
    private boolean showExternalMerchantId;


    public TransactionsReportParam(TransactionsReportType reportType) {
        this.reportType = reportType;
    }

    public String getSortFieldName() {
        return sortFieldName;
    }

    public void setSortFieldName(String sortFieldName) {
        this.sortFieldName = sortFieldName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public TransactionsReportType getReportType() {
        return reportType;
    }

    public void setReportType(TransactionsReportType reportType) {
        this.reportType = reportType;
    }

    public long getRefId() {
        return refId;
    }

    public void setRefId(long refId) {
        this.refId = refId;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getDistChainType() {
        return distChainType;
    }

    public void setDistChainType(String distChainType) {
        this.distChainType = distChainType;
    }

    public boolean isInternationalDeployment() {
        return internationalDeployment;
    }

    public void setInternationalDeployment(boolean internationalDeployment) {
        this.internationalDeployment = internationalDeployment;
    }

    public boolean isDomesticDeployment() {
        return domesticDeployment;
    }

    public void setDomesticDeployment(boolean domesticDeployment) {
        this.domesticDeployment = domesticDeployment;
    }


    public boolean isMexicoDeployment() {
        return mexicoDeployment;
    }

    public void setMexicoDeployment(boolean mexicoDeployment) {
        this.mexicoDeployment = mexicoDeployment;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isIncludeTax() {
        return includeTax;
    }

    public void setIncludeTax(boolean includeTax) {
        this.includeTax = includeTax;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public List<Long> getRepIds() {
        return repIds;
    }

    public void setRepIds(List<Long> repIds) {
        this.repIds = repIds;
    }

    public boolean isAllMerchants() {
        return allMerchants;
    }

    public void setAllMerchants(boolean allMerchants) {
        this.allMerchants = allMerchants;
    }

    public List<Long> getMerchantIds() {
        return merchantIds;
    }

    public void setMerchantIds(List<Long> merchantIds) {
        this.merchantIds = merchantIds;
    }

    public List<Long> getMillenniumNum() {
        return millenniumNum;
    }

    public void setMillenniumNum(List<Long> millenniumNum) {
        this.millenniumNum = millenniumNum;
    }

    public List<String> getRatePlanIds() {
        return ratePlanIds;
    }

    public void setRatePlanIds(List<String> ratePlanIds) {
        this.ratePlanIds = ratePlanIds;
    }

    public List<Long> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Long> productIds) {
        this.productIds = productIds;
    }

    public boolean isPermViewRefCard() {
        return permViewRefCard;
    }

    public void setPermViewRefCard(boolean permViewRefCard) {
        this.permViewRefCard = permViewRefCard;
    }

    public boolean isPermEnableTaxCalc() {
        return permEnableTaxCalc;
    }

    public void setPermEnableTaxCalc(boolean permEnableTaxCalc) {
        this.permEnableTaxCalc = permEnableTaxCalc;
    }

    public boolean isPermShowAdditionalData() {
        return permShowAdditionalData;
    }

    public void setPermShowAdditionalData(boolean permShowAdditionalData) {
        this.permShowAdditionalData = permShowAdditionalData;
    }

    public boolean isPermViewPin() {
        return permViewPin;
    }

    public void setPermViewPin(boolean permViewPin) {
        this.permViewPin = permViewPin;
    }

    public boolean isPermViewAchDate() {
        return permViewAchDate;
    }

    public void setPermViewAchDate(boolean permViewAchDate) {
        this.permViewAchDate = permViewAchDate;
    }

    public boolean isPermViewFilterS2KRatePlan() {
        return permViewFilterS2KRatePlan;
    }

    public void setPermViewFilterS2KRatePlan(boolean permViewFilterS2KRatePlan) {
        this.permViewFilterS2KRatePlan = permViewFilterS2KRatePlan;
    }

    public boolean isShowQrCodeTransactions() {
        return showQrCodeTransactions;
    }

    public void setShowQrCodeTransactions(boolean showQrCodeTransactions) {
        this.showQrCodeTransactions = showQrCodeTransactions;
    }

    public boolean isShowCommissionDetails() {
        return showCommissionDetails;
    }

    public void setShowCommissionDetails(boolean showCommissionDetails) {
        this.showCommissionDetails = showCommissionDetails;
    }

    public String getMxValueAddedTax() {
        return mxValueAddedTax;
    }

    public void setMxValueAddedTax(String mxValueAddedTax) {
        this.mxValueAddedTax = mxValueAddedTax;
    }

    public boolean showExternalMerchantId() {
        return showExternalMerchantId;
    }

    public void setShowExternalMerchantId(boolean showExternalMerchantId) {
        this.showExternalMerchantId = showExternalMerchantId;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
}
