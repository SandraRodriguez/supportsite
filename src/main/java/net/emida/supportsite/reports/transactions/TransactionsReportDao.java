package net.emida.supportsite.reports.transactions;

/**
 * @author Franky Villadiego
 */
public interface TransactionsReportDao {

    long count(ReportData param);

    TransactionsReportResult getResult(ReportData param);
}
