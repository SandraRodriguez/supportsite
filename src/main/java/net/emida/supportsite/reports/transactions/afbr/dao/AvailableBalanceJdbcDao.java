package net.emida.supportsite.reports.transactions.afbr.dao;

import net.emida.supportsite.reports.spiff.deatil.SpiffDetailDao;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailQueryBuilder;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportMapper;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportModel;
import net.emida.supportsite.reports.transactions.afbr.AvailableBalanceDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */

@Repository
public class AvailableBalanceJdbcDao implements AvailableBalanceDao {

    private static final Logger log = LoggerFactory.getLogger(AvailableBalanceJdbcDao.class);

    private static String SQL_COUNT= "SELECT COUNT(*) from carriers";

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll(){
        try {
            return 0L;
        }catch (DataAccessException ex){
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    public List<AvailableBalanceReportModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData){
        try {

            AvailableBalanceQueryBuilder queryBuilder = new AvailableBalanceQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();

            log.debug("Query list: {}", SQL_QUERY);
            //return dummyData();
            return jdbcTemplate.query(SQL_QUERY, new AvailableBalanceReportMapper());
        }catch (DataAccessException ex){
            log.error("Data access error in list spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }catch (Exception ex){
            log.error("Exception in list spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {

            AvailableBalanceQueryBuilder queryBuilder = new AvailableBalanceQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();

            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            //return 7L;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        }catch (DataAccessException ex){
            log.error("Data access error in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }catch (Exception ex){
            log.error("Exception in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }


    private List<AvailableBalanceReportModel> dummyData(){
        List<AvailableBalanceReportModel> list = new ArrayList<AvailableBalanceReportModel>();

        AvailableBalanceReportModel m = new AvailableBalanceReportModel(12345L, "Representante Numero Uno", 10500.0, new Date(), "SKU-001", 100.0);
        list.add(m);

        m = new AvailableBalanceReportModel(1234123445565L, "Representante Numero Uno", 5000.0, new Date(), "SKU-001", 180.0);
        list.add(m);

        m = new AvailableBalanceReportModel(9876132424355L, "Representante Numero Dos", 2150456.0, new Date(), "SKU-100", 250.0);
        list.add(m);

        m = new AvailableBalanceReportModel(3355832654321L, "Representante Numero Tres", 35000.0, new Date(), "SKU-098", 120.0);
        list.add(m);

        m = new AvailableBalanceReportModel(9973344552266L, "Representante Numero Cuatro", 400.0, new Date(), "SKU-987", 15.0);
        list.add(m);

        m = new AvailableBalanceReportModel(2211887766557L, "Representante Numero Cinco", 3500.0, new Date(), "SKU-010", 60.0);
        list.add(m);

        m = new AvailableBalanceReportModel(3866331800940L, "Representante Numero Seis", 500.0, new Date(), "SKU-002", 20.0);
        list.add(m);

        return list;

    }

}
