package net.emida.supportsite.reports.transactions;

import com.debisys.customers.CustomerSearch;
import com.debisys.customers.Merchant;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.TransactionReport;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/**
 * @author Franky Villadiego
 */

@Controller
@RequestMapping(value = UrlPaths.ADMIN_PATH)
public class TransactionsReportController extends BaseController{

    private static final Logger log = LoggerFactory.getLogger(TransactionsReportController.class);

    private static final String TRX_PATH            = "/transactions";
    private static final String TRX_GRID_PATH       = "/transactions/grid";
    private static final String TRX_DOWNLOAD_PATH       = "/transactions/download";
    private static final String TRX_SEARCH_MERCHANTS_PATH   = "/transactions/merchants";
    private static final String REPORT_FORM_PAGE = "reports/transactions/transactionsReportForm";
    private static final String REPORT_RESULT_PAGE = "reports/transactions/transactionsReportResult";
    private static final String MX_DATE_LBL = "jsp.admin.select_date_range";
    private static final String NOT_MX_DATE_LBL = "jsp.admin.select_date_range_international";

    @Autowired
    private TransactionsReportDao transactionsReportDao;

    @Autowired
    private TransactionsReportService transactionsReportService;

    @Autowired @Qualifier("transactionsReportCsvBuilder")
    private CsvBuilderService csvBuilderService;


    @RequestMapping(value = TRX_PATH, method = RequestMethod.GET)
    public String showForm(Map<String, Object> model, @ModelAttribute("reportModel") TransactionsReportForm reportModel,
                           @RequestParam(required = false) Long repId, @RequestParam(required = false) String repName,
                           @RequestParam(required = false) String report){


        fillModel(model, repId, repName, report);

        Date start_date = (Date) getSessionData().getPropertyObj("the_start_date");
        Date end_date = (Date) getSessionData().getPropertyObj("the_end_date");

        reportModel.setStartDate(start_date);
        reportModel.setEndDate(end_date);

        return REPORT_FORM_PAGE;
    }

    private void fillModel(Map<String, Object> model, @RequestParam(required = false) Long repId, @RequestParam(required = false) String repName, @RequestParam(required = false) String report) {
        boolean mexico = super.isMexico();
        String dateLabel = mexico ? getString(MX_DATE_LBL) : getString(NOT_MX_DATE_LBL);
        model.put("dateLabel", dateLabel);

        String timeZoneLabel = buildTimeZoneLabel();
        model.put("timeZoneLabel", timeZoneLabel);


        boolean showIsoName = showIsoName(repId);
        model.put("showIsoName", showIsoName);

        String isoName = showIsoName ? buildIsoName(repName) : "";
        model.put("isoName", isoName);

        boolean showInvoiceId = showInvoiceId();
        model.put("showInvoiceId", showInvoiceId);

        boolean showPinNumber = showPinNumber();
        model.put("showPinNumber", showPinNumber);

        boolean showIsos = showIsos(repId);
        model.put("showIsos", showIsos);
        if(showIsos){
            fillIsos(model);
        }

        boolean showMerchants = showMerchants();
        model.put("showMerchants", showMerchants);
        if(showMerchants) {
            fillMerchants(model, repName, repId);
        }
        
        model.put("report", report);
    }


    @RequestMapping(value = TRX_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @ModelAttribute("reportModel") @Valid TransactionsReportForm reportModel, BindingResult result, HttpServletRequest req,
                              @RequestParam MultiValueMap<String,String> params) throws TechnicalException, ApplicationException {

        log.debug("transactions report building {}", result);

        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());


        if(result.hasErrors()){
            fillModel(model.asMap(), reportModel.getRepId(), reportModel.getRepName(), reportModel.getReport());
            return REPORT_FORM_PAGE;
        }


        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String formattedStarDate = sdf.format(reportModel.getStartDate());
        String formattedEndDate = sdf.format(reportModel.getEndDate());

        getSessionData().setPropertyObj("the_start_date", reportModel.getStartDate());
        getSessionData().setPropertyObj("the_end_date", reportModel.getEndDate());

        model.addAttribute("startDate", formattedStarDate);
        model.addAttribute("endDate", formattedEndDate);

        model.addAttribute("transactionId", reportModel.getTransactionId() != null ? reportModel.getTransactionId() : "");
        model.addAttribute("invoiceId", StringUtil.isNotEmptyStr(reportModel.getInvoiceId()) ? reportModel.getInvoiceId() : "");
        model.addAttribute("pinNumber", reportModel.getPinNumber() != null ? reportModel.getPinNumber() : "");



        String csvMerchantIds = reportModel.toCsvMerchantIds();
        model.addAttribute("merchantIds", StringUtil.isNotEmptyStr(csvMerchantIds) ? csvMerchantIds : "");
        model.addAttribute("allMerchants", reportModel.isAllMerchants());

        String csvIsoIds = reportModel.toCsvIsoIds();
        model.addAttribute("isoIds", StringUtil.isNotEmptyStr(csvIsoIds) ? csvIsoIds : "");

        boolean permShowCommissionDetails = allowPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT);
        model.addAttribute("showCommissionDetails", permShowCommissionDetails);

        boolean permTaxCalc = allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
        model.addAttribute("showTaxCalc", permTaxCalc);

        boolean permViewCarrierNumber = allowPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT);
        model.addAttribute("viewCarrierNumber", permViewCarrierNumber);

        boolean showAdditionalData = allowPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA);
        model.addAttribute("showAdditionalData", showAdditionalData);

        boolean viewPin = allowPermission(DebisysConstants.PERM_VIEWPIN);
        model.addAttribute("viewPin", viewPin);

        boolean viewAchDate = allowPermission(DebisysConstants.PERM_VIEW_ACH_DATE);
        model.addAttribute("viewAchDate", viewAchDate);

        boolean invoiceNumberEnabled = hasPermissionEnabled(DebisysConstants.INVOICE_NUMBER);
        model.addAttribute("invoiceNumberEnabled", invoiceNumberEnabled);

        boolean viewRefCards = allowPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
        model.addAttribute("viewRefCards", viewRefCards);

        boolean showQrCode = allowPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);
        model.addAttribute("showQrCode", showQrCode);
        
        boolean isPermPromoData = ReportsUtil.isIntlAndHasDataPromoPermission(getSessionData(), servletContext);
        model.addAttribute("isPermPromoData", isPermPromoData);        
        
        boolean showExternalMerchantId = allowPermission(DebisysConstants.PERM_SHOW_EXTERNAL_MERCHANT_ID);
        model.addAttribute("showExternalMerchantId", showExternalMerchantId);


        String trxId = reportModel.getTransactionId() != null ? reportModel.getTransactionId().toString() : null;
        String pinNumber = reportModel.getPinNumber() != null ? reportModel.getPinNumber().toString() : null;

        List<Long> mIds = buildMerchantIds(csvMerchantIds);
        List<Long> isoIds = buildIsoIds(csvIsoIds);

        TransactionsReportParam trp = buildParams(null, null, 0,0,
                reportModel.getStartDate(), reportModel.getEndDate(), mIds,
                trxId, pinNumber, reportModel.getInvoiceId(), isoIds);

        ReportData reportData = transactionsReportService.buildReportData(trp);

        Long totalRecords = transactionsReportDao.count(reportData);

        model.addAttribute("totalRecords", totalRecords);

        return REPORT_RESULT_PAGE;
    }


    @RequestMapping(method = RequestMethod.GET, value = TRX_GRID_PATH, produces="application/json")
    @ResponseBody
    public List<TransactionsReportModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
                                                 @RequestParam String rows, @RequestParam(required = false) String transactionId, @RequestParam(required = false) String invoiceId,
                                                  @RequestParam(required = false) String pinNumber, @RequestParam String startDate,
                                                 @RequestParam String endDate, @RequestParam(required = false) String merchantIds, @RequestParam(required = false) String isoIds){

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. StartDate={}. EndDate={}. TrxId={}. InvoiceId={}. pinNumber={}. Mids={}. IsoIds={}", startDate, endDate, transactionId, invoiceId, pinNumber, merchantIds, isoIds);

        int rowsPerPage = Integer.parseInt(rows);
        int startPage = Integer.parseInt(page);
        int endPage = rowsPerPage + startPage - 1;


        List<Long> mIdList = buildMerchantIds(merchantIds);
        List<Long> isoIdLis = buildIsoIds(isoIds);

        Date theStartDate = DateUtil.parse(startDate, "MM/dd/yyyy");
        Date theEndDate = DateUtil.parse(endDate, "MM/dd/yyyy");

        TransactionsReportParam trp = buildParams(sortField, sortOrder, startPage, endPage, theStartDate, theEndDate, mIdList,
                transactionId, pinNumber, invoiceId, isoIdLis);

        //ReportData reportData = transactionsReportService.buildReportData(trp);

        TransactionsReportResult transactionsReportResult = transactionsReportService.buildReport(trp);


        List<TransactionsReportModel> body = transactionsReportResult.getBody();

        return body;
    }


    @RequestMapping(method = RequestMethod.GET, value = TRX_SEARCH_MERCHANTS_PATH, produces="application/json")
    @ResponseBody
    public List<Map<String, String>> searchMerchant(@RequestParam(value = "query") String searchValue, @RequestParam(required = false) String repName,
                                                    @RequestParam(required = false) Long repId){

        log.debug("Searching merchants by value ={}", searchValue);

        Vector<Vector<String>> merchants = searchMerchants(repName, repId);
        int maxRows = 15;
        List<Map<String, String>> result = new ArrayList<>();
        String selectAllLabel = getString("jsp.tools.searchmerchant.selectAll") + " " + searchValue;
        String selectAllValue = "*" + searchValue;
        Map<String, String> selectAllSearchValueRow = buildRow(selectAllLabel, selectAllValue);
        result.add(selectAllSearchValueRow);
        List<Map<String, String>> filtered = filter(merchants, searchValue, maxRows, result);
        //If no occurrences were found, we cleaning the manual entry in list
        if(filtered.size() == 1) filtered.clear();
        return filtered;
    }


    @RequestMapping(method = RequestMethod.GET, value = TRX_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam(required = false) String transactionId,
                                           @RequestParam(required = false) String pinNumber, @RequestParam(required = false) String invoiceId,
                                           @RequestParam String startDate, @RequestParam String endDate, @RequestParam(required = false) String merchantIds,
                                           @RequestParam(required = false) String isoIds, @RequestParam Integer totalRecords, @RequestParam String sortField,
                                           @RequestParam String sortOrder) throws IOException {

        log.info("Downloading file...");
        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file. StartDate={}. EndDate={}. TrxId={}. Pnumber={}. Mids={}. IsoIds={}", startDate, endDate, transactionId, pinNumber, merchantIds, isoIds);


        List<Long> mIdList = buildMerchantIds(merchantIds);
        List<Long> isoIdLis = buildIsoIds(isoIds);
        boolean isPermPromoData = ReportsUtil.isIntlAndHasDataPromoPermission(getSessionData(), servletContext);

        Date theStartDate = DateUtil.parse(startDate, "MM/dd/yyyy");
        Date theEndDate = DateUtil.parse(endDate, "MM/dd/yyyy");

        TransactionsReportParam trp = buildParams(sortField, sortOrder, 1, totalRecords, theStartDate, theEndDate, mIdList,
                transactionId, pinNumber, invoiceId, isoIdLis);

        TransactionsReportResult transactionsReportResult = transactionsReportService.buildReport(trp);

        List<TransactionsReportModel> list = transactionsReportResult.getBody();
        log.debug("Total list={}", list.size());

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.info("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<>();
        log.info("Getting SessionData to build CSV file...");
        SessionData sessionData = getSessionData();
        User user = sessionData.getUser();
        additionalData.put("user", user.getUsername());
        additionalData.put("language", sessionData.getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());
        additionalData.put("international", isInternational());
        additionalData.put("domestic", isDomestic());

        additionalData.put("isoName", showIsoName(null) ? buildIsoName(null) : "");
        additionalData.put("startDate", startDate);
        additionalData.put("endDate", endDate);

        additionalData.put("transactionId", transactionId);

        additionalData.put("showInvoiceNo", showInvoiceId());
        additionalData.put("invoiceNo", invoiceId);

        additionalData.put("showPinNumber", showPinNumber());
        additionalData.put("pinNumber", pinNumber);

        additionalData.put("merchantIds", merchantIds);


        additionalData.put("showCommissionDetails", allowPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT));
        additionalData.put("showTaxCalc", allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS));
        additionalData.put("viewPin", allowPermission(DebisysConstants.PERM_VIEWPIN));
        additionalData.put("viewCarrierNumber", allowPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT));
        additionalData.put("showAdditionalData", allowPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA));
        additionalData.put("viewAchDate", allowPermission(DebisysConstants.PERM_VIEW_ACH_DATE));
        additionalData.put("invoiceNumberEnabled", hasPermissionEnabled(DebisysConstants.INVOICE_NUMBER));
        additionalData.put("viewRefCards", allowPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS));
        additionalData.put("showQrCode", allowPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS));
        additionalData.put("isPermPromoData", isPermPromoData);
        additionalData.put("showExternalMerchantId", allowPermission(DebisysConstants.PERM_SHOW_EXTERNAL_MERCHANT_ID));
        
        csvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.info("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();

        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

        return zipFileResource;

    }


    private List<Map<String, String>> filter(Vector<Vector<String>> merchants, String searchValue, int maxRows, List<Map<String, String>> result) {
        int count = 0;
        if(merchants != null && !merchants.isEmpty()){
            for (Vector<String> m : merchants) {
                boolean contains = StringUtils.containsIgnoreCase(m.get(1), searchValue);
                if(contains){
                    Map<String, String> row = buildRow(m.get(1), m.get(0));
                    result.add(row);
                    if(++count > maxRows){
                        break;
                    }
                }
            }
        }
        return result;
    }

    private Map<String, String> buildRow(String label, String value) {
        Map<String, String> row = new HashMap<>();
        row.put("label", label);
        row.put("value", value);
        return row;
    }


    private String buildTimeZoneLabel() {
        String reportNote = super.getString("jsp.admin.timezone.reportNote");
        try {

            long refId = Long.parseLong(super.getRefId());
            Vector<String> timeZoneByRep = TimeZone.getTimeZoneByRep(refId);
            String gmtTimeZoneText = timeZoneByRep.get(1);
            String gmtTimeZoneName = timeZoneByRep.get(2);

            return reportNote + ": " + gmtTimeZoneText + "[ " + gmtTimeZoneName + " ]";
        }catch (Exception ex){
            log.error("Error getting time zone...: {}", ex);
        }
        return reportNote + ": Error.";
    }


    private String buildIsoName(String repName) {
        if(StringUtils.isNotEmpty(repName) && isLevelCarrier()){
            return repName;
        }else{
            return getSessionData().getUser().getCompanyName();
        }

    }

    private boolean showIsoName(Long repId) {
        return !(repId == null && isLevelCarrier());
    }

    private boolean showMerchants() {
        return !isLevelMerchant();
    }

    private boolean showIsos(Long repId) {
        return repId == null && isLevelCarrier();
    }

    private boolean showPinNumber() {
        return !isLevelCarrier();
    }

    private boolean showInvoiceId() {
        boolean invoiceNumberEnabled = hasPermissionEnabled(DebisysConstants.INVOICE_NUMBER);
        return invoiceNumberEnabled && isInternational() ;
    }


    private TransactionsReportParam buildParams(String sortFieldName, String sortOrder, int startPage, int endPage,
                                                Date startDate, Date endDate, List<Long> merchantIds, String trxId, String pinNumber,
                                                String invoiceNumber, List<Long> isoIds) {

        TransactionsReportParam params = new TransactionsReportParam(TransactionsReportType.ISO_OR_CARRIER_REPORT);

        //Pagination info
        params.setSortFieldName(sortFieldName);
        params.setSortOrder(sortOrder);
        params.setStartPage(startPage);
        params.setEndPage(endPage);

        //Deployment Info
        params.setDomesticDeployment(isDomestic());
        params.setInternationalDeployment(isInternational());
        params.setMexicoDeployment(isMexico());
        params.setInstanceName(getInstanceName());

        //Session info
        params.setRefId(Long.parseLong(getRefId()));
        params.setAccessLevel(getAccessLevel());
        params.setDistChainType(getDistChainType());

        //Report conditions
        params.setStartDate(DateUtil.atStartOfDay(startDate));
        params.setEndDate(DateUtil.atEndOfDay(endDate));

        params.setTransactionId(trxId);
        params.setPinNumber(pinNumber);
        params.setInvoiceNumber(invoiceNumber);

        //params.setAllMerchants(allMids);
        //List<Long> mIds = buildToList(merchantIds);
        params.setMerchantIds(merchantIds);
        boolean merchantListEmpty = merchantIds == null || merchantIds.isEmpty();
        if(showMerchants()){
            if(merchantListEmpty) {
                params.setAllMerchants(true);
            }
        }else{
            //Is Merchant Level. We take current refId
            long merchantId = Long.parseLong(getRefId());
            merchantIds = Arrays.asList(merchantId);
            params.setMerchantIds(merchantIds);
        }


        //List<Long> repIds = buildToList(isoIds);
        params.setRepIds(isoIds);

        int mxValueAddedTax = getProperty("mxValueAddedTax", 0);
        params.setMxValueAddedTax(String.valueOf(mxValueAddedTax));

        //Permissions
        boolean permShowAdditionalData = allowPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA);
        params.setPermShowAdditionalData(permShowAdditionalData);

        boolean permEnableTaxCalc = allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
        params.setPermEnableTaxCalc(permEnableTaxCalc);

        boolean permViewRefCard = allowPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
        params.setPermViewRefCard(permViewRefCard);

        boolean permViewFilterS2KRatePlan = allowPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS);
        params.setPermViewFilterS2KRatePlan(permViewFilterS2KRatePlan);

        boolean permViewAchDate = allowPermission(DebisysConstants.PERM_VIEW_ACH_DATE);
        params.setPermViewAchDate(permViewAchDate);

        boolean permViewPin = allowPermission(DebisysConstants.PERM_VIEWPIN);
        params.setPermViewPin(permViewPin);

        boolean permShowQrCodeTrx = allowPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS);
        params.setShowQrCodeTransactions(permShowQrCodeTrx);
        
        boolean showExternalMerchantId = allowPermission(DebisysConstants.PERM_SHOW_EXTERNAL_MERCHANT_ID);
        params.setShowExternalMerchantId(showExternalMerchantId);
        
        return params;
    }

    private String buildIds(String csvIds, Set<Long> idList) {
        String searchValue = null;
        if(StringUtil.isNotEmptyStr(csvIds)){
            String[] split = csvIds.split(",");
            for (String id : split) {
                if(id.indexOf("*") == 0){
                    searchValue = id.substring(1, id.length());
                    continue;
                }
                if(StringUtils.isNumeric(id)) {
                    idList.add(Long.valueOf(id));
                }
            }
        }
        return searchValue;
    }

    private List<Long> buildMerchantIds(String csvIds){
        Set<Long> idList = new HashSet<>();
        String searchValue = buildIds(csvIds, idList);
        searchAdditionalMerchants(searchValue, idList);
        List<Long> mIds = new ArrayList<>(idList);
        return mIds;
    }

    private List<Long> buildIsoIds(String csvIds){
        Set<Long> idList = new HashSet<>();
        String searchValue = buildIds(csvIds, idList);
        searchAdditionalIsos(searchValue, idList);
        List<Long> mIds = new ArrayList<>(idList);
        return mIds;
    }


    private void searchAdditionalMerchants(String searchValue, Set<Long> idList) {
        //searchValue = searchValue == null ? "" : searchValue;
        if(StringUtil.isNotEmptyStr(searchValue)) {
            Vector<Vector<String>> merchants = searchMerchants(null, null);
            for (Vector<String> merchant : merchants) {
                if (merchant.get(1).toUpperCase().contains(searchValue.toUpperCase())) {
                    idList.add(Long.valueOf(merchant.get(0)));
                }
            }
        }
    }

    private void searchAdditionalIsos(String searchValue, Set<Long> idList) {
        //TODO:
/*        Vector isos = searchIsos();
        for (Object merchant : isos) {
            Vector<String> iso = (Vector) merchant;
            if(iso.get(0).toUpperCase().contains(searchValue.toUpperCase())){
                idList.add(Long.valueOf(iso.get(1)));
            }
        }*/
    }


    private void fillMerchants(Map<String, Object> model, String repName, Long repId){
        Vector<Vector<String>> merchants = searchMerchants(repName, repId);
        model.put("merchantList", merchants);
    }

    private Vector<Vector<String>> searchMerchants(String repName, Long repId){
        try {
            Vector<Vector<String>> merchantList = null;
            if(StringUtil.isNotEmptyStr(repName) && isLevelCarrier()){
                merchantList = TransactionReport.getDescendantActors(repId, DebisysConstants.MERCHANT);
            }else{
                merchantList = Merchant.getMerchantListReports(getSessionData());
            }
            return merchantList;
        } catch (Exception e) {
            log.error("Error listing merchants. Ex = {}", e);
            throw new TechnicalException("Error listing merchants", e);
        }
    }

    private Vector searchIsos(){
        try {
            Vector ISOs = (new CustomerSearch()).searchRep(1, Integer.MAX_VALUE, getSessionData(), DebisysConstants.REP_TYPE_ISO_5_LEVEL);
            return ISOs;
        } catch (Exception e) {
            log.error("Error listing merchants. Ex = {}", e);
            throw new TechnicalException("Error listing merchants", e);
        }
    }

    private void fillIsos(Map<String, Object> model){
        Vector ISOs = searchIsos();
        model.put("isoList", ISOs);
    }

    @Override
    public int getSection() {
        return 3;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }
}
