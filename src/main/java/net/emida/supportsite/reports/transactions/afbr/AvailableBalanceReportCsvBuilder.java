package net.emida.supportsite.reports.transactions.afbr;

import com.debisys.languages.Languages;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
@Service
public class AvailableBalanceReportCsvBuilder implements CsvBuilderService {


    private static final Logger log = LoggerFactory.getLogger(AvailableBalanceReportCsvBuilder.class);

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {

            List<CsvRow> data = (List<CsvRow>) list;
            //File to fill
            writer = getFileWriter(filePath);

            log.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            List<String> headers = generateHeaders(additionalData);
            String headerRow = CsvUtil.generateRow(headers, true);
            writer.write(headerRow);

            fillWriter(data, writer, additionalData);

        }catch (Exception ex){
            log.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        }finally {
            if(writer != null){
                try {
                    writer.flush();
                    writer.close();
                }catch (Exception ex){

                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {

        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            writer.write(stringifyRow);
        }

    }


    private List<String> generateHeaders(Map<String, Object> additionalData){
        String language = (String) additionalData.get("language");
        List<String> headers = new ArrayList<String>();

        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.id", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.insertDate", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.date", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.userName", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.entityType", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.entityId", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.changeType", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.oldValue", language));
        headers.add(Languages.getString("jsp.admin.reports.balanceAudit.field.newValue", language));
        return headers;

    }

}


