package net.emida.supportsite.reports.transactions.afbr.dao;

import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportModel;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Franky Villadiego
 */
public class AvailableBalanceReportMapper implements RowMapper<AvailableBalanceReportModel>{


    @Override
    public AvailableBalanceReportModel mapRow(ResultSet rs, int i) throws SQLException {

        AvailableBalanceReportModel model = new AvailableBalanceReportModel();

        model.setRowNum(rs.getLong("RWN"));
        model.setRepId(rs.getLong("RepID"));
        model.setRepName(rs.getString("RepName"));
        model.setFinalBalance(rs.getBigDecimal("FinalBalance"));
        model.setLastTrxDate(rs.getTimestamp("LastTrxDate"));
        model.setSku(rs.getString("Sku"));
        model.setAmount(rs.getBigDecimal("Amount"));

        return model;
    }
}
