package net.emida.supportsite.reports.transactions;

import com.debisys.utils.StringUtil;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Date;

/**
 * @author Franky Villadiego
 */
public class TransactionsReportForm {

    private Date startDate;
    private Date endDate;
    private Long transactionId;

    private String invoiceId;       //International and InvoiceNumberPermission(57) is enabled
    private Long pinNumber;         //Not for AccessLevel CARRIER

    private boolean allMerchants;
    private String[] merchantIds;   //Not for AccessLevel MERCHANT

    private boolean allIsos;
    private String[] isoIds;        //For AccessLevel CARRIER


    private String report;
    private String repName;
    private Long repId;


    @NotNull(message = "{com.debisys.reports.error1}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }



    @NotNull(message = "{com.debisys.reports.error3}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Long getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(Long pinNumber) {
        this.pinNumber = pinNumber;
    }

    public String[] getMerchantIds() {
        return merchantIds;
    }

    public void setMerchantIds(String[] merchantIds) {
        this.merchantIds = merchantIds;
    }

    public String[] getIsoIds() {
        return isoIds;
    }

    public void setIsoIds(String[] isoIds) {
        this.isoIds = isoIds;
    }


    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public Long getRepId() {
        return repId;
    }

    public void setRepId(Long repId) {
        this.repId = repId;
    }

    public boolean isAllMerchants() {
        return allMerchants;
    }

    public void setAllMerchants(boolean allMerchants) {
        this.allMerchants = allMerchants;
    }

    public boolean isAllIsos() {
        return allIsos;
    }

    public void setAllIsos(boolean allIsos) {
        this.allIsos = allIsos;
    }

    public String toCsvMerchantIds(){
        if(this.getMerchantIds() != null && this.getMerchantIds().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getMerchantIds()) {
                if(StringUtil.isNotEmptyStr(val)) {
                    sb1.append(val).append(",");
                }
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

    public String toCsvIsoIds(){
        if(this.getIsoIds() != null && this.getIsoIds().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getIsoIds()) {
                if(StringUtil.isNotEmptyStr(val)) {
                    sb1.append(val).append(",");
                }
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

    @Override
    public String toString() {
        return "TransactionsReportForm[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", transactionId=" + transactionId +
                ", invoiceId='" + invoiceId + '\'' +
                ", pinNumber=" + pinNumber +
                ", merchantIds=" + Arrays.toString(merchantIds) +
                ", isoIds=" + Arrays.toString(isoIds) +
                ']';
    }
}
