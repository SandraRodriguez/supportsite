package net.emida.supportsite.reports.transactions.afbr;

/**
 * @author Franky Villadiego
 */
public class AvailableBalanceReportForm {


    private Long repId;
    private String[] reps;


    public Long getRepId() {
        return repId;
    }

    public void setRepId(Long repId) {
        this.repId = repId;
    }

    public String[] getReps() {
        return reps;
    }

    public void setReps(String[] reps) {
        this.reps = reps;
    }

    //for input hidden on download button
    public String toCsvRepsIds(){
        if(this.getReps() != null && this.getReps().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getReps()) {
                sb1.append(val).append(",");
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

}
