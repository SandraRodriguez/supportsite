package net.emida.supportsite.reports.transactions.afbr.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Franky Villadiego
 */
public class AvailableBalanceQueryBuilder {

    private static final Logger log = LoggerFactory.getLogger(AvailableBalanceQueryBuilder.class);

    //Commons fields
    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;

    private String repsIds;

    public AvailableBalanceQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);

        this.repsIds = (String) queryData.get("repId");
        if(repsIds == null || repsIds.trim().isEmpty()){
            this.repsIds = (String)queryData.get("repsIds");
        }

    }

    public AvailableBalanceQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";

        this.repsIds = (String) queryData.get("repId");
        if(repsIds == null || repsIds.trim().isEmpty()){
            this.repsIds = (String)queryData.get("repsIds");
        }

    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");

        return sb0.toString();
    }

    public String buildListQuery() throws Exception {

        if(sortFieldName == null || sortFieldName.isEmpty()){
            sortFieldName = "RepId";
            sortOrder = " ASC ";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();

    }

    private String mainQuery(){

        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("SELECT ").append(queryFields()); //SELECT
        mainQuery.append(queryFrom());                     //FROM
        mainQuery.append(queryWhere());                    //WHERE
        mainQuery.append(buildRepsCondition(this.repsIds));

        return mainQuery.toString();
    }

    private String queryFields(){
        StringBuilder sb = new StringBuilder()
            .append("srmt.rep_id AS repId,")
            .append("srmt.businessname AS repName,")
            .append("srmt.amount AS finalBalance,")
            .append("srmt.datetime AS lastTrxDate,")
            .append("prd.id AS sku,")
            .append("trx.amount AS amount ");

        return sb.toString();
    }

    private String queryFrom(){
        StringBuilder sb = new StringBuilder()
        .append("FROM shared_rep_max_transactions srmt WITH (NOLOCK) ")
        .append("INNER JOIN reps rp WITH (NOLOCK) ON rp.rep_id = srmt.rep_id ")
        .append("INNER JOIN transactions trx WITH (NOLOCK) ON trx.rec_id = srmt.rec_id ")
        .append("INNER JOIN products prd WITH (NOLOCK) ON prd.id = trx.recordedproductid ");
        return sb.toString();
    }

    private String queryWhere(){
        StringBuilder sb = new StringBuilder()
        .append("WHERE 1 = 1");
        return sb.toString();
    }

    private String buildRepsCondition(String repsIds){
        if (repsIds != null && !repsIds.trim().isEmpty() && repsIds.trim().length() > 2){
            StringBuilder sb = new StringBuilder();
            sb.append(" AND srmt.rep_id IN (").append(repsIds).append(") ");
            return sb.toString();
        }
        return "";
    }

}
