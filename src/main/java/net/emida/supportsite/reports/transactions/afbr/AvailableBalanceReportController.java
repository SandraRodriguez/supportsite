package net.emida.supportsite.reports.transactions.afbr;

import com.debisys.customers.Merchant;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.reports.TransactionReport;
import net.emida.supportsite.customers.reps.RepDao;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.spiff.deatil.SpiffDetailReportForm;
import net.emida.supportsite.reports.spiff.deatil.dao.SpiffDetailReportModel;
import net.emida.supportsite.reports.transactions.afbr.dao.AvailableBalanceReportModel;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Franky Villadiego
 */

@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class AvailableBalanceReportController extends BaseController{

    private static final Logger log = LoggerFactory.getLogger(AvailableBalanceReportController.class);

    @Autowired
    private AvailableBalanceDao availableBalanceDao;

    @Autowired
    private RepDao repDao;

    @Autowired
    @Qualifier("availableBalanceReportCsvBuilder")
    private CsvBuilderService afbrCsvBuilderService;

    @RequestMapping(value = UrlPaths.REPS_AFBR_REPORT_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute AvailableBalanceReportForm reportModel){

        fillReps(model);

        return "reports/afbr/availableBalanceReportForm";
    }

    @RequestMapping(value = UrlPaths.REPS_AFBR_REPORT_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid AvailableBalanceReportForm reportModel, BindingResult result) throws TechnicalException, ApplicationException {

        log.debug("reportBuil {}", result);

        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        if(result.hasErrors()){
            model.addAttribute("reportModel", reportModel);
            fillReps(model.asMap());
            return "reports/afbr/availableBalanceReportForm";
        }

        String csvRepsIds = reportModel.toCsvRepsIds();
        String repId = reportModel.getRepId() != null ? reportModel.getRepId().toString() : "";

        if(repId != null && !repId.trim().isEmpty()){
            List<Map<String, Object>> repList = new ArrayList<Map<String, Object>>();
            boolean isValidRepId = isValidRepId(repId, repList);
            if(!isValidRepId){
                model.addAttribute("reportModel", reportModel);
                model.asMap().put("repList", repList);
                result.rejectValue("repId", "invalid");
                return "reports/afbr/availableBalanceReportForm";
            }
        }
        Map<String, Object> queryData = fillQueryData(repId, csvRepsIds);

        Long totalRecords = availableBalanceDao.count(queryData);

        //for hidden input on download button
        model.addAttribute("repsIds", csvRepsIds);
        model.addAttribute("repId", repId);
        model.addAttribute("totalRecords", totalRecords);

        return "reports/afbr/availableBalanceReportResult";
    }

    private boolean isValidRepId(String repId, List<Map<String, Object>> repList) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        List<Map<String, Object>> list = repDao.list(queryData, true);
        repList.addAll(list);
        for (Map<String, Object> row : list) {
            BigDecimal validRepId = (BigDecimal) row.get("repId");
            if (repId.equals(validRepId.toString())) return true;
        }
        return false;
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.REPS_AFBR_REPORT_GRID_PATH, produces="application/json")
    @ResponseBody
    public List<AvailableBalanceReportModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
                                                 @RequestParam String rows, @RequestParam String repId, @RequestParam String repsIds){

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. RepId={}. Reps={}", repId, repsIds);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = fillQueryData(repId, repsIds);

        List<AvailableBalanceReportModel> list = availableBalanceDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);

        return list;
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.REPS_AFBR_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String repId, @RequestParam String repsIds,
                                   @RequestParam Integer totalRecords, @RequestParam String sortField, @RequestParam String sortOrder) throws IOException {

        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file. RepId={}. Reps={}", repId, repsIds);

        Map<String, Object> queryData = fillQueryData(repId, repsIds);
        List<AvailableBalanceReportModel> list = availableBalanceDao.list(1, totalRecords, sortField, sortOrder, queryData);

        log.debug("Total list={}", list.size());

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.debug("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());

        afbrCsvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.debug("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();

        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

        return zipFileResource;

    }

    private void fillReps(Map model){
        try {
            Map<String, Object> queryData = new HashMap<String, Object>();
            queryData.put("accessLevel", getAccessLevel());
            queryData.put("distChainType", getDistChainType());
            queryData.put("refId", getRefId());
            List<Map<String, Object>> list = repDao.list(queryData, true);
            //Vector<Vector> repList = TransactionReport.getRepList(getSessionData());
            model.put("repList", list);
        } catch (TechnicalException e) {
            log.error("Error listing reps. Ex = {}", e);
        }
    }

    private Map<String, Object> fillQueryData(String repId, String repsIds){
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        if((repId == null || repId.trim().isEmpty()) && (repsIds == null || repsIds.isEmpty() || "-1".equals(repsIds))){
            try {
                List<Map<String, Object>> list = repDao.list(queryData, true);
                //Vector<Vector> repList = TransactionReport.getRepList(getSessionData());
                String allRepsIds = tokenizer(list);
                log.debug("AllRepsIds={}", allRepsIds);
                queryData.put("repId", "");
                queryData.put("repsIds", allRepsIds);
            }catch (TechnicalException e){
                log.error("Error listing reps. Ex = {}", e);
                //throw new TechnicalException("Error listing reps", e);
            }

        }else {
            queryData.put("repId", repId);
            queryData.put("repsIds", repsIds);
        }

        return queryData;
    }


    private String tokenizer(List<Map<String, Object>> repList){
        StringBuilder sb = new StringBuilder();
        for (Map<String, Object> row : repList) {
            BigDecimal repId = (BigDecimal) row.get("repId");
            sb.append(repId.toString()).append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 21;
    }
}
