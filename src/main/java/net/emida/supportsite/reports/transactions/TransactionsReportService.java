package net.emida.supportsite.reports.transactions;

/**
 * @author Franky Villadiego
 */

public interface TransactionsReportService {


    ReportData buildReportData(TransactionsReportParam param);

    TransactionsReportResult buildReport(TransactionsReportParam param);

}
