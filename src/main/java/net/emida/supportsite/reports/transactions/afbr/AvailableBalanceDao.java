package net.emida.supportsite.reports.transactions.afbr;

import net.emida.supportsite.reports.transactions.afbr.dao.AvailableBalanceReportModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
public interface AvailableBalanceDao {

    List<AvailableBalanceReportModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    Long count(Map<String, Object> queryData) throws TechnicalException;

}
