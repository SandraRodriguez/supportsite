package net.emida.supportsite.reports.transactions.engine;

import com.debisys.utils.DebisysConstants;
import java.math.BigDecimal;
import java.util.Date;
import net.emida.supportsite.mvc.taxes.TaxService;
import net.emida.supportsite.mvc.timezones.TimeZoneDao;
import net.emida.supportsite.mvc.timezones.TimeZoneDates;
import net.emida.supportsite.mvc.timezones.TimeZoneInfoPojo;
import net.emida.supportsite.reports.transactions.*;
import net.emida.supportsite.util.TimeZoneInfo;
import net.emida.supportsite.util.exceptions.ApplicationException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Franky Villadiego
 */

@Service
public class DefaultTransactionsReportService implements TransactionsReportService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTransactionsReportService.class);


    private TimeZoneDao timeZoneDao;

    private TaxService taxService;
    private TransactionsReportDao transactionsReportDao;


    @Override
    public ReportData buildReportData(TransactionsReportParam param) {
        LOG.info("Building report data for transaction report. From :{} to :{}", param.getStartDate(), param.getEndDate());

        boolean validDates = isValidDates(param.getStartDate(), param.getEndDate());

        if(!validDates){
            throw new ApplicationException("Invalid dates");
        }

        boolean withTax = param.isInternationalDeployment() && param.isPermEnableTaxCalc();


        ReportData rd = new ReportData();
        rd.setShowCommissionDetails(param.isShowCommissionDetails());
        rd.setSortFieldName(param.getSortFieldName());
        rd.setSortOrder(param.getSortOrder());
        rd.setStartPage(param.getStartPage());
        rd.setEndPage(param.getEndPage());
        //rd.setEntityId(param.getEntityId());
        rd.setReportType(param.getReportType());
        rd.setRefId(param.getRefId());
        rd.setAccessLevel(param.getAccessLevel());
        rd.setDistChainType(param.getDistChainType());

        rd.setWithTax(withTax);
        rd.setIncludeTax(param.isIncludeTax());
        rd.setShowExternalMerchantId(param.showExternalMerchantId());

        //pre-calculation for tax if needed
        if(withTax){
            String strTax = getStrTax(param.getRefId(), param.getAccessLevel(), param.getDistChainType(), param.getEndDate());
            rd.setStrTax(strTax);
        }else if(param.isIncludeTax()){
            String strTax = getStrTax(param.getMxValueAddedTax());
            rd.setStrTax(strTax);
        }

        String strAccessLevel = String.valueOf(param.getAccessLevel());
        boolean viewPin = param.isPermViewPin() && DebisysConstants.ISO.equalsIgnoreCase(strAccessLevel) && param.isDomesticDeployment();
        boolean pinNotEmpty = StringUtils.isNotEmpty(param.getPinNumber());

        boolean withAchDate = param.isPermViewAchDate() && DebisysConstants.ISO.equalsIgnoreCase(strAccessLevel) && param.isDomesticDeployment();
        boolean viewReferenceCard = param.isInternationalDeployment() && param.isPermViewRefCard();
        boolean includeAdditionalData = isIncludeAdditionalData(param, param.isPermShowAdditionalData());

        rd.setViewPin(viewPin);
        rd.setPinNotEmpty(pinNotEmpty);

        rd.setWithAchDate(withAchDate);
        rd.setViewReferenceCard(viewReferenceCard);
        rd.setIncludeAdditionalData(includeAdditionalData);
        rd.setShowQrCodeTransactions(param.isShowQrCodeTransactions());

        if(pinNotEmpty){
            //String pin = pinService.getPin(param.getPinNumber());
            rd.setPin(param.getPinNumber());
        }

        rd.setValidDates(validDates);
        if(validDates){
            int accessLevel = Integer.parseInt(param.getAccessLevel());
            TimeZoneDates tzd = timeZoneDao.getTimeZoneFilterDates(accessLevel, new BigDecimal(param.getRefId()),
                    param.getStartDate(), param.getEndDate(), false);
            rd.setTimeZonedDates(tzd);
        }

        if (!param.getReportType().name().equals(DebisysConstants.PW_REF_TYPE_MERCHANT)){
            rd.setAllMerchants(param.isAllMerchants());
        } else{
            rd.setAllMerchants(false);
        }
        rd.setMerchantIds(param.getMerchantIds());
        rd.setTransactionId(param.getTransactionId());
        rd.setInvoiceNumber(param.getInvoiceNumber());
        rd.setMillenniumNum(param.getMillenniumNum());
        rd.setProductIds(param.getProductIds());
        rd.setPermViewFilterS2KRatePlan(param.isPermViewFilterS2KRatePlan());

        rd.setRepIds(param.getRepIds());
        rd.setRatePlanIds(param.getRatePlanIds());

        //TimeZone information
        TimeZoneInfo defaultTZI = buildDefaultTimeZoneInfo(param.getInstanceName());
        rd.setDefaultTimeZoneInfo(defaultTZI);

        int accessLevel = Integer.parseInt(param.getAccessLevel());
        TimeZoneInfo entityTZI = buildEntityTimeZoneInfo(accessLevel, param.getRefId());
        rd.setEntityTimeZoneInfo(entityTZI == null ? defaultTZI : entityTZI);


        return rd;
    }

    private TimeZoneInfo buildDefaultTimeZoneInfo(String instanceName) {
        TimeZoneInfo tzi = null;
        TimeZoneInfoPojo timeZoneInfoPojo = timeZoneDao.findDefault(instanceName);
        if(timeZoneInfoPojo != null){
            LOG.info("Using default TimeZone :{}", timeZoneInfoPojo.getCodeName());
            tzi = new TimeZoneInfo();
            BeanUtils.copyProperties(timeZoneInfoPojo, tzi);
        }else{
            LOG.warn("!!! No default TimeZone available !!!");
        }
        return tzi;
    }

    private TimeZoneInfo buildEntityTimeZoneInfo(int accessLevel, long refId) {
        TimeZoneInfo tzi = null;
        TimeZoneInfoPojo timeZoneInfoPojo = null;
        if(String.valueOf(accessLevel).equals(DebisysConstants.MERCHANT)){
            timeZoneInfoPojo = timeZoneDao.findByMerchantId(refId);
        }else{
            timeZoneInfoPojo = timeZoneDao.findByRepsId(refId);
        }
        if(timeZoneInfoPojo != null){
            tzi = new TimeZoneInfo();
            BeanUtils.copyProperties(timeZoneInfoPojo, tzi);
            LOG.info("Using entity TimeZone :{}", timeZoneInfoPojo.getCodeName());
        }else{
            LOG.warn("!!! No entity TimeZone available !!!");
        }
        return tzi;
    }


    @Override
    public TransactionsReportResult buildReport(TransactionsReportParam param) {

        ReportData reportData = buildReportData(param);

        TransactionsReportResult result = transactionsReportDao.getResult(reportData);
        LOG.info("Report result data done!!!");

        return result;
    }

    private boolean isIncludeAdditionalData(TransactionsReportParam param, boolean showAdditionalData) {
        String strAccessLevel = String.valueOf(param.getAccessLevel());
        return showAdditionalData && param.getReportType().equals(TransactionsReportType.ISO_OR_CARRIER_REPORT)
                && (strAccessLevel.equalsIgnoreCase(DebisysConstants.CARRIER) ||
                strAccessLevel.equalsIgnoreCase(DebisysConstants.ISO));
    }

    private String getStrTax(long refId, String strAccessLevel, String strDistChainType, Date endDate) {
        int accessLevel = Integer.parseInt(strAccessLevel);
        int distChainType = Integer.valueOf(strDistChainType);
        BigDecimal tax = taxService.getTax(refId, accessLevel, distChainType, endDate);
        return tax.toString();
    }

    private String getStrTax(String mxValueAddedTax) {
        BigDecimal tax = taxService.getTax(mxValueAddedTax);
        return tax.toString();
    }

    private boolean isValidDates(Date startDate, Date endDate) {
        if(startDate == null || endDate == null) return false;
        return startDate.compareTo(endDate) != 1;
    }





    @Autowired
    public void setTaxService(TaxService taxService) {
        this.taxService = taxService;
    }



    @Autowired
    public void setTimeZoneDao(TimeZoneDao timeZoneDao) {
        this.timeZoneDao = timeZoneDao;
    }

    @Autowired
    public void setTransactionsReportDao(TransactionsReportDao transactionsReportDao) {
        this.transactionsReportDao = transactionsReportDao;
    }
}
