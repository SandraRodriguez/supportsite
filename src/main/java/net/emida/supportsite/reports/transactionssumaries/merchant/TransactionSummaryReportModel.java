package net.emida.supportsite.reports.transactionssumaries.merchant;

import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Franky Villadiego
 */
public class TransactionSummaryReportModel implements CsvRow{

    private Long rowNum;

    private String salesmanId;          //salesman_id.          Type:varchar.   Class:java.lang.String
    private String salesmanName;        //salesmanname.         Type:nvarchar.  Class:java.lang.String
    private String accountNo;           //account_no.           Type:varchar.   Class:java.lang.String
    private String dba;                 //dba.                  Type:varchar.   Class:java.lang.String
    private String additionalData;      //additionalData.       Type:varchar.   Class:java.lang.String
    private Integer routeId;            //routeId.              Type:int.       Class:java.lang.Integer
    private String routeName;
    private Long merchantId;            //merchant_id.          Type:decimal.   Class:java.math.BigDecimal
    private String invoiceType;         //invoice_type.         Type:nvarchar.  Class:java.lang.String

    private String physAddress;         //phys_address.         Type:varchar.   Class:java.lang.String
    private String physCity;            //phys_city.            Type:varchar.   Class:java.lang.String
    private String physState;           //phys_state.           Type:char.      Class:java.lang.String
    private String physZip;             //phys_zip.             Type:char.      Class:java.lang.String


    private double qty;                 //accumulated
    private double totalSales;          //accumulated
    private BigDecimal totalRecharge = new BigDecimal(0);   //accumulated
    private double merchantCommission;  //accumulated
    private double repCommission;       //accumulated
    private double subAgentCommission;  //accumulated
    private double agentCommission;     //accumulated
    private double isoCommission;       //accumulated

    private double availableCredit;
    private double adjAmount;

    private BigDecimal totalBonus = new BigDecimal(0);
    private BigDecimal liabilityLimit = new BigDecimal(0);
    private BigDecimal runningLiability = new BigDecimal(0);


    //With No Tax
    private BigDecimal availableCredit2 = new BigDecimal(0);



    /// With TAX
    private double taxTotalSales;           //accumulated
    private double taxMerchantCommission;   //accumulated
    private double taxRepCommission;        //accumulated
    private double taxSubAgentCommission;   //accumulated
    private double taxAgentCommission;      //accumulated
    private double taxIsoCommission;        //accumulated

    private double taxAmount;               //Accumulated


    public TransactionSummaryReportModel() {
    }

    public TransactionSummaryReportModel(Long merchantId) {
        this.merchantId = merchantId;
    }


    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getDba() {
        return dba;
    }

    public void setDba(String dba) {
        this.dba = dba;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }


    public String getPhysAddress() {
        return physAddress;
    }

    public void setPhysAddress(String physAddress) {
        this.physAddress = physAddress;
    }

    public String getPhysCity() {
        return physCity;
    }

    public void setPhysCity(String physCity) {
        this.physCity = physCity;
    }

    public String getPhysState() {
        return physState;
    }

    public void setPhysState(String physState) {
        this.physState = physState;
    }

    public String getPhysZip() {
        return physZip;
    }

    public void setPhysZip(String physZip) {
        this.physZip = physZip;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }

    public double getMerchantCommission() {
        return merchantCommission;
    }

    public void setMerchantCommission(double merchantCommission) {
        this.merchantCommission = merchantCommission;
    }

    public double getRepCommission() {
        return repCommission;
    }

    public void setRepCommission(double repCommission) {
        this.repCommission = repCommission;
    }

    public double getSubAgentCommission() {
        return subAgentCommission;
    }

    public void setSubAgentCommission(double subAgentCommission) {
        this.subAgentCommission = subAgentCommission;
    }

    public double getAgentCommission() {
        return agentCommission;
    }

    public void setAgentCommission(double agentCommission) {
        this.agentCommission = agentCommission;
    }

    public double getIsoCommission() {
        return isoCommission;
    }

    public void setIsoCommission(double isoCommission) {
        this.isoCommission = isoCommission;
    }

    public double getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(double availableCredit) {
        this.availableCredit = availableCredit;
    }

    public double getAdjAmount() {
        return adjAmount;
    }

    public void setAdjAmount(double adjAmount) {
        this.adjAmount = adjAmount;
    }

    public BigDecimal getTotalBonus() {
        return totalBonus;
    }

    public void setTotalBonus(BigDecimal totalBonus) {
        this.totalBonus = totalBonus;
    }

    public BigDecimal getTotalRecharge() {
        return totalRecharge;
    }

    public void setTotalRecharge(BigDecimal totalRecharge) {
        this.totalRecharge = totalRecharge;
    }

    public BigDecimal getLiabilityLimit() {
        return liabilityLimit;
    }

    public void setLiabilityLimit(BigDecimal liabilityLimit) {
        this.liabilityLimit = liabilityLimit;
    }

    public BigDecimal getRunningLiability() {
        return runningLiability;
    }

    public void setRunningLiability(BigDecimal runningLiability) {
        this.runningLiability = runningLiability;
    }

    public BigDecimal getAvailableCredit2() {
        return availableCredit2;
    }

    public void setAvailableCredit2(BigDecimal availableCredit2) {
        this.availableCredit2 = availableCredit2;
    }

    public double getTaxTotalSales() {
        return taxTotalSales;
    }

    public void setTaxTotalSales(double taxTotalSales) {
        this.taxTotalSales = taxTotalSales;
    }

    public double getTaxMerchantCommission() {
        return taxMerchantCommission;
    }

    public void setTaxMerchantCommission(double taxMerchantCommission) {
        this.taxMerchantCommission = taxMerchantCommission;
    }

    public double getTaxRepCommission() {
        return taxRepCommission;
    }

    public void setTaxRepCommission(double taxRepCommission) {
        this.taxRepCommission = taxRepCommission;
    }

    public double getTaxSubAgentCommission() {
        return taxSubAgentCommission;
    }

    public void setTaxSubAgentCommission(double taxSubAgentCommission) {
        this.taxSubAgentCommission = taxSubAgentCommission;
    }

    public double getTaxAgentCommission() {
        return taxAgentCommission;
    }

    public void setTaxAgentCommission(double taxAgentCommission) {
        this.taxAgentCommission = taxAgentCommission;
    }

    public double getTaxIsoCommission() {
        return taxIsoCommission;
    }

    public void setTaxIsoCommission(double taxIsoCommission) {
        this.taxIsoCommission = taxIsoCommission;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

///// Helper Methods \\\\\\

    public void addQty(double qty) {
        this.qty += qty;
    }

    public void addTotalSales(double totalSales) {
        this.totalSales += totalSales;
    }

    public void addTotalRecharge(BigDecimal totalRecharge) {
        this.totalRecharge = totalRecharge != null ? this.totalRecharge.add(totalRecharge) : this.totalRecharge;
    }

    public void addMerchantCommission(double merchantCommission) {
        this.merchantCommission += merchantCommission;
    }

    public void addRepCommission(double repCommission) {
        this.repCommission += repCommission;
    }

    public void addSubAgentCommission(double subAgentCommission) {
        this.subAgentCommission += subAgentCommission;
    }

    public void addAgentCommission(double agentCommission) {
        this.agentCommission += agentCommission;
    }

    public void addIsoCommission(double isoCommission) {
        this.isoCommission += isoCommission;
    }



    public void addTaxTotalSales(double taxTotalSales) {
        this.taxTotalSales += taxTotalSales;
    }

    public void addTaxMerchantCommission(double taxMerchantCommission) {
        this.taxMerchantCommission += taxMerchantCommission;
    }

    public void addTaxRepCommission(double taxRepCommission) {
        this.taxRepCommission += taxRepCommission;
    }

    public void addTaxSubAgentCommission(double taxSubAgentCommission) {
        this.taxSubAgentCommission += taxSubAgentCommission;
    }

    public void addTaxAgentCommission(double taxAgentCommission) {
        this.taxAgentCommission += taxAgentCommission;
    }

    public void addTaxIsoCommission(double taxIsoCommission) {
        this.taxIsoCommission += taxIsoCommission;
    }

    public void addTaxAmount(double taxAmount){
        this.taxAmount += taxAmount;
    }
    
    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }


    @Override
    public List<CsvCell> cells() {
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cell0 = new CsvCell(dba);
        list.add(cell0);

        CsvCell cell1 = new CsvCell(merchantId.toString());
        list.add(cell1);

        CsvCell cell2 = new CsvCell(String.valueOf(qty));
        list.add(cell2);

        CsvCell cell3 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(totalSales)));
        list.add(cell3);

        CsvCell cell4 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(merchantCommission)));
        list.add(cell4);

        CsvCell cell5 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(repCommission)));
        list.add(cell5);

        CsvCell cell6 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(subAgentCommission)));
        list.add(cell6);

        CsvCell cell7 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(agentCommission)));
        list.add(cell7);

        CsvCell cell8 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(isoCommission)));
        list.add(cell8);

        CsvCell cell9 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(runningLiability.toString())));
        list.add(cell9);

        CsvCell cell10 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(liabilityLimit.toString())));
        list.add(cell10);

        CsvCell cell11 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(availableCredit)));
        list.add(cell11);

        CsvCell cell12 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(adjAmount)));
        list.add(cell12);

        CsvCell cell13 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(taxTotalSales)));
        list.add(cell13);

        CsvCell cell14 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(taxMerchantCommission)));
        list.add(cell14);

        CsvCell cell15 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(taxRepCommission)));
        list.add(cell15);

        CsvCell cell16 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(taxSubAgentCommission)));
        list.add(cell16);

        CsvCell cell17 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(taxAgentCommission)));
        list.add(cell17);

        CsvCell cell18 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(taxIsoCommission)));
        list.add(cell18);

        CsvCell cell19 = new CsvCell(StringUtil.isNotEmptyStr(physAddress) ? physAddress : "");
        list.add(cell19);

        CsvCell cell20 = new CsvCell(StringUtil.isNotEmptyStr(physCity) ? physCity : "");
        list.add(cell20);

        CsvCell cell21 = new CsvCell(StringUtil.isNotEmptyStr(physState) ? physState : "");
        list.add(cell21);

        CsvCell cell22 = new CsvCell(StringUtil.isNotEmptyStr(physZip) ? physZip : "");
        list.add(cell22);

        CsvCell cell23 = new CsvCell(NumberUtil.formatCurrency(totalBonus.toString()));
        list.add(cell23);

        CsvCell cell24 = new CsvCell(NumberUtil.formatCurrency(totalRecharge.toString()));
        list.add(cell24);

        CsvCell cell25 = new CsvCell(StringUtil.isNotEmptyStr(routeName) ? routeName : "");
        list.add(cell25);

        CsvCell cell26 = new CsvCell(NumberUtil.formatCurrency(String.valueOf(taxAmount)));
        list.add(cell26);

        CsvCell cell27 = new CsvCell(StringUtil.isNotEmptyStr(invoiceType) ? invoiceType : "");
        list.add(cell27);

        CsvCell cell28 = new CsvCell(StringUtil.isNotEmptyStr(salesmanId) ? salesmanId : "");
        list.add(cell28);

        CsvCell cell29 = new CsvCell(StringUtil.isNotEmptyStr(salesmanName) ? salesmanName : "");
        list.add(cell29);

        CsvCell cell30 = new CsvCell(StringUtil.isNotEmptyStr(accountNo) ? accountNo : "");
        list.add(cell30);
        
        CsvCell cell31 = new CsvCell((StringUtil.isNotEmptyStr(additionalData)) ? additionalData : "");
        list.add(cell31);

        return list;
    }


}
