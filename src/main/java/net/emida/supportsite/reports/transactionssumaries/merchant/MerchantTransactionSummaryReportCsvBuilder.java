package net.emida.supportsite.reports.transactionssumaries.merchant;

import com.debisys.languages.Languages;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import net.emida.supportsite.reports.transactionssumaries.merchant.engine.MerchantSummaryTotals;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Franky Villadiego
 */

@Service
public class MerchantTransactionSummaryReportCsvBuilder implements CsvBuilderService {


    private static final Logger log = LoggerFactory.getLogger(MerchantTransactionSummaryReportCsvBuilder.class);


    @Override
    public void buildCsv(String filePath, List<?> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {

            List<CsvRow> data = (List<CsvRow>) list;
            //File to fill
            writer = getFileWriter(filePath);

            log.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            buildReportHeader(writer, additionalData);

            List<String> headers = generateHeaders(additionalData);

            MerchantSummaryTotals totals = (MerchantSummaryTotals) additionalData.get("totals");

            fillWriter(headers, data, totals, writer, additionalData);

        } catch (Exception ex) {
            log.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {

                }
            }
        }
    }

    private void buildReportHeader(Writer writer, Map<String, Object> ad) throws IOException {
        String lang = (String) ad.get("language");

        String timeZoneLabel = (String)ad.get("timeZoneLabel");

        String startDate = (String)ad.get("startDate");
        String endDate = (String) ad.get("endDate");

        String reportTitleLabel = Languages.getString("jsp.admin.reports.transaction.merchant_summary", lang);

        String fromLabel = Languages.getString("jsp.admin.from", lang);
        String toLabel = Languages.getString("jsp.admin.to", lang);

        String transNoReflectedLabel = Languages.getString("jsp.admin.reports.test_trans", lang);


        StringBuilder sb = new StringBuilder();

        printCsvCell(timeZoneLabel, sb,false, true);      //TimeZoneLabel

        String allTitle = reportTitleLabel + " " + fromLabel + " " + startDate + " " + toLabel + " " + endDate;
        printCsvCell(allTitle, sb,false, true);      //Report Title


        printCsvCell(transNoReflectedLabel, sb,false, true);      //No test transactions added

        printCsvCell("", sb,false, true);             //Line break

        writer.write(sb.toString());


    }


    private void printCsvCell(String value, StringBuilder sb, boolean addComma, boolean addLineBreak){
        sb.append("\"").append(value).append("\"");

        if(addComma) sb.append(",");

        if(addLineBreak) sb.append("\n");
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private List<String> generateHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");

        List<String> headers = Arrays.asList(
                Languages.getString("jsp.admin.reports.tran_no", language),         // 0
                Languages.getString("jsp.admin.reports.dba", language),             // 1

                Languages.getString("jsp.admin.customers.merchants_info.address", language),        // 2
                Languages.getString("jsp.admin.customers.merchants_edit.city", language),           // 3
                Languages.getString("jsp.admin.customers.merchants_edit.state_domestic", language), // 4
                Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic", language),   // 5

                Languages.getString("jsp.admin.reports.id", language),          // 6

                Languages.getString("jsp.admin.reports.SalesRepId", language),  // 7
                Languages.getString("jsp.admin.reports.SalesRepName", language),    // 8
                Languages.getString("jsp.admin.reports.AccountNo", language),       // 9

                Languages.getString("jsp.admin.reports.transactions.commerce.invoice_type", language),  //10

                Languages.getString("jsp.admin.customers.merchants_edit.route", language),      //11
                Languages.getString("jsp.admin.customers.merchants_edit.city", language),       //12

                Languages.getString("jsp.admin.reports.qty", language),             // 13
                Languages.getString("jsp.admin.reports.recharge", language),        // 14

                Languages.getString("jsp.admin.reports.bonus", language),           // 15
                Languages.getString("jsp.admin.reports.total_recharge", language),        // 16

                //showTaxCols(mx, intl) ?    -     :
                Languages.getString("jsp.admin.reports.total", language) + Languages.getString("jsp.admin.reports.minusvat2", language),        // 17
                Languages.getString("jsp.admin.reports.netAmount", language),       // 18
                //showTaxAmount(intl)
                Languages.getString("jsp.admin.reports.taxAmount", language),        // 19


                //-- iso_commission
                Languages.getString("jsp.admin.iso_percent", language),         // 20
                Languages.getString("jsp.admin.net_iso_percent", language),     // 21

                Languages.getString("jsp.admin.iso_percent", language) + Languages.getString("jsp.admin.reports.minusvat2", language),        // 22


                //---  agent_commission      ?
                Languages.getString("jsp.admin.agent_percent", language),             //23
                Languages.getString("jsp.admin.net_agent_percent", language),        // 24

                Languages.getString("jsp.admin.agent_percent", language) + Languages.getString("jsp.admin.reports.minusvat2", language),        // 25


                //--- subagent_commission  ?
                Languages.getString("jsp.admin.subagent_percent", language),        // 26
                Languages.getString("jsp.admin.net_subagent_percent", language),     // 27

                Languages.getString("jsp.admin.subagent_percent", language) + Languages.getString("jsp.admin.reports.minusvat2", language),        // 28


                //---  rep_commission  ?
                Languages.getString("jsp.admin.rep_percent", language),            // 29
                Languages.getString("jsp.admin.net_rep_percent", language),        // 30

                Languages.getString("jsp.admin.rep_percent", language) + Languages.getString("jsp.admin.reports.minusvat2", language),        // 31


                //--- merchant_commission   ?
                Languages.getString("jsp.admin.merchant_percent", language),        // 32
                Languages.getString("jsp.admin.net_merchant_percent", language),     // 33

                Languages.getString("jsp.admin.merchant_percent", language) + Languages.getString("jsp.admin.reports.minusvat2", language),        // 34

                Languages.getString("jsp.admin.reports.adjustment", language),          // 35

                Languages.getString("jsp.admin.reports.balance", language),             // 36
                Languages.getString("jsp.admin.reports.limit", language),               // 37
                Languages.getString("jsp.admin.reports.available", language)            // 38


        );


        return headers;

    }


    private void fillWriter(List<String> headers, List<CsvRow> data, MerchantSummaryTotals totals, Writer writer, Map<String, Object> additionalData) throws IOException{

        log.info("Writing CSV file...");

        printHeaders(headers, writer, additionalData);

        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();

            StringBuilder sb = new StringBuilder();

            printCol(0, cells, sb);             // DBA

            if(showAddress(additionalData)) printCol(19, cells, sb);    // Address
            if(showAddress(additionalData)) printCol(20, cells, sb);    //City
            if(showAddress(additionalData)) printCol(21, cells, sb);    //State
            if(showAddress(additionalData)) printCol(22, cells, sb);    //Zip

            printCol(1, cells, sb);             //MerchantId

            if(showS2000Tools(additionalData)) printCol(28, cells, sb);     //SalesRepId
            if(showS2000Tools(additionalData)) printCol(29, cells, sb);     //SalesManName
            if(showS2000Tools(additionalData)) printCol(30, cells, sb);     //AccountNo

            if(showInvoice(additionalData)) printCol(27, cells, sb);        //InvoiceType

            if(showRoutes(additionalData)) printCol(25, cells, sb);        //RouteName
            if(showRoutes(additionalData)) printCol(20, cells, sb);        //City

            printCol(2, cells, sb);             //QTY
            printCol(3, cells, sb);             //TotalSales (Recharge)

            if(showBonus(additionalData)) printCol(23, cells, sb);        //TotalBonus
            if(showBonus(additionalData)) printCol(24, cells, sb);        //TotalRecharge

            if(showTaxColumn(additionalData)) printCol(13, cells, sb);      //TaxTotalSales

            if(showInterTaxCol(additionalData)) printCol(26, cells, sb);           //TaxAmount

            if(showIsoCommission(additionalData)) printCol(8, cells, sb);           //IsoCommission
            if(showTaxIsoCommission(additionalData)) printCol(18, cells, sb);       //TaxIsoCommission

            if(showAgentCommission(additionalData)) printCol(7, cells, sb);        //AgentCommission
            if(showTaxAgentCommission(additionalData)) printCol(17, cells, sb);        //TaxAgentCommission

            if(showSubAgentCommission(additionalData)) printCol(6, cells, sb);           //SubAgentCommission
            if(showTaxSubAgentCommission(additionalData)) printCol(16, cells, sb);       //TaxSubAgentCommission

            if(showRepCommission(additionalData)) printCol(5, cells, sb);           //RepCommission
            if(showTaxRepCommission(additionalData)) printCol(15, cells, sb);      //TaxRepCommission

            printCol(4, cells, sb);             //MerchantCommission
            if(showTaxMerchantCommission(additionalData)) printCol(14, cells, sb);      //TaxMerchantCommission

            if(showIsoCommission(additionalData)) printCol(12, cells, sb);           //AdjAmount

            printCol(9, cells, sb);             //RunningLiability
            printCol(10, cells, sb);             //liabilityLimit
            printCol(11, cells, sb);             //AvailableCredit

            sb.deleteCharAt(sb.length() - 1);
            sb.append("\n");
            writer.write(sb.toString());

        }

        printTotals(writer, totals, additionalData);


        log.info("CSV file has been generated!!!");
    }

    private void printHeaders(List<String> headers, Writer writer, Map<String, Object> additionalData) throws IOException {
        StringBuilder sb = new StringBuilder();

        printColHeader(1, headers, sb);             //DBA

        if(showAddress(additionalData)) printColHeader(2, headers, sb);     //Address
        if(showAddress(additionalData)) printColHeader(3, headers, sb);     //City
        if(showAddress(additionalData)) printColHeader(4, headers, sb);     //State
        if(showAddress(additionalData)) printColHeader(5, headers, sb);     //Zip

        printColHeader(6, headers, sb);             //MerchantId

        if(showS2000Tools(additionalData)) printColHeader(7, headers, sb);     //SalesRepId
        if(showS2000Tools(additionalData)) printColHeader(8, headers, sb);     //SalesManName
        if(showS2000Tools(additionalData)) printColHeader(9, headers, sb);     //AccountNo

        if(showInvoice(additionalData)) printColHeader(10, headers, sb);        //InvoiceType

        if(showRoutes(additionalData)) printColHeader(11, headers, sb);        //RouteName
        if(showRoutes(additionalData)) printColHeader(12, headers, sb);        //City

        printColHeader(13, headers, sb);             //QTY
        printColHeader(14, headers, sb);             //TotalSales (Recharge)

        if(showBonus(additionalData)) printColHeader(15, headers, sb);        //TotalBonus
        if(showBonus(additionalData)) printColHeader(16, headers, sb);        //TotalRecharge

        if(showTaxColumn(additionalData)) {
            if(isMexico(additionalData)) printColHeader(17, headers, sb);           //TaxTotalSales
            if(isInternational(additionalData)) printColHeader(18, headers, sb);
            if(isDomestic(additionalData)) printColHeader(17, headers, sb);
        }

        if(showInterTaxCol(additionalData)) printColHeader(19, headers, sb);           //TaxAmount

        if(showIsoCommission(additionalData)) {
            printColHeader(20, headers, sb);                                    //IsoCommission
        }
        if(showTaxIsoCommission(additionalData)){
            if(isInternational(additionalData)) printColHeader(21, headers, sb); //TaxIsoCommission
            if(isMexico(additionalData)) printColHeader(22, headers, sb);
        }

        if(showAgentCommission(additionalData)) {
            printColHeader(23, headers, sb);                                     //AgentCommission
        }
        if(showTaxAgentCommission(additionalData)) {
            if(isInternational(additionalData)) printColHeader(24, headers, sb); //TaxAgentCommission
            if(isMexico(additionalData)) printColHeader(25, headers, sb);
        }

        if(showSubAgentCommission(additionalData)) {
            printColHeader(26, headers, sb);           //SubAgentCommission
        }
        if(showTaxSubAgentCommission(additionalData)) {
            if(isInternational(additionalData)) printColHeader(27, headers, sb);    //TaxSubAgentCommission
            if(isMexico(additionalData)) printColHeader(28, headers, sb);
        }

        if(showRepCommission(additionalData)) {
            printColHeader(29, headers, sb);                                        //RepCommission
        }
        if(showTaxRepCommission(additionalData)) {
            if(isInternational(additionalData)) printColHeader(30, headers, sb);    //TaxRepCommission
            if(isMexico(additionalData)) printColHeader(31, headers, sb);
        }

        printColHeader(32, headers, sb);             //MerchantCommission
        if(showTaxMerchantCommission(additionalData)) {
            if(isInternational(additionalData)) printColHeader(33, headers, sb);      //TaxMerchantCommission
            if(isMexico(additionalData)) printColHeader(34, headers, sb);
        }
        if(showIsoCommission(additionalData)) printColHeader(35, headers, sb);           //AdjAmount

        printColHeader(36, headers, sb);             //RunningLiability
        printColHeader(37, headers, sb);             //liabilityLimit
        printColHeader(38, headers, sb);             //AvailableCredit


        sb.deleteCharAt(sb.length() - 1);
        sb.append("\n");
        writer.write(sb.toString());
    }


    private void printTotals(Writer writer, MerchantSummaryTotals totals, Map<String, Object> additionalData) throws IOException {

        String language = (String) additionalData.get("language");
        String totalsLabel = Languages.getString("jsp.admin.reports.totals", language);

        StringBuilder sb = new StringBuilder();

        printCol("", sb);             // DBA

        if(showAddress(additionalData)) printCol("",  sb);    // Address
        if(showAddress(additionalData)) printCol("", sb);    //City
        if(showAddress(additionalData)) printCol("", sb);    //State
        if(showAddress(additionalData)) printCol("", sb);    //Zip

        printCol("", sb);             //MerchantId

        if(showS2000Tools(additionalData)) printCol("", sb);     //SalesRepId
        if(showS2000Tools(additionalData)) printCol("", sb);     //SalesManName
        if(showS2000Tools(additionalData)) printCol("", sb);     //AccountNo

        if(showInvoice(additionalData)) printCol("", sb);        //InvoiceType

        if(showRoutes(additionalData)) printCol("", sb);        //RouteName
        if(showRoutes(additionalData)) printCol("", sb);        //City

        sb.delete(sb.length() - 3, sb.length());
        sb.append(totalsLabel + ",");

        printCol(String.valueOf(totals.getQty()), sb);             //QTY
        printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTotalSales())), sb);             //TotalSales (Recharge)

        if(showBonus(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTotalBonus())), sb);        //TotalBonus
        if(showBonus(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTotalRecharge())), sb);        //TotalRecharge

        if(showTaxColumn(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTaxTotalSales())), sb);      //TaxTotalSales

        if(showInterTaxCol(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTotalTax())), sb);           //TaxAmount

        if(showIsoCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getIsoCommission())), sb);           //IsoCommission
        if(showTaxIsoCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTaxIsoCommission())), sb);       //TaxIsoCommission

        if(showAgentCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getAgentCommission())), sb);        //AgentCommission
        if(showTaxAgentCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTaxAgentCommission())), sb);        //TaxAgentCommission

        if(showSubAgentCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getSubagentCommission())), sb);           //SubAgentCommission
        if(showTaxSubAgentCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTaxSubagentCommission())), sb);       //TaxSubAgentCommission

        if(showRepCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getRepCommission())), sb);           //RepCommission
        if(showTaxRepCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTaxRepCommission())), sb);      //TaxRepCommission

        printCol(NumberUtil.formatCurrency(String.valueOf(totals.getMerchantCommission())), sb);             //MerchantCommission
        if(showTaxMerchantCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getTaxMerchantCommission())), sb);      //TaxMerchantCommission

        if(showIsoCommission(additionalData)) printCol(NumberUtil.formatCurrency(String.valueOf(totals.getAdjAmount())), sb);           //AdjAmount

        printCol("", sb);             //RunningLiability
        printCol("", sb);             //liabilityLimit
        printCol("", sb);             //AvailableCredit

        sb.deleteCharAt(sb.length() - 1);
        sb.append("\n");
        writer.write(sb.toString());


    }


    private void printCol(int col, List<CsvCell> cells, StringBuilder sb) {
        sb.append("\"").append(cells.get(col).getValue()).append("\"").append(",");
    }
    private void printCol(String value, StringBuilder sb) {
        sb.append("\"").append(value).append("\"").append(",");
    }
    private void printColHeader(int col, List<String> headers, StringBuilder sb) {
        sb.append("\"").append(headers.get(col)).append("\"").append(",");
    }


    private boolean isInternational(Map<String, Object> additionalData){
        Boolean international = (Boolean) additionalData.get("international");
        return international;
    }
    private boolean isMexico(Map<String, Object> additionalData){
        Boolean mexico = (Boolean) additionalData.get("mexico");
        return mexico;
    }
    private boolean isDomestic(Map<String, Object> additionalData){
        Boolean domestic = (Boolean) additionalData.get("domestic");
        return domestic;
    }

    private boolean showAddress(Map<String, Object> additionalData){
        String accessLevel = (String) additionalData.get("accessLevel");
        boolean levelISO = accessLevel.equals(DebisysConstants.ISO);
        boolean levelCarrier = accessLevel.equals(DebisysConstants.CARRIER);
        return (levelISO && isDomestic(additionalData)) || levelCarrier;
    }
    private boolean showS2000Tools(Map<String, Object> additionalData){
        Boolean s2000Tools = (Boolean) additionalData.get("s2000Tools");
        String accessLevel = (String) additionalData.get("accessLevel");
        boolean levelISO = accessLevel.equals(DebisysConstants.ISO);
        return levelISO && s2000Tools;
    }
    private boolean showInvoice(Map<String, Object> additionalData){
        String accessLevel = (String) additionalData.get("accessLevel");
        boolean levelISO = accessLevel.equals(DebisysConstants.ISO);
        return levelISO;
    }
    private boolean showRoutes(Map<String, Object> additionalData){
        return isInternational(additionalData) || isMexico(additionalData);
    }
    private boolean showBonus(Map<String, Object> additionalData){
        return isInternational(additionalData);
    }
    private boolean showTaxColumn(Map<String, Object> additionalData){
        return showMxTaxCol(additionalData) || showInterTaxCol(additionalData);
    }
    private boolean showMxTaxCol(Map<String, Object> additionalData){
        Boolean taxesMex = (Boolean) additionalData.get("taxesMex");
        return isMexico(additionalData) && taxesMex;
    }
    private boolean showInterTaxCol(Map<String, Object> ad){
        Boolean taxesInt = (Boolean) ad.get("taxesInt");
        return isInternational(ad) && taxesInt;
    }

    private boolean showIsoCommission(Map<String, Object> additionalData){
        String accessLevel = (String) additionalData.get("accessLevel");
        boolean levelISO = accessLevel.equals(DebisysConstants.ISO);
        boolean levelCarrier = accessLevel.equals(DebisysConstants.CARRIER);
        return levelISO || levelCarrier;
    }

    private boolean showTaxIsoCommission(Map<String, Object> ad){
        return showIsoCommission(ad) && (showMxTaxCol(ad) || showInterTaxCol(ad) );
    }

    private boolean showAgentCommission(Map<String, Object> additionalData){
        String accessLevel = (String) additionalData.get("accessLevel");
        boolean levelAgent = accessLevel.equals(DebisysConstants.AGENT);
        return showIsoCommission(additionalData) || levelAgent;
    }

    private boolean showTaxAgentCommission(Map<String, Object> ad){
        return showAgentCommission(ad) && (showMxTaxCol(ad) || showInterTaxCol(ad));
    }

    private boolean showSubAgentCommission(Map<String, Object> additionalData){
        String accessLevel = (String) additionalData.get("accessLevel");
        boolean levelSubAgent = accessLevel.equals(DebisysConstants.SUBAGENT);
        return showAgentCommission(additionalData) || levelSubAgent;
    }

    private boolean showTaxSubAgentCommission(Map<String, Object> ad){
        return showSubAgentCommission(ad) && (showMxTaxCol(ad) || showInterTaxCol(ad));
    }

    private boolean showRepCommission(Map<String, Object> additionalData){
        String accessLevel = (String) additionalData.get("accessLevel");
        boolean levelRep = accessLevel.equals(DebisysConstants.REP);
        return showSubAgentCommission(additionalData) || levelRep;
    }

    private boolean showTaxRepCommission(Map<String, Object> ad){
        return showRepCommission(ad) && (showMxTaxCol(ad) || showInterTaxCol(ad));
    }

    private boolean showTaxMerchantCommission(Map<String, Object> ad){
        return showMxTaxCol(ad) || showInterTaxCol(ad);
    }
}
