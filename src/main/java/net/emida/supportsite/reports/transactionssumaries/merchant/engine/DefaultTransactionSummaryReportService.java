package net.emida.supportsite.reports.transactionssumaries.merchant.engine;

import net.emida.supportsite.mvc.taxes.TaxService;
import net.emida.supportsite.mvc.timezones.TimeZoneDao;
import net.emida.supportsite.mvc.timezones.TimeZoneDates;
import net.emida.supportsite.reports.transactionssumaries.merchant.*;
import net.emida.supportsite.util.exceptions.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Franky Villadiego
 */

@Service
public class DefaultTransactionSummaryReportService implements TransactionSummaryReportService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultTransactionSummaryReportService.class);


    private TimeZoneDao timeZoneDao;
    private TaxService taxService;
    private TransactionSummaryReportDao reportDao;


    @Override
    public DaoReportData buildReportData(ServiceReportParam param) {
        LOG.info("Building report data for transaction summary report. From :{} to :{}", param.getStartDate(), param.getEndDate());

        boolean validDates = isValidDates(param.getStartDate(), param.getEndDate());

        if(!validDates){
            throw new ApplicationException("Invalid dates");
        }


        DaoReportData rd = new DaoReportData();
        rd.setSortFieldName(param.getSortFieldName());
        rd.setSortOrder(param.getSortOrder());
        rd.setStartPage(param.getStartPage());
        rd.setEndPage(param.getEndPage());

        rd.setRefId(param.getRefId());
        rd.setAccessLevel(param.getAccessLevel());
        rd.setDistChainType(param.getDistChainType());


        boolean internationalTaxEnabled = param.isInternationalDeployment() && param.isPermEnableTaxCalc();
        boolean mexicoTaxEnabled = param.isMexicoDeployment() && param.isPermMxReportWithTaxes();
        rd.setInternationalTaxEnabled(internationalTaxEnabled);
        rd.setMxTaxEnabled(mexicoTaxEnabled);

        boolean taxEnabled = internationalTaxEnabled || mexicoTaxEnabled;
        rd.setTaxEnabled(taxEnabled);
        //pre-calculation for tax if needed
        if(internationalTaxEnabled){
            String strTax = getStrTax(param.getRefId(), param.getAccessLevel(), param.getDistChainType(), param.getEndDate());
            rd.setStrTax(strTax);
        }else if(mexicoTaxEnabled){
            double mxTax = 1 + ( param.getMxValueAddedTax() / 100 );
            rd.setStrTax(String.valueOf(mxTax));
        }

        rd.setValidDates(validDates);
        if(validDates){
            int accessLevel = Integer.parseInt(param.getAccessLevel());
            TimeZoneDates tzd = timeZoneDao.getTimeZoneFilterDates(accessLevel, new BigDecimal(param.getRefId()),
                    param.getStartDate(), param.getEndDate(), false);
            rd.setTimeZonedDates(tzd);
        }

        rd.setMerchantIds(param.getMerchantIds());
        rd.setRepIds(param.getRepIds());
        rd.setProductIds(param.getProductIds());
        rd.setSaleIds(param.getSaleIds());

        return rd;
    }



    @Override
    public TransactionSummaryReportResult buildReport(ServiceReportParam param) {

        DaoReportData reportData = buildReportData(param);

        TransactionSummaryReportResult result = reportDao.getResult(reportData);

        return result;
    }

    private String getStrTax(long refId, String strAccessLevel, String strDistChainType, Date endDate) {
        int accessLevel = Integer.parseInt(strAccessLevel);
        int distChainType = Integer.valueOf(strDistChainType);
        BigDecimal tax = taxService.getTax(refId, accessLevel, distChainType, endDate);
        return tax.toString();
    }

    private boolean isValidDates(Date startDate, Date endDate) {
        if(startDate == null || endDate == null) return false;
        return startDate.compareTo(endDate) != 1;
    }



    @Autowired
    public void setTaxService(TaxService taxService) {
        this.taxService = taxService;
    }

    @Autowired
    public void setTimeZoneDao(TimeZoneDao timeZoneDao) {
        this.timeZoneDao = timeZoneDao;
    }

    @Autowired
    public void setReportDao(TransactionSummaryReportDao reportDao) {
        this.reportDao = reportDao;
    }
}
