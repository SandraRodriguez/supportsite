package net.emida.supportsite.reports.transactionssumaries.merchant.engine;

/**
 * @author Franky Villadiego
 */

public class MerchantSummaryTotals {


    private double qty = 0d;
    private double totalSales = 0d;
    private double adjAmount = 0d;
    private double totalBonus = 0d;
    private double totalRecharge = 0d;

    private double merchantCommission = 0d;
    private double repCommission = 0d;
    private double subagentCommission = 0d;
    private double agentCommission = 0d;
    private double isoCommission = 0d;



    //With TAX
    private double taxTotalSales = 0d;
    private double taxMerchantCommission = 0d;
    private double taxRepCommission = 0d;
    private double taxSubagentCommission = 0d;
    private double taxAgentCommission = 0d;
    private double taxIsoCommission = 0d;
    private double totalTax = 0d;


    public double getQty() {
        return qty;
    }

    public double getTotalSales() {
        return totalSales;
    }

    public double getMerchantCommission() {
        return merchantCommission;
    }

    public double getRepCommission() {
        return repCommission;
    }

    public double getSubagentCommission() {
        return subagentCommission;
    }

    public double getAgentCommission() {
        return agentCommission;
    }

    public double getIsoCommission() {
        return isoCommission;
    }

    public double getAdjAmount() {
        return adjAmount;
    }

    public double getTaxTotalSales() {
        return taxTotalSales;
    }

    public double getTaxMerchantCommission() {
        return taxMerchantCommission;
    }

    public double getTaxRepCommission() {
        return taxRepCommission;
    }

    public double getTaxSubagentCommission() {
        return taxSubagentCommission;
    }

    public double getTaxAgentCommission() {
        return taxAgentCommission;
    }

    public double getTaxIsoCommission() {
        return taxIsoCommission;
    }

    public double getTotalBonus() {
        return totalBonus;
    }

    public double getTotalRecharge() {
        return totalRecharge;
    }

    public double getTotalTax() {
        return totalTax;
    }



    ////////


    public void addQty(double qty) {
        this.qty += qty;
    }

    public void addTotalSales(double totalSales) {
        this.totalSales += totalSales;
    }

    public void addMerchantCommission(double merchantCommission) {
        this.merchantCommission += merchantCommission;
    }

    public void addRepCommission(double repCommission) {
        this.repCommission += repCommission;
    }

    public void addSubagentCommission(double subagentCommission) {
        this.subagentCommission += subagentCommission;
    }

    public void addAgentCommission(double agentCommission) {
        this.agentCommission += agentCommission;
    }

    public void addIsoCommission(double isoCommission) {
        this.isoCommission += isoCommission;
    }

    public void addAdjAmount(double adjAmount) {
        this.adjAmount += adjAmount;
    }

    public void addTaxTotalSales(double taxTotalSales) {
        this.taxTotalSales += taxTotalSales;
    }

    public void addTaxMerchantCommission(double taxMerchantCommission) {
        this.taxMerchantCommission += taxMerchantCommission;
    }

    public void addTaxRepCommission(double taxRepCommission) {
        this.taxRepCommission += taxRepCommission;
    }

    public void addTaxSubagentCommission(double taxSubagentCommission) {
        this.taxSubagentCommission += taxSubagentCommission;
    }

    public void addTaxAgentCommission(double taxAgentCommission) {
        this.taxAgentCommission += taxAgentCommission;
    }

    public void addTaxIsoCommission(double taxIsoCommission) {
        this.taxIsoCommission += taxIsoCommission;
    }

    public void addTotalBonus(double totalBonus) {
        this.totalBonus += totalBonus;
    }

    public void addTotalRecharge(double totalRecharge) {
        this.totalRecharge += totalRecharge;
    }

    public void addTotalTax(double totalTax) {
        this.totalTax += totalTax;
    }
}
