package net.emida.supportsite.reports.transactionssumaries.merchant.engine;

import net.emida.supportsite.reports.transactionssumaries.merchant.TransactionSummaryReportModel;
import net.emida.supportsite.util.RSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Franky Villadiego
 */
public class TransactionSummaryModelExtractor implements ResultSetExtractor<List<TransactionSummaryReportModel>> {


    private static final Logger log = LoggerFactory.getLogger(TransactionSummaryModelExtractor.class);

    private boolean taxEnabled;

    public TransactionSummaryModelExtractor(boolean taxEnabled) {
        this.taxEnabled = taxEnabled;
    }

    @Override
    public List<TransactionSummaryReportModel> extractData(ResultSet rs) throws SQLException, DataAccessException {


        Map<Long, TransactionSummaryReportModel> tmpChache = new HashMap();
        int rowNum = 1;
        while (rs.next()){
            Long merchantId = rs.getLong("merchant_id");
            TransactionSummaryReportModel model = tmpChache.get(merchantId);
            if(model == null){
                model = new TransactionSummaryReportModel(merchantId);
                tmpChache.put(merchantId, model);
                model.setRowNum((long)rowNum);
                rowNum++;
            }

            model.setSalesmanId(RSUtil.getString(rs, "salesman_id"));
            model.setSalesmanName(RSUtil.getString(rs, "salesmanname"));
            model.setAccountNo(RSUtil.getString(rs, "account_no"));
            model.setDba(RSUtil.getString(rs, "dba"));
            model.setAdditionalData(RSUtil.getString(rs, "additionalData"));
            model.setRouteId(RSUtil.getInteger(rs, "routeId"));
            model.setRouteName(RSUtil.getString(rs, "routeName"));
            model.setInvoiceType(RSUtil.getString(rs, "invoice_type"));
            model.setPhysAddress(RSUtil.getString(rs, "phys_address"));
            model.setPhysCity(RSUtil.getString(rs, "phys_city"));
            model.setPhysState(RSUtil.getString(rs, "phys_state"));
            model.setPhysZip(RSUtil.getString(rs, "phys_zip"));

            model.setAvailableCredit(RSUtil.getDouble(rs, "available_credit", 0.0));
            model.setAdjAmount(RSUtil.getDouble(rs, "adj_amount", 0.0));
            model.setTotalBonus(RSUtil.getBigDecimal(rs, "total_bonus", 0.0));
            model.setRunningLiability(RSUtil.getBigDecimal(rs, "runningLiability", 0.0));
            model.setLiabilityLimit(RSUtil.getBigDecimal(rs, "liabilityLimit", 0.0));


            model.addQty(RSUtil.getDouble(rs, "qty", 0.0));
            model.addTotalSales(RSUtil.getDouble(rs, "total_sales", 0.0));
            model.addTotalRecharge(RSUtil.getBigDecimal(rs, "total_recharge", 0.0));

            model.addMerchantCommission(RSUtil.getDouble(rs, "merchant_commission", 0.0));
            model.addRepCommission(RSUtil.getDouble(rs, "rep_commission", 0.0));
            model.addSubAgentCommission(RSUtil.getDouble(rs, "subagent_commission", 0.0));
            model.addAgentCommission(RSUtil.getDouble(rs, "agent_commission", 0.0));
            model.addIsoCommission(RSUtil.getDouble(rs, "iso_commission", 0.0));


            if(taxEnabled) {
                model.addTaxTotalSales(RSUtil.getDouble(rs, "tax_total_sales", 0.0));
                model.addTaxMerchantCommission(RSUtil.getDouble(rs, "tax_merchant_commission", 0.0));
                model.addTaxRepCommission(RSUtil.getDouble(rs, "tax_rep_commission", 0.0));
                model.addTaxSubAgentCommission(RSUtil.getDouble(rs, "tax_subagent_commission", 0.0));
                model.addTaxAgentCommission(RSUtil.getDouble(rs, "tax_agent_commission", 0.0));
                model.addTaxIsoCommission(RSUtil.getDouble(rs, "tax_iso_commission", 0.0));
                model.addTaxAmount(RSUtil.getDouble(rs, "tax_amount", 0.0));
            }else{
                //model.setAvailableCredit2();        // ????
            }

        }

        List<TransactionSummaryReportModel> list = new ArrayList();
        TransactionSummaryReportModel m = new TransactionSummaryReportModel();
        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        for(int idx = 1; idx <= columnCount; idx++){
            String columnName = metaData.getColumnName(idx);
            String columnTypeName = metaData.getColumnTypeName(idx);
            String columnClassName = metaData.getColumnClassName(idx);
            log.debug(" ##### Col :{}. Name:{}. Type:{}. Class:{}", idx, columnName, columnTypeName, columnClassName);
        }

        Collection<TransactionSummaryReportModel> values = tmpChache.values();
        list.addAll(values);

        return list;
    }


/*    public TransactionSummaryReportModel mapRow(ResultSet rs, int rowNum) throws SQLException {

        TransactionSummaryReportModel m = new TransactionSummaryReportModel();



        return m;
    }*/





}
