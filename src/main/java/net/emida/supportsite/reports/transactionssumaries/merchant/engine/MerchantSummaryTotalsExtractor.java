package net.emida.supportsite.reports.transactionssumaries.merchant.engine;

import net.emida.supportsite.util.RSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * @author Franky Villadiego
 */
public class MerchantSummaryTotalsExtractor implements ResultSetExtractor<MerchantSummaryTotals> /*, RowMapper<MerchantSummaryTotals>*/ {


    private static final Logger log = LoggerFactory.getLogger(MerchantSummaryTotalsExtractor.class);

    //private MerchantSummaryTotals totals;
    private boolean taxEnabled;

    public MerchantSummaryTotalsExtractor(boolean taxEnabled) {
        this.taxEnabled = taxEnabled;
    }

/*    @Override
    public MerchantSummaryTotals mapRow(ResultSet rs, int rowNum) throws SQLException {

        totals.addQty(Double.parseDouble(rs.getString("qty")));
        totals.addTotalSales(Double.parseDouble(rs.getString("total_sales")));
        totals.addMerchantCommission(Double.parseDouble(rs.getString("merchant_commission")));
        totals.addRepCommission(Double.parseDouble(rs.getString("rep_commission")));
        totals.addSubagentCommission(Double.parseDouble(rs.getString("subagent_commission")));
        totals.addAgentCommission(Double.parseDouble(rs.getString("agent_commission")));
        totals.addIsoCommission(Double.parseDouble(rs.getString("iso_commission")));
        totals.addAdjAmount(Double.parseDouble(rs.getString("adj_amount")));
        totals.addTotalBonus(Double.parseDouble(rs.getString("total_bonus")));
        totals.addTotalRecharge(Double.parseDouble(rs.getString("total_recharge")));



        //With TAX
        if(taxEnabled) {
            totals.addTaxTotalSales(Double.parseDouble(rs.getString("tax_total_sales")));
            totals.addTaxMerchantCommission(Double.parseDouble(rs.getString("tax_merchant_commission")));
            totals.addTaxRepCommission(Double.parseDouble(rs.getString("tax_rep_commission")));
            totals.addTaxSubagentCommission(Double.parseDouble(rs.getString("tax_subagent_commission")));
            totals.addTaxAgentCommission(Double.parseDouble(rs.getString("tax_agent_commission")));
            totals.addTaxIsoCommission(Double.parseDouble(rs.getString("tax_iso_commission")));
            totals.addTotalTax(Double.parseDouble(rs.getString("tax_amount")));
        }
        return null;
    }*/

    @Override
    public MerchantSummaryTotals extractData(ResultSet rs) throws SQLException, DataAccessException {

        MerchantSummaryTotals model = new MerchantSummaryTotals();

        while (rs.next()){
            model.addQty(RSUtil.getDouble(rs, "qty", 0.0));
            model.addTotalSales(RSUtil.getDouble(rs, "total_sales", 0.0));

            model.addMerchantCommission(RSUtil.getDouble(rs, "merchant_commission", 0.0));
            model.addRepCommission(RSUtil.getDouble(rs, "rep_commission", 0.0));
            model.addSubagentCommission(RSUtil.getDouble(rs, "subagent_commission", 0.0));
            model.addAgentCommission(RSUtil.getDouble(rs, "agent_commission", 0.0));
            model.addIsoCommission(RSUtil.getDouble(rs, "iso_commission", 0.0));

            model.addAdjAmount(RSUtil.getDouble(rs, "adj_amount", 0.0));
            model.addTotalBonus(RSUtil.getDouble(rs, "total_bonus", 0.0));
            model.addTotalRecharge(RSUtil.getDouble(rs, "total_recharge", 0.0));


            //With TAX
            if(taxEnabled) {
                model.addTaxTotalSales(RSUtil.getDouble(rs, "tax_total_sales", 0.0));
                model.addTaxMerchantCommission(RSUtil.getDouble(rs, "tax_merchant_commission", 0.0));
                model.addTaxRepCommission(RSUtil.getDouble(rs, "tax_rep_commission", 0.0));
                model.addTaxSubagentCommission(RSUtil.getDouble(rs, "tax_subagent_commission", 0.0));
                model.addTaxAgentCommission(RSUtil.getDouble(rs, "tax_agent_commission", 0.0));
                model.addTaxIsoCommission(RSUtil.getDouble(rs, "tax_iso_commission", 0.0));
                model.addTotalTax(RSUtil.getDouble(rs, "tax_amount", 0.0));
            }

        }


        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        for(int idx = 1; idx <= columnCount; idx++){
            String columnName = metaData.getColumnName(idx);
            String columnTypeName = metaData.getColumnTypeName(idx);
            String columnClassName = metaData.getColumnClassName(idx);
            log.info(" ##### Col :{}. Name:{}. Type:{}. Class:{}", idx, columnName, columnTypeName, columnClassName);
        }


        return model;
    }
}
