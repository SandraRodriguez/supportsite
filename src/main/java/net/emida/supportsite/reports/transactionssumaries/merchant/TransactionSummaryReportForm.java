package net.emida.supportsite.reports.transactionssumaries.merchant;

import com.debisys.utils.StringUtil;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.Date;

/**
 * @author Franky Villadiego
 */
public class TransactionSummaryReportForm {

    private Date startDate;
    private Date endDate;


    private boolean allMerchants;
    private String[] merchantIds;   //Not for AccessLevel MERCHANT

    private String[] salesRepIds;        //For AccessLevel CARRIER

    private String[] ratePlansIds;

    private boolean useTaxValue;


    @NotNull(message = "{com.debisys.reports.error1}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }



    @NotNull(message = "{com.debisys.reports.error3}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }



    public String[] getMerchantIds() {
        return merchantIds;
    }

    public void setMerchantIds(String[] merchantIds) {
        this.merchantIds = merchantIds;
    }


    public boolean isAllMerchants() {
        return allMerchants;
    }

    public void setAllMerchants(boolean allMerchants) {
        this.allMerchants = allMerchants;
    }

    public String[] getSalesRepIds() {
        return salesRepIds;
    }

    public void setSalesRepIds(String[] salesRepIds) {
        this.salesRepIds = salesRepIds;
    }


    public String[] getRatePlansIds() {
        return ratePlansIds;
    }

    public void setRatePlansIds(String[] ratePlansIds) {
        this.ratePlansIds = ratePlansIds;
    }

    public boolean isUseTaxValue() {
        return useTaxValue;
    }

    public void setUseTaxValue(boolean useTaxValue) {
        this.useTaxValue = useTaxValue;
    }

    public String toCsvMerchantIds(){
        if(this.getMerchantIds() != null && this.getMerchantIds().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getMerchantIds()) {
                if(StringUtil.isNotEmptyStr(val)) {
                    sb1.append(val).append(",");
                }
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

    public String toCsvSaleRepsIds(){
        if(this.getSalesRepIds() != null && this.getSalesRepIds().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getSalesRepIds()) {
                if(StringUtil.isNotEmptyStr(val)) {
                    sb1.append(val).append(",");
                }
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

    public String toCsvRatePlanIds(){
        if(this.getRatePlansIds() != null && this.getRatePlansIds().length > 0) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : this.getRatePlansIds()) {
                if(StringUtil.isNotEmptyStr(val)) {
                    sb1.append(val).append(",");
                }
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        }else{
            return "";
        }
    }

    @Override
    public String toString() {
        return "TransactionSummaryReportForm[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", merchantIds=" + Arrays.toString(merchantIds) +
                ']';
    }
}
