package net.emida.supportsite.reports.transactionssumaries.merchant;


import net.emida.supportsite.reports.transactionssumaries.merchant.engine.MerchantSummaryTotals;

import java.util.List;

/**
 * @author Franky Villadiego
 */

public class TransactionSummaryReportResult {


    private MerchantSummaryTotals totals;
    private Long totalRecords;
    private List<TransactionSummaryReportModel> body;


    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<TransactionSummaryReportModel> getBody() {
        return body;
    }

    public void setBody(List<TransactionSummaryReportModel> body) {
        this.body = body;
    }

    public MerchantSummaryTotals getTotals() {
        return totals;
    }

    public void setTotals(MerchantSummaryTotals totals) {
        this.totals = totals;
    }
}
