package net.emida.supportsite.reports.transactionssumaries.merchant;

import com.debisys.customers.Merchant;
import com.debisys.reports.ReportsUtil;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.mvc.rateplans.MerchantRatePlanPojo;
import net.emida.supportsite.mvc.rateplans.RatePlanDao;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Franky Villadiego
 */

@Controller
@RequestMapping(value = UrlPaths.ADMIN_PATH + UrlPaths.REPORT_PATH)
public class TransactionSummaryReportController extends BaseController{

    private static final Logger log = LoggerFactory.getLogger(TransactionSummaryReportController.class);

    private static final String TRX_PATH            = "/transactions/merchants";
    private static final String TRX_GRID_PATH       = TRX_PATH +  "/grid";
    private static final String TRX_DOWNLOAD_PATH           = TRX_PATH +  "/download";

    private static final String TRX_SEARCH_MERCHANTS_PATH   = "/transactions/merchants/search";
    private static final String TRX_SEARCH_RATE_PLANS_PATH   = "/transactions/merchants/rate-plans";
    private static final String TRX_LIST_RATE_PLANS_PATH   = "/transactions/merchants/rate-plan-list";

    //Pages
    private static final String REPORT_FORM_PAGE = "reports/transactionssummaries/transactionSummaryReportForm";
    private static final String REPORT_RESULT_PAGE = "reports/transactionssummaries/transactionSummaryReportResult";

    private static final String MX_DATE_LBL = "jsp.admin.select_date_range";
    private static final String NOT_MX_DATE_LBL = "jsp.admin.select_date_range_international";

    @Autowired
    private TransactionSummaryReportDao transactionSummaryReportDao;

    @Autowired
    private RatePlanDao ratePlanDao;

    @Autowired
    private TransactionSummaryReportService reportService;

    @Autowired @Qualifier("merchantTransactionSummaryReportCsvBuilder")
    private CsvBuilderService csvBuilderService;


    @RequestMapping(value = TRX_PATH, method = RequestMethod.GET)
    public String showForm(Map model, @ModelAttribute("reportModel") TransactionSummaryReportForm reportModel){

        fillInitialFormValues(model);

        return REPORT_FORM_PAGE;
    }

    private void fillInitialFormValues(Map model) {
        model.put("timeZoneLabel", buildTimeZoneLabel());
        model.put("dateLabel", isMexico() ? getString(MX_DATE_LBL) : getString(NOT_MX_DATE_LBL));

        Vector<Vector<String>> merchants = null;
        model.put("showMerchants", showMerchants());
        if(showMerchants()) {
            //fillMerchants(model, null, null);
            merchants = searchMerchants();
            model.put("merchantList", merchants);
        }

        model.put("showSalesRep", showSalesRep());
        if(showSalesRep()){
            fillSalesRep(model);
        }

        model.put("showS2KRatePlans", showS2KRatePlans());
        if(showMerchants() && showS2KRatePlans()){
            List<MerchantRatePlanPojo> ratePlans = ratePlanDao.getNewRatePlansForAllMerchants(Long.parseLong(getRefId()), null);
            model.put("ratePlanList", ratePlans);
        }

        model.put("useTaxValue", chkUseTaxValue());
        model.put("taxesInt", allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS));
        model.put("taxesMex", allowPermission(DebisysConstants.GENERATE_REPORT_WITH_TAXES_MX));
        model.put("isIntlAndHasDataPromoPermission", ReportsUtil.isIntlAndHasDataPromoPermission(getSessionData(), servletContext));

    }


    @RequestMapping(value = TRX_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @ModelAttribute("reportModel") @Valid TransactionSummaryReportForm reportModel, BindingResult result,
                              @RequestParam MultiValueMap<String,String> params) {

        log.debug("transactions report building {}", result);

        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());


        if(result.hasErrors()){
            fillInitialFormValues(model.asMap());
            return REPORT_FORM_PAGE;
        }

        model.addAttribute("timeZoneLabel", buildTimeZoneLabel());


        String csvMerchantIds = reportModel.toCsvMerchantIds();
        model.addAttribute("merchantIds", StringUtil.isNotEmptyStr(csvMerchantIds) ? csvMerchantIds : "");

        String csvSaleRepsIds = reportModel.toCsvSaleRepsIds();
        model.addAttribute("saleRepsIds", StringUtil.isNotEmptyStr(csvSaleRepsIds) ? csvSaleRepsIds : "");

        String csvRatePlanIds = reportModel.toCsvRatePlanIds();
        model.addAttribute("ratePlansIds", StringUtil.isNotEmptyStr(csvRatePlanIds) ? csvRatePlanIds : "");


        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String formattedStarDate = sdf.format(reportModel.getStartDate());
        String formattedEndDate = sdf.format(reportModel.getEndDate());

        model.addAttribute("startDate", formattedStarDate);
        model.addAttribute("endDate", formattedEndDate);

        String encodedStartDate = encodeDate(formattedStarDate);
        String encodedEndDate = encodeDate(formattedEndDate);
        model.addAttribute("encodedStartDate", encodedStartDate);
        model.addAttribute("encodedEndDate", encodedEndDate);

        boolean s2000Tools = allowPermission(DebisysConstants.SYSTEM_2000_TOOLS);
        model.addAttribute("s2000Tools", s2000Tools);

        //boolean permTaxCalc = allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
        //model.addAttribute("showTaxCalc", permTaxCalc);

        model.addAttribute("useTaxValue", reportModel.isUseTaxValue());
        model.addAttribute("taxesInt", allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS));
        model.addAttribute("taxesMex", allowPermission(DebisysConstants.GENERATE_REPORT_WITH_TAXES_MX));
        model.addAttribute("isIntlAndHasDataPromoPermission", ReportsUtil.isIntlAndHasDataPromoPermission(getSessionData(), servletContext));

        return REPORT_RESULT_PAGE;
    }

    private String encodeDate(String formattedDate) {
        try {
            return URLEncoder.encode(formattedDate, "UTF8");
        }catch (Exception ex){

        }
        return "";
    }

    private ServiceReportParam buildParams(boolean domestic, boolean international, boolean mexico, Long refId, String accLevel, String chain,
                                           Date startDate, Date endDate, List<Long> mIds, List<Long> saleIds)  {
        ServiceReportParam serviceParam = new ServiceReportParam();

        //Deployment Info
        serviceParam.setDomesticDeployment(domestic);
        serviceParam.setInternationalDeployment(international);
        serviceParam.setMexicoDeployment(mexico);

        //Session info
        serviceParam.setRefId(refId);
        serviceParam.setAccessLevel(accLevel);
        serviceParam.setDistChainType(chain);

        //Report conditions
        serviceParam.setStartDate(DateUtil.atStartOfDay(startDate));
        serviceParam.setEndDate(DateUtil.atEndOfDay(endDate));


        serviceParam.setMerchantIds(mIds);
        serviceParam.setSaleIds(saleIds);


        boolean permTaxCalc = allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
        serviceParam.setPermEnableTaxCalc(international && permTaxCalc);

        boolean permMxReportWithTaxes = allowPermission(DebisysConstants.GENERATE_REPORT_WITH_TAXES_MX);
        serviceParam.setPermMxReportWithTaxes(permMxReportWithTaxes);
        serviceParam.setMxValueAddedTax(getProperty("mxValueAddedTax", 0.0));

        return serviceParam;
    }


    @RequestMapping(method = RequestMethod.GET, value = TRX_GRID_PATH, produces="application/json")
    @ResponseBody
    public TransactionSummaryReportResult listGrid(@RequestParam(value = "start", required = false) String page, @RequestParam(required = false) String sortField,
                                                  @RequestParam(required = false) String sortOrder,
                                                  @RequestParam(required = false) String rows, @RequestParam String startDate, @RequestParam String endDate,
                                                  @RequestParam(required = false) String merchantIds, @RequestParam(required = false) String saleRepsIds){

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. StartDate={}. EndDate={}. TrxId={}. InvoiceId={}. pinNumber={}. Mids={}. IsoIds={}", startDate, endDate, merchantIds);

        //int rowsPerPage = Integer.parseInt(rows);
        //int startPage = Integer.parseInt(page);
        //int endPage = rowsPerPage + startPage - 1;


        List<Long> mIdList = buildMerchantIds(merchantIds);
        List<Long> saleRepsIdList = buildSaleRepsIds(saleRepsIds);

        Date theStartDate = DateUtil.parse(startDate, "MM/dd/yyyy");
        Date theEndDate = DateUtil.parse(endDate, "MM/dd/yyyy");

        ServiceReportParam serviceParam = buildParams(isDomestic(), isInternational(), isMexico(), Long.parseLong(getRefId()),
                getAccessLevel(), getDistChainType(), theStartDate, theEndDate, mIdList, saleRepsIdList);

        TransactionSummaryReportResult reportResult = reportService.buildReport(serviceParam);

        return reportResult;
    }



    @RequestMapping(method = RequestMethod.GET, value = TRX_SEARCH_MERCHANTS_PATH, produces="application/json")
    @ResponseBody
    public List<Map<String, String>> searchMerchant(@RequestParam(value = "query") String searchValue){

        log.debug("Searching merchants by value ={}", searchValue);

        Vector<Vector<String>> merchants = searchMerchants();
        int maxRows = 15;
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        String selectAllLabel = getString("jsp.tools.searchmerchant.selectAll") + " " + searchValue;
        String selectAllValue = "*" + searchValue;
        Map<String, String> selectAllSearchValueRow = buildRow(selectAllLabel, selectAllValue);
        result.add(selectAllSearchValueRow);
        List<Map<String, String>> filtered = filter(merchants, searchValue, maxRows, result);
        //If no occurrences were found, we cleaning the manual entry in list
        if(filtered.size() == 1) filtered.clear();
        return filtered;
    }

    @RequestMapping(method = RequestMethod.POST, value = TRX_SEARCH_RATE_PLANS_PATH, produces="text/plain")
    @ResponseBody
    public String searchRatePlans(@RequestParam String mIds){
        log.debug("Searching ratre plans by merchant ids={}", mIds);
        List<Long> mIdList = buildMerchantIds(mIds);
        mIds = StringUtil.numberListToCsv(mIdList);
         /*if(StringUtil.isNotEmptyStr(mIds)){
            if(mIds.indexOf("*") == 0){
                List<Long> ids = buildMerchantIds(mIds);
                mIds = StringUtil.numberListToCsv(ids);
            }
         }*/

        return StringUtil.isNotEmptyStr(mIds) ?  ratePlansForMerchants(mIds) : "";
    }


    @RequestMapping(method = RequestMethod.GET, value = TRX_LIST_RATE_PLANS_PATH, produces="application/json")
    @ResponseBody
    public List<MerchantRatePlanPojo> listRatePlansForAllMerchants(){
        log.debug("Listing rate plans for all merchants..");
        if(showMerchants() && showS2KRatePlans()){
            return ratePlanDao.getNewRatePlansForAllMerchants(Long.parseLong(getRefId()), null);
        }
        return new ArrayList();
    }


    @RequestMapping(method = RequestMethod.GET, value = TRX_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String startDate, @RequestParam String endDate,
                                           @RequestParam(required = false) String merchantIds, @RequestParam(required = false) String saleRepsIds,
                                           @RequestParam Integer totalRecords, @RequestParam String sortField,
                                           @RequestParam String sortOrder) throws IOException {

        log.info("Downloading file...");
        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file. StartDate={}. EndDate={}. Mids={}. SaleRepsIds={}", startDate, endDate, merchantIds, saleRepsIds);


        List<Long> mIdList = buildMerchantIds(merchantIds);
        List<Long> saleRepsIdList = buildSaleRepsIds(saleRepsIds);

        Date theStartDate = DateUtil.parse(startDate, "MM/dd/yyyy");
        Date theEndDate = DateUtil.parse(endDate, "MM/dd/yyyy");


        ServiceReportParam serviceParam = buildParams(isDomestic(), isInternational(), isMexico(), Long.parseLong(getRefId()),
                getAccessLevel(), getDistChainType(), theStartDate, theEndDate, mIdList, saleRepsIdList);

        TransactionSummaryReportResult reportResult = reportService.buildReport(serviceParam);


        List<TransactionSummaryReportModel> list = reportResult.getBody();
        log.debug("Total list={}", list.size());

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.info("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());
        additionalData.put("international", isInternational());
        additionalData.put("domestic", isDomestic());
        additionalData.put("mexico", isMexico());


        additionalData.put("s2000Tools", allowPermission(DebisysConstants.SYSTEM_2000_TOOLS));
        additionalData.put("taxesMex", allowPermission(DebisysConstants.GENERATE_REPORT_WITH_TAXES_MX));
        additionalData.put("taxesInt", allowPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS));

        additionalData.put("totals", reportResult.getTotals());

        additionalData.put("timeZoneLabel", buildTimeZoneLabel());
        additionalData.put("startDate", startDate);
        additionalData.put("endDate", endDate);

        csvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.info("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();

        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

        return zipFileResource;

    }




    private boolean chkUseTaxValue() {
        boolean reportWithTaxes = allowPermission(DebisysConstants.GENERATE_REPORT_WITH_TAXES_MX);
        return isMexico() && reportWithTaxes;
    }

    private List<Map<String, String>> filter(Vector<Vector<String>> merchants, String searchValue, int maxRows, List<Map<String, String>> result) {
        int count = 0;
        if(merchants != null && !merchants.isEmpty()){
            for (Vector<String> m : merchants) {
                boolean contains = StringUtils.containsIgnoreCase(m.get(1), searchValue);
                if(contains){
                    Map<String, String> row = buildRow(m.get(1), m.get(0).toString());
                    result.add(row);
                    if(++count > maxRows){
                        break;
                    }
                }
            }
        }
        return result;
    }

    private Map<String, String> buildRow(String label, String value) {
        Map<String, String> row = new HashMap<String, String>();
        row.put("label", label);
        row.put("value", value);
        return row;
    }


    private String buildTimeZoneLabel() {
        String reportNote = super.getString("jsp.admin.timezone.reportNote");
        try {
            long refId = Long.parseLong(super.getRefId());
            Vector<String> timeZoneByRep = TimeZone.getTimeZoneByRep(refId);
            String gmtTimeZoneText = timeZoneByRep.get(1);
            String gmtTimeZoneName = timeZoneByRep.get(2);

            return reportNote + ": " + gmtTimeZoneText + "[ " + gmtTimeZoneName + " ]";
        }catch (Exception ex){
            log.error("Error getting time zone...: {}", ex);
        }
        return reportNote + ": Error.";
    }

    private boolean showMerchants() {
        return !isLevelMerchant();
    }

    private String buildIds(String csvIds, Set<Long> idList) {
        String searchValue = null;
        if(StringUtil.isNotEmptyStr(csvIds)){
            String[] split = csvIds.split(",");
            for (String id : split) {
                if(id.indexOf("*") == 0){
                    searchValue = id.substring(1, id.length());
                    continue;
                }
                if(StringUtils.isNumeric(id)) {
                    idList.add(Long.valueOf(id));
                }
            }
        }
        return searchValue;
    }

    private List<Long> buildMerchantIds(String csvIds){
        Set<Long> idList = new HashSet<Long>();
        String searchValue = buildIds(csvIds, idList);
        searchAdditionalMerchants(searchValue, idList);
        List<Long> mIds = new ArrayList<Long>(idList);
        return mIds;
    }

    private List<Long> buildSaleRepsIds(String csvIds){
        List<Long> idList = new ArrayList();
        if(StringUtil.isNotEmptyStr(csvIds)){
            String[] split = csvIds.split(",");
            for (String id : split) {
                idList.add(Long.valueOf(id));
            }
        }
        return idList;
    }

    private String buildMerchantIds(Vector<Vector<String>> m){
        StringBuilder sb = new StringBuilder();
        if(m != null && !m.isEmpty()){
            Iterator<Vector<String>> it = m.iterator();
            while (it.hasNext()){
                Vector<String> row = it.next();
                sb.append(row.get(0));
                if(it.hasNext()){
                    sb.append(",");
                }
            }
        }
        return sb.toString();
    }



    private void searchAdditionalMerchants(String searchValue, Set<Long> idList) {
        if(StringUtil.isNotEmptyStr(searchValue)) {
            Vector<Vector<String>> merchants = searchMerchants();
            for (Vector<String> merchant : merchants) {
                if (merchant.get(1).toUpperCase().contains(searchValue.toUpperCase())) {
                    idList.add(Long.valueOf(merchant.get(0)));
                }
            }
        }
    }


    private void fillMerchants(Map model, String repName, Long repId){
        Vector<Vector<String>> merchants = searchMerchants();
        model.put("merchantList", merchants);
    }

    private Vector<Vector<String>> searchMerchants(){
        try {
            Vector<Vector<String>> merchantList = Merchant.getMerchantListReports(getSessionData());
            return merchantList;
        } catch (Exception e) {
            log.error("Error listing merchants. Ex = {}", e);
            throw new TechnicalException("Error listing merchants", e);
        }
    }


    private boolean showS2KRatePlans() {
        boolean showSalesRep = showSalesRep();
        boolean s2kRatePlans = allowPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS);
        return showSalesRep && s2kRatePlans;
    }

    private void fillSalesRep(Map model) {
        Vector<Vector> salesReps = searchSalesRep();
        model.put("salesRepList", salesReps);
    }

    private boolean showSalesRep() {
        boolean system2000ToolsEnabled = allowPermission(DebisysConstants.SYSTEM_2000_TOOLS);
        boolean levelIso = isLevelIso3() || isLevelIso5();
        return levelIso && system2000ToolsEnabled;
    }

    private Vector<Vector> searchSalesRep(){
        Vector result = Merchant.getMerchants2ksalesman(getRefId());
        return result;
    }


    private String ratePlansForMerchants(String mids){
        String ratePlansByMerchants = Merchant.getRatePlansByMerchants(mids);
        return ratePlansByMerchants;
    }


    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 1;
    }
}
