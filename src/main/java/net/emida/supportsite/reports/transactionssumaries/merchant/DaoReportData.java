package net.emida.supportsite.reports.transactionssumaries.merchant;


import net.emida.supportsite.mvc.timezones.TimeZoneDates;
import net.emida.supportsite.reports.transactions.TransactionsReportType;
import net.emida.supportsite.util.TimeZoneInfo;

import java.util.List;

/**
 * @author Franky Villadiego
 */

public class DaoReportData {

    //Pagination info
    private String sortFieldName;
    private String sortOrder;
    private int startPage;
    private int endPage;


    private long refId;
    private String accessLevel;
    private String distChainType;

    private boolean internationalTaxEnabled;
    private boolean mxTaxEnabled;
    private boolean taxEnabled;
    private String strTax;

    private List<Long> merchantIds;
    private List<Long> productIds;
    private List<Long> repIds;
    private List<Long> saleIds;

    private TimeZoneDates timeZonedDates;


    ///
    private Long entityId;
    private TransactionsReportType reportType;



    private String pin;
    private String invoiceNumber;
    private String transactionId;

    private List<Long> millenniumNum;
    private List<String> ratePlanIds;

    private boolean allMerchants;


    private boolean viewReferenceCard;
    private boolean permViewFilterS2KRatePlan;
    private boolean withTax;
    private boolean withAchDate;
    private boolean includeAdditionalData;
    private boolean viewPin;
    private boolean pinNotEmpty;
    private boolean showQrCodeTransactions;
    private boolean showCommissionDetails;

    private boolean validDates;

    private TimeZoneInfo defaultTimeZoneInfo;
    private TimeZoneInfo entityTimeZoneInfo;

    public String getSortFieldName() {
        return sortFieldName;
    }

    public void setSortFieldName(String sortFieldName) {
        this.sortFieldName = sortFieldName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public void setReportType(TransactionsReportType reportType) {
        this.reportType = reportType;
    }

    public TransactionsReportType getReportType() {
        return reportType;
    }

    public long getRefId() {
        return refId;
    }

    public void setRefId(long refId) {
        this.refId = refId;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getDistChainType() {
        return distChainType;
    }

    public void setDistChainType(String distChainType) {
        this.distChainType = distChainType;
    }

    public boolean isTaxEnabled() {
        return taxEnabled;
    }

    public void setTaxEnabled(boolean taxEnabled) {
        this.taxEnabled = taxEnabled;
    }

    public String getStrTax() {
        return strTax;
    }

    public void setStrTax(String strTax) {
        this.strTax = strTax;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }


    public boolean isAllMerchants() {
        return allMerchants;
    }

    public void setAllMerchants(boolean allMerchants) {
        this.allMerchants = allMerchants;
    }

    public List<Long> getMerchantIds() {
        return merchantIds;
    }

    public void setMerchantIds(List<Long> merchantIds) {
        this.merchantIds = merchantIds;
    }

    public List<Long> getMillenniumNum() {
        return millenniumNum;
    }

    public void setMillenniumNum(List<Long> millenniumNum) {
        this.millenniumNum = millenniumNum;
    }

    public List<String> getRatePlanIds() {
        return ratePlanIds;
    }

    public void setRatePlanIds(List<String> ratePlanIds) {
        this.ratePlanIds = ratePlanIds;
    }

    public List<Long> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Long> productIds) {
        this.productIds = productIds;
    }

    public boolean isViewReferenceCard() {
        return viewReferenceCard;
    }

    public void setViewReferenceCard(boolean viewReferenceCard) {
        this.viewReferenceCard = viewReferenceCard;
    }

    public boolean isWithTax() {
        return withTax;
    }

    public void setWithTax(boolean withTax) {
        this.withTax = withTax;
    }

    public boolean isWithAchDate() {
        return withAchDate;
    }

    public void setWithAchDate(boolean withAchDate) {
        this.withAchDate = withAchDate;
    }

    public boolean isIncludeAdditionalData() {
        return includeAdditionalData;
    }

    public void setIncludeAdditionalData(boolean includeAdditionalData) {
        this.includeAdditionalData = includeAdditionalData;
    }

    public boolean isViewPin() {
        return viewPin;
    }

    public void setViewPin(boolean viewPin) {
        this.viewPin = viewPin;
    }

    public boolean isPinNotEmpty() {
        return pinNotEmpty;
    }

    public void setPinNotEmpty(boolean pinNotEmpty) {
        this.pinNotEmpty = pinNotEmpty;
    }

    public boolean isValidDates() {
        return validDates;
    }

    public void setValidDates(boolean validDates) {
        this.validDates = validDates;
    }

    public TimeZoneDates getTimeZonedDates() {
        return timeZonedDates;
    }

    public void setTimeZonedDates(TimeZoneDates timeZonedDates) {
        this.timeZonedDates = timeZonedDates;
    }

    public List<Long> getRepIds() {
        return repIds;
    }

    public void setRepIds(List<Long> repIds) {
        this.repIds = repIds;
    }

    public boolean isPermViewFilterS2KRatePlan() {
        return permViewFilterS2KRatePlan;
    }

    public void setPermViewFilterS2KRatePlan(boolean permViewFilterS2KRatePlan) {
        this.permViewFilterS2KRatePlan = permViewFilterS2KRatePlan;
    }

    public boolean isShowQrCodeTransactions() {
        return showQrCodeTransactions;
    }

    public void setShowQrCodeTransactions(boolean showQrCodeTransactions) {
        this.showQrCodeTransactions = showQrCodeTransactions;
    }


    public boolean isShowCommissionDetails() {
        return showCommissionDetails;
    }

    public void setShowCommissionDetails(boolean showCommissionDetails) {
        this.showCommissionDetails = showCommissionDetails;
    }


    public TimeZoneInfo getDefaultTimeZoneInfo() {
        return defaultTimeZoneInfo;
    }

    public void setDefaultTimeZoneInfo(TimeZoneInfo defaultTimeZoneInfo) {
        this.defaultTimeZoneInfo = defaultTimeZoneInfo;
    }

    public TimeZoneInfo getEntityTimeZoneInfo() {
        return entityTimeZoneInfo;
    }

    public void setEntityTimeZoneInfo(TimeZoneInfo entityTimeZoneInfo) {
        this.entityTimeZoneInfo = entityTimeZoneInfo;
    }


    public List<Long> getSaleIds() {
        return saleIds;
    }

    public void setSaleIds(List<Long> saleIds) {
        this.saleIds = saleIds;
    }


    public boolean isInternationalTaxEnabled() {
        return internationalTaxEnabled;
    }

    public void setInternationalTaxEnabled(boolean internationalTaxEnabled) {
        this.internationalTaxEnabled = internationalTaxEnabled;
        this.taxEnabled = internationalTaxEnabled || mxTaxEnabled;
    }

    public boolean isMxTaxEnabled() {
        return mxTaxEnabled;
    }

    public void setMxTaxEnabled(boolean mxTaxEnabled) {
        this.mxTaxEnabled = mxTaxEnabled;
        this.taxEnabled = internationalTaxEnabled || mxTaxEnabled;
    }

    /**
     * @return the entityId
     */
    public Long getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }
}
