package net.emida.supportsite.reports.transactionssumaries.merchant.engine;

import com.debisys.reports.ReportsUtil;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;
import net.emida.supportsite.mvc.timezones.TimeZoneDates;

import net.emida.supportsite.reports.transactionssumaries.merchant.DaoReportData;
import net.emida.supportsite.reports.transactionssumaries.merchant.TransactionSummaryReportDao;
import net.emida.supportsite.reports.transactionssumaries.merchant.TransactionSummaryReportModel;
import net.emida.supportsite.reports.transactionssumaries.merchant.TransactionSummaryReportResult;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Franky Villadiego
 */

@Repository
public class TransactionSummaryReportJdbcDao implements TransactionSummaryReportDao {


    private static final Logger log = LoggerFactory.getLogger(TransactionSummaryReportJdbcDao.class);

    private static final String COUNT_TOKEN = "_COUNT_TOKEN_";
    private static final String ADDITIONAL_DATA = "additionalData";
    private static final String WT_ADDITIONAL_DATA = ", wt.additionalData";
    private static final String COMMA_ADDITIONAL_DATA = ", additionalData";

    @Autowired
    private HttpServletRequest httpServletRequest;
    
    @Autowired
    private ServletContext servletContext;

    @Autowired @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;


    @Override
    public long count(DaoReportData param) {
        try {
            //FROM
            //String fromQuery = buildFromQuery(param);

            //WHERE
            //String whereQuery = buildWhereQuery(param);

            //String countQuery = "SELECT COUNT(*)  " + fromQuery + " " + whereQuery;
            return jdbcTemplate.queryForObject("", Long.class);
        }catch (EmptyResultDataAccessException ex){
            return 0;
        }catch (Exception ex){
            log.error("Error in Count query. ex:{}", ex);
        }
        return 0;
    }

    public TransactionSummaryReportResult getResult(DaoReportData param) {

        try {
            log.info("Building transaction summary by merchant report for refId:{}. With dates :{}", param.getRefId(), param.getTimeZonedDates());

            MerchantSummaryTotals totals = fillTotalsValues(param);

            List<TransactionSummaryReportModel> list = fillValues(param);

            TransactionSummaryReportResult result = new TransactionSummaryReportResult();
            result.setBody(list);
            result.setTotals(totals);
            result.setTotalRecords(Long.valueOf(list.size()));

            return result;
        }catch (EmptyResultDataAccessException ex){
            TransactionSummaryReportResult result = new TransactionSummaryReportResult();
            result.setBody(new ArrayList<TransactionSummaryReportModel>());
            result.setTotalRecords(0L);
            return result;
        }catch (Exception ex){
            log.error("Error in main query for transactions. Ex:{}", ex);
        }
        TransactionSummaryReportResult result = new TransactionSummaryReportResult();
        result.setBody(new ArrayList<TransactionSummaryReportModel>());
        result.setTotalRecords(0L);
        return result;
    }

    private List<TransactionSummaryReportModel> fillValues(DaoReportData param) {

        try{
            log.debug("Building totals query...");
            StringBuilder queryMain = buildMainQuery(param);

            log.debug("MAIN QUERY :{}", queryMain);

            TransactionSummaryModelExtractor rsExtractor = new TransactionSummaryModelExtractor(param.isTaxEnabled());
            List<TransactionSummaryReportModel> result = jdbcTemplate.query(queryMain.toString(), rsExtractor);

            log.debug("Main ready...");
            return result;
        }catch (EmptyResultDataAccessException ex){
            log.warn("No merchant transaction summaries. Ex:{}", ex);
            return new ArrayList();
        }catch (Exception ex){
            log.error("Error in main query for transactions. Ex:{}", ex);
            throw new TechnicalException("DataAccess Error", ex);
        }
    }

    private MerchantSummaryTotals fillTotalsValues(DaoReportData param) throws TechnicalException{

        try{
            log.debug("Building totals query...");
            StringBuilder queryTotals = buildTotalsQuery(param);

            log.debug("TOTALS QUERY :{}", queryTotals);

            MerchantSummaryTotalsExtractor rsExtractor = new MerchantSummaryTotalsExtractor(param.isTaxEnabled());
            MerchantSummaryTotals result = jdbcTemplate.query(queryTotals.toString(), rsExtractor);
            log.debug("Total ready...");
            return result;
        }catch (EmptyResultDataAccessException ex){
            log.warn("No merchant transaction summaries. Ex:{}", ex);
            return null;
        }catch (Exception ex){
            log.error("Error in main query for transactions. Ex:{}", ex);
            throw new TechnicalException("DataAccess Error", ex);
        }
    }



    private boolean ISOLevel5(String strAccessLevel, String strDistChain) {
        return strAccessLevel.equals(DebisysConstants.ISO) && strDistChain.equals(DebisysConstants.DIST_CHAIN_5_LEVEL);
    }
    private boolean ISOLevel3(String strAccessLevel, String strDistChain) {
        return strAccessLevel.equals(DebisysConstants.ISO) && strDistChain.equals(DebisysConstants.DIST_CHAIN_3_LEVEL);
    }


    private String rateStatement(){
        String rate_div =
        " 100 * (" +
        "CASE WHEN p.program_id=154 THEN wt.amount " +
        "ELSE " +
            "CASE WHEN p.ach_type=6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt " +
            "ELSE " +
                "CASE WHEN ISNULL(wt.transfixedfeeamt,0)=0 THEN wt.amount " +
                "ELSE wt.transfixedfeeamt " +
                "END " +
            "END " +
        "END ) ";
        return rate_div;
    }




    ///// ////////////////  \\\\\\\\\\\\\\\\  \\\\\\\
    ///             MAIN                   \\\\
    ///// ////////////////  \\\\\\\\\\\\\\\\  \\\\\\\

    private StringBuilder buildMainQuery(DaoReportData param){

        StringBuilder selectQuery = new StringBuilder();
        buildMainQuerySelect(selectQuery);

        if (param.isTaxEnabled()) {
            addMainQuerySelectTaxEnabled(selectQuery, param.isInternationalTaxEnabled());
        }else{
            addMainQuerySelectTaxNotEnabled(selectQuery);
        }

        //MERCHANT TRANSACTIONS INNER QUERY
        StringBuilder innerQuery = buildInnerQuery(param);

        String selectAlternative = selectQuery.toString().replaceFirst(ADDITIONAL_DATA, ((this.isPromoDataPermissionEnabled()) ? COMMA_ADDITIONAL_DATA : ""));
        
        // GROUP BY
        String groupByMainQuery = this.addGroupMainQuery().replaceFirst(ADDITIONAL_DATA, ((this.isPromoDataPermissionEnabled()) ? COMMA_ADDITIONAL_DATA : ""));
        
        StringBuilder mainQuery = new StringBuilder();
        mainQuery
        .append(selectAlternative)
        .append(" FROM (").append(innerQuery).append(") merchantTransactions ")
        .append(", invoice_types it ")
        .append(" WHERE merchantTransactions.invoice = it.id ")
        .append(groupByMainQuery)
        .append(" ORDER BY dba ASC ");

        return mainQuery;
    }

    private void buildMainQuerySelect(StringBuilder sb) {

        sb
        .append("SELECT ")
        .append("  salesman_id")
        .append(", salesmanname")
        .append(", account_no")
        .append(", dba")
        .append(ADDITIONAL_DATA)
        .append(", routeId")
        .append(", routeName")
        .append(", merchant_id")
        .append(", it.description AS invoice_type")
        .append(", sum(qty) AS qty")
        .append(", sum(total_sales) AS total_sales")
        .append(", sum(merchant_commission) AS merchant_commission")
        .append(", sum(rep_commission) AS rep_commission")
        .append(", sum(subagent_commission) AS subagent_commission")
        .append(", sum(agent_commission) AS agent_commission")
        .append(", sum(iso_commission) AS iso_commission")
        .append(", sum(available_credit) AS available_credit")
        .append(", sum(adj_amount) AS adj_amount")
        .append(", phys_address AS phys_address")
        .append(", phys_city")
        .append(", phys_state")
        .append(", phys_zip")
        .append(", sum(total_bonus) AS total_bonus")
        .append(", sum(total_recharge) AS total_recharge")
        .append(", liabilityLimit ");

    }

    private void addMainQuerySelectTaxNotEnabled(StringBuilder sb) {
        sb
        .append(", runningLiability")
        .append(", available_credit ");
    }

    private void addMainQuerySelectTaxEnabled(StringBuilder sb, boolean internationalTaxEnabled) {
        sb
        .append(", sum(tax_total_sales) AS tax_total_sales")
        .append(", sum(tax_merchant_commission) AS tax_merchant_commission")
        .append(", sum(tax_rep_commission) AS tax_rep_commission")
        .append(", sum(tax_agent_commission) AS tax_agent_commission")
        .append(", sum(tax_subagent_commission) AS tax_subagent_commission")
        .append(", sum(tax_iso_commission) AS tax_iso_commission")
        .append(", sum(runningliability) AS runningLiability ");

        if(internationalTaxEnabled){
            sb.append(", sum(tax_amount) AS tax_amount ");
        }

    }



    private String addGroupMainQuery() {
        StringBuilder sb = new StringBuilder();

        sb.append(" GROUP BY ")
        .append("merchant_id")
        .append(", dba")
        .append(ADDITIONAL_DATA)
        .append(", it.description")
        .append(", RouteID")
        .append(", liabilitylimit")
        .append(", runningliability")
        .append(", available_credit")
        .append(", phys_address")
        .append(", phys_city")
        .append(", phys_state")
        .append(", phys_zip")
        .append(", salesman_id")
        .append(", salesmanname")
        .append(", account_no ")
        .append(", routeName ");

        return sb.toString();
    }

    private void buildGroupStatement(StringBuilder sb) {
        sb.append(" GROUP BY ")
        .append("wt.merchant_id")
        .append(", m.dba")
        .append(ADDITIONAL_DATA)
        .append(", m.routeId")
        .append(", m.invoice")
        .append(", m.runningliability")
        .append(", m.liabilitylimit")
        .append(", m.phys_address")
        .append(", m.phys_city")
        .append(", m.phys_state")
        .append(", m.phys_zip")
        .append(", m.salesman_id")
        .append(", s.salesmanname")
        .append(", m.account_no ")
        .append(", rt.routeName ");

    }


    private StringBuilder buildInnerQuery(DaoReportData param){

        String accessLevel = String.valueOf(param.getAccessLevel());
        String chain = String.valueOf(param.getDistChainType());
        String refId = String.valueOf(param.getRefId());


        //SELECT
        StringBuilder selectQuery = new StringBuilder();
        buildInnerQuerySelect(selectQuery);

        if (param.isInternationalTaxEnabled()) {
            addInnerQuerySelectInternationalTaxColumns(selectQuery, param.getStrTax());
        }
        if (param.isMxTaxEnabled()) {
            addInnerQuerySelectMxTaxColumns(selectQuery, param.getStrTax());
        }

        //FROM
        StringBuilder fromQuery = new StringBuilder();
        addWebTransactionsFrom(fromQuery, refId);


        boolean levelCarrier = accessLevel.equals(DebisysConstants.CARRIER);
        if (levelCarrier) {
            addMerchantCarrierJoin(fromQuery);
        }

        //WHERE
        StringBuilder whereQuery = new StringBuilder();
        fillSQLWhere(whereQuery, refId, accessLevel, chain, param.getMerchantIds(), param.getRepIds(), param.getProductIds(), param.getSaleIds());

        //StringBuilder strDateTime = new StringBuilder();
        addWhereDates(param.getTimeZonedDates(), whereQuery);

        //Billing Types
        StringBuilder whereBillingTypePositives = new StringBuilder();
        addWhereBillTypePositives(whereBillingTypePositives);

        StringBuilder whereBillingTypeNegatives = new StringBuilder();
        addWhereBillTypeNegatives(whereBillingTypeNegatives);


        // GROUP BY
        StringBuilder groupStatement = new StringBuilder();
        buildGroupStatement(groupStatement);
        String groupByWithAdditionalData = groupStatement.toString().replaceFirst(ADDITIONAL_DATA, ((this.isPromoDataPermissionEnabled()) ? WT_ADDITIONAL_DATA : ""));

        String selectWithCount = selectQuery.toString().replaceFirst(COUNT_TOKEN, "count(wt.rec_id) as qty").replaceFirst(ADDITIONAL_DATA, ((this.isPromoDataPermissionEnabled()) ? WT_ADDITIONAL_DATA : ""));
        String selectWithCountZero = selectQuery.toString().replaceFirst(COUNT_TOKEN, "0 as qty").replaceFirst(ADDITIONAL_DATA, ((this.isPromoDataPermissionEnabled()) ? WT_ADDITIONAL_DATA : ""));


        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append(selectWithCount).append(fromQuery).append(whereQuery).append(whereBillingTypePositives).append(groupByWithAdditionalData);
        mainQuery.append(" UNION ALL ");
        mainQuery.append(selectWithCountZero).append(fromQuery).append(whereQuery).append(whereBillingTypeNegatives).append(groupByWithAdditionalData);

        return mainQuery;
    }


    private void buildInnerQuerySelect(StringBuilder sb) {

        sb.append("SELECT ")
        .append(COUNT_TOKEN)
        .append(", ISNULL(m.salesman_id,'') salesman_id")
        .append(", ISNULL(s.salesmanname,'') salesmanname")
        .append(", ISNULL(m.account_no,'') account_no")
        .append(", m.dba")
        .append(ADDITIONAL_DATA)
        .append(", m.routeId")
        .append(", rt.routeName")
        .append(", m.invoice")
        .append(", wt.merchant_id")
        .append(", sum(wt.amount) as total_sales")
        .append(", sum(wt.merchant_rate /  ").append(rateStatement()).append(") AS merchant_commission")
        .append(", sum(wt.rep_rate / ").append(rateStatement()).append(") AS rep_commission")
        .append(", sum(wt.subagent_rate / ").append(rateStatement()).append(") AS subagent_commission")
        .append(", sum(wt.agent_rate / ").append(rateStatement()).append(") AS agent_commission")
        .append(", sum(wt.iso_rate / ").append(rateStatement()).append(") AS iso_commission")
        .append(", m.runningLiability")
        .append(", m.liabilityLimit")
        .append(", m.liabilityLimit - m.runningLiability AS available_credit")
        .append(", sum((wt.amount * (100 - wt.iso_discount_rate) / 100) * cast(wt.iso_discount_rate AS bit)) AS adj_amount")
        .append(", m.phys_address")
        .append(", m.phys_city")
        .append(", m.phys_state")
        .append(", m.phys_zip")
        .append(", sum(wt.bonus_amount) AS total_bonus")
        .append(", sum(wt.bonus_amount) + sum(wt.amount) AS total_recharge ");

    }

    private void addInnerQuerySelectInternationalTaxColumns(StringBuilder sb, String tax) {

        sb
        .append(", sum(wt.amount - wt.amount/(1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_amount")
        .append(", sum(wt.amount / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_total_sales")

/*        .append(", sum((wt.merchant_rate / ").append(tax).append(") / (1 + isnull(wt.taxpercentage / 10000, - 1))) AS tax_merchant_commission")
        .append(", sum((wt.rep_rate / ").append(tax).append(") / (1 + isnull(wt.taxpercentage / 10000, - 1))) AS tax_rep_commission")
        .append(", sum((wt.agent_rate / ").append(tax).append(") / (1 + isnull(wt.taxpercentage / 10000, - 1))) AS tax_agent_commission")
        .append(", sum((wt.subagent_rate / ").append(tax).append(") / (1 + isnull(wt.taxpercentage / 10000, - 1))) AS tax_subagent_commission")
        .append(", sum((wt.iso_rate / ").append(tax).append(") / (1 + isnull(wt.taxpercentage / 10000, - 1))) AS tax_iso_commission ")*/

        .append(", sum((wt.merchant_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_merchant_commission")
        .append(", sum((wt.rep_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_rep_commission")
        .append(", sum((wt.agent_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_agent_commission")
        .append(", sum((wt.subagent_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_subagent_commission")
        .append(", sum((wt.iso_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_iso_commission ")


        ;

    }
    private void addInnerQuerySelectMxTaxColumns(StringBuilder sb, String tax) {

        sb
        .append(", sum(wt.amount / ").append(tax).append(") as tax_total_sales")
        .append(", sum(wt.merchant_rate / ").append(rateStatement()).append(") /").append(tax).append(" AS tax_merchant_commission")
        .append(", sum(wt.rep_rate / ").append(rateStatement()).append(") /").append(tax).append(" AS tax_rep_commission")
        .append(", sum(wt.agent_rate / ").append(rateStatement()).append(") /").append(tax).append(" AS tax_agent_commission")
        .append(", sum(wt.subagent_rate / ").append(rateStatement()).append(") /").append(tax).append(" AS tax_subagent_commission")
        .append(", sum(wt.iso_rate / ").append(rateStatement()).append(") /").append(tax).append(" AS tax_iso_commission ");

    }



    ///// ////////////////  \\\\\\\\\\\\\\\\  \\\\\\\
    ///             QUERY TOTALS               \\\\
    ///// ////////////////  \\\\\\\\\\\\\\\\  \\\\\\\

    private StringBuilder buildTotalsQuery(DaoReportData param) {

        String accessLevel = String.valueOf(param.getAccessLevel());
        String chain = String.valueOf(param.getDistChainType());
        String refId = String.valueOf(param.getRefId());

        //SELECT
        StringBuilder selectQuery = new StringBuilder();
        buildTotalQuerySelect(selectQuery);

        if (param.isInternationalTaxEnabled()) {
            addTotalQueryInternationalTaxColumns(selectQuery, param.getStrTax());
        }
        if (param.isMxTaxEnabled()) {
            addTotalQueryMxTaxColumns(selectQuery, param.getStrTax());
        }

        //FROM
        StringBuilder fromQuery = new StringBuilder();
        addWebTransactionsFrom(fromQuery, refId);


        boolean levelCarrier = accessLevel.equals(DebisysConstants.CARRIER);
        if (levelCarrier) {
            addMerchantCarrierJoin(fromQuery);
        }


        //WHERE
        StringBuilder whereQuery = new StringBuilder();
        fillSQLWhere(whereQuery, refId, accessLevel, chain, param.getMerchantIds(), param.getRepIds(), param.getProductIds(), param.getSaleIds());

        addWhereDates(param.getTimeZonedDates(), whereQuery);

        StringBuilder whereBillingTypePositives = new StringBuilder();
        addWhereBillTypePositives(whereBillingTypePositives);

        StringBuilder whereBillingTypeNegatives = new StringBuilder();
        addWhereBillTypeNegatives(whereBillingTypeNegatives);
        

        String selectWithCount = selectQuery.toString().replaceFirst(COUNT_TOKEN, "count(wt.rec_id) as qty");
        String selectWithCountZero = selectQuery.toString().replaceFirst(COUNT_TOKEN, "0 as qty");

        
        //MAIN TOTAL QUERY
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append(selectWithCount).append(fromQuery).append(whereQuery).append(whereBillingTypePositives).append(" HAVING count(wt.rec_id) > 0 ");
        mainQuery.append(" UNION ALL ");
        mainQuery.append(selectWithCountZero).append(fromQuery).append(whereQuery).append(whereBillingTypeNegatives).append(" HAVING count(wt.rec_id) > 0 ");

        return mainQuery;
    }



    private void addWhereBillTypeNegatives(StringBuilder sb) {
        sb
        .append(" AND wt.billing_typeID in (2,5) ");
    }

    private void addWhereBillTypePositives(StringBuilder sb) {
        sb
        .append(" AND wt.billing_typeID in (1,3,6,0,11) ");
    }
    
    private void addWhereDates(TimeZoneDates tzd, StringBuilder whereQuery) {
        String initDate = DateUtil.formatSqlDateTimeMillis(tzd.getStartDate());
        String finishDate = DateUtil.formatSqlDateTimeMillis(tzd.getEndDate());
        whereQuery.append(" AND (wt.datetime  >= '" + initDate + "' ");
        whereQuery.append(" AND wt.datetime  < '" + finishDate + "') ");
    }

    private void fillSQLWhere(StringBuilder sb, String refId,  String accessLevel, String chain, List<Long> merchantIds, List<Long> repIds,
                              List<Long> productIds, List<Long> saleIds) {

        sb.append(" WHERE 1 = 1 ");

        if(ISOLevel3(accessLevel, chain)) {
            sb.append(" AND wt.rep_id IN (SELECT rep_id FROM reps with (nolock) WHERE iso_id =").append(refId).append(") ");
        }else if (ISOLevel5(accessLevel, chain)){
            sb.append(" AND wt.rep_id IN")
            .append("(SELECT rep_id FROM reps with (nolock) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN")
                .append("(SELECT rep_id FROM reps with (nolock) WHERE type=").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id IN")
                    .append("(SELECT rep_id FROM reps with (nolock) WHERE type=").append(DebisysConstants.REP_TYPE_AGENT).append(" AND iso_id =").append(refId)
            .append("))) ");
        }else if (accessLevel.equals(DebisysConstants.AGENT)){
            sb.append(" AND wt.rep_id IN")
            .append("(SELECT rep_id FROM reps with (nolock) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN")
                .append("(SELECT rep_id FROM reps with (nolock) WHERE type=").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id =").append(refId)
            .append(")) ");
        }else if (accessLevel.equals(DebisysConstants.SUBAGENT)){
            sb.append(" AND wt.rep_id IN")
            .append("(SELECT rep_id FROM reps with (nolock) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(refId)
            .append(") ");
        }else if (accessLevel.equals(DebisysConstants.REP)){
            sb.append(" AND wt.rep_id = ").append(refId).append(" ");
        }else if (accessLevel.equals(DebisysConstants.MERCHANT)){
            sb.append(" AND wt.merchant_id =").append(refId).append(" ");
        }else if (accessLevel.equals(DebisysConstants.CARRIER)){
            sb.append(" AND mc.carrierid in (SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid =").append(refId).append(" ");
        }


        boolean validMerchantIds = merchantIds != null && !merchantIds.isEmpty();
        boolean validRepIds = repIds != null && !repIds.isEmpty();
        boolean validProductIds = productIds != null && !productIds.isEmpty();
        boolean validSaleIds = saleIds != null && !saleIds.isEmpty();

        if(validMerchantIds){
            String csvIds = StringUtil.numberListToCsv(merchantIds);
            sb.append(" AND wt.merchant_id in (").append(csvIds).append(") ");
        }

        if(validRepIds){
            String csvIds = StringUtil.numberListToCsv(repIds);
            sb.append(" AND wt.merchant_id IN (SELECT merchant_id FROM merchants with (nolock) WHERE rep_id IN (").append(csvIds).append(")) ");
        }

        if(validProductIds){
            String csvIds = StringUtil.numberListToCsv(productIds);
            sb.append(" AND wt.id in (").append(csvIds).append(") ");
        }

        if(validSaleIds){
            String csvIds = StringUtil.numberListToCsv(saleIds);
            sb.append(" AND m.salesman_id in (").append(csvIds).append(") ");
        }
    }


    private void addWebTransactionsFrom(StringBuilder sb, String refId) {
        sb
        .append(" FROM web_transactions wt WITH (NOLOCK) ")
        .append("INNER JOIN merchants m WITH (NOLOCK) ON wt.merchant_id=m.merchant_id ")
        .append("LEFT JOIN s2ksalesman s with (nolock) on s.salesmanid=LTRIM(RTRIM(m.salesman_id)) and s.isoid =").append(refId).append(" ")
        .append("INNER JOIN products p WITH (NOLOCK) ON p.id = wt.id ")
        .append("LEFT OUTER JOIN routes rt WITH (NOLOCK) ON rt.RouteID = m.RouteID ");
    }

    private void addMerchantCarrierJoin(StringBuilder sb) {
        sb
        .append(" INNER JOIN merchantCarriers mc WITH (NOLOCK) ON mc.merchantId=wt.merchant_id and mc.carrierId = p.carrier_id ");
    }

    private void buildTotalQuerySelect(StringBuilder sb) {

        sb.append("SELECT ")
        .append(COUNT_TOKEN)
        .append(", sum(wt.amount) AS total_sales")
        .append(", sum(wt.merchant_rate / ").append(rateStatement()).append(") AS merchant_commission")
        .append(", sum(wt.rep_rate / ").append(rateStatement()).append(") AS rep_commission")
        .append(", sum(wt.agent_rate /").append(rateStatement()).append(") as agent_commission")
        .append(", sum(wt.subagent_rate / ").append(rateStatement()).append(") as subagent_commission")
        .append(", sum(wt.iso_rate / ").append(rateStatement()).append(") as iso_commission")
        .append(", sum((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate as bit)) as adj_amount")
        .append(", sum(wt.bonus_amount) as total_bonus")
        .append(", sum(wt.bonus_amount) + sum(wt.amount) as total_recharge ");
    }

    private void addTotalQueryInternationalTaxColumns(StringBuilder sb, String tax) {
        sb
        .append(", sum(isnull(wt.taxtype,0)) AS taxtypecheck")
        .append(", sum(wt.amount - wt.amount/(1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_amount")
        .append(", sum(wt.amount/(1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_total_sales")
        .append(", sum((wt.merchant_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_merchant_commission")
        .append(", sum((wt.rep_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_rep_commission")
        .append(", sum((wt.agent_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_agent_commission")
        .append(", sum((wt.subagent_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_subagent_commission")
        .append(", sum((wt.iso_rate / ").append(rateStatement()).append(") / (1 + isnull(wt.taxpercentage / 10000, ").append(tax).append(" - 1))) as tax_iso_commission ")
        ;
    }

    private void addTotalQueryMxTaxColumns(StringBuilder sb, String tax) {
        sb
        .append(", sum(wt.amount / ").append(tax).append(") as tax_total_sales")
        .append(", sum(wt.merchant_rate / ").append(rateStatement()).append(") / ").append(tax).append(" AS tax_merchant_commission")
        .append(", sum(wt.rep_rate / ").append(rateStatement()).append(") / ").append(tax).append(" AS tax_rep_commission")
        .append(", sum(wt.agent_rate / ").append(rateStatement()).append(") / ").append(tax).append(" AS tax_agent_commission")
        .append(", sum(wt.subagent_rate / ").append(rateStatement()).append(") / ").append(tax).append(" AS tax_subagent_commission")
        .append(", sum(wt.iso_rate / ").append(rateStatement()).append(") / ").append(tax).append(" AS tax_iso_commission ")
        ;
    }
    
    private SessionData getSessionData(HttpSession session){
        SessionData sessionData = (SessionData)session.getAttribute("SessionData");
        if(sessionData == null){
            sessionData = new SessionData();
        }
        return sessionData;
    }

    protected SessionData getSessionData() {
        return getSessionData(httpServletRequest.getSession());
    }
    
    protected boolean isPromoDataPermissionEnabled() {
        try {
            return ReportsUtil.isIntlAndHasDataPromoPermission(getSessionData(), servletContext);
        } catch (Exception e) {
            return false;
        }
    }
    
}
