package net.emida.supportsite.reports.transactionssumaries.merchant;


import java.util.Date;
import java.util.List;

/**
 * @author Franky Villadiego
 */
public class ServiceReportParam {

    //Pagination info
    private String sortFieldName;
    private String sortOrder;
    private int startPage;
    private int endPage;

    //Required data
    boolean internationalDeployment;
    boolean domesticDeployment;
    boolean mexicoDeployment;
    private long refId;
    private String accessLevel;
    private String distChainType;
    private Date startDate;
    private Date endDate;

    //Optional data
    private List<Long> repIds;
    private List<Long> saleIds;
    private List<Long> merchantIds;
    private List<Long> productIds;


    private double mxValueAddedTax;

    //Permissions
    private boolean permEnableTaxCalc;
    private boolean permMxReportWithTaxes;


    public String getSortFieldName() {
        return sortFieldName;
    }

    public void setSortFieldName(String sortFieldName) {
        this.sortFieldName = sortFieldName;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public boolean isInternationalDeployment() {
        return internationalDeployment;
    }

    public void setInternationalDeployment(boolean internationalDeployment) {
        this.internationalDeployment = internationalDeployment;
    }

    public boolean isDomesticDeployment() {
        return domesticDeployment;
    }

    public void setDomesticDeployment(boolean domesticDeployment) {
        this.domesticDeployment = domesticDeployment;
    }

    public boolean isMexicoDeployment() {
        return mexicoDeployment;
    }

    public void setMexicoDeployment(boolean mexicoDeployment) {
        this.mexicoDeployment = mexicoDeployment;
    }

    public long getRefId() {
        return refId;
    }

    public void setRefId(long refId) {
        this.refId = refId;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getDistChainType() {
        return distChainType;
    }

    public void setDistChainType(String distChainType) {
        this.distChainType = distChainType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Long> getRepIds() {
        return repIds;
    }

    public void setRepIds(List<Long> repIds) {
        this.repIds = repIds;
    }

    public List<Long> getSaleIds() {
        return saleIds;
    }

    public void setSaleIds(List<Long> saleIds) {
        this.saleIds = saleIds;
    }

    public List<Long> getMerchantIds() {
        return merchantIds;
    }

    public void setMerchantIds(List<Long> merchantIds) {
        this.merchantIds = merchantIds;
    }

    public List<Long> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<Long> productIds) {
        this.productIds = productIds;
    }

    public boolean isPermEnableTaxCalc() {
        return permEnableTaxCalc;
    }

    public void setPermEnableTaxCalc(boolean permEnableTaxCalc) {
        this.permEnableTaxCalc = permEnableTaxCalc;
    }

    public boolean isPermMxReportWithTaxes() {
        return permMxReportWithTaxes;
    }

    public void setPermMxReportWithTaxes(boolean permMxReportWithTaxes) {
        this.permMxReportWithTaxes = permMxReportWithTaxes;
    }

    public double getMxValueAddedTax() {
        return mxValueAddedTax;
    }

    public void setMxValueAddedTax(double mxValueAddedTax) {
        this.mxValueAddedTax = mxValueAddedTax;
    }
}
