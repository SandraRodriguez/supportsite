package net.emida.supportsite.reports.transactionssumaries.merchant;



/**
 * @author Franky Villadiego
 */
public interface TransactionSummaryReportDao {

    long count(DaoReportData param);

    TransactionSummaryReportResult getResult(DaoReportData param);
}
