package net.emida.supportsite.reports.transactionssumaries.multisourcetransactionreport;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;

@Controller
@RequestMapping(value= UrlPaths.REPORT_PATH)
public class MultiSourceTransactionReport  extends BaseController{
	
	
	
	@RequestMapping(value= UrlPaths.MULTISOURCE_TRANSACTION_REPORT_FILTERS, method=RequestMethod.GET )
	public String reportFilters(){
		
		System.out.println(" ya pase por aqui: ......");
		return "";
		
	}

	@Override
	public int getSection() {
		return 4;
	}

	@Override
	public int getSectionPage() {
		 return 14;
	}

}
