package net.emida.supportsite.reports.transactionssumaries.merchant;



/**
 * @author Franky Villadiego
 */

public interface TransactionSummaryReportService {


    DaoReportData buildReportData(ServiceReportParam param);

    TransactionSummaryReportResult buildReport(ServiceReportParam param);

}
