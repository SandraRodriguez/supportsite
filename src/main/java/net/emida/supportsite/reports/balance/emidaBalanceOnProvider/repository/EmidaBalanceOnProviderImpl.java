/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.repository;

import java.util.List;
import java.util.Calendar;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.emida.supportsite.reports.balance.emidaBalanceOnProvider.dto.EmidaBalanceOnProviderDto;


/**
 *
 * @author fernandob
 */
@Repository
public class EmidaBalanceOnProviderImpl implements IEmidaBalanceOnProviderDao{
        
    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<EmidaBalanceOnProviderDto> getEmidaBalanceOnProviderResult(Calendar startDate, 
            Calendar endDate, String[] providerIdList) throws TechnicalException {
        
        List<EmidaBalanceOnProviderDto> retValue;
        StringBuilder sbSqlQuery = new StringBuilder();
        sbSqlQuery.append("SELECT EBP.[balanceQueryDate], EBP.[providerId], PR.[name], EBP.[operatorName], EBP.[balance] ");
        sbSqlQuery.append("FROM [dbo].[EmidaBalanceOnProvider] EBP WITH(NOLOCK) ");
        sbSqlQuery.append("INNER JOIN [dbo].[Provider] PR WITH(NOLOCK) ");
        sbSqlQuery.append("ON EBP.[providerId] = PR.[provider_id] ");
        sbSqlQuery.append("WHERE EBP.[balanceQueryDate] >= ? AND EBP.[balanceQueryDate] <= ? ");
        
        String sqlQuery;
        if((providerIdList != null) && (providerIdList.length > 0)){
            sbSqlQuery.append(" AND EBP.[providerId] IN (");
            for(int i = 0;i < providerIdList.length;i++){
                sbSqlQuery.append(providerIdList[i]);
                if(i < (providerIdList.length - 1)){
                    sbSqlQuery.append(",");
                }
            }
            sbSqlQuery.append(")");
            sqlQuery = sbSqlQuery.toString();
        }else{
            sqlQuery = sbSqlQuery.toString();
        }
        ArrayList<Object> queryParams = new ArrayList<Object>();
        queryParams.add(startDate.getTime());
        queryParams.add(endDate.getTime());
        retValue = jdbcTemplate.query(sqlQuery, queryParams.toArray(), new EmidaBalanceOnProviderMapper());
        
        return retValue;        
    }
    
}
