/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.services;

import java.util.Calendar;
import java.util.List;
import net.emida.supportsite.reports.balance.emidaBalanceOnProvider.dto.EmidaBalanceOnProviderDto;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author fernandob
 */
public interface IStorageService {
    
    public List<EmidaBalanceOnProviderDto> getEmidaBalanceOnProviderResult(Calendar startDate,
            Calendar endDate, String providerIdList) throws TechnicalException;;
    
}
