/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.services;

import java.util.Map;
import java.io.Writer;
import java.util.List;
import org.slf4j.Logger;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedWriter;
import org.slf4j.LoggerFactory;
import com.debisys.languages.Languages;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvUtil;
import org.springframework.stereotype.Service;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.TechnicalException;


/**
 *
 * @author fernandob
 */
@Service
public class EmidaBalanceOnProviderCsvBuilder implements CsvBuilderService {

    private static final Logger logger = LoggerFactory.getLogger(EmidaBalanceOnProviderCsvBuilder.class);

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {
            if (list == null){
                throw new IllegalArgumentException("Result lists expected");
            }

            //File to fill
            writer = getFileWriter(filePath);

            logger.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            String language = (String) additionalData.get("language");
            
            // Report Title
            writer.write(Languages.getString("jsp.report.balance.emidaBalanceOnProvider.title", language));;

            // Add report filter values
            
            writer.write(String.format("\n\n%s : %s\n\n",
                    Languages.getString("jsp.report.balance.emidaBalanceOnProvider.startDate", language),
                    additionalData.get("startDate").toString()));
            writer.write(String.format("\n\n%s : %s\n\n",
                    Languages.getString("jsp.report.balance.emidaBalanceOnProvider.endDate", language),
                    additionalData.get("endDate").toString()));
            writer.write(String.format("\n\n%s : %s\n\n",
                    Languages.getString("jsp.report.balance.emidaBalanceOnProvider.providerList", language),
                    additionalData.get("providerList").toString()));

            // Add report column headers
            List<String> reportHeaders = generateReportHeaders(additionalData);
            String reportHeaderRow = CsvUtil.generateRow(reportHeaders, true);            
            writer.write(reportHeaderRow);

            // Write report content
            List<CsvRow> reportRows = (List<CsvRow>) list;
            this.fillWriter(reportRows, writer, additionalData);

        } catch (Exception ex) {
            logger.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {
                    logger.error("Error closing csv file : {}", filePath);
                    throw new TechnicalException(ex.getMessage(), ex);
                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {
        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            writer.write(stringifyRow);
        }
    }

    private List<String> generateReportHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");
        List<String> headers = new ArrayList<String>();

        headers.add(Languages.getString("jsp.report.balance.emidaBalanceOnProvider.balanceQueryDate", language));
        headers.add(Languages.getString("jsp.report.balance.emidaBalanceOnProvider.providerId", language));
        headers.add(Languages.getString("jsp.report.balance.emidaBalanceOnProvider.providerName", language));
        headers.add(Languages.getString("jsp.report.balance.emidaBalanceOnProvider.operatorName", language));
        headers.add(Languages.getString("jsp.report.balance.emidaBalanceOnProvider.balance", language));
        return headers;
    }

}
