/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.services;

import java.util.Map;
import java.util.List;
import java.util.Locale;
import org.slf4j.Logger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Calendar;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import com.debisys.utils.DateUtil;
import org.springframework.ui.Model;
import javax.servlet.http.HttpSession;
import org.springframework.ui.ModelMap;
import net.emida.supportsite.dto.Provider;
import net.emida.supportsite.mvc.UrlPaths;
import org.springframework.http.MediaType;
import javax.servlet.http.HttpServletRequest;
import net.emida.supportsite.dao.ProviderDao;
import javax.servlet.http.HttpServletResponse;
import net.emida.supportsite.mvc.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.core.io.FileSystemResource;
import net.emida.supportsite.util.csv.CsvBuilderService;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Qualifier;
import net.emida.supportsite.reports.balance.emidaBalanceOnProvider.dto.EmidaBalanceOnProviderDto;
import net.emida.supportsite.reports.balance.emidaBalanceOnProvider.repository.IEmidaBalanceOnProviderDao;

/**
 *
 * @author fernandob
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class EmidaBalanceOnProviderController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(EmidaBalanceOnProviderController.class);

    private static final String REPORT_DATE_FORMAT = "MM/dd/yyyy"; // How param dates arrive to the controller
    private static final String REPORT_PATH = "reports/balance/emidaBalanceOnProvider/emidaBalanceOnProvider";

    @Autowired
    private IEmidaBalanceOnProviderDao emidaBalanceDao;

    @Autowired
    @Qualifier("emidaBalanceOnProviderCsvBuilder")
    private CsvBuilderService csvBuilderService;

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 65;
    }

    @RequestMapping(value = UrlPaths.EMIDA_BALANCE_ON_PROVIDER, method = RequestMethod.GET)
    public String showEmidaBalanceOnProviderForm(ModelMap modelMap) {
        List<Provider> providerList = ProviderDao.getAllProviders();
        modelMap.addAttribute("providerList", providerList);

        return REPORT_PATH;
    }

    @RequestMapping(value = UrlPaths.EMIDA_BALANCE_ON_PROVIDER_CONTENT, method = RequestMethod.POST)
    @ResponseBody
    public List<EmidaBalanceOnProviderDto> getEmidaBalanceOnProviderResult(@RequestParam(value = "startDate") String startDate,
            @RequestParam(value = "endDate") String endDate,
            @RequestParam(value = "providerList[]") String[] providerList,
            Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        
        Calendar calStartDate = DateUtil.fromStringToCalendar(startDate, REPORT_DATE_FORMAT);
        calStartDate.set(Calendar.HOUR_OF_DAY, 0);
        calStartDate.set(Calendar.MINUTE, 0);
        calStartDate.set(Calendar.SECOND, 0);
        calStartDate.set(Calendar.MILLISECOND, 0);

        Calendar calEndDate = DateUtil.fromStringToCalendar(endDate, REPORT_DATE_FORMAT);
        calEndDate.set(Calendar.HOUR_OF_DAY, 23);
        calEndDate.set(Calendar.MINUTE, 59);
        calEndDate.set(Calendar.SECOND, 59);
        calEndDate.set(Calendar.MILLISECOND, 0);

        log.info(String.format("getEmidaBalanceOnProviderResult params : startDate = %s | endDate = %s | providers = %s",
                DateUtil.formatCalendarDate(calStartDate, null), DateUtil.formatCalendarDate(calEndDate, null), Arrays.toString(providerList)));
        
        List<EmidaBalanceOnProviderDto> retValue = this.emidaBalanceDao.getEmidaBalanceOnProviderResult(calStartDate, calEndDate, providerList);
        return retValue;
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.EMIDA_BALANCE_ON_PROVIDER_DOWNLOAD, 
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam(value = "startDate") String startDate,
            @RequestParam(value = "endDate") String endDate,
            @RequestParam(value = "providerList") String[] providerList) throws IOException {

        log.info("Downloading file...");

        Calendar calStartDate = DateUtil.fromStringToCalendar(startDate, REPORT_DATE_FORMAT);
        calStartDate.set(Calendar.HOUR_OF_DAY, 0);
        calStartDate.set(Calendar.MINUTE, 0);
        calStartDate.set(Calendar.SECOND, 0);
        calStartDate.set(Calendar.MILLISECOND, 0);

        Calendar calEndDate = DateUtil.fromStringToCalendar(endDate, REPORT_DATE_FORMAT);
        calEndDate.set(Calendar.HOUR_OF_DAY, 23);
        calEndDate.set(Calendar.MINUTE, 59);
        calEndDate.set(Calendar.SECOND, 59);
        calEndDate.set(Calendar.MILLISECOND, 0);
        
        String startDateFormatted = DateUtil.formatCalendarDate(calStartDate, null);
        String endDateFormatted = DateUtil.formatCalendarDate(calEndDate, null);

        log.info(String.format("getEmidaBalanceOnProviderResult params : startDate = %s | endDate = %s | providers = %s",
                startDateFormatted, endDateFormatted, Arrays.toString(providerList)));

        StringBuilder sbProviderList = new StringBuilder();
        for (String currentProviderId : providerList) {
            sbProviderList.append("|");
            sbProviderList.append(currentProviderId);
        }
        String strProviderList = (sbProviderList.length() > 0)?sbProviderList.toString().substring(1):"";

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("startDate", startDateFormatted);
        additionalData.put("endDate", endDateFormatted);
        additionalData.put("providerList", strProviderList);

        // Get report content
        List<EmidaBalanceOnProviderDto> reportResult = this.emidaBalanceDao.getEmidaBalanceOnProviderResult(calStartDate, calEndDate, providerList);

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.info("Generated path = {}", filePath);

        csvBuilderService.buildCsv(filePath, reportResult, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.info("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();

        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

        return zipFileResource;

    }

}
