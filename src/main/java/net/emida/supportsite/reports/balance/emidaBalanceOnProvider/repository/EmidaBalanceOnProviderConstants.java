/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.repository;

/**
 *
 * @author fernandob
 */
public class EmidaBalanceOnProviderConstants {
    
    // Report result column names
    public static final String BALANCE_QUERY_DATE = "balanceQueryDate";
    public static final String PROVIDER_ID = "providerId";
    public static final String PROVIDER_NAME = "name";
    public static final String OPERATOR_NAME = "operatorName";
    public static final String BALANCE = "balance";
    
}
