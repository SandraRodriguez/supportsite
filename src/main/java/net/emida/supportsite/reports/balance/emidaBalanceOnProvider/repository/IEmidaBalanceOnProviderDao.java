/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.repository;

import java.util.List;
import java.util.Calendar;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.emida.supportsite.reports.balance.emidaBalanceOnProvider.dto.EmidaBalanceOnProviderDto;

/**
 *
 * @author fernandob
 */
public interface IEmidaBalanceOnProviderDao {

    /**
     * This method will bring results saved by BJJobs when checking Emida
     * balances on providers
     * @param startDate Inital interval date for report data
     * @param endDate   Final interval date for report data
     * @param providerIdList Pipe separated list of provider IDs to be used on the IN sql clause
     * @return  A list of reresult records indicating the balance cheked on the
     *          selected providers list
     * @throws TechnicalException 
     */
    public List<EmidaBalanceOnProviderDto> getEmidaBalanceOnProviderResult(Calendar startDate,
            Calendar endDate, String[] providerIdList)throws TechnicalException;
    
}
