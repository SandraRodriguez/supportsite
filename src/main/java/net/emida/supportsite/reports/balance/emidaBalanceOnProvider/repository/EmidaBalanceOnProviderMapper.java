/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import static com.debisys.utils.DbUtil.hasColumn;
import net.emida.supportsite.reports.balance.emidaBalanceOnProvider.dto.EmidaBalanceOnProviderDto;
import static net.emida.supportsite.reports.balance.emidaBalanceOnProvider.repository.EmidaBalanceOnProviderConstants.*;

/**
 *
 * @author fernandob
 */
public class EmidaBalanceOnProviderMapper implements RowMapper<EmidaBalanceOnProviderDto> {

    @Override
    public EmidaBalanceOnProviderDto mapRow(ResultSet rs, int i) throws SQLException {
        EmidaBalanceOnProviderDto emidaBalance = new EmidaBalanceOnProviderDto();
        if ((hasColumn(rs, BALANCE_QUERY_DATE)) && (rs.getString(BALANCE_QUERY_DATE) != null)){            
            emidaBalance.setBalanceQueryDate(rs.getString(BALANCE_QUERY_DATE));
        }
        if (hasColumn(rs, PROVIDER_ID)) {            
            emidaBalance.setProviderId((rs.getString(PROVIDER_ID) == null) ? 0L : rs.getLong(PROVIDER_ID));
        }
        if (hasColumn(rs, PROVIDER_NAME)) {
            emidaBalance.setProviderName((rs.getString(PROVIDER_NAME) == null) ? "" : rs.getString(PROVIDER_NAME));
        }
        if (hasColumn(rs, OPERATOR_NAME)) {
            emidaBalance.setOperatorName((rs.getString(OPERATOR_NAME) == null) ? "" : rs.getString(OPERATOR_NAME));
        }
        if (hasColumn(rs, BALANCE)) {            
            emidaBalance.setBalance((rs.getString(BALANCE) == null) ? 0.0 : rs.getDouble(BALANCE));
        }        
        return emidaBalance;
    }

}
