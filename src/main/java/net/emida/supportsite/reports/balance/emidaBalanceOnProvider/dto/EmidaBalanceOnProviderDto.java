/*
 * EMIDA all rights reserved 1999-2019.
 */
package net.emida.supportsite.reports.balance.emidaBalanceOnProvider.dto;

import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 * Emida balance on provider result POJO
 * @author fernandob
 */
public class EmidaBalanceOnProviderDto implements CsvRow{
    
    private String balanceQueryDate;
    private long providerId;
    private String providerName;
    private String operatorName;
    private double balance;

    public String getBalanceQueryDate() {
        return balanceQueryDate;
    }

    public void setBalanceQueryDate(String balanceQueryDate) {
        this.balanceQueryDate = balanceQueryDate;
    }

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }
    
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public EmidaBalanceOnProviderDto() {
        this.balanceQueryDate = "";
        this.providerId = 0L;
        this.providerName = "";
        this.operatorName = "";
        this.balance = 0.0;
    }
    
    public EmidaBalanceOnProviderDto(String balanceQueryDate, 
            long providerId, String providerName, String operatorName, double balance) {
        this.balanceQueryDate = balanceQueryDate;
        this.providerId = providerId;
        this.providerName = providerName;
        this.operatorName = operatorName;
        this.balance = balance;
    }

    @Override
    public List<CsvCell> cells() {
        List<CsvCell> retValue = new ArrayList<CsvCell>();
        
        // Balance query date
        CsvCell balanceQueryDateCell = new CsvCell(this.balanceQueryDate);
        retValue.add(balanceQueryDateCell);
        // Provider Id
        CsvCell providerIdCell = new CsvCell(String.valueOf(this.providerId));
        retValue.add(providerIdCell);
        // Provider Name
        CsvCell providerNameCell = new CsvCell(this.providerName);
        retValue.add(providerNameCell);
        // Operator Name
        CsvCell operatorNameCell = new CsvCell(this.operatorName);
        retValue.add(operatorNameCell);
        // Balance
        CsvCell balanceCell = new CsvCell(String.valueOf(this.balance));
        retValue.add(balanceCell);
        
        return retValue;
    }
}
