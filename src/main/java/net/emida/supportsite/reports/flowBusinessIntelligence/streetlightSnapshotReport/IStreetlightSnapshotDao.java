
package net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport.dao.StreetlightSnapshotDetailModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author dgarzon
 */

public interface IStreetlightSnapshotDao {
    public List<StreetlightSnapshotDetailModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;
    public Long count(Map<String, Object> queryData) throws TechnicalException;
    
}
