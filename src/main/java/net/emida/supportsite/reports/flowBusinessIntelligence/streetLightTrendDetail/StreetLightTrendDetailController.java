
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.dao.StreetLightTrendDetailModel;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.dao.StreetLightTrendDetailQueryBuilder;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dgarzon
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class StreetLightTrendDetailController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(StreetLightTrendDetailController.class);

    @Autowired
    private IStreetLightTrendDetailDao iStreetLightTrendDetailDao;
    
    @Autowired
    @Qualifier("streetLightTrendDetailCsvBuilder")
    private CsvBuilderService streetLightTrendDetailCsvBuilder;

    @RequestMapping(value = UrlPaths.STREETLIGHT_TREND_DETAIL_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute StreetLightTrendDetailForm reportModel) {

        fillFilter(model);
        return "reports/flowBusinessIntelligence/streetLightTrendDetail/StreetLightTrendDetailForm";
    }

    @RequestMapping(value = UrlPaths.STREETLIGHT_TREND_DETAIL_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid StreetLightTrendDetailForm reportModel, BindingResult result) throws TechnicalException, ApplicationException, ParseException {

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillFilter(model.asMap());
            return "reports/flowBusinessIntelligence/streetLightTrendDetail/StreetLightTrendDetailForm";
            
        }

        log.debug("reportBuild {}", result);
        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());
        
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        String strStartDate = format.format(reportModel.getStartDate());
        String strEndDate = format.format(reportModel.getEndDate());
        
        String startMonth1 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(strEndDate, 1 , -1 );
        String endMonth1 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(strEndDate, 2 , -1 );
        String startMonth2 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(strEndDate, 1 , -2 );
        String endMonth2 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(strEndDate, 2 , -2 );

        model.addAttribute("startDate", strStartDate);
        model.addAttribute("endDate", strEndDate);
        model.addAttribute("startMonth1", startMonth1);
        model.addAttribute("endMonth1", endMonth1);
        model.addAttribute("startMonth2", startMonth2);
        model.addAttribute("endMonth2", endMonth2);
        
        model.addAttribute("light", reportModel.getLight());
        model.addAttribute("filterBy", reportModel.getFilterBy());
        model.addAttribute("filterValues", reportModel.getFilterValues());
        
        
        Map<String, Object> queryData = fillQueryData(strStartDate, strEndDate, reportModel.getLight(),
                reportModel.getFilterBy(), reportModel.getFilterValues(), startMonth1, endMonth1, startMonth2, endMonth2);

        Long totalRecords = iStreetLightTrendDetailDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);

        if (queryData != null) {
            queryData.clear();
        }
        queryData = null;

        return "reports/flowBusinessIntelligence/streetLightTrendDetail/streetLightTrendDetailResult";
    }

    @RequestMapping(method = RequestMethod.POST, value = UrlPaths.STREETLIGHT_TREND_DETAIL_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<StreetLightTrendDetailModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String light, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam String filterBy, @RequestParam String filterValues
             ) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. light={}. filterBy={}. filterValues={}.", light , filterBy, filterValues);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, light, filterBy, filterValues, null,null,null,null);
        List<StreetLightTrendDetailModel> list = iStreetLightTrendDetailDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);

        if (queryData != null) {
            queryData.clear();
        }
        queryData = null;

        return list;
    }

    private Map<String, Object> fillQueryData(String startDate, String endDate, String light, String filterBy, String filterValues,
            String startMonth1, String endMonth1, String startMonth2, String endMonth2) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put("light", light);
        queryData.put("filterBy", filterBy);
        queryData.put("filterValues", filterValues);
        
        queryData.put("startMonth1", startMonth1);
        queryData.put("endMonth1", endMonth1);
        queryData.put("startMonth2", startMonth2);
        queryData.put("endMonth2", endMonth2);
        
        return queryData;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 14;
    }


    private void fillFilter(Map model) {
        try {
            model.put("strAccessLevel", getAccessLevel());
            model.put("strRefId", getRefId());
            model.put("strDistChainType", getDistChainType());
        } catch (TechnicalException e) {
            log.error("Error setting merchant variables. Ex = {}", e);
        }
    }
    
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.STREETLIGHT_TREND_DETAIL_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String light, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam Integer totalRecords, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String filterBy, @RequestParam String filterValues
            ) throws IOException {

        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Listing grid. light={}. filterBy={}. filterValues={}.", light , filterBy, filterValues);
        
        String startMonth1 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(endDate, 1 , -1 );
        String endMonth1 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(endDate, 2 , -1 );
        String startMonth2 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(endDate, 1 , -2 );
        String endMonth2 = StreetLightTrendDetailQueryBuilder.getDateMonthBefore(endDate, 2 , -2 );


        Map<String, Object> queryData = fillQueryData(startDate, endDate, light, filterBy, filterValues, startMonth1,endMonth1,startMonth2,endMonth2);
        List<StreetLightTrendDetailModel> list = iStreetLightTrendDetailDao.list(1, totalRecords, sortField, sortOrder, queryData);
        
        log.debug("Total list={}", list.size());
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.debug("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());
        additionalData.put("startDate", startDate);
        additionalData.put("endDate", endDate);
        additionalData.put("startMonth1", startMonth1);
        additionalData.put("endMonth1", endMonth1);
        additionalData.put("startMonth2", startMonth2);
        additionalData.put("endMonth2", endMonth2);

        streetLightTrendDetailCsvBuilder.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.debug("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");
        return zipFileResource;

    }
    
    
    

}

