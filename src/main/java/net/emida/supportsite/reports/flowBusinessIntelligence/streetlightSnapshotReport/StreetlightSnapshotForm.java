
package net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport;

/**
 *
 * @author dgarzon
 */
public class StreetlightSnapshotForm {
    
    private String light;

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }
    
    

    @Override
    public String toString() {
        return "StreetlightSnapshotForm[" +
                "light=" + light +                
                ']';
    }
}
