
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail;

import com.debisys.languages.Languages;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author dgarzon
 */
@Service
public class StreetLightTrendDetailCsvBuilder  implements CsvBuilderService {

    private static final Logger log = LoggerFactory.getLogger(StreetLightTrendDetailCsvBuilder.class);

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {
            List<CsvRow> data = (List<CsvRow>) list;
            //File to fill
            writer = getFileWriter(filePath);

            log.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file
            
            String language = (String) additionalData.get("language");
            String startDate = (String) additionalData.get("startDate");
            String endDate = (String) additionalData.get("endDate");
            
            String startMonth1 = (String) additionalData.get("startMonth1");
            String endMonth1 = (String) additionalData.get("endMonth1");
            String startMonth2 = (String) additionalData.get("startMonth2");
            String endMonth2 = (String) additionalData.get("endMonth2");            

            writer.write("\"" + Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.title", language) + "\"\n");
            writer.write("\"" + Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.currentPeriod", language) + "\",\"" + startDate +" - "+endDate+ "\"\n");
            writer.write("\"" + Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.month1", language) + "\",\"" + startMonth1 +" - "+endMonth1+ "\"\n");
            writer.write("\"" + Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.month2", language) + "\",\"" + startMonth2 +" - "+endMonth2+ "\"\n\n");

            List<String> headers = generateHeaders(additionalData);
            String headerRow = CsvUtil.generateRow(headers, true);
            writer.write(headerRow);

            fillWriter(data, writer, additionalData);

        } catch (Exception ex) {
            log.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {

            if (additionalData != null) {
                additionalData.clear();
            }
            additionalData = null;

            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {

                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {
        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            writer.write(stringifyRow);
            cells.clear();
            cells = null;
        }
    }

    private List<String> generateHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");
        List<String> headers = new ArrayList<String>();
        headers.add("#");
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.agentName", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.subAgentName", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.repName", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.merchantName", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.merchantId", language));
        
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.currentPeriod", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.month1", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightTrendDetail.month2", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.lessDayOfBalance", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.1And3DayOfBalance", language));
        headers.add(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.more3DayOfBalance", language));

        return headers;
    }

}
