
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.dao;

import com.debisys.utils.NumberUtil;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendDetailMapper implements RowMapper<StreetLightTrendDetailModel>{

    @Override
    public StreetLightTrendDetailModel mapRow(ResultSet rs, int i) throws SQLException {
        StreetLightTrendDetailModel model = new StreetLightTrendDetailModel();
        
        model.setRwn(rs.getBigDecimal("rwn"));
        model.setAgentName(rs.getString("agenteName"));
        model.setSubAgentName(rs.getString("subAgenteName"));
        model.setRepName(rs.getString("repName"));
        model.setMerchantName(rs.getString("merchantName"));
        model.setMerchant_id(rs.getBigDecimal("currentMerchant"));
        
        
        model.setAvgCurrentMonth(NumberUtil.formatCurrency(rs.getString("avgCurrentMonth")));
        model.setAvgMonthLess1(NumberUtil.formatCurrency(rs.getString("avgMonthLess1")));
        model.setAvgMonthLess2(NumberUtil.formatCurrency(rs.getString("avgMonthLess2")));
        
        model.setRemainingDaysRed(rs.getInt("TotalA"));
        model.setRemainingDaysYellow(rs.getInt("TotalB"));
        model.setRemainingDaysGreen(rs.getInt("TotalC"));
        
        return model;
    }
    
}
