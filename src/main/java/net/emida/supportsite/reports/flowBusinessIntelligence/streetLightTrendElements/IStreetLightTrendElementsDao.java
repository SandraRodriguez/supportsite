
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements.dao.StreetLightTrendElementsModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author dgarzon
 */
public interface IStreetLightTrendElementsDao {
    public List<StreetLightTrendElementsModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;
    public Long count(Map<String, Object> queryData) throws TechnicalException;
}
