
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.dao.StreetLightTrendDetailModel;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author dgarzon
 */
public interface IStreetLightTrendDetailDao {
    public List<StreetLightTrendDetailModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;
    public Long count(Map<String, Object> queryData) throws TechnicalException;
}
