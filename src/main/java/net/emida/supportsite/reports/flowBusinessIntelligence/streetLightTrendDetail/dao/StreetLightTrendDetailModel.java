
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendDetailModel implements CsvRow{

    
    private BigDecimal rwn;
    private String agentName;
    private String subAgentName;
    private String repName;
    private String merchantName;
    private BigDecimal merchant_id;
    private String avgCurrentMonth;
    private String avgMonthLess1;
    private String avgMonthLess2;
    private int remainingDaysRed;
    private int remainingDaysYellow;
    private int remainingDaysGreen;
    

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getSubAgentName() {
        return subAgentName;
    }

    public void setSubAgentName(String subAgentName) {
        this.subAgentName = subAgentName;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public BigDecimal getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(BigDecimal merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getAvgCurrentMonth() {
        return avgCurrentMonth;
    }

    public void setAvgCurrentMonth(String avgCurrentMonth) {
        this.avgCurrentMonth = avgCurrentMonth;
    }

    public String getAvgMonthLess1() {
        return avgMonthLess1;
    }

    public void setAvgMonthLess1(String avgMonthLess1) {
        this.avgMonthLess1 = avgMonthLess1;
    }

    public String getAvgMonthLess2() {
        return avgMonthLess2;
    }

    public void setAvgMonthLess2(String avgMonthLess2) {
        this.avgMonthLess2 = avgMonthLess2;
    }

    public int getRemainingDaysRed() {
        return remainingDaysRed;
    }

    public void setRemainingDaysRed(int remainingDaysRed) {
        this.remainingDaysRed = remainingDaysRed;
    }

    public int getRemainingDaysYellow() {
        return remainingDaysYellow;
    }

    public void setRemainingDaysYellow(int remainingDaysYellow) {
        this.remainingDaysYellow = remainingDaysYellow;
    }

    public int getRemainingDaysGreen() {
        return remainingDaysGreen;
    }

    public void setRemainingDaysGreen(int remainingDaysGreen) {
        this.remainingDaysGreen = remainingDaysGreen;
    }

    
    
    
    @Override
    public List<CsvCell> cells() {
        
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cella = new CsvCell(String.valueOf(rwn));
        list.add(cella);
        
        list.add(new CsvCell(agentName));
        list.add(new CsvCell(subAgentName));
        list.add(new CsvCell(repName));
        list.add(new CsvCell(merchantName));
        list.add(new CsvCell(String.valueOf(merchant_id)));
        
        list.add(new CsvCell(String.valueOf(avgCurrentMonth)));
        list.add(new CsvCell(String.valueOf(avgMonthLess1)));
        list.add(new CsvCell(String.valueOf(avgMonthLess2)));
        
        list.add(new CsvCell(String.valueOf(remainingDaysRed)));
        list.add(new CsvCell(String.valueOf(remainingDaysYellow)));
        list.add(new CsvCell(String.valueOf(remainingDaysGreen)));
               
        return list;
    }

    public BigDecimal getRwn() {
        return rwn;
    }

    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }

    
}
