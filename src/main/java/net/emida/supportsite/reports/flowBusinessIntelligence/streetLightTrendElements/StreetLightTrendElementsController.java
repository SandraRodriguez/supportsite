
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements.dao.StreetLightTrendElementsModel;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dgarzon
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class StreetLightTrendElementsController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(StreetLightTrendElementsController.class);

    @Autowired
    private IStreetLightTrendElementsDao iStreetLightTrendElementsDao;
    
    @Autowired
    @Qualifier("streetLightTrendElementsCsvBuilder")
    private CsvBuilderService streetLightElementsCsvBuilder;

    @RequestMapping(value = UrlPaths.STREETLIGHT_TREND_ELEMENTS_PATH, method = RequestMethod.POST)
    public String reportForm(Map model, @ModelAttribute StreetLightTrendElementsForm reportModel) {

        fillFilter(model);
        return "reports/flowBusinessIntelligence/streetLightTrendElements/StreetLightTrendElementsForm";
    }

    @RequestMapping(value = UrlPaths.STREETLIGHT_TREND_ELEMENTS_PATH, method = RequestMethod.GET)
    public String reportBuild(Model model, @Valid StreetLightTrendElementsForm reportModel, BindingResult result) throws TechnicalException, ApplicationException, ParseException {

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillFilter(model.asMap());
            return "reports/flowBusinessIntelligence/streetLightTrendElements/StreetLightTrendElementsForm";
        }

        log.debug("reportBuild {}", result);
        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        model.addAttribute("startDate", reportModel.getStartDate());
        model.addAttribute("endDate", reportModel.getEndDate());
        model.addAttribute("light", reportModel.getLight());
        model.addAttribute("filterBy", reportModel.getFilterBy());
        model.addAttribute("filterValues", reportModel.getFilterValues());
        
        
        Map<String, Object> queryData = fillQueryData(reportModel.getStartDate(), reportModel.getEndDate(), reportModel.getLight(),reportModel.getFilterBy(), reportModel.getFilterValues());

        Long totalRecords = iStreetLightTrendElementsDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);

        if (queryData != null) {
            queryData.clear();
        }
        queryData = null;

        return "reports/flowBusinessIntelligence/streetLightTrend/streetLightTrendResult";
    }

    @RequestMapping(method = RequestMethod.POST, value = UrlPaths.STREETLIGHT_TREND_ELEMENTS_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<StreetLightTrendElementsModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String light, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam String filterBy, @RequestParam String filterValues) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. light={}. filterBy={}. filterValues={}.", light , filterBy, filterValues);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, light, filterBy, filterValues);
        List<StreetLightTrendElementsModel> list = iStreetLightTrendElementsDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);

        if (queryData != null) {
            queryData.clear();
        }
        queryData = null;

        return list;
    }

    private Map<String, Object> fillQueryData(String startDate, String endDate, String light, String filterBy, String filterValues) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put("light", light);
        queryData.put("filterBy", filterBy);
        queryData.put("filterValues", filterValues);
        
        return queryData;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 14;
    }


    private void fillFilter(Map model) {
        try {
            model.put("strAccessLevel", getAccessLevel());
            model.put("strRefId", getRefId());
            model.put("strDistChainType", getDistChainType());
        } catch (TechnicalException e) {
            log.error("Error setting merchant variables. Ex = {}", e);
        }
    }
    
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.STREETLIGHT_TREND_ELEMENTS_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String light, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam Integer totalRecords, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String filterBy, @RequestParam String filterValues) throws IOException {

        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Listing grid. light={}. filterBy={}. filterValues={}.", light , filterBy, filterValues);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, light, filterBy, filterValues);
        List<StreetLightTrendElementsModel> list = iStreetLightTrendElementsDao.list(1, totalRecords, sortField, sortOrder, queryData);
        
        log.debug("Total list={}", list.size());
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.debug("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());
        additionalData.put("startDate", startDate);
        additionalData.put("endDate", endDate);

        streetLightElementsCsvBuilder.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.debug("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");
        return zipFileResource;

    }

}

