
package net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author dgarzon
 */
public class StreetlightSnapshotDetailModel implements CsvRow{

    
    private BigDecimal rwn;
    //private String light;
    private String agentName;
    private String subAgentName;
    private String repName;
    private String merchantName;
    private BigDecimal merchant_id;

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getSubAgentName() {
        return subAgentName;
    }

    public void setSubAgentName(String subAgentName) {
        this.subAgentName = subAgentName;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public BigDecimal getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(BigDecimal merchant_id) {
        this.merchant_id = merchant_id;
    }

    /*public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }*/
    
    @Override
    public List<CsvCell> cells() {
        
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cella = new CsvCell(String.valueOf(rwn));
        list.add(cella);
        
        CsvCell cell0 = new CsvCell(agentName);
        list.add(cell0);
        CsvCell cell1 = new CsvCell(subAgentName);
        list.add(cell1);
        CsvCell cell2 = new CsvCell(repName);
        list.add(cell2);
        CsvCell cell3 = new CsvCell(merchantName);
        list.add(cell3);
        CsvCell cell4 = new CsvCell(String.valueOf(merchant_id));
        list.add(cell4);
        
               
        return list;
    }

    /**
     * @return the rwn
     */
    public BigDecimal getRwn() {
        return rwn;
    }

    /**
     * @param rwn the rwn to set
     */
    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }

    
}
