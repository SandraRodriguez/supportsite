
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements;

import java.util.Date;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendElementsForm {
    
    private String startDate;
    private String endDate;
    private String light;
    private String filterBy;
    private String filterValues;
    private String iso;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    
    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(String filterBy) {
        this.filterBy = filterBy;
    }

    public String getFilterValues() {
        return filterValues;
    }

    public void setFilterValues(String filterValues) {
        this.filterValues = filterValues;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }
    
    
    @Override
    public String toString() {
        return "StreetLightelementsForm[" +
                "light=" + light +
                ",startDate=" + startDate +
                ",endDate=" + endDate +
                ']';
    }
}
