
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.dao;

import java.util.List;
import java.util.Map;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.IStreetLightTrendDetailDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dgarzon
 */
@Repository
public class StreetLightTrendDetailJdbcDao implements IStreetLightTrendDetailDao{
    private static final Logger log = LoggerFactory.getLogger(StreetLightTrendDetailJdbcDao.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public Long countAll() {
        try {
            return 0L;
        } catch (DataAccessException ex) {
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<StreetLightTrendDetailModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {

        try {
            StreetLightTrendDetailQueryBuilder queryBuilder = new StreetLightTrendDetailQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.buildListQuery();
            log.debug("Query : {}", SQL_QUERY);
            if (queryData != null) {
                queryData.clear();
            }
            queryData = null;
            return jdbcTemplate.query(SQL_QUERY, new StreetLightTrendDetailMapper());

        } catch (DataAccessException ex) {
            log.error("Data access error in list StreetLightTrendDetail report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in list StreetLightTrendDetail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {
            StreetLightTrendDetailQueryBuilder queryBuilder = new StreetLightTrendDetailQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.buildCountQuery();
            log.debug("Query Count : {}", SQL_QUERY_COUNT);
            if (queryData != null) {
                queryData.clear();
            }
            queryData = null;
            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            log.error("Data access error in count StreetLightTrendDetail report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            log.error("Exception in count StreetLightTrendDetail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

}
