
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements.dao;

import com.debisys.reports.streetlightSnapshot.StreetlightSnapshotFactory;
import com.debisys.utils.TimeZone;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendElementsQueryBuilder {
    private static final Logger log = LoggerFactory.getLogger(StreetLightTrendElementsQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    private String light;
    private String iso;
    private String startDate;
    private String endDate;
    private String filterBy;
    private String filterValues;

    /**
     *
     * @param queryData
     */
    public StreetLightTrendElementsQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.light = (String) queryData.get("light");
        this.iso = (String) queryData.get("refId");
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.filterBy = (String) queryData.get("filterBy");
        this.filterValues = (String) queryData.get("filterValues");
    }

    public StreetLightTrendElementsQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.light = (String) queryData.get("light");
        this.iso = (String) queryData.get("refId");
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.filterBy = (String) queryData.get("filterBy");
        this.filterValues = (String) queryData.get("filterValues");
    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery(true));
        sb0.append(") AS tbl1 ");
        return sb0.toString();
    }


    public String mainQuery(boolean isCount) {
        
        StringBuilder mainQuery = new StringBuilder();
        try {
            String daysBefore30 = StreetlightSnapshotFactory.getDate30DaysBefore(endDate);
            Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates("1", iso, startDate, endDate + " 23:59:59.999", false);
            Vector vTZFilter30DaysBefore = TimeZone.getTimeZoneFilterDates("1", iso, daysBefore30, endDate + " 23:59:59.999", false);
            
            //###################  TEMP VALUES ########################
            /*filterBy = "iso";
            filterValues = "380654193158";
            iso = "380654193158";
            vTimeZoneFilterDates.set(0, "2017-03-21 00:00:00");
            vTimeZoneFilterDates.set(1, "2017-04-21 23:59:59");
            
            vTZFilter30DaysBefore.set(0, "2017-03-21 00:00:00");
            vTZFilter30DaysBefore.set(1, "2017-04-21 23:59:59");//*/
            //###################  END TEMP VALUES  ###################
            
            String whereLight = getWhereLight();
            String whereEntity = getWhereEntity();
            
            mainQuery.append(" SELECT ");
            mainQuery.append(" ROUND(((cw.merchant_CreditLimit - cw.merchant_runningliability)/sumAmount),1) as remaining_days, convert(varchar(19),cw.datetime, 121) AS datetime, b.merchant_id AS merchantId, ");
            mainQuery.append(" cw.agenteName, cw.subAgenteName, cw.repName, cw.merchantName, (cw.merchant_CreditLimit-merchant_runningLiability) as balance ");
            mainQuery.append("  FROM(  ");
            mainQuery.append("  SELECT  ");
            //mainQuery.append("  	(SUM(amount) / 30) as sumAmount, m.merchant_id, m.rep_id ");
            
            mainQuery.append("          (SELECT (sum(wt1.dailytotalamount) / 30) FROM aggdailytotalsalessummary wt1 WITH(NOLOCK) WHERE m.merchant_id = wt1.merchant_id ");
            mainQuery.append("          AND wt1.input_system >= '"+vTZFilter30DaysBefore.get(0)+"' and wt1.input_system <= '"+vTZFilter30DaysBefore.get(1)+"') AS sumAmount, ");
            mainQuery.append("          m.merchant_id, m.rep_id "); // ").append(numDays).append("
            
            mainQuery.append("  	FROM  merchants m  with(nolock)  ");
            mainQuery.append("  	LEFT JOIN  aggdailytotalsalessummary wt WITH(NOLOCK) ON (wt.merchant_id = m.merchant_id)  ");
            mainQuery.append("  	INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1) ");
            mainQuery.append("  	INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5) ");
            mainQuery.append("  	INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4) ");
            mainQuery.append("  	INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3)) ");
            mainQuery.append("  	WHERE ");
            mainQuery.append(whereEntity);
            mainQuery.append("  	((m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) ");
            mainQuery.append("  	AND (input_system >= '").append(vTimeZoneFilterDates.get(0)).append("' and input_system <= '").append(vTimeZoneFilterDates.get(1)).append("') ");
            mainQuery.append("  	GROUP BY m.merchant_id, m.liabilitylimit, m.runningliability, m.rep_id ");
            mainQuery.append("  	HAVING SUM(dailytotalamount) > 0 ");
            mainQuery.append("  ) as b  ");
            mainQuery.append("   ");
            mainQuery.append("  LEFT JOIN BalanceAll_CWC cw WITH(NOLOCK) ON b.merchant_id = cw.merchant ");
            mainQuery.append("  WHERE cw.datetime >= '").append(vTimeZoneFilterDates.get(0)).append("' and cw.datetime <= '").append(vTimeZoneFilterDates.get(1)).append("' ");
            mainQuery.append(whereLight);
            
            if(!isCount){
                mainQuery.append("  order by cw.merchantName,cw.datetime ASC ");
            }
            
        } catch (Exception ex) {
            log.error("Error StreetLightTrendElementsQueryBuilder ", ex);
        }
        return mainQuery.toString();
        

    }
    
    private String getWhereLight(){
        String whereLight = "";
        if(this.light.equalsIgnoreCase("RED") ){
            whereLight = " AND ROUND(((merchant_CreditLimit - merchant_runningliability)/sumAmount),1) < 1 ";
        }
        else if(this.light.equalsIgnoreCase("YELLOW") ){
            whereLight = " AND (ROUND(((merchant_CreditLimit - merchant_runningliability)/sumAmount),1) >= 1 AND ROUND(((merchant_CreditLimit - merchant_runningliability)/sumAmount),1) < 3 )";
        }
        else if(this.light.equalsIgnoreCase("GREEN") ){
            whereLight = " AND ROUND(((merchant_CreditLimit - merchant_runningliability)/sumAmount),1) >= 3 ";
        }
        return whereLight;
    }
    
    private String getWhereEntity(){        
        String whereFilter = "";
        if(filterBy.equalsIgnoreCase("merchants")){
            whereFilter = " m.merchant_id IN ("+filterValues+") AND ";
        }
        else if(filterBy.equalsIgnoreCase("reps")){
            whereFilter = " r.rep_id IN ("+filterValues+") AND ";
        }
        else if(filterBy.equalsIgnoreCase("subAgents")){
            whereFilter = " s.rep_id IN ("+filterValues+") AND ";
        }
        else if(filterBy.equalsIgnoreCase("agents")){
            whereFilter = " a.rep_id IN ("+filterValues+") AND ";
        }
        else{
            whereFilter = " i.rep_id = "+iso+" AND ";
        }
        return whereFilter;
    }

    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "merchantName";
            sortOrder = " ASC ";
        }

        StringBuilder sb0 = new StringBuilder();
        
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery(true));

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }
   
}
