/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author dgarzon
 */
public class StreetlightSnapshotMapper implements RowMapper<StreetlightSnapshotDetailModel>{

    @Override
    public StreetlightSnapshotDetailModel mapRow(ResultSet rs, int i) throws SQLException {
        StreetlightSnapshotDetailModel model = new StreetlightSnapshotDetailModel();
        
        model.setRwn(rs.getBigDecimal("rwn"));
        model.setAgentName(rs.getString("agentName"));
        model.setSubAgentName(rs.getString("subAgentName"));
        model.setRepName(rs.getString("repName"));
        model.setMerchantName(rs.getString("merchantName"));
        model.setMerchant_id(rs.getBigDecimal("merchant_id"));
        
        return model;
    }
    
}