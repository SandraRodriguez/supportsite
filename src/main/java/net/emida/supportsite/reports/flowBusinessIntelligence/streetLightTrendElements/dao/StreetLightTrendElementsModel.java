
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendElementsModel implements CsvRow{

    
    private BigDecimal rwn;
    private double remainingDays;
    private String dateTime;
    private String currentMerchantBalance;
    private String agentName;
    private String subAgentName;
    private String repName;
    private String merchantName;
    private BigDecimal merchant_id;

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getSubAgentName() {
        return subAgentName;
    }

    public void setSubAgentName(String subAgentName) {
        this.subAgentName = subAgentName;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public BigDecimal getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(BigDecimal merchant_id) {
        this.merchant_id = merchant_id;
    }

    public double getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(double remainingDays) {
        this.remainingDays = remainingDays;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getCurrentMerchantBalance() {
        return currentMerchantBalance;
    }

    public void setCurrentMerchantBalance(String currentMerchantBalance) {
        this.currentMerchantBalance = currentMerchantBalance;
    }

    
    
    @Override
    public List<CsvCell> cells() {
        
        List<CsvCell> list = new ArrayList<CsvCell>();

        CsvCell cella = new CsvCell(String.valueOf(rwn));
        list.add(cella);
        
        CsvCell cell0 = new CsvCell(agentName);
        list.add(cell0);
        CsvCell cell1 = new CsvCell(subAgentName);
        list.add(cell1);
        CsvCell cell2 = new CsvCell(repName);
        list.add(cell2);
        CsvCell cell3 = new CsvCell(merchantName);
        list.add(cell3);
        CsvCell cell4 = new CsvCell(String.valueOf(merchant_id));
        list.add(cell4);
        
        
        list.add( new CsvCell(String.valueOf(currentMerchantBalance)));
        list.add(new CsvCell(String.valueOf(dateTime)));
        list.add(new CsvCell(String.valueOf(remainingDays)));
        
               
        return list;
    }

    /**
     * @return the rwn
     */
    public BigDecimal getRwn() {
        return rwn;
    }

    /**
     * @param rwn the rwn to set
     */
    public void setRwn(BigDecimal rwn) {
        this.rwn = rwn;
    }

    
}
