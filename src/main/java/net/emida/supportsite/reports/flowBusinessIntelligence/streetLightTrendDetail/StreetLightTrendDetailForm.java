
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendDetailForm {
    
    private Date startDate;
    private Date endDate;
    private String light;
    private String filterBy;
    private String filterValues;
    private String iso;
    
    private String startMonth1;
    private String endMonth1;
    private String startMonth2;
    private String endMonth2;

    
    @NotNull(message = "{com.debisys.reports.error1}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @NotNull(message = "{com.debisys.reports.error3}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    
    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getFilterBy() {
        return filterBy;
    }

    public void setFilterBy(String filterBy) {
        this.filterBy = filterBy;
    }

    public String getFilterValues() {
        return filterValues;
    }

    public void setFilterValues(String filterValues) {
        this.filterValues = filterValues;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getStartMonth1() {
        return startMonth1;
    }

    public void setStartMonth1(String startMonth1) {
        this.startMonth1 = startMonth1;
    }

    public String getEndMonth1() {
        return endMonth1;
    }

    public void setEndMonth1(String endMonth1) {
        this.endMonth1 = endMonth1;
    }

    public String getStartMonth2() {
        return startMonth2;
    }

    public void setStartMonth2(String startMonth2) {
        this.startMonth2 = startMonth2;
    }

    public String getEndMonth2() {
        return endMonth2;
    }

    public void setEndMonth2(String endMonth2) {
        this.endMonth2 = endMonth2;
    }
        
    @Override
    public String toString() {
        return "StreetlightSnapshotForm[" +
                "light=" + light +
                ",startDate=" + startDate +
                ",endDate=" + endDate +
                ']';
    }
}
