package net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport.dao.StreetlightSnapshotDetailModel;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dgarzon
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class StreetlightSnapshotController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(StreetlightSnapshotController.class);

    @Autowired
    private IStreetlightSnapshotDao iStreetlightSnapshotDao;
    
    @Autowired
    @Qualifier("streetlightSnapshotCsvBuilder")
    private CsvBuilderService streetlightSnapshotCsvBuilder;

    @RequestMapping(value = UrlPaths.STREETLIGHT_SNAPSHOT_REPORT_PATH, method = RequestMethod.POST)    
    public String reportForm(Map model, @ModelAttribute StreetlightSnapshotForm reportModel) {

        fillBalanceHistory(model);
        return "reports/flowBusinessIntelligence/streetlightSnapshotReport/streetlightSnapshotForm";        
    }

    @RequestMapping(value = UrlPaths.STREETLIGHT_SNAPSHOT_REPORT_PATH, method = RequestMethod.GET)
    public String reportBuild(Model model, @Valid StreetlightSnapshotForm reportModel, BindingResult result) throws TechnicalException, ApplicationException {

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            fillBalanceHistory(model.asMap());
            return "reports/flowBusinessIntelligence/streetlightSnapshotReport/streetlightSnapshotForm";
        }

        log.debug("reportBuild {}", result);
        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());


        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Calendar calendar = new GregorianCalendar();
        String formatEndDate = sdf.format(calendar.getTime());
        calendar.set(Calendar.DATE, (calendar.get(Calendar.DATE) - 30));
        String formatStartDate = sdf.format(calendar.getTime());

        model.addAttribute("startDate", formatEndDate);
        model.addAttribute("endDate", formatStartDate);

        model.addAttribute("light", reportModel.getLight());
        Map<String, Object> queryData = fillQueryData(formatEndDate, formatStartDate, reportModel.getLight());

        Long totalRecords = iStreetlightSnapshotDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);        

        if (queryData != null) {
            queryData.clear();
        }
        queryData = null;

        return "reports/flowBusinessIntelligence/streetlightSnapshotReport/streetlightSnapshotResult";
    }

    @RequestMapping(method = RequestMethod.POST, value = UrlPaths.STREETLIGHT_SNAPSHOT_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<StreetlightSnapshotDetailModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String light, @RequestParam String startDate, @RequestParam String endDate) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. light={}.", light);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, light);
        List<StreetlightSnapshotDetailModel> list = iStreetlightSnapshotDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);

        if (queryData != null) {
            queryData.clear();
        }
        queryData = null;

        return list;
    }

    private Map<String, Object> fillQueryData(String startDate, String endDate, String light) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put("light", light);
        return queryData;
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 14;
    }

    private void fillBalanceHistory(Map model) {
        fillMerchantsFilter(model);
    }

    private void fillMerchantsFilter(Map model) {
        try {
            model.put("strAccessLevel", getAccessLevel());
            model.put("strRefId", getRefId());
            model.put("strDistChainType", getDistChainType());
        } catch (TechnicalException e) {
            log.error("Error setting merchant variables. Ex = {}", e);
        }
    }
    
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.STREETLIGHT_SNAPSHOT_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String light, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam Integer totalRecords, @RequestParam String sortField, @RequestParam String sortOrder) throws IOException {

        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file. Light={}", light);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, light);
        List<StreetlightSnapshotDetailModel> list = iStreetlightSnapshotDao.list(1, totalRecords, sortField, sortOrder, queryData);
        
        log.debug("Total list={}", list.size());
        
        if(queryData != null){
            queryData.clear();
        }
        queryData = null;

        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.debug("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());
        additionalData.put("startDate", startDate);
        additionalData.put("endDate", endDate);

        streetlightSnapshotCsvBuilder.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.debug("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();
        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");
        return zipFileResource;

    }

}
