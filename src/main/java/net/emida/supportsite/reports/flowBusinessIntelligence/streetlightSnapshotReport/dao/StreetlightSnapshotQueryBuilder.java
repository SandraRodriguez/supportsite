
package net.emida.supportsite.reports.flowBusinessIntelligence.streetlightSnapshotReport.dao;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dgarzon
 */
public class StreetlightSnapshotQueryBuilder {
    private static final Logger log = LoggerFactory.getLogger(StreetlightSnapshotQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    private String light;
    private String iso;

    /**
     *
     * @param queryData
     */
    public StreetlightSnapshotQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.light = (String) queryData.get("light");
        this.iso = (String) queryData.get("refId");
    }

    public StreetlightSnapshotQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.light = (String) queryData.get("light");
        this.iso = (String) queryData.get("refId");
    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery());
        sb0.append(") AS tbl1 ");
        return sb0.toString();
    }


    public String mainQuery() {
        
        //###################  TEMP VALUES ########################
        //iso = "380654193158";
        
        StringBuilder mainQuery = new StringBuilder();
        mainQuery.append("select ");
        mainQuery.append(" remaining_days, agentName, subAgentName, repName, merchantName, merchant_id ");
        mainQuery.append("from( ");
        mainQuery.append("select ");
        mainQuery.append("ROUND( (m.liabilitylimit - m.runningliability) / (sum(amount) / 30),1) AS remaining_days, a.businessname AS agentName, s.businessname AS subAgentName, r.businessname AS repName, ");
        mainQuery.append("m.dba as merchantName, m.merchant_id ");
        mainQuery.append("from web_transactions wt with(nolock) ");
        mainQuery.append("INNER JOIN merchants m WITH(NOLOCK) ON (m.merchant_id = wt.merchant_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) ");
        mainQuery.append("INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1) ");
        mainQuery.append("INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5) ");
        mainQuery.append("INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4) ");
        mainQuery.append("INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3)) ");
        mainQuery.append("WHERE i.rep_id = ").append(iso);
        
        //mainQuery.append("AND datetime >= '2015-03-21 00:00:00' and datetime <= '2017-04-21 23:59:59' ");
        mainQuery.append(" AND wt.DATETIME >= DATEADD(d, -30, dbo.GetLocalTimeByAccessLevel(1, "+iso+", GETDATE(), 0)) ");
	mainQuery.append(" AND wt.DATETIME < DATEADD(d, 1, dbo.GetLocalTimeByAccessLevel(1, "+iso+", GETDATE(), 0)) ");
        
        mainQuery.append("GROUP BY m.merchant_id, m.liabilitylimit, m.runningliability, a.businessname, s.businessname, r.businessname, m.dba ");
        mainQuery.append("HAVING sum(amount) > 0 ");
        mainQuery.append(") as b ");
        
        if(light.equalsIgnoreCase("red")){
            mainQuery.append(" WHERE remaining_days < 1");
        }
        else if(light.equalsIgnoreCase("yellow")){
            mainQuery.append(" WHERE remaining_days >= 1 AND remaining_days < 3");
        }
        else if(light.equalsIgnoreCase("green")){
            mainQuery.append(" WHERE remaining_days >= 3");
        }

        return mainQuery.toString();

    }

    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "merchantName";
            sortOrder = " ASC ";
        }

        StringBuilder sb0 = new StringBuilder();
        
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }
   
}
