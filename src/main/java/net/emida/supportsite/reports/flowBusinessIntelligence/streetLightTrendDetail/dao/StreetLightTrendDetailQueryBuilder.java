
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendDetail.dao;

import com.debisys.utils.TimeZone;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendDetailQueryBuilder {
    private static final Logger log = LoggerFactory.getLogger(StreetLightTrendDetailQueryBuilder.class);

    private int startPage;
    private int endPage;
    private String sortFieldName;
    private String sortOrder;
    private String iso;
    private String startDate;
    private String endDate;
    private String filterBy;
    private String filterValues;

    /**
     *
     * @param queryData
     */
    public StreetLightTrendDetailQueryBuilder(Map<String, Object> queryData) {
        log.debug("Query Map = {}", queryData);
        this.iso = (String) queryData.get("refId");
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.filterBy = (String) queryData.get("filterBy");
        this.filterValues = (String) queryData.get("filterValues");

    }

    public StreetLightTrendDetailQueryBuilder(int startPage, int endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = " " + sortFieldName + " ";
        this.sortOrder = " " + sortOrder + " ";
        this.iso = (String) queryData.get("refId");
        this.startDate = (String) queryData.get("startDate");
        this.endDate = (String) queryData.get("endDate");
        this.filterBy = (String) queryData.get("filterBy");
        this.filterValues = (String) queryData.get("filterValues");

    }

    public String buildCountQuery() throws Exception {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM (");
        sb0.append(mainQuery());
        sb0.append(") AS tbl1 ");
        return sb0.toString();
    }


    public String mainQuery() {
        
        StringBuilder mainQuery = new StringBuilder();
        try {
                        
            String nextDay = addDays(endDate, 1);
            long numDays = getNumDays(startDate, nextDay);
            if(numDays == 0){
                numDays = 1;
            }
            
            log.info("Total Days ["+numDays+"]");
            
            String startMonth1 = getDateMonthBefore(endDate, 1 , -1 );
            String endMonth1 = getDateMonthBefore(endDate, 2 , -1 );
            String startMonth2 = getDateMonthBefore(endDate, 1 , -2 );
            String endMonth2 = getDateMonthBefore(endDate, 2 , -2 );
            
            int quantityDaysMonth1 = getQuantityDaysOfMonth(startMonth1);
            int quantityDaysMonth2 = getQuantityDaysOfMonth(startMonth2);
                    
            Vector vTimeZoneDates = TimeZone.getTimeZoneFilterDates("1", iso, startDate, endDate + " 23:59:59.999", false);
            Vector vTimeZoneDateMonth1 = TimeZone.getTimeZoneFilterDates("1", iso, startMonth1, endMonth1 + " 23:59:59.999", false);
            Vector vTimeZoneDateMonth2 = TimeZone.getTimeZoneFilterDates("1", iso, startMonth2, endMonth2 + " 23:59:59.999", false);

            //###################  TEMP VALUES ########################
            /*filterBy = "iso";
            filterValues = "380654193158";
            iso = "380654193158";
            vTimeZoneDates.set(0, "2017-03-21 00:00:00");
            vTimeZoneDates.set(1, "2017-04-21 23:59:59"); //*/
            
            //###################  END TEMP VALUES  ###################
            
            String whereEntity = getWhereEntity();
            
            
            mainQuery.append(" SELECT SUM(a) TotalA, SUM(b) TotalB, SUM(c) TotalC, rta.agenteName, rta.subAgenteName, rta.repName, rta.merchantName, rta.currentMerchant, ");
            mainQuery.append(" 	rta.avgBalance AS avgCurrentMonth, rta.month1 AS avgMonthLess1, rta.month2 AS avgMonthLess2 ");
            mainQuery.append(" 	FROM( ");
            mainQuery.append(" 	SELECT ");
            mainQuery.append(" 		SUM(CASE WHEN rta0.remaining_days < 1 THEN 1 ELSE 0 END) as a, ");
            mainQuery.append(" 		SUM(CASE WHEN rta0.remaining_days >= 1 AND rta0.remaining_days < 3 THEN 1 ELSE 0 END) AS b, ");
            mainQuery.append(" 		SUM (CASE WHEN rta0.remaining_days >= 3 THEN 1 ELSE 0 END) AS c, ");
            mainQuery.append(" 		cMerchant AS currentMerchant, rta0.agenteName, rta0.subAgenteName, rta0.repName, rta0.merchantName, ");
            mainQuery.append(" 		rta0.avgBalance, rta0.month1, rta0.month2 ");
            mainQuery.append(" 	FROM( ");
            mainQuery.append(" 		SELECT ");
            mainQuery.append(" 			ROUND(((cw.merchant_CreditLimit - cw.merchant_runningliability)/b.avgBalance),1) as remaining_days, cw.datetime, cw.agenteName, cw.subAgenteName, cw.repName, ");
            mainQuery.append(" 			cw.merchantName, cw.merchant AS cMerchant, b.avgBalance, b.month1, b.month2 ");
            mainQuery.append(" 		FROM( ");
            mainQuery.append(" 		SELECT ");
            mainQuery.append(" 			(sum(wt.amount) / ").append(numDays).append(") as avgBalance, m.merchant_id, ");
            mainQuery.append(" 			(SELECT (sum(wt1.amount) / ").append(quantityDaysMonth1).append(") FROM web_transactions wt1 WITH(NOLOCK) WHERE m.merchant_id = wt1.merchant_id ");
            mainQuery.append(" 				AND wt1.datetime >= '").append(vTimeZoneDateMonth1.get(0)).append("' and wt1.datetime <= '").append(vTimeZoneDateMonth1.get(1)).append("') AS month1, ");
            
            mainQuery.append(" 			(SELECT (sum(wt2.amount) / ").append(quantityDaysMonth2).append(") FROM web_transactions wt2 WITH(NOLOCK) WHERE m.merchant_id = wt2.merchant_id ");
            mainQuery.append(" 				AND wt2.datetime >= '").append(vTimeZoneDateMonth2.get(0)).append("' and wt2.datetime <= '").append(vTimeZoneDateMonth2.get(1)).append("') AS month2 ");
            mainQuery.append(" 			FROM web_transactions wt WITH(NOLOCK) ");
            mainQuery.append(" 			INNER JOIN merchants m WITH(NOLOCK) ON (m.merchant_id = wt.merchant_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) ");
            mainQuery.append(" 			INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1) ");
            mainQuery.append(" 			INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5) ");
            mainQuery.append(" 			INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4) ");
            mainQuery.append(" 			INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3)) ");
            mainQuery.append(" 			WHERE ");
            mainQuery.append(whereEntity);
            mainQuery.append(" 			wt.datetime >= '").append(vTimeZoneDates.get(0)).append("' and wt.datetime <= '").append(vTimeZoneDates.get(1)).append("' ");
            mainQuery.append(" 			GROUP BY m.merchant_id, m.liabilitylimit, m.runningliability ");
            mainQuery.append(" 			HAVING sum(amount) > 0 ");
            mainQuery.append(" 		) as b ");
            mainQuery.append(" 		INNER JOIN BalanceAll_CWC cw WITH(NOLOCK) ON b.merchant_id = cw.merchant ");
            mainQuery.append(" 		WHERE cw.datetime >= '").append(vTimeZoneDates.get(0)).append("' and cw.datetime <= '").append(vTimeZoneDates.get(1)).append("' ");
            mainQuery.append(" 	) rta0 ");
            mainQuery.append(" 	group by remaining_days, cMerchant,  rta0.agenteName, rta0.subAgenteName, rta0.repName, rta0.merchantName, avgBalance, rta0.month1, rta0.month2 ");
            mainQuery.append(" ) rta ");
            mainQuery.append(" GROUP BY currentMerchant, avgBalance, rta.agenteName, rta.subAgenteName, rta.repName, rta.merchantName, rta.month1, rta.month2 ");
            
        } catch (Exception ex) {
            log.error("Error StreetLightTrendElementsQueryBuilder ", ex);
        }
        return mainQuery.toString();
    }
    
    private String getWhereEntity(){        
        String whereFilter = "";
        if(filterBy.equalsIgnoreCase("merchants")){
            whereFilter = " m.merchant_id IN ("+filterValues+") AND ";
        }
        else if(filterBy.equalsIgnoreCase("reps")){
            whereFilter = " r.rep_id IN ("+filterValues+") AND ";
        }
        else if(filterBy.equalsIgnoreCase("subAgents")){
            whereFilter = " s.rep_id IN ("+filterValues+") AND ";
        }
        else if(filterBy.equalsIgnoreCase("agents")){
            whereFilter = " a.rep_id IN ("+filterValues+") AND ";
        }
        else{
            whereFilter = " i.rep_id = "+iso+" AND ";
        }
        return whereFilter;
    }

    public String buildFilters() {
        return "s";
    }

    public String buildPagination() {
        return "s";
    }

    public String buildListQuery() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "merchantName";
            sortOrder = " ASC ";
        }

        StringBuilder sb0 = new StringBuilder();
        
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(sortFieldName + sortOrder).append(") AS RWN, * FROM (");

        sb0.append(mainQuery());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }
    
    private static int getQuantityDaysOfMonth(String currentDate){
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = myFormat.parse(currentDate);            
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
            return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        } catch (ParseException e) {
            log.error("Error getQuantityDaysOfMonth ", e);
        }
        return 30;
    }
    
    private static long getNumDays(String startDate, String endDate){
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = myFormat.parse(startDate);
            Date date2 = myFormat.parse(endDate);
            long diff = date2.getTime() - date1.getTime();
            return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            log.error("Error getNumDays ", e);
        }
        return -1;
    }
    
    public static String getDateMonthBefore(String currentDate, int statusDate, int monthsBefore){
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = myFormat.parse(currentDate);            
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
           
            calendar.add(Calendar.MONTH, monthsBefore);
            if(statusDate == 1){// Get startDate
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            }
            else if(statusDate == 2){// get end date
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            }
            //myFormat.applyPattern("yyyy-MM-dd");
            return myFormat.format(calendar.getTime());
        } catch (ParseException e) {
            log.error("Error getDateMonthBefore ", e);
        }
        return currentDate;
    }

    private String addDays(String currentDate, int i) {
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = myFormat.parse(currentDate);            
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
           
            calendar.add(Calendar.DATE, i);
            return myFormat.format(calendar.getTime());
        } catch (ParseException e) {
            log.error("Error getDateMonthBefore ", e);
        }
        return currentDate;
    }
    
   
}
