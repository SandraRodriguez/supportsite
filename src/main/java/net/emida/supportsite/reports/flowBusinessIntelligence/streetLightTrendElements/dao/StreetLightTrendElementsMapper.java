
package net.emida.supportsite.reports.flowBusinessIntelligence.streetLightTrendElements.dao;

import com.debisys.utils.NumberUtil;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendElementsMapper implements RowMapper<StreetLightTrendElementsModel>{

    @Override
    public StreetLightTrendElementsModel mapRow(ResultSet rs, int i) throws SQLException {
        StreetLightTrendElementsModel model = new StreetLightTrendElementsModel();
        
        model.setRwn(rs.getBigDecimal("rwn"));
        model.setAgentName(rs.getString("agenteName"));
        model.setSubAgentName(rs.getString("subAgenteName"));
        model.setRepName(rs.getString("repName"));
        model.setMerchantName(rs.getString("merchantName"));
        model.setMerchant_id(rs.getBigDecimal("merchantId"));
        
        model.setRemainingDays(rs.getDouble("remaining_days"));
        model.setCurrentMerchantBalance(NumberUtil.formatCurrency(rs.getString("balance")));
        model.setDateTime(rs.getString("datetime")); 
        
        return model;
    }
    
}
