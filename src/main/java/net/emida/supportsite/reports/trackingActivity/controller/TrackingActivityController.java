/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.controller;

import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalActivityLogDto;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalRegistrationDto;
import net.emida.supportsite.reports.trackingActivity.dto.TrackingTerminalRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import net.emida.supportsite.reports.trackingActivity.repository.ITrackingActivityDao;
import net.emida.supportsite.util.exceptions.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class TrackingActivityController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackingActivityController.class);

    @Autowired
    private ITrackingActivityDao trackingActivity;

    @RequestMapping(value = UrlPaths.TERMINAL_REGISTRATION, method = RequestMethod.GET)
    public String __initTerminalRegistration__() {
        return "reports/trackingActivity/terminalRegistration";
    }

    @RequestMapping(value = UrlPaths.LOGIN_ACTIVITY, method = RequestMethod.GET)
    public String __initLoginActivity__() {
        return "reports/trackingActivity/loginActivity";
    }

    @RequestMapping(value = UrlPaths.TERMINAL_REGISTRATION + "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<TerminalRegistrationDto> findTerminalRegistration(Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return trackingActivity.findAllTerminalRegistration();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.TERMINAL_REGISTRATION + "/allNotOk", method = RequestMethod.GET)
    @ResponseBody
    public List<TerminalRegistrationDto> notOkTerminalRegistration(Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return trackingActivity.notOkTerminalRegistration();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.TERMINAL_REGISTRATION + "/findOne", method = RequestMethod.GET)
    @ResponseBody
    public TrackingTerminalRegistrationDto findOneTerminalRegistration(@RequestParam("userId") String userId,
            @RequestParam("registrationCode") String registrationCode, @RequestParam("millenniumNo") long millenniumNo, Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return trackingActivity.findOneTerminalRegistration(millenniumNo, registrationCode);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.LOGIN_ACTIVITY + "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<TerminalActivityLogDto> findAllTerminalActivityLogs(Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return trackingActivity.findAllTerminalActivityLogs();
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @RequestMapping(value = UrlPaths.LOGIN_ACTIVITY + "/findOne", method = RequestMethod.GET)
    @ResponseBody
    public TerminalActivityLogDto findOneTerminalActivityLog(@RequestParam("recId") long recId, Model model, Locale locale,
            HttpServletRequest req, HttpSession httpSession) {
        try {
            return trackingActivity.findOneTerminalActivityLog(recId);
        } catch (TechnicalException err) {
            LOGGER.error(err.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 67;
    }

}
