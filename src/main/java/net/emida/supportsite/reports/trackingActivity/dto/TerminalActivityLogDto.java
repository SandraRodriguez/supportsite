/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
public class TerminalActivityLogDto implements Serializable {

    private long recId;
    private Date activityDate;
    private String customConfigId;
    private long merchantId;
    private long millenniumNo;
    private String registrationCode;
    private String userId;
    private String userIp;
    private String serverIp;
    private String description;
    private String errorCode;
    private String urlAddress;
    private String classs;
    private String funtion;
    private String errorMessage;
    private String clientUserAgent;
    private String clientVersion;
    private String serverVersion;

    public TerminalActivityLogDto() {
    }

    /**
     *
     * @param recId
     * @param activityDate
     * @param customConfigId
     * @param merchantId
     * @param millenniumNo
     * @param registrationCode
     * @param userId
     * @param userIp
     * @param serverIp
     * @param description
     * @param errorCode
     */
    public TerminalActivityLogDto(long recId, Date activityDate, String customConfigId, long merchantId, long millenniumNo, String registrationCode, String userId, String userIp, String serverIp, String description, String errorCode) {
        this.recId = recId;
        this.activityDate = activityDate;
        this.customConfigId = customConfigId;
        this.merchantId = merchantId;
        this.millenniumNo = millenniumNo;
        this.registrationCode = registrationCode;
        this.userId = userId;
        this.userIp = userIp;
        this.serverIp = serverIp;
        this.description = description;
        this.errorCode = errorCode;
    }

    /**
     *
     * @param recId
     * @param activityDate
     * @param customConfigId
     * @param merchantId
     * @param millenniumNo
     * @param registrationCode
     * @param userId
     * @param userIp
     * @param serverIp
     * @param description
     * @param errorCode
     * @param urlAddress
     * @param classs
     * @param funtion
     * @param errorMessage
     * @param clientUserAgent
     * @param clientVersion
     * @param serverVersion
     */
    public TerminalActivityLogDto(long recId, Date activityDate, String customConfigId, long merchantId, long millenniumNo, String registrationCode, String userId, String userIp, String serverIp, String description, String errorCode, String urlAddress, String classs, String funtion, String errorMessage, String clientUserAgent, String clientVersion, String serverVersion) {
        this.recId = recId;
        this.activityDate = activityDate;
        this.customConfigId = customConfigId;
        this.merchantId = merchantId;
        this.millenniumNo = millenniumNo;
        this.registrationCode = registrationCode;
        this.userId = userId;
        this.userIp = userIp;
        this.serverIp = serverIp;
        this.description = description;
        this.errorCode = errorCode;
        this.urlAddress = urlAddress;
        this.classs = classs;
        this.funtion = funtion;
        this.errorMessage = errorMessage;
        this.clientUserAgent = clientUserAgent;
        this.clientVersion = clientVersion;
        this.serverVersion = serverVersion;
    }

    public long getRecId() {
        return recId;
    }

    public void setRecId(long recId) {
        this.recId = recId;
    }

    public Date getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    public String getCustomConfigId() {
        return customConfigId;
    }

    public void setCustomConfigId(String customConfigId) {
        this.customConfigId = customConfigId;
    }

    public long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(long merchantId) {
        this.merchantId = merchantId;
    }

    public long getMillenniumNo() {
        return millenniumNo;
    }

    public void setMillenniumNo(long millenniumNo) {
        this.millenniumNo = millenniumNo;
    }

    public String getRegistrationCode() {
        return registrationCode;
    }

    public void setRegistrationCode(String registrationCode) {
        this.registrationCode = registrationCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getUrlAddress() {
        return urlAddress;
    }

    public void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    public String getClasss() {
        return classs;
    }

    public void setClasss(String classs) {
        this.classs = classs;
    }

    public String getFuntion() {
        return funtion;
    }

    public void setFuntion(String funtion) {
        this.funtion = funtion;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getClientUserAgent() {
        return clientUserAgent;
    }

    public void setClientUserAgent(String clientUserAgent) {
        this.clientUserAgent = clientUserAgent;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

}
