/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.repository;

import java.util.List;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalActivityLogDto;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalRegistrationDto;
import net.emida.supportsite.reports.trackingActivity.dto.TrackingTerminalRegistrationDto;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
public interface ITrackingActivityDao {

    /**
     *
     * @return
     */
    public List<TerminalRegistrationDto> findAllTerminalRegistration();

    /**
     *
     * @return
     */
    public List<TerminalRegistrationDto> notOkTerminalRegistration();

    /**
     *
     * @param millenniumNo
     * @param registrationCode
     * @return
     */
    public TrackingTerminalRegistrationDto findOneTerminalRegistration(long millenniumNo, String registrationCode);

    /**
     *
     * @return
     */
    public List<TerminalActivityLogDto> findAllTerminalActivityLogs();

    /**
     *
     * @param recId
     * @return
     */
    public TerminalActivityLogDto findOneTerminalActivityLog(long recId);
}
