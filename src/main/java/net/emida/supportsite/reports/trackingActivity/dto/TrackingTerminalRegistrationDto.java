/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.dto;

import java.io.Serializable;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
public class TrackingTerminalRegistrationDto implements Serializable {

    private TerminalRegistrationDto terminalRegistrationDto;
    private TerminalTypesDto terminalTypesDto;
    private TerminalResponseTypeDto terminalResponseTypeDto;

    public TrackingTerminalRegistrationDto() {
    }

    /**
     *
     * @param terminalRegistrationDto
     * @param terminalTypesDto
     * @param terminalResponseTypeDto
     */
    public TrackingTerminalRegistrationDto(TerminalRegistrationDto terminalRegistrationDto, TerminalTypesDto terminalTypesDto, TerminalResponseTypeDto terminalResponseTypeDto) {
        this.terminalRegistrationDto = terminalRegistrationDto;
        this.terminalTypesDto = terminalTypesDto;
        this.terminalResponseTypeDto = terminalResponseTypeDto;
    }

    public TerminalRegistrationDto getTerminalRegistrationDto() {
        return terminalRegistrationDto;
    }

    public void setTerminalRegistrationDto(TerminalRegistrationDto terminalRegistrationDto) {
        this.terminalRegistrationDto = terminalRegistrationDto;
    }

    public TerminalTypesDto getTerminalTypesDto() {
        return terminalTypesDto;
    }

    public void setTerminalTypesDto(TerminalTypesDto terminalTypesDto) {
        this.terminalTypesDto = terminalTypesDto;
    }

    public TerminalResponseTypeDto getTerminalResponseTypeDto() {
        return terminalResponseTypeDto;
    }

    public void setTerminalResponseTypeDto(TerminalResponseTypeDto terminalResponseTypeDto) {
        this.terminalResponseTypeDto = terminalResponseTypeDto;
    }

}
