/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.dto;

import java.io.Serializable;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
public class TerminalTypesDto implements Serializable {

    private int terminal_type;
    private String terminal_type_description;

    public TerminalTypesDto() {
    }

    /**
     *
     * @param terminal_type
     * @param terminal_type_description
     */
    public TerminalTypesDto(int terminal_type, String terminal_type_description) {
        this.terminal_type = terminal_type;
        this.terminal_type_description = terminal_type_description;
    }

    public int getTerminal_type() {
        return terminal_type;
    }

    public void setTerminal_type(int terminal_type) {
        this.terminal_type = terminal_type;
    }

    public String getTerminal_type_description() {
        return terminal_type_description;
    }

    public void setTerminal_type_description(String terminal_type_description) {
        this.terminal_type_description = terminal_type_description;
    }

}
