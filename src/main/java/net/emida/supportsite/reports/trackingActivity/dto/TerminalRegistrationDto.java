/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
public class TerminalRegistrationDto implements Serializable {

    private long millennium_no;
    private int disabledInd;
    private int registrationInd;
    private Date registrationDate;
    private String registrationIp;
    private int registrationAttemptNum;
    private Date lastLoginAttemptDate;
    private Date lastLoginSuccessDate;
    private String lastLoginAttemptIpAddr;
    private String lastLoginSuccessIpAddr;
    private int lastLoginAttemptNum;
    private String userId;
    private String password;
    private int registrationIgnoreInd;
    private String registrationCode;
    private String restrictTolpAddr;
    private int ipAddrIgnoreInd;
    private String restrictToSubnetMask;
    private String customConfigId;
    private int certificateIgnoreInd;
    private String certificateStatusCode;
    private Date certificateStatusDate;
    private Date certificateRequestDate;
    private int certificateInstallAlowedInd;
    private int certificateInstallRemainNum;
    private int certificateInstallCompleteNum;
    private int products;

    public TerminalRegistrationDto() {
    }

    /**
     *
     * @param millennium_no
     * @param registrationInd
     * @param registrationDate
     * @param registrationIp
     * @param registrationAttemptNum
     * @param lastLoginAttemptNum
     * @param userId
     * @param registrationCode
     * @param registrationIgnoreInd
     * @param restrictTolpAddr
     * @param ipAddrIgnoreInd
     * @param customConfigId
     */
    public TerminalRegistrationDto(long millennium_no, int registrationInd, Date registrationDate, String registrationIp, int registrationAttemptNum, int lastLoginAttemptNum, String userId, String registrationCode, int registrationIgnoreInd, String restrictTolpAddr, int ipAddrIgnoreInd, String customConfigId) {
        this.millennium_no = millennium_no;
        this.registrationInd = registrationInd;
        this.registrationDate = registrationDate;
        this.registrationIp = registrationIp;
        this.registrationAttemptNum = registrationAttemptNum;
        this.lastLoginAttemptNum = lastLoginAttemptNum;
        this.userId = userId;
        this.registrationCode = registrationCode;
        this.registrationIgnoreInd = registrationIgnoreInd;
        this.restrictTolpAddr = restrictTolpAddr;
        this.ipAddrIgnoreInd = ipAddrIgnoreInd;
        this.customConfigId = customConfigId;
    }

    /**
     *
     * @param millennium_no
     * @param disabledInd
     * @param registrationInd
     * @param registrationDate
     * @param registrationIp
     * @param registrationAttemptNum
     * @param lastLoginAttemptDate
     * @param lastLoginSuccessDate
     * @param lastLoginAttemptIpAddr
     * @param lastLoginSuccessIpAddr
     * @param lastLoginAttemptNum
     * @param userId
     * @param password
     * @param registrationIgnoreInd
     * @param registrationCode
     * @param restrictTolpAddr
     * @param ipAddrIgnoreInd
     * @param restrictToSubnetMask
     * @param customConfigId
     * @param certificateIgnoreInd
     * @param certificateStatusCode
     * @param certificateStatusDate
     * @param certificateRequestDate
     * @param certificateInstallAlowedInd
     * @param certificateInstallRemainNum
     * @param certificateInstallCompleteNum
     * @param products
     */
    public TerminalRegistrationDto(long millennium_no, int disabledInd, int registrationInd, Date registrationDate, String registrationIp, int registrationAttemptNum, Date lastLoginAttemptDate, Date lastLoginSuccessDate, String lastLoginAttemptIpAddr, String lastLoginSuccessIpAddr, int lastLoginAttemptNum, String userId, String password, int registrationIgnoreInd, String registrationCode, String restrictTolpAddr, int ipAddrIgnoreInd, String restrictToSubnetMask, String customConfigId, int certificateIgnoreInd, String certificateStatusCode, Date certificateStatusDate, Date certificateRequestDate, int certificateInstallAlowedInd, int certificateInstallRemainNum, int certificateInstallCompleteNum, int products) {
        this.millennium_no = millennium_no;
        this.disabledInd = disabledInd;
        this.registrationInd = registrationInd;
        this.registrationDate = registrationDate;
        this.registrationIp = registrationIp;
        this.registrationAttemptNum = registrationAttemptNum;
        this.lastLoginAttemptDate = lastLoginAttemptDate;
        this.lastLoginSuccessDate = lastLoginSuccessDate;
        this.lastLoginAttemptIpAddr = lastLoginAttemptIpAddr;
        this.lastLoginSuccessIpAddr = lastLoginSuccessIpAddr;
        this.lastLoginAttemptNum = lastLoginAttemptNum;
        this.userId = userId;
        this.password = password;
        this.registrationIgnoreInd = registrationIgnoreInd;
        this.registrationCode = registrationCode;
        this.restrictTolpAddr = restrictTolpAddr;
        this.ipAddrIgnoreInd = ipAddrIgnoreInd;
        this.restrictToSubnetMask = restrictToSubnetMask;
        this.customConfigId = customConfigId;
        this.certificateIgnoreInd = certificateIgnoreInd;
        this.certificateStatusCode = certificateStatusCode;
        this.certificateStatusDate = certificateStatusDate;
        this.certificateRequestDate = certificateRequestDate;
        this.certificateInstallAlowedInd = certificateInstallAlowedInd;
        this.certificateInstallRemainNum = certificateInstallRemainNum;
        this.certificateInstallCompleteNum = certificateInstallCompleteNum;
        this.products = products;
    }

    public long getMillennium_no() {
        return millennium_no;
    }

    public void setMillennium_no(long millennium_no) {
        this.millennium_no = millennium_no;
    }

    public int getDisabledInd() {
        return disabledInd;
    }

    public void setDisabledInd(int disabledInd) {
        this.disabledInd = disabledInd;
    }

    public int getRegistrationInd() {
        return registrationInd;
    }

    public void setRegistrationInd(int registrationInd) {
        this.registrationInd = registrationInd;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getRegistrationIp() {
        return registrationIp;
    }

    public void setRegistrationIp(String registrationIp) {
        this.registrationIp = registrationIp;
    }

    public int getRegistrationAttemptNum() {
        return registrationAttemptNum;
    }

    public void setRegistrationAttemptNum(int registrationAttemptNum) {
        this.registrationAttemptNum = registrationAttemptNum;
    }

    public Date getLastLoginAttemptDate() {
        return lastLoginAttemptDate;
    }

    public void setLastLoginAttemptDate(Date lastLoginAttemptDate) {
        this.lastLoginAttemptDate = lastLoginAttemptDate;
    }

    public Date getLastLoginSuccessDate() {
        return lastLoginSuccessDate;
    }

    public void setLastLoginSuccessDate(Date lastLoginSuccessDate) {
        this.lastLoginSuccessDate = lastLoginSuccessDate;
    }

    public String getLastLoginAttemptIpAddr() {
        return lastLoginAttemptIpAddr;
    }

    public void setLastLoginAttemptIpAddr(String lastLoginAttemptIpAddr) {
        this.lastLoginAttemptIpAddr = lastLoginAttemptIpAddr;
    }

    public String getLastLoginSuccessIpAddr() {
        return lastLoginSuccessIpAddr;
    }

    public void setLastLoginSuccessIpAddr(String lastLoginSuccessIpAddr) {
        this.lastLoginSuccessIpAddr = lastLoginSuccessIpAddr;
    }

    public int getLastLoginAttemptNum() {
        return lastLoginAttemptNum;
    }

    public void setLastLoginAttemptNum(int lastLoginAttemptNum) {
        this.lastLoginAttemptNum = lastLoginAttemptNum;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRegistrationIgnoreInd() {
        return registrationIgnoreInd;
    }

    public void setRegistrationIgnoreInd(int registrationIgnoreInd) {
        this.registrationIgnoreInd = registrationIgnoreInd;
    }

    public String getRegistrationCode() {
        return registrationCode;
    }

    public void setRegistrationCode(String registrationCode) {
        this.registrationCode = registrationCode;
    }

    public String getRestrictTolpAddr() {
        return restrictTolpAddr;
    }

    public void setRestrictTolpAddr(String restrictTolpAddr) {
        this.restrictTolpAddr = restrictTolpAddr;
    }

    public int getIpAddrIgnoreInd() {
        return ipAddrIgnoreInd;
    }

    public void setIpAddrIgnoreInd(int ipAddrIgnoreInd) {
        this.ipAddrIgnoreInd = ipAddrIgnoreInd;
    }

    public String getRestrictToSubnetMask() {
        return restrictToSubnetMask;
    }

    public void setRestrictToSubnetMask(String restrictToSubnetMask) {
        this.restrictToSubnetMask = restrictToSubnetMask;
    }

    public String getCustomConfigId() {
        return customConfigId;
    }

    public void setCustomConfigId(String customConfigId) {
        this.customConfigId = customConfigId;
    }

    public int getCertificateIgnoreInd() {
        return certificateIgnoreInd;
    }

    public void setCertificateIgnoreInd(int certificateIgnoreInd) {
        this.certificateIgnoreInd = certificateIgnoreInd;
    }

    public String getCertificateStatusCode() {
        return certificateStatusCode;
    }

    public void setCertificateStatusCode(String certificateStatusCode) {
        this.certificateStatusCode = certificateStatusCode;
    }

    public Date getCertificateStatusDate() {
        return certificateStatusDate;
    }

    public void setCertificateStatusDate(Date certificateStatusDate) {
        this.certificateStatusDate = certificateStatusDate;
    }

    public Date getCertificateRequestDate() {
        return certificateRequestDate;
    }

    public void setCertificateRequestDate(Date certificateRequestDate) {
        this.certificateRequestDate = certificateRequestDate;
    }

    public int getCertificateInstallAlowedInd() {
        return certificateInstallAlowedInd;
    }

    public void setCertificateInstallAlowedInd(int certificateInstallAlowedInd) {
        this.certificateInstallAlowedInd = certificateInstallAlowedInd;
    }

    public int getCertificateInstallRemainNum() {
        return certificateInstallRemainNum;
    }

    public void setCertificateInstallRemainNum(int certificateInstallRemainNum) {
        this.certificateInstallRemainNum = certificateInstallRemainNum;
    }

    public int getCertificateInstallCompleteNum() {
        return certificateInstallCompleteNum;
    }

    public void setCertificateInstallCompleteNum(int certificateInstallCompleteNum) {
        this.certificateInstallCompleteNum = certificateInstallCompleteNum;
    }

    public int getProducts() {
        return products;
    }

    public void setProducts(int products) {
        this.products = products;
    }

}
