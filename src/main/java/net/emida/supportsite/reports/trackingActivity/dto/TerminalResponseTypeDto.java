/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.dto;

import java.io.Serializable;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
public class TerminalResponseTypeDto implements Serializable {

    private int terminalResponseType;
    private String terminalResponseDescription;

    public TerminalResponseTypeDto() {
    }

    /**
     *
     * @param terminalResponseType
     * @param terminalResponseDescription
     */
    public TerminalResponseTypeDto(int terminalResponseType, String terminalResponseDescription) {
        this.terminalResponseType = terminalResponseType;
        this.terminalResponseDescription = terminalResponseDescription;
    }

    public int getTerminalResponseType() {
        return terminalResponseType;
    }

    public void setTerminalResponseType(int terminalResponseType) {
        this.terminalResponseType = terminalResponseType;
    }

    public String getTerminalResponseDescription() {
        return terminalResponseDescription;
    }

    public void setTerminalResponseDescription(String terminalResponseDescription) {
        this.terminalResponseDescription = terminalResponseDescription;
    }

}
