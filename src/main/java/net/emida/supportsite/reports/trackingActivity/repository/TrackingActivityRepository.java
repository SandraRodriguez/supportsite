/* 
 * Copyright 2017 Emida, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not 
 * use this file except in compliance with the License. You may obtain a copy 
 * of the License at 
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0 
 *   
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT 
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the 
 * License for the specific language governing permissions and limitations 
 * under the License.
 * 
 */
package net.emida.supportsite.reports.trackingActivity.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.WeakHashMap;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalActivityLogDto;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalRegistrationDto;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalResponseTypeDto;
import net.emida.supportsite.reports.trackingActivity.dto.TerminalTypesDto;
import net.emida.supportsite.reports.trackingActivity.dto.TrackingTerminalRegistrationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author janez - janez@emida.net
 * @since 11/14/2017
 * @version 1.0
 * @see https://emidadev.atlassian.net/browse/DTU-3842
 */
@Repository("ITrackingActivityDao")
public class TrackingActivityRepository implements ITrackingActivityDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrackingActivityRepository.class);

    private static final String FIND_ALL_TERMINAL_REGISTRATION = "SELECT tr.millennium_no, tr.RegistrationInd, tr.RegistrationDate, tr.RegistrationIp, tr.RegistrationAttemptNum, tr.LastLoginAttemptNum, tr.UserId, tr.RegistrationCode, tr.RegistrationIgnoreInd, "
            + "tr.RestrictTolpAddr, tr.ipAddrIgnoreInd, tr.CustomConfigId "
            + "FROM Terminal_Registration tr WITH(nolock)";

    private static final String FIND_ALL_NOT_TERMINAL_REGISTRATION = "SELECT tr.millennium_no, tr.RegistrationInd, tr.RegistrationDate, tr.RegistrationIp, tr.RegistrationAttemptNum, tr.LastLoginAttemptNum, tr.UserId, tr.RegistrationCode, tr.RegistrationIgnoreInd, "
            + "tr.RestrictTolpAddr, tr.ipAddrIgnoreInd, tr.CustomConfigId "
            + "FROM Terminal_Registration tr WITH(nolock) "
            + "WHERE tr.RegistrationAttemptNum > 3 and tr.RegistrationInd = -1 ORDER BY tr.millennium_no";

    private static final String FIND_A_TERMINAL_REGISTRATION = "SELECT rg.id as 'id', "
            + "rg.millennium_no as 'millennium_no', "
            + "rg.DisabledInd as 'DisabledInd', "
            + "rg.RegistrationInd as 'RegistrationInd', "
            + "rg.RegistrationDate as 'RegistrationDate', "
            + "rg.RegistrationIp as 'RegistrationIp', "
            + "rg.RegistrationAttemptNum as 'RegistrationAttemptNum', "
            + "rg.LastLoginAttemptDate as 'LastLoginAttemptDate', "
            + "rg.LastLoginSuccessDate as 'LastLoginSuccessDate', "
            + "rg.LastLoginAttemptIpAddr as 'LastLoginAttemptIpAddr', "
            + "rg.LastLoginSuccessIpAddr as 'LastLoginSuccessIpAddr', "
            + "rg.LastLoginAttemptNum as 'LastLoginAttemptNum', "
            + "rg.UserId as 'UserId', "
            + "rg.Password as 'Password', "
            + "rg.RegistrationIgnoreInd as 'RegistrationIgnoreInd', "
            + "rg.RegistrationCode as 'RegistrationCode', "
            + "rg.ipAddrIgnoreInd as 'ipAddrIgnoreInd', "
            + "rg.RestrictTolpAddr as 'RestrictTolpAddr', "
            + "rg.RestrictToSubnetMask as 'RestrictToSubnetMask', "
            + "rg.CustomConfigId as 'CustomConfigId', "
            + "tt.terminal_type as 'terminal_type', "
            + "tt.description as 'terminal_type_description', "
            + "tr.type as 'terminal_response_type', "
            + "tr.description as 'terminal_response_description', "
            + "rg.CertificateIgnoreInd as 'CertificateIgnoreInd', "
            + "rg.CertificateStatusCode as 'CertificateStatusCode', "
            + "rg.CertificateStatusDate as 'CertificateStatusDate', "
            + "rg.CertificateRequestDate as 'CertificateRequestDate', "
            + "rg.CertificateInstallAlowedInd as 'CertificateInstallAlowedInd', "
            + "rg.CertificateInstallRemainNum as 'CertificateInstallRemainNum', "
            + "rg.CertificateInstallCompleteNum as 'CertificateInstallCompleteNum', "
            + "(SELECT count(*) as products FROM terminal_rates tr1 WHERE tr1.millennium_no = ? AND tr1.disabled = 0) as products "
            + "FROM terminal_registration rg, "
            + "terminals t, "
            + "terminal_response_type tr, "
            + "terminal_types tt "
            + "WHERE rg.millennium_no = ?"
            + "AND rg.RegistrationCode = ?"
            + "AND rg.millennium_no = t.millennium_no "
            + "and t.terminal_type = tt.terminal_type "
            + "and tt.response_type = tr.type";

    private static final String FIND_ALL_TERMINAL_ACTIVITY = "SELECT tal.rec_id, tal.activity_date, tal.CustomConfigId, tal.Merchant_id, tal.millennium_no, tal.RegistrationCode, tal.Userid, tal.user_ip, tal.server_ip, tal.description, tal.ErrorCode "
            + "FROM dbo.Terminal_Activity_Log tal WITH(NOLOCK) WHERE tal.Userid NOT IN ('esmas', 'liquidm1', 'nagios') AND tal.activity_date >= dateadd(dd, -3, GETDATE()) ORDER BY rec_id DESC";

    private static final String FIND_ONE_TERMINAL_ACTIVITY = "SELECT * FROM dbo.Terminal_Activity_Log tal WITH(NOLOCK) WHERE tal.rec_id = ?";

    @Autowired
    @Qualifier("masterJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    private WeakHashMap<Long, List<TerminalRegistrationDto>> weakHashTerminalRegistrarion = new WeakHashMap<Long, List<TerminalRegistrationDto>>();

    @Override
    public List<TerminalRegistrationDto> findAllTerminalRegistration() {
        if (weakHashTerminalRegistrarion.isEmpty()) {
            weakHashTerminalRegistrarion.put(1L, this.jdbcTemplate.query(FIND_ALL_TERMINAL_REGISTRATION, new Object[]{},
                    new RowMapper<TerminalRegistrationDto>() {
                /**
                 *
                 * @param resultSet
                 * @param index
                 * @return
                 * @throws SQLException
                 */
                @Override
                public TerminalRegistrationDto mapRow(ResultSet resultSet, int index) throws SQLException {
                    return new TerminalRegistrationDto(resultSet.getLong("millennium_no"), resultSet.getInt("RegistrationInd"),
                            resultSet.getDate("RegistrationDate"), resultSet.getString("RegistrationIp"), resultSet.getInt("RegistrationAttemptNum"),
                            resultSet.getInt("LastLoginAttemptNum"), resultSet.getString("UserId"),
                            resultSet.getString("RegistrationCode"), resultSet.getInt("RegistrationIgnoreInd"),
                            resultSet.getString("RestrictTolpAddr"), resultSet.getInt("ipAddrIgnoreInd"), resultSet.getString("CustomConfigId"));
                }
            }));
            return weakHashTerminalRegistrarion.get(1L);
        } else {
            return weakHashTerminalRegistrarion.get(1L);
        }
    }

    @Override
    public List<TerminalRegistrationDto> notOkTerminalRegistration() {
        return this.jdbcTemplate.query(FIND_ALL_NOT_TERMINAL_REGISTRATION, new Object[]{},
                new RowMapper<TerminalRegistrationDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public TerminalRegistrationDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new TerminalRegistrationDto(resultSet.getLong("millennium_no"), resultSet.getInt("RegistrationInd"),
                        resultSet.getDate("RegistrationDate"), resultSet.getString("RegistrationIp"), resultSet.getInt("RegistrationAttemptNum"),
                        resultSet.getInt("LastLoginAttemptNum"), resultSet.getString("UserId"),
                        resultSet.getString("RegistrationCode"), resultSet.getInt("RegistrationIgnoreInd"),
                        resultSet.getString("RestrictTolpAddr"), resultSet.getInt("ipAddrIgnoreInd"), resultSet.getString("CustomConfigId"));
            }
        });
    }

    /**
     *
     * @param millenniumNo
     * @param registrationCode
     * @return
     */
    @Override
    public TrackingTerminalRegistrationDto findOneTerminalRegistration(long millenniumNo, String registrationCode) {
        LOGGER.info(FIND_A_TERMINAL_REGISTRATION, "{}", millenniumNo, registrationCode);
        return this.jdbcTemplate.queryForObject(FIND_A_TERMINAL_REGISTRATION, new Object[]{
            millenniumNo,
            millenniumNo,
            registrationCode
        },
                new RowMapper<TrackingTerminalRegistrationDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public TrackingTerminalRegistrationDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new TrackingTerminalRegistrationDto(new TerminalRegistrationDto(resultSet.getLong("millennium_no"), resultSet.getInt("DisabledInd"),
                        resultSet.getInt("RegistrationInd"), resultSet.getDate("RegistrationDate"), resultSet.getString("RegistrationIp"), resultSet.getInt("RegistrationAttemptNum"),
                        resultSet.getDate("LastLoginAttemptDate"), resultSet.getDate("LastLoginSuccessDate"), resultSet.getString("LastLoginAttemptIpAddr"), resultSet.getString("LastLoginSuccessIpAddr"),
                        resultSet.getInt("LastLoginAttemptNum"), resultSet.getString("UserId"), resultSet.getString("Password"), resultSet.getInt("RegistrationIgnoreInd"), resultSet.getString("RegistrationCode"),
                        resultSet.getString("RestrictTolpAddr"), resultSet.getInt("ipAddrIgnoreInd"), resultSet.getString("RestrictToSubnetMask"), resultSet.getString("CustomConfigId"), resultSet.getInt("CertificateIgnoreInd"),
                        resultSet.getString("CertificateStatusCode"), resultSet.getDate("CertificateStatusDate"), resultSet.getDate("CertificateRequestDate"), resultSet.getInt("CertificateInstallAlowedInd"), resultSet.getInt("CertificateInstallRemainNum"), resultSet.getInt("CertificateInstallCompleteNum"), resultSet.getInt("products")),
                        new TerminalTypesDto(resultSet.getInt("terminal_type"), resultSet.getString("terminal_response_type")),
                        new TerminalResponseTypeDto(resultSet.getInt("terminal_response_type"), resultSet.getString("terminal_response_description")));
            }
        });
    }

    @Override
    public List<TerminalActivityLogDto> findAllTerminalActivityLogs() {
        return this.jdbcTemplate.query(FIND_ALL_TERMINAL_ACTIVITY, new Object[]{},
                new RowMapper<TerminalActivityLogDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public TerminalActivityLogDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new TerminalActivityLogDto(resultSet.getLong("rec_id"), resultSet.getDate("activity_date"), resultSet.getString("CustomConfigId"), resultSet.getLong("Merchant_id"),
                        resultSet.getLong("millennium_no"), resultSet.getString("RegistrationCode"), resultSet.getString("Userid"), resultSet.getString("user_ip"), resultSet.getString("server_ip"),
                        resultSet.getString("description"), resultSet.getString("ErrorCode"));
            }
        });
    }

    /**
     *
     * @param recId
     * @return
     */
    @Override
    public TerminalActivityLogDto findOneTerminalActivityLog(long recId) {
        return this.jdbcTemplate.queryForObject(FIND_ONE_TERMINAL_ACTIVITY, new Object[]{
            recId
        },
                new RowMapper<TerminalActivityLogDto>() {
            /**
             *
             * @param resultSet
             * @param index
             * @return
             * @throws SQLException
             */
            @Override
            public TerminalActivityLogDto mapRow(ResultSet resultSet, int index) throws SQLException {
                return new TerminalActivityLogDto(resultSet.getLong("rec_id"), resultSet.getDate("activity_date"), resultSet.getString("CustomConfigId"),
                        resultSet.getLong("Merchant_id"), resultSet.getLong("millennium_no"), resultSet.getString("RegistrationCode"), resultSet.getString("Userid"),
                        resultSet.getString("user_ip"), resultSet.getString("server_ip"),
                        resultSet.getString("description"), resultSet.getString("ErrorCode"), resultSet.getString("url_address"), resultSet.getString("ClassName"), resultSet.getString("FunctionName"),
                        resultSet.getString("ErrorMessage"), resultSet.getString("client_user_agent"), resultSet.getString("client_version"), resultSet.getString("server_version"));
            }
        });
    }

}
