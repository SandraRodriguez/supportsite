package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier;

import java.util.Map;
import java.util.List;
import net.emida.supportsite.reports.base.IDetailDao;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author fernandob
 * @param <T> Detail model class
 */
public interface ISpreadByProviderNCarrierReportDetailDao<T> extends IDetailDao<T> {

    List<Map<String, Object>> listMultisourceProviders(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

    List<Map<String, Object>> listMultisourceCarriers(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException;

}
