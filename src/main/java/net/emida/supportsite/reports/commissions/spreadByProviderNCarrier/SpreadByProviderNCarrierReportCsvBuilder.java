package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier;

import java.util.Map;
import java.util.List;
import java.io.Writer;
import org.slf4j.Logger;
import java.io.FileWriter;
import java.util.ArrayList;
import java.io.IOException;
import java.io.BufferedWriter;
import org.slf4j.LoggerFactory;
import com.debisys.languages.Languages;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvUtil;
import net.emida.supportsite.util.csv.CsvCell;
import org.springframework.stereotype.Service;
import net.emida.supportsite.util.csv.CsvBuilderService;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 * @author Fernando Briceno based on Franky Villadiego work
 */
@Service
public class SpreadByProviderNCarrierReportCsvBuilder implements CsvBuilderService {

    private static final Logger logger = LoggerFactory.getLogger(SpreadByProviderNCarrierReportCsvBuilder.class);

    @Override
    public void buildCsv(String filePath, List<? extends Object> list, Map<String, Object> additionalData) {
        Writer writer = null;
        try {

            List<CsvRow> data = (List<CsvRow>) list;
            //File to fill
            writer = getFileWriter(filePath);

            logger.debug("Creating temp file : {}", filePath);
            writer.flush(); //Create temp file

            List<String> headers = generateHeaders(additionalData);
            String headerRow = CsvUtil.generateRow(headers, true);
            writer.write(headerRow);

            fillWriter(data, writer, additionalData);

        } catch (Exception ex) {
            logger.error("Error building csv file : {}", filePath);
            throw new TechnicalException(ex.getMessage(), ex);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (Exception ex) {

                }
            }
        }
    }

    private Writer getFileWriter(String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        return bufferedWriter;
    }

    private void fillWriter(List<CsvRow> data, Writer writer, Map<String, Object> additionalData) throws IOException {
        for (CsvRow row : data) {
            List<CsvCell> cells = row.cells();
            String stringifyRow = CsvUtil.stringifyRow(cells, true);
            writer.write(stringifyRow);
        }
    }

    private List<String> generateHeaders(Map<String, Object> additionalData) {
        String language = (String) additionalData.get("language");
        List<String> headers = new ArrayList<String>();

        headers.add("#");
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.providerName", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.carrierName", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.trxCount", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.trxAmount", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.isoCommission", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.agentCommission", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.subagentCommission", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.repCommission", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.merchantCommission", language));
        headers.add(Languages.getString("jsp.admin.reports.commissionSpreadByProvCarReport.totalChannelCommission", language));
        return headers;
    }

}
