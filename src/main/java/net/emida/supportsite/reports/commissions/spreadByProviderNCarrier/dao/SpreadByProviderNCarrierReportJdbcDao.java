package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao;

import java.util.Map;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataAccessException;
import net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.ISpreadByProviderNCarrierReportDetailDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import net.emida.supportsite.util.exceptions.TechnicalException;

/**
 *
 * @author fernandob
 */
@Repository
public class SpreadByProviderNCarrierReportJdbcDao implements ISpreadByProviderNCarrierReportDetailDao<SpreadByProviderNCarrierReportModel> {

    private static final Logger logger = LoggerFactory.getLogger(SpreadByProviderNCarrierReportJdbcDao.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<SpreadByProviderNCarrierReportModel> list(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {
        try {
            SpreadByProviderNCarrierReportQueryBuilder queryBuilder = new SpreadByProviderNCarrierReportQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.generateSqlString();

            logger.debug("Query : {}", SQL_QUERY);

            return jdbcTemplate.query(SQL_QUERY, new SpreadByProviderNCarrierReportMapper());
        } catch (DataAccessException ex) {
            logger.error("Data access error in list spread by provider and carrier report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            logger.error("Exception in list spread by provider and carrier report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public Long count(Map<String, Object> queryData) throws TechnicalException {
        try {
            SpreadByProviderNCarrierReportQueryBuilder queryBuilder = new SpreadByProviderNCarrierReportQueryBuilder(queryData);
            String SQL_QUERY_COUNT = queryBuilder.generateRecordCountQuery();

            logger.debug("Query Count : {}", SQL_QUERY_COUNT);

            return jdbcTemplate.queryForObject(SQL_QUERY_COUNT, Long.class);
        } catch (DataAccessException ex) {
            logger.error("Data access error in count spread by provider and carrier report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            logger.error("Exception in count spiff detail report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<Map<String, Object>> listMultisourceProviders(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {
        try {
            SpreadByProviderNCarrierReportQueryBuilder queryBuilder = new SpreadByProviderNCarrierReportQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.generateMultisourceProvidersString();

            logger.debug("Query : {}", SQL_QUERY);

            return jdbcTemplate.queryForList(SQL_QUERY);
        } catch (DataAccessException ex) {
            logger.error("Data access error in list spread by provider and carrier report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            logger.error("Exception in list spread by provider and carrier report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

    @Override
    public List<Map<String, Object>> listMultisourceCarriers(Integer startPage, Integer endPage, String sortFieldName, String sortOrder, Map<String, Object> queryData) throws TechnicalException {
        try {
            SpreadByProviderNCarrierReportQueryBuilder queryBuilder = new SpreadByProviderNCarrierReportQueryBuilder(startPage, endPage, sortFieldName, sortOrder, queryData);
            String SQL_QUERY = queryBuilder.generateMultisourceCarriersString();

            logger.debug("Query : {}", SQL_QUERY);

            return jdbcTemplate.queryForList(SQL_QUERY);
        } catch (DataAccessException ex) {
            logger.error("Data access error in list spread by provider and carrier report");
            throw new TechnicalException(ex.getMessage(), ex);
        } catch (Exception ex) {
            logger.error("Exception in list spread by provider and carrier report");
            throw new TechnicalException(ex.getMessage(), ex);
        }
    }

}
