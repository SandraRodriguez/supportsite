package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier;

import java.util.Map;
import java.util.List;
import org.slf4j.Logger;
import java.util.HashMap;
import java.io.IOException;
import javax.validation.Valid;
import org.slf4j.LoggerFactory;
import java.text.SimpleDateFormat;
import org.springframework.ui.Model;
import org.springframework.http.MediaType;
import net.emida.supportsite.mvc.UrlPaths;
import javax.servlet.http.HttpServletResponse;
import net.emida.supportsite.mvc.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.core.io.FileSystemResource;
import net.emida.supportsite.util.csv.CsvBuilderService;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import net.emida.supportsite.util.exceptions.TechnicalException;
import net.emida.supportsite.util.exceptions.ApplicationException;
import net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao.SpreadByProviderNCarrierReportModel;
import net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao.SpreadByProviderNCarrierReportJdbcDao;
import static net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao.SpreadByProviderNCarrierReportConstants.CARRIER_IDS;
import static net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao.SpreadByProviderNCarrierReportConstants.PROVIDER_IDS;

/**
 *
 * @author fernandob
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH)
public class SpreadByProviderNCarrierReportController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(SpreadByProviderNCarrierReportController.class);

    private static final String FORM_PATH = "reports/commissions/spreadByProviderNCarrier/spreadByProviderNCarrierReportForm";
    private static final String RESULTS_PATH = "reports/commissions/spreadByProviderNCarrier/spreadByProviderNCarrierReportResult";

    @Autowired
    private SpreadByProviderNCarrierReportJdbcDao detailDao;

    @Autowired
    @Qualifier("spreadByProviderNCarrierReportCsvBuilder")
    private CsvBuilderService csvBuilderService;

    private Map<String, Object> fillQueryData(String startDate, String endDate, String providerIds, String carrierIds) {
        Map<String, Object> queryData = new HashMap<String, Object>();
        queryData.put("accessLevel", getAccessLevel());
        queryData.put("distChainType", getDistChainType());
        queryData.put("refId", getRefId());
        queryData.put("startDate", startDate);
        queryData.put("endDate", endDate);
        queryData.put(PROVIDER_IDS, providerIds);
        queryData.put(CARRIER_IDS, carrierIds);
        return queryData;
    }

    private void fillFormComboData(Map model) {
        List<Map<String, Object>> providerList = detailDao.listMultisourceProviders(0, 0, null, null, null);
        model.put("providerList", providerList);

        List<Map<String, Object>> carrierList = detailDao.listMultisourceCarriers(0, 0, null, null, null);
        model.put("carrierList", carrierList);
    }

    @RequestMapping(value = UrlPaths.SPREAD_BY_PROVIDER_CARRIER_REPORT_PATH, method = RequestMethod.GET)
    public String reportForm(Map model, @ModelAttribute SpreadByProviderNCarrierReportForm reportModel) {

        this.fillFormComboData(model);

        return FORM_PATH;
    }

    @RequestMapping(value = UrlPaths.SPREAD_BY_PROVIDER_CARRIER_REPORT_PATH, method = RequestMethod.POST)
    public String reportBuild(Model model, @Valid SpreadByProviderNCarrierReportForm reportModel, BindingResult result) throws TechnicalException, ApplicationException {

        log.debug("reportBuil {}", result);

        log.debug("ReportModel ={}", reportModel);
        log.debug("HasError={}", result.hasErrors());
        log.debug("{}", result.getAllErrors());

        if (result.hasErrors()) {
            model.addAttribute("reportModel", reportModel);
            this.fillFormComboData(model.asMap());
            return FORM_PATH;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String formattedStarDate = sdf.format(reportModel.getStartDate());
        String formattedEndDate = sdf.format(reportModel.getEndDate());

        model.addAttribute("startDate", formattedStarDate);
        model.addAttribute("endDate", formattedEndDate);
        model.addAttribute(PROVIDER_IDS, reportModel.getProviderIdsString());
        model.addAttribute(CARRIER_IDS, reportModel.getCarrierIdsString());

        Map<String, Object> queryData = this.fillQueryData(formattedStarDate, formattedEndDate,
                reportModel.getProviderIdsString(), reportModel.getCarrierIdsString());

        Long totalRecords = this.detailDao.count(queryData);
        model.addAttribute("totalRecords", totalRecords);
        return RESULTS_PATH;
    }

    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.SPREAD_BY_PROVIDER_CARRIER_REPORT_GRID_PATH, produces = "application/json")
    @ResponseBody
    public List<SpreadByProviderNCarrierReportModel> listGrid(@RequestParam(value = "start") String page, @RequestParam String sortField, @RequestParam String sortOrder,
            @RequestParam String rows, @RequestParam String startDate, @RequestParam String endDate,
            @RequestParam String providerIds, @RequestParam String carrierIds) {

        log.debug("listing grid. Page={}. Field={}. Order={}. Rows={}", page, sortField, sortOrder, rows);
        log.debug("Listing grid. StartDate={}. EndDate={}. ProviderIds={}. CarrierIds={}", startDate, endDate, providerIds, carrierIds);

        int rowsPerPage = Integer.parseInt(rows);
        int start = Integer.parseInt(page);

        Map<String, Object> queryData = this.fillQueryData(startDate, endDate, providerIds, carrierIds);

        List<SpreadByProviderNCarrierReportModel> list = detailDao.list(start, rowsPerPage + start - 1, sortField, sortOrder, queryData);

        return list;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = UrlPaths.SPREAD_BY_PROVIDER_CARRIER_REPORT_DOWNLOAD_PATH, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource downloadFile(HttpServletResponse response, @RequestParam String startDate,
                             @RequestParam String endDate, @RequestParam String providerIds, @RequestParam String carrierIds, @RequestParam Integer totalRecords, @RequestParam String sortField,
                             @RequestParam String sortOrder) throws IOException {

        log.info("Downloading file...");
        log.debug("Download file. Field={}. Order={}. TotalRecords={}", sortField, sortOrder, totalRecords);
        log.debug("Download file. StartDate={}. EndDate={}. ProviderIds={}. CarrierIds={}", startDate, endDate, providerIds, carrierIds);

        Map<String, Object> queryData = fillQueryData(startDate, endDate, providerIds, carrierIds);
        List<SpreadByProviderNCarrierReportModel> list = this.detailDao.list(1, totalRecords, sortField, sortOrder, queryData);
        log.debug("Total list={}", list.size());
        String randomFileName = generateRandomFileName();
        String filePath = generateDestinationFilePath(randomFileName, ".csv");
        log.info("Generated path = {}", filePath);

        Map<String, Object> additionalData = new HashMap<String, Object>();
        additionalData.put("language", getSessionData().getLanguage());
        additionalData.put("accessLevel", getAccessLevel());
        additionalData.put("distChainType", getDistChainType());

        csvBuilderService.buildCsv(filePath, list, additionalData);
        log.debug("File csv has been built!");

        FileSystemResource csvFileResource = new FileSystemResource(filePath);
        log.debug("Generating zip file...");
        String generateZipFile = generateZipFile(randomFileName, csvFileResource);
        log.info("Zip file ={}", generateZipFile);
        FileSystemResource zipFileResource = new FileSystemResource(generateZipFile);

        //Delete csv file
        csvFileResource.getFile().delete();

        response.setContentType("application/zip");
        response.setHeader("Content-disposition", "attachment;filename=" + getFilePrefix() + randomFileName + ".zip");

        return zipFileResource;

    }
    

    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 64;
    }

}
