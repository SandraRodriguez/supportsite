package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao;


import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import static com.debisys.utils.DbUtil.hasColumn;
import static net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao.SpreadByProviderNCarrierReportConstants.*;

/**
 *
 * @author fernandob
 */
public class SpreadByProviderNCarrierReportMapper implements RowMapper<SpreadByProviderNCarrierReportModel> {

    @Override
    public SpreadByProviderNCarrierReportModel mapRow(ResultSet rs, int i) throws SQLException {
        SpreadByProviderNCarrierReportModel newRecord = new SpreadByProviderNCarrierReportModel();
        if (hasColumn(rs, ROW_NUMBER)) {
            newRecord.setRowNum((rs.getString(ROW_NUMBER) == null) ? 0L : rs.getLong(ROW_NUMBER));
        }
        if (hasColumn(rs, PROVIDER_NAME)) {
            newRecord.setProviderName((rs.getString(PROVIDER_NAME) == null) ? "" : rs.getString(PROVIDER_NAME));
        }
        if (hasColumn(rs, CARRIER_NAME)) {
            newRecord.setCarrierName((rs.getString(CARRIER_NAME) == null) ? "" : rs.getString(CARRIER_NAME));
        }
        if (hasColumn(rs, TRX_COUNT)) {
            newRecord.setTrxCount((rs.getString(TRX_COUNT) == null) ? 0L : rs.getLong(TRX_COUNT));
        }
        if (hasColumn(rs, TRX_AMOUNT)) {
            newRecord.setTrxAmount((rs.getString(TRX_AMOUNT) == null) ? BigDecimal.ZERO : rs.getBigDecimal(TRX_AMOUNT));
        }
        if (hasColumn(rs, ISO_COMMISSION)) {
            newRecord.setIsoCommission((rs.getString(ISO_COMMISSION) == null) ? BigDecimal.ZERO : rs.getBigDecimal(ISO_COMMISSION));
        }
        if (hasColumn(rs, AGENT_COMMISSION)) {
            newRecord.setAgentCommission((rs.getString(AGENT_COMMISSION) == null) ? BigDecimal.ZERO : rs.getBigDecimal(AGENT_COMMISSION));
        }
        if (hasColumn(rs, SUBAGENT_COMMISSION)) {
            newRecord.setSubagentCommission((rs.getString(SUBAGENT_COMMISSION) == null) ? BigDecimal.ZERO : rs.getBigDecimal(SUBAGENT_COMMISSION));
        }
        if (hasColumn(rs, REP_COMMISSION)) {
            newRecord.setRepCommission((rs.getString(REP_COMMISSION) == null) ? BigDecimal.ZERO : rs.getBigDecimal(REP_COMMISSION));
        }
        if (hasColumn(rs, MERCHANT_COMMISSION)) {
            newRecord.setMerchantCommission((rs.getString(MERCHANT_COMMISSION) == null) ? BigDecimal.ZERO : rs.getBigDecimal(MERCHANT_COMMISSION));
        }
        if (hasColumn(rs, TOTAL_CHANNEL_COMMISSION)) {
            newRecord.setTotalChannelCommission((rs.getString(TOTAL_CHANNEL_COMMISSION) == null) ? BigDecimal.ZERO : rs.getBigDecimal(TOTAL_CHANNEL_COMMISSION));
        }
        return newRecord;
    }

}
