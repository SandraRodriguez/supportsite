package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier;

import java.util.Date;
import java.util.Arrays;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import static com.debisys.utils.StringUtil.toCsvStringFromStringArray;


/**
 *
 * @author fernandob
 */
public class SpreadByProviderNCarrierReportForm {

    private Date startDate;
    private Date endDate;
    private String[] providerIds;
    private String[] carrierIds;

    @NotNull(message = "{NotNull.spreadByProviderNCarrierReportForm.startDate}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @NotNull(message = "{NotNull.spreadByProviderNCarrierReportForm.endDate}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String[] getProviderIds() {
        return providerIds;
    }

    public String getProviderIdsString() {
        return toCsvStringFromStringArray(this.providerIds);
    }

    public void setProviderIds(String[] providerIds) {
        this.providerIds = providerIds;
    }

    public String[] getCarrierIds() {
        return carrierIds;
    }

    public String getCarrierIdsString() {
        return toCsvStringFromStringArray(this.carrierIds);
    }

    public void setCarrierIds(String[] carrierIds) {
        this.carrierIds = carrierIds;
    }

    @Override
    public String toString() {
        return "SpreadByProviderNCarrierReportForm{" + "startDate=" + startDate
                + ", endDate=" + endDate
                + ", providerIds=" + Arrays.toString(providerIds)
                + ", carrierIds=" + Arrays.toString(carrierIds) + '}';
    }

}
