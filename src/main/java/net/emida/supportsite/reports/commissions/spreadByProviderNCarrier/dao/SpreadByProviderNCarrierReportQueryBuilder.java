package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.emida.supportsite.reports.base.QueryBuilder;
import static net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao.SpreadByProviderNCarrierReportConstants.CARRIER_IDS;
import static net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao.SpreadByProviderNCarrierReportConstants.PROVIDER_IDS;

/**
 *
 * @author fernandob
 */
public final class SpreadByProviderNCarrierReportQueryBuilder extends QueryBuilder {

    private static final Logger logger = LoggerFactory.getLogger(SpreadByProviderNCarrierReportQueryBuilder.class);
    
    private static final boolean IS_FOR_SUMMARY= true;
    private static final boolean NOT_IS_FOR_SUMMARY= false;



    // attributes
    private String csvProviderIds;
    private String csvCarrierIds;

    public String getCsvProviderIds() {
        return csvProviderIds;
    }

    public void setCsvProviderIds(String csvProviderIds) {
        this.csvProviderIds = csvProviderIds;
    }

    public String getCsvCarrierIds() {
        return csvCarrierIds;
    }

    public void setCsvCarrierIds(String csvCarrierIds) {
        this.csvCarrierIds = csvCarrierIds;
    }

    @Override
    protected void getQueryData(Map<String, Object> queryData) {
        if (queryData != null) {
            logger.debug("Query Map = {}", queryData);
            super.getQueryData(queryData);

            if (queryData.containsKey(PROVIDER_IDS)) {
                this.csvProviderIds = (String) queryData.get(PROVIDER_IDS);
            }
            if (queryData.containsKey(CARRIER_IDS)) {
                this.csvCarrierIds = (String) queryData.get(CARRIER_IDS);
            }
        }
    }

    public SpreadByProviderNCarrierReportQueryBuilder(Map<String, Object> queryData) {
        this.getQueryData(queryData);
    }

    public SpreadByProviderNCarrierReportQueryBuilder(int startPage, int endPage,
            String sortFieldName, String sortOrder, Map<String, Object> queryData) {
        this.getQueryData(queryData);
        this.startPage = startPage;
        this.endPage = endPage;
        this.sortFieldName = String.format(" %s ", sortFieldName);
        this.sortOrder = String.format(" %s ", sortOrder);
    }

    private String generateCoreQuery() {
        String retValue = null;

        try {
            retValue = String.format("		SELECT  %s \n"
            					   + "FROM %s  \n"
            					   + "WHERE MS.Related_rec_id <> 0 \n"
            					   + "AND PR.provider_id <> 259 \n"
            					   + "%s \n"
            					   + "GROUP BY isnull(PR.display_name,PRFront.display_name), isnull(C.Name, CFront.Name)",
                    this.generateQueryFields(), this.generateQueryFromTables(),
                    this.generateQueryWhere());
        } catch (Exception localException) {
            logger.error(String.format("%s.%s", SpreadByProviderNCarrierReportQueryBuilder.class.getName(),
                    "Error while generating report main query"), localException);
        }
        return retValue;
    }
    
    
    
    private String generateCoreQuerySpreadByProviderNCarrier() {
        String retValue = null;
        
        StringBuilder sb = new StringBuilder();

        try {
        	sb.append("		SELECT   ")
        	  .append(this.generateQueryFields())
        	  .append("FROM  ")
        	  .append(this.generateQueryFromTables())
        	  .append(" WHERE isnull(PR.provider_id,PRFront.provider_id) <> 259  \n")
        	  .append(this.generateQueryWhere())
        	  .append("GROUP BY isnull(PR.display_name,PRFront.display_name), isnull(C.Name, CFront.Name)");

        } catch (Exception localException) {
            logger.error(String.format("%s.%s", SpreadByProviderNCarrierReportQueryBuilder.class.getName(),
                    "Error while generating report main query"), localException);
        }
        return sb.toString();
    }
    
    

    private String generateQueryFields() {
        StringBuilder strSqlFields = new StringBuilder();

        strSqlFields.append("  isnull(PR.display_name,PRFront.display_name) providerName, \n")
        			.append("				isnull(C.Name, CFront.Name) carrierName, \n")
        			.append("				COUNT(wtFront.rec_id) trxCount, \n")
        			.append("				SUM(wtFront.amount) trxAmount, \n")
        			.append(this.queryCalculateIsoCommission(NOT_IS_FOR_SUMMARY))
        			.append(this.queryCalculateAgentCommission(NOT_IS_FOR_SUMMARY))
        			.append(this.queryCalculateSubagentCommission(NOT_IS_FOR_SUMMARY))
        			.append(this.queryCalculateRepCommission(NOT_IS_FOR_SUMMARY))
        			.append(this.queryCalculateMerchantCommission(NOT_IS_FOR_SUMMARY))
        			.append(this.queryCalculateTotalChannelCommission());

        return strSqlFields.toString();
    }
    
    
  
    
    
    


	private String queryCalculateTotalChannelCommission() {
		StringBuilder strSqlFields = new StringBuilder();
		 
		 strSqlFields.append("("+this.queryCalculateIsoCommission(IS_FOR_SUMMARY))
		 			 .append("	+ "+this.queryCalculateAgentCommission(IS_FOR_SUMMARY))
		 			 .append("	+ "+this.queryCalculateSubagentCommission(IS_FOR_SUMMARY))		 			
		 			 .append("	+ "+this.queryCalculateRepCommission(IS_FOR_SUMMARY))
		 			 .append("	+ "+this.queryCalculateMerchantCommission(IS_FOR_SUMMARY))
		 			 .append("				) As totalChannelCommission \n");

		 
		return strSqlFields.toString();

	}

	private Object queryCalculateMerchantCommission(boolean summary) {
		StringBuilder strSqlFields = new StringBuilder();
		 
		 strSqlFields.append("				SUM(wtFront.merchant_rate / 100 * (CASE PFront.program_id \n")
		 			 .append("													 WHEN 154 THEN wtFront.amount \n")
		 			 .append("														ELSE CASE PFront.ach_type \n")
		 			 .append("																WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt \n")
		 			 .append("																ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) \n")
		 			 .append("																		WHEN 0 THEN wtFront.amount	 \n")
		 			 .append("																		ELSE wtFront.transfixedfeeamt \n")
		 			 .append("																	 END \n")
		 			 .append("															  END \n");
		 
		 if(summary){
				strSqlFields.append("													END)) \n");

		 }else{

				strSqlFields.append("													END) \n")
				 			.append("					) merchantCommission, \n");
		 }

		 
		return strSqlFields.toString();
	}

	private String queryCalculateRepCommission(boolean summary) {
		StringBuilder strSqlFields = new StringBuilder();
		 
		 strSqlFields.append("				 SUM(wtFront.rep_rate / 100 * (CASE PFront.program_id \n")
		 			 .append("													WHEN 154 THEN wtFront.amount \n")
		 			 .append("													ELSE CASE PFront.ach_type \n")
		 			 .append("															WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt \n")
		 			 .append("															ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) \n")
		 			 .append("																WHEN 0	THEN wtFront.amount	 \n")
		 			 .append("																ELSE wtFront.transfixedfeeamt \n")
		 			 .append("															END \n")
		 			 .append("													END \n");
		 			
		 			
		 if(summary){
				strSqlFields.append("													END)) \n");
				
		 }else{
			 strSqlFields.append("													END) \n")
	 			.append("					) repCommission, \n");
		 }
		 
		return strSqlFields.toString();
	}


	private Object queryCalculateSubagentCommission(boolean summary ) {
		StringBuilder strSqlFields = new StringBuilder();
		 
		 strSqlFields.append("				 SUM(wtFront.subagent_rate / 100 * (CASE PFront.program_id \n")
		 			 .append("														WHEN 154 THEN wtFront.amount \n")
		 			 .append("														ELSE CASE PFront.ach_type \n")
		 			 .append("																WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt \n")
		 			 .append("																ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) \n")
		 			 .append("																		WHEN 0 THEN wtFront.amount	 \n")
		 			 .append("																		ELSE wtFront.transfixedfeeamt \n")
		 			 .append("																END \n")
		 			 .append("														END \n");
		 
		 if(summary){
				strSqlFields.append("													END)) \n");
		 }else{
				strSqlFields.append("													END) \n")
	 			.append("				       ) subagentCommission, \n");

		 }
		 
		return strSqlFields.toString();
	}

	private String queryCalculateAgentCommission(boolean summary) {

		 StringBuilder strSqlFields = new StringBuilder();
		 
		 strSqlFields.append("				SUM(wtFront.agent_rate / 100 *(CASE PFront.program_id \n")
		 			 .append("													WHEN 154 THEN wtFront.amount \n")
		 			 .append("													ELSE CASE PFront.ach_type \n")
		 			 .append("															WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt \n")
		 			 .append("															ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) \n")
		 			 .append("																	WHEN 0 THEN wtFront.amount	 \n")
		 			 .append("																	ELSE wtFront.transfixedfeeamt \n")
		 			 .append("																END \n")
		 			 .append("														END \n");
		 

		 if(summary){
				strSqlFields.append("													END)) \n");

		 }else{
				strSqlFields.append("													END) \n")
	 			.append("				      ) agentCommission, \n");
		 }
		 
		return strSqlFields.toString();
	}

	private Object queryCalculateIsoCommission(boolean summary) {
		
		 StringBuilder strSqlFields = new StringBuilder();
		 
		 strSqlFields.append("				SUM(wtFront.iso_rate / 100 *(CASE PFront.program_id \n")
		 			 .append("												WHEN 154 THEN wtFront.amount \n")
		 			 .append("												ELSE CASE PFront.ach_type \n")
		 			 .append("														WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt \n")
		 			 .append("														ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) \n")
		 			 .append("																WHEN 0 THEN wtFront.amount \n")
		 			 .append("																ELSE wtFront.transfixedfeeamt \n")
		 			 .append("															  END \n")
		 			 .append("													  END \n");

		 
		 if(summary){
				strSqlFields.append("													END)) \n");
		 }else{
				strSqlFields.append("													END) \n")
	 			.append("					) isoCommission, \n");

		 }
		 
		return strSqlFields.toString();
	}

//	private String generateQueryFieldsSpredByProviderNCarrier() {
//        StringBuilder strSqlFields = new StringBuilder();
//
//        strSqlFields.append(" PR.display_name providerName ");
//        strSqlFields.append("  , C.Name carrierName ");
//        strSqlFields.append("  , COUNT(wtFront.rec_id) trxCount ");
//        strSqlFields.append("  , SUM(wtFront.amount) trxAmount ");
//        strSqlFields.append("  , SUM(wtFront.iso_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) isoCommission ");
//        strSqlFields.append("  , SUM(wtFront.agent_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) agentCommission ");
//        strSqlFields.append("  , SUM(wtFront.subagent_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) subagentCommission ");
//        strSqlFields.append("  , SUM(wtFront.rep_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) repCommission ");
//        strSqlFields.append("  , SUM(wtFront.merchant_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) merchantCommission ");
//        strSqlFields.append("  , (SUM(wtFront.iso_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) +  ");
//        strSqlFields.append("     SUM(wtFront.agent_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) + ");
//        strSqlFields.append("     SUM(wtFront.subagent_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) +  ");
//        strSqlFields.append("     SUM(wtFront.rep_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END )) + ");
//        strSqlFields.append("     SUM(wtFront.merchant_rate / 100 * (CASE PFront.program_id WHEN 154 THEN wtFront.amount ELSE CASE PFront.ach_type WHEN 6 THEN wtFront.Fixed_trans_fee + wtFront.transFixedFeeAmt ELSE CASE ISNULL(wtFront.transfixedfeeamt, 0) WHEN 0 THEN wtFront.amount ELSE wtFront.transfixedfeeamt END END END ))) As totalChannelCommission ");
//
//        return strSqlFields.toString();
//    }

    private String generateQueryFromTables() {

        StringBuilder strSqlFromTables = new StringBuilder();
        strSqlFromTables.append("dbo.web_transactions wtFront WITH (NOLOCK) \n")
						.append("INNER JOIN dbo.Products PFront WITH (NOLOCK) ON wtFront.id = PFront.id  \n ")
						.append("INNER JOIN dbo.Provider PRFront WITH (NOLOCK) ON PFront.program_id = PRFront.provider_id  \n ")
						.append("LEFT JOIN dbo.web_transactions wtBack WITH (NOLOCK) ON wtFront.related_rec_id = wtBack.rec_id  \n ")
						.append("LEFT JOIN dbo.Products PBack WITH (NOLOCK) ON wtBack.id = PBack.id  \n ")
						.append("LEFT JOIN dbo.MultiSourceTransactions MS WITH (NOLOCK) ON MS.Rec_id = wtFront.rec_id AND MS.Related_rec_id = wtBack.rec_id  \n ")
						.append("LEFT JOIN dbo.Provider PR WITH (NOLOCK) ON PBack.program_id = PR.provider_id  \n ")
						.append("LEFT JOIN dbo.Carriers C WITH (NOLOCK) ON PBack.carrier_id = C.id  \n ")
						.append("INNER JOIN dbo.Carriers CFront WITH (NOLOCK) ON PFront.carrier_id = CFront.id \n ");

        return strSqlFromTables.toString();
    }

    private String generateQueryWhere() throws Exception {
        StringBuilder strSqlWhere = new StringBuilder();
        if ((this.csvProviderIds != null) && (!this.csvProviderIds.isEmpty())) {
            if (this.csvProviderIds.contains(",")) {
                strSqlWhere.append(String.format(" AND PR.provider_id IN (%s)", this.csvProviderIds));
            } else {
                strSqlWhere.append(String.format(" AND PR.provider_id = %s", this.csvProviderIds));
            }
        }
        if ((this.csvCarrierIds != null) && (!this.csvCarrierIds.isEmpty())) {
            if (this.csvCarrierIds.contains(",")) {
                strSqlWhere.append(String.format(" AND C.id IN (%s)", this.csvCarrierIds));
            } else {
                strSqlWhere.append(String.format(" AND C.id = %s", this.csvCarrierIds));
            }
        }
        strSqlWhere.append(this.buildDatesRangeWhere("wtFront.[datetime]"));
        return strSqlWhere.toString();
    }

    public String generateRecordCountQuery() {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT COUNT(*) FROM ( \n");

        sb0.append(this.generateCoreQuerySpreadByProviderNCarrier());

        sb0.append(") AS tbl1 ");

        return sb0.toString();
    }

    public String generateSqlString() {
        if (sortFieldName == null || sortFieldName.isEmpty()) {
            sortFieldName = "PR.display_name";
            sortOrder = "ASC";
        }

        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM ( \n");
        sb0.append("	SELECT ROW_NUMBER() OVER(ORDER BY ").append(String.format("%s %s", sortFieldName, sortOrder)).append(") AS RWN, * FROM ( \n");

        sb0.append(this.generateCoreQuerySpreadByProviderNCarrier());

        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(startPage).append(" AND ").append(endPage);

        return sb0.toString();
    }
    
    public String generateMultisourceProvidersString()
    {
        StringBuilder sbProviders = new StringBuilder();
        sbProviders.append(" SELECT PR.provider_id, PR.name, PR.display_name ");
        sbProviders.append(" FROM dbo.MultiSource MS WITH(NOLOCK) ");
        sbProviders.append("  INNER JOIN dbo.Products P WITH(NOLOCK) ON MS.Alt_Product_id = P.id ");
        sbProviders.append("  INNER JOIN dbo.Provider PR WITH(NOLOCK)ON P.program_id = PR.provider_id ");
        sbProviders.append(" GROUP BY PR.provider_id, PR.name, PR.display_name ");
        sbProviders.append(" ORDER BY PR.name, PR.display_name");
        return sbProviders.toString();
    }

    public String generateMultisourceCarriersString()
    {
        StringBuilder sbCarriers = new StringBuilder();
        sbCarriers.append(" SELECT C.ID, C.Name ");
        sbCarriers.append(" FROM dbo.MultiSource MS WITH(NOLOCK) ");
        sbCarriers.append("  INNER JOIN dbo.Products P WITH(NOLOCK) ON MS.Alt_Product_id = P.id ");
        sbCarriers.append("  INNER JOIN dbo.Carriers C WITH(NOLOCK)ON P.carrier_id = C.id ");
        sbCarriers.append(" GROUP BY C.ID, C.Name ");
        sbCarriers.append(" ORDER BY C.Name ");
        return sbCarriers.toString();
    }
    
    
}
