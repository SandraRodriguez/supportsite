package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;
import com.debisys.utils.NumberUtil;
import net.emida.supportsite.util.csv.CsvRow;
import net.emida.supportsite.util.csv.CsvCell;

/**
 *
 * @author fernandob
 */
public class SpreadByProviderNCarrierReportModel implements CsvRow {

    private Long rowNum;
    private String providerName;
    private String carrierName;
    private long trxCount;
    private BigDecimal trxAmount;
    private BigDecimal isoCommission;
    private BigDecimal agentCommission;
    private BigDecimal subagentCommission;
    private BigDecimal repCommission;
    private BigDecimal merchantCommission;
    private BigDecimal totalChannelCommission;

    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public long getTrxCount() {
        return trxCount;
    }

    public void setTrxCount(long trxCount) {
        this.trxCount = trxCount;
    }

    public BigDecimal getTrxAmount() {
        return trxAmount;
    }

    public void setTrxAmount(BigDecimal trxAmount) {
        this.trxAmount = trxAmount;
    }

    public BigDecimal getIsoCommission() {
        return isoCommission;
    }

    public void setIsoCommission(BigDecimal isoCommission) {
        this.isoCommission = isoCommission;
    }

    public BigDecimal getAgentCommission() {
        return agentCommission;
    }

    public void setAgentCommission(BigDecimal agentCommission) {
        this.agentCommission = agentCommission;
    }

    public BigDecimal getSubagentCommission() {
        return subagentCommission;
    }

    public void setSubagentCommission(BigDecimal subagentCommission) {
        this.subagentCommission = subagentCommission;
    }

    public BigDecimal getRepCommission() {
        return repCommission;
    }

    public void setRepCommission(BigDecimal repCommission) {
        this.repCommission = repCommission;
    }

    public BigDecimal getMerchantCommission() {
        return merchantCommission;
    }

    public void setMerchantCommission(BigDecimal merchantCommission) {
        this.merchantCommission = merchantCommission;
    }

    public BigDecimal getTotalChannelCommission() {
        return totalChannelCommission;
    }

    public void setTotalChannelCommission(BigDecimal totalChannelCommission) {
        this.totalChannelCommission = totalChannelCommission;
    }

    public SpreadByProviderNCarrierReportModel() {
        this.rowNum = 0L;
        this.providerName = "";
        this.carrierName = "";
        this.trxCount = 0L;
        this.trxAmount = BigDecimal.ZERO;
        this.isoCommission = BigDecimal.ZERO;
        this.agentCommission = BigDecimal.ZERO;
        this.subagentCommission = BigDecimal.ZERO;
        this.repCommission = BigDecimal.ZERO;
        this.merchantCommission = BigDecimal.ZERO;
        this.totalChannelCommission = BigDecimal.ZERO;
    }

    public SpreadByProviderNCarrierReportModel(long rowNum, String providerName, String carrierName,
            long trxCount, BigDecimal trxAmount, BigDecimal isoCommission, BigDecimal agentCommission,
            BigDecimal subagentCommission, BigDecimal repCommission, BigDecimal merchantCommission,
            BigDecimal totalChannelCommission) {
        this.rowNum = rowNum;
        this.providerName = providerName;
        this.carrierName = carrierName;
        this.trxCount = trxCount;
        this.trxAmount = trxAmount;
        this.isoCommission = isoCommission;
        this.agentCommission = agentCommission;
        this.subagentCommission = subagentCommission;
        this.repCommission = repCommission;
        this.merchantCommission = merchantCommission;
        this.totalChannelCommission = totalChannelCommission;
    }

    @Override
    public String toString() {
        return "spreadByProviderNCarrierReportModel{" + "rowNum=" + rowNum + ", providerName=" + providerName + ", carrierName=" + carrierName + ", trxCount=" + trxCount + ", trxAmount=" + trxAmount + ", isoCommission=" + isoCommission + ", agentCommission=" + agentCommission + ", subagentCommission=" + subagentCommission + ", repCommission=" + repCommission + ", merchantCommission=" + merchantCommission + ", totalChannelCommission=" + totalChannelCommission + '}';
    }

    @Override
    public List<CsvCell> cells() {
        List<CsvCell> retValue = new ArrayList<CsvCell>();
        
        // Row number
        CsvCell cell = new CsvCell(this.rowNum.toString());
        retValue.add(cell);
        // Provider name
        cell = new CsvCell(this.providerName);
        retValue.add(cell);
        // Carrier name
        cell = new CsvCell(this.carrierName);
        retValue.add(cell);
        // Transaction count
        cell = new CsvCell(String.valueOf(this.trxCount));
        retValue.add(cell);
        // Transaction sum amount
        cell = new CsvCell(NumberUtil.formatCurrency(this.trxAmount.toString()));
        retValue.add(cell);
        // Iso commission
        cell = new CsvCell(NumberUtil.formatCurrency(this.isoCommission.toString()));
        retValue.add(cell);
        // Agent commission
        cell = new CsvCell(NumberUtil.formatCurrency(this.agentCommission.toString()));
        retValue.add(cell);
        // Subgent commission
        cell = new CsvCell(NumberUtil.formatCurrency(this.subagentCommission.toString()));
        retValue.add(cell);
        // Rep commission
        cell = new CsvCell(NumberUtil.formatCurrency(this.repCommission.toString()));
        retValue.add(cell);
        // Merchant commission
        cell = new CsvCell(NumberUtil.formatCurrency(this.merchantCommission.toString()));
        retValue.add(cell);
        // Total channel commission
        cell = new CsvCell(NumberUtil.formatCurrency(this.totalChannelCommission.toString()));
        retValue.add(cell);
        return retValue;
    }

}
