package net.emida.supportsite.reports.commissions.spreadByProviderNCarrier.dao;

/**
 *
 * @author fernandob
 */
public class SpreadByProviderNCarrierReportConstants {

    
    // Report parameter names
    public static final String PROVIDER_IDS = "providerIds";
    public static final String CARRIER_IDS = "carrierIds";


    // Recordset related column names
    public static final String ROW_NUMBER = "RWN";
    public static final String PROVIDER_NAME = "providerName";
    public static final String CARRIER_NAME = "carrierName";
    public static final String TRX_COUNT = "trxCount";
    public static final String TRX_AMOUNT = "trxAmount";
    public static final String ISO_COMMISSION = "isoCommission";
    public static final String AGENT_COMMISSION = "agentCommission";
    public static final String SUBAGENT_COMMISSION = "subagentCommission";
    public static final String REP_COMMISSION = "repCommission";
    public static final String MERCHANT_COMMISSION = "merchantCommission";
    public static final String TOTAL_CHANNEL_COMMISSION = "totalChannelCommission";
    


}
