package net.emida.supportsite.reports.trxByRegion.results;

import java.util.List;
import net.emida.supportsite.reports.trxByRegion.TransactionsByRegionReportModel;

/**
 * @author Emida
 */

public class TransactionsByRegionReportResult {


    private Long totalRecords;
    private List<TransactionsByRegionReportModel> body;


    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<TransactionsByRegionReportModel> getBody() {
        return body;
    }

    public void setBody(List<TransactionsByRegionReportModel> body) {
        this.body = body;
    }
}
