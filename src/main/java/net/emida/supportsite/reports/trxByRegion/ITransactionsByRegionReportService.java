package net.emida.supportsite.reports.trxByRegion;

import net.emida.supportsite.reports.trxByRegion.forms.TransactionsByRegionReportParam;
import net.emida.supportsite.reports.trxByRegion.results.TransactionsByRegionReportResult;

/**
 * @author Emida
 */

public interface ITransactionsByRegionReportService {


    ReportByRegionData buildReportData(TransactionsByRegionReportParam param);

    TransactionsByRegionReportResult buildReport(TransactionsByRegionReportParam param);

}
