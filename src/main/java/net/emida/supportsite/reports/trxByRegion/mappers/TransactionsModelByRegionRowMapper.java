package net.emida.supportsite.reports.trxByRegion.mappers;

import com.debisys.utils.DateUtil;
import com.debisys.utils.StringUtil;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import net.emida.supportsite.reports.trxByRegion.TransactionsByRegionReportModel;
//import net.emida.supportsite.reports.transactions.TransactionsReportModel;
import net.emida.supportsite.util.RSUtil;
import net.emida.supportsite.util.TimeZoneInfo;
import net.emida.supportsite.util.TimeZoneUtils;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

/**
 * @author Emida
 */
public class TransactionsModelByRegionRowMapper implements RowMapper<TransactionsByRegionReportModel> {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TransactionsModelByRegionRowMapper.class);

    private final boolean showAccountIdQrCodeTransactions;
    private final boolean showCommissionDetails;
    private final TimeZoneInfo defaultTimeZoneInfo;
    private final TimeZoneInfo entityTimeZoneInfo;


    public TransactionsModelByRegionRowMapper(boolean showAccountIdQrCodeTransactions, boolean showCommissionDetails, TimeZoneInfo defaultTimeZoneInfo, TimeZoneInfo entityTimeZoneInfo) {
        this.showAccountIdQrCodeTransactions = showAccountIdQrCodeTransactions;
        this.showCommissionDetails = showCommissionDetails;
        this.defaultTimeZoneInfo = defaultTimeZoneInfo;
        this.entityTimeZoneInfo = entityTimeZoneInfo;
    }

    @Override
    public TransactionsByRegionReportModel mapRow(ResultSet rs, int rowNum) throws SQLException {

        TransactionsByRegionReportModel m = new TransactionsByRegionReportModel();
        m.setRowNum(rs.getLong("RWN"));
        m.setShowCommissionDetails(showCommissionDetails);
        m.setRecId(rs.getInt("recId"));
        m.setInvoiceNo(rs.getInt("invoiceNo"));
        m.setMillenniumNo(rs.getInt("millenniumNo"));
        String trmDesc = rs.getString("trmDescription");
        m.setTrmDescription(StringUtil.isNotEmptyStr(trmDesc) ? trmDesc.trim() : "");

        String dba = rs.getString("dba");
        m.setDba(StringUtil.isNotEmptyStr(dba) ? dba.trim() : "");

        String physCity = rs.getString("physCity");
        m.setPhysCity(StringUtil.isNotEmptyStr(physCity) ? physCity.trim() : "");

        String physCounty = rs.getString("physCounty");
        m.setPhysCounty(StringUtil.isNotEmptyStr(physCounty) ? physCounty.trim() : "");

        String physState = rs.getString("physState");
        m.setPhysState(StringUtil.isNotEmptyStr(physState) ? physState.trim() : "");

        Timestamp datetime = rs.getTimestamp("datetime");
        java.util.Date utcDateTime = TimeZoneUtils.getUTCDateTime(new Date(datetime.getTime()), defaultTimeZoneInfo);
        java.util.Date entityDateTime = TimeZoneUtils.getLocalDateTime(utcDateTime, entityTimeZoneInfo);
        String trxDate = DateUtil.formatDateTime(new Date(entityDateTime.getTime()));
        m.setDatetime(trxDate);

        m.setMerchantId(rs.getBigDecimal("merchantId").longValue());
        m.setControlNo(rs.getInt("controlNo"));


        String clerkName = rs.getString("clerkName");
        m.setClerkName(StringUtil.isNotEmptyStr(clerkName) ? clerkName.trim() : "");

        String clerk = rs.getString("clerk");
        m.setClerk(StringUtil.isNotEmptyStr(clerk) ? clerk.trim() : "");

        String transactionType = rs.getString("transactionType");
        m.setTransactionType(StringUtil.isNotEmptyStr(transactionType) ? transactionType.trim() : "");

        boolean qrCodeTransaction = isQrCodeTransaction(rs.getBoolean("qrCode"));
        String ani = rs.getString("ani");
        ani = StringUtil.isNotEmptyStr(ani) ? ani.trim() : "";
        if (qrCodeTransaction) {
            m.setQrCode("(QRCode)");
            if(showAccountIdQrCodeTransactions){
                m.setAni(ani);
            }else{
                ani = StringUtil.hideXNumberDigits(ani);
                m.setAni(ani);
            }
        } else {
            m.setQrCode("");
            m.setAni(ani);
        }

        String otherInfo = rs.getString("otherInfo");
        otherInfo = StringUtil.isNotEmptyStr(otherInfo) ? otherInfo.trim() : "";
        m.setOtherInfo(otherInfo);

        String password = rs.getString("password");
        m.setPassword(StringUtil.isNotEmptyStr(password) ? password.trim() : "");

        m.setBalance(rs.getBigDecimal("balance"));
        m.setId(rs.getInt("id"));

        String description = rs.getString("description");
        m.setDescription(StringUtil.isNotEmptyStr(description) ? description.trim() : "");

        m.setProductTransType(rs.getInt("productTransType"));
        m.setBonusAmount(rs.getBigDecimal("bonusAmount"));
        m.setTotalRecharge(rs.getBigDecimal("totalRecharge"));
        m.setTaxType(rs.getInt("taxType"));

        String ratePlanName = rs.getString("ratePlanName");
        m.setRatePlanName(StringUtil.isNotEmptyStr(ratePlanName) ? ratePlanName.trim() : "");

        String taxPercentage = rs.getString("taxPercentage");
        m.setTaxPercentage(StringUtil.isNotEmptyStr(taxPercentage) ? taxPercentage.trim() : "");

        m.setMerchantCommission(new BigDecimal(rs.getDouble("merchantCommission")));
        m.setRepCommission(new BigDecimal(rs.getDouble("repCommission")));
        m.setSubAgentCommission(new BigDecimal(rs.getDouble("subAgentCommission")));
        m.setAgentCommission(new BigDecimal(rs.getDouble("agentCommission")));
        m.setIsoCommission(new BigDecimal(rs.getDouble("isoCommission")));
        m.setAmount(rs.getBigDecimal("amount"));

        //This fields are conditional
        m.setNetAmount(RSUtil.getBigDecimal(rs, "netAmount", true));
        m.setTaxAmount(RSUtil.getBigDecimal(rs, "taxAmount", true));


        String pin = RSUtil.getString(rs, "pin", true);
        m.setPin(StringUtil.isNotEmptyStr(pin) ? pin.trim() : "");

        //
        Timestamp achDatetime = RSUtil.getTimestamp(rs, "achDatetime", true);
        String strAchDatetime;
        if(achDatetime != null){
            java.util.Date utcAchDateTime = TimeZoneUtils.getUTCDateTime(new Date(achDatetime.getTime()), defaultTimeZoneInfo);
            java.util.Date entityAchDateTime = TimeZoneUtils.getLocalDateTime(utcAchDateTime, entityTimeZoneInfo);
            strAchDatetime = DateUtil.formatDateTime(entityAchDateTime);
        }else{
            strAchDatetime = "TBD";
        }
        m.setAchDatetime(strAchDatetime);

        String info = RSUtil.getString(rs, "info", true);
        m.setInfo(StringUtil.isNotEmptyStr(info) ? info.trim() : "");

        String additionalData = RSUtil.getString(rs, "additionalData", true);
        m.setAdditionalData(StringUtil.isNotEmptyStr(additionalData) ? additionalData.trim() : "");
        
        String receipt = RSUtil.getString(rs, "receipt", true);
        try {
            if(receipt != null){
                m.setReceipt(URLEncoder.encode(receipt, "UTF-8"));
            } else{
                m.setReceipt("");
            }
        } catch (UnsupportedEncodingException ex) {
            LOG.error("receipt transaction cannot be encode for [" + m.getRecId() + "]", ex);
        }        
        String strExternalMerchantId = RSUtil.getString(rs, "externalMerchantId", true);        
        m.setExternalMerchantId(((strExternalMerchantId != null) && (!strExternalMerchantId.isEmpty())) ? strExternalMerchantId.trim() : "");
        m.setRegion(rs.getInt("region"));
        m.setProviderId(rs.getInt("providerId"));
        m.setProviderName(rs.getString("providerName"));
        
        return m;
    }

    private boolean isQrCodeTransaction(Boolean qrCode) {
        return qrCode != null && qrCode;
    }
}
