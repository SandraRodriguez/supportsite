package net.emida.supportsite.reports.trxByRegion;

import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.util.csv.CsvCell;
import net.emida.supportsite.util.csv.CsvRow;

/**
 * @author Emida
 */
public class TransactionsByRegionReportModel implements CsvRow {

    private Long rowNum;

    private Integer recId;              //rec_id
    private Integer millenniumNo;       //millennium_no
    private String trmDescription;      //trm_description
    private String dba;                 //dba
    private Long merchantId;            //merchant_id
    private String datetime;         //datetime
    private String physState;           //phys_state
    private String physCity;            //phys_city
    private String physCounty;          //phys_county

    private String clerkName;           //clerk_name
    private String clerk;               //clerk

    private String ani;                 //ani
    private String qrCode;             //isQRCode
    private String password;            //password
    private BigDecimal amount;          //amount
    private BigDecimal balance;         //balance
    private Integer id;                 //id            --> web_transactions
    private Integer controlNo;          //control_no
    private Integer productTransType;    //product_trans_type
    private String transactionType;     //transaction_type
    private String otherInfo;           //other_info

    private BigDecimal bonusAmount;     //bonus_amount
    private BigDecimal totalRecharge;   //total_recharge
    private String pin;                 //pin
    private String achDatetime;      //ach_datetime
    private String description;

    private String additionalData;      //additionalData

    private Integer invoiceNo;          //invoice_no
    private String taxPercentage;       //taxpercentage
    private Integer taxType;            //taxtype
    private BigDecimal netAmount;           //netamount --- era String
    private BigDecimal taxAmount;           //taxamount --- era String

    private BigDecimal isoCommission;       //iso_commission
    private BigDecimal agentCommission;     //agent_commission
    private BigDecimal subAgentCommission;  //subagent_commission
    private BigDecimal repCommission;       //rep_commission
    private BigDecimal merchantCommission;  //merchant_commission

    private String info;                //info
    private String ratePlanName;        //rateplanName
    private boolean showCommissionDetails;
    protected String receipt;                //receipt
    protected String externalMerchantId;    // externalMerchantId
    private Integer region;
    private Integer providerId;
    private String providerName;
    
    public Long getRowNum() {
        return rowNum;
    }

    public void setRowNum(Long rowNum) {
        this.rowNum = rowNum;
    }

    public Integer getRecId() {
        return recId;
    }

    public void setRecId(Integer recId) {
        this.recId = recId;
    }

    public Integer getMillenniumNo() {
        return millenniumNo;
    }

    public void setMillenniumNo(Integer millenniumNo) {
        this.millenniumNo = millenniumNo;
    }

    public String getTrmDescription() {
        return trmDescription;
    }

    public void setTrmDescription(String trmDescription) {
        this.trmDescription = trmDescription;
    }

    public String getDba() {
        return dba;
    }

    public void setDba(String dba) {
        this.dba = dba;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getPhysState() {
        return physState;
    }

    public void setPhysState(String physState) {
        this.physState = physState;
    }

    public String getPhysCity() {
        return physCity;
    }

    public void setPhysCity(String physCity) {
        this.physCity = physCity;
    }

    public String getPhysCounty() {
        return physCounty;
    }

    public void setPhysCounty(String physCounty) {
        this.physCounty = physCounty;
    }

    public String getClerkName() {
        return clerkName;
    }

    public void setClerkName(String clerkName) {
        this.clerkName = clerkName;
    }

    public String getClerk() {
        return clerk;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    public String getAni() {
        return ani;
    }

    public void setAni(String ani) {
        this.ani = ani;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getControlNo() {
        return controlNo;
    }

    public void setControlNo(Integer controlNo) {
        this.controlNo = controlNo;
    }

    public Integer getProductTransType() {
        return productTransType;
    }

    public void setProductTransType(Integer productTransType) {
        this.productTransType = productTransType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }

    public BigDecimal getTotalRecharge() {
        return totalRecharge;
    }

    public void setTotalRecharge(BigDecimal totalRecharge) {
        this.totalRecharge = totalRecharge;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getAchDatetime() {
        return achDatetime;
    }

    public void setAchDatetime(String achDatetime) {
        this.achDatetime = achDatetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public Integer getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(Integer invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public Integer getTaxType() {
        return taxType;
    }

    public void setTaxType(Integer taxType) {
        this.taxType = taxType;
    }

    public BigDecimal getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public BigDecimal getIsoCommission() {
        return isoCommission;
    }

    public void setIsoCommission(BigDecimal isoCommission) {
        this.isoCommission = isoCommission;
    }

    public BigDecimal getAgentCommission() {
        return agentCommission;
    }

    public void setAgentCommission(BigDecimal agentCommission) {
        this.agentCommission = agentCommission;
    }

    public BigDecimal getSubAgentCommission() {
        return subAgentCommission;
    }

    public void setSubAgentCommission(BigDecimal subAgentCommission) {
        this.subAgentCommission = subAgentCommission;
    }

    public BigDecimal getRepCommission() {
        return repCommission;
    }

    public void setRepCommission(BigDecimal repCommission) {
        this.repCommission = repCommission;
    }

    public BigDecimal getMerchantCommission() {
        return merchantCommission;
    }

    public void setMerchantCommission(BigDecimal merchantCommission) {
        this.merchantCommission = merchantCommission;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getRatePlanName() {
        return ratePlanName;
    }

    public void setRatePlanName(String ratePlanName) {
        this.ratePlanName = ratePlanName;
    }

    public boolean isShowCommissionDetails() {
        return showCommissionDetails;
    }

    public void setShowCommissionDetails(boolean showCommissionDetails) {
        this.showCommissionDetails = showCommissionDetails;
    }

    /**
     * @return the receipt
     */
    public String getReceipt() {
        return receipt;
    }

    /**
     * @param receipt the receipt to set
     */
    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getExternalMerchantId() {
        return externalMerchantId;
    }

    public void setExternalMerchantId(String externalMerchantId) {
        this.externalMerchantId = externalMerchantId;
    }

    @Override
    public List<CsvCell> cells() {

        SimpleDateFormat sdfWithTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<CsvCell> list = new ArrayList<>();

        CsvCell cell0 = new CsvCell(recId.toString());
        list.add(cell0);

        CsvCell cell1 = new CsvCell(millenniumNo.toString());
        list.add(cell1);

        CsvCell cell2 = new CsvCell(dba);
        list.add(cell2);

        CsvCell cell3 = new CsvCell(merchantId.toString());
        list.add(cell3);

        CsvCell cell4 = new CsvCell(StringUtil.isNotEmptyStr(datetime) ? datetime : "");
        list.add(cell4);

        CsvCell cell5 = new CsvCell(StringUtil.isNotEmptyStr(physCity) ? physCity : "");
        list.add(cell5);

        CsvCell cell6 = new CsvCell(StringUtil.isNotEmptyStr(physCounty) ? physCounty : "");
        list.add(cell6);

        CsvCell cell7 = new CsvCell(StringUtil.isNotEmptyStr(clerkName) ? clerkName : "");
        list.add(cell7);

        CsvCell cell8 = new CsvCell(StringUtil.isNotEmptyStr(clerk) ? clerk : "");
        list.add(cell8);

        CsvCell cell9 = new CsvCell(StringUtil.isNotEmptyStr(ani) ? ani : "");
        list.add(cell9);

        CsvCell cell10 = new CsvCell(StringUtil.isNotEmptyStr(password) ? password : "");
        list.add(cell10);

        CsvCell cell11 = new CsvCell(NumberUtil.formatCurrency(amount.toString()));
        list.add(cell11);

        //Commissions
        CsvCell cell12 = new CsvCell(NumberUtil.formatCurrency(isoCommission.toString()));
        list.add(cell12);

        CsvCell cell13 = new CsvCell(NumberUtil.formatCurrency(agentCommission.toString()));
        list.add(cell13);

        CsvCell cell14 = new CsvCell(NumberUtil.formatCurrency(subAgentCommission.toString()));
        list.add(cell14);

        CsvCell cell15 = new CsvCell(NumberUtil.formatCurrency(repCommission.toString()));
        list.add(cell15);

        CsvCell cell16 = new CsvCell(NumberUtil.formatCurrency(merchantCommission.toString()));
        list.add(cell16);

        CsvCell cell17 = new CsvCell(bonusAmount != null ? NumberUtil.formatCurrency(bonusAmount.toString()) : "");
        list.add(cell17);

        CsvCell cell18 = new CsvCell(totalRecharge != null ? NumberUtil.formatCurrency(totalRecharge.toString()) : "");
        list.add(cell18);

        CsvCell cell19 = new CsvCell(netAmount != null ? NumberUtil.formatCurrency(netAmount.toString()) : "");
        list.add(cell19);

        CsvCell cell20 = new CsvCell(taxAmount != null ? NumberUtil.formatCurrency(taxAmount.toString()) : "");
        list.add(cell20);

        CsvCell cell21 = new CsvCell(StringUtil.isNotEmptyStr(taxPercentage) ? taxPercentage : "");
        list.add(cell21);

        CsvCell cell22 = new CsvCell(taxType != null ? taxType.toString() : "");
        list.add(cell22);

        CsvCell cell23 = new CsvCell(balance != null ? NumberUtil.formatCurrency(balance.toString()) : "");
        list.add(cell23);

        CsvCell cell24 = new CsvCell(id != null ? id.toString() : "");
        list.add(cell24);

        CsvCell cell25 = new CsvCell(StringUtil.isNotEmptyStr(description) ? description : "");
        list.add(cell25);

        CsvCell cell26 = new CsvCell(controlNo != null ? controlNo.toString() : "");
        list.add(cell26);

        CsvCell cell27 = new CsvCell(StringUtil.isNotEmptyStr(otherInfo) ? otherInfo : "");
        list.add(cell27);

        CsvCell cell28 = new CsvCell(StringUtil.isNotEmptyStr(additionalData) ? additionalData : "");
        list.add(cell28);

        CsvCell cell29 = new CsvCell(StringUtil.isNotEmptyStr(trmDescription) ? trmDescription : "");
        list.add(cell29);

        CsvCell cell30 = new CsvCell(StringUtil.isNotEmptyStr(pin) ? pin : "");
        list.add(cell30);

        CsvCell cell31 = new CsvCell(StringUtil.isNotEmptyStr(achDatetime) ? achDatetime : "");
        list.add(cell31);

        CsvCell cell32 = new CsvCell(StringUtil.isNotEmptyStr(transactionType) ? transactionType : "");
        list.add(cell32);

        CsvCell cell33 = new CsvCell(invoiceNo != null ? invoiceNo.toString() : "");
        list.add(cell33);

        CsvCell cell34 = new CsvCell(StringUtil.isNotEmptyStr(info) ? info : "");
        list.add(cell34);

        CsvCell cell35 = new CsvCell(productTransType != null ? productTransType.toString() : "");
        list.add(cell35);

        CsvCell cell36 = new CsvCell(externalMerchantId != null ? externalMerchantId : "");
        list.add(cell36);

        return list;
    }

    /**
     * @return the region
     */
    public Integer getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(Integer region) {
        this.region = region;
    }

    /**
     * @return the providerId
     */
    public Integer getProviderId() {
        return providerId;
    }

    /**
     * @param providerId the providerId to set
     */
    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    /**
     * @return the providerName
     */
    public String getProviderName() {
        return providerName;
    }

    /**
     * @param providerName the providerName to set
     */
    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

}
