package net.emida.supportsite.reports.trxByRegion.dao;

import net.emida.supportsite.reports.trxByRegion.ReportByRegionData;
import net.emida.supportsite.reports.trxByRegion.results.TransactionsByRegionReportResult;

/**
 * @author Emida
 */
public interface ITransactionsByRegionReportDao {

    long count(ReportByRegionData param);

    TransactionsByRegionReportResult getResult(ReportByRegionData param);
}
