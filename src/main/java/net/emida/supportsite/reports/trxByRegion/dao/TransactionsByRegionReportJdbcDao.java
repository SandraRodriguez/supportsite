package net.emida.supportsite.reports.trxByRegion.dao;

import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.mvc.timezones.TimeZoneDates;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.reports.trxByRegion.ReportByRegionData;
import net.emida.supportsite.reports.trxByRegion.TransactionsByRegionReportModel;
import net.emida.supportsite.reports.trxByRegion.mappers.TransactionsModelByRegionRowMapper;
import net.emida.supportsite.reports.trxByRegion.results.TransactionsByRegionReportResult;

/**
 * @author Emida
 */
@Repository
public class TransactionsByRegionReportJdbcDao implements ITransactionsByRegionReportDao {

    private static final Logger log = LoggerFactory.getLogger(TransactionsByRegionReportJdbcDao.class);

    @Autowired
    @Qualifier("replicaJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Override
    public long count(ReportByRegionData param) {
        try {
            //FROM
            String fromQuery = buildFromQuery(param);

            //WHERE
            String whereQuery = buildWhereQuery(param);

            String countQuery = "SELECT COUNT(1)  " + fromQuery + " " + whereQuery;
            return jdbcTemplate.queryForObject(countQuery, Long.class);
        } catch (EmptyResultDataAccessException ex) {
            return 0;
        } catch (Exception ex) {
            log.error("Error in Count query. ex:{}", ex);
        }
        return 0;
    }

    @Override
    public TransactionsByRegionReportResult getResult(ReportByRegionData param) {

        try {
            log.info("Building merchant transaction report for refId:{}. With dates :{}", param.getRefId(), param.getTimeZonedDates());

            //SELECT
            String selectQuery = buildSelectQuery(param);

            //FROM
            String fromQuery = buildFromQuery(param);

            //WHERE
            String whereQuery = buildWhereQuery(param);

            StringBuilder mainQuery = new StringBuilder()
                    .append(selectQuery).append(" ")
                    .append(fromQuery).append(" ")
                    .append(whereQuery);

            log.debug("Query Transaction Report :{}", mainQuery.toString());

            long totalRecords = count(param);

            ///***   PAGINATION   ***\\\
            param.setSortFieldName(StringUtils.isEmpty(param.getSortFieldName()) ? "recId" : param.getSortFieldName());
            param.setSortOrder(StringUtils.isEmpty(param.getSortOrder()) ? "DESC" : param.getSortOrder());

            mainQuery = getQuery(param, mainQuery);

            log.debug("Paginated Query Transaction Report :{}", mainQuery.toString());

            TransactionsModelByRegionRowMapper rowMapper = new TransactionsModelByRegionRowMapper(param.isShowQrCodeTransactions(),
                    param.isShowCommissionDetails(), param.getDefaultTimeZoneInfo(), param.getEntityTimeZoneInfo());
            List<TransactionsByRegionReportModel> lst = jdbcTemplate.query(mainQuery.toString(), rowMapper);

            log.info("Total retrieved records :{}", lst != null ? lst.size() : 0);

            TransactionsByRegionReportResult result = new TransactionsByRegionReportResult();
            result.setBody(lst);
            result.setTotalRecords(totalRecords);

            return result;
        } catch (EmptyResultDataAccessException ex) {
            TransactionsByRegionReportResult result = new TransactionsByRegionReportResult();
            result.setBody(new ArrayList<TransactionsByRegionReportModel>());
            result.setTotalRecords(0L);
            return result;
        } catch (Exception ex) {
            log.error("Error in main query for transactions. Ex:{}", ex);
        }
        TransactionsByRegionReportResult result = new TransactionsByRegionReportResult();
        result.setBody(new ArrayList<TransactionsByRegionReportModel>());
        result.setTotalRecords(0L);
        return result;
    }

    private StringBuilder getQuery(ReportByRegionData param, StringBuilder mainQuery) {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(param.getSortFieldName()).append(" ").append(param.getSortOrder()).append(") AS RWN, * FROM (");
        sb0.append(mainQuery);
        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(param.getStartPage()).append(" AND ").append(param.getEndPage());
        return sb0;
    }

    private String buildSelectQuery(ReportByRegionData param) {

        StringBuilder selectQuery = new StringBuilder();
        mainSelect(param.getRefId(), Integer.parseInt(param.getAccessLevel()), selectQuery);

        if (param.isWithTax()) {
            queryWithTaxPerm(param.getStrTax(), selectQuery);
        } else if (param.isIncludeTax()) {
            selectQuery.append(", (wt.amount / ").append(param.getStrTax()).append(") AS").append(" amount ");
        } else {
            selectQuery.append(", wt.amount AS").append(" amount ");
        }

        if (param.isViewPin()) {
            selectQuery.append(", t.pin AS pin ");
        }
        if (param.isWithAchDate()) {
            selectQuery.append(", ach_t.Process_Date AS").append(" achDatetime ");
        }
        if (param.isViewReferenceCard()) {
            addReferenceCardFields(selectQuery);
        }
        if (param.isIncludeAdditionalData()) {
            selectQuery.append(", wt.additionalData AS additionalData ");
        }

        if (param.showExternalMerchantId()) {
            selectQuery.append(", wt.externalMerchantId AS externalMerchantId ");
        }

        return selectQuery.toString();
    }

    private String buildFromQuery(ReportByRegionData param) {

        String strAccessLevel = String.valueOf(param.getAccessLevel());

        StringBuilder fromQuery = new StringBuilder();
        fromQuery.append(" FROM web_transactions wt WITH(NOLOCK) INNER JOIN products p WITH(NOLOCK) ON p.id = wt.id ");
        
        if (param.isViewPin() || param.isPinNotEmpty()) {
            fromQuery.append("INNER JOIN transactions t WITH(NOLOCK) on (wt.rec_id = t.rec_id) ");
        }/*
        if (param.isWithAchDate()) {
            fromQuery.append("LEFT OUTER JOIN ach_transaction_Audit ach_t WITH(NOLOCK) on (wt.rec_id = ach_t.rec_id and wt.merchant_id = ach_t.entity_id) ");
        }
        if (param.isViewReferenceCard()) {
            fromQuery.append("LEFT OUTER JOIN hostTransactionsrecord htr WITH(NOLOCK) on htr.transaction_id = wT.rec_id ");
        }*/
        fromQuery.append("INNER JOIN terminals WITH(NOLOCK) on wt.millennium_no = terminals.millennium_no ");
        fromQuery.append("INNER JOIN terminal_types WITH(NOLOCK) on terminals.terminal_type = terminal_types.terminal_type ");

        boolean merchantReportCase14 = false; //param.getReportType() == TransactionsReportType.MERCHANT_REPORT_CASE_14;
        boolean levelCarrier = strAccessLevel.equals(DebisysConstants.CARRIER);
        if (merchantReportCase14 && levelCarrier) {
            Long merchantId = param.getMerchantIds() != null && !param.getMerchantIds().isEmpty() ? param.getMerchantIds().get(0) : null;
            if (merchantId == null) {
                log.error("Invalid value for merchantId :{}, in query", merchantId);
            }
            fromQuery.append(" INNER JOIN merchantCarriers mc WITH (NOLOCK) ON mc.merchantId = wt.merchant_id and mc.carrierId = p.carrier_id ")
                    .append(" AND wt.merchant_id =").append(merchantId).append(" ");
        }

        fromQuery.append("LEFT JOIN rateplan rp WITH(NOLOCK) ON terminals.Rateplanid = rp.RatePlanID ");
        fromQuery.append("LEFT JOIN receiptcache rc with(nolock) ON rc.transactionId =  wt.rec_id  ");
        fromQuery.append("INNER JOIN ProviderCredentialsHistory pch WITH(NOLOCK) ON pch.transactionId = wt.rec_id   ");
        fromQuery.append("INNER JOIN provider pr      WITH(NOLOCK) ON p.program_id = pr.provider_id   ");
        
        return fromQuery.toString();
    }

    private String buildWhereQuery(ReportByRegionData param) {

        //***************************************************************************
        //NOTA : OJO
        //Este codigo simula lo que esta en la clase com.debisys.reports.schedule.TransactionReport
        // en el metodo generateSqlString().
        //***************************************************************************
        String strAccessLevel = String.valueOf(param.getAccessLevel());
        String strDistChain = String.valueOf(param.getDistChainType());
        Long repId = param.getRepIds() != null && !param.getRepIds().isEmpty() ? param.getRepIds().get(0) : null;
        Long merchantId = param.getMerchantIds() != null && !param.getMerchantIds().isEmpty() ? param.getMerchantIds().get(0) : null;

        StringBuilder whereQuery = new StringBuilder();
        whereQuery.append(" WHERE 1=1 ");

        if (levelIso3(strAccessLevel, strDistChain)) {
            addIsoCarrierWhere_LevelIso3(param.getRefId(), whereQuery);
        } // LEVEL ISO 5
        else if (levelIso5(strAccessLevel, strDistChain)) {
            addIsoCarrierWhere_LevelIso5(param.getRefId(), whereQuery);
        }

        if (param.isValidDates()) {
            addDatesWhere(param.getTimeZonedDates(), whereQuery);
        }
        return whereQuery.toString();
    }

    /////  WHERE for ISO/CARRIER Report \\\\\\\
    private void addIsoCarrierWhere_LevelCarrier(long refId, List<Long> repIds, StringBuilder whereQuery) {
        if (repIds != null && !repIds.isEmpty()) {
            String repQuery;
            if (repIds.isEmpty()) {
                repQuery = " SELECT m.rep_id FROM merchants m WITH (NOLOCK) "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.merchantid "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "INNER JOIN products p WITH (NOLOCK) ON rc.CarrierID = p.carrier_id AND wt.id = p.id WHERE " + "rc.repid = " + refId;
            } else {
                String joinedRepIds = StringUtils.join(repIds, ",");
                repQuery = " SELECT rep_id FROM reps WITH (nolock) WHERE type=1 and iso_id IN "
                        + "(SELECT rep_id FROM reps WITH (nolock) WHERE type=5 and iso_id IN "
                        + "(SELECT rep_id FROM reps WITH (nolock) WHERE type=4 and iso_id IN "
                        + "(SELECT rep_id FROM reps WITH (nolock) WHERE type IN (2,3) AND rep_id IN (" + joinedRepIds + ")))) "
                        + "AND rep_id IN (SELECT DISTINCT rep_id FROM Merchants m WITH (NOLOCK) "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID AND m.merchant_id = wt.merchant_id "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "INNER JOIN Products p WITH (NOLOCK) ON rc.CarrierID = p.carrier_id AND wt.id = p.id " + "WHERE rc.repid = " + refId
                        + ")";
            }
            whereQuery.append(" AND wt.rep_id IN(").append(repQuery).append(") ");
        }
    }

    private void addIsoCarrierWhere_LevelIso5(long refId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id IN(")
                .append("select rep_id from reps with (nolock) where iso_id in(")
                .append("select rep_id from reps with (nolock) where iso_id in(")
                .append("select rep_id from reps with (nolock) where iso_id = ").append(refId)
                .append(")")
                .append(")")
                .append(") ");
    }

    private void addIsoCarrierWhere_LevelIso3(long refId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id IN(")
                .append("select rep_id from reps with (nolock) where iso_id =").append(refId)
                .append(") ");
    }

    /////// WHERE for Rep Report \\\\\\\\\\
    private void addRepWhere_LevelCarrier(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id IN(")
                .append("SELECT DISTINCT r.rep_id FROM reps AS r WITH(nolock) ")
                .append("INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id ")
                .append("INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id ")
                .append("INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid ")
                .append("INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id ")
                .append("WHERE ta.repid =").append(refId).append(" AND wt.rep_id=").append(repId)
                .append(") ");
    }

    private void addRepWhere_LevelSubAgent(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id =").append(repId).append(" AND wt.rep_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(refId)
                .append(") ");
    }

    private void addRepWhere_LevelAgent(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id =").append(repId).append(" AND wt.rep_id IN(")
                .append("SELECT rep_id FROm reps WITH(NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id =").append(refId)
                .append(")")
                .append(") ");
    }

    private void addRepWhere_LevelIso3(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id =").append(repId).append(" AND wt.rep_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id=").append(refId)
                .append(") ");
    }

    private void addRepWhere_LevelIso5(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id = ").append(repId).append(" AND wt.rep_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_AGENT).append(" AND iso_id =").append(refId)
                .append(")")
                .append(")")
                .append(") ");
    }

    /////// WHERE for SubAgent Report \\\\\\\\\\
    private void addSubAgentWhere_LevelSubAgent(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id in(")
                .append("select rep_id from reps with (nolock) where type = 1 ").append(" and iso_id = ").append(refId)
                .append(") ");
    }

    private void addSubAgentWhere_LevelAgent(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id in(")
                .append("select rep_id from reps with (nolock) where type = 1 and iso_id in(")
                .append("select rep_id from reps with (nolock) where type = 5 AND rep_id = ").append(repId).append(" and iso_id = ").append(refId)
                .append(")")
                .append(") ");
    }

    private void addSubAgentWhere_LevelIso(long refId, long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id in (")
                .append("select rep_id from reps with (nolock) where type = 1 and iso_id IN(")
                .append("select rep_id from reps with (nolock) where type = 5 and rep_id = ").append(repId).append(" AND iso_id in(")
                .append("select rep_id from reps with (nolock) where type = 4 and iso_id = ").append(refId)
                .append(")")
                .append(")")
                .append(") ");
    }

    private void addSubAgentWhere_LevelCarrier(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id in (")
                .append("select rep_id from reps with (nolock) where type=1 and iso_id IN(")
                .append("select rep_id from reps with (nolock) where type = 5 ").append("and rep_id = ").append(repId).append(" and rep_id IN(")
                .append("SELECT DISTINCT R1.rep_id from reps AS r with(nolock) ")
                .append("INNER JOIN dbo.reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id ")
                .append("INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id ")
                .append("INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON mc.merchantId = merchants.merchant_id ")
                .append("INNER JOIN RepCarriers ta WITH (NOLOCK)   on ta.carrierid = mc.carrierId ")
                .append("INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierId = p.carrier_id ")
                .append("WHERE ta.repId = ").append(refId)
                .append(")")
                .append(")")
                .append(") ");
    }

    /////// WHERE for Agent Report \\\\\\\\\\
    private void addAgentWhere_LevelAgent(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id IN(")
                .append("select rep_id from reps with (nolock) where type = 1 and iso_id IN(")
                .append("select rep_id from reps with (nolock) where type = 5 and iso_id = ").append(refId)
                .append(")")
                .append(") ");
    }

    private void addAgentWhere_LevelIso(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=1 AND iso_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=5 AND iso_id IN(")
                .append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=4 ").append(" AND rep_id=").append(repId).append(" AND iso_id =").append(refId)
                .append(")")
                .append(")")
                .append(") ");
    }

    private void addAgentWhere_LevelCarrier(long refId, Long repId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.rep_id in (")
                .append("SELECT rep_id FROM reps WITH (nolock) WHERE type=1 AND iso_id IN(")
                .append("SELECT rep_id FROM reps WITH (nolock) WHERE type=5 AND iso_id IN(")
                .append("SELECT rep_id FROM reps WITH (nolock) WHERE type=4 AND rep_id=").append(repId).append(" AND rep_id IN (")
                .append("SELECT DISTINCT r2.rep_id FROM reps AS r WITH(nolock) ")
                .append("INNER JOIN dbo.reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id ")
                .append("INNER JOIN reps AS r2 WITH (nolock) ON r1.iso_id = r2.rep_id ")
                .append("INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id ")
                .append("INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id ")
                .append("INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid ")
                .append("INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id ")
                .append("WHERE ta.repid =").append(refId)
                .append(")")
                .append(")")
                .append(")")
                .append(") ");
    }

    /////// WHERE for Merchant Report \\\\\\\\\\
    private void addMerchantWhere_LevelCarrier(long refId, List<Long> merchantIds, StringBuilder where) {
        Long merchantId = merchantIds.get(0);
        if (merchantId == null) {
            log.error("Error getting merchantId for query.");
        }

        where
                .append(" AND wt.rep_id in(")
                .append("SELECT DISTINCT merchants.rep_id FROM merchants WITH (NOLOCK) ")
                .append("INNER JOIN reps WITH (NOLOCK) ON merchants.rep_id = reps.rep_id ")
                .append("LEFT JOIN Routes WITH (NOLOCK) ON merchants.RouteID = Routes.RouteID ")
                .append("INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid = merchants.merchant_id ")
                .append("INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid ")
                .append("INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id ")
                .append("WHERE ta.repid = ").append(refId).append(" AND wt.merchant_id=").append(merchantId);
    }

    private void addMerchantWhere_LevelSubAgent(long merchantId, long refId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.merchant_id = ").append(merchantId).append(" AND wt.rep_id in ( ")
                .append("select rep_id from reps with (nolock) where type = ").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(refId)
                .append(") ");
    }

    private void addMerchantWhere_LevelAgent(long merchantId, long refId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.merchant_id = ").append(merchantId).append(" AND wt.rep_id in ( ")
                .append("select rep_id from reps with (nolock) where type = ").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(")
                .append("select rep_id from reps with (nolock) where type = ").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id =").append(refId)
                .append(")")
                .append(") ");
    }

    private void addMerchantWhere_LevelIso5(long merchantId, long refId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.merchant_id = ").append(merchantId).append(" AND wt.rep_id in ( ")
                .append("select rep_id from reps with (nolock) where type = ").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(")
                .append("select rep_id from reps with (nolock) where type = ").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id IN(")
                .append("select rep_id from reps with (nolock) where type = ").append(DebisysConstants.REP_TYPE_AGENT).append(" and iso_id = ").append(refId)
                .append(")")
                .append(")")
                .append(") ");
    }

    private void addMerchantWhere_LevelIso3(long merchantId, long refId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND wt.merchant_id = ").append(merchantId).append(" AND wt.rep_id in( ")
                .append("select rep_id from reps with (nolock) where iso_id = ").append(refId)
                .append(") ");
    }

    private void addMerchantWhere_LevelCarrier_2(long refId, StringBuilder whereQuery) {
        whereQuery
                .append(" AND mc.carrierId IN (")
                .append("SELECT carrierId FROM RepCarriers WITH(NOLOCK) WHERE repId = ").append(refId)
                .append(") ");
    }

    private boolean levelIso5(String strAccessLevel, String strDistChain) {
        return strAccessLevel.equals(DebisysConstants.ISO) && strDistChain.equals(DebisysConstants.DIST_CHAIN_5_LEVEL);
    }

    private boolean levelIso3(String strAccessLevel, String strDistChain) {
        return strAccessLevel.equals(DebisysConstants.ISO) && strDistChain.equals(DebisysConstants.DIST_CHAIN_3_LEVEL);
    }

    private void mainSelect(long refId, int accessLevel, StringBuilder sb) {
        sb
                .append("SELECT DISTINCT wt.rec_id AS").append(" recId ")
                .append(", wt.invoice_no AS").append(" invoiceNo ")
                .append(", wt.millennium_no AS").append(" millenniumNo ")
                .append(", terminal_types.description AS").append(" trmDescription ")
                .append(", wt.dba AS").append(" dba ")
                .append(", wt.phys_city AS").append(" physCity ")
                .append(", wt.phys_county AS").append(" physCounty ")
                .append(", wt.phys_state AS").append(" physState ")
                .append(", wt.datetime AS").append(" datetime ")
                //.append(", dbo.GetLocalTimeByAccessLevel(").append(accessLevel).append(",").append(refId).append(", wt.datetime, 1) AS").append(" datetime ")
                .append(", wt.merchant_id AS").append(" merchantId ")
                .append(", wt.control_no AS").append(" controlNo ")
                .append(", wt.clerk_name AS").append(" clerkName ")
                .append(", wt.clerk AS ").append(" clerk ")
                .append(", wt.transaction_type AS").append(" transactionType ")
                .append(", wt.ani AS").append(" ani ")
                .append(", wt.isQRCode AS").append(" qrCode ")
                .append(", wt.other_info AS").append(" otherInfo ")
                .append(", wt.password AS").append(" password ")
                .append(", wt.balance AS").append(" balance ")
                .append(", wt.id AS").append(" id ")
                .append(", wt.description AS").append(" description ")
                .append(", wt.product_trans_type AS").append(" productTransType ")
                .append(", wt.bonus_amount AS").append(" bonusAmount ")
                .append(", (wt.bonus_amount + wt.amount) AS").append(" totalRecharge ")
                .append(", wt.taxtype AS").append(" taxType ")
                .append(", rp.Name AS").append(" ratePlanName ")
                .append(", CAST(round(CAST(ISNULL(wt.taxpercentage,0) / 100  AS float),2) AS VARCHAR(10)) + '%' AS").append(" taxPercentage ")
                .append(", (wt.merchant_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.amount - p.Fixed_fee ELSE wt.amount END)) END) AS ").append(" merchantCommission ")
                .append(", (wt.rep_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.amount - p.Fixed_fee ELSE wt.amount END)) END) AS ").append(" repCommission ")
                .append(", (wt.agent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.amount - p.Fixed_fee ELSE wt.amount END)) END) AS ").append(" agentCommission ")
                .append(", (wt.subagent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.amount - p.Fixed_fee ELSE wt.amount END)) END) AS ").append(" subAgentCommission ")
                .append(", (wt.iso_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.amount - p.Fixed_fee ELSE wt.amount END)) END) AS ").append(" isoCommission ")
                .append(", rc.Receipt AS receipt, pch.region,  pr.name AS providerName, pr.provider_id AS providerId ");
    }

    private void queryWithTaxPerm(String strTax, StringBuilder sb) {
        sb.append(", wt.amount AS").append(" amount ");
        sb.append(", wt.amount / (1 + isnull(wt.taxpercentage / 10000, ").append(strTax).append(" - 1)) AS").append(" netAmount ");
        sb.append(", wt.amount - wt.amount / (1 + isnull(wt.taxpercentage / 10000, ").append(strTax).append(" - 1)) AS").append(" taxAmount ");
    }

    private void addReferenceCardFields(StringBuilder mainQuery) {
        mainQuery.append(", CASE htr.info1 WHEN NULL THEN '' ELSE CASE WHEN (CHARINDEX('s', htr.info1) > 0) and (CHARINDEX('=', htr.info1) > 0) and (htr.info2 != wt.ani) and (LEN(WT.ani) > 0)");
        mainQuery.append(" THEN RIGHT(htr.info1, (LEN(htr.info1) - 1)) ELSE '' END END info ");
    }

    private void addAllMerchantWhere(long refId, String accessLevel, String distChainType, StringBuilder wq) {
        if (levelIso3(accessLevel, distChainType)) {
            wq.append(" AND wt.rep_id IN(SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = ").append(refId).append(") ");
        } else if (levelIso5(accessLevel, distChainType)) {
            wq.append(" AND wt.rep_id IN(")
                    .append("SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(")
                    .append("SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id IN(")
                    .append("SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_AGENT).append(" AND iso_id =").append(refId)
                    .append("))) ");
        } else if (accessLevel.equals(DebisysConstants.AGENT)) {
            wq.append(" AND wt.rep_id IN(")
                    .append("SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(")
                    .append("SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id =").append(refId)
                    .append(")) ");
        } else if (accessLevel.equals(DebisysConstants.SUBAGENT)) {
            wq.append(" AND wt.rep_id IN(")
                    .append("SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(refId)
                    .append(") ");
        } else if (accessLevel.equals(DebisysConstants.REP)) {
            wq.append(" AND wt.rep_id=").append(refId).append(" ");
        } else if (accessLevel.equals(DebisysConstants.MERCHANT)) {
            wq.append(" AND wt.merchant_id =").append(refId).append(" ");
        }
    }

    private void addMerchantWhere(List<Long> merchantIds, StringBuilder whereQuery) {
        if (merchantIds != null && !merchantIds.isEmpty()) {
            String joinedIds = StringUtils.join(merchantIds, ",");
            if (merchantIds.size() == 1) {
                whereQuery.append(" AND wt.merchant_id =").append(joinedIds);
            } else {
                whereQuery.append(" AND wt.merchant_id IN ( ").append(joinedIds).append(") ");
            }
        }
    }

    private void addTransactionWhere(String transactionId, StringBuilder whereQuery) {
        if (StringUtils.isNotEmpty(transactionId)) {
            whereQuery.append(" AND wt.rec_id = ").append(transactionId).append(" ");
        }
    }

    private void addInvoiceWhere(String invoiceNumber, StringBuilder whereQuery) {
        if (StringUtils.isNotEmpty(invoiceNumber)) {
            whereQuery.append(" AND wt.invoice_no = ").append(invoiceNumber).append(" ");
        }
    }

    private void addS2KRatePlanFilterWhere(boolean permViewFilterS2KRatePlan, List<String> ratePlanIds, StringBuilder whereQuery) {
        if (permViewFilterS2KRatePlan && !CollectionUtils.isEmpty(ratePlanIds)) {
            String joinedIds = StringUtils.join(ratePlanIds, ",");
            whereQuery.append(" AND (rp.RatePlanID IN (").append(joinedIds).append(")) ");
        }
    }

    private void addMillenniumNoWhere(List<Long> millenniumNum, StringBuilder whereQuery) {
        if (millenniumNum != null && !millenniumNum.isEmpty()) {
            String joinedIds = StringUtils.join(millenniumNum, ",");
            whereQuery.append(" AND wt.millennium_no IN (").append(joinedIds).append(") ");
        }
    }

    private void addRecordedProductIdWhere(List<Long> productIds, StringBuilder whereQuery) {
        if (productIds != null && !productIds.isEmpty()) {
            String joinedIds = StringUtils.join(productIds, ",");
            whereQuery.append(" AND wt.RecordedProductid IN (").append(joinedIds).append(") ");
        }
    }

    private void addPinWhere(String pin, StringBuilder whereQuery) {
        //String pin = pinService.getPin(pinNumber);
        whereQuery.append(" AND wt.product_trans_type = 2 ");
        whereQuery.append(" AND wt.amount > 0 ");
        whereQuery.append(" AND t.pin = '").append(pin).append("' ");
    }

    private void addDatesWhere(TimeZoneDates tzd, StringBuilder whereQuery) {
        String initDate = DateUtil.formatSqlDateTimeMillis(tzd.getStartDate());
        String finishDate = DateUtil.formatSqlDateTimeMillis(tzd.getEndDate());
        whereQuery.append(" AND (wt.datetime  >= '").append(initDate).append("' ");
        whereQuery.append(" AND wt.datetime  < '").append(finishDate).append("') ");
    }

}
