
package net.emida.supportsite.reports.switchAccount.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author emida
 */
public class SwitchAccountUtil {
    
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

    
    public static final boolean validateStr(String field) {
        return (field != null && !field.isEmpty());
    } 
    
    public static final boolean validateDate(String date) {
        try {
            format.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
    
    public static final boolean validateInteger(String number) {
        try {
            return number.matches("\\d+");
        } catch (Throwable e) {
            return false;
        }
    }

}
