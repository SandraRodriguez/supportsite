
package net.emida.supportsite.reports.switchAccount.model;

import net.emida.supportsite.reports.switchAccount.util.SwitchAccountUtil;

/**
 *
 * @author emida
 */
public class SwitchAccountRequest {
    
    private String startDate;
    private String endDate;
    private String terminalId;
    private String merchantId;
    private boolean download;
    
    public SwitchAccountRequest() {}

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public boolean isDownload() {
        return download;
    }

    public void setDownload(boolean download) {
        this.download = download;
    }
    
    @Override
    public String toString() {
        return "StartDate : " + (SwitchAccountUtil.validateStr(this.startDate) ? this.startDate : "") + " EndDate : " + (SwitchAccountUtil.validateStr(this.endDate) ? this.endDate : "") + " Terminal Id : " + (SwitchAccountUtil.validateStr(this.terminalId) ? this.terminalId : "") + " Merchant Id : " + (SwitchAccountUtil.validateStr(this.merchantId) ? this.merchantId : "");
    }
}
