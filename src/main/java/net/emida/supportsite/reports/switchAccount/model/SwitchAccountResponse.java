
package net.emida.supportsite.reports.switchAccount.model;

/**
 *
 * @author emida
 */
public class SwitchAccountResponse {
    
    /**
     * Associated Data
     */
    private String dba;
    private Long merchantId;
    private Long terminalId;
    private Long associatedTerminalId;
    private String terminalType;
    private String terminalLabel;
    private String terminalCreate;
    private String terminalBranding;
    private String terminalVersion;
    private String associatedDate;
    
    /**
     * Owner Data
     */
    private String ownerDba;
    private Long ownerMerchantId;
    private Long ownerTerminalId;
    private String ownerUser;
    private String ownerTerminalType;
    private String ownerTerminalLabel;
    private String ownerTerminalCreate;
    private String ownerTerminalBranding;
    private String ownerTerminalVersion;
    
    public SwitchAccountResponse () {}
    
    
    public String getDba() {
        return dba;
    }

    public void setDba(String dba) {
        this.dba = dba;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(Long terminalId) {
        this.terminalId = terminalId;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getTerminalLabel() {
        return terminalLabel;
    }

    public void setTerminalLabel(String terminalLabel) {
        this.terminalLabel = terminalLabel;
    }

    public String getTerminalCreate() {
        return terminalCreate;
    }

    public void setTerminalCreate(String terminalCreate) {
        this.terminalCreate = terminalCreate;
    }

    public String getTerminalBranding() {
        return terminalBranding;
    }

    public void setTerminalBranding(String terminalBranding) {
        this.terminalBranding = terminalBranding;
    }

    public String getTerminalVersion() {
        return terminalVersion;
    }

    public void setTerminalVersion(String terminalVersion) {
        this.terminalVersion = terminalVersion;
    }

    public String getAssociatedDate() {
        return associatedDate;
    }

    public void setAssociatedDate(String associatedDate) {
        this.associatedDate = associatedDate;
    }

    public Long getAssociatedTerminalId() {
        return associatedTerminalId;
    }

    public void setAssociatedTerminalId(Long associatedTerminalId) {
        this.associatedTerminalId = associatedTerminalId;
    }
    
    public String getOwnerDba() {
        return ownerDba;
    }

    public void setOwnerDba(String ownerDba) {
        this.ownerDba = ownerDba;
    }

    public Long getOwnerMerchantId() {
        return ownerMerchantId;
    }

    public void setOwnerMerchantId(Long ownerMerchantId) {
        this.ownerMerchantId = ownerMerchantId;
    }

    public Long getOwnerTerminalId() {
        return ownerTerminalId;
    }

    public void setOwnerTerminalId(Long ownerTerminalId) {
        this.ownerTerminalId = ownerTerminalId;
    }

    public String getOwnerTerminalType() {
        return ownerTerminalType;
    }

    public void setOwnerTerminalType(String ownerTerminalType) {
        this.ownerTerminalType = ownerTerminalType;
    }

    public String getOwnerTerminalLabel() {
        return ownerTerminalLabel;
    }

    public void setOwnerTerminalLabel(String ownerTerminalLabel) {
        this.ownerTerminalLabel = ownerTerminalLabel;
    }

    public String getOwnerTerminalCreate() {
        return ownerTerminalCreate;
    }

    public void setOwnerTerminalCreate(String ownerTerminalCreate) {
        this.ownerTerminalCreate = ownerTerminalCreate;
    }

    public String getOwnerTerminalBranding() {
        return ownerTerminalBranding;
    }

    public void setOwnerTerminalBranding(String ownerTerminalBranding) {
        this.ownerTerminalBranding = ownerTerminalBranding;
    }

    public String getOwnerTerminalVersion() {
        return ownerTerminalVersion;
    }

    public void setOwnerTerminalVersion(String ownerTerminalVersion) {
        this.ownerTerminalVersion = ownerTerminalVersion;
    }
    
    public String getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(String ownerUser) {
        this.ownerUser = ownerUser;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SwitchAccountResponse) {
            if ((this.associatedTerminalId.longValue() != this.terminalId.longValue())) {
                return true;
            }
        }
        return false;
    }
   
    @Override
    public int hashCode() {
        return 0;
    }
}
