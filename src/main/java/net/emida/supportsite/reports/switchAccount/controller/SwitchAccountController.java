package net.emida.supportsite.reports.switchAccount.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpSession;
import net.emida.supportsite.mvc.BaseController;
import net.emida.supportsite.mvc.UrlPaths;
import net.emida.supportsite.reports.switchAccount.model.SwitchAccountRequest;
import net.emida.supportsite.reports.switchAccount.model.SwitchAccountResponse;
import net.emida.supportsite.reports.switchAccount.services.ISwitchAccountRepository;
import net.emida.supportsite.reports.switchAccount.util.SwitchAccountUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author emida
 */
@Controller
@RequestMapping(value = UrlPaths.REPORT_PATH + UrlPaths.REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT)
public class SwitchAccountController extends BaseController {
    
    private static final Logger _logger = Logger.getLogger(SwitchAccountController.class);
    
    @Autowired
    private ISwitchAccountRepository switchService;
    
    @RequestMapping(value = UrlPaths.REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT_VIEW, method = RequestMethod.GET)
    public String __init__() {
        return "reports/switchAccount/switchAccount";
    }
    
    @ResponseBody
    @RequestMapping(value = UrlPaths.REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT_GET_ALL_TERMINALS, method = RequestMethod.POST)
    public List getFoundTerminals(HttpSession session, SwitchAccountRequest request) {
        List<SwitchAccountResponse> terminals = new ArrayList<SwitchAccountResponse>();
        try {
            if (this.validateRequest(request)) {
                terminals = this.switchService.getAllTerminals(request);
                Set<SwitchAccountResponse> owners = this.removeDuplicatesObjectList(terminals);
                this.buildDatatable(terminals, owners);
            }
            return terminals;
            
        } catch (Exception e) {
            _logger.error("SwitchAccountController.getFoundTerminals - ERROR! REMOVING DUPLICATES TO GET OWNERS TERMINALS :  " + e);
            return new ArrayList<>();
        }
    }
    
    private boolean validateRequest(SwitchAccountRequest request) {
        try {
            return (request != null && SwitchAccountUtil.validateDate(request.getStartDate()) && SwitchAccountUtil.validateDate(request.getEndDate()) && 
                     (((SwitchAccountUtil.validateInteger(request.getTerminalId()))) || (SwitchAccountUtil.validateInteger(request.getMerchantId()))));
        } catch (Exception e) {
            return false;
        }
    }
    
    private Set<SwitchAccountResponse> removeDuplicatesObjectList(List<SwitchAccountResponse> terminals) {
        Set<SwitchAccountResponse> terminalsOwners = new HashSet<SwitchAccountResponse>();
        try {
            terminalsOwners.addAll(terminals);
            return terminalsOwners;
        } catch (Exception e) {
            _logger.error("SwitchAccountController.removeDuplicatesObjectList - ERROR! REMOVING DUPLICATES TO GET OWNERS TERMINALS :  " + e);
            return new HashSet<SwitchAccountResponse>();
        }
    }
    
    
    private void buildDatatable(List<SwitchAccountResponse> associatedTerminals, Set<SwitchAccountResponse> owners) {
        try {
            this.removeOwnerRow(associatedTerminals);
            for (SwitchAccountResponse associatedTerminal : associatedTerminals) {
                this.findAndSetOwnerData(owners, associatedTerminal);
            }
        } catch (Exception e) {
            _logger.error("SwitchAccountController.buildDatatable - ERROR! ADDING ADDITIONAL INFO OWNER FOR EACH ASSOCIATED TERMINAL  :  " + e);
        }
    }
    
    private void removeOwnerRow(List<SwitchAccountResponse> associatedTerminals) {
        try {
            for (Iterator<SwitchAccountResponse> iterator = associatedTerminals.listIterator(); iterator.hasNext();) {
                SwitchAccountResponse associatedTerminal = iterator.next();
                if (associatedTerminal.getTerminalId().longValue() == associatedTerminal.getAssociatedTerminalId().longValue()) {
                    iterator.remove();
                }
            }
        } catch (Exception e) {
            _logger.error("SwitchAccountController.removeOwnerRow - ERROR! REMOVING OWNER ROW FROM LIST  :  " + e);
            
        }
    }
    
    private void findAndSetOwnerData (Set<SwitchAccountResponse> owners, SwitchAccountResponse associatedTerminal) {
        try {
            for (SwitchAccountResponse owner : owners) {
                if ((associatedTerminal != null && (associatedTerminal.getTerminalId() != null)) && (owner.getTerminalId().longValue() == associatedTerminal.getTerminalId().longValue())) {
                    associatedTerminal.setOwnerDba(owner.getDba());
                    associatedTerminal.setOwnerMerchantId(owner.getMerchantId());
                    associatedTerminal.setOwnerTerminalId(owner.getTerminalId());
                    associatedTerminal.setOwnerUser(owner.getOwnerUser());
                    associatedTerminal.setOwnerTerminalType(owner.getTerminalType());
                    associatedTerminal.setOwnerTerminalLabel(owner.getTerminalLabel());
                    associatedTerminal.setOwnerTerminalCreate(owner.getTerminalCreate());
                    associatedTerminal.setOwnerTerminalBranding(owner.getTerminalBranding());
                    associatedTerminal.setOwnerTerminalVersion(owner.getTerminalVersion());
                }           
            }            
        } catch (Exception e) {
            _logger.error("SwitchAccountController.findAndSetOwnerData - ERROR! FINDING OWNER FOR TERMINAL  :  " + e);
        }
    }
    
    @Override
    public int getSection() {
        return 4;
    }

    @Override
    public int getSectionPage() {
        return 67;
    }
}
