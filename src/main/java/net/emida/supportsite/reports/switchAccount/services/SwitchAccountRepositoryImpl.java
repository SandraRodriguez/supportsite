
package net.emida.supportsite.reports.switchAccount.services;

import java.util.ArrayList;
import java.util.List;
import net.emida.supportsite.reports.switchAccount.model.SwitchAccountRequest;
import net.emida.supportsite.reports.switchAccount.model.SwitchAccountResponse;
import net.emida.supportsite.reports.switchAccount.util.SwitchAccountUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author emida
 */
@Repository("ISwitchAccountRepository")
public class SwitchAccountRepositoryImpl implements ISwitchAccountRepository {
    
    private static final Logger _logger = Logger.getLogger(SwitchAccountRepositoryImpl.class);
    
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate parameterJdbcTemplate;
    
    private final String GET_ALL_TERMINALS = "EXEC getTerminalsByDate :terminalId, :merchantId, :startDate, :endDate ";

    
    @Override
    public List<SwitchAccountResponse> getAllTerminals(SwitchAccountRequest request) {
        try {
            _logger.info("SwitchAccountRepositoryImpl.getAllTerminals - QUERY : " + GET_ALL_TERMINALS + "\n PARAMS : " + request.toString());
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("terminalId", (SwitchAccountUtil.validateStr(request.getTerminalId()) ? request.getTerminalId() : null));
            params.addValue("merchantId", (SwitchAccountUtil.validateStr(request.getMerchantId()) ? request.getMerchantId() : null));
            params.addValue("startDate", request.getStartDate());
            params.addValue("endDate", request.getEndDate());
            return this.parameterJdbcTemplate.query(GET_ALL_TERMINALS, params, new BeanPropertyRowMapper(SwitchAccountResponse.class));
        } catch (Exception e) {
            _logger.error("SwitchAccountRepositoryImpl.getAllTerminals - ERROR! RETRIEVING TERMINALS WITH :  " + request.toString() + e);
            return new ArrayList<SwitchAccountResponse>();
        }
    }

    @Autowired @Qualifier("masterJdbcTemplate")
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Autowired @Qualifier("masterNamedParamJdbcTemplate")
    public void setParameterJdbcTemplate(NamedParameterJdbcTemplate parameterJdbcTemplate) {
        this.parameterJdbcTemplate = parameterJdbcTemplate;
    }

}
