package net.emida.supportsite.reports.switchAccount.services;

import java.util.List;
import net.emida.supportsite.reports.switchAccount.model.SwitchAccountRequest;
import net.emida.supportsite.reports.switchAccount.model.SwitchAccountResponse;

/**
 *
 * @author emida
 */
public interface ISwitchAccountRepository {

    List<SwitchAccountResponse> getAllTerminals(SwitchAccountRequest request);
    
}
