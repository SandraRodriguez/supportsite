/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceHistoryByReps.detail;

import java.util.Date;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author g.martinez
 */
public class BalanceHistoryByRepsForm {
 
    private Date startDate;
    private Date endDate;
    private String reps;    

    /**
     * @return the startDate
     */
    @NotNull(message = "{com.debisys.reports.error1}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    @NotNull(message = "{com.debisys.reports.error3}")
    @DateTimeFormat(pattern = "MM/dd/yyyy")    
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public String getReps() {
        return reps;
    }

    public void setReps(String reps) {
        this.reps = reps;
    }
    

    @Override
    public String toString() {
        return "BalanceHistoryModel[" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", reps=" + reps +                
                ']';
    }

}
