/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.emida.supportsite.reports.balanceHistoryByReps.detail;

/**
 *
 * @author dgarzon
 */
public class BalanceHistoryByRepsConstants {
    //filters
    public static final String HISTORY_FILTER_SELECT = "SELECT";
    public static final String HISTORY_FILTER_ENTITY_TYPE = "ENTITY TYPE";
    public static final String HISTORY_FILTER_ENTITY_ID = "ENTITY ID";
 
}
