package net.emida.supportsite.reports.balanceHistoryByReps.detail.dao;

import com.debisys.utils.NumberUtil;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;


/**
 *
 * @author g.martinez
 */
public class BalanceHistoryByRepsMapper implements RowMapper<BalanceHistoryByRepsModel>{

    @Override
    public BalanceHistoryByRepsModel mapRow(ResultSet rs, int i) throws SQLException {
        BalanceHistoryByRepsModel model = new BalanceHistoryByRepsModel();
        model.setRwn(rs.getBigDecimal("rwn"));
        model.setEntityId(rs.getBigDecimal("entityId"));
        model.setOpenBalance(rs.getBigDecimal("openBalance").setScale(2,RoundingMode.HALF_UP));
        model.setTransactions(rs.getBigDecimal("transactions").setScale(2,RoundingMode.HALF_UP));
        model.setDeposits(rs.getBigDecimal("deposits").setScale(2,RoundingMode.HALF_UP));
        model.setCloseBalance(rs.getBigDecimal("closeBalance").setScale(2,RoundingMode.HALF_UP));   
        model.setEntityName(rs.getString("businessname"));
        
        model.setCountSharedBalance1(rs.getInt("countSharedBalance1"));
        model.setCountSharedBalance0(rs.getInt("countSharedBalance0"));
        
        model.setOpenBalanceFormat(NumberUtil.formatCurrency(rs.getString("openBalance")));
        model.setTransactionsFormat(NumberUtil.formatCurrency(rs.getString("transactions")));
        model.setDepositsFormat(NumberUtil.formatCurrency(rs.getString("deposits")));
        model.setCloseBalanceFormat(NumberUtil.formatCurrency(rs.getString("closeBalance")));
       
        checkRepChanges(model);
        
        return model;
    }
    
    private void checkRepChanges(BalanceHistoryByRepsModel model){
        
        if(model.getCountSharedBalance0() > 0){
            model.setEntityName(model.getEntityName()+"  ** ");
        }
    }
}
