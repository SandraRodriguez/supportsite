/**
 * 
 */
package com.debisys.provider;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.TerminalException;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author nmartinez
 *
 */
public class Provider {

	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
	static Category cat = Category.getInstance(Provider.class);
	
	private int provider_id;
	private String name;
	private String classname;
	private String address;
	private int port;
	private int timeout;
	private boolean disabled;
	private String disabled_response;
	private int ref_id;
	private boolean H2HXml;
	private boolean Check_Duplicate;
	private boolean always_check;
	private boolean UsesMapping;
	/**
	 * @return the provider_id
	 */
	public int getProvider_id() {
		return provider_id;
	}
	/**
	 * @param providerId the provider_id to set
	 */
	public void setProvider_id(int providerId) {
		provider_id = providerId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the classname
	 */
	public String getClassname() {
		return classname;
	}
	/**
	 * @param classname the classname to set
	 */
	public void setClassname(String classname) {
		this.classname = classname;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * @return the timeout
	 */
	public int getTimeout() {
		return timeout;
	}
	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}
	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	/**
	 * @return the disabled_response
	 */
	public String getDisabled_response() {
		return disabled_response;
	}
	/**
	 * @param disabledResponse the disabled_response to set
	 */
	public void setDisabled_response(String disabledResponse) {
		disabled_response = disabledResponse;
	}
	/**
	 * @return the ref_id
	 */
	public int getRef_id() {
		return ref_id;
	}
	/**
	 * @param refId the ref_id to set
	 */
	public void setRef_id(int refId) {
		ref_id = refId;
	}
	/**
	 * @return the h2HXml
	 */
	public boolean isH2HXml() {
		return H2HXml;
	}
	/**
	 * @param h2hXml the h2HXml to set
	 */
	public void setH2HXml(boolean h2hXml) {
		H2HXml = h2hXml;
	}
	/**
	 * @return the check_Duplicate
	 */
	public boolean isCheck_Duplicate() {
		return Check_Duplicate;
	}
	/**
	 * @param checkDuplicate the check_Duplicate to set
	 */
	public void setCheck_Duplicate(boolean checkDuplicate) {
		Check_Duplicate = checkDuplicate;
	}
	/**
	 * @return the always_check
	 */
	public boolean isAlways_check() {
		return always_check;
	}
	/**
	 * @param alwaysCheck the always_check to set
	 */
	public void setAlways_check(boolean alwaysCheck) {
		always_check = alwaysCheck;
	}
	/**
	 * @return the usesMapping
	 */
	public boolean isUsesMapping() {
		return UsesMapping;
	}
	/**
	 * @param usesMapping the usesMapping to set
	 */
	public void setUsesMapping(boolean usesMapping) {
		UsesMapping = usesMapping;
	}
	
	
	/**
	 * 
	 * @param refId	 
	 * @return
	 */
	public static ArrayList<Provider> getListProvidersByRefId(String refId)
	{
		ArrayList<Provider> listProvider = new ArrayList<Provider>();
		Connection dbConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TerminalException();
			}									
							
			String strSQL = sql_bundle.getString("getProviders");
			pst = dbConn.prepareStatement(strSQL);
			
			pst.setString(1, refId);
						
			rs = pst.executeQuery();
			while (rs.next())
			{
				Provider prov = new Provider();
				prov.setProvider_id(rs.getInt(1));
				prov.setName(rs.getString(2));
			
				listProvider.add(prov);
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getListProvidersByRefId", e);
			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pst, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection in getListProvidersByRefId", e);
			}
		}
		
		
		return listProvider;
	}
	
}
