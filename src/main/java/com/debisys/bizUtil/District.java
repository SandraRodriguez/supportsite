package com.debisys.bizUtil;

import com.debisys.exceptions.UserException;
import com.debisys.utils.DbUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author Emida
 */
public class District {

    private String idNew;
    private String name;

    private static final Logger LOG = Logger.getLogger(District.class);
    private static final ResourceBundle SQL_BUNDLE = ResourceBundle.getBundle("com.debisys.bizUtil.sql");

    public District(String id, String name) {
        this.idNew = id;
        this.name = name;
    }

    /**
     * 
     * @param isoId
     * @return 
     */
    public static List<District> findByCountryByIso(Long isoId) {
        List<District> districts = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(SQL_BUNDLE.getString("pkgDefaultDb"));
            if (dbConn == null) {
                LOG.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(SQL_BUNDLE.getString("getAllDistrict"));
            pstmt.setLong(1, isoId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                if (districts == null) {
                    districts = new ArrayList<>();
                }
                String id = rs.getString("idNew");
                String name = rs.getString("Name");
                District district = new District(id, name);
                districts.add(district);
            }

        } catch (UserException | SQLException | TorqueException e) {
            LOG.error("Error during findAll District ", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return districts;
    }

    /**
     * @return the idNew
     */
    public String getIdNew() {
        return idNew;
    }

    /**
     * @param idNew the idNew to set
     */
    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
