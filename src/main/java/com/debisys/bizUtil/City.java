package com.debisys.bizUtil;

import com.debisys.exceptions.UserException;
import com.debisys.utils.DbUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author Emida
 */
public class City {   

    
    private String districtName;
    private String name;

    private static final Logger LOG = Logger.getLogger(City.class);
    private static final ResourceBundle SQL_BUNDLE = ResourceBundle.getBundle("com.debisys.bizUtil.sql");

    /**
     * 
     * @param districtName
     * @param name 
     */
    public City( String districtName, String name) {
        this.districtName = districtName;
        this.name = name;
    }

    /**
     * 
     * @param isoId
     * @return 
     */
    public static List<City> findAllCities(Long isoId) {
        List<City> cities = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(SQL_BUNDLE.getString("pkgDefaultDb"));
            if (dbConn == null) {
                LOG.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(SQL_BUNDLE.getString("getCitiesByDistrict"));            
            pstmt.setLong(1, isoId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                if (cities == null) {
                    cities = new ArrayList<>();
                }
                String districtName = rs.getString("districtName");
                String name = rs.getString("Name");
                City city = new City(districtName, name);
                cities.add(city);
            }

        } catch (UserException | SQLException | TorqueException e) {
            LOG.error("Error during findAllCities ", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return cities;
    }
      

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * @return the districtName
     */
    public String getDistrictName() {
        return districtName;
    }

    /**
     * @param districtName the districtName to set
     */
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

}
