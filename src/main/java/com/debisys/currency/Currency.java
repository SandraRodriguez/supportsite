package com.debisys.currency;

import com.debisys.exceptions.UserException;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 * ADDED FOR LOCALIZATION
 *
 * @author swright
 */
public class Currency extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static boolean inited = false;
    private static final Logger cat = Logger.getLogger(Currency.class);

    private static String currencySymbol = "$";  //default
    private static String currencyCountryCode = "USD"; //default
    private static String currencyFormat = "0"; //default
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.currency.sql");

    @Override
    public void init() throws ServletException {
        cat.debug("Initializing currency settings");
        if (inited) {
            return;
        }
        inited = true;

        try {
            cat.debug("***************************");
            cat.debug("Config ACHRange: " + DebisysConfigListener.getACHRange(this.getServletContext()));
            cat.debug("Config CustomConfigType: " + DebisysConfigListener.getCustomConfigType(this.getServletContext()));
            cat.debug("Config DeploymentType: " + DebisysConfigListener.getDeploymentType(this.getServletContext()));
            cat.debug("Config DownloadPath: " + DebisysConfigListener.getDownloadPath(this.getServletContext()));
            cat.debug("Config DownloadUrl: " + DebisysConfigListener.getDownloadUrl(this.getServletContext()));
            cat.debug("Config FilePrefix: " + DebisysConfigListener.getFilePrefix(this.getServletContext()));
            cat.debug("Config Instance: " + DebisysConfigListener.getInstance(this.getServletContext()));
            cat.debug("Config Language: " + DebisysConfigListener.getLanguage(this.getServletContext()));
            cat.debug("Config MailHost: " + DebisysConfigListener.getMailHost(this.getServletContext()));
            cat.debug("Config MarketPlaceDeployName: " + DebisysConfigListener.getMarketPlaceDeployName(this.getServletContext()));
            cat.debug("Config MxValueAddedTax: " + DebisysConfigListener.getMxValueAddedTax(this.getServletContext()));
            cat.debug("Config PhysicalTerminals: " + DebisysConfigListener.getPhysicalTerminals(this.getServletContext()));
            cat.debug("Config ServerType: " + DebisysConfigListener.getServerType(this.getServletContext()));
            cat.debug("Config WorkingDir: " + DebisysConfigListener.getWorkingDir(this.getServletContext()));
            cat.debug("***************************");
            Currency.initialize(this.getServletContext());
            cat.info("Currency symbol initialized to " + currencySymbol);
            cat.info("Currency format initialized to " + currencyFormat);
            cat.info("Currency country code initialized to " + currencyCountryCode);
        } catch (Exception e) {
            cat.error("Currency initialization failed!");
            throw new ServletException(e.toString());
        }
    }

    public static void initialize(ServletContext context) {
        cat.debug("Init'ing localization values with Instance name: " + DebisysConfigListener.getInstance(context));
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new UserException();
            }

            pstmt = dbConn.prepareStatement(sql_bundle.getString("getProperty"));
            pstmt.setString(1, "support");
            pstmt.setString(2, "global");
            pstmt.setString(3, DebisysConfigListener.getInstance(context));
            pstmt.setString(4, "currencySymbol");
            rs = pstmt.executeQuery();
            if (rs.next()) {
                cat.debug("Got currencySymbol from DB");
                currencySymbol = rs.getString("value");
            } else {
                cat.debug("Using default value for currencySymbol");
            }

            pstmt = dbConn.prepareStatement(sql_bundle.getString("getProperty"));
            pstmt.setString(1, "support");
            pstmt.setString(2, "global");
            pstmt.setString(3, DebisysConfigListener.getInstance(context));
            pstmt.setString(4, "currencyFormat");
            rs = pstmt.executeQuery();
            if (rs.next()) {
                cat.debug("Got currencyFormat from DB");
                currencyFormat = rs.getString("value");
            } else {
                cat.debug("Using default value for currencyFormat");
            }

            pstmt = dbConn.prepareStatement(sql_bundle.getString("getProperty"));
            pstmt.setString(1, "support");
            pstmt.setString(2, "global");
            pstmt.setString(3, DebisysConfigListener.getInstance(context));
            pstmt.setString(4, "currencyCountryCode");
            rs = pstmt.executeQuery();
            if (rs.next()) {
                cat.debug("Got currencyCountryCode from DB");
                currencyCountryCode = rs.getString("value");
            } else {
                cat.debug("Using default value for currencyCountryCode");
            }

        } catch (UserException | SQLException | TorqueException e) {
            cat.error("Error during currency initialize", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
    }

    public static String getCurrencyFormat() {
        return currencyFormat;
    }

    public static String getCurrencySymbol() {
        return currencySymbol;
    }

    public static String getCurrencyCountryCode() {
        return currencyCountryCode;
    }
    
}
