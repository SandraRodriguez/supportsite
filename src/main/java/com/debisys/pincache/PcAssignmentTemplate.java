package com.debisys.pincache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.PincacheTemplateException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.JSONObject;
import com.emida.utils.dbUtils.TorqueHelper;

public class PcAssignmentTemplate {

	/* Mapper code */
	
	private static Logger cat = Logger.getLogger(PcAssignmentTemplate.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pincache.sql");
		
	public static ArrayList<PcAssignmentTemplate> getAllByOwnerId(long iso_id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<PcAssignmentTemplate> templates = new ArrayList<PcAssignmentTemplate>();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPcAssignmentTemplateByOwnerId"));
			cat.debug("PcAssignmentTemplate.getAllByOwnerId: " + sql_bundle.getString("getPcAssignmentTemplateByOwnerId") + "/* "+ iso_id +" */");
			pstmt.setLong(1, iso_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				PcAssignmentTemplate template = new PcAssignmentTemplate(rs.getString(1), rs.getLong(2), rs.getLong(3), rs.getString(4), rs.getString(5));
				template.ratePlanName = rs.getString(6);
				templates.add(template);
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.getAllByOwnerId. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return templates;
	}

	public static PcAssignmentTemplate getByTerminalId(String terminal_id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PcAssignmentTemplate template = new PcAssignmentTemplate();
		try {
			long tid = Long.parseLong(terminal_id);
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPcAsTemplateByTerminalId"));
			cat.debug("PcAssignmentTemplate.getById: " + sql_bundle.getString("getPcAsTemplateByTerminalId") + "/* "+ terminal_id +" */");
			pstmt.setLong(1, tid);
			rs = pstmt.executeQuery();
			if(rs.next()){
				template = new PcAssignmentTemplate(rs.getString(1), rs.getLong(2), rs.getLong(3), rs.getString(4), rs.getString(5));
				template.setRatePlanName(rs.getString(6));
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.getById. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return template;
	}

	public static ArrayList<PcAssignmentTemplate> getAssignableTemplates(String site_id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<PcAssignmentTemplate> templates = new ArrayList<PcAssignmentTemplate>();
		try {
			long lsid = Long.parseLong(site_id);
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getAssignableTemplates"));
			cat.debug("PcAssignmentTemplate.getAssignableTemplates: " + sql_bundle.getString("getAssignableTemplates") + "/* "+ site_id +" */");
			pstmt.setLong(1, lsid);
			rs = pstmt.executeQuery();
			while(rs.next()){
				templates.add(new PcAssignmentTemplate(rs.getString(1), rs.getLong(2), rs.getLong(3), rs.getString(4), rs.getString(5)));
			}
		} catch (Exception ex) {
			cat.error("Error during getAssignableTemplates.getAssignableTemplates. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return templates;
	}
	
	public static Vector<Vector<String>> getApplicableRatePlansByRep(String rep_id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<String>> rateplans = new Vector<Vector<String>>();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getApplicableRatePlansByRep"));
			cat.debug("PcAssignmentTemplate.getApplicableRatePlansByRep: " + sql_bundle.getString("getApplicableRatePlansByRep") + "/* "+ rep_id +" */");
			pstmt.setString(1, rep_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				Vector<String> v = new Vector<String>();
				v.add(rs.getString(1));
				v.add(rs.getString(2));
				rateplans.add(v);
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.getApplicableRatePlansByRep. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return rateplans;
	}

	public static JSONObject getApplicableRatePlansByRepAjax(String sData, SessionData sessionData) throws PincacheTemplateException {
		JSONObject json = new JSONObject();
		try{
			Vector<Vector<String>> rps = getApplicableRatePlansByRep(sData);
			json.put("items", rps);
		}catch (Exception e){
			cat.error("Error during getMerchantsByActorAjax", e);
			throw new PincacheTemplateException();
		}
		return json;
	}

	public static PcAssignmentTemplate getById(String id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PcAssignmentTemplate template = new PcAssignmentTemplate();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPcAssignmentTemplateById"));
			cat.debug("PcAssignmentTemplate.getById: " + sql_bundle.getString("getPcAssignmentTemplateById") + "/* "+ id +" */");
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				template = new PcAssignmentTemplate(rs.getString(1), rs.getLong(2), rs.getLong(3), rs.getString(4), rs.getString(5));
				template.ratePlanName = rs.getString(6);
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.getById. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return template;
	}

	public static Vector<Vector<String>> getPincacheableReps(String sAccessLevel, String sActorId, String sChainLevel) throws CustomerException {
		Vector<Vector<String>> vResult = new Vector<Vector<String>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null){
				cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String sSQL = "SELECT DISTINCT re.rep_id, re.businessname FROM reps re (NOLOCK) ";

			if (sAccessLevel.equals(DebisysConstants.SUBAGENT) || (sAccessLevel.equals(DebisysConstants.ISO) && sChainLevel.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))){
				sSQL += "INNER JOIN reps AS r WITH(NOLOCK) ON re.rep_id = r.rep_id INNER JOIN RatePlan As rp WITH(NOLOCK) ON (rp.OwnerId = re.rep_id AND rp.IsTemplate = 0) " 
					 + "WHERE r.rep_id IN (SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id = " + sActorId + ") ORDER BY re.businessname";
			}else if (sAccessLevel.equals(DebisysConstants.AGENT)){
				sSQL += "INNER JOIN reps AS r WITH(NOLOCK) ON re.rep_id = r.rep_id INNER JOIN RatePlan As rp WITH(NOLOCK) ON (rp.OwnerId = re.rep_id AND rp.IsTemplate = 0) " 
					 + "WHERE r.rep_id IN (SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id IN"
					 + "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id = " + sActorId + ")) ORDER BY re.businessname";
			}else if (sAccessLevel.equals(DebisysConstants.ISO) && sChainLevel.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
				 sSQL += "INNER JOIN reps AS r WITH(NOLOCK) ON re.rep_id = r.rep_id INNER JOIN RatePlan As rp WITH(NOLOCK) ON (rp.OwnerId = re.rep_id AND rp.IsTemplate = 0) " 
					 + "WHERE r.rep_id IN (SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id IN"
					 + "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id IN (SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id = " + sActorId
					 + "))) ORDER BY re.businessname";
			}

			cat.debug("getPincacheableReps: " + sSQL);
			pstmt = dbConn.prepareStatement(sSQL);
			rs = pstmt.executeQuery();

			while (rs.next()){
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("rep_id"));
				vecTemp.add(rs.getString("businessname"));
				vResult.add(vecTemp);
			}
		}catch (Exception e){
			cat.error("Error during getPincacheableReps", e);
			throw new CustomerException();
		}finally{
			try{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}catch (Exception e){
				cat.error("Error during closeConnection", e);
			}
		}
		return vResult;
	}	
	
	public static boolean isRemovable(String template_id) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("validateRemovableTemplate"));
			cat.debug("PcAssignmentTemplate.remove: " + sql_bundle.getString("validateRemovableTemplate") + " /* " + template_id + " */");
			pstmt.setString(1, template_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				ok = (rs.getInt(1) == 0);
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.isRemovable: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}
	
	public static boolean remove(String template_id) throws PincacheTemplateException {
		boolean ok = false;
		if(isRemovable(template_id)){
			Connection con = null;
			PreparedStatement pstmt = null;
			try {
				con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (con == null) {
					cat.error("Can't get database connection");
					throw new PincacheTemplateException();
				}
				pstmt = con.prepareStatement(sql_bundle.getString("removePcAssignmentTemplate"));
				cat.debug("PcAssignmentTemplate.remove: " + sql_bundle.getString("removePcAssignmentTemplate") + " /* " + template_id + " */");
				pstmt.setString(1, template_id);
				pstmt.execute();
				ok = true;
			} catch (Exception ex) {
				cat.error("Error during PcAssignmentTemplate.remove: " + ex);
				throw new PincacheTemplateException();
			} finally {
				try {
					TorqueHelper.closeConnection(con, pstmt, null);
				} catch (Exception e) {
					cat.error("Error during release connection", e);
				}
			}
		}
		return ok;
	}
	
	public static boolean save(PcAssignmentTemplate template) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			if(template.getId() == null){
				pstmt = con.prepareStatement(sql_bundle.getString("addPcAssignmentTemplate"));
				cat.debug("PcAssignmentTemplate.save: " + sql_bundle.getString("addPcAssignmentTemplate")+ " /* " + template.getRepOwnerId() + ", "+ template.getRatePlanId() + ", " + template.getName()+ ", " + template.getDescription() + " */");
				
				pstmt.setLong(1, template.getRepOwnerId());
				pstmt.setLong(2, template.getRatePlanId());
				pstmt.setString(3, template.getName());
				pstmt.setString(4, template.getDescription());
				rs = pstmt.executeQuery();
				if(rs.next()){
					template.id = rs.getString(1);
				}
			}else{
				pstmt = con.prepareStatement(sql_bundle.getString("savePcAssignmentTemplate"));
				cat.debug("PcAssignmentTemplate.save: " + sql_bundle.getString("savePcAssignmentTemplate") + " /* " + template.getRepOwnerId() + ", "+ template.getRatePlanId() + ", " + template.getName()+ ", " + template.getDescription() + ", " + template.getId() + " */");
				pstmt.setLong(1, template.getRepOwnerId());
				pstmt.setLong(2, template.getRatePlanId());
				pstmt.setString(3, template.getName());
				pstmt.setString(4, template.getDescription());
				pstmt.setString(5, template.getId());
				pstmt.execute();
			}
			
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.save: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return false;
	}	
	
	
	/* Dynamic code*/
	
	public static ArrayList<String> validate(PcAssignmentTemplate template, String lang) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<String> errors = new ArrayList<String>();
		if(template.name != null && template.name.length() > 0){
			try {
				con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (con == null) {
					cat.error("Can't get database connection");
					throw new PincacheTemplateException();
				}
				pstmt = con.prepareStatement(sql_bundle.getString("countPcAsTeByIsoIdAndName"));
				cat.debug("PcAssignmentTemplate.validate: " + sql_bundle.getString("countPcAsTeByIsoIdAndName"));
				
				//template.generateId();
				pstmt.setLong(1, template.getRepOwnerId());
				pstmt.setString(2, template.getDescription());
				//pstmt.setString(3, template.getDescription());
				rs = pstmt.executeQuery();
				if(rs.next()){
					if(rs.getInt(1) > 0){
						errors.add(Languages.getString("jsp.admin.pincache.name.alreadyexists", lang));
					}
				}
			} catch (Exception ex) {
				cat.error("Error during PcAssignmentTemplate.validate " + ex);
				throw new PincacheTemplateException();
			} finally {
				try {
					TorqueHelper.closeConnection(con, pstmt, rs);
				} catch (Exception e) {
					cat.error("Error during release connection", e);
				}
			}
		}else{
			errors.add(Languages.getString("jsp.admin.pincache.name.empty", lang));
		}
		return errors;
	}	
	
	private String description;
	
	private String id;
	
	private String name;
	
	private ArrayList<PcAssignmentTemplateProduct> products;
	
	private long ratePlanId;
	
	private String ratePlanName;

	/**
	 * @param ratePlanName the ratePlanName to set
	 */
	public void setRatePlanName(String ratePlanName) {
		this.ratePlanName = ratePlanName;
	}

	/**
	 * @return the ratePlanName
	 */
	public String getRatePlanName() {
		return ratePlanName;
	}

	private long repOwnerId;

	private int totalPines;
	

	public PcAssignmentTemplate(){
		//products = new ArrayList<PcAssignmentTemplateProduct>();
		products = null;
	}
	
	public PcAssignmentTemplate(String id, long rep_id, long ratePlanId, String name, String description) {
		this.id = id;
		this.repOwnerId = rep_id;
		this.ratePlanId = ratePlanId;
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the description
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the products
	 */
	public ArrayList<PcAssignmentTemplateProduct> getProducts() {
		if(products == null){
			try {
				products = PcAssignmentTemplateProduct.getAllByTemplateId(id);
			} catch (PincacheTemplateException e) {
				e.printStackTrace();
			}
		}
		return products;
	}

	/**
	 * @return the repOwnerId
	 */
	public long getRatePlanId() {
		return ratePlanId;
	}

	/**
	 * @return the repOwnerId
	 */
	public long getRepOwnerId() {
		return repOwnerId;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param description the description to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param products the products to set
	 */
	//public void setProducts(ArrayList<PcAssignmentTemplateProduct> products) {
		//this.products = products;
	//}

	/**
	 * @param repOwnerId the repOwnerId to set
	 */
	public void setRatePlanId(long rateplan_id) {
		this.ratePlanId = rateplan_id;
	}

	/**
	 * @param repOwnerId the repOwnerId to set
	 */
	public void setRepOwnerId(long rep_id) {
		this.repOwnerId = rep_id;
	}
	
	/*public void addProduct(PcAssignmentTemplateProduct p){
		products.add(p);
	}
	
	public void removeProduct(PcAssignmentTemplateProduct p){
		products.remove(p);
	}*/
	
	public int getTotalPines(){
		totalPines = 0;
		getProducts();
		for(PcAssignmentTemplateProduct p: products){
			totalPines +=p.getQuantity();
		}
		return totalPines;
	}
}
