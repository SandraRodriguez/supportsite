package com.debisys.pincache;

import java.util.Date;

public class PcNonProcessedSale {
	private Date saleDate;
	private String clerk;
	private long invoiceNumber;
	private long siteId;
	/**
	 * @return the siteId
	 */
	public long getSiteId() {
		return siteId;
	}
	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}
	/**
	 * @return the saleDate
	 */
	public Date getSaleDate() {
		return saleDate;
	}
	/**
	 * @param saleDate the saleDate to set
	 */
	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}
	/**
	 * @return the clerk
	 */
	public String getClerk() {
		return clerk;
	}
	/**
	 * @param clerk the clerk to set
	 */
	public void setClerk(String clerk) {
		this.clerk = clerk;
	}
	/**
	 * @return the invoiceNumber
	 */
	public long getInvoiceNumber() {
		return invoiceNumber;
	}
	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(long invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
}
