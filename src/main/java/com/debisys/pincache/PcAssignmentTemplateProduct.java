package com.debisys.pincache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.PincacheTemplateException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.JSONObject;
import com.emida.utils.dbUtils.TorqueHelper;

public class PcAssignmentTemplateProduct {
	
	private static Logger cat = Logger.getLogger(PcAssignmentTemplateProduct.class);

	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pincache.sql");

	public static ArrayList<PcAssignmentTemplateProduct> getAllByTemplateId(String template_id) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<PcAssignmentTemplateProduct> products = new ArrayList<PcAssignmentTemplateProduct>();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPcAsTeProdByTemplateId"));
			cat.debug("PcAssignmentTemplate.getAllByTemplateId: " + sql_bundle.getString("getPcAsTeProdByTemplateId") + "/* "+ template_id +" */");
			pstmt.setString(1, template_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				products.add(new PcAssignmentTemplateProduct(rs.getString(1), rs.getLong(2), rs.getInt(3), rs.getString(4)));
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.getAllByTemplateId. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return products;
	}



	public static ArrayList<ArrayList<String>>  getPincacheableProducts(String rateplan_id, String template_id) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> products = new ArrayList<ArrayList<String>> ();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPincacheableProducts"));
			cat.debug("PcAssignmentTemplate.getPincacheableProducts: " + sql_bundle.getString("getPincacheableProducts") + "/* "+ rateplan_id +" */");
			pstmt.setString(1, rateplan_id);
			pstmt.setString(2, template_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				ArrayList<String> v = new ArrayList<String>();
				v.add(rs.getString(1));
				v.add(rs.getString(2));
				products.add(v);
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.getCachepineableProducts. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return products;
	}
	
	public static JSONObject getPincacheableProductsAjax(String sData, SessionData sessionData) throws PincacheTemplateException {
		JSONObject json = new JSONObject();
		try{
			String[] sd = sData.split("_");
			ArrayList<ArrayList<String>> rps = getPincacheableProducts(sd[0], sd[1]);
			json.put("items", rps);
		}catch (Exception e){
			cat.error("Error during getMerchantsByActorAjax", e);
			throw new PincacheTemplateException();
		}
		return json;
	}
	
	public static boolean remove(String template_id, String product_id, String language) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean ok = false;
		if(template_id != null && !template_id.equals("") && !template_id.equals("0")){
			if(product_id != null && !product_id.equals("") && !product_id.equals("0")){
				try {
					con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
					if (con == null) {
						cat.error("Can't get database connection");
						throw new PincacheTemplateException();
					}
					pstmt = con.prepareStatement(sql_bundle.getString("removePcAsTeProduct"));
					cat.debug("PcAssignmentTemplate.remove: " + sql_bundle.getString("removePcAsTeProduct") + " /* " + template_id + ", " + product_id + " */");
					pstmt.setString(1, template_id);
					pstmt.setString(2, product_id);
					pstmt.execute();
					ok = true;
				} catch (Exception ex) {
					cat.error("Error during PcAssignmentTemplate.remove: " + ex);
					throw new PincacheTemplateException();
				} finally {
					try {
						TorqueHelper.closeConnection(con, pstmt, null);
					} catch (Exception e) {
						cat.error("Error during release connection", e);
					}
				}
			}else{
				cat.error("Error during PcAssignmentTemplate.remove: ProductId not provided");
			}
		}else{
			cat.error("Error during PcAssignmentTemplate.remove: TemplateId not provided");
		}
		return ok;
	}
	
	/**
	 * Saves or updates the AssignmentTemplateProduct
	 * */
	
	public static boolean save(PcAssignmentTemplateProduct product) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			if(product.getTemplate_id() != null && !product.getTemplate_id().equals("") && product.getProduct_id() > 0){
				pstmt = con.prepareStatement(sql_bundle.getString("savePcAssignmentTemplateProduct"));
				cat.debug("PcAssignmentTemplateProduct.save: " + sql_bundle.getString("savePcAssignmentTemplateProduct")+ " /* " + product.getQuantity() + ", " + product.getTemplate_id() + ", " + product.getProduct_id() + " */");
				pstmt.setInt(1, product.getQuantity());
				pstmt.setString(2, product.getTemplate_id());
				pstmt.setLong(3, product.getProduct_id());
				rs = pstmt.executeQuery();
				if(rs.next()){
					if(rs.getInt(1) == 0){
						pstmt.close();
						pstmt = con.prepareStatement(sql_bundle.getString("addPcAssignmentTemplateProduct"));
						cat.debug("PcAssignmentTemplateProduct.save: " + sql_bundle.getString("addPcAssignmentTemplateProduct")+ " /* " + product.getTemplate_id() + ", " + product.getProduct_id() + ", " + product.getQuantity() + " */");
						
						pstmt.setString(1, product.getTemplate_id());
						pstmt.setLong(2, product.getProduct_id());
						pstmt.setInt(3, product.getQuantity());
						pstmt.execute();
					}
				}
				return true;
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.save: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return false;
	}

	public static ArrayList<String> validate(PcAssignmentTemplateProduct product, String lang) throws PincacheTemplateException{
		//Connection dbConn = null;
		ArrayList<String> errors = new ArrayList<String>();
		if(product.getTemplate_id() == null || product.getTemplate_id().equals("")){
			errors.add(Languages.getString("jsp.admin.pincache.notemplateid", lang));
		}
		if(product.getTemplate_id() == null || product.getTemplate_id().equals("")){
			errors.add(Languages.getString("jsp.admin.pincache.notproduct", lang));
		}
		if(product.getQuantity() < 0 || product.getQuantity() > 1000){
			errors.add(Languages.getString("jsp.admin.pincache.invalidquantity", lang));
		}
		/*if(template.description != null && template.description.length() > 0){
			try {
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null) {
					cat.error("Can't get database connection");
					throw new PincacheTemplateException();
				}
				PreparedStatement pstmt;
				pstmt = dbConn.prepareStatement(sql_bundle.getString("countPcAsTeByRepIdAndDesc"));
				cat.debug("PcAssignmentTemplate.validate: " + sql_bundle.getString("countPcAsTeByRepIdAndDesc"));
				
				//template.generateId();
				pstmt.setLong(1, template.getRep_id());
				pstmt.setString(2, template.getDescription());
				//pstmt.setString(3, template.getDescription());
				ResultSet rs = pstmt.executeQuery();
				if(rs.next()){
					if(rs.getInt(1) > 0){
						errors.add(Languages.getString("jsp.admin.pincache.desc.alreadyexists", lang));
					}
				}
			} catch (Exception ex) {
				cat.error("Error during PcAssignmentTemplateProduct.validate " + ex);
				throw new PincacheTemplateException();
			} finally {
				try {
					TorqueHelper.closeConnection(con, pstmt, rs);
				} catch (Exception e) {
					cat.error("Error during release connection", e);
				}
			}
		}else{
			errors.add(Languages.getString("jsp.admin.pincache.desc.empty", lang));
		}*/
		return errors;
	}

	private String description;

	private long product_id;

	private int quantity;

	private String template_id;

	/**
	 * 
	 */
	public PcAssignmentTemplateProduct() {
	}


	
	/**
	 * @param template_id
	 * @param product_id
	 * @param quantity
	 */
	public PcAssignmentTemplateProduct(String template_id, long product_id, int quantity) {
		this.template_id = template_id;
		this.product_id = product_id;
		this.quantity = quantity;
	}

	public PcAssignmentTemplateProduct(String template_id, long product_id, int quantity, String description) {
		this.template_id = template_id;
		this.product_id = product_id;
		this.quantity = quantity;
		this.description = description;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @return the product_id
	 */
	public long getProduct_id() {
		return product_id;
	}	

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	
	/**
	 * @return the template_id
	 */
	public String getTemplate_id() {
		return template_id;
	}	
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @param product_id the product_id to set
	 */
	public void setProduct_id(long product_id) {
		this.product_id = product_id;
	}	

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @param template_id the template_id to set
	 */
	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}



	public static int countAssignments(String template_id, String product_id) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int count = 0;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("countAsByProductAndTemplate"));
			cat.debug("countAsByProductAndTemplate: " + sql_bundle.getString("countAsByProductAndTemplate") + "/* "+ product_id +", "+ template_id +" */");
			pstmt.setString(1, product_id);
			pstmt.setString(2, template_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				count = rs.getInt(1);
			}
		} catch (Exception ex) {
			cat.error("Error during countAssignments. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return count;
	}
}
