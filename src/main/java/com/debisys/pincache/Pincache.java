/**
 * 
 */
package com.debisys.pincache;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.torque.Torque;

import com.debisys.exceptions.PincacheException;
import com.debisys.exceptions.PincacheTemplateException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.JSONObject;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author jcruz
 *
 */
public class Pincache {

	private static Logger cat = Logger.getLogger(Pincache.class);

	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pincache.sql");

	public static String formatCurrency(String s) {
		DecimalFormat money = new DecimalFormat("$0.00");
		double amount = 0.0;
		try{
			amount = Double.parseDouble(s);
		}catch(Exception e){}
		return money.format(amount);
	}
	
	public static ArrayList<ArrayList<String>> getPincacheDetails(String product_id, String site_id) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> products = new ArrayList<ArrayList<String>>();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPincacheDetails"));
			cat.debug("PcAssignmentTemplate.getPincacheDetails: " + sql_bundle.getString("getPincacheDetails") + "/* " + product_id +", " + site_id +" */");
			pstmt.setString(1, product_id);
			pstmt.setString(2, site_id);
			rs = pstmt.executeQuery();
			while(rs.next()){
				ArrayList<String> v = new ArrayList<String>();
				for(int i=1; i <= 5; i++){
					String s = rs.getString(i);
					if(i == 5){
						s = formatCurrency(s); 
					}
					if(s == null){
						s = "";
					}
					v.add(s);
				}
				products.add(v);
			}
		} catch (Exception ex) {
			cat.error("Error during PcAssignmentTemplate.getPincacheDetails. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return products;
	}
	
	public static ArrayList<ArrayList<String>> getPincacheReport(String rep_id, String strDistChainType, String strAccessLevel) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> products = new ArrayList<ArrayList<String>>();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheException();
			}
			String query = sql_bundle.getString("getPincacheReport");
			String chainfields = "'', '',";
			if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
				chainfields = "r4.businessname, r3.businessname, ";
				query += "	INNER JOIN reps AS r2 ON(r2.rep_id = r1.iso_id)"
						+"	INNER JOIN reps AS r3 ON(r3.rep_id = r2.iso_id)"
						+"	INNER JOIN reps AS r4 ON(r4.rep_id = r3.iso_id AND r4.iso_id = r4.rep_id) ";
				if(strAccessLevel.equals(DebisysConstants.ISO)){
					query += "	WHERE (r4.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.AGENT)){
					query += "	WHERE (r3.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.SUBAGENT)){
					query += "	WHERE (r2.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.REP)){
					query += "	WHERE (r1.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
					query += "	WHERE (m.merchant_id = ?)";
				}
			}else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
				chainfields = "";
				query += "	INNER JOIN reps AS r2 ON(r2.rep_id = r1.iso_id AND r2.iso_id = r2.rep_id)";
				if(strAccessLevel.equals(DebisysConstants.ISO)){
					query += "	WHERE (r2.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.REP)){
					query += "	WHERE (r1.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
					query += "	WHERE (m.merchant_id = ?)";
				}
			}
			
			query = String.format(query, chainfields);
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, rep_id);
			cat.debug("PcAssignmentTemplate.getPincacheReport: " + query + "/* " + rep_id +" */");

			//pstmt.setString(1, rep_id);
			rs = pstmt.executeQuery();
			if(rs != null){
				ResultSetMetaData rsMetaData = rs.getMetaData();
				int numCols = rsMetaData.getColumnCount();
				while(rs.next()){
					ArrayList<String> v = new ArrayList<String>();
					
					for(int i=1; i <= numCols; i++){
						String s = rs.getString(i);
						if(i == numCols){
							s = formatCurrency(s); 
						}
						if(s == null){
							s = "";
						}
						v.add(s);
					}
					products.add(v);
				}
			}
		} catch (Exception ex) {
			cat.error("Error during getPincacheReport. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return products;
	}

	public static StringBuilder getReportCSV(String rep_id, String lang, String strDistChainType, String strAccessLevel){
		StringBuilder results = new StringBuilder();
		ArrayList<String> headers = getReportHeaders(lang, strDistChainType);
		ArrayList<ArrayList<String>> data = null;
		try {
			data = getPincacheReport(rep_id, strDistChainType, strAccessLevel);
		} catch (PincacheException e) {
			e.printStackTrace();
		}
		int i;
		for(i = 0; i < headers.size() - 1; i++){
			if(i > 0){
				results.append(",");
			}
			results.append("\"" + headers.get(i) + "\"");
		}
		results.append("\n");
		if(data != null){ 
			for(ArrayList<String> row: data){
				for(i = 1; i < row.size(); i++){
					if(i > 1){
						results.append(",");
					}
					results.append("\"" + row.get(i) + "\"");
				}
				results.append("\n");
			}		
		}
		return results;
	}
	
	public static ArrayList<String> getReportHeaders(String lang, String strDistChainType){
		ArrayList<String> headers = new ArrayList<String>();
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header1", lang));
		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			headers.add(Languages.getString("jsp.admin.pincache.reportpins.header2", lang));
			headers.add(Languages.getString("jsp.admin.pincache.reportpins.header3", lang));
		}
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header4", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header5", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header6", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header7", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header8", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header9", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportpins.header10", lang));
		return headers;		
	}
	
	public static HSSFWorkbook getReportXLS(String rep_id, String lang, String strDistChainType, String strAccessLevel){
		ArrayList<String> headers = getReportHeaders(lang, strDistChainType);
		ArrayList<ArrayList<String>> data = null;
		try {
			data = getPincacheReport(rep_id, strDistChainType, strAccessLevel);
		} catch (PincacheException e) {
			e.printStackTrace();
		}
		
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("CACHED PINS");

		int i;
		int j = 2;
		HSSFRow row = sheet.createRow(j);
		
		HSSFCellStyle style = wb.createCellStyle();
		style.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		
		HSSFFont font = wb.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor( HSSFColor.WHITE.index);
		style.setFont(font);
		
		for(i = 0; i < headers.size() - 1; i++){
			HSSFCell cell = row.createCell(i);
			cell.setCellValue(headers.get(i));
			cell.setCellStyle(style);
			sheet.autoSizeColumn((short)cell.getColumnIndex()); 
		}
		HSSFCellStyle styleCurrencyFormat = wb.createCellStyle();
		styleCurrencyFormat.setDataFormat(HSSFDataFormat.getBuiltinFormat("$ #.##"));
		
		style = wb.createCellStyle();
		style.setFillForegroundColor(HSSFColor.WHITE.index);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		style.setLeftBorderColor(HSSFColor.BLUE_GREY.index);
		style.setRightBorderColor(HSSFColor.BLUE_GREY.index);
		style.setTopBorderColor(HSSFColor.BLUE_GREY.index);
		style.setBottomBorderColor(HSSFColor.BLUE_GREY.index);
		font = wb.createFont();
		font.setColor( HSSFColor.BLACK.index);
		style.setFont(font);
		if(data != null){ 
			for(ArrayList<String> rowData: data){
				j++;
				row = sheet.createRow(j);
				for(i = 1; i < rowData.size(); i++){
					HSSFCell cell = row.createCell(i-1);
					sheet.autoSizeColumn((short)cell.getColumnIndex());
					if(i == rowData.size() - 2){
						double total = Double.parseDouble(rowData.get(i));
						cell.setCellValue(total);
						cell.setCellStyle(style);
					}else if(i == rowData.size() - 1){
						double total = Double.parseDouble(rowData.get(i).replace("$", ""));
						cell.setCellValue(total);
						cell.setCellStyle(styleCurrencyFormat);
					}else{
						cell.setCellValue(rowData.get(i));
						cell.setCellStyle(style);
					}
				}
			}		
		}
		
		return wb;
	}	
	
	public static void processRequest(SessionData sessionData, HttpServletRequest request, HttpServletResponse response){
		ArrayList<String> errors = null;
		if (sessionData.isLoggedIn()){
			try{
				User u = sessionData.getUser();
				long rep_id = Long.parseLong(u.getRefId());
				String  action = request.getParameter("action");
				if(action != null && action.equals("save_template")){
					String rp_id = request.getParameter("rateplan_id");
					if(rp_id != null && !rp_id.equals("")){
						long rateplan_id = Long.parseLong(rp_id);
						PcAssignmentTemplate template = new PcAssignmentTemplate(null, rep_id, rateplan_id, request.getParameter("name"), request.getParameter("description"));
						errors = PcAssignmentTemplate.validate(template, sessionData.getLanguage());
						if(errors.size() == 0){
							PcAssignmentTemplate.save(template);
						}else{
							request.setAttribute("errors", errors);
						}
					}else{
						errors = new ArrayList<String>();
						errors.add(Languages.getString("jsp.admin.pincache.norateplan", sessionData.getLanguage()));
					}
				/*}else if(action != null && action.equals("remove_template")){
					String template_id = request.getParameter("template_id");
					if(PcAssignmentTemplate.isRemovable(template_id)){
						PcAssignmentTemplate.remove(template_id);
					}else{
						errors = new ArrayList<String>();
						errors.add(Languages.getString("jsp.admin.pincache.deleteerror", sessionData.getLanguage()));
					}*/
				/*}else if(action != null && action.equals("remove_product")){
					String template_id = request.getParameter("template_id");
					String product_id = request.getParameter("product_id");
					try{
						if(!PcAssignmentTemplateProduct.remove(template_id, product_id, sessionData.getLanguage())){
							errors = new ArrayList<String>();
							errors.add(Languages.getString("jsp.admin.pincache.deleteerror", sessionData.getLanguage()));
						}
					} catch (PincacheTemplateException e) {
						errors = new ArrayList<String>();
						errors.add(Languages.getString("jsp.admin.pincache.deleteerror", sessionData.getLanguage()));
					}*/			
				}else if(action != null && (action.equals("save_product") || action.equals("edit_product"))){
					String qty = request.getParameter("quantity");
					String prod = request.getParameter("product_id");
					if(prod != null && !prod.equals("")){
						if(qty != null && !qty.equals("") ){
							int q = Integer.parseInt(qty);
							long prod_id = Long.parseLong(prod);
							PcAssignmentTemplateProduct product = new PcAssignmentTemplateProduct(request.getParameter("template_id"), prod_id, q);
							try {
								errors = PcAssignmentTemplateProduct.validate(product, sessionData.getLanguage());
								if(errors != null && errors.size() == 0){
									PcAssignmentTemplateProduct.save(product);
								}
							} catch (PincacheTemplateException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else{
							errors = new ArrayList<String>();
							errors.add(Languages.getString("jsp.admin.pincache.invalidquantity", sessionData.getLanguage()));
						}
					}else{
						errors = new ArrayList<String>();
						errors.add(Languages.getString("jsp.admin.pincache.invalidproduct", sessionData.getLanguage()));
					}
				}
				request.setAttribute("templates", PcAssignmentTemplate.getAllByOwnerId(rep_id));
				
				String strAccessLevel = sessionData.getProperty("access_level");
				String strDistChainType = sessionData.getProperty("dist_chain_type");
				request.setAttribute("reps", PcAssignmentTemplate.getPincacheableReps(strAccessLevel, u.getRefId(), strDistChainType));
			} catch (Exception e) {
				errors = new ArrayList<String>();
				errors.add(Languages.getString("jsp.admin.pincache.operationerror", sessionData.getLanguage()));
				e.printStackTrace();
			}
			request.setAttribute("errors", errors);
		}else{
			try {
				response.sendRedirect("/support/login.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static JSONObject removeProductAjax(String sData, SessionData sessionData) throws PincacheException {
		JSONObject json = new JSONObject();
		try{
			String strAccessLevel = sessionData.getProperty("access_level");
			if (strAccessLevel != null && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
				String[] sd = sData.split("_");
				String template_id = sd[0];
				String product_id = sd[1];
				if(PcAssignmentTemplateProduct.countAssignments(template_id, product_id) == 0){
					if(PcAssignmentTemplateProduct.remove(template_id, product_id, sessionData.getLanguage())){
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.deleted", sessionData.getLanguage())});
						json.put("status", "success");
					}else{
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.deleteerror", sessionData.getLanguage())});
						json.put("status", "error");
					}
				}else{
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.productwithassignmentserror", sessionData.getLanguage())});
					json.put("status", "error");
				}
			}else{
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during removeTemplateAjax", e);
			throw new PincacheException();
		}
		return json;
	}	
	
	public static JSONObject removeTemplateAjax(String sData, SessionData sessionData) throws PincacheException {
		JSONObject json = new JSONObject();
		try {
			String strAccessLevel = sessionData.getProperty("access_level");
			if (strAccessLevel != null && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
				if(PcAssignmentTemplate.isRemovable(sData) && PcAssignmentTemplate.remove(sData)){
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.deleted", sessionData.getLanguage())});
					json.put("status", "success");
				}else{
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.deleteerror", sessionData.getLanguage())});
					json.put("status", "error");
				}			
			}else{
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during removeTemplateAjax", e);
			throw new PincacheException();
		}
		return json;
	}

	public static JSONObject enableMerchantAjax(String sData, SessionData sessionData) throws PincacheException {
		JSONObject json = new JSONObject();
		try {
			//String strAccessLevel = sessionData.getProperty("access_level");
			//if (strAccessLevel != null && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
			if (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
				String[] sd = sData.split("_");
				String merchant_id = sd[0];
				boolean enable = Boolean.parseBoolean(sd[1]);
				if(enable){
					if(enableMerchant(merchant_id)){
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.enabled", sessionData.getLanguage())});
						json.put("status", "success");
					}else{
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.enableerror", sessionData.getLanguage())});
						json.put("status", "error");
					}
				}else{
					if(disableMerchant(merchant_id, sd[2], sd[3], sessionData.getLanguage())){
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.disabled", sessionData.getLanguage())});
						json.put("status", "success");
					}else{
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.disableerror", sessionData.getLanguage())});
						json.put("status", "error");
					}
				}
			}else{
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during enableMerchantAjax", e);
			throw new PincacheException();
		}
		return json;
	}

	private static boolean disableMerchant(String merchant_id, String sDeploymentType, String sCustomConfigType, String lang) throws PincacheException {
		boolean ok = false;
		if(!hasPinesAssigned(merchant_id)){
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (con == null) {
					cat.error("Can't get database connection");
					throw new PincacheException();
				}
				double value = PcReservedBalance.getRBByMerchantId(merchant_id);
				PcReservedBalance.reserveForMerchant(merchant_id, value, sDeploymentType, sCustomConfigType, lang, true);
				
				pstmt = con.prepareStatement(sql_bundle.getString("disablePcMerchant"));
				cat.debug("disablePcMerchant: " + sql_bundle.getString("disablePcMerchant") + " /* " + merchant_id + " */");
				pstmt.setString(1, merchant_id);
				pstmt.execute();
				ok = true;
			} catch (Exception ex) {
				cat.error("Error during disablePcMerchant: " + ex);
				throw new PincacheException();
			} finally {
				try {
					TorqueHelper.closeConnection(con, pstmt, rs);
				} catch (Exception e) {
					cat.error("Error during release connection", e);
				}
			}
		}
		return ok;
	}

	public static boolean hasPinesAssigned(String merchant_id) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("countPinsAssignedbyMerchant"));
			cat.debug("canDisablePcMerchant: " + sql_bundle.getString("countPinsAssignedbyMerchant") + " /* " + merchant_id + " */");
			pstmt.setString(1, merchant_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				ok = (rs.getInt(1) > 0);
			}
		} catch (Exception ex) {
			cat.error("Error during canDisablePcMerchant: " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}

	private static boolean enableMerchant(String merchant_id) throws PincacheException {
		boolean ok = false;
		if(canEnableMerchant(merchant_id)){
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (con == null) {
					cat.error("Can't get database connection");
					throw new PincacheException();
				}
				pstmt = con.prepareStatement(sql_bundle.getString("enablePCMerchant"));
				cat.debug("enablePCMerchant: " + sql_bundle.getString("enablePCMerchant") + " /* " + merchant_id + " */");
				pstmt.setString(1, merchant_id);
				pstmt.execute();
				ok = true;
			} catch (Exception ex) {
				cat.error("Error during enablePCMerchant: " + ex);
				throw new PincacheException();
			} finally {
				try {
					TorqueHelper.closeConnection(con, pstmt, rs);
				} catch (Exception e) {
					cat.error("Error during release connection", e);
				}
			}
		}
		return ok;
	}

	private static boolean canEnableMerchant(String merchant_id) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("canEnablePcMerchant"));
			cat.debug("canEnablePcMerchant: " + sql_bundle.getString("canEnablePcMerchant") + " /* " + merchant_id + " */");
			pstmt.setString(1, merchant_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				ok = (rs.getInt(1) == 1);
			}
		} catch (Exception ex) {
			cat.error("Error during canEnablePcMerchant: " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}
	
	public static JSONObject changePINStatusAjax(String sData, SessionData sessionData) throws PincacheException {
		JSONObject json = new JSONObject();
		try {
			if (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE_ADV)) {
				String[] sd = sData.split("_");
				if(sd.length == 3 && sd[0] != null && !sd[0].equals("") && !sd[0].equals("undefined") && sd[1] != null && !sd[1].equals("") && !sd[1].equals("undefined") ){
					if(sd[2] != null && !sd[2].equals("") && !sd[2].equals("undefined")){
						String[] sdp = sd[0].split("@");
						ArrayList<String> msgs = new ArrayList<String>();
						for(String s: sdp){
							PcAssignment pa = PcAssignment.getPcAsById(s);
							if(pa != null){
								if(PcAssignment.changeStatus(pa, sd[1], sd[2], sessionData)){
									msgs.add("<div class=\"success\"><strong>PIN [Control#"+ pa.getControl_no() + "]</strong>: " + Languages.getString("jsp.admin.pincache.statusChangedOk", sessionData.getLanguage())+ "</div>");
								}else{
									msgs.add("<div class=\"error\"><strong>PIN [Control#"+ pa.getControl_no()+ "]</strong>: " + Languages.getString("jsp.admin.pincache.statusError", sessionData.getLanguage())+ "</div>");
								}
							}else{
								msgs.add("<div class=\"warning\"><strong>PIN [Control#"+ pa.getControl_no()+ "]</strong>: " + Languages.getString("jsp.admin.pincache.pinnotfound", sessionData.getLanguage())+ "</div>");
							}
						}
						json.put("msgs", msgs);
						json.put("status", "");
					}else{
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noteerror", sessionData.getLanguage())});
						json.put("status", "error");
					}
				}else{
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.operationerror", sessionData.getLanguage())});
					json.put("status", "error");
				}			
			}else{
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during changePINStatusAjax", e);
			throw new PincacheException();
		}
		return json;
	}	
}
