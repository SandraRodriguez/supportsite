package com.debisys.pincache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.PincacheException;
import com.debisys.exceptions.PincacheTemplateException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.JSONObject;
import com.emida.utils.dbUtils.TorqueHelper;

public class PcTerminalTemplate {
	
	private static Logger cat = Logger.getLogger(PcTerminalTemplate.class);

	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pincache.sql");
	
	private String terminal_id;
	private String template_id;
	
	/**
	 * @return the terminal_id
	 */
	public String getTerminal_id() {
		return terminal_id;
	}
	
	/**
	 * @param terminal_id the terminal_id to set
	 */
	public void setTerminal_id(String terminal_id) {
		this.terminal_id = terminal_id;
	}

	/**
	 * @return the template_id
	 */
	public String getTemplate_id() {
		return template_id;
	}

	/**
	 * @param template_id the template_id to set
	 */
	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public static JSONObject removeAssignmentAjax(String sData, SessionData sessionData) throws PincacheTemplateException{
		JSONObject json = new JSONObject();
		try {
			String strAccessLevel = sessionData.getProperty("access_level");
			if (strAccessLevel != null && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
				if(remove(sData)){
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.terminal.unassigned", sessionData.getLanguage())});
					json.put("status", "ok");
				}else{
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.operationerror", sessionData.getLanguage())});
					json.put("status", "error");
				}
			}else{
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during removeAssignmentAjax", e);
			throw new PincacheTemplateException();
		}
		return json;
	}
	
	public static JSONObject createAssignmentAjax(String sData, SessionData sessionData) throws PincacheTemplateException{
		JSONObject json = new JSONObject();
		try {
			String strAccessLevel = sessionData.getProperty("access_level");
			if (strAccessLevel != null && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
				String[] sd = sData.split("_");
				PcTerminalTemplate tp = new PcTerminalTemplate();
				tp.setTerminal_id(sd[0]);
				tp.setTemplate_id(sd[1]);
				if(!exists(tp)){
					if(save(tp)){
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.terminal.assigned", sessionData.getLanguage())});
						json.put("status", "ok");
					}else{
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.terminal.saveerror", sessionData.getLanguage())});
						json.put("status", "error");
					}
				}else{
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.terminal.alreadyassigned", sessionData.getLanguage())});
					json.put("status", "error");
				}
			}else{
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during createAssignmentAjax", e);
			throw new PincacheTemplateException();
		}
		return json;
	}

	private static boolean remove(String terminal_id) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("removeTerminalTemplate"));
			cat.debug("PcAssignmentTemplate.getAllByTemplateId: " + sql_bundle.getString("removeTerminalTemplate") + "/* "+ terminal_id +" */");
			pstmt.setString(1, terminal_id);
			pstmt.execute();
			ok = true;
		} catch (Exception ex) {
			cat.error("Error during PcTerminalTemplate.remove. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}	
	
	private static boolean save(PcTerminalTemplate tp) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("assignTerminalTemplate"));
			cat.debug("PcAssignmentTemplate.getAllByTemplateId: " + sql_bundle.getString("assignTerminalTemplate") + "/* "+ tp.terminal_id +", "+ tp.template_id +" */");
			pstmt.setString(1, tp.terminal_id);
			pstmt.setString(2, tp.template_id);
			pstmt.execute();
			ok = true;
		} catch (Exception ex) {
			cat.error("Error during PcTerminalTemplate.save. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}
	
	private static boolean exists(PcTerminalTemplate tp) throws PincacheTemplateException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("countTerminalTemplate"));
			cat.debug("countTerminalTemplate: " + sql_bundle.getString("countTerminalTemplate") + "/* "+ tp.terminal_id +", "+ tp.template_id +" */");
			pstmt.setString(1, tp.terminal_id);
			pstmt.setString(2, tp.template_id);
			rs = pstmt.executeQuery();
			if(rs.next() && rs.getInt(1) > 0){
				ok = true;
			}
		} catch (Exception ex) {
			cat.error("Error during PcTerminalTemplate.exists. " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}

}
