package com.debisys.pincache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.PincacheException;
import com.debisys.languages.Languages;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.dbUtils.TorqueHelper;

public class PcTermlSync {
	
	{
		filterSiteId = 0L;
	}

	private static Logger cat = Logger.getLogger(PcTermlSync.class);

	private static String filterEndDate;

	private static Long filterSiteId;
	
	private static String filterStartDate;
	
	private static String filterRepId;
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pincache.sql");

	public static String formatCurrency(String s) {
		DecimalFormat money = new DecimalFormat("$0.00");
		double amount = 0.0;
		try{
			amount = Double.parseDouble(s);
		}catch(Exception e){}
		return money.format(amount);
	}

	/**
	 * @return the filterEndDate
	 */
	public static String getFilterEndDate() {
		return filterEndDate;
	}

	/**
	 * @return the filterSiteId
	 */
	public static Long getFilterSiteId() {
		return filterSiteId;
	}

	/**
	 * @return the filterStartDate
	 */
	public static String getFilterStartDate() {
		return filterStartDate;
	}
	
	public static ArrayList<String> getReportHeaders(String lang){
		ArrayList<String> headers = new ArrayList<String>();
		headers.add(Languages.getString("jsp.admin.pincache.reportsync.header1", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportsync.header2", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportsync.header3", lang));
		headers.add(Languages.getString("jsp.admin.pincache.reportsync.header4", lang));
		return headers;		
	}

	public static ArrayList<ArrayList<String>> getSyncReport(String strDistChainType, String strAccessLevel) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<ArrayList<String>> products = new ArrayList<ArrayList<String>>();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheException();
			}
			String query = "SELECT t.millennium_no, ps.syncDate, ps.soldPinsCount, ps.nonSoldPinsCount FROM terminals AS t WITH(NOLOCK) INNER JOIN PinCacheTerminalSynchronization AS ps WITH(NOLOCK) ON(ps.terminal_id = t.terminal_id) INNER JOIN merchants AS m ON(t.merchant_id = m.merchant_id) INNER JOIN reps AS r1 ON(r1.rep_id = m.rep_id) ";
			String chainfields = "'', '',";
			if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
				chainfields = "r4.businessname, r3.businessname, ";
				query += "	INNER JOIN reps AS r2 ON(r2.rep_id = r1.iso_id)"
						+"	INNER JOIN reps AS r3 ON(r3.rep_id = r2.iso_id)"
						+"	INNER JOIN reps AS r4 ON(r4.rep_id = r3.iso_id AND r4.iso_id = r4.rep_id) ";
				if(strAccessLevel.equals(DebisysConstants.ISO)){
					query += "	WHERE (r4.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.AGENT)){
					query += "	WHERE (r3.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.SUBAGENT)){
					query += "	WHERE (r2.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.REP)){
					query += "	WHERE (r1.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
					query += "	WHERE (m.merchant_id = ?)";
				}
			}else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
				chainfields = "";
				query += "	INNER JOIN reps AS r2 ON(r2.rep_id = r1.iso_id AND r2.iso_id = r2.rep_id)";
				if(strAccessLevel.equals(DebisysConstants.ISO)){
					query += "	WHERE (r2.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.REP)){
					query += "	WHERE (r1.rep_id = ?)";
				}else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
					query += "	WHERE (m.merchant_id = ?)";
				}
			}
			query = String.format(query, chainfields);
			//query +=  " AND (r.rep_id = "+ filterRepId +" OR r.iso_id = "+ filterRepId +")";
			if(filterSiteId != 0){
				query += " AND t.millennium_no = ?";
			}
			boolean doFilterDate  =false;
			if(filterStartDate != null && !filterStartDate.equals("") && filterEndDate != null && !filterEndDate.equals("")){
				filterStartDate += " 00:00:00";
				filterEndDate += " 23:59:59";
				query += " AND (ps.syncDate >= ? AND ps.syncDate <= ?)";
				doFilterDate = true;
			}
			cat.debug("getSyncReportByTermId: " + query + "/* " + filterRepId +", " + filterSiteId +", " + filterStartDate +", " + filterEndDate +" */");
			
			pstmt = con.prepareStatement(query);
			int j = 1;
			pstmt.setString(j++, filterRepId);
			if(filterSiteId != 0){
				pstmt.setLong(j++, filterSiteId);
			}
			if(doFilterDate){
				pstmt.setString(j++, filterStartDate);
				pstmt.setString(j++, filterEndDate);
			}
			rs = pstmt.executeQuery();
			if(rs != null){
				ResultSetMetaData rsMetaData = rs.getMetaData();
				int numCols = rsMetaData.getColumnCount();
				while(rs.next()){
					ArrayList<String> v = new ArrayList<String>();
					
					for(int i=1; i <= numCols; i++){
						String s = rs.getString(i);
						/*if(i == numCols){
							s = formatCurrency(s); 
						}*/
						if(s == null){
							s = "";
						}
						v.add(s);
					}
					products.add(v);
				}
			}
		} catch (Exception ex) {
			cat.error("Error during getSyncReportByTermId. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return products;
	}

	/**
	 * @param filterEndDate the filterEndDate to set
	 */
	public static void setFilterEndDate(String filterEndDate) {
		PcTermlSync.filterEndDate = filterEndDate;
	}

	/**
	 * @param filterSiteId the filterSiteId to set
	 */
	public static void setFilterSiteId(Long filterSiteId) {
		PcTermlSync.filterSiteId = filterSiteId;
	}

	public static void setFilterSiteId(String filterSiteId) {
		try{
			PcTermlSync.filterSiteId = Long.parseLong(filterSiteId);
		}catch(Exception ex){
			PcTermlSync.filterSiteId = 0L;
		}
	}

	/**
	 * @param filterStartDate the filterStartDate to set
	 */
	public static void setFilterStartDate(String filterStartDate) {
		PcTermlSync.filterStartDate = filterStartDate;
	}

	private long nonSoldPinsCount;
	private long soldPinsCount;
	private Date syncDate;
	private long terminal_id;

	/**
	 * @return the nonSoldPinsCount
	 */
	public long getNonSoldPinsCount() {
		return nonSoldPinsCount;
	}

	/**
	 * @return the soldPinsCount
	 */
	public long getSoldPinsCount() {
		return soldPinsCount;
	}

	/**
	 * @return the syncDate
	 */
	public Date getSyncDate() {
		return syncDate;
	}

	/**
	 * @return the terminal_id
	 */
	public long getTerminal_id() {
		return terminal_id;
	}

	/**
	 * @param nonSoldPinsCount the nonSoldPinsCount to set
	 */
	public void setNonSoldPinsCount(long nonSoldPinsCount) {
		this.nonSoldPinsCount = nonSoldPinsCount;
	}

	/**
	 * @param soldPinsCount the soldPinsCount to set
	 */
	public void setSoldPinsCount(long soldPinsCount) {
		this.soldPinsCount = soldPinsCount;
	}
	
	/**
	 * @param syncDate the syncDate to set
	 */
	public void setSyncDate(Date syncDate) {
		this.syncDate = syncDate;
	}	

	/**
	 * @param terminal_id the terminal_id to set
	 */
	public void setTerminal_id(long terminal_id) {
		this.terminal_id = terminal_id;
	}

	/**
	 * @return the filterRepId
	 */
	public static String getFilterRepId() {
		return filterRepId;
	}

	/**
	 * @param filterRepId the filterRepId to set
	 */
	public static void setFilterRepId(String filterRepId) {
		PcTermlSync.filterRepId = filterRepId;
	}

}
