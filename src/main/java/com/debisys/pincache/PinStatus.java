package com.debisys.pincache;

public class PinStatus {
	public static final int AVAILABLE = 1;// Available
	public static final int ASSIGNED = 2;// Assigned
	public static final int SOLD = 3;// Sold
	public static final int DOWNLOADED = 4;// Downloaded
	public static final int QUARANTINED = 5;// Quarantined
	public static final int DISABLED = 6;// Disabled
	public static final int PENDING_RETURN = 7;// Pending Return
	public static final int PENDING_SOLD = 8;// Pending Sold
	public static final int PENDING_QUARANTINE = 9;// Pending Quarantine
	public static final int PENDING_SOLD_QUARANTINE = 10;// Pending Sold in Quarantine
	public static final int PENDING_RETURN_QUARANTINE = 11;// Pending Return in Quarantine
}
