package com.debisys.pincache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.exceptions.PincacheException;
import com.debisys.exceptions.PincacheTemplateException;
import com.debisys.languages.Languages;
import com.debisys.terminals.Terminal;
import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;

public class PcAssignment {

	private static final String ADM_SUP_SITE = "AdmSupSite";

	private static final int ACTIVATION = 1;

	private static final int SALE = 1;

	private static Logger cat = Logger.getLogger(PcAssignment.class);

	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pincache.sql");

	static boolean changeStatus(PcAssignment pa, String toStatus, String userNote, SessionData sessionData) {
		boolean ok = false;
		try {
			int newStatus = Integer.parseInt(toStatus);
			if (pa != null) {
				long merchantId = getPcAsMerchantId(pa);
				String note = "";
				switch (pa.getPinStatusId()) {
				case PinStatus.ASSIGNED:
					switch (newStatus) {
					case PinStatus.SOLD:
						ok = goProcessPin(pa);
						break;
					case PinStatus.QUARANTINED:
						goChangePINStatus(pa, newStatus);
						ok = true;
						break;
					/*case PinStatus.PENDING_SOLD:
						goChangePINStatus(pa, newStatus);
						putIntoNonProcessed(pa);
						ok = true;
						break;*/
					}
					break;
				case PinStatus.QUARANTINED:
					switch (newStatus) {
					case PinStatus.ASSIGNED:
						goChangePINStatus(pa, newStatus);
						ok = true;
						break;
					case PinStatus.SOLD:
						ok = goProcessPin(pa);
						break;
					/*case PinStatus.PENDING_SOLD:
						goChangePINStatus(pa, newStatus);
						putIntoNonProcessed(pa);
						ok = true;
						break;*/
					}
					break;
				case PinStatus.PENDING_SOLD:
					switch (newStatus) {
					case PinStatus.SOLD:
						ok = goProcessPin(pa);
						break;
					}
					break;
				}
				if (ok) {
					note = Languages.getString(
							"jsp.admin.pincache.statusChanged",
							new String[] { pa.getControl_no(),
									Languages.getString("jsp.admin.pincache.status" + pa.getPinStatusId(), sessionData.getLanguage()),
									Languages.getString("jsp.admin.pincache.status" + newStatus, sessionData.getLanguage()), }, sessionData.getLanguage())
							+ "\n " + userNote;
				} else {
					note = Languages.getString(
							"jsp.admin.pincache.statusChangedFail",
							new String[] { pa.getControl_no(),
									Languages.getString("jsp.admin.pincache.status" + pa.getPinStatusId(), sessionData.getLanguage()),
									Languages.getString("jsp.admin.pincache.status" + newStatus, sessionData.getLanguage()), }, sessionData.getLanguage())
							+ "\n " + userNote;
				}
				new Merchant().addMerchantNote(merchantId, note, sessionData.getUser().getUsername());
			}
		} catch (PincacheException e) {
			e.printStackTrace();
		}
		return ok;
	}

	private static void putIntoNonProcessed(PcAssignment pa) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("putIntoNonProcessed"));
			cat.debug("putIntoNonProcessed: " + sql_bundle.getString("putIntoNonProcessed") + "/* " + pa.getId() + " */");
			pstmt.setLong(1, pa.getTerminalId());
			pstmt.setString(2, pa.getPin());
			pstmt.setLong(3, pa.getProductId());
			pstmt.setLong(4, Long.parseLong(pa.getControl_no()));
			pstmt.setLong(5, pa.getTransactionId());
			pstmt.setString(6, ADM_SUP_SITE);
			pstmt.execute();
		} catch (Exception ex) {
			cat.error("Error during putIntoNonProcessed. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
	}

	private static long getPcAsMerchantId(PcAssignment pa) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		long merchant_id = 0L;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPcAsMerchantId"));
			cat.debug("getPcAsMerchantId: " + sql_bundle.getString("getPcAsMerchantId") + "/* " + pa.getId() + " */");
			pstmt.setString(1, pa.getId());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				merchant_id = rs.getLong(1);
			}
		} catch (Exception ex) {
			cat.error("Error during getPcAsMerchantId. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return merchant_id;
	}

	/**
	 * Remove the PinCacheAssignment record
	 * 
	 * @param pa
	 * @throws PincacheException
	 */
	private static boolean removePINAssigment(PcAssignment pa){
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean ok =  false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("removePINAssigment"));
			cat.debug("removePINAssigment: " + sql_bundle.getString("removePINAssigment") + "/* " + pa.getTerminalId() + ", " + pa.getProductId() + ", "
					+ pa.getPin() + " */");
			pstmt.setLong(1, pa.getTerminalId());
			pstmt.setLong(2, pa.getProductId());
			pstmt.setString(3, pa.getPin());
			pstmt.execute();
			ok = true;
		} catch (Exception ex) {
			cat.error("Error during removePINAssigment. " + ex);
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}

	private static boolean goProcessPin(PcAssignment pa) throws PincacheException {
		boolean ok = false;
		SimpleDateFormat sd =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		StringBuilder logTtrnx = new StringBuilder();
		Date initTime = new Date();
		int soldCode = PinStatus.SOLD;
		PcNonProcessedSale npi = getNonProcessedSaleInfo(pa);
		if(npi == null){
			npi = new PcNonProcessedSale();
			npi.setClerk(ADM_SUP_SITE);
			npi.setInvoiceNumber(0L);
			npi.setSaleDate(new Date());
		}
		int resultCode = 12;
		String resultCodeTxt = "Sale could not be done";
		try{
			if(pa != null){
				cat.debug("Checking if pin is already sold");
				logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Checking if pin is already sold");
				if (!isSoldPin(pa)) {
					cat.debug("Saving transaction");
					logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Saving transaction");
					if (goSellPiN(pa, npi.getClerk(), npi.getInvoiceNumber(), npi.getSaleDate())){
						cat.debug("Updating pin status");
						logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Updating pin status\n");
						if(goChangePINStatus(pa, PinStatus.SOLD)){
							logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Removing pin assignment\n");
							cat.debug("Removing pin assignment");
							if(removePINAssigment(pa)){
								cat.debug("Successful sale");
								removeNonProcessed(pa);
								logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Successful sale\n");
								resultCodeTxt = "Successful processed request";
								resultCode = 0;
								ok = true;
							}else{
								cat.debug("Pin assignment could not be deleted");
								logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Pin assignment could not be deleted\n");
								resultCodeTxt = "Assignment removal failed";
								resultCode = 10;
							}
						}else{
							cat.debug("Pin status could not be updated");
							logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Pin status could not be updated\n");
							resultCodeTxt = "Failed to update pin status";
							resultCode = 11;
						}
					}else{
						cat.debug("Pin could not be sold");
						logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Pin could not be sold\n");
						resultCodeTxt = "Sale could not be done";
						resultCode = 12;
						goChangePINStatus(pa, PinStatus.PENDING_SOLD);
						putIntoNonProcessed(pa);
						logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Pin changed to PENDING_SOLD state\n");
						cat.debug("Pin changed to PENDING_SOLD state");
					}
				}else{
					logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Pin already sold\n");
					cat.debug("Pin already sold");
					resultCodeTxt = "Pin already sold";
					resultCode = 9;
				}
			}else{
				throw new PincacheException();
			}
		}catch (Exception e){
			cat.error("Error during goSell. " + e);
			logTtrnx.append(sd.format(new Date()) + "[" + pa.getTransactionId() + " - " + pa.getControl_no()  + "]" + "Error during goSell.\n" + e);
		}finally{
			long diff = (new Date()).getTime() - initTime.getTime();
			long duration = diff / 1000;
			logHostTransaction(pa, npi, logTtrnx.toString(), duration, resultCode, resultCodeTxt);
		}
		return ok;
	}

	private static void logHostTransaction(PcAssignment pa, PcNonProcessedSale npi, String transactionText, long duration, int resultCode, String resultCodeTxt) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			int j = 1;
			SimpleDateFormat sd =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String receiveString = String.format("[Product Id={0}|Pin={1}|Control Number={2}|Pin Status={3}|Transaction Id={4}|Clerk={5}|InvoiceNumber={6}|Sale Date={7}]", pa.getProductId(), pa.getPin(), pa.getControl_no(), pa.getPinStatusId(), pa.getTransactionId(), npi.getClerk(), npi.getInvoiceNumber(), sd.format(npi.getSaleDate()));
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("logHostTransaction"));
			cat.debug("logHostTransaction: " + sql_bundle.getString("logHostTransaction") + "/* " +  " */");
			pstmt.setLong(j++, npi.getSiteId());//@account_id = terminal.Millennium_no, SqlDbType.Int
			pstmt.setTimestamp(j++, new java.sql.Timestamp((new Date()).getTime()));//@datetime = DateTime.Now, SqlDbType.DateTime
			pstmt.setLong(j++, pa.getTransactionId());//@transaction_id = soldPin.TransactionId, SqlDbType.Int // The transaction ID was obtained when the pin was reserved
			pstmt.setDouble(j++, 0);//@db_wait = 0, SqlDbType.Float
			pstmt.setInt(j++, ACTIVATION);//@type = TRANSACTION_TYPES.ACTIVATION, SqlDbType.Int
			pstmt.setString(j++, receiveString);//@receiveString = soldPin.ToString(), SqlDbType.Char, 150
			pstmt.setString(j++, ADM_SUP_SITE);//@hostId = DEFAULT_APP_NAME, SqlDbType.Char, 10
			pstmt.setInt(j++, 80);//@port = 80, SqlDbType.Int
			pstmt.setLong(j++, duration);//@duration = duration, SqlDbType.Int
			pstmt.setInt(j++, 1);//@status = 1, SqlDbType.Int
			pstmt.setString(j++, transactionText);//@transText = transactionText.ToString(), SqlDbType.VarChar, 7000
			pstmt.setString(j++, String.valueOf(pa.getProductId()));//@info1 = string.Empty, SqlDbType.Char, 30
			pstmt.setString(j++, String.valueOf(pa.getPin()));//@info2 = string.Empty, SqlDbType.VarChar, 128
			pstmt.setLong(j++, npi.getInvoiceNumber());//@invoice_no = soldPin.InvoiceNumber, SqlDbType.Int
			pstmt.setInt(j++, resultCode);//@result_code = int.Parse(resultCode.key), SqlDbType.Int
			pstmt.setDouble(j++, pa.getAmount());//@amount = soldPin.ProductAmmount, SqlDbType.Decimal
			pstmt.setString(j++, resultCodeTxt);//@response = resultCode.value, SqlDbType.VarChar, 512
			pstmt.setLong(j++, pa.getProductId());//@productId = soldPin.ProductId, SqlDbType.Int
			pstmt.execute();
		} catch (Exception ex) {
			cat.error("Error during logHostTransaction. " + ex);
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
	}

	private static PcNonProcessedSale getNonProcessedSaleInfo(PcAssignment pa) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PcNonProcessedSale npi = null;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getNonProcessedSaleInfo"));
			cat.debug("getNonProcessedSaleInfo: " + sql_bundle.getString("getNonProcessedSaleInfo") + "/* " + " */");
			pstmt.setLong(1, pa.getTerminalId());
			pstmt.setLong(2, pa.getProductId());
			pstmt.setString(3, pa.getPin());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				npi = new PcNonProcessedSale();
				npi.setSaleDate((Date)rs.getDate(1));
				npi.setClerk(rs.getString(2));
				npi.setInvoiceNumber(rs.getLong(3));
				npi.setSiteId(rs.getLong(3));
			}
		} catch (Exception ex) {
			cat.error("Error during getNonProcessedSaleInfo. " + ex);
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return npi;
	}

	private static void removeNonProcessed(PcAssignment pa) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("removePcNonProcessedSales"));
			cat.debug("removePcNonProcessedSales: " + sql_bundle.getString("removePcNonProcessedSales") + "/* " + pa.getId() + " */");
			pstmt.setLong(1, pa.getTerminalId());
			pstmt.setLong(2, pa.getProductId());
			pstmt.setString(3, pa.getPin());
		} catch (Exception ex) {
			cat.error("Error during removeNonProcessed. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
	}

	private static boolean isSoldPin(PcAssignment pa) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean isIn = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("isSoldPin"));
			cat.debug("isSoldPin: " + sql_bundle.getString("isSoldPin") + "/* " + pa.getId() + " */");
			pstmt.setLong(1, Long.parseLong(pa.getControl_no()));
			pstmt.setString(2, pa.getPin());
			pstmt.setLong(3, pa.getProductId());
			pstmt.setInt(4, PinStatus.SOLD);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				isIn = (rs.getInt(1) > 0);
			}
		} catch (Exception ex) {
			cat.error("Error during isSoldPin. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return isIn;
	}

	private static boolean goChangePINStatus(PcAssignment pa, int newStatus){
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("goChangePINStatus"));
			cat.debug("goChangePINStatus: " + sql_bundle.getString("goChangePINStatus") + "/* " + newStatus + ", " + pa.getControl_no() + " */");
			pstmt.setInt(1, newStatus);
			pstmt.setString(2, pa.getControl_no());
			pstmt.execute();
			ok  =true;
		} catch (Exception ex) {
			cat.error("Error during goChangePINStatus. " + ex);
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}

	private static boolean goSellPiN(PcAssignment pa, String clerk, Long invoiceNumber , java.util.Date saleDate) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("goSellPiN"));
			cat.debug("goSellPiN: " + sql_bundle.getString("goSellPiN") + "/* " + pa.getTerminalId() + ", " + pa.getControl_no() + ", " +ACTIVATION  + ", " +pa.getAmount() + ", " +clerk + ", " +"SUPPORT SITE" + ", " +80 + ", " +saleDate.getTime() + ", " + pa.getTransactionId() + ", " +0 + ", " +0 + ", " +SALE + ", " +pa.getProductId() + ", " +invoiceNumber + " */");
			pstmt.setTimestamp(1, new java.sql.Timestamp(saleDate.getTime()));
			pstmt.setLong(2, pa.getTerminalId());// @account_id = terminal.Millennium_no, SqlDbType.Int
			pstmt.setLong(3, Long.parseLong(pa.getControl_no()));// @control_no = soldPin.ControlNo, SqlDbType.Int
			pstmt.setInt(4, ACTIVATION);
			pstmt.setDouble(5, pa.getAmount());// @amount = soldPin.ProductAmmount, SqlDbType.Money
			pstmt.setString(6, clerk);// @clerk = soldPin.Clerk, SqlDbType.Char, 10
			pstmt.setString(7, "SUPPORT SITE");// @client = DEFAULT_APP_NAME, SqlDbType.Char, 10
			pstmt.setInt(8, 80);
			pstmt.setLong(9, pa.getTransactionId());// @transaction_id = soldPin.TransactionId, SqlDbType.Int
			pstmt.setInt(10, 0);// @new_balance
			pstmt.setInt(11, 0);// @product_buy_rate
			pstmt.setInt(12, SALE);// @billing_typeId
			pstmt.setLong(13, pa.getProductId());// @RecordedProductId = soldPin.ProductId, SqlDbType.Int
			pstmt.setLong(14, invoiceNumber);// @RecordedProductId = soldPin.ProductId, SqlDbType.Int
			pstmt.execute();
			ok = true;
		} catch (Exception ex) {
			cat.error("Error during goSellAssignment. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}

	static PcAssignment getPcAsById(String pa_id) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		PcAssignment pa = new PcAssignment();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPcAsById"));
			cat.debug("getPcAsById: " + sql_bundle.getString("getPcAsById") + "/* " + pa_id + " */");
			pstmt.setString(1, pa_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				pa.setId(rs.getString(1));
				pa.description = rs.getString(2);
				if (pa.description == null) {
					pa.description = "";
				}
				pa.sequence_no = rs.getString(3);
				if (pa.sequence_no == null) {
					pa.sequence_no = "";
				}
				pa.control_no = rs.getString(4);
				if (pa.control_no == null) {
					pa.control_no = "";
				}
				pa.carrier_control_no = rs.getString(5);
				if (pa.carrier_control_no == null) {
					pa.carrier_control_no = "";
				}
				pa.amount = rs.getDouble(6);
				pa.pin = rs.getString(7);
				if (pa.pin == null) {
					pa.pin = "";
				}
				pa.pinStatusId = rs.getInt(8);
				pa.terminalId = rs.getLong(9);
				pa.productId = rs.getLong(10);
				pa.transactionId = rs.getLong(11);
			}
		} catch (Exception ex) {
			cat.error("Error during getPcAsById. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return pa;
	}

	public static ArrayList<PcAssignment> getPINsBySiteId(String site_id) throws PincacheException {
		return getPINsByTerminalId(new Terminal().getTerminalIdBySite(site_id));
	}

	public static ArrayList<PcAssignment> getPINsByTerminalId(String terminal_id) throws PincacheException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<PcAssignment> pins = new ArrayList<PcAssignment>();
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getPINsByTerminalId"));
			cat.debug("getPINsByTerminalId: " + sql_bundle.getString("getPINsByTerminalId") + "/* " + terminal_id + " */");
			pstmt.setString(1, terminal_id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				PcAssignment p = new PcAssignment();// new PcAssignment(rs.getString(1), rs.getLong(2), rs.getLong(3), rs.getString(4), rs.getString(5));
				p.setId(rs.getString(1));
				p.description = rs.getString(2);
				if (p.description == null) {
					p.description = "";
				}
				p.sequence_no = rs.getString(3);
				if (p.sequence_no == null) {
					p.sequence_no = "";
				}
				p.control_no = rs.getString(4);
				if (p.control_no == null) {
					p.control_no = "";
				}
				p.carrier_control_no = rs.getString(5);
				if (p.carrier_control_no == null) {
					p.carrier_control_no = "";
				}
				p.amount = rs.getDouble(6);
				p.pin = rs.getString(7);
				if (p.pin == null) {
					p.pin = "";
				}
				p.pinStatusId = rs.getInt(8);
				p.terminalId = rs.getLong(9);
				p.productId = rs.getLong(10);
				p.transactionId = rs.getLong(11);
				pins.add(p);
			}
		} catch (Exception ex) {
			cat.error("Error during getPINsByTerminalId. " + ex);
			throw new PincacheException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return pins;
	}

	private double amount;

	private String carrier_control_no;

	private String control_no;

	private String description;

	private String id;

	private String pin;

	private int pinStatusId;

	private long terminalId;

	private long productId;

	private long transactionId;

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId
	 *            the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the terminalId
	 */
	public long getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId
	 *            the terminalId to set
	 */
	public void setTerminalId(long terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @return the productId
	 */
	public long getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(long productId) {
		this.productId = productId;
	}

	private String sequence_no;

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @return the carrier_control_no
	 */
	public String getCarrier_control_no() {
		return carrier_control_no;
	}

	/**
	 * @return the control_no
	 */
	public String getControl_no() {
		return control_no;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * @return the pinStatusId
	 */
	public int getPinStatusId() {
		return pinStatusId;
	}

	/**
	 * @return the sequence_no
	 */
	public String getSequence_no() {
		return sequence_no;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @param carrier_control_no the carrier_control_no to set
	 */
	public void setCarrier_control_no(String carrier_control_no) {
		this.carrier_control_no = carrier_control_no;
	}

	/**
	 * @param control_no the control_no to set
	 */
	public void setControl_no(String control_no) {
		this.control_no = control_no;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @param pin the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}

	/**
	 * @param pinStatusId the pinStatusId to set
	 */
	public void setPinStatusId(int pinStatusId) {
		this.pinStatusId = pinStatusId;
	}

	/**
	 * @param sequence_no the sequence_no to set
	 */
	public void setSequence_no(String sequence_no) {
		this.sequence_no = sequence_no;
	}
}