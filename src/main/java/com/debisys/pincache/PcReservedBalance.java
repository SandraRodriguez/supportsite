package com.debisys.pincache;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.PincacheTemplateException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.JSONObject;
import com.emida.utils.dbUtils.TorqueHelper;

public class PcReservedBalance {
	
	private static Logger cat = Logger.getLogger(PcReservedBalance.class);

	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pincache.sql");

	public static double getRBByMerchantId(String merchant_id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		double rb = 0.0;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getRBByMerchantId"));
			cat.debug("PcReservedBalance.getRBByMerchantId: " + sql_bundle.getString("getRBByMerchantId") + " /* " + merchant_id + " */");
			pstmt.setString(1, merchant_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				rb = rs.getDouble(1);
			}
		} catch (Exception ex) {
			cat.error("Error during PcReservedBalance.getRBByMerchantId: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return rb;
	}

	public static double getRBByRepId(String rep_id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		double rb = 0.0;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getRBByRepId"));
			cat.debug("PcReservedBalance.getRBByRepId: " + sql_bundle.getString("getRBByRepId") + " /* " + rep_id + " */");
			pstmt.setString(1, rep_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				rb = rs.getDouble(1);
			}
		} catch (Exception ex) {
			cat.error("Error during PcReservedBalance.getRBByRepId: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return rb;
	}
	
	public static JSONObject merchantReserveAjax(String sData, SessionData sessionData) throws PincacheTemplateException{
		JSONObject json = new JSONObject();
		try{
			try {
				//String strAccessLevel = sessionData.getProperty("access_level");
				if (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
					String[] sd = sData.split("_"); // merchantId, amount, sDeploymentType_sCustomConfigType
					boolean isReturn = Boolean.parseBoolean(sd[4]);
					double val = Double.parseDouble(sd[1]);
					if(val > 0.0){
						if(reserveForMerchant(sd[0], val, sd[2], sd[3], sessionData.getLanguage(), isReturn)){
							if(isReturn){
								json.put("msgs", new String[]{Languages.getString("jsp.admin.reservedbalance.returned", sessionData.getLanguage())});
							}else{
								json.put("msgs", new String[]{Languages.getString("jsp.admin.reservedbalance.reserved", sessionData.getLanguage())});
							}
							json.put("status", "ok");
						}else{
							json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.operationerror", sessionData.getLanguage())});
							json.put("status", "error");
						}
					}else{
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.invalidamount", sessionData.getLanguage())});
						json.put("status", "error");
					}
				}else{
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
					json.put("status", "error");
				}
			}catch (NumberFormatException fe){
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.invalidamount", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during removeAssignmentAjax", e);
			throw new PincacheTemplateException();
		}
		return json;
	}
	
	static boolean reserveForMerchant(String merchant_id, double value, String sDeploymentType, String sCustomConfigType, String lang, boolean isReturn) throws PincacheTemplateException {
		Connection con = null;
		CallableStatement cs = null;
		PreparedStatement pstmt = null;
		boolean ok = false;
		if(isReturn){
			double minBalance = getMinBalanceByMerchant(merchant_id);
			double currentBalance = getRBByMerchantId(merchant_id);
			double maxToReturn = currentBalance - minBalance; 
			if(value > maxToReturn){
				return false;
			}
		}
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			cs = con.prepareCall(sql_bundle.getString("applyMerchantPayment"));
			cat.debug("PcReservedBalance.reserveForMerchant: " + sql_bundle.getString("applyMerchantPayment") + " /* " + " */");
			cs.setString(1, "0"); //@Millennium_no INT,
			if(isReturn){
				cs.setDouble(2, value);// @PaymentAmount MONEY,
			}else{
				cs.setDouble(2, -value);// @PaymentAmount MONEY,
			}
			cs.setDate(3, new java.sql.Date((new java.util.Date()).getTime()));// @DepositDate DATETIME = NULL,
			cs.setDouble(4, 0.0);// @Commission MONEY = 0,
			cs.setInt(5, 1);//  @CommissionByValue BIT = 1,--Indicates if the commission is by value or by %
			cs.setString(6, Languages.getString("jsp.admin.reservedbalance.reserve", lang));//  @PaymentDescription NVARCHAR(255) = '',
			cs.setString(7, "N/A");//@Bank NVARCHAR(50) = '', 
			cs.setString(8, "N/A");//@RefNumber NVARCHAR(50) = '',
			cs.setString(9, "N/A");//@UserName NVARCHAR(255) = 'ApplyMerchantPayment SP',
			cs.setString(10, sDeploymentType);//  @DeploymentType INT = 1,--0 is Domestic, 1 is International
			cs.setString(11, sCustomConfigType);//  @CustomConfigType INT = 1,--0 is Default, 1 is Mexico
			
			cs.registerOutParameter(12, Types.INTEGER); //  @ErrorCode INT OUTPUT,
			cs.setInt(12, 0);
			
			cs.registerOutParameter(13, Types.VARCHAR); //  @ErrorDesc NVARCHAR(255) = '' OUTPUT,
			cs.setString(13, "");

			cs.setInt(14, 0);//  @MerchantCreditPaymentType INT = NULL,--Added for Tiendas consentidas
			cs.setInt(15, 0);//  @Bank_id INT = NULL,--Added for Tiendas consentidas
			cs.setString(16, merchant_id);//  @MerchantID AS NUMERIC(15, 0) = 0, -- Added for MX Fee Daemon, in FeeAgreements the siteId is not available, but the merchantId is available, then the SP will use it
			//  @ExternalRepID NVARCHAR(50) = NULL
			cs.execute();
			int nResult = cs.getInt(12);
			if(nResult == 0){
				pstmt = con.prepareStatement(sql_bundle.getString("reserveMerchantBalance"));
				cat.debug("PcReservedBalance.reserveForMerchant: " + sql_bundle.getString("reserveMerchantBalance") + " /* " + " */");
				pstmt.setString(1, merchant_id);
				if(isReturn){
					pstmt.setDouble(2, -value);
				}else{
					pstmt.setDouble(2, value);
				}
				pstmt.execute();
				ok = true;
			}
		} catch (Exception ex) {
			cat.error("Error during PcReservedBalance.reserveForMerchant: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, cs, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
			try {
				TorqueHelper.closeConnection(null, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}
	
	public static JSONObject repReserveAjax(String sData, SessionData sessionData) throws PincacheTemplateException{
		JSONObject json = new JSONObject();
		try{
			try {
				//String strAccessLevel = sessionData.getProperty("access_level");
				if (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
					String[] sd = sData.split("_"); // repId, amount, sDeploymentType_sCustomConfigType
					boolean isReturn = Boolean.parseBoolean(sd[4]);
					double val = Double.parseDouble(sd[1]);
					if(val > 0.0){					
						if(reserveForRep(sd[0], val, sd[2], sd[3], sessionData.getLanguage(), isReturn)){
							if(isReturn){
								json.put("msgs", new String[]{Languages.getString("jsp.admin.reservedbalance.returned", sessionData.getLanguage())});
							}else{
								json.put("msgs", new String[]{Languages.getString("jsp.admin.reservedbalance.reserved", sessionData.getLanguage())});
							}
							json.put("status", "ok");
						}else{
							json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.operationerror", sessionData.getLanguage())});
							json.put("status", "error");
						}
					}else{
						json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.invalidamount", sessionData.getLanguage())});
						json.put("status", "error");
					}
				}else{
					json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.noaccess", sessionData.getLanguage())});
					json.put("status", "error");
				}
			}catch (NumberFormatException fe){
				json.put("msgs", new String[]{Languages.getString("jsp.admin.pincache.invalidamount", sessionData.getLanguage())});
				json.put("status", "error");
			}
		}catch (Exception e){
			cat.error("Error during repReserveAjax", e);
			throw new PincacheTemplateException();
		}
		return json;
	}

	private static boolean reserveForRep(String rep_id, double value, String sDeploymentType, String sCustomConfigType, String lang, boolean isReturn) throws PincacheTemplateException {
		Connection con = null;
		CallableStatement cs = null;
		PreparedStatement pstmt = null;
		boolean ok = false;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			//pstmt = con.prepareStatement(sql_bundle.getString("reserveForRep"));
			//cat.debug("PcReservedBalance.reserveForMerchant: " + sql_bundle.getString("reserveForRep") + " /* " + rep_id + ",  " + value + " */");
			//pstmt.setString(1, rep_id);
			//pstmt.execute();
			/*con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}*/
			
			cs = con.prepareCall(sql_bundle.getString("applyRepPayment"));
			cat.debug("applyRepPayment: " + sql_bundle.getString("applyRepPayment") + " /* " + rep_id + ", " + value + " */");
			cs.setString(1, rep_id); // @RepID NUMERIC(15, 0),
			if(isReturn){
				cs.setDouble(2, value);// @PaymentAmount MONEY,
			}else{
				cs.setDouble(2, -value);// @PaymentAmount MONEY,
			}
			cs.setDouble(3, 0.0);// @Commission MONEY = 0,
			cs.setString(4, Languages.getString("jsp.admin.reservedbalance.reserve", lang));// @PaymentDescription NVARCHAR(255) = '',
			cs.setString(5, "N/A");// @UserName NVARCHAR(255) = 'ApplyRepPayment SP',
			cs.setString(6, sDeploymentType);//  @DeploymentType INT = 1,--0 is Domestic, 1 is International
			cs.setString(7, sCustomConfigType);//  @CustomConfigType INT = 1,--0 is Default, 1 is Mexico
			cs.registerOutParameter(8, Types.INTEGER); //  @ErrorCode INT OUTPUT,
			cs.setInt(8, 0);
			cs.setDate(9, new java.sql.Date((new java.util.Date()).getTime()));// @DepositDate DATETIME = NULL,
			cs.registerOutParameter(10, Types.VARCHAR); //  @ErrorDesc NVARCHAR(255) = '' OUTPUT,
			cs.setString(10, "");
			cs.execute();
			int nResult = cs.getInt(8);
			if(nResult == 0){
				pstmt = con.prepareStatement(sql_bundle.getString("reserveRepBalance"));
				cat.debug("reserveForRep: " + sql_bundle.getString("reserveRepBalance") + " /* " + " */");
				pstmt.setString(1, rep_id);
				if(isReturn){
					pstmt.setDouble(2, -value);
				}else{
					pstmt.setDouble(2, value);
				}
				pstmt.execute();
				ok = true;
			}			
		} catch (Exception ex) {
			cat.error("Error during reserveForRep: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return ok;
	}
	
	public static double getMinBalanceByMerchant(String merchant_id) throws PincacheTemplateException{
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		double mb = 0.0;
		try {
			con = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (con == null) {
				cat.error("Can't get database connection");
				throw new PincacheTemplateException();
			}
			pstmt = con.prepareStatement(sql_bundle.getString("getMinBalanceByMerchant"));
			cat.debug("PcReservedBalance.getMinBalanceByMerchant: " + sql_bundle.getString("getMinBalanceByMerchant") + " /* " + merchant_id + " */");
			pstmt.setString(1, merchant_id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				mb = rs.getDouble(1);
			}
		} catch (Exception ex) {
			cat.error("Error during PcReservedBalance.getRBByRepId: " + ex);
			throw new PincacheTemplateException();
		} finally {
			try {
				TorqueHelper.closeConnection(con, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during release connection", e);
			}
		}
		return mb;
	}
}
