package com.debisys.transactions;

import static com.emida.utils.crypto.TRProxyFactory.dataProtection;

import static com.emida.utils.dbUtils.CommonQueries.getSkuFromPins;
import static com.emida.utils.dbUtils.CommonQueries.getSkusWithEncryption;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import com.debisys.dateformat.DateFormat;
import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.terminals.Terminal;
import com.debisys.tools.MarkedCollection;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TaxTypes;
import com.debisys.utils.TimeZone;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * Holds the information for transactions.<P>
 *
 * @author Jay Chi
 */

public class CarrierSearch implements HttpSessionBindingListener
{

  public boolean isdone = false;
  private String rep_id = "";
  private String product_ids = "";
  private String merchant_ids = "";
  private String merchant_id = "";
  private String start_date = "";
  private String end_date = "";
  private String millennium_no = "";
  
  /*BEGIN Release 21 - Adeed by YH*/
  private String transactionID = "";
  private String invoiceID = "";
  private String PINNumber = "";
  /*END Release 21 - Adeed by YH*/
  
  private Hashtable validationErrors = new Hashtable();
  public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
  public static Calendar dateToday= Calendar.getInstance();
 
  
  public int icount = 1 ;
  public int max = -1 ;
  
  	//DBSY-977
	private String rechargeAmt = "";
	private String criteria = "";
	private String transactionResult = "";
	private String sort = "";
	private String col = "";
	// End DBSY-977
  
  //log4j logging
  static Category cat = Category.getInstance(CarrierSearch.class);
  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.transactions.sql");


/***
   * ADDED FOR LOCALIZATION
   * @return
   */
  public String getStartDateFormatted()
  {
      return this.getDateFormatted(this.start_date);
  }
  
  public void setmax(int nmax)
  {
	  this.max = nmax;
  }
  /***
   * ADDED FOR LOCALIZATION
   * @return
   */
  public String getEndDateFormatted()
  {
      return this.getDateFormatted(this.end_date);
  }
  
  /***
   * ADDED FOR LOCALIZATION
   * @param dateToFormat
   * @return
   */
  private String getDateFormatted(String dateToFormat)
  {
      if(DateUtil.isValid(dateToFormat))
      {
            String date = null;
            try
            {
                // Current support site date is in MM/dd/YYYY.
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                java.util.Date dd = sdf.parse(dateToFormat);
                
                // We will specify a new date format through the database
                SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
                date = sdf1.format(dd); 
                
                // Since Java SimpleDateFormat does not know whether a datetime contains
                // a time or not, remove the time (usually 12:00:00 AM) from the end of the date ranges
                date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
            } 
            catch (ParseException e)
            {
                cat.error("Error during CarrierSearch date localization ", e);
            }      
            return date;
      }
      else
      {
          //Something isn't valid in the date string - don't try to localize it
          return StringUtil.toString(dateToFormat);
      }
  }  
  
  public String getStartDate()
  {
    return StringUtil.toString(this.start_date);
  }

  public void setStartDate(String strValue)
  {
	  this.start_date = strValue;
  }

  public String getEndDate()
  {
    return StringUtil.toString(this.end_date);
  }

  public void setEndDate(String strValue)
  {
	  this.end_date = strValue;
  }
  public String getProductIds()
  {
    return StringUtil.toString(this.product_ids);
  }

  public void setProductIds(String strValue)
  {
	  this.product_ids = strValue;
  }

  public String getMerchantIds()
  {
    return StringUtil.toString(this.merchant_ids);
  }

  public void setMerchantIds(String strValue)
  {
	  this.merchant_ids = strValue;
  }

  public String getRepId()
  {
    return StringUtil.toString(this.rep_id);
  }

  public void setRepIdMultiple(String strValue)
	{
		this.rep_id = strValue;
	}

  public void setRepId(String strValue)
  {
    //double dblRepId;
    try
    {
      /*dblRepId = */Double.parseDouble(strValue);
    }
    catch (NumberFormatException nfe)
    {
      strValue = "";
    }
    this.rep_id = strValue;
  }

  public String getMerchantId()
  {
    return StringUtil.toString(this.merchant_id);
  }

  public void setMerchantId(String strValue)
  {
    //double dblMerchantId;
    try
    {
      /*dblMerchantId = */Double.parseDouble(strValue);
    }
    catch (NumberFormatException nfe)
    {
      strValue = "";
    }
    this.merchant_id = strValue;
  }

/*BEGIN Release 11 - Added by LF*/
  public String getMillennium_No()
  {
    return StringUtil.toString(this.millennium_no);
  }

  public void setMillennium_No(String strValue)
  {
	  this.millennium_no = strValue;
  }
/*END Release 11 - Added by LF*/
  
  public void setMerchantIds(String[] strValue)
  {
    String strTemp = "";
    if (strValue != null && strValue.length > 0)
    {
      boolean isFirst = true;
      for (int i = 0; i < strValue.length; i++)
      {
        if (strValue[i] != null && !strValue[i].equals(""))
        {
          if (!isFirst)
          {
            strTemp = strTemp + "," + strValue[i];
          } else
          {
            strTemp = strValue[i];
            isFirst = false;
          }
        }
      }
    }

    this.merchant_ids = strTemp;
  }  
  
  public String getInvoiceNo()
  {
    return StringUtil.toString(this.invoiceID);
  }
  public String getTransactionID()
  {
    return StringUtil.toString(this.transactionID);
  }

  public void setTransactionID(String strValue)
  {
	  this.transactionID = strValue;
  }
  public void setInvoiceNo(String strValue)
  {
	  this.invoiceID = strValue;
  }
  
  public String getPINNumber()
  {
    return StringUtil.toString(this.PINNumber);
  }

  public void setPINNumber(String strValue)
  {
	  this.PINNumber = strValue;
  }  
  
  
  	//DBSY-977
	public String getRechargeAmt()
	{
		return this.rechargeAmt;
	}
	
	public void setRechargeAmt(String strValue)
	{
		this.rechargeAmt = strValue;
	}
	
	public String getCriteria()
	{
		return this.criteria;
	}
	
	public void setCriteria(String strValue)
	{
		this.criteria = strValue;
	}
	
	public String getTransactionResult()
	{
		return this.transactionResult;
	}
	
	public void setTransactionResult(String strValue)
	{
		this.transactionResult = strValue;
	}
	
	public String getCol()
	{
		return StringUtil.toString(this.col);
	}

	public void setCol(String strCol)
	{
		this.col = strCol;
	}

	public String getSort()
	{
		return StringUtil.toString(this.sort);
	}

	public void setSort(String strSort)
	{
		this.sort = strSort;
	}
	// END DBSY-977
  
  /*END Release 21 - Adeed by YH*/

    /**
     * Returns all the transaction data for download purposes. Called right after the 
     * PreparedStatement has been executed in {@link #search(int, int, SessionData, int, ServletContext)}. 
     * @param sessionData SessionData reference
     * @param strAccessLevel String with access level
     * @param deploymentType String with deploymentType
     * @param rs ResultSet containing all data fecthed from CarrierSearch (not just page sections)
     * @return Vector<Vector<String>>
     */
    private Vector<Vector<String>> searchAll(SessionData sessionData, String strAccessLevel, String deploymentType, ResultSet rs) {
	Vector<Vector<String>> _data = new Vector<Vector<String>>();
	try {
	    	    
	    cat.debug("Positioning ResultSet to first row .. succeeded? " + rs.first());
	    while (rs.next()) {
		Vector<String> row = new Vector<String>();
		row.add(0, StringUtil.toString(rs.getString("rec_id")));
		row.add(1, StringUtil.toString(rs.getString("millennium_no")));
		row.add(2, StringUtil.toString(rs.getString("dba")));
		row.add(3, StringUtil.toString(rs.getString("merchant_id")));
		row.add(4, DateUtil.formatDateTime(rs.getTimestamp("datetime")));
		row.add(5, StringUtil.toString(rs.getString("phys_city")
			+ ","
			+ StringUtil.toString(rs.getString("phys_county"))));

		if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals("")) {
		    row.add(6, StringUtil.toString(rs.getString("clerk_name")));
		} else {
		    row.add(6, StringUtil.toString(rs.getString("clerk")));
		}
		
		row.add(7, StringUtil.toString(rs.getString("ani")));		
		row.add(8, StringUtil.toString(rs.getString("password")));		
		row.add(9, rs.getString("amount"));
		row.add(10, StringUtil.toString(rs.getString("balance")));
		row.add(11, StringUtil.toString(rs.getString("id")) + "/"
			+ StringUtil.toString(rs.getString("description")));
		row.add(12, StringUtil.toString(rs.getString("control_no")));		
		row.add(13, StringUtil.toString(rs.getString("product_trans_type")));
		if (rs.getString("transaction_type") != null && !rs.getString("transaction_type").equals("")) {
		    row.add(14, StringUtil.toString(rs.getString("transaction_type")));
		} else {
		    row.add(14, "");
		}
		String other_info = StringUtil.toString(rs.getString("other_info"));
		if (other_info.trim() != "") {
		    if (other_info.length() > 6) {
			other_info = other_info.substring((other_info.length() - 6), other_info.length());
		    }
		}
		row.add(15, other_info);
		row.add(16, StringUtil.toString(rs.getString("phys_state")));
		row.add(17, NumberUtil.formatCurrency(rs.getString("bonus_amount")));
		row.add(18, NumberUtil.formatCurrency(rs.getString("total_recharge")));

		if (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
		    row.add(19, StringUtil.toString(rs.getString("pin")));
		} else {
		    row.add(19, "");
		}

		if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
		    boolean ach_datetime_ok = rs.getTimestamp("ach_datetime") != null 
		    	&& !"".equals(rs.getTimestamp("ach_datetime"));
		    String ach_datetime = 
			(ach_datetime_ok) ? DateUtil.formatDateTime(rs.getTimestamp("ach_datetime")) : "TBD"; 
		    row.add(20, ach_datetime);
		} else {
		    row.add(20, "");
		}

		if (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
		    row.add(21, StringUtil.toString(rs.getString("pin")));
		} else {
		    row.add(21, "");
		}

		if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
		    boolean ach_datetime_ok = rs.getTimestamp("ach_datetime") != null 
		    	&& !"".equals(rs.getTimestamp("ach_datetime"));
		    String ach_datetime = 
			(ach_datetime_ok) ? DateUtil.formatDateTime(rs.getTimestamp("ach_datetime")) : "TBD"; 
		    row.add(22, ach_datetime);
		} else {
		    row.add(22, "");
		}

		_data.add(row);		      
	    }
	    cat.debug("saveCarrierSearchData for download completed!");
	} catch (SQLException e) {
	    cat.error("Error browsing CarrierSearchData result set. "
		    + e.getClass().getName() + " (" + e.getMessage() + ")");
	}
	return _data;
    }
    
   
  /**
   * @param intPageNumber
   * @param intRecordsPerPage
   * @param sessionData
   * @param intSectionPage
   * @param context
   * @param includeTax
   * @return
   * @throws TransactionException
   */
   public Vector search(int intPageNumber, int intRecordsPerPage, SessionData sessionData, int intSectionPage, ServletContext context, boolean includeTax) throws TransactionException
{
	  String deploymentType = DebisysConfigListener.getDeploymentType(context);
	  //String customConfigType = DebisysConfigListener.getCustomConfigType(context);
	  Connection dbConn = null;
	  Vector vecTransactions = new Vector();
	  String strSearchSQL = "";
	  boolean showAdditionalData = false;
	  showAdditionalData = sessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA);
	  PreparedStatement pstmt = null;
	  ResultSet rs = null;
	  try
	  {
	    dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    if (dbConn == null)
	    {
	      cat.error("Can't get database connection");
	      throw new TransactionException();
	    }
	    
	    ArrayList<String> skusWithEncryption = null;
	    
	    //get a count of total matches
	    
	    String strRefId = sessionData.getProperty("ref_id");
	    String strAccessLevel = sessionData.getProperty("access_level");
	    String strDistChainType = sessionData.getProperty("dist_chain_type");
	    this.rep_id = this.getRepId();
	    sessionData.setProperty("carr_id", this.rep_id);
	    
	    double dTax = 1;
	    String strSQL="";
	    
	    strSQL = "select distinct wt.rec_id,wt.invoice_no, wt.millennium_no, terminal_types.description as trm_description, wt.dba, wt.phys_city, wt.phys_county, "
						+ 
						"wt.merchant_id, wt.amount, " + "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", wt.datetime, 1) as datetime, wt.control_no, wt.clerk_name, "
						+ "wt.transaction_type, wt.phys_state, wt.other_info, wt.ani, wt.password, (wt.balance) as balance, "
						+ "wt.id, wt.description, wt.product_trans_type, wt.bonus_amount, (wt.bonus_amount + wt.amount) as total_recharge, wt.taxpercentage / 100 as taxpercentage, wt.taxtype, "
						+ "(wt.merchant_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS merchant_commission, "
                                                + " (wt.rep_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS rep_commission, "
                                                + " (wt.agent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS agent_commission, "
                                                + " (wt.subagent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS subagent_commission, "
                                                + " (wt.iso_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS iso_commission ";
		
	    
	    String strAdditionalTablesCarrier="";
	    String strRepsSelect = "";

	    switch (intSectionPage)
	    {
	      //transactions.jsp
	      case 1:
	    	  
	    	if (strAccessLevel.equals(DebisysConstants.CARRIER))
			{
	    		if (showAdditionalData) strSQL += ", wt.additionalData ";
	    		
	    		
	    		if (this.rep_id.length() == 0)
				{
					strRepsSelect = " SELECT m.rep_id FROM merchants m WITH (NOLOCK) "
						+ "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.merchantid "
									+ "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
									+ "INNER JOIN products p WITH (NOLOCK) ON rc.CarrierID = p.carrier_id AND wt.id = p.id WHERE " + "rc.repid = " + strRefId;
				}
				else
				{
					strRepsSelect = " SELECT rep_id FROM reps WITH (nolock) WHERE type=1 and iso_id IN "
							+ "(SELECT rep_id FROM reps WITH (nolock) WHERE type=5 and iso_id IN "
							+ "(SELECT rep_id FROM reps WITH (nolock) WHERE type=4 and iso_id IN "
							+ "(SELECT rep_id FROM reps WITH (nolock) WHERE type IN (2,3) AND rep_id IN (" + this.rep_id + ")))) "
							+ "AND rep_id IN (SELECT DISTINCT rep_id FROM Merchants m WITH (NOLOCK) "
									+ "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID AND m.merchant_id = wt.merchant_id "
									+ "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
									+ "INNER JOIN Products p WITH (NOLOCK) ON rc.CarrierID = p.carrier_id AND wt.id = p.id " + "WHERE rc.repid = " + strRefId
									+ ")";
				}

	    		 strSearchSQL = " wt.rep_id in (" +strRepsSelect + ") ";
	    		 
			}else if (strAccessLevel.equals(DebisysConstants.ISO))
	        {
	        	if (showAdditionalData) strSQL += ", wt.additionalData ";
	          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	          {
	        	strRepsSelect = "select rep_id from reps with (nolock) where iso_id = " + strRefId;
	            strSearchSQL = " wt.rep_id IN ("+strRepsSelect + ")";
	          }
	          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	          {
	        	strRepsSelect = "select rep_id from reps with (nolock) where iso_id in "
									+ "(select rep_id from reps with (nolock) where iso_id in " + "(select rep_id from reps with (nolock) where iso_id = "
									+ strRefId + "))";
	            strSearchSQL = " wt.rep_id IN (" +strRepsSelect+ ") ";

	          }
	          if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))
						{
							strSearchSQL += "AND wt.id IN (";
							strSearchSQL += "SELECT p.id FROM Products p WITH (NOLOCK) INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.carrier_id = rc.CarrierID ";
							strSearchSQL += "WHERE rc.RepID = " + sessionData.getUser().getRefId() + ")";
						}
	        }
	        break;
	        //rep_transactions.jsp
	      case 54:
	    	  
	    	 if (strAccessLevel.equals(DebisysConstants.CARRIER))
			 {
	    		 strRepsSelect= "SELECT DISTINCT r.rep_id FROM reps AS r WITH(nolock) "+
		        	"INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id "+ 
		        	"INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id "+ 
		        	"INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid "+
		        	"INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
		        	"WHERE ta.repid =" + strRefId +
		        	" AND  wt.rep_id="+ this.rep_id;
					//" AND wt.id in " +
	    			//"(SELECT id FROM products WHERE carrier_id IN " +
	    			//"(SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid ="+ strRefId + " )) ";
	    		 
	    		 strSearchSQL = " wt.rep_id in (" +strRepsSelect+")";
	    		 
			 }else if (strAccessLevel.equals(DebisysConstants.ISO))
	         {
	          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	          {
	        	strRepsSelect = "select rep_id from reps with (nolock) where iso_id=" + strRefId ;
	            strSearchSQL = " wt.rep_id = " + this.rep_id + " AND wt.rep_id in (select rep_id from reps with (nolock) where iso_id=" + strRefId + ")";
	          }
	          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	          {
	        	strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
			                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
			                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + "))";
	            strSearchSQL = " wt.rep_id = " + this.rep_id + " AND wt.rep_id in (" +strRepsSelect+") ";
	          }
	        }
	        else if (strAccessLevel.equals(DebisysConstants.AGENT))
	        {
	          strSearchSQL = " wt.rep_id = " + this.rep_id + " AND wt.rep_id in " +
	              " (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
	              "  (select rep_id from reps with (nolock) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id=" + strRefId + ")) ";

	        }
	        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
	        {
	          strSearchSQL = " wt.rep_id = " + this.rep_id + " AND wt.rep_id in " +
	              " (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id=" + strRefId + ")";
	        }
	        else if (strAccessLevel.equals(DebisysConstants.REP))
	        {
	        	strSearchSQL = " wt.rep_id = " + strRefId;
	        }
	        break;
	        //merchants_transactions.jsp
	      //can be accessed via reports or transactions section
	      case 52:
	    	  if (strAccessLevel.equals(DebisysConstants.CARRIER))
				 {
	    		  strRepsSelect= "SELECT DISTINCT merchants.rep_id FROM merchants WITH (NOLOCK) "+ 
					"INNER JOIN reps WITH (NOLOCK) ON merchants.rep_id = reps.rep_id "+ 
					"LEFT JOIN Routes WITH (NOLOCK) ON merchants.RouteID = Routes.RouteID "+  
					"INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid = merchants.merchant_id "+ 
					"INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid "+
					"INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
					"WHERE ta.repid = " + strRefId + 
					" AND wt.merchant_id="+ this.merchant_id;
					//" AND wt.id in " +
	    			//"(SELECT id FROM products WHERE carrier_id IN " +
	    			//"(SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid ="+ strRefId + " )) ";
	    		 
	    		  
		    		 strSearchSQL = " wt.rep_id in (" +strRepsSelect+")";
		    		 
				 }
	      case 14:
	    	 if (strAccessLevel.equals(DebisysConstants.CARRIER))
			 {			       
	    		  strRepsSelect=" ";
			      strSearchSQL = " mc.carrierid in (SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid = "+strRefId+")";
			      //strAdditionalTablesCarrier += " inner join products p WITH (NOLOCK) ON p.id = wt.id";
			      strAdditionalTablesCarrier += " inner join merchantcarriers mc WITH (NOLOCK) ON mc.merchantid=wt.merchant_id and mc.carrierid = p.carrier_id AND wt.merchant_id =" + this.merchant_id;
			 }
	    	 else if (strAccessLevel.equals(DebisysConstants.ISO))
	         {
	          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	          {
	        	  strRepsSelect="select rep_id from reps with (nolock) where iso_id=" + strRefId;
	            strSearchSQL = " wt.merchant_id = " + this.merchant_id + " AND wt.rep_id in (select rep_id from reps with (nolock) where iso_id=" + strRefId + ")";
	          }
	          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	          {
	        	  strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
	                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
	                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + "))";
	            strSearchSQL = " wt.merchant_id = " + this.merchant_id + " AND wt.rep_id in (" +strRepsSelect+ ") ";
	          }
	        }
	        else if (strAccessLevel.equals(DebisysConstants.AGENT))
	        {
	          strSearchSQL = " wt.merchant_id = " + this.merchant_id + " AND wt.rep_id in " +
	              " (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
	              "  (select rep_id from reps with (nolock) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id=" + strRefId + ")) ";
	        }
	        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
	        {
	          strSearchSQL = " wt.merchant_id = " + this.merchant_id + " AND rep_id in " +
	              " (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id=" + strRefId + ")";
	        }
	        else if (strAccessLevel.equals(DebisysConstants.REP))
	        {
	          strSearchSQL = " wt.merchant_id = " + this.merchant_id + " AND wt.rep_id = " + strRefId;
	        }
	        else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
	        {
	          strSearchSQL = " wt.merchant_id = " + strRefId;
	        }
	        
	        break;
	        //agents_transactions.jsp
	      case 50:	    	  
	    	 if (strAccessLevel.equals(DebisysConstants.CARRIER))
		     {
	    		 strRepsSelect="SELECT rep_id FROM reps WITH (nolock) WHERE type=1 AND iso_id IN "+
					"(SELECT rep_id FROM reps WITH (nolock) WHERE type=5 AND iso_id IN "+ 
					"(SELECT rep_id FROM reps WITH (nolock) WHERE type=4 " +
					"AND rep_id=" + this.rep_id +
					" AND rep_id IN "+ 
					"( "+
					"SELECT DISTINCT r2.rep_id FROM reps AS r WITH(nolock) "+ 
					"INNER JOIN dbo.reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id "+ 
					"INNER JOIN reps AS r2 WITH (nolock) ON r1.iso_id = r2.rep_id "+ 
					"INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id "+ 
					"INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id "+
					"INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid "+
					"INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
					"WHERE ta.repid =" + strRefId + 
					")))";
					//" AND wt.id in " +
	    			//"(SELECT id FROM products WHERE carrier_id IN " +
	    			//"(SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid ="+ strRefId + " )) ";
	    		 
		        strSearchSQL = " wt.rep_id in (" +strRepsSelect+")";
		        
		     }else if (strAccessLevel.equals(DebisysConstants.ISO))
		     {
		        strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
		             "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
		              "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and rep_id=" + this.rep_id + " and iso_id = " + strRefId + "))";
		         strSearchSQL = " wt.rep_id IN (" +strRepsSelect+") ";
		     }else if (strAccessLevel.equals(DebisysConstants.AGENT))
		     {
		        strSearchSQL = " wt.rep_id IN " +
		             "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
		             "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = " + strRefId + ")) ";
		     }

	        break;
	        //subagents_transactions.jsp
	      case 51:
	    	  if (strAccessLevel.equals(DebisysConstants.CARRIER))
			     {
	    		  strRepsSelect="select rep_id from reps with (nolock) where type=1 and iso_id in "+
		    			  "(select rep_id from reps with (nolock) where type=5 " +
		    			  "and rep_id=" + this.rep_id + 
		    			  " and rep_id IN "+ 
		    			  "( "+
						  "SELECT DISTINCT R1.rep_id from reps AS r with(nolock) "+ 
						  "INNER JOIN dbo.reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id "+
						  "INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id "+ 
						  "INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id "+ 
						  "INNER JOIN Repcarriers ta WITH (NOLOCK)   on ta.carrierid = mc.carrierid "+
						  "INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
						  "WHERE ta.repid =" + strRefId +
						  "))";
						  //" AND wt.id in " +
						  //"(SELECT id FROM products WHERE carrier_id IN " +
						  //"(SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid ="+ strRefId + " )) ";		  
							
			        strSearchSQL = " wt.rep_id in (" +strRepsSelect+")";
			        
			   }else if (strAccessLevel.equals(DebisysConstants.ISO))
		        {
		        	strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
		              "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and rep_id=" + this.rep_id + " AND iso_id in " +
		              "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + "))";
		          strSearchSQL = " wt.rep_id in (" +strRepsSelect+")";
		        }
		        else if (strAccessLevel.equals(DebisysConstants.AGENT))
		        {
		          strSearchSQL = " wt.rep_id in " +
		              " (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
		              "  (select rep_id from reps with (nolock) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND rep_id=" + this.rep_id + "and iso_id=" + strRefId + "))";
	
		        }
	        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
	        {
	          strSearchSQL = " wt.rep_id in " +
	              " (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id=" + strRefId + ")";

	        }
	        break;
	      case 7:
	        if (strAccessLevel.equals(DebisysConstants.CARRIER))
	        {
	        	strRepsSelect="select rep_id from reps with (nolock) where type in  (2,3)";
	        	strSearchSQL = " wt.rep_id in (" +strRepsSelect+")";
	        }
	        break;
	      default:
	        cat.error("Error during search");
	        throw new TransactionException();
	    }

  	if (this.merchant_ids != null && !this.merchant_ids.equals(""))
		{
			strSearchSQL = strSearchSQL + " AND wt.merchant_id in (" + this.merchant_ids + ") ";
		}
	
		if (this.transactionID != null && !this.transactionID.equals(""))
		{
			strSearchSQL = strSearchSQL + " AND wt.rec_id = " + this.transactionID + " ";
		}
		
		if (this.invoiceID != null && !this.invoiceID.equals(""))
		{
			strSearchSQL = strSearchSQL + " AND wt.invoice_no = " + this.invoiceID + " ";
		}

  		String strCarrierUserProds = sessionData.getProperty("carrierUserProds");

		if (strCarrierUserProds != null && !strCarrierUserProds.equals(""))
		{
			strSearchSQL = strSearchSQL + " AND wt.id in (" + strCarrierUserProds + ") ";
		}
		else 
			return new Vector();
		
////
	    //Added by Jacuna. R25. DBSY-525
  	String strExtraFields = "";
  	String strExtraJoin = "";
	    if((sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) 
				&& strAccessLevel.equals(DebisysConstants.ISO)
				&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		|| this.PINNumber != null && !this.PINNumber.equals("")){
	    	
	    	if(sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) 
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
	    		strExtraFields = ", t.pin ";
			}
	    	strExtraJoin = " inner join transactions t with (nolock) on (wt.rec_id = t.rec_id) ";   	
	    
		    if (this.PINNumber != null && !this.PINNumber.equals("") )
			{
				skusWithEncryption = getSkusWithEncryption(dbConn);
				strSearchSQL = strSearchSQL + " AND wt.product_trans_type = 2 ";
				strSearchSQL = strSearchSQL + " AND wt.amount > 0 ";
				// pin encryption: protect
				String pin = this.PINNumber;
				String sku = getSkuFromPins(pin, dbConn);
				if (skusWithEncryption.contains(sku)) {
					pin = dataProtection().t0(pin);
				}
				strSearchSQL = strSearchSQL + " AND t.pin = '" + pin + "' ";				
			}
		    //strSQL +=" where ";
		  //END. Added by Jacuna. R25. DBSY-525
	    }

	    
	    if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) 
  			&& strAccessLevel.equals(DebisysConstants.ISO)
  			&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
	    	strExtraFields += ", dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", ach_t.Process_Date, 1) as ach_datetime ";
	    	strExtraJoin += " left outer join ach_transaction_Audit ach_t with (nolock) on (wt.rec_id = ach_t.rec_id and wt.merchant_id = ach_t.entity_id)";	    
		}
  	
  		strExtraJoin += " inner join terminals with (nolock) on wt.millennium_no = terminals.millennium_no";
		strExtraJoin += " inner join terminal_types with (nolock) on terminals.terminal_type = terminal_types.terminal_type " + strAdditionalTablesCarrier;
		strSQL += strExtraFields + " from web_transactions wt with (nolock) inner join products p WITH (NOLOCK) ON p.id = wt.id " + strExtraJoin;
					
	    strSQL +=" where ";
	    
	    if ( !this.millennium_no.equals("") )
	    {
	    	strSQL += " wt.millennium_no in (" + this.millennium_no + ") AND ";
	    }
	    Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
	    
	    // Filtering by productIds in the Where
	    if (this.product_ids != null && !this.product_ids.equals(""))
	    {
	    	strSearchSQL = strSearchSQL + " AND wt.RecordedProductid IN (" + this.product_ids + ") ";
	    }
	    
	    String strSQLWhere = strSQLWhere = " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and wt.datetime < '" + vTimeZoneFilterDates.get(1) + "') " + "order by wt.rec_id";
	    
	    pstmt = dbConn.prepareStatement(strSQL + strSearchSQL + strSQLWhere, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    cat.debug(strSQL + strSearchSQL + strSQLWhere);
	    
	    if ( intSectionPage==14)
	    {
	    	sessionData.setProperty("sqlCarrier14", strSQL + strSearchSQL + strSQLWhere);
	    }
	   
	    rs = pstmt.executeQuery();
	    int intRecordCount = DbUtil.getRecordCount(rs);
	    //first row is always the count
	    vecTransactions.add(Integer.valueOf(intRecordCount));
	    if (intRecordCount > 0)
	    {
	      rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
	      
	      Hashtable<Integer, String> ht_taxTypes = TaxTypes.getTaxTypes();
	      
	      for (int i=0;i<intRecordsPerPage;i++)
	      {
	        Vector<String> vecTemp = new Vector<String>();
	        vecTemp.add(0, StringUtil.toString(rs.getString("rec_id")));
	        vecTemp.add(1, StringUtil.toString(rs.getString("millennium_no")));
	        vecTemp.add(2, StringUtil.toString(rs.getString("dba")));
	        vecTemp.add(3, StringUtil.toString(rs.getString("merchant_id")));
	        vecTemp.add(4, DateUtil.formatDateTime(rs.getTimestamp("datetime")));
	        vecTemp.add(5, StringUtil.toString(rs.getString("phys_city")));
	        if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals(""))
	        {
	          vecTemp.add(6, StringUtil.toString(rs.getString("clerk_name")));
	        }
	        else
	        {
	        	vecTemp.add(6, "N/A");
	        }
	        //ani
	        vecTemp.add(7, StringUtil.toString(rs.getString("ani")));
	        //password
	        vecTemp.add(8, StringUtil.toString(rs.getString("password")));
              // LOCALIZATION - SW
              // Just pass the string back to the callind method. Causing trouble with page logic
	        //vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
              vecTemp.add(9, NumberUtil.formatAmount(rs.getString("amount")));
              // END LOCALIZATION
	        //balance
	        vecTemp.add(10, NumberUtil.formatAmount(rs.getString("balance")));
	        //product id/description
	        vecTemp.add(11, StringUtil.toString(rs.getString("id")) + "/" + StringUtil.toString(rs.getString("description")));

	        vecTemp.add(12, StringUtil.toString(rs.getString("control_no")));
	        //trans_type for view pin functionality
	        vecTemp.add(13, StringUtil.toString(rs.getString("product_trans_type")));
	        //retrieve the type description 2, and check if it is null or not
	        if (rs.getString("transaction_type") != null && !rs.getString("transaction_type").equals(""))
	        {
	      	  vecTemp.add(14, StringUtil.toString(rs.getString("transaction_type")));
	        }
	        else
	        {
	      	  vecTemp.add(14, "");
	        }
	        String other_info = StringUtil.toString(rs.getString("other_info"));
	        if(other_info.trim() != "")
	        {
	        	if(other_info.length() > 6)
	        	{
	        		other_info = other_info.substring((other_info.length()-6), other_info.length());
	        	}
	        }
	        vecTemp.add(15, other_info);
	        vecTemp.add(16, StringUtil.toString(rs.getString("phys_state")));
	        vecTemp.add(17, NumberUtil.formatCurrency(rs.getString("bonus_amount")));
	        vecTemp.add(18, NumberUtil.formatCurrency(rs.getString("total_recharge")));
	        
	        if (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO) && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) 
	        {
	        	vecTemp.add(19, StringUtil.toString(rs.getString("pin")));        		        	
	        }
	        else 
	        {
	        	vecTemp.add(19, "");	        	
	        }
	        
	        if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && strAccessLevel.equals(DebisysConstants.ISO) && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) 
	        {
	        	vecTemp.add(20, (rs.getTimestamp("ach_datetime")!=null && !"".equals(rs.getTimestamp("ach_datetime")))? DateUtil.formatDateTime(rs.getTimestamp("ach_datetime")):"TBD" );	        		        	
	        } 
	        else 
	        {
	        	vecTemp.add(20, "");
	        }       

	        vecTemp.add(21, rs.getString("trm_description"));
	        if (showAdditionalData && intSectionPage == 1) { //only for transactions.jsp
	        	vecTemp.add(22, rs.getString("additionalData"));
	        }
	        else 
	        {
	        	vecTemp.add(22, "");
	        }	               
	        vecTemp.add(23, rs.getString("invoice_no"));
	        
	        
	        	vecTemp.add(24, "");
	        	vecTemp.add(25, "");
	        	vecTemp.add(26, "");
	        	vecTemp.add(27, "");
	        
	        vecTemp.add(28, StringUtil.toString(rs.getString("phys_county")) );
	        
	        /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
	        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
	        
	        vecTemp.add(29,  NumberUtil.formatAmount(rs.getString("agent_commission")));
	        vecTemp.add(30,  NumberUtil.formatAmount(rs.getString("subagent_commission")));
	        vecTemp.add(31,  NumberUtil.formatAmount(rs.getString("iso_commission")));
	        vecTemp.add(32,  NumberUtil.formatAmount(rs.getString("merchant_commission")));
	        vecTemp.add(33,  NumberUtil.formatAmount(rs.getString("rep_commission")));
	        }
	        else{
	        	
	        	vecTemp.add(29,  "");
	        	vecTemp.add(30,  "");
	        	vecTemp.add(31,  "");
	        	vecTemp.add(32,  "");
	        	vecTemp.add(33,  "");
	        }
	        /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
        
	        vecTransactions.add(vecTemp);
	        if ( !rs.next() )
	        {
	            break;
	        }

	      } // end for
	    } // end if record count > 0
	    
	  }
	  catch (Exception e)
	  {
	    cat.error("Error during search", e);
	    throw new TransactionException();
	  }
	  finally
	  {
	    try {
	      TorqueHelper.closeConnection(dbConn, pstmt, rs);
	    } catch (Exception e) {
	      cat.error("Error during closeConnection", e);
	    }
	  }
	  return vecTransactions;

}
   
   /**
    * @param intPageNumber
    * @param intRecordsPerPage
    * @param sessionData
    * @param intSectionPage
    * @param context
    * @param includeTax
    * @return
    * @throws TransactionException
    */
    public Vector searchOrig(int intPageNumber, int intRecordsPerPage, SessionData sessionData, int intSectionPage, ServletContext context, boolean includeTax) throws TransactionException
 {
 	  Connection dbConn = null;
 	  Vector vecTransactions = new Vector();
 	  String strSearchSQL = "";
 	  PreparedStatement pstmt = null;
 	  ResultSet rs = null;
 	  try
 	  {
 	    dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
 	    if (dbConn == null)
 	    {
 	      cat.error("Can't get database connection");
 	      throw new TransactionException();
 	    }
 	    
 	    ArrayList<String> skusWithEncryption = null;
 	    
 	    //get a count of total matches
	    
 	    String strRefId = sessionData.getProperty("ref_id");
 	    String strAccessLevel = sessionData.getProperty("access_level");
 	    String strDistChainType = sessionData.getProperty("dist_chain_type");
 	    String strSQL="";
		strSQL = "select distinct wt.rec_id,wt.invoice_no, wt.millennium_no, terminal_types.description as trm_description, wt.dba, "
					+ 
					"wt.merchant_id, wt.amount, " + "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", wt.datetime, 1) as datetime, "
					+ "wt.id, wt.description, wt.bonus_amount, (wt.bonus_amount + wt.amount) as total_recharge, "
					/* DBSY-940 Transaction Origination Report */
					+ "wt.org_IP_Address as org_ip_address, "
					+ "wt.org_Phone_Number as org_phone_number ";
					/* END DBSY-940 Transaction Origination Report */			
 	    
 	    String strRepsSelect = "";
 	    
        if (strAccessLevel.equals(DebisysConstants.ISO))
        {
          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
          {
        	strRepsSelect = "select rep_id from reps with (nolock) where iso_id = " + strRefId;
            strSearchSQL = " wt.rep_id IN ("+strRepsSelect + ")";
          }
          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
          {
        	strRepsSelect = "select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
			                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
			                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))";
            strSearchSQL = " wt.rep_id IN (" +strRepsSelect+ ") ";

          }
        }

   	if (this.merchant_ids != null && !this.merchant_ids.equals(""))
 		{
 			strSearchSQL = strSearchSQL + " AND wt.merchant_id in (" + this.merchant_ids + ") ";
 		}
 	
 		if (this.transactionID != null && !this.transactionID.equals(""))
 		{
 			strSearchSQL = strSearchSQL + " AND wt.rec_id = " + this.transactionID + " ";
 		}
 		
 		if (this.invoiceID != null && !this.invoiceID.equals(""))
 		{
 			strSearchSQL = strSearchSQL + " AND wt.invoice_no = " + this.invoiceID + " ";
 		}
 	  	String strExtraFields = "";
 	  	String strExtraJoin = "";
   		strExtraJoin += " inner join terminals with (nolock) on wt.millennium_no = terminals.millennium_no";
 			strExtraJoin += " inner join terminal_types with (nolock) on terminals.terminal_type = terminal_types.terminal_type";
 			strSQL += strExtraFields + " from web_transactions wt with (nolock) " + strExtraJoin;
 	    strSQL +=" where ";
 	    
 	    Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
 	    
 	    String strSQLWhere = "AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and wt.datetime < '" + vTimeZoneFilterDates.get(1) + "') " + "order by wt.rec_id";

 	    pstmt = dbConn.prepareStatement(strSQL + strSearchSQL + strSQLWhere, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
 	    cat.debug(strSQL + strSearchSQL + strSQLWhere);
 	    rs = pstmt.executeQuery();
 	    int intRecordCount = DbUtil.getRecordCount(rs);
 	    //first row is always the count
 	    vecTransactions.add(Integer.valueOf(intRecordCount));
 	    if (intRecordCount > 0)
 	    {
 	      rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
 	      
 	      Hashtable<Integer, String> ht_taxTypes = TaxTypes.getTaxTypes();
 	      
 	      for (int i=0;i<intRecordsPerPage;i++)
 	      {
			Vector<String> vecTemp = new Vector<String>();
 	        vecTemp.add(0, StringUtil.toString(rs.getString("rec_id")));
 	        vecTemp.add(1, StringUtil.toString(rs.getString("millennium_no")));
 	        vecTemp.add(2, StringUtil.toString(rs.getString("dba")));
 	        vecTemp.add(3, StringUtil.toString(rs.getString("merchant_id")));
 	        vecTemp.add(4, DateUtil.formatDateTime(rs.getTimestamp("datetime")));
            // LOCALIZATION - SW
            // Just pass the string back to the callind method. Causing trouble with page logic
            vecTemp.add(5, NumberUtil.formatAmount(rs.getString("amount")));
            // END LOCALIZATION
 	        //product id/description
 	        vecTemp.add(6, StringUtil.toString(rs.getString("id")) + "/" + StringUtil.toString(rs.getString("description")));
			vecTemp.add(7, StringUtil.toString(rs.getString("id")));
			vecTemp.add(8, StringUtil.toString(rs.getString("description")));

 	        vecTemp.add(9, NumberUtil.formatCurrency(rs.getString("total_recharge")));
 	        
 	        vecTemp.add(10, rs.getString("trm_description"));
 	        vecTemp.add(11, rs.getString("invoice_no"));
	        
	        if (rs.getString("org_ip_address")!= null && rs.getString("org_ip_address")!= "") {
	        	vecTemp.add(12,rs.getString("org_ip_address"));
	        } else {
	        	vecTemp.add(12,"N/A");
	        }
	        if (rs.getString("org_phone_number")!= null && rs.getString("org_phone_number")!= "") {
	        	vecTemp.add(13,rs.getString("org_phone_number"));
	        } else {
	        	vecTemp.add(13,"N/A");
	        }
 	        vecTransactions.add(vecTemp);
 	        if ( !rs.next() )
 	        {
 	            break;
 	        }

 	      } // end for
 	    } // end if record count > 0
 	    
 	  }
 	  catch (Exception e)
 	  {
 	    cat.error("Error during search", e);
 	    throw new TransactionException();
 	  }
 	  finally
 	  {
 	    try {
 	      TorqueHelper.closeConnection(dbConn, pstmt, rs);
 	    } catch (Exception e) {
 	      cat.error("Error during closeConnection", e);
 	    }
 	  }
 	  return vecTransactions;
 }
   
   
   	/***
   	 * DBSY-890/977 SW
   	 * Utility to get host transaction types and their descriptions in hashtable
   	 * @param dbConn
   	 * @return
   	 */
   	public Hashtable<String, String> getHostTransactionTypes(Connection dbConn)
   	{
	    Hashtable<String, String> tranTypeTable = new Hashtable<String, String>();
	    PreparedStatement pstmt2 = null;
        ResultSet rs2 = null;
	    try
	    {
	   		String strSQLgetTypes = CarrierSearch.sql_bundle.getString("getTypes");
	        
	        CarrierSearch.cat.debug(strSQLgetTypes);
	        try {
				pstmt2 = dbConn.prepareStatement(strSQLgetTypes);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        rs2 = pstmt2.executeQuery();
	        
	        while (rs2.next())
	        {
	        	tranTypeTable.put(StringUtil.toString(rs2.getString("hostTrans_id")), StringUtil.toString(rs2.getString("description")));
	        }
	    } catch (SQLException e) {
			e.printStackTrace();
		}  
    	catch (Exception e)
    	{
    		cat.error("Error during getHostTransactionTypes", e);
    	}
    	finally
    	{
    		try {
    			// dont close dbConn because it is still being used by the caller
    	        rs2.close();
    	        pstmt2.close();
    		} catch (Exception e) {
    			cat.error("Error during closeConnection", e);
    		}
    	}
        
        return tranTypeTable;
   	}

   	/***
   	 * DBSY-890/977 SW
   	 * Utility to get host transaction result codes and their descriptions in hashtable
   	 * @param dbConn
   	 * @return
   	 */
   	public Hashtable<String, String> getHostTransactionResultCodes(Connection dbConn)
   	{
	    Hashtable<String, String> tranResultCodeTable = new Hashtable<String, String>();
	    PreparedStatement pstmt2 = null;
        ResultSet rs2 = null;
	    try
	    {
		    String strSQLgetResult = CarrierSearch.sql_bundle.getString("getResults");
	
	        CarrierSearch.cat.debug(strSQLgetResult);
	        pstmt2 = dbConn.prepareStatement(strSQLgetResult);
	        rs2 = pstmt2.executeQuery();
	        while (rs2.next())
	        {
	        	tranResultCodeTable.put(StringUtil.toString(rs2.getString("result_id")), StringUtil.toString(rs2.getString("description")));
	        }
	    } catch (SQLException e) {
			e.printStackTrace();
		} 
    	catch (Exception e)
    	{
    		cat.error("Error during getHostTransactionResultCodes", e);
    	}
    	finally
    	{
    		try {
    			// dont close dbConn because it is still being used by the caller
    	        rs2.close();
    	        pstmt2.close();
    		} catch (Exception e) {
    			cat.error("Error during closeConnection", e);
    		}
    	}
        return tranResultCodeTable;
   	}
   	
   	/***
   	 * DBSY-977 Get the count of the total amount of records for the search query. Do this to help out paging.
   	 * @param intPageNumber
   	 * @param intRecordsPerPage
   	 * @param sessionData
   	 * @param context
   	 * @return
   	 * @throws TransactionException
   	 */
   	public int carrierUserSearchGetCount(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context) throws TransactionException
    {	
   		CarrierSearch.cat.debug("Begin carrierUserSearchGetCount");
    	int resultCount = 0;
	   
    	Connection dbConn = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	
        String strRefId = sessionData.getProperty("ref_id");
        String strAccessLevel = sessionData.getProperty("access_level");
        String strCarrierUserProds = sessionData.getProperty("carrierUserProds");
	    String strDistChainType = sessionData.getProperty("dist_chain_type");
        
  	  	String strSiteIDsSelect = "";
  	  	
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
		    if (dbConn == null)
		    {
		    	cat.error("Can't get database connection");
		    	throw new TransactionException();
		    }
		    
		    
		    String strSQL = CarrierSearch.sql_bundle.getString("getCarrierUserTransactionsCount");
	        strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
	        strSQL = strSQL.replaceAll("_REFID_", strRefId);
	        
	        // Make sure we dont do something like this AND ht.product_id IN ()
	        // Return 0 if there aren't any products set. Carrier users need products
	        if(strCarrierUserProds != null && !strCarrierUserProds.equals(""))
	        	strSQL = strSQL + " AND ht.productid in (" + strCarrierUserProds + ") ";
	        else 
	        	return 0;

	        try { if(!this.transactionID.equals(""))Integer.parseInt(this.transactionID); } catch(Exception e) { return 0; }
		    if (this.transactionID != null && !this.transactionID.equals(""))
		    	strSQL = strSQL + " AND ht.transaction_id = " + this.transactionID + " ";

	        try { if(!this.invoiceID.equals(""))Integer.parseInt(this.invoiceID); } catch(Exception e) { return 0; }
		    if (this.invoiceID != null && !this.invoiceID.equals(""))
		    	strSQL = strSQL + " AND ht.invoice_no = " + this.invoiceID + " ";

	        try { if(!this.rechargeAmt.equals(""))Integer.parseInt(this.rechargeAmt); } catch(Exception e) { return 0; }
		    if(this.rechargeAmt != null && !this.rechargeAmt.equals(""))
		    {
		    	// Try to put the amount in a decimal form?
		    	strSQL = strSQL + " AND ht.amount = " + NumberUtil.formatAmount(this.rechargeAmt) + " ";
		    }

	        try { if(!this.PINNumber.equals(""))Integer.parseInt(this.PINNumber); } catch(Exception e) { return 0; }
		    if(this.PINNumber != null && !this.PINNumber.equals(""))
		    	strSQL = strSQL + " AND ht.info2 = '" + this.PINNumber + "' ";
		    
		    // Search part or all of a dba, site ID, merchant ID, or merchant contact phone number
		    if (this.criteria != null && !this.criteria.equals(""))
		    {
		    	strSQL = strSQL + " AND (m.merchant_id like '%" + this.criteria.replaceAll("'", "''") + "%' OR m.contact_phone like '%" + this.criteria.replaceAll("'", "''") + "%' OR m.dba like '%" + this.criteria.replaceAll("'", "''") + "%' OR t.millennium_no like '%" + this.criteria.replaceAll("'", "''") + "%')";
		    }
		    
		    // If anything but ALL selected, only show those results
		    if(!this.transactionResult.equals("ALL"))
		    {
		    	if(this.transactionResult.equals("SUCCESS"))
		    		strSQL = strSQL + " AND ht.result_code = 0 ";
		    	else if(this.transactionResult.equals("FAIL"))
		    		strSQL = strSQL + " AND ht.result_code <> 0 ";
		    }
		 
		    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		    {
		    	strSiteIDsSelect = "select millennium_no from terminals with (nolock) where merchant_id in " +
		    		"(select merchant_id from merchants with (nolock) where rep_id in " +
		    		"(select rep_id from reps with (nolock) where iso_id = " + strRefId + "))";
		    	strSQL = strSQL + " AND ht.millennium_no IN (" + strSiteIDsSelect + ") ";
		    }
		    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		    {
		    	strSiteIDsSelect = "select millennium_no from terminals with (nolock) where merchant_id in " +
		    		"(select merchant_id from merchants with (nolock) where rep_id in " +
		    		"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
		    		"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
		    		"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))))";
		    	strSQL = strSQL + " AND ht.millennium_no IN (" + strSiteIDsSelect + ") ";
		    }
		    
		    CarrierSearch.cat.debug("carrierUserSearchGetCount SQL query: " + strSQL);
		    pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
		    pstmt.setString(1, vTimeZoneFilterDates.get(0).toString());
		    cat.debug("1: " + vTimeZoneFilterDates.get(0).toString());
		    pstmt.setString(2, vTimeZoneFilterDates.get(1).toString());
		    cat.debug("2: " + vTimeZoneFilterDates.get(1).toString());
		    
		    rs = pstmt.executeQuery();

	        if(rs.next())
	        {
	        	resultCount = rs.getInt(1);
	        }
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during search", e);
    		throw new TransactionException();
    	}
    	finally
    	{
    		try {
    			TorqueHelper.closeConnection(dbConn, pstmt, rs);
    		} catch (Exception e) {
    			cat.error("Error during closeConnection", e);
    		}
    	}
   		CarrierSearch.cat.debug("End carrierUserSearchGetCount");
    	return resultCount;
    }
   	
   	
   	/**
   	 * DBSY-890/DBSY-977 SW
   	 * Modified transactions search for carrier level users
   	 * Gets a set of all transactions from hostTransactions.
   	 * Also gets a supplemental amount of info from web_transactions for all of the successful ones. Basically all of the successful
   	 * transactions in the Vector have an additional amount of columns
   	 * @param intPageNumber
   	 * @param intRecordsPerPage actual # of records per page you want, or -1 if downloading transactions
   	 * @param sessionData
   	 * @param context
   	 * @return Transaction results containing failed/successful transactions. Successful transactions have more columns than failed ones.
   	 * @throws TransactionException
   	 */
    public Vector<Vector<String>> carrierUserSearch(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context) throws TransactionException
    {	
   		CarrierSearch.cat.debug("Begin carrierUserSearch");
    	Vector<Vector<String>> vecResults = new Vector<Vector<String>>();
	   
    	Connection dbConn = null;
    	PreparedStatement pstmt = null;
    	ResultSet rs = null;
    	
        String strRefId = sessionData.getProperty("ref_id");
        String strAccessLevel = sessionData.getProperty("access_level");
        String strCarrierUserProds = sessionData.getProperty("carrierUserProds");
	    String strDistChainType = sessionData.getProperty("dist_chain_type");
        
  	  	String strSiteIDsSelect = "";
  	  	
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
		    if (dbConn == null)
		    {
		    	cat.error("Can't get database connection");
		    	throw new TransactionException();
		    }
		    
		    
		    String strSQLPre = CarrierSearch.sql_bundle.getString("getCarrierUserTransactionsPre");
		    String strSQLPost = CarrierSearch.sql_bundle.getString("getCarrierUserTransactionsPost");
		    String strSQL = CarrierSearch.sql_bundle.getString("getCarrierUserTransactions");
		    String strSQLWebTrans = CarrierSearch.sql_bundle.getString("getCarrierUserWebTransactions");
	        strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
	        strSQL = strSQL.replaceAll("_REFID_", strRefId);
	        
	        // Make sure we dont do something like this AND ht.product_id IN ()
	        // Carrier users need to have products set in order to view this report.
	        if(strCarrierUserProds != null && !strCarrierUserProds.equals(""))
	        	strSQL = strSQL + " AND ht.productid in (" + strCarrierUserProds + ") ";
	        else 
	        	return vecResults;
	        
	        try { if(!this.transactionID.equals(""))Integer.parseInt(this.transactionID); } catch(Exception e) { return vecResults; }
		    if (this.transactionID != null && !this.transactionID.equals(""))
		    	strSQL = strSQL + " AND ht.transaction_id = " + this.transactionID + " ";

	        try { if(!this.invoiceID.equals(""))Integer.parseInt(this.invoiceID); } catch(Exception e) { return vecResults; }
		    if (this.invoiceID != null && !this.invoiceID.equals(""))
		    	strSQL = strSQL + " AND ht.invoice_no = " + this.invoiceID + " ";

	        try { if(!this.rechargeAmt.equals("")) Integer.parseInt(this.rechargeAmt); } catch(Exception e) { return vecResults; }
		    if(this.rechargeAmt != null && !this.rechargeAmt.equals(""))
		    {
		    	// Try to put the amount in a decimal form?
		    	strSQL = strSQL + " AND ht.amount = " + NumberUtil.formatAmount(this.rechargeAmt) + " ";
		    }

		    // PIN is 16 digits. int only supports up to the value 2000000
	        try { if(!this.PINNumber.equals(""))Long.parseLong(this.PINNumber); } catch(Exception e) { return vecResults; }
		    if(this.PINNumber != null && !this.PINNumber.equals(""))
		    	strSQL = strSQL + " AND ht.info2 = '" + this.PINNumber + "' ";
		    
		    // Search part or all of a dba, site ID, merchant ID, or merchant contact phone number
		    if (this.criteria != null && !this.criteria.equals(""))
		    {
		    	strSQL = strSQL + " AND (m.merchant_id like '%" + this.criteria.replaceAll("'", "''") + "%' OR m.contact_phone like '%" + this.criteria.replaceAll("'", "''") + "%' OR m.dba like '%" + this.criteria.replaceAll("'", "''") + "%' OR t.millennium_no like '%" + this.criteria.replaceAll("'", "''") + "%')";
		    }
		    
		    // If anything but ALL selected, only show those results
		    if(!this.transactionResult.equals("ALL"))
		    {
		    	if(this.transactionResult.equals("SUCCESS"))
		    		strSQL = strSQL + " AND ht.result_code = 0 ";
		    	else if(this.transactionResult.equals("FAIL"))
		    		strSQL = strSQL + " AND ht.result_code <> 0 ";
		    }
		 
		    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		    {
		    	strSiteIDsSelect = "select millennium_no from terminals with (nolock) where merchant_id in " +
		    		"(select merchant_id from merchants with (nolock) where rep_id in " +
		    		"(select rep_id from reps with (nolock) where iso_id = " + strRefId + "))";
		    	strSQL = strSQL + " AND ht.millennium_no IN (" + strSiteIDsSelect + ") ";
		    }
		    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		    {
		    	strSiteIDsSelect = "select millennium_no from terminals with (nolock) where merchant_id in " +
		    		"(select merchant_id from merchants with (nolock) where rep_id in " +
		    		"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
		    		"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
		    		"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))))";
		    	strSQL = strSQL + " AND ht.millennium_no IN (" + strSiteIDsSelect + ") ";
		    }
		    
		    // Column sorting here
		    int intCol = 0;
		    int intSort = 0;
		    String strCol = "";
		    String strSort = "";

		    try
		    {
		    	if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
		    	{
		    		intCol = Integer.parseInt(this.col);
		    		intSort = Integer.parseInt(this.sort);
		    	}
		    }
		    catch (NumberFormatException nfe)
		    {
		    	intCol = 0;
		    	intSort = 0;
		    }

		    if (intSort == 1)
		    	strSort = "DESC";
		    else
		    	strSort = "ASC";

		    switch(intCol)
		    {
			    case 1:
			    	strCol = "DateTime";
			    	break;
			    case 2:
			    	strCol = "TransactionID";
			    	break;
			    case 3:
			    	strCol = "Amount";
			    	break;
			    case 4:
			    	strCol = "DBA";
			    	break;
			    case 6:
			    	strCol = "ProductID";
			    	break;
			    case 7:
			    	strCol = "Phone";
			    	break;
			    case 8:
			    	strCol = "SiteID";
			    	break;
			    case 9:
			    	strCol = "TransactionType";
			    	break;
			    case 10:
			    	strCol = "TerminalType";
			    	break;
			    case 11:
			    	strCol = "Host";
			    	break;
			    case 12:
			    	strCol = "Duration";
			    	break;
			    case 13:
			    	strCol = "ResultCode";
			    	break;
			    case 14:
			    	strCol = "ProviderResponse";
			    	break;
			    case 15:
			    	strCol = "InvoiceNo";
			    	break;
			    default:
			    	strCol = "DateTime";
		    		break;
		    }
		    strSQLPre = strSQLPre.replace("_ORDERBY_", "ORDER BY " + strCol + " " + strSort);
		    // End column sorting  
		    
		    strSQL = strSQLPre + strSQL + strSQLPost;
		    
		    // For paging if not a download - if it is a download, dont forget to remove the placeholder
		    if(intRecordsPerPage != -1)
		    	strSQL = strSQL.replace("_ROWNUMBER_", "where RowNumber between ? and ?");
		    else
		    	strSQL = strSQL.replace("_ROWNUMBER_", "");
		    
		    CarrierSearch.cat.debug("carrierUserSearch SQL query: " + strSQL);
		    pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		    Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
		    pstmt.setString(1, vTimeZoneFilterDates.get(0).toString());
		    cat.debug("1: " + vTimeZoneFilterDates.get(0).toString());
		    pstmt.setString(2, vTimeZoneFilterDates.get(1).toString());
		    cat.debug("2: " + vTimeZoneFilterDates.get(1).toString());
		    
		    if(intRecordsPerPage != -1)
		    {
			    int firstRecIndex = 1 + (intPageNumber - 1)*intRecordsPerPage;
			    int lastRecordIndex = intPageNumber * intRecordsPerPage;
			    pstmt.setString(3, String.valueOf(firstRecIndex));
			    cat.debug("3: " + String.valueOf(firstRecIndex));
			    pstmt.setString(4, String.valueOf(lastRecordIndex));
			    cat.debug("4: " + String.valueOf(lastRecordIndex));
		    }
		    
		    rs = pstmt.executeQuery();

		    Hashtable<String, String> tranTypeTable = this.getHostTransactionTypes(dbConn);
		    Hashtable<String, String> tranResultCodeTable = this.getHostTransactionResultCodes(dbConn);
		    Hashtable<String, String> termTypeTable = Terminal.getTerminalTypes();

	        while(rs.next())
	        {
	        	// This is all to get the clerk name - messy. If possible, do this another way
	        	String clerkId = "";
	            String amount = "";
	            int counter = 1;

	            String receiveString = StringUtil.toString(rs.getString("ReceiveString"));
	            int begin = 0;
	            int end = 0;
	            boolean search = true;
	            boolean isPinDistSale = (receiveString.indexOf("<PinDistSale>") != -1);
	            end = receiveString.indexOf(",");
	            if (end < 0)
	            {
	            	end = receiveString.length();
	            }
	            if (isPinDistSale)
	            {
	            	while (search && counter <= 5)
	            	{
	            		String temp = receiveString.substring(begin, end);
	            		if (receiveString.indexOf(",") > -1)
	            		{
	            			receiveString = receiveString.substring(end + 1);
	            			end = receiveString.indexOf(",");
	            			if (end < 0)
	            			{
	            				end = receiveString.length();
	            			}
	            		}
	            		else
	            		{
	            			search = false;
	            		}
	            		// assume the 2nd data item stores clerk id - exit after you find it
	            		if (counter == 2)
	            		{
	            			String item = temp.substring(temp.indexOf("=") + 1, temp.length());
	            			clerkId = item;
	            			search = false;
	            		}
	            		// no longer assume anything about amount. It is returned in the resultSet.
	            		++counter;
	            	}
	            }
	            else
	            {
	            	// we are not interested in the data items after 6
	            	while (search && counter <= 6)
	            	{
	            		String temp = receiveString.substring(begin, end);
	            		if (receiveString.indexOf(",") > -1)
	            		{
	            			receiveString = receiveString.substring(end + 1);
	            			end = receiveString.indexOf(",");
	            			if (end < 0)
	            			{
	            				end = receiveString.length();
	            			}
	            		}
	            		else
	            		{
	            			search = false;
	            		}
	            		// assume the 3rd data item stores clerk id - exit after you find it
	            		if (counter == 3)
	            		{
	            			clerkId = temp;
	            			search = false;
	            		}
	            		// no longer assume anything about amount. It is returned in the resultSet.
	            		++counter;
	            	}
	            }
	            amount = rs.getString("Amount");
	            String mil_no = rs.getString("SiteID");

	            String sqlClerkName = "select t.Name from terminal_passwords t with (nolock) where t.password = '" + clerkId + "' " + "and t.millennium_no = '" + mil_no
	                + "'";

	            PreparedStatement pstmtClerk = null;
	            pstmtClerk = dbConn.prepareStatement(sqlClerkName);
	            ResultSet rsClerk = pstmtClerk.executeQuery();
	            String clerkName = "";
	            if (rsClerk.next())
	            {
	            	clerkName = rsClerk.getString("Name");
	            }
	            rsClerk.close();
	            pstmtClerk.close();
	            // Finished getting clerk name
	            
	        	Vector<String> vecTemp = new Vector<String>();
	        	vecTemp.add(0, DateUtil.formatDateTime(rs.getTimestamp("DateTime")));
	        	vecTemp.add(1, StringUtil.toString(rs.getString("TransactionID")));
	        	vecTemp.add(2, NumberUtil.formatCurrency(rs.getString("Amount")));
	        	vecTemp.add(3, StringUtil.toString(rs.getString("DBA")));
	        	vecTemp.add(4, clerkName);
	        	String sku = StringUtil.toString(rs.getString("ProductID"));
	        	vecTemp.add(5, sku);
	        	
	        	if(!StringUtil.toString(rs.getString("TransactionType")).equals("21") 
	        			&& !StringUtil.toString(rs.getString("TransactionType")).equals("22")
	        			&& !StringUtil.toString(rs.getString("TransactionType")).equals("23"))
	        		vecTemp.add(6, StringUtil.toString(rs.getString("Phone")));
	        	else
	        		vecTemp.add(6, "");
	        	
	        	vecTemp.add(7, StringUtil.toString(rs.getString("SiteID")));
	        	String typeDescription = tranTypeTable.get(StringUtil.toString(rs.getString("TransactionType")));
	        	if (typeDescription != null && !typeDescription.equals(""))
	        		vecTemp.add(8, StringUtil.toString(rs.getString("TransactionType")) + "&#47; " + typeDescription.trim());
	        	else
	        		vecTemp.add(8, StringUtil.toString(rs.getString("TransactionType")));
	        	
	        	String termTypeDescription = termTypeTable.get(StringUtil.toString(rs.getString("TerminalType"))); 
	        	if (termTypeDescription != null && !termTypeDescription.equals(""))
	        		vecTemp.add(9, termTypeDescription);
	        	else
	        		vecTemp.add(9, StringUtil.toString(rs.getString("TerminalType")));

	        	vecTemp.add(10, StringUtil.toString(rs.getString("Host")));
	        	vecTemp.add(11, StringUtil.toString(rs.getString("Duration")));
	        	vecTemp.add(12, StringUtil.toString(rs.getString("ResultCode")));
	        	String resultCode = StringUtil.toString(rs.getString("ResultCode"));
	        	String resultDescription = tranResultCodeTable.get(resultCode);
	        	if (resultDescription != null && !resultDescription.equals(""))
	        		vecTemp.add(13, resultCode + "&#47; " + resultDescription.trim());
	        	else
	        		vecTemp.add(13, resultCode);

	        	vecTemp.add(14, StringUtil.toString(rs.getString("ProviderResponse")));
	        	vecTemp.add(15, StringUtil.toString(String.valueOf(rs.getInt("InvoiceNo"))));

	        	// Add additional successful information
	        	if(resultCode.equals("0"))
	        	{
	        		PreparedStatement pstmtSuccess = null;
	        		pstmtSuccess = dbConn.prepareStatement(strSQLWebTrans);
	        		pstmtSuccess.setString(1, StringUtil.toString(rs.getString("TransactionID")));
		            ResultSet rsSuccess = pstmtSuccess.executeQuery();
		            
		            if (rsSuccess.next())
		            {
		        		vecTemp.add(16, StringUtil.toString(rsSuccess.getString("MerchantID")));
						vecTemp.add(17, StringUtil.toString(rsSuccess.getString("City")));
						vecTemp.add(18, StringUtil.toString(rsSuccess.getString("County")));
						vecTemp.add(19, StringUtil.toString(rsSuccess.getString("Clerk")));
						vecTemp.add(20, NumberUtil.formatCurrency(StringUtil.toString(rsSuccess.getString("BonusAmount"))));
						vecTemp.add(21, NumberUtil.formatCurrency(StringUtil.toString(rsSuccess.getString("Balance"))));
						vecTemp.add(22, StringUtil.toString(rsSuccess.getString("ControlNo")));
						vecTemp.add(23, StringUtil.toString(rsSuccess.getString("TransactionType")));
						vecTemp.add(24, "");
						vecTemp.add(25, StringUtil.toString(rsSuccess.getString("RefNo")));
						vecTemp.add(26, NumberUtil.formatCurrency(StringUtil.toString(rsSuccess.getString("TotalRecharge"))));
		            }
		            rsSuccess.close();
		            pstmtSuccess.close();
		            rsSuccess = null;
		            pstmtSuccess = null;
	        	}
	        	vecResults.add(vecTemp);
	        } 
	        cat.debug("VecResult size: " + vecResults.size());
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during search", e);
    		throw new TransactionException();
    	}
    	finally
    	{
    		try {
    			TorqueHelper.closeConnection(dbConn, pstmt, rs);
    		} catch (Exception e) {
    			cat.error("Error during closeConnection", e);
    		}
    	}
    	cat.debug("END carrierUserSearch");
    	
    	return vecResults;
    }
    
    /***
     * DBSY-890/DBSY-977
     * @param sessionData usually pass in SessionData from JSP
     * @param context usually pass in application from JSP
     * @return Download URL
     * @throws TransactionException
     */
    public String downloadCarrierUserTrans(SessionData sessionData, ServletContext context) throws TransactionException
    {
    	// Dont quite know what this is used for yet. maybe download bar?
    	this.isdone = false;
        long start = System.currentTimeMillis();
    	String strFileName = "";

        String strAccessLevel = sessionData.getProperty("access_level");
        String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
        String downloadPath = DebisysConfigListener.getDownloadPath(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);

    	boolean customConfigTypeIsMexico = DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
    	boolean customConfigTypeIsDefault = DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

    	boolean deployTypeIsIntl = DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
    	boolean deployTypeIsDomestic = DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
    	
    	boolean invoiceNumberEnabled = com.debisys.users.User.isInvoiceNumberEnabled(sessionData, context);

    	boolean hasPermViewPin = sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN);
    	
    	boolean accessLevelIsISO = strAccessLevel.equals(DebisysConstants.ISO);
    	
    	boolean hadSuccessfulTrans = false;
    	
        FileChannel fc = null;
        
        try
        {
        	strFileName = this.generateFileName(context);    
        	fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel();

        	// Get all the results
          	long startTime = System.currentTimeMillis();
          	
          	// Search part or all of a dba, site ID, merchant ID, or merchant contact phone number
          	Vector<Vector<String>> vecResults = this.carrierUserSearch(1, -1, sessionData, context);
          	long endTime = System.currentTimeMillis();
          	cat.debug("Time to get results: " + (((endTime - startTime) / 1000.0)) + " seconds");
          	// reset start for fileout
          	startTime = endTime;
          	cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
            
          	// Create all of the headers for successful transactions. Speed things up a bit	
    		StringBuffer successHeaders = new StringBuffer();
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.merchant_id",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.city",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.county",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.clerk",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.ref_no",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.bonus",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.total_recharge",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.balance",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.control_no",sessionData.getLanguage()) + "\",");
    		successHeaders.append("\"" + Languages.getString("jsp.admin.reports.transaction_type",sessionData.getLanguage()) + "\",");
    		
    		if(hasPermViewPin && accessLevelIsISO && deployTypeIsDomestic) 
    			successHeaders.append("\"" + Languages.getString("jsp.admin.reports.pin_number",sessionData.getLanguage()) + "\",");	

          	// Main Headers for all transaction results
    		StringBuffer generalHeaders = new StringBuffer();
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.date",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.tran_no",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.recharge",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.dba",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.clerk",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.product",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.phone",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.term_no",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.transaction_type",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_type",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.host",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.duration",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.result",sessionData.getLanguage()) + "\",");
    		generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.provider_response",sessionData.getLanguage()) + "\",");

			if(invoiceNumberEnabled && deployTypeIsIntl && customConfigTypeIsDefault)
				generalHeaders.append("\"" + Languages.getString("jsp.admin.reports.invoiceno",sessionData.getLanguage()) + "\",");

			this.writetofile(fc, generalHeaders.toString());
			
			// Don't show the other headers if a customer is searching for only failed transasctions
			// Otherwise just show it in case there are successful ones
			if(!this.transactionResult.equals("FAIL"))
				this.writetofile(fc, successHeaders.toString());
			
      	  	this.writetofile(fc,"\r\n");
      	  	
            // I think this is a row count of the excel doc
            this.icount=1;
            Iterator<Vector<String>> it = vecResults.iterator();
            
            while (it.hasNext())
            {
            	Vector<String> vecTemp = null;
            	vecTemp = it.next();
            	
            	// where is the max set?
            	if( (this.max < this.icount) && this.max!=-1)
            		break;

          	  	// Datetime
            	this.writetofile(fc,"\"" + vecTemp.get(0) + "\",");
    			// Transaction ID
    			this.writetofile(fc,"\"" + vecTemp.get(1) + "\",");
    			// Recharge value
    			this.writetofile(fc,"\"" + vecTemp.get(2) + "\",");
    			// DBA
    			this.writetofile(fc,"\"" + vecTemp.get(3) + "\",");
    			// Clerk 
    			this.writetofile(fc,"\"" + vecTemp.get(4) + "\",");
    			// Product ID
    			this.writetofile(fc,"\"" + vecTemp.get(5) + "\",");
    			// Phone/Reference #
    			this.writetofile(fc,"\"" + vecTemp.get(6) + "\",");
    			// Site ID
    			this.writetofile(fc,"\"" + vecTemp.get(7) + "\",");
    			// Transaction type/Transaction type description
    			this.writetofile(fc,"\"" + vecTemp.get(8).replace("&#47;", "/") + "\",");
    			// Terminal Type
    			this.writetofile(fc,"\"" + vecTemp.get(9).replace("&#47;", "/") + "\",");
    			// Host application type
    			this.writetofile(fc,"\"" + vecTemp.get(10) + "\",");
    			// Transaction duration
    			this.writetofile(fc,"\"" + vecTemp.get(11) + "\",");
    			// Result/Result Description
    			this.writetofile(fc,"\"" + vecTemp.get(13).replace("&#47;", "/").trim() + "\",");
    			// Provider Response
    			this.writetofile(fc,"\"" + vecTemp.get(14).trim() + "\",");
    			
    			// Invoice number
    			if(invoiceNumberEnabled && deployTypeIsIntl && customConfigTypeIsDefault)
    				this.writetofile(fc,"\"" + vecTemp.get(15) + "\",");

    			// Show more info if the transaction is successful
    			// Looks like for certain transaction types, there is no web_transaction record. For example, Type 1/Host Report
    			// Also, not always is there web_transactions info in the DB for successful transactions
    			if(vecTemp.get(12).equals("0") && vecTemp.size() > 16)
          	  	{
    				// Use this for determining if we need to write in headers for success
    				hadSuccessfulTrans = true;
					// Merchant ID
    				this.writetofile(fc,"\"" + vecTemp.get(16) + "\",");
					// City
    				this.writetofile(fc,"\"" + vecTemp.get(17) + "\",");
					// County
    				this.writetofile(fc,"\"" + vecTemp.get(18) + "\",");
					// Clerk
    				this.writetofile(fc,"\"" + vecTemp.get(19) + "\",");
					// Ref No
    				this.writetofile(fc,"\"" + vecTemp.get(25) + "\",");
					// Bonus Amt
    				this.writetofile(fc,"\"" + vecTemp.get(20) + "\",");
					// Total Recharge
    				this.writetofile(fc,"\"" + vecTemp.get(26) + "\",");
					// Balance
    				this.writetofile(fc,"\"" + vecTemp.get(21) + "\",");
					// Control #
    				this.writetofile(fc,"\"" + vecTemp.get(22) + "\",");
					// Transaction Type
    				this.writetofile(fc,"\"" + vecTemp.get(23) + "\",");
					
					// PIN
    				if(hasPermViewPin && accessLevelIsISO && deployTypeIsDomestic) 
    				{
    					if(vecTemp.get(24) != null)
    						this.writetofile(fc,"\"" + vecTemp.get(24) + "\",");
    				}
              	  	this.writetofile(fc,"\r\n");
          	  	}
    			else if(vecTemp.get(12).equals("0") && vecTemp.size() <= 16)
    			{
    				// Warn the user that this is a successful transaction but no web_transactions info was avavilable
    				this.writetofile(fc,"\"No additional information available for this transaction\",");
              	  	this.writetofile(fc,"\r\n");
    			}
    			else
          	  		this.writetofile(fc,"\r\n");
    			
          	  	this.icount++;
            }
            
            fc.close();
            endTime = System.currentTimeMillis();
            cat.debug("File Out: " + (((endTime - startTime) / 1000.0)) + " seconds");
            // reset for zip file out
            startTime = endTime;
            File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
            long size = sourceFile.length();
            byte[] buffer = new byte[(int) size];
            String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
            cat.debug("Zip File Name: " + zipFileName);
            downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
            cat.debug("Download URL: " + downloadUrl);
            
            try
            {
            	ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

            	// Set the compression ratio
            	out.setLevel(Deflater.DEFAULT_COMPRESSION);
            	FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

            	// Add ZIP entry to output stream.
            	out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

            	int len;
            	while ((len = in.read(buffer)) > 0)
            	{
            		out.write(buffer, 0, len);
            	}

            	// Close the current entry
            	out.closeEntry();
            	// Close the current file input stream
            	in.close();
            	out.close();
            }
            catch (IllegalArgumentException iae)
            {
            	iae.printStackTrace();
            }
            catch (FileNotFoundException fnfe)
            {
            	fnfe.printStackTrace();
            }
            catch (IOException ioe)
            {
            	ioe.printStackTrace();
            }
            sourceFile.delete();
            endTime = System.currentTimeMillis();
            cat.debug("Zip Out: " + (((endTime - startTime) / 1000.0)) + " seconds");
        }
        catch (IOException ioe)
        {
        	// break radio silence about transaction report downloads!
        	cat.error("Download transactions failed! " + ioe.getClass().getName() + " (" + ioe.getMessage() + ")", ioe);
            try
            {
            	if (fc != null)
            	{
            		fc.close();
            	}
            }
            catch (IOException ioe2)
            {
            }
        }
        
        long end = System.currentTimeMillis();

        // Display the elapsed time to the standard output
        cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
        this.isdone = true;
    	cat.debug("END downloadCarrierUserTrans");
        return downloadUrl;
    }
    
//combines transactions vector with pins and cards
//needed because the database join takes too long
  private Vector combineResults(Vector vecTransactions, String strControlNo) throws TransactionException
  {
    long start = System.currentTimeMillis();
    Connection dbConn = null;
    String strSQL = "";
    PreparedStatement pstmt;

    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new TransactionException();
      }

      //get all related cards and pins load a hash and update the return vector.
      //load the hash
      Hashtable hashPinsCards = new Hashtable();
      strSQL = "";
      strSQL = sql_bundle.getString("getCardProducts");
      strControlNo = strControlNo.substring(0, (strControlNo.length() - 1));
      strSQL = strSQL + " ( " + strControlNo + ") ";
      strSQL = strSQL + "UNION " + sql_bundle.getString("getPinProducts");
      strSQL = strSQL + " ( " + strControlNo + ") ";
      cat.debug(strSQL);
      pstmt = dbConn.prepareStatement(strSQL);

      long startQuery = System.currentTimeMillis();
      ResultSet rs = pstmt.executeQuery();
      long endQuery = System.currentTimeMillis();
      cat.debug("Combine Query: " + (((endQuery - startQuery) / 1000.0)) + " seconds");

      while (rs.next())
      {
        Vector vecTemp = new Vector();
        vecTemp.add(StringUtil.toString(rs.getString("ani")));
        vecTemp.add(StringUtil.toString(rs.getString("password")));
        vecTemp.add(StringUtil.toString(rs.getString("balance")));
        vecTemp.add(StringUtil.toString(rs.getString("id")));
        vecTemp.add(StringUtil.toString(rs.getString("description")));
        vecTemp.add(StringUtil.toString(rs.getString("trans_type")));
        hashPinsCards.put(rs.getString("control_no"), vecTemp);
      }
      pstmt.close();
      rs.close();

      //update the return vector
      for (int i = 1; i < vecTransactions.size(); i++)
      {
        //Vector vecPinsCards = new Vector();
        //Vector vecTemp = new Vector();
        Vector vecTemp = (Vector) vecTransactions.elementAt(i);
        //control_no
        strControlNo = vecTemp.get(12).toString();
        Vector vecPinsCards = (Vector) hashPinsCards.get(strControlNo);
        //ani
        //password
        if (vecPinsCards == null)
        {
          vecTemp.setElementAt("", 7);
          vecTemp.setElementAt("", 8);
          vecTemp.setElementAt("", 10);
          vecTemp.setElementAt("", 11);
          vecTemp.setElementAt("", 13);

        }
        else 
        {
          vecTemp.setElementAt(vecPinsCards.get(0).toString(), 7);
          vecTemp.setElementAt(vecPinsCards.get(1).toString(), 8);
          //balance
          if (vecPinsCards.get(2) == null || vecPinsCards.get(2).toString().equals(""))
          {
            vecTemp.setElementAt(vecTemp.get(9), 10);
          }
          else
          {
            vecTemp.setElementAt(NumberUtil.formatCurrency(vecPinsCards.get(2).toString()), 10);
          }

          //id / description
          vecTemp.setElementAt(vecPinsCards.get(3).toString() + "/" + vecPinsCards.get(4).toString(), 11);
          vecTemp.setElementAt(vecPinsCards.get(5).toString(), 13);

        }

        vecTransactions.setElementAt(vecTemp, i);
      }

    }
    catch (Exception e)
    {
      cat.error("Error during combineResults", e);
      throw new TransactionException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    long end = System.currentTimeMillis();
    cat.debug("Combine Total: " + (((end - start) / 1000.0)) + " seconds");
    return vecTransactions;

  }


  /**
   * Returns a hash of errors.
   *
   * @return Hash of errors.
   */

  public Hashtable getErrors()
  {
    return this.validationErrors;
  }

  /**
   * Returns a validation error against a specific field.  If a field
   * was found to have an error during
   * {@link #validateDateRange validateDateRange},  the error message
   * can be accessed via this method.
   *
   * @param fieldname The bean property name of the field
   * @return The error message for the field or null if none is
   *         present.
   */

  public String getFieldError(String fieldname)
  {
    return ((String) this.validationErrors.get(fieldname));
  }


  /**
   * Sets the validation error against a specific field.  Used by
   * {@link #validateDateRange validateDateRange}.
   *
   * @param fieldname The bean property name of the field
   * @param error     The error message for the field or null if none is
   *                  present.
   */

  public void addFieldError(String fieldname, String error)
  {
	  this.validationErrors.put(fieldname, error);
  }


  /**
   * Validates for missing or invalid fields.
   *
   * @return <code>true</code> if the entity passes the validation
   *         checks,  otherwise  <code>false</code>
   */

  public boolean validateDateRange(SessionData sessionData)
  {
	  this.validationErrors.clear();
    boolean valid = true;

    if ((this.start_date == null) ||
        (this.start_date.length() == 0))
    {
      this.addFieldError("startdate", Languages.getString("com.debisys.reports.error1",sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.start_date))
    {
      this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2",sessionData.getLanguage()));
      valid = false;
    }

    if ((this.end_date == null) ||
        (this.end_date.length() == 0))
    {
      this.addFieldError("enddate", Languages.getString("com.debisys.reports.error3",sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.end_date))
    {
      this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4",sessionData.getLanguage()));
      valid = false;
    }

    if (valid)
    {

      Date dtStartDate = new Date();
      Date dtEndDate = new Date();
      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
      formatter.setLenient(false);
      try
      {
        dtStartDate = formatter.parse(this.start_date);
      }
      catch (ParseException ex)
      {
        this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2",sessionData.getLanguage()));
        valid = false;
      }
      try
      {
        dtEndDate = formatter.parse(this.end_date);
      }
      catch (ParseException ex)
      {
        this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4",sessionData.getLanguage()));
        valid = false;
      }
      if (dtStartDate.after(dtEndDate))
      {
        this.addFieldError("date", "Start date must be before end date.");
        valid = false;
      }

    }

    return valid;
  }

  
  
  /**
  * @author nmartinez
  * For sake the report generation we have to know what page come from the request
  */
  
	//public final static int Transaction_Mercsumbytrm_Detail_jsp=0;
	//public final static int Agents_Transactions_jsp=1;
	//public final static int Merchants_Transactions_jsp=2;
	//public final static int Reps_Transactions_jsp=3;
	//public final static int Subagents_Transactions_jsp=4;
	//public final static int Transactions_jsp=5;
  

  public boolean isdone(){
	  return this.isdone;
  }

  public int getCount(){
	  return this.icount;
  }
  
  /**
  * @param sessionData
  * @param intSectionPage
  * @param context
  * @param sourcePage
  * @return
  * @throws TransactionException
  */
  public String download_orig(SessionData sessionData, int intSectionPage, ServletContext context) throws TransactionException
  {

    this.isdone = false;
    long start = System.currentTimeMillis();
    Connection dbConn = null;
    String strSQL = "";
    String strFileName = "";
    
    String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
    String downloadPath = DebisysConfigListener.getDownloadPath(context);
    String workingDir = DebisysConfigListener.getWorkingDir(context);
    String filePrefix = DebisysConfigListener.getFilePrefix(context);
    String deploymentType = DebisysConfigListener.getDeploymentType(context);
    String customConfigType = DebisysConfigListener.getCustomConfigType(context);
    String strRefId = sessionData.getProperty("ref_id");
    String strAccessLevel = sessionData.getProperty("access_level");
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    PreparedStatement pstmt = null;
  //Added by Jacuna. R25 DBSY-525
    String str_ADDITIONAL_FIELDS_ = ",wt.org_IP_Address as org_ip_address";
    str_ADDITIONAL_FIELDS_ += ", wt.org_Phone_Number as org_phone_number";
    str_ADDITIONAL_FIELDS_ += ",terminal_types.description as trm_description ";
    String str_ADDITIONAL_JOINS = "INNER JOIN terminals WITH (NOLOCK) ON terminals.millennium_no = wt.millennium_no "; 
    str_ADDITIONAL_JOINS += "INNER JOIN terminal_types WITH (NOLOCK) ON terminal_types.terminal_type=terminals.terminal_type";
    boolean blnUSEAdditionalFields = false;
  //END. Added by Jacuna. R25 DBSY-525
    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new TransactionException();
      }

      Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date + " 23:59:59.999", false);

      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
        {
        	strSQL = sql_bundle.getString("downloadISO3PINACH");
        	cat.debug("downloadISO3PINACH");
			strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
			strSQL = strSQL.replaceAll("_REFID_", strRefId);
			if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
			    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
			}
			if (this.transactionID != null && !this.transactionID.equals(""))
			{
				strSQL += " AND wt.rec_id = " + this.transactionID + " ";
			}
			if (this.invoiceID != null && !this.invoiceID.equals(""))
			{
				strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
			}
						
			strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
			strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS);	
		  strSQL += " ORDER BY wt.rec_id";
          pstmt = dbConn.prepareStatement(strSQL);
          pstmt.setDouble(1, Double.parseDouble(strRefId));
          pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
          pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
        }
        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
        {
        	strSQL = sql_bundle.getString("downloadISO5PINACH");
        	cat.debug("downloadISO5PINACH");
			strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
			strSQL = strSQL.replaceAll("_REFID_", strRefId);
			if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
			    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
			}
			if (this.transactionID != null && !this.transactionID.equals(""))
			{
				strSQL += " AND wt.rec_id = " + this.transactionID + " ";
			}
			if (this.invoiceID != null && !this.invoiceID.equals(""))
			{
				strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
			}

			strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
			strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS);	
			strSQL += " ORDER BY wt.rec_id";
          pstmt = dbConn.prepareStatement(strSQL);
          pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
          pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
          pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
          pstmt.setDouble(4, Double.parseDouble(strRefId));
          pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
          pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
        }
      }

      cat.debug(strSQL);
      long startQuery = System.currentTimeMillis();
      ResultSet rs = pstmt.executeQuery();
      long endQuery = System.currentTimeMillis();
      cat.debug("Download Query: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
      startQuery = System.currentTimeMillis();

      strFileName = this.generateFileName(context);    

	File file = new File(workingDir);
	if(!file.exists()) {
		file.mkdir();
	}
	
      FileChannel fc = 
          new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw")
      .getChannel();
     // BufferedWriter outputFile = null;
      try
      {
       // outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
        cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
        //outputFile.flush();

        Date dateReportTransactions = new Date();
        String actualDate;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        formatter.setLenient(false);
        actualDate = formatter.format(dateReportTransactions);
        
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.namereportexcel",sessionData.getLanguage())+"\"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.generatedby",sessionData.getLanguage())+"\","+sessionData.getUser().getUsername()+"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.generationdate",sessionData.getLanguage())+"\","+actualDate+"\n");
        this.writetofile(fc,"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.filtersapplied",sessionData.getLanguage())+"\"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.iso_name",sessionData.getLanguage())+"\","+sessionData.getUser().getCompanyName() + "\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.start_date",sessionData.getLanguage())+"\"," +this.start_date+"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.end_date",sessionData.getLanguage())+"\"," +this.end_date+"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.reports.transaction_id",sessionData.getLanguage())+"\"," +this.transactionID+"\n");
        if(!strAccessLevel.equals(DebisysConstants.MERCHANT)){
        	if(this.merchant_ids != null && !this.merchant_ids.equals("")){
        		this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.merchants",sessionData.getLanguage())+"\","+this.merchant_ids+"\n");
        	}else{
        		this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.merchants",sessionData.getLanguage())+"\","+Languages.getString("jsp.admin.reports.all",sessionData.getLanguage())+"\n");
        	}
        		
        }
        this.writetofile(fc,"\n");	
        //*****************************************//
        //Begin add by nmartinez.R30. DBSY-815
        this.writetofile(fc,"\n");
        this.writetofile(fc,"\"#\",");
  		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.tran_no",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.term_no",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.dba",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\""  + Languages.getString("jsp.admin.reports.merchant_id",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.date",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.amount",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.product",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.description",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.transactions.terminaltype",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.transactions.org_ip_address",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.transactions.org_phone_number",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\r\n");
        this.icount=1;
        //End add by nmartinez.R30. DBSY-815
        //*****************************************//
        
        Hashtable<Integer, String> ht_taxTypes = TaxTypes.getTaxTypes();
        
        while (rs.next())
        {
          if( (this.max < this.icount) && this.max!=-1)
        	break;

      	  this.writetofile(fc,"\"" + this.icount + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("rec_id")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("millennium_no")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("dba")) + "\",");
          this.writetofile(fc,"\""  +StringUtil.toString(rs.getString("merchant_id")) + "\",");
          this.writetofile(fc,"\"" + DateUtil.formatSqlDateTime(rs.getTimestamp("datetime")) + "\",");
          this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("amount")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("id")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("description")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("trm_description")) + "\",");
          String org_ip_address = StringUtil.toString(rs.getString("org_ip_address"));
          if (org_ip_address.compareTo("")==0)
        	  org_ip_address = "N/A";
          this.writetofile(fc,"\"" + org_ip_address + "\",");
          String org_phone_number = StringUtil.toString(rs.getString("org_phone_number"));
          if (org_phone_number.compareTo("")==0)
        	  org_phone_number = "N/A";          
          this.writetofile(fc,"\"" + org_phone_number + "\",");
          this.writetofile(fc,"");
          this.writetofile(fc,"\r\n");
          this.icount++;
        }
        fc.close();
        pstmt.close();
        rs.close();
        endQuery = System.currentTimeMillis();
        cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
        startQuery = System.currentTimeMillis();
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        cat.debug("Zip File Name: " + zipFileName);
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
        cat.debug("Download URL: " + downloadUrl);
        
        try
        {

          ZipOutputStream out =
              new ZipOutputStream(new FileOutputStream(zipFileName));

          // Set the compression ratio
          out.setLevel(Deflater.DEFAULT_COMPRESSION);
          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

          // Add ZIP entry to output stream.
          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

          // Transfer bytes from the current file to the ZIP file
          //out.write(buffer, 0, in.read(buffer));

          int len;
          while ((len = in.read(buffer)) > 0)
          {
            out.write(buffer, 0, len);
          }

          // Close the current entry
          out.closeEntry();
          // Close the current file input stream
          in.close();
          out.close();
        }
        catch (IllegalArgumentException iae)
        {
          iae.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
          fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
        sourceFile.delete();
        endQuery = System.currentTimeMillis();
        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");


      }
      catch (IOException ioe)
      {
	  // break radio silence about transaction report downloads!
	  cat.error("Download transactions failed! " + ioe.getClass().getName() 
		  + " (" + ioe.getMessage() + ")", ioe);
        try
        {
          if (fc != null)
          {
        	  fc.close();
          }
        }
        catch (IOException ioe2)
        {
        }
      }
    }
    catch (Exception e)
    {
      cat.error("Error during download", e);
      throw new TransactionException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    long end = System.currentTimeMillis();

    // Display the elapsed time to the standard output
    cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
    this.isdone = true;
    return downloadUrl;
  }

  
  /**
  * @param sessionData
  * @param intSectionPage
  * @param context
  * @param sourcePage
  * @return
  * @throws TransactionException
  */
  public String download(SessionData sessionData, int intSectionPage, ServletContext context) throws TransactionException
  {

    this.isdone = false;
    long start = System.currentTimeMillis();
    Connection dbConn = null;
    String strSQL = "";
    String strFileName = "";
    
    String strCarrierUserProds = sessionData.getProperty("carrierUserProds");

	
    String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
    String downloadPath = DebisysConfigListener.getDownloadPath(context);
    String workingDir = DebisysConfigListener.getWorkingDir(context);
    String filePrefix = DebisysConfigListener.getFilePrefix(context);
    String deploymentType = DebisysConfigListener.getDeploymentType(context);
    String customConfigType = DebisysConfigListener.getCustomConfigType(context);
    String strRefId = sessionData.getProperty("ref_id");
    String strAccessLevel = sessionData.getProperty("access_level");
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    PreparedStatement pstmt = null;
  //Added by Jacuna. R25 DBSY-525
    String str_ADDITIONAL_FIELDS_ = "";
    
    
    str_ADDITIONAL_FIELDS_ += ", (wt.merchant_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS merchant_commission, "
    					+ " (wt.rep_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS rep_commission, "
    					+ " (wt.agent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS agent_commission, "
    					+ " (wt.subagent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS subagent_commission, "
    					+ " (wt.iso_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) AS iso_commission ";
       
    String str_ADDITIONAL_JOINS_ = " inner join products p WITH (NOLOCK) ON p.id = wt.id ";
    boolean blnUSEAdditionalFields = false;
    boolean showAdditionalDataColumn = sessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA);

    boolean permissionViewPinISODomestic = (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && 
    					  						strAccessLevel.equals(DebisysConstants.ISO) && 
    					  							DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) );
    
    
    
    boolean permissionAchDateIsoDomestic = sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && 
    										strAccessLevel.equals(DebisysConstants.ISO) && 
    											DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
    
    
    if ( permissionViewPinISODomestic || (this.PINNumber != null && !this.PINNumber.equals("")) )
    {
    	blnUSEAdditionalFields = true;
    	str_ADDITIONAL_FIELDS_ += " , t.pin";
    	str_ADDITIONAL_JOINS_ += " inner join transactions t with (nolock) on (wt.rec_id = t.rec_id) ";
    }
    
    if ( permissionAchDateIsoDomestic )
    {
    	blnUSEAdditionalFields = true;
    	str_ADDITIONAL_FIELDS_ += " , dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", ach_t.Process_Date, 1) as ach_datetime";
    	str_ADDITIONAL_JOINS_ += " left outer join ach_transaction_audit ach_t with (nolock) on ( wt.rec_id = ach_t.rec_id and wt.merchant_id = ach_t.entity_id) ";
    } 
    
    boolean invoicePermission = com.debisys.users.User.isInvoiceNumberEnabled(sessionData, context) && 
									DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
										DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
    
    
  //END. Added by Jacuna. R25 DBSY-525
    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new TransactionException();
      }

      Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date + " 23:59:59.999", false);
      CarrierSearch.cat.debug("sectionpage="+intSectionPage);
      //get a count of total matches
      switch (intSectionPage)
      {
        //transactions.jsp
        case 1:
        	
        	  if (strAccessLevel.equals(DebisysConstants.CARRIER))
			{
        		if (this.rep_id.length() == 0)
				{
					strSQL = sql_bundle.getString("downloadCarrierISO_NoRep");
					cat.debug("downloadCarrierISO_NoRep");
				}
				else
				{
					strSQL = sql_bundle.getString("downloadCarrierISO_Rep");
					cat.debug("downloadCarrierISO_Rep");
				}
				
                
                strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
                strSQL = strSQL.replaceAll("_REFID_", strRefId);

				strSQL = strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
						strSQL = strSQL.replaceAll("_MULTIREPID_", this.rep_id);

  				//END. Added by Jacuna. R25 DBSY-525     
  				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
  				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
  							}
  				

  							if (this.transactionID != null && !this.transactionID.equals(""))
  							{
  								strSQL += " AND wt.rec_id = " + this.transactionID + " ";
  							}
  							if (this.invoiceID != null && !this.invoiceID.equals(""))
  							{
  								strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
  							}
  							
  							if (this.PINNumber != null && !this.PINNumber.equals(""))
  							{
  								ArrayList<String> skusWithEncryption = null;
  								skusWithEncryption = getSkusWithEncryption(dbConn);
  								strSQL += " AND wt.product_trans_type = 2 ";
  								strSQL += " AND wt.amount > 0 ";
  								// pin encryption: protect
  								String pin = this.PINNumber;
  								String sku = getSkuFromPins(pin, dbConn);
  								if (skusWithEncryption.contains(sku))
  								{
  									pin = dataProtection().t0(pin);
  								}
  								strSQL += " AND t.pin = '" + pin + "' ";
  				}

  				strSQL += " ORDER BY wt.rec_id";
  				strSQL = strSQL.replaceAll("_STARTDATE_", vTimeZoneFilterDates.get(0).toString());
  				strSQL = strSQL.replaceAll("_ENDDATE_", vTimeZoneFilterDates.get(1).toString());
                pstmt = dbConn.prepareStatement(strSQL);
                
          }
        		 
          else if (strAccessLevel.equals(DebisysConstants.ISO))
          {
        	  
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
            {
            	
            		strSQL = sql_bundle.getString("downloadISO3PINACH");
            		cat.debug("downloadISO3PINACH");
            	
            	
            	strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
				//Added by Jacuna. R25 DBSY-525
				if(blnUSEAdditionalFields) 
				{
					String strRepsSelect = "SELECT rep_id FROM reps with (nolock) WHERE iso_id =" + strRefId;
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				}else{
				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
					/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				}
				//END. Added by Jacuna. R25 DBSY-525
				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
							}

								if (this.transactionID != null && !this.transactionID.equals(""))
								{
									strSQL += " AND wt.rec_id = " + this.transactionID + " ";
								}
								if (this.invoiceID != null && !this.invoiceID.equals(""))
								{
									strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
								}
							
							if (this.PINNumber != null && !this.PINNumber.equals(""))
							{
								ArrayList<String> skusWithEncryption = null;
								skusWithEncryption = getSkusWithEncryption(dbConn);
								strSQL += " AND wt.product_trans_type = 2 ";
								strSQL += " AND wt.amount > 0 ";
								// pin encryption: protect
								String pin = this.PINNumber;
								String sku = getSkuFromPins(pin, dbConn);
								if (skusWithEncryption.contains(sku))
								{
									pin = dataProtection().t0(pin);
								}
								strSQL += " AND t.pin = '" + pin + "' ";
				}
				if ( sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
				{
					strSQL += " AND wt.id IN (";
					strSQL += "SELECT p.id FROM Products p WITH (NOLOCK) INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.carrier_id = rc.CarrierID ";
					strSQL += "WHERE rc.RepID = " + sessionData.getUser().getRefId() + ")";
				}

				strSQL += " ORDER BY wt.rec_id";
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(strRefId));
              pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
            }
            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {

          		strSQL = sql_bundle.getString("downloadISO5PINACH");
              
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
				//Added by Jacuna. R25 DBSY-525
				if(blnUSEAdditionalFields) 
				{
					String strRepsSelect = "SELECT rep_id FROM reps with (nolock) WHERE type="+DebisysConstants.REP_TYPE_REP+
						" AND iso_id IN (SELECT rep_id FROM reps with (nolock) WHERE type="+DebisysConstants.REP_TYPE_SUBAGENT+
						" AND iso_id IN (SELECT rep_id FROM reps with (nolock) WHERE type="+DebisysConstants.REP_TYPE_AGENT+" AND iso_id="+strRefId+"))";
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				}else{
				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/     			
				}
				//END. Added by Jacuna. R25 DBSY-525     
				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
							}

							if (this.transactionID != null && !this.transactionID.equals(""))
							{
								strSQL += " AND wt.rec_id = " + this.transactionID + " ";
							}
							if (this.invoiceID != null && !this.invoiceID.equals(""))
							{
								strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
							}
							
							if (this.PINNumber != null && !this.PINNumber.equals(""))
							{
								ArrayList<String> skusWithEncryption = null;
								skusWithEncryption = getSkusWithEncryption(dbConn);
								strSQL += " AND wt.product_trans_type = 2 ";
								strSQL += " AND wt.amount > 0 ";
								// pin encryption: protect
								String pin = this.PINNumber;
								String sku = getSkuFromPins(pin, dbConn);
								if (skusWithEncryption.contains(sku))
								{
									pin = dataProtection().t0(pin);
								}
								strSQL += " AND t.pin = '" + pin + "' ";
				}
							if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))
							{
								strSQL += " AND wt.id IN (";
								strSQL += "SELECT p.id FROM Products p WITH (NOLOCK) INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.carrier_id = rc.CarrierID ";
								strSQL += "WHERE rc.RepID = " + sessionData.getUser().getRefId() + ")";
							}

				strSQL += " ORDER BY wt.rec_id";
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
              pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
              pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
              pstmt.setDouble(4, Double.parseDouble(strRefId));
              pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
            }
          }
          break;
          //rep_transactions.jsp
        case 54:
        	if (strAccessLevel.equals(DebisysConstants.CARRIER))
          {
               
              		strSQL = sql_bundle.getString("downloadCarrierRepPINACH");     
                      cat.debug("downloadCarrierRepPINACH");     		
              	
                  strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
                  strSQL = strSQL.replaceAll("_REFID_", strRefId);
                
  				//Added by Jacuna. R25 DBSY-525
  				if(blnUSEAdditionalFields) 
  				{
  					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  				}else{
  				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
  					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
  					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  				    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
  				}
  				//END. Added by Jacuna. R25 DBSY-525     
  				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {
  				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
  				}
				if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))
				{
					strSQL += " AND wt.id IN (";
					strSQL += "SELECT p.id FROM Products p WITH (NOLOCK) INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.carrier_id = rc.CarrierID ";
					strSQL += "WHERE rc.RepID = " + sessionData.getUser().getRefId() + ")";
				}
				
  		
  				
  			      strSQL += " ORDER BY wt.rec_id";
  			   
 				strSQL = strSQL.replaceAll("_STARTDATE_", vTimeZoneFilterDates.get(0).toString());
  				strSQL = strSQL.replaceAll("_ENDDATE_", vTimeZoneFilterDates.get(1).toString());
                pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                pstmt.setDouble(1, Long.parseLong(this.rep_id));
                
            }
        	else if (strAccessLevel.equals(DebisysConstants.ISO))
          {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
            {
            	
            		strSQL = sql_bundle.getString("downloadRep3PINACH");     
                    cat.debug("downloadRep3PINACH");     		
            	
                strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
                strSQL = strSQL.replaceAll("_REFID_", strRefId);
              
				//Added by Jacuna. R25 DBSY-525
				if(blnUSEAdditionalFields) 
				{
					String strRepsSelect = "SELECT rep_id FROM reps WITH (nolock) WHERE iso_id = ?";
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				}else{
				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", ""); 
				    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				}
				//END. Added by Jacuna. R25 DBSY-525     
				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {
				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
				}
				if(strCarrierUserProds.length() > 0)
  					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
  				else // return no records.. since no products attached
  					strSQL += " AND wt.id =\"\" ";
			      strSQL += " ORDER BY wt.rec_id";
			 
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.rep_id));
              pstmt.setDouble(2, Double.parseDouble(strRefId));
              pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
            }
            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
            	
            	
            	strSQL = sql_bundle.getString("downloadRep5PINACH");    
            	cat.debug("downloadRep5PINACH");     		         		
            	
                strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
                strSQL = strSQL.replaceAll("_REFID_", strRefId);
             
				//Added by Jacuna. R25 DBSY-525
				if(blnUSEAdditionalFields) 
				{
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				}else{
				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);  
				    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/     			
				}
				//END. Added by Jacuna. R25 DBSY-525
				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
				}
				if(strCarrierUserProds.length() > 0)
  					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
  				else // return no records.. since no products attached
  					strSQL += " AND wt.id =\"\" ";
			      strSQL += " ORDER BY wt.rec_id";
		     
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.rep_id));
              pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
              pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
              pstmt.setInt(4, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
              pstmt.setDouble(5, Double.parseDouble(strRefId));
              pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());
            }
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
        	
          	strSQL = sql_bundle.getString("downloadRep5Agent");        
            cat.debug("downloadRep5Agent");     	  		
          	
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
              
              
  			if (blnUSEAdditionalFields)
			{
				strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				 /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/ 
			}
			else
			{
				strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				 /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/ 
			}    
            
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
    	}
          strSQL += " ORDER BY wt.rec_id";
          
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.rep_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(4, Double.parseDouble(strRefId));
            pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
          {
        	  
        	  strSQL = sql_bundle.getString("downloadRep5SubAgent");
        	  cat.debug("downloadRep5SubAgent");     	  	      		
            	
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
            
          
	          if (this.merchant_ids != null && this.merchant_ids.length() > 0) 
	          {		    
    	    	strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	    }
	          
			if (blnUSEAdditionalFields)
			{
				strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				 /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/ 
			}
			else
			{
				strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				 /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/ 
			}    
	          
          strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.rep_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setDouble(3, Double.parseDouble(strRefId));
            pstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.REP))
          {
        	
        	  strSQL = sql_bundle.getString("downloadRep");      
        	  cat.debug("downloadRep");     	 
        	  
        	  strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
        	  strSQL = strSQL.replaceAll("_REFID_", strRefId);             
           
        
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    	strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	    }
    		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
			if (blnUSEAdditionalFields)
			{
				strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				 /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/ 
			}
			else
			{
				strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", "");
				 /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/ 
			}

			if (this.transactionID != null && !this.transactionID.equals(""))
						{
							strSQL += " AND wt.rec_id = " + this.transactionID + " ";
						}
			
			if (this.invoiceID != null && !this.invoiceID.equals(""))
			{
				strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
			}

			if (this.PINNumber != null && !this.PINNumber.equals(""))
						{
							ArrayList<String> skusWithEncryption = null;
							skusWithEncryption = getSkusWithEncryption(dbConn);
							strSQL += " AND wt.product_trans_type = 2 ";
							strSQL += " AND wt.amount > 0 ";
							// pin encryption: protect
							String pin = this.PINNumber;
							String sku = getSkuFromPins(pin, dbConn);
							if (skusWithEncryption.contains(sku))
							{
								pin = dataProtection().t0(pin);
							}
							strSQL += " AND t.pin = '" + pin + "' ";
						}

          strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(strRefId));
            pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
          }
          break;
          //merchant_transactions.jsp
        case 52:
        	if (strAccessLevel.equals(DebisysConstants.CARRIER))
            {
        		
        		strSQL = sql_bundle.getString("downloadCarrierMerchant");
        		cat.debug("downloadCarrierMerchant");
                
                strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
                strSQL = strSQL.replaceAll("_REFID_", strRefId);
               
                if ( !this.millennium_no.equals("") )
                {
              	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
                }
                else
                {
              	  strSQL = strSQL.replaceAll("_SITEID_", "");
                }
  				//Added by Jacuna. R25 DBSY-525
  				if(blnUSEAdditionalFields) 
  				{
  					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  				}else{
  				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
  					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
  					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  				    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/     			
  				}
  				//END. Added by Jacuna. R25 DBSY-525
  				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
  				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
  				}

  		  		
  				
  			      strSQL += " ORDER BY wt.rec_id";
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setDouble(1, Long.parseLong(strRefId));
                pstmt.setDouble(2, Long.parseLong(this.merchant_id));
                //pstmt.setDouble(3, Long.parseLong(strRefId));
                pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
                pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
              
            }
        	/**************this lines was not in last version since 5 Mayo 2008***********************/
        	/**************It was added by mistakes in QA BY EMUNOZ***********************************/
            else 
            {
            	if (strAccessLevel.equals(DebisysConstants.MERCHANT))
                {
              	this.merchant_id = sessionData.getProperty("ref_id");
                }
              
          		strSQL = sql_bundle.getString("downloadMerchant");
                  cat.debug("downloadMerchant");     
          	
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);

              if ( !this.millennium_no.equals("") )
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
              }
              else
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", "");
              }
      		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
  			if (blnUSEAdditionalFields)
  			{
  							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  							/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
  							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  							/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
  							
  						}
  						else
  						{
  							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", "");
  							/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
  							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  							/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
  						}

  			if (this.merchant_ids != null && this.merchant_ids.length() > 0)
  						{
  							strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
  						}
      		if (this.transactionID != null && !this.transactionID.equals(""))
  			{
  							strSQL += " AND wt.rec_id = " + this.transactionID + " ";
  						}
      		if (this.invoiceID != null && !this.invoiceID.equals(""))
  			{
  							strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
  						}
      		
      	    if (this.PINNumber != null && !this.PINNumber.equals(""))
  			{
  							ArrayList<String> skusWithEncryption = null;
  							skusWithEncryption = getSkusWithEncryption(dbConn);
  							strSQL += " AND wt.product_trans_type = 2 ";
  							strSQL += " AND wt.amount > 0 ";
  							// pin encryption: protect
  							String pin = this.PINNumber;
  							String sku = getSkuFromPins(pin, dbConn);
  							if (skusWithEncryption.contains(sku))
  							{
  								pin = dataProtection().t0(pin);
  							}
  							strSQL += " AND t.pin = '" + pin + "' ";
  						}

  						strSQL += " ORDER BY wt.rec_id";
  	          if(CarrierSearch.cat.isDebugEnabled())
  	        	CarrierSearch.cat.debug(strSQL);
  	          
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
              pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
            }
        	 break;
         	/**************end - this lines was not in last version since 5 Mayo 2008***********************/
         	/**************end -It was added by mistakes in QA BY EMUNOZ***********************************/
        case 14:
        	
          if (strAccessLevel.equals(DebisysConstants.CARRIER))
          {
        	  strSQL = sessionData.getProperty("sqlCarrier14"); 
        	  pstmt = dbConn.prepareStatement(strSQL);
          }
          else if (strAccessLevel.equals(DebisysConstants.ISO))
          {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
            {
            	
            	strSQL = sql_bundle.getString("downloadMerchant3");   	    
            	cat.debug("downloadMerchant3");     	  	      	   	
              
            	strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            	strSQL = strSQL.replaceAll("_REFID_", strRefId);

              if ( !this.millennium_no.equals("") )
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
              }
              else
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", "");
              }
				//Added by Jacuna. R25 DBSY-525
				if(blnUSEAdditionalFields) 
				{
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				}else{
				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					
					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", "");						  
				    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/     			
				}
				//END. Added by Jacuna. R25 DBSY-525    
				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
				}

	      		if(strCarrierUserProds.length() > 0)
						strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
					else // return no records.. since no products attached
						strSQL += " AND wt.id =\"\" ";
	      		
			      strSQL += " ORDER BY wt.rec_id";
			     
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
              pstmt.setDouble(2, Double.parseDouble(strRefId));
              pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
            }
            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
            	
              		 strSQL = sql_bundle.getString("downloadMerchant5");
                     cat.debug("downloadMerchant5");
              	
                strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
                strSQL = strSQL.replaceAll("_REFID_", strRefId);
             
              if ( !this.millennium_no.equals("") )
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
              }
              else
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", "");
              }
				//Added by Jacuna. R25 DBSY-525
				if(blnUSEAdditionalFields) 
				{
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				}else{
				    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);	   
				    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/     			
				}
				//END. Added by Jacuna. R25 DBSY-525
				if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
				    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
				}

	      		if(strCarrierUserProds.length() > 0)
						strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
					else // return no records.. since no products attached
						strSQL += " AND wt.id =\"\" ";
			      strSQL += " ORDER BY wt.rec_id";
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
              pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
              pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
              pstmt.setInt(4, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
              pstmt.setDouble(5, Double.parseDouble(strRefId));
              pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());
            }
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
        	  
        	  strSQL = sql_bundle.getString("downloadMerchant5Agent");	
        	  cat.debug("downloadMerchant5Agent");     
            	
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			 if(blnUSEAdditionalFields) 
				{
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
				}else{
				   
					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);	   
				        			
				} 
				 /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
           
            if ( !this.millennium_no.equals("") )
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
            }
            else
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", "");
            }
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	}

      		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
          strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(4, Double.parseDouble(strRefId));
            pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
          {
        	
        	  strSQL = sql_bundle.getString("downloadMerchant5SubAgent");	
        	  cat.debug("downloadMerchant5SubAgent");     
          	
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              
              /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
 			 if(blnUSEAdditionalFields) 
 				{
 					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
 					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
 				}else{
 				   
 					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
 					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
 					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);	   
 				        			
 				} 
 			 /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/              
            
            if ( !this.millennium_no.equals("") )
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
            }
            else
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", "");
            }
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	}

      		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
          strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setDouble(3, Double.parseDouble(strRefId));
            pstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.REP))
          {
        	  
        	  strSQL = sql_bundle.getString("downloadMerchantRep");
        	  cat.debug("downloadMerchantRep");     
            	
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              
              /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
  			 if(blnUSEAdditionalFields) 
  				{
  					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  				}else{
  				   
  					//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
  					strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  					strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);	   
  				        			
  				} 
  			 /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/                
            
            if ( !this.millennium_no.equals("") )
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
            }
            else
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", "");
            }
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	}

      		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
          strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setDouble(2, Double.parseDouble(strRefId));
            pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
          {
        	this.merchant_id = sessionData.getProperty("ref_id");
            
            
        	strSQL = sql_bundle.getString("downloadMerchant");
        	cat.debug("downloadMerchant");     
                
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);

            if ( !this.millennium_no.equals("") )
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
            }
            else
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", "");
            }
			if (blnUSEAdditionalFields)
			{
							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
							/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
							/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
							
						}
						else
						{
							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
							/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
							/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    
						}

			if (this.merchant_ids != null && this.merchant_ids.length() > 0)
						{
							strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
						}
    		if (this.transactionID != null && !this.transactionID.equals(""))
			{
							strSQL += " AND wt.rec_id = " + this.transactionID + " ";
						}
    		if (this.invoiceID != null && !this.invoiceID.equals(""))
			{
							strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
						}
    		
    	    if (this.PINNumber != null && !this.PINNumber.equals(""))
			{
							ArrayList<String> skusWithEncryption = null;
							skusWithEncryption = getSkusWithEncryption(dbConn);
							strSQL += " AND wt.product_trans_type = 2 ";
							strSQL += " AND wt.amount > 0 ";
							// pin encryption: protect
							String pin = this.PINNumber;
							String sku = getSkuFromPins(pin, dbConn);
							if (skusWithEncryption.contains(sku))
							{
								pin = dataProtection().t0(pin);
							}
							strSQL += " AND t.pin = '" + pin + "' ";
						}

      		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
						strSQL += " ORDER BY wt.rec_id";
	          
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
          }
          break;
          //agents_transactions.jsp
        case 50:
        	
        	if (strAccessLevel.equals(DebisysConstants.CARRIER))
          {
        	  
        		strSQL = sql_bundle.getString("downloadCarrierAgentPINACH");
        		cat.debug("downloadCarrierAgentPINACH");
        		
    	          
        		strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
        		strSQL = strSQL.replaceAll("_REFID_", strRefId);

        		//Added by Jacuna. R25 DBSY-525
        		if(blnUSEAdditionalFields) 
        		{
        			strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
        			strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
        		}else{
        			/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
        			//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
        			strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
        			strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
        			/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/      			
        		}
  			//END. Added by Jacuna. R25 DBSY-525         
  			if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
  			    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
  			}
			if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))
			{
				strSQL += " AND wt.id IN (";
				strSQL += "SELECT p.id FROM Products p WITH (NOLOCK) INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.carrier_id = rc.CarrierID ";
				strSQL += "WHERE rc.RepID = " + sessionData.getUser().getRefId() + ")";
			}
  		      strSQL += " ORDER BY wt.rec_id";
				strSQL = strSQL.replaceAll("_STARTDATE_", vTimeZoneFilterDates.get(0).toString());
  				strSQL = strSQL.replaceAll("_ENDDATE_", vTimeZoneFilterDates.get(1).toString());
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setLong(1, Long.parseLong(this.rep_id));
                          
              
            }
          else if (strAccessLevel.equals(DebisysConstants.ISO))
          {
        	  
        	  strSQL = sql_bundle.getString("downloadAgentISOPINACH");
        	  cat.debug("downloadAgentISOPINACH");
          	  
  	          
        	  strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
        	  strSQL = strSQL.replaceAll("_REFID_", strRefId);

			//Added by Jacuna. R25 DBSY-525
			if(blnUSEAdditionalFields) 
			{
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
			}else{
			    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
			    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/      			
			}
			//END. Added by Jacuna. R25 DBSY-525         
			if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
			    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
			}
			if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
		      strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
            pstmt.setDouble(4, Double.parseDouble(this.rep_id));
            pstmt.setDouble(5, Double.parseDouble(strRefId));
            pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
        	  
            strSQL = sql_bundle.getString("downloadAgent");
            cat.debug("downloadAgent");
          	  
        	  
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	}		if(strCarrierUserProds.length() > 0)
				strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
			else // return no records.. since no products attached
				strSQL += " AND wt.id =\"\" ";

			if (blnUSEAdditionalFields)
						{
							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
						    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
						    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
						}
						else
						{
							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
						    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/							
							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
						    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/							
						}

						if (this.transactionID != null && !this.transactionID.equals(""))
						{
							strSQL += " AND wt.rec_id = " + this.transactionID + " ";
						}
						if (this.invoiceID != null && !this.invoiceID.equals(""))
						{
							strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
						}
						
						if (this.PINNumber != null && !this.PINNumber.equals(""))
						{
							ArrayList<String> skusWithEncryption = null;
							skusWithEncryption = getSkusWithEncryption(dbConn);
							strSQL += " AND wt.product_trans_type = 2 ";
							strSQL += " AND wt.amount > 0 ";
							// pin encryption: protect
							String pin = this.PINNumber;
							String sku = getSkuFromPins(pin, dbConn);
							if (skusWithEncryption.contains(sku))
							{
								pin = dataProtection().t0(pin);
							}
							strSQL += " AND t.pin = '" + pin + "' ";
						}

          strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(3, Double.parseDouble(strRefId));
            pstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
          }

          break;
          //subagents_transactions.jsp
        case 51:
        	if (strAccessLevel.equals(DebisysConstants.CARRIER))
          {
        	  
        		strSQL = sql_bundle.getString("downloadCarrierSubAgentPINACH");
        		cat.debug("downloadCarrierSubAgentPINACH");
        		
        		strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
        		strSQL = strSQL.replaceAll("_REFID_", strRefId);

  			//Added by Jacuna. R25 DBSY-525
  			if(blnUSEAdditionalFields) 
  			{
  				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  			}else{
  			    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
  				//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
  				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
  				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
  			    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/     			
  			}
  			//END. Added by Jacuna. R25 DBSY-525
  			if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
  			    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
  			}
			if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))
			{
				strSQL += " AND wt.id IN (";
				strSQL += "SELECT p.id FROM Products p WITH (NOLOCK) INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.carrier_id = rc.CarrierID ";
				strSQL += "WHERE rc.RepID = " + sessionData.getUser().getRefId() + ")";
			}
  		      strSQL += " ORDER BY wt.rec_id";
				strSQL = strSQL.replaceAll("_STARTDATE_", vTimeZoneFilterDates.get(0).toString());
  				strSQL = strSQL.replaceAll("_ENDDATE_", vTimeZoneFilterDates.get(1).toString());
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Long.parseLong(this.rep_id));
            
          }
          else if (strAccessLevel.equals(DebisysConstants.ISO))
          {
        	  
            	  strSQL = sql_bundle.getString("downloadSubAgentISOPINACH");
        		  cat.debug("downloadSubAgentISOPINACH");
          	  
            cat.debug("downloadSubAgentISOPINACH");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);

			//Added by Jacuna. R25 DBSY-525
			if(blnUSEAdditionalFields) 
			{
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
			}else{
			    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				//strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);	   
			    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/     			
			}
			//END. Added by Jacuna. R25 DBSY-525
			if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
			if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
			    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
			}
		      strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(3, Double.parseDouble(this.rep_id));
            pstmt.setInt(4, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
            pstmt.setDouble(5, Double.parseDouble(strRefId));
            pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
        	  
            strSQL = sql_bundle.getString("downloadSubAgentAgent");
            cat.debug("downloadSubAgentAgent");
          	  
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
    		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	}
            /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			if(blnUSEAdditionalFields) 
			{
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
			}else{
			    //strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", "");
				strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
				strSQL=strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);	   
			}
			/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			
          strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(3, Double.parseDouble(this.rep_id));
            pstmt.setDouble(4, Double.parseDouble(strRefId));
            pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
          {
        	  
            strSQL = sql_bundle.getString("downloadSubAgent");
            cat.debug("downloadSubAgent");
          	  
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
    		if(strCarrierUserProds.length() > 0)
					strSQL += " AND wt.id in (" + strCarrierUserProds + ") ";
				else // return no records.. since no products attached
					strSQL += " AND wt.id =\"\" ";
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
    	    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
    	}

			if (blnUSEAdditionalFields)
						{
							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", str_ADDITIONAL_JOINS_);
							/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
							/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
						}
						else
						{
							strSQL = strSQL.replaceAll("_ADDITIONAL_JOINS_", "");
							/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
							strSQL=strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
							/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/							
						}

						if (this.transactionID != null && !this.transactionID.equals(""))
						{
							strSQL += " AND wt.rec_id = " + this.transactionID + " ";
						}

						if (this.invoiceID != null && !this.invoiceID.equals(""))
						{
							strSQL += " AND wt.invoice_no = " + this.invoiceID + " ";
						}
						
						if (this.PINNumber != null && !this.PINNumber.equals(""))
						{
							ArrayList<String> skusWithEncryption = null;
							skusWithEncryption = getSkusWithEncryption(dbConn);
							strSQL += " AND wt.product_trans_type = 2 ";
							strSQL += " AND wt.amount > 0 ";
							// pin encryption: protect
							String pin = this.PINNumber;
							String sku = getSkuFromPins(pin, dbConn);
							if (skusWithEncryption.contains(sku))
							{
								pin = dataProtection().t0(pin);
							}
							strSQL += " AND t.pin = '" + pin + "' ";
						}

						strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setDouble(2, Double.parseDouble(strRefId));
            pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
          }
          break;

        default:
          cat.error("Error during download");
          throw new TransactionException();
      }

      cat.debug(strSQL);
      long startQuery = System.currentTimeMillis();
      ResultSet rs = pstmt.executeQuery();
            
      long endQuery = System.currentTimeMillis();
      cat.debug("Download Query: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
      startQuery = System.currentTimeMillis();

      strFileName = this.generateFileName(context);    

	File file = new File(workingDir);
	if(!file.exists()) {
		file.mkdir();
	}
	
      FileChannel fc = 
          new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw")
      .getChannel();
     // BufferedWriter outputFile = null;
      try
      {
       // outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
        cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
        //outputFile.flush();

        Date dateReportTransactions = new Date();
        String actualDate;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        formatter.setLenient(false);
        actualDate = formatter.format(dateReportTransactions);
        
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.namereportexcel",sessionData.getLanguage())+"\"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.generatedby",sessionData.getLanguage())+"\","+sessionData.getUser().getUsername()+"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.generationdate",sessionData.getLanguage())+"\","+actualDate+"\n");
        this.writetofile(fc,"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.filtersapplied",sessionData.getLanguage())+"\"\n");
        if (sessionData.getProperty("repName") != null)
				{
					this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name",sessionData.getLanguage()) + "\"," + sessionData.getProperty("repName") + "\n");
				}
				else
				{
					this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name",sessionData.getLanguage()) + "\"," + sessionData.getUser().getCompanyName() + "\n");
				}

        this.writetofile(fc,"\""+Languages.getString("jsp.admin.start_date",sessionData.getLanguage())+"\"," +this.start_date+"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.end_date",sessionData.getLanguage())+"\"," +this.end_date+"\n");
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.reports.transaction_id",sessionData.getLanguage())+"\"," +this.transactionID+"\n");
        if(invoicePermission){
        	this.writetofile(fc,"\""+Languages.getString("jsp.admin.reports.invoiceno",sessionData.getLanguage())+"\","+this.invoiceID +"\n");
        }
        this.writetofile(fc,"\""+Languages.getString("jsp.admin.reports.pinreturn_search.pin",sessionData.getLanguage())+"\","+this.PINNumber +"\n");
        if(!strAccessLevel.equals(DebisysConstants.MERCHANT)){
        	if(this.merchant_ids != null && !this.merchant_ids.equals("")){
        		this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.merchants",sessionData.getLanguage())+"\","+this.merchant_ids+"\n");
        	}else{
        		this.writetofile(fc,"\""+Languages.getString("jsp.admin.transactions.merchants",sessionData.getLanguage())+"\","+Languages.getString("jsp.admin.reports.all",sessionData.getLanguage())+"\n");
        	}
        		
        }
        this.writetofile(fc,"\n");	
        	
       
        
        //*****************************************//
        //Begin add by nmartinez.R30. DBSY-815
        this.defineTitlesForDownload(sessionData, intSectionPage, context,deploymentType, customConfigType, strAccessLevel,blnUSEAdditionalFields, showAdditionalDataColumn,invoicePermission, fc);
        this.icount=1;
        //End add by nmartinez.R30. DBSY-815
        //*****************************************//
        
        Hashtable<Integer, String> ht_taxTypes = TaxTypes.getTaxTypes();
        
        while (rs.next())
        {
          if( (this.max < this.icount) && this.max!=-1)
        	break;

      	  this.writetofile(fc,"\"" + this.icount + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("rec_id")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("millennium_no")) + "\",");
		  
		  
		  if (customConfigType.equalsIgnoreCase(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		  {	
			if ((intSectionPage == 3) || (intSectionPage == 14))
			{
				this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("trm_description")) + "\",");
			}
		  }
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("dba")) + "\",");
          this.writetofile(fc,"\""  +StringUtil.toString(rs.getString("merchant_id")) + "\",");
          this.writetofile(fc,"\"" + DateUtil.formatSqlDateTime(rs.getTimestamp("datetime")) + "\",");
          
          
          if((intSectionPage == 2) || (intSectionPage == 3) || (intSectionPage == 14) || (intSectionPage == 5) || (intSectionPage == 6))
          {
              if (deploymentType.equalsIgnoreCase(DebisysConstants.DEPLOYMENT_DOMESTIC))
              {
            	  this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("phys_state")) + "\",");
              }
          }
          
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("phys_city")) +"\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("phys_county"))+ "\",");
          
          if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals(""))
          {
        	  this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("clerk_name")) + "\",");
          }
          else
          {
        	  this.writetofile(fc,"\"N/A\",");
          }
          
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("ani")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("password")) + "\",");
          this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("amount")) + "\",");
          /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
          if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
        	  
        	  if(strAccessLevel.equals(DebisysConstants.CARRIER)){
                	this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("iso_commission")) + "\",");
                     this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("agent_commission")) + "\",");
                     this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("subagent_commission")) + "\",");
                     this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
                		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");         
              }
        	  else if(strAccessLevel.equals(DebisysConstants.ISO)){
          	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
          		 this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("iso_commission")) + "\",");
          		 this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
          		 this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");
           	 }
           	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
           		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("iso_commission")) + "\",");
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("agent_commission")) + "\",");
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("subagent_commission")) + "\",");
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
           		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");         
           	 }
           	}
           	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("agent_commission")) + "\",");
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("subagent_commission")) + "\",");
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
           		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");  
           	}
           	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("subagent_commission")) + "\",");
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
           		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");  
           	}
           	else if (strAccessLevel.equals(DebisysConstants.REP)){
                this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
           		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");  
           	}
           	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
           		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");  
           	}
          }  
          
          /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/          
          
          // here should go bonus & total recharge!!!
          // cat.debug("Setting bonus & totalRecharge: bonus=" + rs.getString("bonus_amount"));
          if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
          {        	  
        	  double totalRecharge = rs.getDouble("amount") + rs.getDouble("bonus_amount");
        	  //cat.debug("we seem to be overseas, so let's print bonus and totalrecharge=" + totalRecharge);
        	  this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("bonus_amount")) + "\",");
        	  this.writetofile(fc,"\"" + NumberUtil.formatAmount("" + totalRecharge) + "\",");
          }
          
          this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("balance")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("id")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("description")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("control_no")) + "\",");
          
        //Added by Jacuna. R25 DBSY-525
		if(blnUSEAdditionalFields) 
		{
				//sourcePage=this.
				
				if(sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
					if(StringUtil.toString(rs.getString("product_trans_type")).equals("2") && Double.parseDouble(StringUtil.toString(rs.getString("amount")).replaceAll(",", "")) > 0)
						this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("pin")) + "\",");
					else 
						this.writetofile(fc,"\"N/A\",");          
				}
				
				if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{ 			
					this.writetofile(fc,"\"" + ((rs.getTimestamp("ach_datetime")!=null)? DateUtil.formatDateTime(rs.getTimestamp("ach_datetime")):"TBD") + "\",");
				}
		}
		//END.Added by Jacuna. R25 DBSY-525
		
	      if (customConfigType.equalsIgnoreCase(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) 
          {
        	  	String other_info = StringUtil.toString(rs.getString("other_info"));
	  	        if(other_info.trim() != "")
	  	        {
	  	        	if(other_info.length() > 6)
	  	        	{
	  	        		other_info = other_info.substring((other_info.length()-6), other_info.length());
	  	        	}
	  	        }

	            this.writetofile(fc,"\"" + StringUtil.toString(other_info) + "\",");
          }
          if (rs.getString("transaction_type") != null && !rs.getString("transaction_type").equals(""))
          {
            this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("transaction_type")) + "\",");
          }
          else
          {
            this.writetofile(fc,"");
          }
          
          
          if(com.debisys.users.User.isInvoiceNumberEnabled(sessionData, context)
          			&& DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
          			&& DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
          {
        	  this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("invoice_no")) + "\"");
  		  }
          
          this.writetofile(fc,"\r\n");
          this.icount++;
        }
        fc.close();
        pstmt.close();
        rs.close();
        endQuery = System.currentTimeMillis();
        cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
        startQuery = System.currentTimeMillis();
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        cat.debug("Zip File Name: " + zipFileName);
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
        cat.debug("Download URL: " + downloadUrl);
        
        try
        {

          ZipOutputStream out =
              new ZipOutputStream(new FileOutputStream(zipFileName));

          // Set the compression ratio
          out.setLevel(Deflater.DEFAULT_COMPRESSION);
          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

          // Add ZIP entry to output stream.
          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

          // Transfer bytes from the current file to the ZIP file
          //out.write(buffer, 0, in.read(buffer));

          int len;
          while ((len = in.read(buffer)) > 0)
          {
            out.write(buffer, 0, len);
          }

          // Close the current entry
          out.closeEntry();
          // Close the current file input stream
          in.close();
          out.close();
        }
        catch (IllegalArgumentException iae)
        {
          iae.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
          fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
        sourceFile.delete();
        endQuery = System.currentTimeMillis();
        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");


      }
      catch (IOException ioe)
      {
	  // break radio silence about transaction report downloads!
	  cat.error("Download transactions failed! " + ioe.getClass().getName() 
		  + " (" + ioe.getMessage() + ")", ioe);
        try
        {
          if (fc != null)
          {
        	  fc.close();
          }
        }
        catch (IOException ioe2)
        {
        }
      }
    }
    catch (Exception e)
    {
      cat.error("Error during download", e);
      throw new TransactionException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    long end = System.currentTimeMillis();

    // Display the elapsed time to the standard output
    cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
    this.isdone = true;
    return downloadUrl;
  }
  private void writetofile(FileChannel fc,String c) throws IOException{
      fc.write(ByteBuffer.wrap(c.getBytes()));
  }
  
  	 
  	 
  /**
     * General-purpose download method for transaction reports. It relies on the original 
     * underlying query (specifically in the filters used) since it uses data obtained in a
     * call to {@link #searchAll(SessionData, String, String, ResultSet)} 
     * @param data Vector<Vector<String>> 
     * @param intSectionPage
     *                sectionPage parameter
     * @param context
     *                ServletContext with valuable parameters for the export
     *                operation
     * @return downloadURL to response.
     */
    public String download(Vector<Vector<String>> data, String markers, int intSectionPage, 
	    ServletContext context) {
	long start = System.currentTimeMillis();
	String strFileName = null;
	BufferedWriter outputFile = null;
	String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
	String downloadPath = DebisysConfigListener.getDownloadPath(context);
	String workingDir = DebisysConfigListener.getWorkingDir(context);
	String filePrefix = DebisysConfigListener.getFilePrefix(context);
	try {
	    strFileName = this.generateFileName(context);
	    outputFile = new BufferedWriter(new FileWriter(workingDir
		    + filePrefix + strFileName + ".csv"));
	    cat.debug("Temp File Name: " + workingDir + filePrefix
		    + strFileName + ".csv");
	    outputFile.flush();
	    String fileContents = MarkedCollection.dataSetAsCSV(data, markers);
	    outputFile.write(fileContents);
	    outputFile.flush();
	    outputFile.close();

	    long end = System.currentTimeMillis();
	    cat.debug("File Out: " + (((end - start) / 1000.0)) + " seconds");
	    start = System.currentTimeMillis();
	    File sourceFile = new File(workingDir + filePrefix + strFileName
		    + ".csv");
	    long size = sourceFile.length();
	    byte[] buffer = new byte[(int) size];
	    String zipFileName = downloadPath + filePrefix + strFileName
		    + ".zip";
	    cat.debug("Zip File Name: " + zipFileName);
	    downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
	    cat.debug("Download URL: " + downloadUrl);

	    try {
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
			zipFileName));

		// Set the compression ratio
		out.setLevel(Deflater.DEFAULT_COMPRESSION);
		FileInputStream in = 
		    new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

		// Add ZIP entry to output stream.
		out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

		// Transfer bytes from the current file to the ZIP file
		// out.write(buffer, 0, in.read(buffer));

		int len;
		while ((len = in.read(buffer)) > 0) {
		    out.write(buffer, 0, len);
		}

		// Close the current entry
		out.closeEntry();
		// Close the current file input stream
		in.close();
		out.close();
	    } catch (Exception e) {
		cat.error("Error in zip operation! " + e.getClass().getName()
			+ " (" + e.getMessage() + ")");
	    }
	    sourceFile.delete();
	    end = System.currentTimeMillis();
	    cat.debug("Zip Out: " + (((end - start) / 1000.0)) + " seconds");
	} catch (IOException ioe) {
	    cat.error("Download transactions failed! "
		    + ioe.getClass().getName() + " (" + ioe.getMessage() + ")", ioe);
	    try {
		if (outputFile != null) {
		    outputFile.close();
		}
	    } catch (IOException ioe2) {}
	}
	return downloadUrl;
    }
    
    

  private String generateRandomNumber()
  {
    StringBuffer s = new StringBuffer();
    // number between 1-9 because first digit must not be 0
    int nextInt = (int) ((Math.random() * 9) + 1);
    s = s.append(nextInt);

    for (int i = 0; i <= 10; i++)
    {
      //number between 0-9
      nextInt = (int) (Math.random() * 10);
      s = s.append(nextInt);
    }

    return s.toString();
  }

  private boolean checkFileName(String strFileName, ServletContext context)
  {
    boolean isUnique = true;
    String downloadPath = DebisysConfigListener.getDownloadPath(context);
    String workingDir = DebisysConfigListener.getWorkingDir(context);
    String filePrefix = DebisysConfigListener.getFilePrefix(context);

    File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
    if (f.exists())
    {
      //duplicate file found
      cat.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
      return false;
    }

    f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
    if (f.exists())
    {
      //duplicate file found
      cat.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
      return false;
    }

    return isUnique;
  }

  public String generateFileName(ServletContext context)
  {
    boolean isUnique = false;
    String strFileName = "";
    while (!isUnique)
    {
      strFileName = this.generateRandomNumber();
      isUnique = this.checkFileName(strFileName, context);
    }
    return strFileName;
  }


  public void valueBound(HttpSessionBindingEvent event)
  {
  }

  public void valueUnbound(HttpSessionBindingEvent event)
  {
  }

  public static void main(String[] args)
  {
  }


 
  public String downloadMx(SessionData sessionData, int intSectionPage, ServletContext context)
      throws TransactionException
  {
	this.isdone = false;
    long start = System.currentTimeMillis();
    Connection dbConn = null;
    String strSQL = "";
    String strFileName = "";
    //String strIdSQL = "";
    double dTax = 1 + (DebisysConfigListener.getMxValueAddedTax(context) / 100);

    String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
    String downloadPath = DebisysConfigListener.getDownloadPath(context);
    String workingDir = DebisysConfigListener.getWorkingDir(context);
    String filePrefix = DebisysConfigListener.getFilePrefix(context);
    String deploymentType = DebisysConfigListener.getDeploymentType(context);
    String customConfigType = DebisysConfigListener.getCustomConfigType(context);
    String strRefId = sessionData.getProperty("ref_id");
    String strAccessLevel = sessionData.getProperty("access_level");
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    PreparedStatement pstmt = null;
    
    String str_ADDITIONAL_FIELDS_ = "";
    
    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
    
    str_ADDITIONAL_FIELDS_ += ", wt.merchant_rate * wt.amount / 100 as merchant_commission, "
    					+" wt.rep_rate * wt.amount / 100 as rep_commission, "
    					+" wt.agent_rate * wt.amount / 100 as agent_commission, "
    					+" wt.subagent_rate * wt.amount / 100 as subagent_commission, "
    					+" wt.iso_rate * wt.amount / 100 as iso_commission ";
       
    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/

    boolean invoicePermission = com.debisys.users.User.isInvoiceNumberEnabled(sessionData, context) && 
									DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
											DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
    
    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new TransactionException();
      }

      Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date + " 23:59:59.999", false);

      //get a count of total matches
      switch (intSectionPage)
      {
        //transactions.jsp
        case 1:
          if (strAccessLevel.equals(DebisysConstants.ISO))
          {

            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
            {
              strSQL = sql_bundle.getString("downloadISO3");
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(strRefId));
              pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());

            }
            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
              strSQL = sql_bundle.getString("downloadISO5");
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
              pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
              pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
              pstmt.setDouble(4, Double.parseDouble(strRefId));
              pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
            }
          }
          break;
          //rep_transactions.jsp
        case 2:
          if (strAccessLevel.equals(DebisysConstants.ISO))
          {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
            {
              strSQL = sql_bundle.getString("downloadRep3Mx");
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              strSQL = strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
              
              if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
              strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.rep_id));
              pstmt.setDouble(2, Double.parseDouble(strRefId));
              pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
            }
            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
              strSQL = sql_bundle.getString("downloadRep5Mx");
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              strSQL= strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
              
              if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
              strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.rep_id));
              pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
              pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
              pstmt.setInt(4, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
              pstmt.setDouble(5, Double.parseDouble(strRefId));
              pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());

            }
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
            strSQL = sql_bundle.getString("downloadRep5AgentMx");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
            strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.rep_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(4, Double.parseDouble(strRefId));
            pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());

          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
          {
            strSQL = sql_bundle.getString("downloadRep5SubAgentMx");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
            strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.rep_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setDouble(3, Double.parseDouble(strRefId));
            pstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.REP))
          {
            strSQL = sql_bundle.getString("downloadRepMx");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
            strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(strRefId));
            pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
          }
          break;
          //merchant_transactions.jsp
        case 3:
        case 14:        
          if (strAccessLevel.equals(DebisysConstants.ISO))
          {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
            {
              strSQL = sql_bundle.getString("downloadMerchant3Mx");
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
              strSQL= strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
              
              if ( !this.millennium_no.equals("") )
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
              }
              else
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", "");
              }
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
              pstmt.setDouble(2, Double.parseDouble(strRefId));
              pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
            }
            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
              strSQL = sql_bundle.getString("downloadMerchant5Mx");
              strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
              strSQL = strSQL.replaceAll("_REFID_", strRefId);
              strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
              strSQL= strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
              if ( !this.millennium_no.equals("") )
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
              }
              else
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", "");
              }
              pstmt = dbConn.prepareStatement(strSQL);
              pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
              pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
              pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
              pstmt.setInt(4, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
              pstmt.setDouble(5, Double.parseDouble(strRefId));
              pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
              pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());
            }
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
            strSQL = sql_bundle.getString("downloadMerchant5AgentMx");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
            if ( !this.millennium_no.equals("") )
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
            }
            else
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", "");
            }
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(4, Double.parseDouble(strRefId));
            pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());

          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
          {
            strSQL = sql_bundle.getString("downloadMerchant5SubAgentMx");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
            if ( !this.millennium_no.equals("") )
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
            }
            else
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", "");
            }
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setDouble(3, Double.parseDouble(strRefId));
            pstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.REP))
          {
            strSQL = sql_bundle.getString("downloadMerchantRepMx");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
            strSQL= strSQL.replaceAll("_ADDITIONAL_FIELDS_", str_ADDITIONAL_FIELDS_);
            if ( !this.millennium_no.equals("") )
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
            }
            else
            {
          	  strSQL = strSQL.replaceAll("_SITEID_", "");
            }
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setDouble(2, Double.parseDouble(strRefId));
            pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
          {
        	  this.merchant_id = sessionData.getProperty("ref_id");
              if ( !this.millennium_no.equals("") )
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", " AND wt.millennium_no in (" + this.millennium_no + ") ");
              }
              else
              {
            	  strSQL = strSQL.replaceAll("_SITEID_", "");
              }
            strSQL = sql_bundle.getString("downloadMerchantMx");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchant_id));
            pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
          }
          break;
          //agents_transactions.jsp
        case 5:
          if (strAccessLevel.equals(DebisysConstants.ISO))
          {
            strSQL = sql_bundle.getString("downloadAgentISO");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
            pstmt.setDouble(4, Double.parseDouble(this.rep_id));
            pstmt.setDouble(5, Double.parseDouble(strRefId));
            pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
            strSQL = sql_bundle.getString("downloadAgent");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		    
		    strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") "; 
		}
	      strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(3, Double.parseDouble(strRefId));
            pstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
          }

          break;
          //subagents_transactions.jsp
        case 6:
          if (strAccessLevel.equals(DebisysConstants.ISO))
          {
            strSQL = sql_bundle.getString("downloadSubAgentISO");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		
        	strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
            }
            strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(3, Double.parseDouble(this.rep_id));
            pstmt.setInt(4, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
            pstmt.setDouble(5, Double.parseDouble(strRefId));
            pstmt.setString(6, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(7, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.AGENT))
          {
            strSQL = sql_bundle.getString("downloadSubAgentAgent");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		
        	strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
            }
            strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
            pstmt.setDouble(3, Double.parseDouble(this.rep_id));
            pstmt.setDouble(4, Double.parseDouble(strRefId));
            pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
          }
          else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
          {
            strSQL = sql_bundle.getString("downloadSubAgent");
            strSQL = strSQL.replaceAll("_ACCESSLEVEL_", strAccessLevel);
            strSQL = strSQL.replaceAll("_REFID_", strRefId);
            if (this.merchant_ids != null && this.merchant_ids.length() > 0) {		
        	strSQL += " AND wt.merchant_id in (" + this.merchant_ids + ") ";
            }
            strSQL += " ORDER BY wt.rec_id";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
            pstmt.setDouble(2, Double.parseDouble(strRefId));
            pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
            pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
          }
          break;

        default:
          cat.error("Error during download");
          throw new TransactionException();
      }
      cat.debug(strSQL);
      long startQuery = System.currentTimeMillis();
      ResultSet rs = pstmt.executeQuery();
      long endQuery = System.currentTimeMillis();
      cat.debug("Download Query: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
      startQuery = System.currentTimeMillis(); 
      
      strFileName = this.generateFileName(context);    
      FileChannel fc = 
          new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw")
      .getChannel();

      try
      {
    	
        //outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
        cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
        
        
        //********************************************//
        //DBSY-815 R30 Jorge Nicol�s Mart�nez Romero
        //All report headers should be exportable
        this.defineTitlesForDownload(sessionData, intSectionPage, context,deploymentType, customConfigType, strAccessLevel,false, false,invoicePermission, fc);
        this.icount=1;
        //End DBSY-815 R30 Jorge Nicol�s Mart�nez Romero
        //********************************************//
           
        while (rs.next())
        {
          if( (this.max < this.icount) && this.max!=-1)
        	  break;
          
          this.writetofile(fc,"\"" + this.icount + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("rec_id")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("millennium_no")) + "\",");
          if ((intSectionPage == 3) || (intSectionPage == 14))
			{
				this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("trm_description")) + "\",");
			}
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("dba")) + "\",");
          this.writetofile(fc,StringUtil.toString(rs.getString("merchant_id")) + ",");
          this.writetofile(fc,"\"" + DateUtil.formatSqlDateTime(rs.getTimestamp("datetime")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("phys_city")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("phys_county")) + "\",");
	          
            if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals(""))
            {
              this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("clerk_name")) + "\",");
            }
            else
            {
	        	  this.writetofile(fc,"\"N/A\",");
            }
	          
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("ani")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("password")) + "\",");
          this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("amount")) + "\",");
          
   	  
          /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/    	  
    	  if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {	        	  
	           	if(strAccessLevel.equals(DebisysConstants.ISO)){
	          	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("iso_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("iso_commission")) + "\",");
	           	 }
	           	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
		          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("iso_commission")) + "\",");
		          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("agent_commission")) + "\",");
		          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("subagent_commission")) + "\",");
		          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
		          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");
	           	 }
	           	}
	           	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("agent_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("subagent_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");
	           	}
	           	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("subagent_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");
	           	}
	           	else if (strAccessLevel.equals(DebisysConstants.REP)){
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("rep_commission")) + "\",");
	          		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");
	           	}
	           	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
	           		this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("merchant_commission")) + "\",");
	           	}
	          }  
    	  /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
          
          this.writetofile(fc,"\"" + NumberUtil.formatAmount(rs.getString("balance")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("id")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("description")) + "\",");
          this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("control_no")) + "\",");
          
          if (customConfigType.equalsIgnoreCase(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) 
          {
        	  	String other_info = StringUtil.toString(rs.getString("other_info"));
	  	        if(other_info.trim() != "")
	  	        {
	  	        	if(other_info.length() > 6)
	  	        	{
	  	        		other_info = other_info.substring((other_info.length()-6), other_info.length());
	  	        	}
	  	        }

	            this.writetofile(fc,"\"" + StringUtil.toString(other_info) + "\",");
          }
          if (rs.getString("transaction_type") != null && !rs.getString("transaction_type").equals(""))
          {
            this.writetofile(fc,"\"" + StringUtil.toString(rs.getString("transaction_type")) + "\"");
          }
          else
          {
            this.writetofile(fc,"");
          }
          this.writetofile(fc,"\r\n");
          this.icount++;
        }
        fc.close();
        pstmt.close();
        rs.close();
        endQuery = System.currentTimeMillis();
        cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
        startQuery = System.currentTimeMillis();
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        cat.debug("Zip File Name: " + zipFileName);
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
        cat.debug("Download URL: " + downloadUrl);

        try
        {
          ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

          // Set the compression ratio
          out.setLevel(Deflater.DEFAULT_COMPRESSION);
          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

          // Add ZIP entry to output stream.
          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

          // Transfer bytes from the current file to the ZIP file
          //out.write(buffer, 0, in.read(buffer));

          int len;
          while ((len = in.read(buffer)) > 0)
          {
            out.write(buffer, 0, len);
          }

          // Close the current entry
          out.closeEntry();
          // Close the current file input stream
          in.close();
          out.close();
        }
        catch (IllegalArgumentException iae)
        {
          iae.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
          fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
        sourceFile.delete();
        endQuery = System.currentTimeMillis();
        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");


      }
      catch (IOException ioe)
      {
	    cat.error("[downloadMx] IOException 1: ", ioe);
        try
        {
          if (fc != null)
          {
            fc.close();
          }
        }
        catch (IOException ioe2)
        {
        	cat.error("[downloadMx] IOException 2: ", ioe2);
        }
      }

    }
    catch (Exception e)
    {
      cat.error("[downloadMx] Error during download: ", e);
      throw new TransactionException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    long end = System.currentTimeMillis();

    // Display the elapsed time to the standard output
    cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
    this.isdone = true;
    return downloadUrl;
  }

//combines transactions vector with pins and cards
//needed because the database join takes too long
  private Vector combineResultsMx(Vector vecTransactions, String strControlNo, ServletContext context)
    throws TransactionException
  {
    long start = System.currentTimeMillis();
    Connection dbConn = null;
    String strSQL = "";
    PreparedStatement pstmt;
    double dTax = 1 + (DebisysConfigListener.getMxValueAddedTax(context) / 100);

    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new TransactionException();
      }

      //get all related cards and pins load a hash and update the return vector.
      //load the hash
      Hashtable hashPinsCards = new Hashtable();
      strSQL = "";
      strSQL = sql_bundle.getString("getCardProducts");
      strControlNo = strControlNo.substring(0, (strControlNo.length() - 1));
      strSQL = strSQL + " ( " + strControlNo + ") ";
      strSQL = strSQL + "UNION " + sql_bundle.getString("getPinProducts");
      strSQL = strSQL + " ( " + strControlNo + ") ";
      cat.debug(strSQL);
      pstmt = dbConn.prepareStatement(strSQL);

      long startQuery = System.currentTimeMillis();
      ResultSet rs = pstmt.executeQuery();
      long endQuery = System.currentTimeMillis();
      cat.debug("Combine Query: " + (((endQuery - startQuery) / 1000.0)) + " seconds");

      while (rs.next())
      {
        Vector vecTemp = new Vector();
        vecTemp.add(StringUtil.toString(rs.getString("ani")));
        vecTemp.add(StringUtil.toString(rs.getString("password")));
        vecTemp.add(StringUtil.toString(rs.getString("balance")));
        vecTemp.add(StringUtil.toString(rs.getString("id")));
        vecTemp.add(StringUtil.toString(rs.getString("description")));
        vecTemp.add(StringUtil.toString(rs.getString("trans_type")));
        hashPinsCards.put(rs.getString("control_no"), vecTemp);
      }
      pstmt.close();
      rs.close();

      //update the return vector
      for (int i = 1; i < vecTransactions.size(); i++)
      {
        //Vector vecPinsCards = new Vector();
        //Vector vecTemp = new Vector();
        Vector vecTemp = (Vector) vecTransactions.elementAt(i);
        //control_no
        strControlNo = vecTemp.get(12).toString();
        Vector vecPinsCards = (Vector) hashPinsCards.get(strControlNo);
        //ani
        //password
        if (vecPinsCards == null)
        {
          vecTemp.setElementAt("", 7);
          vecTemp.setElementAt("", 8);
          vecTemp.setElementAt("", 10);
          vecTemp.setElementAt("", 11);
          vecTemp.setElementAt("", 13);

        }
        else 
        {
          vecTemp.setElementAt(vecPinsCards.get(0).toString(), 7);
          vecTemp.setElementAt(vecPinsCards.get(1).toString(), 8);
          //balance
          if (vecPinsCards.get(2) == null || vecPinsCards.get(2).toString().equals(""))
          {
            vecTemp.setElementAt(NumberUtil.formatAmount(Double.toString(Double.parseDouble(vecTemp.get(9).toString().replace('$', ' ').replace(',', ' ').replaceAll(" ", "")) * dTax)), 10);
          }
          else
          {
            vecTemp.setElementAt(NumberUtil.formatCurrency(vecPinsCards.get(2).toString()), 10);
          }

          //id / description
          vecTemp.setElementAt(vecPinsCards.get(3).toString() + "/" + vecPinsCards.get(4).toString(), 11);
          vecTemp.setElementAt(vecPinsCards.get(5).toString(), 13);

        }

        vecTransactions.setElementAt(vecTemp, i);
      }

    }
    catch (Exception e)
    {
      cat.error("Error during combineResultsMx", e);
      throw new TransactionException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    long end = System.currentTimeMillis();
    cat.debug("Combine Total: " + (((end - start) / 1000.0)) + " seconds");
    return vecTransactions;

  }
/* END MiniRelease 2.0 - Added by LF */


	/**
	 * Retrieves information of the PIN from specified control_no
	 *
	 * @throws TransactionException Thrown on errors
	 */
	public static Vector getViewPinInfo(String sTransId, SessionData sessionData) throws TransactionException{
		Vector vResult = new Vector();
		Connection cnx = null;
		PreparedStatement ps = null;
		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		try{
			cnx = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cnx == null){
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			ArrayList<String> skusWithEncryption = getSkusWithEncryption(cnx);
			//SELECT t.pin, t.control_no, pr.description, pr.amount, tr.response, p.activation_date
			// FROM reps as r1 (nolock), reps as r2(nolock), reps as r3(nolock), reps as r4(nolock), merchants as m (nolock), pins p(nolock)
			// INNER JOIN products pr (nolock) ON p.product_id = pr.id
			// INNER JOIN transactions t(nolock) ON p.control_no = t.control_no
			// INNER JOIN terminals te (nolock)ON t.account_id = te.millennium_no
			// INNER JOIN terminal_types tt(nolock) ON te.terminal_type = tt.terminal_type
			// LEFT JOIN terminal_responses tr(nolock) ON tt.response_type = tr.type AND pr.id = tr.product_id AND tr.language_option=1
			// WHERE t.rec_id = ? AND te.merchant_id=m.merchant_id AND r4.rep_id=m.rep_id and r4.iso_id=r3.rep_id and r3.iso_id=r2.rep_id and r2.iso_id=r1.rep_id
			String strSql = sql_bundle.getString("getViewPinInfo");
			if (strAccessLevel.equals(DebisysConstants.ISO)){
				strSql = strSql + " AND r1.iso_id= ? ";
			}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
				strSql = strSql + " AND r2.rep_id= ? ";
			}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				strSql = strSql + " AND r3.rep_id= ? ";
			}else if (strAccessLevel.equals(DebisysConstants.REP)){
				strSql = strSql + " AND r4.rep_id= ? ";
			}else{//its a merchant
				strSql = strSql + " AND te.merchant_id= ?";
			}
			cat.debug(strSql + "/* "+ sTransId + ", " + strRefId +" */");
			ps = cnx.prepareStatement(strSql);
			ps.setString(1, sTransId);
			ps.setString(2, strRefId);
			ResultSet rs = ps.executeQuery();
			if ( rs.next() ){
				vResult.add(rs.getString("control_no"));
				// pin encryption: unprotect
				String pin = rs.getString("pin");
				String sku = getSkuFromPins(pin, cnx); 
				if (skusWithEncryption.contains(sku)) {
					pin = dataProtection().r0(pin);
				}
				vResult.add(pin);
				vResult.add(rs.getString("description"));
				vResult.add(rs.getString("amount"));
				vResult.add(StringUtil.formatTerminalResponse(StringUtil.toString(rs.getString("response"))));
				vResult.add(StringUtil.toString(rs.getString("activation_date")));
			}
			rs.close();
			ps.close();
		}catch (Exception e){
			cat.error("Error during getViewPinInfo", e);
			throw new TransactionException();
		}finally{
			try{
				Torque.closeConnection(cnx);
			}catch (Exception e){
				cat.error("Error during closeConnection", e);
			}
		}
		return vResult;
	}//End of function getViewPinInfo
/* END Release 5.0 - Added by LF */

  /*BEGIN Release 10.0 - Added by LF*/
  public static String downloadErrorDetailReport(ServletContext context, SessionData sessionData, Vector<Vector<String>> vData) throws TransactionException
  {
    long start = System.currentTimeMillis();
    String strFileName = "";
    String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
    String downloadPath = DebisysConfigListener.getDownloadPath(context);
    String workingDir = DebisysConfigListener.getWorkingDir(context);
    String filePrefix = DebisysConfigListener.getFilePrefix(context);

    try
    {
      long startQuery = 0;
      long endQuery = 0;
      startQuery = System.currentTimeMillis();
      BufferedWriter outputFile = null;
      try
      {
        strFileName = (new CarrierSearch()).generateFileName(context);
        outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
        cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
        outputFile.flush();

		outputFile.write("\"" + Languages.getString("jsp.admin.reports.date",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.transaction_id",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.amount",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.clerk_id",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.product_id",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.phone",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.term_no",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.type",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.host",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.port",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.duration",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.result",sessionData.getLanguage()) + "\",");
		outputFile.write("\"" + Languages.getString("jsp.admin.reports.provider_response",sessionData.getLanguage()) + "\",");
		
		if(com.debisys.users.User.isInvoiceNumberEnabled(sessionData, context)
				&& DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
				&& DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
			outputFile.write("\"" + Languages.getString("jsp.admin.reports.invoiceno",sessionData.getLanguage()) + "\",");
		}

        outputFile.write("\r\n");
        
        for ( Vector<String> row : vData )
        {
        	for ( String field : row )
        	{
        		outputFile.write("\"" + field + "\",");
        	}
          outputFile.write("\r\n");
        }
        outputFile.flush();
        outputFile.close();
        endQuery = System.currentTimeMillis();
        cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
        startQuery = System.currentTimeMillis();
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        cat.debug("Zip File Name: " + zipFileName);
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
        cat.debug("Download URL: " + downloadUrl);

        try
        {

          ZipOutputStream out =
              new ZipOutputStream(new FileOutputStream(zipFileName));

          // Set the compression ratio
          out.setLevel(Deflater.DEFAULT_COMPRESSION);
          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

          // Add ZIP entry to output stream.
          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

          // Transfer bytes from the current file to the ZIP file
          //out.write(buffer, 0, in.read(buffer));

          int len;
          while ((len = in.read(buffer)) > 0)
          {
            out.write(buffer, 0, len);
          }

          // Close the current entry
          out.closeEntry();
          // Close the current file input stream
          in.close();
          out.close();
        }
        catch (IllegalArgumentException iae)
        {
          iae.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
          fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
        sourceFile.delete();
        endQuery = System.currentTimeMillis();
        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");


      }
      catch (IOException ioe)
      {
        try
        {
          if (outputFile != null)
          {
            outputFile.close();
          }
        }
        catch (IOException ioe2)
        {
        }
      }


    }
    catch (Exception e)
    {
      cat.error("Error during downloadErrorDetailReport", e);
      throw new TransactionException();
    }
    long end = System.currentTimeMillis();

    // Display the elapsed time to the standard output
    cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");

    return downloadUrl;
  }//End of function downloadErrorDetailReport

  public static Vector getIsoList()
  throws TransactionException
  {
	    Vector vecResultList = new Vector();
	    Connection dbConn = null;
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new TransactionException();
	    	}
	    	PreparedStatement pstmt = null;
			String strSQLGetIsoList  = "SELECT rep_id, businessname FROM reps WITH (nolock) WHERE  type in (2,3) ORDER BY businessname";
			cat.debug(strSQLGetIsoList);
			pstmt = dbConn.prepareStatement(strSQLGetIsoList);
			ResultSet rsGetIsoList = pstmt.executeQuery();
			while (rsGetIsoList.next())
			{
				Vector vecTemp = new Vector();
		        vecTemp.add(StringUtil.toString(rsGetIsoList.getString("rep_id")));
		        vecTemp.add(StringUtil.toString(rsGetIsoList.getString("businessname")));
				vecResultList.add(vecTemp);
			}
			rsGetIsoList.close();
			pstmt.close();
			pstmt = null;		    	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getIsoList", e);
	    	throw new TransactionException();
	    }
	    finally
	    {
	    	try
	    	{
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}	
	    }
	    return vecResultList;
  }  
  
  public static String downloadErrorSummaryReport(ServletContext context, Vector vData,SessionData sessionData) throws TransactionException
  {
    long start = System.currentTimeMillis();
    String strFileName = "";
    String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
    String downloadPath = DebisysConfigListener.getDownloadPath(context);
    String workingDir = DebisysConfigListener.getWorkingDir(context);
    String filePrefix = DebisysConfigListener.getFilePrefix(context);

    try
    {
      long startQuery = 0;
      long endQuery = 0;
      startQuery = System.currentTimeMillis();
      BufferedWriter outputFile = null;
      try
      {
        strFileName = (new CarrierSearch()).generateFileName(context);
        outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
        cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
        outputFile.flush();
        
        outputFile.write("\"" + Languages.getString("jsp.admin.reports.error_type",sessionData.getLanguage()) + "\",");
        outputFile.write("\"" + Languages.getString("jsp.admin.reports.qty",sessionData.getLanguage()) + "\",");
        outputFile.write("\"" + Languages.getString("jsp.admin.reports.total_errors",sessionData.getLanguage()) + "\",");
        outputFile.write("\"" + Languages.getString("jsp.admin.reports.error_percent",sessionData.getLanguage()) + "\",");

  		outputFile.write("\r\n");
        
        double dSum = Double.parseDouble(((Vector)vData.get(0)).get(0).toString());
        for ( int i = 1; i < vData.size(); i++ )
        {
        	outputFile.write("\"" + ((Vector)vData.get(i)).get(1) + "\",");
        	outputFile.write("\"" + ((Vector)vData.get(i)).get(2) + "\",");
        	outputFile.write("\"" + dSum + "\",");
        	if ( dSum == 0 )
        	{
        		outputFile.write("\"100%\",");
        	}
        	else
        	{
        		outputFile.write("\"" + (Integer.parseInt(((Vector)vData.get(i)).get(2).toString())/dSum) * 100 + "%\",");
        	}
        	outputFile.write("\r\n");
        }
        outputFile.flush();
        outputFile.close();
        endQuery = System.currentTimeMillis();
        cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
        startQuery = System.currentTimeMillis();
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        cat.debug("Zip File Name: " + zipFileName);
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
        cat.debug("Download URL: " + downloadUrl);

        try
        {

          ZipOutputStream out =
              new ZipOutputStream(new FileOutputStream(zipFileName));

          // Set the compression ratio
          out.setLevel(Deflater.DEFAULT_COMPRESSION);
          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

          // Add ZIP entry to output stream.
          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

          // Transfer bytes from the current file to the ZIP file
          //out.write(buffer, 0, in.read(buffer));

          int len;
          while ((len = in.read(buffer)) > 0)
          {
            out.write(buffer, 0, len);
          }

          // Close the current entry
          out.closeEntry();
          // Close the current file input stream
          in.close();
          out.close();
        }
        catch (IllegalArgumentException iae)
        {
          iae.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
          fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
        sourceFile.delete();
        endQuery = System.currentTimeMillis();
        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");


      }
      catch (IOException ioe)
      {
        try
        {
          if (outputFile != null)
          {
            outputFile.close();
          }
        }
        catch (IOException ioe2)
        {
        }
      }


    }
    catch (Exception e)
    {
      cat.error("Error during downloadErrorSummaryReport", e);
      throw new TransactionException();
    }
    long end = System.currentTimeMillis();

    // Display the elapsed time to the standard output
    cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");

    return downloadUrl;
  }//End of function downloadErrorSummaryReport
  
  /*END Release 10.0 - Added by LF*/
  
  
  
  
  //********************************************//
  //DBSY-815 R30 Jorge Nicol�s Mart�nez Romero
  //All report headers should be exportable
  /**
	* @param sessionData
	* @param intSectionPage
	* @param context
	* @param deploymentType
	* @param customConfigType
	* @param strAccessLevel
	* @param blnUSEAdditionalFields
	* @param showAdditionalDataColumn
	* @param outputFile
	* @throws IOException
	*/
  	private void defineTitlesForDownload(SessionData sessionData,
  			 							  int intSectionPage, 
  			 							  ServletContext context, 
  			 							  String deploymentType,
  			 							  String customConfigType, 
  			 							  String strAccessLevel,
  			 							  boolean blnUSEAdditionalFields, 
  			 							  boolean showAdditionalDataColumn,
  			 							  boolean invoicePermission,
  			 							  FileChannel fc) throws IOException 
	{
		/***********************************/
		/***********************************/
		/***********************************/
  		this.writetofile(fc,"\"#\",");
  		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.tran_no",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.term_no",sessionData.getLanguage()) + "\",");
		if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
			if ((intSectionPage == 3) || (intSectionPage == 14))
			{
				this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.termtypedesc",sessionData.getLanguage()) + "\",");
			}
		}
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.dba",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\""  + Languages.getString("jsp.admin.reports.merchant_id",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.date",sessionData.getLanguage()) + "\",");
		
		if((intSectionPage == 2) || (intSectionPage == 3) || (intSectionPage == 14) || (intSectionPage == 5) || (intSectionPage == 6))
        {
            if (deploymentType.equalsIgnoreCase(DebisysConstants.DEPLOYMENT_DOMESTIC))
            {
            	this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.phys_state",sessionData.getLanguage()) + "\",");
            }
        }

		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.city",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.county",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.clerk",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reference",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.ref_no",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.recharge",sessionData.getLanguage()) + "\",");
		
		/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
		String strDistChainType = sessionData.getProperty("dist_chain_type");
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {
             if (strAccessLevel.equals(DebisysConstants.CARRIER)){
            	 this.writetofile(fc,"\"" + Languages.getString("jsp.admin.iso_percent",sessionData.getLanguage()) + "\",");  
           		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.agent_percent",sessionData.getLanguage()) + "\",");
           		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.subagent_percent",sessionData.getLanguage()) + "\",");
           		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.rep_percent",sessionData.getLanguage()) + "\",");
           		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.merchant_percent",sessionData.getLanguage()) + "\",");
            }
             else if(strAccessLevel.equals(DebisysConstants.ISO) ){
        	 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
          		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.iso_percent",sessionData.getLanguage()) + "\",");  
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.rep_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.merchant_percent",sessionData.getLanguage()) + "\",");
         	 }
         	 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
         		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.iso_percent",sessionData.getLanguage()) + "\",");  
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.agent_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.subagent_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.rep_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.merchant_percent",sessionData.getLanguage()) + "\",");
         	 }
         	}
         	else if (strAccessLevel.equals(DebisysConstants.AGENT)){
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.agent_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.subagent_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.rep_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.merchant_percent",sessionData.getLanguage()) + "\",");
         	}
         	else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.subagent_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.rep_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.merchant_percent",sessionData.getLanguage()) + "\",");
         	}
         	else if (strAccessLevel.equals(DebisysConstants.REP)){
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.rep_percent",sessionData.getLanguage()) + "\",");
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.merchant_percent",sessionData.getLanguage()) + "\",");
         	}
         	else if(strAccessLevel.equals(DebisysConstants.MERCHANT)){
        		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.merchant_percent",sessionData.getLanguage()) + "\",");
         	}
        }  		
        /*END  DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/		
		
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
		{        	  
		  this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.bonus",sessionData.getLanguage()) + "\",");
		  this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.total_recharge",sessionData.getLanguage()) + "\",");
		}
		
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.balance",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.product",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.description",sessionData.getLanguage()) + "\",");
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.control_no",sessionData.getLanguage()) + "\",");
		
		if (customConfigType.equalsIgnoreCase(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) 
		{
		  this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.additionalData",sessionData.getLanguage()) + "\",");
		}
		if(blnUSEAdditionalFields) 
		{
			if(sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{    		
				this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.pin_number",sessionData.getLanguage()) + "\",");
				          
			}
			if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{ 			
				this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.ach_date",sessionData.getLanguage()) + "\",");
			}
		}
		
		
		if (customConfigType.equalsIgnoreCase(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{          
		  if((intSectionPage == 2) || (intSectionPage == 3) || (intSectionPage == 14) || (intSectionPage == 5) || (intSectionPage == 6))
		  {
			  //writetofile(fc,"=\"" + Languages.getString("jsp.admin.reports.other_info") + "\",");
		  }
		}
		
		this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.transaction_type",sessionData.getLanguage()) + "\",");
		
		if(invoicePermission)
    	{
		  this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.invoiceno",sessionData.getLanguage()) + "\"");
    	}
		this.writetofile(fc,"\r\n");
		
		/***********************************/
		/***********************************/
		/***********************************/
	}
  	//End DBSY-815 R30 Jorge Nicol�s Mart�nez Romero
    //*****************************************//
  //********************************************//
    //DBSY-815 R30 Jorge Nicol�s Mart�nez Romero
    //All report headers should be exportable
    /**
  	* @param sessionData
  	* @param intSectionPage
  	* @param context
  	* @param deploymentType
  	* @param customConfigType
  	* @param strAccessLevel
  	* @param blnUSEAdditionalFields
  	* @param showAdditionalDataColumn
  	* @param outputFile
  	* @throws IOException
  	*/
    	private void defineTitlesForDownload(SessionData sessionData,
    			 							  int intSectionPage, 
    			 							  ServletContext context, 
    			 							  String deploymentType,
    			 							  String customConfigType, 
    			 							  String strAccessLevel,
    			 							  boolean blnUSEAdditionalFields, 
    			 							  boolean showAdditionalDataColumn,
    			 							  BufferedWriter outputFile) throws IOException 
  	{
  		/***********************************/
  		/***********************************/
  		/***********************************/
  		outputFile.write("\"#\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.tran_no",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.term_no",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.dba",sessionData.getLanguage()) + "\",");
  		outputFile.write("=\""  + Languages.getString("jsp.admin.reports.merchant_id",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.date",sessionData.getLanguage()) + "\",");
  		
  		if((intSectionPage == 2) || (intSectionPage == 3) || (intSectionPage == 14) || (intSectionPage == 5) || (intSectionPage == 6))
          {
              if (deploymentType.equalsIgnoreCase(DebisysConstants.DEPLOYMENT_DOMESTIC))
              {
            	  outputFile.write("\"" + Languages.getString("jsp.admin.reports.phys_state",sessionData.getLanguage()) + "\",");
  }
          }

  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.city_county",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.clerk",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.reference",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.ref_no",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.recharge",sessionData.getLanguage()) + "\",");
  		
  		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
  		{        	  
  		  outputFile.write("\"" + Languages.getString("jsp.admin.reports.bonus",sessionData.getLanguage()) + "\",");
  		  outputFile.write("\"" + Languages.getString("jsp.admin.reports.total_recharge",sessionData.getLanguage()) + "\",");
  		}
  		
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.balance",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.product",sessionData.getLanguage()) + "\",");
  		outputFile.write("\"" + Languages.getString("jsp.admin.description",sessionData.getLanguage()) + "\",");
  		outputFile.write("=\"" + Languages.getString("jsp.admin.reports.control_no",sessionData.getLanguage()) + "\",");
  		
  		if (showAdditionalDataColumn) 
  		{
  		  outputFile.write("\"" + Languages.getString("jsp.admin.reports.additionalData",sessionData.getLanguage()) + "\",");
  		}
  		if(blnUSEAdditionalFields) 
  		{
  			if(sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
  			{    		
  				outputFile.write("\"" + Languages.getString("jsp.admin.reports.pin_number",sessionData.getLanguage()) + "\",");
  				          
  			}
  			if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
  			{ 			
  				outputFile.write("\"" + Languages.getString("jsp.admin.reports.ach_date",sessionData.getLanguage()) + "\",");
  			}
  		}
  		
  		
  		if (customConfigType.equalsIgnoreCase(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  		{          
  		  if((intSectionPage == 2) || (intSectionPage == 3) || (intSectionPage == 14) || (intSectionPage == 5) || (intSectionPage == 6))
  		  {
  			  outputFile.write("=\"" + Languages.getString("jsp.admin.reports.other_info",sessionData.getLanguage()) + "\",");
  		  }
  		}
  		
  		outputFile.write("\"" + Languages.getString("jsp.admin.reports.transaction_type",sessionData.getLanguage()) + "\"");
  		outputFile.write("\r\n");
  		
  		/***********************************/
  		/***********************************/
  		/***********************************/
  	}
    	//End DBSY-815 R30 Jorge Nicol�s Mart�nez Romero
      //*****************************************//
    	
}
