package com.debisys.transactions;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;

/**
 * Holds the information for CMS Management.
 *
 * @author Yecid Hurtado
 */

public class CMS implements HttpSessionBindingListener
{

	  //log4j logging
	  static Category cat = Category.getInstance(CMS.class);
	  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.transactions.sql");
	  public static String AllocateAll = "1";
	  public static String AllocateActive = "2";
	  public static String AllocateCancelled = "3";
	  public static String AllocateRange = "4";
	  public static String AllocateByEndDate = "5";
	  public static String AllocateSelected = "6";	
	  public static String Channel = "CHANNEL";
	  public static String Distributor = "DISTRIBUTOR";
	  public static String Subdistributor = "SUBDISTRIBUTOR";
	  public static String Promotor = "PROMOTOR";
	  public static String SKULOAD = "8900004"; //Este product ID sera usado como LOAD y no debe ser mostrado en la lista de CardPrograms
	  public static String ActivationTransactionTypeId = "1,2";
	  public static String CreditTransactionTypeId = "3";
	  
	  public void valueBound(HttpSessionBindingEvent event)
	  {
	  }

	  public void valueUnbound(HttpSessionBindingEvent event)
	  {
	  }

	  public static void main(String[] args)
	  {
	  }

	  /**
	   * Gets a list of card program
	   *
	   * @throws TransactionException Thrown on database errors
	   */

	  public static Vector getCardsProgramList(SessionData sessionData)
	      throws TransactionException
	  {
	    Vector vecCardProgramList = new Vector();
	    Connection dbConn = null;
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new TransactionException();
	    	}
	    	
	    	String strRefId = sessionData.getProperty("ref_id");
	    	String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
	    	PreparedStatement pstmtCompanyID = dbConn.prepareStatement(strSQLCompanyID);
	    	pstmtCompanyID.setString(1, strRefId);
	        ResultSet rsCompanyID = pstmtCompanyID.executeQuery();
	        String strCompanyID = "";
	        while (rsCompanyID.next())
	        {
	        	strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
	        }
	        rsCompanyID.close();
	        pstmtCompanyID.close();
	        
	        //strCompanyID = "1";
	    	String strSQL = sql_bundle.getString("getCardPrograms");
	    	PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
	        pstmt.setString(1, strCompanyID);
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next())
	        {
	          Vector vecTemp = new Vector();
	          String strCardProgramId = StringUtil.toString(rs.getString("CardProgramId"));
	          if(!strCardProgramId.equals(SKULOAD))
	          {
	        	  vecTemp.add(strCardProgramId);
	        	  vecTemp.add(StringUtil.toString(rs.getString("CardProgramName")));
	        	  vecCardProgramList.add(vecTemp);
	          }
	        }

	        rs.close();
	        pstmt.close();
	      	    
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during getCardsProgramList", e);
	      throw new TransactionException();
	    }
	    finally
	    {
	      try
	      {
	        Torque.closeConnection(dbConn);
	      }
	      catch (Exception e)
	      {
	        cat.error("Error during closeConnection", e);
	      }
	    }

	    return vecCardProgramList;
	  }
	  
	  public static Vector getSalesCardsProgramList(SessionData sessionData)
      throws TransactionException
      {
		  Vector vecCardProgramList = new Vector();
		  Connection dbConn = null;
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			  if (dbConn == null)
			  {
				  cat.error("Can't get database connection");
				  throw new TransactionException();
			  }
			  String strRefId = sessionData.getProperty("ref_id");
			  String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
			  PreparedStatement pstmtCompanyID = dbConn.prepareStatement(strSQLCompanyID);
			  pstmtCompanyID.setString(1, strRefId);
			  ResultSet rsCompanyID = pstmtCompanyID.executeQuery();
			  String strCompanyID = "";
			  while (rsCompanyID.next())
			  {
				  strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
			  }
			  rsCompanyID.close();
			  pstmtCompanyID.close();
			  String strSQL = sql_bundle.getString("getCardPrograms");
			  PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			  pstmt.setString(1, strCompanyID);
			  ResultSet rs = pstmt.executeQuery();
			  while (rs.next())
			  {
				  Vector vecTemp = new Vector();
				  vecTemp.add(StringUtil.toString(rs.getString("CardProgramId")));
				  vecTemp.add(StringUtil.toString(rs.getString("CardProgramName")));
				  vecCardProgramList.add(vecTemp);
			  }
			  rs.close();
			  pstmt.close();
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during getCardsProgramList", e);
			  throw new TransactionException();
		  }
		  finally
		  {
			  try
			  {
				  Torque.closeConnection(dbConn);
			  }
			  catch (Exception e)
			  {
				  cat.error("Error during closeConnection", e);
			  }
		  }
		  return vecCardProgramList;
      }
	  	  
	  
	  public static String getCardsProgramListByRefId(String strRefId)
      throws TransactionException
  {
    String Result = "";
    Connection dbConn = null;
    try
    {
    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
    	if (dbConn == null)
    	{
    		cat.error("Can't get database connection");
    		throw new TransactionException();
    	}

    	String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
    	PreparedStatement pstmtCompanyID = dbConn.prepareStatement(strSQLCompanyID);
    	pstmtCompanyID.setString(1, strRefId);
        ResultSet rsCompanyID = pstmtCompanyID.executeQuery();
        String strCompanyID = "";
        while (rsCompanyID.next())
        {
        	strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
        }
        rsCompanyID.close();
        pstmtCompanyID.close();
        
        //strCompanyID = "1";
    	String strSQL = sql_bundle.getString("getCardPrograms");
    	PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
        pstmt.setString(1, strCompanyID);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next())
        {
          String strCardProgramId = StringUtil.toString(rs.getString("CardProgramId"));
          if(!strCardProgramId.equals(SKULOAD))
          {
        	  Result += strCardProgramId + ",";
        	  
	      
          }
        }
        if(!Result.equals(""))Result = Result.substring(0,(Result.length() - 1));
        rs.close();
        pstmt.close();
      	    
    }
    catch (Exception e)
    {
      cat.error("Error during getCardsProgramListByRefId", e);
      throw new TransactionException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }

    return Result;
  }	  
	  
	  public static Vector getCompaniesbyRoleList(SessionData sessionData, String CompanyRoleID)
      throws TransactionException
      {
		  Vector vecCompaniesbyRoleList = new Vector();
		  Connection dbConn = null;
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			  if (dbConn == null)
			  {
				  cat.error("Can't get database connection");
				  throw new TransactionException();
			  }
			  if(!CompanyRoleID.equals(""))
			  {
				  if(!CompanyRoleID.equals("-1"))
				  {				  
					  PreparedStatement pstmt = null;
					  String strRefId = sessionData.getProperty("ref_id");
					  String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
					  pstmt = dbConn.prepareStatement(strSQLCompanyID);
					  pstmt.setString(1, strRefId);
					  ResultSet rsCompanyID = pstmt.executeQuery();
					  String strCompanyID = "";
					  String strCompanyName = "";
					  while (rsCompanyID.next())
					  {
						  strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
						  strCompanyName = StringUtil.toString(rsCompanyID.getString("CompanyName"));
					  }
					  rsCompanyID.close();
					  pstmt.close();
					  pstmt = null;
					  String strSQLCompanyRoleCode  = sql_bundle.getString("getCompanyRoleCode");
					  pstmt = dbConn.prepareStatement(strSQLCompanyRoleCode);
					  pstmt.setString(1, CompanyRoleID);
					  ResultSet rsCompanyRoleCode = pstmt.executeQuery();
					  String strCompanyRoleCode = "";
					  while (rsCompanyRoleCode.next())
					  {
						  strCompanyRoleCode = StringUtil.toString(rsCompanyRoleCode.getString("CompanyRoleCode"));
					  }
					  rsCompanyRoleCode.close();
					  pstmt.close();			  
					  String strAccessLevel = sessionData.getProperty("access_level");
					  String strDistChainType = sessionData.getProperty("dist_chain_type");
					  Vector vecDistributorIds = new Vector();
					  Vector vecSubdistributorIds = new Vector();
					  Vector vecSubdistributorRepIds = new Vector();
					  Vector vecPromotorIds = new Vector();
					  String strCompanyRoleId = "";
					  if (strAccessLevel.equals(DebisysConstants.ISO))
					  {
						  if(strCompanyRoleCode.equals(Channel))
						  {
							  Vector vecTemp = new Vector();
							  vecTemp.add(strCompanyID);
							  vecTemp.add(strCompanyName);
							  vecCompaniesbyRoleList.add(vecTemp);
						  }	
						  else if(strCompanyRoleCode.equals(Distributor))
						  {
							  strCompanyRoleId =  getCompanyRoleId(Distributor);
							  vecDistributorIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
							  vecCompaniesbyRoleList = vecDistributorIds;
						  }
						  else if(strCompanyRoleCode.equals(Subdistributor))
						  {
							  if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
							  {
								  strCompanyRoleId =  getCompanyRoleId(Distributor);
								  vecDistributorIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
								  strCompanyRoleId =  getCompanyRoleId(Subdistributor);
								  vecSubdistributorIds = getCompByRoleAndParentId(getIdsByVector(vecDistributorIds), strCompanyRoleId);
								  vecSubdistributorRepIds = getCompByRoleAndParentId(getIdsByVector(vecSubdistributorIds), strCompanyRoleId);

								  if(vecSubdistributorRepIds != null)
								  {
									  for ( int i = 0; i < vecSubdistributorRepIds.size(); i++ )
									  {
										  Vector vecTemp = new Vector();
										  vecTemp.add(((Vector)vecSubdistributorRepIds.get(i)).get(0));
										  vecTemp.add(((Vector)vecSubdistributorRepIds.get(i)).get(1));
										  vecSubdistributorIds.add(vecTemp);
									  }
									  vecCompaniesbyRoleList = vecSubdistributorIds;
								  }
								  else
								  {
									  	vecCompaniesbyRoleList = vecSubdistributorIds;
								  }
							  }
						  }
						  else if(strCompanyRoleCode.equals(Promotor))
						  {
							  if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
							  {
								  strCompanyRoleId =  getCompanyRoleId(Distributor);
								  vecDistributorIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
								  strCompanyRoleId =  getCompanyRoleId(Subdistributor);
								  vecSubdistributorIds = getCompByRoleAndParentId(getIdsByVector(vecDistributorIds), strCompanyRoleId);
								  vecSubdistributorRepIds = getCompByRoleAndParentId(getIdsByVector(vecSubdistributorIds), strCompanyRoleId);
								  strCompanyRoleId =  getCompanyRoleId(Promotor);
								  vecPromotorIds = getCompByRoleAndParentId(getIdsByVector(vecSubdistributorRepIds), strCompanyRoleId);
							  }
							  else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
							  {
								  strCompanyRoleId =  getCompanyRoleId(Distributor);
								  vecDistributorIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
								  strCompanyRoleId =  getCompanyRoleId(Promotor);
								  vecPromotorIds = getCompByRoleAndParentId(getIdsByVector(vecDistributorIds), strCompanyRoleId);
							  }
							  vecCompaniesbyRoleList = vecPromotorIds;
						  } 
					  }
					  else if (strAccessLevel.equals(DebisysConstants.AGENT))
					  {
						  if(strCompanyRoleCode.equals(Distributor))
						  {
							  Vector vecTemp = new Vector();
							  vecTemp.add(strCompanyID);
							  vecTemp.add(strCompanyName);
							  vecCompaniesbyRoleList.add(vecTemp);
						  }
						  else if(strCompanyRoleCode.equals(Subdistributor))
						  {
							  strCompanyRoleId =  getCompanyRoleId(Subdistributor);
							  vecSubdistributorIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
							  vecSubdistributorRepIds = getCompByRoleAndParentId(getIdsByVector(vecSubdistributorIds), strCompanyRoleId);

							  if(vecSubdistributorRepIds != null)
							  {
								  for ( int i = 0; i < vecSubdistributorRepIds.size(); i++ )
								  {
									  Vector vecTemp = new Vector();
									  vecTemp.add(((Vector)vecSubdistributorRepIds.get(i)).get(0));
									  vecTemp.add(((Vector)vecSubdistributorRepIds.get(i)).get(1));
									  vecSubdistributorIds.add(vecTemp);
								  }
								  vecCompaniesbyRoleList = vecSubdistributorIds;
							  }
							  else
							  {
								  	vecCompaniesbyRoleList = vecSubdistributorIds;
							  }
						  }
						  else if(strCompanyRoleCode.equals(Promotor))
						  {
							  strCompanyRoleId =  getCompanyRoleId(Subdistributor);
							  vecSubdistributorIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
							  vecSubdistributorRepIds = getCompByRoleAndParentId(getIdsByVector(vecSubdistributorIds), strCompanyRoleId);
							  strCompanyRoleId =  getCompanyRoleId(Promotor);
							  vecPromotorIds = getCompByRoleAndParentId(getIdsByVector(vecSubdistributorRepIds), strCompanyRoleId);
							  vecCompaniesbyRoleList = vecPromotorIds;
						  } 						  
					  }
					  else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					  {
						  if(strCompanyRoleCode.equals(Subdistributor))
						  {
							  Vector vecTempSb = new Vector();
							  vecTempSb.add(strCompanyID);
							  vecTempSb.add(strCompanyName);
							  vecSubdistributorIds.add(vecTempSb);
							  strCompanyRoleId =  getCompanyRoleId(Subdistributor);
							  vecSubdistributorRepIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);

							  if(vecSubdistributorRepIds != null)
							  {
								  for ( int i = 0; i < vecSubdistributorRepIds.size(); i++ )
								  {
									  Vector vecTemp = new Vector();
									  vecTemp.add(((Vector)vecSubdistributorRepIds.get(i)).get(0));
									  vecTemp.add(((Vector)vecSubdistributorRepIds.get(i)).get(1));
									  vecSubdistributorIds.add(vecTemp);
								  }
								  vecCompaniesbyRoleList = vecSubdistributorIds;
							  }
							  else
							  {
								  vecCompaniesbyRoleList = vecSubdistributorIds;
							  }
						  }
						  else if(strCompanyRoleCode.equals(Promotor))
						  {
							  strCompanyRoleId =  getCompanyRoleId(Subdistributor);
							  vecSubdistributorRepIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
							  strCompanyRoleId =  getCompanyRoleId(Promotor);
							  vecPromotorIds = getCompByRoleAndParentId(getIdsByVector(vecSubdistributorRepIds), strCompanyRoleId);
							  vecCompaniesbyRoleList = vecPromotorIds;
						  } 						  
					  }
					  else if (strAccessLevel.equals(DebisysConstants.REP))
					  {
						  if((strCompanyRoleCode.equals(Subdistributor)) || (strCompanyRoleCode.equals(Distributor)))
						  {
							  Vector vecTemp = new Vector();
							  vecTemp.add(strCompanyID);
							  vecTemp.add(strCompanyName);
							  vecCompaniesbyRoleList.add(vecTemp);						  
						  }
						  else if(strCompanyRoleCode.equals(Promotor))
						  {						  
							  strCompanyRoleId =  getCompanyRoleId(Promotor);
							  vecPromotorIds = getCompByRoleAndParentId(strCompanyID, strCompanyRoleId);
							  vecCompaniesbyRoleList = vecPromotorIds;
						  }
					  }	
					  else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
					  {
						  Vector vecTemp = new Vector();
						  vecTemp.add(strCompanyID);
						  vecTemp.add(strCompanyName);
						  vecCompaniesbyRoleList.add(vecTemp);
					  }	
				  }
			  }
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during getCompaniesbyRoleList", e);
			  throw new TransactionException();
		  }
		  finally
		  {
			  try
			  {
				  Torque.closeConnection(dbConn);
			  }
			  catch (Exception e)
			  {
				  cat.error("Error during closeConnection", e);
			  }
		  }
		  return vecCompaniesbyRoleList;
      }	  
	  
	  /**
	   * Gets a list of company roles
	   *
	   * @throws TransactionException Thrown on database errors
	   */

	  public static Vector getCompanyRolesList(SessionData sessionData)
	      throws TransactionException
	  {
	    Vector vecCompanyRolesList = new Vector();
	    Connection dbConn = null;
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new TransactionException();
	    	}
	    	//String 
	    	String strSQL = sql_bundle.getString("getCompanyRoles");
	    	PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
	        ResultSet rs = pstmt.executeQuery();
	        
	        String strAccessLevel = sessionData.getProperty("access_level");
	        String strDistChainType = sessionData.getProperty("dist_chain_type");
	        if (strAccessLevel.equals(DebisysConstants.ISO))
	        {
	          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	          {
	  	        while (rs.next())
		        {
		          Vector vecTemp = new Vector();
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Channel))
		          {
		        	  vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.iso", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Distributor))
		          {
		        	  vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.rep", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }		          
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Promotor))
		          {
		        	  vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }		          
		        }
	          }
	          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	          {
		  	        while (rs.next())
			        {
			          Vector vecTemp = new Vector();
			          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Channel))
			          {
				          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
				          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.iso", sessionData.getLanguage()));
				          vecCompanyRolesList.add(vecTemp);
			          }
			          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Distributor))
			          {
				          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
				          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.agent", sessionData.getLanguage()));
				          vecCompanyRolesList.add(vecTemp);
			          }	
			          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Subdistributor))
			          {
				          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
				          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.sub_agent_rep", sessionData.getLanguage()));
				          vecCompanyRolesList.add(vecTemp);
			          }				          
			          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Promotor))
			          {
				          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
				          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage()));
				          vecCompanyRolesList.add(vecTemp);
			          }	
			        }
	          }
	        }
	        else if (strAccessLevel.equals(DebisysConstants.AGENT))
	        {
	  	        while (rs.next())
		        {
		          Vector vecTemp = new Vector();
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Distributor))
		          {
			          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.agent", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }	
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Subdistributor))
		          {
			          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.sub_agent_rep", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }				          
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Promotor))
		          {
			          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }	
		        }
	        }
	        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
	        {
	  	        while (rs.next())
		        {
		          Vector vecTemp = new Vector();
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Subdistributor))
		          {
			          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.sub_agent_rep", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }				          
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Promotor))
		          {
			          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }	
		        }
	        }

	        else if (strAccessLevel.equals(DebisysConstants.REP))
	        {
		          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		          {
		  	        while (rs.next())
			        {
			          Vector vecTemp = new Vector();
			          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Distributor))
			          {
				          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
				          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.rep", sessionData.getLanguage()));
				          vecCompanyRolesList.add(vecTemp);
			          }		          
			          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Promotor))
			          {
				          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
				          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage()));
				          vecCompanyRolesList.add(vecTemp);
			          }		          
			        }
		          }
		          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		          {
			  	        while (rs.next())
				        {
				          Vector vecTemp = new Vector();
				          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Subdistributor))
				          {
					          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
					          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.rep", sessionData.getLanguage()));
					          vecCompanyRolesList.add(vecTemp);
				          }				          
				          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Promotor))
				          {
					          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
					          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage()));
					          vecCompanyRolesList.add(vecTemp);
				          }	
				        }
		          }
	        }
	        else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
	        {
	  	        while (rs.next())
		        {
		          Vector vecTemp = new Vector();
		          if(StringUtil.toString(rs.getString("CompanyRoleCode")).equals(Promotor))
		          {
			          vecTemp.add(StringUtil.toString(rs.getString("CompanyRoleId")));
			          vecTemp.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage()));
			          vecCompanyRolesList.add(vecTemp);
		          }	
		        }
	        }	        
	        
	        rs.close();
	        pstmt.close();	      	    
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during getCompanyRolesList", e);
	      throw new TransactionException();
	    }
	    finally
	    {
	      try
	      {
	        Torque.closeConnection(dbConn);
	      }
	      catch (Exception e)
	      {
	        cat.error("Error during closeConnection", e);
	      }
	    }

	    return vecCompanyRolesList;
	  }	  
	  
	  /**
	   * Gets a list of company roles
	   *
	   * @throws TransactionException Thrown on database errors
	   */

	  public static Vector getCardProgramsTypesList()
	      throws TransactionException
	  {
	    Vector vecCompanyRolesList = new Vector();
	    Connection dbConn = null;
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new TransactionException();
	    	}
	    	String strSQL = sql_bundle.getString("getCardProgramsTypes");
	    	PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next())
	        {
	          Vector vecTemp = new Vector();
	          vecTemp.add(StringUtil.toString(rs.getString("CardProgramTypeID")));
	          vecTemp.add(StringUtil.toString(rs.getString("CardProgramTypeName")));
	          vecCompanyRolesList.add(vecTemp);
	        }

	        rs.close();
	        pstmt.close();
	      	    
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during getCardProgramsTypesList", e);
	      throw new TransactionException();
	    }
	    finally
	    {
	      try
	      {
	        Torque.closeConnection(dbConn);
	      }
	      catch (Exception e)
	      {
	        cat.error("Error during closeConnection", e);
	      }
	    }

	    return vecCompanyRolesList;
	  }	  
	  
	  
	  /**
	   * Gets a list of company roles
	   *
	   * @throws TransactionException Thrown on database errors
	   */

	  public static Vector getStatusList()
	      throws TransactionException
	  {
	    Vector vecCompanyRolesList = new Vector();
	    Connection dbConn = null;
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new TransactionException();
	    	}
	    	String strSQL = sql_bundle.getString("getStatus");
	    	PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
	        pstmt.setString(1, "CARD");
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next())
	        {
	          Vector vecTemp = new Vector();
	          vecTemp.add(StringUtil.toString(rs.getString("StatusID")));
	          vecTemp.add(StringUtil.toString(rs.getString("StatusName")));
	          vecCompanyRolesList.add(vecTemp);
	        }

	        rs.close();
	        pstmt.close();
	      	    
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during getStatusList", e);
	      throw new TransactionException();
	    }
	    finally
	    {
	      try
	      {
	        Torque.closeConnection(dbConn);
	      }
	      catch (Exception e)
	      {
	        cat.error("Error during closeConnection", e);
	      }
	    }

	    return vecCompanyRolesList;
	  }		  
	  
	  /**
	   * Gets a list of card program
	   *
	   * @throws TransactionException Thrown on database errors
	   */

	  public static Vector getCompaniesList(SessionData sessionData, boolean blnOrigin)
	      throws TransactionException
	  {
	    Vector vecCompanyList = new Vector();
	    Connection dbConn = null;
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new TransactionException();
	    	}
	    	
	    	String strRefId = sessionData.getProperty("ref_id");
	    	String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
	    	PreparedStatement pstmtCompanyID = dbConn.prepareStatement(strSQLCompanyID);
	    	pstmtCompanyID.setString(1, strRefId);
	        ResultSet rsCompanyID = pstmtCompanyID.executeQuery();
	        String strCompanyID = "";
	        while (rsCompanyID.next())
	        {
	        	strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
	        }
	        rsCompanyID.close();
	        pstmtCompanyID.close();
	        
	        //strCompanyID = "1";	    	
	    	String strSQL = sql_bundle.getString("getCompanies");
	    	PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
	    	String strCompanyRoleId = "";
	    	if(blnOrigin)
	    	{
	    		strCompanyRoleId = sessionData.getProperty("OriginCompanyRoleId");
	    	}
	    	else
	    	{
	    		strCompanyRoleId = sessionData.getProperty("DestinationCompanyRoleId");
	    	}
	    	String strCardProgramId = sessionData.getProperty("CardProgramId");
	    	pstmt.setString(1, strCompanyRoleId);
	    	pstmt.setString(2, strCardProgramId);
	        pstmt.setString(3, strCompanyID);
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next())
	        {
	          Vector vecTemp = new Vector();
	          vecTemp.add(StringUtil.toString(rs.getString("CompanyId")));
	          vecTemp.add(StringUtil.toString(rs.getString("CompanyName")));
	          vecCompanyList.add(vecTemp);
	        }

	        rs.close();
	        pstmt.close();
	      	    
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during getCompaniesList", e);
	      throw new TransactionException();
	    }
	    finally
	    {
	      try
	      {
	        Torque.closeConnection(dbConn);
	      }
	      catch (Exception e)
	      {
	        cat.error("Error during closeConnection", e);
	      }
	    }

	    return vecCompanyList;
	  }		  
	  
	  /**
	   * Gets a list of card program
	   *
	   * @throws TransactionException Thrown on database errors
	   */

	  public static Vector getActionList(SessionData sessionData)
	      throws TransactionException
	  {
	    Vector vecActionList = new Vector();
	    try
	    {
	    	
	          Vector vecTemp1 = new Vector();
	          vecTemp1.add(AllocateAll);
	          vecTemp1.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.allocate_all", sessionData.getLanguage()));
	          
	          Vector vecTemp2 = new Vector();
	          vecTemp2.add(AllocateActive);
	          vecTemp2.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.allocate_active", sessionData.getLanguage()));

	          Vector vecTemp3 = new Vector();
	          vecTemp3.add(AllocateCancelled);
	          vecTemp3.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.allocate_cancelled", sessionData.getLanguage()));

	          Vector vecTemp4 = new Vector();
	          vecTemp4.add(AllocateRange);
	          vecTemp4.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.allocate_range", sessionData.getLanguage()));

	          //Vector vecTemp5 = new Vector();
	          //vecTemp5.add(AllocateByEndDate);
	          //vecTemp5.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.allocate_conclusion_date", sessionData.getLanguage()));

	          Vector vecTemp6 = new Vector();
	          vecTemp6.add(AllocateSelected);
	          vecTemp6.add(Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.allocate_selected", sessionData.getLanguage()));

	          vecActionList.add(vecTemp1);
	          vecActionList.add(vecTemp2);
	          vecActionList.add(vecTemp3);
	          vecActionList.add(vecTemp4);
	          //vecActionList.add(vecTemp5);
	          vecActionList.add(vecTemp6);
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during getActionList", e);
	      throw new TransactionException();
	    }
	    return vecActionList;
	  }	

	  public static Vector assignInventory(String ModeID, String ExecutedBy, String CardProgramId, String OriginCompanyID, String DestinationCompanyID)
      throws TransactionException
      {
		  Connection dbConn = null;
		  Vector vecResult = new Vector();
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			  if (dbConn == null)
			  {
				  cat.error("Can't get database connection");
				  throw new TransactionException();
			  }
			  String strSQL = sql_bundle.getString("assignInventory");
			  PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			  pstmt.setString(1, ModeID);
			  pstmt.setString(2, ExecutedBy);
			  pstmt.setString(3, CardProgramId);
			  pstmt.setString(4, OriginCompanyID);
			  pstmt.setString(5, DestinationCompanyID);        
			  ResultSet rs = pstmt.executeQuery();
		      while (rs.next())
		      {
		          Vector vecTemp = new Vector();
		          vecTemp.add(StringUtil.toString(rs.getString("SerialNumber")));
		          vecResult.add(vecTemp);
		      }            
			  rs.close();
			  pstmt.close();
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during assignInventory", e);
			  throw new TransactionException();
		  }
		  finally
		  {
			  try
			  {
				  Torque.closeConnection(dbConn);
			  }
			  catch (Exception e)
			  {
				  cat.error("Error during closeConnection", e);
			  }
		  }
		  return vecResult;
      }		  
 
	  public static Vector assignRange(String ModeID, String ExecutedBy, String CardProgramId, 
			  String OriginCompanyID, String DestinationCompanyID, String RangeFrom, String RangeTo)
      throws TransactionException
      {
		  Connection dbConn = null;
		  Vector vecResult = new Vector();
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			  if (dbConn == null)
			  {
				  cat.error("Can't get database connection");
				  throw new TransactionException();
			  }
			  String strSQL = sql_bundle.getString("assignRange");
			  PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			  pstmt.setString(1, ModeID);
			  pstmt.setString(2, ExecutedBy);
			  pstmt.setString(3, CardProgramId);
			  pstmt.setString(4, OriginCompanyID);
			  pstmt.setString(5, DestinationCompanyID);
			  pstmt.setString(6, RangeFrom);
			  pstmt.setString(7, RangeTo);          
			  ResultSet rs = pstmt.executeQuery();
			  int intResult = 0;
			  while (rs.next())
			  {
				  Vector vecTemp = new Vector();
				  vecTemp.add(StringUtil.toString(rs.getString("SerialNumber")));
				  vecResult.add(vecTemp);
			  }            
			  
			  rs.close();
			  pstmt.close();
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during assignRange", e);
			  throw new TransactionException();
		  }
		  finally
		  {
			  try
			  {
				  Torque.closeConnection(dbConn);
			  }
			  catch (Exception e)
			  {
				  cat.error("Error during closeConnection", e);
			  }
		  }
		  return vecResult;
      }	  
	  
	  public static Vector assignSelected(String ModeID, String ExecutedBy, String CardsIDs, 
			  String CardProgramId, String OriginCompanyID, String DestinationCompanyID)
      throws TransactionException
      {
		  Connection dbConn = null;
		  Vector vecResult = new Vector();
		  try
		  {
			  dbConn = org.apache.torque.util.Transaction.begin(sql_bundle.getString("pkgAlternateDb"));
			  if (dbConn == null)
			  {
				  cat.error("Can't get database connection");
				  throw new TransactionException();
			  }
			  String strSQL = sql_bundle.getString("assignSelected");
			  String[] Cards = CardsIDs.split(",");
			  for (int i = 0; i < Cards.length; i++)
			  {
				  int intResult = 0;
				  PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
				  pstmt.setString(1, "1");
				  pstmt.setString(2, Cards[i]);
				  pstmt.setString(3, CardProgramId);
				  pstmt.setString(4, DestinationCompanyID);
				  pstmt.setString(5, ExecutedBy);
				  ResultSet rs = pstmt.executeQuery();
				  while (rs.next())
				  {
					  intResult = rs.getInt("ROWCOUNT");
					  Vector vecTemp = new Vector();
					  vecTemp.add(Cards[i]);
					  vecResult.add(vecTemp);
				  }            
				  if(intResult <= 0)
				  {
					  vecResult = null;
					  dbConn.rollback();
					  break;
				  }
				  rs.close();
				  pstmt.close();
			  }
			  if(vecResult.size()>0)
			  {
				  dbConn.commit();
			  }
		  }
		  catch (Exception e)
		  {
			  try
			  {
				  vecResult = null;
				  dbConn.rollback();
			  }
			  catch(Exception edbc){}
			  cat.error("Error during assignSelected", e);
			  throw new TransactionException();
		  }
		  finally
		  {
			  try
			  {
				  Torque.closeConnection(dbConn);
			  }
			  catch (Exception e)
			  {
				  cat.error("Error during closeConnection", e);
			  }
		  }
		  return vecResult;
      }		  
	  
	  public static Vector getReportCardSalesSearch(SessionData sessionData, String[] CardProgramIdList, 
			  String PlayerTypeId, String PlayerId, String StartDate, String EndDate)
      throws TransactionException
      {
		    Vector vecResultList = new Vector();
		    Connection dbConn = null;
		    Connection dbConnDys = null;
		    PlayerTypeId = StringUtil.escape(PlayerTypeId);
		    PlayerId = StringUtil.escape(PlayerId);
		    StartDate = StringUtil.escape(StartDate);
		    EndDate = StringUtil.escape(EndDate);
		    try
		    {
		    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    	dbConnDys = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
		    	if (dbConn == null)
		    	{
		    		cat.error("Can't get database connection");
		    		throw new TransactionException();
		    	}
		    	PreparedStatement pstmt = null;
		    	String strCompanyID = "";
		    	String strExternalCompanyID = "";
		    	String strCompanyRoleId = "";

		    	if(PlayerId.equals("-1"))
		    	{
			    	String strRefId = sessionData.getProperty("ref_id");
		    		String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
		    		pstmt = dbConn.prepareStatement(strSQLCompanyID);
		    		pstmt.setString(1, strRefId);
		    		ResultSet rsCompanyID = pstmt.executeQuery();
		    		while (rsCompanyID.next())
		    		{
		    			strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
		    		}
		    		rsCompanyID.close();
		    		pstmt.close();    	
		    		pstmt = null;
		    	}
		    	else
		    	{
		    		strCompanyID = PlayerId;
		    	}
		    	if(PlayerTypeId.equals("-1"))
		    	{
		    		String strSQLCompanyRoleID  = sql_bundle.getString("getCompanyRoleIdByCompanyId");
		    		pstmt = dbConn.prepareStatement(strSQLCompanyRoleID);
		    		pstmt.setString(1, strCompanyID);
		    		ResultSet rsCompanyID = pstmt.executeQuery();
		    		while (rsCompanyID.next())
		    		{
		    			strCompanyRoleId = StringUtil.toString(rsCompanyID.getString("CompanyRoleId"));
		    		}
		    		rsCompanyID.close();
		    		pstmt.close();    	
		    		pstmt = null;
		    	}
		    	else
		    	{
		    		strCompanyRoleId = PlayerTypeId;
		    	}
			  	
		    	String strSQLExtCompanyID  = sql_bundle.getString("getExternalCompanyID");
		    	PreparedStatement pstmtExtCompanyID = dbConn.prepareStatement(strSQLExtCompanyID);
		    	pstmtExtCompanyID.setString(1, strCompanyID);
		    	ResultSet rsExtCompanyID = pstmtExtCompanyID.executeQuery();
		    	while (rsExtCompanyID.next())
		    	{
		    		strExternalCompanyID = StringUtil.toString(rsExtCompanyID.getString("ExternalCompanyID"));
		    	}
		    	rsExtCompanyID.close();
		    	pstmtExtCompanyID.close();
		    	
		    	String CardProgramIds = getStringValues(CardProgramIdList);
		    	String[] CardPrograms = null;
		    	if(CardProgramIds.equals(""))
		    	{
		    		CardPrograms = getCardsProgramListByRefId(strExternalCompanyID).split(",");
		    	}
		    	else
		    	{
		    		String[] CardProgramsTemp = CardProgramIds.split(",");
		    		if(CardProgramsTemp.length >1)
		    		{
		    			String resultCardPrograms = "";
				    	for (int i = 0; i < CardProgramsTemp.length; i++)
				    	{
				            if(!CardProgramsTemp[i].equals(SKULOAD))
				            {
				            	resultCardPrograms += CardProgramsTemp[i] + ",";
				            }				    	
				    	}
				    	if(!resultCardPrograms.equals(""))resultCardPrograms = resultCardPrograms.substring(0,(resultCardPrograms.length() - 1));
				    	CardPrograms  = resultCardPrograms.split(",");
		    		}
		    		else
		    		{
		    			CardPrograms = CardProgramsTemp;
		    		}
		    	}
		    	int intRepType = 0;
		    	
		    	String strSQLAccessLevel = sql_bundle.getString("getAccessLevel");
		    	PreparedStatement pstmtAccessLevel = dbConnDys.prepareStatement(strSQLAccessLevel);
		    	pstmtAccessLevel.setString(1, strExternalCompanyID);
		        ResultSet rsAccessLevel = pstmtAccessLevel.executeQuery();
		        while (rsAccessLevel.next())
		        {
		        	intRepType = rsAccessLevel.getInt("type");
		        }

		        rsAccessLevel.close();
		        pstmtAccessLevel.close();	
		    	
		        strExternalCompanyID = StringUtil.escape(strExternalCompanyID);
		    	for (int i = 0; i < CardPrograms.length; i++)
		    	{
		    		String CardProgramId = "";
			    	String strDistributorIds = "";
			    	String strSubdistributorIds = "";
			    	String strSubdistributorRepIds = "";
			    	String strExternalPromotorIds = "";
			    	String strSerials = "";
			    	String strSQL = "";
			    	String strSQLMerchants = "";
			    	String strType = "";
		    		if(CardPrograms[i].equals(""))
		    		{
		    			CardProgramId = null;
		    		}
		    		else
		    		{
		    			CardProgramId = CardPrograms[i];
		    		}
		    		
	    			if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_5_LEVEL))
	    			{
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN "; 
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE iso_id IN ";
    					strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
   						strSQLMerchants +="WHERE iso_id IN ";
						strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) "; 
    					strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))))";
	    			}
	    			else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_3_LEVEL))
    				{
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
	    			}
	    			else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_AGENT))
	    			{
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE iso_id IN "; 
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + ")))";
	    			}
	    	        else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT))
	    	        {
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
	    	        }
	    	        else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_REP))
	    	        {
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id = " + strExternalCompanyID + ")";
	    	        }
	    	        else
	    	        {
	    	        	strSQLMerchants = "AND wt.merchant_id = " + strExternalCompanyID;
	    	        }
	    			CardProgramId = StringUtil.escape(CardProgramId);
	    			String strSQLCardProgramName  = sql_bundle.getString("getCardProgramName");
					pstmt = dbConn.prepareStatement(strSQLCardProgramName);
					pstmt.setString(1, CardProgramId);
					ResultSet rsCardprogramName = pstmt.executeQuery();
					String strCardprogramName = "";
					while (rsCardprogramName.next())
					{
						strCardprogramName = StringUtil.toString(rsCardprogramName.getString("CardProgramName"));
					}
					rsCardprogramName.close();
					pstmt.close(); 	    						
	    			
	    			String strSQLTotal = "SELECT count(wt.rec_id) as qty, "
	    			    + "sum(wt.amount) as total_sales, "
	    			    + "sum(wt.merchant_rate / 100 * wt.amount) as merchant_commission, "
	    			    + "sum(wt.rep_rate / 100 * wt.amount) as rep_commission, "
	    			    + "sum(wt.agent_rate / 100 * wt.amount) as agent_commission, "
	    			    + "sum(wt.subagent_rate / 100 * wt.amount) as subagent_commission, "
	    			    + "sum(wt.iso_rate / 100 * wt.amount) as iso_commission, "
	    			    + "sum((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate as bit)) as adj_amount "
	    			    + " FROM web_transactions wt WITH (nolock) WHERE "; 
	    			  
	    			String strSQLWhere = " wt.id = '" + CardProgramId +"'";
	    			if((!StartDate.equals(""))&&(!EndDate.equals(""))) 
	    			{
	    				strSQLWhere +=" AND (wt.datetime >= '" + StartDate + "' AND wt.datetime < '" + DateUtil.addSubtractDays(EndDate,1) + "') ";
	    			}
	    			strSQLWhere += " AND wt.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ";
	    			strSQLWhere += " AND NOT EXISTS ( ";
	    			strSQLWhere += " SELECT 1 ";
	    			strSQLWhere += " FROM web_transactions D WITH (nolock) ";
	    			strSQLWhere += " WHERE D.id = '" + CardProgramId +"'";
	    			strSQLWhere += " AND D.Transaction_Type_Id= " + CreditTransactionTypeId;
	    			strSQLWhere += " AND wt.other_info = D.other_info ";
	    			if(CardProgramId.equals(SKULOAD))
	    			{
	    				strSQLWhere += " AND wt.amount = ABS(D.amount) ";
	    			}
	    			strSQLWhere += " AND wt.datetime = (SELECT MAX(C.datetime) FROM web_transactions C WITH (nolock) ";
	    			strSQLWhere += " WHERE C.id = '" + CardProgramId +"'";
	    			strSQLWhere += " AND C.other_info = D.other_info ";
	    			strSQLWhere += " AND C.datetime < D.datetime ";
	    			if(CardProgramId.equals(SKULOAD))
	    			{
	    				strSQLWhere += " AND C.amount = abs(D.amount) ";
	    			}
	    			strSQLWhere += " AND C.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ) ";
	    			strSQLWhere += " AND wt.datetime < D.datetime) ";    			
	    			strSQLTotal = strSQLTotal + strSQLWhere + strSQLMerchants;
	    			cat.debug(strSQLTotal);
	    			pstmt = dbConnDys.prepareStatement(strSQLTotal);
	    			ResultSet rsSold = pstmt.executeQuery();
	    			if (rsSold.next())
	    			{
	    				int totalSales = rsSold.getInt("total_sales");
	    				if(totalSales > 0)
	    				{
	    					Vector vecTemp = new Vector();
	    					vecTemp.add(strCardprogramName);
	    					vecTemp.add(CardProgramId);
	    					vecTemp.add(Integer.toString((int) rsSold.getDouble("qty")));
	    					vecTemp.add(NumberUtil.formatAmount(rsSold.getString("total_sales")));
	    					vecTemp.add(NumberUtil.formatAmount(rsSold.getString("iso_commission")));
	    					vecTemp.add(NumberUtil.formatAmount(rsSold.getString("agent_commission")));
	    					vecTemp.add(NumberUtil.formatAmount(rsSold.getString("subagent_commission")));
	    					vecTemp.add(NumberUtil.formatAmount(rsSold.getString("rep_commission")));
	    					vecTemp.add(NumberUtil.formatAmount(rsSold.getString("adj_amount")));
	    					vecTemp.add(strExternalCompanyID);
	    					vecTemp.add(strCompanyID);
	    					vecTemp.add(StartDate);
	    					vecTemp.add(EndDate);
	    					vecResultList.add(vecTemp);
	    				}
	    			}
	    			rsSold.close();
	    			pstmt.close();
	    			pstmt = null;	
		    	}
		    }
		    catch (Exception e)
		    {
		    	cat.error("Error during getReportCardSalesSearch", e);
		    	throw new TransactionException();
		    }
		    finally
		    {
		    	try
		    	{
		    		Torque.closeConnection(dbConn);
		    		Torque.closeConnection(dbConnDys);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("Error during closeConnection", e);
		    	}	
		    }
		    return vecResultList;
      }		  
	  
	  public static Vector getReportCardSearch(SessionData sessionData, String CardProgramID, 
			  String SerialNumber, String CardTypeID, String StatusID, String CompanyRoleID, 
			  String CompanyID, String CreationDate)
      throws TransactionException
      {
		  Vector vecCardSearchList = new Vector();
		  Connection dbConn = null;
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			  if (dbConn == null)
			  {
				  cat.error("Can't get database connection");
				  throw new TransactionException();
			  }
			  String strSQL = sql_bundle.getString("getCards");
			  PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			  if(CardProgramID.equals("-1"))
			  {
				  pstmt.setString(1, null);
			  }else
			  {
				  pstmt.setString(1, CardProgramID);
			  }
			  if(SerialNumber.equals(""))
			  {
				  pstmt.setString(2, null);
			  }else
			  {
				  pstmt.setString(2, SerialNumber);
			  }
			  if(StatusID.equals("-1"))
			  {
				  pstmt.setString(3, null);
			  }else
			  {
				  pstmt.setString(3, StatusID);
			  }
			  if(CompanyRoleID.equals("-1"))
			  {
				  pstmt.setString(4, null);
			  }else
			  {
				  pstmt.setString(4, CompanyRoleID);
			  }
			  if(CompanyID.equals("-1"))
			  {
				  String strRefId = sessionData.getProperty("ref_id");
				  String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
				  PreparedStatement pstmtCompanyID = dbConn.prepareStatement(strSQLCompanyID);
				  pstmtCompanyID.setString(1, strRefId);
				  ResultSet rsCompanyID = pstmtCompanyID.executeQuery();
				  String strCompanyID = "";
				  while (rsCompanyID.next())
				  {
					  strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
				  }
				  rsCompanyID.close();
				  pstmtCompanyID.close();
				  pstmt.setString(5, strCompanyID);
			  }else
			  {
				  pstmt.setString(5, CompanyID);
			  }
			  if(CreationDate.equals(""))
			  {
				  pstmt.setString(6, null);
			  }else
			  {
				  pstmt.setString(6, CreationDate);
			  	}
			  if(CardTypeID.equals("-2"))
			  {
				  pstmt.setString(7, null);
			  }else
			  {
				  pstmt.setString(7, CardTypeID);
			  }
			  ResultSet rs = pstmt.executeQuery();
			  while (rs.next())
			  {
				  Vector vecTemp = new Vector();
				  String CompanyTypeDbys = getPlayerTypeNameDebisys(rs.getString("CompanyRoleID"), rs.getString("CompanyID"),sessionData);
				  vecTemp.add(CompanyTypeDbys);
				  vecTemp.add(StringUtil.toString(rs.getString("CompanyName")));
				  vecTemp.add(StringUtil.toString(rs.getString("CardProgramName")));
				  //String CategoryNameDbys = getCategoryNameDebisys(rs.getString("CardProgramID"));
				  //vecTemp.add(CategoryNameDbys);
				  vecTemp.add(StringUtil.toString(rs.getString("CardProgramTypeName")));
				  vecTemp.add(StringUtil.toString(rs.getString("StatusName")));
				  vecTemp.add(StringUtil.toString(rs.getString("SerialNumber")));
				  java.sql.Date dtCreationDate = rs.getDate("CreationDate");
				  if(dtCreationDate != null)
				  {
					  vecTemp.add(rs.getDate("CreationDate"));
				  }
				  else
				  {
					  vecTemp.add("");
				  }
				  java.sql.Date dtActivationDate = rs.getDate("ActivationDate");
				  if(dtActivationDate != null)
				  {
					  vecTemp.add(rs.getDate("ActivationDate"));
				  }
				  else
				  {
					  vecTemp.add("");
				  }
				  vecCardSearchList.add(vecTemp);
			  }        
			  rs.close();
			  pstmt.close();
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during getReportCardSearch", e);
			  throw new TransactionException();
		  }
		  finally
		  {
			  try
			  {
				  Torque.closeConnection(dbConn);
			  }
			  catch (Exception e)
			  {
				  cat.error("Error during closeConnection", e);
			  }
		  }
		  return vecCardSearchList;
      }	  
	  
	  public static Vector getCurrentCardsQtySales(SessionData sessionData, String strCardProgramId, 
			  String strExternalCompanyID, String strCompanyID, String strStartDate, String strEndDate, 
			  String strReportType)
      throws TransactionException
      {
		    Vector vecResultList = new Vector();
		    Connection dbConn = null;
		    Connection dbConnDys = null;
		    
		    strCardProgramId = StringUtil.escape(strCardProgramId); 
			strExternalCompanyID = StringUtil.escape(strExternalCompanyID);
			strCompanyID = StringUtil.escape(strCompanyID);
			strStartDate = StringUtil.escape(strStartDate);
			strEndDate = StringUtil.escape(strEndDate);
			strReportType = StringUtil.escape(strReportType);
		    try
		    {
		    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    	dbConnDys = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
		    	if (dbConn == null)
		    	{
		    		cat.error("Can't get database connection");
		    		throw new TransactionException();
		    	}
		    	PreparedStatement pstmt = null;
		    		
		    	int intRepType = 0;
		    	
		    	String strSQLAccessLevel = sql_bundle.getString("getAccessLevel");
		    	PreparedStatement pstmtAccessLevel = dbConnDys.prepareStatement(strSQLAccessLevel);
		    	pstmtAccessLevel.setString(1, strExternalCompanyID);
		        ResultSet rsAccessLevel = pstmtAccessLevel.executeQuery();
		        while (rsAccessLevel.next())
		        {
		        	intRepType = rsAccessLevel.getInt("type");
		        }

		        rsAccessLevel.close();
		        pstmtAccessLevel.close();	
		        
		    	String strSerials = "";
		    	String strSQL = "";
		    	String strSQLMerchants = "";
		    	String strAccessLevel = sessionData.getProperty("access_level");
		    	String strDistChainType = sessionData.getProperty("dist_chain_type");
		    	
		    	if(strReportType.equals("0"))
		    	{
	    			if (strAccessLevel.equals(DebisysConstants.ISO))
	    			{
	    				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	    				{
		    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
		    				strSQLMerchants +="WHERE M.rep_id IN "; 
		    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
		    				strSQLMerchants +="WHERE iso_id IN ";
	    					strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	   						strSQLMerchants +="WHERE iso_id IN ";
							strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) "; 
	    					strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))))";
	    				}
	    				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	    				{
		    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
		    				strSQLMerchants +="WHERE M.rep_id IN ";
		    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
		    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
	    				}
	    			}
	    			else if (strAccessLevel.equals(DebisysConstants.AGENT))
	    			{
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE iso_id IN "; 
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + ")))";		    				
	    			}
	    	        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
	    	        {
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
	    	        }
	    	        else if (strAccessLevel.equals(DebisysConstants.REP))
	    	        {
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id = " + strExternalCompanyID + ")";
	    	        }
	    	        else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
	    	        {
	    	        	strSQLMerchants = "AND wt.merchant_id = " + strExternalCompanyID;
	    	        }
	            	String strSQLTotal =  "SELECT wt.other_info FROM web_transactions wt WITH (nolock) WHERE";
	            	String strSQLWhere = " wt.id = '" + strCardProgramId +"'";
	            	strSQLWhere += " AND wt.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ";
	            	strSQLWhere += " AND NOT EXISTS ( ";
	            	strSQLWhere += " SELECT 1 ";
	            	strSQLWhere += " FROM web_transactions D WITH (nolock) ";
	            	strSQLWhere += " WHERE D.id = '" + strCardProgramId +"'";
	            	strSQLWhere += " AND D.Transaction_Type_Id= " + CreditTransactionTypeId;
	            	strSQLWhere += " AND wt.other_info = D.other_info ";
	            	strSQLWhere += " AND wt.datetime = (SELECT MAX(C.datetime) FROM web_transactions C WITH (nolock) ";
	            	strSQLWhere += " WHERE C.id = '" + strCardProgramId +"'";
	            	strSQLWhere += " AND C.other_info = D.other_info ";
	            	strSQLWhere += " AND C.datetime < D.datetime ";
	            	strSQLWhere += " AND C.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ) ";
	            	strSQLWhere += " AND wt.datetime < D.datetime) "; 
	            	
	            	strSQLTotal = strSQLTotal + strSQLWhere + strSQLMerchants;
	            	cat.debug(strSQLTotal);
	            	pstmt = dbConnDys.prepareStatement(strSQLTotal);
	            	ResultSet rsSold = pstmt.executeQuery();
					while (rsSold.next())
					{
	            		strSerials += StringUtil.toString(rsSold.getString("other_info")) + ",";
	            	}
	            	rsSold.close();
	            	pstmt.close();
	            	pstmt = null;	
		    	}
		    	else if(strReportType.equals("1"))
		    	{
			    	String strCompaniesIds = "";
			    	String strDistributorIds = "";
			    	String strSubdistributorIds = "";
			    	String strSubdistributorRepIds = "";
			    	String strPromotorIds = "";
			    	String strExternalPromotorIds = "";
			    	String strCompanyRoleId = "";
			    	
		    		if (strAccessLevel.equals(DebisysConstants.ISO))
		    		{
		    			if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		    			{
		    				strCompanyRoleId =  getCompanyRoleId(Subdistributor);
		    				strSubdistributorIds = getCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    				strSubdistributorRepIds = getCompaniesByRoleAndParentId(strSubdistributorIds, strCompanyRoleId);
		    				strCompanyRoleId =  getCompanyRoleId(Promotor);
		    				strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(strSubdistributorRepIds, strCompanyRoleId);
		    			}
		    			else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		    			{
		    				strCompanyRoleId =  getCompanyRoleId(Promotor);
		    				strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    			}
		    		}
		    		else if (strAccessLevel.equals(DebisysConstants.AGENT))
		    		{
		    			strCompanyRoleId =  getCompanyRoleId(Subdistributor);
		    			strSubdistributorRepIds = getCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    			strCompanyRoleId =  getCompanyRoleId(Promotor);
		    			strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(strSubdistributorRepIds, strCompanyRoleId);
		    		}
		    		else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		    		{
		    			strCompanyRoleId =  getCompanyRoleId(Promotor);
		    			strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    		}
		    		else if(strAccessLevel.equals(DebisysConstants.REP))
		    		{
    					String strSQLExtCompanyID  = sql_bundle.getString("getExternalCompanyID");
    					PreparedStatement pstmtExtCompanyID = dbConn.prepareStatement(strSQLExtCompanyID);
    					pstmtExtCompanyID.setString(1, strCompanyID);
    			    	ResultSet rsExtCompanyID = pstmtExtCompanyID.executeQuery();
    			    	while (rsExtCompanyID.next())
    			    	{
    			    		strExternalPromotorIds = StringUtil.toString(rsExtCompanyID.getString("ExternalCompanyID"));
    			    	}
    			    	rsExtCompanyID.close();
    			    	pstmtExtCompanyID.close();
		    		}
		    		if(!strExternalPromotorIds.equals(""))
		    		{
		    			String strSQLTotal =  "SELECT count(wt.rec_id) as qtySold FROM web_transactions wt WITH (nolock) WHERE";
		    			String strSQLWhere =  " wt.merchant_id in (" + strExternalPromotorIds +") AND";
		    			strSQLWhere = " wt.id = '" + strCardProgramId +"'";
		    			strSQLWhere += " AND wt.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ";
		    			strSQLWhere += " AND NOT EXISTS ( ";
		    			strSQLWhere += " SELECT 1 ";
		    			strSQLWhere += " FROM web_transactions D WITH (nolock) ";
		    			strSQLWhere += " WHERE D.id = '" + strCardProgramId +"'";
		    			strSQLWhere += " AND D.Transaction_Type_Id= " + CreditTransactionTypeId;
		    			strSQLWhere += " AND wt.other_info = D.other_info ";
		    			strSQLWhere += " AND wt.datetime = (SELECT MAX(C.datetime) FROM web_transactions C WITH (nolock) ";
		    			strSQLWhere += " WHERE C.id = '" + strCardProgramId +"'";
		    			strSQLWhere += " AND C.other_info = D.other_info ";
		    			strSQLWhere += " AND C.datetime < D.datetime ";
		    			strSQLWhere += " AND C.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ) ";
		    			strSQLWhere += " AND wt.datetime < D.datetime) "; 
		    			strSQLTotal = strSQLTotal + strSQLWhere;
		    			cat.debug(strSQLTotal);
		    			pstmt = dbConnDys.prepareStatement(strSQLTotal);
		    			ResultSet rsSold = pstmt.executeQuery();
						while (rsSold.next())
						{
		    				strSerials += StringUtil.toString(rsSold.getString("other_info")) + ",";
		    			}
		    			rsSold.close();
		    			pstmt.close();
		    			pstmt = null;
		    		}	
		    	}
		    	if(strReportType.equals("2"))
		    	{
	    			if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_5_LEVEL))
	    			{
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN "; 
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE iso_id IN ";
    					strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
   						strSQLMerchants +="WHERE iso_id IN ";
						strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) "; 
    					strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))))";
	    			}
	    			else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_3_LEVEL))
    				{
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
	    			}
	    			else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_AGENT))
	    			{
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE iso_id IN "; 
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + ")))";
	    			}
	    	        else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT))
	    	        {
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id IN ";
	    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
	    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
	    	        }
	    	        else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_REP))
	    	        {
	    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
	    				strSQLMerchants +="WHERE M.rep_id = " + strExternalCompanyID + ")";
	    	        }
	    	        else
	    	        {
	    	        	strSQLMerchants = "AND wt.merchant_id = " + strExternalCompanyID;
	    	        }
	    			
	    			String strSQLTotal = "SELECT wt.other_info"
	    			    + " FROM web_transactions wt WITH (nolock) WHERE "; 
	    			  
	    			String strSQLWhere = " wt.id = '" + strCardProgramId +"'";
	    			if((!strStartDate.equals(""))&&(!strEndDate.equals(""))) 
	    			{
	    				strSQLWhere +=" AND (wt.datetime >= '" + strStartDate + "' AND wt.datetime < '" + DateUtil.addSubtractDays(strEndDate,1) + "') ";
	    			}
	    			strSQLWhere += " AND wt.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ";
	    			strSQLWhere += " AND NOT EXISTS ( ";
	    			strSQLWhere += " SELECT 1 ";
	    			strSQLWhere += " FROM web_transactions D WITH (nolock) ";
	    			strSQLWhere += " WHERE D.id = '" + strCardProgramId +"'";
	    			strSQLWhere += " AND D.Transaction_Type_Id= " + CreditTransactionTypeId;
	    			strSQLWhere += " AND wt.other_info = D.other_info ";
	    			if(strCardProgramId.equals(SKULOAD))
	    			{
	    				strSQLWhere += " AND wt.amount = ABS(D.amount) ";
	    			}
	    			strSQLWhere += " AND wt.datetime = (SELECT MAX(C.datetime) FROM web_transactions C WITH (nolock) ";
	    			strSQLWhere += " WHERE C.id = '" + strCardProgramId +"'";
	    			strSQLWhere += " AND C.other_info = D.other_info ";
	    			strSQLWhere += " AND C.datetime < D.datetime ";
	    			if(strCardProgramId.equals(SKULOAD))
	    			{
	    				strSQLWhere += " AND C.amount = abs(D.amount) ";
	    			}
	    			strSQLWhere += " AND C.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ) ";
	    			strSQLWhere += " AND wt.datetime < D.datetime) ";    			
	    			strSQLTotal = strSQLTotal + strSQLWhere + strSQLMerchants;
	    			cat.debug(strSQLTotal);
	    			pstmt = dbConnDys.prepareStatement(strSQLTotal);
	    			ResultSet rsSold = pstmt.executeQuery();
					while (rsSold.next())
					{
	    				strSerials += StringUtil.toString(rsSold.getString("other_info")) + ",";
	    			}
	    			rsSold.close();
	    			pstmt.close();
	    			pstmt = null;	
		    	}
		    	if(!strSerials.equals(""))
    			{
    				strSerials = strSerials.substring(0,(strSerials.length() - 1));
    		    	String[] Serials = strSerials.split(",");
    		    	
    		    	for (int i = 0; i < Serials.length; i++)
    		    	{
    		    		String strSQLGetCards = sql_bundle.getString("getCardsRep");
    					PreparedStatement pstmtGetCards  = dbConn.prepareStatement(strSQLGetCards);
    					pstmtGetCards.setString(1, strCardProgramId);
    					pstmtGetCards.setString(2, Serials[i]);
    					pstmtGetCards.setString(3, null);
    					pstmtGetCards.setString(4, null);
				    	pstmtGetCards.setString(5, strCompanyID);
    					pstmtGetCards.setString(6, null);
    					pstmtGetCards.setString(7, null);
    					ResultSet rs = pstmtGetCards.executeQuery();
    					while (rs.next())
    					{
    						Vector vecTemp = new Vector();
    						String CompanyTypeDbys = getPlayerTypeNameDebisys(rs.getString("CompanyRoleID"), rs.getString("CompanyID"),sessionData);
    						vecTemp.add(CompanyTypeDbys);
    						vecTemp.add(StringUtil.toString(rs.getString("CompanyName")));
    						vecTemp.add(StringUtil.toString(rs.getString("CardProgramName")));
    						vecTemp.add(StringUtil.toString(rs.getString("CardProgramTypeName")));
    						vecTemp.add(StringUtil.toString(rs.getString("StatusName")));
    						vecTemp.add(StringUtil.toString(rs.getString("SerialNumber")));
    						java.sql.Date dtCreationDate = rs.getDate("CreationDate");
    						if(dtCreationDate != null)
    						{
    							vecTemp.add(rs.getDate("CreationDate"));
    						}
    						else
    						{
    							vecTemp.add("");
    						}
    						java.sql.Date dtActivationDate = rs.getDate("ActivationDate");
    						if(dtActivationDate != null)
    						{
    							vecTemp.add(rs.getDate("ActivationDate"));
    						}
    						else
    						{
    							vecTemp.add("");
    						}
    						vecResultList.add(vecTemp);
    					}        
    					rs.close();
    					pstmtGetCards.close();
    		    	}
    			}		    	
		    }
		    catch (Exception e)
		    {
		    	cat.error("Error during getCurrentCardsQtySales", e);
		    	throw new TransactionException();
		    }
		    finally
		    {
		    	try
		    	{
		    		Torque.closeConnection(dbConn);
		    		Torque.closeConnection(dbConnDys);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("Error during closeConnection", e);
		    	}	
		    }
		    return vecResultList;
      }	 	  

	  public static Vector getCurrentInventory(SessionData sessionData, String[] CardProgramIdList, String ReportType)
      throws TransactionException
      {
		    Vector vecResultList = new Vector();
		    Connection dbConn = null;
		    Connection dbConnDys = null;
		    try
		    {
		    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    	dbConnDys = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
		    	if (dbConn == null)
		    	{
		    		cat.error("Can't get database connection");
		    		throw new TransactionException();
		    	}
		    	PreparedStatement pstmt = null;
		    	String strExternalCompanyID = sessionData.getProperty("ref_id");
		    	String strSQLCompanyID  = sql_bundle.getString("getCompanyID");
		    	pstmt = dbConn.prepareStatement(strSQLCompanyID);
		    	pstmt.setString(1, strExternalCompanyID);
		    	ResultSet rsCompanyID = pstmt.executeQuery();
		    	String strCompanyID = "";
		    	while (rsCompanyID.next())
		    	{
		    		strCompanyID = StringUtil.toString(rsCompanyID.getString("CompanyID"));
		    	}
		    	rsCompanyID.close();
		    	pstmt.close();    	
		    	pstmt = null;
		    	String CardProgramIds = getStringValues(CardProgramIdList);
		    	String[] CardPrograms = null;
		    	if(CardProgramIds.equals(""))
		    	{
		    		CardPrograms = getCardsProgramListByRefId(strExternalCompanyID).split(",");
		    	}
		    	else
		    	{
		    		CardPrograms = CardProgramIds.split(",");
		    	}
		    	String strAccessLevel = sessionData.getProperty("access_level");
		    	String strDistChainType = sessionData.getProperty("dist_chain_type");
		    	
		    	for (int i = 0; i < CardPrograms.length; i++)
		    	{
		    		String CardProgramId = "";
			    	String strCompaniesIds = "";
			    	String strDistributorIds = "";
			    	String strSubdistributorIds = "";
			    	String strSubdistributorRepIds = "";
			    	String strPromotorIds = "";
			    	String strExternalPromotorIds = "";
			    	String strCompanyRoleId = "";
			    	String strSerials = "";
			    	String strSQLMerchants = "";
			    	int intStock = 0;
			    	int intAssigned = 0;
			    	int intSold = 0;
			    	String strSQL = "";		    		
	    			CardProgramId = CardPrograms[i];
		    		String strType = "";	
		    		if(ReportType.equals("0"))
		    		{
		    			strSQL = sql_bundle.getString("getCardsByCompanyId");
		    			pstmt = dbConn.prepareStatement(strSQL);
		    			pstmt.setString(1, CardProgramId);
		    			pstmt.setString(2, strCompanyID);
		    			ResultSet rs = pstmt.executeQuery();
		    			while (rs.next())
		    			{
		    				intStock++;
		    			}
		    			rs.close();
		    			pstmt.close();
		    			pstmt = null;
		    			if (strAccessLevel.equals(DebisysConstants.ISO))
		    			{
		    				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		    				{
			    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
			    				strSQLMerchants +="WHERE M.rep_id IN "; 
			    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
			    				strSQLMerchants +="WHERE iso_id IN ";
		    					strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
		   						strSQLMerchants +="WHERE iso_id IN ";
								strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) "; 
		    					strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))))";
		    					strType = "5";
		    				}
		    				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		    				{
			    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
			    				strSQLMerchants +="WHERE M.rep_id IN ";
			    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
			    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
		    					strType = "4";
		    				}
		    			}
		    			else if (strAccessLevel.equals(DebisysConstants.AGENT))
		    			{
		    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
		    				strSQLMerchants +="WHERE M.rep_id IN ";
		    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
		    				strSQLMerchants +="WHERE iso_id IN "; 
		    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
		    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + ")))";		    				
	    					strType = "3";
		    			}
		    	        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		    	        {
		    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
		    				strSQLMerchants +="WHERE M.rep_id IN ";
		    				strSQLMerchants +="(SELECT rep_id FROM reps WITH (nolock) ";
		    				strSQLMerchants +="WHERE  iso_id = " + strExternalCompanyID + "))";
		    				strType = "2";
		    	        }
		    	        else if (strAccessLevel.equals(DebisysConstants.REP))
		    	        {
		    				strSQLMerchants = "AND wt.merchant_id IN (SELECT M.merchant_id FROM merchants M WITH (nolock) ";
		    				strSQLMerchants +="WHERE M.rep_id = " + strExternalCompanyID + ")";
		    				strType = "1";
		    	        }
		    	        else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
		    	        {
		    	        	strSQLMerchants = "AND wt.merchant_id = " + strExternalCompanyID;
		    				strType = "0";
		    	        }
	    				strSQL = sql_bundle.getString("getInventoryByCompanyAndCardProgram");
	    				pstmt = dbConn.prepareStatement(strSQL);
	    				pstmt.setString(1, strCompanyID);
	    				pstmt.setString(2, strType);
	    				pstmt.setString(3, CardProgramId);
	    				pstmt.setString(4, "1");
	    				ResultSet rsAssigned = pstmt.executeQuery();
	    				while (rsAssigned.next())
	    				{
	    					intAssigned++;
	    				}
	    				rs.close();
	    				pstmt.close();
	    				pstmt = null;
			            if(intAssigned > 0)
			            {
			            	String strSQLTotal =  "SELECT count(wt.rec_id) as qtySold FROM web_transactions wt WITH (nolock) WHERE";
			            	String strSQLWhere = " wt.id = '" + CardProgramId +"'";
			            	strSQLWhere += " AND wt.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ";
			            	strSQLWhere += " AND NOT EXISTS ( ";
			            	strSQLWhere += " SELECT 1 ";
			            	strSQLWhere += " FROM web_transactions D WITH (nolock) ";
			            	strSQLWhere += " WHERE D.id = '" + CardProgramId +"'";
			            	strSQLWhere += " AND D.Transaction_Type_Id= " + CreditTransactionTypeId;
			            	strSQLWhere += " AND wt.other_info = D.other_info ";
			            	strSQLWhere += " AND wt.datetime = (SELECT MAX(C.datetime) FROM web_transactions C WITH (nolock) ";
			            	strSQLWhere += " WHERE C.id = '" + CardProgramId +"'";
			            	strSQLWhere += " AND C.other_info = D.other_info ";
			            	strSQLWhere += " AND C.datetime < D.datetime ";
			            	strSQLWhere += " AND C.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ) ";
			            	strSQLWhere += " AND wt.datetime < D.datetime) "; 
			            	
			            	strSQLTotal = strSQLTotal + strSQLWhere + strSQLMerchants;
			            	cat.debug(strSQLTotal);
			            	pstmt = dbConnDys.prepareStatement(strSQLTotal);
			            	ResultSet rsSold = pstmt.executeQuery();
			            	if (rsSold.next())
			            	{
			            		intSold = rsSold.getInt("qtySold");
			            	}
			            	rsSold.close();
			            	pstmt.close();
			            	pstmt = null;	
			            	String strSQLCardProgramName  = sql_bundle.getString("getCardProgramName");
			            	pstmt = dbConn.prepareStatement(strSQLCardProgramName);
			            	pstmt.setString(1, CardProgramId);
			            	ResultSet rsCardprogramName = pstmt.executeQuery();
			            	String strCardprogramName = "";
			            	while (rsCardprogramName.next())
			            	{
			            		strCardprogramName = StringUtil.toString(rsCardprogramName.getString("CardProgramName"));
			            	}
			            	rsCardprogramName.close();
			            	pstmt.close();
			            	Vector vecTemp = new Vector();
			            	vecTemp.add(strCardprogramName);
			            	vecTemp.add(intStock);
			            	vecTemp.add(intAssigned);
			            	vecTemp.add(intSold);
			            	vecTemp.add(CardProgramId);
			            	vecTemp.add(strExternalCompanyID);
			            	vecTemp.add(strCompanyID);
			            	vecResultList.add(vecTemp);
			            }
		    		}
		    		else if(ReportType.equals("1"))
		    		{
		    			if (strAccessLevel.equals(DebisysConstants.ISO))
		    			{
		    					strCompanyRoleId =  getCompanyRoleId(Distributor);
		    					strDistributorIds = getCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    			}
		    			else if (strAccessLevel.equals(DebisysConstants.AGENT))
		    			{
	    					strCompanyRoleId =  getCompanyRoleId(Subdistributor);
	    					strSubdistributorIds = getCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    			}
		    	        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		    	        {
	    					strCompanyRoleId =  getCompanyRoleId(Subdistributor);
	    					strSubdistributorRepIds = getCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    	        }
		    	        else if (strAccessLevel.equals(DebisysConstants.REP))
		    	        {
		    					strCompanyRoleId =  getCompanyRoleId(Promotor);
		    					strPromotorIds = getCompaniesByRoleAndParentId(strCompanyID, strCompanyRoleId);
		    	        }
		    	        else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
		    	        {
		    					strPromotorIds = strCompanyID;
		    	        }		    			
	    				String Tmp = strDistributorIds + "," + strSubdistributorIds + "," + strSubdistributorRepIds + "," + strPromotorIds;
	    				String[] DownlineIds = getStringValues(Tmp.split(",")).split(",");
	    		    	for (int j = 0; j < DownlineIds.length; j++)
	    		    	{
	    		    		intStock =0;
			    			strSQL = sql_bundle.getString("getCardsByCompanyId");
			    			pstmt = dbConn.prepareStatement(strSQL);
			    			pstmt.setString(1, CardProgramId);
			    			pstmt.setString(2, DownlineIds[j]);
			    			ResultSet rsDownlineIds = pstmt.executeQuery();
			    			while (rsDownlineIds.next())
			    			{
			    				intStock++;
			    			}
			    			rsDownlineIds.close();
			    			pstmt.close();
			    			pstmt = null;
					    	if(intStock > 0)
					    	{
					    		if (strAccessLevel.equals(DebisysConstants.ISO))
					    		{
					    			if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					    			{
					    				strCompanyRoleId =  getCompanyRoleId(Subdistributor);
					    				strSubdistributorIds = getCompaniesByRoleAndParentId(DownlineIds[j], strCompanyRoleId);
					    				strSubdistributorRepIds = getCompaniesByRoleAndParentId(strSubdistributorIds, strCompanyRoleId);
					    				strCompanyRoleId =  getCompanyRoleId(Promotor);
					    				strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(strSubdistributorRepIds, strCompanyRoleId);
					    				strType = "5";
					    			}
					    			else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
					    			{
					    				strCompanyRoleId =  getCompanyRoleId(Promotor);
					    				strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(DownlineIds[j], strCompanyRoleId);
					    				strType = "4";
					    			}
					    		}
					    		else if (strAccessLevel.equals(DebisysConstants.AGENT))
					    		{
					    			strCompanyRoleId =  getCompanyRoleId(Subdistributor);
					    			strSubdistributorRepIds = getCompaniesByRoleAndParentId(DownlineIds[j], strCompanyRoleId);
					    			strCompanyRoleId =  getCompanyRoleId(Promotor);
					    			strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(strSubdistributorRepIds, strCompanyRoleId);
					    			strType = "3";
					    		}
					    		else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					    		{
					    			strCompanyRoleId =  getCompanyRoleId(Promotor);
					    			strExternalPromotorIds = getExternalCompaniesByRoleAndParentId(DownlineIds[j], strCompanyRoleId);
					    			strType = "2";
					    		}
					    		else if(strAccessLevel.equals(DebisysConstants.REP))
					    		{
			    					String strSQLExtCompanyID  = sql_bundle.getString("getExternalCompanyID");
			    					PreparedStatement pstmtExtCompanyID = dbConn.prepareStatement(strSQLExtCompanyID);
			    					pstmtExtCompanyID.setString(1, DownlineIds[j]);
			    			    	ResultSet rsExtCompanyID = pstmtExtCompanyID.executeQuery();
			    			    	while (rsExtCompanyID.next())
			    			    	{
			    			    		strExternalPromotorIds = StringUtil.toString(rsExtCompanyID.getString("ExternalCompanyID"));
			    			    	}
			    			    	rsExtCompanyID.close();
			    			    	pstmtExtCompanyID.close();
			    			    	strType = "1";
					    		}
					    		if(!strExternalPromotorIds.equals(""))
					    		{
					    			String strSQLTotal =  "SELECT count(wt.rec_id) as qtySold FROM web_transactions wt WITH (nolock) WHERE";
					    			String strSQLWhere =  " wt.merchant_id in (" + strExternalPromotorIds +") AND";
					    			strSQLWhere = " wt.id = '" + CardProgramId +"'";
					    			strSQLWhere += " AND wt.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ";
					    			strSQLWhere += " AND NOT EXISTS ( ";
					    			strSQLWhere += " SELECT 1 ";
					    			strSQLWhere += " FROM web_transactions D WITH (nolock) ";
					    			strSQLWhere += " WHERE D.id = '" + CardProgramId +"'";
					    			strSQLWhere += " AND D.Transaction_Type_Id= " + CreditTransactionTypeId;
					    			strSQLWhere += " AND wt.other_info = D.other_info ";
					    			strSQLWhere += " AND wt.datetime = (SELECT MAX(C.datetime) FROM web_transactions C WITH (nolock) ";
					    			strSQLWhere += " WHERE C.id = '" + CardProgramId +"'";
					    			strSQLWhere += " AND C.other_info = D.other_info ";
					    			strSQLWhere += " AND C.datetime < D.datetime ";
					    			strSQLWhere += " AND C.Transaction_Type_Id IN (" + ActivationTransactionTypeId + ") ) ";
					    			strSQLWhere += " AND wt.datetime < D.datetime) "; 
					    			strSQLTotal = strSQLTotal + strSQLWhere;
					    			cat.debug(strSQLTotal);
					    			pstmt = dbConnDys.prepareStatement(strSQLTotal);
					    			ResultSet rsSold = pstmt.executeQuery();
					    			if (rsSold.next())
					    			{
					    				intSold = rsSold.getInt("qtySold");
					    			}
					    			rsSold.close();
					    			pstmt.close();
					    			pstmt = null;
					    		}	
					    		String strSQLCardProgramName  = sql_bundle.getString("getCardProgramName");
					    		pstmt = dbConn.prepareStatement(strSQLCardProgramName);
					    		pstmt.setString(1, CardProgramId);
					    		ResultSet rsCardprogramName = pstmt.executeQuery();
					    		String strCardprogramName = "";
					    		while (rsCardprogramName.next())
					    		{
					    			strCardprogramName = StringUtil.toString(rsCardprogramName.getString("CardProgramName"));
					    		}
					    		rsCardprogramName.close();
					    		pstmt.close(); 
					    		String strSQLActorName  = sql_bundle.getString("getCompanyName");
					    		pstmt = dbConn.prepareStatement(strSQLActorName);
					    		pstmt.setString(1, DownlineIds[j]);
					    		ResultSet rsActorName = pstmt.executeQuery();
					    		String strActorName = "";
					    		while (rsActorName.next())
					    		{
					    			strActorName = StringUtil.toString(rsActorName.getString("CompanyName"));
					    		}
					    		rsCardprogramName.close();
					    		pstmt.close();
				    			Vector vecTemp = new Vector();
				    			vecTemp.add(strCardprogramName);
					    		vecTemp.add(strActorName);
					    		vecTemp.add(intSold);
					    		vecTemp.add(intStock);
					    		vecTemp.add(CardProgramId);
					    		vecTemp.add(strExternalCompanyID);
					    		vecTemp.add(DownlineIds[j]);
					    		vecResultList.add(vecTemp);
					    	}
	    		    	}
		    		}
		    	}
		    }
		    catch (Exception e)
		    {
		    	cat.error("Error during getCurrentInventory", e);
		    	throw new TransactionException();
		    }
		    finally
		    {
		    	try
		    	{
		    		Torque.closeConnection(dbConn);
		    		Torque.closeConnection(dbConnDys);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("Error during closeConnection", e);
		    	}	
		    }
		    return vecResultList;
      }	  
	
	  public static Vector getAvailableItems(String CardProgramID, String CompanyID, SessionData sessionData)
      throws TransactionException
      {
		  Vector vecCardSearchList = new Vector();
		  Connection dbConn = null;
		  CardProgramID = StringUtil.escape(CardProgramID);
		  CompanyID = StringUtil.escape(CompanyID);
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			  if (dbConn == null)
			  {
				  cat.error("Can't get database connection");
				  throw new TransactionException();
			  }
			  String strSQL = sql_bundle.getString("getCards");
			  PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			  pstmt.setString(1, CardProgramID);
			  pstmt.setString(2, null);
			  pstmt.setString(3, null);
			  pstmt.setString(4, null);
			  pstmt.setString(5, CompanyID);
			  pstmt.setString(6, null);
			  pstmt.setString(7, null);
			  ResultSet rs = pstmt.executeQuery();
			  while (rs.next())
			  {
				  Vector vecTemp = new Vector();
				  String CompanyTypeDbys = getPlayerTypeNameDebisys(rs.getString("CompanyRoleID"), rs.getString("CompanyID"),sessionData);
				  vecTemp.add(CompanyTypeDbys);          
				  vecTemp.add(StringUtil.toString(rs.getString("CompanyName")));
				  vecTemp.add(StringUtil.toString(rs.getString("SerialNumber")));
				  vecTemp.add(StringUtil.toString(rs.getString("CardProgramName")));
		          java.sql.Date dtExpirationDate = rs.getDate("ExpirationDate");
		          if(dtExpirationDate != null)
		          {
		        	  vecTemp.add(rs.getDate("ExpirationDate"));
		          }
		          else
		          {
		        	  vecTemp.add("");
		          }				  
				  vecTemp.add(StringUtil.toString(rs.getString("StatusName")));
				  //String CategoryNameDbys = getCategoryNameDebisys(rs.getString("CardProgramID"));
				  //vecTemp.add(CategoryNameDbys);
				  vecTemp.add(StringUtil.toString(rs.getString("CardProgramTypeName")));
				  vecCardSearchList.add(vecTemp);
			  }        
			  rs.close();
			  pstmt.close();
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during getAvailableItems", e);
			  throw new TransactionException();
		  }
		  finally
		  {
			  try
			  {
				  Torque.closeConnection(dbConn);
			  }
			  catch (Exception e)
			  {
				  cat.error("Error during closeConnection", e);
			  }
		  }
		  return vecCardSearchList;
      }		  
	  
	  public static String getStringValues(String[] strValue)
	  {
	    String strTemp = "";
	    if (strValue != null && strValue.length > 0)
	    {
	      boolean isFirst = true;
	      for (int i = 0; i < strValue.length; i++)
	      {
	        if (strValue[i] != null && !strValue[i].equals(""))
	        {
	          if (!isFirst)
	          {
	            strTemp = strTemp + "," + strValue[i];
	          }
	          else
	          {
	            strTemp = strValue[i];
	            isFirst = false;
	          }
	        }
	      }
	    }

	    return strTemp;
	  }	  
	  
	  public static String getCompanyRoleId(String CompanyRoleCode) throws TransactionException
	  {
		  Connection dbConn = null;
		  String CompanyRoleId = "";
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		      if (dbConn == null)
		      {
		    	  cat.error("Can't get database connection");
		    	  throw new TransactionException();
		      }
		      String strSQL = sql_bundle.getString("getCompanyRoleId");
		      PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
		      pstmt.setString(1, CompanyRoleCode);
		      ResultSet rsCompanyRoleId = pstmt.executeQuery();
		      while (rsCompanyRoleId.next())
		      {
		    	  CompanyRoleId = rsCompanyRoleId.getString("CompanyRoleID");
		      }
		      rsCompanyRoleId.close();
		      pstmt.close();
		    }
		    catch (Exception e)
		    {
		      cat.error("Error during getCompanyRoleId", e);
		      throw new TransactionException();
		    }
		    finally
		    {
		      try
		      {
		        Torque.closeConnection(dbConn);
		      }
		      catch (Exception e)
		      {
		        cat.error("Error during closeConnection", e);
		      }
		    }

		    return CompanyRoleId;
	  }
	  
	  public static String getCompaniesByRoleAndParentId(String ParentId, String CompanyRoleId) throws TransactionException
	  {
		  Connection dbConn = null;
		  String CopaniesId = "";
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		      if (dbConn == null)
		      {
		    	  cat.error("Can't get database connection");
		    	  throw new TransactionException();
		      }
  		      String strSQL = sql_bundle.getString("getCompaniesByRoleAndParentId");
  		      PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
		      pstmt.setString(1, ParentId);
		      pstmt.setString(2, CompanyRoleId);
		      ResultSet rsCompanyRoleId = pstmt.executeQuery();
		      while (rsCompanyRoleId.next())
		      {
		    	  CopaniesId += rsCompanyRoleId.getString("CompanyID") + ",";
		      }
		      rsCompanyRoleId.close();
		      pstmt.close();
		      if(!CopaniesId.equals(""))CopaniesId = CopaniesId.substring(0,(CopaniesId.length() - 1));
		    }
		    catch (Exception e)
		    {
		      cat.error("Error during getCompaniesByRoleAndParentId", e);
		      throw new TransactionException();
		    }
		    finally
		    {
		      try
		      {
		        Torque.closeConnection(dbConn);
		      }
		      catch (Exception e)
		      {
		        cat.error("Error during closeConnection", e);
		      }
		    }

		    return CopaniesId;
	  }	 
	  
	  public static String getExternalCompaniesByRoleAndParentId(String ParentId, String CompanyRoleId) throws TransactionException
	  {
		  Connection dbConn = null;
		  String CopaniesId = "";
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		      if (dbConn == null)
		      {
		    	  cat.error("Can't get database connection");
		    	  throw new TransactionException();
		      }
  		      String strSQL = sql_bundle.getString("getCompaniesByRoleAndParentId");
  		      PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
		      pstmt.setString(1, ParentId);
		      pstmt.setString(2, CompanyRoleId);
		      ResultSet rsCompanyRoleId = pstmt.executeQuery();
		      while (rsCompanyRoleId.next())
		      {
		    	  CopaniesId += rsCompanyRoleId.getString("ExternalCompanyID") + ",";
		      }
		      rsCompanyRoleId.close();
		      pstmt.close();
		      if(!CopaniesId.equals(""))CopaniesId = CopaniesId.substring(0,(CopaniesId.length() - 1));
		    }
		    catch (Exception e)
		    {
		      cat.error("Error during getExternalCompaniesByRoleAndParentId", e);
		      throw new TransactionException();
		    }
		    finally
		    {
		      try
		      {
		        Torque.closeConnection(dbConn);
		      }
		      catch (Exception e)
		      {
		        cat.error("Error during closeConnection", e);
		      }
		    }

		    return CopaniesId;
	  }	
	  
	  public static String getIdsByVector(Vector ListIDs) throws TransactionException
	  {
		  String CopaniesId = "";
		  try
		  {
			  for ( int i = 0; i < ListIDs.size(); i++ )
		      {
		    	  CopaniesId += ((Vector)ListIDs.get(i)).get(0) + ",";
		      }
		      if(!CopaniesId.equals(""))CopaniesId = CopaniesId.substring(0,(CopaniesId.length() - 1));
		    }
		    catch (Exception e)
		    {
		      cat.error("Error during getIdsByVector", e);
		      throw new TransactionException();
		    }
		    return CopaniesId;
	  }
	  
	  public static Vector getCompByRoleAndParentId(String ParentId, String CompanyRoleId) throws TransactionException
	  {
		  Connection dbConn = null;
		  Vector vecResult = new Vector();
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		      if (dbConn == null)
		      {
		    	  cat.error("Can't get database connection");
		    	  throw new TransactionException();
		      }
  		      String strSQL = sql_bundle.getString("getCompaniesByRoleAndParentId");
  		      PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
		      pstmt.setString(1, ParentId);
		      pstmt.setString(2, CompanyRoleId);
		      ResultSet rsCompanyRoleId = pstmt.executeQuery();
		      while (rsCompanyRoleId.next())
		      {
		    	  Vector vecTemp = new Vector();
		          vecTemp.add(StringUtil.toString(rsCompanyRoleId.getString("CompanyID")));
		          vecTemp.add(StringUtil.toString(rsCompanyRoleId.getString("CompanyName")));
		          vecResult.add(vecTemp);
		      }
		      rsCompanyRoleId.close();
		      pstmt.close();
		    }
		    catch (Exception e)
		    {
		      cat.error("Error during getCompByRoleAndParentId", e);
		      throw new TransactionException();
		    }
		    finally
		    {
		      try
		      {
		        Torque.closeConnection(dbConn);
		      }
		      catch (Exception e)
		      {
		        cat.error("Error during closeConnection", e);
		      }
		    }

		    return vecResult;
	  }	 
	  
	  public static String downloadCardSearchReport(ServletContext context, Vector vData, int ColumsNbr) throws TransactionException
	  {
	    long start = System.currentTimeMillis();
	    String strFileName = "";
	    String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
	    String downloadPath = DebisysConfigListener.getDownloadPath(context);
	    String workingDir = DebisysConfigListener.getWorkingDir(context);
	    String filePrefix = DebisysConfigListener.getFilePrefix(context);

	    try
	    {
	      long startQuery = 0;
	      long endQuery = 0;
	      startQuery = System.currentTimeMillis();
	      BufferedWriter outputFile = null;
	      try
	      {
	        strFileName = (new TransactionSearch()).generateFileName(context);
	        outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
	        cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
	        outputFile.flush();
	        for ( int i = 0; i < vData.size(); i++ )
	        {
	        	for ( int j = 0; j < ColumsNbr; j++ )
	        	{
	        		outputFile.write("\"" + ((Vector)vData.get(i)).get(j) + "\",");
	        	}
	          outputFile.write("\r\n");
	        }
	        outputFile.flush();
	        outputFile.close();
	        endQuery = System.currentTimeMillis();
	        cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
	        startQuery = System.currentTimeMillis();
	        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
	        long size = sourceFile.length();
	        byte[] buffer = new byte[(int) size];
	        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
	        cat.debug("Zip File Name: " + zipFileName);
	        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
	        cat.debug("Download URL: " + downloadUrl);

	        try
	        {

	          ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

	          // Set the compression ratio
	          out.setLevel(Deflater.DEFAULT_COMPRESSION);
	          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

	          // Add ZIP entry to output stream.
	          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

	          // Transfer bytes from the current file to the ZIP file
	          //out.write(buffer, 0, in.read(buffer));

	          int len;
	          while ((len = in.read(buffer)) > 0)
	          {
	            out.write(buffer, 0, len);
	          }

	          // Close the current entry
	          out.closeEntry();
	          // Close the current file input stream
	          in.close();
	          out.close();
	        }
	        catch (IllegalArgumentException iae)
	        {
	          iae.printStackTrace();
	        }
	        catch (FileNotFoundException fnfe)
	        {
	          fnfe.printStackTrace();
	        }
	        catch (IOException ioe)
	        {
	          ioe.printStackTrace();
	        }
	        sourceFile.delete();
	        endQuery = System.currentTimeMillis();
	        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");


	      }
	      catch (IOException ioe)
	      {
	        try
	        {
	          if (outputFile != null)
	          {
	            outputFile.close();
	          }
	        }
	        catch (IOException ioe2)
	        {
	        }
	      }


	    }
	    catch (Exception e)
	    {
	      cat.error("Error during downloadCardSearchReport", e);
	      throw new TransactionException();
	    }
	    long end = System.currentTimeMillis();

	    // Display the elapsed time to the standard output
	    cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");

	    return downloadUrl;
	  }	  
	  
	  public static String getPlayerTypeNameDebisys(String CompanyRoleID, String CompanyID,SessionData sessionData) 
      throws TransactionException
      {
		  String strResult = "";
		    Connection dbConn = null;
		    Connection dbConnDys = null;
		    try
		    {
		    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    	if (dbConn == null)
		    	{
		    		cat.error("Can't get database connection");
		    		throw new TransactionException();
		    	}
		    	PreparedStatement pstmt = null;  
				String strSQLCompanyRoleCode  = sql_bundle.getString("getCompanyRoleCode");
				pstmt = dbConn.prepareStatement(strSQLCompanyRoleCode);
				pstmt.setString(1, CompanyRoleID);
				ResultSet rsCompanyRoleCode = pstmt.executeQuery();
				String strCompanyRoleCode = "";
				String strExternalCompanyID = "";
				int intRepType = -1;
				while (rsCompanyRoleCode.next())
				{
					strCompanyRoleCode = StringUtil.toString(rsCompanyRoleCode.getString("CompanyRoleCode"));
				}
				rsCompanyRoleCode.close();
				pstmt.close();
				
		        if(strCompanyRoleCode.equals(Channel))
		        {
		        	strResult = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.iso", sessionData.getLanguage());
		        }
		        if((strCompanyRoleCode.equals(Distributor))||(strCompanyRoleCode.equals(Subdistributor)))
		        {
			    	String strSQLCompanyID  = sql_bundle.getString("getExternalCompanyID");
			    	PreparedStatement pstmtCompanyID = dbConn.prepareStatement(strSQLCompanyID);
			    	pstmtCompanyID.setString(1, CompanyID);
			    	ResultSet rsCompanyID = pstmtCompanyID.executeQuery();
			    	while (rsCompanyID.next())
			    	{
			    		strExternalCompanyID = StringUtil.toString(rsCompanyID.getString("ExternalCompanyID"));
			    	}
			    	rsCompanyID.close();
			    	pstmtCompanyID.close();
			    	
			    	dbConnDys = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			    	if (dbConnDys == null)
			    	{
			    		cat.error("Can't get database connection");
			    		throw new TransactionException();
			    	}
			    	String strSQLAccessLevel = sql_bundle.getString("getAccessLevel");
			    	PreparedStatement pstmtAccessLevel = dbConnDys.prepareStatement(strSQLAccessLevel);
			    	pstmtAccessLevel.setString(1, strExternalCompanyID);
			        ResultSet rsAccessLevel = pstmtAccessLevel.executeQuery();
			        while (rsAccessLevel.next())
			        {
			        	intRepType = rsAccessLevel.getInt("type");
			        }

			        rsAccessLevel.close();
			        pstmtAccessLevel.close();
		        	if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_REP))
		        	{
		        		strResult = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.rep", sessionData.getLanguage());
		        	}
		        	if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_AGENT))
		        	{
		        		strResult = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.agent", sessionData.getLanguage());
		        	}

		        	if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT))
			        {
			        	strResult = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.sub_agent", sessionData.getLanguage());
			        }

		        }
		        if(strCompanyRoleCode.equals(Promotor))
		        {
		        	strResult = Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.merchant", sessionData.getLanguage());
		        }
		    }
		    catch (Exception e)
		    {
		    	cat.error("Error during getPlayerTypeNameDebisys", e);
		    	throw new TransactionException();
		    }
		    finally
		    {
		    	try
		    	{
		    		Torque.closeConnection(dbConn);
		    		Torque.closeConnection(dbConnDys);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("Error during closeConnection", e);
		    	}	
		    }
		    return strResult;		    	
	  }
	  
	  public static String getCategoryNameDebisys(String CardProgramID) 
      throws TransactionException
      {
	        String CategoryName = "";
		    Connection dbConnDys = null;
		    try
		    {
			    	dbConnDys = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			    	if (dbConnDys == null)
			    	{
			    		cat.error("Can't get database connection");
			    		throw new TransactionException();
			    	}
			    	String strSQLCategoryName = sql_bundle.getString("getCategoryName");
			    	PreparedStatement pstmtCategoryName = dbConnDys.prepareStatement(strSQLCategoryName);
			    	pstmtCategoryName.setString(1, CardProgramID);
			        ResultSet rsCategoryName = pstmtCategoryName.executeQuery();
			        while (rsCategoryName.next())
			        {
			        	CategoryName = rsCategoryName.getString("short_name");
			        }
			        rsCategoryName.close();
			        pstmtCategoryName.close();			    	
		    }
		    catch (Exception e)
		    {
		    	cat.error("Error during getCategoryNameDebisys", e);
		    	throw new TransactionException();
		    }
		    finally
		    {
		    	try
		    	{
		    		Torque.closeConnection(dbConnDys);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("Error during closeConnection", e);
		    	}	
		    }
		    return CategoryName;		    	
	  }	 
	  
	  public static String getCategoryIDDebisys(String CardProgramID) 
      throws TransactionException
      {
	        String CategoryId = "";
		    Connection dbConnDys = null;
		    try
		    {
			    	dbConnDys = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			    	if (dbConnDys == null)
			    	{
			    		cat.error("Can't get database connection");
			    		throw new TransactionException();
			    	}
			    	String strSQLCategoryName = sql_bundle.getString("getCategoryName");
			    	PreparedStatement pstmtCategoryName = dbConnDys.prepareStatement(strSQLCategoryName);
			    	pstmtCategoryName.setString(1, CardProgramID);
			        ResultSet rsCategoryName = pstmtCategoryName.executeQuery();
			        while (rsCategoryName.next())
			        {
			        	CategoryId = rsCategoryName.getString("id");
			        }
			        rsCategoryName.close();
			        pstmtCategoryName.close();			    	
		    }
		    catch (Exception e)
		    {
		    	cat.error("Error during getCategoryIDDebisys", e);
		    	throw new TransactionException();
		    }
		    finally
		    {
		    	try
		    	{
		    		Torque.closeConnection(dbConnDys);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("Error during closeConnection", e);
		    	}	
		    }
		    return CategoryId;		    	
	  }	 
	  
	  public static String validateDateRange(String start_date, String end_date,SessionData sessionData)
	  {
	    String strResult = "";
	    boolean valid = true;
	    if ((start_date == null) || (start_date.length() == 0))
	    {
	    	strResult += Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()) + "<br>";
	    	valid = false;
	    }
	    else if (!DateUtil.isValid(start_date))
	    {
	    	strResult += Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()) + "<br>";
	    	valid = false;
	    }

	    if ((end_date == null) || (end_date.length() == 0))
	    {
	    	strResult += Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()) + "<br>";
	    	valid = false;
	    }
	    else if (!DateUtil.isValid(end_date))
	    {
	    	strResult += Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()) + "<br>";
	    	valid = false;
	    }

	    if (valid)
	    {

	      Date dtStartDate = new Date();
	      Date dtEndDate = new Date();
	      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	      formatter.setLenient(false);
	      try
	      {
	        dtStartDate = formatter.parse(start_date);
	      }
	      catch (ParseException ex)
	      {
	    	  strResult += Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()) + "<br>";
	      }
	      try
	      {
	        dtEndDate = formatter.parse(end_date);
	      }
	      catch (ParseException ex)
	      {
	    	  strResult += Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()) + "<br>";;
	      }
	      if (dtStartDate.after(dtEndDate))
	      {
	    	  strResult += Languages.getString("com.debisys.reports.error5", sessionData.getLanguage()) + "<br>";
	      }

	    }

	    return strResult;
	  }	  
}
