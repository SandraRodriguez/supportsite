package com.debisys.promotions;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class PromoDetails implements HttpSessionBindingListener {
	int detailsId = -1;

	int thresholdId = -1;
	String propertyName = "";
	String propertyValue = "";
	
	
	public int getDetailsId()
	{
		return detailsId;
	}

	public void setDetailsId(int detailsId)
	{
		this.detailsId = detailsId;
	}

	public int getThresholdId()
	{
		return thresholdId;
	}

	public void setThresholdId(int thresholdId)
	{
		this.thresholdId = thresholdId;
	}

	public String getPropertyName()
	{
		return propertyName;
	}

	public void setPropertyName(String propertyName)
	{
		this.propertyName = propertyName;
	}

	public String getPropertyValue()
	{
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue)
	{
		this.propertyValue = propertyValue;
	}

	
	
	
	
	public void valueBound(HttpSessionBindingEvent event) {
		
	}

	public void valueUnbound(HttpSessionBindingEvent event) {
		
	}	

}
