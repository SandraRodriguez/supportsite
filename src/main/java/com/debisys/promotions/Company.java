package com.debisys.promotions;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class Company implements HttpSessionBindingListener {
	long companyID = -1;
	String companyName = "";

	public String getCompanyName()	{
		return companyName;
	}

	public void setCompanyName(String CompanyName) {
		this.companyName = CompanyName;
	}
	
	public long getCompanyID()	{
		return companyID;
	}

	public void setCompanyID(long CompanyID) {
		this.companyID = CompanyID;
	}	
	
	
	public void valueBound(HttpSessionBindingEvent event) {
		
	}

	public void valueUnbound(HttpSessionBindingEvent event) {
		
	}	

}
