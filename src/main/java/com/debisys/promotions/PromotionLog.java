package com.debisys.promotions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.PromotionException;
import com.debisys.util.Constants;

import com.emida.utils.dbUtils.TorqueHelper;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;


/**
 * @author Claudia Cuesta
 */

public class PromotionLog implements HttpSessionBindingListener{
	private int promotionID = -1;
	private String dateTime = "";
	private String oldValue = "";
	private String newValue = "";
	private String userName = "";
	private String changeName = "";
	private String tableName = "";
	private String fieldChanged = "";
	private long merchantID = -1;
	private int merchantTest = -1;
	private String accountNumber = "";
	private int authMerchant = -1;
	private int allMerchant = -1;
	private int authSite = -1;
	private int allSite = -1;
	private int siteID = -1;
	private int siteTest = -1;	
	private int accountTest = -1;	
	private float minTransaction = -1;
	private int registryID = -1;
	private String promoName = "";
	
	static Category   cat = Category.getInstance(Promotion.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.promotions.sql");		
	
	public int getRegistryID()	{
		return registryID;
	}

	public void setRegistryID(int RegistryID) {
		this.registryID = RegistryID;
	}
	
	public String getPromoName()	{
		return promoName;
	}

	public void setPromoName(String PromoName) {
		this.promoName = PromoName;
	}
	
	public int getPromotionID()	{
		return promotionID;
	}

	public void setPromotionID(int PromotionID) {
		this.promotionID = PromotionID;
	}
	
	public String getDateTime()	{
		return dateTime;
	}

	public void setDateTime(String DateTime) {
		this.dateTime = DateTime;
	}	
	
	public String getOldValue()	{
		return oldValue;
	}

	public void setOldValue(String OldValue) {
		this.oldValue = OldValue;
	}
	
	public String getNewValue()	{
		return newValue;
	}

	public void setNewValue(String NewValue) {
		this.newValue = NewValue;
	}	
	
	public String getUserName()	{
		return userName;
	}

	public void setUserName(String UserName) {
		this.userName = UserName;
	}	
	
	public String getChangeName()	{
		return changeName;
	}

	public void setChangeName(String ChangeName) {
		this.changeName = ChangeName;
	}	
	
	public String getTableName()	{
		return tableName;
	}

	public void setTableName(String TableName) {
		this.tableName = TableName;
	}
	
	public String getFieldChanged()	{
		return fieldChanged;
	}

	public void setFieldChanged(String FieldChanged) {
		this.fieldChanged = FieldChanged;
	}	
	
	public long getMerchantID()	{
		return merchantID;
	}

	public void setMerchantID(long MerchantID) {
		this.merchantID = MerchantID;
	}	
	
	public int getMerchantTest()	{
		return merchantTest;
	}

	public void setMerchantTest(int MerchantTest) {
		this.merchantTest = MerchantTest;
	}	
	
	public String getAccountNumber()	{
		return accountNumber;
	}

	public void setAccountNumber(String AccountNumber) {
		this.accountNumber = AccountNumber;
	}
	
	public int getAccountTest()	{
		return accountTest;
	}

	public void setAccountTest(int AccountTest) {
		this.accountTest = AccountTest;
	}	
	
	public float getMinTransaction()	{
		return minTransaction;
	}

	public void setMinTransaction(float MinTransaction) {
		this.minTransaction = MinTransaction;
	}
	
	public void setAuthMerchant(int AuthMerchant) {
		this.authMerchant = AuthMerchant;
	}
	
	public int getAuthMerchant()	{
		return authMerchant;
	}
	
	public void setAllMerchant(int AllMerchant) {
		this.allMerchant = AllMerchant;
	}
	
	public int getAllMerchant()	{
		return allMerchant;
	}
	
	public void setAuthSite(int AuthSite) {
		this.authSite = AuthSite;
	}
	
	public int getAuthSite()	{
		return authSite;
	}
	
	public void setSiteID(int SiteID) {
		this.siteID = SiteID;
	}
	
	public int getSiteID()	{
		return siteID;
	}
	
	public void setSiteTest(int SiteTest) {
		this.siteTest = SiteTest;
	}
	
	public int getSiteTest()	{
		return siteTest;
	}
	
	public void setAllSite(int AllSite) {
		this.allSite = AllSite;
	}
	
	public int getAllSite()	{
		return allSite;
	}	
	
	public void valueBound(HttpSessionBindingEvent event){
		
	}

	public void valueUnbound(HttpSessionBindingEvent event){
		
	}	
}
