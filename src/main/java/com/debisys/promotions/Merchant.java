package com.debisys.promotions;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class Merchant implements HttpSessionBindingListener {
	long merchantID = -1;
	String merchantName = "";

	public String getMerchantName()	{
		return merchantName;
	}

	public void setMerchantName(String MerchantName) {
		this.merchantName = MerchantName;
	}
	
	public long getMerchantID()	{
		return merchantID;
	}

	public void setMerchantID(long MerchantID) {
		this.merchantID = MerchantID;
	}	
	
	
	public void valueBound(HttpSessionBindingEvent event) {
		
	}

	public void valueUnbound(HttpSessionBindingEvent event) {
		
	}	

}
