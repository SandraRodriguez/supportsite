package com.debisys.promotions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.PromotionException;
import com.debisys.util.Constants;

import com.emida.utils.dbUtils.TorqueHelper;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;


/**
 * @author Claudia Cuesta
 */

public class Product implements HttpSessionBindingListener{
	private int productID = -1;
	private String productDes = "";
	
	static Category   cat = Category.getInstance(Promotion.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.promotions.sql");		
	
	public int getProductID()	{
		return productID;
	}

	public void setProductID(int ProductID) {
		this.productID = ProductID;
	}
	
	public String getProductDes()	{
		return productDes;
	}

	public void setProductDes(String ProductDes) {
		this.productDes = ProductDes;
	}	
	
	
	public static Vector<Product> GetProductList(long isoID)throws PromotionException {
		Vector<Product> result = new Vector<Product>();
		
  		Connection dbConn = null;

  		String strSQLSummary = "";
  		PreparedStatement pstmt = null;
  		ResultSet rs = null;

  		try
  		{
  			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
  			if (dbConn == null)
  			{
  				cat.error("Can't get database connection");
  				throw new PromotionException();//
  			}	

			strSQLSummary = sql_bundle.getString("GetProductsByISO");
			pstmt = dbConn.prepareStatement(strSQLSummary);
			pstmt.setLong(1, isoID);			
			cat.debug(strSQLSummary + "/*" + Long.toString(isoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());	            
            while (rs.next()) {
            	Product product = new Product();
  				product.productID = rs.getInt("PRODUCTID");
  				product.productDes = rs.getString("DESCRIPTION");
  				
  				result.add(product);
  			}
  		}
  		catch (Exception e)
  		{
  			cat.error("Error during GetProductList", e);
  			throw new PromotionException();
  		}
  		finally
  		{
  			try
  			{
  				TorqueHelper.closeConnection(dbConn, pstmt, rs);
  			}
  			catch (Exception e)
  			{
  				cat.error("Error during closeConnection", e);
  			}
  		}
		return result;
	}	
	
	public static Vector<Product> GetProductsByCarrier(long isoID,long RepID)throws PromotionException {
		Vector<Product> result = new Vector<Product>();
		
  		Connection dbConn = null;

  		String strSQLSummary = "";
  		PreparedStatement pstmt = null;
  		ResultSet rs = null;

  		try
  		{
  			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
  			if (dbConn == null)
  			{
  				cat.error("Can't get database connection");
  				throw new PromotionException();//
  			}	

			if (isoID == -1)
				strSQLSummary = sql_bundle.getString("GetProductsByCarrier");
			else
				strSQLSummary = sql_bundle.getString("GetProductsByCarrierISO");
				
			pstmt = dbConn.prepareStatement(strSQLSummary);
			pstmt.setLong(1, RepID);
			if (isoID != -1)
				pstmt.setLong(2, isoID);					
			cat.debug(strSQLSummary + "/*" + Long.toString(isoID) + "*/");
			cat.debug(strSQLSummary + "/*" + Long.toString(RepID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());	            
            while (rs.next()) {
            	Product product = new Product();
  				product.productID = rs.getInt("PRODUCTID");
  				product.productDes = rs.getString("DESCRIPTION");
  				
  				result.add(product);
  			}
  		}
  		catch (Exception e)
  		{
  			cat.error("Error during GetProductsByCarrier", e);
  			throw new PromotionException();
  		}
  		finally
  		{
  			try
  			{
  				TorqueHelper.closeConnection(dbConn, pstmt, rs);
  			}
  			catch (Exception e)
  			{
  				cat.error("Error during closeConnection", e);
  			}
  		}
		return result;
	}		
	
	public void valueBound(HttpSessionBindingEvent event){
		
	}

	public void valueUnbound(HttpSessionBindingEvent event){
		
	}	
}
