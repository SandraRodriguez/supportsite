package com.debisys.promotions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Category;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.PromotionException;
import com.debisys.util.Constants;

import com.emida.utils.dbUtils.TorqueHelper;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import com.emida.utils.dbUtils.TorqueHelper;

import com.debisys.dateformat.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.servlet.ServletContext;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;

/**
 * @author Claudia Cuesta
 */

public class ISO implements HttpSessionBindingListener{
	private long isoID = -1;
	private String isoName = "";
	private int isoType = -1;
	
	static Category   cat = Category.getInstance(Promotion.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.promotions.sql");	
	
	public int getIsoType()	{
		return isoType;
	}

	public void setIsoType(int IsoType) {
		this.isoID = IsoType;
	}
	
	public long getIsoID()	{
		return isoID;
	}

	public void setIsoID(long IsoID) {
		this.isoID = IsoID;
	}
	
	public String getIsoName()	{
		return isoName;
	}

	public void setIsoName(String IsoName) {
		this.isoName = IsoName;
	}	
	
	public static Vector<ISO> GetISOList()throws PromotionException {
		
		Vector<ISO> result = new Vector<ISO>();
		
  		Connection dbConn = null;

  		String strSQLSummary = "";
  		PreparedStatement pstmt = null;
  		ResultSet rs = null;

  		try
  		{
  			dbConn = Torque.getConnection(ISO.sql_bundle.getString("pkgAlternateDb"));
  			if (dbConn == null)
  			{
  				cat.error("Can't get database connection");
  				throw new PromotionException();//
  			}	

			strSQLSummary = sql_bundle.getString("GetISOs");
			pstmt = dbConn.prepareStatement(strSQLSummary);
			cat.debug(strSQLSummary);
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());	            
            while (rs.next()) {
            	ISO iso = new ISO();
  				iso.isoID = rs.getLong("rep_ID");
  				iso.isoName = rs.getString("businessName");
  				iso.isoType = rs.getInt("type");
  				result.add(iso);
  			}
  		}
  		catch (Exception e)
  		{
  			cat.error("Error during GetISOList", e);
  			throw new PromotionException();
  		}
  		finally
  		{
  			try
  			{
  				TorqueHelper.closeConnection(dbConn, pstmt, rs);
  			}
  			catch (Exception e)
  			{
  				cat.error("Error during closeConnection", e);
  			}
  		}

  		return result;
	}
	
public static Vector<ISO> GetISOListByCarrier(long carrierID)throws PromotionException {
		
		Vector<ISO> result = new Vector<ISO>();
		
  		Connection dbConn = null;

  		String strSQLSummary = "";
  		PreparedStatement pstmt = null;
  		ResultSet rs = null;

  		try
  		{
  			dbConn = Torque.getConnection(ISO.sql_bundle.getString("pkgAlternateDb"));
  			if (dbConn == null)
  			{
  				cat.error("Can't get database connection");
  				throw new PromotionException();//
  			}	

			strSQLSummary = sql_bundle.getString("GetISOsByCarrier");
			pstmt = dbConn.prepareStatement(strSQLSummary);
			pstmt.setLong(1, carrierID);
			cat.debug(strSQLSummary);
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());	            
            while (rs.next()) {
            	ISO iso = new ISO();
  				iso.isoID = rs.getLong("rep_ID");
  				iso.isoName = rs.getString("businessName");
  				iso.isoType = rs.getInt("RepType");
  				result.add(iso);
  			}
  		}
  		catch (Exception e)
  		{
  			cat.error("Error during GetISOListByCarrier", e);
  			throw new PromotionException();
  		}
  		finally
  		{
  			try
  			{
  				TorqueHelper.closeConnection(dbConn, pstmt, rs);
  			}
  			catch (Exception e)
  			{
  				cat.error("Error during closeConnection", e);
  			}
  		}

  		return result;
	}

	

	public void valueBound(HttpSessionBindingEvent event){
		
	}

	public void valueUnbound(HttpSessionBindingEvent event){
		
	}	
}
