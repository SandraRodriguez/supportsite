package com.debisys.promotions;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.PromotionException;
import com.debisys.exceptions.ReportException;
import com.debisys.reports.pojo.TerminalTypePojo;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author Claudia Cuesta
 */
public class Promotion implements HttpSessionBindingListener {
    
    public enum RECURRENCE_PATTERN {
        NONE, WEEKLY, MONTHLY
    }
    
    private int promoId = -1;
    private String merchantCountry = "";
    private Product product = new Product();
    private String startDate = "";
    private String endDate = "";
    private int active = 0;
    private String description = "";
    private String adText = "";
    private int priority = -1;
    private int chances = 0;
    private int outOf = 0;
    private String data = "";
    private int hourOffsetEnd = 0;
    private int hourOffset = 0;
    private ISO iso = new ISO();
    private int status = 0;
    private int lastDayMonth = 0;
    private double minBalRequired = 0.0;
    private String promoIdNew;
    private boolean qrCodeOnly = false;
    private RECURRENCE_PATTERN recurrenceType;
    private Calendar startDateCal;
    private Calendar endDateCal;
    private int minStart;
    private int minEnd;
    private String recurrencePattern;
    private boolean concatenatedPromo;
    private int concatenatedPromoInterval;
    
    static Category cat = Category.getInstance(Promotion.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.promotions.sql");

    /**
     * @return the recurrencePattern
     */
    public String getRecurrencePattern() {
        return recurrencePattern;
    }

    /**
     * @param recurrencePattern the recurrencePattern to set
     */
    public void setRecurrencePattern(String recurrencePattern) {
        this.recurrencePattern = recurrencePattern;
    }
       
    /**
     * @return the recurrenceType
     */
    public RECURRENCE_PATTERN getRecurrenceType() {
        return recurrenceType;
    }

    /**
     * @param recurrenceType the recurrenceType to set
     */
    public void setRecurrenceType(RECURRENCE_PATTERN recurrenceType) {
        this.recurrenceType = recurrenceType;
    }
    
    /**
     * @return the startDateCal
     */
    public Calendar getStartDateCal() {
        return startDateCal;
    }

    /**
     * @param startDateCal the startDateCal to set
     */
    public void setStartDateCal(Calendar startDateCal) {
        this.startDateCal = startDateCal;
    }

    /**
     * @return the endDateCal
     */
    public Calendar getEndDateCal() {
        return endDateCal;
    }

    /**
     * @param endDateCal the endDateCal to set
     */
    public void setEndDateCal(Calendar endDateCal) {
        this.endDateCal = endDateCal;
    }

    /**
     * @return the minStart
     */
    public int getMinStart() {
        return minStart;
    }

    /**
     * @param minStart the minStart to set
     */
    public void setMinStart(int minStart) {
        this.minStart = minStart;
    }

    /**
     * @return the minEnd
     */
    public int getMinEnd() {
        return minEnd;
    }

    /**
     * @param minEnd the minEnd to set
     */
    public void setMinEnd(int minEnd) {
        this.minEnd = minEnd;
    }
    
    public String getData() {
        return data;
    }

    public void setData(String Data) {
        this.data = Data;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product Product) {
        this.product.setProductID(Product.getProductID());
        this.product.setProductDes(Product.getProductDes());
    }

    public int gethourOffsetEnd() {
        return hourOffsetEnd;
    }

    public void setHourOffsetEnd(int HourOffsetEnd) {
        this.hourOffsetEnd = HourOffsetEnd;
    }

    public int gethourOffset() {
        return hourOffset;
    }

    public void setHourOffset(int HourOffset) {
        this.hourOffset = HourOffset;
    }

    public ISO getISO() {
        return iso;
    }

    public void setISO(ISO Iso) {
        this.iso.setIsoID(Iso.getIsoID());
        this.iso.setIsoName(Iso.getIsoName());
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int Priority) {
        this.priority = Priority;
    }

    public int getChances() {
        return chances;
    }

    public void setChances(int Chances) {
        this.chances = Chances;
    }

    public int getOutOf() {
        return outOf;
    }

    public void setOutOf(int OutOf) {
        this.outOf = OutOf;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int Status) {
        this.status = Status;
    }

    public int getPromoId() {
        return promoId;
    }

    public void setPromoId(int PromoId) {
        this.promoId = PromoId;
    }

    public String getMerchantCountry() {
        return merchantCountry;
    }

    public void setMerchantCountry(String MerchantCountry) {
        this.merchantCountry = MerchantCountry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String Description) {
        this.description = Description;
    }

    public String getAdText() {
        return adText;
    }

    public void setAdText(String AdText) {
        this.adText = AdText;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int Active) {
        this.active = Active;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String StartDate) {
        this.startDate = StartDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String EndDate) {
        this.endDate = EndDate;
    }

    public int getLastDayMonth() {
        return lastDayMonth;
    }

    public void setLastDayMonth(int lastDayMonth) {
        this.lastDayMonth = lastDayMonth;
    }

    public double getMinBalRequired() {
        return minBalRequired;
    }

    public void setMinBalRequired(double minBalRequired) {
        this.minBalRequired = minBalRequired;
    }

    public boolean isQrCodeOnly() {
        return qrCodeOnly;
    }

    public void setQrCodeOnly(boolean qrCodeOnly) {
        this.qrCodeOnly = qrCodeOnly;
    }

    public boolean isConcatenatedPromo() {
        return concatenatedPromo;
    }

    public void setConcatenatedPromo(boolean concatenatedPromo) {
        this.concatenatedPromo = concatenatedPromo;
    }

    public int getConcatenatedPromoInterval() {
        return concatenatedPromoInterval;
    }

    public void setConcatenatedPromoInterval(int concatenatedPromoInterval) {
        this.concatenatedPromoInterval = concatenatedPromoInterval;
    }
    
    
    
    

    public static void ChangeStatus(int promID, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new PromotionException();
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("ChangeStatus"));

            pstmt.setInt(1, promID);
            pstmt.setString(2, sessionData.getUser().getUsername());
            pstmt.setString(3, "SUPPORT SITE");
            pstmt.setString(4, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Integer.toString(promID) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback ChangeStatus", ex);
            }
            cat.error("Error during ChangeStatus", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;

    }

    public static void AddSite(int promID, int siteID, int test, int auth, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new PromotionException();
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("AddSitePromo"));

            pstmt.setInt(1, promID);
            pstmt.setInt(2, siteID);
            pstmt.setInt(3, test);
            pstmt.setInt(4, auth);
            pstmt.setString(5, sessionData.getUser().getUsername());
            pstmt.setString(6, "SUPPORT SITE");
            pstmt.setString(7, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Integer.toString(promID) + "," + Integer.toString(siteID) + "," + Integer.toString(test) + "," + Integer.toString(auth) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback AddSitePromo", ex);
            }
            cat.error("Error during AddSite", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void AddAccount(int promID, String account, int test, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("AddAccountPromo"));

            pstmt.setInt(1, promID);
            pstmt.setString(2, account);
            pstmt.setInt(3, test);
            pstmt.setString(4, sessionData.getUser().getUsername());
            pstmt.setString(5, "SUPPORT SITE");
            pstmt.setString(6, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Integer.toString(promID) + "," + account + "," + Integer.toString(test) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback AddAccountPromo", ex);
            }
            cat.error("Error during AddAccount", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void AddMerchant(int promID, long merchantID, int test, int auth, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("AddMerchantPromo"));

            pstmt.setInt(1, promID);
            pstmt.setLong(2, merchantID);
            pstmt.setInt(3, test);
            pstmt.setInt(4, auth);
            pstmt.setString(5, sessionData.getUser().getUsername());
            pstmt.setString(6, "SUPPORT SITE");
            pstmt.setString(7, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Integer.toString(promID) + "," + Long.toString(merchantID) + "," + Integer.toString(test) + "," + Integer.toString(auth) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback AddMerchantPromo", ex);
            }
            cat.error("Error during AddMerchant", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void SaveProduct(int promID, String[] productList, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ResultSet bs = null;
        Map<Integer, String> promoMap = new HashMap<Integer, String>();
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);
            pstmt = dbConn.prepareStatement(sql_bundle.getString("GetPromoProducts"));
            pstmt.setInt(1, promID);

            rs = pstmt.executeQuery();
            Vector results = new Vector();
            while (rs.next()) {
                if (rs.getInt("ProductId") == -1) {
                    pstmt = dbConn.prepareStatement(sql_bundle.getString("DeletePromoWithoutProduct"));
                    pstmt.setInt(1, promID);
                    pstmt.execute();
                } else {
                    results.add(rs.getInt("ProductId"));
                }

            }
            for (int i = 0; i < productList.length; i++) {
                boolean bFound = false;
                for (int j = 0; j < results.size(); j++) {
                    if (Integer.valueOf(productList[i]).equals(results.elementAt(j))) {
                        bFound = true;
                        promoMap.put((Integer) results.elementAt(j), "");
                        break;
                    }

                }
                if (!bFound) {
                    int priority = GetHighestPriority(productList[i]);
                    pstmt = dbConn.prepareStatement(sql_bundle.getString("AddProductPromo"));
                    pstmt.setInt(1, promID);
                    pstmt.setInt(2, Integer.valueOf(productList[i]));
                    if (priority == -1) {
                        pstmt.setInt(3, 0);
                    } else {
                        pstmt.setInt(3, priority + 1);
                    }
                    pstmt.setString(4, "SUPPORT SITE");
                    pstmt.setString(5, sessionData.getUser().getUsername());
                    pstmt.setString(6, instance);
                    pstmt.addBatch();
                    cat.debug(pstmt + "/*" + Integer.toString(promID) + "," + productList[i] + "," + priority + "*/");
                    pstmt.executeBatch();
                }

            }
            for (int k = 0; k < results.size(); k++) {
                if (!promoMap.containsKey((Integer) results.elementAt(k))) {
                    pstmt = dbConn.prepareStatement(sql_bundle.getString("DeleteProductPromo"));
                    pstmt.setInt(1, promID);
                    pstmt.setInt(2, (Integer) results.elementAt(k));
                    pstmt.execute();
                }

            }

            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback AddPromoProduct", ex);
            }
            cat.error("Error during AddPromoProduct", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void UpdatePriority(SessionData sessionData, String instance, int promID, int way, String productId) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("UpdatePriority"));

            pstmt.setInt(1, promID);
            pstmt.setInt(2, way);
            pstmt.setString(3, sessionData.getUser().getUsername());
            pstmt.setString(4, "SUPPORT SITE");
            pstmt.setString(5, instance);
            pstmt.setInt(6, Integer.valueOf(productId));
            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Integer.toString(promID) + "," + Integer.toString(way) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback UpdatePriority", ex);
            }
            cat.error("Error during UpdatePriority", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static int UpdateThreshold(int promID, int recID, float transMinAmount, float bonus, String AddReceipt, int awardType, int consTypeAward, SessionData sessionData, String instance, int thresholdOutOf, int thresholdChances) throws PromotionException {
        int iReturn = -1;
        Connection dbConn = null;
        CallableStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareCall(sql_bundle.getString("EditPromoThreshold"));

            pstmt.setInt(1, promID);
            pstmt.setInt(2, recID);
            pstmt.setFloat(3, transMinAmount);
            pstmt.setFloat(4, bonus);
            pstmt.setString(5, AddReceipt);
            pstmt.setInt(6, awardType);
            pstmt.setInt(7, consTypeAward);
            pstmt.setString(8, sessionData.getUser().getUsername());
            pstmt.setString(9, "SUPPORT SITE");
            pstmt.setString(10, instance);
            pstmt.setInt(11, thresholdOutOf);
            pstmt.setInt(12, thresholdChances);
            pstmt.registerOutParameter(13, java.sql.Types.INTEGER);

            cat.debug(pstmt + "/*" + Float.toString(transMinAmount) + "," + Float.toString(bonus) + "," + AddReceipt + "," + Integer.toString(awardType) + "," + Integer.toString(consTypeAward) + "," + Integer.toString(recID) + "," + Integer.toString(promID) + "," + Integer.toString(thresholdChances) + "," + Integer.toString(thresholdOutOf) + "*/");
            pstmt.execute();
            cat.debug(pstmt.getWarnings());
            iReturn = pstmt.getInt(13);
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback EditPromoThreshold", ex);
            }
            cat.error("Error during EditPromoThreshold", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return iReturn;
    }

    public static int InsertThreshold(int promID, float transMinAmount, float bonus, String AddReceipt, int awardType, int consTypeAward, SessionData sessionData, String instance, int thresholdChances, int thresholdOutof) throws PromotionException {
        int iReturn = -1;
        Connection dbConn = null;
        CallableStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareCall(sql_bundle.getString("InsertPromoThreshold"));

            pstmt.setInt(1, promID);
            pstmt.setFloat(2, transMinAmount);
            pstmt.setFloat(3, bonus);
            pstmt.setString(4, AddReceipt);
            pstmt.setInt(5, awardType);
            pstmt.setInt(6, consTypeAward);
            pstmt.setString(7, sessionData.getUser().getUsername());
            pstmt.setString(8, "SUPPORT SITE");
            pstmt.setString(9, instance);
            pstmt.setInt(10, thresholdChances);
            pstmt.setInt(11, thresholdOutof);
            pstmt.registerOutParameter(12, java.sql.Types.INTEGER);

            cat.info(pstmt + "/*" + Integer.toString(promID) + Float.toString(transMinAmount) + "," + Float.toString(bonus) + "," + AddReceipt + "," + Integer.toString(awardType) + "," + "," + thresholdChances + "," + thresholdOutof + "," + Integer.toString(consTypeAward) + "," + "*/");
            pstmt.execute();
            cat.debug(pstmt.getWarnings());
            iReturn = pstmt.getInt(12);
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback InsertPromoThreshold", ex);
            }
            cat.error("Error during InsertPromoThreshold", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return iReturn;
    }

    public static int InsertPromoDetail(int thresholdId, String name, String value, SessionData sessionData, String instance) throws PromotionException {
        int iReturn = -1;
        Connection dbConn = null;
        CallableStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareCall(sql_bundle.getString("InsertPromoDetail"));

            pstmt.setInt(1, thresholdId);
            pstmt.setString(2, name);
            pstmt.setString(3, value);
            pstmt.setString(4, sessionData.getUser().getUsername());
            pstmt.setString(5, "SUPPORT SITE");
            pstmt.setString(6, instance);
            pstmt.registerOutParameter(7, java.sql.Types.INTEGER);
            cat.debug(pstmt + "/*" + Integer.toString(thresholdId) + "," + name + "," + value + "," + "*/");
            pstmt.execute();
            cat.debug(pstmt.getWarnings());
            iReturn = pstmt.getInt(7);
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback InsertPromoDetail", ex);
            }
            cat.error("Error during InsertPromoDetail", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return iReturn;
    }

    public static int InsertPromo(Promotion promo, SessionData sessionData, String instance) throws PromotionException {
        int promoID = -1;
        Connection dbConn = null;
        CallableStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareCall(sql_bundle.getString("InsertPromo"));

            //pstmt.setInt(1, 0);
            pstmt.setInt(1, promo.product.getProductID());
            pstmt.setInt(2, promo.active);
            pstmt.setString(3, promo.description);
            pstmt.setString(4, promo.adText);
            pstmt.setInt(5, promo.priority);
            pstmt.setInt(6, promo.chances);
            pstmt.setInt(7, promo.outOf);
            pstmt.setString(8, promo.data);
            pstmt.setLong(9, promo.iso.getIsoID());
            pstmt.setString(10, sessionData.getUser().getUsername());
            pstmt.setString(11, "SUPPORT SITE");
            pstmt.setString(12, instance);
            pstmt.setInt(13, promo.lastDayMonth);
            pstmt.setDouble(14, promo.minBalRequired);
            pstmt.setBoolean(15, promo.qrCodeOnly);
            pstmt.registerOutParameter(16, java.sql.Types.INTEGER);
            cat.debug(pstmt + "/* Insert New Promotion */");
            pstmt.execute();
            cat.debug(pstmt.getWarnings());
            promoID = pstmt.getInt(16);

            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback InsertPromo", ex);
            }
            cat.error("Error during InsertPromo", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
        }
        return promoID;
    }

    public static void RemoveMerchant(long merchantID, int promID, int auth, int test, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("RemoveMerchant"));

            pstmt.setInt(1, promID);
            pstmt.setLong(2, merchantID);
            pstmt.setInt(3, test);
            pstmt.setInt(4, auth);
            pstmt.setString(5, sessionData.getUser().getUsername());
            pstmt.setString(6, "SUPPORT SITE");
            pstmt.setString(7, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Long.toString(merchantID) + "," + Integer.toString(promID) + "," + Integer.toString(test) + "," + Integer.toString(auth) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback RemoveMerchant", ex);
            }
            cat.error("Error during RemoveMerchant", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void RemoveSite(int siteID, int promID, int auth, int test, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("RemoveSite"));

            pstmt.setInt(1, promID);
            pstmt.setInt(2, siteID);
            pstmt.setInt(3, test);
            pstmt.setInt(4, auth);
            pstmt.setString(5, sessionData.getUser().getUsername());
            pstmt.setString(6, "SUPPORT SITE");
            pstmt.setString(7, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Long.toString(siteID) + "," + Integer.toString(promID) + "," + Integer.toString(test) + "," + Integer.toString(auth) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback RemoveSite", ex);
            }
            cat.error("Error during RemoveSite", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void RemoveAccountN(int promID, String accountN, int test, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("RemoveAccountNumber"));

            pstmt.setInt(1, promID);
            pstmt.setString(2, accountN);
            pstmt.setInt(3, test);
            pstmt.setString(4, sessionData.getUser().getUsername());
            pstmt.setString(5, "SUPPORT SITE");
            pstmt.setString(6, instance);

            pstmt.addBatch();
            cat.debug(pstmt.getConnection().toString() + "/*" + Integer.toString(promID) + "," + accountN + "," + Integer.toString(test) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback RemoveAccountNumber", ex);
            }
            cat.error("Error during RemoveAccountNumber", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void RemoveThreshold(int promID, int recID, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("RemovePromoThreshold"));

            pstmt.setInt(1, promID);
            pstmt.setInt(2, recID);
            pstmt.setString(3, sessionData.getUser().getUsername());
            pstmt.setString(4, "SUPPORT SITE");
            pstmt.setString(5, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Integer.toString(promID) + "," + Integer.toString(recID) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback RemovePromoThreshold", ex);
            }
            cat.error("Error during RemovePromoThreshold", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void RemovePromoDetail(SessionData sessionData, String strRecId, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("RemovePromoDetail"));

            pstmt.setInt(1, Integer.parseInt(strRecId));
            pstmt.setString(2, sessionData.getUser().getUsername());
            pstmt.setString(3, "SUPPORT SITE");
            pstmt.setString(4, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + strRecId + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback RemovePromoDetail", ex);
            }
            cat.error("Error during RemovePromoThreshold", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static void RemovePromotion(int promID, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(sql_bundle.getString("RemovePromotion"));

            pstmt.setInt(1, promID);
            pstmt.setString(2, sessionData.getUser().getUsername());
            pstmt.setString(3, "SUPPORT SITE");
            pstmt.setString(4, instance);

            pstmt.addBatch();
            cat.debug(pstmt + "/*" + Integer.toString(promID) + "*/");
            pstmt.executeBatch();
            cat.debug(pstmt.getWarnings());
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback RemovePromotion", ex);
            }
            cat.error("Error during RemovePromotion", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static int UpdatePromo(Promotion promo, SessionData sessionData, String instance) throws PromotionException {
        int result = -1;

        Connection dbConn = null;

        String strSQLSummary = "";
        CallableStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                return result;
            }            

            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");
            formatter.setLenient(false);

            Calendar clStartDate = Calendar.getInstance();
            Date dtStartDate = formatter.parse(promo.getStartDate());
            clStartDate.setTime(dtStartDate);
            Calendar clEndDate = Calendar.getInstance();
            Date dtEndDate = formatter.parse(promo.getEndDate());
            clEndDate.setTime(dtEndDate);

            strSQLSummary = sql_bundle.getString("UpdatePromotion");
            pstmt = dbConn.prepareCall(strSQLSummary);
            pstmt.setInt(1, promo.promoId);
            //pstmt.setInt(2, promo.product.getProductID());
            promo.hourOffset = clStartDate.get(Calendar.HOUR_OF_DAY);
            if (promo.hourOffset != 0) {
                clStartDate.add(Calendar.DATE, 1);
                promo.hourOffset = 24 - promo.hourOffset;
            }
            promo.hourOffsetEnd = clEndDate.get(Calendar.HOUR_OF_DAY);
            if (promo.hourOffsetEnd != 0) {
                clEndDate.add(Calendar.DATE, 1);
                promo.hourOffsetEnd = 24 - promo.hourOffsetEnd;
            }
            if ( clStartDate.before(clEndDate)){
                dbConn.setAutoCommit(false);            
                pstmt.setDate(2, new java.sql.Date(clStartDate.getTimeInMillis()));
                pstmt.setInt(3, promo.hourOffset);
                pstmt.setDate(4, new java.sql.Date(clEndDate.getTimeInMillis()));
                pstmt.setInt(5, promo.hourOffsetEnd);
                pstmt.setString(6, promo.description);
                pstmt.setString(7, promo.adText);
                pstmt.setInt(8, promo.chances);
                pstmt.setInt(9, promo.outOf);
                pstmt.setString(10, promo.data);
                pstmt.setLong(11, promo.iso.getIsoID());
                pstmt.setString(12, sessionData.getUser().getUsername());
                pstmt.setString(13, "SUPPORT SITE");
                pstmt.setString(14, instance);
                pstmt.setInt(15, promo.lastDayMonth);
                pstmt.setDouble(16, promo.minBalRequired);
                pstmt.setBoolean(17, promo.qrCodeOnly);
                pstmt.setInt(18, clStartDate.get(Calendar.MINUTE));
                pstmt.setInt(19, clEndDate.get(Calendar.MINUTE));

                pstmt.setString(20, promo.getRecurrenceType().toString());
                if ( promo.getRecurrencePattern() !=null){
                    pstmt.setString(21, promo.getRecurrencePattern());
                } else{
                    pstmt.setNull(21, java.sql.Types.NULL);
                }
                pstmt.setBoolean(22, promo.isConcatenatedPromo());
                pstmt.setInt(23, promo.getConcatenatedPromoInterval());
                pstmt.registerOutParameter(24, java.sql.Types.INTEGER);
                cat.debug(pstmt + "/*" + Integer.toString(promo.promoId) + "*/");
                cat.debug(pstmt.getWarnings());
                pstmt.execute();
                result = pstmt.getInt(24);
                pstmt.close();
                dbConn.commit();
                dbConn.setAutoCommit(true);
            } else{
                 result = 2;
                 cat.error("The end date must be after of start date!!!");
            }
        } catch (Exception e) {
            cat.error("Error during UpdatePromo", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }

        return result;

    }

    public static Vector<Integer> GetSitesByMerchant(long merchantID) throws PromotionException {

        Vector<Integer> vecReturn = new Vector<Integer>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetSitesByMerchant");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setLong(1, merchantID);
            cat.debug(strSQLSummary + "/*" + Long.toString(merchantID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                vecReturn.add(rs.getInt("millennium_no"));
            }
        } catch (Exception e) {
            cat.error("Error during GetSitesByMerchant", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Promotion GetPromotionInfo(int promoID) throws PromotionException {

        Promotion prom = new Promotion();

        Connection dbConn = null;
        String strSQLSummary = "";
        CallableStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
            formatter.setLenient(false);

            Calendar clStartDate = Calendar.getInstance();
            Calendar clEndDate = Calendar.getInstance();

            strSQLSummary = sql_bundle.getString("GetPromotionInfo");
            pstmt = dbConn.prepareCall(strSQLSummary);
            pstmt.setInt(1, promoID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                Product prod = new Product();
                ISO isotemp = new ISO();
                try {
                    prod.setProductID(rs.getInt("productid"));
                } catch (Exception e) {
                    prod.setProductID(-1);
                }
                try {
                    prod.setProductDes(rs.getString("description"));
                } catch (Exception e) {
                    prod.setProductDes("");
                }
                prom.setProduct(prod);
                prom.description = rs.getString("promodescription");
                prom.hourOffset = rs.getInt("houroffset");
                prom.hourOffsetEnd = rs.getInt("houroffsetend");
                Date dtStartDate = formatter.parse(DateUtil.formatDateTimePromo(rs.getTimestamp("startdate")));
                clStartDate.setTime(dtStartDate);
                if (prom.hourOffset != 0) {
                    clStartDate.add(Calendar.DAY_OF_MONTH, -1);
                }
                prom.setStartDateCal(clStartDate);
                Date dtEndDate = formatter.parse(DateUtil.formatDateTimePromo(rs.getTimestamp("enddate")));
                clEndDate.setTime(dtEndDate);
                if (prom.hourOffsetEnd != 0) {
                    clEndDate.add(Calendar.DAY_OF_MONTH, -1);
                }
                prom.setEndDateCal(clEndDate);
                prom.startDate = DateUtil.formatDateTimePromo(clStartDate.getTime());
                prom.startDate = prom.startDate.substring(0, 10);
                prom.endDate = DateUtil.formatDateTimePromo(clEndDate.getTime());
                prom.endDate = prom.endDate.substring(0, 10);
                if (prom.hourOffset != 0) {
                    prom.hourOffset = 24 - prom.hourOffset;
                }
                if (prom.hourOffsetEnd != 0) {
                    prom.hourOffsetEnd = 24 - prom.hourOffsetEnd;
                }

                prom.active = rs.getInt("ActivePromo");
                prom.promoId = rs.getInt("PromoID");
                try {
                    isotemp.setIsoID(rs.getLong("Rep_ID"));
                } catch (Exception e) {
                    isotemp.setIsoID(-1);
                }
                try {
                    isotemp.setIsoName(rs.getString("BusinessName"));
                } catch (Exception e) {
                    isotemp.setIsoName("");
                }
                prom.setISO(isotemp);
                prom.priority = rs.getInt("Priority");
                prom.merchantCountry = rs.getString("merchantcountry");
                prom.adText = rs.getString("additionalreceipttext");
                prom.chances = rs.getInt("chances");
                prom.outOf = rs.getInt("outof");
                prom.data = rs.getString("data");
                prom.status = rs.getInt("status");
                prom.minBalRequired = rs.getDouble("minBalRequired");
                prom.lastDayMonth = rs.getInt("lastDayMonth");
                prom.setPromoIdNew(rs.getString("PromoIdNew"));
                prom.setQrCodeOnly(rs.getBoolean("qrCodeOnly"));
                
                
                prom.setMinStart(rs.getInt("MINSTART"));
                prom.setMinEnd(rs.getInt("MINEND"));
                if ( rs.getString("RECURRENCETYPE").equals(RECURRENCE_PATTERN.MONTHLY.toString()) ){
                    prom.setRecurrenceType(RECURRENCE_PATTERN.MONTHLY);
                } else if ( rs.getString("RECURRENCETYPE").equals(RECURRENCE_PATTERN.WEEKLY.toString()) ){
                    prom.setRecurrenceType(RECURRENCE_PATTERN.WEEKLY);
                } else{
                    prom.setRecurrenceType(RECURRENCE_PATTERN.NONE);
                }
                prom.setRecurrencePattern(rs.getString("RECURRENCEPATTERN"));
                prom.setConcatenatedPromo(rs.getBoolean("isConcatenatedPromo"));
                prom.setConcatenatedPromoInterval(rs.getInt("ConcatenatedPromoInterval"));                
            }
        } catch (Exception e) {
            cat.error("Error during GetPromotionInfo", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return prom;
    }

    public static Vector<Vector> GetMerchants(int promoID) throws PromotionException {

        Vector<Vector> vecReturn = new Vector<Vector>();

        Vector<Merchant> vecAutMerchants = new Vector<Merchant>();
        Vector<Merchant> vecNonMerchants = new Vector<Merchant>();
        Vector<Merchant> vecAutTestMerchants = new Vector<Merchant>();
        Vector<Merchant> vecNonTestMerchants = new Vector<Merchant>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int iAut = -1, iTest = -1;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetMerchants");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, promoID);
            pstmt.setInt(2, promoID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                Merchant tempMer = new Merchant();
                iAut = rs.getInt("Authorized");
                iTest = rs.getInt("TestMerchant");
                tempMer.setMerchantID(rs.getLong("merchantid"));
                tempMer.setMerchantName(rs.getString("merchantname"));
                if ((iAut == 1) && (iTest == 0)) {
                    vecAutMerchants.add(tempMer);
                } else if ((iAut == 0) && (iTest == 0)) {
                    vecNonMerchants.add(tempMer);
                } else if ((iAut == 1) && (iTest == 1)) {
                    vecAutTestMerchants.add(tempMer);
                } else if ((iAut == 0) && (iTest == 1)) {
                    vecNonTestMerchants.add(tempMer);
                }
            }
            vecReturn.add(vecAutMerchants);
            vecReturn.add(vecNonMerchants);
            vecReturn.add(vecAutTestMerchants);
            vecReturn.add(vecNonTestMerchants);
        } catch (Exception e) {
            cat.error("Error during GetMerchants", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Vector<Vector> GetAccounts(int promoID) throws PromotionException {

        Vector<Vector> vecReturn = new Vector<Vector>();

        Vector<String> vecAutAccounts = new Vector<String>();
        Vector<String> vecAutTestAccounts = new Vector<String>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int iTest = -1;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetAccounts");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, promoID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                String tempAccount = rs.getString("ACCOUNTNUMBER");
                iTest = rs.getInt("TEST");
                if (iTest == 0) {
                    vecAutAccounts.add(tempAccount);
                } else if (iTest == 1) {
                    vecAutTestAccounts.add(tempAccount);
                }
            }
            vecReturn.add(vecAutAccounts);
            vecReturn.add(vecAutTestAccounts);

        } catch (Exception e) {
            cat.error("Error during GetAccountN", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Vector<Vector> GetSites(int promoID) throws PromotionException {

        Vector<Vector> vecReturn = new Vector<Vector>();

        Vector<Integer> vecAutSites = new Vector<Integer>();
        Vector<Integer> vecNonSites = new Vector<Integer>();
        Vector<Integer> vecAutTestSites = new Vector<Integer>();
        Vector<Integer> vecNonTestSites = new Vector<Integer>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int iAut = -1, iTest = -1;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetSites");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, promoID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                int tempSite = rs.getInt("millenniumno");
                iAut = rs.getInt("Authorized");
                iTest = rs.getInt("TestSite");
                if ((iAut == 1) && (iTest == 0)) {
                    vecAutSites.add(tempSite);
                } else if ((iAut == 0) && (iTest == 0)) {
                    vecNonSites.add(tempSite);
                } else if ((iAut == 1) && (iTest == 1)) {
                    vecAutTestSites.add(tempSite);
                } else if ((iAut == 0) && (iTest == 1)) {
                    vecNonTestSites.add(tempSite);
                }
            }
            vecReturn.add(vecAutSites);
            vecReturn.add(vecNonSites);
            vecReturn.add(vecAutTestSites);
            vecReturn.add(vecNonTestSites);
        } catch (Exception e) {
            cat.error("Error during GetSites", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Vector<Merchant> GetMerchantsByRep(long repID) throws PromotionException {

        Vector<Merchant> vecReturn = new Vector<Merchant>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetMerchantsByRep");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setLong(1, repID);
            cat.debug(strSQLSummary + "/*" + Long.toString(repID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                Merchant tempMer = new Merchant();
                tempMer.setMerchantID(rs.getLong("MERCHANT_ID"));
                tempMer.setMerchantName(rs.getString("LEGAL_BUSINESSNAME"));
                vecReturn.add(tempMer);
            }

        } catch (Exception e) {
            cat.error("Error during GetMerchantsByRep", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Vector<PromThreshold> GetPromoThresholds(int promID) throws PromotionException {

        Vector<PromThreshold> vecReturn = new Vector<PromThreshold>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetPromoThresholds");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, promID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                PromThreshold tempPrThres = new PromThreshold();
                tempPrThres.setPromoID(promID);
                tempPrThres.setAddReceipt(rs.getString("ADDTEXT"));
                tempPrThres.setAwardTypeID(rs.getInt("AWARDTYPEID"));
                tempPrThres.setBonusAmount(rs.getFloat("BONUS"));
                tempPrThres.setConsAwardTypeID(rs.getInt("CONSOLATIONAWARDTYPEID"));
                tempPrThres.setTransAmountMin(rs.getFloat("AMOUNT"));
                tempPrThres.setRecID(rs.getInt("RECID"));
                tempPrThres.setMinAmount(NumberUtil.formatAmount(rs.getString("AMOUNT")));
                tempPrThres.setBonus(NumberUtil.formatAmount(rs.getString("BONUS")));
                tempPrThres.setThresholdChances(rs.getInt("chances"));
                tempPrThres.setThresholdOutOf(rs.getInt("outOf"));
                tempPrThres.setThreshChances(rs.getString("chances"));
                tempPrThres.setThreshOutOf(rs.getString("outOf"));
                vecReturn.add(tempPrThres);
            }

        } catch (Exception e) {
            cat.error("Error during GetPromoThresholds", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Vector<PromoDetails> GetPromoDetails(int thresholdId) throws PromotionException {

        Vector<PromoDetails> vecReturn = new Vector<PromoDetails>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetPromoThresholdDetails");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, thresholdId);
            cat.debug(strSQLSummary + "/*" + Integer.toString(thresholdId) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                PromoDetails tempPrThresDetail = new PromoDetails();
                tempPrThresDetail.setThresholdId(thresholdId);
                tempPrThresDetail.setDetailsId(rs.getInt("details_id"));
                tempPrThresDetail.setPropertyName(rs.getString("property_name"));
                tempPrThresDetail.setPropertyValue(rs.getString("property_value"));
                vecReturn.add(tempPrThresDetail);
            }

        } catch (Exception e) {
            cat.error("Error during GetPromoDetails", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Vector<Promotion> GetPromoList(long isoID, int productID, long carrierID) throws PromotionException {

        Vector<Promotion> result = new Vector<Promotion>();
        Vector<Promotion> finalResult = new Vector<Promotion>();
        Map<Integer, String> promoProductMap = new HashMap<Integer, String>();
        List<Integer> promoList = new ArrayList<Integer>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetPromotions");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setLong(1, isoID);
            pstmt.setInt(2, productID);
            pstmt.setLong(3, carrierID);
            cat.debug(strSQLSummary + "/*" + Long.toString(isoID) + "," + Integer.toString(productID) + "," + Long.toString(carrierID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                Promotion prom = new Promotion();
                Product prod = new Product();
                ISO isotemp = new ISO();
                prod.setProductID(rs.getInt("productID"));
                prod.setProductDes(rs.getString("description"));
                prom.setProduct(prod);
                prom.description = rs.getString("promodescription");
                prom.startDate = DateUtil.formatDateTimePromo(rs.getTimestamp("startdate"));
                prom.endDate = DateUtil.formatDateTimePromo(rs.getTimestamp("enddate"));
                prom.active = rs.getInt("ActivePromo");
                prom.promoId = rs.getInt("PromoID");
                isotemp.setIsoID(rs.getLong("ISOID"));
                isotemp.setIsoName(rs.getString("BusinessName"));
                prom.setISO(isotemp);
                prom.priority = rs.getInt("Priority");
                if (promoProductMap.containsKey(rs.getInt("PromoID"))) {
                    promoProductMap.put(rs.getInt("PromoID"), "m");
                } else {
                    promoProductMap.put(rs.getInt("PromoID"), "s");

                }
                result.add(prom);

            }

            Iterator iterator = result.iterator();
            while (iterator.hasNext()) {
                Promotion p = null;
                p = (Promotion) iterator.next();
                if (promoProductMap.get(p.getPromoId()).equals("s") || promoProductMap.get(p.getPromoId()).equals("m")) {

                    if (promoProductMap.get(p.getPromoId()).equals("m")) {
                        Product prod = new Product();
                        prod.setProductID(-2);
                        prod.setProductDes("Multiple");
                        p.setProduct(prod);
                        promoProductMap.put(p.getPromoId(), "ms");
                    }
                    finalResult.add(p);

                }

            }

        } catch (Exception e) {
            cat.error("Error during GetPromotions", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return finalResult;
    }

    public static Vector<PromotionLog> GetPromoLog(int promoID) throws PromotionException {

        Vector<PromotionLog> result = new Vector<PromotionLog>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetPromotionLog");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setString(1, Integer.toString(promoID));
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                PromotionLog promlog = new PromotionLog();
                promlog.setPromotionID(rs.getInt("PRID"));
                promlog.setDateTime(DateUtil.formatDateTimePromo(rs.getTimestamp("DATE_TIME")));
                promlog.setOldValue(rs.getString("OLD_VALUE"));
                promlog.setNewValue(rs.getString("NEW_VALUE"));
                promlog.setUserName(rs.getString("USER_NAME"));
                promlog.setChangeName(rs.getString("NAME"));
                promlog.setTableName(rs.getString("TABLE_NAME"));
                promlog.setFieldChanged(rs.getString("FIELD_CHANGED"));
                promlog.setPromoName(rs.getString("PROMODESCRIPTION"));
                promlog.setRegistryID(rs.getInt("ID"));

                String key_value = rs.getString("KEY_VALUE");
                if (promlog.getTableName().equals("PROMO_THRESHOLDS")) {
                    String[] strSplitted = key_value.split(";");
                    if (NumberUtil.isNumeric(strSplitted[1])) {
                        promlog.setMinTransaction(Float.parseFloat(NumberUtil.formatAmount(strSplitted[1])));
                    }
                } else if (promlog.getTableName().equals("PROMO_ACCOUNTNUMBER")) {
                    String[] strSplitted = key_value.split(";");
                    promlog.setAccountNumber(strSplitted[1]);
                    if (promlog.getChangeName().equals("Account Number Registry")) {
                        if (NumberUtil.isNumeric(strSplitted[2])) {
                            promlog.setAccountTest(Integer.parseInt(strSplitted[2]));
                        }
                    }
                } else if (promlog.getTableName().equals("PROMO_MERCHANT")) {
                    String[] strSplitted = key_value.split(";");
                    if (NumberUtil.isNumeric(strSplitted[1])) {
                        promlog.setMerchantID(Long.parseLong(strSplitted[1]));
                    }
                    if (NumberUtil.isNumeric(strSplitted[2])) {
                        promlog.setMerchantTest(Integer.parseInt(strSplitted[2]));
                    }
                    if (promlog.getChangeName().equals("Merchant Registry")) {
                        if (NumberUtil.isNumeric(strSplitted[3])) {
                            promlog.setAuthMerchant(Integer.parseInt(strSplitted[3]));
                        }
                        if (NumberUtil.isNumeric(strSplitted[4])) {
                            promlog.setAllMerchant(Integer.parseInt(strSplitted[4]));
                        }
                    }
                } else if (promlog.getTableName().equals("PROMO_SITE")) {
                    String[] strSplitted = key_value.split(";");
                    if (NumberUtil.isNumeric(strSplitted[1])) {
                        promlog.setSiteID(Integer.parseInt(strSplitted[1]));
                    }
                    if (NumberUtil.isNumeric(strSplitted[2])) {
                        promlog.setSiteTest(Integer.parseInt(strSplitted[2]));
                    }
                    if (promlog.getChangeName().equals("Site Registry")) {
                        if (NumberUtil.isNumeric(strSplitted[3])) {
                            promlog.setAuthSite(Integer.parseInt(strSplitted[3]));
                        }
                        if (NumberUtil.isNumeric(strSplitted[4])) {
                            promlog.setAllSite(Integer.parseInt(strSplitted[3]));
                        }
                    }
                }

                result.add(promlog);

            }
        } catch (Exception e) {
            cat.error("Error during GetPromotionLog", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return result;
    }

    public static String GetServerDate() throws PromotionException {

        String result = "";

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetServerDate");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            cat.debug(strSQLSummary);
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                result = DateUtil.formatDateTimePromo(rs.getTimestamp(1));
            }
        } catch (Exception e) {
            cat.error("Error during GetServerDate", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return result;
    }

    public static Vector<Company> GetCompanies(long companyID, int level) throws PromotionException {

        Vector<Company> result = new Vector<Company>();

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetCompanies");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, level);
            pstmt.setLong(2, companyID);
            cat.debug(strSQLSummary + "/*" + Long.toString(companyID) + "," + Integer.toString(level) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                Company compTemp = new Company();
                compTemp.companyName = rs.getString("businessname");
                compTemp.companyID = rs.getLong("rep_id");
                result.add(compTemp);
            }
        } catch (Exception e) {
            cat.error("Error during GetCompanies", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return result;
    }

    public void valueBound(HttpSessionBindingEvent event) {

    }

    public void valueUnbound(HttpSessionBindingEvent event) {

    }

    public static int GetHighestPriority(String productId) throws PromotionException {

        Connection dbConn = null;

        String strSQLSummary = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int priority = -1;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            strSQLSummary = sql_bundle.getString("GetHighestPriority");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, Integer.valueOf(productId));
            cat.debug(strSQLSummary + "/*" + productId + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                priority = rs.getInt("priority");

            }
        } catch (Exception e) {
            cat.error("Error during GetHighestPriority", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return priority;
    }

    public static Vector GetProducts(int promoID) throws PromotionException {

        Vector vecReturn = new Vector();

        CallableStatement pstmt = null;
        ResultSet rs = null;
        String strSQLSummary = "";
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
            formatter.setLenient(false);

            strSQLSummary = sql_bundle.getString("GetProducts");
            pstmt = dbConn.prepareCall(strSQLSummary);
            pstmt.setInt(1, promoID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getInt("PRODUCTID"));
                vecTemp.add(rs.getString("Description"));
                vecReturn.add(vecTemp);
            }

        } catch (Exception e) {
            cat.error("Error during GetProducts", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static Vector getISO(HttpServletRequest request, SessionData sessionData) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        Vector vecResults = new Vector();

        String strSQL = sql_bundle.getString("getISO");

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            cat.debug(strSQL);

            pstmt = dbConn.prepareStatement(strSQL);
            String isoId = sessionData.getProperty("iso_id");
            pstmt.setString(1, isoId);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {

                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("businessname").trim());
                vecTemp.add(rs.getLong("rep_id"));
                vecResults.add(vecTemp);

            }
            rs.close();
            pstmt.close();

        } catch (Exception e) {
            cat.error("Error during getISO", e);
            throw new ReportException();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecResults;
    }

    public static void saveTree(int promID, String entity, SessionData sessionData, String instance) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String[] entityList;
        String[] entityTypes;
        String entityType = "";
        String parentId = "";
        String entityId = "";
        String status = "";
        int isTest = 0;
        int isLive = 0;
        String type = "";
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            dbConn.setAutoCommit(false);
            pstmt = dbConn.prepareStatement(sql_bundle.getString("updateDeleteRecord"));
            pstmt.setInt(1, promID);
            pstmt.execute();
            cat.debug(pstmt.getWarnings());

            entityList = entity.split(",");
            int typeId = 0;
            for (int i = 0; i < entityList.length; i++) {
                entityTypes = entityList[i].split("-");
                entityId = entityTypes[0];
                entityType = entityTypes[1];
                parentId = entityTypes[2];
                status = entityTypes[3];
                if (entityType.equals("i")) {
                    type = "ISO";
                } else if (entityType.equals("a")) {
                    type = "Agent";
                } else if (entityType.equals("sa")) {
                    type = "SubAgent";
                } else if (entityType.equals("r")) {
                    type = "Rep";
                } else if (entityType.equals("m")) {
                    type = "Merchant";
                } else {
                    type = "Site";
                }

                if (status.equals("t")) {
                    isTest = 1;
                    isLive = 0;
                } else if (status.equals("p")) {
                    isLive = 1;
                    isTest = 0;
                } else if (status.equals("pr")) {
                    isLive = 2;
                    isTest = 0;
                } else if (status.equals("d")) {
                		isLive = 0;
                		isTest = 0;
                }

                pstmt = dbConn.prepareStatement(sql_bundle.getString("getEntity"));
                pstmt.setString(1, type);
                rs = pstmt.executeQuery();
                cat.debug(pstmt.getWarnings());
                while (rs.next()) {
                    typeId = rs.getInt("type_id");
                }

                pstmt = dbConn.prepareStatement(sql_bundle.getString("checkIfEntityExists"));
                pstmt.setLong(1, Long.parseLong(entityId));
                pstmt.setInt(2, promID);
                pstmt.setInt(3, typeId);
                pstmt.setLong(4, Long.valueOf(parentId));
                rs = pstmt.executeQuery();
                int resultCount = 0;

                while (rs.next()) {
                    resultCount++;
                }

                if (resultCount == 0) {
                    pstmt = dbConn.prepareStatement(sql_bundle.getString("saveEntity"));
                    pstmt.setLong(1, Long.valueOf(entityId));
                    pstmt.setInt(2, promID);
                    pstmt.setInt(3, typeId);
                    pstmt.setInt(4, isLive);
                    pstmt.setInt(5, isTest);
                    pstmt.setLong(6, Long.valueOf(parentId));
                    pstmt.setInt(7, 0);
                    pstmt.addBatch();
                    cat.debug(pstmt + "--" + Integer.toString(promID) + "," + entityId + "--" + "parentId" + parentId);
                    pstmt.executeBatch();
                    cat.debug(pstmt.getWarnings());

                } else {
                    pstmt = dbConn.prepareStatement(sql_bundle.getString("updateEntity"));
                    pstmt.setInt(1, isLive);
                    pstmt.setInt(2, isTest);
                    pstmt.setInt(3, promID);
                    pstmt.setLong(4, Long.valueOf(entityId));
                    pstmt.setLong(5, typeId);
                    pstmt.addBatch();
                    cat.debug(pstmt + "--" + Integer.toString(promID) + "," + entityId + "--");
                    pstmt.executeBatch();
                    cat.debug(pstmt.getWarnings());

                }

            }
            dbConn.commit();
            dbConn.setAutoCommit(true);
            pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteUnModifiedEntities"));
            pstmt.setInt(1, promID);
            pstmt.execute();
            rs.close();
            pstmt.close();

        } catch (Exception e) {
            cat.info(e.getStackTrace());
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                cat.error("Error during rollback saveEntity", ex);
            }
            cat.error("Error during saveEntity", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }

        return;
    }

    public static String getTree(int promoId, SessionData sessionData) throws PromotionException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = "";
        Vector vecSiteResults = new Vector();
        Vector vecISOResults = new Vector();
        Vector vecAgentResults = new Vector();
        Vector vecSubAgentResults = new Vector();
        Vector vecRepResults = new Vector();
        Vector vecMerResults = new Vector();
        int resultCount = 0;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            pstmt = dbConn.prepareStatement(sql_bundle.getString("getTreeStructure"));
            pstmt.setInt(1, promoId);
            pstmt.setInt(2, promoId);
            pstmt.setInt(3, promoId);
            pstmt.setInt(4, promoId);
            pstmt.setInt(5, promoId);
            pstmt.setInt(6, promoId);
            pstmt.setInt(7, promoId);
            pstmt.setInt(8, promoId);
            cat.debug(sql_bundle.getString("getTreeStructure"));
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    resultCount++;
                    Vector vecTemp = new Vector();
                    vecTemp.add(rs.getLong("levelid"));
                    vecTemp.add(rs.getString("name").trim().replace("'", "\\'"));
                    vecTemp.add(rs.getInt("levelType"));
                    vecTemp.add(rs.getBoolean("isLive"));
                    vecTemp.add(rs.getBoolean("isTest"));
                    vecTemp.add(rs.getLong("parentID"));
                    if (rs.getInt("levelType") == 1) {
                        vecISOResults.add(vecTemp);
                    }
                    if (rs.getInt("levelType") == 2) {
                        vecAgentResults.add(vecTemp);
                    }
                    if (rs.getInt("levelType") == 3) {
                        vecSubAgentResults.add(vecTemp);
                    }
                    if (rs.getInt("levelType") == 4) {
                        vecRepResults.add(vecTemp);
                    }
                    if (rs.getInt("levelType") == 5) {
                        vecMerResults.add(vecTemp);
                    }
                    if (rs.getInt("levelType") == 6) {
                        vecSiteResults.add(vecTemp);
                    }

                }
                rs.close();
                pstmt.close();

                if (resultCount == 0) {
                    return result;
                }
                Iterator it = vecISOResults.iterator();
                if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    while (it.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        result = "[{'data':'" + vecTemp.get(1).toString() + "','attr':{'id':'" + vecTemp.get(0) + "-i-" + vecTemp.get(0) + "','class':''},'state':'open','metadata':{},'children':[";
                        Iterator itAgent = vecAgentResults.iterator();
                        while (itAgent.hasNext()) {
                            Vector vecAgentTemp = null;
                            vecAgentTemp = (Vector) itAgent.next();
                            result = result + "{'data':'" + vecAgentTemp.get(1).toString() + "','attr':{'id':'" + vecAgentTemp.get(0) + "-a-" + vecAgentTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                            result = result + ",'children' : [";
                            Iterator itSubAgent = vecSubAgentResults.iterator();
                            //int hasSubagent = 0;
                            while (itSubAgent.hasNext()) {

                                Vector vecSubAgentTemp = null;
                                vecSubAgentTemp = (Vector) itSubAgent.next();
                                if (vecSubAgentTemp.get(5).equals(vecAgentTemp.get(0))) {
                                    //hasSubagent=1;
                                    result = result + "{'data':'" + vecSubAgentTemp.get(1).toString() + "','attr':{'id':'" + vecSubAgentTemp.get(0) + "-sa-" + vecSubAgentTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                                    result = result + ",children : [";
                                    Iterator itRep = vecRepResults.iterator();
                                    //int hasRep=0;
                                    while (itRep.hasNext()) {
                                        Vector vecRepTemp = null;
                                        vecRepTemp = (Vector) itRep.next();
                                        if (vecRepTemp.get(5).equals(vecSubAgentTemp.get(0))) {
                                            //hasRep=1;
                                            result = result + "{'data':'" + vecRepTemp.get(1).toString() + "','attr':{'id':'" + vecRepTemp.get(0) + "-r-" + vecRepTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                                            result = result + ",children : [";
                                            Iterator itMer = vecMerResults.iterator();
					 	          		//int hasMerchant=0;

                                            while (itMer.hasNext()) {
                                                Vector vecMerTemp = null;
                                                vecMerTemp = (Vector) itMer.next();
                                                if (vecMerTemp.get(5).equals(vecRepTemp.get(0))) {
                                                    //hasMerchant=1;
                                                    result = result + "{'data':'" + vecMerTemp.get(1).toString() + "','attr':{'id':'" + vecMerTemp.get(0) + "-m-" + vecMerTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                                                    result = result + ",children : [";
                                                    Iterator itSite = vecSiteResults.iterator();
                                                    //int hasSite=0;
                                                    while (itSite.hasNext()) {
                                                        Vector vecSiteTemp = null;
                                                        vecSiteTemp = (Vector) itSite.next();

                                                        if (vecSiteTemp.get(5).equals(vecMerTemp.get(0))) {
                                                            //hasSite=1;
                                                            result = result + "{'data':'" + vecSiteTemp.get(1).toString() + "','attr':{'id':'" + vecSiteTemp.get(0) + "-s-" + vecSiteTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                                                            //result= result+",children : [";
                                                            result = result + "},";// Mer
                                                        }
                                                    }
                                                    //if (hasSite==1)
                                                    if (result.substring(result.length() - 2, result.length() - 1) == ",") {
                                                        result = result.substring(0, result.length() - 1);
                                                    }
                                                    result = result + "]";
                                                    result = result + "},";

                                                }
                                            }
                                            //if (hasMerchant==1)
                                            if (result.substring(result.length() - 2, result.length() - 1) == ",") {
                                                result = result.substring(0, result.length() - 1);
                                            }
                                            result = result + "]";
                                            result = result + "},";

                                            //result = result +  "},";// Rep
                                        }

                                    }
                                    //if (hasRep==1)
                                    if (result.substring(result.length() - 2, result.length() - 1) == ",") {
                                        result = result.substring(0, result.length() - 1);
                                    }
                                    result = result + "]";
                                    result = result + "},";

                                    //result = result +  "},";
                                }
                            }// End of sub
                            if (result.substring(result.length() - 2, result.length() - 1) == ",") {
                                result = result.substring(0, result.length() - 1);
                            }
                            result = result + "]";
                            result = result + "},";
                        }// End of Agent
                        if (result.substring(result.length() - 2, result.length() - 1) == ",") {
                            result = result.substring(0, result.length() - 1);
                        }
                        result = result + "]";
                    }// end of ISO
                    result = result + "}]";

                } else {
                    while (it.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) it.next();
                        result = "[{'data':'" + vecTemp.get(1).toString() + "','attr':{'id':'" + vecTemp.get(0) + "-i-" + vecTemp.get(0) + "','class':''},'state':'open','metadata':{},'children':[";
                        Iterator itRep = vecRepResults.iterator();
                        while (itRep.hasNext()) {
                            Vector vecRepTemp = null;
                            vecRepTemp = (Vector) itRep.next();
                            result = result + "{'data':'" + vecRepTemp.get(1).toString() + "','attr':{'id':'" + vecRepTemp.get(0) + "-r-" + vecRepTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                            result = result + ",'children' : [";
                            Iterator itMer = vecMerResults.iterator();
                            int hasMerchant = 0;

                            while (itMer.hasNext()) {
                                Vector vecMerTemp = null;
                                vecMerTemp = (Vector) itMer.next();
                                if (vecMerTemp.get(5).equals(vecRepTemp.get(0))) {

                                    hasMerchant = 1;
                                    result = result + "{'data':'" + vecMerTemp.get(1).toString() + "','attr':{'id':'" + vecMerTemp.get(0) + "-m-" + vecMerTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                                    result = result + ",children : [";
                                    Iterator itSite = vecSiteResults.iterator();
                                    int hasSite = 0;
                                    while (itSite.hasNext()) {
                                        Vector vecSiteTemp = null;
                                        vecSiteTemp = (Vector) itSite.next();

                                        if (vecSiteTemp.get(5).equals(vecMerTemp.get(0))) {
                                            hasSite = 1;
                                            result = result + "{'data':'" + vecSiteTemp.get(1).toString() + "','attr':{'id':'" + vecSiteTemp.get(0) + "-s-" + vecSiteTemp.get(5) + "','class':''},'state':'open','metadata':{}";
                                            //result= result+",children : [";
                                            result = result + "},";// Mer
                                        }
                                    }
                                    if (hasSite == 1) {
                                        result = result.substring(0, result.length() - 1);
                                    }
                                    result = result + "]";
                                    result = result + "},";

                                }

                            }
                            if (hasMerchant == 1);
                            result = result + "]";
                            result = result + "},";

                        }
                        if (result.substring(result.length() - 2, result.length() - 1) == ",") {
                            result = result.substring(0, result.length() - 1);
                        }
                        result = result + "]";
                    }// end of ISO
                    result = result + "}]";

                }

            }

        } catch (Exception e) {
            cat.error("Error during GetTree", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return result;
    }

    public static String getCheckedNodes(int promoId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = "";
        Vector vecResults = new Vector();
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }
            pstmt = dbConn.prepareStatement(sql_bundle.getString("getCheckedEntities"));
            pstmt.setInt(1, promoId);
            cat.debug(sql_bundle.getString("getCheckedEntities"));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getLong("levelid"));
                if (rs.getInt("levelType") == 1) {
                    vecTemp.add("i");
                } else if (rs.getInt("levelType") == 2) {
                    vecTemp.add("a");
                } else if (rs.getInt("levelType") == 3) {
                    vecTemp.add("sa");
                } else if (rs.getInt("levelType") == 4) {
                    vecTemp.add("r");
                } else if (rs.getInt("levelType") == 5) {
                    vecTemp.add("m");
                } else if (rs.getInt("levelType") == 6) {
                    vecTemp.add("s");
                }

                vecTemp.add(rs.getLong("parentID"));
                vecResults.add(vecTemp);

            }
            rs.close();
            pstmt.close();

            Iterator it = vecResults.iterator();
            while (it.hasNext()) {
                Vector temp = null;
                temp = (Vector) it.next();
                if (result != "") {
                    result = result + "'#" + temp.get(0) + "-" + temp.get(1) + "-" + temp.get(2) + "',";
                } else {
                    result = "'#" + temp.get(0) + "-" + temp.get(1) + "-" + temp.get(2) + "',";
                }

            }
            if (result != "") {
                result = result.substring(0, result.length() - 1);
            }
        } catch (Exception e) {
            cat.error("Error during GetCheckedTree", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }

        return result;
    }

    public String GetProductsForEmail(int promoID) throws PromotionException {

        String vecReturn = "";

        CallableStatement pstmt = null;
        ResultSet rs = null;
        String strSQLSummary = "";
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
            formatter.setLenient(false);

            strSQLSummary = sql_bundle.getString("GetProducts");
            pstmt = dbConn.prepareCall(strSQLSummary);
            pstmt.setInt(1, promoID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                String productDescription = rs.getString("Description");
                if (vecReturn != "") {
                    vecReturn = vecReturn + "," + productDescription;
                } else {
                    vecReturn = productDescription;
                }
            }

        } catch (Exception e) {
            cat.error("Error during GetProductsForEmail", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecReturn;
    }

    public static void sendEmail(String mailHost, Promotion promotion, String userName, ServletContext context, String sMails, String recurrenceValue) throws Exception {
        Vector vecPromoThresholds = Promotion.GetPromoThresholds(promotion.getPromoId());
        String sPromoThresholds = "";
        Iterator it = null;
        String sHeader = "<body bgcolor=\"#ffffff\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #f1f9fe;width:700px;\"><tr><td><b>Promo ID: _PromoId_</b></td></tr><tr><td><b>Edited by User : _USERNAME_ </b></td></tr>";
        String sPromoDetails = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Promo Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\">"
                + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Promo Description:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_PROMONAME_</td></tr>"
                + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Products:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_PRODUCTS_</td></tr>"
                + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Recurrence Type:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_RECURRENCE_TYPE_</td></tr>"
                + "<tr><td style=\"font-size: 11pt;\" nowrap><b></b></td><td>&nbsp;</td><td>_RECURRENCE_VALUE_</td></tr>"
                + "<tr><td style=\"font-size: 11pt;\" nowrap><b>StartDate:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_STARTDATE_</td></tr>"
                + "<tr><td style=\"font-size: 11pt;\" nowrap><b>End Date:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_ENDDATE_</td></tr>"
                + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Promo Status:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PROMOSTATUS_</td></tr></table></td></tr>";
        String sPromoThresholdsHeader = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Promo Thresholds</td></tr>";
        String sPromoThresholdComplete = "";
        it = vecPromoThresholds.iterator();
        while (it.hasNext()) {
            sPromoThresholds = "<tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\">"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Minmum Amount:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_MINAMOUNT_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Bonus Value:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_BONUSAMOUNT_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Additional Text:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_ADDITIONALTEXT_</td></tr></table></td></tr>";

            PromThreshold promTemp = (PromThreshold) it.next();
            sPromoThresholds = sPromoThresholds.replace("_MINAMOUNT_", String.valueOf("$" + promTemp.getMinAmount()));
            if (promTemp.getBonusAmount() >= 0) {
                sPromoThresholds = sPromoThresholds.replace("_BONUSAMOUNT_", String.valueOf("$" + promTemp.getBonus()));
            } else {
                sPromoThresholds = sPromoThresholds.replace("_BONUSAMOUNT_", String.valueOf(NumberUtil.formatAmount(Float.toString(promTemp.getBonusAmount() * -100)) + "%"));
            }
            sPromoThresholds = sPromoThresholds.replace("_ADDITIONALTEXT_", String.valueOf(promTemp.getAddReceipt()));
            sPromoThresholdComplete = sPromoThresholdComplete + sPromoThresholds;
        }

        String sPromoEntityHeader = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Note : To see the configured Promo Entities please go to Promo section of support site.</td></tr>";

        String sFooter = "</table></body>";

        sHeader = sHeader.replaceAll("_PromoId_", String.valueOf(promotion.getPromoId())).replace("_USERNAME_", userName);

        sPromoDetails = sPromoDetails.replace("_PROMONAME_", String.valueOf(promotion.getDescription()));
        sPromoDetails = sPromoDetails.replace("_PRODUCTS_", String.valueOf(promotion.GetProductsForEmail(promotion.getPromoId())));
        switch (promotion.getRecurrenceType()) {
            case NONE:
                sPromoDetails = sPromoDetails.replace("_RECURRENCE_TYPE_", RECURRENCE_PATTERN.NONE.toString());
                break;
            case WEEKLY:
                sPromoDetails = sPromoDetails.replace("_RECURRENCE_TYPE_", RECURRENCE_PATTERN.WEEKLY.toString());
                break;
            case MONTHLY:
                sPromoDetails = sPromoDetails.replace("_RECURRENCE_TYPE_", RECURRENCE_PATTERN.MONTHLY.toString());
                break;
            default:
                break;
        }
        sPromoDetails = sPromoDetails.replace("_RECURRENCE_VALUE_", recurrenceValue);      
        
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");
        formatter.setLenient(false);
        Calendar clStartDate = promotion.getStartDateCal();
        clStartDate.add(Calendar.HOUR, promotion.gethourOffset());
        clStartDate.add(Calendar.MINUTE, promotion.getMinStart());
        String dtStartDate = formatter.format(clStartDate.getTime());

        Calendar clEndDate = promotion.getEndDateCal();
        clEndDate.add(Calendar.HOUR, promotion.gethourOffsetEnd());
        clEndDate.add(Calendar.MINUTE, promotion.getMinEnd());
        String dtEndDate = formatter.format(clEndDate.getTime());
                                                    
        sPromoDetails = sPromoDetails.replace("_STARTDATE_", dtStartDate);
        sPromoDetails = sPromoDetails.replace("_ENDDATE_", dtEndDate);
        //sPromoDetails = sPromoDetails.replace("_STARTDATE_", String.valueOf(promotion.getStartDate()));
        //sPromoDetails = sPromoDetails.replace("_ENDDATE_", String.valueOf(promotion.getEndDate()));
        String status = "";
        if (promotion.getActive() == 0) {
            status = "Inactive";
        } else {
            status = "Active";
        }
        sPromoDetails = sPromoDetails.replace("_PROMOSTATUS_", status);

        java.util.Properties props;
        props = System.getProperties();
        props.put("mail.smtp.host", mailHost);
        Session s = Session.getDefaultInstance(props, null);

        MimeMessage message = new MimeMessage(s);
        message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
        message.setFrom(new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));

        if (sMails != null) {
            message.setRecipients(Message.RecipientType.CC, sMails.replaceAll(";", ","));
        }

        message.setSubject("Promo status update for: [" + promotion.getPromoId() + "]");
        message.setContent(sHeader + sPromoDetails + sPromoThresholdsHeader + sPromoThresholdComplete + sPromoEntityHeader + sFooter, "text/html");

        Transport.send(message);

    }

    public static String checkEntityConfiguration(int promoID) throws PromotionException {

        String isExists = "";

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String strSQLSummary = "";
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }
            strSQLSummary = sql_bundle.getString("entityConfiguredCheck");
            pstmt = dbConn.prepareStatement(strSQLSummary);
            pstmt.setInt(1, promoID);
            cat.debug(strSQLSummary + "/*" + Integer.toString(promoID) + "*/");
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                isExists = "true";
                break;
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during checkEntityConfiguration", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return isExists;
    }

    /**
     * @return the promoIdNew
     */
    public String getPromoIdNew() {
        return promoIdNew;
    }

    /**
     * @param promoIdNew the promoIdNew to set
     */
    public void setPromoIdNew(String promoIdNew) {
        this.promoIdNew = promoIdNew;
    }

    /**
     *
     * @return @throws PromotionException
     */
    public static ArrayList<TerminalTypePojo> GetTerminalTypes() throws PromotionException {
        ArrayList<TerminalTypePojo> terminalTypes = new ArrayList<TerminalTypePojo>();
        Connection dbConn = null;
        String strSQLGetTerminalTypes = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }
            strSQLGetTerminalTypes = "SELECT tt.terminal_type, tt.description, tt.IdNew FROM terminal_types tt WITH(NOLOCK) ORDER BY tt.description";
            pstmt = dbConn.prepareStatement(strSQLGetTerminalTypes);
            cat.debug("GetTerminalTypes " + strSQLGetTerminalTypes);
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                TerminalTypePojo type = new TerminalTypePojo();
                type.setId(rs.getString("terminal_type"));
                type.setDescripton(rs.getString("description"));
                type.setIdNewTerminalTypeId(rs.getString("IdNew"));
                terminalTypes.add(type);
            }
            pstmt.close();
            rs.close();
        } catch (Exception e) {
            cat.error("Error during GetMerchants", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return terminalTypes;
    }
    
    private static HashMap<String, TerminalTypePojo> GetTerminalTypesHash() throws PromotionException {
        HashMap<String, TerminalTypePojo> terminalTypes = new HashMap<String, TerminalTypePojo>();
        Connection dbConn = null;
        String strSQLGetTerminalTypes = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }
            strSQLGetTerminalTypes = "SELECT tt.terminal_type, tt.description, tt.IdNew FROM terminal_types tt WITH(NOLOCK) ORDER BY tt.description";
            pstmt = dbConn.prepareStatement(strSQLGetTerminalTypes);
            cat.debug("GetTerminalTypes " + strSQLGetTerminalTypes);
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                TerminalTypePojo type = new TerminalTypePojo();
                type.setId(rs.getString("terminal_type"));
                type.setDescripton(rs.getString("description"));
                type.setIdNew(rs.getString("IdNew"));
                terminalTypes.put(type.getIdNew(), type);
            }
            pstmt.close();
            rs.close();
        } catch (Exception e) {
            cat.error("Error during GetMerchants", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return terminalTypes;
    }

    /**
     *
     * @param promoIdNew
     * @return
     * @throws PromotionException
     */
    public static ArrayList<TerminalTypePojo> GetTerminalTypesByPromo(String promoIdNew) throws PromotionException {
        ArrayList<TerminalTypePojo> terminalTypes = null;
        Connection dbConn = null;
        String strSQLGetTerminalTypes = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }
            strSQLGetTerminalTypes = "select tt.terminal_type, tt.description, tt.IdNew, pr.IdNew AS ttIdNew from Promo_Allow_By_TerminalType pr WITH(NOLOCK) \n"
                    + "INNER JOIN terminal_types tt WITH(NOLOCK) ON pr.Terminal_typeIdNew = tt.IdNew\n"
                    + "WHERE pr.PromoIdNew=?";
            pstmt = dbConn.prepareStatement(strSQLGetTerminalTypes);
            cat.debug("GetTerminalTypesByPromo " + strSQLGetTerminalTypes);
            pstmt.setString(1, promoIdNew);
            rs = pstmt.executeQuery();
            cat.debug(pstmt.getWarnings());
            while (rs.next()) {
                TerminalTypePojo type = new TerminalTypePojo();
                type.setId(rs.getString("terminal_type"));
                type.setDescripton(rs.getString("description"));
                type.setIdNewTerminalTypeId(rs.getString("IdNew"));
                type.setIdNew(rs.getString("ttIdNew"));
                if (terminalTypes == null) {
                    terminalTypes = new ArrayList<TerminalTypePojo>();
                }
                terminalTypes.add(type);
            }
            pstmt.close();
            rs.close();
        } catch (Exception e) {
            cat.error("Error during GetMerchants", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return terminalTypes;
    }
    
    /**
     * 
     * @param promoIdNew
     * @param terminalTypesIdNew
     * @param oldPromoAllowTerminalTypesIdNew
     * @param userID
     * @param instance
     * @param terminalTypesInformation
     * @return
     * @throws PromotionException 
     */
    public static boolean UpdateTerminalTypesByPromo(String promoIdNew, 
                                                     String[] terminalTypesIdNew, 
                                                     String oldPromoAllowTerminalTypesIdNew,
                                                     String userID, 
                                                     String instance,
                                                     String promoId) throws PromotionException {
        boolean result = false;
        Connection dbConn = null;        
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<String> toDelete = new ArrayList<String>();
        ArrayList<String> toInsert = new ArrayList<String>();
        ArrayList<String> toNull = new ArrayList<String>();
        String insertLog = "INSERT INTO LOGCHANGES (DATE_TIME,LOG_CHANGE_TYPE_ID,KEY_VALUE,OLD_VALUE,NEW_VALUE,APPLICATION_NAME,USER_NAME,INSTANCE,REASON)"+
                            " VALUES (GETDATE(),?,?,?,?,?,?,?,?)";
        
        try {
            if ( oldPromoAllowTerminalTypesIdNew != null){
                String currentdata[] = oldPromoAllowTerminalTypesIdNew.split(",");
                for (String data : currentdata){
                    boolean foundTerminalType = false;
                    if (data!=null && data.length()>0){
                        String[] data1 = data.split("\\|");
                        String currentIdNew = data1[0];
                        String currentTerminalTypeIdNew = data1[1];
                        if ( terminalTypesIdNew != null){
                            for ( String newTerminalType : terminalTypesIdNew ){
                                if (newTerminalType.equals(currentTerminalTypeIdNew)){
                                    foundTerminalType = true;
                                    //means that terminal type do not was unmarked from UI.
                                    break;
                                }                        
                            }
                        }
                        if ( !foundTerminalType){
                            toDelete.add(currentIdNew+"|"+currentTerminalTypeIdNew);
                        } else{
                            toNull.add(currentTerminalTypeIdNew);
                        }
                    }
                }
            }
            if ( terminalTypesIdNew != null){
                for ( String newTerminalType : terminalTypesIdNew ){
                    if (!toNull.contains(newTerminalType)){
                        toInsert.add(newTerminalType);
                    }                
                }
            }
            if(toInsert.size()>0 || toDelete.size()>0 )
            {
                HashMap<String, TerminalTypePojo> terminalTypesInformation = GetTerminalTypesHash();
                dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    cat.error("Can't get database connection");
                }
                
                String logIdAudit="";                
                pstmt = dbConn.prepareStatement("SELECT LOG_CHANGE_TYPE_ID FROM LOGCHANGESTYPES (NOLOCK) WHERE [NAME] = 'Promo Terminal Types' AND table_name = 'Promo_Allow_By_TerminalType'");
                rs = pstmt.executeQuery();
                if (rs.next()){
                    logIdAudit = rs.getString(1);
                }
                pstmt.close();
                rs.close();
                                                              
                dbConn.setAutoCommit(false);
                if(toInsert.size()>0 )
                {
                    pstmt = dbConn.prepareStatement("INSERT INTO Promo_Allow_By_TerminalType (PromoIdNew,Terminal_typeIdNew) VALUES (?,?)");
                    for(String insertTerminalTypeId : toInsert){
                        pstmt.setString(1, promoIdNew);
                        pstmt.setString(2, insertTerminalTypeId);
                        pstmt.addBatch();                        
                    }
                    pstmt.executeBatch();
                    pstmt.close();
                    
                    ///////////////////////////////////////////////////////////////////////////////////////////////
                    //AUDIT ZONE
                    pstmt = dbConn.prepareStatement(insertLog);
                    for(String insertTerminalTypeId : toInsert){
                        TerminalTypePojo objTerminalType = terminalTypesInformation.get(insertTerminalTypeId);
                        pstmt.setString(1, logIdAudit);
                        pstmt.setString(2, objTerminalType.getDescripton());
                        pstmt.setString(3, "INSERT");
                        pstmt.setNull(4, java.sql.Types.VARCHAR);
                        
                        pstmt.setString(5, "SUPPORT");
                        pstmt.setString(6, userID);
                        pstmt.setString(7, instance);
                        pstmt.setString(8, "Inserting to the promo id = ["+promoId+"]");
                        pstmt.addBatch();                        
                    }                    
                    pstmt.executeBatch();
                    pstmt.close();
                    //////////////////////////////////////////////////////////////////////////////////////
                } 
                if (toDelete.size()>0){                    
                    pstmt = dbConn.prepareStatement("DELETE FROM Promo_Allow_By_TerminalType WHERE IdNew=?");
                    for(String deletetInfo : toDelete){
                        String[] deleteData = deletetInfo.split("\\|");
                        pstmt.setString(1, deleteData[0]);                        
                        pstmt.addBatch();
                    }
                    pstmt.executeBatch();
                    pstmt.close();
                                     
                    ///////////////////////////////////////////////////////////////////////////////////////////////
                    //AUDIT ZONE
                    pstmt = dbConn.prepareStatement(insertLog);
                    for(String deleteTerminalTypeId : toDelete){
                        String[] deleteData = deleteTerminalTypeId.split("\\|");
                        TerminalTypePojo objTerminalType = terminalTypesInformation.get(deleteData[1]);
                        
                        pstmt.setString(1, logIdAudit);
                        pstmt.setString(2, objTerminalType.getDescripton());
                        pstmt.setString(3, "DELETE");
                        pstmt.setNull(4, java.sql.Types.VARCHAR);                        
                        pstmt.setString(5, "SUPPORT");
                        pstmt.setString(6, userID);
                        pstmt.setString(7, instance);
                        pstmt.setString(8, "Deleting from the promo id = ["+promoId+"]");
                        
                        pstmt.addBatch();                        
                    }                    
                    pstmt.executeBatch();
                    pstmt.close();
                    ///////////////////////////////////////////////////////////////////////////////////////////////
                    
                }            
                dbConn.commit();
                dbConn.setAutoCommit(true);
                result = true;
            }
            
        } catch (Exception e) {
             cat.error("Error during UpdateTerminalTypesByPromo", e);
            try{
                dbConn.rollback();
            } catch(Exception ex){
                 cat.error("Error doing rollback ", e);            
            }           
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return result;
    }
}
