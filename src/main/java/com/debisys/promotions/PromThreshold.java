package com.debisys.promotions;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class PromThreshold implements HttpSessionBindingListener {
	int recID = -1;
	int promoID = -1;
	float transAmountMin = 0;
	float bonusAmount = 0;
	String addReceipt = "";
	int awardTypeID = 0;
	int consAwardTypeID =0;
	String minAmount = "";
	String bonus = "";
	int thresholdChances=0;
	int thresholdOutOf=0;
	String threshChances="";
	public String getThreshChances()
	{
		return threshChances;
	}

	public void setThreshChances(String threshChances)
	{
		this.threshChances = threshChances;
	}

	public String getThreshOutOf()
	{
		return threshOutOf;
	}

	public void setThreshOutOf(String threshOutOf)
	{
		this.threshOutOf = threshOutOf;
	}

	String threshOutOf="";


	public int getRecID()	{
		return recID;
	}

	public void setRecID(int RecID) {
		this.recID = RecID;
	}
	
	public int getPromoID()	{
		return promoID;
	}

	public void setPromoID(int PromoID) {
		this.promoID = PromoID;
	}
	
	public String getMinAmount()	{
		return minAmount;
	}

	public void setMinAmount(String MinAmount) {
		this.minAmount = MinAmount;
	}
	
	public String getBonus()	{
		return bonus;
	}

	public void setBonus(String Bonus) {
		this.bonus = Bonus;
	}
	
	public float getTransAmountMin()	{
		return transAmountMin;
	}

	public void setTransAmountMin(float TransAmountMin) {
		this.transAmountMin = TransAmountMin;
	}	
	
	public float getBonusAmount()	{
		return bonusAmount;
	}

	public void setBonusAmount(float BonusAmount) {
		this.bonusAmount = BonusAmount;
	}	
	
	public int getAwardTypeID()	{
		return awardTypeID;
	}

	public void setAwardTypeID(int AwardTypeID) {
		this.awardTypeID = AwardTypeID;
	}		
	
	public String getAddReceipt()	{
		return addReceipt;
	}

	public void setAddReceipt(String AddReceipt) {
		this.addReceipt = AddReceipt;
	}	
	
	public int getConsAwardTypeID()	{
		return consAwardTypeID;
	}

	public void setConsAwardTypeID(int ConsAwardTypeID) {
		this.consAwardTypeID = ConsAwardTypeID;
	}	
	
	
	public void valueBound(HttpSessionBindingEvent event) {
		
	}

	public void valueUnbound(HttpSessionBindingEvent event) {
		
	}	
	
	public int getThresholdChances()
	{
		return thresholdChances;
	}

	public void setThresholdChances(int thresholdChances)
	{
		this.thresholdChances = thresholdChances;
	}
	
	public int getThresholdOutOf()
	{
		return thresholdOutOf;
	}

	public void setThresholdOutOf(int thresholdOutOf)
	{
		this.thresholdOutOf = thresholdOutOf;
	}

}
