package com.debisys.tools.pushMessaging;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class PushMessagingSetup {

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.pushMessaging.sql");

    static Category cat = Category.getInstance(PushMessagingSetup.class);

    public static List<PushMessagingPojo> getPushMessaging(String id) {
        List<PushMessagingPojo> list = new ArrayList<PushMessagingPojo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            String sql = PushMessagingSetup.sql_bundle.getString("getPushMessaging");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                if (id == null || (id != null && id.equalsIgnoreCase(rs.getString("id")))) {
                    PushMessagingPojo obj = new PushMessagingPojo();
                    obj.setId(rs.getString("id"));
                    obj.setPushNotificationComTypesId(rs.getString("pushNotificationComTypesId"));
                    obj.setNotificationDate(rs.getTimestamp("notificationDate"));
                    obj.setAndroidMethod(rs.getBoolean("androidMethod"));
                    obj.setIosMethod(rs.getBoolean("iosMethod"));
                    obj.setTitle(rs.getString("title"));
                    obj.setEnglishMessage(rs.getString("englishMessage"));
                    obj.setSpanishMessage(rs.getString("spanishMessage"));
                    obj.setActive(rs.getBoolean("active"));
                    obj.setGroupList(getPushMessagingProcessGroupsByPushMsg(id));
                    
                    list.add(obj);
                }
            }
        } catch (Exception e) {
            cat.error("Error during getPushMessaging", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public void insertPushMessaging(SessionData sessionData, ServletContext context, PushMessagingPojo program, String[] groupIds) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            program.setId(uuid);
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));

            strSQL = PushMessagingSetup.sql_bundle.getString("insertPushMessaging");

            cat.debug("Insert insertPushMessaging " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, program.getId());
            pst.setString(2, program.getPushNotificationComTypesId());
            pst.setTimestamp(3, new Timestamp(new Date().getTime()));
            pst.setTimestamp(4, program.getNotificationDate());
            pst.setBoolean(5, program.isAndroidMethod());
            pst.setBoolean(6, program.isIosMethod());
            pst.setString(7, program.getTitle());
            pst.setString(8, program.getEnglishMessage());
            pst.setString(9, program.getSpanishMessage());
            pst.setBoolean(10, program.isActive());
            pst.setBoolean(11, program.isSmartPhone());
            pst.executeUpdate();
            
            insertMessagingProcessGroups(uuid, groupIds);
            
        } catch (Exception e) {
            cat.error("Error insertPushMessaging", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void updatePushMessaging(PushMessagingPojo obj, String[] groupIds) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            strSQL = PushMessagingSetup.sql_bundle.getString("updatePushMessaging");

            SimpleDateFormat formatResult = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String sd = (obj.getNotificationDate() != null) ? formatResult.format(new Date(obj.getNotificationDate().getTime())) : null;

            cat.debug("updatePushMessaging " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            pst.setString(1, obj.getPushNotificationComTypesId());
            pst.setString(2, sd);
            pst.setBoolean(3, obj.isAndroidMethod());
            pst.setBoolean(4, obj.isIosMethod());
            pst.setString(5, obj.getTitle());
            pst.setString(6, obj.getEnglishMessage());
            pst.setString(7, obj.getSpanishMessage());
            pst.setBoolean(8, obj.isActive());
            pst.setBoolean(9, obj.isSmartPhone());
            pst.setString(10, obj.getId());
            pst.executeUpdate();
            
            deleteMessagingProcessGroupsByPushMsg(obj.getId());
            insertMessagingProcessGroups(obj.getId(), groupIds);

        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error inserting updatePushMessaging", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void deletePushMessaging(SessionData sessionData, ServletContext context, String pushMessagingId) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            deleteMessagingProcessGroupsByPushMsg(pushMessagingId);
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = PushMessagingSetup.sql_bundle.getString("deletePushMessaging");
            cat.debug("deletePushMessaging " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, pushMessagingId);
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error deletePushMessaging ", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static List<PushNotificationComTypes> getPushNotificationComTypesList(SessionData sessionData, ServletContext context) {
        List<PushNotificationComTypes> list = new ArrayList<PushNotificationComTypes>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String language = sessionData.getLanguage();
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            String sql = PushMessagingSetup.sql_bundle.getString("getpushNotificationComTypesList");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                PushNotificationComTypes obj = new PushNotificationComTypes(rs.getString("id"), (language.equals("english") || language.equals("0"))? rs.getString("name"):rs.getString("nameSpanish"));
                list.add(obj);
            }
        } catch (Exception e) {
            cat.error("Error during getAccountSourceList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static PushNotificationComTypes getPushNotificationComTypesId(String pushNotificationComTypesId) {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            String sql = PushMessagingSetup.sql_bundle.getString("getpushNotificationComTypesById");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, pushNotificationComTypesId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return new PushNotificationComTypes(rs.getString("id"), rs.getString("name"));
            }
        } catch (Exception e) {
            cat.error("Error during getPushNotificationComTypesId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public void enableDisablePushMessaging(String programId) {
        PushMessagingPojo program = getPushMessaging(programId).get(0);
        if (program.isActive()) {
            program.setActive(false);
        } else {
            program.setActive(true);
        }

        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            strSQL = PushMessagingSetup.sql_bundle.getString("enableDisablePushMessaging");

            cat.debug("enableDisable PushMessaging " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            pst.setBoolean(1, program.isActive());
            pst.setString(2, program.getId());
            pst.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error inserting enableDisable PushMessaging", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static List<PushMessagingProcessGroup> getPushMessagingProcessGroupsByPushMsg(String pushMessagingId) {
        List<PushMessagingProcessGroup> list = new ArrayList<PushMessagingProcessGroup>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            String sql = PushMessagingSetup.sql_bundle.getString("getPushMessagingProcessGroupsByPushMsg");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, pushMessagingId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                PushMessagingProcessGroup obj = new PushMessagingProcessGroup();
                obj.setId(rs.getString("id"));
                obj.setPushMessagingId(rs.getString("pushMessagingId"));
                obj.setPushMessagingGroupsId(rs.getString("pushMessagingGroupsId"));
                list.add(obj);
            }
        } catch (Exception e) {
            cat.error("Error during getPushMessagingProcessGroupsByPushMsg", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public List<PushMessagingGroups> getPushMessagingGroups() {
        List<PushMessagingGroups> list = new ArrayList<PushMessagingGroups>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            String sql = PushMessagingSetup.sql_bundle.getString("getPushMessagingGroups");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                PushMessagingGroups obj = new PushMessagingGroups();
                obj.setId(rs.getString("id"));
                obj.setGroupId(rs.getString("groupId"));
                obj.setDescription(rs.getString("description"));
                list.add(obj);
            }
        } catch (Exception e) {
            cat.error("Error during getPushMessagingGroups", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public void insertMessagingProcessGroups(String pushMessagingId, String[] groupIds) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            strSQL = PushMessagingSetup.sql_bundle.getString("insertMessagingProcessGroups");
            
            for (int i = 0; i < groupIds.length; i++) {
                String groupId = groupIds[i];
                cat.debug("Insert insertMessagingProcessGroups " + strSQL);
                pst = dbConn.prepareStatement(strSQL);
                pst.setString(1, UUID.randomUUID().toString().toUpperCase());
                pst.setString(2, pushMessagingId);
                pst.setString(3, groupId);
                pst.executeUpdate();
                
                pst.close();
                pst= null;
            }

        } catch (Exception e) {
            cat.error("Error insertPushMessaging", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }
    
    public void deleteMessagingProcessGroupsByPushMsg(String pushMessagingId) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(PushMessagingSetup.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = PushMessagingSetup.sql_bundle.getString("deleteMessagingProcessGroupsByPushMsg");
            cat.debug("deleteMessagingProcessGroupsByPushMsg " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, pushMessagingId);
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error deleteMessagingProcessGroupsByPushMsg ", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

}
