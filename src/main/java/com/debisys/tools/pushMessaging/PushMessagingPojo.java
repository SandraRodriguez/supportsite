package com.debisys.tools.pushMessaging;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author dgarzon
 */
public class PushMessagingPojo {
    
    private String id;
    private String pushNotificationComTypesId;
    private Timestamp createDate;
    private Timestamp notificationDate;
    private boolean androidMethod;
    private boolean iosMethod;
    private String title;
    private String englishMessage;
    private String spanishMessage;
    private boolean active;
    private boolean isSmartPhone;
    private List<PushMessagingProcessGroup> groupList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPushNotificationComTypesId() {
        return pushNotificationComTypesId;
    }

    public void setPushNotificationComTypesId(String pushNotificationComTypesId) {
        this.pushNotificationComTypesId = pushNotificationComTypesId;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(Timestamp notificationDate) {
        this.notificationDate = notificationDate;
    }

    public boolean isAndroidMethod() {
        return androidMethod;
    }

    public void setAndroidMethod(boolean androidMethod) {
        this.androidMethod = androidMethod;
    }

    public boolean isIosMethod() {
        return iosMethod;
    }

    public void setIosMethod(boolean iosMethod) {
        this.iosMethod = iosMethod;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEnglishMessage() {
        return englishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        this.englishMessage = englishMessage;
    }

    public String getSpanishMessage() {
        return spanishMessage;
    }

    public void setSpanishMessage(String spanishMessage) {
        this.spanishMessage = spanishMessage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<PushMessagingProcessGroup> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<PushMessagingProcessGroup> groupList) {
        this.groupList = groupList;
    }

    public boolean isSmartPhone() {
        return isSmartPhone;
    }

    public void setIsSmartPhone(boolean isSmartPhone) {
        this.isSmartPhone = isSmartPhone;
    }
    
    
    
}

