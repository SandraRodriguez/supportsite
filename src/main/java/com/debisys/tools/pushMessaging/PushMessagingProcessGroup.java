package com.debisys.tools.pushMessaging;

/**
 *
 * @author dgarzon
 */
public class PushMessagingProcessGroup {
    
    private String id;
    private String pushMessagingId;
    private String pushMessagingGroupsId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPushMessagingId() {
        return pushMessagingId;
    }

    public void setPushMessagingId(String pushMessagingId) {
        this.pushMessagingId = pushMessagingId;
    }

    public String getPushMessagingGroupsId() {
        return pushMessagingGroupsId;
    }

    public void setPushMessagingGroupsId(String pushMessagingGroupsId) {
        this.pushMessagingGroupsId = pushMessagingGroupsId;
    }
    
    
}
