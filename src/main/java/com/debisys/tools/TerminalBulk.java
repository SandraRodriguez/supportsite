/**
 *
 */
package com.debisys.tools;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletContext;

import com.debisys.languages.Languages;
import com.debisys.terminals.Terminal;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;

/**
 * @author nmartinez
 *
 */
public class TerminalBulk extends Terminal
{

	private final StringBuffer messages  = new StringBuffer();

	private Integer      processOK = 0;

	private String       emailNotification;
	private Integer      purchaseType;
	//private String       pcTerminalPassword;
	//private String       pcTerminalUser;

	private String       terminalPassword;
	private String       terminalUser;

	private String       repSetUpMailsNotification;
	private String       dba;
	private String       contactEmail;
	private String       monthlyFee;
	private String       mailHost;
	private String       accessLevel;
	private String       companyName;
	private String       userName;
	private String       refId;

	private String       url;
	private String       terminalDesc;
	private Boolean      isPCTerminal;

	private Long RatePlanId = null;


	private String FirstName;
	private String LastName;

	public String getUrl()
	{
		return this.url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getTerminalDesc()
	{
		return this.terminalDesc;
	}

	public void setTerminalDesc(String terminalDesc)
	{
		this.terminalDesc = terminalDesc;
	}

	public Boolean getIsPCTerminal()
	{
		return this.isPCTerminal;
	}

	public void setIsPCTerminal(Boolean isPCTerminal)
	{
		this.isPCTerminal = isPCTerminal;
	}

	// public String getPcTerminalPassword()
	// {
	//   return this.pcTerminalPassword;
	// }

	// public void setPcTerminalPassword(String pcTerminalPassword)
	// {
	//   this.pcTerminalPassword = pcTerminalPassword;
	// }

	// public String getPcTerminalUser()
	// {
	//  return this.pcTerminalUser;
	//}

	// public void setPcTerminalUser(String pcTerminalUser)
	// {
	//   this.pcTerminalUser = pcTerminalUser;
	// }

	public String getEmailNotification()
	{
		return this.emailNotification;
	}

	public void setEmailNotification(String emailNotification)
	{
		this.emailNotification = emailNotification;
	}

	public Integer getPurchaseType()
	{
		return this.purchaseType;
	}

	public void setPurchaseType(Integer purchaseType)
	{
		this.purchaseType = purchaseType;
	}

	public Integer getProcessOK()
	{
		return this.processOK;
	}

	/**
	 * This method try to send a email notification to setup group, contact
	 * merchant and terminal notification mail parameter in the bulk file used to
	 * load merchants.
	 */
	public void ProcessOK(ServletContext context,SessionData sessionData)
	{
		try
		{

			String descTerminal = "[" + this.getTerminalType() + "] " + this.getTerminalDesc();
			String emails="";
			if (this.repSetUpMailsNotification != null)
			{
				emails = this.repSetUpMailsNotification + "," + this.getEmailNotification();
			}
			else
			{
				emails = this.getEmailNotification();
			}
			if(this.getTerminalType().equals("26"))
			{
				this.isPCTerminal = true;
			}
			this.sendMailNotification(this.mailHost, this.accessLevel, this.refId, this.companyName, this.userName, this.dba, this.contactEmail, this.monthlyFee,
 "debisys", emails, this.isPCTerminal,
					this.url, descTerminal, !DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO), null, true, false,false,false, false, "", "", context);
			this.processOK = 2;
			this.messages.append(Languages.getString("jsp.admin.tools.send_mail_ok", sessionData.getLanguage()) + "<br>");
		}
		catch (Exception ex)
		{
			this.processOK = 3;
			this.messages.append(Languages.getString("jsp.admin.tools.error_send_mail", sessionData.getLanguage()) + "<br>");
		}
	}

	/**
	 * @param dba
	 * @param contactEmail
	 * @param monthlyFee
	 * @param mailHost
	 * @param accessLevel
	 * @param companyName
	 * @param emails
	 * @param userName
	 * @param refId
	 */
	public void SetUpParametersForEmail(String dba, String contactEmail, String monthlyFee, String mailHost, String accessLevel, String companyName,
			String emails, String userName, String refId)
	{
		this.dba = dba;
		this.contactEmail = contactEmail;
		this.monthlyFee = monthlyFee;
		this.mailHost = mailHost;
		this.accessLevel = accessLevel;
		this.companyName = companyName;
		this.repSetUpMailsNotification = emails;
		this.userName = userName;
		this.refId = refId;
	}

	public String getTerminalPassword()
	{
		return this.terminalPassword;
	}

	public void setTerminalPassword(String terminalPassword)
	{
		this.terminalPassword = terminalPassword;
	}

	public String getTerminalUser()
	{
		return this.terminalUser;
	}

	public void setTerminalUser(String terminalUser)
	{
		this.terminalUser = terminalUser;
	}

	public String getTableErrorValidation()
	{

		int irowCss = 1;
		Hashtable errorHashtable = this.getErrors();
		Enumeration e = errorHashtable.elements();
		StringBuffer errorTerminal = new StringBuffer();
		while (e.hasMoreElements())
		{
			errorTerminal.append("<tr class=\"" + irowCss + "\"><td class=\"rowred1\">" + e.nextElement() + "</td></tr>");
			if (irowCss == 1)
			{
				irowCss = 2;
			}
			else
			{
				irowCss = 1;
			}
		}

		if (this.messages.toString().length() > 0)
		{
			errorTerminal.append("<tr class=\"" + irowCss + "\"><td class=\"rowred1\">" + this.messages.toString() + "</td></tr>");
		}

		if (errorTerminal.toString().length() == 0)
		{
			return "";
		}
		else
		{
			return "<table>" + errorTerminal.toString() + "</table>";
		}

	}

	public StringBuffer getMessages()
	{
		return this.messages;
	}

	public String getFirstName() {
		return this.FirstName;
	}

	public void setFirstName(String firstName) {
		this.FirstName = firstName;
	}

	public String getLastName() {
		return this.LastName;
	}

	public void setLastName(String lastName) {
		this.LastName = lastName;
	}

	/**
	 * @return the ratePlanId
	 */
	public Long getRatePlanId() {
		return RatePlanId;
	}

	/**
	 * @param ratePlanId the ratePlanId to set
	 */
	public void setRatePlanId(Long ratePlanId) {
		RatePlanId = ratePlanId;
	}



}
