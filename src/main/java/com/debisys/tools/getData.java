package com.debisys.tools;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import com.debisys.exceptions.ReportException;

@SuppressWarnings("deprecation")
public class getData {	
	
	public static Category cat = Category.getInstance(getData.class);
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");
	private int totalsku;
	private List<String> sku;
	private int totalid;
	private List<String> id;
	private int totalname;
	private List<String> name;
	/***
	 * @param 
	 * @return
	 * @throws ReportException
	 */
	public getData(String type,String isoid) 
	{
		
		if(type.equals("sku")){
		sku = new ArrayList<String>();
		
		
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("getallsku");
	    
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		sku.add(String.valueOf(rs.getInt("id")).trim());
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("getData sku => Error during gets2kstockcode", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("getData sku => Error during closeConnection ", e);
	    	}
	    }
	    
	    totalsku = sku.size();
		}
		else if(type.equals("salesID")){
			id = new ArrayList<String>();
			
			
		    Connection dbConn = null;
		    PreparedStatement pstmt = null;
		    String strSQL = sql_bundle.getString("getallsalesmanid");
		    
		    try
		    {
		    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    	if (dbConn == null)
		    	{
		    		cat.error("Can't get database connection");
		    		 
		    	}
		    	cat.debug(strSQL);
		    	pstmt = dbConn.prepareStatement(strSQL);
		    	pstmt.setString(1, isoid);
		      	ResultSet rs = pstmt.executeQuery();

		      	while (rs.next())
		      	{
		    		id.add(String.valueOf(rs.getString("salesmanid")).trim());
		      	}
		      	rs.close();
		      	pstmt.close();
		    }
		    catch (Exception e)
		    {
		    	cat.error("getData id => Error during gets2kstockcode", e);
		    	 
		    }
		    finally
		    {
		    	try
		    	{
		    		if (pstmt != null)
		    			pstmt.close();
		    		Torque.closeConnection(dbConn);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("getData id => Error during closeConnection ", e);
		    	}
		    }
		    
		    totalid = id.size();
		}
		else if(type.equals("name")){
			name = new ArrayList<String>();
			
			
		    Connection dbConn = null;
		    PreparedStatement pstmt = null;
		    String strSQL = sql_bundle.getString("getallsalesmanname");
		    
		    try
		    {
		    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    	if (dbConn == null)
		    	{
		    		cat.error("Can't get database connection");
		    		 
		    	}

		    	pstmt = dbConn.prepareStatement(strSQL);
		    	pstmt.setString(1, isoid);
		      	ResultSet rs = pstmt.executeQuery();

		      	while (rs.next())
		      	{
		    		name.add(String.valueOf(rs.getString("salesmanname")).trim());
		      	}
		      	rs.close();
		      	pstmt.close();
		    }
		    catch (Exception e)
		    {
		    	cat.error("getData name => Error during gets2kstockcode", e);
		    	 
		    }
		    finally
		    {
		    	try
		    	{
		    		if (pstmt != null)
		    			pstmt.close();
		    		Torque.closeConnection(dbConn);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("getData name => Error during closeConnection ", e);
		    	}
		    }
		    
		    totalname = name.size();
		}
	}
	
	public List<String> getSalesmanID(String query) {
		String tempid = null;
		query = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for(int i=0; i<totalid; i++) {
			tempid = id.get(i).toLowerCase();
			if(tempid.contains(query)) {
				matched.add(id.get(i));
			}
		}
		return matched;
	}
	
	public List<String> getSalesmanName(String query) {
		String tempid = null;
		query = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for(int i=0; i<totalname; i++) {
			tempid = name.get(i).toLowerCase();
			if(tempid.contains(query)) {
				matched.add(name.get(i));
			}
		}
		return matched;
	}
	
	public List<String> getPID(String query) {
		String tempid = null;
		query = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for(int i=0; i<totalsku; i++) {
			tempid = sku.get(i).toLowerCase();
			if(tempid.contains(query)) {
				matched.add(sku.get(i));
			}
		}
		return matched;
	}
}
