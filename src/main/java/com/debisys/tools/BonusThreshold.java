package com.debisys.tools;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.users.SessionData;

/***
 * All of the Java code used to process Bonus Thresholds and Bonus Threshold SMS Messages.
 * 
 * DBSY-568
 * @author swright
 *
 */
public class BonusThreshold
{
	// log4j logging
	public static Category cat = Category.getInstance(BonusThreshold.class);

	// SQL
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");

	/**
	 * This gets all of the current providers in the system.
	 * @return
	 * @throws ReportException
	 */
	public Vector getProviders() throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getProviders");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("provider_id"));
	      		vecTemp.add(rs.getString("name"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholds", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/**
	 * This will insert a bonus threshold SMS Message under the ID of the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public int insertBonusThresholdSMS(HttpServletRequest request, SessionData sessionData) throws ReportException {
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("insertBonusThresholdSMS");
		int result;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			cat.debug(strSQL);

			String IsoID = sessionData.getProperty("iso_id");
			String provider = request.getParameter("newProvider");
			String msgName = request.getParameter("newMsgName");
			String message = request.getParameter("msg");

			if (IsoID == null || provider == null || msgName == null
					|| message == null)
				return 0;
			
			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(IsoID));
			cstmt.setInt(2, Integer.parseInt(provider));
			cstmt.setString(3, msgName);
			cstmt.setString(4, message);
			cstmt.registerOutParameter(5, Types.INTEGER);
			cstmt.setInt(5, 0);
			cstmt.execute();

			result = cstmt.getInt(5);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during insertBonusThresholdSMS", e);
			throw new ReportException();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
				result = 0;
			}
		}
		return result;
	}

	/***
	 * This will be used to update a specific SMS Messages using the ID os the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void updateBonusThresholdSMS(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("updateBonusThresholdSMS");

    	cat.debug("Updating bonus threshold SMS msg");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	String IsoID, oldProvider, newProvider, newMsgName, oldMsgName, msg;

	    	IsoID = sessionData.getProperty("iso_id");
    		oldProvider = request.getParameter("oldProvider");
    		newProvider = request.getParameter("newProvider");
    		newMsgName = request.getParameter("newMsgName");
    		oldMsgName = request.getParameter("oldMsgName");
    		msg = request.getParameter("msg");

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, msg);
	    	pstmt.setString(2, newMsgName);
	    	pstmt.setInt(3, Integer.parseInt(newProvider));
	    	pstmt.setLong(4, Long.parseLong(IsoID));
	    	pstmt.setInt(5, Integer.parseInt(oldProvider));
	    	pstmt.setString(6, oldMsgName);
	    	pstmt.executeUpdate();

	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during updateBonusThresholdSMS", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}

	/***
	 * This query is to get a specific SMS Message using the ID of the ISO currently logged on. 
	 * Once we have the message, we can edit it, and then save it back.
	 * @param providerId
	 * @param name
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getBonusThresholdSMS(String providerId, String name, SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getBonusThresholdSMS2");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);
	    	String IsoID = sessionData.getProperty("iso_id");
	    	
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setLong(1, Long.parseLong(IsoID));
	    	pstmt.setInt(2, Integer.parseInt(providerId));
	    	pstmt.setString(3, name);
//      		cat.debug("Parms - provider: " + Integer.parseInt(providerId) + ", name: " +  name);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("provider_id"));
	      		vecTemp.add(rs.getString("name"));
	      		vecTemp.add(rs.getString("message"));
//	      		cat.debug("provider: " + rs.getInt("provider_id") + ", name: " +  rs.getString("name"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholdSMS", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/***
	 * This will get all of the bonus threshold SMS Messages for the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getBonusThresholdSMS(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getBonusThresholdSMS1");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	String isoId = sessionData.getProperty("iso_id");
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("provider_id"));
	      		vecTemp.add(rs.getString("name"));
	      		vecTemp.add(rs.getString("message"));
//	      		cat.debug("provider: " + rs.getInt("provider_id") + ", name: " +  rs.getString("name"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholdSMS", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/***
	 * DBSY-568 SW
	 * Will get all of the Bonus Threshold info
	 * Right now only used by CCT.
	 * @return
	 * @throws ReportException
	 */
	public Vector getBonusThresholds(SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getBonusThresholds");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);
	    	String isoId = sessionData.getProperty("iso_id");

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getString("Threshold"));
	      		vecTemp.add(rs.getString("ExtraBonus"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholds", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/***
	 * This will update a specific bonus threshold using the ID of the ISO currently logged on. 
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void updateBonusThresholds(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("updateBonusThresholds");

    	cat.debug("Updating bonus thresholds");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	// Use this to iterate through the rows sumitted in the request.
	    	int iterator = 1;
	    	String threshold, prevThreshold;
	    	String bonus, prevBonus;
	    	String isoId = sessionData.getProperty("iso_id");

	    	// Do this because I'm not sure how many thresholds are going to
	    	// be coming through the request.
	    	do
	    	{
	    		threshold = request.getParameter("thresholds" + iterator);
	    		bonus = request.getParameter("extraBonuses" + iterator);
	    		prevThreshold = request.getParameter("prevThresholds" + iterator);
	    		prevBonus = request.getParameter("prevExtraBonuses" + iterator);


		    	pstmt = dbConn.prepareStatement(strSQL);
		    	pstmt.setString(1, threshold);
		    	pstmt.setString(2, bonus);
		    	pstmt.setBigDecimal(3, new BigDecimal(isoId));
		    	pstmt.setString(4, prevThreshold);
		    	pstmt.setString(5, prevBonus);
		    	pstmt.executeUpdate();
		    	iterator++;
	    	}
	    	while(threshold != null && bonus != null);

	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during insertBonusThreshold", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}

	/***
	 * This will delete a bonus threshold using the ID of the ISO currently logged on
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void deleteBonusThreshold(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("deleteBonusThreshold");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	String threshold = request.getParameter("prevThreshold");
	    	String bonus = request.getParameter("prevBonus");
	    	String isoId = sessionData.getProperty("iso_id");

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setBigDecimal(1, new BigDecimal(isoId));
	    	pstmt.setString(2, threshold);
	    	pstmt.setString(3, bonus);
	    	pstmt.executeUpdate();

	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during deleteBonusThreshold", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}

	/***
	 * This will insert a bonus threshold for a specific ISO using a DB stored procedure
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public int insertBonusThreshold(HttpServletRequest request, SessionData sessionData) throws ReportException {
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("insertBonusThreshold");
		int result;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			cat.debug(strSQL);

			String threshold = request.getParameter("newThreshold");
			String bonus = request.getParameter("newBonus");
			String isoID = sessionData.getProperty("iso_id");

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setBigDecimal(1, new BigDecimal(isoID));
			cstmt.setString(2, threshold);
			cstmt.setString(3, bonus);
			cstmt.registerOutParameter(4, Types.INTEGER);
			cstmt.setInt(4, 0);
			cstmt.execute();

			result = cstmt.getInt(4);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during insertBonusThreshold", e);
			throw new ReportException();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
				result = 0;
			}
		}
		return result;
	}
}
