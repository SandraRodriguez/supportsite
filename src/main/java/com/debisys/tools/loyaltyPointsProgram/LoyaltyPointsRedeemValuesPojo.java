
package com.debisys.tools.loyaltyPointsProgram;

/**
 *
 * @author dgarzon
 */
public class LoyaltyPointsRedeemValuesPojo {

    private String id;
    private String loyaltyPointsProgramId;
    private int points;
    private double freeValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoyaltyPointsProgramId() {
        return loyaltyPointsProgramId;
    }

    public void setLoyaltyPointsProgramId(String loyaltyPointsProgramId) {
        this.loyaltyPointsProgramId = loyaltyPointsProgramId;
    }

    

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public double getFreeValue() {
        return freeValue;
    }

    public void setFreeValue(double freeValue) {
        this.freeValue = freeValue;
    }
    
    
}
