package com.debisys.tools.loyaltyPointsProgram;

import com.debisys.exceptions.ReportException;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.LogChanges;
import com.emida.utils.dbUtils.TorqueHelper;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class LoyaltyPointsProgram {

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.loyaltyPointsProgram.sql");
    
    private static final String AUDIT_LOYALTY_ACCOUNTSOURCE = "loyaltyPoints accountSourceId";
    private static final String AUDIT_LOYALTY_STARTDATE = "loyaltyPoints  startDate";
    private static final String AUDIT_LOYALTY_ENDDATE = "loyaltyPoints endDate";
    private static final String AUDIT_LOYALTY_NAME = "loyaltyPoints name";
    private static final String AUDIT_LOYALTY_CARRIERID = "loyaltyPoints carrierId";
    private static final String AUDIT_LOYALTY_POINTS = "loyaltyPoints points";
    private static final String AUDIT_LOYALTY_VALUE = "loyaltyPoints value";
    private static final String AUDIT_LOYALTY_VALUETYPE = "loyaltyPoints valueType";
    private static final String AUDIT_LOYALTY_ACTIVE = "loyaltyPoints active";
    private static final String AUDIT_LOYALTY_SMSNOTIFICATION = "loyaltyPoints smsNotifications";
    private static final String AUDIT_LOYALTY_SMSMESSAGE = "loyaltyPoints smsMessage";
    private static final String AUDIT_LOYALTY_REDEEM_PROGRAM = "loyaltyPointsRedeemValues ProgramId";
    private static final String AUDIT_LOYALTY_REDEEM_POINTS = "loyaltyPointsRedeemValues points";
    private static final String AUDIT_LOYALTY_REDEEM_FREEVALUE = "loyaltyPointsRedeemValues freeValue";

    static Category cat = Category.getInstance(LoyaltyPointsProgram.class);

    public static List<LoyaltyPointsProgramPojo> getLoyaltyPointsProgramByCarrier(String id, String carrierId) {
        List<LoyaltyPointsProgramPojo> list = new ArrayList<LoyaltyPointsProgramPojo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            String sql = LoyaltyPointsProgram.sql_bundle.getString("getLoyaltyPointsProgramByCarrier");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setBigDecimal(1, new BigDecimal(carrierId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                if (id == null || (id != null && id.equalsIgnoreCase(rs.getString("id")))) {
                    LoyaltyPointsProgramPojo loyalty = new LoyaltyPointsProgramPojo();
                    loyalty.setId(rs.getString("id"));
                    loyalty.setAccountSourceId(rs.getString("accountSourceId"));
                    loyalty.setStartDate(rs.getTimestamp("startDate"));
                    loyalty.setEndDate(rs.getTimestamp("endDate"));
                    loyalty.setName(rs.getString("name"));
                    loyalty.setCarrierId(rs.getBigDecimal("carrierId"));
                    loyalty.setPoints(rs.getInt("points"));
                    loyalty.setValue(rs.getDouble("value"));
                    loyalty.setValueType(rs.getString("valueType"));
                    loyalty.setActive(rs.getBoolean("active"));
                    loyalty.setSmsNotifications(rs.getBoolean("smsNotifications"));
                    loyalty.setSmsMessage(rs.getString("smsMessage"));

                    list.add(loyalty);

                }
            }
        } catch (Exception e) {
            cat.error("Error during getLoyaltyPointsProgramByCarrier", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static List<LoyaltyPointsRedeemValuesPojo> getLoyaltyPointsRedeemValues(String loyaltyPointsProgramId) {
        List<LoyaltyPointsRedeemValuesPojo> list = new ArrayList<LoyaltyPointsRedeemValuesPojo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            String sql = LoyaltyPointsProgram.sql_bundle.getString("getLoyaltyPointsRedeemValues");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, loyaltyPointsProgramId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                LoyaltyPointsRedeemValuesPojo pojo = new LoyaltyPointsRedeemValuesPojo();
                pojo.setId(rs.getString("id"));
                pojo.setLoyaltyPointsProgramId(rs.getString("loyaltyPointsProgramId"));
                pojo.setPoints(rs.getInt("points"));
                pojo.setFreeValue(rs.getDouble("freeValue"));
                list.add(pojo);
            }
        } catch (Exception e) {
            cat.error("Error during getLoyaltyPointsRedeemValues", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public void insertLoyaltyPointsProgram(SessionData sessionData, ServletContext context, LoyaltyPointsProgramPojo program, List<LoyaltyPointsRedeemValuesPojo> redeemValuesList) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            program.setId(uuid);
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));

            strSQL = LoyaltyPointsProgram.sql_bundle.getString("insertLoyaltyPointsProgram");

            cat.debug("Insert insertLoyaltyPointsProgram " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, program.getId());
            pst.setString(2, program.getAccountSourceId());
            pst.setTimestamp(3, program.getStartDate());
            pst.setTimestamp(4, program.getEndDate());
            pst.setString(5, program.getName());
            pst.setBigDecimal(6, program.getCarrierId());
            pst.setInt(7, program.getPoints());
            pst.setDouble(8, program.getValue());
            pst.setString(9, program.getValueType());
            pst.setBoolean(10, program.isActive());
            pst.setBoolean(11, program.isSmsNotifications());
            pst.setString(12, program.getSmsMessage());
            pst.executeUpdate();

            // INSERT insertLoyaltyPointsRedeemValues
            insertLoyaltyPointsRedeemValues(uuid, redeemValuesList);
            
            insertAUDITLoyaltyProgram(sessionData, context, program,redeemValuesList);
            
        } catch (Exception e) {
            cat.error("Error inserting insertLoyaltyPointsProgram", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void updateLoyaltyPointsProgram(LoyaltyPointsProgramPojo program, List<LoyaltyPointsRedeemValuesPojo> redeemValuesList) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            strSQL = LoyaltyPointsProgram.sql_bundle.getString("updateLoyaltyPointsProgram");

            SimpleDateFormat formatResult = new SimpleDateFormat("yyyy-MM-dd");
            String sd = (program.getStartDate()!= null) ? formatResult.format( new Date(program.getStartDate().getTime())) : null;
            String ed = (program.getEndDate() != null) ? formatResult.format( new Date(program.getEndDate().getTime())) : null;

            cat.debug("updateLoyaltyPointsProgram " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            pst.setString(1, program.getAccountSourceId());
            pst.setString(2, sd);
            pst.setString(3, ed);
            pst.setString(4, program.getName());
            pst.setInt(5, program.getPoints());
            pst.setDouble(6, program.getValue());
            pst.setString(7, program.getValueType());
            pst.setBoolean(8, program.isActive());
            pst.setBoolean(9, program.isSmsNotifications());
            pst.setString(10, program.getSmsMessage());
            pst.setString(11, program.getId());
            pst.executeUpdate();
            
            
            
            // delete old list
            deleteLoyaltyPointsRedeemValues(program.getId());

            // INSERT Detail
            insertLoyaltyPointsRedeemValues(program.getId(), redeemValuesList);
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error inserting updateLoyaltyPointsProgram", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void insertLoyaltyPointsRedeemValues(String loyaltyPointsProgramId, List<LoyaltyPointsRedeemValuesPojo> redeemValuesList) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = LoyaltyPointsProgram.sql_bundle.getString("insertLoyaltyPointsRedeemValues");
            cat.debug("Insert insertLoyaltyPointsRedeemValues" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            for (LoyaltyPointsRedeemValuesPojo redeemValue : redeemValuesList) {
                pst.setString(1, UUID.randomUUID().toString().toUpperCase());
                pst.setString(2, loyaltyPointsProgramId);
                pst.setInt(3, redeemValue.getPoints());
                pst.setDouble(4, redeemValue.getFreeValue());
                pst.executeUpdate();
            }
        } catch (Exception e) {
            cat.error("Error inserting insertLoyaltyPointsRedeemValues", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void deleteLoyaltyPointsProgram(SessionData sessionData, ServletContext context, String loyaltyPointsProgramId, String carrierId) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            List<LoyaltyPointsProgramPojo> loyaltyPointsProgramByCarrier = getLoyaltyPointsProgramByCarrier(loyaltyPointsProgramId, carrierId);
            List<LoyaltyPointsRedeemValuesPojo> listRedeem = getLoyaltyPointsRedeemValues(loyaltyPointsProgramId);
            
            deleteLoyaltyPointsRedeemValues(loyaltyPointsProgramId);

            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = LoyaltyPointsProgram.sql_bundle.getString("deleteLoyaltyPointsProgram");
            cat.debug("deleting deleteLoyaltyPointsProgram" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, loyaltyPointsProgramId);
            pst.executeUpdate();
            
            deleteAUDITLoyaltyProgram(sessionData, context, loyaltyPointsProgramByCarrier.get(0), listRedeem);

        } catch (Exception e) {
            cat.error("Error deleting deleteLoyaltyPointsProgram", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void deleteLoyaltyPointsRedeemValues(String loyaltyPointsProgramId) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = LoyaltyPointsProgram.sql_bundle.getString("deleteLoyaltyPointsRedeemValues");
            cat.debug("delete deleteLoyaltyPointsRedeemValues " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, loyaltyPointsProgramId);
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error deleteLoyaltyPointsRedeemValues", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static List<AccountSource> getAccountSourceList() {
        List<AccountSource> list = new ArrayList<AccountSource>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            String sql = LoyaltyPointsProgram.sql_bundle.getString("getAccountSourceList");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                AccountSource loyalty = new AccountSource(rs.getString("id"), rs.getString("description"), rs.getString("keyValue"));
                list.add(loyalty);
            }
        } catch (Exception e) {
            cat.error("Error during getAccountSourceList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }
    
    public static AccountSource getAccountSourceById(String accountSourceId) {
        
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            String sql = LoyaltyPointsProgram.sql_bundle.getString("getAccountSourceById");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, accountSourceId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return new AccountSource(rs.getString("id"), rs.getString("description"), rs.getString("keyValue"));
            }
        } catch (Exception e) {
            cat.error("Error during getAccountSourceById", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public String getReportQuery(String stockLevelsId, List<String> fieldsToRead, String strAccessLevel, String strRefId, String strDistChainType) {
        StringBuilder filterChain = new StringBuilder("");
        String strSelectFields = fieldsToRead.toString().replaceAll("\\[", " ").replaceAll("\\]", " ");
        StringBuilder sql = new StringBuilder("SELECT " + strSelectFields);
        sql.append(" FROM MerchantsStockLevels msl WITH(NOLOCK) ");
        sql.append(" INNER JOIN MerchantsStockLevelsDetail msld WITH(NOLOCK) ON (msld.IdMerchantsStockLevels = msl.id) ");
        sql.append(" INNER JOIN merchants m WITH(NOLOCK) ON (m.merchant_id = msld.merchant_id) ");

        if (strAccessLevel.equals(DebisysConstants.ISO)) {
            if (strDistChainType.equals(DebisysConstants.REP_TYPE_ISO_5_LEVEL)) {
                filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
                filterChain.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id");
                filterChain.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 5 AND iso_id ");
                filterChain.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 4 AND iso_id = " + strRefId + "))) ");
            } else if (strDistChainType.equals(DebisysConstants.REP_TYPE_ISO_3_LEVEL)) {
                filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
                filterChain.append(" WHERE r.rep_id = " + strRefId + " ");
            }
        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
            filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            filterChain.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id");
            filterChain.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 5 AND iso_id = " + strRefId + ")) ");
        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
            filterChain.append(" INNER JOIN reps WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            filterChain.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id AND iso_id = " + strRefId + ")) ");
        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
            filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            filterChain.append(" WHERE r.rep_id = " + strRefId + " ");
        }
        sql.append(filterChain);
        sql.append(" AND msl.id = '" + stockLevelsId + "' AND m.datecancelled IS NULL AND  m.cancelled=0 AND m.latitude <> 0 AND m.longitude <> 0 ");
        return sql.toString();
    }
    
    private static void insertAUDITLoyaltyProgram(SessionData sessionData, ServletContext context, LoyaltyPointsProgramPojo program, 
            List<LoyaltyPointsRedeemValuesPojo> redeemValuesList) throws Exception {
        
        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);

        int idLogChangeAccount = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_ACCOUNTSOURCE);
        int idLogChangeStartDate = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_STARTDATE);
        int idLogChangeEndDate = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_ENDDATE);
        int idLogChangeName = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_NAME);
        int idLogChangeCarrier = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_CARRIERID);
        
        int idLogChangePoints = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_POINTS);
        int idLogChangeValue = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_VALUE);
        int idLogChangeValueType = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_VALUETYPE);
        int idLogChangeActive = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_ACTIVE);
        int idLogChangeSmsNotification = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_SMSNOTIFICATION);
        int idLogChangeSmsMessage = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_SMSMESSAGE);

        String reason = "Inserted Loyalty Points Program with Carrier: " + program.getCarrierId()+ "  Name [" + program.getName()+ "] Id[" + program.getId()+ "]";
        logChanges.log_changed(idLogChangeAccount, program.getCarrierId().toString(), program.getAccountSourceId(), "", reason);
        logChanges.log_changed(idLogChangeStartDate, program.getCarrierId().toString(), (program.getStartDate()!=null)?program.getStartDate().toString():"", "", reason);
        logChanges.log_changed(idLogChangeEndDate, program.getCarrierId().toString(), (program.getEndDate() !=null)?program.getEndDate().toString():"", "", reason);
        
        logChanges.log_changed(idLogChangeName, program.getCarrierId().toString(), ""+program.getName(), "", reason);
        logChanges.log_changed(idLogChangeCarrier, program.getCarrierId().toString(), ""+program.getCarrierId().toString(), "", reason);
        logChanges.log_changed(idLogChangePoints, program.getCarrierId().toString(), ""+program.getPoints(), "", reason);
        logChanges.log_changed(idLogChangeValue, program.getCarrierId().toString(), ""+program.getValue(), "", reason);
        logChanges.log_changed(idLogChangeValueType, program.getCarrierId().toString(), ""+program.getValueType(), "", reason);
        logChanges.log_changed(idLogChangeActive, program.getCarrierId().toString(), ""+program.isActive(), "", reason);
        logChanges.log_changed(idLogChangeSmsNotification, program.getCarrierId().toString(), ""+program.isSmsNotifications(), "", reason);
        logChanges.log_changed(idLogChangeSmsMessage, program.getCarrierId().toString(), ""+program.getSmsMessage(), "", reason);

        insertAUDITRedeemValues(logChanges, program.getId(), redeemValuesList);
    }
    
    private static void insertAUDITRedeemValues(LogChanges logChanges, String loyaltyProgramId, List<LoyaltyPointsRedeemValuesPojo> redeemValuesList) throws Exception{
        String reasonIso = "Inserted Redeem Values with point: [%s] freevalue= [%s]  loyaltyProgramId [%s]";
        
        int redeemProgram = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_REDEEM_PROGRAM);
        for (LoyaltyPointsRedeemValuesPojo program : redeemValuesList) {
            logChanges.log_changed(redeemProgram, loyaltyProgramId, ""+program.getPoints(), "", String.format(reasonIso, program.getPoints(),program.getFreeValue(),loyaltyProgramId));
        }
    }
    
    public void enableDisableProgram(String programId, String carrierId){
        LoyaltyPointsProgramPojo program = getLoyaltyPointsProgramByCarrier(programId, carrierId).get(0);
        if(program.isActive()){
            program.setActive(false);
        }
        else{
            program.setActive(true);
        }
        
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            dbConn = TorqueHelper.getConnection(LoyaltyPointsProgram.sql_bundle.getString("pkgDefaultDb"));
            strSQL = LoyaltyPointsProgram.sql_bundle.getString("enableDisableProgram");

            cat.debug("enableDisableProgram " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            pst.setBoolean(1, program.isActive());
            pst.setString(2, program.getId());
            pst.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error inserting enableDisableProgram", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    /*private static void updateReferralAgentsAUDIT(SessionData sessionData, ServletContext context, ReferralAgentsAssociationPojo raOld, ReferralAgentsAssociationPojo raAssociation) throws Exception {
        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);

        int idLogChangeAssociation = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT);
        int idLogChangeDescription = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_DESCRIPTION);
        int idLogChangeEnable = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ENABLE);
        int idLogChangeIso = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ISOID);
        int idLogChangeProduct = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_PRODUCTID);
        String reason = "Updated Referral Agent Association with referral agent: " + raAssociation.getReferralAgentId() + "  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        if (raOld.getReferralAgentId().doubleValue() != raAssociation.getReferralAgentId().doubleValue()) {
            logChanges.log_changed(idLogChangeAssociation, raAssociation.getReferralAgentId().toString(), raAssociation.getReferralAgentId().toString(), raOld.getReferralAgentId().toString(), reason);
        }
        if (!raOld.getDescription().trim().equals(raAssociation.getDescription())) {
            logChanges.log_changed(idLogChangeDescription, raAssociation.getReferralAgentId().toString(), raAssociation.getDescription(), raOld.getDescription(), reason);
        }
        if ((raOld.isEnabled() && !raAssociation.isEnabled()) || (!raOld.isEnabled() && raAssociation.isEnabled())) {
            logChanges.log_changed(idLogChangeEnable, raAssociation.getReferralAgentId().toString(), ""+raAssociation.isEnabled(), ""+raOld.isEnabled(), reason);
        }

        String reasonIsoDeleted = "Deleted ISO [%s] with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        String reasonIsoInserted = "Inserted ISO [%s] with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        
        boolean exist = false;
        for (ReferralAgentsIsosPojo isoOld : raOld.getIsosList()) {
            for (int i =0; i < raAssociation.getIsosList().size(); i++) {
                if(!exist && isoOld.getIsoId().doubleValue() == raAssociation.getIsosList().get(i).getIsoId().doubleValue()){
                    exist = true;
                    raAssociation.getIsosList().get(i).setVerified(true);
                }
            }
            if(!exist){
                // REPORT ISOS DELETED
                logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), "",isoOld.getIsoId().toString(), String.format(reasonIsoDeleted, isoOld.getIsoId(), raAssociation.getReferralAgentId()));
            }
            exist = false;
        }
        
        // Insert New ISO fiels
        for (ReferralAgentsIsosPojo iso : raAssociation.getIsosList()) {
            if(!iso.isVerified()){
                logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), iso.getIsoId().toString(),"", String.format(reasonIsoInserted, iso.getIsoId(), raAssociation.getReferralAgentId()));
            }
        }
        
        String reasonProductDeleted = "Deleted Product [%s] with Rate[%s], with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        String reasonProductUpdated = "Updated Product [%s] oldRate:[%s], newRate:[%s], with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        String reasonProductInserted = "Inserted Product [%s], with Rate[%s], with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        
        boolean equalsProduct = false;
        boolean equalsRate = false;
        BigDecimal newRate = new BigDecimal(0);
        for (ReferralAgentsProductsPojo productOld : raOld.getProductsList()) {
            for (int i =0; i < raAssociation.getProductsList().size(); i++) {
                if(!exist && productOld.getProductId() == raAssociation.getProductsList().get(i).getProductId()){
                    equalsProduct = true;
                    raAssociation.getProductsList().get(i).setVerified(true);
                    if(!equalsRate && productOld.getRate().doubleValue() == raAssociation.getProductsList().get(i).getRate().doubleValue()){
                        equalsRate = true;
                    }
                    else{
                        newRate = new BigDecimal(raAssociation.getProductsList().get(i).getRate().doubleValue());
                    }
                }
                
            }
            if(!equalsProduct){
                logChanges.log_changed(idLogChangeProduct, raAssociation.getReferralAgentId().toString(), "" , productOld.getProductId()+"_"+productOld.getRate(), String.format(reasonProductDeleted, productOld.getProductId(),productOld.getRate(), raAssociation.getReferralAgentId()));
            }
            else if (equalsProduct && !equalsRate){
                logChanges.log_changed(idLogChangeProduct, raAssociation.getReferralAgentId().toString(), productOld.getProductId()+"_"+newRate.doubleValue() , 
                        productOld.getProductId()+"_"+productOld.getRate(), String.format(reasonProductUpdated, productOld.getProductId(),productOld.getRate(), newRate.doubleValue(), raAssociation.getReferralAgentId()));
            }
            equalsProduct = false;
            equalsRate = false;
            newRate = new BigDecimal(0);
        }
        
        for (ReferralAgentsProductsPojo p: raAssociation.getProductsList()) {
            if(!p.isVerified()){
                logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), p.getProductId()+"_"+p.getRate(),"", String.format(reasonProductInserted, p.getProductId(), p.getRate(), raAssociation.getReferralAgentId()));
            }
        }

    }*/

    private static void deleteAUDITLoyaltyProgram(SessionData sessionData, ServletContext context, LoyaltyPointsProgramPojo program, 
            List<LoyaltyPointsRedeemValuesPojo> redeemValuesList) throws Exception {
        
        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);

        int idLogChangeAccount = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_ACCOUNTSOURCE);
        int idLogChangeStartDate = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_STARTDATE);
        int idLogChangeEndDate = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_ENDDATE);
        int idLogChangeName = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_NAME);
        int idLogChangeCarrier = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_CARRIERID);
        
        int idLogChangePoints = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_POINTS);
        int idLogChangeValue = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_VALUE);
        int idLogChangeValueType = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_VALUETYPE);
        int idLogChangeActive = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_ACTIVE);
        int idLogChangeSmsNotification = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_SMSNOTIFICATION);
        int idLogChangeSmsMessage = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_SMSMESSAGE);

        String reason = "Deleting Loyalty Points Program with Carrier: " + program.getCarrierId()+ "  Name [" + program.getName()+ "] Id[" + program.getId()+ "]";
        logChanges.log_changed(idLogChangeAccount, program.getCarrierId().toString(), program.getAccountSourceId(), "", reason);
        logChanges.log_changed(idLogChangeStartDate, program.getCarrierId().toString(), (program.getStartDate() !=null)?program.getStartDate().toString():"", "", reason);
        logChanges.log_changed(idLogChangeEndDate, program.getCarrierId().toString(), (program.getEndDate() !=null)?program.getEndDate().toString():"", "", reason);
        
        logChanges.log_changed(idLogChangeName, program.getCarrierId().toString(), ""+program.getName(), "", reason);
        logChanges.log_changed(idLogChangeCarrier, program.getCarrierId().toString(), ""+program.getCarrierId().toString(), "", reason);
        logChanges.log_changed(idLogChangePoints, program.getCarrierId().toString(), ""+program.getPoints(), "", reason);
        logChanges.log_changed(idLogChangeValue, program.getCarrierId().toString(), ""+program.getValue(), "", reason);
        logChanges.log_changed(idLogChangeValueType, program.getCarrierId().toString(), ""+program.getValueType(), "", reason);
        logChanges.log_changed(idLogChangeActive, program.getCarrierId().toString(), ""+program.isActive(), "", reason);
        logChanges.log_changed(idLogChangeSmsNotification, program.getCarrierId().toString(), ""+program.isSmsNotifications(), "", reason);
        logChanges.log_changed(idLogChangeSmsMessage, program.getCarrierId().toString(), ""+program.getSmsMessage(), "", reason);

        deleteAUDITRedeemValues(logChanges, program.getId(), redeemValuesList);
    }
    
    private static void deleteAUDITRedeemValues(LogChanges logChanges, String loyaltyProgramId, List<LoyaltyPointsRedeemValuesPojo> redeemValuesList) throws Exception{
        String reasonIso = "Deleting Redeem Values with point: [%s] freevalue= [%s]  loyaltyProgramId [%s]";
        
        int redeemProgram = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_REDEEM_POINTS);
        int redeemProgramFreeValues = logChanges.getLogChangeIdByName(AUDIT_LOYALTY_REDEEM_FREEVALUE);
        for (LoyaltyPointsRedeemValuesPojo program : redeemValuesList) {
            logChanges.log_changed(redeemProgram, loyaltyProgramId, ""+program.getPoints(), "", String.format(reasonIso, program.getPoints(),program.getFreeValue(),loyaltyProgramId));
        }
    }

}
