
package com.debisys.tools.loyaltyPointsProgram;

/**
 *
 * @author dgarzon
 */
public class AccountSource {
    
    private String id;
    private String description;
    private String keyValue;

    public AccountSource(String id, String description, String keyValue) {
        this.id = id;
        this.description = description;
        this.keyValue = keyValue;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }
    
    
}
