
package com.debisys.tools.loyaltyPointsProgram;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 *
 * @author dgarzon
 */
public class LoyaltyPointsProgramPojo {
    
    private String id;
    private String accountSourceId;
    private Timestamp startDate;
    private Timestamp endDate;
    private String name;
    private BigDecimal carrierId;
    private int points;
    private double value;
    private String valueType;
    private boolean active;
    private boolean smsNotifications;
    private String smsMessage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountSourceId() {
        return accountSourceId;
    }

    public void setAccountSourceId(String accountSourceId) {
        this.accountSourceId = accountSourceId;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(BigDecimal carrierId) {
        this.carrierId = carrierId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isSmsNotifications() {
        return smsNotifications;
    }

    public void setSmsNotifications(boolean smsNotifications) {
        this.smsNotifications = smsNotifications;
    }

    public String getSmsMessage() {
        return smsMessage;
    }

    public void setSmsMessage(String smsMessage) {
        this.smsMessage = smsMessage;
    }
    
    
}
