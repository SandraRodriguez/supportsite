package com.debisys.tools;

import com.debisys.reports.PropertiesByFeature;
import com.google.gson.JsonObject;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Emida
 */
public class TokenService {

    private TokenService() {
    }
    
    public static JsonObject buildToken(String entityId, String language, Boolean hasAdminPerm, String urlReturn) {
        JsonObject jsonResponse = new JsonObject();
        jsonResponse.addProperty("response", "false");
        try {
            HashMap<String, PropertiesByFeature> properties = PropertiesByFeature.findPropertiesByFeatureHash(PropertiesByFeature.FEATURE_DOCUMENT_FLOW);

            String url = properties.get("URL").getValue();
            Long expiration = Long.parseLong(properties.get("EXPIRATION").getValue());
            String secret = properties.get("SECRET").getValue();

            JsonObject json = new JsonObject();
            json.addProperty("entityId", entityId);
            json.addProperty("lang", language);
            json.addProperty("href", urlReturn);
            json.addProperty("isAdmin", hasAdminPerm);

            Map<String, Object> claims = new HashMap();
            String token = Jwts.builder()
                    .setClaims(claims)
                    .setSubject(json.toString())
                    .setIssuedAt(new Date(System.currentTimeMillis()))
                    .setExpiration(new Date(System.currentTimeMillis() + (expiration * 1000)))
                    .signWith(SignatureAlgorithm.HS512, secret).compact();

            //adding response values
            jsonResponse.addProperty("response", "true");
            jsonResponse.addProperty("token", token);
            jsonResponse.addProperty("url", url);
        } catch (Exception ex) {

        }
        return jsonResponse;
    }
    

    
}
