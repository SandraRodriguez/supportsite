package com.debisys.tools.paymentsbulk;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Category;
import com.debisys.languages.Languages;
import com.debisys.tools.paymentsbulk.Payment.Status;
import com.debisys.users.SessionData;
import com.debisys.utils.Base;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.dbUtils.TorqueHelper;

public class PaymentBulk
{
	
	static Category cat = Category.getInstance(PaymentBulk.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.paymentsbulk.sql");
	
	private Map<String, String> merchants = new HashMap<String, String>();
	
	private ArrayList<String> merchantsReferenceNo = new ArrayList<String>(100);
	
	private ArrayList<String> errorsProcess = new ArrayList<String>(100);
	
	public ArrayList<String> getErrorsProcess()
	{
		return errorsProcess;
	}
	
	private String idFirstTime = "";
	private String descFirstTime = "";
	private String idnoFirstTime = "";
	private String descnoFirstTime = "";
	
	/**
	 * @param request
	 * @param workingDir
	 * @param sessionData
	 * @param deploymentType
	 * @param customConfigType
	 * @param strRefId
	 * @param mailHost
	 * @param instance
	 * @param email
	 * @param languaje
	 * @return
	 */
	public ArrayList<Payment> processPaymentBulk(HttpServletRequest request, String workingDir, SessionData sessionData, String deploymentType,
			String customConfigType, String strRefId, String mailHost, String instance, String email, String languaje)
	{
		
		this.errorsProcess.clear();
		ArrayList<Payment> paymentsrows = new ArrayList<Payment>(100);
		String saveFile = "";
		
		String firstTime = request.getParameter("first");
		String[] firstChains = firstTime.split("\\*");
		idFirstTime = firstChains[0];
		descFirstTime = firstChains[1];
		
		String noFirstTime = request.getParameter("nofirst");
		String[] noFirstChains = noFirstTime.split("\\*");
		idnoFirstTime = noFirstChains[0];
		descnoFirstTime = noFirstChains[1];
		
		String bank = request.getParameter("bank");
		
		String userName = sessionData.getProperty("username");
		
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		String contentType = request.getContentType();
		if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0))
		{
			byte dataBytes[] = null;
			try
			{
				DataInputStream in = new DataInputStream(request.getInputStream());
				int formDataLength = request.getContentLength();
				dataBytes = new byte[formDataLength];
				int byteRead = 0;
				int totalBytesRead = 0;
				
				while (totalBytesRead < formDataLength)
				{
					byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
					totalBytesRead += byteRead;
				}
			}
			catch (IOException ioe)
			{
				this.addErrorLog(ioe, "processPaymentBulk Method",sessionData);
				return paymentsrows;
				
			}
			String file = new String(dataBytes);
			saveFile = file.substring(file.indexOf("filename=\"") + 10);
			saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
			saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));
			
			// int lastIndex = contentType.lastIndexOf("=");
			// String boundary = contentType.substring(lastIndex + 1,
			// contentType.length());
			// int pos;
			// pos = file.indexOf("filename=\"");
			// pos = file.indexOf("\n", pos) + 1;
			// pos = file.indexOf("\n", pos) + 1;
			// pos = file.indexOf("\n", pos) + 1;
			// int boundaryLocation = file.indexOf(boundary, pos) - 4;
			// int startPos = ((file.substring(0, pos)).getBytes()).length;
			// int endPos = ((file.substring(0,
			// boundaryLocation)).getBytes()).length;
			
			List<String> lines;
			String inputFile = new String(dataBytes);
			
			lines = Arrays.asList(inputFile.split("\n"));
			
			// row 0 = id number
			// row 1 = content info
			// row 2 =
			// row 3 =
			// row 4 = begin of data values separated by |
			
			int low = lines.size() - 2;
			// because the last one row is for id number
			
			for (int i = 4; i <= low; i++)
			{
				// System.out.println(lines.get(i));
				String[] values = lines.get(i).split("\\|");
				
				if (values.length >= 5)
				{
					Payment payment = new Payment(i,sessionData);
					payment.setNoTienda(values[0],sessionData);
					payment.setDatePaymentFile(values[1],sessionData);
					payment.setNoMembresia(values[2],sessionData);
					payment.setTicket(values[3],sessionData);
					payment.setUnidadesCompra(values[4],sessionData);
					
					payment.setMerchantId("----");
					payment.setPaymentType("----");
					payment.setDescPaymentType("----");
					
					StringBuffer extra = new StringBuffer();
					int sizeValues = values.length - 1;
					for (int j = 0; j <= sizeValues; j++)
					{
						if (j == sizeValues)
						{
							extra.append(values[j].trim());
						}
						else
						{
							extra.append(values[j] + "|");
						}
					}
					payment.setCompleteInfo(extra.toString());
					if (payment.getErrorLog().size() > 0)
					{
						payment.setStatus(Status.BADDATA,sessionData);
					}
					else
					{
						payment.setStatus(Status.NOPROCESS,sessionData);
					}
					
					paymentsrows.add(payment);
				}
				else
				{
					if (lines.get(i).trim().length() != 0)
					{
						this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.row_invalid", sessionData.getLanguage()) + " " + lines.get(i));
					}
				}
			}
		}
		
		if (paymentsrows.size() == 0)
		{
			this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.no_rows", sessionData.getLanguage()));
			return paymentsrows;
		}
		
		try
		{
			findHistoryMerchantsPayment(strRefId, strDistChainType);
			
		}
		catch (Exception e)
		{
			this.addErrorLog(e, " finding sql resources",sessionData);
			return paymentsrows;
		}
		
		if (merchants != null && merchants.size() > 0)
		{
			
			analizeArrayPayments(paymentsrows, this.merchants, this.merchantsReferenceNo, bank,sessionData);
			
			processPaymentsMerchantsOK(paymentsrows, userName, deploymentType, customConfigType, languaje,sessionData);
			
			// workingDir
			notifiesISOEmail("C:\\TEMP", mailHost, instance, email, paymentsrows,sessionData);
		}
		else
		{
			this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.no_merchants_find", sessionData.getLanguage()));
		}
		
		return paymentsrows;
		
	}
	
	/**
	 * @param workingDir
	 * @param mailHost
	 * @param instance
	 * @param email
	 * @param paymentsrows
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void notifiesISOEmail(String workingDir, String mailHost, String instance, String email, ArrayList<Payment> paymentsrows,SessionData sessionData)
	{
		String subject = Languages.getString("java.com.debisys.tools.paymentsbulk.email_subject", sessionData.getLanguage());
		String from = "processpaymentbulk@emida.net";
		String textMessage = Languages.getString("java.com.debisys.tools.paymentsbulk.email_no_respond", sessionData.getLanguage());
		String[] fileNames = new String[6];
		boolean resultFileGenerate = false;
		
		try
		{
			String fileNameOk = "";
			String fileNameBADDATA = "";
			String fileNameNOMERCHANT = "";
			String fileNameREPEATEDTICKET = "";
			String fileNameSHAREDCREDIT = "";
			String fileNameFAILPAY = "";
			
			java.lang.StringBuffer rowsOK = new StringBuffer();
			java.lang.StringBuffer rowsBADDATA = new StringBuffer();
			java.lang.StringBuffer rowsNOMERCHANT = new StringBuffer();
			java.lang.StringBuffer rowsREPEATEDTICKET = new StringBuffer();
			java.lang.StringBuffer rowsSHAREDCREDIT = new StringBuffer();
			java.lang.StringBuffer rowsFAILPAY = new StringBuffer();
			
			// workingDir="C:\\TEMP";
			
			for (Payment pay : paymentsrows)
			{
				if (pay.getStatus() == Status.OK)
				{
					rowsOK.append(pay.getCompleteInfo() + "\n");
					fileNameOk = workingDir + Status.OK.toString() + ".txt";
				}
				else
					if (pay.getStatus() == Status.BADDATA)
					{
						rowsBADDATA.append(pay.getCompleteInfo() + "\n");
						fileNameBADDATA = workingDir + Status.BADDATA.toString() + ".txt";
					}
					else
						if (pay.getStatus() == Status.NOMERCHANT)
						{
							rowsNOMERCHANT.append(pay.getCompleteInfo() + "\n");
							fileNameNOMERCHANT = workingDir + Status.NOMERCHANT.toString() + ".txt";
						}
						else
							if (pay.getStatus() == Status.REPEATEDTICKET)
							{
								rowsREPEATEDTICKET.append(pay.getCompleteInfo() + "\n");
								fileNameREPEATEDTICKET = workingDir + Status.REPEATEDTICKET.toString() + ".txt";
							}
							else
								if (pay.getStatus() == Status.SHAREDCREDIT)
								{
									rowsSHAREDCREDIT.append(pay.getCompleteInfo() + "\n");
									fileNameSHAREDCREDIT = workingDir + Status.SHAREDCREDIT.toString() + ".txt";
								}
								else
									if (pay.getStatus() == Status.FAILPAY)
									{
										
										rowsFAILPAY.append(pay.getCompleteInfo() + "\n");
										fileNameFAILPAY = workingDir + Status.FAILPAY.toString() + ".txt";
									}
			}
			
			if (generateFile(fileNameOk, fileNames, 0, rowsOK.toString(),sessionData))
			{
				resultFileGenerate = true;
			}
			if (generateFile(fileNameBADDATA, fileNames, 1, rowsBADDATA.toString(),sessionData))
			{
				resultFileGenerate = true;
			}
			if (generateFile(fileNameNOMERCHANT, fileNames, 2, rowsNOMERCHANT.toString(),sessionData))
			{
				resultFileGenerate = true;
			}
			if (generateFile(fileNameREPEATEDTICKET, fileNames, 3, rowsREPEATEDTICKET.toString(),sessionData))
			{
				resultFileGenerate = true;
			}
			if (generateFile(fileNameSHAREDCREDIT, fileNames, 4, rowsSHAREDCREDIT.toString(),sessionData))
			{
				resultFileGenerate = true;
			}
			if (generateFile(fileNameFAILPAY, fileNames, 5, rowsFAILPAY.toString(),sessionData))
			{
				resultFileGenerate = true;
			}
			
			if (resultFileGenerate)
			{
				this.sendMail(subject, mailHost, from, textMessage, instance, email, fileNames);
			}
			
		}
		catch (MessagingException e)
		{
			addErrorLog(e, "files info summary",sessionData);
			this.addErrorLog(e.getMessage());
		}
		finally
		{
			for (String fil : fileNames)
			{
				if (fil != null)
				{
					File fileEmail = new File(fil);
					fileEmail.delete();
				}
			}
		}
	}
	
	/**
	 * @param fileName
	 * @param fileNames
	 * @param position
	 * @param data
	 */
	private boolean generateFile(String fileName, String[] fileNames, int position, String data,SessionData sessionData)
	{
		byte dataBytes[] = null;
		boolean resultFile = false;
		if (fileName.length() > 0)
		{
			FileOutputStream fileOut;
			try
			{
				dataBytes = data.getBytes();
				fileOut = new FileOutputStream(fileName);
				fileOut.write(dataBytes);
				fileOut.flush();
				fileOut.close();
				fileNames[position] = fileName;
				resultFile = true;
			}
			catch (FileNotFoundException e)
			{
				addErrorLog(e, fileName,sessionData);
				
			}
			catch (IOException e)
			{
				addErrorLog(e, fileName,sessionData);
			}
		}
		return resultFile;
	}
	
	/**
	 * @param e
	 */
	private void addErrorLog(Exception e, String obs,SessionData sessionData)
	{
		if (e instanceof FileNotFoundException)
		{
			this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.file_file", sessionData.getLanguage()) + " " + obs);
			cat.error("File Exception ", e);
		}
		else
			if (e instanceof SQLException)
			{
				this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.persist_fail", sessionData.getLanguage()) + " " + obs);
				cat.error("SQL Exception ", e);
			}
			else
				if (e instanceof MessagingException)
				{
					this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.messagin_fail", sessionData.getLanguage()) + " " + obs);
					cat.error("MessagingException Exception ", e);
				}
				else
					if (e instanceof IOException)
					{
						this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.io_fail", sessionData.getLanguage()) + " " + obs);
						cat.error("MessagingException Exception ", e);
					}
	}
	
	/**
	 * @param description
	 */
	public void addErrorLog(String description)
	{
		cat.error(description);
		errorsProcess.add(description);
	}
	
	/**
	 * @param paymentsOK
	 * @param merchants
	 * @param merchantsReferenceNo
	 * @param firstTime
	 * @param noFirstTime
	 * @param bank
	 */
	private void analizeArrayPayments(ArrayList<Payment> paymentsOK, Map<String, String> merchants, ArrayList<String> merchantsReferenceNo,
			String bank,SessionData sessionData)
	{
		
		for (Payment pay : paymentsOK)
		{
			// Status.NOPROCESS means everything is ok so far
			if (pay.getStatus() == Status.NOPROCESS)
			{
				
				String values = merchants.get(pay.getNoMembresia());
				
				if (values != null)
				{
					// find the merchant
					String[] val = values.split("\\*");
					pay.setMerchantId(val[0]);
					
					// if merchant is not shared credit
					if (!val[2].equals("4"))
					{
						boolean existTicket = false;
						String ticketRepeated = new String();
						if (merchantsReferenceNo.size() != 0)
						{
							for (String merchantsReference : merchantsReferenceNo)
							{
								String[] pairMerchantReference = merchantsReference.split("\\*");
								if (pairMerchantReference[0].equals(pay.getMerchantId()))
								{
									// System.out.println(pairMerchantReference[1] + " " +
									// pay.getTicket());
									if (pairMerchantReference[1].equals(pay.getTicket()))
									{
										existTicket = true;
										break;
										
									}
								}
							}
							
							if (!existTicket)
							{
								ticketRepeated = pay.getMerchantId() + "*" + pay.getTicket();
								if (ticketRepeated.length() > 0)
								{
									merchantsReferenceNo.add(ticketRepeated);
								}
							}
							
						}
						else
						{
							merchantsReferenceNo.add(pay.getMerchantId() + "*" + pay.getTicket());
						}
						
						if (!existTicket)
						{
							pay.setBank(bank);
							
							// define paymnet type
							String amountPayments = val[1];
							if (amountPayments.equals("-1"))
							{
								pay.setPaymentType(this.idFirstTime);
								pay.setDescPaymentType(this.descFirstTime);
								
								merchants.put(pay.getNoMembresia().trim(), pay.getMerchantId().trim() + "*" + pay.getUnidadesCompra() + "*" + val[2]);
								
							}
							else
							{
								pay.setPaymentType(this.idnoFirstTime);
								pay.setDescPaymentType(this.descnoFirstTime);
							}
							
						}
						else
						{
							// ticket allready exist
							pay.setStatus(Status.REPEATEDTICKET,sessionData);
						}
					}
					else
					{
						// merchant is shared credit so, only rep can do pays.
						pay.setStatus(Status.SHAREDCREDIT,sessionData);
					}
					
				}
				else
				{
					// membership no exist, means merchant no exist
					pay.setStatus(Status.NOMERCHANT,sessionData);
				}
			}
		}
		// System.out.println("fin analize");
	}

	/**
	 * @param strRefId
	 * @param strDistChainType
	 * @throws SQLException
	 */
	public void findHistoryMerchantsPayment(String strRefId,
			String strDistChainType) throws SQLException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		boolean isLevel3ISOChain = strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL);
		
		try {
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new SQLException("Can't get database connection");
			}
			/*
			 * SELECT m.merchant_id AS merchant, m.control_number AS controlNo,
			 * sum(payment) AS payments, m.merchant_type AS pagos FROM merchants
			 * m WITH (NOLOCK) INNER JOIN reps r WITH (NOLOCK) ON m.rep_id =
			 * r.rep_id LEFT JOIN merchant_credits c ON
			 * m.merchant_id=c.merchant_id WHERE r.iso_id = ? AND
			 * m.control_number IS NOT NULL GROUP BY m.merchant_id,
			 * m.control_number, m.dba, m.merchant_type
			 */

			String strSQL = "";
			String bundleKey = isLevel3ISOChain? "getMerchantsHistoryPayment" : "getMerchantsHistoryPayment5Levels";	
			
			strSQL = sql_bundle.getString(bundleKey);			

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, Long.valueOf(strRefId));
			rs = pstmt.executeQuery();

			while (rs.next()) {
				String merchantId = rs.getString("merchantID");
				
				String controlNo = rs.getString("controlNo");				  
				
				if (controlNo == null || controlNo.trim().length() == 0)
					controlNo = "-1";

				String amountPayments = rs.getString("total");
				
				if (amountPayments == null || amountPayments.trim().length() == 0)					
					amountPayments = "-1";

				String merchantType = rs.getString("merchantType");
				
				if (merchantType == null || merchantType.trim().length() == 0)
					merchantType = "0";

				String value = merchantId + "*" + amountPayments + "*" + merchantType;

				merchants.put(controlNo, value);
			}
			
			TorqueHelper.closeStatement(pstmt, rs);
			
			bundleKey = isLevel3ISOChain ? "getMerchantsReferenceL3ISO" : "getMerchantsReferenceL5ISO";
			
			strSQL = sql_bundle.getString(bundleKey);
			
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, Long.valueOf(strRefId));
			rs = pstmt.executeQuery();

			while (rs.next()) {
				String merchantID = rs.getString("merchantID");
				String refNo = rs.getString("refNo");
				if (refNo != null && refNo.trim().length() > 0) {
					merchantsReferenceNo.add(merchantID + "*" + refNo);
				}
			}
		} catch (Exception e) {
			cat.error("Error during findHistoryMerchantsPayment", e);
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}

	}
	
	/**
	 * @param paymentsOK
	 * @param userName
	 * @param deploymentType
	 * @param customConfigType
	 */
	public void processPaymentsMerchantsOK(ArrayList<Payment> paymentsOK, String userName, String deploymentType, String customConfigType,
			String languaje,SessionData sessionData)
	{
		for (Payment pay : paymentsOK)
		{
			if (pay.getStatus() == Status.NOPROCESS)
			{
				executepayment(pay, userName, deploymentType, customConfigType, languaje,sessionData);
			}
		}
	}
	
	/**
	 * @param pay
	 * @param userName
	 * @param deploymentType
	 * @param customConfigType
	 */
	private void executepayment(Payment pay, String userName, String deploymentType, String customConfigType, String languaje,SessionData sessionData)
	{
		Connection dbConn = null;
		CallableStatement cst = null;
		try
		{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			dbConn.setAutoCommit(false);
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}
			String strSQL = sql_bundle.getString("applyMerchantPayment");
			cst = dbConn.prepareCall(strSQL);
			
			cst = dbConn.prepareCall(strSQL);
			cst.setLong(1, Long.valueOf(pay.getMerchantId()));
			cst.setDouble(2, Double.parseDouble(pay.getUnidadesCompra()));
			
			/*
			 * long t = pay.getDatePayment().getTime(); Calendar cl = Calendar.getInstance();
			 * cl.setTimeInMillis(t); int day = cl.get(Calendar.DATE); int month =
			 * cl.get(Calendar.MONTH)+1; int year = cl.get(Calendar.YEAR); int hour =
			 * cl.get(Calendar.HOUR); int seconds = cl.get(Calendar.MINUTE); String parseDatePay =
			 * day+"/"+month+"/"+year+" "+hour+":"+seconds;
			 */
			cst.setString(3, pay.getDatePaymentString());
			
			cst.setDouble(4, 0);
			cst.setInt(5, 1);
			cst.setNull(6, Types.VARCHAR);
			cst.setNull(7, Types.VARCHAR);
			cst.setString(8, pay.getTicket());
			cst.setString(9, userName);
			cst.setString(10, deploymentType);
			cst.setString(11, customConfigType);
			cst.setInt(12, Integer.parseInt(pay.getPaymentType()));
			cst.setInt(13, Integer.parseInt(pay.getBank()));
			cst.setInt(14, Integer.parseInt(languaje));
			
			cst.registerOutParameter(15, Types.INTEGER);
			cst.setInt(15, 0);
			cst.registerOutParameter(16, Types.VARCHAR);
			cst.setString(16, "");
			
			cst.execute();
			int result = cst.getInt(15);
			if (result == 0)
			{
				pay.setStatus(Status.OK,sessionData);
			}
			else
			{
				String reason = cst.getString(16).trim();
				pay.setStatus(Status.FAILPAY,sessionData);
				pay.setErrorInsertDescription(reason);
			}
			dbConn.commit();
			dbConn.setAutoCommit(true);
			
		}
		catch (SQLException sqe)
		{
			cat.error("Error SQLException during executepayment", sqe);
			pay.setStatus(Status.FAILPAY,sessionData);
			pay.setErrorInsertDescription(sqe.getMessage().trim());
			// this.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.persist_fail_saving", sessionData.getLanguage())
			// +" ticket : "+ pay.getTicket().substring(0,
			// pay.getTicket().length()-4));
			if (dbConn != null)
			{
				try
				{
					dbConn.rollback();
				}
				catch (Exception ex)
				{
					cat.error("Error SQLException during executepayment-rollback", sqe);
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error Exception during executepayment", e);
			pay.setStatus(Status.FAILPAY,sessionData);
			if (dbConn != null)
			{
				try
				{
					dbConn.rollback();
				}
				catch (Exception ex)
				{
					cat.error("Error Exception during executepayment-rollback", e);
				}
			}
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, cst, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		
	}
	
	/**
	 * @param strRefId
	 * @return
	 */
	public ArrayList<Base> findBanksByIso(String strRefId)
	{
		ArrayList<Base> arrayBanks = new ArrayList<Base>();
		Connection dbConn = null;
		
		try
		{
			
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}
			String strSQL = sql_bundle.getString("getBanksByIso");
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			// pstmt.setLong(1, Long.valueOf(strRefId));
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Base bank = new Base();
				bank.setId(rs.getString(1));
				bank.setDescription(rs.getString(2));
				arrayBanks.add(bank);
			}
			rs.close();
			pstmt.close();
			
		}
		catch (Exception e)
		{
			cat.error("Error during findBanksByIso", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return arrayBanks;
		
	}
	
	/**
	 * @return
	 */
	public ArrayList<Base> findPaymentsTypes()
	{
		ArrayList<Base> arrayPayments = new ArrayList<Base>();
		Connection dbConn = null;
		
		try
		{
			
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}
			String strSQL = sql_bundle.getString("getPaymentsTypes");
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Base paymentsTypes = new Base();
				paymentsTypes.setId(rs.getString(1));
				paymentsTypes.setDescription(rs.getString(2));
				arrayPayments.add(paymentsTypes);
			}
			rs.close();
			pstmt.close();
			
		}
		catch (Exception e)
		{
			cat.error("Error during findPaymentsTypes", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return arrayPayments;
		
	}
	
	/**
	 * @param subject
	 * @param mailHost
	 * @param sMails
	 * @param from
	 * @param textMessage
	 * @param instance
	 * @param toEmail
	 * @param filenames
	 * @throws MessagingException
	 */
	private void sendMail(String subject, String mailHost, String from, String textMessage, String instance, String toEmail, String[] filenames)
			throws MessagingException
	{
		// String sMails,
		java.util.Properties props;
		props = System.getProperties();
		props.put("mail.smtp.host", mailHost);
		Session s = Session.getDefaultInstance(props, null);
		
		MimeMessage message = new MimeMessage(s);
		
		message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
		// message.setFrom(new InternetAddress(from));
		message.setFrom(new InternetAddress("nmartinez@emida.net"));
		
		String[] toEmailEnd = toEmail.split("\\;");
		if (toEmailEnd.length > 0)
		{
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmailEnd[0]));
			StringBuffer carbonCopy = new StringBuffer();
			for (String cc : toEmailEnd)
			{
				if (cc.length() > 0)
					carbonCopy.append(cc + ",");
			}
			if (carbonCopy != null)
			{
				if (carbonCopy.length() > 0)
				{
					message.setRecipients(Message.RecipientType.CC, carbonCopy.toString());
				}
			}
		}
		
		message.setSubject(subject);
		
		Multipart mp = new MimeMultipart();
		
		// attach the files to the message
		for (String filename : filenames)
		{
			if (filename != null)
			{
				FileDataSource fds = new FileDataSource(filename);
				MimeBodyPart mbp2 = new MimeBodyPart();
				mbp2.setDataHandler(new DataHandler(fds));
				mbp2.setFileName(fds.getName());
				
				mp.addBodyPart(mbp2);
			}
		}
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText(textMessage + "\n\n");
		mp.addBodyPart(messageBodyPart);
		message.setContent(mp);
		
		Transport.send(message);
		
	}
	
}
