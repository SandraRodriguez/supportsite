package com.debisys.tools.paymentsbulk;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;

public class Payment {
	
	public String noTienda;
	public Date datePayment;
	public String datePaymentString;
	public String noMembresia;
	public String ticket;
	public String unidadesCompra;
	private Integer row;
	private Status status;
	private String merchantId;
	private String paymentType;
	private String bank;
	private String obs;
	private String completeInfo;
	private ArrayList<String> errorLog = new ArrayList<String>();
	private String errorInsertDescription;
	private String descPaymentType;
	
	
	/**
	 * @return the datePayment
	 */
	public String getDatePaymentString()
	{
		return datePaymentString;
	}
	
	
	/**
	 * @return the datePayment
	 */
	public Date getDatePayment()
	{
		return datePayment;
	}

	/**
	 * @param datePayment the datePayment to set
	 */
	public void setDatePayment(Date datePayment)
	{
		this.datePayment = datePayment;
	}
	
	
	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	public String getDescPaymentType() {
		return descPaymentType;
	}

	public void setDescPaymentType(String descPaymentType) {
		this.descPaymentType = descPaymentType;
	}

	public String getErrorInsertDescription() {
		return errorInsertDescription;
	}

	public void setErrorInsertDescription(String errorInsertDescription) {
		this.errorInsertDescription = errorInsertDescription;
	}

	/**
	 * @return
	 */
	public String getCompleteInfo() {
		String finalDescription="";
		java.lang.StringBuffer chainErrorLog = new StringBuffer();
		for(String errorDesc : this.errorLog)
		{
			chainErrorLog.append(errorDesc+" ");
		}
	  if (this.getErrorInsertDescription()!=null){
		  finalDescription=completeInfo+"|"+this.getObs()+"|"+this.getErrorInsertDescription();
	  }
	  else{
		  finalDescription= completeInfo+"|"+this.getObs();
	  }
	  
	  if (chainErrorLog.toString().length()>0){
		  finalDescription = finalDescription+"|"+chainErrorLog.toString();
	  }
	  return finalDescription;
	}

	public void setCompleteInfo(String completeInformation) {
		this.completeInfo = completeInformation;
	}

	public enum Status {
		OK, BADDATA, NOMERCHANT, REPEATEDTICKET, FAILPAY,SHAREDCREDIT,NOPROCESS
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs.trim();
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status, SessionData sessionData) {
		this.status = status;
		
		if (this.status==Status.BADDATA){
			this.setObs(Languages.getString("java.com.debisys.tools.paymentsbulk.Payment.BADDATA", sessionData.getLanguage()));
			
		}
		else if (this.status==Status.FAILPAY){
			this.setObs(Languages.getString("java.com.debisys.tools.paymentsbulk.Payment.FAILPAY", sessionData.getLanguage()));
			
		}
		else if (this.status==Status.REPEATEDTICKET){
			this.setObs(Languages.getString("java.com.debisys.tools.paymentsbulk.Payment.REPEATEDTICKET", sessionData.getLanguage()));
			
		}
		else if (this.status==Status.OK){
			this.setObs(Languages.getString("java.com.debisys.tools.paymentsbulk.Payment.OK", sessionData.getLanguage()));
			
		}
		else if (this.status==Status.NOMERCHANT){
			this.setObs(Languages.getString("java.com.debisys.tools.paymentsbulk.Payment.NOMERCHANT", sessionData.getLanguage()));
			
		}
		else if (this.status==Status.SHAREDCREDIT){
			this.setObs(Languages.getString("java.com.debisys.tools.paymentsbulk.Payment.SHAREDCREDIT", sessionData.getLanguage()));
			
		}
		else if (this.status==Status.NOPROCESS){
			this.setObs(Languages.getString("java.com.debisys.tools.paymentsbulk.Payment.NOPROCESS", sessionData.getLanguage()));
			
		}
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId
	 *            the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the errorLog
	 */
	public ArrayList<String> getErrorLog() {
		return errorLog;
	}

	public Payment(Integer rowId,SessionData sessionData) {
		this.setRow(rowId);
		this.setStatus(status.OK,sessionData);

	}

	/**
	 * @return the row
	 */
	public Integer getRow() {
		return row;
	}

	/**
	 * @param row
	 *            the row to set
	 */
	public void setRow(Integer row) {
		this.row = row;
	}

	/**
	 * @return the noTienda
	 */
	public String getNoTienda() {
		return noTienda;
	}

	/**
	 * @param ooTienda
	 *            the ooTienda to set
	 */
	public void setNoTienda(String noTienda,SessionData sessionData) {
		if (noTienda.length() > 4) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.store_length", sessionData.getLanguage()),sessionData);
		}
		if (!IsNumeric(noTienda)) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.store_number", sessionData.getLanguage()),sessionData);
		}

		this.noTienda = noTienda;
	}

	
	/**
	 * @param fechahoera
	 *            the fechahoera to set
	 */
	public void setDatePaymentFile(String fechahoera,SessionData sessionData) {
		
		if (fechahoera.length() > 16) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.date_length", sessionData.getLanguage()),sessionData);
		}

		if (fechahoera == null) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.date_empty", sessionData.getLanguage()),sessionData);
		}

		// set the format to use as a constructor argument
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		if (fechahoera.trim().length() != dateFormat.toPattern().length()) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.date_invalid", sessionData.getLanguage()),sessionData);
		}

		dateFormat.setLenient(false);

		try {
			// parse the inDate parameter
			Date payDate = dateFormat.parse(fechahoera.trim());
			String spayDate = dateFormat.format(payDate);
			
			Date now = Calendar.getInstance().getTime();
						
			if (payDate.after(now)){
				addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.date_great", sessionData.getLanguage()),sessionData);
			}	
			this.setDatePayment(payDate);
			this.datePaymentString = spayDate;
			
			
		} catch (ParseException pe) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.date_invalid_parse", sessionData.getLanguage()),sessionData);
		}
		
	}

	/**
	 * @return the noMembresia
	 */
	public String getNoMembresia() {
		return noMembresia;
	}

	/**
	 * @param noMembresia
	 *            the noMembresia to set
	 */
	public void setNoMembresia(String noMembresia,SessionData sessionData) {
		
		if (noMembresia.length() > 15) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.membership_length", sessionData.getLanguage()),sessionData);
		}
		if (!IsNumeric(noTienda)) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.membership_number", sessionData.getLanguage()),sessionData);
		}
		this.noMembresia = noMembresia;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket,SessionData sessionData) {
		
		if (ticket.length() > 19) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.ticket_length", sessionData.getLanguage()),sessionData);
		}
		if (!IsNumeric(ticket)) {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.ticket_number", sessionData.getLanguage()),sessionData);
		}
		int year = Calendar.getInstance().get(Calendar.YEAR);
		this.ticket = ticket+year;	
		
	}

	/**
	 * @return the unidadesCompra
	 */
	public String getUnidadesCompra() {
		return unidadesCompra;
	}

	/**
	 * @param unidadesCompra
	 *            the unidadesCompra to set
	 */
	public void setUnidadesCompra(String unidadesCompra,SessionData sessionData) {
		String strCurrency=unidadesCompra;
		if (unidadesCompra != null && !unidadesCompra.equals("")) {
			try {
				DecimalFormat df = new DecimalFormat("###0.0");
				df.setMaximumFractionDigits(2);
				df.setMinimumFractionDigits(2);
				df.parse(unidadesCompra);
				strCurrency = df.format(Double.parseDouble(unidadesCompra));
			} catch (NumberFormatException nfe) {
				addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.amount_format", sessionData.getLanguage()),sessionData);
			}
			catch (ParseException pfe) {
				addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.amount_parse", sessionData.getLanguage()),sessionData);
			}
		} else {
			addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.amount_required", sessionData.getLanguage()),sessionData);
		}
		this.unidadesCompra = strCurrency;
	}

	
	public void addErrorLog(String error,SessionData sessionData) {
		//System.out.println(error);
		errorLog.add(error);
		this.setStatus(Status.BADDATA,sessionData);
	}

	/**
	 * @param value
	 * @return
	 */
	public static boolean IsNumeric(String value) {

		for (int i = 0; i <= value.length() - 1; i++) {
			if (value.charAt(i) < '0' || value.charAt(i) > '9') {
				return false;
			}
		}
		return true;
	}

}
