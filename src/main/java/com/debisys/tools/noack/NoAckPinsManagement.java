package com.debisys.tools.noack;

import com.debisys.exceptions.ReportException;
import com.debisys.util.Constants;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class NoAckPinsManagement {

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");

    private static Logger cat = Logger.getLogger(NoAckPinsManagement.class);
    
    public NoAckPinsManagement() {
    }

    /**
     * 
     * @param userId
     * @param trxId 
     */
    public static void chargePinAmount(String userId, int trxId) {
        PinsIsolatedPojo pinsIsolated = findStatement(trxId);
        if (pinsIsolated != null) {
            executeStatement(pinsIsolated);
            if (trxExist(pinsIsolated)) {
                changePinIsolated(pinsIsolated, StatuPinIsolated.CHARGED, "Pin charged by user=["+userId+"]", Constants.STATUS_PIN_SOLD);
            }
        }
    }

    /**
     * 
     * @param trxId
     * @return 
     */
    private static PinsIsolatedPojo findStatement(int trxId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String strSQL = "";
        PinsIsolatedPojo result = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = "SELECT p.Id, p.LogTrx, p.controlNo, p.Rec_id, p.Pin FROM PinsIsolated p WITH(NOLOCK) WHERE p.Rec_id=? AND p.StatusId=?";

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, trxId);
            pstmt.setInt(2, StatuPinIsolated.PROCESSED.getValue());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = new PinsIsolatedPojo();
                result.setId(rs.getInt("Id"));
                result.setLogTrx(rs.getString("LogTrx"));
                result.setControlNo(rs.getInt("ControlNo"));
                result.setRec_id(rs.getInt("Rec_id"));
                result.setPin(rs.getString("Pin"));
            }
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during findExecutiveById", e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection on findStatement", e);
            }
        }
        return result;
    }

    /**
     * 
     * @param pinIsolated 
     */
    private static void executeStatement(PinsIsolatedPojo pinIsolated) {
        Connection dbConn = null;
        CallableStatement cstmt = null;
        String strSQL;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = "EXEC " + pinIsolated.getLogTrx();
            cstmt = dbConn.prepareCall(strSQL);
            cstmt.execute();
            cstmt.close();
        } catch (Exception e) {
            cat.error("Error during executeStatement ", e);
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection on executeStatement  ", e);
            }
        }
    }

    /**
     * 
     * @param pinIsolated
     * @param status
     * @param message
     * @param statusPin 
     */
    private static void changePinIsolated(PinsIsolatedPojo pinIsolated, StatuPinIsolated status, String message, int statusPin) {

        String sqlPinsIsolated = "UPDATE PinsIsolated SET StatusId = ? WHERE Rec_Id = ?";
        String sqlPinsIsolatedHistory = "INSERT INTO PinsIsolatedHistory (PinsIsolatedId,Comments,DateTime,StatusId) VALUES (?,?,GETDATE(),?)";        
        String sqlPins = "UPDATE Pins SET pinStatusId=?, activation_date=ISNULL(activation_date, getDate()) Where control_no=? and pin=?";

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        int recordsAffected = 0;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            pstmt = dbConn.prepareStatement(sqlPins);
            pstmt.setInt(1, statusPin);
            pstmt.setInt(2, pinIsolated.getControlNo());
            pstmt.setString(3, pinIsolated.getPin());
            recordsAffected = pstmt.executeUpdate();
            pstmt.close();

            if (recordsAffected == 1) {
                cat.info("Pin updated OK for controlNo=["+pinIsolated.getControlNo()+"]");
                pstmt = dbConn.prepareStatement(sqlPinsIsolated);
                pstmt.setInt(1, status.getValue());
                pstmt.setInt(2, pinIsolated.getRec_id());

                recordsAffected = pstmt.executeUpdate();
                pstmt.close();

                if (recordsAffected == 1) {
                    cat.info("PinsIsolated updated OK for rec_id=["+pinIsolated.getRec_id()+"]");
                    pstmt = dbConn.prepareStatement(sqlPinsIsolatedHistory);
                    pstmt.setInt(1, pinIsolated.getId());
                    pstmt.setString(2, message);
                    pstmt.setInt(3, status.getValue());
                    recordsAffected = pstmt.executeUpdate();
                    if (recordsAffected==1){                        
                        cat.info("PinsIsolatedHistory inserted OK for rec_id=["+pinIsolated.getRec_id()+"]");
                    }
                    pstmt.close();
                }
            }

        } catch (Exception e) {
            cat.error("Error during findExecutiveById", e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection on findStatement", e);
            }
        }
    }

    /**
     * 
     * @param pinsIsolated
     * @return 
     */
    private static boolean trxExist(PinsIsolatedPojo pinsIsolated) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String strSQL = "";
        Boolean result = false;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = "SELECT COUNT(*) FROM Transactions t WITH(NOLOCK) WHERE t.rec_id=? ";

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, pinsIsolated.getRec_id());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                result = true;
            }
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during trxExist ", e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection on trxExist ", e);
            }
        }
        return result;
    }
}
