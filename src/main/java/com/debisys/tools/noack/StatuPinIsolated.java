package com.debisys.tools.noack;

/**
 *
 * @author nmartinez
 */
public enum StatuPinIsolated {
    
    INSERTED(1), PROCESSED(2), WAITING(3), CHECK_PROVIDER(4), CHARGED(5);
    private final int value;
    

    private StatuPinIsolated(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
