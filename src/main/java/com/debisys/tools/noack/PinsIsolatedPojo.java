package com.debisys.tools.noack;

/**
 *
 * @author nmartinez
 */
public class PinsIsolatedPojo {

    private Integer Id;
    private Integer Rec_id;
    private Integer Millennium_no;
    private String Pin;
    private Integer StatusId;
    private Integer Invoice;
    private String ReceiveString;
    private Integer ProviderId;
    private Integer ProductId;
    private Integer ControlNo;
    private String LogTrx;

    /**
     * @return the Id
     */
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return the Rec_id
     */
    public Integer getRec_id() {
        return Rec_id;
    }

    /**
     * @param Rec_id the Rec_id to set
     */
    public void setRec_id(Integer Rec_id) {
        this.Rec_id = Rec_id;
    }

    /**
     * @return the Millennium_no
     */
    public Integer getMillennium_no() {
        return Millennium_no;
    }

    /**
     * @param Millennium_no the Millennium_no to set
     */
    public void setMillennium_no(Integer Millennium_no) {
        this.Millennium_no = Millennium_no;
    }

    /**
     * @return the Pin
     */
    public String getPin() {
        return Pin;
    }

    /**
     * @param Pin the Pin to set
     */
    public void setPin(String Pin) {
        this.Pin = Pin;
    }

    /**
     * @return the StatusId
     */
    public Integer getStatusId() {
        return StatusId;
    }

    /**
     * @param StatusId the StatusId to set
     */
    public void setStatusId(Integer StatusId) {
        this.StatusId = StatusId;
    }

    /**
     * @return the Invoice
     */
    public Integer getInvoice() {
        return Invoice;
    }

    /**
     * @param Invoice the Invoice to set
     */
    public void setInvoice(Integer Invoice) {
        this.Invoice = Invoice;
    }

    /**
     * @return the ReceiveString
     */
    public String getReceiveString() {
        return ReceiveString;
    }

    /**
     * @param ReceiveString the ReceiveString to set
     */
    public void setReceiveString(String ReceiveString) {
        this.ReceiveString = ReceiveString;
    }

    /**
     * @return the ProviderId
     */
    public Integer getProviderId() {
        return ProviderId;
    }

    /**
     * @param ProviderId the ProviderId to set
     */
    public void setProviderId(Integer ProviderId) {
        this.ProviderId = ProviderId;
    }

    /**
     * @return the ProductId
     */
    public Integer getProductId() {
        return ProductId;
    }

    /**
     * @param ProductId the ProductId to set
     */
    public void setProductId(Integer ProductId) {
        this.ProductId = ProductId;
    }

    /**
     * @return the ControlNo
     */
    public Integer getControlNo() {
        return ControlNo;
    }

    /**
     * @param ControlNo the ControlNo to set
     */
    public void setControlNo(Integer ControlNo) {
        this.ControlNo = ControlNo;
    }

    /**
     * @return the LogTrx
     */
    public String getLogTrx() {
        return LogTrx;
    }

    /**
     * @param LogTrx the LogTrx to set
     */
    public void setLogTrx(String LogTrx) {
        this.LogTrx = LogTrx;
    }
    
    
    
}
