package com.debisys.tools.dto;

public class S2kStockCode {

	private String productId;
	private String description;
	private String stockCode;
	private boolean excludeTax;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public boolean isExcludeTax() {
		return excludeTax;
	}
	public void setExcludeTax(boolean excludeTax) {
		this.excludeTax = excludeTax;
	}
}
