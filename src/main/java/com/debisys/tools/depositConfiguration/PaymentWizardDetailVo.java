
package com.debisys.tools.depositConfiguration;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class PaymentWizardDetailVo {
    
    private String id;
    private String paymentWizardId;
    private String label;
    private int fieldLength;
    private int fieldMinLength;
    private String fieldType;
    private String fieldPrefix;
    private String helpText;
    private Long imageId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentWizardId() {
        return paymentWizardId;
    }

    public void setPaymentWizardId(String paymentWizardId) {
        this.paymentWizardId = paymentWizardId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getFieldLength() {
        return fieldLength;
    }

    public void setFieldLength(int fieldLength) {
        this.fieldLength = fieldLength;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldPrefix() {
        return fieldPrefix;
    }

    public void setFieldPrefix(String fieldPrefix) {
        this.fieldPrefix = fieldPrefix;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public int getFieldMinLength() {
        return fieldMinLength;
    }

    public void setFieldMinLength(int fieldMinLength) {
        this.fieldMinLength = fieldMinLength;
    }
    
}
