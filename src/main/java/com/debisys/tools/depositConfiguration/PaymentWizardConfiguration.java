package com.debisys.tools.depositConfiguration;

import com.debisys.exceptions.ImageException;
import com.debisys.images.Image;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Category;
import sun.misc.BASE64Encoder;

/**
 *
 * @author dgarzon
 */
public class PaymentWizardConfiguration {

    public final static String IMAGE_TYPE_PAYMENT_WIZARD = "PAYMENT_WIZARD";
    private static byte[] dataImage = null;

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.depositConfiguration.sql");
    static Category cat = Category.getInstance(PaymentWizardConfiguration.class);

    public static List<PaymentWizardVo> getPaymentWizardList() {
        List<PaymentWizardVo> list = new ArrayList<PaymentWizardVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("paymentWizardAllList");

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                PaymentWizardVo paymentWizard = new PaymentWizardVo();
                paymentWizard.setId(rs.getString("id"));
                paymentWizard.setDescription(rs.getString("description"));
                paymentWizard.setEnabled(rs.getBoolean("enabled"));
                paymentWizard.setBanksDepositTypes(BanksDepositTypesConf.getBanksDepositTypesById(rs.getString("banksDepositTypesId")));
                list.add(paymentWizard);
            }
        } catch (Exception e) {
            cat.error("Error during getPaymentWizardList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static PaymentWizardVo getPaymentWizardById(String paymentWizardId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("paymentWizardById");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, paymentWizardId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                PaymentWizardVo paymentWizard = new PaymentWizardVo();
                paymentWizard.setId(rs.getString("id"));
                paymentWizard.setDescription(rs.getString("description"));
                paymentWizard.setEnabled(rs.getBoolean("enabled"));
                paymentWizard.setBanksDepositTypes(BanksDepositTypesConf.getBanksDepositTypesById(rs.getString("banksDepositTypesId")));
                paymentWizard.setListDetail(getPaymentWizardDetail(rs.getString("id")));
                return paymentWizard;
            }
        } catch (Exception e) {
            cat.error("Error during getPaymentWizardList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static PaymentWizardVo getPaymentWizardByBanksDepositTypeId(String bankDepositTypeId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("paymentWizardByBanksDepositTypeId");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, bankDepositTypeId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                PaymentWizardVo paymentWizard = new PaymentWizardVo();
                paymentWizard.setId(rs.getString("id"));
                paymentWizard.setDescription(rs.getString("description"));
                paymentWizard.setEnabled(rs.getBoolean("enabled"));
                paymentWizard.setBanksDepositTypes(BanksDepositTypesConf.getBanksDepositTypesById(rs.getString("banksDepositTypesId")));
                paymentWizard.setListDetail(getPaymentWizardDetail(rs.getString("id")));
                return paymentWizard;
            }
        } catch (Exception e) {
            cat.error("Error during getPaymentWizardByBanksDepositTypeId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static List<PaymentWizardDetailVo> getPaymentWizardDetail(String parentId) {
        List<PaymentWizardDetailVo> list = new ArrayList<PaymentWizardDetailVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("paymentWizardDetailAllListByParent");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, parentId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                PaymentWizardDetailVo pwDetail = new PaymentWizardDetailVo();
                pwDetail.setId(rs.getString("id"));
                pwDetail.setPaymentWizardId(rs.getString("paymentWizardId"));
                pwDetail.setLabel(rs.getString("label"));
                pwDetail.setFieldMinLength(rs.getInt("fieldMinLength"));
                pwDetail.setFieldLength(rs.getInt("fieldLength"));
                pwDetail.setFieldType(rs.getString("fieldType"));
                pwDetail.setFieldPrefix(rs.getString("fieldPrefix"));
                pwDetail.setHelpText(rs.getString("helpText"));
                pwDetail.setImageId(rs.getLong("imageId"));
                list.add(pwDetail);
            }
        } catch (Exception e) {
            cat.error("Error during getPaymentWizardDetail", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static void paymentWizardInsert(PaymentWizardVo paymentWizard) {
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String strSQL = sql_bundle.getString("paymentWizardInsert");
            cat.debug("Inserting paymentWizardInsert" + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            String key = UUID.randomUUID().toString().toUpperCase();
            pst.setString(1, key);
            pst.setString(2, paymentWizard.getBanksDepositTypes().getId());
            pst.setString(3, paymentWizard.getDescription());
            pst.setBoolean(4, paymentWizard.isEnabled());
            pst.executeUpdate();

            paymentWizardDetailInsertList(paymentWizard.getListDetail(), key);

        } catch (Exception e) {
            cat.error("Error inserting paymentWizardInsert", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void paymentWizardUpdate(PaymentWizardVo paymentWizard) {
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String strSQL = sql_bundle.getString("paymentWizardUpdate");
            cat.debug("Updating paymentWizardUpdate" + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            pst.setString(1, paymentWizard.getBanksDepositTypes().getId());
            pst.setString(2, paymentWizard.getDescription());
            pst.setBoolean(3, paymentWizard.isEnabled());
            pst.setString(4, paymentWizard.getId());
            pst.executeUpdate();

            deletePaymentWizardDetail(paymentWizard.getId());
            paymentWizardDetailInsertList(paymentWizard.getListDetail(), paymentWizard.getId());

        } catch (Exception e) {
            cat.error("Error in paymentWizardUpdate", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void deletePaymentWizard(String id) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            deletePaymentWizardDetail(id);// delete detail 
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("paymentWizardDelete");
            cat.debug("Query paymentWizardDelete " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, id);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error deleting paymentWizardDelete", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void deletePaymentWizardDetail(String parentId) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("paymentWizardDetailDelete");
            cat.debug("Query paymentWizardDetailDelete " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, parentId);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error deleting paymentWizardDetailDelete", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void paymentWizardDetailInsertList(List<PaymentWizardDetailVo> detailList, String paymentWizardId) {
        for (PaymentWizardDetailVo dt : detailList) {
            paymentWizardDetailInsert(dt, paymentWizardId);
        }
    }

    public static void paymentWizardDetailInsert(PaymentWizardDetailVo paymentWizardDetailVo, String paymentWizardId) {
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {

            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String strSQL = sql_bundle.getString("paymentWizardDetailInsert");
            cat.debug("Inserting paymentWizardDetailInsert" + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            String key = UUID.randomUUID().toString().toUpperCase();
            pst.setString(1, key);
            pst.setString(2, paymentWizardId);
            pst.setString(3, paymentWizardDetailVo.getLabel());
            pst.setInt(4, paymentWizardDetailVo.getFieldLength());
            pst.setString(5, paymentWizardDetailVo.getFieldType());
            pst.setString(6, paymentWizardDetailVo.getFieldPrefix());
            pst.setString(7, paymentWizardDetailVo.getHelpText());
            if (paymentWizardDetailVo.getImageId() != null && paymentWizardDetailVo.getImageId() != 0) {
                pst.setLong(8, paymentWizardDetailVo.getImageId());
            }
            else {
                pst.setNull(8, Types.INTEGER);
            }
            pst.setInt(9, paymentWizardDetailVo.getFieldMinLength());

            pst.executeUpdate();
            dataImage = null;

        } catch (Exception e) {
            cat.error("Error inserting paymentWizardDetailInsert", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static String getImageBytes(String paymentWizardById) {
        try {
            PaymentWizardVo paymentWizard = getPaymentWizardById(paymentWizardById);
            if (paymentWizard != null) {

                Long imageId = paymentWizard.getListDetail().get(0).getImageId();
                Image img = new Image();
                img.setImageId(imageId);
                img.load();
                return img.getData();
            }
        } catch (ImageException ex) {
            cat.error("Error getImageBytes", ex);
        }
        return null;
    }

    public static Map<String, String> uploadImage(HttpServletRequest request) {
        //process only if its multipart content

        Map<String, String> mapValues = new HashMap<String, String>();
        Long imageId = null;
        String paymentWizardId = null;

        if (ServletFileUpload.isMultipartContent(request)) {

            try {

                List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                for (FileItem multipartItem : multiparts) {

                    if (!multipartItem.isFormField()) {
                        dataImage = multipartItem.get();
                        imageId = saveImage(multipartItem.getName());
                    }
                    else if (multipartItem.isFormField()) {
                        if (multipartItem.getFieldName().equalsIgnoreCase("paymentWizardId")) {
                            paymentWizardId = multipartItem.getString();
                        }
                    }
                }

                if (imageId != null && paymentWizardId != null) {
                    PaymentWizardVo paymentWizardById = getPaymentWizardById(paymentWizardId);
                    paymentWizardById.getListDetail().get(0).setImageId(imageId);
                    paymentWizardUpdate(paymentWizardById);
                }

                request.setAttribute("message", "File Uploaded Successfully");

            } catch (Exception ex) {
                request.setAttribute("message", "File Upload Failed due to " + ex);
            }
        }
        else {
            request.setAttribute("message", "Sorry this Servlet only handles file upload request");
        }
        return mapValues;

    }

    private static Long saveImage(String fileName) throws ImageException {

        if (dataImage != null) {
            Image image = new Image();
            image.setName(fileName);
            image.setType(IMAGE_TYPE_PAYMENT_WIZARD);
            image.setAlternativeText("");
            image.setDescription("");
            image.setData(new BASE64Encoder().encodeBuffer(dataImage));
            long imageId = image.save();
            image.setName(imageId + "_" + fileName);
            image.save();

            image.saveData();
            return imageId;
        }
        return null;
    }

}
