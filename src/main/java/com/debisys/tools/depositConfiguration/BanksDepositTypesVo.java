
package com.debisys.tools.depositConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dgarzon
 */
public class BanksDepositTypesVo {
    
    private String id;
    private String banksId;
    private String bankName;
    private String depositTypeSelected;
    private List<DepositTypesVo> depositTypeList;

    public BanksDepositTypesVo() {
        depositTypeList = new ArrayList<DepositTypesVo>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBanksId() {
        return banksId;
    }

    public void setBanksId(String banksId) {
        this.banksId = banksId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public List<DepositTypesVo> getDepositTypeList() {
        return depositTypeList;
    }

    public void setDepositTypeList(List<DepositTypesVo> depositTypeList) {
        this.depositTypeList = depositTypeList;
    }

    public void addDepositType(DepositTypesVo depositTypesVo){
        depositTypeList.add(depositTypesVo);
    }

    public String getDepositTypeSelected() {
        return depositTypeSelected;
    }

    public void setDepositTypeSelected(String depositTypeSelected) {
        this.depositTypeSelected = depositTypeSelected;
    }
    
    public boolean existDepositTypeId(String depositTypeId){
        for (DepositTypesVo depositTypeList1 : depositTypeList) {
            if(depositTypeList1.getId().equalsIgnoreCase(depositTypeId)){
                return true;
            }
        }
        return false;
    }
    
}
