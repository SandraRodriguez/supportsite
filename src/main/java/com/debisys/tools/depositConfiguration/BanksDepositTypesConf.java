package com.debisys.tools.depositConfiguration;

import com.debisys.reports.banks.Banks;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class BanksDepositTypesConf {

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.depositConfiguration.sql");
    static Category cat = Category.getInstance(BanksDepositTypesConf.class);

    public static List<BanksDepositTypesVo> getBanksDepositTypesAllList() {
        List<BanksDepositTypesVo> list = new ArrayList<BanksDepositTypesVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("banksDepositTypesAllList");

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                BanksDepositTypesVo banksDepositType = new BanksDepositTypesVo();
                banksDepositType.setId(rs.getString("id"));
                banksDepositType.setBanksId(rs.getString("banksId"));
                banksDepositType.setBankName(getBankById(rs.getString("banksId")).getName());
                banksDepositType.setDepositTypeSelected(rs.getString("depositTypesId"));
                list.add(banksDepositType);
            }
        } catch (Exception e) {
            cat.error("Error during getDepositTypesList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static List<BanksDepositTypesVo> getBanksDepositTypesListByBanks() {
        List<BanksDepositTypesVo> list = new ArrayList<BanksDepositTypesVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("banksDepositTypesAllListByBanks");

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                BanksDepositTypesVo banksDepositType = new BanksDepositTypesVo();
                banksDepositType.setBanksId(rs.getString("banksId"));
                banksDepositType.setBankName(getBankById(rs.getString("banksId")).getName());
                banksDepositType.setDepositTypeList(getDepositTypesListByBankId(rs.getString("banksId")));
                list.add(banksDepositType);
            }
        } catch (Exception e) {
            cat.error("Error during getDepositTypesList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    private static List<DepositTypesVo> getDepositTypesListByBankId(String bankId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("banksDepositTypesByBanks");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, bankId);
            rs = pstmt.executeQuery();

            List<String> depositTypesIdsList = new ArrayList<String>();
            while (rs.next()) {
                depositTypesIdsList.add(rs.getString("depositTypesId"));
            }
            return DepositTypesConf.getDepositTypesByIds(depositTypesIdsList);

        } catch (Exception e) {
            cat.error("Error during getDepositTypesListByBankId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return new ArrayList<DepositTypesVo>();
    }

    public static BanksDepositTypesVo getBanksDepositTypesVoByBankId(String bankId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("banksDepositTypesByBanks");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, bankId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                BanksDepositTypesVo banksDepositType = new BanksDepositTypesVo();
                banksDepositType.setId(rs.getString("id"));
                banksDepositType.setBanksId(rs.getString("banksId"));
                banksDepositType.setBankName(getBankById(rs.getString("banksId")).getName());
                banksDepositType.setDepositTypeList(getDepositTypesListByBankId(rs.getString("banksId")));
                banksDepositType.setDepositTypeSelected(rs.getString("depositTypesId"));
                return banksDepositType;
            }
        } catch (Exception e) {
            cat.error("Error during getBanksDepositTypesVoByBankId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static BanksDepositTypesVo getBanksDepositTypesByBankIdAndDepositType(String bankId, String depositTypeId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("banksDepositTypesByBanksAndDepositType");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, bankId);
            pstmt.setString(2, depositTypeId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                BanksDepositTypesVo banksDepositType = new BanksDepositTypesVo();
                banksDepositType.setId(rs.getString("id"));
                banksDepositType.setBanksId(rs.getString("banksId"));
                banksDepositType.setBankName(getBankById(rs.getString("banksId")).getName());
                banksDepositType.setDepositTypeList(getDepositTypesListByBankId(rs.getString("banksId")));
                return banksDepositType;
            }
        } catch (Exception e) {
            cat.error("Error during getBanksDepositTypesVoByBankId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static BanksDepositTypesVo getBanksDepositTypesById(String id) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("banksDepositTypesById");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                BanksDepositTypesVo banksDepositType = new BanksDepositTypesVo();
                banksDepositType.setId(rs.getString("id"));
                banksDepositType.setBanksId(rs.getString("banksId"));
                banksDepositType.setBankName(getBankById(rs.getString("banksId")).getName());
                banksDepositType.setDepositTypeSelected(rs.getString("depositTypesId"));
                banksDepositType.setDepositTypeList(getDepositTypesListByBankId(rs.getString("banksId")));
                return banksDepositType;
            }
        } catch (Exception e) {
            cat.error("Error during getBanksDepositTypesVoByBankId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static BanksDepositTypesVo getBanksDepositTypesByDepositTypesId(String depositTypesId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("banksDepositTypesByDepositTypesId");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, depositTypesId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                BanksDepositTypesVo banksDepositType = new BanksDepositTypesVo();
                banksDepositType.setId(rs.getString("id"));
                banksDepositType.setBanksId(rs.getString("banksId"));
                banksDepositType.setBankName(getBankById(rs.getString("banksId")).getName());
                banksDepositType.setDepositTypeList(getDepositTypesListByBankId(rs.getString("banksId")));
                return banksDepositType;
            }
        } catch (Exception e) {
            cat.error("Error during getBanksDepositTypesByDepositTypesId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }
    
    public static void insertBanksDepositTypes(BanksDepositTypesVo banksDeposit, String[] depositTypesArr) {
        for (String str : depositTypesArr) {
            insertBanksDepositTypes(banksDeposit, str);
        }
    }

    public static void insertBanksDepositTypes(BanksDepositTypesVo banksDeposit, String depositType) {
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String strSQL = sql_bundle.getString("banksDepositInsert");
            cat.debug("Inserting insertBanksDepositTypes" + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            pst.setString(1, UUID.randomUUID().toString().toUpperCase());
            pst.setString(2, banksDeposit.getBanksId());
            pst.setString(3, depositType);
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error inserting insertBanksDepositTypes", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void updateBanksDepositsTypes(BanksDepositTypesVo banksDeposit, String[] selectDepositTypes) {
        
        BanksDepositTypesVo bankDT = getBanksDepositTypesVoByBankId(banksDeposit.getId());
        List<DepositTypesVo> depositTypeList = bankDT.getDepositTypeList();
        for (DepositTypesVo dt : depositTypeList) {
            BanksDepositTypesVo bkDT = getBanksDepositTypesByBankIdAndDepositType(bankDT.getBanksId(), dt.getId());
            if(bkDT != null){
                PaymentWizardVo pw = PaymentWizardConfiguration.getPaymentWizardByBanksDepositTypeId(bkDT.getId());
                if (pw == null) {
                    deleteBanksDepositTypes(bkDT.getId());
                }
            }
        }
        
        

        for (String dt : selectDepositTypes) {
            BanksDepositTypesVo banksDT = getBanksDepositTypesByBankIdAndDepositType(banksDeposit.getId(), dt);
            if (banksDT == null) {
                insertBanksDepositTypes(banksDeposit, dt);
            }
        }
    }

    public static void deleteBanksDepositTypes(String bankDepositType) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("banksDepositDelete");
            cat.debug("Query banksDepositDelete " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, bankDepositType);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error deleting banksDepositDelete", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }
    
    public static void deleteBanksDepositTypesByBankId(String bank) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("banksDepositDeleteByBank");
            cat.debug("Query deleteBanksDepositTypesByBankId " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, bank);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error deleting deleteBanksDepositTypesByBankId", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }
    
    

    public static Banks getBankById(String id) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("bankById");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                Banks bank = new Banks(rs.getLong("ID"), rs.getString("Code"), rs.getString("Name"), rs.getBoolean("Status"));
                return bank;
            }
        } catch (Exception e) {
            cat.error("Error during getBanksDepositTypesVoById", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static boolean isBankDepositTypeDuplicate(String idPaymentWizard, String selectedBank, String selectDepositType) {
        List<BanksDepositTypesVo> banksDepositTypesList = getBanksDepositTypesAllList();
        for (BanksDepositTypesVo bankDepositType : banksDepositTypesList) {
            if (bankDepositType.getBanksId().equalsIgnoreCase(selectedBank) && bankDepositType.getDepositTypeSelected().equalsIgnoreCase(selectDepositType)) {
                PaymentWizardVo pw = PaymentWizardConfiguration.getPaymentWizardByBanksDepositTypeId(bankDepositType.getId());
                if(pw != null && !pw.getId().equalsIgnoreCase(idPaymentWizard)){
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean canDeleteBanksDepositType(String banksId) {

        List<BanksDepositTypesVo> banksDepositTypesList = getBanksDepositTypesAllList();
        for (BanksDepositTypesVo bankDepositType : banksDepositTypesList) {
            if (bankDepositType.getBanksId().equalsIgnoreCase(banksId)) {
                PaymentWizardVo paymentwizard = PaymentWizardConfiguration.getPaymentWizardByBanksDepositTypeId(bankDepositType.getId());
                return paymentwizard == null;
            }
        }
        return true;
    }
}
