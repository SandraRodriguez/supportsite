
package com.debisys.tools.depositConfiguration;

/**
 *
 * @author dgarzon
 */
public class DepositTypesVo {
           
    private String id;
    private String descriptionEnglish;
    private String descriptionSpanish;
    private String externalCode;
    private String externalCodeDescription;
    private String hasTransferAccount;
    private String depositCode;    
    private String errorCode;
    
    /**
     * 
     */
    public DepositTypesVo() {
    }

    /**
     * 
     * @param id
     * @param descriptionEnglish
     * @param descriptionSpanish 
     */
    public DepositTypesVo(String id, String descriptionEnglish, String descriptionSpanish) {
        this.id = id;
        this.descriptionEnglish = descriptionEnglish;
        this.descriptionSpanish = descriptionSpanish;
    }
    
    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    
    /**
     * @return the depositCode
     */
    public String getDepositCode() {
        return depositCode;
    }

    /**
     * @param depositCode the depositCode to set
     */
    public void setDepositCode(String depositCode) {
        this.depositCode = depositCode;
    }
    
    /**
     * @return the hasTransferAccount
     */
    public String getHasTransferAccount() {
        return hasTransferAccount;
    }

    /**
     * @param hasTransferAccount the hasTransferAccount to set
     */
    public void setHasTransferAccount(String hasTransferAccount) {
        this.hasTransferAccount = hasTransferAccount;
    }
  

    /**
     * @return the externalCodeDescription
     */
    public String getExternalCodeDescription() {
        return externalCodeDescription;
    }

    /**
     * @param externalCodeDescription the externalCodeDescription to set
     */
    public void setExternalCodeDescription(String externalCodeDescription) {
        this.externalCodeDescription = externalCodeDescription;
    }
    
    /**
     * @return the externalCode
     */
    public String getExternalCode() {
        return externalCode;
    }

    /**
     * @param externalCode the externalCode to set
     */
    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescriptionEnglish() {
        return descriptionEnglish;
    }

    public void setDescriptionEnglish(String descriptionEnglish) {
        this.descriptionEnglish = descriptionEnglish;
    }

    public String getDescriptionSpanish() {
        return descriptionSpanish;
    }

    public void setDescriptionSpanish(String descriptionSpanish) {
        this.descriptionSpanish = descriptionSpanish;
    }
    
    
}
