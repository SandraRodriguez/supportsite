
package com.debisys.tools.depositConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dgarzon
 */
public class PaymentWizardVo {
    
    private String id;
    private String description;
    private boolean enabled;
    private BanksDepositTypesVo banksDepositTypes;
    private List<PaymentWizardDetailVo> listDetail;
    private File imageFile;

    public PaymentWizardVo() {
        listDetail = new ArrayList<PaymentWizardDetailVo>();
        id = "";
        description = "";
        enabled = true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    public void addPaymentWizardDetail(PaymentWizardDetailVo obj){
        listDetail.add(obj);
    }

    public List<PaymentWizardDetailVo> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<PaymentWizardDetailVo> listDetail) {
        this.listDetail = listDetail;
    }

    public BanksDepositTypesVo getBanksDepositTypes() {
        return banksDepositTypes;
    }

    public void setBanksDepositTypes(BanksDepositTypesVo banksDepositTypes) {
        this.banksDepositTypes = banksDepositTypes;
    }

    public File getImageFile() {
        return imageFile;
    }

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }
    
    
}
