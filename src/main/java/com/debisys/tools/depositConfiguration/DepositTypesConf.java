package com.debisys.tools.depositConfiguration;

import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class DepositTypesConf {

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.depositConfiguration.sql");
    static Category cat = Category.getInstance(DepositTypesConf.class);

    public static List<DepositTypesVo> getDepositTypesList() {
        List<DepositTypesVo> list = new ArrayList<DepositTypesVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("depositTypesAllList");

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                DepositTypesVo depositType = new DepositTypesVo();
                depositType.setId(rs.getString("id"));
                depositType.setDescriptionEnglish(rs.getString("descriptionEnglish"));
                depositType.setDescriptionSpanish(rs.getString("descriptionSpanish"));
                depositType.setExternalCode(rs.getString("externalCode"));
                depositType.setExternalCodeDescription(rs.getString("description"));
                depositType.setDepositCode(rs.getString("depositCode"));
                if ( rs.getBoolean("transferAccount")){
                    depositType.setHasTransferAccount("1");
                } else{
                    depositType.setHasTransferAccount("0");
                }
                list.add(depositType);
            }
        } catch (Exception e) {
            cat.error("Error during getDepositTypesList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }
   

    public static DepositTypesVo getDepositTypesById(String id) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("depositTypesById");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                DepositTypesVo depositType = new DepositTypesVo();
                depositType.setId(rs.getString("id"));
                depositType.setDescriptionEnglish(rs.getString("descriptionEnglish"));
                depositType.setDescriptionSpanish(rs.getString("descriptionSpanish"));
                depositType.setExternalCode(rs.getString("externalCode"));
                depositType.setDepositCode(rs.getString("depositCode"));
                if ( rs.getBoolean("transferAccount")){
                    depositType.setHasTransferAccount("1");
                } else{
                    depositType.setHasTransferAccount("0");
                }
                return depositType;
            }
        } catch (Exception e) {
            cat.error("Error during getDepositTypesById", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    

    public static List<DepositTypesVo> getDepositTypesByIds(List<String> idsList) {
        List<DepositTypesVo> nameList = new ArrayList<DepositTypesVo>();
        List<DepositTypesVo> allList = getDepositTypesList();
        for (DepositTypesVo obj : allList) {
            for (String id : idsList) {
               if(obj.getId().equalsIgnoreCase(id)){
                   nameList.add(obj);
                   break;
               }
            }
        }
        return nameList;
    }

    public static void insertDepositTypes(DepositTypesVo depositType) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            depositType.setId(uuid);
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("depositTypesInsert");
            cat.debug("Inserting insertDepositTypes" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, uuid);
            pst.setString(2, depositType.getDescriptionEnglish());
            pst.setString(3, depositType.getDescriptionSpanish());
            pst.setString(4, depositType.getExternalCode());
            pst.setString(5, depositType.getHasTransferAccount());
            pst.setString(6, depositType.getDepositCode().toUpperCase());
            pst.executeUpdate();

        } catch (Exception e) {
            if (e.getMessage().contains("descriptionSpanish")){
                depositType.setErrorCode("indexSpa");
            }else if (e.getMessage().contains("descriptionEnglish")) {
                depositType.setErrorCode("indexEng");
            } else{
                depositType.setErrorCode("errorGeneral");                
            }
            cat.error("Error inserting insertDepositTypes", e);            
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void updateDepositTypes(DepositTypesVo depositType) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("depositTypesUpdate");
            cat.debug("updating updateDepositTypes" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, depositType.getDescriptionEnglish());
            pst.setString(2, depositType.getDescriptionSpanish());
            pst.setString(3, depositType.getExternalCode());
            pst.setString(4, depositType.getHasTransferAccount());
            pst.setString(5, depositType.getDepositCode().toUpperCase());
            pst.setString(6, depositType.getId());
            pst.executeUpdate();
        } catch (Exception e) {
            if (e.getMessage().contains("descriptionSpanish")){
                depositType.setErrorCode("indexSpa");
            }else if (e.getMessage().contains("descriptionEnglish")) {
                depositType.setErrorCode("indexEng");
            } else{
                depositType.setErrorCode("errorGeneral");                
            }
            cat.error("Error inserting updateDepositTypes", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void deleteDepositTypes(String id) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("depositTypesDelete");
            cat.debug("deleting deleteDepositTypes " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, id);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error deleting deleteDepositTypes", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }
    
    public static boolean canDeleteDepositType(String id){
        BanksDepositTypesVo banksDepositTypesByDepositTypesId = BanksDepositTypesConf.getBanksDepositTypesByDepositTypesId(id);
        return banksDepositTypesByDepositTypesId == null;
    }
    
    public static boolean isDuplicateDepositType(String descriptionEng, String descriptionSpa){
        
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("depositTypesByDescription");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, descriptionEng.trim());
            pstmt.setString(2, descriptionSpa.trim());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;                
            }
        } catch (Exception e) {
            cat.error("Error during getDepositTypesById", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return false;
    }
    
    public static List<DepositExternalTypeVo> getDepositExternalTypesList() {
        List<DepositExternalTypeVo> list = new ArrayList<DepositExternalTypeVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("depositTypesAllList");
            sql = "SELECT id, description, code FROM ExternalPaymentCodes WITH(NOLOCK)";
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                DepositExternalTypeVo depositType = new DepositExternalTypeVo();
                depositType.setId(rs.getString("id"));
                depositType.setCode(rs.getString("code"));
                depositType.setDescription(rs.getString("description"));
                
                //depositType.setDescriptionEnglish(rs.getString("descriptionEnglish"));
                //depositType.setDescriptionSpanish(rs.getString("descriptionSpanish"));
                list.add(depositType);
            }
        } catch (Exception e) {
            cat.error("Error during getDepositExternalTypesList ", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }
    
}
