/**
 * 
 */
package com.debisys.tools;

/**
 * @author nmartinez
 * 
 */
public class Contact
{
  private Integer ContactID;
  private Double  PlayerID;
  private Integer ContactTypeID;
  private String  ContactName;
  private String  ContactPhone;
  private String  ContactFax;
  private String  ContactEmail;
  private String  ContactCellphone;
  private String  ContactDepartment;

  public Contact()
  {

  }

  public Double getPlayerID()
  {
    return this.PlayerID;
  }

  public void setPlayerID(Double playerID)
  {
    this.PlayerID = playerID;
  }

  public Integer getContactTypeID()
  {
    return this.ContactTypeID;
  }

  public void setContactTypeID(Integer contactTypeID)
  {
    this.ContactTypeID = contactTypeID;
  }

  public String getContactName()
  {
    return this.ContactName;
  }

  public void setContactName(String contactName)
  {
    this.ContactName = contactName;
  }

  public String getContactPhone()
  {
    return this.ContactPhone;
  }

  public void setContactPhone(String contactPhone)
  {
    this.ContactPhone = contactPhone;
  }

  public String getContactFax()
  {
    return this.ContactFax;
  }

  public void setContactFax(String contactFax)
  {
    this.ContactFax = contactFax;
  }

  public String getContactEmail()
  {
    return this.ContactEmail;
  }

  public void setContactEmail(String contactEmail)
  {
    this.ContactEmail = contactEmail;
  }

  public String getContactCellphone()
  {
    return this.ContactCellphone;
  }

  public void setContactCellphone(String contactCellphone)
  {
    this.ContactCellphone = contactCellphone;
  }

  public String getContactDepartment()
  {
    return this.ContactDepartment;
  }

  public void setContactDepartment(String contactDepartment)
  {
    this.ContactDepartment = contactDepartment;
  }

  public Integer getContactID()
  {
    return this.ContactID;
  }

}
