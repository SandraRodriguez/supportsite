package com.debisys.tools;

import java.util.Vector;

import org.apache.log4j.Logger;

/**
 * Utility class backed by a Collection of data, which typically represents a
 * data row for presentation layer. It has also a reference to a Collection<Integer>
 * with position markers to the columns to display in the row.
 * 
 * @author rbuitrago@emida.net
 * 
 * @param <E>
 *                Generic type class, means that the backing Collection should
 *                be typed as well.
 */
public class MarkedCollection<E> {

    private static Logger log = Logger.getLogger(MarkedCollection.class);
    private java.util.ArrayList<E> data;
    private java.util.ArrayList<Integer> markers;

    /**
     * Populates data and inits markers collections.
     * 
     * @param someData
     *                Collection<E> to back this object.
     */
    public MarkedCollection(java.util.Collection<E> someData) {
	data = new java.util.ArrayList<E>(someData);
	markers = new java.util.ArrayList<Integer>();
    }

    /**
     * Returns the Object located at the position passed as an argument.
     * Implicitly marks the desired position for the object in the row.
     * 
     * @param i
     *                position where the desired Object<E> is located within
     *                backing data Collection
     * @return Object reference, or null it it was not found.
     */
    public E get(int i) {
	markers.add(i);
	return data.get(i);
    }

    /**
     * Explicitly marks the desired position for the Object passed as an
     * argument, and returns the Object untouched.
     * 
     * @param item
     *                Object ref to be retorned as-is
     * @param i
     *                position as desired to be marked in markers collection
     * @return same Object ref as passed by argument
     */
    public E mark(final E item, int i) {
	markers.add(i);
	return item;
    }

    /**
     * Sorts the elements in the backing data collection (row data). If there
     * are more marked positions than elements in backing data collection, the
     * unsorted collection is returned. If any position marker points to an
     * unexistant slot in backing data collection, the marker slot index in
     * markers collection is used as index to get the element from backing data
     * collection.
     * 
     * @return Sorted Collection<E>, according to positions loaded in markers
     *         collection.
     */
    public java.util.Collection<E> sorted() {
	java.util.Collection<E> sorted = new java.util.ArrayList<E>();
	if (data.size() < markers.size()) {
	    log.error("Cannot sort " + data.size()
		    + " data elements according to " + markers.size()
		    + " position markers!. Returning unsorted data as-is");
	    return data;
	}
	for (int i = 0; i < markers.size(); i++) {
	    try {
		sorted.add(data.get(markers.get(i)));
	    } catch (Exception e) {
		log.warn("can't get unsorted[" + markers.get(i)
			+ "]; inserting unsorted[" + i + "] instead ..");
		sorted.add(data.get(i));
	    }
	}
	return sorted;
    }

    /**
     * @return CSV string with the contents of backing markers collection, or an
     *         empty string if no markers are set.
     */
    public String getMarkers() {
	if (markers == null || markers.isEmpty()) {
	    log.error("No markers set!");
	    return "";
	}
	String asString = "";
	for (Integer mark : markers) {
	    asString += mark + ",";
	}
	return asString.substring(0, asString.lastIndexOf(","));
    }

    /**
     * Populates backing markers collection with data parsed from a CVS string
     * passed as an argument.
     * 
     * @param csv
     *                Input string must be in CSV format, representing each
     *                element a position marker
     */
    public void setMarkers(String csv) {
	if (csv == null || csv.indexOf(",") == -1) {
	    log.error("Can't set markers from invalid String! (" + csv + ")");
	    return;
	}
	markers = new java.util.ArrayList<Integer>();
	for (String pos : csv.split("\\,")) {
	    markers.add(new Integer(pos));
	}
    }

    /**
     * @param data
     *                Vector<Vector<E>> of data, representing a set of data
     *                rows to be exported as CSV.
     * @param csvMarkers
     *                CSV String to set as position markers for each row in
     *                dataSet.
     * @return A Serialized String with the form ROW1\nROW2\n[...]\nROWn, with
     *         each row as returned from {@link #dataRowAsCSV(String)}
     */
    public static <E> String dataSetAsCSV(Vector<Vector<E>> data,
	    String csvMarkers) {
	if (data == null || data.isEmpty()) {
	    return null;
	}
	String serialized = "";
	String lineSep = System.getProperty("line.separator");
	for (Vector<E> row : data) {
	    serialized += new MarkedCollection<E>(row).dataRowAsCSV(csvMarkers)
		    + "\";";
	}
	return serialized.substring(0, serialized.lastIndexOf(";")).replaceAll(
		"\\;", lineSep);
    }

    /**
     * @return Returns contents of backing data collection as a exportable CSV
     *         string: Each data field in a row is surrounded by double quotes.
     */
    public String asCSV() {
	return dataRowAsCSV(null);
    }

    /**
     * @param csvMarkers
     *                (non exportable) CSV with the markers to populate backing
     *                position markers collection
     * @return A Serialized String with the form
     *         "FIELD1","FIELD2",[...],"FIELDn" (enclosed in double quotes)
     */
    public String dataRowAsCSV(String csvMarkers) {
	if (data == null || data.isEmpty()) {
	    return null;
	}
	if (csvMarkers != null)
	    setMarkers(csvMarkers);
	String serialized = "\"";
	for (E field : sorted()) {
	    serialized += field + "\",\"";
	}
	return serialized.substring(0, serialized.lastIndexOf("\","));
    }
}
