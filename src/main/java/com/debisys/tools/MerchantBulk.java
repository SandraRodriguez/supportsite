package com.debisys.tools;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.ServletContext;

import com.debisys.customers.Merchant;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;

/**
 * @author nmartinez
 * 
 */
public class MerchantBulk extends Merchant
{
	protected Contact    objContact = new Contact();

	private StringBuffer messages   = new StringBuffer();

	private Integer      processOK  = 0;

	private String       repSetUpMailsNotification;
	private String       emailNotification;
	private String       userId;
	private String       userPassword;
	private String       mailHost;
	private String       accessLevel;
	private String       companyName;
	private String       userName;

	// private Hashtable hashErrors = new Hashtable();

	public MerchantBulk(String mailHost, String accessLevel, String companyName, String userName)
	{
		this.arrayTerminals = new ArrayList<TerminalBulk>(0);
		this.mailHost = mailHost;
		this.accessLevel = accessLevel;
		this.companyName = companyName;
		this.userName = userName;
		this.setMerchantType(DebisysConstants.MERCHANT);
	}

	private ArrayList<TerminalBulk> arrayTerminals;

	public ArrayList<TerminalBulk> getArrayTerminals()
	{
		return this.arrayTerminals;
	}

	public void setArrayTerminals(ArrayList<TerminalBulk> arrayTerminals)
	{
		this.arrayTerminals = arrayTerminals;
	}

	public String getUserId()
	{
		return this.userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getUserPassword()
	{
		return this.userPassword;
	}

	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}

	public String getEmailNotification()
	{
		return this.emailNotification;
	}

	public void setEmailNotification(String emailNotification)
	{
		this.emailNotification = emailNotification;
	}

	public String getTableErrorValidation()
	{
		Boolean findErrors = Boolean.FALSE;
		int irowCss = 1;
		Hashtable errorHashtable = this.getErrors();
		Enumeration e = errorHashtable.elements();
		StringBuffer errorMerchant = new StringBuffer();
		// errorMerchant.append("<table>");

		while (e.hasMoreElements())
		{
			errorMerchant.append("<tr class=\"" + irowCss + "\"><td class=\"rowred1\" >" + e.nextElement() + "</td></tr>");
			if (irowCss == 1)
			{
				irowCss = 2;
			}
			else
			{
				irowCss = 1;
			}
		}
		if (irowCss == 1)
		{
			irowCss = 2;
		}
		else
		{
			irowCss = 1;
		}
		// errorMerchant.append("</table>");
		if (this.messages.toString().length() > 0)
		{
			errorMerchant.append("<tr class=\"" + irowCss + "\"><td class=\"rowred1\">" + this.messages.toString() + "</td></tr>");
		}

		if (errorMerchant.toString().length() == 0)
		{
			return "";
		}
		else
		{
			return "<table>" + errorMerchant.toString() + "</table>";
		}
	}

	/**
	 * This method try to send a email notification to setup group, contact
	 * merchant and terminal notification mail parameter in the bulk file used to
	 * load merchants.
	 */
	public void ProcessOK(SessionData sessionData, ServletContext context)
	{
		try
		{
			String emails =""; //this.getRepSetUpMailsNotification() + "," + this.getEmailNotification();

			if (this.repSetUpMailsNotification != null)
			{
				emails = this.repSetUpMailsNotification + "," + this.getEmailNotification();
			}
			else
			{
				emails = this.getEmailNotification();
			}

			this.sendMailNotification(this.mailHost, this.accessLevel, this.companyName, this.userName, this.getUserId(), this.getUserPassword(), emails, context);
			this.processOK = 2;
			this.messages.append(Languages.getString("jsp.admin.tools.send_mail_ok", sessionData.getLanguage()) + "<br>");
		}
		catch (javax.mail.MessagingException ex)
		{
			this.processOK = 3;
			this.messages.append(Languages.getString("jsp.admin.tools.error_send_mail", sessionData.getLanguage()) + " <br>");
		}
		catch (IllegalStateException e)
		{
			this.processOK = 3;
			this.messages.append(Languages.getString("jsp.admin.tools.error_send_mail", sessionData.getLanguage()) + " <br>");
		}
		catch (Exception e)
		{
			this.processOK = 3;
			this.messages.append(Languages.getString("jsp.admin.tools.error_send_mail", sessionData.getLanguage()) + " <br>");
		}

	}

	public String getRepSetUpMailsNotification()
	{
		return this.repSetUpMailsNotification;
	}

	public void setRepSetUpMailsNotification(String repSetUpMailsNotification)
	{
		this.repSetUpMailsNotification = repSetUpMailsNotification;
	}

	public String getMailHost()
	{
		return this.mailHost;
	}

	public String getAccessLevel()
	{
		return this.accessLevel;
	}

	public String getCompanyName()
	{
		return this.companyName;
	}

	public Integer getProcessOK()
	{
		return this.processOK;
	}

}
