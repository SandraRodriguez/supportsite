package com.debisys.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.*;

import org.apache.torque.Torque;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;

public class BulkMerchants extends ValidateInformation {

	private SessionData sessionData;
	private ServletContext context;
	// private Integer bulkId;

	private StringBuffer messagesError = new StringBuffer(1000);

	/**
	 * @param sessionData
	 * @param context
	 * @param customConfigType
	 * @param remoteHost
	 * @param localAddr
	 * @param contextPath
	 * @param mailHost
	 * @param companyName
	 * @param userName
	 */
	public BulkMerchants(SessionData sessionData, ServletContext context, String customConfigType, String remoteHost, String localAddr, String contextPath,
			String mailHost, String companyName, String userName) {

		this.customConfigType = customConfigType;
		this.sessionData = sessionData;
		this.context = context;
		this.remoteHost = remoteHost;
		this.localAddr = localAddr;
		this.contextPath = contextPath;
		this.accessLevel = sessionData.getProperty("access_level");
		this.refId = sessionData.getProperty("ref_id");
		this.DistChainType = sessionData.getProperty("dist_chain_type");
		this.deploymentType = DebisysConfigListener.getDeploymentType(context);
		this.mailHost = mailHost;
		this.companyName = companyName;
		this.userName = userName;
		findTypesValues(sessionData);
	}

	@SuppressWarnings("unchecked")
	private String saveFile(javax.servlet.http.HttpServletRequest request, String workingDir, SessionData sessionData) throws IOException, FileUploadException {
		String saveFileName = "";
		cat.debug("Init Save File");
		// Los envios multipart se utilizan cuando hay paso de archivos
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
				List fileItemsList = servletFileUpload.parseRequest(request);
				Iterator itr = fileItemsList.iterator();
				while (itr.hasNext()) {
					FileItem item = (FileItem) itr.next();
					if (!item.isFormField()) {
						File fullFile = new File(item.getName());
						saveFileName = fullFile.getName();
						File fNew = new File(workingDir, saveFileName);
						item.write(fNew);
						// FileOutputStream fos = new FileOutputStream(workingDir + saveFileName);
						// fos.write(item.getString().getBytes());
						// fos.flush();
						// fos.close();
					}
				}
			} catch (Exception e) {
				cat.error("MerchantBulk saveFile error: " + e.getMessage() + "-" + e.getLocalizedMessage() + "-" + e);
				this.addMessagesError(Languages.getString("jsp.admin.tools.general_error", sessionData.getLanguage()));
			}
		} else {
			cat.error("MerchantBulk saveFile error: No MultipartContent");
			this.addMessagesError(Languages.getString("jsp.admin.tools.general_error", sessionData.getLanguage()));
		}
		cat.debug("End Save File: " + workingDir + saveFileName);
		return workingDir + saveFileName;

	}

	/**
	 * Analyze the excel file, it's build a ArrayList of MerchantBulk Object where each one can has one or may objects of TerminalBulk objects
	 * 
	 * @param filename
	 * @return ArrayList of MerchantBulk objects
	 */
	public ArrayList<MerchantBulk> analizeInputStream(javax.servlet.http.HttpServletRequest request, String workingDir, SessionData sessionData,
			ServletContext context) {
		String dirFile = "";
		try {
			boolean bWebServiceAuthenticationEnabled = com.debisys.users.User.isWebServiceAutherticationTerminalsEnabled(sessionData, context);
			// create a poi workbook from the excel spreadsheet file
			// POIFSFileSystem fs = new POIFSFileSystem(new
			// FileInputStream(filename));
			dirFile = saveFile(request, workingDir, sessionData);
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(dirFile));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			// for (int k = 1; k < wb.getNumberOfSheets(); k++)
			// {
			HSSFSheet sheet = wb.getSheetAt(0);
			int rows = sheet.getPhysicalNumberOfRows();
			objMerchant = new MerchantBulk(this.mailHost, this.accessLevel, this.companyName, this.userName);
			int numberRowsOk = 2;
			for (int r = 2; r <= rows; r++) {
				if (sheet.getRow(r) != null) {
					HSSFRow row = sheet.getRow(r);
					HSSFCell cell = row.getCell(0);
					if (cell != null) {
						switch (cell.getCellType()) {
						case HSSFCell.CELL_TYPE_FORMULA:
							// error
							// this.addMessagesError(Languages.getString("jsp.admin.tools.general_validate_merchantID", sessionData.getLanguage())+ " "+r);
							break;
						case HSSFCell.CELL_TYPE_NUMERIC:
							try {
								numberRowsOk++;
							} catch (Exception e) {
								// ERROR
								// log error for user
							}
							break;
						case HSSFCell.CELL_TYPE_STRING:

							try {
								Double.parseDouble(cell.getRichStringCellValue().getString().trim());
								cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
								numberRowsOk++;
							} catch (Exception localException) {

							}
							// error
							// this.addMessagesError(Languages.getString("jsp.admin.tools.general_validate_merchantID", sessionData.getLanguage()) + " "+r);
							break;
						default:
							// this.addMessagesError(Languages.getString("jsp.admin.tools.general_validate_merchantID", sessionData.getLanguage())+ " "+r);
							break;
						}
					}
				}
			}
			if (numberRowsOk > 200) {
				this.addMessagesError(Languages.getString("jsp.admin.reports.analysis.rows_limited", sessionData.getLanguage()));
			} else {
				for (int r = 2; r <= numberRowsOk; r++) {
					if (sheet.getRow(r) != null) {
						HSSFRow row = sheet.getRow(r);
						HSSFCell cell = row.getCell(0);
						if (cell != null) {
							// HSSFRow row = sheet.getRow(r);
							analizeArrayCellsOfRow(row, r, bWebServiceAuthenticationEnabled);
							this.numTerminalsInMerchant++;
						}
					}
				}
				addMerchantToArray();
				// }

				boolean fixErrorPostValidations = false;

				for (MerchantBulk objMerchantBulk : arrayMerchant) {
					if (objMerchantBulk.getTableErrorValidation().length() > 0) {
						fixErrorPostValidations = true;
						break;
					}

					for (TerminalBulk objTerminalBulk : objMerchantBulk.getArrayTerminals()) {
						if (objTerminalBulk.getTableErrorValidation().length() > 0) {
							fixErrorPostValidations = true;
							break;
						}
					}
				}
				if (this.getMessagesError().toString().length() > 0) {
					fixErrorPostValidations = true;
				}
				if (!fixErrorPostValidations) {
					// validate is any PC Terminal user is Unique
					if (validatePCTerminalUser(this.arrayMerchant, bWebServiceAuthenticationEnabled)) {
						ProcessBulk objProcessBulk = new ProcessBulk(this.accessLevel, this.refId, this.deploymentType, this.customConfigType, this.localAddr,
								this.remoteHost, this.contextPath);
						objProcessBulk.doBulkMerchantTerminals(this.arrayMerchant, sessionData, context);
					}
				}
			}
		} catch (IOException iex) {
			cat.error("MerchantBulk analizeInputStream IOException: " + iex.getMessage() + "-" + iex.getLocalizedMessage() + "-" + iex);
			this.addMessagesError(Languages.getString("jsp.admin.tools.general_error", sessionData.getLanguage()));
		} catch (FileUploadException fuex) {
			cat.error("MerchantBulk analizeInputStream FileUploadException: " + fuex.getMessage() + "-" + fuex.getLocalizedMessage() + "-" + fuex);
			this.addMessagesError(Languages.getString("jsp.admin.tools.general_error", sessionData.getLanguage()));
		} catch (Exception e) {
			cat.error("MerchantBulk analizeInputStream Exception: " + e.getMessage() + "-" + e.getLocalizedMessage() + "-" + e);
			this.addMessagesError(Languages.getString("jsp.admin.tools.general_error", sessionData.getLanguage()));
		} finally {
			// borrar el archivo creado
		}
		return this.arrayMerchant;
	}

	/**
	 * @param arrayMerchant
	 * @return
	 */
	private boolean validatePCTerminalUser(ArrayList<MerchantBulk> arrayMerchant, boolean isWebServiceAuthenticationEnabled) {
		boolean bResultValidate = false;
		Connection dbConn = null;
		try {
			Hashtable<String, String> hashPcTermUser = new Hashtable<String, String>();
			java.lang.StringBuffer pcTerminalUser = new StringBuffer(500);
			for (MerchantBulk objMerchantBulk : arrayMerchant) {
				for (TerminalBulk objTerminalBulk : objMerchantBulk.getArrayTerminals()) {
					// if PCTerminal, SMS Phone (merchant),SMS Phone (subscriber) validate
					// userid
					if (objTerminalBulk.getTerminalType().equals("26") || objTerminalBulk.getTerminalType().equals("40")
							|| objTerminalBulk.getTerminalType().equals(DebisysConstants.WEB_SERVICE_KIOSK_TERMINAL_TYPE)
							|| objTerminalBulk.getTerminalType().equals(DebisysConstants.SMS_PHONE_KIOSK_MERCHANT_TERMINAL_TYPE)
							|| objTerminalBulk.getTerminalType().equals("56") || objTerminalBulk.getTerminalType().equals("23")
							&& isWebServiceAuthenticationEnabled) {
						hashPcTermUser.put(objTerminalBulk.getPcTermUsername(), objMerchantBulk.getMerchantId());
						pcTerminalUser.append(" select '" + objTerminalBulk.getPcTermUsername() + "' us union");
					}
				}
			}
			String sql = pcTerminalUser.toString();
			if (sql.length() > 0) {
				int len = sql.length();
				sql = sql.substring(0, len - 5);

				PreparedStatement pst = null;

				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn != null) {
					String strSQL = "SELECT b.UserId ,a.us FROM (" + sql + ") a left join Terminal_Registration b on b.userid=a.us";
					// String strSQL = "SELECT UserId FROM Terminal_Registration where
					// userid in(" + sql + ")";
					pst = dbConn.prepareStatement(strSQL);
					ResultSet rs = pst.executeQuery();
					boolean analizePCUsers = false;

					while (rs.next()) {
						analizePCUsers = true;
						if (rs.getString(1) == null) {
							hashPcTermUser.remove(rs.getString(2));
						}
					}

					rs.close();
					pst.close();
					if (hashPcTermUser.size() > 0 && analizePCUsers) {
						Enumeration keys = hashPcTermUser.keys();
						while (keys.hasMoreElements()) {
							Object key = keys.nextElement();
							Object value = hashPcTermUser.get(key);
							this.addMessagesError(Languages.getString("jsp.admin.tools.pcterminal_userid", sessionData.getLanguage())
									.replaceAll("XXX", key.toString()).replaceAll("YYY", value.toString()));
						}
					} else {
						bResultValidate = true;
					}
				}
			} else {
				bResultValidate = true;
			}
		} catch (Exception e) {
			cat.error("Error validating validatePCTerminalUser ", e);

		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return bResultValidate;
	}

	/**
	 * @param row
	 * @param rowIdFile
	 */
	private void analizeArrayCellsOfRow(HSSFRow row, int rowIdFile, boolean isWebServiceAuthenticationEnabled) {
		Boolean keepAnalize = Boolean.TRUE;
		int cells = row.getLastCellNum(); // row.getPhysicalNumberOfCells();
		int c = 0;

		for (; c < cells; c++) {
			HSSFCell cell = row.getCell(c);
			String value = "";
			// cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			if (cell != null) {
				switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_FORMULA:
					// value = "FORMULA ";
					// go to error , the document can't had fields with formulas, only
					// values are accepted
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					try {
						value = Double.toString(cell.getNumericCellValue()).trim();

						cat.debug("");
					} catch (Exception e) {
						value = "0";
						// log error for user
					}
					break;
				case HSSFCell.CELL_TYPE_STRING:
					try {
						value = cell.getRichStringCellValue().getString().trim();
						// value = cell.getStringCellValue().trim();
					} catch (Exception e) {
						value = "";
					}
					break;
				}
			}

			if (c == 0) {
				if (com.debisys.utils.NumberUtil.isNumeric(value)) {
					validateValueCell(c, rowIdFile, value, isWebServiceAuthenticationEnabled);
				} else {
					keepAnalize = Boolean.FALSE;
					this.addMessagesError(Languages.getString("jsp.admin.tools.validateMerchantConsecutive", sessionData.getLanguage()) + " " + (rowIdFile + 1));
				}
			} else {
				validateValueCell(c, rowIdFile, value, isWebServiceAuthenticationEnabled);
			}
		}

		if (keepAnalize) { // means that the first field, Merchant id it's OK
			try {
				this.objTerminal.setMerchantId(this.objMerchant.getMerchantId());
				// this.objTerminal.validateAddTerminal(sessionData, null, context,
				// "Debisys");
				this.objTerminal.SetUpParametersForEmail(this.objMerchant.getDba(), this.objMerchant.getContactEmail(), this.objMerchant.getMonthlyFee(),
						this.objMerchant.getMailHost(), this.objMerchant.getAccessLevel(), this.objMerchant.getCompanyName(),
						this.objMerchant.getRepSetUpMailsNotification(), this.userName, this.refId);

				this.objMerchant.getArrayTerminals().add(this.objTerminal);
				this.objTerminal = new TerminalBulk();

			} catch (Exception e) {
				this.messagesError.append(Languages.getString("jsp.admin.tools.messages_error_add_merchant", sessionData.getLanguage()) + "<br>");
				cat.error("Error analizeArrayCellsOfRow : " + e.getMessage());
			}
		}
	}

	/**
	 * @param cell
	 * @param rowIdFile
	 * @param valueFromCell
	 */
	private void validateValueCell(int cell, int rowIdFile, String valueFromCell, boolean isWebServiceAuthenticationEnabled) {
		try {
			switch (cell) {
			case 0:
				validateMerchantChange(valueFromCell, sessionData);
				break;
			}
			if (this.numTerminalsInMerchant == 1) {
				validateMerchant(valueFromCell, cell);
				validateTerminal(valueFromCell, cell, isWebServiceAuthenticationEnabled);
			} else {
				validateTerminal(valueFromCell, cell, isWebServiceAuthenticationEnabled);
			}

		} catch (Exception ex) {
			String error = Languages.getString("jsp.admin.tools.messages_error", sessionData.getLanguage()).replaceFirst("XXX", String.valueOf(rowIdFile))
					.replaceFirst("YYY", String.valueOf(cell));
			this.messagesError.append(error + "<br>");
		}
	}

	/**
	 * @param valueFromCell
	 * @param cell
	 * @param rowIdFile
	 */
	private void validateMerchant(String valueFromCell, int cell) {
		if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
			validateMerchantMex(valueFromCell, cell);
		} else {
			validateMerchantDomestic(valueFromCell, cell);
		}

	}

	/**
	 * @param valueFromCell
	 * @param cell
	 */
	private void validateMerchantMex(String valueFromCell, int cell) {
		valueFromCell = valueFromCell.trim();
		switch (cell) {
		/** ************************************************************************* */
		case 1:
			validateRepId(valueFromCell, sessionData);
			break;
		case 2:
			validateBusinessName(valueFromCell, sessionData);
			break;
		case 3:
			validateDba(valueFromCell, sessionData);
			break;
		case 4:
			validateBusinessTypeId(valueFromCell);
			break;
		case 5:
			validateMerchantSegment(valueFromCell);
			break;
		case 6:
			validateBusinessHours(valueFromCell);
			break;
		case 7:
			validateRegionTelcel(valueFromCell);
			break;
		case 8:
			validateInvoice(valueFromCell);
			break;
		case 9:
			validateMerchantClassification(valueFromCell, sessionData);
			break;
		case 10:
			validateControlNumber(valueFromCell, sessionData);
			break;
		/** ************************************************************************* */
		case 11:
			validatePhyAdress(valueFromCell);
			break;
		case 12:
			validatePhysColony(valueFromCell);
			break;
		case 13:
			validatePhysDelegation(valueFromCell);
			break;
		case 14:
			validatePhysCity(valueFromCell);
			break;
		case 15:
			validatePhysZip(valueFromCell);
			break;
		case 16:
			validatePhysState(valueFromCell);
			break;
		case 17:
			validatePhyCounty(valueFromCell);
			break;
		case 18:
			validatePhyCountry(valueFromCell);
			break;
		case 19:
			validateRoute(valueFromCell);
			break;
		/** ************************************************************************* */
		case 20:
			validateSameValuesForBilling(valueFromCell, sessionData);
			break;
		/** ************************************************************************* */
		case 21: //
			validateBillingAddress(valueFromCell, sessionData);
			break;
		case 22:
			validateBillingColony(valueFromCell);
			break;
		case 23:
			validateBillingDelegation(valueFromCell);
			break;
		case 24:
			validateBillingCity(valueFromCell);
			break;
		case 25:
			validateBillingZip(valueFromCell);
			break;
		case 26:
			validateBillingState(valueFromCell);
			break;
		case 27:
			validateBillingCounty(valueFromCell);
			break;
		case 28:
			validateBillingCountry(valueFromCell, sessionData);
			break;
		/** ************************************************************************* */
		case 29:
			validateContactType(valueFromCell);
			break;
		case 30:
			validateContactName(valueFromCell, sessionData);
			break;
		case 31:
			validateContactPhone(valueFromCell, sessionData);
			break;
		case 32:
			validateContactFax(valueFromCell);
			break;
		case 33:
			validateContactEmailMex(valueFromCell);
			break;
		case 34:
			validateContactCellphone(valueFromCell);
			break;
		case 35:
			validateContactDepartment(valueFromCell);
			break;
		/** ************************************************************************** */
		case 36:
			validatePaymentType(valueFromCell);
			break;
		/** ************************************************************************** */
		case 37:
			validateProcessorType(valueFromCell, sessionData);
			break;
		case 38:
			validateTaxId(valueFromCell, sessionData);
			break;
		case 39:
			validateBankName(valueFromCell);
			break;
		case 40:
			validateAccountType(valueFromCell);
			break;
		case 41:
			validateCLABE(valueFromCell);
			/** ************************************************************************** */
			break;
		case 42:
			validateAccountNumber(valueFromCell);
			break;
		case 43:
			// validateCreditType(valueFromCell);
			/** ************************************************************************** */
			break;
		case 44:
			// validateCreditLimit(valueFromCell);
			break;
		/** ************************************************************************** */
		case 45:
			validateEmailNotificationMerchant(valueFromCell);
			break;

		}
	}

	/**
	 * @param valueFromCell
	 * @param cell
	 * @param rowIdFile
	 */
	private void validateTerminal(String valueFromCell, int cell, boolean isWebServiceAuthenticationEnabled) {
		if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
			validateTerminalMex(valueFromCell, cell, isWebServiceAuthenticationEnabled);
		} else {
			validateTerminalDomestic(valueFromCell, cell, isWebServiceAuthenticationEnabled);
		}
	}

	/**
	 * @param valueFromCell
	 * @param cell
	 */
	private void validateTerminalMex(String valueFromCell, int cell, boolean isWebServiceAuthenticationEnabled) {
		valueFromCell = valueFromCell.trim();
		switch (cell) {
		case 46:
			validateTerminalType(valueFromCell, sessionData);
			break;
		case 47:
			validatePlanModel(valueFromCell, sessionData);
			break;
		case 48:
			validateTerminalPlan(valueFromCell, sessionData);
			break;
		case 49:
			validatePCTerminalUser(valueFromCell, isWebServiceAuthenticationEnabled, sessionData);
			break;
		case 50:
			validatePCTerminalPassword(valueFromCell, isWebServiceAuthenticationEnabled, sessionData);
			break;
		case 51:
			validatePCTerminalBrand(valueFromCell, sessionData);
			break;
		case 52:
			validateClerkCodePass(valueFromCell);
			break;
		case 53:
			validateClerkCodeUserName(valueFromCell, sessionData);
			break;
		case 54:
			validateNameClerk(valueFromCell, sessionData);
			break;
		case 55:
			validateLastName(valueFromCell, sessionData);
			break;
		case 56:
			validateMarketPlaceGroup(valueFromCell, sessionData);
			break;
		case 57:
			validateMarketPlaceKey(valueFromCell, sessionData);
			break;
		case 58:
			validateEmailNotificationTerminal(valueFromCell);
			break;
		case 59:
			validateCertificateIgnoreInd(valueFromCell, sessionData);
			break;
		case 60:
			validateCertificateInstallAlowedInd(valueFromCell, sessionData);
			break;
		case 61:
			validateCertificateInstallRemainNum(valueFromCell);
			break;
		}
	}

	/**
	 * @param valueFromCell
	 * @param cell
	 */
	private void validateTerminalDomestic(String valueFromCell, int cell, boolean isWebServiceAuthenticationEnabled) {
		valueFromCell = valueFromCell.trim();
		switch (cell) {
		case 21:
			validateTerminalType(valueFromCell, sessionData);
			break;
		case 22:
			validatePlanModel(valueFromCell, sessionData);
			break;
		case 23:
			validateTerminalPlan(valueFromCell, sessionData);
			break;
		case 24:
			validatePCTerminalUser(valueFromCell, isWebServiceAuthenticationEnabled, sessionData);
			break;
		case 25:
			validatePCTerminalPassword(valueFromCell, isWebServiceAuthenticationEnabled, sessionData);
			break;
		case 26:
			validatePCTerminalBrand(valueFromCell, sessionData);
			break;
		case 27:
			validateClerkCodePass(valueFromCell);
			break;
		case 28:
			validateClerkCodeUserName(valueFromCell, sessionData);
			break;
		case 29:
			validateMarketPlaceGroup(valueFromCell, sessionData);
			break;
		case 30:
			validateMarketPlaceKey(valueFromCell, sessionData);
			break;
		case 31:
			validateEmailNotificationTerminal(valueFromCell);
			break;
		case 32:
			validateCertificateIgnoreInd(valueFromCell, sessionData);
			break;
		case 33:
			validateCertificateInstallAlowedInd(valueFromCell, sessionData);
			break;
		case 34:
			validateCertificateInstallRemainNum(valueFromCell);
			break;
		}
	}

	/**
	 * @param valueFromCell
	 * @param cell
	 */
	private void validateMerchantDomestic(String valueFromCell, int cell) {
		valueFromCell = valueFromCell.trim();
		switch (cell) {
		case 1:
			validateRepId(valueFromCell, sessionData);
			break;
		case 2:
			validateBusinessName(valueFromCell, sessionData);
			break;
		case 3:
			validateDba(valueFromCell, sessionData);
			break;
		case 4:
			validatePhyAdress(valueFromCell);
			break;
		case 5:
			this.validatePhysCity(valueFromCell);
			break;
		case 6:
			validatePhysState(valueFromCell);
			break;
		case 7:
			validatePhysZip(valueFromCell);
			break;
		case 8:
			validatePhysCountry(valueFromCell, sessionData);
			break;
		case 9:
			validateRoute(valueFromCell);
			break;
		case 10:
			objMerchant.setContactName(valueFromCell);
			break;
		case 11:
			objMerchant.setContactPhone(valueFromCell);
			break;
		case 12:
			objMerchant.setContactFax(valueFromCell);
			break;
		case 13:
			objMerchant.setContactEmail(valueFromCell);
			break;
		case 14:
			validatePaymentType(valueFromCell);
			break;
		case 15:
			validateProcessorType(valueFromCell, sessionData);
			break;
		case 16:
			validateTaxId(valueFromCell, sessionData);
			break;
		case 17:
			validateRoutingNumber(valueFromCell, sessionData);
			break;
		case 18:
			objMerchant.setAccountNumber(valueFromCell);
			break;
		case 19:
			validateMonthlyFeeThreshold(valueFromCell);
			break;
		case 20:
			validateEmailNotificationMerchant(valueFromCell);
			break;
		}
	}

	public String getComboTypes() {
		java.lang.StringBuffer com = new StringBuffer();

		com.append("<table class=main>");

		com.append(this.comboTypes.toString());

		com.append("</table>");

		return com.toString();
	}

}
