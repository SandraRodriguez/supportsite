/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools.simInventory;

import com.debisys.exceptions.ReportException;
import static com.debisys.tools.simInventory.SimInventory.cat;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.servlet.ServletContext;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author dgarzon
 */
public class SimProviderDao {
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.simInventory.sql");

    static Category cat = Category.getInstance(SimInventory.class);

    public static List<SimProviderPojo> getSimCarrierList() throws ReportException {
        List<SimProviderPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimProviderDao.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimProviderDao.sql_bundle.getString("getSimCarrierList");
            cat.debug("getSimCarrierList Preparing statement for query: " + sql);
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimProviderPojo simProvider = new SimProviderPojo(rs.getString("idNew"), rs.getString("name"));
                retValue.add(simProvider);
            }

        } catch (Exception e) {
            cat.error("Error during getSimCarrierList", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }
    
    public static List<SimProductPojo> getSimCarrierProductsList(String carrierIdnew) throws ReportException {
        List<SimProductPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimProviderDao.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimProviderDao.sql_bundle.getString("getProductListByCarrierIdnew");
            sql = sql.replace("IN_LIST", carrierIdnew);
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimProductPojo simCarrierProduct = new SimProductPojo(rs.getInt("id"), rs.getString("description").trim(), rs.getString("carrierIdNew"));
                retValue.add(simCarrierProduct);
            }
        } catch (Exception e) {
            cat.error("Error during getSimCarrierProductsList ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }
    
    public static Map<String, SimProductPojo> getSimProductsMapByName(boolean byProductId) throws ReportException{
        
        String carrierIds = "";
        Map<String,SimProductPojo> map = new HashMap<String, SimProductPojo>();
        List<SimProviderPojo> carriersList = getSimCarrierList();
        for (SimProviderPojo carrier : carriersList) {
            carrierIds += ",'"+carrier.getId()+"'";
        }
        carrierIds = carrierIds.replaceFirst(",", "");
        List<SimProductPojo> productsList = getSimCarrierProductsList(carrierIds);
        
        for (SimProductPojo simProduct : productsList) {
            if(byProductId){
                map.put(""+simProduct.getProductId(), simProduct);
            }
            else{
                map.put(simProduct.getDescription(), simProduct);
            }
            
        }
        return map;
    }
    
    public static Map<String, String> getSimCarrierMapByName(boolean byId) throws ReportException{
        Map<String,String> map = new HashMap<String, String>();
        List<SimProviderPojo> simCarrierList = getSimCarrierList();
        for (SimProviderPojo pojo : simCarrierList) {
            if(byId){
                map.put(pojo.getId(),pojo.getName().trim());
            }
            else{
                map.put(pojo.getName().trim(),pojo.getId());
            }
        }
        return map;
    }
    
    /**
     * Search in table SimCarrierMapping join Carrier
     * @param carrierId  Key table SimCarrierMapping
     * @return
     * @throws ReportException 
     */
    public static SimProviderPojo getSimCarrierById(String carrierId) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimProviderDao.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimProviderDao.sql_bundle.getString("getSimCarrierList");
            sql += " WHERE c.idNew = ? ";
            cat.debug("getSimProviders query: " + sql+"  /*"+carrierId+"*/");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, carrierId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return new SimProviderPojo(rs.getString("idNew"), rs.getString("name"));
            }

        } catch (Exception e) {
            cat.error("Error during getSimProviders", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return null;
    }
    
    private static void closeStatements(Connection dbConn, PreparedStatement pstmt, ResultSet rs) {
        try {
            Torque.closeConnection(dbConn);
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            cat.error("Error during closeConnection", e);
        }
    }

}
