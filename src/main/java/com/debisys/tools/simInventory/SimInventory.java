package com.debisys.tools.simInventory;

import com.debisys.exceptions.ReportException;
import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 *
 * @author dgarzon
 */
public class SimInventory {

    public static final String SIM_STATUS_READY = "R";
    public static final String SIM_STATUS_ASSIGNED = "AS";

    public static final String BATCH_STATUS_READY = "R";
    public static final String BATCH_STATUS_ASSIGNED = "A";

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.simInventory.sql");

    static Category cat = Category.getInstance(SimInventory.class);

    public static List<BatchesInfo> getBatchesListAllStatus(String isoId, boolean orderDesc) throws ReportException {
        return getBatchesList(null, null, null, null, isoId, null, null, orderDesc, true);
    }

    public static List<BatchesInfo> getBatchesList(String isoId, boolean orderDesc) throws ReportException {
        return getBatchesList(null, null, null, null, isoId, null, null, orderDesc, false);
    }

    public static List<BatchesInfo> getBatchesList(String batchId, String individualSim, String simProviderId, String simTypeId, String isoId, String startDate, String endDate, boolean orderDesc, boolean allStatus) throws ReportException {

        List<BatchesInfo> listObj = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = "SELECT sb.id, sb.batchId, count(s.simBatchId) as quantity, sb.createDate "
                    + "FROM simBatches sb WITH(NOLOCK) "
                    + "INNER JOIN sims s WITH(NOLOCK) ON sb.id = s.simBatchId "
                    + "INNER JOIN simTypes st WITH(NOLOCK) ON st.id = s.simTypeId "
                    + "INNER JOIN Carriers c WITH(NOLOCK) ON c.idNew = s.simProviderid "
                    + "INNER JOIN simStatus ss WITH(NOLOCK) ON s.simStatusId = ss.id "
                    + "WHERE ";

            if (!allStatus) {
                sql += "(ss.code = 'R' OR ss.code = 'AS') ";
            }
            else {
                sql += "(ss.code = 'R' OR ss.code = 'A' OR ss.code = 'L' OR ss.code = 'C' OR ss.code = 'AS' )";
            }

            String where = "";
            if (batchId != null && !batchId.trim().equals("")) {
                where += " sb.BatchId LIKE '%" + batchId + "%' OR ";
            }
            if (simTypeId != null && !simTypeId.trim().equals("") && !simTypeId.trim().equals("0")) {
                where += " s.simTypeId = '" + simTypeId + "' OR ";
            }
            if (simProviderId != null && !simProviderId.trim().equals("") && !simProviderId.trim().equals("0")) {
                where += " s.simProviderId = '" + simProviderId + "' OR ";
            }
            if (individualSim != null && !individualSim.trim().equals("")) {
                where += " s.simNumber LIKE '%" + individualSim + "%' OR ";
            }
            boolean hasDate = false;
            if (startDate != null && !startDate.trim().equals("") && endDate != null && !endDate.trim().equals("")) {
                hasDate = true;
                SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
                formatDate.parse(startDate);

                if (!where.trim().equals("")) {
                    where = where.substring(0, where.length() - 3) + " AND ";
                }
                where += " (s.loadDatetime > '" + startDate + "' AND s.loadDatetime < '" + endDate + "  23:59:59') ";
            }

            if (isoId != null && !isoId.trim().equals("") && !isoId.trim().equals("0")) {
                if (!where.trim().equals("") && hasDate) {
                    where += " AND ";
                }
                else if (!where.trim().equals("") && !hasDate) {
                    where = where.substring(0, where.length() - 3);
                    where += " AND ";
                }

                where += " (s.isoId = '" + isoId + "') ";
            }

            if (!where.trim().equals("")) {
                sql += " AND (" + where + ") ";
            }
            sql += " GROUP BY sb.id, sb.BatchId, sb.createDate ";

            if (orderDesc) {
                sql += "ORDER BY sb.createDate DESC ";
            }
            else {
                sql += " ORDER BY sb.createDate DESC ";
            }

            cat.debug("getBatchesList query: " + sql);
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                BatchesInfo batchesInfo = new BatchesInfo();
                batchesInfo.setId(rs.getString("id"));
                batchesInfo.setQuantity(rs.getInt("quantity"));
                batchesInfo.setLoadDatetime(rs.getTimestamp("createDate"));
                batchesInfo.setBatchId(rs.getInt("batchId"));
                listObj.add(batchesInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error during getBatchesList", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return listObj;

    }

    public static String getMerchantDBAById(String merchantId, String isoId) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            BigDecimal merchant = new BigDecimal(merchantId);
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = "select m.merchant_id, m.dba, agent.iso_id "
                    + "from merchants m WITH(NOLOCK)"
                    + "INNER JOIN reps rep WITH(NOLOCK) ON (rep.rep_id = m.rep_id) "
                    + "INNER JOIN reps subagent WITH(NOLOCK) ON (subagent.rep_id = rep.iso_id) "
                    + "INNER JOIN reps agent WITH(NOLOCK) ON (agent.rep_id = subagent.iso_id) "
                    + "INNER JOIN reps iso WITH(NOLOCK) ON (iso.rep_id = agent.iso_id) "
                    + "WHERE m.merchant_id = ? AND iso.rep_id  = ? ";
            cat.debug("getMerchantById query: " + sql + " /*" + merchantId + "," + isoId + "*/");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setBigDecimal(1, merchant);
            pstmt.setBigDecimal(2, new BigDecimal(isoId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getString("dba");
            }
        } catch (Exception e) {
            cat.error("Error during getMerchantDBAById", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return null;
    }

    public static List<String> verifySimsListDuplicateEdit(String simsArray[], String simBatchId) throws ReportException {
        return verifySimsListDuplicate(simsArray, simBatchId);
    }

    public static List<String> verifySimsListDuplicate(String simsArray[]) throws ReportException {
        return verifySimsListDuplicate(simsArray, null);
    }

    public static List<String> verifySimsListDuplicate(String simsArray[], String simBatchId) throws ReportException {
        List<String> listDuplicate = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql = "";
        try {
            if (simsArray == null || simsArray.length == 0) {
                listDuplicate.add("NOTHING");
                return listDuplicate;
            }
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

            sql = "SELECT * FROM sims WITH(NOLOCK) WHERE simNumber IN (";

            String where = "";
            for (int i = 0; i < simsArray.length; i++) {
                if (!simsArray[i].trim().equals("")) {
                    where += "'" + simsArray[i] + "'";
                    if (i < simsArray.length - 1) {
                        where += ",";
                    }
                }
            }
            if (where.endsWith(",")) {
                where = where.substring(0, where.length() - 1);
            }
            if (where.trim().equals("")) {
                listDuplicate.add("NOTHING");
                return listDuplicate;
            }

            where += ")";
            if (simBatchId != null) {
                where += " AND simBatchId != '" + simBatchId + "' ";
            }

            sql += where;

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            boolean existData = false;
            while (rs.next()) {
                existData = true;
                listDuplicate.add(rs.getString("simNumber"));
            }

            if (!existData) {
                listDuplicate.add("NOTHING");
            }
        } catch (Exception e) {
            cat.debug("getSimTypesList Preparing statement for query: " + sql);
            cat.error("Error during verifySimsListDuplicate", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return listDuplicate;

    }

    public ResponseInsertBatch insertSimBatches(String batchIdSelected, String[] simsTypeArray, String[] simsProviderArray, 
            String[] simsProductArray, String[] carrierForProductHiddenArray, String isoId, String[] simsList, 
            String startDate, String endDate) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        int batchId = 0;
        if (batchIdSelected.equalsIgnoreCase("-1")) {
            batchId = Integer.parseInt(generateBatchId());
        }
        else {
            String[] arrTemp = batchIdSelected.split("-");
            batchId = Integer.parseInt(arrTemp[0]);
        }

        ResponseInsertBatch response = new ResponseInsertBatch();
        response.setBatchId(batchId);
        response.setArrSimsInserted(new ArrayList<String>());
        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            if (batchIdSelected.equalsIgnoreCase("-1")) {
                strSQL = SimInventory.sql_bundle.getString("insertSimBatches");
                dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

                cat.debug("Insert SimBatches" + strSQL);
                pst = dbConn.prepareStatement(strSQL);
                pst.setString(1, uuid);
                pst.setString(2, getSimBatchesStatus(BATCH_STATUS_READY));
                pst.setTimestamp(3, new Timestamp((new Date()).getTime()));
                pst.setInt(4, batchId);
                pst.executeUpdate();
            }
            else {
                uuid = checkBatchId(String.valueOf(batchId));
            }

            // INSERT SIMS
            List<String> list = insertSims(uuid, simsTypeArray, simsProviderArray, 
                    simsProductArray, carrierForProductHiddenArray, isoId, simsList, startDate, endDate);
            response.setArrSimsInserted(list);
        } catch (Exception e) {
            cat.error("Error inserting SimBatches", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pst, null);
        }
        return response;
    }

    public List<String> insertSims(String batchId, String[] simsTypeArray, String[] simsProviderArray, 
            String[] simsProductArray, String[] carrierForProductHiddenArray, String isoId, String[] simsList,
            String startDate, String endDate) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        List<String> listResponseSims = new ArrayList<String>();

        try {
            cat.debug("Insert Sims list");
            strSQL = SimInventory.sql_bundle.getString("insertSims");
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

            String simStatusId = getSimStatus(SIM_STATUS_READY);
            Timestamp time = new Timestamp((new Date()).getTime());
            List<String> verifySimsListDuplicate = verifySimsListDuplicate(simsList);

            pst = dbConn.prepareStatement(strSQL);
            for (int i = 0; i < simsList.length; i++) {
                if (!simsList[i].trim().equals("") && canInsertSim(verifySimsListDuplicate, simsList, simsList[i])) {
                    listResponseSims.add(simsList[i]);
                    pst.setString(1, UUID.randomUUID().toString().toUpperCase());
                    pst.setString(2, batchId);
                    pst.setString(3, simsTypeArray[i]);
                    pst.setString(4, simsProviderArray[i]);
                    pst.setString(5, simStatusId);
                    pst.setString(6, simsList[i]);
                    pst.setString(7, isoId);
                    pst.setTimestamp(8, time);
                    
                    if(!simsProductArray[i].trim().equals("") && !simsProductArray[i].equalsIgnoreCase("ERROR_NOT_FOUND") 
                            && carrierForProductHiddenArray[i].equalsIgnoreCase(simsProviderArray[i])){
                        pst.setInt(9, Integer.parseInt(simsProductArray[i]));
                    }
                    else{
                        pst.setNull(9, java.sql.Types.INTEGER);
                    }
                    pst.setString(10, startDate);
                    pst.setString(11, endDate);
                    pst.execute();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error inserting Sims list", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pst, null);
        }
        return listResponseSims;
    }

    public ResponseInsertBatch updateSims(String batchCode, String[] simsTypeArray, String[] simsProviderArray, String[] simsList, String[] simsIds, String[] simsProductArray) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        ResponseInsertBatch response = new ResponseInsertBatch();
        response.setBatchId(Integer.parseInt(batchCode));
        response.setArrSimsInserted(new ArrayList<String>());
        List<String> listResponseSims = new ArrayList<String>();

        try {
            cat.debug("Updating Sims list");
            strSQL = SimInventory.sql_bundle.getString("updateSims");
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

            pst = dbConn.prepareStatement(strSQL);
            for (int i = 0; i < simsIds.length; i++) {
                if (!simsList[i].trim().equals("")) {
                    listResponseSims.add(simsList[i]);
                    pst.setString(1, simsTypeArray[i]);
                    pst.setString(2, simsProviderArray[i]);
                    pst.setString(3, simsList[i].trim());
                    pst.setString(4, simsProductArray[i].trim());
                    pst.setString(5, simsIds[i]);
                    pst.executeUpdate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error Updating Sims list", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pst, null);
        }
        response.setArrSimsInserted(listResponseSims);
        return response;
    }

    public static int updateSimBatches(String batchId, String merchantId, String defaultSiteId, String shippingDate) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            strSQL = SimInventory.sql_bundle.getString("assignSimBatches");
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

            cat.debug("assigning SimBatches" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, getSimBatchesStatus(BATCH_STATUS_ASSIGNED));
            pst.setTimestamp(2, new Timestamp((new Date()).getTime()));
            pst.setString(3, batchId);
            pst.executeUpdate();

            // UPDATE SIMS
            return assignSims(batchId, merchantId, defaultSiteId, shippingDate);
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error updateSimBatches", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pst, null);
        }
    }

    public static int assignSims(String batchId, String merchantId, String defaultSiteId, String shippingDate) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            strSQL = SimInventory.sql_bundle.getString("assignSims");
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

            cat.debug("assignSims Query" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, getSimStatus(SIM_STATUS_ASSIGNED));
            pst.setBigDecimal(2, new BigDecimal(merchantId));
            pst.setTimestamp(3, new Timestamp((new Date()).getTime()));
            if (!shippingDate.trim().equals("")) {
                pst.setString(4, shippingDate);
            }
            else {
                pst.setNull(4, java.sql.Types.TIMESTAMP);
            }
            pst.setInt(5, Integer.parseInt(defaultSiteId));
            pst.setString(6, batchId);
            pst.setString(7, getSimStatus(BATCH_STATUS_ASSIGNED));

            return pst.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error assignSims", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pst, null);
        }
    }

    private boolean canInsertSim(List<String> duplicateList, String[] simsList, String sim) throws ReportException {

        // Verify sims in the DB list
        boolean canInsert = true;
        for (String duplicateList1 : duplicateList) {
            if (duplicateList1.trim().equals(sim.trim())) {
                canInsert = false;
            }
        }
        if (canInsert) {
            String[] arrTemp = new String[1];
            arrTemp[0] = sim;
            List<String> verifySimsListDuplicate = verifySimsListDuplicate(arrTemp);
            for (String duplicateList1 : verifySimsListDuplicate) {
                if (duplicateList1.trim().equals(sim.trim())) {
                    canInsert = false;
                }
            }
        }
        else {
            return canInsert;
        }

        return canInsert;
    }

    public SimInventoryRequestMultipart analizeInputStream(javax.servlet.http.HttpServletRequest request, SessionData sessionData) {
        String line = "";
        cat.info("Init Save File Sim Inventory");
        List<SimUploadLine> listSims = new ArrayList();
        SimInventoryRequestMultipart multipartResponse = new SimInventoryRequestMultipart();
        String simProviderRequest = "";
        String simTypeRequest = "";
        String simProductRequest = "";
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                Map<String, String> simCarrierMap = SimProviderDao.getSimCarrierMapByName(false);
                Map<String, SimTypesPojo> simTypeMap = SimTypesDao.getSimTypeMapByName();
                Map<String, SimProductPojo> simProductMap = SimProviderDao.getSimProductsMapByName(false);

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator itr = fileItemsList.iterator();

                String[] split;
                String simNumber = "";
                String simCarrierDes = "";
                String simTypeDes = "";
                String simProductDescription = "";
                FileItem item = null;
                BufferedReader bufferedReader = null;
                boolean isRowData = false;
                while (itr.hasNext()) {
                    item = (FileItem) itr.next();
                    if (!item.isFormField()) {
                        bufferedReader = new BufferedReader((new InputStreamReader(item.getInputStream())));
                        while ((line = bufferedReader.readLine()) != null) {
                            if (!line.trim().equals("") && isRowData) {
                                split = line.split(",");
                                if (split.length >= 3) {
                                    simNumber = split[0];
                                    simCarrierDes = split[1].trim();
                                    simTypeDes = split[2].trim();
                                    SimTypesPojo typePojo = simTypeMap.get(simTypeDes);
                                    String typeId = (typePojo == null) ? null : typePojo.getId();
                                    SimUploadLine simUploadLine = new SimUploadLine(simNumber, simCarrierDes, simCarrierMap.get(simCarrierDes), simTypeDes, typeId);
                                    
                                    if (split.length == 4) { // Getting Sim product
                                        simProductDescription = split[3].trim();
                                        SimProductPojo prod = simProductMap.get(simProductDescription);
                                        if(prod != null){
                                            simUploadLine.setSimProductId(""+prod.getProductId());
                                            simUploadLine.setSimCurrentCarrierForProductId(prod.getCarrierIdNew());
                                        }
                                        else{
                                            simUploadLine.setSimProductId("ERROR_NOT_FOUND");
                                            simUploadLine.setSimCurrentCarrierForProductId("ERROR_NOT_FOUND");
                                        }
                                        simUploadLine.setSimProductDescription(simProductDescription);
                                    }
                                    
                                    listSims.add(simUploadLine);
                                }
                            }
                            isRowData = true;
                        }
                    }
                    if (item.isFormField()) {  // Check regular field.
                        String inputName = (String) item.getFieldName();
                        if (inputName.equalsIgnoreCase("simProvider")) {
                            simProviderRequest = (String) item.getString();
                            multipartResponse.setSimCarrierId(simProviderRequest);
                        }
                        if (inputName.equalsIgnoreCase("simType")) {
                            simTypeRequest = (String) item.getString();
                            multipartResponse.setSimTypeId(simTypeRequest);
                        }
                        if (inputName.equalsIgnoreCase("simProduct")) {
                            simProductRequest = (String) item.getString();
                            multipartResponse.setSimProduct(simProductRequest);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                cat.error("MerchantBulk saveFile error: " + e.getMessage() + "-" + e.getLocalizedMessage() + "-" + e);
            }
        }
        else {
            cat.error("MerchantBulk saveFile error: No MultipartContent");
        }
        multipartResponse.setListReaded(listSims);
        return multipartResponse;
    }

    public String generateBatchId() {
        boolean isUnique = false;
        String batchId = "";

        while (!isUnique) {
            batchId = this.generateRandomNumber();
            isUnique = this.isUniqueBatchId(batchId);
        }

        return batchId;
    }

    public static String getSimBatchesStatus(String batchesStatus) {

        String batchStatusId = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("getSimBatchesStatus");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, batchesStatus);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                batchStatusId = rs.getString("id");
            }
        } catch (Exception e) {
            cat.error("Error during checkSiteId", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return batchStatusId;
    }

    public static String getSimStatus(String simStatus) {

        String batchStatusId = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("getSimStatus");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, simStatus);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                batchStatusId = rs.getString("id");
            }
        } catch (Exception e) {
            cat.error("Error during getSimStatus", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return batchStatusId;
    }

    public static List<String> getSimNumberListsByBatchIdWithStatus(String batchId) {
        return getSimNumberListsByBatchId(batchId, true);
    }

    public static List<String> getSimNumberListsByBatchId(String batchId) {
        return getSimNumberListsByBatchId(batchId, false);
    }

    private static List<String> getSimNumberListsByBatchId(String batchId, boolean withStatus) {

        List<String> simNumberList = new ArrayList();
        List<SimPojo> simNumberListPojo = getSimNumberListsObjectsByBatchId(batchId);
        if (simNumberListPojo.isEmpty()) {
            simNumberList.add("NOTHING");
        }
        else {
            try {
                for (SimPojo sim : simNumberListPojo) {
                    String str = sim.getSimNumber() + "," + sim.getSimProviderName() + "," + sim.getSimTypeDescription()+ "," + sim.getSimProductDescription();
                    if (withStatus) {
                        str += "," + sim.getStatusCode();
                    }
                    simNumberList.add(str);
                }
            } catch (Exception e) {
                cat.error("Error during getSimNumberListsByBatchId", e);
            }
        }
        return simNumberList;
    }

    public static List<SimPojo> getSimNumberListsObjectsByBatchId(String batchId) {

        List<SimPojo> simNumberList = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            Map<String, String> simCarrierMap = SimProviderDao.getSimCarrierMapByName(true);
            Map<String, SimTypesPojo> simTypeMap = SimTypesDao.getSimTypeMapById();
            Map<String, SimProductPojo> simProductMap = SimProviderDao.getSimProductsMapByName(true);
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("getSimListsByBatchId");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, batchId);
            rs = pstmt.executeQuery();
            while (rs.next()) {

                SimPojo simPojo = new SimPojo();
                simPojo.setSimNumber(rs.getString("simNumber"));
                simPojo.setSimProviderId(rs.getString("simProviderId"));
                simPojo.setSimTypeId(rs.getString("simTypeId"));
                simPojo.setId(rs.getString("id"));
                simPojo.setBatchId(batchId);
                simPojo.setStatusCode(rs.getString("statusCode"));
                simPojo.setActivationDate(rs.getTimestamp("activationDate"));
                simPojo.setSimProductId(rs.getString("productId"));
                simPojo.setSimProviderName(simCarrierMap.get(simPojo.getSimProviderId()));
                simPojo.setSimTypeDescription(simTypeMap.get(simPojo.getSimTypeId()).getDescriptionEng());
                if(simPojo.getSimProductId() != null && !simPojo.getSimProductId().equals("0")){
                    simPojo.setSimProductDescription(simProductMap.get(simPojo.getSimProductId()).getDescription());
                }
                else{
                    simPojo.setSimProductId("");
                    simPojo.setSimProductDescription("");
                }
                simNumberList.add(simPojo);
            }
        } catch (Exception e) {
            cat.error("Error during getSimNumberListsObjectsByBatchId", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return simNumberList;
    }

    private boolean isUniqueBatchId(String batchId) {
        String uuid = checkBatchId(batchId);
        return uuid == null;
    }

    private String checkBatchId(String batchId) {

        String idUUID = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("verifyBathId");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(batchId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                idUUID = rs.getString("id");
            }
        } catch (Exception e) {
            cat.error("Error during checkSiteId", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return idUUID;
    }

    public boolean verifyBatch(String batchIdSelected, String isoId) {
        String[] arrTemp = batchIdSelected.split("-"); // format 4751767-[2016-10-28 16:02:23.647]
        if (arrTemp == null || arrTemp.length == 0 || arrTemp.length == 1) {
            return false;
        }
        boolean existBatch = !isUniqueBatchId(arrTemp[0]);
        if(!existBatch){
            return existBatch;
        }
        else{
            int countSims = getCountFreeSims(arrTemp[0], getFieldIdFromAutoComplete(isoId));
            return countSims != 0;
        }
    }
    
    public String getFieldIdFromAutoComplete(String realValue) {
        String[] arrTemp = realValue.split("-"); // format 4751767-[2016-10-28 16:02:23.647]
        if (arrTemp == null || arrTemp.length == 0 || arrTemp.length == 1) {
            return null;
        }
        return arrTemp[0];
    }
    
    public boolean isValidIsoId(String isoIdSelected){
        String isoId = getFieldIdFromAutoComplete(isoIdSelected);
        if(isoId == null){
            return false;
        }
        String isoFound = getIsoId(isoId);
        if(isoFound == null){
            return false;
        }
        return true;
    }
    

    // special number generator for site id's
    private String generateRandomNumber() {
        StringBuffer s = new StringBuffer();
        String[] firstChars = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        int nextInt;
        int index = (int) ((Math.random() * firstChars.length));
        nextInt = Integer.parseInt(firstChars[index]);
        s = s.append(nextInt);

        for (int i = 0; i <= 5; i++) {
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private static void closeStatements(Connection dbConn, PreparedStatement pstmt, ResultSet rs) {
        try {
            Torque.closeConnection(dbConn);
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            cat.error("Error during closeConnection", e);
        }
    }

    public static List<IsoPojo> getIsosList() throws ReportException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<IsoPojo> isosList = new ArrayList();

        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("selectAllIsos");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                isosList.add(new IsoPojo(rs.getInt("type"), rs.getBigDecimal("rep_id"), rs.getString("businessname")));
            }
        } catch (Exception e) {
            cat.error("Error during getMerchantDBAById", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return isosList;
    }

    public int moveSimBatchToIso(String isoOrigin, String batchNumber, String isoDestination) throws ReportException {

        String batchIdOld = checkBatchId(batchNumber);
        String simStatusIdReady = getSimStatus(SIM_STATUS_READY);
        String simStatusIdAssigned = getSimStatus(SIM_STATUS_ASSIGNED);

        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        String newSimBatchesId = "";
        int batchId = 0;
        try {
            
            batchId = Integer.parseInt(generateBatchId());
            newSimBatchesId = insertBatchMovingIso(batchId);
            
            strSQL = SimInventory.sql_bundle.getString("updateMovingSims");
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

            cat.debug("moveSimBatchToIso Query" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, newSimBatchesId);
            pst.setString(2, simStatusIdReady);
            pst.setBigDecimal(3, new BigDecimal(isoDestination));
            pst.setNull(4, Types.BIGINT);// MerchantId
            pst.setNull(5, Types.TIMESTAMP);// assignmentDate
            pst.setNull(6, Types.TIMESTAMP);// shippingDate
            pst.setString(7, batchIdOld);
            pst.setBigDecimal(8, new BigDecimal(isoOrigin));
            pst.setString(9, simStatusIdReady);
            pst.setString(10, simStatusIdAssigned);
            
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error moveSimBatchToIso", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pst, null);
        }
        return batchId;
    }

    private String insertBatchMovingIso(int batchId) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        String uuid = null;
        
        try {
            uuid = UUID.randomUUID().toString().toUpperCase();
            strSQL = SimInventory.sql_bundle.getString("insertSimBatches");
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));

            cat.debug("Insert SimBatches Moving Iso" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, uuid);
            pst.setString(2, getSimBatchesStatus(BATCH_STATUS_READY));
            pst.setTimestamp(3, new Timestamp((new Date()).getTime()));
            pst.setInt(4, batchId);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error inserting insertBatchMovingIso", e);
        } finally {
            closeStatements(dbConn, pst, null);
        }
        return uuid;
    }
    
    public int countSimsByIso(int batchCode, String isoId){
        String batchId = checkBatchId(String.valueOf(batchCode));
        int countSims = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("countSimsByBatchIdAndIso");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, batchId);
            pstmt.setBigDecimal(2, new BigDecimal(isoId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                countSims = rs.getInt("quantity");
            }
        } catch (Exception e) {
            cat.error("Error during countSimsByIso", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return countSims;
    }
    
    public int countSimsByMasterNotNull(int batchCode, String isoId){
        String batchId = checkBatchId(String.valueOf(batchCode));
        int countSims = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("countSimsByBatchIdAndMasterId");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, batchId);
            pstmt.setBigDecimal(2, new BigDecimal(isoId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                countSims = rs.getInt("quantity");
            }
        } catch (Exception e) {
            cat.error("Error during countSimsByMasterId", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return countSims;
    }
    
    
    
    private static boolean isNumber(String field){
        try{
            Double.parseDouble(field);
            return true;
        }catch(NumberFormatException e){
            return false;
        }
    }
    
    public int getCountFreeSims(String batchNumber, String isoOrigin) {

        int count = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        String batchIdOld = checkBatchId(batchNumber);
        String simStatusIdReady = getSimStatus(SIM_STATUS_READY);
        String simStatusIdAssigned = getSimStatus(SIM_STATUS_ASSIGNED);
        
        try {
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("countFreeSims");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, batchIdOld);
            pstmt.setBigDecimal(2, new BigDecimal(isoOrigin));
            pstmt.setString(3, simStatusIdReady);
            pstmt.setString(4, simStatusIdAssigned);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt("quantity");
            }
        } catch (Exception e) {
            cat.error("Error during getSimStatus", e);
            return 0;
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return count;
    }
    
    public static String getIsoId(String isoId) {

        String isoIdResponse = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            boolean isnumber = isNumber(isoId);
            if(!isnumber){
                return null;
            }
            dbConn = TorqueHelper.getConnection(SimInventory.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimInventory.sql_bundle.getString("getIsoById");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setBigDecimal(1, new BigDecimal(isoId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                isoIdResponse = rs.getString("rep_id");
            }
        } catch (Exception e) {
            cat.error("Error during getSimStatus", e);
            return null;
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return isoIdResponse;
    }
}
