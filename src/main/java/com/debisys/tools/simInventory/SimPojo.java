package com.debisys.tools.simInventory;

import java.sql.Timestamp;

/**
 *
 * @author dgarzon
 */
public class SimPojo {
    private String id;
    private String batchId;
    private String simTypeId;
    private String simProviderId;
    private String simTypeDescription;
    private String simProviderName;
    private String statusCode;
    private String simNumber;
    private String simProductId;
    private String simProductDescription;
    private Timestamp activationDate;

    private String startDateTime;
    private String endDateTime;
            
    private String merchantId;
    private String isoId;
    private String merchantName;
    private String isoName;
    private String creationDateBatch;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getSimTypeId() {
        return simTypeId;
    }

    public void setSimTypeId(String simTypeId) {
        this.simTypeId = simTypeId;
    }

    public String getSimProviderId() {
        return simProviderId;
    }

    public void setSimProviderId(String simProviderId) {
        this.simProviderId = simProviderId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public Timestamp getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Timestamp activationDate) {
        this.activationDate = activationDate;
    }

    public String getSimTypeDescription() {
        return simTypeDescription;
    }

    public void setSimTypeDescription(String simTypeDescription) {
        this.simTypeDescription = simTypeDescription;
    }

    public String getSimProviderName() {
        return simProviderName;
    }

    public void setSimProviderName(String simProviderName) {
        this.simProviderName = simProviderName;
    }

    public String getSimProductId() {
        return simProductId;
    }

    public void setSimProductId(String simProductId) {
        this.simProductId = simProductId;
    }

    public String getSimProductDescription() {
        return simProductDescription;
    }

    public void setSimProductDescription(String simProductDescription) {
        this.simProductDescription = simProductDescription;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the isoName
     */
    public String getIsoName() {
        return isoName;
    }

    /**
     * @param isoName the isoName to set
     */
    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }

    /**
     * @return the creationDateBatch
     */
    public String getCreationDateBatch() {
        return creationDateBatch;
    }

    /**
     * @param creationDateBatch the creationDateBatch to set
     */
    public void setCreationDateBatch(String creationDateBatch) {
        this.creationDateBatch = creationDateBatch;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the isoId
     */
    public String getIsoId() {
        return isoId;
    }

    /**
     * @param isoId the isoId to set
     */
    public void setIsoId(String isoId) {
        this.isoId = isoId;
    }

    /**
     * @return the startDateTime
     */
    public String getStartDateTime() {
        return startDateTime;
    }

    /**
     * @param startDateTime the startDateTime to set
     */
    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    /**
     * @return the endDateTime
     */
    public String getEndDateTime() {
        return endDateTime;
    }

    /**
     * @param endDateTime the endDateTime to set
     */
    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }
    
    
}
