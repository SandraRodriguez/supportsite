
package com.debisys.tools.simInventory;

import java.sql.Timestamp;

/**
 *
 * @author dgarzon
 */
public class BatchesInfo {
    
    private String id;
    private String SIMProvider;
    private String SIMType;
    private int quantity;
    private Timestamp loadDatetime;
    private int batchId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSIMProvider() {
        return SIMProvider;
    }

    public void setSIMProvider(String SIMProvider) {
        this.SIMProvider = SIMProvider;
    }

    public String getSIMType() {
        return SIMType;
    }

    public void setSIMType(String SIMType) {
        this.SIMType = SIMType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Timestamp getLoadDatetime() {
        return loadDatetime;
    }

    public void setLoadDatetime(Timestamp loadDatetime) {
        this.loadDatetime = loadDatetime;
    }

    public int getBatchId() {
        return batchId;
    }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }
    
    
}
