
package com.debisys.tools.simInventory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dgarzon
 */
public class SimInventoryRequestMultipart {
    
    private String simCarrierId;
    private String simTypeId;
    private String simProduct;
    private List<SimUploadLine> listReaded;

    public SimInventoryRequestMultipart() {
        simCarrierId = "";
        simTypeId = "";
        simProduct = "";
        listReaded = new ArrayList<SimUploadLine>();
    }

    public String getSimCarrierId() {
        return simCarrierId;
    }

    public void setSimCarrierId(String simCarrierId) {
        this.simCarrierId = simCarrierId;
    }

    public String getSimTypeId() {
        return simTypeId;
    }

    public void setSimTypeId(String simTypeId) {
        this.simTypeId = simTypeId;
    }

    public String getSimProduct() {
        return simProduct;
    }

    public void setSimProduct(String simProduct) {
        this.simProduct = simProduct;
    }

    public List<SimUploadLine> getListReaded() {
        return listReaded;
    }

    public void setListReaded(List<SimUploadLine> listReaded) {
        this.listReaded = listReaded;
    }
}
