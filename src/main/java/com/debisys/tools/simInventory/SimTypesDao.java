/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools.simInventory;

import com.debisys.exceptions.ReportException;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 *
 * @author dgarzon
 */
public class SimTypesDao {
    
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.simInventory.sql");

    static Category cat = Category.getInstance(SimTypesDao.class);
    
    
    public static List<SimTypesPojo> getSimTypesList() throws ReportException {
        List<SimTypesPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimTypesDao.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimTypesDao.sql_bundle.getString("getSimTypesList");
            cat.debug("getSimTypesList Preparing statement for query: " + sql);

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimTypesPojo simType = new SimTypesPojo(rs.getString("id"), rs.getString("descriptionEng"), rs.getString("descriptionSpa"), rs.getString("code"));
                retValue.add(simType);
            }
        } catch (Exception e) {
            cat.error("Error during getSimTypesList", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }
    
    public static Map<String, SimTypesPojo> getSimTypeMapByName() throws ReportException{
        Map<String,SimTypesPojo> map = new HashMap<String, SimTypesPojo>();
        List<SimTypesPojo> simTypesList = getSimTypesList();
        for (SimTypesPojo pojo : simTypesList) {
            map.put(pojo.getDescriptionEng().trim(),pojo);
        }
        return map;
    }
    
    public static Map<String, SimTypesPojo> getSimTypeMapById() throws ReportException{
        Map<String,SimTypesPojo> map = new HashMap<String, SimTypesPojo>();
        List<SimTypesPojo> simTypesList = getSimTypesList();
        for (SimTypesPojo pojo : simTypesList) {
            map.put(pojo.getId(), pojo);
        }
        return map;
    }
    
    public static SimTypesPojo getSimTypesById(String simTypeId) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimTypesDao.sql_bundle.getString("pkgDefaultDb"));
            String sql = SimTypesDao.sql_bundle.getString("getSimTypesList");
            sql += " WHERE id = ? ";
            cat.debug("getSimTypesById Preparing statement for query: " + sql);
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, simTypeId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return new SimTypesPojo(rs.getString("id"), rs.getString("descriptionEng"), rs.getString("descriptionSpa"),rs.getString("code"));
            }
        } catch (Exception e) {
            cat.error("Error during getSimTypesById", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return null;
    }
    
    private static void closeStatements(Connection dbConn, PreparedStatement pstmt, ResultSet rs) {
        try {
            Torque.closeConnection(dbConn);
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            cat.error("Error during closeConnection", e);
        }
    }
}
