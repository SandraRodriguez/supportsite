package com.debisys.tools.simInventory;

/**
 *
 * @author dgarzon
 */
public class SimTypesPojo {
    
    private String id;
    private String descriptionEng;
    private String descriptionSpa;
    private String code;

    public SimTypesPojo(String id, String descriptionEng, String descriptionSpa, String code) {
        this.id = id;
        this.descriptionEng = descriptionEng;
        this.descriptionSpa = descriptionSpa;
        this.code = code;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescriptionEng() {
        return descriptionEng;
    }

    public void setDescriptionEng(String descriptionEng) {
        this.descriptionEng = descriptionEng;
    }

    public String getDescriptionSpa() {
        return descriptionSpa;
    }

    public void setDescriptionSpa(String descriptionSpa) {
        this.descriptionSpa = descriptionSpa;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    
}
