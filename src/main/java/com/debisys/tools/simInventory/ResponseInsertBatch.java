/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools.simInventory;

import java.util.List;

/**
 *
 * @author dgarzon
 */
public class ResponseInsertBatch {
    
    private int batchId;
    private List<String> arrSimsInserted;

    public int getBatchId() {
        return batchId;
    }

    public void setBatchId(int batchId) {
        this.batchId = batchId;
    }

    public List<String> getArrSimsInserted() {
        return arrSimsInserted;
    }

    public void setArrSimsInserted(List<String> arrSimsInserted) {
        this.arrSimsInserted = arrSimsInserted;
    }
    
    
}
