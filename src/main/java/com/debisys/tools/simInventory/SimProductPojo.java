
package com.debisys.tools.simInventory;

/**
 *
 * @author dgarzon
 */
public class SimProductPojo {
    
    private int productId;
    private String description;
    private String carrierIdNew;

    public SimProductPojo(int productId, String description, String carrierIdNew) {
        this.productId = productId;
        this.description = description;
        this.carrierIdNew = carrierIdNew;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCarrierIdNew() {
        return carrierIdNew;
    }

    public void setCarrierIdNew(String carrierIdNew) {
        this.carrierIdNew = carrierIdNew;
    }
    
}
