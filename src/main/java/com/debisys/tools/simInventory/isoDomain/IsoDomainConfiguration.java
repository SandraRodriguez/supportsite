
package com.debisys.tools.simInventory.isoDomain;

import com.debisys.exceptions.TransactionException;
import com.debisys.users.SessionData;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.Vector;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class IsoDomainConfiguration {
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.simInventory.sql");
    static Category cat = Category.getInstance(IsoDomainConfiguration.class);

    public static List<IsoDomainVo> getIsoDomainList() throws Exception {
        List<IsoDomainVo> list = new ArrayList<IsoDomainVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("selectAllIsoDomain");

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                IsoDomainVo isoDomain = new IsoDomainVo(rs.getString("id"), rs.getBigDecimal("isoId"), rs.getString("isoDomain"), rs.getInt("defaultSiteId"));
                isoDomain.setIsoBusinessName(getIsoBusinessName(rs.getBigDecimal("isoId")));
                isoDomain.setMerchantDba(getMerchantDbaBySiteId(rs.getInt("defaultSiteId")));
                list.add(isoDomain);
            }
        } catch (Exception e) {
            cat.error("Error during getIsoLoginUsersList", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        Collections.sort(list);
        return list;
    }

    public static String getIsoBusinessName(BigDecimal isoId) throws Exception {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("selectIsoBusinessName");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setBigDecimal(1, isoId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getString("businessname");
            }
        } catch (Exception e) {
            cat.error("Error during getIsoBusinessName", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static IsoDomainVo getIsoDomainByPK(String id) throws Exception {
        String sql = sql_bundle.getString("selectIsoDomainByPKId");
        return getIsoLoginByUserId(id, sql);
    }

    public static IsoDomainVo getIsoDomainsByDomain(String domain) throws Exception {
        String sql = sql_bundle.getString("selectIsoDomainByDomain");
        return getIsoLoginByUserId(domain, sql);
    }

    private static IsoDomainVo getIsoLoginByUserId(String userId, String sql) throws Exception {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                IsoDomainVo isoDomain = new IsoDomainVo(rs.getString("id"), rs.getBigDecimal("isoId"), rs.getString("isoDomain"), rs.getInt("defaultSiteId"));
                isoDomain.setIsoBusinessName(getIsoBusinessName(rs.getBigDecimal("isoId")));
                isoDomain.setMerchantDba(getMerchantDbaBySiteId(rs.getInt("defaultSiteId")));
                return isoDomain;
            }
        } catch (Exception e) {
            cat.error("Error during getIsoLoginByUserId", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }
    
    public static String getMerchantDbaBySiteId(int siteId) throws Exception {
        
        if(siteId == 0){
            return "";
        }
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("selectMerchantDba");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, siteId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getString("dba");
            }
        } catch (Exception e) {
            cat.error("Error during getIsoBusinessName", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static boolean insertIsoDomain(SessionData sessionData, IsoDomainVo isoDomain) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            isoDomain.setId(uuid);
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("isoDomainInsert");
            cat.debug("SQL IsoLogin" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, uuid);
            pst.setBigDecimal(2, isoDomain.getIsoId());
            pst.setString(3, isoDomain.getIsoDomain());
            pst.setInt(4, isoDomain.getDefaultSiteId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            cat.error("Error insertIsoLogin", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
        return false;
    }

    public static boolean updateIsoDomain(IsoDomainVo isoDomain) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("updateIsoDomain");
            cat.debug("SQL updateIsoDomain" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            
            pst.setBigDecimal(1, isoDomain.getIsoId());
            pst.setString(2, isoDomain.getIsoDomain());
            pst.setInt(3, isoDomain.getDefaultSiteId());            
            pst.setString(4, isoDomain.getId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            cat.error("Error updateIsoDomain", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
        return false;
    }

    public static void deleteIsoDomainById(String id) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("isoDomainDelete");
            cat.debug("SQL isoDomainDelete " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, id);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error isoDomainDelete", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static Vector getIsoList(BigDecimal isoInclude) throws TransactionException {
        Vector vecResultList = new Vector();
        Connection dbConn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new TransactionException();
            }
            String isoList = "";
            List<IsoDomainVo> isoLoginUsersList = getIsoDomainList();
            for (IsoDomainVo isoLogin : isoLoginUsersList) {
                if(isoInclude == null || !isoLogin.getIsoId().toString().equalsIgnoreCase(isoInclude.toString())){
                    isoList += isoLogin.getIsoId()+",";
                }
            }
            if(!isoList.isEmpty()){
                isoList = "AND rep_id NOT IN ("+isoList.substring(0, isoList.length()-1)+")";
            }
            
            String strSQLGetIsoList = "SELECT rep_id, businessname FROM reps WITH (nolock) WHERE  type in (2,3) "+isoList+" ORDER BY businessname";
            cat.debug(strSQLGetIsoList);
            pstmt = dbConn.prepareStatement(strSQLGetIsoList);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
                vecTemp.add(StringUtil.toString(rs.getString("businessname")));
                vecResultList.add(vecTemp);
            }
        } catch (Exception e) {
            cat.error("Error during getIsoList", e);
            throw new TransactionException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecResultList;
    }
    
    public static Vector getSiteIdsListByIso(String iso) throws TransactionException {
        Vector vecResultList = new Vector();
        Connection dbConn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new TransactionException();
            }
            
            StringBuilder sb = new StringBuilder();
            
            sb.append("SELECT t.millennium_no, m.dba ");
            sb.append("FROM terminals t WITH (nolock) ");
            sb.append("INNER JOIN merchants m WITH (nolock) ON (m.merchant_id = t.merchant_id) ");
            sb.append("INNER JOIN reps r WITH (nolock) ON (m.rep_id = r.rep_id) ");
            sb.append("INNER JOIN reps s WITH (nolock) ON (r.iso_id = s.rep_id) ");
            sb.append("INNER JOIN reps a WITH (nolock) ON (s.iso_id = a.rep_id) ");
            sb.append("INNER JOIN reps i WITH (nolock) ON (a.iso_id = i.rep_id) ");
            sb.append("WHERE i.rep_id = ? AND (t.terminal_type = 23 OR t.terminal_type = 152) ORDER BY m.dba ");
            
            cat.debug(sb.toString());
            pstmt = dbConn.prepareStatement(sb.toString());
            pstmt.setBigDecimal(1, new BigDecimal(iso));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
                vecTemp.add(StringUtil.toString(rs.getString("dba")));
                vecResultList.add(vecTemp);
            }
        } catch (Exception e) {
            cat.error("Error during getSiteIdsListByIso", e);
            throw new TransactionException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecResultList;
    }
    
    
    public static Vector getSiteIdsListByMerchantId(String merchantId) throws TransactionException {
        Vector vecResultList = new Vector();
        Connection dbConn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new TransactionException();
            }
            
            StringBuilder sb = new StringBuilder();
            
            sb.append("SELECT t.millennium_no, m.dba ");
            sb.append("FROM terminals t WITH (nolock) ");
            sb.append("INNER JOIN merchants m WITH (nolock) ON (m.merchant_id = t.merchant_id) ");
            sb.append("WHERE m.merchant_id = ? AND (t.terminal_type = 23 OR t.terminal_type = 152) ORDER BY m.dba ");
            
            cat.debug(sb.toString());
            pstmt = dbConn.prepareStatement(sb.toString());
            pstmt.setBigDecimal(1, new BigDecimal(merchantId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
                vecTemp.add(StringUtil.toString(rs.getString("dba")));
                vecResultList.add(vecTemp);
            }
        } catch (Exception e) {
            cat.error("Error during getSiteIdsListByMerchantId", e);
            throw new TransactionException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecResultList;
    }


}
