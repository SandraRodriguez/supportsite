
package com.debisys.tools.simInventory.isoDomain;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class IsoDomainVo implements Comparable<IsoDomainVo>{
    
    private String id;
    private BigDecimal isoId;
    private String isoDomain;
    private int defaultSiteId;
    private String isoBusinessName;
    private String merchantDba;

    public IsoDomainVo() {
    }
    

    public IsoDomainVo(String id, BigDecimal isoId, String isoDomain, int defaultSiteId) {
        this.id = id;
        this.isoId = isoId;
        this.isoDomain = isoDomain;
        this.defaultSiteId = defaultSiteId;
        this.merchantDba = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getIsoId() {
        return isoId;
    }

    public void setIsoId(BigDecimal isoId) {
        this.isoId = isoId;
    }

    public String getIsoDomain() {
        return isoDomain;
    }

    public void setIsoDomain(String isoDomain) {
        this.isoDomain = isoDomain;
    }

    public int getDefaultSiteId() {
        return defaultSiteId;
    }

    public void setDefaultSiteId(int defaultSiteId) {
        this.defaultSiteId = defaultSiteId;
    }

    public String getIsoBusinessName() {
        return isoBusinessName;
    }

    public void setIsoBusinessName(String isoBusinessName) {
        this.isoBusinessName = isoBusinessName;
    }

    public String getMerchantDba() {
        return merchantDba;
    }

    public void setMerchantDba(String merchantDba) {
        this.merchantDba = merchantDba;
    }
    
    @Override
    public int compareTo(IsoDomainVo o) {
        return this.isoBusinessName.toUpperCase().compareTo(o.isoBusinessName.toUpperCase());
    }
}
