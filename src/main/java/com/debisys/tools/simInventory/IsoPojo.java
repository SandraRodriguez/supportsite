
package com.debisys.tools.simInventory;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class IsoPojo {
    
    private int type;
    private BigDecimal rep_id;
    private String businessname;

    public IsoPojo(int type, BigDecimal rep_id, String businessname) {
        this.type = type;
        this.rep_id = rep_id;
        this.businessname = businessname;
    }
    
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public BigDecimal getRep_id() {
        return rep_id;
    }

    public void setRep_id(BigDecimal rep_id) {
        this.rep_id = rep_id;
    }

    public String getBusinessname() {
        return businessname;
    }

    public void setBusinessname(String businessname) {
        this.businessname = businessname;
    }
    
    
}
