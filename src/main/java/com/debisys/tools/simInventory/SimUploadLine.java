
package com.debisys.tools.simInventory;

/**
 *
 * @author dgarzon
 */
public class SimUploadLine {
    
    private String simNumber;
    private String simCarrierDescription;
    private String simCarrierId;
    private String simTypeDescription;
    private String simTypeId; 
    private String simProductDescription;
    private String simProductId;
    private String simCurrentCarrierForProductId;

    public SimUploadLine(String simNumber, String simCarrierDescription, String simCarrierId, String simTypeDescription, String simTypeId) {
        this.simNumber = simNumber;
        this.simCarrierDescription = simCarrierDescription;
        this.simCarrierId = simCarrierId;
        this.simTypeDescription = simTypeDescription;
        this.simTypeId = simTypeId;
        this.simProductDescription = "";
        this.simProductId = "";
        this.simCurrentCarrierForProductId = "";
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getSimCarrierDescription() {
        return simCarrierDescription;
    }

    public void setSimCarrierDescription(String simCarrierDescription) {
        this.simCarrierDescription = simCarrierDescription;
    }

    public String getSimTypeDescription() {
        return simTypeDescription;
    }

    public void setSimTypeDescription(String simTypeDescription) {
        this.simTypeDescription = simTypeDescription;
    }

    public String getSimCarrierId() {
        return simCarrierId;
    }

    public void setSimCarrierId(String simCarrierId) {
        this.simCarrierId = simCarrierId;
    }

    public String getSimTypeId() {
        return simTypeId;
    }

    public void setSimTypeId(String simTypeId) {
        this.simTypeId = simTypeId;
    }

    public String getSimProductDescription() {
        return simProductDescription;
    }

    public void setSimProductDescription(String simProductDescription) {
        this.simProductDescription = simProductDescription;
    }

    public String getSimProductId() {
        return simProductId;
    }

    public void setSimProductId(String simProductId) {
        this.simProductId = simProductId;
    }

    public String getSimCurrentCarrierForProductId() {
        return simCurrentCarrierForProductId;
    }

    public void setSimCurrentCarrierForProductId(String simCurrentCarrierForProductId) {
        this.simCurrentCarrierForProductId = simCurrentCarrierForProductId;
    }
    
}
