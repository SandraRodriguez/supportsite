/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools.simInventory;

import com.debisys.exceptions.ReportException;
import com.debisys.utils.QueryReportData;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class SimEditProduct {

    private static final Integer WEB_ACTIVATION_TERMINAL_TYPE = 152;

    static Category cat = Category.getInstance(SimEditProduct.class);

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.simInventory.sql");
    
    private SimEditProduct() {
    }

    public static List<SimProductPojo> getSimProductsList(String carriersIds) throws ReportException {
        List<SimProductPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimEditProduct.sql_bundle.getString("pkgDefaultDb"));
            String sql;
            if (carriersIds == null) {
                sql = "SELECT p.id, p.description AS description, c.IdNew AS carrierIdNew FROM products p WITH(NOLOCK) \n"
                        + "INNER JOIN SimCarrierMapping s WITH(NOLOCK) ON p.carrier_id = s.EmidaCarrierId \n"
                        + "INNER JOIN carriers c WITH(NOLOCK) ON c.ID = p.carrier_id "
                        + "WHERE (p.trans_type=25 or p.trans_type=20) ORDER BY description ASC ";
            } else {
                String[] carrier = carriersIds.split(",");
                StringBuilder ca = new StringBuilder();
                for (String carrierIdNew : carrier) {
                    ca.append("'").append(carrierIdNew).append("',");
                }
                String carriersIn = ca.toString().substring(0, ca.toString().length() - 1);
                sql = "SELECT p.id, p.description , c.IdNew AS carrierIdNew FROM products p WITH(NOLOCK) "
                        + "INNER JOIN SimCarrierMapping s WITH(NOLOCK) ON p.carrier_id = s.EmidaCarrierId \n"
                        + "INNER JOIN Carriers c WITH(NOLOCK) ON c.ID = s.EmidaCarrierId \n"
                        + " WHERE (p.trans_type=25 or p.trans_type=20) AND c.IdNew in (" + carriersIn + ") \n"
                        + " ORDER BY description ASC";
            }
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimProductPojo product = new SimProductPojo(rs.getInt("id"), rs.getString("description").trim(), rs.getString("carrierIdNew").trim());
                retValue.add(product);
            }
        } catch (Exception e) {
            cat.error("Error during getSimProductsList ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static Integer getCountSims(QueryReportData reporInfo) throws ReportException {
        Integer retValue = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimEditProduct.sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 1);
            cat.info("getting count sims sql [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = rs.getInt(1);
            }
        } catch (Exception e) {
            cat.error("Error during getCountSims ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static List<SimPojo> getSims(QueryReportData reporInfo) throws ReportException {
        List<SimPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimEditProduct.sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 0);
            cat.info("getting sim sql [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimPojo sim = new SimPojo();
                sim.setSimNumber(rs.getString("simNumber"));
                sim.setSimProductId(rs.getString("simProductId"));
                sim.setSimProductDescription(rs.getString("simProductDescription"));
                if (reporInfo.getExpired()){
                    sim.setStartDateTime(rs.getString("startDateTime"));
                    sim.setEndDateTime(rs.getString("endDateTime"));
                }                
                retValue.add(sim);
            }
        } catch (Exception e) {
            cat.error("Error during getSims ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static Integer changeProductAndCarrier(QueryReportData reporInfo) throws ReportException {
        Integer retValue = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimEditProduct.sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 2);
            cat.info("updating sims product Ids sql [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            retValue = pstmt.executeUpdate();
        } catch (Exception e) {
            cat.error("Error during changeProductAndCarrier ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static Integer deleteSims(QueryReportData reporInfo) throws ReportException {
        Integer retValue = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimEditProduct.sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 4);
            cat.info("deleting sims with sql [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            retValue = pstmt.executeUpdate();
        } catch (Exception e) {
            cat.error("Error during deleteSims ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }
        
    public static Integer changeProductAndCarrierAndStatus(QueryReportData reporInfo) throws ReportException {
        Integer retValue = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(SimEditProduct.sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 3);
            cat.info("updating sims product Ids sql [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            retValue = pstmt.executeUpdate();
        } catch (Exception e) {
            cat.error("Error during changeProductAndCarrierAndStatus ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    private static String getQuery(QueryReportData reporInfo, Integer actionType) {
        StringBuilder select = new StringBuilder();
        StringBuilder where = new StringBuilder();
        select.append(" SELECT s.simNumber AS simNumber, s.productId AS simProductId, p.description AS simProductDescription, s.defaultSiteId ");
        if (reporInfo.getExpired()) {
            select.append(" ,s.startDateTime, s.endDateTime ");
        }
        StringBuilder body = new StringBuilder();
        body.append(" FROM SIMS s WITH(NOLOCK) INNER JOIN products p WITH(NOLOCK) ON p.id= s.productId ");
        if (reporInfo.getExpired()) {
            body.append(" INNER JOIN simStatus st WITH(NOLOCK) ON st.id= s.simStatusId AND st.code='EX' ");
            if (reporInfo.getStartExpiredMoment()!=null && reporInfo.getEndExpiredMoment()!=null){            
                body.append(" AND s.endDateTime >'").append(reporInfo.getStartExpiredMoment()).append("'  ");
                body.append(" AND s.endDateTime   < DATEADD(day, 1,'").append(reporInfo.getEndExpiredMoment()).append("')  \n");
            }
        } else {
            body.append(" INNER JOIN simStatus st WITH(NOLOCK) ON st.id= s.simStatusId AND (st.code='AS' OR st.code='R')");
        }
        if (reporInfo.getBatchId() != null) {
            body.append(" INNER JOIN simBatches b WITH(NOLOCK) ON s.simBatchId = b.id AND  b.batchId = ").append(reporInfo.getBatchId());
        } else if (reporInfo.getIndividualSim() != null) {
            where.append(" s.simNumber='").append(reporInfo.getIndividualSim()).append("' AND ");
        } else if (reporInfo.getProducts() != null) {
            String[] data = reporInfo.getProducts().split(",");
            StringBuilder inData = new StringBuilder();
            for (String dataInfo : data) {
                inData.append(dataInfo).append(",");
            }
            String appendWhereData = inData.toString().substring(0, inData.toString().length() - 1);
            where.append(" p.id in (").append(appendWhereData).append(") AND ");
        } else if (reporInfo.getCarriers() != null) {
            if (reporInfo.getCarriers().equals("-1")) {
                body.append(" INNER JOIN SimCarrierMapping c WITH(NOLOCK) ON c.EmidaCarrierId = p.carrier_id ");
            } else {
                String[] data = reporInfo.getCarriers().split(",");
                StringBuilder inData = new StringBuilder();
                for (String dataInfo : data) {
                    inData.append("'").append(dataInfo).append("',");
                }
                String appendWhereData = inData.toString().substring(0, inData.toString().length() - 1);
                where.append(" p.carrierIdNew in (").append(appendWhereData).append(") AND ");
            }
        }
        if (reporInfo.getSimInfo() != null) {
            where.append(" s.simNumber like '").append(reporInfo.getSimInfo()).append("%' AND ");
        }
        where.append(" s.productId IS NOT NULL ");
        where.append(" AND (p.trans_type=25 or p.trans_type=20) ");
        body = body.append(" WHERE ").append(where);
        StringBuilder sb0 = new StringBuilder();
        if (actionType == 0) {
            sb0.append("SELECT * FROM (");
            sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(reporInfo.getSortFieldName()).append(" ").append(reporInfo.getSortOrder()).append(") AS RWN, * FROM (");
            sb0.append(select).append(body).append(" \n");
            sb0.append(") AS tbl1 ");
            sb0.append(") AS tbl2 ");
            sb0.append(" WHERE RWN BETWEEN ").append(reporInfo.getStartPage()).append(" AND ").append(reporInfo.getEndPage());
        } else if (actionType == 1) {
            sb0 = new StringBuilder(" SELECT COUNT(*) ");
            sb0.append(body);
        } else if (actionType == 2) {
            sb0 = new StringBuilder(" UPDATE Sims SET simProviderId='" + reporInfo.getNewCarrier() + "', productId=" + reporInfo.getNewProduct() + " ");
            sb0.append(body);
        } else if (actionType == 3) {
            sb0 = new StringBuilder(" UPDATE Sims SET ");
            if (reporInfo.getNewCarrier() != null && reporInfo.getNewProduct() != null) {
                sb0.append(" simProviderId='").append(reporInfo.getNewCarrier());
                sb0.append("', productId=").append(reporInfo.getNewProduct()).append(", ");
            }
            sb0.append(" startDateTime='").append(reporInfo.getStartDate()).append("', ");
            sb0.append(" endDateTime='").append(reporInfo.getEndDate()).append("', ");
            String ReadyId = findStatusIdByCode("R");
            sb0.append(" simStatusId='").append(ReadyId).append("' ");
            
            sb0.append(body);
        } else if (actionType == 4){
            sb0 = new StringBuilder(" DELETE s ");
            sb0.append(body);
        }
        return sb0.toString();
    }

    private static String findStatusIdByCode(String code) {
        String retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(SimEditProduct.sql_bundle.getString("pkgDefaultDb"));
            String sql = "SELECT s.id FROM simStatus s WITH(NOLOCK) WHERE s.code='"+code+"'";
            cat.info("find id for ready status [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()){
                retValue = rs.getString(1);
            }
        } catch (Exception e) {
            cat.error("Error during findStatusIdByCode ", e);            
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }
    
    private static void closeStatements(Connection dbConn, PreparedStatement pstmt, ResultSet rs) {
        try {
            Torque.closeConnection(dbConn);
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            cat.error("Error during closeConnection", e);
        }
    }

}
