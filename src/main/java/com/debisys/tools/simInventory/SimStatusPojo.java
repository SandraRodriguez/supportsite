/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools.simInventory;

/**
 *
 * @author nmartinez
 */
public class SimStatusPojo {

    private String id;
    private String descriptionEng;
    private String descriptionSpa;
    private String code;

    public SimStatusPojo(String id, String descriptionEng, String descriptionSpa, String code) {
        this.id = id;
        this.descriptionEng = descriptionEng;
        this.descriptionSpa = descriptionSpa;
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescriptionEng() {
        return descriptionEng;
    }

    public void setDescriptionEng(String descriptionEng) {
        this.descriptionEng = descriptionEng;
    }

    public String getDescriptionSpa() {
        return descriptionSpa;
    }

    public void setDescriptionSpa(String descriptionSpa) {
        this.descriptionSpa = descriptionSpa;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
