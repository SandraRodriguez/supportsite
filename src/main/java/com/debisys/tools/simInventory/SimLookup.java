/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools.simInventory;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.SimReport;
import com.debisys.reports.TransactionReport;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.QueryReportData;
import com.emida.utils.dbUtils.TorqueHelper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import org.apache.log4j.Category;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class SimLookup {

    private static final Logger LOG = Logger.getLogger(SimLookup.class);

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.simInventory.sql");

    private SimLookup() {
    }

    private static String getQuery(QueryReportData reporInfo, Integer actionType) {
        StringBuilder select = new StringBuilder();
        StringBuilder where = new StringBuilder();
        select.append(" SELECT s.simNumber AS simNumber, ISNULL(s.productId,'0') AS simProductId, ");
        select.append(" ISNULL(p.description,'') AS simProductDescription, ISNULL(s.defaultSiteId,'0') AS defaultSiteId, ");
        select.append(" r.businessname AS isoName, s.isoId, m.dba AS merchantName, s.merchantId, b.batchId, ");
        select.append(" b.createDate AS creationDateBatch, st.descriptionEng+'*'+st.descriptionSpa AS statusCode ");

        StringBuilder body = new StringBuilder();
        body.append(" FROM SIMS s WITH(NOLOCK) ");
        body.append(" INNER JOIN simStatus     st WITH(NOLOCK) ON st.id= s.simStatusId ");
        body.append(" LEFT OUTER JOIN merchants m WITH(NOLOCK) ON m.merchant_id= s.merchantId ");
        body.append(" INNER JOIN reps           r WITH(NOLOCK) ON r.rep_id= s.isoId ");
        body.append(" INNER JOIN simBatches     b WITH(NOLOCK) ON s.simBatchId = b.id ");
        body.append(" LEFT OUTER JOIN products  p WITH(NOLOCK) ON p.id= s.productId ");

        if (reporInfo.getBatchId() != null) {
            where.append(" b.batchId = ").append(reporInfo.getBatchId()).append(" AND ");
        } else if (reporInfo.getIndividualSim() != null) {
            where.append(" s.simNumber='").append(reporInfo.getIndividualSim()).append("' AND ");
        } else if (reporInfo.getProducts() != null) {
            String[] data = reporInfo.getProducts().split(",");
            StringBuilder inData = new StringBuilder();
            for (String dataInfo : data) {
                inData.append(dataInfo).append(",");
            }
            String appendWhereData = inData.toString().substring(0, inData.toString().length() - 1);
            where.append(" p.id in (").append(appendWhereData).append(") AND ");
        } else if (reporInfo.getCarriers() != null) {
            if (reporInfo.getCarriers().equals("-1")) {
                body.append(" INNER JOIN SimCarrierMapping c WITH(NOLOCK) ON c.EmidaCarrierId = p.carrier_id ");
            } else {
                String[] data = reporInfo.getCarriers().split(",");
                StringBuilder inData = new StringBuilder();
                for (String dataInfo : data) {
                    inData.append("'").append(dataInfo).append("',");
                }
                String appendWhereData = inData.toString().substring(0, inData.toString().length() - 1);
                where.append(" p.carrierIdNew in (").append(appendWhereData).append(") AND ");
            }
        }
        if (reporInfo.getSimInfo() != null) {
            where.append(" s.simNumber like '").append(reporInfo.getSimInfo()).append("%' AND ");
        }
        if (reporInfo.getStatus() != null) {
            String[] data = reporInfo.getStatus().split(",");
            StringBuilder inData = new StringBuilder();
            for (String dataInfo : data) {
                inData.append("'").append(dataInfo).append("',");
            }
            String appendWhereData = inData.toString().substring(0, inData.toString().length() - 1);
            where.append(" s.simStatusId in (").append(appendWhereData).append(") AND ");
        }
        where.append(" s.simNumber IS NOT NULL ");
        body = body.append(" WHERE ").append(where);
        StringBuilder sb0 = new StringBuilder();
        if (actionType == 0) {
            sb0.append("SELECT * FROM (");
            sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(reporInfo.getSortFieldName()).append(" ").append(reporInfo.getSortOrder()).append(") AS RWN, * FROM (");
            sb0.append(select).append(body).append(" \n");
            sb0.append(") AS tbl1 ");
            sb0.append(") AS tbl2 ");
            sb0.append(" WHERE RWN BETWEEN ").append(reporInfo.getStartPage()).append(" AND ").append(reporInfo.getEndPage());
        } else if (actionType == 1) {
            sb0 = new StringBuilder(" SELECT COUNT(*) ");
            sb0.append(body);
        } else if (actionType == 2) {
            sb0 = new StringBuilder();
            sb0.append(select).append(body).append(" \n");
        }
        return sb0.toString();
    }

    public static Integer getCountSims(QueryReportData reporInfo) throws ReportException {
        Integer retValue = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 1);
            LOG.info("getting count sims sql [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = rs.getInt(1);
            }
        } catch (Exception e) {
            LOG.error("Error during getCountSims ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static List<SimPojo> getSims(QueryReportData reporInfo) {
        List<SimPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 0);
            LOG.info("getting sim sql [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimPojo sim = buildSimPojoByRs(rs);
                retValue.add(sim);
            }
        } catch (Exception e) {
            LOG.error("Error during getSims ", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    private static SimPojo buildSimPojoByRs(ResultSet rs) throws SQLException {
        SimPojo sim = new SimPojo();
        sim.setSimNumber(rs.getString("simNumber"));
        sim.setSimProductId(rs.getString("simProductId"));
        sim.setSimProductDescription(rs.getString("simProductDescription").trim());
        sim.setStatusCode(rs.getString("statusCode"));
        if (rs.getString("merchantId") != null) {
            sim.setMerchantId(rs.getString("merchantId"));
        } else {
            sim.setMerchantId("");
        }
        sim.setIsoId(rs.getString("isoId"));
        if (rs.getString("merchantName") != null) {
            sim.setMerchantName(rs.getString("merchantName"));
        } else {
            sim.setMerchantName("");
        }
        sim.setIsoName(rs.getString("isoName"));
        sim.setBatchId(rs.getString("batchId"));
        sim.setCreationDateBatch(rs.getString("creationDateBatch"));
        return sim;
    }

    public static List<SimPojo> downloadRows(QueryReportData reporInfo) {
        List<SimPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo, 2);
            LOG.info("getting sim sql for downloadRows [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimPojo sim = buildSimPojoByRs(rs);
                retValue.add(sim);
            }
        } catch (Exception e) {
            LOG.error("Error during downloadRows ", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static List<SimStatusPojo> findSimsStatus() {
        List<SimStatusPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = "SELECT s.id, s.descriptionEng, s.descriptionSpa, s.code FROM simStatus s WITH(NOLOCK) WHERE s.code NOT IN ('L', 'C') ";
            LOG.info("find status for sims [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                SimStatusPojo pojo = new SimStatusPojo(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
                retValue.add(pojo);
            }
        } catch (Exception e) {
            LOG.error("Error during findSimsStatus ", e);
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    private static void closeStatements(Connection dbConn, PreparedStatement pstmt, ResultSet rs) {
        try {
            Torque.closeConnection(dbConn);
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            LOG.error("Error during closeConnection", e);
        }
    }

    public static String generateFileName(ServletContext context) {
        boolean isUnique = false;
        String strFileName = "";
        while (!isUnique) {
            strFileName = generateRandomNumber();
            isUnique = checkFileName(strFileName, context);
        }
        return strFileName;
    }

    private static String generateRandomNumber() {
        StringBuffer s = new StringBuffer();
        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);

        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private static boolean checkFileName(String strFileName, ServletContext context) {
        boolean isUnique = true;
        String downloadPath = DebisysConfigListener.getDownloadPath(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);

        File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
        if (f.exists()) {
            // duplicate file found
            LOG.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
            return false;
        }

        f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
        if (f.exists()) {
            // duplicate file found
            LOG.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
            return false;
        }

        return isUnique;
    }

    public static String generateCsvReport(QueryReportData reporInfo, ServletContext context,
            SessionData sessionData, String headers) throws ReportException {
        String strFileName = "";
        String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);
        String downloadPath = DebisysConfigListener.getDownloadPath(context);

        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");

        LOG.debug("**********************************************");
        LOG.debug("downloadUrl=" + downloadUrl);
        LOG.debug("workingDir=" + workingDir);
        LOG.debug("filePrefix=" + filePrefix);
        LOG.debug("downloadPath=" + downloadPath);
        LOG.debug("strAccessLevel=" + strAccessLevel);
        LOG.debug("downloadUrl=" + strDistChainType);
        LOG.debug("**********************************************");
        try {
            strFileName = generateFileName(context);
            LOG.debug("generatedFileName=" + strFileName);
            BufferedWriter outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
            outputFile.flush();
            LOG.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");

            outputFile.write(headers);
            List<SimPojo> rows = SimLookup.downloadRows(reporInfo);
            writeValues(rows, outputFile, sessionData);
            outputFile.flush();
            outputFile.close();

            File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
            long size = sourceFile.length();
            byte[] buffer = new byte[(int) size];
            String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
            LOG.debug("Zip File Name: " + zipFileName);
            downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
            LOG.debug("Download URL: " + downloadUrl);
            ZipOutputStream out = null;
            FileInputStream in = null;
            try {
                out = new ZipOutputStream(new FileOutputStream(zipFileName));

                // Set the compression ratio
                out.setLevel(Deflater.DEFAULT_COMPRESSION);
                in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

                // Transfer bytes from the current file to the ZIP file
                // out.write(buffer, 0, in.read(buffer));
                int len;
                while ((len = in.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }

            } catch (Exception e) {

            } finally {
                if (out != null) {
                    out.closeEntry();
                }
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }
            sourceFile.delete();
        } catch (Exception e) {
            LOG.error("Error during generateCsvReport", e);
            throw new ReportException();
        } finally {
        }
        return downloadUrl;
    }

    private static void writeValues(List<SimPojo> rows, BufferedWriter outputFile, SessionData sessionData) throws IOException {
        if (outputFile != null) {
            for (SimPojo row : rows) {
                StringBuilder sb = new StringBuilder();
                sb.append("\"").append(row.getSimNumber()).append("\"").append(",");
                String[] statusCode = row.getStatusCode().split("\\*");
                String statusDesc = statusCode[1];
                if (sessionData.getLanguage().equals("english")) {
                    statusDesc = statusCode[0];
                }
                sb.append("\"").append(statusDesc).append("\"").append(",");
                sb.append("\"").append(row.getSimProductId()).append("\"").append(",");
                sb.append("\"").append(row.getSimProductDescription()).append("\"").append(",");
                sb.append("\"").append(row.getMerchantName()).append("\"").append(",");
                sb.append("\"").append(row.getMerchantId()).append("\"").append(",");
                sb.append("\"").append(row.getIsoName()).append("\"").append(",");
                sb.append("\"").append(row.getIsoId()).append("\"").append(",");
                sb.append("\"").append(row.getBatchId()).append("\"").append(",");
                sb.append("\"").append(row.getCreationDateBatch()).append("\"").append("\n");
                outputFile.write(sb.toString());
            }
        }
    }
}
