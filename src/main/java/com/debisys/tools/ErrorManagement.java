package com.debisys.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;

import com.debisys.exceptions.ErrorsException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DbUtil;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

public class ErrorManagement implements HttpSessionBindingListener
{
	private String criteria = "";
	private String sort = "";
	private String col = "";
	private String rep_id = "";
	private String providerId = "";
	private String errorCode = "";
	private String errorMessage = "";
	private Hashtable<String, String> validationErrors = new Hashtable<String, String>();

	private static Logger cat = Logger.getLogger(ErrorManagement.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");

	public String getCriteria()
	{
		return StringUtil.toString(this.criteria);
	}

	public void setCriteria(String strCriteria)
	{
		this.criteria = strCriteria.trim();
	}

	public String getCol()
	{
		return StringUtil.toString(this.col);
	}

	public void setCol(String strCol)
	{
		this.col = strCol;
	}

	public String getSort()
	{
		return StringUtil.toString(this.sort);
	}

	public void setSort(String strSort)
	{
		this.sort = strSort;
	}

	public String getRepId()
	{
		return StringUtil.toString(this.rep_id);
	}

	public void setRepId(String strValue)
	{
		try
		{
			Double.parseDouble(strValue);
		}
		catch (NumberFormatException nfe)
		{
			strValue = "";
		}
		this.rep_id = strValue;
	}

	public String getProviderId()
	{
		return StringUtil.toString(this.providerId);
	}

	public void setProviderId(String strProviderId)
	{
		this.providerId = strProviderId.trim();
	}

	public String getErrorCode()
	{
		return StringUtil.toString(this.errorCode);
	}

	public void setErrorCode(String strErrorCode)
	{
		this.errorCode = strErrorCode.trim();
	}

	public String getErrorMessage()
	{
		return StringUtil.toString(this.errorMessage);
	}

	public void setErrorMessage(String strErrorMessage)
	{
		this.errorMessage = strErrorMessage.trim();
	}

	public Hashtable<String, String> getErrors()
	{
		return this.validationErrors;
	}

	public boolean isError()
	{
		return (this.validationErrors.size() > 0);
	}

	public String getFieldError(String fieldname)
	{
		return this.validationErrors.get(fieldname);
	}

	public void addFieldError(String fieldname, String error)
	{
		this.validationErrors.put(fieldname, error);
	}

	public Vector<Object> searchErrorsByIso(int intPageNumber, int intRecordsPerPage, SessionData sessionData) throws ErrorsException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Object> vecCustomers = new Vector<Object>();
		try
		{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ErrorsException();
			}
			String strSQLWhere = "";
			String strIsoId = sessionData.getProperty("iso_id");
			String strSQL="";
			strSQL= "SELECT ipm.provider_id, ipm.provider_name, pec.provider_error_code, pem.provider_error_message FROM ISOProviderMapping ipm INNER JOIN ProviderErrorCodes pec ON ipm.provider_id = pec.provider_id AND ipm.iso_id = pec.iso_id INNER JOIN ProviderErrorMessages pem ON pec.provider_id = pem.provider_id AND pec.provider_error_code = pem.provider_error_code AND pec.iso_id = pem.iso_id  WHERE ipm.iso_id = ";
			strSQLWhere = strSQLWhere + strIsoId;
			if (this.criteria != null && !this.criteria.equals(""))
			{
				strSQLWhere = strSQLWhere + " AND (ipm.provider_name LIKE '%" + this.criteria.replaceAll("'", "''") + "%'  OR pec.provider_error_code LIKE '%" + this.criteria.replaceAll("'", "''") + "%')";
			}
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";
			try
			{
				if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
				{
					intCol = Integer.parseInt(this.col);
					intSort = Integer.parseInt(this.sort);
				}
			}
			catch (NumberFormatException nfe)
			{
				intCol = 0;
				intSort = 0;
			}
			if (intSort == 2)
			{
				strSort = "DESC";
			}
			else
			{
				strSort = "ASC";
			}
			switch (intCol)
			{
				case 1:
					strCol = "ipm.provider_id";
					break;
				case 2:
					strCol = "ipm.provider_name";
					break;
				case 3:
					strCol = "pec.provider_error_code";
					break;
				case 4:
					strCol = "pem.provider_error_message";
					break;
				default:
					strCol = "ipm.provider_id";
					break;
			}
			String sql = "";
			sql = strSQL + strSQLWhere + " ORDER BY " + strCol + " " + strSort;
			pstmt = dbConn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			cat.debug(sql);
			rs = pstmt.executeQuery();
			int intRecordCount = DbUtil.getRecordCount(rs);
			vecCustomers.add(Integer.valueOf(intRecordCount));
			if (intRecordCount > 0)
			{
				if (intRecordsPerPage==-1)
				{
					intRecordsPerPage = intRecordCount;
				}
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage,intRecordCount, intPageNumber));
				for (int i = 0; i < intRecordsPerPage; i++)
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("provider_id")));
					vecTemp.add(StringUtil.toString(rs.getString("provider_name")));
					vecTemp.add(StringUtil.toString(rs.getString("provider_error_code")));
					vecTemp.add(StringUtil.toString(rs.getString("provider_error_message")));
					vecCustomers.add(vecTemp);
					if ( !rs.next() )
					{
						break;
					}
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error during searchErrorsByIso", e);
			throw new ErrorsException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecCustomers;
	}

	public void deleteErrorCode(String providerId, String errorCode, SessionData sessionData) throws ErrorsException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		try
		{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ErrorsException();
			}
			String strIsoId = sessionData.getProperty("iso_id");
			pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteErrorMessage"));
			pstmt.setString(1, strIsoId);
			pstmt.setString(2, providerId);
			pstmt.setString(3, errorCode);
			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);

			pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteErrorCode"));
			pstmt.setString(1, strIsoId);
			pstmt.setString(2, providerId);
			pstmt.setString(3, errorCode);
			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);

			//cat.debug("Error code deleted: Iso: " + strIsoId + ". Provider Id: " + providerId + ". Error Code: " + errorCode + ".");
		}
		catch (Exception e)
		{
			cat.error("Error during deleteErrorCode", e);
			throw new ErrorsException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}

	public String getErrorMessage(String providerId, String errorCode, SessionData sessionData) throws ErrorsException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String message = "";
		try
		{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ErrorsException();
			}
			String strIsoId = sessionData.getProperty("iso_id");
			pstmt = dbConn.prepareStatement(sql_bundle.getString("getErrorMessage"));
			pstmt.setString(1, strIsoId);
			pstmt.setString(2, providerId);
			pstmt.setString(3, errorCode);
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				message = StringUtil.toString(rs.getString("Message"));
			}
			TorqueHelper.closeStatement(pstmt, rs);
		}
		catch (Exception e)
		{
			cat.error("Error during getErrorMessage", e);
			throw new ErrorsException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
		return message;
	}

	public void updateErrorMessage(String providerId, String errorCode, String errorMessage, SessionData sessionData) throws ErrorsException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		this.validationErrors.clear();
		try
		{
			if ((errorMessage == null) || (errorMessage.length() == 0))
			{
				this.addFieldError("errorMessage", Languages.getString("jsp.admin.tools.edit.update_error.error_message", sessionData.getLanguage()));
				this.setErrorMessage(errorMessage);
				return;
			}
			else if (errorMessage.length() > 140)
			{
				this.addFieldError("errorMessage", Languages.getString("jsp.admin.tools.edit.update_error.error_message_length", sessionData.getLanguage()));
				this.setErrorMessage(errorMessage);
				return;
			}

			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ErrorsException();
			}
			String strIsoId = sessionData.getProperty("iso_id");
			pstmt = dbConn.prepareStatement(sql_bundle.getString("updateErrorMessage"));
			pstmt.setString(1, errorMessage);
			pstmt.setString(2, strIsoId);
			pstmt.setString(3, providerId);
			pstmt.setString(4, errorCode);
			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);
		}
		catch (Exception e)
		{
			cat.error("Error during updateErrorMessage", e);
			throw new ErrorsException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}

	public static Vector<Vector<String>> getWildcards() throws ErrorsException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<String>> vecWildcards = new Vector<Vector<String>>();
		try
		{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ErrorsException();
			}
			String strQuery = "";
			strQuery = sql_bundle.getString("getWildcards");
			cat.debug(strQuery);
			pstmt = dbConn.prepareStatement(strQuery);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("wildcard"));
				vecTemp.add(rs.getString("description"));
				vecWildcards.add(vecTemp);
			}
			TorqueHelper.closeStatement(pstmt, rs);
		}
		catch (Exception e)
		{
			cat.error("Error during getWildcards", e);
			throw new ErrorsException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecWildcards;
	}

	public static Vector<Vector<String>> getProvidersByIso(SessionData sessionData) throws ErrorsException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<String>> vecProviders = new Vector<Vector<String>>();
		try
		{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ErrorsException();
			}
			String strIsoId = sessionData.getProperty("iso_id");
			String strQuery = "";
			strQuery = sql_bundle.getString("getProvidersByIso");
			pstmt = dbConn.prepareStatement(strQuery);
			pstmt.setString(1, strIsoId);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("providerId"));
				vecTemp.add(rs.getString("providerName"));
				vecProviders.add(vecTemp);
			}
			TorqueHelper.closeStatement(pstmt, rs);
		}
		catch (Exception e)
		{
			cat.error("Error during getProvidersByIso", e);
			throw new ErrorsException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecProviders;
	}

	public void insertError(String providerId, String errorCode, String errorMessage, SessionData sessionData) throws ErrorsException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		this.validationErrors.clear();
		try
		{
			if ((providerId == null) || (providerId.length() == 0))
			{
				this.addFieldError("provider", Languages.getString("jsp.admin.tools.edit.add_error.provider", sessionData.getLanguage()));
				this.setProviderId(providerId);
				this.setErrorCode(errorCode);
				this.setErrorMessage(errorMessage);
				return;
			}
			else if (!isNumeric(providerId))
			{
				this.addFieldError("provider", Languages.getString("jsp.admin.tools.edit.add_error.provider_inv", sessionData.getLanguage()));
				this.setProviderId(providerId);
				this.setErrorCode(errorCode);
				this.setErrorMessage(errorMessage);
				return;
			}

			if ((errorCode == null) || (errorCode.length() == 0))
			{
				this.addFieldError("errorCode", Languages.getString("jsp.admin.tools.edit.add_error.error_code", sessionData.getLanguage()));
				this.setProviderId(providerId);
				this.setErrorCode(errorCode);
				this.setErrorMessage(errorMessage);
				return;
			}
			else if (errorCode.length() > 30)
			{
				this.addFieldError("errorCode", Languages.getString("jsp.admin.tools.edit.add_error.error_code_length", sessionData.getLanguage()));
				this.setProviderId(providerId);
				this.setErrorCode(errorCode);
				this.setErrorMessage(errorMessage);
				return;
			}

			if ((errorMessage == null) || (errorMessage.length() == 0))
			{
				this.addFieldError("errorMessage", Languages.getString("jsp.admin.tools.edit.add_error.error_message", sessionData.getLanguage()));
				this.setProviderId(providerId);
				this.setErrorCode(errorCode);
				this.setErrorMessage(errorMessage);
				return;
			}
			else if (errorMessage.length() > 140)
			{
				this.addFieldError("errorMessage", Languages.getString("jsp.admin.tools.edit.add_error.error_message_length", sessionData.getLanguage()));
				this.setProviderId(providerId);
				this.setErrorCode(errorCode);
				this.setErrorMessage(errorMessage);
				return;
			}

			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ErrorsException();
			}
			String strIsoId = sessionData.getProperty("iso_id");
			
			pstmt = dbConn.prepareStatement(sql_bundle.getString("getErrorMessage"));
			pstmt.setString(1, strIsoId);
			pstmt.setString(2, providerId);
			pstmt.setString(3, errorCode);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				this.addFieldError("errorCode", Languages.getString("jsp.admin.tools.edit.add_error.error_code_duplicated", sessionData.getLanguage()));
				this.setProviderId(providerId);
				this.setErrorCode(errorCode);
				this.setErrorMessage(errorMessage);
				return;
			}			
			TorqueHelper.closeStatement(pstmt, rs);
			
			pstmt = dbConn.prepareStatement(sql_bundle.getString("insertErrorCode"));
			pstmt.setString(1, strIsoId);
			pstmt.setString(2, providerId);
			pstmt.setString(3, errorCode);
			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);

			pstmt = dbConn.prepareStatement(sql_bundle.getString("insertErrorMessage"));
			pstmt.setString(1, strIsoId);
			pstmt.setString(2, providerId);
			pstmt.setString(3, errorCode);
			pstmt.setString(4, errorMessage);
			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);
		}
		catch (Exception e)
		{
			cat.error("Error during insertError", e);
			throw new ErrorsException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}


	public void valueBound(HttpSessionBindingEvent event)
	{
	}

	public void valueUnbound(HttpSessionBindingEvent event)
	{
	}

	public static boolean isNumeric(String str)
	{
		try
		{
			Integer.parseInt(str);
		}
		catch(NumberFormatException nfe)
		{
			return false;
		}
		return true;
  }
}

