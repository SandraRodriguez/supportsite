package com.debisys.tools;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.users.SessionData;
import com.debisys.utils.JSONException;
import com.debisys.utils.JSONObject;


public class CrossBorderExchangeRate
{
	// log4j logging
	public static Logger cat =  Logger.getLogger(CrossBorderExchangeRate.class);

	// SQL
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");

	public static Map currencyCountry= new HashMap();
	String isoCountry="";
	
	
	/**
	 * This gets crossborderexchange rate data for a given id.
	 * @return
	 * @throws ReportException
	 */
	public Vector getEditExchangeRate(String id, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getEditExchangeRate");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	String isoId = sessionData.getProperty("iso_id");
	    	pstmt.setBigDecimal(1, new BigDecimal(isoId));
	    	pstmt.setInt(2,Integer.parseInt(id));
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getString("countryName"));
	      		vecTemp.add(rs.getInt("product_id"));
	      		vecTemp.add(rs.getString("origin_currency_code"));
	      		vecTemp.add(rs.getString("destination_currency_code"));
	      		vecTemp.add(rs.getDouble("rate"));
	      		vecTemp.add(rs.getDouble("flat_fee"));
	      		vecTemp.add(rs.getDouble("percentage_fee"));
	      		vecTemp.add(rs.getInt("originId"));
	      		vecTemp.add(rs.getInt("destinationId"));
	      		vecTemp.add(rs.getString("countryName"));
	      		vecTemp.add(rs.getInt("id"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("getEditExchangeRate", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}
	
	
	
	/**
	 * This gets country details for a given ISO.
	 * @return
	 * @throws ReportException
	 */
	public Vector getISOCountry(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    
	    String strSQL = sql_bundle.getString("getISOCountry");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	String isoId = sessionData.getProperty("iso_id");
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();
	      	while(rs.next())
	      	{
	      		
		      		Vector vecTemp = new Vector();
		      		vecTemp.add(rs.getString("businessname").trim());
		      		vecTemp.add(rs.getString("country").trim());
		      		vecTemp.add(rs.getString("currency").trim());
		      		vecTemp.add(rs.getString("countryId"));
		      		vecResults.add(vecTemp);
		
	      	}
	      	rs.close();
	      	pstmt.close();
	      	
	      	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getISOCountry", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/**
	 * This will insert a crossborder exchange rate under the ID of the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public int insertXborderExchangeRate(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		int result=1;
		Connection dbConn = null;
	    String strSQL = sql_bundle.getString("insertXborderExchangeRate");
	    String strSQL1 = sql_bundle.getString("checkInsert");
	    PreparedStatement pstmt = null;
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL1);

	    	String IsoID = sessionData.getProperty("iso_id");
	    	String originCountryId = request.getParameter("origCountry");
	    	String originCurrency = request.getParameter("originCurrency");
	    	String destinationCurrency = request.getParameter("destinationCurrency");
	    	String destinationCountry = request.getParameter("xborderCountries");
	    	String products = request.getParameter("products");
	    	String rate=request.getParameter("rate");
	    	String flatFee=request.getParameter("flatFee");
	    	String  percentageFee=request.getParameter("percentageFee");
	    	pstmt = dbConn.prepareStatement(strSQL1);
	    	pstmt.setLong(1, Long.parseLong(IsoID));
	    	pstmt.setInt(2, Integer.parseInt(originCountryId));
	    	pstmt.setInt(3, Integer.parseInt(destinationCountry));
	    	pstmt.setInt(4, Integer.parseInt(products));
	    	ResultSet rs = pstmt.executeQuery();
	    	while (rs.next())
	      	{
	    	  result=0;
	    	  
	      	}
	    	if(result==1){
	    		cat.debug(strSQL);
		    	pstmt = dbConn.prepareStatement(strSQL);
		    	pstmt.setLong(1, Long.parseLong(IsoID));
		    	pstmt.setInt(2, Integer.parseInt(originCountryId));
		    	pstmt.setInt(3, Integer.parseInt(destinationCountry));
		    	pstmt.setInt(4, Integer.parseInt(products));
		    	pstmt.setString(5, originCurrency);
		    	pstmt.setString(6, destinationCurrency);
		    	pstmt.setDouble(7, Double.parseDouble(rate));
		    	pstmt.setDouble(8, Double.parseDouble(flatFee));
		    	pstmt.setDouble(9, Double.parseDouble(percentageFee));
		    	pstmt.executeUpdate();
		    	cat.info("Route added by --"+IsoID);
		    	
	    	}
	    	rs.close();
	    	pstmt.close();
	    	
	    	return result;
	    }
	    catch (Exception e)
	    {
	    	
	    	cat.error("Error during insertXborderExchangeRate", e);
	    	throw new ReportException();
	    	
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    		
	    	}
	    }
	   
	}

	/***
	 * This will be used to update a crossborder exchange rate entry using the ID of the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void updateXborderExchangeRate(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("updateXborderExchangeRate");

    	cat.debug("Updating CrossBorderExchange rate --");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	String IsoID, oldRate, oldFlatFee, oldPercentageFee, newRate,newFlatFee,newPercentageFee,id;

	    	IsoID = sessionData.getProperty("iso_id");
	    	oldRate = request.getParameter("oldRate");
	    	oldFlatFee = request.getParameter("oldFlatFee");
	    	oldPercentageFee = request.getParameter("oldPercentageFee");
	    	newRate = request.getParameter("rate");
	    	newFlatFee = request.getParameter("flatFee");
	    	newPercentageFee = request.getParameter("percentageFee");
	    	id = request.getParameter("id");
	    	cat.info("Values before update by user "+sessionData.getProperty("username")+" --are rate:"+oldRate+" , flatFee:"+oldFlatFee+" and PercentageFee:"+oldPercentageFee);
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setDouble(1, Double.parseDouble(newRate));
	    	pstmt.setDouble(2,Double.parseDouble(newFlatFee));
	    	pstmt.setDouble(3, Double.parseDouble(newPercentageFee));
	    	pstmt.setLong(4, Long.parseLong(IsoID));
	    	pstmt.setInt(5, Integer.parseInt(id));
	    	pstmt.executeUpdate();
	    	cat.info("Values after update by user "+sessionData.getProperty("username")+" --are rate:"+newRate+" , flatFee:"+newFlatFee+" and PercentageFee:"+newPercentageFee);
	    	
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during updateXborderExchangeRate", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}

	/***
	 * This gets all the counties in system
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getCountries(SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getCountries");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("countryId"));
	      		vecTemp.add(rs.getString("countryName"));
	      		vecTemp.add(rs.getString("currency"));
	      		currencyCountry.put(rs.getString("countryId"), rs.getString("currency"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getCountries", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/***
	 * This gets all the cross border products part of ISO rateplan of ISO logged in 
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getCrossborderProducts(SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getCrossborderProducts");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	String isoId = sessionData.getProperty("iso_id");
	    	pstmt.setString(1, isoId);
                pstmt.setString(2, isoId);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("id"));
	      		vecTemp.add(rs.getString("description"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getCrossborderProducts", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}
	
	/***
	 * This will get all of crossborderexchange rate entries for the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getXborderExchangeRates(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getXborderExchangeRates");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	String isoId = sessionData.getProperty("iso_id");
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getString("countryName"));
	      		vecTemp.add(rs.getInt("product_id"));
	      		vecTemp.add(rs.getString("origin_currency_code"));
	      		vecTemp.add(rs.getString("destination_currency_code"));
	      		vecTemp.add(rs.getDouble("rate"));
	      		vecTemp.add(rs.getDouble("flat_fee"));
	      		vecTemp.add(rs.getDouble("percentage_fee"));
	      		vecTemp.add(rs.getInt("originId"));
	      		vecTemp.add(rs.getInt("destinationId"));
	      		vecTemp.add(rs.getString("countryName"));
	      		vecTemp.add(rs.getInt("id"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getXborderExchangeRates", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}


	
public static JSONObject getDestinationCurrency(String type, SessionData sessionData) throws JSONException {
		JSONObject json = new JSONObject();
			try
			{
				Vector jsonVec = new Vector();

			String currency= (String)CrossBorderExchangeRate.currencyCountry.get(type);
			json.put("currency", currency);
			}
			catch (Exception e) {
				cat.error("Error during getDestinationCurrency", e);
				
			}

		return json;
	}
	
	

	
	/***
	 * This will delete crossborderExchangeRate using the ID of the ISO currently logged on
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void deleteXborderExchangeRate(HttpServletRequest request,SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	   String strSQL = sql_bundle.getString("deleteXborderExchangeRate");
	   String strSQL1=sql_bundle.getString("getEditExchangeRate");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL1);
	    	String isoId = sessionData.getProperty("iso_id");
	    	String id=request.getParameter("delId");
	    	pstmt = dbConn.prepareStatement(strSQL1);
	    	pstmt.setBigDecimal(1, new BigDecimal(isoId));
	    	pstmt.setInt(2,Integer.parseInt(id));
	    	ResultSet rs = pstmt.executeQuery();
	    	while (rs.next())
	      	{
	    	cat.info("Before deletion the values are Product: "+rs.getInt("product_id")+" destinationcode:"+rs.getString("destination_currency_code")+ "-- rate"+rs.getDouble("rate")+ "--flatFee:"+rs.getDouble("flat_fee")+"percentFee--"+rs.getDouble("percentage_fee")+"DestinationCountryId:--"+rs.getInt("destinationId"));
	      	}
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	cat.debug(strSQL);
	    	pstmt.setBigDecimal(1, new BigDecimal(isoId));
	    	pstmt.setInt(2,Integer.parseInt(id));
	    	pstmt.executeUpdate();
	    	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during deleteXborderExchangeRate", e);
	    	throw new ReportException();
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}

	
}
