package com.debisys.tools;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;

/***
 * All of the Java code used to process Bonus Thresholds and Bonus Threshold SMS Messages.
 * 
 * DBSY-568
 * @author swright
 *
 */
public class PhoneRewards
{
	// log4j logging
	public static Category cat = Category.getInstance(PhoneRewards.class);
	public String PhoneRewardsErrorCode;
	public String PhoneRewardsAmount;
	public String percentageset;
	
	
	// SQL
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");
	
	/**
	 * This gets all of the current providers in the system.
	 * @return Vector of all providers containing provider ID and name
	 * @throws ReportException
	 * @author cchabtini
	 */
	public Vector getProviders() throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getProviders");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("provider_id"));
	      		vecTemp.add(rs.getString("name"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholds", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/**
	 * This will insert an SMS Message under the ID of the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public int insertBonusThresholdSMS(HttpServletRequest request, SessionData sessionData) throws ReportException {
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("insertPhoneRewardsSMS");
		int result;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}

			cat.debug(strSQL);

			String IsoID = sessionData.getProperty("iso_id");
			String provider = request.getParameter("newProvider");
			String message = request.getParameter("msg");

			if (IsoID == null || provider == null || message == null)
				return 0;

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(IsoID));
			cstmt.setInt(2, Integer.parseInt(provider));
			cstmt.setString(3, message);
			cstmt.registerOutParameter(4, Types.INTEGER);
			cstmt.setInt(4, 0);
			cstmt.execute();

			result = cstmt.getInt(4);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during insertBonusThresholdSMS", e);
			throw new ReportException();
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
				result = 0;
			}
		}
		return result;
	}
	

	/***
	 * This will be used to get the Percentage Value
	 * @return Integer representing Percentage value
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 * @author cchabtini
	 */
	public static double getPercentage()
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    double per = 0;
	    String strSQL = sql_bundle.getString("getPercentage");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();
	      	while (rs.next())
	      	{
	      	per = rs.getDouble("Percentage");
	      	}
	      		
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholds", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return per;
	}
	
	
	/***
	 * This will be used to reset user account
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void resetaccountid(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
		CallableStatement cst = null;
	    String strSQL = sql_bundle.getString("resetaccountid");
    	cat.debug("Updating Percentage");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}
	    	cat.debug(strSQL + "using " + request.getParameter("resetid"));
	    	cst = dbConn.prepareCall(strSQL);
	    	cst.setString(1,  request.getParameter("resetid") );		
	    	cst.registerOutParameter(2, Types.INTEGER);
	    	cst.setInt(2, 0);		
	    	cst.registerOutParameter(3, Types.DOUBLE);
	    	cst.setDouble(3, 0);
	    	cst.execute();
	    	this.PhoneRewardsErrorCode = String.valueOf(cst.getInt(2));
	    	cat.debug("returndouble="+cst.getDouble(3));
	    	this.PhoneRewardsAmount = String.valueOf(cst.getDouble(3));
	    	cst.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during ", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		TorqueHelper.closeConnection(dbConn, cst, null);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}

	public String getPhoneRewardsErrorCode(){
		return this.PhoneRewardsErrorCode;
	}

	public String getPercentageSet(){
		return this.percentageset;
	}
	
	public String getPhoneRewardsAmount(){
		return this.PhoneRewardsAmount;
	}
	
	/***
	 * This will be used to update the Percentage Value
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 * @author cchabtini
	 */
	public void updatePercentage(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("updatePercentage");

    	cat.debug("Updating Percentage");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}
	    	cat.debug(strSQL);
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	this.percentageset = request.getParameter("percentage");
	    	pstmt.setString(1, this.percentageset );
	    	pstmt.executeUpdate();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during ", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}
	

	/***
	 * This will be used to get the Promotion Status 
	 * @return String representing which promotion is enabled
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 * @author cchabtini
	 */
	public String getPromotionStatus(SessionData sessionData)
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    int enable = 0;
	    String strSQL = sql_bundle.getString("getStatus");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();
	      	while(rs.next())
	      	{
	      	enable = rs.getInt("Enabled");
	      	}	
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholds", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    if (enable==1){
	    	 return Languages.getString("jsp.admin.tools.bonusphonerewards.status.case1", sessionData.getLanguage());
	    }
	    else if (enable==2)
	    	 return Languages.getString("jsp.admin.tools.bonusphonerewards.status.case2", sessionData.getLanguage());
	    else
	    	 return Languages.getString("jsp.admin.tools.bonusphonerewards.status.case3", sessionData.getLanguage());
	}

	/***
	 * This will be used to get the Percentage Value
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public int getPromotion()
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    int enable = 0;
	    String strSQL = sql_bundle.getString("getStatus");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();
	      	while (rs.next())
	      	{	      	enable = rs.getInt("Enabled");
	      	
	      	}
	      		
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholds", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }

	    return enable;
	}
	
	/***
	 * This will be used to update the Percentage Value
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void updatePromotionStatus(HttpServletRequest request, SessionData sessionData, int value) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("updateStatus");

    	cat.debug("Updating Percentage");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}
	    	cat.debug(strSQL);
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setInt(1,value);
	    	pstmt.executeUpdate();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during ", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}
	
	/***
	 * This will be used to update a specific SMS Messages using the ID os the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void delete(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("deletePhoneRewardsSMS");

    	cat.debug("Updating bonus threshold SMS msg");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);

	    	String IsoID, deleteProvider, deletemsgName;

	    	IsoID = sessionData.getProperty("iso_id");
	    	deleteProvider = request.getParameter("deleteProvider");

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setLong(1, Long.parseLong(IsoID));
	    	pstmt.setInt(2, Integer.parseInt(deleteProvider));
	    	pstmt.executeUpdate();

	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during updateBonusThresholdSMS", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}
	
	
	
	/***
	 * This will be used to update a specific SMS Messages using the ID os the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @throws ReportException
	 */
	public void updateBonusThresholdSMS(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("updatePhoneRewardsSMS");

    	cat.debug("Updating bonus threshold SMS msg");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);

	    	String IsoID, oldProvider, newProvider, msg;

	    	IsoID = sessionData.getProperty("iso_id");
    		oldProvider = request.getParameter("oldProvider");
    		newProvider = request.getParameter("newProvider");
    		msg = request.getParameter("msg");

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, msg);
	    	pstmt.setInt(2, Integer.parseInt(newProvider));
	    	pstmt.setLong(3, Long.parseLong(IsoID));
	    	pstmt.setInt(4, Integer.parseInt(oldProvider));
	    	pstmt.executeUpdate();

	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during updateBonusThresholdSMS", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	}
	
	
	
	/***
	 * This query is to get a specific SMS Message using the ID of the ISO currently logged on. 
	 * Once we have the message, we can edit it, and then save it back.
	 * @param providerId
	 * @param name
	 * @param sessionData
	 * @return Vector containing all of the SMS message components
	 * @throws ReportException
	 */
	public Vector getBonusThresholdSMS(String providerId, SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getPhoneRewardsSMS2");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);
	    	String IsoID = sessionData.getProperty("iso_id");
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setLong(1, Long.parseLong(IsoID));
	    	pstmt.setInt(2, Integer.parseInt(providerId));
	      	ResultSet rs = pstmt.executeQuery();
	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("provider_id"));
	      		vecTemp.add(rs.getString("message"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholdSMS", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	/***
	 * This will get all of the bonus threshold SMS Messages for the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getBonusThresholdSMS(HttpServletRequest request, SessionData sessionData) throws ReportException
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getPhoneRewardsSMS1");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	cat.debug(strSQL);

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	String isoId = sessionData.getProperty("iso_id");
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getInt("provider_id"));
	      		vecTemp.add(rs.getString("message"));
//	      		cat.debug("provider: " + rs.getInt("provider_id") + ", name: " +  rs.getString("name"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during getBonusThresholdSMS", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection", e);
	    	}
	    }
	    return vecResults;
	}

	

	
}
