
package com.debisys.tools.isoLogin;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 *
 * @author dgarzon
 */
public class IsoLoginVo implements Comparable<IsoLoginVo>{
    
    private String id;
    private BigDecimal isoId;
    private String isoBusinessName;
    private String userId;
    private String password;
    private BigDecimal lastLoginAttemptNum;
    private Timestamp lastLoginAttemptDate;
    private Timestamp lastLoginSuccessDate;
    private String token;
    private BigDecimal defaultRepId;
    private String defaultRepBusinessName;

    public IsoLoginVo() {
    }
    
    public IsoLoginVo(BigDecimal isoId, String userId, String password, String token, BigDecimal defaultRepId) {
        this.isoId = isoId;
        this.userId = userId;
        this.password = password;
        this.lastLoginAttemptNum = new BigDecimal(BigInteger.ZERO);
        this.token = token;
        this.defaultRepId = defaultRepId;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getIsoId() {
        return isoId;
    }

    public void setIsoId(BigDecimal isoId) {
        this.isoId = isoId;
    }

    public String getIsoBusinessName() {
        return isoBusinessName;
    }

    public void setIsoBusinessName(String isoBusinessName) {
        this.isoBusinessName = isoBusinessName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getLastLoginAttemptNum() {
        return lastLoginAttemptNum;
    }

    public void setLastLoginAttemptNum(BigDecimal lastLoginAttemptNum) {
        this.lastLoginAttemptNum = lastLoginAttemptNum;
    }

    public Timestamp getLastLoginAttemptDate() {
        return lastLoginAttemptDate;
    }

    public void setLastLoginAttemptDate(Timestamp lastLoginAttemptDate) {
        this.lastLoginAttemptDate = lastLoginAttemptDate;
    }

    public Timestamp getLastLoginSuccessDate() {
        return lastLoginSuccessDate;
    }

    public void setLastLoginSuccessDate(Timestamp lastLoginSuccessDate) {
        this.lastLoginSuccessDate = lastLoginSuccessDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public BigDecimal getDefaultRepId() {
        return defaultRepId;
    }

    public void setDefaultRepId(BigDecimal defaultRepId) {
        this.defaultRepId = defaultRepId;
    }

    public String getDefaultRepBusinessName() {
        return defaultRepBusinessName;
    }

    public void setDefaultRepBusinessName(String defaultRepBusinessName) {
        this.defaultRepBusinessName = defaultRepBusinessName;
    }

    @Override
    public int compareTo(IsoLoginVo o) {
        return this.isoBusinessName.toUpperCase().compareTo(o.isoBusinessName.toUpperCase());
    }
    
    
}
