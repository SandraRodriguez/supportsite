package com.debisys.tools.isoLogin;

import com.debisys.exceptions.TransactionException;
import com.debisys.transactions.TransactionSearch;
import com.debisys.users.SessionData;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class IsoLoginConfiguration {

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.isoLogin.sql");
    static Category cat = Category.getInstance(IsoLoginConfiguration.class);

    public static List<IsoLoginVo> getIsoLoginUsersList() throws Exception {
        List<IsoLoginVo> list = new ArrayList<IsoLoginVo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("selectAllIsoLogin");

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                IsoLoginVo isoLogin = new IsoLoginVo(rs.getBigDecimal("isoId"), rs.getString("userId"), rs.getString("password"), rs.getString("token"), rs.getBigDecimal("defaultRep"));
                isoLogin.setId(rs.getString("id"));
                isoLogin.setIsoBusinessName(getIsoBusinessName(rs.getBigDecimal("isoId")));
                isoLogin.setDefaultRepBusinessName(StringUtil.toString(getIsoBusinessName(rs.getBigDecimal("defaultRep"))));
                list.add(isoLogin);
            }
        } catch (Exception e) {
            cat.error("Error during getIsoLoginUsersList", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        Collections.sort(list);
        return list;
    }

    public static String getIsoBusinessName(BigDecimal isoId) throws Exception {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("selectIsoBusinessName");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setBigDecimal(1, isoId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getString("businessname");
            }
        } catch (Exception e) {
            cat.error("Error during getIsoBusinessName", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static IsoLoginVo getIsoLoginByPK(String id) throws Exception {
        String sql = sql_bundle.getString("selectIsoLoginByPKId");
        return getIsoLoginByUserId(id, sql);
    }

    public static IsoLoginVo getIsoLoginByUserId(String userId) throws Exception {
        String sql = sql_bundle.getString("selectIsoLoginByUserId");
        return getIsoLoginByUserId(userId, sql);
    }

    private static IsoLoginVo getIsoLoginByUserId(String userId, String sql) throws Exception {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                IsoLoginVo isoLogin = new IsoLoginVo(rs.getBigDecimal("isoId"), rs.getString("userId"), rs.getString("password"), rs.getString("token"), rs.getBigDecimal("defaultRep"));
                isoLogin.setId(rs.getString("id"));
                isoLogin.setIsoBusinessName(getIsoBusinessName(rs.getBigDecimal("isoId")));
                isoLogin.setDefaultRepBusinessName(StringUtil.toString(getIsoBusinessName(rs.getBigDecimal("defaultRep"))));
                return isoLogin;
            }
        } catch (Exception e) {
            cat.error("Error during getIsoLoginByUserId", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    public static boolean insertIsoLogin(SessionData sessionData, IsoLoginVo isoLogin) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            isoLogin.setId(uuid);
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("isoLoginInsert");
            cat.debug("SQL IsoLogin" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, uuid);
            pst.setBigDecimal(2, isoLogin.getIsoId());
            pst.setString(3, isoLogin.getUserId());
            pst.setString(4, isoLogin.getPassword());
            pst.setBigDecimal(5, BigDecimal.ZERO);
            pst.setNull(6, Types.DATE);
            pst.setNull(7, Types.DATE);
            pst.setString(8, generateSHA1(isoLogin.getIsoId().toString()));
            pst.setBigDecimal(9, isoLogin.getDefaultRepId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            cat.error("Error insertIsoLogin", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
        return false;
    }

    public static boolean updateIsoLogin(IsoLoginVo isoLogin) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("updateIsoLogin");
            cat.debug("SQL updateIsoLogin" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setBigDecimal(1, isoLogin.getIsoId());
            pst.setString(2, isoLogin.getUserId());
            pst.setString(3, isoLogin.getPassword());
            pst.setString(4, generateSHA1(isoLogin.getIsoId().toString()));
            pst.setBigDecimal(5, isoLogin.getDefaultRepId());
            pst.setString(6, isoLogin.getId());
            pst.executeUpdate();
            return true;
        } catch (Exception e) {
            cat.error("Error updateIsoLogin", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
        return false;
    }

    public static void deleteIsoLoginById(String id) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("isoLoginDelete");
            cat.debug("SQL isoLoginDelete " + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, id);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error isoLoginDelete", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static Vector getIsoList(BigDecimal isoInclude) throws TransactionException {
        Vector vecResultList = new Vector();
        Connection dbConn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new TransactionException();
            }
            String isoList = "";
            List<IsoLoginVo> isoLoginUsersList = getIsoLoginUsersList();
            for (IsoLoginVo isoLogin : isoLoginUsersList) {
                if(isoInclude == null || !isoLogin.getIsoId().toString().equalsIgnoreCase(isoInclude.toString())){
                    isoList += isoLogin.getIsoId()+",";
                }
            }
            if(!isoList.isEmpty()){
                isoList = "AND rep_id NOT IN ("+isoList.substring(0, isoList.length()-1)+")";
            }
            
            String strSQLGetIsoList = "SELECT rep_id, businessname FROM reps WITH (nolock) WHERE  type in (2,3) "+isoList+" ORDER BY businessname";
            cat.debug(strSQLGetIsoList);
            pstmt = dbConn.prepareStatement(strSQLGetIsoList);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
                vecTemp.add(StringUtil.toString(rs.getString("businessname")));
                vecResultList.add(vecTemp);
            }
        } catch (Exception e) {
            cat.error("Error during getIsoList", e);
            throw new TransactionException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecResultList;
    }
    
    public static Vector getRepListByIso(String iso) throws TransactionException {
        Vector vecResultList = new Vector();
        Connection dbConn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new TransactionException();
            }
            
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT r.rep_id, r.businessname FROM reps r WITH (nolock) ");
            sb.append("INNER JOIN reps s WITH (nolock) ON (r.iso_id = s.rep_id) ");
            sb.append("INNER JOIN reps a WITH (nolock) ON (s.iso_id = a.rep_id) ");
            sb.append("INNER JOIN reps i WITH (nolock) ON (a.iso_id = i.rep_id) ");
            sb.append("WHERE r.type = 1 AND i.rep_id = ? ORDER BY businessname");
            
            cat.debug(sb.toString());
            pstmt = dbConn.prepareStatement(sb.toString());
            pstmt.setBigDecimal(1, new BigDecimal(iso));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
                vecTemp.add(StringUtil.toString(rs.getString("businessname")));
                vecResultList.add(vecTemp);
            }
        } catch (Exception e) {
            cat.error("Error during getRepListByIso", e);
            throw new TransactionException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecResultList;
    }
    
    public static String generateSHA1(String iso) {
        return Base64.encodeBase64String(DigestUtils.sha(iso)).toUpperCase();
    }

}
