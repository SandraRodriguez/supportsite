
package com.debisys.tools.stockLevels;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class MerchantsStockLevelsPojo {

    private String id;
    private String description;
    private BigDecimal isoId;
    private String lowColor;
    private String mediumColor;
    private String highColor;
    private double lowMinValue;
    private double lowMaxValue;
    private double mediumMinValue;
    private double mediumMaxValue;
    private double highMinValue;
    private double highMaxValue;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getIsoId() {
        return isoId;
    }

    public void setIsoId(BigDecimal isoId) {
        this.isoId = isoId;
    }

    public String getLowColor() {
        return lowColor;
    }

    public void setLowColor(String lowColor) {
        this.lowColor = lowColor;
    }

    public String getMediumColor() {
        return mediumColor;
    }

    public void setMediumColor(String mediumColor) {
        this.mediumColor = mediumColor;
    }

    public String getHighColor() {
        return highColor;
    }

    public void setHighColor(String highColor) {
        this.highColor = highColor;
    }

    public double getLowMinValue() {
        return lowMinValue;
    }

    public void setLowMinValue(double lowMinValue) {
        this.lowMinValue = lowMinValue;
    }

    public double getLowMaxValue() {
        return lowMaxValue;
    }

    public void setLowMaxValue(double lowMaxValue) {
        this.lowMaxValue = lowMaxValue;
    }

    public double getMediumMinValue() {
        return mediumMinValue;
    }

    public void setMediumMinValue(double mediumMinValue) {
        this.mediumMinValue = mediumMinValue;
    }

    public double getMediumMaxValue() {
        return mediumMaxValue;
    }

    public void setMediumMaxValue(double mediumMaxValue) {
        this.mediumMaxValue = mediumMaxValue;
    }

    public double getHighMinValue() {
        return highMinValue;
    }

    public void setHighMinValue(double highMinValue) {
        this.highMinValue = highMinValue;
    }

    public double getHighMaxValue() {
        return highMaxValue;
    }

    public void setHighMaxValue(double highMaxValue) {
        this.highMaxValue = highMaxValue;
    }

    
}
