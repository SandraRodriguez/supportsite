package com.debisys.tools.stockLevels;

import com.debisys.exceptions.ReportException;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.dbUtils.TorqueHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class MerchantStockLevels {

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.stockLevels.sql");

    static Category cat = Category.getInstance(MerchantStockLevels.class);

    public static List<String> verifyDuplicateMerchants(String merchantArray[]) throws ReportException {
        List<String> listDuplicateMerchants = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql = "";
        try {
            if (merchantArray == null || merchantArray.length == 0) {
                listDuplicateMerchants.add("NOTHING,NOTHING");
                return listDuplicateMerchants;
            }
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));

            sql = "SELECT d.*, m.dba "
                    + "FROM MerchantsStockLevels ms with(nolock) "
                    + "INNER JOIN MerchantsStockLevelsDetail d with(nolock) ON d.IdMerchantsStockLevels = ms.id "
                    + "INNER JOIN merchants m WITH(NOLOCK) ON m.merchant_id = d.merchant_id "
                    + "WHERE d.merchant_id IN (";

            String where = "";
            for (int i = 0; i < merchantArray.length; i++) {
                if (!merchantArray[i].trim().equals("")) {
                    where += "'" + merchantArray[i] + "'";
                    if (i < merchantArray.length - 1) {
                        where += ",";
                    }
                }
            }

            if (where.endsWith(",")) {
                where = where.substring(0, where.length() - 1);
            }

            sql += where + ")";

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            boolean existData = false;
            while (rs.next()) {

                existData = true;
                listDuplicateMerchants.add(rs.getString("merchant_id") + "," + rs.getString("dba"));
            }
            if (!existData) {
                listDuplicateMerchants.add("NOTHING,NOTHING");
            }
        } catch (Exception e) {
            e.printStackTrace();
            cat.debug("verifyDuplicateMerchants Preparing statement for query: " + sql);
            cat.error("Error during verifyDuplicateMerchants", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return listDuplicateMerchants;
    }

    public static List<String> getMerchantBussinessName(String merchantArray[]) throws ReportException {
        List<String> listMerchants = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql = "";
        try {
            if (merchantArray == null || merchantArray.length == 0) {
                listMerchants.add("NOTHING,NOTHING");
                return listMerchants;
            }
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));

            sql = "SELECT merchant_id, dba FROM merchants m WITH(NOLOCK) WHERE merchant_id IN ( ";

            String where = "";
            for (int i = 0; i < merchantArray.length; i++) {
                if (!merchantArray[i].trim().equals("")) {
                    where += " " + merchantArray[i] + " ";
                    if (i < merchantArray.length - 1) {
                        where += ",";
                    }
                }
            }
            if (where.endsWith(",")) {
                where = where.substring(0, where.length() - 1);
            }
            sql += where + ")";

            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            boolean existData = false;
            while (rs.next()) {
                existData = true;
                listMerchants.add(rs.getString("merchant_id") + "," + rs.getString("dba"));
            }
            if (!existData) {
                listMerchants.add("NOTHING,NOTHING");
            }
        } catch (Exception e) {
            e.printStackTrace();
            cat.debug("getMerchantBussinessName Preparing statement for query: " + sql);
            cat.error("Error during getMerchantBussinessName", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return listMerchants;
    }

    public static List<MerchantsStockLevelsPojo> getMerchantStockLevelsByIso(String id, String isoId) {
        List<MerchantsStockLevelsPojo> list = new ArrayList<MerchantsStockLevelsPojo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));
            String sql = MerchantStockLevels.sql_bundle.getString("getMerchantStockLevelsByIso");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setBigDecimal(1, new BigDecimal(isoId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                if (id == null || (id != null && id.equalsIgnoreCase(rs.getString("id")))) {
                    MerchantsStockLevelsPojo stockLevels = new MerchantsStockLevelsPojo();
                    stockLevels.setId(rs.getString("id"));
                    stockLevels.setDescription(rs.getString("description"));
                    stockLevels.setIsoId(rs.getBigDecimal("isoId"));
                    stockLevels.setLowColor(rs.getString("lowColor"));
                    stockLevels.setMediumColor(rs.getString("mediumColor"));
                    stockLevels.setHighColor(rs.getString("highColor"));
                    stockLevels.setLowMinValue(rs.getDouble("lowMinValue"));
                    stockLevels.setLowMaxValue(rs.getDouble("lowMaxValue"));
                    stockLevels.setMediumMinValue(rs.getDouble("mediumMinValue"));
                    stockLevels.setMediumMaxValue(rs.getDouble("mediumMaxValue"));
                    stockLevels.setHighMinValue(rs.getDouble("highMinValue"));
                    stockLevels.setHighMaxValue(rs.getDouble("highMaxValue"));

                    list.add(stockLevels);
                }
            }
        } catch (Exception e) {
            cat.error("Error during getMerchantStockLevelsByIso", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static List<String> getMerchantStockLevelsDetail(String IdMerchantsStockLevels) {
        List<String> list = new ArrayList<String>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));
            String sql = MerchantStockLevels.sql_bundle.getString("getMerchantStockLevelsDetail");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, IdMerchantsStockLevels);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("merchant_id"));
            }
        } catch (Exception e) {
            cat.error("Error during getMerchantStockLevelsByIso", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public void insertMerchantsStockLevels(MerchantsStockLevelsPojo merchantsStockLevels, String merchantArray[]) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));

            strSQL = MerchantStockLevels.sql_bundle.getString("insertMerchantsStockLevels");

            cat.debug("Insert merchantsStockLevels" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, uuid);
            pst.setString(2, merchantsStockLevels.getDescription());
            pst.setBigDecimal(3, merchantsStockLevels.getIsoId());
            pst.setString(4, merchantsStockLevels.getLowColor());
            pst.setString(5, merchantsStockLevels.getMediumColor());
            pst.setString(6, merchantsStockLevels.getHighColor());
            pst.setDouble(7, merchantsStockLevels.getLowMinValue());
            pst.setDouble(8, merchantsStockLevels.getLowMaxValue());
            pst.setDouble(9, merchantsStockLevels.getMediumMinValue());
            pst.setDouble(10, merchantsStockLevels.getMediumMaxValue());
            pst.setDouble(11, merchantsStockLevels.getHighMinValue());
            pst.setDouble(12, merchantsStockLevels.getHighMaxValue());
            pst.executeUpdate();

            // INSERT MerchantsStockLevelsDetail
            insertMerchantsStockLevelsDetail(uuid, merchantArray);
        } catch (Exception e) {
            cat.error("Error inserting insertMerchantsStockLevels", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void updateMerchantsStockLevels(MerchantsStockLevelsPojo merchantsStockLevels, String merchantArray[]) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));
            strSQL = MerchantStockLevels.sql_bundle.getString("updateMerchantsStockLevels");

            cat.debug("updateMerchantsStockLevels " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            pst.setString(1, merchantsStockLevels.getDescription());
            pst.setString(2, merchantsStockLevels.getLowColor());
            pst.setString(3, merchantsStockLevels.getMediumColor());
            pst.setString(4, merchantsStockLevels.getHighColor());
            pst.setDouble(5, merchantsStockLevels.getLowMinValue());
            pst.setDouble(6, merchantsStockLevels.getLowMaxValue());
            pst.setDouble(7, merchantsStockLevels.getMediumMinValue());
            pst.setDouble(8, merchantsStockLevels.getMediumMaxValue());
            pst.setDouble(9, merchantsStockLevels.getHighMinValue());
            pst.setDouble(10, merchantsStockLevels.getHighMaxValue());
            pst.setString(11, merchantsStockLevels.getId());
            pst.executeUpdate();

            // delete old merchant list
            deleteMerchantsStockLevelsDetail(merchantsStockLevels.getId());

            // INSERT MerchantsStockLevelsDetail
            insertMerchantsStockLevelsDetail(merchantsStockLevels.getId(), merchantArray);
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error inserting insertMerchantsStockLevels", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void insertMerchantsStockLevelsDetail(String idMerchantsStockLevels, String merchantArray[]) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = MerchantStockLevels.sql_bundle.getString("insertMerchantsStockLevelsDetail");
            cat.debug("Insert insertMerchantsStockLevelsDetail" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            for (String merchantId : merchantArray) {
                pst.setString(1, UUID.randomUUID().toString().toUpperCase());
                pst.setString(2, idMerchantsStockLevels);
                pst.setString(3, merchantId);
                pst.executeUpdate();
            }
        } catch (Exception e) {
            cat.error("Error inserting insertMerchantsStockLevelsDetail", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void deleteMerchantsStockLevels(String idMerchantsStockLevels) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            deleteMerchantsStockLevelsDetail(idMerchantsStockLevels);

            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = MerchantStockLevels.sql_bundle.getString("deleteMerchantStockLevels");
            cat.debug("delete deleteMerchantsStockLevels" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, idMerchantsStockLevels);
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error deleting deleteMerchantsStockLevels", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void deleteMerchantsStockLevelsDetail(String idMerchantsStockLevels) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(MerchantStockLevels.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = MerchantStockLevels.sql_bundle.getString("deleteMerchantStockLevelsDetail");
            cat.debug("delete insertMerchantsStockLevelsDetail" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, idMerchantsStockLevels);
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error deleteMerchantStockLevelsDetail", e);
            throw new ReportException();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    /**
     *
     * @param stockLevelsId
     * @param fieldsToRead
     * @param strAccessLevel
     * @param strRefId
     * @param strDistChainType
     * @param low
     * @param max
     * @return
     */
    public String getReportQuery(String stockLevelsId, List<String> fieldsToRead, String strAccessLevel,
            String strRefId, String strDistChainType, String low, String max) {
        StringBuilder filterChain = new StringBuilder("");
        String strSelectFields = fieldsToRead.toString().replaceAll("\\[", " ").replaceAll("\\]", " ");
        StringBuilder sql = new StringBuilder("SELECT " + strSelectFields);
        sql.append(" FROM MerchantsStockLevels msl WITH(NOLOCK) ");
        sql.append(" INNER JOIN MerchantsStockLevelsDetail msld WITH(NOLOCK) ON (msld.IdMerchantsStockLevels = msl.id) ");
        sql.append(" INNER JOIN merchants m WITH(NOLOCK) ON (m.merchant_id = msld.merchant_id) ");

        if (strAccessLevel.equals(DebisysConstants.ISO)) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
                filterChain.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id");
                filterChain.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 5 AND iso_id ");
                filterChain.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 4 AND iso_id = " + strRefId + "))) ");
            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
                filterChain.append(" WHERE r.iso_id = " + strRefId + " ");
            }
        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
            filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            filterChain.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id");
            filterChain.append("                IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 5 AND iso_id = " + strRefId + ")) ");
        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
            filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            filterChain.append(" WHERE r.rep_id IN ( SELECT rep_id FROM reps WITH(NOLOCK) WHERE type = 1 AND iso_id = " + strRefId + ") ");
        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
            filterChain.append(" INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id ");
            filterChain.append(" WHERE r.rep_id = " + strRefId + " ");
        }
        sql.append(filterChain);
        sql.append(" AND msl.id = '" + stockLevelsId + "' AND m.datecancelled IS NULL AND  m.cancelled=0 AND m.latitude <> 0 AND m.longitude <> 0 ");
        if (low != null && max != null) {
            if (low.equals("-1") && max.equals("-1")) {
                sql.append(" AND ( ((m.LiabilityLimit - m.RunningLiability) <= msl.lowMinValue OR (m.LiabilityLimit - m.RunningLiability) >= msl.lowMaxValue) ");
                sql.append(" AND ((m.LiabilityLimit - m.RunningLiability) <= msl.mediumMinValue OR (m.LiabilityLimit - m.RunningLiability) >= msl.mediumMaxValue) ");
                sql.append(" AND ((m.LiabilityLimit - m.RunningLiability) <= msl.highMinValue OR (m.LiabilityLimit - m.RunningLiability) >= msl.highMaxValue)) ");
            } else {
                sql.append(" AND ( (m.LiabilityLimit - m.RunningLiability) >= " + low + " AND (m.LiabilityLimit - m.RunningLiability) <= " + max + " ) ");
            }
        }
        cat.info("getReportQuery "+sql.toString());
        return sql.toString();

    }

    private FileChannel fc = null;

    /**
     * @param fc
     * @param c
     * @throws IOException
     */
    private void writetofile(FileChannel fc, String c) throws IOException {
        fc.write(ByteBuffer.wrap(c.getBytes()));
    }

    /**
     *
     * @param results
     * @param sessionData
     * @param application
     * @param headers
     * @param strFileName
     * @param titles
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public String download(Vector results, SessionData sessionData, ServletContext application, ArrayList<String> headers,
            String strFileName, ArrayList<String> titles)
            throws SQLException, IOException {
        //ArrayList<String> headers
        String downloadUrl = DebisysConfigListener.getDownloadUrl(application);
        String downloadPath = DebisysConfigListener.getDownloadPath(application);
        String workingDir = DebisysConfigListener.getWorkingDir(application);
        String filePrefix = DebisysConfigListener.getFilePrefix(application);

        if (fc == null) {
            fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel();
        }
        for (String titlesInfo : titles) {
            writetofile(fc, titlesInfo + ",");
            writetofile(fc, "\r\n");
        }
        writetofile(fc, "\r\n");

        for (String colum : headers) {
            writetofile(fc, colum + ",");
        }
        writetofile(fc, "\r\n");
        int sizeVector = results.size();
        if (sizeVector > 0) {
            for (int i = 0; i < sizeVector; i++) {
                Vector value = (Vector) results.get(i);
                String merchantName = (String) value.get(0);
                String merchantId = (String) value.get(1);
                String Phone = (String) value.get(2);
                Double LiabilityLimit = formatAmountDown((String) value.get(3));
                Double RunningLiability = formatAmountDown((String) value.get(4));
                Double bal = LiabilityLimit - RunningLiability;
                String Balance = com.debisys.utils.NumberUtil.formatCurrency(bal.toString(), true);
                String RepName = (String) value.get(5);

                writetofile(fc, "\"" + merchantName + "\",");
                writetofile(fc, "\"" + merchantId + "\",");
                writetofile(fc, "\"" + Phone + "\",");
                writetofile(fc, "\"" + Balance + "\",");
                writetofile(fc, "\"" + RepName + "\",");
                writetofile(fc, "\r\n");
            }
            writetofile(fc, "\r\n");
        }
        writetofile(fc, "\r\n");
        fc.close();
        downloadUrl = createZipFile(strFileName, downloadUrl, downloadPath, workingDir, filePrefix);
        return downloadUrl;
    }

    private static double formatAmountDown(String strAmount) {
        Number Currency = 0;
        if (strAmount != null && !strAmount.equals("")) {
            try {
                DecimalFormat df = new DecimalFormat("###0.00");
                Currency = df.parse(strAmount);
            } catch (Exception nfe) {
                Currency = 0.00;
            }
        } else {
            Currency = 0.00;
        }
        return Currency.doubleValue();
    }

    private static String createZipFile(String strFileName, String downloadUrl, String downloadPath, String workingDir, String filePrefix) {
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";

        try {
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
            // Set the compression ratio
            out.setLevel(Deflater.DEFAULT_COMPRESSION);
            FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");
            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
            int len;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
            // Close the current entry
            out.closeEntry();
            // Close the current file input stream
            in.close();
            out.close();
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            sourceFile.delete();
        }
        return downloadUrl;
    }

    public static String generateFileName(ServletContext context) {
        boolean isUnique = false;
        String strFileName = "";
        while (!isUnique) {
            strFileName = generateRandomNumber();
            isUnique = checkFileName(strFileName, context);
        }
        return strFileName;
    }

    private static String generateRandomNumber() {
        StringBuffer s = new StringBuffer();
        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);

        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private static boolean checkFileName(String strFileName, ServletContext context) {
        boolean isUnique = true;
        String downloadPath = DebisysConfigListener.getDownloadPath(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);

        File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
        if (f.exists()) {
            // duplicate file found
            cat.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
            return false;
        }
        f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
        if (f.exists()) {
            // duplicate file found
            cat.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
            return false;
        }
        return isUnique;
    }

}
