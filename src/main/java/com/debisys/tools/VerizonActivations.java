/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools;

import com.debisys.reports.pojo.BasicPojo;
import com.debisys.utils.DateUtil;
import com.debisys.utils.SimControl;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import java.util.Date;
import java.util.Calendar;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 *
 * @author nmartinez
 */
public class VerizonActivations implements HttpSessionBindingListener {

    private static Logger logger = Logger.getLogger(VerizonActivations.class);

    private String activationRequestId;
    private Calendar requestDate;
    private String requestStatus;
    private String subscriberFirstName;
    private String subscriberLastName;
    private String streetNumber;
    private String streetName;
    private String zipCode;
    private String city;
    private String stateIdNew;
    private String preferredLanguage;
    private String desiredAreaCode;
    private String contactPhone;
    private String sim;
    private String esnImei;
    private int transactionId;
    private String phoneNumber;
    private long accountPinNumber;
    private String pin;
    private int linesRequested;

    public String getActivationRequestId() {
        return activationRequestId;
    }

    public void setActivationRequestId(String activationRequestId) {
        this.activationRequestId = activationRequestId;
    }

    public Calendar getRequestDate() {
        return requestDate;
    }

    public String getRequestDateStr() {
        return DateUtil.formatDate(this.requestDate.getTime());
    }

    public void setRequestDate(Calendar requestDate) {
        this.requestDate = requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate.setTime(requestDate);
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getSubscriberFirstName() {
        return subscriberFirstName;
    }

    public void setSubscriberFirstName(String subscriberFirstName) {
        this.subscriberFirstName = subscriberFirstName;
    }

    public String getSubscriberLastName() {
        return subscriberLastName;
    }

    public void setSubscriberLastName(String subscriberLastName) {
        this.subscriberLastName = subscriberLastName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateIdNew() {
        return stateIdNew;
    }

    public void setStateIdNew(String stateIdNew) {
        this.stateIdNew = stateIdNew;
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public String getDesiredAreaCode() {
        return desiredAreaCode;
    }

    public void setDesiredAreaCode(String desiredAreaCode) {
        this.desiredAreaCode = desiredAreaCode;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getAccountPinNumber() {
        return accountPinNumber;
    }

    public void setAccountPinNumber(long accountPinNumber) {
        this.accountPinNumber = accountPinNumber;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getLinesRequested() {
        return linesRequested;
    }

    public void setLinesRequested(int linesRequested) {
        this.linesRequested = linesRequested;
    }

    public VerizonActivations() {
        this.activationRequestId = "";
        this.requestDate = Calendar.getInstance();
        this.requestStatus = "";
        this.subscriberFirstName = "";
        this.subscriberLastName = "";
        this.streetNumber = "";
        this.streetName = "";
        this.zipCode = "";
        this.city = "";
        this.stateIdNew = "";
        this.preferredLanguage = "";
        this.desiredAreaCode = "";
        this.contactPhone = "";
        this.sim = "";
        //this.imei = "";
        this.esnImei = "";
        this.transactionId = 0;
        this.phoneNumber = "";
        this.accountPinNumber = 0;
        this.pin = "";
        this.linesRequested = 0;
    }

    public VerizonActivations(String activationRequestId, Calendar requestDate,
            String requestStatus, String subscriberFirstName,
            String subscriberLastName, String streetNumber, String streetName,
            String zipCode, String city, String stateIdNew,
            String preferredLanguage, String desiredAreaCode,
            String contactPhone, String sim, String esnImei,
            int transactionId, String phoneNumber, long accountPinNumber,
            String pin, int linesRequested) {
        this.activationRequestId = activationRequestId;
        this.requestDate = requestDate;
        this.requestStatus = requestStatus;
        this.subscriberFirstName = subscriberFirstName;
        this.subscriberLastName = subscriberLastName;
        this.streetNumber = streetNumber;
        this.streetName = streetName;
        this.zipCode = zipCode;
        this.city = city;
        this.stateIdNew = stateIdNew;
        this.preferredLanguage = preferredLanguage;
        this.desiredAreaCode = desiredAreaCode;
        this.contactPhone = contactPhone;
        this.sim = sim;
        this.esnImei = esnImei;
        this.transactionId = transactionId;
        this.phoneNumber = phoneNumber;
        this.accountPinNumber = accountPinNumber;
        this.pin = pin;
        this.linesRequested = linesRequested;
    }

    @Override
    public String toString() {
        return "VerizonActivations{" + "activationRequestId=" + activationRequestId + ", requestDate=" + requestDate + ", requestStatus=" + requestStatus + ", subscriberFirstName=" + subscriberFirstName + ", subscriberLastName=" + subscriberLastName + ", streetNumber=" + streetNumber + ", streetName=" + streetName + ", zipCode=" + zipCode + ", city=" + city + ", stateIdNew=" + stateIdNew + ", preferredLanguage=" + preferredLanguage + ", desiredAreaCode=" + desiredAreaCode + ", contactPhone=" + contactPhone + ", sim=" + sim + ", esnImei=" + esnImei + ", transactionId=" + transactionId + ", phoneNumber=" + phoneNumber + ", accountPinNumber=" + accountPinNumber + ", pin=" + pin + ", linesRequested=" + linesRequested + '}';
    }

    private static VerizonActivations getActivationRequestFromRs(ResultSet rs) {
        VerizonActivations retValue = null;
        try {
            if (rs != null) {
                retValue = new VerizonActivations();
                retValue.setAccountPinNumber(rs.getLong("accountPinNumber"));
                retValue.setActivationRequestId(rs.getString("activationRequestId"));
                retValue.setCity(rs.getString("city"));
                retValue.setContactPhone(rs.getString("contactPhone"));
                retValue.setDesiredAreaCode(rs.getString("desiredAreaCode"));
                retValue.setEsnImei(rs.getString("esnImei"));
                retValue.setLinesRequested(rs.getInt("linesRequested"));
                retValue.setPhoneNumber(rs.getString("phoneNumber"));
                retValue.setPin(rs.getString("pin"));
                retValue.setPreferredLanguage(rs.getString("preferredLanguage"));
                retValue.setRequestDate(rs.getDate("requestDate"));
                retValue.setRequestStatus(rs.getString("requestStatus"));
                retValue.setSim(rs.getString("sim"));
                retValue.setStateIdNew(rs.getString("stateIdNew"));
                retValue.setStreetName(rs.getString("streetName"));
                retValue.setStreetNumber(rs.getString("streetNumber"));
                retValue.setSubscriberFirstName(rs.getString("subscriberFirstName"));
                retValue.setSubscriberLastName(rs.getString("subscriberLastName"));
                retValue.setTransactionId(rs.getInt("transactionId"));
                retValue.setZipCode(rs.getString("zipCode"));
            }
        } catch (Exception localException) {
            logger.error("getActivationRequestStatusFromRs. Error while getting activation request status", localException);
        }

        return retValue;
    }

    /**
     *
     * @param trxId
     * @return
     */
    public static VerizonActivations getActivationRequestByRequestId(String trxId) {
        VerizonActivations retValue = null;

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection();
            StringBuilder sqlStringB = new StringBuilder();

            sqlStringB.append(" SELECT [activationRequestId], [requestDate] ");
            sqlStringB.append("        , [requestStatus], [subscriberFirstName] ");
            sqlStringB.append("        , [subscriberLastName], [streetNumber] ");
            sqlStringB.append("        , [streetName], [zipCode] , [city], [stateIdNew] ");
            sqlStringB.append("        , [preferredLanguage], [desiredAreaCode] ");
            sqlStringB.append("        , [contactPhone], [sim], [esnImei] ");
            sqlStringB.append("        , [transactionId], [phoneNumber] ");
            sqlStringB.append("        , [accountPinNumber], [pin], [linesRequested] ");
            sqlStringB.append(" FROM [activationRequest] WITH(NOLOCK) ");
            sqlStringB.append(" WHERE [transactionId] = ? ");

            String sqlString = sqlStringB.toString();

            pstmt = dbConn.prepareStatement(sqlString);
            pstmt.setString(1, trxId);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = getActivationRequestFromRs(rs);
            }
            dbConn.close();
        } catch (Exception localSqlException) {
            logger.info("Exception getActivationRequestByRequestId = " + localSqlException);
        } finally {
            try {
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                }
            } catch (Exception e) {
                logger.info("Exception getActivationRequestByRequestId Torque.closeConnection(dbConn) " + e);
            }
        }
        return retValue;
    }

    public static final String STATUS_PENDING = "PEN";
    public static final String STATUS_COMPLETED = "CMP";
    public static final String STATUS_CANCELLED = "CAN";

    public static final String TRANSACTION_TYPE_PIN = "21";
    public static final String TRANSACTION_TYPE_ACTIVATION = "25";
    public static final String TRANSACTION_TYPE_SPIFF = "18";

    public static final String TOKEN_LINES_REQUESTED = "LINES_REQUESTED_";
    public static final String TOKEN_FIRST_NAME = "FIRST_NAME_";
    public static final String TOKEN_LAST_NAME = "LAST_NAME_";
    public static final String TOKEN_STREET_NUMBER = "STREET_NUMBER_";
    public static final String TOKEN_STREET_NAME = "STREET_NAME_";
    public static final String TOKEN_ZIP_CODE = "ZIP_CODE_";
    public static final String TOKEN_CITY = "CITY_";
    public static final String TOKEN_STATE = "STATE_";
    public static final String TOKEN_PRE_LANGUAGE = "LANGUAGE_";
    public static final String TOKEN_AREA_CODE = "AREA_CODE_";
    public static final String TOKEN_PHONE_NUMBER = "PHONE_NUMBER_";
    public static final String TOKEN_SIM = "SIM_";
    public static final String TOKEN_ESN_IMEI = "ESN_IMEI_";
    //public static final String TOKEN_IMEI = "IMEI_";
    public static final String TOKEN_ACCOUNT_PIN_NUMBER = "ACCOUNT_PIN_NUMBER_";
    public static final String TOKEN_PLAN = "PLAN_";
    public static final String TOKEN_ACTIVATION_PHONE = "ACTIVATION_PHONE_";
    public static final String TOKEN_REQUEST_STATUS = "REQUEST_STATUS_";
    public static final String TOKEN_WARNNING_1 = "WARNNING_1_";
    public static final String TOKEN_ACTIVATION_DATE = "ACTIVATION_DATE_";

    /**
     *
     * @param linesRequested
     * @param firstName
     * @param lastName
     * @param streetNumber
     * @param streetName
     * @param zipCode
     * @param city
     * @param areaCode
     * @param phoneNumber
     * @param sim
     * @param esn
     * @param imei
     * @param accountPinNumber
     * @param phoneActivation
     * @param styleErrorField
     * @return
     */
    public String validateScreenControls(SimControl linesRequested, SimControl firstName, SimControl lastName, SimControl streetNumber,
            SimControl streetName, SimControl zipCode, SimControl city, SimControl areaCode,
            SimControl phoneNumber, SimControl sim, SimControl esnImei, SimControl accountPinNumber,
            SimControl phoneActivation, String styleErrorField) {
        String result = null;
        HashMap<String, String> controlErrors = new HashMap<String, String>();
        StringBuilder messagesControls = new StringBuilder();
        //messagesControls.append(this.requiredFieldsMessages + "<br/>");

        linesRequested.setValue(String.valueOf(this.linesRequested));
        firstName.setValue(this.subscriberFirstName);
        lastName.setValue(this.subscriberLastName);
        streetNumber.setValue(this.streetNumber);
        streetName.setValue(this.streetName);
        zipCode.setValue(this.zipCode);
        city.setValue(this.city);
        areaCode.setValue(this.desiredAreaCode);
        phoneNumber.setValue(this.contactPhone);
        sim.setValue(this.sim);
        esnImei.setValue(this.esnImei);
        //imei.setValue(this.imei);
        accountPinNumber.setValue(String.valueOf(this.accountPinNumber));

        boolean validatePhone = false;
        ArrayList<BasicPojo> statusValues = findReqStatus();
        for (BasicPojo status : statusValues) {
            if (status.getId().toLowerCase().equals(this.requestStatus.toLowerCase())
                    && status.getDescripton().toLowerCase().equals(STATUS_COMPLETED.toLowerCase())) {
                validatePhone = true;
            }
        }
        phoneActivation.setValue(this.phoneNumber);

        UtilActivations.validateControl(linesRequested, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(firstName, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(lastName, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(streetNumber, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(streetName, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(zipCode, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(city, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(areaCode, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(phoneNumber, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(sim, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(esnImei, controlErrors, messagesControls, styleErrorField);
        UtilActivations.validateControl(accountPinNumber, controlErrors, messagesControls, styleErrorField);
        if (validatePhone) {
            UtilActivations.validateControl(phoneActivation, controlErrors, messagesControls, styleErrorField);
        }

        if (controlErrors.size() > 0) {
            result = messagesControls.toString();
        }
        return result;
    }

    /**
     * 
     * @param applicationName
     * @param userSession
     * @param instance
     * @return 
     */
    public boolean changeActivatiobByStatusCode(String applicationName, String userSession, String instance) {
        boolean retValue = false;

        Connection dbConn = null;
        CallableStatement cstmt = null;

        try {
            dbConn = Torque.getConnection();

            cstmt = dbConn.prepareCall("EXEC [VoidSimActivation] ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");

            cstmt.setInt(1, this.transactionId);
            cstmt.setString(2, this.requestStatus);
            cstmt.setString(3, this.subscriberFirstName);
            cstmt.setString(4, this.subscriberLastName);
            cstmt.setString(5, this.streetNumber);
            cstmt.setString(6, this.streetName);
            cstmt.setString(7, this.zipCode);
            cstmt.setString(8, this.city);
            cstmt.setString(9, this.stateIdNew);
            cstmt.setString(10, this.preferredLanguage);
            cstmt.setString(11, this.desiredAreaCode);
            cstmt.setString(12, this.contactPhone);
            cstmt.setString(13, this.sim);
            cstmt.setString(14, this.esnImei);
            cstmt.setString(15, this.phoneNumber);
            cstmt.setLong(16, this.accountPinNumber);
            cstmt.setInt(17, this.linesRequested);

            cstmt.setString(18, applicationName);
            cstmt.setString(19, userSession);
            cstmt.setString(20, instance);
            
            cstmt.registerOutParameter(21, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(22, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(23, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(24, java.sql.Types.INTEGER);
            cstmt.execute();

            int trxIdVoidSimActivation = cstmt.getInt(21);
            int trxIdVoidSpiffActivation = cstmt.getInt(22);
            int pinAvailableAgain = cstmt.getInt(23);
            int messageCode = cstmt.getInt(24);

            logger.info("trxIdVoidSimActivation   = [" + trxIdVoidSimActivation + "]");
            logger.info("trxIdVoidSpiffActivation = [" + trxIdVoidSpiffActivation + "]");
            logger.info("pinAvailableAgain        = [" + pinAvailableAgain + "]");
            logger.info("messageCode              = [" + messageCode + "]");

            cstmt.close();
            dbConn.close();
        } catch (Exception localSqlException) {
            logger.info("Exception changeActivatiobByStatusCode = [" + localSqlException+"]");
        } finally {
            try {
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                }
            } catch (Exception e) {
                logger.info("Exception changeActivatiobByStatusCode Torque.closeConnection(dbConn) " + e);
            }
        }
        return retValue;
    }

    /**
     *
     * @param countryId
     * @return
     */
    public static java.util.ArrayList<BasicPojo> findStates(String countryId) {
        ArrayList<BasicPojo> countries = new ArrayList<BasicPojo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection();
            StringBuilder sqlStringB = new StringBuilder();

            sqlStringB.append(" SELECT s.idNew, s.StateName FROM states s WHERE s.CountryId=? ORDER BY s.StateName ");

            String sqlString = sqlStringB.toString();

            pstmt = dbConn.prepareStatement(sqlString);
            pstmt.setString(1, countryId);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                BasicPojo pojo = new BasicPojo();
                pojo.setId(rs.getString(1));
                pojo.setDescripton(rs.getString(2));
                countries.add(pojo);
            }
            dbConn.close();
        } catch (Exception localSqlException) {
            logger.info("Exception findStates = " + localSqlException);
        } finally {
            try {
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                }
            } catch (Exception e) {
                logger.info("Exception findStates Torque.closeConnection(dbConn) " + e);
            }
        }
        return countries;
    }

    /**
     *
     * @param countryId
     * @return
     */
    public static java.util.ArrayList<BasicPojo> findReqStatus() {
        ArrayList<BasicPojo> reqStatus = new ArrayList<BasicPojo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection();
            StringBuilder sqlStringB = new StringBuilder();
            sqlStringB.append(" SELECT statusId, statusCode FROM activationRequestStatus WITH(NOLOCK) ");
            String sqlString = sqlStringB.toString();
            pstmt = dbConn.prepareStatement(sqlString);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                BasicPojo pojo = new BasicPojo();
                pojo.setId(rs.getString(1));
                pojo.setDescripton(rs.getString(2));
                reqStatus.add(pojo);
            }
            dbConn.close();
        } catch (Exception localSqlException) {
            logger.info("Exception findReqStatus = " + localSqlException);
        } finally {
            try {
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                }
            } catch (Exception e) {
                logger.info("Exception findReqStatus Torque.closeConnection(dbConn) " + e);
            }
        }
        return reqStatus;
    }

    /**
     *
     * @param isoId
     * @param trxId
     * @return
     */
    public static boolean validatrxIdByIsoId(String isoId, String trxId) {

        CallableStatement cstmt = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean result = false;
        Long merchantId = null;
        Long isoTrxId = null;
        try {
            dbConn = Torque.getConnection();
            StringBuilder sqlStringB = new StringBuilder();

            sqlStringB.append(" SELECT w.merchant_id FROM web_transactions w WITH(NOLOCK) WHERE w.rec_id=? ");
            String sqlString = sqlStringB.toString();
            pstmt = dbConn.prepareStatement(sqlString);
            pstmt.setString(1, trxId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                merchantId = rs.getLong(1);
                pstmt.close();
                rs.close();
                cstmt = dbConn.prepareCall("EXEC [GetIsoIDByMerchantID] ?");
                cstmt.setLong(1, merchantId);
                rs = cstmt.executeQuery();
                if (rs.next()) {
                    isoTrxId = rs.getLong(1);
                    if (isoTrxId.toString().equals(isoId)) {
                        result = true;
                    } else {
                        logger.info("The ISO of trx id=[" + trxId + "] do not match with to the ISO user's !!!");
                    }
                } else {
                    logger.info("Merchant id=[" + merchantId + "] not exist in platforms!!!");
                }
            } else {
                logger.info("Transaction id=[" + trxId + "] not exist in web_transactions!!!");
            }
            dbConn.close();
        } catch (Exception localSqlException) {
            logger.info("Exception validatrxIdByIsoId = " + localSqlException);
        } finally {
            try {
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                }
            } catch (Exception e) {
                logger.info("Exception validatrxIdByIsoId Torque.closeConnection(dbConn) " + e);
            }
        }
        return result;
    }

    @Override
    public void valueBound(HttpSessionBindingEvent hsbe) {

    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent hsbe) {

    }

    /**
     * @return the esnImei
     */
    public String getEsnImei() {
        return esnImei;
    }

    /**
     * @param esnImei the esnImei to set
     */
    public void setEsnImei(String esnImei) {
        this.esnImei = esnImei;
    }

}
