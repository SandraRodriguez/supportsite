/**
 *
 */
package com.debisys.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import java.util.Iterator;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.languages.Languages;
import com.debisys.presentation.html.HtmlComponent;
import com.debisys.presentation.html.HtmlField;
import com.debisys.terminals.Terminal;
import com.debisys.users.SessionData;
import com.debisys.utils.CountryInfo;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.TimeZone;
import com.debisys.customers.Merchant;

/**
 * @author nmartinez
 * 
 */
public class ValidateInformation {

	private StringBuffer messagesError = new StringBuffer(1000);

	protected static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");
	protected static Category cat = Category.getInstance(ValidateInformation.class);

	private Hashtable hashTerminalTypes = new Hashtable();
	private Hashtable hashRepsOfIso = new Hashtable();
	private Hashtable hashRepsMails = new Hashtable();
	private Hashtable hashStates = new Hashtable();
	private Hashtable hashPlans = new Hashtable();
	private Hashtable<String, Long> hashNewPlans = new Hashtable<String, Long>();
	private Hashtable hashCreditTypes = new Hashtable();
	private Hashtable hashProcessors = new Hashtable();
	private Hashtable hashPayments = new Hashtable();
	private Hashtable hashSegmentsTypes = new Hashtable();
	private Hashtable hashBusinessTypes = new Hashtable();
	private Hashtable hashRegionTelcel = new Hashtable();
	// R27. DBSY-570. added by Jacuna
	private Hashtable hashInvoiceTypes = new Hashtable();
	private Hashtable hashMerchantClassifications = new Hashtable();
	// End R27. DBSY-570.

	private Hashtable hashAccountTypes = new Hashtable();
	private Hashtable hashContactTypes = new Hashtable();
	private Hashtable hashRoute = new Hashtable();
	private Hashtable hashAplicationGroup = new Hashtable();
	private Hashtable hashKeysGroup = new Hashtable();
	private Hashtable hashPCTErmBrand = new Hashtable();

	protected MerchantBulk objMerchant = null;
	protected TerminalBulk objTerminal = new TerminalBulk();

	protected String accessLevel;
	protected String refId;
	protected String deploymentType;
	protected String DistChainType;
	protected String customConfigType;
	protected String mailHost;
	protected String companyName;
	protected String userName;
	protected String remoteHost;
	protected String localAddr;
	protected String contextPath;

	protected Integer numTerminalsInMerchant = 1;
	protected Integer merchantConsecutive = null;

	private Boolean flagSameValuesForBilling = Boolean.FALSE;

	protected ArrayList<MerchantBulk> arrayMerchant = new ArrayList<MerchantBulk>(0);

	protected StringBuffer comboTypes = new StringBuffer(1000);

	public ValidateInformation() {
		// Hashtable<String, Double> balance = new Hashtable<String, Double>();
	}

	/**
	 * @param valueFromCell
	 * @return
	 */
	private String ValidateExcelFielToString(String valueFromCell) {
		DecimalFormat df = new DecimalFormat("000000000000000");
		String valueBefore = df.format(Double.parseDouble(valueFromCell));
		Long value = new Long(valueBefore);
		return value.toString();
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBusinessName(String valueFromCell, SessionData sessionData) {

		if (valueFromCell.length() > 1) {
			objMerchant.setBusinessName(valueFromCell);
		} else {
			this.objMerchant.addFieldError("BusinessName",
					Languages.getString("jsp.admin.customers.merchants_edit.error_business_name", sessionData.getLanguage()));
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateDba(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 1) {
			objMerchant.setDba(valueFromCell);
		} else {
			this.objMerchant.addFieldError("Dba", Languages.getString("jsp.admin.customers.merchants_edit.error_dba", sessionData.getLanguage()));
		}
	}

	/**
   *
   */
	protected void addMerchantToArray() {
		numTerminalsInMerchant = 1;
		// this.objMerchant.validate(sessionData, context);
		arrayMerchant.add(this.objMerchant);
		// initialize the MerchantBulk object
		objMerchant = new MerchantBulk(mailHost, accessLevel, companyName, userName);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhysCountry(String valueFromCell, SessionData sessionData) {
		try {
			HashMap m = CountryInfo.getAllCountriesID();
			if (m.containsValue(valueFromCell)) {
				this.objMerchant.setPhysCountry(valueFromCell);
			} else {
				this.objMerchant.addFieldError("Country", Languages.getString("jsp.admin.customers.merchants_Country_error", sessionData.getLanguage()));
			}
		} catch (Exception e) {
			cat.error("Error getAllCountriesID validate country Bulkmerchant" + e);
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateRoute(String valueFromCell) {
		String temp = "";
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			temp = this.ValidateExcelFielToString(valueFromCell);
		} else {
			temp = valueFromCell;
		}
		Integer value = (Integer) this.hashRoute.get(temp);
		if (value != null) {
			this.objMerchant.setMerchantRouteId(value);
		}
	}

	public static Integer getAbaCheckDigit(String aba) {
		aba = aba.trim();
		int sum = 0;
		sum += Integer.parseInt(aba.substring(0, 1)) * 3;
		sum += Integer.parseInt(aba.substring(1, 2)) * 7;
		sum += Integer.parseInt(aba.substring(2, 3)) * 1;
		sum += Integer.parseInt(aba.substring(3, 4)) * 3;
		sum += Integer.parseInt(aba.substring(4, 5)) * 7;
		sum += Integer.parseInt(aba.substring(5, 6)) * 1;
		sum += Integer.parseInt(aba.substring(6, 7)) * 3;
		sum += Integer.parseInt(aba.substring(7, 8)) * 7;
		Integer checkDigit = 10 - ((sum % 10 == 0) ? 10 : (sum % 10));
		return checkDigit;
	}

	public boolean isValidAba(String aba, String[] validAbaStr) {
		if (aba != null && validAbaStr != null && ArrayUtils.contains(validAbaStr, aba.trim())) {
			return false;
		}
		if (aba == null || aba.trim().equals("")) {// Not empty
			return false;
		}

		if ((aba.trim().length() != 9)) {// Length 9
			return false;
		}

		if (!aba.trim().matches("\\d{9}")) {// numbers only
			return false;
		}

		if (aba.trim().matches("[0]*")) {// Not All 0
			return false;
		}

		if (!getAbaCheckDigit(aba).equals(Integer.parseInt(aba.substring(8, 9)))) { // Check Digit OK
			return false;
		}
		return true;
	}

	protected void validateRoutingNumber(String valueFromCell, SessionData sessionData) {
		String temp = "";
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			temp = this.ValidateExcelFielToString(valueFromCell);
		} else {
			temp = valueFromCell;
		}

		if (temp != null) {
			this.objMerchant.setRoutingNumber(temp);
		}

		/*
		 * if(isValidAba(temp, new String[]{"NA","N/A"})){ this.objMerchant.setRoutingNumber(temp.toString()); }else{ this.objMerchant.addFieldError("Aba",
		 * Languages.getString("jsp.admin.customers.edit_field_aba.error_aba_format", sessionData.getLanguage())); }
		 */
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateSameValuesForBilling(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.toUpperCase().equals("SI")) {
			flagSameValuesForBilling = Boolean.TRUE;
			this.objMerchant.setMailAddress(this.objMerchant.getPhysAddress());
			this.objMerchant.setMailColony(this.objMerchant.getPhysColony());
			this.objMerchant.setMailDelegation(this.objMerchant.getPhysDelegation());
			this.objMerchant.setMailCity(this.objMerchant.getPhysCity());
			this.objMerchant.setMailZip(this.objMerchant.getPhysZip());
			this.objMerchant.setMailState(this.objMerchant.getPhysState());
			try {
				HashMap m = CountryInfo.getAllCountriesID();
				if (m.containsValue(this.objMerchant.getPhysCountry().toString())) {
					this.objMerchant.setMailCountry(this.objMerchant.getPhysCountry());
				} else {
					this.objMerchant.addFieldError("Country", Languages.getString("jsp.admin.customers.merchants_BCountry_error", sessionData.getLanguage()));
				}
			} catch (Exception e) {
				cat.error("Error getAllCountriesID validate country Bulkmerchant" + e);
			}

		} else {
			flagSameValuesForBilling = Boolean.FALSE;
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingAddress(String valueFromCell, SessionData sessionData) {
		if (!flagSameValuesForBilling) {
			if (valueFromCell.length() > 0) {
				this.objMerchant.setMailAddress(valueFromCell);
			} else {
				this.objMerchant.addFieldError("BillingAddress",
						Languages.getString("jsp.admin.customers.merchants_edit.error_billing_address", sessionData.getLanguage()));
			}
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingCounty(String valueFromCell) {
		if (!flagSameValuesForBilling) {
			this.objMerchant.setMailCounty(valueFromCell);

		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingState(String valueFromCell) {
		String value = (String) this.hashStates.get(valueFromCell.trim().toUpperCase());
		if (value != null) {
			objMerchant.setMailState(value);
		}

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingZip(String valueFromCell) {
		String value = "";
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			value = this.ValidateExcelFielToString(valueFromCell);
			objMerchant.setMailZip(value);
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingCountry(String valueFromCell, SessionData sessionData) {
		if (!flagSameValuesForBilling) {
			if (valueFromCell.length() > 0) {
				try {
					HashMap m = CountryInfo.getAllCountriesID();
					if (m.containsValue(valueFromCell)) {
						objMerchant.setMailCountry(valueFromCell);
					} else {
						this.objMerchant.addFieldError("Country",
								Languages.getString("jsp.admin.customers.merchants_BCountry_error", sessionData.getLanguage()));
					}
				} catch (Exception e) {
					cat.error("Error getAllCountriesID validate country Bulkmerchant" + e);
				}

			} else {
				this.objMerchant.addFieldError("BillingCountry",
						Languages.getString("jsp.admin.customers.merchants_edit.error_billing_country", sessionData.getLanguage()));
			}
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingCity(String valueFromCell) {
		objMerchant.setMailCity(valueFromCell);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingDelegation(String valueFromCell) {
		objMerchant.setMailDelegation(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBillingColony(String valueFromCell) {
		objMerchant.setMailColony(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhysZip(String valueFromCell) {
		String value = "";
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			value = this.ValidateExcelFielToString(valueFromCell);
			this.objMerchant.setPhysZip(value);
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateMailAdress(String valueFromCell) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhysCity(String valueFromCell) {
		this.objMerchant.setPhysCity(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhysDelegation(String valueFromCell) {
		this.objMerchant.setPhysDelegation(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhysColony(String valueFromCell) {
		this.objMerchant.setPhysColony(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhyCountry(String valueFromCell) {
		this.objMerchant.setPhysCountry(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhyAdress(String valueFromCell) {
		this.objMerchant.setPhysAddress(valueFromCell);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateInvoice(String valueFromCell) {
		if (valueFromCell.length() > 0) {
			// if (valueFromCell.toUpperCase().equals("SI"))
			// this.objMerchant.setInvoice(1);
			// else if (valueFromCell.toUpperCase().equals("NO"))
			// this.objMerchant.setInvoice(0);

			try {
				Integer in = (Integer) this.hashInvoiceTypes.get(valueFromCell.trim().toUpperCase());
				if (in != null) {
					this.objMerchant.setInvoice(in.intValue());
				} else {
				}
			} catch (Exception e) {
				this.objMerchant.setInvoice(0);
			}
		}

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateControlNumber(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {

			this.objMerchant.setControlNumber(valueFromCell);
			if (!this.objMerchant.checkMerchantControlNumber(null, null)) {
				this.objMerchant.setControlNumber(null);
				this.objMerchant.addFieldError("ControlNumber",
						Languages.getString("jsp.admin.customers.merchants_control_number_error", sessionData.getLanguage()));
			}

		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateMerchantClassification(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {
			Integer in = (Integer) this.hashMerchantClassifications.get(valueFromCell.trim().toUpperCase());
			if (in != null)
				this.objMerchant.setMerchantClassification(in.intValue());
			else
				this.objMerchant.addFieldError("MerchantClassification",
						Languages.getString("jsp.admin.customers.merchants_MerchantClassification_error", sessionData.getLanguage()));
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateRegionTelcel(String valueFromCell) {
		Integer value = (Integer) this.hashRegionTelcel.get(valueFromCell.trim().toUpperCase());
		if (value != null)
			this.objMerchant.setRegion(value);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBusinessHours(String valueFromCell) {
		this.objMerchant.setBusinessHours(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateMerchantSegment(String valueFromCell) {
		Integer value = (Integer) this.hashSegmentsTypes.get(valueFromCell.trim().toUpperCase());
		if (value != null)
			this.objMerchant.setMerchantSegmentId(value);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBusinessTypeId(String valueFromCell) {
		Integer value = (Integer) this.hashBusinessTypes.get(valueFromCell.trim().toUpperCase());
		if (value != null) {
			this.objMerchant.setBusinessTypeId(value.toString());
		} else {
			// can be null
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateEmailNotificationMerchant(String valueFromCell) {
		String repid = this.objMerchant.getRepId();
		String emailRep = (String) this.hashRepsMails.get(repid);
		if (emailRep != null)
			this.objMerchant.setRepSetUpMailsNotification(emailRep);
		if (valueFromCell.length() > 0)
			this.objMerchant.setEmailNotification(valueFromCell);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateTaxId(String valueFromCell, SessionData sessionData) {
		String value = "";
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			value = this.ValidateExcelFielToString(valueFromCell);
		} else {
			value = valueFromCell;
		}

		if (value.length() > 0) {
			objMerchant.setTaxId(value);
		} else {
			if (this.customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
				this.objMerchant.addFieldError("TaxId", Languages.getString("jsp.admin.customers.merchants_edit.error_taxid", sessionData.getLanguage()));
			} else {
				if (value.length() > 15) {
					this.objMerchant.addFieldError("TaxId", Languages.getString("jsp.admin.customers.merchants_edit.error_taxid", sessionData.getLanguage()));
				}
			}
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateMonthlyFeeThreshold(String valueFromCell) {
		try {
			Double value = Double.parseDouble(valueFromCell);
			objMerchant.setMonthlyFeeThreshold(value.toString());
		} catch (Exception ex) {
			objMerchant.setMonthlyFeeThreshold("0");
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhysState(String valueFromCell) {
		String value = (String) this.hashStates.get(valueFromCell.trim().toUpperCase());
		if (value != null) {
			objMerchant.setPhysState(value);
		}

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateTerminalPlan(String valueFromCell, SessionData sessionData) {
		if(this.objMerchant.isUseRatePlanModel()){
			Long value = (Long) this.hashNewPlans.get(valueFromCell.trim().toUpperCase());
			if (value != null) {
				objTerminal.setIsoRatePlanId("0");
				objTerminal.setRepRatePlanId("0");
				objTerminal.setRatePlanId(value);
			} else {
				this.objTerminal.addFieldError("validateTerminalPlan", Languages.getString("jsp.admin.tools.validateTerminalPlan", sessionData.getLanguage()));
			}
		}else{
			Long value = (Long) this.hashPlans.get(valueFromCell.trim().toUpperCase());
			if (value != null) {
				objTerminal.setIsoRatePlanId(value.toString());
			} else {
				this.objTerminal.addFieldError("validateTerminalPlan", Languages.getString("jsp.admin.tools.validateTerminalPlan", sessionData.getLanguage()));
			}
		}
	}

	protected void validatePlanModel(String valueFromCell, SessionData sessionData) {
		valueFromCell = valueFromCell.toUpperCase();
		if (valueFromCell != null && (valueFromCell.equals("Y") || valueFromCell.equals("N"))) {
			this.objMerchant.setUseRatePlanModel(valueFromCell.equals("Y"));
		} else {
			this.objTerminal.addFieldError("validatePlanModel", Languages.getString("jsp.admin.tools.validatePlanModel", sessionData.getLanguage()));
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePCTerminalUser(String valueFromCell, boolean isWebServiceAuthenticationEnabled, SessionData sessionData) {
		if (this.objTerminal.getTerminalType().equals("26") || this.objTerminal.getTerminalType().equals("40")
				|| this.objTerminal.getTerminalType().equals(DebisysConstants.WEB_SERVICE_KIOSK_TERMINAL_TYPE)
				|| this.objTerminal.getTerminalType().equals(DebisysConstants.SMS_PHONE_KIOSK_MERCHANT_TERMINAL_TYPE)
				|| this.objTerminal.getTerminalType().equals("56") || (this.objTerminal.getTerminalType().equals("23") && isWebServiceAuthenticationEnabled)) {
			// Only for PC Terminal and SMS devices
			String value = "";
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				value = this.ValidateExcelFielToString(valueFromCell);
			} else {
				value = valueFromCell;
			}

			if (value.length() > 3 && value.length() < 16) {
				this.objTerminal.setPcTermUsername(value);
			} else {
				this.objTerminal.addFieldError("PCTerminalUser", Languages.getString("com.debisys.users.error_username_length", sessionData.getLanguage())
						+ " " + Languages.getString("com.debisys.users.error_username_length_16", sessionData.getLanguage()));
			}
		}
	}

	protected void validatePCTerminalBrand(String valueFromCell, SessionData sessionData) {
		if (this.objTerminal.getTerminalType().equals("26")) { // only for PC TERMINAL devices
			String value = (String) this.hashPCTErmBrand.get(valueFromCell.trim().toUpperCase());

			if (value != null) {
				String[] values = value.split("_SEPARADOR_");
				this.objTerminal.setPcTermBrand(valueFromCell.trim().toUpperCase());
				this.objTerminal.setUrl(values[1]);
			} else {
				this.objTerminal.addFieldError("PCTerminalBrand", Languages.getString("jsp.admin.tools.validateBranding", sessionData.getLanguage()));
			}
		}
	}

	protected void validateCertificateIgnoreInd(String valueFromCell, SessionData sessionData) {
		if (this.objTerminal.getTerminalType().equals("26")) { // only for PC TERMINAL devices
			String value = "";
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				value = this.ValidateExcelFielToString(valueFromCell);
			} else {
				if (valueFromCell.toUpperCase().equals("SI") || valueFromCell.toUpperCase().equals("YES")) {
					value = "1";
				} else {
					if (valueFromCell.toUpperCase().equals("NO")) {
						value = "0";
					} else {
						value = valueFromCell;
					}
				}
			}

			if (value.length() > 0 && value.length() < 2) {
				if (value.equals("1")) {
					this.objTerminal.setChkCertificate(true);
				} else if (value.equals("0")) {
					this.objTerminal.setChkCertificate(false);
				} else {
					this.objTerminal.addFieldError("CertificateIgnoreInd",
							Languages.getString("jsp.admin.customers.reps_add.error_CertInd_invalid", sessionData.getLanguage()));
				}
			}
		}
	}

	protected void validateCertificateInstallAlowedInd(String valueFromCell, SessionData sessionData) {
		if (this.objTerminal.getTerminalType().equals("26")) { // only for PC TERMINAL devices
			String value = "";
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				value = this.ValidateExcelFielToString(valueFromCell);
			} else {
				if (valueFromCell.toUpperCase().equals("SI") || valueFromCell.toUpperCase().equals("YES")) {
					value = "1";
				} else {
					if (valueFromCell.toUpperCase().equals("NO")) {
						value = "0";
					} else {
						value = valueFromCell;
					}
				}
			}

			if (value.length() > 0 && value.length() < 2) {
				if (value.equals("1")) {
					this.objTerminal.setChkAllowCertificate(true);
				} else if (value.equals("0")) {
					this.objTerminal.setChkAllowCertificate(false);
				} else {
					this.objTerminal.addFieldError("CertificateIgnoreInd",
							Languages.getString("jsp.admin.customers.reps_add.error_CertAllow_invalid", sessionData.getLanguage()));
				}
			}
		}
	}

	protected void validateCertificateInstallRemainNum(String valueFromCell) {
		if (this.objTerminal.getTerminalType().equals("26")) { // only for PC TERMINAL devices
			String value = "";
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				value = this.ValidateExcelFielToString(valueFromCell);
			} else {
				value = valueFromCell;
			}

			if (value.length() > 0 && value.length() < 19) {
				this.objTerminal.setTxtAllowCertificate(value);
			}
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePCTerminalPassword(String valueFromCell, boolean isWebServiceAuthenticationEnabled, SessionData sessionData) {
		if (this.objTerminal.getTerminalType().equals("26") || this.objTerminal.getTerminalType().equals("40")
				|| this.objTerminal.getTerminalType().equals(DebisysConstants.WEB_SERVICE_KIOSK_TERMINAL_TYPE)
				|| this.objTerminal.getTerminalType().equals(DebisysConstants.SMS_PHONE_KIOSK_MERCHANT_TERMINAL_TYPE)
				|| this.objTerminal.getTerminalType().equals("56") || (this.objTerminal.getTerminalType().equals("23") && isWebServiceAuthenticationEnabled)) {
			// Only for PC Terminal and SMS devices
			String value = "";
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				value = this.ValidateExcelFielToString(valueFromCell);
			} else {
				value = valueFromCell;
			}

			if (value.length() > 3 && value.length() < 16) {
				this.objTerminal.setPcTermPassword(value);
			} else {
				this.objTerminal.addFieldError("PCTerminalPassword", Languages.getString("com.debisys.users.error_password_length", sessionData.getLanguage())
						+ " " + Languages.getString("com.debisys.users.error_username_length_16", sessionData.getLanguage()));
			}
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateClerkCodePass(String valueFromCell) {
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			String value = this.ValidateExcelFielToString(valueFromCell);
			this.objTerminal.setClerkCode(value);
		} else {
			this.objTerminal.setClerkCode(valueFromCell);
		}

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateClerkCodeUserName(String valueFromCell, SessionData sessionData) {
		String value = "";
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			value = this.ValidateExcelFielToString(valueFromCell);
		} else {
			value = valueFromCell;
		}

		if (value.length() >= 3) {
			this.objTerminal.setClerkName(value);
		} else {
			this.objTerminal.addFieldError("ClerkCodeUserName", Languages.getString("com.debisys.users.error_username_length", sessionData.getLanguage()));
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateMarketPlaceGroup(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {
			Integer value = (Integer) this.hashAplicationGroup.get(valueFromCell.trim().toUpperCase());
			if (value != null) {
				this.objTerminal.setMarketPlaceGroup(value);
			} else {
				this.objTerminal.addFieldError("MarketPlace Group",
						Languages.getString("jsp.admin.customers.reps_add.error_marketplace_group_invalid", sessionData.getLanguage()));
			}
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateMarketPlaceKey(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {
			Integer value = (Integer) this.hashKeysGroup.get(valueFromCell.trim().toUpperCase());
			if (value != null) {
				this.objTerminal.setMarketPlaceKey(value);
			} else {
				// this.objTerminal.addFieldError("MarketPlaceGroup",
				this.objTerminal.addFieldError("MarketPlaceKey",
						Languages.getString("jsp.admin.customers.reps_add.error_marketplace_key_invalid", sessionData.getLanguage()));
			}
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePurchaceType(String valueFromCell) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateEmailNotificationTerminal(String valueFromCell) {
		if (valueFromCell.length() > 0)
			this.objTerminal.setEmailNotification(valueFromCell);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateRepId(String valueFromCell, SessionData sessionData) {
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			String value = ValidateExcelFielToString(valueFromCell);
			String repname = (String) hashRepsOfIso.get(value.toString());
			if (repname != null) {
				objMerchant.setRepId(value.toString());
			} else {
				objMerchant.setRepId(value.toString());
				this.objMerchant.addFieldError("RepId", Languages.getString("jsp.admin.customers.merchants_edit.error_rep_id", sessionData.getLanguage()));
			}
		} else {
			this.objMerchant.addFieldError("RepId", Languages.getString("jsp.admin.customers.merchants_edit.error_rep_id", sessionData.getLanguage()));
		}
	}

	/**
	 * @param valueFromCell
	 * @param cell
	 * @param rowIdFile
	 */
	protected void validateMerchantChange(String valueFromCell, SessionData sessionData) {
		try {
			Integer val = new Integer((new Double(Double.parseDouble(valueFromCell))).intValue());

			if (merchantConsecutive == null) {
				merchantConsecutive = new Integer((new Double(Double.parseDouble(valueFromCell))).intValue());
			}

			Integer merchantConsecutiveAux = new Integer((new Double(Double.parseDouble(valueFromCell))).intValue());
			if (merchantConsecutive.equals(merchantConsecutiveAux)) {
				merchantConsecutive = merchantConsecutiveAux;
				this.objMerchant.setMerchantId(merchantConsecutive.toString());
			} else {
				addMerchantToArray();
				merchantConsecutive = merchantConsecutiveAux;
				this.objMerchant.setMerchantId(merchantConsecutive.toString());

			}

		} catch (Exception e) {

			this.addMessagesError(Languages.getString("jsp.admin.tools.validateMerchantConsecutive", sessionData.getLanguage()));
		}

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateTerminalType(String valueFromCell, SessionData sessionData) {
		Integer value = (Integer) this.hashTerminalTypes.get((valueFromCell.trim()).toUpperCase());
		if (value != null) {
			objTerminal.setTerminalType(value.toString());
			objTerminal.setTerminalDesc(valueFromCell);
		} else {
			this.objTerminal.addFieldError("validateTerminalType", Languages.getString("jsp.admin.tools.validateTerminalType", sessionData.getLanguage()));
		}

	}

	protected void validateProcessorType(String valueFromCell, SessionData sessionData) {
		Integer value = (Integer) this.hashProcessors.get(valueFromCell.trim().toUpperCase());
		if (value != null) {
			objMerchant.setProcessType(value);
		}

	}

	protected void validatePaymentType(String valueFromCell) {
		Integer value = (Integer) this.hashPayments.get(valueFromCell.trim().toUpperCase());
		if (value != null) {
			objMerchant.setPaymentType(value);
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validatePhyCounty(String valueFromCell) {
		this.objMerchant.setPhysCounty(valueFromCell);

	}

	public java.lang.StringBuffer getMessagesError() {
		return messagesError;
	}

	public void addMessagesError(String message) {
		messagesError.append(message + "<br>");
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateContactType(String valueFromCell) {
		Integer value = (Integer) this.hashContactTypes.get(valueFromCell.trim().toUpperCase());
		if (value != null) {
			this.objMerchant.objContact.setContactTypeID(value);
			this.objMerchant.setContactTypeId(value);
		} else
			this.objMerchant.addFieldError("ContactType", "Tipo Contacto Obligatorio.");

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateContactName(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {
			this.objMerchant.objContact.setContactName(valueFromCell);
			this.objMerchant.setContactName(valueFromCell);
		} else {
			this.objMerchant.addFieldError("ContactName", Languages.getString("jsp.admin.customers.reps_add.error_contact_first", sessionData.getLanguage()));
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateContactPhone(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {
			String temp = "";
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				temp = this.ValidateExcelFielToString(valueFromCell);
			} else {
				temp = valueFromCell;
			}
			this.objMerchant.objContact.setContactPhone(temp);
			this.objMerchant.setContactPhone(temp);
		} else {
			this.objMerchant.addFieldError("ContactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone", sessionData.getLanguage()));
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateContactFax(String valueFromCell) {
		String temp = "";
		if (valueFromCell.length() > 0) {
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				temp = this.ValidateExcelFielToString(valueFromCell);
			} else {
				temp = valueFromCell;
			}
		} else {
			temp = valueFromCell;
		}
		this.objMerchant.objContact.setContactFax(temp);
		this.objMerchant.setContactFax(temp);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateContactEmailMex(String valueFromCell) {
		this.objMerchant.objContact.setContactEmail(valueFromCell);
		this.objMerchant.setContactEmail(valueFromCell);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateContactDepartment(String valueFromCell) {
		this.objMerchant.objContact.setContactDepartment(valueFromCell);
		this.objMerchant.setContactDepartment(valueFromCell);

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateContactCellphone(String valueFromCell) {
		String temp = "";
		if (valueFromCell.length() > 0) {
			if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
				temp = this.ValidateExcelFielToString(valueFromCell);
			} else {
				temp = valueFromCell;
			}
		} else {
			temp = valueFromCell;
		}
		this.objMerchant.objContact.setContactCellphone(temp);
		this.objMerchant.setContactCellPhone(temp);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateBankName(String valueFromCell) {
		this.objMerchant.setBankName(valueFromCell);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateAccountType(String valueFromCell) {
		Integer value = (Integer) this.hashAccountTypes.get(valueFromCell.trim().toUpperCase());
		if (value != null)
			this.objMerchant.setAccountTypeId(value);
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateCLABE(String valueFromCell) {
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			String value = ValidateExcelFielToString(valueFromCell);
			this.objMerchant.setRoutingNumber(value);
		} else {
			this.objMerchant.setRoutingNumber(valueFromCell);
		}
	}

	/**
	 * @param valueFromCell
	 */
	protected void validateAccountNumber(String valueFromCell) {
		if (com.debisys.utils.NumberUtil.isNumeric(valueFromCell)) {
			String value = ValidateExcelFielToString(valueFromCell);
			this.objMerchant.setAccountNumber(value);
		} else {
			this.objMerchant.setAccountNumber(valueFromCell);
			;
		}

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateUserId(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 3) {
			this.objMerchant.setUserId(valueFromCell);
		} else {
			this.objMerchant.addFieldError("UserId", Languages.getString("com.debisys.users.error_username_length", sessionData.getLanguage()));

		}

	}

	/**
	 * @param valueFromCell
	 */
	protected void validateUserPassword(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 3) {
			this.objMerchant.setUserPassword(valueFromCell);
		} else {
			// this.objMerchant.addFieldError("UserId",
			// Languages.getString("com.debisys.users.error_password_length", sessionData.getLanguage()));
			this.objMerchant.addFieldError("UserPassword", Languages.getString("com.debisys.users.error_password_length", sessionData.getLanguage()));
		}
	}

	protected void validateNameClerk(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {
			this.objTerminal.setFirstName(valueFromCell);
		} else {
			this.objTerminal.addFieldError("NameClerk", Languages.getString("com.debisys.users.error_password_length", sessionData.getLanguage()));
		}
	}

	protected void validateLastName(String valueFromCell, SessionData sessionData) {
		if (valueFromCell.length() > 0) {
			this.objTerminal.setLastName(valueFromCell);
		} else {
			this.objTerminal.addFieldError("NameClerk", Languages.getString("com.debisys.users.error_password_length", sessionData.getLanguage()));
		}
	}

	/**
   *
   */
	protected void findTypesValues(SessionData sessionData) {
		Connection dbConn = null;
		PreparedStatement pst = null;
		// java.lang.StringBuffer comboType = new StringBuffer(500);
		String description = "";
		Integer value = -1;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn != null) {
				String strSQL = "select terminal_type,description from terminal_types with (nolock) where terminal_type not in (52,53,54)";
				pst = dbConn.prepareStatement(strSQL);
				ResultSet rs = pst.executeQuery();
				comboTypes.append("<tr>");
				comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_add_terminal.terminal_type", sessionData.getLanguage())
						+ toolTip(Languages.getString("jsp.bulkmerchant.terminal_type.tooltip", sessionData.getLanguage())) +"</td>");
				comboTypes.append(" <td>" + Languages.getString("jsp.admin.customer.terminal.groupcombo", sessionData.getLanguage())
						+ toolTip(Languages.getString("jsp.bulkmerchant.groupcombo.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append(" <td>" + Languages.getString("jsp.admin.customer.terminal.keycombo", sessionData.getLanguage())
						+ toolTip(Languages.getString("jsp.bulkmerchant.keycombo.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append("</tr>");
				comboTypes.append("<tr>");
				comboTypes.append("</tr>");
				comboTypes.append("<tr>");
				comboTypes.append("<td>");
				comboTypes.append("  <select id='terminal_types'>");
				while (rs.next()) {
					description = rs.getString("description").toUpperCase();
					value = rs.getInt(1);
					hashTerminalTypes.put(description, value);
					comboTypes.append("<option value='" + value + "'>" + description + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				rs.close();

				pst = dbConn.prepareStatement(sql_bundle.getString("getAplicationGroup"));
				rs = pst.executeQuery();
				comboTypes.append("<td>");
				comboTypes.append("  <select id='AplicationGroup'>");
				while (rs.next()) {
					hashAplicationGroup.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
					comboTypes.append("<option value='" + rs.getInt(2) + "'>" + rs.getString(1).trim().toUpperCase() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				rs.close();
				pst.close();

				pst = dbConn.prepareStatement(sql_bundle.getString("getKeysGroup"));
				rs = pst.executeQuery();
				comboTypes.append("<td>");
				comboTypes.append("  <select id='KeysGroup'>");
				while (rs.next()) {
					hashKeysGroup.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
					comboTypes.append("<option value='" + rs.getInt(2) + "'>" + rs.getString(1).trim().toUpperCase() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");

				rs.close();
				pst.close();

				comboTypes.append("<tr>");//
				comboTypes.append("<td>" + Languages.getString("jsp.admin.customers.terminal.pc_term_brand", sessionData.getLanguage()) 
						+ toolTip(Languages.getString("jsp.bulkmerchant.pcterminal_brand.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append("<td>" + Languages.getString("jsp.bulkmerchant.reps", sessionData.getLanguage())
						+ toolTip(Languages.getString("jsp.bulkmerchant.reps.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append("<td>" + Languages.getString("jsp.bulkmerchant.rate_plans", sessionData.getLanguage())
						+ toolTip(Languages.getString("jsp.bulkmerchant.rate_plans.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append("<td>" + Languages.getString("jsp.bulkmerchant.nrate_plans", sessionData.getLanguage())
						+ toolTip(Languages.getString("jsp.bulkmerchant.nrate_plans.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append("<td>" + Languages.getString("jsp.includes.menu.bulkmerchants.paymenttype", sessionData.getLanguage()) + "</td>");
				comboTypes.append("<td>" + Languages.getString("jsp.admin.customers.merchants_info.merchant_type", sessionData.getLanguage()) 
						+ toolTip(Languages.getString("jsp.bulkmerchant.credit_type.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append("</tr>");
				comboTypes.append("<tr>");
				comboTypes.append("</tr>");
				comboTypes.append("<tr>");
				// hashPCTErmBrand

				pst = dbConn.prepareStatement(sql_bundle.getString("getBrand"));
				pst.setLong(1, Long.parseLong(this.refId));
				rs = pst.executeQuery();
				comboTypes.append("<td>");
				comboTypes.append("  <select id='Brand'>");
				while (rs.next()) {
					hashPCTErmBrand.put(rs.getString(1).trim(), rs.getString(2).trim() + "_SEPARADOR_" + rs.getString(3).trim());
					comboTypes.append("<option value='" + rs.getString(2).trim() + "'>" + rs.getString(1).trim() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				
				String strRepType = com.debisys.utils.DebisysConstants.REP_TYPE_REP;
				String strSQLWhere = "";
				// String strAccessLevel = sessionData.getProperty("access_level");
				// String strRefId = sessionData.getProperty("ref_id");
				// strDistChainType = sessionData.getProperty("dist_chain_type");
				// login level
				if (this.accessLevel.equals(DebisysConstants.ISO)) {
					if (DistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
						if (strRepType.equals(DebisysConstants.REP_TYPE_AGENT)) {
							strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + this.refId + " and rep_id <> "
									+ this.refId;
						} else if (strRepType.equals(DebisysConstants.REP_TYPE_SUBAGENT)) {
							strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id "
									+ "in (select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + this.refId + ")";

							if (this.refId != null && !this.refId.equals("")) {
								strSQLWhere = strSQLWhere + " and iso_id=" + this.refId;
							}

						} else if (strRepType.equals(DebisysConstants.REP_TYPE_REP)) {
							strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_REP + " and ";
							strSQLWhere = strSQLWhere + " (iso_id in " +
							// subagents
									"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
									// agents
									"(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + this.refId + "))";

							strSQLWhere = strSQLWhere + ")";
						}
					} else if (DistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
						strSQLWhere = strSQLWhere + " iso_id=" + this.refId;
					}

				}

				pst = dbConn.prepareStatement("select rep_id ,businessname from reps with (nolock) where  " + strSQLWhere + " order by reps.businessname ASC");
				rs = pst.executeQuery();

				comboTypes.append("<td>");
				comboTypes.append("  <select id='reps'>");
				while (rs.next()) {
					hashRepsOfIso.put(rs.getString(1).trim().toUpperCase(), rs.getString(2));
					//comboTypes.append("<option value='" + rs.getString(2) + "'>" + rs.getString(2) + "-" + rs.getString(1).trim().toUpperCase() + "</option>");
					comboTypes.append("<option value='" + rs.getString(2) + "'>" + rs.getString(2) + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");

				pst = dbConn.prepareStatement("select rep_id ,businessname, email from reps with (nolock) where  " + strSQLWhere
						+ "  and CHARINDEX('@',email)>0 order by reps.businessname ASC");
				rs = pst.executeQuery();
				while (rs.next()) {
					hashRepsMails.put(rs.getString(1).trim().toUpperCase(), rs.getString(3));
				}				
				
				pst = dbConn.prepareStatement(sql_bundle.getString("getPlans"));
				pst.setLong(1, Long.parseLong(this.refId));
				rs = pst.executeQuery();
				comboTypes.append("<td>");
				comboTypes.append("  <select id='Plans'>");
				while (rs.next()) {
					hashPlans.put(rs.getString(1).trim().toUpperCase(), rs.getLong(2));
					comboTypes.append("<option value='" + rs.getString(2) + "'>" + rs.getString(1).trim().toUpperCase() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				rs.close();
				pst.close();
				
				String query = "SELECT DISTINCT b.RatePlanID, b.name FROM dbo.rep_iso_rate_plan_glue AS a WITH(NOLOCK) INNER JOIN RatePlan AS b WITH(NOLOCK) ON a.iso_rate_plan_id = b.RatePlanID WHERE a.isNewRatePlanModel=1 AND b.isTemplate = 0 AND a.rep_id= ?";
				pst = dbConn.prepareStatement(query);
				pst.setLong(1, Long.parseLong(this.refId));
				rs = pst.executeQuery();
				comboTypes.append("<td>");
				comboTypes.append("  <select id='NewPlans'>");
				while (rs.next()) {
					hashNewPlans.put(rs.getString(2).trim().toUpperCase(), rs.getLong(1));
					comboTypes.append("<option value='" + rs.getString(1) + "'>" + rs.getString(2).trim().toUpperCase() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				rs.close();
				pst.close();

				pst = dbConn.prepareStatement(sql_bundle.getString("processors"));
				rs = pst.executeQuery();
				while (rs.next()) {
					hashProcessors.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
				}
				rs.close();
				pst.close();

				pst = dbConn.prepareStatement(sql_bundle.getString("paymenttypes"));
				rs = pst.executeQuery();
				comboTypes.append("<td>");
				comboTypes.append("  <select id='paymenttypes'>");
				while (rs.next()) {
					hashPayments.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
					comboTypes.append("<option value='" + rs.getString(2) + "'>" + rs.getString(1).trim().toUpperCase() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				rs.close();
				pst.close();

				pst = dbConn.prepareStatement(sql_bundle.getString("credittypes"));
				rs = pst.executeQuery();
				comboTypes.append("<td>");
				comboTypes.append("  <select id='paymenttypes'>");
				while (rs.next()) {
					hashCreditTypes.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
					comboTypes.append("<option value='" + rs.getString(2) + "'>" + rs.getString(1).trim().toUpperCase() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				comboTypes.append("</tr>");
				rs.close();
				pst.close();

				pst = dbConn.prepareStatement(sql_bundle.getString("getRoutes"));
				rs = pst.executeQuery();
				while (rs.next()) {
					hashRoute.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
				}
				rs.close();
				pst.close();

				/** ************************************************************************************************* */
				/** ************************************************************************************************* */
				/**
				 * ***********************************SPECIAL QUERYS JUST FOR MEXICO
				 */
				if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {

					pst = dbConn.prepareStatement(sql_bundle.getString("getSegments"));
					rs = pst.executeQuery();

					comboTypes.append("<tr>");//
					comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_edit.merchantsegment", sessionData.getLanguage()) + "</td>");
					comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_edit.businesstype", sessionData.getLanguage()) + "</td>");
					comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_edit.Region", sessionData.getLanguage()) + "</td>");
					comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_edit.accounttype", sessionData.getLanguage()) + "</td>");
					comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_edit.contacttype", sessionData.getLanguage()) + "</td>");
					comboTypes.append("</tr>");

					comboTypes.append("<tr>");
					comboTypes.append("<td>");
					comboTypes.append("  <select id='Segments'>");
					while (rs.next()) {
						String descValue = Languages
								.getString("jsp.admin.customers.merchants_edit.merchantsegment_" + rs.getString(1), sessionData.getLanguage()).trim()
								.toUpperCase();
						hashSegmentsTypes.put(descValue, rs.getInt(2));
						comboTypes.append("<option value='" + rs.getInt(2) + "'>" + descValue + "</option>");
					}
					comboTypes.append("  </select>");
					comboTypes.append("</td>");
					rs.close();
					pst.close();

					pst = dbConn.prepareStatement(sql_bundle.getString("getBusinessTypes"));
					rs = pst.executeQuery();
					comboTypes.append("<td>");
					comboTypes.append("  <select id='BusinessTypes'>");
					while (rs.next()) {
						hashBusinessTypes.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
						comboTypes.append("<option value='" + rs.getInt(2) + "'>" + rs.getString(1).trim().toUpperCase() + "</option>");
					}
					comboTypes.append("  </select>");
					comboTypes.append("</td>");
					rs.close();
					pst.close();

					pst = dbConn.prepareStatement(sql_bundle.getString("getRegionTelcel"));
					rs = pst.executeQuery();
					comboTypes.append("<td>");
					comboTypes.append("  <select id='RegionTelcel'>");
					while (rs.next()) {
						hashRegionTelcel.put(rs.getString(1).trim().toUpperCase(), rs.getInt(2));
						comboTypes.append("<option value='" + rs.getInt(2) + "'>" + rs.getString(1).trim().toUpperCase() + "</option>");
					}
					comboTypes.append("  </select>");
					comboTypes.append("</td>");
					rs.close();
					pst.close();

					pst = dbConn.prepareStatement(sql_bundle.getString("getAccountTypes"));
					rs = pst.executeQuery();
					comboTypes.append("<td>");
					comboTypes.append("  <select id='AccountTypes'>");
					while (rs.next()) {
						String descValue = Languages.getString("jsp.admin.customers.merchants_edit.accounttype_" + rs.getString(1), sessionData.getLanguage())
								.trim().toUpperCase();
						hashAccountTypes.put(descValue, rs.getInt(2));
						comboTypes.append("<option value='" + rs.getInt(2) + "'>" + descValue + "</option>");
					}
					comboTypes.append("  </select>");
					comboTypes.append("</td>");
					rs.close();
					pst.close();

					pst = dbConn.prepareStatement(sql_bundle.getString("getContactTypes"));
					rs = pst.executeQuery();
					comboTypes.append("<td>");
					comboTypes.append("  <select id='ContactTypes'>");
					while (rs.next()) {
						String descValue = Languages.getString("jsp.admin.customers.merchants_edit.contacttype_" + rs.getString(1), sessionData.getLanguage())
								.trim().toUpperCase();
						hashContactTypes.put(descValue, rs.getInt(2));
						comboTypes.append("<option value='" + rs.getInt(2) + "'>" + descValue + "</option>");
					}
					comboTypes.append("  </select>");
					comboTypes.append("</td>");
					rs.close();
					pst.close();
					comboTypes.append("</tr>");

					comboTypes.append("<tr>");//
					comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_edit.Invoice_type", sessionData.getLanguage()) + "</td>");
					comboTypes.append(" <td>" + Languages.getString("jsp.admin.customers.merchants_edit.merchant_classification", sessionData.getLanguage())
							+ "</td>");
					comboTypes.append("</tr>");

					comboTypes.append("<tr>");//
					// invoiceType
					Iterator itInvoiceTypes = Merchant.getInvoiceTypes().iterator();
					comboTypes.append("<td>");
					comboTypes.append("  <select id='InvoiceTypes'>");

					while (itInvoiceTypes.hasNext()) {
						java.util.Vector vecTemp = null;
						vecTemp = (Vector) itInvoiceTypes.next();
						String strId = vecTemp.get(0).toString().trim().toUpperCase();
						String strName = vecTemp.get(1).toString().trim().toUpperCase();
						hashInvoiceTypes.put(strName, new Integer(strId));
						comboTypes.append("<option value=\"" + strId + "\">" + strName + "</option>");

					}
					comboTypes.append("</select>");
					comboTypes.append("</td>");

					// Merchant Classifications
					Iterator itMerchantClassifications = Merchant.getMerchantClassifications(sessionData).iterator();
					comboTypes.append("<td>");
					comboTypes.append("  <select id='MerchantClassifications'>");

					while (itMerchantClassifications.hasNext()) {
						java.util.Vector vecTemp = null;
						vecTemp = (Vector) itMerchantClassifications.next();
						String strId = vecTemp.get(0).toString().trim().toUpperCase();
						String strName = vecTemp.get(1).toString().trim().toUpperCase();
						hashMerchantClassifications.put(strName, new Integer(strId));
						comboTypes.append("<option value=\"" + strId + "\">" + strName + "</option>");
					}
					comboTypes.append("</select>");
					comboTypes.append("</td>");

					//

					comboTypes.append("</tr>");
					/** ************************************************************************************************* */
					/** ************************************************************************************************* */
				}

				// /////////////////////////////////// DBSY-907 Adding available countries to examples

				comboTypes.append("<tr>");
				if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
					comboTypes.append(" <td>" + Languages.getString("jsp.includes.menu.bulkmerchants.states", sessionData.getLanguage())
							+ toolTip(Languages.getString("jsp.bulkmerchant.state.tooltip", sessionData.getLanguage())) + "</td>");
				}
				comboTypes.append(" <td>" + Languages.getString("jsp.admin.reports.invoice_merchants.country", sessionData.getLanguage())
						+ toolTip(Languages.getString("jsp.bulkmerchant.country.tooltip", sessionData.getLanguage())) + "</td>");
				comboTypes.append("</tr>");
				comboTypes.append("<tr>");
				pst = dbConn.prepareStatement(sql_bundle.getString("getStates"));
				if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
					pst.setString(1, "MEXICO");
				else
					pst.setString(1, "USA");

				rs = pst.executeQuery();

				if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
					comboTypes.append("<td>");
					comboTypes.append("  <select id='States'>");
				}

				while (rs.next()) {
					hashStates.put(rs.getString(2).trim().toUpperCase(), rs.getString(1));
					if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
						comboTypes.append("<option value='" + rs.getString(3) + "'>" + rs.getString(2).trim().toUpperCase() + "</option>");
					}
				}
				if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
					comboTypes.append("  </select>");
					comboTypes.append("</td>");
				}
				comboTypes.append("<td>");
				HashMap validcountries = CountryInfo.getAllCountriesID();
				comboTypes.append("  <select id='Countries'>");
				Set set = validcountries.entrySet();
				Iterator i = set.iterator();
				while (i.hasNext()) {
					Map.Entry me = (Map.Entry) i.next();
					comboTypes.append("<option value='" + me.getKey() + "'>" + me.getValue() + "</option>");
				}
				comboTypes.append("  </select>");
				comboTypes.append("</td>");
				comboTypes.append("</tr>");

				comboTypes.append("</tr>");
				// //////////////////////////////////
				rs.close();
				pst.close();
				
				comboTypes.append("<tr>");
				if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
					comboTypes.append("<td colspan=\"3\">" + Languages.getString("jsp.bulkmerchant.timezone", sessionData.getLanguage())
							+ toolTip(Languages.getString("jsp.bulkmerchant.timezone.tooltip", sessionData.getLanguage())) + "</td>");
				}
				comboTypes.append("</tr>");
				comboTypes.append("<tr>");
				comboTypes.append("<td colspan=\"3\">"+getTimezoneCombo()+"</td>");
				comboTypes.append("</tr>");
				
			} else {
				cat.error("Can't get database connection");
				// throw new TerminalException();
			}
		} catch (Exception e) {
			cat.error("Error during findTerminalTypes", e);

		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}

		}
	}
	
	private String getTimezoneCombo(){
		StringBuilder sb = new StringBuilder();
		sb.append("<select>");
		Iterator<Vector<String>> itList;
		try {
			itList = TimeZone.getTimeZoneList().iterator();
			while ( itList.hasNext() ){
				Vector vItem = itList.next();
				sb.append("<option value=\""+vItem.get(0).toString()+"\">"+vItem.get(2).toString()+" ["+vItem.get(1).toString()+"]</option>");
				vItem.clear();
				vItem = null;
			}
			itList = null;
			sb.append("</select>");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sb.toString();
	}

	private String toolTip(String tip) {
		return "<span class=\"toolTip\" title=\""+tip+"\">";
	}
}
