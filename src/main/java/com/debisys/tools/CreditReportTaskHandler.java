package com.debisys.tools;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

@SuppressWarnings("deprecation")
public class  CreditReportTaskHandler
{

	// log4j logging
	public static Category cat = Category.getInstance(CreditReportTaskHandler.class);
	
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");

	  private SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	  private SimpleDateFormat oldformatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
	private Hashtable validationErrors = new Hashtable();
	private String email="";
	
	private String jobid;
	private String jobstartDate = "";
	private String selectedagent="";
	private String selectedsub="";
	private String entity="";

	public void setSelectedagent(String selectedagent)
	{
		this.selectedagent = selectedagent;
	}
	public void setSelectedsub(String selectedsub)
	{
		this.selectedsub = selectedsub;
	}

	
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}


	public String getEntity()
	{
		return entity;
	}
	public void setEntity(String entity)
	{
		this.entity = entity;
	}
	
	public String getselectedsub(){
		return this.selectedsub;
	}

	public String getselectedagent(){
		return this.selectedagent;
	}
	
	
	public String getjobstartDate(){
		return StringUtil.toString(this.jobstartDate);
	}
	


	public String getjobid(){
		return StringUtil.toString(this.jobid);
	}

	public void setjobid(String temp){
		this.jobid = temp;
	}
	
	public void setjobstartDate(String temp){
		this.jobstartDate = temp;
	}
	
	
	public String getselectedsubName(String selectedsub)
	{
		Connection dbConn = null;
		PreparedStatement pstmt= null;
		 String strSQL = sql_bundle.getString("getEntityName");
		 String subagentName="";

		    try
		    {
		    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
		    	if (dbConn == null)
		    	{
		    		cat.error("getselectedsubName: Can't get database connection");
		    		 
		    	}
		    	cat.debug(strSQL);

		    	pstmt = dbConn.prepareStatement(strSQL);
		    	pstmt.setLong(1, Long.valueOf(selectedsub));
		      	ResultSet rs = pstmt.executeQuery();
		      	
		    	while (rs.next())
		      	{
		      		
		    		subagentName=rs.getString("businessname");     		
		      	}
		      	rs.close();
		      	pstmt.close();
		    }
		    catch (Exception e)
		    {
		    	cat.error("getCreditjob => Error during getCreditjob", e);
		    }
		    finally
		    {
		    	try
		    	{
		    		if (pstmt != null)
		    			pstmt.close();
		    		Torque.closeConnection(dbConn);
		    	}
		    	catch (Exception e)
		    	{
		    		cat.error("gets2kjob => Error during closeConnection ", e);
		    	}
		    }
		    return subagentName;
		}
	
	
		  /**
	   * Returns a hash of errors.
	   * 
	   * @return Hash of errors.
	   */

	  public Hashtable getErrors()
	  {
	    return this.validationErrors;
	  }

	  public void clearvalidation()
	  {
		  this.validationErrors.clear();
	  }
	  
	 public boolean validateDateRange(SessionData sessionData)
	  {
	    this.validationErrors.clear();
	    boolean valid = true;

	   
	     
	      if ((this.jobstartDate == null) || (this.jobstartDate.length() == 0))
	      {
	        this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error5", sessionData.getLanguage()));
	        valid = false;
	      }
	      else if (!DateUtil.isValidDateTime(this.jobstartDate))
	      {
	        this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error6", sessionData.getLanguage()));
	        valid = false;
	      }
	    
	    if ((valid))
	    {

	      Date dtStartDate = new Date();
	      Date dtEndDate = new Date();
	      Date dtJobDate = new Date();
	      Date dtcurrentday = new Date();
	      String currentday = this.formatter.format(Calendar.getInstance().getTime());
	      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	      formatter.setLenient(false);
	      
	      try
	      {
	    	  dtJobDate = formatter.parse(this.jobstartDate);
	    	  dtcurrentday = formatter.parse(currentday);
	    	  
	      }
	      catch (ParseException ex)
	      {
	        this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error6", sessionData.getLanguage()));
	        valid = false;
	      }
	      if (dtStartDate.after(dtEndDate))
	      {
	        this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error7", sessionData.getLanguage()));
	        valid = false;
	      }
	      if (dtEndDate.after(dtJobDate))
	      {
	        this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error8", sessionData.getLanguage()));
	        valid = false;
	      }
	      if (dtcurrentday.after(dtJobDate) || dtcurrentday.equals(dtJobDate))
	      {
	        this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error10", sessionData.getLanguage()));
	        valid = false;
	      }

	    }

	    return valid;
	  }
	 
	 
	 public void addFieldError(String fieldname, String error)
	  {
	    this.validationErrors.put(fieldname, error);
	  }

	  public String getFieldError(String fieldname)
	  {
	    return ((String) this.validationErrors.get(fieldname));
	  }
	  
	public String inserttask(String isoid, String agentID,String subID,String entity, String jobdate, String email,
			int status,String strAccessLevel) throws Exception{

		Vector<String> vTimeZoneFilterJobDate = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, jobdate, jobdate, false);
		if (agentID == null || agentID.equals("")) {
			agentID = "0";
		}
		if (subID == null || subID.equals("")) {
			subID = "0";
		}
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("insertCreditBalanceTask");
		String result = "";
		int errorcode = -5;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}
			cat.debug(strSQL);

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setString(2, vTimeZoneFilterJobDate.get(0).toString());
			cstmt.setInt(3, status);
			cstmt.setLong(4, Long.valueOf(agentID));
			cstmt.setLong(5, Long.valueOf(subID));
			cstmt.setString(6, entity);
			cstmt.setString(7, email);
			cstmt.registerOutParameter(8, Types.INTEGER);
			cstmt.setInt(8, 0);
			cstmt.registerOutParameter(9, Types.NUMERIC);
			cstmt.setLong(9, 0);
			cstmt.execute();
			errorcode = cstmt.getInt(8);

			result = String.valueOf(cstmt.getLong(8));
			if (errorcode == -1) {
				result = "-1";
			} else if (errorcode == -4) {
				result = "-4";
			}
			cstmt.close();
		} catch (Exception e) {
			cat.error("Errorcode" + errorcode + " during inserttask", e);
			return "-1";
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("inserttask: Error during closeConnection", e);
				result = "-1";
			}
		}
		return result;
		
	}
	
	public int edittask(String isoid, String agentID,String subID,String entity, String jobdate, String email,
			int status,String strAccessLevel,String jobid) throws Exception {

		cat.debug("isoid" + isoid);
		cat.debug("jobid" + jobid);
		cat.debug("jobdate" + jobdate);
		cat.debug("strAccessLevel" + strAccessLevel);
		if (agentID == null || agentID.equals("")) {
			agentID = "0";
		}
		if (subID == null || subID.equals("")) {
			subID = "0";
		}
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("updateCreditBalanceTask");
		Vector<String> vTimeZoneFilterJobDate = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, jobdate, jobdate, false);

		int errorcode = -1;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}
			cat.debug(strSQL);

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setString(2, jobid);
			cstmt.setString(3, vTimeZoneFilterJobDate.get(0).toString());
			cstmt.setLong(4, Long.valueOf(agentID));
			cstmt.setLong(5, Long.valueOf(subID));
			cstmt.setString(6, entity);
			cstmt.setString(7, email);
			cstmt.registerOutParameter(8, Types.INTEGER);
			cstmt.setInt(8, 0);
			cstmt.execute();
			errorcode = cstmt.getInt(8);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Errorcode" + errorcode + " during edittask", e);
			return -1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("edittask: Error during closeConnection", e);
				errorcode = -1;
			}
		}
		return errorcode;
	}

	public int deletetask(String jobid){
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("deleteCreditBalanceTask");
		int errorcode = -1;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}
			cat.debug(strSQL);

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(jobid));
			cstmt.registerOutParameter(2, Types.INTEGER);
			cstmt.setInt(2, 0);
			cstmt.execute();
			errorcode = cstmt.getInt(2);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during deletetask", e);
			return -1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("deletetask: Error during closeConnection", e);
				errorcode = -1;
			}
		}
		return errorcode;
		
	}
	

	/***
	 * This will get all tasks info specific to a jobid
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getCreditjob(String jobid,String strAccessLevel,String isoid) 
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector   vecResults = new Vector();
	    
	    String strSQL = sql_bundle.getString("getCreditBalanceJobById");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    	}

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, jobid);
	      	ResultSet rs = pstmt.executeQuery();
	    	while (rs.next())
	      	{
	      		this.setjobid(String.valueOf(rs.getLong("jobid")));
	      		String jstart = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, rs.getString("jobdate"),
  						rs.getString("jobdate"), true).get(0).toString(); 
	      		this.setjobstartDate( this.formatter.format( this.oldformatter.parse(jstart).getTime() ));
	      			String email=rs.getString("email");
	      		this.setEmail(email);
	      		long agentid=rs.getLong("agentid");
	      		long subagentid=rs.getLong("subagentid");
	      		this.setSelectedagent(String.valueOf(agentid));
	      		this.setSelectedsub(String.valueOf(subagentid));
	      		this.setEntity(rs.getString("entity"));	      		
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("getCreditjob => Error during getCreditjob", e);
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("gets2kjob => Error during closeConnection ", e);
	    	}
	    }
	    return vecResults;
	}
	

	/***
	 * 
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public String getjobstatus(String isoId,String filter) 
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null; 
	    String result="";
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
		    String strSQL = sql_bundle.getString("getjobstatus");
	    	if (dbConn == null)
	    	{
	    		CreditReportTaskHandler.cat.error("Can't get database connection");
	    		result ="ERROR";
	    	}
	    	CreditReportTaskHandler.cat.debug("sql=" +strSQL);
	    	CreditReportTaskHandler.cat.debug("isoId=" +isoId);
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();
	      	
	      	int s = -1;
	      	if(filter.equals("Scheduled")){
      			s = 0;
      		}
      		else if(filter.equals("Processing")){
      			s = 1;
      		}
	      	else if(filter.equals("Completed")){
      			s = 2;
	      	}
	      	else if(filter.equals("Completed with Warnings")){
      			s=3;
      		}
      		else if(filter.equals("Failed")){
      			s=4;
      		}
	      	
	      	if(s==-1){
	      	while (rs.next())
	      	{
	      		result += String.valueOf(rs.getLong("jobid")) + ",";
	      		result += String.valueOf(rs.getInt("status")) + ";";
	      	}
	      	}else{
	      		while (rs.next())
		      	{
	      			int status = rs.getInt("status");
		      		if(status == s){
	      			result += String.valueOf(rs.getLong("jobid")) + ",";
		      		result += String.valueOf(status) + ";";
		      		}
		      	}	
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	CreditReportTaskHandler.cat.error("getjobstatus => Error during getjobstatus", e);
	    	result ="ERROR";
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{	
	    		result ="ERROR";
	    		CreditReportTaskHandler.cat.error("getjobstatus => Error during closeConnection ", e);
	    	}
	    }
	    CreditReportTaskHandler.cat.debug("stringgenerated=" + result);
	    return result;
	}

	/***
	 * 
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector getCreditJoblist(String isoId,String strAccessLevel) 
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getCreditBalancejoblist");
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}
	    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	Calendar cal= Calendar.getInstance();
	    	cal.add(Calendar.DATE, -7);
	    	String jobdate=formatter.format(cal.getTime());
	    	Vector vTimeZoneFilterJobDate = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoId, jobdate, jobdate, false);
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, isoId);
	    	pstmt.setString(2,vTimeZoneFilterJobDate.get(0).toString());
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getLong("jobid"));
	      		String jstart = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoId, rs.getString("jobdate"),
  						rs.getString("jobdate"), true).get(0).toString();  
	      		vecTemp.add(  this.formatter.format( this.oldformatter.parse(jstart).getTime() ));
	      		vecTemp.add(rs.getInt("status"));
	      		vecTemp.add(rs.getString("entity"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("gets2klist => Error during gets2klist", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("gets2klist => Error during closeConnection ", e);
	    	}
	    }
	    return vecResults;
	}
		
}