package com.debisys.tools;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.tools.dto.S2kStockCode;
import com.emida.utils.dbUtils.TorqueHelper;


public class s2k
{
	// log4j logging
	public static Category cat = Category.getInstance(s2k.class);
	
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");
	
	/**
	 * This will delete a stockcode and product ID under the ID of the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return 0,1,2 - Error 
	 * 		   5 Stock code deleted successfully
 	 * @throws
	 */
	public int deletestockcode(String isoid, String productid, String stockcode) {
		int result = 0;
		Connection dbConn = null;
		PreparedStatement cstmt = null;
		String strSQL = sql_bundle.getString("deletestockcode");
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}
			cat.debug(strSQL);

			cat.debug(isoid);
			cat.debug(productid);
			cat.debug(stockcode);
			if (isoid == null || productid == null)
				return 2;

			cstmt = dbConn.prepareStatement(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setInt(2, Integer.parseInt(productid));
			cstmt.execute();
			cstmt.close();
			result = 5;
		} catch (Exception e) {
			cat.error("Error during deletestockcode", e);
			return 1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("deletestockcode: Error during closeConnection", e);
				result = 0;
			}
		}
		return result;
	}
	
	
	/**
	 * This will edit existing stockcode and product ID under the ID of the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return 0,1,2 - Error 
	 * 		   3 ProductID not found,
	 * 		   4 ProductID already exists
	 * 		   5 Stock code added successfully
 	 * @throws
	 */
	public int editstoccode(String isoid ,String productid ,String stockcode,String oldpid ,String sc, boolean excludeTax) {
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("editstockcode");
		int result;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}

			cat.debug(strSQL);
			if (isoid == null || productid == null || stockcode == null)
				return 2;

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setInt(2, Integer.parseInt(productid));
			cstmt.setString(3, stockcode);
			cstmt.setInt(4, Integer.parseInt(oldpid));
			cstmt.setString(5, sc);
			cstmt.setBoolean(6, excludeTax);
			cstmt.registerOutParameter(7, Types.INTEGER);
			cstmt.setInt(7, 0);
			cstmt.execute();
			result = cstmt.getInt(7);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during editstockcode", e);
			return 1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("editstockcode, Error during closeConnection", e);
				result = 0;
			}
		}
		return result;
	}
	
	/**
	 * This will insert a stockcode and product ID under the ID of the ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return 0,1,2 - Error 
	 * 		   3 ProductID not found,
	 * 		   4 ProductID already exists
	 * 		   5 Stock code added successfully
 	 * @throws
	 */
	public int addstoccode(String isoid, String productid, String stockcode, boolean excludeTax) {
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("insertstockcode");
		int result;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}
			cat.debug(strSQL);

			if (isoid == null || productid == null || stockcode == null)
				return 2;

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setInt(2, Integer.parseInt(productid));
			cstmt.setString(3, stockcode);
			cstmt.setBoolean(4, excludeTax);
			cstmt.registerOutParameter(5, Types.INTEGER);
			cstmt.setInt(5, 0);
			cstmt.execute();
			result = cstmt.getInt(5);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during addstoccode", e);
			return 1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
				result = 0;
			}
		}
		return result;
	}
	


	/***
	 * This will get all of product ids and description with valid stock code
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public String getdescription(String pid) 
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String Results = null;
	    String strSQL = sql_bundle.getString("getdescription");
	    
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, pid);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Results = rs.getString("description").trim();
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("gets2kstockcode => Error during gets2kstockcode", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("gets2kstockcode => Error during closeConnection ", e);
	    	}
	    }
	    return Results;
	}
	/***
	 * This will get all of product ids and description with valid stock code
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public List<S2kStockCode> gets2kstockcode(String isoId) {
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    ArrayList<S2kStockCode> vecResults = new ArrayList<S2kStockCode>();
	    String strSQL = sql_bundle.getString("getstockcode");
	    
	    try{
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, isoId);
	      	rs = pstmt.executeQuery();

	      	while (rs.next()){      		
	      		S2kStockCode stock = new S2kStockCode();
	      		stock.setProductId(rs.getString("productid"));
	      		stock.setDescription(rs.getString("description"));
	      		stock.setStockCode(rs.getString("stockcode"));
	      		stock.setExcludeTax(rs.getBoolean("excludeTax"));
	      		vecResults.add(stock);
	      	}
	    } catch (Exception e){
	    	cat.error("gets2kstockcode => Error during gets2kstockcode", e);
	    } finally {
	    	TorqueHelper.closeConnection(dbConn, pstmt, rs);
	    }
	    return vecResults;
	}
	
	
	
	

	/***
	 * This will get all of salesman ids and names
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector gets2ksalesman(String isoId) 
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getsalesmanid");
	    
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getString("salesmanid"));
	      		vecTemp.add(rs.getString("salesmanname"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("gets2kstockcode => Error during gets2kstockcode", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("gets2kstockcode => Error during closeConnection ", e);
	    	}
	    }
	    return vecResults;
	}

	
	/***
	 * This will get all of salesman name by his id
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public String gets2ksalesmanbyid(String isoId, String id) {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String vecResults = "";
		String strSQL = sql_bundle.getString("getsalesmanidbyid");

		try {
			dbConn = Torque.getConnection(sql_bundle
					.getString("pkgAlternateDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				return vecResults;
			}

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, isoId);
			pstmt.setString(2, id);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				vecResults = rs.getString("salesmanname");
			}
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			cat.error("gets2kstockcode => Error during gets2kstockcode", e);
			return vecResults;
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("gets2kstockcode => Error during closeConnection ", e);
			}
		}
		return vecResults;
	}

	

	/***
	 * This will get all of salesman name by his name
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public String gets2kverify(String isoId, String id, String name) {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String vecResults = "False";
		String strSQL = sql_bundle.getString("verifysalesman");

		try {
			dbConn = Torque.getConnection(sql_bundle
					.getString("pkgAlternateDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				return "False";
			}

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, isoId);
			pstmt.setString(2, id);
			pstmt.setString(3, name);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				vecResults = rs.getString("Response");
			}
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			cat.error("gets2kstockcode => Error during gets2kstockcode", e);
			return "False";
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("gets2kstockcode => Error during closeConnection ", e);
				vecResults = "False";
			}
		}
		return vecResults;
	}
	

	
	/***
	 * This will get all of salesman name by his name
	 * @param request
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public String gets2ksalesmanbyname(String isoId, String name) {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String vecResults = "";
		String strSQL = sql_bundle.getString("getsalesmanidbyname");

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				return vecResults;
			}

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, isoId);
			pstmt.setString(2, name);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				vecResults = rs.getString("salesmanid");
			}
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			cat.error("gets2kstockcode => Error during gets2kstockcode", e);
			return vecResults;
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("gets2kstockcode => Error during closeConnection ", e);
			}
		}
		return vecResults;
	}
	

	/**
	 * This will delete a salesman under the ISO currently logged on.
	 * @return 0,1,2 - Error 
	 * 		   5 deleted successfully
 	 * @throws
	 */
	public int deletesalesman(String isoid, String sid, String sn) {
		int result = 0;
		Connection dbConn = null;
		PreparedStatement cstmt = null;
		String strSQL = sql_bundle.getString("deletesalesman");
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}
			cat.debug(strSQL);

			cat.debug(isoid);
			cat.debug(sid);
			cat.debug(sn);
			if (isoid == null || sid == null)
				return 2;

			cstmt = dbConn.prepareStatement(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setString(2, sid);
			cstmt.execute();
			cstmt.close();
			result = 5;
		} catch (Exception e) {
			cat.error("Error during deletesalesman", e);
			return 1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("deletesalesman: Error during closeConnection", e);
				result = 0;
			}
		}
		return result;
	}
	
	
	/**
	 * This will edit existing salesman ID and name under the ISO currently logged on.
	 * @return 0,1,2 - Error 
	 * 		   4 Salesman ID already exists
	 * 		   5 edited successfully
 	 * @throws
	 */
	public int editsalesman(String isoid, String sid, String sn, String oldsid, String oldsn) {
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("editsalesman");
		int result = 0;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}

			cat.debug(strSQL);
			if (isoid == null || sid == null || sn == null)
				return 2;

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setString(2, sid);
			cstmt.setString(3, sn);
			cstmt.setString(4, oldsid);
			cstmt.setString(5, oldsn);
			cstmt.registerOutParameter(6, Types.INTEGER);
			cstmt.setInt(6, 0);
			cstmt.execute();
			result = cstmt.getInt(6);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during editsalesman", e);
			return 1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("editsalesman, Error during closeConnection", e);
			}
		}
		return result;
	}
	
	/**
	 * This will insert a salesman ID and name under ISO currently logged on.
	 * @param request
	 * @param sessionData
	 * @return 0,1,2 - Error 
	 * 		   4 salesman ID already exists
	 * 		   5 salesman added successfully
 	 * @throws
	 */
	public int addsalesman(String isoid, String sid, String sn) {
		Connection dbConn = null;
		CallableStatement cstmt = null;
		String strSQL = sql_bundle.getString("insertsalesman");
		int result = 0;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");

			}
			cat.debug(strSQL);

			if (isoid == null || sid == null || sn == null)
				return 2;

			cstmt = dbConn.prepareCall(strSQL);
			cstmt.setLong(1, Long.parseLong(isoid));
			cstmt.setString(2, sid);
			cstmt.setString(3, sn);
			cstmt.registerOutParameter(4, Types.INTEGER);
			cstmt.setInt(4, 0);
			cstmt.execute();
			result = cstmt.getInt(4);
			cstmt.close();
		} catch (Exception e) {
			cat.error("Error during addsalesman", e);
			return 1;
		} finally {
			try {
				if (cstmt != null)
					cstmt.close();
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return result;
	}
}