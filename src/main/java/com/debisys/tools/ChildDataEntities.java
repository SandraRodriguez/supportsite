package com.debisys.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;

@SuppressWarnings("deprecation")
public class ChildDataEntities {	
	
	public static Category cat = Category.getInstance(ChildDataEntities.class);
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");
	private final List<String> id;
	private final List<String> name;
	/***
	 * @param 
	 * @return
	 * @throws ReportException
	 */
	public ChildDataEntities(String[] idNums,int queryType, String agentTooLong, String subAgentTooLong, String repTooLong) 
	{	
		id = new ArrayList<String>();
		name = new ArrayList<String>();
		
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    //String strSQL = sql_bundle.getString("getallsku");
	    String strSQL = null;
	    if(queryType == 0){
	    	strSQL = "select r.rep_id, r.businessname from reps r with(nolock) where r.iso_id = ? ";
	    	if(idNums != null && idNums.length > 1){
	    		for(int i = 0; i<idNums.length-1;i++){
	    			strSQL += " OR r.iso_id = ?";
	    		}
	    	}
	    	strSQL += " ORDER by r.businessname ";
	    }else if(queryType == -1){
	    	strSQL = "SELECT m.merchant_id, m.legal_businessname FROM merchants m with(nolock) WHERE m.rep_id = ?";
	    	if(idNums != null  && idNums.length > 1){
	    		for(int i =0; i< idNums.length-1;i++){
		    		strSQL += " OR m.rep_id = ?";
		    	}
	    	}
	    	strSQL += " ORDER by  m.legal_businessname ";
	    }else if(queryType == -2){
	    	//cat.debug("New Query Type");
	    	if(agentTooLong != null && subAgentTooLong != null && repTooLong != null){//This can only happen at the iso level!  Check for zeroes and go on.
	    		if(Integer.parseInt(subAgentTooLong) == 0 && Integer.parseInt(repTooLong) == 0){
	    			strSQL = "SELECT merchant_id, legal_businessname FROM merchants with(nolock) WHERE rep_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?))) ORDER by  legal_businessname";
	    		}else if(Integer.parseInt(subAgentTooLong) != 0){
	    			strSQL = "SELECT merchant_id, legal_businessname FROM merchants with(nolock) WHERE rep_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?)) ORDER by legal_businessname";
	    		}else if(Integer.parseInt(repTooLong) != 0){
	    			strSQL = "SELECT merchant_id, legal_businessname FROM merchants with(nolock) WHERE rep_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?) ORDER by legal_businessname";
	    		}
	    	}else if(agentTooLong != null && subAgentTooLong != null){//Only happens on the iso level.
	    		if(Integer.parseInt(subAgentTooLong) == 0){
	    			//cat.debug("Now in the ALL statement for creating the rep list");
	    			strSQL = "SELECT rep_id, businessname FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?)) ORDER by businessname";
	    		}else{
	    			//cat.debug("Limiting to one agent when creating the rep list, safe case.");
	    			strSQL = "SELECT rep_id, businessname FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE rep_id = ? AND iso_id = ?)) ORDER by businessname";	    		
	    		}
	    	}else if(repTooLong != null && subAgentTooLong != null){//Only happens on the agent level
	    		if(Integer.parseInt(repTooLong) == 0){
	    			//cat.debug("Now in the ALL statement for creating the merchant list");
	    			strSQL = "SELECT merchant_id, legal_businessname FROM merchants with(nolock) WHERE rep_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?)) ORDER by legal_businessname";
	    		}else{
	    			//cat.debug("Limiting to one agent when creating the rep list, safe case.");
	    			strSQL = "SELECT merchant_id, legal_businessname FROM merchants with(nolock) WHERE rep_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE rep_id = ? AND iso_id = ?)) ORDER by legal_businessname";	    		
	    		}
	    	}else if(agentTooLong != null){//We want a list of all sub-agents, but our list of agents is too large.  Should only be here in the iso level. 
	    		strSQL = "SELECT rep_id, businessname FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?) ORDER by businessname";
	    	}else if(subAgentTooLong != null){//We want a list of all reps, but our list of sub-agents is too large.  Should only be here in the agent level. 
	    		strSQL = "SELECT rep_id, businessname FROM reps with(nolock) WHERE iso_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?) ORDER by businessname";
	    	}else if(repTooLong != null){//We want a list of all reps, but our list of sub-agents is too large.  Should only be here in the sub-agent level. 
	    		strSQL = "SELECT merchant_id, legal_businessname FROM merchants with(nolock) WHERE rep_id in (SELECT rep_id FROM reps with(nolock) WHERE iso_id = ?) ORDER by legal_businessname ";
	    	}
	    }
	    cat.debug(strSQL);
	    cat.debug("agentTooLong="+agentTooLong+" subAgentTooLong="+subAgentTooLong+" repTooLong="+repTooLong);
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}
	    	pstmt = dbConn.prepareStatement(strSQL);
	    	if(idNums != null){
	    		for(int i = 0; i < idNums.length;i++){
	    			//if(i == 0){
	    			//	cat.debug("First added param" + idNums[i] + " and length of idNums:"+idNums.length);
	    			//}
	    			pstmt.setString(i+1, idNums[i]);
	    		}
	    	}else{
	    		if(agentTooLong != null && subAgentTooLong != null && repTooLong != null){
	    			if(Integer.parseInt(subAgentTooLong) == 0 && Integer.parseInt(repTooLong) == 0){
	    				pstmt.setString(1, agentTooLong);
	    				//cat.debug("ISO id search:"+agentTooLong);
	    			}else if(Integer.parseInt(subAgentTooLong) != 0){
	    				pstmt.setString(1, subAgentTooLong);
	    			}else if(Integer.parseInt(repTooLong) != 0){
	    				pstmt.setString(1, repTooLong);
	    			}
	    		}else if(agentTooLong != null && subAgentTooLong != null){
	    			if(Integer.parseInt(subAgentTooLong) == 0){
	    				pstmt.setString(1, agentTooLong);
	    				//cat.debug("ISO id added:"+agentTooLong);
	    			}else{
	    				pstmt.setString(1, subAgentTooLong);
	    				//cat.debug("ISO id added:"+agentTooLong);
	    				pstmt.setString(2, agentTooLong);
	    				//cat.debug("Agent id added:"+subAgentTooLong);
	    			}
	    		}else if(repTooLong != null && subAgentTooLong != null){
	    			if(Integer.parseInt(repTooLong) == 0){
	    				pstmt.setString(1, subAgentTooLong);
	    			}else{
	    				pstmt.setString(1, repTooLong);
	    				pstmt.setString(2, subAgentTooLong);
	    			}
	    		}else if(repTooLong != null){
	    			pstmt.setString(1, repTooLong);
	    		}else if(subAgentTooLong != null){
	    			pstmt.setString(1, subAgentTooLong);
	    		}else if(agentTooLong != null){
	    			pstmt.setString(1, agentTooLong);
	    		}
	    	}
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		if(queryType == 0){
	      			id.add(rs.getString("rep_id"));
	      			String str = rs.getString("businessname");
	      			str = str.replaceAll("&", "&amp;");
	      			name.add(str);
	      		}else if(queryType == -1){
	      			//if(id.size() < 2){
	      			//	cat.debug("Doing the merchant accumulating!");
	      			//}
	      			id.add(rs.getString("merchant_id"));
	      			String str = rs.getString("legal_businessname");
	      			str = str.replaceAll("&", "&amp;");
	      			name.add(str);
	      		}else if(queryType == -2){
	      			if(repTooLong != null){
	      				id.add(rs.getString("merchant_id"));
		      			String str = rs.getString("legal_businessname");
		      			str = str.replaceAll("&", "&amp;");
		      			name.add(str);
	      			}else{
	      				id.add(rs.getString("rep_id"));
	      				String str = rs.getString("businessname");
	      				str = str.replaceAll("&", "&amp;");
	      				name.add(str);
	      			}
	      		}
	      	}
	      	//cat.debug("ID Size:" + id.size()+ " and name size:" + name.size());
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("getChildData => Error during initiliazation", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("getData sku => Error during closeConnection ", e);
	    	}
	    }
	}
	
	public List<String> getIDs() {
		//String debugString = "";
		List<String> matched = new ArrayList<String>();
		for(int i=0; i<id.size(); i++) {
			matched.add(id.get(i).trim());
			//debugString += id.get(i) + " | ";
		}
		//cat.debug(debugString);
		return matched;
	}
	
	public List<String> getNames() {
		//String debugString = "";
		List<String> matched = new ArrayList<String>();
		for(int i=0; i<name.size(); i++) {
			matched.add(name.get(i).trim() );
			//debugString += name.get(i) + " | ";
		}
		//cat.debug(debugString);
		return matched;
	}
}
