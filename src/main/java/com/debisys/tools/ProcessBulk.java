/**
 *
 */
package com.debisys.tools;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.customers.Rep;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.CountryInfo;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;

/**
 * @author nmartinez
 *
 */
public class ProcessBulk {

	private static ResourceBundle sql_bundle = ResourceBundle
	.getBundle("com.debisys.tools.sql");
	private static Category cat = Category.getInstance(ProcessBulk.class);

	private StringBuffer messages = new StringBuffer();

	private String customConfigType;
	private String strAccessLevel;
	private String strRefId;
	private String deploymentType;
	private String localAddr;
	private String remoteHost;
	private String contextPath;
	private Integer bulkJob;

	/**
	 * @param strAccessLevel
	 * @param strRefId
	 * @param deploymentType
	 * @param customConfigType
	 * @param localAddr
	 * @param remoteHost
	 * @param contextPath
	 */
	public ProcessBulk(String strAccessLevel, String strRefId,
			String deploymentType, String customConfigType, String localAddr,
			String remoteHost, String contextPath) {
		this.customConfigType = customConfigType;
		this.strAccessLevel = strAccessLevel;
		this.strRefId = strRefId;
		this.deploymentType = deploymentType;
		this.localAddr = localAddr;
		this.remoteHost = remoteHost;
		this.contextPath = contextPath;
	}

	/**
	 * @param arrayMerchantProcess
	 */
	public void doBulkMerchantTerminals(
			ArrayList<MerchantBulk> arrayMerchantProcess,
			SessionData sessionData, ServletContext context) {
		Connection dbConn = null;

		try {
			dbConn = Torque.getConnection(ProcessBulk.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				ProcessBulk.cat.error("Can't get database connection");
				throw new Exception();
			}
			dbConn.setAutoCommit(false);
			// PreparedStatement pst =
			// dbConn.prepareStatement(sql_bundle.getString("bulkmaster"),
			// java.sql.Statement.RETURN_GENERATED_KEYS);
			// pst.setLong(1, Long.parseLong(strRefId));
			// pst.executeUpdate();
			// ResultSet rs = pst.getGeneratedKeys();
			// if (rs.next())
			// {
			// bulkJob = rs.getInt(1);
			// rs.close();
			for (MerchantBulk objMerchantBulk : arrayMerchantProcess) {
				this.addMerchantBulk(objMerchantBulk, dbConn, this.bulkJob, sessionData,
						context);
			}
			// }
			// else
			// {
			// rs.close();
			// messages.append(Languages.getString("jsp.admin.tools.masterbulktable")
			// + "<br>");
			// cannot process the file

			// }
			// pst.close();
		} catch (Exception e) {
			this.messages.append(Languages
					.getString("jsp.admin.tools.general_error",sessionData.getLanguage())
					+ "<br>");
			ProcessBulk.cat.error("Error during doBulkMerchantTerminals ", e);
		} finally {
			try {
				dbConn.setAutoCommit(true);
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				ProcessBulk.cat.error("Error during release connection", e);
			}
		}

	}

	/**
	 * @param objMerchantBulk
	 * @param dbConn
	 * @param bulkJob
	 */
	private void addMerchantBulk(MerchantBulk objMerchantBulk,
			Connection dbConn, Integer bulkJob, SessionData sessionData,
			ServletContext context) {
		boolean bWebServiceAuthenticationEnabled = com.debisys.users.User.isWebServiceAutherticationTerminalsEnabled(sessionData, context);
		Merchant objMerchant = new Merchant();
		// //////////////////////////////////////////////////// check if ISO
		// constant or not
		String RefId = sessionData.getProperty("ref_id");
		// rep id
		ProcessBulk.cat.debug("the staticIDCountryISO1 info is : "
				+ context.getAttribute("staticIDCountryISO1"));
		ProcessBulk.cat.debug("the staticIDConstantValue1 info is : "
				+ context.getAttribute("staticIDConstantValue1"));
		ProcessBulk.cat.debug("Compare to ISO ID is : " + RefId);
		Vector<String> staticISO = new Vector<String>();
		// check if Country ISO matches Static ISO Conditions
		// Static ISO added to define constant values for a country ISO
		for (int i = 1;; i++) {
			if (context.getAttribute("staticIDCountryISO" + i) == null
					|| context.getAttribute("staticIDCountryISO" + i) == "")
			{
				break;
			}
			if (context.getAttribute("staticIDConstantValue" + i) == null
					|| context.getAttribute("staticIDConstantValue" + i) == "")
			{
				break;
			}
			if (RefId.equals(context.getAttribute("staticIDCountryISO" + i))) {
				staticISO.add((String) context
						.getAttribute("staticIDCountryISO" + i));
				staticISO.add((String) context
						.getAttribute("staticIDConstantValue" + i));
				break;
			}
		}
		String merchantId = "";
		if (staticISO.size()<2) {
			merchantId = objMerchant.generateMerchantId();// if no special
			// cases, id is
			// generated randomly
			ProcessBulk.cat.debug("Generate ID without constants values==>" + merchantId);
		} else {
			merchantId = objMerchant.generatePartiallyStaticId(staticISO);
			ProcessBulk.cat.debug("partial Approach generatedID==>" + merchantId);
		}
		// ////////////////////////////////////////////////////check if ISO
		// constant or not
		// ////////////////////it was String merchantId =
		// objMerchant.generateMerchantId();
		CallableStatement cstmt = null;

		try {
			cstmt = dbConn.prepareCall(ProcessBulk.sql_bundle.getString("addMerchant"));
			ProcessBulk.cat.debug("addMerchantBulk ==>" + ProcessBulk.sql_bundle.getString("addMerchant"));
			cstmt.setDouble(1, Double.parseDouble(merchantId));
			if (this.deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
				if (Double.parseDouble(objMerchantBulk.getCreditLimit()) > 0)
				{
					cstmt.setInt(2, Integer.parseInt(DebisysConstants.MERCHANT_TYPE_CREDIT));
				}
				else
				{// unlimited
					cstmt.setInt(2, Integer.parseInt(DebisysConstants.MERCHANT_TYPE_UNLIMITED));
				}
			}
			else if (this.deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
			{
				if ( this.customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
				{
					Rep r = new Rep();
					if (this.strAccessLevel.equals(DebisysConstants.ISO) || this.strAccessLevel.equals(DebisysConstants.AGENT) || this.strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						r.setRepId(objMerchantBulk.getRepId());
					}
					else if (this.strAccessLevel.equals(DebisysConstants.REP))
					{
						r.setRepId(this.strRefId);
					}
					r.getRep(sessionData, context);
					if ( r.getSharedBalance().equals("1") )
					{
						cstmt.setInt(2, Integer.parseInt(DebisysConstants.MERCHANT_TYPE_SHARED));
					}
					else
					{
						cstmt.setInt(2, Integer.parseInt(DebisysConstants.MERCHANT_TYPE_PREPAID));
					}
					r = null;
				}
				else
				{
					cstmt.setInt(2, Integer.parseInt(DebisysConstants.MERCHANT_TYPE_PREPAID));
				}
			}

			if (this.strAccessLevel.equals(DebisysConstants.ISO) || this.strAccessLevel.equals(DebisysConstants.AGENT) || this.strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				cstmt.setDouble(3, Double.parseDouble(objMerchantBulk.getRepId()));
			}
			else if (this.strAccessLevel.equals(DebisysConstants.REP))
			{
				cstmt.setDouble(3, Double.parseDouble(this.strRefId));
			}
			// legacy
			cstmt.setInt(4, 0);
			cstmt.setInt(5, 0);
			cstmt.setInt(6, 0);
			cstmt.setInt(7, 0);
			cstmt.setInt(8, 0);
			cstmt.setInt(9, 0);
			cstmt.setInt(10, 0);
			cstmt.setInt(11, 0);
			cstmt.setInt(12, 0);
			cstmt.setInt(13, 0);
			cstmt.setString(14, objMerchantBulk.getBusinessName());
			cstmt.setString(15, objMerchantBulk.getTaxId());
			cstmt.setString(16, objMerchantBulk.getContactEmail());
			cstmt.setString(17, objMerchantBulk.getContactName());
			cstmt.setString(18, objMerchantBulk.getDba());
			cstmt.setString(19, objMerchantBulk.getContactPhone());
			cstmt.setString(20, objMerchantBulk.getContactFax());
			cstmt.setString(21, objMerchantBulk.getPhysAddress());
			cstmt.setString(22, objMerchantBulk.getPhysCity());
			cstmt.setString(23, objMerchantBulk.getPhysCounty());
			cstmt.setString(24, objMerchantBulk.getPhysState());
			cstmt.setString(25, objMerchantBulk.getPhysZip());
			HashMap m = CountryInfo.getAllCountriesID();
			int value = ProcessBulk.getKeysFromValue(m,objMerchantBulk.getPhysCountry());
			cstmt.setInt(26, value);
			cstmt.setString(27, objMerchantBulk.getPhysCountry());
			if (this.customConfigType
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
					&& (objMerchantBulk.getMailState() != null)) {
				cstmt.setString(28, objMerchantBulk.getMailAddress());
				cstmt.setString(29, objMerchantBulk.getMailCity());
				cstmt.setString(30, objMerchantBulk.getMailState());
				cstmt.setString(31, objMerchantBulk.getMailZip());
				int value2 = ProcessBulk.getKeysFromValue(m,objMerchantBulk.getMailCountry());
				cstmt.setInt(32, value2);
				cstmt.setString(33, objMerchantBulk.getPhysCountry());
			} else {
				cstmt.setString(28, objMerchantBulk.getPhysAddress());
				cstmt.setString(29, objMerchantBulk.getPhysCity());
				cstmt.setString(30, objMerchantBulk.getPhysState());
				cstmt.setString(31, objMerchantBulk.getPhysZip());
				int value2 = ProcessBulk.getKeysFromValue(m,objMerchantBulk.getPhysCountry());
				cstmt.setInt(32, value2);
				cstmt.setString(33, objMerchantBulk.getPhysCountry());
			}

			cstmt.setString(34, objMerchantBulk.getBusinessTypeId());
			cstmt.setString(35, objMerchantBulk.getRoutingNumber());
			cstmt.setString(36, objMerchantBulk.getAccountNumber());
			cstmt.setDouble(37, Double.parseDouble(objMerchantBulk
					.getMonthlyFeeThreshold()));
			cstmt.setNull(38, java.sql.Types.INTEGER);
			cstmt.setNull(39, java.sql.Types.FLOAT);
			cstmt.setNull(40, java.sql.Types.DATE);
			cstmt.setNull(41, java.sql.Types.VARCHAR);

			if (this.customConfigType
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
				cstmt.setInt(42, objMerchantBulk.getRegion());
				cstmt.setInt(43, objMerchantBulk.getInvoice());
			} else {
				cstmt.setNull(42, java.sql.Types.INTEGER);
				cstmt.setNull(43, java.sql.Types.INTEGER);
			}

			if (this.deploymentType
					.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
				if (objMerchantBulk.getMerchantRouteId() > 0) {
					cstmt.setInt(44, objMerchantBulk.getMerchantRouteId());
				} else {
					cstmt.setNull(44, java.sql.Types.INTEGER);
				}
			} else {
				cstmt.setNull(44, java.sql.Types.INTEGER);
			}

			if (this.customConfigType
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
				cstmt.setString(45, objMerchantBulk.getPhysColony());
				cstmt.setString(46, objMerchantBulk.getPhysDelegation());
				cstmt.setString(47, objMerchantBulk.getMailColony());
				cstmt.setString(48, objMerchantBulk.getMailDelegation());
				cstmt.setString(49, objMerchantBulk.getMailCounty());
				cstmt.setString(50, objMerchantBulk.getBankName());
				cstmt.setInt(51, objMerchantBulk.getMerchantSegmentId());
				cstmt.setString(52, objMerchantBulk.getBusinessHours());
				cstmt.setInt(53, Integer
						.parseInt(DebisysConstants.MERCHANT_TYPE_PREPAID));// Integer.parseInt(objMerchantBulk.getMerchantType()));
				cstmt.setInt(54, objMerchantBulk.getAccountTypeId());
			} else {
				cstmt.setNull(45, java.sql.Types.VARCHAR);
				cstmt.setNull(46, java.sql.Types.VARCHAR);
				cstmt.setNull(47, java.sql.Types.VARCHAR);
				cstmt.setNull(48, java.sql.Types.VARCHAR);
				cstmt.setNull(49, java.sql.Types.VARCHAR);
				cstmt.setNull(50, java.sql.Types.VARCHAR);
				cstmt.setNull(51, java.sql.Types.INTEGER);
				cstmt.setNull(52, java.sql.Types.VARCHAR);
				cstmt.setInt(53, Integer
						.parseInt(DebisysConstants.MERCHANT_TYPE_PREPAID));
				// cstmt.setNull(51, java.sql.Types.INTEGER);
				cstmt.setInt(54, 1);
			}

			cstmt.setInt(55, objMerchantBulk.getPaymentType());
			cstmt.setInt(56, objMerchantBulk.getProcessType());
			//

			cstmt.setString(57, objMerchantBulk.getUserId());
			cstmt.setString(58, objMerchantBulk.getUserPassword());

			cstmt.registerOutParameter(59, java.sql.Types.INTEGER);

			if (this.customConfigType
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
				cstmt.setInt(60, objMerchantBulk.getMerchantClassification());
				cstmt.setString(61, objMerchantBulk.getControlNumber());

			} else {
				cstmt.setNull(60, java.sql.Types.INTEGER);
				cstmt.setNull(61, java.sql.Types.VARCHAR);
			}
			
			if(objMerchantBulk.isUseRatePlanModel()){
				cstmt.setInt(62, 1);
			}else{
				cstmt.setInt(62, 0);
			}

			cstmt.execute();
			String recIdGenerated = String.valueOf(cstmt.getInt(59));

			objMerchantBulk.setUserId(recIdGenerated);
			objMerchantBulk.setUserPassword(recIdGenerated);

			cstmt.close();
			objMerchantBulk.setMerchantId(merchantId);

			if (this.customConfigType
					.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
				cstmt = dbConn.prepareCall(ProcessBulk.sql_bundle
						.getString("insertContactMx"));
				cstmt.setDouble(1, Double.parseDouble(objMerchantBulk
						.getMerchantId()));
				cstmt.setInt(2, objMerchantBulk.objContact.getContactTypeID());
				cstmt.setString(3, objMerchantBulk.objContact.getContactName());
				cstmt
				.setString(4, objMerchantBulk.objContact
						.getContactPhone());
				cstmt.setString(5, objMerchantBulk.objContact.getContactFax());
				cstmt
				.setString(6, objMerchantBulk.objContact
						.getContactEmail());
				cstmt.setString(7, objMerchantBulk.objContact
						.getContactCellphone());
				cstmt.setString(8, objMerchantBulk.objContact
						.getContactDepartment());
				cstmt.executeUpdate();
				cstmt.close();
			}

			for (TerminalBulk objTerminal : objMerchantBulk.getArrayTerminals()) {
				objTerminal.setMerchantId(merchantId);
				this.addTerminalBulk(dbConn, objTerminal, bulkJob, context,bWebServiceAuthenticationEnabled,sessionData);
			}
			objMerchantBulk.ProcessOK(sessionData, context);
			dbConn.commit();
		} catch (Exception e) {
			this.messages.append(Languages
					.getString("jsp.admin.tools.error_add_merchant",sessionData.getLanguage())
					+ " -- " + e + "<br>");
			try {
				dbConn.rollback();
			} catch (Exception ex) {
				ProcessBulk.cat.error("Error doing RollBack ", ex);
			} finally {
				ProcessBulk.cat.error("Error saving merchant ", e);

			}

		} finally {
			try {
				if (cstmt != null)
				{
					cstmt.close();
				}
			} catch (Exception e) {
				ProcessBulk.cat.error("Error closing CallableStatement ", e);
			}
		}

	}

	/**
	 * @param dbConn
	 * @param objTerminal
	 * @param bulkJob
	 * @param localAddr
	 * @param remoteHost
	 * @param contextPath
	 * @throws SQLException
	 * @throws SQLException
	 */
	private void addTerminalBulk(Connection dbConn, TerminalBulk objTerminal,
			Integer bulkJob, ServletContext context,boolean bWebServiceAuthenticationEnabled,SessionData sessionData) throws SQLException {
		CallableStatement cstmt = null;
		PreparedStatement pstmt = null;
		// try{

		String strTerminalId = objTerminal.generateTerminalId();
		objTerminal.setTerminalId(strTerminalId);
		String strSiteId = objTerminal.generateSiteId();
		objTerminal.setSiteId(strSiteId);
		cstmt = dbConn.prepareCall(ProcessBulk.sql_bundle.getString("addTerminal"));

		cstmt.setDouble(1, Double.parseDouble(objTerminal.getMerchantId()));
		cstmt.setNull(2, java.sql.Types.INTEGER); // owner_id
		cstmt.setNull(3, java.sql.Types.VARCHAR); // lynk_no
		cstmt.setNull(4, java.sql.Types.VARCHAR); // gensar_no
		cstmt.setNull(5, java.sql.Types.VARCHAR); // visanet_no
		cstmt.setNull(6, java.sql.Types.VARCHAR); // manager_card
		cstmt.setNull(7, java.sql.Types.VARCHAR); // serial_no
		cstmt.setInt(8, Integer.parseInt(objTerminal.getTerminalType())); // termina_type
		cstmt.setInt(9, 0);// pinpad type
		cstmt.setInt(10, 0); // printer type
		cstmt.setNull(11, java.sql.Types.VARCHAR);// modem_no
		cstmt.setNull(12, java.sql.Types.VARCHAR);// nurit_no
		cstmt.setNull(13, java.sql.Types.DATE);// ship_date
		cstmt.setNull(14, java.sql.Types.INTEGER);// bank_id
		cstmt.setNull(15, java.sql.Types.VARCHAR);// bank_acct_no
		cstmt.setNull(16, java.sql.Types.VARCHAR);// aba_no
		cstmt.setNull(17, java.sql.Types.VARCHAR);// buypass_no
		cstmt.setNull(18, java.sql.Types.VARCHAR);// mellon_no
		cstmt.setNull(19, java.sql.Types.INTEGER);// rateplan
		cstmt.setInt(20, 2); // version

		cstmt.setInt(21, 1);// password
		cstmt.setNull(22, java.sql.Types.INTEGER);// rep_comm
		cstmt.setNull(23, java.sql.Types.INTEGER);// merch_comm

		// purchase type = customer provided or FSO Rental
		if ((NumberUtil.isNumeric(objTerminal.getMonthlyFeeThreshold()) && (Double
				.parseDouble(objTerminal.getMonthlyFeeThreshold()) > 0))
				|| (NumberUtil.isNumeric(objTerminal.getMonthlyFee()) && (Double
						.parseDouble(objTerminal.getMonthlyFee()) > 0))) {
			cstmt.setInt(24, 15);// purchase_type 15 FSO Rentals
		} else {
			cstmt.setInt(24, 5);// purchase_type 5 Customer Provided
		}

		cstmt.setNull(25, java.sql.Types.VARCHAR);// pinpad_sernum
		cstmt.setNull(26, java.sql.Types.VARCHAR);// printer_sernum
		cstmt.setNull(27, java.sql.Types.VARCHAR);// other1
		cstmt.setNull(27, java.sql.Types.VARCHAR);// other1
		cstmt.setNull(28, java.sql.Types.VARCHAR);// other2
		cstmt.setNull(29, java.sql.Types.VARCHAR);// lynk
		cstmt.setNull(30, java.sql.Types.FLOAT);// amount
		cstmt.setNull(31, java.sql.Types.FLOAT);// DeploymentFeeAmount
		cstmt.setNull(32, java.sql.Types.DATE);// DeploymentFeeReceived
		cstmt.setNull(33, java.sql.Types.VARCHAR);// DeployedBy
		cstmt.setNull(34, java.sql.Types.SMALLINT);// verifyProduct
		cstmt.setInt(35, 0);// batch_seq_no
		cstmt.setNull(36, java.sql.Types.VARCHAR);// location
		cstmt.setInt(37, Integer.parseInt(objTerminal.getIsoRatePlanId()));// ISO_Rateplan_id
		cstmt.setInt(38, 0);// AllowReturns
		cstmt.setDouble(39, Double
				.parseDouble(objTerminal.getReconnectionTax()));// Reconnection_Fees_Tax
		cstmt.setDouble(40, Double
				.parseDouble(objTerminal.getReconnectionFee()));// Reconnection_Fees_Fee
		cstmt.setDouble(41, Double.parseDouble(objTerminal
				.getAnnualMaintenanceTax()));// Annual_Maintenance_Tax
		cstmt.setDouble(42, Double.parseDouble(objTerminal
				.getAnnualMaintenanceFee()));// Annual_Maintenance_Fee
		cstmt.setDouble(43, Double.parseDouble(objTerminal
				.getAnnualInsuranceTax()));// Annual_Insurance_Tax
		cstmt.setDouble(44, Double.parseDouble(objTerminal
				.getAnnualInsuranceFee()));// Annual_Insurance_Fee
		cstmt.setDouble(45, Double.parseDouble(objTerminal
				.getAnnualAffiliationTax()));// Annual_Affiliation_Tax
		cstmt.setDouble(46, Double.parseDouble(objTerminal
				.getAnnualAffiliationFee()));// Annual_Affiliation_Fee
		cstmt.setNull(47, java.sql.Types.VARCHAR);// SerialNumber
		cstmt.setNull(48, java.sql.Types.VARCHAR);// SIM
		cstmt.setNull(49, java.sql.Types.VARCHAR);// IMCI
		cstmt.setNull(50, java.sql.Types.INTEGER);// qcomm_terminal_id
		cstmt.setNull(51, java.sql.Types.DATE);// qcomm_transition_date
		cstmt.setNull(52, java.sql.Types.VARCHAR);// terminal_version
		cstmt.setNull(53, java.sql.Types.VARCHAR);// terminal_note
		cstmt.setNull(54, java.sql.Types.BIT);// billing
		cstmt.setNull(55, java.sql.Types.INTEGER);// physicalTerminalId
		cstmt.setInt(56, objTerminal.getMarketPlaceKey());// MarketPlaceKey
		cstmt.setInt(57, objTerminal.getMarketPlaceGroup());// MarketPlaceGroup
		cstmt.setInt(58, Integer.parseInt(objTerminal.getSiteId()));// millennium_no
		cstmt.setDouble(59, Double.parseDouble(objTerminal.getTerminalId()));// millennium_no

		cstmt.setString(60, objTerminal.getClerkCode());// passwordClerk
		cstmt.setString(61, objTerminal.getClerkName());// userNameClerk
		cstmt.setInt(62, 0);// bulkMaster for future implementations

		if (this.customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
			cstmt.setString(61, objTerminal.getFirstName() + " "
					+ objTerminal.getLastName());
			cstmt.setString(63, objTerminal.getFirstName());
			cstmt.setString(64, objTerminal.getLastName());

		} else {
			// cstmt.setString(63,objTerminal.getClerkName());
			cstmt.setNull(63, java.sql.Types.VARCHAR);
			cstmt.setNull(64, java.sql.Types.VARCHAR);

		}
		if(objTerminal.getRatePlanId() != null){
			cstmt.setLong(65, objTerminal.getRatePlanId());// MarketPlaceKey
		}else{
			cstmt.setNull(65, java.sql.Types.INTEGER);
		}
		cstmt.executeUpdate();
		cstmt.close();

		if (objTerminal.getTerminalType().equals("26")
				|| objTerminal.getTerminalType().equals("40")
				|| objTerminal.getTerminalType().equals(DebisysConstants.WEB_SERVICE_KIOSK_TERMINAL_TYPE)
				|| objTerminal.getTerminalType().equals(DebisysConstants.SMS_PHONE_KIOSK_MERCHANT_TERMINAL_TYPE)
				|| objTerminal.getTerminalType().equals("56")|| objTerminal.getTerminalType().equals("23")&& bWebServiceAuthenticationEnabled) {
			objTerminal.setIsPCTerminal(false);

			if (!objTerminal.isChkManualRegCode()) {
				objTerminal.setTxtRegistration(strSiteId);
			}
			if (objTerminal.getTxtAllowCertificate().length() == 0) {
				objTerminal.setTxtAllowCertificate("0");
			}
			String sUpdateInfo = "";
			sUpdateInfo = "[millennium_no=" + strSiteId
			+ "] [RegistrationCode=" + objTerminal.getTxtRegistration()
			+ "] [RegistrationInd=-1] [RegistrationDate=null] ";
			sUpdateInfo += "[UserId=" + objTerminal.getPcTermUsername()
			+ "] [Password=" + objTerminal.getPcTermPassword()
			+ "] [RegistrationIp=null] [RegistrationAttemptNum=0] ";
			sUpdateInfo += "[RegistrationIgnoreInd="
				+ (objTerminal.isChkRegCode() ? "-1" : "1")
				+ "] [CustomConfigId=" + objTerminal.getPcTermBrand()
				+ "] ";
			sUpdateInfo += "[RestrictTolpAddr=" + objTerminal.getTxtIPAddress()
			+ "] [RestrictToSubnetMask="
			+ objTerminal.getTxtSubnetMask() + "] ";
			sUpdateInfo += "[ipAddrIgnoreInd="
				+ (objTerminal.isChkIPRestriction() ? "-1" : "1")
				+ "] [CertificateIgnoreInd="
				+ (objTerminal.isChkCertificate() ? "-1" : "1") + "] ";
			sUpdateInfo += "[CertificateInstallAlowedInd="
				+ (objTerminal.isChkAllowCertificate() ? "-1" : "1")
				+ "] [CertificateInstallRemainNum="
				+ objTerminal.getTxtAllowCertificate() + "] ";

			pstmt = dbConn.prepareStatement(ProcessBulk.sql_bundle
					.getString("addTerminalRegistration"));
			pstmt.setInt(1, Integer.parseInt(strSiteId));
			pstmt.setString(2, objTerminal.getTxtRegistration());
			// RegistrationInd = -1
			// RegistrationDate = null

			pstmt.setString(3, objTerminal.getPcTermUsername());
			pstmt.setString(4, objTerminal.getPcTermPassword());

			// pstmt.setString(3, "NIC4563");
			// pstmt.setString(4, "NIC5674D");

			// RegistrationIp = null
			// RegistrationAttemptNum = 0
			pstmt.setInt(5, (objTerminal.isChkRegCode() ? -1 : 1));// RegistrationIgnoreInd
			pstmt.setString(6, objTerminal.getPcTermBrand());// CustomConfigId
			pstmt.setString(7, objTerminal.getTxtIPAddress());// RestrictToIpAddr
			pstmt.setString(8, objTerminal.getTxtSubnetMask());// RestrictToSubnetMask
			pstmt.setInt(9, (objTerminal.isChkIPRestriction() ? -1 : 1));// ipAddrIgnoreInd
			pstmt.setInt(10, (objTerminal.isChkCertificate() ? -1 : 1));// CertificateIgnoreInd
			pstmt.setInt(11, (objTerminal.isChkAllowCertificate() ? -1 : 1));// CertificateInstallAlowedInd
			pstmt.setInt(12, Integer.parseInt(objTerminal.getTxtAllowCertificate()));// CertificateInstallRemainNum

			pstmt.executeUpdate();
			pstmt.close();

			pstmt = dbConn.prepareStatement(ProcessBulk.sql_bundle
					.getString("addTerminalActivityLog"));
			// CurrentDate
			pstmt.setInt(1, Integer.parseInt(strSiteId));
			pstmt.setString(2, objTerminal.getTxtRegistration());
			pstmt.setString(3, objTerminal.getPcTermUsername());
			pstmt.setString(4, this.remoteHost);
			pstmt.setString(5, this.localAddr);
			pstmt.setString(6, this.contextPath);
			// Class = SupportSite
			pstmt.setString(7, "addTerminalBulk");// FunctionName
			pstmt.setString(8, "ADMIN ADD: " + sUpdateInfo);// Description
			pstmt.setString(9, "OK");// ErrorCode
			pstmt.setString(10, sUpdateInfo);// Error Message
			pstmt.setString(11, objTerminal.getPcTermBrand());// CustomConfigId

			pstmt.executeUpdate();
			pstmt.close();
		} else {
			objTerminal.setIsPCTerminal(true);
		}
		objTerminal.ProcessOK(context,sessionData);

		/*
		 * } catch (Exception e) {
		 * messages.append(Languages.getString("jsp.admin.tools.error_add_terminal"
		 * ) + " -- " + e + " -- " + "<br>");
		 * cat.error("Error saving terminal ", e); throw new Exception(); }
		 * finally { try { if (cstmt != null) cstmt.close(); if (pstmt != null)
		 * pstmt.close(); } catch (Exception e) {
		 * cat.error("Error closing CallableStatement ", e); } }
		 */
	}

	/**
	 * @return
	 */
	public Integer getIdBulk() {

		return this.bulkJob;
	}

	/**
	 * param map , key
	 * @return if key exists in map, returns key else returns 0 (undetermined country value)
	 */
	public static int getKeysFromValue(HashMap hm,String value){
		Set ref = hm.keySet();
		Iterator it = ref.iterator();
		int key = 0 ;
		while (it.hasNext()) {
			Object o = it.next();
			if(hm.get(o).toString().equals(value)) {
				return Integer.parseInt(o.toString());
			}
		}
		return key;
	}

}
