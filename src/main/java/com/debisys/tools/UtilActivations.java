/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools;

import com.debisys.utils.SimControl;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 *
 * @author nmartinez
 */
public class UtilActivations {

    private static Logger _logger = Logger.getLogger(UtilActivations.class);

    /**
     *
     * @param control
     * @param mapErrorControls
     */
    public static void validateControl(SimControl control, HashMap<String, String> mapErrorControls,
            StringBuilder messagesControls, String styleErrorField) {
        int controlLength = control.getValue().length();
        int controlLengthVal = Integer.parseInt(control.getLength());
        if (controlLength == 0 && control.getRequired()) {
            controlError(control, mapErrorControls, messagesControls, styleErrorField);
        } else if (control.getRequired() && control.getValidateLength()) {

            if (control.getOperatorLengthValidator() != null) {
                if (control.getOperatorLengthValidator().equals(">")) {
                    if (!(controlLength > controlLengthVal)) {
                        controlError(control, mapErrorControls, messagesControls, styleErrorField);
                    }
                } else if (control.getOperatorLengthValidator().equals(">=")) {
                    if (!(controlLength >= controlLengthVal)) {
                        controlError(control, mapErrorControls, messagesControls, styleErrorField);
                    }
                } else if (control.getOperatorLengthValidator().equals("<")) {
                    if (!(controlLength < controlLengthVal)) {
                        controlError(control, mapErrorControls, messagesControls, styleErrorField);
                    }
                } else if (control.getOperatorLengthValidator().equals("<=")) {
                    if (!(controlLength <= controlLengthVal)) {
                        controlError(control, mapErrorControls, messagesControls, styleErrorField);
                    }
                } else if (control.getOperatorLengthValidator().equals("=")) {
                    if (!(controlLength == controlLengthVal)) {
                        controlError(control, mapErrorControls, messagesControls, styleErrorField);
                    }
                }
            }
        } else {
            control.setStyleError("");
        }
        if (!mapErrorControls.containsKey(control.getControlId())) {
            control.setStyleError("");
        }
    }

    /**
     *
     * @param control
     * @param mapErrorControls
     * @param messagesControls
     */
    private static void controlError(SimControl control, HashMap<String, String> mapErrorControls,
            StringBuilder messagesControls, String styleErrorField) {
        control.setStyleError(styleErrorField);
        mapErrorControls.put(control.getControlId(), control.getMessageValLength());
        if (control.getMessageValLength() != null) {
            messagesControls.append(control.getMessageValLength());
            messagesControls.append("<br/>");
        }
    }
}
