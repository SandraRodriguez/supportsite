/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.tools.terminals;

import com.debisys.exceptions.ErrorsException;
import com.debisys.reports.pojo.TerminalPermissionsPropertiesByEntityPojo;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author nmartinez
 */
public class TerminalPermissionsPropertiesByEntity {

    private static Logger cat = Logger.getLogger(TerminalPermissionsPropertiesByEntity.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.terminals.sql");

    private TerminalPermissionsPropertiesByEntity() {

    }

    /**
     *
     * @param entity
     * @param code
     * @return
     * @throws ErrorsException
     */
    public static ArrayList<TerminalPermissionsPropertiesByEntityPojo> getPropertiesByPermissionCodeAndId(String entity, String code) throws ErrorsException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<TerminalPermissionsPropertiesByEntityPojo> lst = new ArrayList<TerminalPermissionsPropertiesByEntityPojo>();
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ErrorsException();
            }
            String strQuery = sql_bundle.getString("getPropertiesByPermissionCodeAndId");
            cat.info(strQuery);
            pstmt = dbConn.prepareStatement(strQuery);
            pstmt.setString(1, entity);
            pstmt.setString(2, code);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                int enable = 1;
                if (rs.getInt("EnabledProperty") == 0) {
                    enable = 0;
                } else if (rs.getInt("EnabledEntity") == 0) {
                    enable = 2;
                }
                String propertyId = rs.getString("propertyId");
                TerminalPermissionsPropertiesByEntityPojo tppPojo
                        = new TerminalPermissionsPropertiesByEntityPojo(rs.getString("IdNew"),
                                entity,
                                rs.getString("Property"),
                                rs.getString("PropertyValue"),
                                enable,
                                rs.getString("Datetime"),
                                rs.getInt("type"), propertyId);
                
                
                lst.add(tppPojo);
            }
            TorqueHelper.closeStatement(pstmt, rs);
        } catch (Exception e) {
            cat.error("Error during getPropertiesByPermissionCodeAndId ", e);
            throw new ErrorsException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return lst;
    }

    public static ArrayList<String> getPermissionsByCodeAndId(String code) throws ErrorsException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<String> lst = new ArrayList<String>();
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ErrorsException();
            }
            String strQuery = sql_bundle.getString("getPermissionsByCodeAndId");
            cat.info(strQuery);
            pstmt = dbConn.prepareStatement(strQuery);
            pstmt.setString(1, code);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                lst.add(rs.getString("Property"));
            }
            TorqueHelper.closeStatement(pstmt, rs);
        } catch (Exception e) {
            cat.error("Error during getPermissionsByCodeAndId ", e);
            throw new ErrorsException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return lst;
    }

    public static void updatePropertyById(String id, String status, String value, String propertyName) throws ErrorsException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ErrorsException();
            }
            String strQuery = sql_bundle.getString("updatePropertyByValueId");
            if (value == null) {
                strQuery = sql_bundle.getString("updatePropertyByStatusId");
                cat.info(strQuery);
                pstmt = dbConn.prepareStatement(strQuery);
                pstmt.setInt(1, Integer.parseInt(status));
                pstmt.setString(2, id);
            } else {
                cat.info(strQuery);
                pstmt = dbConn.prepareStatement(strQuery);                
                if ( value.length()==0 ){
                    pstmt.setNull(1, java.sql.Types.VARCHAR);            
                } else{
                    pstmt.setString(1, value);            
                }
                pstmt.setInt(2, Integer.parseInt(status));
                pstmt.setString(3, id);
            }
            pstmt.executeUpdate();
            TorqueHelper.closeStatement(pstmt, null);
        } catch (Exception e) {
            cat.error("Error during updatePropertyById, Cannot update " + propertyName, e);
            //throw new ErrorsException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
    }

    public static void insertIntoPropertiesEntity(String status, String value, String propertyId, String entityId) throws ErrorsException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ErrorsException();
            }
            
            String strQuery = sql_bundle.getString("insertIntoPropertiesEntity");
            //[EntityId],[TerminalPermissionPropertyIdNew],[PropertyValue]
            cat.info(strQuery);
            pstmt = dbConn.prepareStatement(strQuery);
            pstmt.setString(1, entityId);
            pstmt.setString(2, propertyId);
            if ( value == null || value.length()==0 ){
                pstmt.setNull(3, java.sql.Types.VARCHAR);            
            } else{
                pstmt.setString(3, value);            
            }
            pstmt.setInt(4, Integer.parseInt(status));
            

            pstmt.executeUpdate();
            TorqueHelper.closeStatement(pstmt, null);
        } catch (Exception e) {
            cat.error("Error during updatePropertyById ", e);
            //throw new ErrorsException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
    }

}
