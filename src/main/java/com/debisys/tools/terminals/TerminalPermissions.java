package com.debisys.tools.terminals;

import com.debisys.exceptions.ErrorsException;
import com.debisys.reports.pojo.MerchantPojo;
import com.debisys.reports.pojo.TerminalPermissionBasicPojo;
import com.debisys.reports.pojo.TerminalPermissionsByEntityPojo;
import com.debisys.utils.LogChanges;
import com.debisys.utils.NumberUtil;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author nmartinez
 */
public class TerminalPermissions {
    
    /**
     * Eternal login
     */
    public static final String CODE_ETERNAL_LOGIN ="EL";
    
    /**
     * No clerk required
     */
    public static final String CODE_NO_CLERK ="NC";
    
    /**
     * Requests for SMS Receipts
     */
    public static final String CODE_RSMSR ="RSMSR";
    
    /**
     * Link to the support-site
     */
    public static final String CODE_LSS ="SSLINK";
    
    /**
     * Link to the support-site
     */
    public static final String CARRIERS_COMBO ="CARRCOMBO";
    
    /**
     * Link to the support-site
     */
    public static final String VIEW_PIN ="VIEW_PIN";
    
    /**
     * 
     */
    public static final String NO_BALANCE_MERCHANT ="NOBALANCEMERCHANT";
    
    /**
     * 
     */
    public static final String TOKEN_SECURITY ="TOKENSECURITY";
    
    /**
     * Link to the support-site
     */
    public static final String PREPAID_USING_CARD_PROCESSOR ="PREPAID_USING_CARD_PROCESSOR";

    /**
     * 
     */
    public static final String CHAT ="CHAT";
    
    /**
     * 
     */
    public static final String MARKET_PLACE ="MARKETPLACE";
    
    /**
     * 
     */
    public static final String CUSTOMER_ACCOUNT_MANAGEMENT ="CRM";
    
    /**
     * 
     */
    public static final String VIEW_COMMISSIONS_DETAIL ="VIEWCOMMISSIONS";
    
    /*
    
    */
    public static final String VIEW_ECOMMERCE ="ECOMMERCE";

    /**
     * View the 2 accounts fields on top up flow pcterminal ng
     */
    public static final String VIEW_ACCOUNTS_NO_TOPUPL ="VIEWACCOUNTSTOPUP";
    
    /**
     * View tool for admin the document flow to ISOS
     */
    public static final String VIEW_DOCUMENT_MANAGEMENT ="VIEWDOCMANAGEMENT";

    private static Logger cat = Logger.getLogger(TerminalPermissions.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.terminals.sql");

    
    /**
     * 
     * @param entityId
     * @param permissionCode
     * @return
     * @throws ErrorsException 
     */
    public static TerminalPermissionBasicPojo getEntitiesByParent(String entityId, String permissionCode) throws ErrorsException
    {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        TerminalPermissionBasicPojo terminalPermissionBasicPojo = new TerminalPermissionBasicPojo();
        ArrayList<TerminalPermissionsByEntityPojo> lstTerminalPermissions = new ArrayList<TerminalPermissionsByEntityPojo>();
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new ErrorsException();
            }
            String strQuery = sql_bundle.getString("getEntitiesByParent");
            cat.debug(strQuery);
            pstmt = dbConn.prepareStatement(strQuery);
            pstmt.setString(1, entityId);
            pstmt.setString(2, permissionCode);
            rs = pstmt.executeQuery();
            while (rs.next())
            {
                if ( !entityId.equals(rs.getString(2)) ){
                    TerminalPermissionsByEntityPojo tpPojo = new TerminalPermissionsByEntityPojo();
                    tpPojo.setIdNew( rs.getString(1) );
                    tpPojo.setEntityId( rs.getString(2) );
                    tpPojo.setStatus( rs.getString(3) );
                    tpPojo.setDatetime( rs.getString(4) );
                    tpPojo.setEntityName( rs.getString(5) );
                    lstTerminalPermissions.add(tpPojo); 
                }
                else{
                    terminalPermissionBasicPojo.setIsIsoSelected(true);
                    terminalPermissionBasicPojo.setIdNew( rs.getString(1) );
                }
            }
            TorqueHelper.closeStatement(pstmt, rs);
        }
        catch (Exception e)
        {
            cat.error("Error during getEntitiesByParent", e);
            throw new ErrorsException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }
            terminalPermissionBasicPojo.setLstTerminalPermissionsByEntityPojo(lstTerminalPermissions);
        }
        return terminalPermissionBasicPojo;
    }
    
    
    /**
     * 
     * @param entityId
     * @param permissionCode
     * @param chainDistribution
     * @return
     * @throws ErrorsException 
     */
    public static ArrayList<MerchantPojo> getMerchantsByParent(String entityId, String permissionCode, String chainDistribution) throws ErrorsException
    {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;        
        ArrayList<MerchantPojo> lstMerchantPojo = new ArrayList<MerchantPojo>();
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new ErrorsException();
            }
            String strQuery ="";
            if ( chainDistribution.equals("2") )
                strQuery = sql_bundle.getString("getMerchantsByParent2");
            else
                strQuery = sql_bundle.getString("getMerchantsByParent1");
            cat.info(strQuery);
            pstmt = dbConn.prepareStatement(strQuery);
            pstmt.setString(1, permissionCode);
            pstmt.setString(2, entityId);            
            rs = pstmt.executeQuery();
            while (rs.next())
            {
                //businessname, reps.rep_id
                MerchantPojo mcPojo = new MerchantPojo();
                if ( rs.getString(10) == null )
                {
                    mcPojo.setMerchant_id(rs.getString(1) );                
                    mcPojo.setDba( rs.getString(2) );                
                    mcPojo.setLegal_businessname( rs.getString(3) );
                    mcPojo.setPhys_address( rs.getString(4) );
                    mcPojo.setPhys_city( rs.getString(5) );
                    mcPojo.setPhys_county( rs.getString(6) );                
                    mcPojo.setPhys_state( rs.getString(7) );                
                    mcPojo.setPhys_zip(rs.getString(8) );                
                    mcPojo.setPhys_country(rs.getString(9) );
                    mcPojo.setRepBuss( rs.getString(11) );
                    mcPojo.setRep_id( rs.getString(12) );
                    lstMerchantPojo.add(mcPojo);       
                }   
                
            }
            TorqueHelper.closeStatement(pstmt, rs);
        }
        catch (Exception e)
        {
            cat.error("Error during getEntitiesByParent", e);
            throw new ErrorsException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }            
        }
        return lstMerchantPojo;
    }
    
    /**
     * 
     * @param permissionCode
     * @return
     * @throws ErrorsException 
     */
    public static String getTerminalPermissionsIdByCode(String permissionCode) throws ErrorsException
    {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;        
        String idNew=null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new ErrorsException();
            }
            String strQuery = sql_bundle.getString("getTerminalPermissionsIdByCode");
            cat.info(strQuery);
            pstmt = dbConn.prepareStatement(strQuery);
            pstmt.setString(1, permissionCode);
                      
            rs = pstmt.executeQuery();
            if (rs.next())
            {                
              idNew = rs.getString(1);
            }
            TorqueHelper.closeStatement(pstmt, rs);
        }
        catch (Exception e)
        {
            cat.error("Error during getTerminalPermissionsIdByCode", e);
            throw new ErrorsException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }            
        }
        return idNew;
    }
    
    
   /**
    * 
    * @param idNewPermission
    * @param entityId
    * @param parentId
    * @param permissionCode
    * @param userName
    * @param application
    * @param LogChangesId
    * @return
    * @throws ErrorsException 
    */
    public static boolean addEntityIdToPermission(String idNewPermission, String entityId, String parentId, 
                                                 String permissionCode, String userName, String application, int LogChangesId) throws ErrorsException
    {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        boolean result = false;
        //String idNew = null;
        try
        {
            //idNew = getTerminalPermissionsIdByCode(permissionCode);
            //if ( idNew != null )
            //{
                dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null)
                {
                    cat.error("Can't get database connection");
                    throw new ErrorsException();
                }
                String strQuery = sql_bundle.getString("addEntityIdToPermission");
                StringBuilder sql = new StringBuilder();
                sql.append("INSERT INTO [TerminalPermissionsByEntity] ([IdNew],[EntityId],[ParentEntityId],[TerminalPermissionsIdNew]) VALUES (?,?,?,?) ");
                sql.append("IF @@ROWCOUNT = 0 ");
                sql.append("BEGIN ");
                sql.append(" INSERT INTO LogChanges (date_time, log_change_type_id, key_value, old_value, new_value, application_name, user_name, instance, reason) ");
                sql.append(" VALUES (GETDATE(), ?, ?, ?, ?, 'support', ?, ?, ?) ");
                sql.append("END ");
                String info =  "entityId=["+entityId+"] ParentEntityId=["+parentId+"]";
                cat.info("Insert into TerminalPermissionsByEntity....");
                pstmt = dbConn.prepareStatement(sql.toString());
                String IdNewRecord = NumberUtil.getUniqueIdentifier();
                pstmt.setString(1, IdNewRecord);
                pstmt.setString(2, entityId);
                pstmt.setString(3, parentId);
                pstmt.setString(4, idNewPermission);
                pstmt.setInt(5, LogChangesId);
                pstmt.setString(6, IdNewRecord);
                pstmt.setString(7, "");
                pstmt.setString(8, info);
                pstmt.setString(9, userName);
                pstmt.setString(10, application);
                pstmt.setString(11, "AUTOMATIC INSERT ACTION");
                
                int irows = pstmt.executeUpdate();
                                
                /*
                if ( irows == 1)
                {
                    result = true;
                    LogChanges logChangeControl = new LogChanges();
                    int nResult = logChangeControl.getLogChangeTypeIdByNameAndTable("Insert","TerminalPermissionsByEntity");
                    String info =  "entityId=["+entityId+"] ParentEntityId=["+parentId+"]";
                    logChangeControl.log_changed(nResult, IdNewRecord, info, "", "AUTOMATIC INSERT ACTION", userName, application);                   
                }
                */ 
                pstmt.close();
                pstmt = null;
            //}            
                       
        }
        catch (Exception e)
        {
            cat.error("Error during addEntityIdToPermission", e);
            throw new ErrorsException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }            
        }
        return result;
    }
    
    /**
     * 
     * @param idNew
     * @param userName
     * @param application
     * @param entityId
     * @param parentId
     * @return
     * @throws ErrorsException 
     */
    public static boolean deleteEntityIdPermission(String idNew, String userName, String application, String entityId, String parentId) throws ErrorsException
    {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        boolean result = false;
        
        try
        {            
            if ( idNew != null )
            {
                dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null)
                {
                    cat.error("Can't get database connection");
                    throw new ErrorsException();
                }
                String strQuery = sql_bundle.getString("deleteEntityIdPermission");
                cat.info(strQuery);
                pstmt = dbConn.prepareStatement(strQuery);
                pstmt.setString(1, idNew);               
                int irows = pstmt.executeUpdate();
                if ( irows == 1)
                {
                    result = true;
                    LogChanges logChangeControl = new LogChanges();
                    int nResult = logChangeControl.getLogChangeTypeIdByNameAndTable("Delete","TerminalPermissionsByEntity");
                    String info =  "entityId=["+entityId+"] ParentEntityId=["+parentId+"]";
                    logChangeControl.log_changed(nResult, idNew, info, "", "AUTOMATIC DELETE ACTION", userName, application);
                }
                
                pstmt.close();
                pstmt = null;
            }            
                       
        }
        catch (Exception e)
        {
            cat.error("Error during deleteEntityIdPermission", e);
            throw new ErrorsException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }            
        }
        return result;
    }
    
}

