package com.debisys.tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;

public class AccountExecutive 
{
	
	// log4j logging
	public static Category cat = Category.getInstance(BonusThreshold.class);

	// SQL
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");

	
	private int id;
	private long actorId;
	private String nameExecutiveLink;
	private String nameExecutive;
	private boolean enable;
	private String enableCheckBox;
	private String descriptionStatus;
	
	public AccountExecutive()
	{
		
	}

	
	/**
	 * @param idAuto
	 * @param actorIdP
	 * @param nameExecutiveP
	 * @param enableP
	 */
	public AccountExecutive(int idAuto,long actorIdP,String nameExecutiveP,boolean enableP,SessionData sessionData)
	{
		this.id=idAuto;
		this.actorId=actorIdP;
		this.nameExecutive = nameExecutiveP;
		this.setNameExecutive(nameExecutiveP);
		
		this.setEnable(enableP,sessionData);	
		
		this.setNameExecutiveLink(nameExecutiveP);
		this.setDescriptionStatus("");
	}
	
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the actorId
	 */
	public long getActorId() {
		return actorId;
	}

	/**
	 * @param actorId the actorId to set
	 */
	public void setActorId(long actorId) {
		this.actorId = actorId;
	}

	/**
	 * @return the nameExecutive
	 */
	public String getNameExecutive() 
	{		
		return nameExecutive;
	}

	/**
	 * @param nameExecutive the nameExecutive to set
	 */
	public void setNameExecutive(String nameExecutive) {
		this.nameExecutive = nameExecutive;
		
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * @param enable the enable to set
	 */
	public void setEnable(boolean enable, SessionData sessionData) 
	{
		this.enable = enable;
		String statusControl="";
		
		this.descriptionStatus = Languages.getString("jsp.admin.reports.tools.accountexecutives.enabled", sessionData.getLanguage());
		
		//disabled=\"disabled\"
		String onclickFunctionJavaScript="sendUpdate(this.checked,"+this.id+");";
		if (enable)
		{
			statusControl="<input id=\""+this.id+"\" type=\"checkbox\" checked=\"checked\" onclick=\""+onclickFunctionJavaScript+"\" >";
		}
		else
		{
			//disabled=\"disabled\"
			statusControl="<input id=\""+this.id+"\" type=\"checkbox\" onclick=\""+onclickFunctionJavaScript+"\" >";
			//this.descriptionStatus = Languages.getString("jsp.admin.reports.tools.accountexecutives.disabled", sessionData.getLanguage());
			
		}		
		this.setEnableCheckBox(statusControl);
		
	}
	
	
	/**
	 * @param name
	 * @param status
	 * @param idActor
	 */
	public static void addExecutive(String name,boolean status,long idActor)
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL ="";
	    
		try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}
	    	
	    	name = org.apache.commons.lang.StringEscapeUtils.escapeSql(name);
	    	if (status)
	    	{
	    		strSQL = "IF (SELECT COUNT(*) FROM AccountExecutives WHERE name='"+name+"')=0 INSERT INTO AccountExecutives ([name],[repId],[enable]) VALUES ('"+name+"',"+idActor+",1)";
	    	}
	    	else
	    	{
	    		strSQL = "IF (SELECT COUNT(*) FROM AccountExecutives WHERE name='"+name+"')=0 INSERT INTO AccountExecutives ([name],[repId],[enable]) VALUES ('"+name+"',"+idActor+",0)";
	    	}
	    	
	    	pstmt = dbConn.prepareStatement(strSQL);
	      	int records = pstmt.executeUpdate();

	      	if (records==1)
	      	{	
	      		cat.info("Insert Executive :"+name);
	      	}
	      	
	      	pstmt.close();	      	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during findExecutiveById", e);
	    	
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection on findExecutiveById", e);
	    	}
	    }	
		
	}
	
	/**
	 * @param name
	 * @param status
	 * @param id
	 */
	public static String updateExecutive (String name,int id,SessionData sessionData)
	{
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL ="";
	    String messageAction="";
		try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}
	    	
	    	name = org.apache.commons.lang.StringEscapeUtils.escapeSql(name);
	    	//if (status)
	    	//{
	    		strSQL = "UPDATE accountexecutives SET name='"+name+"' WHERE id="+id;
	    	//}
	    	//else
	    	//{
	    	//	strSQL = "if (select count(*) from merchants where ExecutiveAccountId="+id+")=0 begin UPDATE accountexecutives SET name='"+name+"' WHERE id="+id+" end";
	    	//}
	    	
	    	pstmt = dbConn.prepareStatement(strSQL);
	      	int records = pstmt.executeUpdate();

	      	if (records==1)
	      	{	
	      		cat.info("Update Executive :"+name);
	      	}
	      	else
	      	{
	      		messageAction=name+" "+Languages.getString("jsp.admin.reports.tools.accountexecutives.message", sessionData.getLanguage());
	      	}
	      	
	      	pstmt.close();	      	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during findExecutiveById", e);
	    	
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection on findExecutiveById", e);
	    	}
	    }
	    return messageAction;
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static AccountExecutive findExecutiveById(int id,SessionData sessionData)
	{
		AccountExecutive account = new AccountExecutive();
		
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL ="";
	    
		try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	strSQL = "select id,name,repId,enable from accountexecutives where id="+id;
	    	
	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	if (rs.next())
	      	{	
	      		account  = new AccountExecutive(rs.getInt(1), rs.getLong(3), rs.getString(2), rs.getBoolean(4),sessionData);	    		
	      	}
	      	rs.close();
	      	pstmt.close();	      	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during findExecutiveById", e);
	    	
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection on findExecutiveById", e);
	    	}
	    }
		return account;
		
	}
	
	
	/**
	 * This gets all of the current providers in the system.
	 * @param actor
	 * @param validateStatus
	 * @return
	 */
	public static ArrayList<AccountExecutive> findExecutivesByActor(long actor,HttpServletRequest request,boolean validateStatus,SessionData sessionData) 
	{
		ArrayList<AccountExecutive> executive = new ArrayList<AccountExecutive>();
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL = sql_bundle.getString("getProviders");

	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	cat.debug(strSQL);

	    	if (validateStatus)
	    	{
	    		strSQL = "select id,name,repId,enable from accountexecutives where repId="+actor+" and enable=1 order by name";
	    	}
	    	else
	    	{
	    		strSQL = "select id,name,repId,enable from accountexecutives where repId="+actor+" order by name";
	    	}
	    	
	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{	
	      		executive.add(new AccountExecutive(rs.getInt(1), rs.getLong(3), rs.getString(2), rs.getBoolean(4),sessionData));
	      	}
	      	rs.close();
	      	pstmt.close();	      	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during findExecutivesByActor", e);
	    	
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection on findExecutivesByActor", e);
	    	}
	    }
	    return executive;
	}

	
	public static ArrayList<String> updateAllByStatus(int status,String strRefId)
	{
		
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL ="";
	    ArrayList<String> statusAll = new ArrayList<String>();
	    ArrayList<String> sqlStatusAll = new ArrayList<String>();
	    
		try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	strSQL = "SELECT a.id,a.name, b.merchant_id, b.dba FROM accountexecutives a with(nolock) LEFT OUTER JOIN merchants b with(nolock) on a.Id=b.ExecutiveAccountId WHERE a.RepId="+strRefId;
	    	
	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	while(rs.next())
	      	{
	      		if ( rs.getLong(3) >  0 && status==0 )
	      		{
	      			statusAll.add(rs.getString(2)+" is used by: ["+rs.getString(4)+"/"+rs.getLong(3)+"]");
	      		}
	      		else
	      		{
	      			if (status==1)
	      			{
	      				sqlStatusAll.add("update accountexecutives set Enable=1 where id="+rs.getInt(1));
	      			}
	      			else
	      			{
	      				sqlStatusAll.add("update accountexecutives set Enable=0 where id="+rs.getInt(1));
	      			}
	      		}
	      	}
	      	
	      	rs.close();
	      	pstmt.close();
	      		      		
      		for(String sqlUpdateStatus : sqlStatusAll)
      		{
      			System.out.println(sqlUpdateStatus);
      			pstmt = dbConn.prepareStatement(sqlUpdateStatus);
      			pstmt.executeUpdate();
      		}	
      		pstmt.close();
	      	
	      		      	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during updateAllByStatus ", e);	    	
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection on updateAllByStatus ", e);
	    	}
	    }
	    return statusAll;
	}

	
	
	/**
	 * @param row
	 * @param status
	 * @return
	 */
	public static ArrayList<String> updateStatusById(String row,String status)
	{
		
		Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    String strSQL ="";
	    ArrayList<String> statusAll = new ArrayList<String>();
	    ArrayList<String> sqlStatusAll = new ArrayList<String>();
	    boolean foundRows=false;
	    
		try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		throw new ReportException();
	    	}

	    	strSQL = "SELECT a.id,a.name, b.merchant_id, b.dba FROM accountexecutives a with(nolock) INNER JOIN merchants b with(nolock) on a.Id=b.ExecutiveAccountId where a.id="+row;
	    	
	    	pstmt = dbConn.prepareStatement(strSQL);
	      	ResultSet rs = pstmt.executeQuery();

	      	while(rs.next())
	      	{
	      		statusAll.add(rs.getString(2)+" is used by: ["+rs.getLong(3)+"/"+rs.getString(4)+"] ");
	      		foundRows=true;
	      	}
	      	
	      	if ( !foundRows )
	      	{
	      		sqlStatusAll.add("update accountexecutives set Enable="+status+" where id="+row);	      		
	      	}
	      	
	      	rs.close();
	      	pstmt.close();
	      		      		
      		for(String sqlUpdateStatus : sqlStatusAll)
      		{
      			System.out.println(sqlUpdateStatus);
      			pstmt = dbConn.prepareStatement(sqlUpdateStatus);
      			pstmt.executeUpdate();
      		}	
      		pstmt.close();
	      	
	      		      	
	    }
	    catch (Exception e)
	    {
	    	cat.error("Error during updateAllByStatus ", e);	    	
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("Error during closeConnection on updateAllByStatus ", e);
	    	}
	    }
	    return statusAll;
	}

	
	/**
	 * @param nameExecutiveLink the nameExecutiveLink to set
	 */
	public void setNameExecutiveLink(String nameExecutiveLink) {
		this.nameExecutiveLink = "<a href='admin/tools/accountexecutives.jsp?typeAction=e&id="+this.getId()+"' style=\"width: 190px\" >"+nameExecutiveLink+"</a>";
	}


	/**
	 * @return the nameExecutiveLink
	 */
	public String getNameExecutiveLink() {
		return nameExecutiveLink;
	}


	/**
	 * @param enableCheckBox the enableCheckBox to set
	 */
	public void setEnableCheckBox(String enableCheckBox) {
		this.enableCheckBox = enableCheckBox;
	}


	/**
	 * @return the enableCheckBox
	 */
	public String getEnableCheckBox() {
		return enableCheckBox;
	}


	/**
	 * @param descriptionStatus the descriptionStatus to set
	 */
	public void setDescriptionStatus(String descriptionStatus) {
		this.descriptionStatus = descriptionStatus;
	}


	/**
	 * @return the descriptionStatus
	 */
	public String getDescriptionStatus(SessionData sessionData) 
	{
		if (this.enable)
		{
			return Languages.getString("jsp.admin.reports.tools.accountexecutives.enabled", sessionData.getLanguage());
		}
		else
		{
			return Languages.getString("jsp.admin.reports.tools.accountexecutives.disabled", sessionData.getLanguage());
		}		
	}
	
}
