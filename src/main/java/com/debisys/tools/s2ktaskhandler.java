package com.debisys.tools;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.TransactionReport;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;
import com.emida.utils.dbUtils.TorqueHelper;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("deprecation")
public class s2ktaskhandler {

    // log4j logging
    public static Category cat = Category.getInstance(s2ktaskhandler.class);

    public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.sql");

    private SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
    private SimpleDateFormat oldformatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
    private Hashtable validationErrors = new Hashtable();
    private String companycode;
    private String location;
    private String shipto;
    private String jobid;
    private String jobstartDate = "";
    private String startdate = "";
    private String enddate = "";
    private Vector loadedagent = new Vector();
    private Vector loadedsub = new Vector();
    private Vector loadedrep = new Vector();
    private Vector loadedmerchants = new Vector();
    private String selectedagent = "";
    private String selectedsub = "";
    private String selectedrep = "";
    private String selectedmerchants = "";
    private int selectiontype = -1;
    private String selected_ids = "";
    private List<String> rateplansIdList = new ArrayList<String>();

    public Vector getloadedmerchants() {
        return this.loadedmerchants;
    }

    public Vector getloadedrep() {
        return this.loadedrep;
    }

    public Vector getloadedsub() {
        return this.loadedsub;
    }

    public Vector getloadedagent() {
        return this.loadedagent;
    }

    public String getselectedmerchants() {
        return this.selectedmerchants;
    }

    public String getselectedrep() {
        return this.selectedrep;
    }

    public String getselectedsub() {
        return this.selectedsub;
    }

    public String getselectedagent() {
        return this.selectedagent;
    }

    public String getjobstartDate() {
        return StringUtil.toString(this.jobstartDate);
    }

    public String getStartDate() {
        return StringUtil.toString(this.startdate);
    }

    public String getEndDate() {
        return StringUtil.toString(this.enddate);
    }

    public String getcompanycode() {
        return StringUtil.toString(this.companycode);
    }

    public String getLocation() {
        return StringUtil.toString(this.location);
    }

    public String getshipto() {
        return StringUtil.toString(this.shipto);
    }

    public String getjobid() {
        return StringUtil.toString(this.jobid);
    }

    public void setjobid(String temp) {
        this.jobid = temp;
    }

    public void setcompanycode(String temp) {
        this.companycode = temp;
    }

    public void setjobstartDate(String temp) {
        this.jobstartDate = temp;
    }

    public void setStartDate(String temp) {
        this.startdate = temp;
    }

    public void setEndDate(String temp) {
        this.enddate = temp;
    }

    public void setLocation(String temp) {
        this.location = temp;
    }

    public void setshipto(String temp) {
        this.shipto = temp;
    }

    public String getSelectedIds() {
        return StringUtil.toString(this.selected_ids);
    }

    public void setSelectedIds(String strValue) {
        this.selected_ids = strValue;
    }

    public int getSelectionType() {
        return this.selectiontype;
    }

    public boolean hasall(String[] array) {
        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                if (array[i].equals("all")) {
                    return true;
                }
            }
        }
        return false;
    }

    public void deletemerchants(String jobid) {

        Connection dbConn = null;
        CallableStatement cstmt = null;
        String strSQL = sql_bundle.getString("deletemerchants");

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("deletemerchants: Can't get database connection");

            }
            cat.debug(strSQL);

            cstmt = dbConn.prepareCall(strSQL);
            cstmt.setLong(1, Long.valueOf(jobid));
            cstmt.execute();
            cstmt.close();
        } catch (Exception e) {
            cat.error("Error during deletemerchants", e);
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("deletemerchants: Error during closeConnection", e);
            }
        }
    }
    
    public void deleteS2kRatePlans(String jobid) {

        s2ktaskhandler.cat.debug("deleteS2kRateplans jobid " + jobid);
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("s2kDeleteRatePlans");
            cat.debug("deleteS2kRateplans SQL:" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setBigDecimal(1, new BigDecimal(jobid));
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error on deleteS2kRateplans ", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public int addmerchants(String strRefId, String jobid) {
        String merchantlist = "";
        if (this.selectiontype == 8) {
            if (this.selected_ids != null && !this.selected_ids.equals("") && !this.selected_ids.contains("all")) {
                merchantlist = this.selected_ids;
            }
            else {
                return 1;//Failed
            }
        }

        if (this.selectiontype != 8 && this.selectiontype != 7 && this.selectiontype != 6) {
            s2ktaskhandler.cat.debug("insert repagentsub data...starting");
            this.insertsaveddata(this.selected_ids, jobid, this.selectiontype);
        }
        else {
            if (this.selectiontype == 7 && this.selectiontype == 6) {
                s2ktaskhandler.cat.debug("insertmerchants67...starting");
                this.insertsaveddata("ALL", jobid, this.selectiontype);
                s2ktaskhandler.cat.debug("insertmerchants67...done");
            }
            else {
                s2ktaskhandler.cat.debug("insertmerchants8...starting");
                this.insertsaveddata(merchantlist, jobid, this.selectiontype);
                s2ktaskhandler.cat.debug("insertmerchants8...done");
            }

        }

        return 0;//Successful

    }
    
    public void addRateplans(String[] ratePlansList, String jobid) {
        for (int i = 0; i < ratePlansList.length; i++) {
            if(ratePlansList[0].equalsIgnoreCase("ALL")){
                return;
            }
            inserts2kRateplan(jobid, ratePlansList[i].replace("\n", "").replace("\r", ""));
        }
    }

    @SuppressWarnings("unchecked")
    public void getbillinglevel(String jobid, String strDistChainType) {
        try {
			// selectiontype indicates an action or selected_ids type

			//6 user ISO, action= load all merchants under ISO
            //4 user ISO5 load all under agents' list
            //7 user ISO3, action= load all merchants under user
            //1 load all from list of reps
            //5  user ISO5 or Agent load all under sub-agents' list
            //8 merchants	
            //0 No merchants selected
            Vector data = this.gets2kselected(jobid);
            Iterator it = data.iterator();
            while (it.hasNext()) {
                Vector vecTemp = null;
                vecTemp = (Vector) it.next();
                this.selectiontype = Integer.valueOf(String.valueOf(vecTemp.get(1)));
                s2ktaskhandler.cat.debug("type===>" + this.getSelectionType());

                if (this.selectiontype != 7 || this.selectiontype != 6) {
                    if (this.selectiontype == 4) {
                        s2ktaskhandler.cat.debug("adding" + vecTemp.get(0).toString());
                        this.loadedagent.add(vecTemp.get(0).toString());
                    }
                    else if (this.selectiontype == 1) {
                        this.loadedrep.add(vecTemp.get(0).toString());
                    }
                    else if (this.selectiontype == 5) {
                        this.loadedsub.add(vecTemp.get(0).toString());
                    }
                    else if (this.selectiontype == 8) {
                        this.loadedmerchants.add(vecTemp.get(0).toString());
                    }
                }
            }

            this.selectedsub = "";
            this.selectedrep = "";
            this.selectedagent = "";
            this.selectedmerchants = "";

            if (this.selectiontype != 7 && this.selectiontype != 6) {
                if (this.selectiontype == 4) {

                    for (int i = 0; i < this.loadedagent.size(); i++) {
                        s2ktaskhandler.cat.debug("i" + i);
                        if (i != 0) {
                            this.selectedagent += "," + (String) this.loadedagent.get(i);
                        }
                        else {
                            this.selectedagent = (String) this.loadedagent.get(i);
                        }
                    }
                }
                else if (this.selectiontype == 5) {

                    //load all selected sub
                    this.selectedsub = "";
                    for (int i = 0; i < this.loadedsub.size(); i++) {
                        if (i != 0) {
                            this.selectedsub += "," + (String) this.loadedsub.get(i);
                        }
                        else {
                            this.selectedsub = (String) this.loadedsub.get(i);
                        }
                    }
                    this.selectedagent = TransactionReport.getParentList(this.selectedsub);

                }
                else if (this.selectiontype == 1) {

                    for (int i = 0; i < this.loadedrep.size(); i++) {
                        if (i != 0) {
                            this.selectedrep += "," + (String) this.loadedrep.get(i);
                        }
                        else {
                            this.selectedrep = (String) this.loadedrep.get(i);
                        }
                    }
                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        this.selectedsub = TransactionReport.getParentList(this.selectedrep);
                        this.selectedagent = TransactionReport.getParentList(this.selectedsub);
                    }

                }
                else if (this.selectiontype == 8) {
                    for (int i = 0; i < this.loadedmerchants.size(); i++) {
                        if (i != 0) {
                            this.selectedmerchants += "," + (String) this.loadedmerchants.get(i);
                        }
                        else {
                            this.selectedmerchants = (String) this.loadedmerchants.get(i);
                        }
                    }
                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        this.selectedrep = TransactionReport.getRepListofMerchants(this.selectedmerchants);
                        this.selectedsub = TransactionReport.getParentList(this.selectedrep);
                        this.selectedagent = TransactionReport.getParentList(this.selectedsub);
                    }
                    else {
                        this.selectedrep = TransactionReport.getRepListofMerchants(this.selectedmerchants);
                    }
                }
            }
        } catch (Exception e) {
            cat.error("Error during getbillinglevel", e);
        }

    }

    public void load(String strDistChainType, String[] agentlist, String[] subagentlist, String[] replist, String[] merchantlist) {
        try {
				// selectiontype indicates an action or selected_ids type
            //6 user ISO, action= load all merchants under ISO
            //4 user ISO5 load all under agents' list
            //7 user ISO3, action= load all merchants under user
            //1 load all from list of reps
            //5  user ISO5 or Agent load all under sub-agents' list
            //8 merchants	
            //0 No merchants selected

            this.selectedsub = "";
            this.selectedrep = "";
            this.selectedagent = "";
            this.selectedmerchants = "";
            if (this.selectiontype == 0) {
                if (agentlist != null) {
                    for (int i = 0; i < agentlist.length; i++) {
                        s2ktaskhandler.cat.debug("i" + i);
                        if (i != 0) {
                            this.selectedagent += "," + agentlist[i].replaceAll("\\r\\n|\\r|\\n", " ");
                        }
                        else {
                            this.selectedagent = agentlist[i].replaceAll("\\r\\n|\\r|\\n", "");
                        }
                    }
                }

                if (subagentlist != null) {
                    for (int i = 0; i < subagentlist.length; i++) {
                        if (i != 0) {
                            this.selectedsub += "," + subagentlist[i];
                        }
                        else {
                            this.selectedsub = subagentlist[i].replaceAll("\\r\\n|\\r|\\n", " ");
                        }
                    }
                }

                if (replist != null) {
                    for (int i = 0; i < replist.length; i++) {
                        if (i != 0) {
                            this.selectedrep += "," + replist[i];
                        }
                        else {
                            this.selectedrep = replist[i];
                        }
                    }
                }

                if (merchantlist != null) {
                    for (int i = 0; i < merchantlist.length; i++) {
                        if (i != 0) {
                            this.selectedmerchants += "," + merchantlist[i];
                        }
                        else {
                            this.selectedmerchants = merchantlist[i];
                        }
                    }
                }
            }
            else if (this.selectiontype != 7 && this.selectiontype != 6) {
                if (this.selectiontype == 4) {

                    for (int i = 0; i < agentlist.length; i++) {
                        s2ktaskhandler.cat.debug("i" + i);
                        if (i != 0) {
                            this.selectedagent += "," + agentlist[i].replaceAll("\\r\\n|\\r|\\n", " ");
                        }
                        else {
                            this.selectedagent = agentlist[i].replaceAll("\\r\\n|\\r|\\n", "");
                        }
                    }
                }
                else if (this.selectiontype == 5) {

                    //load all selected sub
                    this.selectedsub = "";
                    for (int i = 0; i < subagentlist.length; i++) {
                        if (i != 0) {
                            this.selectedsub += "," + subagentlist[i];
                        }
                        else {
                            this.selectedsub = subagentlist[i].replaceAll("\\r\\n|\\r|\\n", " ");
                        }
                    }
                    this.selectedagent = TransactionReport.getParentList(this.selectedsub);

                }
                else if (this.selectiontype == 1) {

                    for (int i = 0; i < replist.length; i++) {
                        if (i != 0) {
                            this.selectedrep += "," + replist[i];
                        }
                        else {
                            this.selectedrep = replist[i];
                        }
                    }
                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        this.selectedsub = TransactionReport.getParentList(this.selectedrep);
                        this.selectedagent = TransactionReport.getParentList(this.selectedsub);
                    }

                }
                else if (this.selectiontype == 8) {
                    for (int i = 0; i < merchantlist.length; i++) {
                        if (i != 0) {
                            this.selectedmerchants += "," + merchantlist[i];
                        }
                        else {
                            this.selectedmerchants = merchantlist[i];
                        }
                    }
                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        this.selectedrep = TransactionReport.getRepListofMerchants(this.selectedmerchants);
                        this.selectedsub = TransactionReport.getParentList(this.selectedrep);
                        this.selectedagent = TransactionReport.getParentList(this.selectedsub);
                    }
                    else {
                        this.selectedrep = TransactionReport.getRepListofMerchants(this.selectedmerchants);
                    }
                }
            }
        } catch (Exception e) {
            cat.error("Error during load", e);
        }

    }

    public void insertsaveddata(String slist, String jobid, int type) {
        slist = slist.replaceAll("\\r\\n|\\r|\\n", " ");
        s2ktaskhandler.cat.debug("merchantlist list " + slist);
        s2ktaskhandler.cat.debug("jobid " + jobid);
        if (slist == "All") {
            this.insertmerchant(slist.trim(), jobid, type);
        }
        String[] list = slist.split(",");
        if (list.length <= 25 && list.length > 0) {
            String temp = "";
            temp = list[0];
            for (int i = 1; i < list.length; i++) {
                temp += "," + list[i];
            }
            this.insertmerchant(temp, jobid, type);
        }
        else if (list.length > 25) {
            String temp = "";
            temp = list[0];
            for (int i = 1; i < 25; i++) {
                temp += "," + list[i];
            }
            this.insertmerchant(temp, jobid, type);
            String newlist = "";
            newlist = list[25];
            for (int i = 26; i < list.length; i++) {
                newlist += "," + list[i];
            }
            this.insertsaveddata(newlist, jobid, type);
        }
    }

    public void insertmerchant(String list, String jobid, int type) {

        s2ktaskhandler.cat.debug("insert list " + list);
        s2ktaskhandler.cat.debug("jobid " + jobid);
        Connection dbConn = null;
        CallableStatement cstmt = null;
        String strSQL = sql_bundle.getString("insertmerchants");

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("insertmerchant: Can't get database connection");

            }
            cat.debug(strSQL);

            cstmt = dbConn.prepareCall(strSQL);
            cstmt.setLong(1, Long.valueOf(jobid));
            cstmt.setString(2, list.trim());
            cstmt.setInt(3, type);
            cstmt.execute();
            cstmt.close();
        } catch (Exception e) {
            cat.error("Error during insertmerchant", e);
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("insertmerchant: Error during closeConnection", e);
            }
        }
    }
    
    public void inserts2kRateplan(String jobid, String ratePlanId) {

        s2ktaskhandler.cat.debug("inserts2kRateplan jobid " + jobid+ "ratePlanId "+ratePlanId);
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            strSQL = sql_bundle.getString("s2kInsertRatePlan");
            cat.debug("inserts2kRateplan SQL:" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, uuid);
            pst.setBigDecimal(2, new BigDecimal(jobid));
            pst.setInt(3, Integer.parseInt(ratePlanId));
            pst.executeUpdate();

        } catch (Exception e) {
            cat.error("Error on inserts2kRateplan ", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public void setSelectedList(String strAccessLevel, String strDistChainType, String strRefId, String[] strselectagentslist, String[] strsubagentslist,
            String[] strrepslist, String[] strmerchantslist) {
        String[] strValue = null;
			// selectiontype indicates an action or selected_ids type
        //6 user ISO, action= load all merchants under ISO
        //4 user ISO5 load all under agents' list
        //7 user ISO3, action= load all merchants under user
        //1 load all from list of reps
        //5  user ISO5 or Agent load all under sub-agents' list
        //8 merchants	
        //0 No merchants selected
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
            if (this.hasall(strselectagentslist)) {
                this.selectiontype = 6;
            }
            else if (this.hasall(strsubagentslist)) {
                strValue = strselectagentslist;
                this.selectiontype = 4;

            }
            else if (this.hasall(strrepslist)) {
                strValue = strsubagentslist;
                this.selectiontype = 5;
            }
            else if (this.hasall(strmerchantslist)) {
                strValue = strrepslist;
                this.selectiontype = 1;

            }
            else if (strmerchantslist != null) {
                if (strmerchantslist.length > 0) {
                    strValue = strmerchantslist;
                    this.selectiontype = 8;
                }
            }
            else {
                this.selectiontype = 0;
            }
        }
        else {
            if (this.hasall(strrepslist)) {
                this.selectiontype = 7;
            }
            else if (this.hasall(strmerchantslist)) {
                strValue = strrepslist;
                this.selectiontype = 1;
            }
            else if (strmerchantslist != null) {
                if (strmerchantslist.length > 0) {
                    strValue = strmerchantslist;
                    this.selectiontype = 8;
                }
            }
            else {
                this.selectiontype = 0;
            }
        }

        if (this.selectiontype != 0) {

            String strTemp = "";
            if (strValue != null && strValue.length > 0) {
                boolean isFirst = true;
                for (int i = 0; i < strValue.length; i++) {
                    if (strValue[i] != null && !strValue[i].equals("") && !strValue[i].equals("all")) {
                        if (!isFirst) {
                            strTemp = strTemp + "," + strValue[i].replaceAll("\\r\\n|\\r|\\n", "");
                        }
                        else {
                            strTemp = strValue[i].replaceAll("\\r\\n|\\r|\\n", "");
                            isFirst = false;
                        }
                    }
                }
            }

            this.selected_ids = strTemp;
            s2ktaskhandler.cat.debug("==>selected_ids" + this.selected_ids);
        }
        s2ktaskhandler.cat.debug("==>selectiontype" + this.selectiontype);

    }

    public String[] convertListToArray(String strValue) {
        String[] strTemp = null;
        if (strValue != null && strValue.length() > 0) {
            strTemp = strValue.split(",");
        }

        return strTemp;
    }

    /**
     * Returns a hash of errors.
     *
     * @return Hash of errors.
     */
    public Hashtable getErrors() {
        return this.validationErrors;
    }

    public void clearvalidation() {
        this.validationErrors.clear();
    }

    public boolean validateDateRange(SessionData sessionData) {
        this.validationErrors.clear();
        boolean valid = true;

        if ((this.startdate == null) || (this.startdate.length() == 0)) {
            this.addFieldError("startdate", Languages.getString("jsp.admin.tools.s2k.error1", sessionData.getLanguage()));
            valid = false;
        }
        else if (!DateUtil.isValidDateTime(this.startdate)) {
            this.addFieldError("startdate", Languages.getString("jsp.admin.tools.s2k.error2", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.enddate == null) || (this.enddate.length() == 0)) {
            this.addFieldError("enddate", Languages.getString("jsp.admin.tools.s2k.error3", sessionData.getLanguage()));
            valid = false;
        }
        else if (!DateUtil.isValidDateTime(this.enddate)) {
            this.addFieldError("enddate", Languages.getString("jsp.admin.tools.s2k.error4", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.jobstartDate == null) || (this.jobstartDate.length() == 0)) {
            this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error5", sessionData.getLanguage()));
            valid = false;
        }
        else if (!DateUtil.isValidDateTime(this.jobstartDate)) {
            this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error6", sessionData.getLanguage()));
            valid = false;
        }

        if ((valid)) {

            Date dtStartDate = new Date();
            Date dtEndDate = new Date();
            Date dtJobDate = new Date();
            Date dtcurrentday = new Date();
            String currentday = this.formatter.format(Calendar.getInstance().getTime());
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            formatter.setLenient(false);
            try {
                dtStartDate = formatter.parse(this.startdate);
            } catch (ParseException ex) {
                this.addFieldError("startdate", Languages.getString("jsp.admin.tools.s2k.error2", sessionData.getLanguage()));
                valid = false;
            }
            try {
                dtEndDate = formatter.parse(this.enddate);
            } catch (ParseException ex) {
                this.addFieldError("enddate", Languages.getString("jsp.admin.tools.s2k.error4", sessionData.getLanguage()));
                valid = false;
            }
            try {
                dtJobDate = formatter.parse(this.jobstartDate);
                dtcurrentday = formatter.parse(currentday);

            } catch (ParseException ex) {
                this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error6", sessionData.getLanguage()));
                valid = false;
            }
            if (dtStartDate.after(dtEndDate)) {
                this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error7", sessionData.getLanguage()));
                valid = false;
            }
            if (dtEndDate.after(dtJobDate)) {
                this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error8", sessionData.getLanguage()));
                valid = false;
            }
            if (dtcurrentday.after(dtJobDate) || dtcurrentday.equals(dtJobDate)) {
                this.addFieldError("jobdate", Languages.getString("jsp.admin.tools.s2k.error10", sessionData.getLanguage()));
                valid = false;
            }

        }

        return valid;
    }

    public void addFieldError(String fieldname, String error) {
        this.validationErrors.put(fieldname, error);
    }

    public String getFieldError(String fieldname) {
        return ((String) this.validationErrors.get(fieldname));
    }

    public String inserttask(String isoid, String jobdate, String startdate,
            String enddate, int status, String companycode, String location,
            String shipto, int loopcount, boolean loop, String strAccessLevel)
            throws Exception {

        Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, startdate, enddate, false);
        Vector vTimeZoneFilterJobDate = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, jobdate, jobdate, false);

        Connection dbConn = null;
        CallableStatement cstmt = null;
        String strSQL = sql_bundle.getString("inserttask");
        String result = "";
        int errorcode = -5;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");

            }
            cat.debug(strSQL);

            cstmt = dbConn.prepareCall(strSQL);
            cstmt.setLong(1, Long.parseLong(isoid));
            cstmt.setString(2, vTimeZoneFilterJobDate.get(0).toString());
            cstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
            cstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
            cstmt.setInt(5, status);
            cstmt.setString(6, companycode);
            cstmt.setString(7, location);
            cstmt.setString(8, shipto);
            cstmt.setInt(9, loopcount);
            cstmt.setBoolean(10, loop);
            cstmt.registerOutParameter(11, Types.INTEGER);
            cstmt.setInt(11, 0);
            cstmt.registerOutParameter(12, Types.NUMERIC);
            cstmt.setLong(12, 0);
            cstmt.execute();
            errorcode = cstmt.getInt(11);

            result = String.valueOf(cstmt.getLong(12));
            if (errorcode == -1) {
                result = "-1";
            }
            else if (errorcode == -4) {
                result = "-4";
            }
            cstmt.close();
        } catch (Exception e) {
            cat.error("Errorcode" + errorcode + " during inserttask", e);
            result = "-1";
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("inserttask: Error during closeConnection", e);
                result = "-1";
            }
        }
        return result;
    }

    public int edittask(String isoid, String jobid, String jobdate,
            String startdate, String enddate, String companycode,
            String location, String shipto, int loopcount, boolean loop,
            String strAccessLevel) throws Exception {

        cat.debug("isoid" + isoid);
        cat.debug("jobid" + jobid);
        cat.debug("jobdate" + jobdate);
        cat.debug("startdate" + startdate);
        cat.debug("enddate" + enddate);
        cat.debug("companycode" + companycode);
        cat.debug("location" + location);
        cat.debug("shipto" + shipto);
        cat.debug("loopcount" + loopcount);
        cat.debug("loop" + loop);
        cat.debug("strAccessLevel" + strAccessLevel);
        Connection dbConn = null;
        CallableStatement cstmt = null;
        String strSQL = sql_bundle.getString("updatetask");
        Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, startdate, enddate, false);
        Vector vTimeZoneFilterJobDate = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, jobdate, jobdate, false);

        int errorcode = -1;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");

            }
            cat.debug(strSQL);

            cstmt = dbConn.prepareCall(strSQL);
            cstmt.setLong(1, Long.parseLong(isoid));
            cstmt.setString(2, jobid);
            cstmt.setString(3, vTimeZoneFilterJobDate.get(0).toString());
            cstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
            cstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
            cstmt.setString(6, companycode);
            cstmt.setString(7, location);
            cstmt.setString(8, shipto);
            cstmt.setInt(9, loopcount);
            cstmt.setBoolean(10, loop);
            cstmt.registerOutParameter(11, Types.INTEGER);
            cstmt.setInt(11, 0);
            cstmt.execute();
            errorcode = cstmt.getInt(11);
            cstmt.close();
        } catch (Exception e) {
            cat.error("Errorcode" + errorcode + " during edittask", e);
            return -1;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("edittask: Error during closeConnection", e);
                errorcode = -1;
            }
        }
        return errorcode;

    }

    public int deletetask(String jobid) {
        Connection dbConn = null;
        CallableStatement cstmt = null;
        String strSQL = sql_bundle.getString("deletetask");
        int errorcode = -1;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");

            }
            cat.debug(strSQL);

            cstmt = dbConn.prepareCall(strSQL);
            cstmt.setLong(1, Long.parseLong(jobid));
            cstmt.registerOutParameter(2, Types.INTEGER);
            cstmt.setInt(2, 0);
            cstmt.execute();
            errorcode = cstmt.getInt(2);
            cstmt.close();
        } catch (Exception e) {
            cat.error("Error during deletetask", e);
            errorcode = -1;
        } finally {
            try {
                if (cstmt != null) {
                    cstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("deletetask: Error during closeConnection", e);
                errorcode = -1;
            }
        }
        return errorcode;
    }

    /**
     * *
     * This will get all tasks info specific to a jobid
     *
     * @param request
     * @param sessionData
     * @return
     * @throws ReportException
     */
    public Vector gets2kjob(String jobid, String strAccessLevel, String isoid) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        Vector vecResults = new Vector();

        String strSQL = sql_bundle.getString("getjobbyid");

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
            }

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, jobid);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                this.setjobid(String.valueOf(rs.getLong("jobid")));
                String jstart = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, rs.getString("jobdate"),
                        rs.getString("jobdate"), true).get(0).toString();
                this.setjobstartDate(this.formatter.format(this.oldformatter.parse(jstart).getTime()));
                Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, rs.getString("startDate"),
                        rs.getString("enddate"), true);
                this.setStartDate(this.formatter.format(this.oldformatter.parse(vTimeZoneFilterDates.get(0).toString()).getTime()));
                this.setEndDate(this.formatter.format(this.oldformatter.parse(vTimeZoneFilterDates.get(1).toString()).getTime()));

                this.setcompanycode(rs.getString("companycode"));
                this.setLocation(rs.getString("location"));
                this.setshipto(rs.getString("shipto"));
            }
            loadS2kRatePlans();
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("gets2kjob => Error during gets2kjob", e);
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("gets2kjob => Error during closeConnection ", e);
            }
        }
        return vecResults;
    }

    /**
     * *
     * This will get all jobs specific to an isoid
     *
     * @param request
     * @param sessionData
     * @return
     * @throws ReportException
     */
    public String gets2kjobattributes(String jobid, String strAccessLevel, String isoid) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        String vecResults = "ERROR";
        String strSQL = sql_bundle.getString("getalljobs");

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                vecResults = "ERROR";
            }

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, jobid);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {

                Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoid, rs.getString("startDate"),
                        rs.getString("enddate"), true);
                vecResults = this.formatter.format(this.oldformatter.parse(vTimeZoneFilterDates.get(0).toString()).getTime());
                vecResults += "=/=" + this.formatter.format(this.oldformatter.parse(vTimeZoneFilterDates.get(1).toString()).getTime());
                vecResults += "=/=" + rs.getString("companycode");
                vecResults += "=/=" + rs.getString("location");
                vecResults += "=/=" + rs.getString("shipto");
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("gets2kjobattributes => Error during gets2kjobattributes", e);
            vecResults = "ERROR";
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("gets2kjobattributes => Error during closeConnection ", e);
                vecResults = "ERROR";
            }
        }
        return vecResults;
    }

    /**
     * *
     *
     * @param request
     * @param sessionData
     * @return
     * @throws ReportException
     */
    public String getjobstatus(String isoId, String filter) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        String result = "";
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String strSQL = sql_bundle.getString("getjobstatus");
            if (dbConn == null) {
                s2ktaskhandler.cat.error("Can't get database connection");
                result = "ERROR";
            }
            s2ktaskhandler.cat.debug("sql=" + strSQL);
            s2ktaskhandler.cat.debug("isoId=" + isoId);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, isoId);
            ResultSet rs = pstmt.executeQuery();

            int s = -1;
            if (filter.equals("Scheduled")) {
                s = 0;
            }
            else if (filter.equals("Processing")) {
                s = 1;
            }
            else if (filter.equals("Completed")) {
                s = 2;
            }
            else if (filter.equals("Completed with Warnings")) {
                s = 3;
            }
            else if (filter.equals("Failed")) {
                s = 4;
            }

            if (s == -1) {
                while (rs.next()) {
                    result += String.valueOf(rs.getLong("jobid")) + ",";
                    result += String.valueOf(rs.getInt("status")) + ";";
                }
            }
            else {
                while (rs.next()) {
                    int status = rs.getInt("status");
                    if (status == s) {
                        result += String.valueOf(rs.getLong("jobid")) + ",";
                        result += String.valueOf(status) + ";";
                    }
                }
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            s2ktaskhandler.cat.error("getjobstatus => Error during getjobstatus", e);
            result = "ERROR";
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                result = "ERROR";
                s2ktaskhandler.cat.error("getjobstatus => Error during closeConnection ", e);
            }
        }
        s2ktaskhandler.cat.debug("stringgenerated=" + result);
        return result;
    }

    /**
     * *
     *
     * @param request
     * @param sessionData
     * @return
     * @throws ReportException
     */
    public Vector gets2klist(String isoId, String strAccessLevel) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        Vector vecResults = new Vector();
        String strSQL = sql_bundle.getString("getjoblist");
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");

            }

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, isoId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getLong("jobid"));
                String jstart = TimeZone.getTimeZoneFilterDates(strAccessLevel, isoId, rs.getString("jobdate"),
                        rs.getString("jobdate"), true).get(0).toString();
                vecTemp.add(this.formatter.format(this.oldformatter.parse(jstart).getTime()));
                vecTemp.add(rs.getInt("status"));
                vecResults.add(vecTemp);
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("gets2klist => Error during gets2klist", e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("gets2klist => Error during closeConnection ", e);
            }
        }
        return vecResults;
    }

    /**
     * *
     *
     * @param request
     * @param sessionData
     * @return
     * @throws ReportException
     */
    public Vector gets2kselected(String jobId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        Vector vecResults = new Vector();
        String strSQL = sql_bundle.getString("getallselected");
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");

            }

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, jobId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("entity"));
                vecTemp.add(rs.getInt("type"));
                vecResults.add(vecTemp);
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("gets2kselected => Error during gets2klist", e);

        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("gets2kselected => Error during closeConnection ", e);
            }
        }
        return vecResults;
    }
    
    private void loadS2kRatePlans() {
        if(getjobid() == null || getjobid().isEmpty()){
            return;
        }
        
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = sql_bundle.getString("getS2kRateplans");

            pstmt = dbConn.prepareStatement(sql);
            pstmt.setBigDecimal(1, new BigDecimal(getjobid()));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                rateplansIdList.add(rs.getString("rateplanId"));
            }
        } catch (Exception e) {
            cat.error("Error during loadS2kRatePlans", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
    }

    public boolean checkagentlist(String id) {
        if (this.selectedagent.contains(id)) {
            return true;
        }
        return false;
    }

    public boolean checksubagentlist(String id) {
        if (this.selectedsub.contains(id)) {
            return true;
        }
        return false;
    }

    public boolean checkreplist(String id) {
        if (this.selectedrep.contains(id)) {
            return true;
        }
        return false;
    }
    
    public boolean checkRatePlanlist(String id) {
        for(String rp : rateplansIdList){
            if(rp.equalsIgnoreCase(id)){
                return true;
            }
        }
        return false;
    }

    public List<String> getRateplansIdList() {
        return rateplansIdList;
    }
    
    
    

    public boolean checkmerchantlist(String id) {
        if (this.selectedmerchants.contains(id)) {
            return true;
        }
        return false;
    }

    

}
