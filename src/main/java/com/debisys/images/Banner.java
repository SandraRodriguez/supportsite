package com.debisys.images;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.BannerException;
import com.debisys.exceptions.BrandingException;
import com.debisys.exceptions.RatePlanException;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

public class Banner implements HttpSessionBindingListener {

    private static final String DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";

    @SuppressWarnings("deprecation")
    static Category cat = Category.getInstance(Banner.class);

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.images.sql");

    private long bannerId;

    private long imageId;

    private long displayTime;

    private Date enterDate;

    private Date endDate;

    private String description;

    private String link;

    private String[] repIds;

    /**
     * @return the bannerId
     */
    public long getBannerId() {
        return bannerId;
    }

    /**
     * @param bannerId the bannerId to set
     */
    public void setBannerId(long bannerId) {
        this.bannerId = bannerId;
    }

    public void valueBound(HttpSessionBindingEvent arg0) {

    }

    public void valueUnbound(HttpSessionBindingEvent arg0) {

    }

    /**
     * @param sessionData
     * @return Vector list of banners allowed in images table
     * @throws BrandingException
     */
    public Vector<Vector<String>> getBanners() throws BannerException {
        Connection dbConn = null;
        Vector<Vector<String>> vecBanners = new Vector<Vector<String>>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new RatePlanException();
            }
            String strSQL = sql_bundle.getString("getBanners");
            pstmt = dbConn.prepareStatement(strSQL);
            cat.debug(strSQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(StringUtil.toString(rs.getString("Id")));
                vecTemp.add(StringUtil.toString(rs.getString("DisplayTime")));
                setEnterDate(rs.getTimestamp("EnterDate"));
                vecTemp.add(getEnterDate() == null ? "" : getEnterDate());
                setEndDate(rs.getTimestamp("EndDate"));
                vecTemp.add(getEndDate() == null ? "" : getEndDate());
                vecTemp.add(StringUtil.toString(rs.getString("Link")));
                vecTemp.add(StringUtil.toString(rs.getString("Description")));
                vecTemp.add(StringUtil.toString(rs.getString("ImageFileName")));
                vecTemp.add(rs.getString("Priority"));
                vecBanners.add(vecTemp);
            }
            rs.close();
            strSQL = sql_bundle.getString("getBanners1");
            pstmt = dbConn.prepareStatement(strSQL);
            cat.debug(strSQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(StringUtil.toString(rs.getString("Id")));
                vecTemp.add(StringUtil.toString(rs.getString("DisplayTime")));
                setEnterDate(rs.getTimestamp("EnterDate"));
                vecTemp.add(getEnterDate() == null ? "" : getEnterDate());
                setEndDate(rs.getTimestamp("EndDate"));
                vecTemp.add(getEndDate() == null ? "" : getEndDate());
                vecTemp.add(StringUtil.toString(rs.getString("Link")));
                vecTemp.add(StringUtil.toString(rs.getString("Description")));
                vecTemp.add(StringUtil.toString(rs.getString("ImageFileName")));
                vecTemp.add(rs.getString("Priority"));
                vecBanners.add(vecTemp);
            }
            rs.close();
        } catch (Exception e) {
            cat.error("Error during getAllISOs", e);
            throw new BannerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecBanners;
    }

    /**
     * @return Vector list of all Isos with a flag turned on for the assigned
     * Isos to the given branding
     * @throws BrandingException
     */
    public Vector<Vector<String>> getAllISOs() throws BannerException {
        Connection dbConn = null;
        Vector<Vector<String>> isos = new Vector<Vector<String>>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new RatePlanException();
            }
            String strSQL = sql_bundle.getString("getAllISOsforBanners");
            pstmt = dbConn.prepareStatement(strSQL);
            cat.debug(strSQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
                vecTemp.add(StringUtil.toString(rs.getString("businessname")));
                isos.add(vecTemp);
            }
        } catch (Exception e) {
            cat.error("Error during getAllISOs", e);
            throw new BannerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return isos;
    }

    public boolean save() throws BannerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean ok = false;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new BannerException();
            }
            String strSQL = sql_bundle.getString(bannerId > 0 ? "updateBanner" : "createBanner");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, imageId);
            pstmt.setLong(2, displayTime);
            pstmt.setString(3, getEnterDate());
            pstmt.setString(4, getEndDate());
            pstmt.setString(5, description);
            pstmt.setString(6, link);
            if (bannerId > 0) {
                pstmt.setLong(7, bannerId);
                pstmt.execute();
                ok = true;
                cat.debug("Updating BannerId= " + bannerId);
            } else {
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    bannerId = rs.getLong("Id");
                    ok = true;
                }
                cat.debug("Creating BannerId= " + bannerId);
            }
        } catch (Exception e) {
            cat.error("Error during Banner.save", e);
            throw new BannerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        if (ok) {
            this.saveAssignedReps();
        }
        return ok;
    }

    public boolean load() throws BannerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean loaded = false;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new RatePlanException();
            }
            String strSQL = sql_bundle.getString("loadBanner");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, bannerId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                setImageId(rs.getLong("ImageId"));
                setDisplayTime(rs.getLong("DisplayTime"));
                setEnterDate(rs.getTimestamp("EnterDate"));
                setEndDate(rs.getTimestamp("EndDate"));
                setDescription(rs.getString("Description"));
                setLink(rs.getString("Link"));
                Vector<String> o = getAssignedReps();
                repIds = new String[o.size()];
                o.copyInto(repIds);
                loaded = true;
            }
            cat.debug("Getting banner values: banner=" + bannerId);
        } catch (Exception e) {
            cat.error("Error during Banner.load", e);
            throw new BannerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return loaded;
    }

    private void setEndDate(Date date) {
        endDate = date;
    }

    private void setEnterDate(Date date) {
        enterDate = date;
    }

    /**
     * @param imageId the imageId to set
     */
    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    /**
     * @return the imageId
     */
    public long getImageId() {
        return imageId;
    }

    /**
     * @param displayTime the displayTime to set
     */
    public void setDisplayTime(Long vdisplayTime) {
        if (vdisplayTime != null) {
            this.displayTime = vdisplayTime;
        } else {
            this.displayTime = 0;
        }
    }

    /**
     * @return the displayTime
     */
    public long getDisplayTime() {
        return displayTime;
    }

    /**
     * @param enterDate the enterDate to set
     */
    public void setEnterDate(String venterDate) {
        enterDate = null;
        if (venterDate != null && !venterDate.equals("")) {
            try {
                this.enterDate = new SimpleDateFormat(DATE_TIME_FORMAT).parse(venterDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return the enterDate
     */
    public String getEnterDate() {
        if (enterDate == null) {
            return null;
        }
        return new SimpleDateFormat(DATE_TIME_FORMAT).format(enterDate);
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String vendDate) {
        endDate = null;
        if (vendDate != null && !vendDate.equals("")) {
            try {
                this.endDate = new SimpleDateFormat(DATE_TIME_FORMAT).parse(vendDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        if (endDate == null) {
            return null;
        }
        return new SimpleDateFormat(DATE_TIME_FORMAT).format(endDate);
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    private void saveAssignedReps() {
        unassignAllReps();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new RatePlanException();
            }
            String strSQL = sql_bundle.getString("assignBannerToRep");
            for (String repId : repIds) {
                long rId = Long.parseLong(repId);
                if (rId > 0) {
                    pstmt = dbConn.prepareStatement(strSQL);
                    pstmt.setLong(1, bannerId);
                    pstmt.setLong(2, rId);
                    pstmt.execute();
                    cat.debug("Assigning rep " + repId + " to banner " + bannerId);
                }
            }
        } catch (Exception e) {
            cat.error("Error during Banner.assignRepId", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
    }

    private void unassignAllReps() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new RatePlanException();
            }
            String strSQL = sql_bundle.getString("unassignAllReps");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, bannerId);
            pstmt.execute();
            cat.debug("Removing reps for banner=" + bannerId);
        } catch (Exception e) {
            cat.error("Error during Banner.unassignAllReps", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
    }

    private Vector<String> getAssignedReps() throws BannerException {
        Connection dbConn = null;
        Vector<String> reps = new Vector<String>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new RatePlanException();
            }
            String strSQL = sql_bundle.getString("getAssignedReps");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, bannerId);
            cat.debug(strSQL);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                reps.add(StringUtil.toString(rs.getString("RepId")));
            }
            cat.debug("Getting all assgined isos for banner =" + bannerId);
        } catch (Exception e) {
            cat.error("Error during Banner.getAssignedIsos", e);
            throw new BannerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return reps;
    }

    /**
     * @param repIds the repIds to set
     */
    public void setRepIds(String[] repIds) {
        this.repIds = repIds;
    }

    /**
     * @return the repIds
     */
    public String[] getRepIds() {
        return repIds;
    }

    /**
     *
     * @return
     */
    public boolean delete() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;        
        boolean ok = false;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new BannerException();
            }
            String strSQL = "DELETE FROM PCTerminalBannersReps WHERE PCTerminalBannerId=?";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, bannerId);
            pstmt.executeUpdate();
            pstmt.close();
            strSQL = "DELETE FROM PCTerminalBanners WHERE Id=?";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, bannerId);
            if (pstmt.executeUpdate() == 1) {
                ok = true;
            }
            cat.debug("delete BannerId= " + bannerId + " sucessfully!!. ");
        } catch (Exception e) {
            cat.error("Error during delete ", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return ok;
    }
    
    /**
     * 
     * @param id
     * @param priority
     * @return 
     */
    public static boolean updatePriority(String id, String priority) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;        
        boolean ok = false;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new BannerException();
            }
            String strSQL = "UPDATE PCTerminalBanners SET Priority=? WHERE ID=?";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, priority);
            pstmt.setString(2, id);
            pstmt.executeUpdate();
            pstmt.close();            
            cat.debug("updatePriority id = [" + id + "] to = ["+priority+"] sucessfully!!. ");
        } catch (Exception e) {
            cat.error("Error during updatePriority ", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return ok;
    }
}
