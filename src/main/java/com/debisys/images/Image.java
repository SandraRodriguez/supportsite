package com.debisys.images;

import com.debisys.exceptions.BannerException;
import com.debisys.exceptions.ImageException;
import com.emida.utils.dbUtils.TorqueHelper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ResourceBundle;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import sun.misc.BASE64Encoder;

public class Image {

    @SuppressWarnings("deprecation")
    static Category cat = Category.getInstance(Banner.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.images.sql");

    private long imageId;
    private String name;
    private String type;
    private String alternativeText;
    private String description;
    private String lastUpdate;
    private String data;
    private String imageTypeId;
    private String errorMessage;

    public static Category getCat() {
        return cat;
    }

    public static void setCat(Category cat) {
        Image.cat = cat;
    }

    public static ResourceBundle getSql_bundle() {
        return sql_bundle;
    }

    public static void setSql_bundle(ResourceBundle sql_bundle) {
        Image.sql_bundle = sql_bundle;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlternativeText() {
        return alternativeText;
    }

    public void setAlternativeText(String alternativeText) {
        this.alternativeText = alternativeText;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getImageTypeId() {
        return imageTypeId;
    }

    public void setImageTypeId(String imageTypeId) {
        this.imageTypeId = imageTypeId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public long save() throws ImageException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new BannerException();
            }
            String strSQL = sql_bundle.getString(imageId > 0 ? "updateImage" : "createImage");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, name);
            pstmt.setString(2, type);
            pstmt.setString(3, alternativeText);
            pstmt.setString(4, description);
            pstmt.setString(5, this.imageTypeId);
            if (imageId > 0) {
                pstmt.setLong(6, imageId);
                pstmt.execute();
                cat.debug("Updating Image: Id= " + imageId + ", name = " + name + ", alt= " + alternativeText + ", description = " + description);
            } else {
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    imageId = rs.getLong(1);
                }
                cat.debug("Creating Image: Id= " + imageId + ", name= " + name + ", alt= " + alternativeText + ", description = " + description);
            }
        } catch (Exception e) {
            cat.error("Error during Image.save Id= " + imageId + ", name = " + name + ", alt= " + alternativeText + ", description = " + description, e);
            throw new ImageException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return imageId;
    }

    public boolean load() throws ImageException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        data = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new BannerException();
            }
            String strSQL = sql_bundle.getString("loadImage");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, "" + imageId);
            cat.debug("Getting data from Image: Id= " + imageId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                setName(rs.getString(1));
                setType(rs.getString(2));
                setAlternativeText(rs.getString(3));
                setDescription(rs.getString(4));
                setLastUpdate(rs.getString(5));
                if (rs.getObject(6) != null) {
                    setData(rs.getBytes(6));
                }
                setImageTypeId(rs.getString(7));
                return true;
            }
            return false;
        } catch (Exception e) {
            cat.error("Error during Image.load", e);
            throw new ImageException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
    }

    public void setData(byte[] buff) {
        this.data = new BASE64Encoder().encodeBuffer(buff);
    }

    public byte[] getDataBytes() {
        if (data != null) {
            try {
                return new sun.misc.BASE64Decoder().decodeBufferToByteBuffer(data).array();
            } catch (IOException e) {

            }
        }
        return null;
    }

    public void saveData() throws ImageException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new BannerException();
            }
            String strSQL = "EXEC sp_saveImageData ?, ?";
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, this.imageId);
            if (data != null && !data.equals("")) {
                byte[] dataBytes = getDataBytes();
                if (dataBytes.length < 4000000) {
                    pstmt.setBytes(2, dataBytes);
                } else {
                    throw new ImageException("jsp.admin.brandings.tool.saveError1");
                }

            } else {
                pstmt.setNull(2, Types.LONGVARBINARY);
            }
            pstmt.execute();
            cat.debug("Saving data for Image: Id= " + imageId);
            pstmt.execute();
        } catch (ImageException i) {
            cat.error("Error during Image.saveData ******* The Image has exceed the length 4 MB. ****** ");
            throw i;
        } catch (Exception e) {
            cat.error("Error during Image.saveData", e);
            throw new ImageException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
    }
}
