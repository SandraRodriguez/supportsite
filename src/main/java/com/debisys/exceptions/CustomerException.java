package com.debisys.exceptions;

public class CustomerException extends Exception {

	private static final long serialVersionUID = -7491089999932154736L;
	
	public CustomerException() {
		super();
	}
	
	public CustomerException(String message) {
		super(message);
	}
	
	public CustomerException(String message, Throwable cause) {
		super(message, cause);
	}

}
