package com.debisys.exceptions;

public class ReportException extends java.lang.Exception {

	private static final long serialVersionUID = 1L;
	
	public ReportException() {
		super();
	}
	
	public ReportException(String message, Throwable e) {
		super(message, e);
	}
}
