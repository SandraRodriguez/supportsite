package com.debisys.exceptions;

public class ImageException extends java.lang.Exception {

	private String errorMessage;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 
	 */
	public ImageException(){
		
	}
	
	/**
	 * 
	 * @param message
	 */
	public ImageException(String message){
		setErrorMessage(message);		
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
}
