/**
 * 
 */
package com.debisys.terminals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 * @author nmartinez
 *
 */
public class CarriersMobile {
	
	static Category cat = Category.getInstance(CarriersMobile.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
	
	
	private String idCarrier;
	private String codeCarrier;
	private String nameCarrier;
	private String descriptionCarrier;
	private String trxIdCarrier;
	
	
	
	/*
	 * 
	 */
	public CarriersMobile(String id, String code, String name, String description, String trxId)
	{
		this.idCarrier = id;
		this.codeCarrier = code;
		this.nameCarrier = name;
		this.descriptionCarrier = description;
		this.trxIdCarrier = trxId;		
	}
	
	
	/*
	 * 
	 */
	public static ArrayList<CarriersMobile> getCarriers(){
		
		ArrayList<CarriersMobile> carriers = new ArrayList<CarriersMobile>();
		Connection dbConn = null;
	    try
	    {
	        dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	        if (dbConn == null)
	        {
	          cat.error("Can't get database connection");
	          throw new Exception();
	        }
	        PreparedStatement pstmt = dbConn.prepareStatement(sql_bundle.getString("getCarriersMobile"));	        
	        ResultSet rs = pstmt.executeQuery();
	        while (rs.next())
	        {
	          carriers.add(new CarriersMobile(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5)));
	        }
	        rs.close();
	        pstmt.close();
	    }
	    catch (Exception e)
	    {
	        cat.error("Error during getCarriers", e);
	    }
	    finally
	    {
	        try
	        {
	          Torque.closeConnection(dbConn);
	        }
	        catch (Exception e)
	        {
	          cat.error("Error during release connection", e);
	        }
	    }
		return carriers;		
	}	
	
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return idCarrier;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.idCarrier = id;
	}



	/**
	 * @return the code
	 */
	public String getCode() {
		return codeCarrier;
	}



	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.codeCarrier = code;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return nameCarrier;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.nameCarrier = name;
	}



	/**
	 * @return the description
	 */
	public String getDescription() {
		return descriptionCarrier;
	}



	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.descriptionCarrier = description;
	}



	/**
	 * @return the trxId
	 */
	public String getTrxId() {
		return trxIdCarrier;
	}



	/**
	 * @param trxId the trxId to set
	 */
	public void setTrxId(String trxId) {
		this.trxIdCarrier = trxId;
	}


}
