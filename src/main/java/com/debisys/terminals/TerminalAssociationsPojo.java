/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.terminals;

/**
 *
 * @author nmartinez
 */
public class TerminalAssociationsPojo {
    
    private String id;
    private String terminalId;
    private String terminalName;    
    private String merchantId;
    private String merchantName;
    private String masterIdToAssociate;
    private String repName;
    private String repId;
    
        
    /**
     * 
     * @param id
     * @param terminalId
     * @param terminalName
     * @param merchantId
     * @param merchantName
     * @param repName
     * @param repId 
     */
    public TerminalAssociationsPojo(String id, String terminalId, String terminalName, String merchantId, 
                                    String merchantName, String repName, String repId){
        this.id = id;
        this.terminalId = terminalId;
        this.terminalName = terminalName;        
        this.merchantId = merchantId;
        this.merchantName = merchantName;
        this.repName = repName;
        this.repId = repId;
    }
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the terminalId
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * @param terminalId the terminalId to set
     */
    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    /**
     * @return the terminalName
     */
    public String getTerminalName() {
        return terminalName;
    }

    /**
     * @param terminalName the terminalName to set
     */
    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }   

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the masterIdToAssociate
     */
    public String getMasterIdToAssociate() {
        return masterIdToAssociate;
    }

    /**
     * @param masterIdToAssociate the masterIdToAssociate to set
     */
    public void setMasterIdToAssociate(String masterIdToAssociate) {
        this.masterIdToAssociate = masterIdToAssociate;
    }

    /**
     * @return the repName
     */
    public String getRepName() {
        return repName;
    }

    /**
     * @param repName the repName to set
     */
    public void setRepName(String repName) {
        this.repName = repName;
    }

    /**
     * @return the repId
     */
    public String getRepId() {
        return repId;
    }

    /**
     * @param repId the repId to set
     */
    public void setRepId(String repId) {
        this.repId = repId;
    }
}
