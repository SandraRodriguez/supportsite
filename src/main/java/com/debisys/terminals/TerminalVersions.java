package com.debisys.terminals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.TerminalException;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.dbUtils.TorqueHelper;

@SuppressWarnings("deprecation")
public class TerminalVersions
{
	static Category cat = Category.getInstance(TerminalVersions.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
	
	public TerminalVersions()
	{
		
	}
	
	private int Id;
	private int terminal_type_id;
	private String version;
	private boolean Check_Inv;
	private boolean Check_Ack;
	private boolean Allow_Audit;
	
	/**
	 * @return the id
	 */
	public int getId()
	{
		return Id;
	}
	
	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id)
	{
		Id = id;
	}
	
	/**
	 * @return the terminal_type_id
	 */
	public int getTerminal_type_id()
	{
		return terminal_type_id;
	}
	
	/**
	 * @param terminal_type_id
	 *            the terminal_type_id to set
	 */
	public void setTerminal_type_id(int terminal_type_id)
	{
		this.terminal_type_id = terminal_type_id;
	}
	
	/**
	 * @return the version
	 */
	public String getVersion()
	{
		return version;
	}
	
	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(String version)
	{
		this.version = version;
	}
	
	/**
	 * @return the check_Inv
	 */
	public boolean isCheck_Inv()
	{
		return Check_Inv;
	}
	
	/**
	 * @param check_Inv
	 *            the check_Inv to set
	 */
	public void setCheck_Inv(boolean check_Inv)
	{
		Check_Inv = check_Inv;
	}
	
	/**
	 * @return the check_Ack
	 */
	public boolean isCheck_Ack()
	{
		return Check_Ack;
	}
	
	/**
	 * @param check_Ack
	 *            the check_Ack to set
	 */
	public void setCheck_Ack(boolean check_Ack)
	{
		Check_Ack = check_Ack;
	}
	
	/**
	 * @return the allow_Audit
	 */
	public boolean isAllow_Audit()
	{
		return Allow_Audit;
	}
	
	/**
	 * @param allow_Audit
	 *            the allow_Audit to set
	 */
	public void setAllow_Audit(boolean allow_Audit)
	{
		Allow_Audit = allow_Audit;
	}
	
	/**
	 * @param sessionData
	 * @param context
	 * @param terminalType
	 * @return
	 */
	public static Vector<Vector<String>> getTerminalVersionsByTerminalType(SessionData sessionData, ServletContext context, String terminalType)
	{
		Vector<Vector<String>> vecTerminalTypes = new Vector<Vector<String>>();
		Connection dbConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TerminalException();
			}
			
			String strSQL = sql_bundle.getString("getTerminalVersionByTerminalType");
			pst = dbConn.prepareStatement(strSQL);
			
			pst.setString(1, terminalType);
			
			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				pst.setInt(2, 1);
			}
			else
				if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
					if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
					{
						pst.setInt(2, 2);
					}
					else
					{
						pst.setInt(2, 0);
					}
				}

			rs = pst.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("version"));
				vecTerminalTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getTerminalVersionsByTerminalType", e);
			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pst, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection in getTerminalVersionsByTerminalType", e);
			}
		}
		
		return vecTerminalTypes;
	}
	
	
	/**
	 * @param sessionData
	 * @return
	 */
	public static ArrayList<String> getTerminalVersionsByTerminalTypeNewTerminal(SessionData sessionData) {
		ArrayList<String> arrayTerminalTypes = new ArrayList<String>();
		Connection dbConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TerminalException();
			}
			
			String strSQL = sql_bundle.getString("getVersionByTerminalType");
			pst = dbConn.prepareStatement(strSQL);
			rs = pst.executeQuery();
			
			while (rs.next())
			{
				String version = rs.getString("version");
				String terminalType = rs.getString("terminal_type");
				arrayTerminalTypes.add(version+"#"+terminalType);
			}
			
			rs.close();
			pst.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getTerminalVersionsByTerminalTypeNewTerminal", e);
			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pst, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection in getTerminalVersionsByTerminalTypeNewTerminal", e);
			}
		}
		
		return arrayTerminalTypes;
	}
	
}
