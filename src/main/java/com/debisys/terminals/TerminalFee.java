package com.debisys.terminals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.Log;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;

public class TerminalFee implements HttpSessionBindingListener
{
    static Category cat = Category.getInstance(TerminalFee.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
    private String merchantId;
    private String siteId;
    private String from ="0.00";
    private String to ="0.00";
    private String type ="0";
    private String ammount ="0.00";
    
      public void valueBound(HttpSessionBindingEvent event)
  {
  }

  public void valueUnbound(HttpSessionBindingEvent event)
  {
  }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }


    public String getAmmount() {
        return ammount;
    }

    public void setAmmount(String ammount) {
        this.ammount = ammount;
    }
    public Vector getRanges(SessionData sessionData)
   {

    Vector vecRanges = new Vector();

      Connection dbConn = null;
      try
      {
        dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
        if (dbConn == null)
        {
          cat.error("Can't get database connection");
          throw new Exception();
        }
        PreparedStatement pstmt =
            dbConn.prepareStatement(sql_bundle.getString("getRanges"));
        pstmt.setDouble(1, Double.parseDouble(siteId));
        ResultSet rs = pstmt.executeQuery();
        while (rs.next())
        {
          Vector vecTemp = new Vector();
          vecTemp.add(StringUtil.toString(rs.getString("Terminal_Fee_id")));
          vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
          vecTemp.add(StringUtil.toString(rs.getString("Description")));
          vecTemp.add(StringUtil.toString(rs.getString("from")));
          vecTemp.add(StringUtil.toString(rs.getString("to")));
          vecTemp.add(StringUtil.toString(rs.getString("fee")));
          vecRanges.add(vecTemp);
        }

        rs.close();
        pstmt.close();
      }
      catch (Exception e)
      {
        cat.error("Error during getRanges", e);
      }
      finally
      {
        try
        {
          Torque.closeConnection(dbConn);
        }
        catch (Exception e)
        {
          cat.error("Error during release connection", e);
        }
      }


    return vecRanges;
  }
    
    public boolean Validate(SessionData sessionData){

    validationErrors.clear();


    if (merchantId == null || merchantId.equals(""))
    {
      addFieldError("merchantId", Languages.getString("com.debisys.terminals.invalid_merchant_id", sessionData.getLanguage()));
      return false;
    }
    else
    {
      Merchant merchant = new Merchant();
      merchant.setMerchantId(merchantId);
      if (!merchant.checkPermission(sessionData))
      {
        //this means merchant does
        // not belong to logged in user
        addFieldError("merchantId", Languages.getString("com.debisys.terminals.error_merchant_id", sessionData.getLanguage()));
        return false;

      }

    }

    if (from == null || !NumberUtil.isNumeric(from))
    {
      addFieldError("from", Languages.getString("com.debisys.terminal_fee.error_from", sessionData.getLanguage()));
      return false;
    }
    if (to == null || !NumberUtil.isNumeric(to))
    {
      addFieldError("to", Languages.getString("com.debisys.terminal_fee.error_to", sessionData.getLanguage()));
      return false;
    }
    
    if (ammount == null || !NumberUtil.isNumeric(ammount))
    {
      addFieldError("ammount", Languages.getString("com.debisys.terminal_fee.error_ammount", sessionData.getLanguage()));
      return false;
    }

    double lfrom=Double.parseDouble(from);
    double lto=Double.parseDouble(to);
    if (lfrom>lto){
       addFieldError("from", Languages.getString("com.debisys.terminal_fee.error_order", sessionData.getLanguage()));
      return false;
        
    
    }
    

    return true;
    }
    
    public void addRange(SessionData sessionData)
    {
         
      Connection dbConn = null;
      try
      {
        dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
        if (dbConn == null)
        {
          cat.error("Can't get database connection");
          throw new Exception();
        }
        PreparedStatement pstmt =
            dbConn.prepareStatement(sql_bundle.getString("addRange"));
        pstmt.setInt(1, Integer.parseInt(siteId));
        pstmt.setDouble(2, Double.parseDouble(from));
        pstmt.setDouble(3, Double.parseDouble(to));
        pstmt.setDouble(4, Double.parseDouble(ammount));
        
        pstmt.executeUpdate();

        pstmt.close();
      }
      catch (Exception e)
      {
        cat.error("Error during addRange", e);
      }
      finally
      {
        try
        {
          Torque.closeConnection(dbConn);
        }
        catch (Exception e)
        {
          cat.error("Error during release connection", e);
        }
      }

        
        
        
    
    }
      public void deleteRange(SessionData sessionData, String strRangeCode)
  {
    Connection dbConn = null;

      if (StringUtil.isAlphaNumeric(strRangeCode))
      {
        try
        {
          dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
          if (dbConn == null)
          {
            cat.error("Can't get database connection");
            throw new Exception();
          }
          PreparedStatement pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteRange"));
          pstmt.setInt(1, Integer.parseInt(strRangeCode));
          pstmt.executeUpdate();
          pstmt.close();
          
          Log.write(sessionData, "Site ID: " + siteId + ". Fee Range  \"" + strRangeCode + "\" deleted.", DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
        }
        catch (Exception e)
        {
          cat.error("Error during deleteClerkCode", e);
        }
        finally
        {
          try
          {
            Torque.closeConnection(dbConn);
          }
          catch (Exception e)
          {
            cat.error("Error during release connection", e);
          }
        }
      }

      

  }
      public Hashtable getErrors()
  {
    return validationErrors;
  }
       private Hashtable validationErrors = new Hashtable();
      public void addFieldError(String fieldname, String error)
  {
    if (!validationErrors.containsKey(fieldname))
    {
      validationErrors.put(fieldname, error);
    }

  }
}

