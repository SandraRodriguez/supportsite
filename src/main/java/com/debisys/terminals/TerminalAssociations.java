/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.terminals;

import com.debisys.exceptions.ReportException;
import com.debisys.users.SessionData;
import com.debisys.utils.QueryReportDataAssoc;
import com.debisys.utils.QueryReportDataAssoc.ActionType;
import com.debisys.utils.QueryReportDataAssoc.ReportType;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class TerminalAssociations {

    static Logger cat = Logger.getLogger(TerminalAssociations.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
    
    private TerminalAssociations() {
    }

    public static Integer addTerminalAssociation(SessionData sessionData, HttpServletRequest request, String currentTerminalId, String relatedTerminaId) throws ReportException {
        Integer rows = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        String sql = null;
        
        try {
            Timestamp creationDate = new Timestamp(Calendar.getInstance().getTimeInMillis());
            String idMaster = getMasterId(currentTerminalId);
            String idMasterRelated = getMasterId(relatedTerminaId);
            String userNameOwner = ((((String) sessionData.getProperty("username")) != null) &&  (!((String) sessionData.getProperty("username")).isEmpty()) ? ((String) sessionData.getProperty("username")) : "");
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));

            if (idMaster == null && idMasterRelated == null) {
                sql = "INSERT INTO [RelatedTerminalsMaster]  ([id]) VALUES (?)";
                String id = UUID.randomUUID().toString().toUpperCase();
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setString(1, id);
                rows = pstmt.executeUpdate();
                if (rows == 1) {
                    pstmt.close();
                    sql = "INSERT INTO [RelatedTerminals] ([masterId],[millennium_no], [creation_date], [millennium_owner], [user_name_owner])  VALUES (?,?,?,?,?)";
                    pstmt = dbConn.prepareStatement(sql);
                    pstmt.setString(1, id);
                    pstmt.setInt(2, Integer.parseInt(currentTerminalId));
                    pstmt.setTimestamp(3, creationDate);
                    pstmt.setInt(4, Integer.parseInt(currentTerminalId));
                    pstmt.setString(5, userNameOwner);
                    rows = pstmt.executeUpdate();

                    pstmt.close();
                    sql = "INSERT INTO [RelatedTerminals] ([masterId],[millennium_no], [creation_date], [millennium_owner], [user_name_owner])  VALUES (?,?,?,?,?)";
                    pstmt = dbConn.prepareStatement(sql);
                    pstmt.setString(1, id);
                    pstmt.setInt(2, Integer.parseInt(relatedTerminaId));
                    pstmt.setTimestamp(3, creationDate);
                    pstmt.setInt(4, Integer.parseInt(currentTerminalId));
                    pstmt.setString(5, userNameOwner);
                    rows = pstmt.executeUpdate();

                }
            } else if (idMaster != null && idMasterRelated == null) {
                sql = "INSERT INTO [dbo].[RelatedTerminals] ([masterId],[millennium_no], [creation_date], [millennium_owner], [user_name_owner])  VALUES (?,?,?,?,?)";
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setString(1, idMaster);
                pstmt.setInt(2, Integer.parseInt(relatedTerminaId));
                pstmt.setTimestamp(3, creationDate);
                pstmt.setInt(4, Integer.parseInt(currentTerminalId));
                pstmt.setString(5, userNameOwner);
                rows = pstmt.executeUpdate();
            } else if (idMaster == null && idMasterRelated != null) {
                sql = "INSERT INTO [dbo].[RelatedTerminals] ([masterId],[millennium_no], [creation_date], [millennium_owner], [user_name_owner])  VALUES (?,?,?,?,?)";
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setString(1, idMasterRelated);
                pstmt.setInt(2, Integer.parseInt(currentTerminalId));
                pstmt.setTimestamp(3, creationDate);
                pstmt.setInt(4, Integer.parseInt(currentTerminalId));
                pstmt.setString(5, userNameOwner);
                rows = pstmt.executeUpdate();
            }
        } catch (Exception e) {
            cat.error("Error during addTerminalAssociation ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, null);
        }
        return rows;
    }
    
    private static String getMasterId(String currentTerminalId) throws ReportException {
        String id = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = "SELECT masterId AS masterId FROM [RelatedTerminals] WITH(NOLOCK) WHERE millennium_no=?";
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, Integer.parseInt(currentTerminalId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                id = rs.getString("masterId");
            }
        } catch (Exception e) {
            cat.error("Error during getMasterId ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return id;
    }

    public static Integer deleteTerminalAssociationById(String id, String masterId) throws ReportException {
        Integer rows = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            Integer countTerminalsByMasterId = countTerminalsByMasterId(masterId);
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = "DELETE FROM [RelatedTerminals] WHERE millennium_no=?";
            if (countTerminalsByMasterId > 2) {
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setInt(1, Integer.parseInt(id));
                rows = pstmt.executeUpdate();
            } else{
                sql = "DELETE FROM [RelatedTerminals] WHERE masterId=?";
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setString(1, masterId);
                rows = pstmt.executeUpdate();
            }
        } catch (Exception e) {
            cat.error("Error during deleteTerminalAssociationById ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, null);
        }
        return rows;
    }

    private static Integer countTerminalsByMasterId(String masterId) throws ReportException {
        Integer countTerminalsByMasterId = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = " SELECT COUNT(*), a.id FROM RelatedTerminalsMaster a INNER JOIN RelatedTerminals b on a.id= b.masterId \n" +
                "  WHERE b.masterId=? GROUP BY a.id";
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, masterId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                countTerminalsByMasterId = rs.getInt(1);
            }
        } catch (Exception e) {
            cat.error("Error during countTerminalsByMasterId ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return countTerminalsByMasterId;
    }

    public static List<TerminalAssociationsPojo> getTerminalAssociationsByTerminalId(QueryReportDataAssoc reporInfo) throws ReportException {
        List<TerminalAssociationsPojo> retValue = new ArrayList();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList groupControl = new ArrayList();
        Integer newIdControl =  new Integer(1);
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo);
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String terminalId = rs.getString("terminalId");
                String terminalName = rs.getString("terminalName");
                String merchantId = rs.getString("merchantId");
                String merchantName = rs.getString("merchantName");
                String repName = rs.getString("repName");
                String repId = rs.getString("repId");

                TerminalAssociationsPojo association = new TerminalAssociationsPojo(id, terminalId, terminalName, merchantId, merchantName, repName, repId);
                if (rs.getString("masterIdToAssociate") != null) {
                    if (reporInfo.getReportType().equals(ReportType.GROUPED)){
                        String uniqueId = rs.getString("masterIdToAssociate");                        
                        if (!groupControl.contains(uniqueId)){                            
                            association.setMasterIdToAssociate(newIdControl.toString());                            
                            groupControl.add(uniqueId);
                            newIdControl++;
                        } else{
                            association.setMasterIdToAssociate(newIdControl.toString());
                        }
                    } else{
                        association.setMasterIdToAssociate(rs.getString("masterIdToAssociate"));
                    }
                }

                retValue.add(association);
            }
        } catch (Exception e) {
            cat.error("Error during getTerminalAssociationsByTerminalId ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static Integer getTerminalAssociationsByTerminalIdCount(QueryReportDataAssoc reporInfo) throws ReportException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Integer retValue = 0;
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            String sql = getQuery(reporInfo);
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = rs.getInt(1);
            }
        } catch (Exception e) {
            cat.error("Error during getTerminalAssociationsByTerminalIdCount ", e);
            throw new ReportException();
        } finally {
            closeStatements(dbConn, pstmt, rs);
        }
        return retValue;
    }

    private static String getQuery(QueryReportDataAssoc reporInfo) {
        StringBuilder select = new StringBuilder();
        StringBuilder body = new StringBuilder();
        StringBuilder sb0 = new StringBuilder();

        if (reporInfo.getReportType().equals(ReportType.CURRENT_TERMINALS)) {

            Integer siteId = Integer.parseInt(reporInfo.getDataFilter().get("siteId").toString());
            select.append(" SELECT rt.id, t.millennium_no AS terminalId, t.terminal_label AS terminalName, "
                    + " m.merchant_id AS merchantId, m.dba AS merchantName, rt.masterId AS masterIdToAssociate,  \n");
            select.append("a.businessname AS repName, a.rep_id AS repId \n");

            body.append(" FROM terminals              t  WITH(NOLOCK) \n");
            body.append(" INNER JOIN merchants        m  WITH(NOLOCK) ON t.merchant_id   = m.merchant_id  \n");
            body.append(" INNER JOIN RelatedTerminals rt WITH(NOLOCK) ON rt.millennium_no = t.millennium_no  \n");
            body.append("    AND rt.masterId = (SELECT masterId FROM RelatedTerminals WITH(NOLOCK) WHERE millennium_no=").append(siteId).append(" ) \n");
            body.append(" INNER JOIN reps  a      WITH(NOLOCK) ON m.rep_id = a.rep_id AND a.type=1 --REP \n");

            if (reporInfo.getReportFeature().equals(ActionType.LIST)) {
                sb0 = buildGeneralSelect(reporInfo, select, body);
            } else if (reporInfo.getReportFeature().equals(ActionType.COUNT)) {
                sb0 = new StringBuilder(" SELECT COUNT(*) ");
                sb0.append(body);
            }

        } else if (reporInfo.getReportType().equals(ReportType.SEARCH_INFO)) {

            StringBuilder filterInfo = new StringBuilder();
            String filter = reporInfo.getDataFilter().get("filter").toString();
            if (filter.length() > 0) {
                if (NumberUtils.isNumber(filter)) {
                    filterInfo.append(" AND (t.millennium_no LIKE '%").append(filter).append("%' ");
                    filterInfo.append(" OR m.merchant_id LIKE '%").append(filter).append("%'");
                    filterInfo.append(" OR a.rep_id LIKE '%").append(filter).append("%')");
                } else {
                    filterInfo.append(" AND (m.dba LIKE '%").append(filter).append("%' ");
                    filterInfo.append(" OR a.businessname LIKE '%").append(filter).append("%'");
                    filterInfo.append(" OR t.terminal_label LIKE '%").append(filter).append("%')");
                }
            }
            String isoId = reporInfo.getDataFilter().get("isoId").toString();
            String terminalTypeId = reporInfo.getDataFilter().get("terminalTypeId").toString();
            select.append(" SELECT t.millennium_no as id, t.millennium_no AS terminalId, t.terminal_label AS terminalName, \n");
            select.append(" m.merchant_id AS merchantId, m.dba AS merchantName, rt.masterId AS masterIdToAssociate, \n");
            select.append(" a.businessname as repName, a.rep_id AS repId  \n");

            body.append("  FROM terminals t        WITH(NOLOCK) \n");
            body.append("INNER JOIN merchants m  WITH(NOLOCK) ON t.merchant_id = m.merchant_id \n");
            body.append("INNER JOIN reps  a      WITH(NOLOCK) ON m.rep_id = a.rep_id AND a.type=1 --REP \n");
            body.append("INNER JOIN reps  b      WITH(NOLOCK) ON a.iso_id = b.rep_id AND b.type=5 --SUBAGENT \n");
            body.append("INNER JOIN reps  c      WITH(NOLOCK) ON b.iso_id = c.rep_id AND c.type=4 --AGENT \n");
            body.append("LEFT OUTER JOIN  RelatedTerminals rt WITH(NOLOCK) ON rt.millennium_no = t.millennium_no \n");
            body.append("WHERE t.terminal_Type=").append(terminalTypeId).append(" AND \n");
            body.append("c.iso_id = ").append(isoId).append(" AND rt.millennium_no IS NULL \n");
            body.append(filterInfo);

            if (reporInfo.getReportFeature().equals(ActionType.LIST)) {
                sb0 = buildGeneralSelect(reporInfo, select, body);
            } else if (reporInfo.getReportFeature().equals(ActionType.COUNT)) {
                sb0 = new StringBuilder(" SELECT COUNT(*) ");
                sb0.append(body);
            }
        } else if (reporInfo.getReportType().equals(ReportType.GROUPED)) {
            StringBuilder filterInfo = new StringBuilder();
            String filter = reporInfo.getDataFilter().get("filter").toString();
            if (filter.length() > 0) {
                if (NumberUtils.isNumber(filter)) {
                    filterInfo.append(" AND (t.millennium_no LIKE '%").append(filter).append("%' ");
                    filterInfo.append(" OR m.merchant_id LIKE '%").append(filter).append("%'");
                    filterInfo.append(" OR a.rep_id LIKE '%").append(filter).append("%')");
                } else {
                    filterInfo.append(" AND (m.dba LIKE '%").append(filter).append("%' ");
                    filterInfo.append(" OR a.businessname LIKE '%").append(filter).append("%')");
                }
            }
            String isoId = reporInfo.getDataFilter().get("isoId").toString();
            String terminalTypeId = reporInfo.getDataFilter().get("terminalTypeId").toString();
            select.append(" SELECT t.millennium_no as id, t.millennium_no AS terminalId, t.terminal_label AS terminalName, \n");
            select.append(" m.merchant_id AS merchantId, m.dba AS merchantName, rt.masterId AS masterIdToAssociate, \n");
            select.append(" a.businessname as repName, a.rep_id AS repId  \n");

            body.append("  FROM terminals t        WITH(NOLOCK) \n");
            body.append("INNER JOIN merchants m  WITH(NOLOCK) ON t.merchant_id = m.merchant_id \n");
            body.append("INNER JOIN reps  a      WITH(NOLOCK) ON m.rep_id = a.rep_id AND a.type=1 --REP \n");
            body.append("INNER JOIN reps  b      WITH(NOLOCK) ON a.iso_id = b.rep_id AND b.type=5 --SUBAGENT \n");
            body.append("INNER JOIN reps  c      WITH(NOLOCK) ON b.iso_id = c.rep_id AND c.type=4 --AGENT \n");
            body.append("INNER JOIN  RelatedTerminals rt WITH(NOLOCK) ON rt.millennium_no = t.millennium_no \n");
            body.append("WHERE t.terminal_Type=").append(terminalTypeId).append(" AND \n");
            body.append("c.iso_id = ").append(isoId).append("  \n");
            //body.append(filterInfo);

            if (reporInfo.getReportFeature().equals(ActionType.LIST)) {
                reporInfo.setSortFieldName("masterIdToAssociate");
                sb0 = buildGeneralSelect(reporInfo, select, body);
            } else if (reporInfo.getReportFeature().equals(ActionType.COUNT)) {
                sb0 = new StringBuilder(" SELECT COUNT(*) ");
                sb0.append(body);
            }
        }
        return sb0.toString();
    }

    private static StringBuilder buildGeneralSelect(QueryReportDataAssoc reporInfo, StringBuilder select, StringBuilder body) {
        StringBuilder sb0 = new StringBuilder();
        sb0.append("SELECT * FROM (");
        sb0.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(reporInfo.getSortFieldName()).append(" ").append(reporInfo.getSortOrder()).append(") AS RWN, * FROM (");
        sb0.append(select).append(body).append(" \n");
        sb0.append(") AS tbl1 ");
        sb0.append(") AS tbl2 ");
        sb0.append(" WHERE RWN BETWEEN ").append(reporInfo.getStartPage()).append(" AND ").append(reporInfo.getEndPage());
        return sb0;
    }

    private static void closeStatements(Connection dbConn, PreparedStatement pstmt, ResultSet rs) {
        try {
            Torque.closeConnection(dbConn);
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            cat.error("Error during closeStatements ", e);
        }
    }

}
