/**
 * 
 */
package com.debisys.terminals;

/**
 * @author nmartinez
 *
 */
public class JavaHandSet {

	private String javaHandSetBranding;
	private String javaHandSetUserName;
	private String javaHandSetPassword;
	private String javaHandSetRegistrationCode;
	private String siteId;
	private int id;
	private boolean reCreateRegistrationCod;
	
	/**
	 * @param userName
	 * @param password
	 * @param branding
	 * @param registrationCod
	 * @param siteId
	 */
	public JavaHandSet(String userName, String password, String branding,boolean reCreateRegistrationCod, int Id,String siteId)
	{
		this.javaHandSetBranding = branding;
		this.javaHandSetUserName = userName;
		this.javaHandSetPassword = password ;
		this.reCreateRegistrationCod = reCreateRegistrationCod;		
		this.id=Id;
		this.siteId=siteId;
	}
	
	/**
	 * Default constructor
	 */
	public JavaHandSet()
	{
		javaHandSetBranding="";
		javaHandSetUserName="";
		javaHandSetPassword="";
		javaHandSetRegistrationCode="";
		id=-1;
	}
	
	/**
	 * @return the javaHandSetBranding
	 */
	public String getJavaHandSetBranding() {
		return javaHandSetBranding;
	}

	/**
	 * @param javaHandSetBranding the javaHandSetBranding to set
	 */
	public void setJavaHandSetBranding(String javaHandSetBranding) {
		this.javaHandSetBranding = javaHandSetBranding;
	}

	/**
	 * @return the javaHandSetUserName
	 */
	public String getJavaHandSetUserName() {
		return javaHandSetUserName;
	}
	/**
	 * @param javaHandSetUserName the javaHandSetUserName to set
	 */
	public void setJavaHandSetUserName(String javaHandSetUserName) {
		this.javaHandSetUserName = javaHandSetUserName;
	}
	/**
	 * @return the javaHandSetPassword
	 */
	public String getJavaHandSetPassword() {
		return javaHandSetPassword;
	}
	/**
	 * @param javaHandSetPassword the javaHandSetPassword to set
	 */
	public void setJavaHandSetPassword(String javaHandSetPassword) {
		this.javaHandSetPassword = javaHandSetPassword;
	}


	/**
	 * @param javaHandSetRegistrationCode the javaHandSetRegistrationCode to set
	 */
	public void setJavaHandSetRegistrationCode(
			String javaHandSetRegistrationCode) {
		this.javaHandSetRegistrationCode = javaHandSetRegistrationCode;
	}


	/**
	 * @return the javaHandSetRegistrationCode
	 */
	public String getJavaHandSetRegistrationCode() {
		return javaHandSetRegistrationCode;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * @param reCreateRegistrationCod the reCreateRegistrationCod to set
	 */
	public void setReCreateRegistrationCod(boolean reCreateRegistrationCod) {
		this.reCreateRegistrationCod = reCreateRegistrationCod;
	}

	/**
	 * @return the reCreateRegistrationCod
	 */
	public boolean isReCreateRegistrationCod() {
		return reCreateRegistrationCod;
	}
	
}
