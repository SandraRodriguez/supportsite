package com.debisys.terminals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.TerminalException;
import com.debisys.languages.Languages;
import com.debisys.properties.Properties;
import com.debisys.reports.PropertiesByFeature;
import com.debisys.terminalTracking.CPhysicalTerminal;
import com.debisys.terminalTracking.CTerminalBrand;
import com.debisys.terminalTracking.CTerminalModel;
import com.debisys.terminalTracking.CTerminalStatus;
import com.debisys.users.SessionData;
import com.debisys.utils.Base;
import com.debisys.utils.CSystemMethods;
import com.debisys.utils.CharsetValidator;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.JSONObject;
import com.debisys.utils.Log;
import com.debisys.utils.LogChanges;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.ValidateEmail;
import com.emida.utils.dbUtils.TorqueHelper;
import net.emida.supportsite.util.RSUtil;
import net.emida.supportsite.util.security.SecurityCipherService;

/**
 * @author
 */
public class Terminal implements HttpSessionBindingListener {

    /**
     *
     */
    public static final String MICROBROWSER_ID = "55";
    // static Category cat = Category.getInstance(Terminal.class);
    static Logger cat = Logger.getLogger(Terminal.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
    private static final Integer DEFAULT_LENGTH_PASSWORD = 16;
    /**
     *
     */
    private static final String GET_TERMINAL_REGISTRATION_BY_ID = "SELECT TOP 12 * FROM [dbo].[Terminal_Registration_RegCode_Log] WHERE id = ? ORDER BY RegCodeDate DESC";
    public static Vector<Vector<String>> vecMarketplaceTerminalType = new Vector<Vector<String>>();
    private String repRatePlanId = "";
    private String repId = "";
    private String merchantId = "";
    private String siteId = "";
    private String terminalId = "";
    private String terminalType = "";
    private String isoRatePlanId = "";
    private String saveRates = "";
    private String clerkName = "";
    private String clerkLastName = "";
    private String clerkCode = "";
    private String pcTermBrand = "";
    private String pcTermUsername = "";
    private String pcTermPassword = "";
    private boolean resetAnswers = false;
    private boolean chkRegCode = false;
    private boolean chkManualRegCode = false;
    private String txtRegistration = "";
    private boolean chkIPRestriction = false;
    private String txtIPAddress = "";
    private String txtSubnetMask = "";
    private boolean chkIsAdmin = false;
    private boolean chkCertificate = false;
    private boolean chkAllowCertificate = false;
    private String txtAllowCertificate = "";
    private String txtLoginErrorCount = "";
    private String ddlRecordStatus = "";
    private String chkRatePlan = "";
    private String termVersion = "";
    private String termNote = "";
    private boolean bCachedReceipts = false;
    private String cacheRecetiptType = "";
    private String monthlyFeeThreshold = "0.00";
    private String monthlyFee = "0.00";
    private String[] productId;
    private String[] newproductId;
    private String rateSelector = "";
    private final Hashtable<String, String> validationErrors = new Hashtable<String, String>();
    private String annualMaintenanceFee = "0.00";
    private String annualMaintenanceTax = "0.00";
    private String annualInsuranceFee = "0.00";
    private String annualInsuranceTax = "0.00";
    private String reconnectionFee = "0.00";
    private String reconnectionTax = "0.00";
    private Boolean isEncrypted = false;

    /* BEGIN MiniRelease 9.0 - Added by YH */
    private String serialNumber = "";
    private String simData = "";
    private String imciData = "";

    /* END MiniRelease 9.0 - Added by YH */

 /* BEGIN MiniRelease 4.0 - Added by LF */
    private String softwareVersion = "0";

    /* END MiniRelease 4.0 - Added by LF */

 /* BEGIN MiniRelease 2.0 - Added by LF */
    private String annualAffiliationFee = "0.00";
    private String annualAffiliationTax = "0.00";

    /* END MiniRelease 2.0 - Added by LF */

 /* BEGIN JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE */
    private CPhysicalTerminal terminalFisico = null;

    /* END JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE */

 /* DJC MARKETPLACE AUTOMATION */
    private Integer MarketPlaceKey = new Integer(0);
    private Integer MarketPlaceGroup = new Integer(0);
    private boolean sMarketPlaceTerminal = false;
    private boolean enablePINCache = false;

    /* R27-DBSY-602 - Added by JA */
    private String serialNumberFormat = "";

    private long Status = 0;
    private String StatusLastChange = "";

    private String RatePlan;

    /*
	 * Reference to the current set of Configuration Parameters
     */
    private String MBConfigurationID = null;

    /*
	 * Reference to the previous set of Configuration Parameters
     */
    private String LastMBConfigurationID = null;

    private String terminalDescription = "";

    private String carrierId = "";

    private String terminalLabel = "";

    private boolean chkConfirmationByUser = false; //this is for the user send option (send to merchant)
    private boolean chkConfirmationByEmail = false; //this is for the user send option (send by email)
    private boolean chkConfirmationBySMS = false; ////this is for the user send option (send by sms)
    private String txtConfirmationEMail = "";
    private String txtConfirmationPhoneNumber = "";

    /**
     * @return the terminalDescription
     */
    public String getTerminalDescription() {
        return this.terminalDescription;
    }

    /**
     * @param terminalDescription the terminalDescription to set
     */
    public void setTerminalDescription(String terminalDescription) {
        this.terminalDescription = terminalDescription;
    }

    /* BEGIN MiniRelease 9.0 - Added by YH */
    /**
     * @return serialNumber
     */
    public String getSerialNumber() {
        return this.serialNumber;
    }

    /**
     * @param serialNumber
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /* BEGIN MiniRelease 9.0 - Added by YH */
    public CPhysicalTerminal getTerminalFisico() {
        return this.terminalFisico;
    }

    //DBSY-814
    private Integer providerId = new Integer(110);
    private String providerTerminalID = "";

    public void setProviderTerminalID(String providerTerminalID) {
        this.providerTerminalID = providerTerminalID;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public String getSFID() throws TerminalException {

        Connection dbConn = null;
        String SFID = "";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }
            String strSQL = Terminal.sql_bundle.getString("getTerminalMapping");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, this.providerId.intValue());
            pstmt.setInt(2, Integer.parseInt(this.siteId));

            Terminal.cat.debug(strSQL + " = " + this.providerId + "," + this.siteId);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                SFID = rs.getString("providerTerminalId");
            }

        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalRates", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return SFID;
    }

    /*
	 * public void setTerminalFisico(CPhysicalTerminal terminal) {
	 * this.terminalFisico = terminal; <<<<<<< Terminal.java //this.serialNumber
	 * = (this.terminalFisico != null)?this.terminalFisico.getSerialNumber():"";
	 * ======= if(this.terminalFisico != null) { this.serialNumber =
	 * this.terminalFisico.getSerialNumber(); } >>>>>>> 1.42 }
     */
    public void setTerminalFisico(CPhysicalTerminal terminal) {
        this.terminalFisico = terminal;

        if (this.terminalFisico != null) {
            this.serialNumber = this.terminalFisico.getSerialNumber();
        }
    }

    /**
     * @param simData the simData to set
     */
    public void setSimData(String simData) {
        this.simData = simData;
    }

    /**
     * @return the simData
     */
    public String getSimData() {
        return this.simData;
    }

    /* END MiniRelease 9.0 - Added by YH */
    /**
     * @param imciData the imciData to set
     */
    public void setImciData(String imciData) {
        this.imciData = imciData;
    }

    /**
     * @return the imciData
     */
    public String getImciData() {
        return this.imciData;
    }

    /* BEGIN MiniRelease 4.0 - Added by LF */
    public String getSoftwareVersion() {
        return this.softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    /* END MiniRelease 4.0 - Added by LF */
 /* BEGIN MiniRelease 2.0 - Added by LF */
    public String getAnnualAffiliationFee() {
        return this.annualAffiliationFee;
    }

    public void setAnnualAffiliationFee(String annualAffiliationFee) {
        this.annualAffiliationFee = annualAffiliationFee;
    }

    public String getAnnualAffiliationTax() {
        return this.annualAffiliationTax;
    }

    public void setAnnualAffiliationTax(String annualAffiliationTax) {
        this.annualAffiliationTax = annualAffiliationTax;
    }

    /* END MiniRelease 2.0 - Added by LF */
    public String getMonthlyFeeThreshold() {
        return this.monthlyFeeThreshold;
    }

    public void setMonthlyFeeThreshold(String monthlyFeeThreshold) {
        this.monthlyFeeThreshold = monthlyFeeThreshold;
    }

    public String getMonthlyFee() {
        return this.monthlyFee;
    }

    public void setMonthlyFee(String monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public String getPcTermUsername() {
        return this.pcTermUsername;
    }

    public void setPcTermUsername(String pcTermUsername) {
        this.pcTermUsername = pcTermUsername;
    }

    public String getPcTermPassword() {
        return this.pcTermPassword;
    }

    public String getPcTermPasswordDecrypt(ServletContext context) {
        try {
            String seeder = (String) context.getAttribute("secretSeed");
            net.emida.supportsite.util.security.SecurityCipherService requestBase = new SecurityCipherService(seeder);
            return requestBase.decrypt(this.pcTermPassword);
        } catch (Exception e) {
            return this.pcTermPassword;
        }
    }

    public void setPcTermPassword(String pcTermPassword) {
        this.pcTermPassword = pcTermPassword;
    }

    /**
     * @param chkRegCode the chkRegCode to set
     */
    public void setChkRegCode(boolean chkRegCode) {
        this.chkRegCode = chkRegCode;
    }

    /**
     * @return the chkRegCode
     */
    public boolean isChkRegCode() {
        return this.chkRegCode;
    }

    /**
     * @param chkManualRegCode the chkManualRegCode to set
     */
    public void setChkManualRegCode(boolean chkManualRegCode) {
        this.chkManualRegCode = chkManualRegCode;
    }

    /**
     * @return the chkManualRegCode
     */
    public boolean isChkManualRegCode() {
        return this.chkManualRegCode;
    }

    /**
     * @param txtRegistration the txtRegistration to set
     */
    public void setTxtRegistration(String txtRegistration) {
        this.txtRegistration = txtRegistration;
    }

    /**
     * @return the txtRegistration
     */
    public String getTxtRegistration() {
        return this.txtRegistration;
    }

    /**
     * @param chkIPRestriction the chkIPRestriction to set
     */
    public void setChkIPRestriction(boolean chkIPRestriction) {
        this.chkIPRestriction = chkIPRestriction;
    }

    /**
     * @return the chkIPRestriction
     */
    public boolean isChkIPRestriction() {
        return this.chkIPRestriction;
    }

    /**
     * @param txtIPAddress the txtIPAddress to set
     */
    public void setTxtIPAddress(String txtIPAddress) {
        this.txtIPAddress = txtIPAddress;
    }

    /**
     * @return the txtIPAddress
     */
    public String getTxtIPAddress() {
        return this.txtIPAddress;
    }

    /**
     * @param txtSubnetMask the txtSubnetMask to set
     */
    public void setTxtSubnetMask(String txtSubnetMask) {
        this.txtSubnetMask = txtSubnetMask;
    }

    /**
     * @return the txtSubnetMask
     */
    public String getTxtSubnetMask() {
        return this.txtSubnetMask;
    }

    public boolean isChkIsAdmin() {
        return chkIsAdmin;
    }

    public void setChkIsAdmin(boolean chkIsAdmin) {
        this.chkIsAdmin = chkIsAdmin;
    }

    public void setIsEncrypted(Boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public Boolean getIsEncrypted() {
        return isEncrypted;
    }

    /**
     * @param chkCertificate the chkCertificate to set
     */
    public void setChkCertificate(boolean chkCertificate) {
        this.chkCertificate = chkCertificate;
    }

    /**
     * @return the chkCertificate
     */
    public boolean isChkCertificate() {
        return this.chkCertificate;
    }

    /**
     * @param chkAllowCertificate the chkAllowCertificate to set
     */
    public void setChkAllowCertificate(boolean chkAllowCertificate) {
        this.chkAllowCertificate = chkAllowCertificate;
    }

    /**
     * @return the chkAllowCertificate
     */
    public boolean isChkAllowCertificate() {
        return this.chkAllowCertificate;
    }

    /**
     * @param txtAllowCertificate the txtAllowCertificate to set
     */
    public void setTxtAllowCertificate(String txtAllowCertificate) {
        this.txtAllowCertificate = txtAllowCertificate;
    }

    /**
     * @return the txtAllowCertificate
     */
    public String getTxtAllowCertificate() {
        return this.txtAllowCertificate;
    }

    /**
     * @param txtLoginErrorCount the txtLoginErrorCount to set
     */
    public void setTxtLoginErrorCount(String txtLoginErrorCount) {
        this.txtLoginErrorCount = txtLoginErrorCount;
    }

    /**
     * @return the txtLoginErrorCount
     */
    public String getTxtLoginErrorCount() {
        return this.txtLoginErrorCount;
    }

    /**
     * @param ddlRecordStatus the ddlRecordStatus to set
     */
    public void setDdlRecordStatus(String ddlRecordStatus) {
        this.ddlRecordStatus = ddlRecordStatus;
    }

    /**
     * @return the ddlRecordStatus
     */
    public String getDdlRecordStatus() {
        return this.ddlRecordStatus;
    }

    /**
     * @param chkRatePlan the chkRatePlan to set
     */
    public void setChkRatePlan(String chkRatePlan) {
        this.chkRatePlan = chkRatePlan;
    }

    /**
     * @return the chkRatePlan
     */
    public String getChkRatePlan() {
        return this.chkRatePlan;
    }

    /**
     * @param pcTermBrand the PC Terminal Brand to set
     */
    public void setPcTermBrand(String pcTermBrand) {
        this.pcTermBrand = pcTermBrand;
    }

    /**
     * @return PC Terminal Brand
     */
    public String getPcTermBrand() {
        return this.pcTermBrand;
    }

    /**
     * @param clerkName the clerkName to set
     */
    public void setClerkName(String clerkName) {
        this.clerkName = clerkName;
    }

    /**
     * @return the clerkName
     */
    public String getClerkName() {
        return this.clerkName;
    }

    /**
     * @param clerkLastName the clerkLastName to set
     */
    public void setClerkLastName(String clerkLastName) {
        this.clerkLastName = clerkLastName;
    }

    /**
     * @return the clerkLastName
     */
    public String getClerkLastName() {
        return this.clerkLastName;
    }

    /**
     * @param clerkCode the clerkCode to set
     */
    public void setClerkCode(String clerkCode) {
        this.clerkCode = clerkCode;
    }

    /**
     * @return the clerkCode
     */
    public String getClerkCode() {
        return this.clerkCode;
    }

    public String getTermVersion() {
        return this.termVersion;
    }

    public void setTermVersion(String termVersion) {
        this.termVersion = termVersion;
    }

    public String getTermNote() {
        return this.termNote;
    }

    public void setTermNote(String termNote) {
        this.termNote = termNote;
    }

    public String getSaveRates() {
        return this.saveRates;
    }

    public void setSaveRates(String saveRates) {
        this.saveRates = saveRates;
    }

    public String getTerminalType() {
        return this.terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getIsoRatePlanId() {
        return this.isoRatePlanId;
    }

    public void setIsoRatePlanId(String isoRatePlanId) {
        this.isoRatePlanId = isoRatePlanId;
    }

    public String getRepRatePlanId() {
        return this.repRatePlanId;
    }

    public void setRepRatePlanId(String repRatePlanId) {
        this.repRatePlanId = repRatePlanId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantId() {
        return this.merchantId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getRepId() {
        return this.repId;
    }

    public String[] getProductId() {
        return this.productId;
    }

    public void setProductId(String[] productId) {
        this.productId = productId;
    }

    public Hashtable<String, String> getErrors() {
        return this.validationErrors;
    }

    public String getTerminalId() {
        return this.terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getSiteId() {
        return this.siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getAnnualMaintenanceFee() {
        return this.annualMaintenanceFee;
    }

    public void setAnnualMaintenanceFee(String annualMaintenanceFee) {
        this.annualMaintenanceFee = annualMaintenanceFee;
    }

    public String getAnnualMaintenanceTax() {
        return this.annualMaintenanceTax;
    }

    public void setAnnualMaintenanceTax(String annualMaintenanceTax) {
        this.annualMaintenanceTax = annualMaintenanceTax;
    }

    public String getAnnualInsuranceFee() {
        return this.annualInsuranceFee;
    }

    public void setAnnualInsuranceFee(String annualInsuranceFee) {
        this.annualInsuranceFee = annualInsuranceFee;
    }

    public String getAnnualInsuranceTax() {
        return this.annualInsuranceTax;
    }

    public void setAnnualInsuranceTax(String annualInsuranceTax) {
        this.annualInsuranceTax = annualInsuranceTax;
    }

    public String getReconnectionFee() {
        return this.reconnectionFee;
    }

    public void setReconnectionFee(String reconectionFee) {
        this.reconnectionFee = reconectionFee;
    }

    public String getReconnectionTax() {
        return this.reconnectionTax;
    }

    public void setReconnectionTax(String reconectionTax) {
        this.reconnectionTax = reconectionTax;
    }

    public boolean getEnbCachedRcp() {
        return this.bCachedReceipts;
    }

    public void setEnbCachedRcp(boolean cachedRcp) {
        this.bCachedReceipts = cachedRcp;
    }

    public void setLastMBConfigurationID(String lastMBConfigurationID) {
        this.LastMBConfigurationID = lastMBConfigurationID;
    }

    public String getLastMBConfigurationID() {
        return this.LastMBConfigurationID;
    }

    /**
     * Sets the validation error against a specific field. Used by
     * {@link #validateTerminal validateRatePlan}.
     *
     * @param fieldname The bean property name of the field
     * @param error The error message for the field or null if none is present.
     */
    public void addFieldError(String fieldname, String error) {
        if (!this.validationErrors.containsKey(fieldname)) {
            this.validationErrors.put(fieldname, error);
        }
    }

    public boolean validateTerminal(SessionData sessionData, Hashtable hashRatePlan) {
        this.validationErrors.clear();

        String strAccessLevel = sessionData.getProperty("access_level");

        if ((this.productId == null) || this.productId.equals("")) {
            this.addFieldError("productId", Languages.getString("com.debisys.terminals.error_product_id", sessionData.getLanguage()));

            return false;
        }

        if (strAccessLevel.equals(DebisysConstants.ISO)) {
            if ((this.repId == null) || this.repId.equals("")) {
                this.addFieldError("repId", Languages.getString("com.debisys.terminals.error_rep_id", sessionData.getLanguage()));

                return false;
            }
        }

        if ((this.repRatePlanId == null) || this.repRatePlanId.equals("")) {
            this.addFieldError("repRatePlanId", Languages.getString("com.debisys.terminals.error_rep_rateplan_id", sessionData.getLanguage()));

            return false;
        }

        if ((this.terminalType == null) || this.terminalType.equals("")) {
            this.addFieldError("terminalType", Languages.getString("com.debisys.terminals.error_terminal_type", sessionData.getLanguage()));

            return false;
        }

        if ((this.monthlyFee == null) || !NumberUtil.isNumeric(this.monthlyFee)) {
            this.addFieldError("monthlyFee", Languages.getString("com.debisys.terminals.error_monthly_fee", sessionData.getLanguage()));

            return false;
        }

        boolean valid = true;

        String strProductId = "";
        Hashtable hashBuyRates = this.getRepBuyRates(sessionData);

        for (int i = 0; i < this.productId.length; i++) {
            strProductId = this.productId[i];

            if (hashBuyRates.containsKey(strProductId)) {
                double buyRate = 0;
                double repRate = 0;

                Vector vecTemp = (Vector) hashRatePlan.get(strProductId);
                buyRate = Double.parseDouble((String) hashBuyRates.get(strProductId));

                if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                    repRate = Double.parseDouble(vecTemp.get(0).toString());

                    if (repRate < 0) {
                        this.addFieldError("rep_" + strProductId, "");
                        valid = false;
                    }
                } else {
                    this.addFieldError("rep_" + strProductId, "");
                    valid = false;
                }

                if (repRate > buyRate) {
                    this.addFieldError("remaining_" + strProductId, "");
                    valid = false;
                }
            } else {
                this.addFieldError("general", Languages.getString("com.debisys.terminals.error2", sessionData.getLanguage()) + " " + strProductId);
                valid = false;
            }
        }

        return valid;
    }

    public boolean validateRates(SessionData sessionData, Hashtable<String, Vector<String>> hashRatePlan, ServletContext context, HttpServletRequest request) throws TerminalException {
        this.validationErrors.clear();

        // check to make sure this user has
        // permission to access this resource.
        Merchant merchant = new Merchant();
        merchant.setMerchantId(this.merchantId);

        if ((this.merchantId == null) || this.merchantId.equals("")) {
            this.addFieldError("merchantId", Languages.getString("com.debisys.terminals.error_merchant_id", sessionData.getLanguage()));

            return false;
        } else if (!merchant.checkPermission(sessionData)) {
            // this means merchant does
            // not belong to logged in user
            this.addFieldError("merchantId", Languages.getString("com.debisys.terminals.error_merchant_id", sessionData.getLanguage()));

            return false;
        } else {
            try {
                merchant.getMerchant(sessionData, context);
                if (merchant.isUseRatePlanModel()) {
                    request.setAttribute("merchantUseRatePlanModel", new Boolean(true));
                } else {
                    request.setAttribute("merchantUseRatePlanModel", new Boolean(false));
                }
            } catch (CustomerException e) {
                e.printStackTrace();
            }
        }

        if ((this.monthlyFee == null) || !NumberUtil.isNumeric(this.monthlyFee)) {
            this.addFieldError("monthlyFee", Languages.getString("com.debisys.terminals.error_monthly_fee", sessionData.getLanguage()));

            return false;
        }

        if (((this.productId == null) || this.productId.equals("")) && !this.rateSelector.equals("1")) {
            this.addFieldError("productId", Languages.getString("com.debisys.terminals.error_product_id", sessionData.getLanguage()));

            return false;
        }

        if ((this.siteId == null) || this.siteId.equals("")) {
            this.addFieldError("siteId", Languages.getString("com.debisys.terminals.error_site_id", sessionData.getLanguage()));

            return false;
        }

        if (this.serialNumber.length() > 0) {
            // Added by JA. Check for Serial uniqueness for all the reps in the
            // chain and apply the
            // option of deactivate serial number wherever it exists in the
            // chain
            Vector<Vector<Object>> terminals = Terminal.getTerminalsBySerialNumbers(sessionData, this.serialNumber, this.siteId, false, 0, 1);

            if (terminals.size() > 0 && ((Integer) terminals.get(0).get(0)).intValue() > 0) {
                StringBuffer terminalList = new StringBuffer();
                for (int i = 1; i < terminals.size(); i++) {
                    Vector<Object> terminal = terminals.get(i);
                    terminalList.append((String) terminal.get(8)); // milleniun_no
                    if (i < terminals.size() - 1) {
                        terminalList.append(",");
                    }
                }
                this.addFieldError("serialNumber", Languages.getString("com.debisys.terminals.error_terminal_type.serialnumber_deactivation", new Object[]{this.serialNumber, "deactivateSerialNumber('" + this.serialNumber + "','" + this.siteId + "');",
                    terminalList.toString()}, sessionData.getLanguage()));
                return false;
            }
        }

        // 2009-04-29 .FB - Serial uniqueness now should be validated no matter
        // the deployment
        boolean isSerialNumberDuplicated = !this.IsSerialNumberUnique(this.serialNumber, this.siteId);
        if ((this.serialNumber.length() > 0) && (isSerialNumberDuplicated)) {

            // Added by JA. Send email when other terminals has this serial
            // number
            Vector<Vector<Object>> terminals = Terminal.getTerminalsBySerialNumbers(sessionData, this.serialNumber, this.siteId, true, 0, 1);
            try {
                String serverName = request.getServerName();
                String serverPort = Integer.toString(request.getServerPort());
                String baseHref = "http://" + serverName;
                //String domainName = "";

                if (serverPort != null && !serverPort.equals("") && !serverPort.equals("80") && !serverPort.equals("443")) {
                    baseHref += ":" + serverPort + "/support";
                }
                Vector terminal = terminals.get(1);
                String strSiteID = (String) terminal.get(8);// millenium_no
                String strISO_id = (String) terminal.get(6);
                String strRefId = sessionData.getProperty("ref_id");
                String bodyMessage = Languages.getString("com.debisys.terminals.error_terminal_type.emailBodyMessage", new Object[]{strRefId, strSiteID, strISO_id, baseHref}, sessionData.getLanguage());
                String mailHost = DebisysConfigListener.getMailHost(context);
                String instance = DebisysConfigListener.getInstance(context);
                String mails = "";
                String from = Languages.getString("com.debisys.terminals.error_terminal_type.emailFrom", sessionData.getLanguage());
                String subject = Languages.getString("com.debisys.terminals.error_terminal_type.emailSubjectEdit", sessionData.getLanguage());

                ValidateEmail.sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
            } catch (Exception e) {
                Terminal.cat.error("Error during ValidateEmail.sendMail", e);
            }

            this.addFieldError("serialNumber", Languages.getString("com.debisys.terminals.error_terminal_type.serialnumber", new Object[]{this.serialNumber}, sessionData.getLanguage()));
            return false;
        }

        // JFM 26-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE
        // Se modifica la validacion, pues para terminales Microbrowser el
        // serial no
        // debe validarse
        if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))// if
        // (
        // DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
        // && !this.terminalType.equals(MICROBROWSER_ID))
        {
            if ((sessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)) && (this.serialNumber.length() > 0) && ((isSerialNumberDuplicated) || (com.debisys.terminalTracking.CPhysicalTerminal.serialNumberExists(this.serialNumber)))) {
                this.addFieldError("serialNumber", Languages.getString("com.debisys.terminals.error_terminal_type.serialnumber", new Object[]{this.serialNumber}, sessionData.getLanguage()));

                return false;
            }
        }

        boolean valid = true;
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strProductId = "";

        if (!merchant.isUseRatePlanModel()) {
            if (request.getParameter("rateSelector") != null) {
                // check for the new ratePlan

                Hashtable hashBuyRates;

                if (this.repRatePlanId.length() > 0 && "REP".equals(this.chkRatePlan)) {
                    hashBuyRates = this.getRepBuyRates(sessionData);
                } else {
                    hashBuyRates = this.getIsoBuyRates(sessionData);
                }

                for (int i = 0; i < this.newproductId.length; i++) {
                    strProductId = this.newproductId[i];

                    if (hashBuyRates.containsKey(strProductId)) {
                        double buyRate = 0;
                        double repRate = 0;

                        Vector<String> vecTemp = hashRatePlan.get(strProductId);
                        buyRate = Double.parseDouble((String) hashBuyRates.get(strProductId));

                        if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                            repRate = Double.parseDouble(vecTemp.get(0).toString());

                            if (repRate < 0) {
                                this.addFieldError("newrep_" + strProductId, Languages.getString("jsp.admin.rates.reprateinvalid", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId));
                                valid = false;
                            }
                        } else {
                            this.addFieldError("newrep_" + strProductId, Languages.getString("jsp.admin.rates.reprateinvalid", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId));
                            valid = false;
                        }

                        if (repRate > buyRate) {
                            this.addFieldError("newremaining_" + strProductId, Languages.getString("jsp.admin.rates.remainingrateinvalid", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId) + " (" + repRate + " > " + buyRate + ")");
                            valid = false;
                        }
                    } else {
                        this.addFieldError("general_" + strProductId, Languages.getString("com.debisys.terminals.error4", sessionData.getLanguage()) + " " + strProductId);
                        valid = false;
                    }
                }

            }// End if the user is adding a new terminal rate
            else {
                Vector vecTerminalRates = this.getTerminalRates(strAccessLevel);
                Iterator it = vecTerminalRates.iterator();

                Hashtable<String, String> hashRates = new Hashtable<String, String>();

                while (it.hasNext()) {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();

                    if (!hashRates.containsKey(vecTemp.get(0).toString())) {
                        String strTemp = vecTemp.get(2).toString();

                        double tmpTotal = 0;
                        // double tmpIso = 0;
                        // double tmpAgent = 0;
                        // double tmpSubAgent = 0;

                        if (NumberUtil.isNumeric(strTemp)) {
                            tmpTotal = Double.parseDouble(strTemp);
                        }

                        /*
						 * if (NumberUtil.isNumeric(vecTemp.get(3).toString()))
						 * { tmpIso =
						 * Double.parseDouble(vecTemp.get(3).toString()); } if
						 * (NumberUtil.isNumeric(vecTemp.get(4).toString())) {
						 * tmpAgent =
						 * Double.parseDouble(vecTemp.get(4).toString()); } if
						 * (NumberUtil.isNumeric(vecTemp.get(5).toString())) {
						 * tmpSubAgent = Double.parseDouble(vecTemp.get(5)
						 * .toString()); }
                         */
                        strTemp = Double.toString(tmpTotal); // - (tmpIso +
                        // tmpAgent
                        // + tmpSubAgent));

                        hashRates.put(vecTemp.get(0).toString(), strTemp);
                    }
                }

                // parature tkt 9949440 forcing some rates rounding - loading
                // allowed decimal zeroes
                int allowance = DebisysConfigListener.getRatesDecimalZeroes(context).intValue();
                for (int i = 0; i < this.productId.length; i++) {
                    strProductId = this.productId[i];

                    if (hashRates.containsKey(strProductId)) {
                        double buyRate = 0;
                        double isoRate = 0;
                        double agentRate = 0;
                        double subagentRate = 0;
                        double repRate = 0;

                        Vector<String> vecTemp = hashRatePlan.get(strProductId);
                        buyRate = Double.parseDouble(hashRates.get(strProductId));

                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                            if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                                isoRate = Double.parseDouble(vecTemp.get(0).toString());

                                if (isoRate < 0) {
                                    this.addFieldError("iso_" + strProductId, "");
                                    valid = false;
                                }
                            } else {
                                this.addFieldError("iso_" + strProductId, "");
                                valid = false;
                            }

                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                                    agentRate = Double.parseDouble(vecTemp.get(1).toString());

                                    if (agentRate < 0) {
                                        this.addFieldError("agent_" + strProductId, "");
                                        valid = false;
                                    }
                                } else {
                                    this.addFieldError("agent_" + strProductId, "");
                                    valid = false;
                                }

                                if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                                    subagentRate = Double.parseDouble(vecTemp.get(2).toString());

                                    if (subagentRate < 0) {
                                        this.addFieldError("subagent_" + strProductId, "");
                                        valid = false;
                                    }
                                } else {
                                    this.addFieldError("subagent_" + strProductId, "");
                                    valid = false;
                                }

                                if (NumberUtil.isNumeric(vecTemp.get(3).toString())) {
                                    repRate = Double.parseDouble(vecTemp.get(3).toString());

                                    if (repRate < 0) {
                                        this.addFieldError("rep_" + strProductId, "");
                                        valid = false;
                                    }
                                } else {
                                    this.addFieldError("rep_" + strProductId, "");
                                    valid = false;
                                }

                                // parature tkt 9949440 forcing some rates
                                // rounding
                                // - comparing
                                double allRates = (isoRate + agentRate + subagentRate + repRate);
                                double difference = allRates - buyRate;
                                difference = NumberUtil.roundRate(difference, allowance);
                                if (difference > 0) {
                                    this.addFieldError("remaining_" + strProductId, "");
                                    valid = false;
                                }
                            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                                    repRate = Double.parseDouble(vecTemp.get(1).toString());

                                    if (repRate < 0) {
                                        this.addFieldError("rep_" + strProductId, "");
                                        valid = false;
                                    }
                                } else {
                                    this.addFieldError("rep_" + strProductId, "");
                                    valid = false;
                                }

                                // parature tkt 9949440 forcing some rates
                                // rounding
                                // - comparing
                                double allRates = (isoRate + repRate);
                                double difference = allRates - buyRate;
                                difference = NumberUtil.roundRate(difference, allowance);
                                if (difference > 0) {
                                    this.addFieldError("remaining_" + strProductId, "");
                                    valid = false;
                                }
                            }
                        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                            if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                                agentRate = Double.parseDouble(vecTemp.get(0).toString());

                                if (agentRate < 0) {
                                    this.addFieldError("agent_" + strProductId, "");
                                    valid = false;
                                }
                            } else {
                                this.addFieldError("agent_" + strProductId, "");
                                valid = false;
                            }

                            if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                                subagentRate = Double.parseDouble(vecTemp.get(1).toString());

                                if (subagentRate < 0) {
                                    this.addFieldError("subagent_" + strProductId, "");
                                    valid = false;
                                }
                            } else {
                                this.addFieldError("subagent_" + strProductId, "");
                                valid = false;
                            }

                            if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                                repRate = Double.parseDouble(vecTemp.get(2).toString());

                                if (repRate < 0) {
                                    this.addFieldError("rep_" + strProductId, "");
                                    valid = false;
                                }
                            } else {
                                this.addFieldError("rep_" + strProductId, "");
                                valid = false;
                            }

                            // parature tkt 9949440 forcing some rates rounding
                            // -
                            // comparing
                            double allRates = (agentRate + subagentRate + repRate);
                            double difference = allRates - buyRate;
                            if (difference > 0) {
                                this.addFieldError("remaining_" + strProductId, "");
                                valid = false;
                            }

                        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {

                            if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                                subagentRate = Double.parseDouble(vecTemp.get(0).toString());

                                if (subagentRate < 0) {
                                    this.addFieldError("subagent_" + strProductId, "");
                                    valid = false;
                                }
                            } else {
                                this.addFieldError("subagent_" + strProductId, "");
                                valid = false;
                            }

                            if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                                repRate = Double.parseDouble(vecTemp.get(1).toString());

                                if (repRate < 0) {
                                    this.addFieldError("rep_" + strProductId, "");
                                    valid = false;
                                }
                            } else {
                                this.addFieldError("rep_" + strProductId, "");
                                valid = false;
                            }

                            // parature tkt 9949440 forcing some rates rounding
                            // -
                            // comparing
                            double allRates = (subagentRate + repRate);
                            double difference = allRates - buyRate;
                            if (difference > 0) {
                                this.addFieldError("remaining_" + strProductId, "");
                                valid = false;
                            }

                        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                            if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                                repRate = Double.parseDouble(vecTemp.get(0).toString());

                                if (repRate < 0) {
                                    this.addFieldError("rep_" + strProductId, "");
                                    valid = false;
                                }
                            } else {
                                this.addFieldError("rep_" + strProductId, "");
                                valid = false;
                            }

                            // parature tkt 9949440 forcing some rates rounding
                            // -
                            // comparing
                            double allRates = repRate;
                            double difference = allRates - buyRate;
                            if (difference > 0) {
                                this.addFieldError("remaining_" + strProductId, "");
                                valid = false;
                            }

                        }

                    } else {
                        this.addFieldError("general", Languages.getString("com.debisys.terminals.error3", sessionData.getLanguage()) + " " + strProductId);
                        valid = false;
                    }
                }
            }
        }

        // David Castro Validate that the Serial number doesn't exist on
        // MarketPlace already, only
        // if Terminal is MarketPlace type
        if (this.sMarketPlaceTerminal == true) {
            if (this.serialNumber != null && this.serialNumber != "") {
                Connection dbConn = null;
                try {
                    dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgMarketPlaceDb"));

                    if (dbConn == null) {
                        Terminal.cat.error("Can't get MarketPlace database connection");
                        throw new TerminalException();
                    }
                    PreparedStatement pstmt;
                    pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("findTerminalMarketPlaceSiteId"));
                    Terminal.cat.debug("MarketPlace execute query:" + Terminal.sql_bundle.getString("findTerminalMarketPlaceSiteId"));

                    pstmt.setString(1, this.serialNumber);
                    pstmt.setInt(2, Integer.parseInt(this.siteId + 0));

                    ResultSet rs = pstmt.executeQuery();

                    if (rs.next() == true) {
                        Terminal.cat.error("The terminal exists already in MP, SerialNo:" + this.serialNumber);
                        this.addFieldError("TerminalMP", Languages.getString("jsp.admi.customer.terminal.add_terminal_error_exists_Market_Place", sessionData.getLanguage()).replaceAll("_SITEIDACTIVE_", rs.getString("logic_number")));
                        valid = false;
                    }
                } catch (Exception ex) {
                    this.addFieldError("TerminalMP", Languages.getString("jsp.admi.customer.terminal.add_terminal_error_marketplace", sessionData.getLanguage()));
                    Terminal.cat.error("No fue posible verificar la terminal S/N en el marketPlace: " + ex);
                    throw new TerminalException();
                } finally {
                    try {
                        Torque.closeConnection(dbConn);
                    } catch (Exception e) {
                        Terminal.cat.error("Error during release connection", e);
                    }
                }
            } else {
                this.addFieldError("serialmissing", Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.emptySerial", sessionData.getLanguage()));
                valid = false;
            }
        }

        return valid;
    }

    public String decodePassword(ServletContext context) {
        String password = "";
        try {
            String seeder = (String) context.getAttribute("secretSeed");
            SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
            password = cipherService.decrypt(this.pcTermPassword);
            return password;
        } catch (Exception e) {
            return this.pcTermPassword;
        }       
    }

    public boolean validateAddTerminal(SessionData sessionData, Hashtable<String, Vector<String>> hashRatePlan, ServletContext context, HttpServletRequest request) throws TerminalException {
        this.validationErrors.clear();

        /*
        String seeder = (String) context.getAttribute("secretSeed");
        SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
        this.pcTermPassword = cipherService.encrypt(this.pcTermPassword);
         */
        // String strAccessLevel =
        // sessionData.getProperty("access_level");//Never
        // read
        // check to make sure this user has
        // permission to access this resource.
        boolean bWebServiceAuthenticationEnabled = com.debisys.users.User.isWebServiceAutherticationTerminalsEnabled(sessionData, context);

        Merchant merchant = new Merchant();
        merchant.setMerchantId(this.merchantId);

        if ((this.merchantId == null) || this.merchantId.equals("")) {
            this.addFieldError("merchantId", Languages.getString("com.debisys.terminals.invalid_merchant_id", sessionData.getLanguage()));

            return false;
        } else if (!merchant.checkPermission(sessionData)) {
            // this means merchant does
            // not belong to logged in user
            this.addFieldError("merchantId", Languages.getString("com.debisys.terminals.error_merchant_id", sessionData.getLanguage()));

            return false;
        } else {
            try {
                merchant.getMerchant(sessionData, context);
                request.setAttribute("merchantDataBaseInfo", merchant);
            } catch (CustomerException e) {
                this.addFieldError("merchantId", Languages.getString("jsp.admin.customers.error_find_merchant", sessionData.getLanguage()));
            }
        }

        if ((this.monthlyFee == null) || !NumberUtil.isNumeric(this.monthlyFee)) {
            this.addFieldError("monthlyFee", Languages.getString("com.debisys.terminals.error_monthly_fee", sessionData.getLanguage()));

            return false;
        }

        //David Castro, allow only 1 terminal of type FMR to be created at the time - Type 91
        if (this.hasFMRTerminal() == true) {
            this.addFieldError("hasFMRTerminal", Languages.getString("com.debisys.terminals.error_hasFMRTerminal", sessionData.getLanguage()));
            return false;
        }

        if ((this.monthlyFee == null) || !NumberUtil.isNumeric(this.monthlyFee)) {
            this.addFieldError("monthlyFee", Languages.getString("com.debisys.terminals.error_monthly_fee", sessionData.getLanguage()));

            return false;
        }

        if (!merchant.isUseRatePlanModel()) {
            if ((this.productId == null) || this.productId.equals("")) {
                this.addFieldError("productId", Languages.getString("com.debisys.terminals.error_product_id", sessionData.getLanguage()));

                return false;
            }
        }

        if ((this.terminalType == null) || this.terminalType.equals("")) {
            this.addFieldError("terminalType", Languages.getString("com.debisys.terminals.error_terminal_type", sessionData.getLanguage()));

            return false;
        }

        if (this.serialNumber.length() > 0) {
            // Added by JA. Check for Serial uniqueness for all the reps in the
            // chain and apply the
            // option of deactivate serial number wherever it exist in the chain
            Vector<Vector<Object>> terminals = Terminal.getTerminalsBySerialNumbers(sessionData, this.serialNumber, null, false, 0, 1);

            if (terminals.size() > 0 && ((Integer) (terminals.get(0)).get(0)).intValue() > 0) {
                StringBuffer terminalList = new StringBuffer();
                for (int i = 1; i < terminals.size(); i++) {
                    Vector terminal = terminals.get(i);
                    terminalList.append((String) terminal.get(8));// millenium_no
                    if (i < terminals.size() - 1) {
                        terminalList.append(",");
                    }
                }

                this.addFieldError("serialNumber", Languages
                        .getString("com.debisys.terminals.error_terminal_type.serialnumber_deactivation", new Object[]{this.serialNumber, "deactivateSerialNumber('" + this.serialNumber + "','');", terminalList.toString()}, sessionData.getLanguage()));
                return false;
            }
        }

        // 2009-04-29 .FB - Serial uniqueness now should be validated no matter
        // the deployment
        boolean isSerialNumberDuplicated = !this.IsSerialNumberUnique(this.serialNumber, "");
        if ((this.serialNumber.length() > 0) && (isSerialNumberDuplicated)) {
            // Added by JA. Send email when other terminals has this serial
            // number
            Vector terminals = Terminal.getTerminalsBySerialNumbers(sessionData, this.serialNumber, null, true, 0, 1);
            try {
                String serverName = request.getServerName();
                String serverPort = Integer.toString(request.getServerPort());
                String baseHref = "http://" + serverName;
                //String domainName = "";

                if (serverPort != null && !serverPort.equals("") && !serverPort.equals("80") && !serverPort.equals("443")) {
                    baseHref += ":" + serverPort + "/support";
                }
                Vector terminal = (Vector) terminals.get(1);
                String strSiteID = (String) terminal.get(8);// millenium_no
                String strISO_id = (String) terminal.get(6);
                String strRefId = sessionData.getProperty("ref_id");
                String bodyMessage = Languages.getString("com.debisys.terminals.error_terminal_type.emailBodyMessage", new Object[]{strRefId, strSiteID, strISO_id, baseHref}, sessionData.getLanguage());
                String mailHost = DebisysConfigListener.getMailHost(context);
                String instance = DebisysConfigListener.getInstance(context);
                String mails = "";
                String from = Languages.getString("com.debisys.terminals.error_terminal_type.emailFrom", sessionData.getLanguage());
                String subject = Languages.getString("com.debisys.terminals.error_terminal_type.emailSubjectAdd", sessionData.getLanguage());

                ValidateEmail.sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
            } catch (Exception e) {
                Terminal.cat.error("Error during ValidateEmail.sendMail", e);
            }

            this.addFieldError("serialNumber", Languages.getString("com.debisys.terminals.error_terminal_type.serialnumber", new Object[]{this.serialNumber}, sessionData.getLanguage()));
            return false;
        }

        // BEGIN Release 16.5 New fields to the terminal. Add by YH
        if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            /*
			 * if (this.termVersion == null || this.termVersion.equals("")) {
			 * addFieldError("termVersion",
			 * Languages.getString("com.debisys.terminals.error_terminal_version"
			 * )); return false; }
             */

            //******************************************************************//
            // The routing number for banking information needs to be limited to
            // 9 characters for all levels.
            //R30 DBSY 714 Jorge Nicol�s Mart�nez Romero
            //Monthly Fee Character Limit on the Add/Edit Terminal Page
            //Max 2 digits
            Pattern patt = Pattern.compile("([0-9]{0,3}).([0-9]{0,2})");
            Matcher m = patt.matcher(this.monthlyFee);
            if (!m.matches()) {
                this.addFieldError("monthlyFee", Languages.getString("jsp.admin.terminal.monthlyFee.PatternError", sessionData.getLanguage()));
                return false;
            }
            // END R30 DBSY 714
            // *****************************************************//
        }

        // END Release 16.5 Add by YH
        // JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE - Se cambia la
        // condicion, ya no se valida si es microbrowser para mexico, pues el
        // serial
        // debe conseguirse automatico
        // if
        // ((DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
        // && this.terminalType.equals("52")) ||
        // sessionData.getUser().isQcommBusiness()== true )
        if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
            if ((!sessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)) && (this.serialNumber.length() > 0) && ((isSerialNumberDuplicated) || (com.debisys.terminalTracking.CPhysicalTerminal.serialNumberExists(this.serialNumber)))) {
                this.addFieldError("serialNumber", Languages.getString("com.debisys.terminals.error_terminal_type.serialnumber", new Object[]{this.serialNumber}, sessionData.getLanguage()));

                return false;
            }
        }

        // JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE - Se agrega
        // validacion de que exista terminal fisico en caso de ser microbrowser,
        // en
        // mexico
        /*
		 * if (Terminal.IsTerminalInPhysicalList(context, this.terminalType) &&
		 * sessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)
		 * && this.terminalFisico == null) { addFieldError("terminalType",
		 * Languages
		 * .getString("com.debisys.terminals.error_empty_physicalterminal"));
		 * return false; }
         */
        // 2009-07-22 FB. Validate if phone is already on terminal registration
        if (this.terminalType.equalsIgnoreCase(DebisysConstants.SMS_PHONE_MERCHANT_TERMINAL_TYPE)
                || this.terminalType.equalsIgnoreCase(DebisysConstants.SMS_PHONE_SUBSCRIBER_TERMINAL_TYPE)
                || this.terminalType.equalsIgnoreCase(DebisysConstants.SMS_PHONE_KIOSK_MERCHANT_TERMINAL_TYPE)
                || this.terminalType.equalsIgnoreCase(DebisysConstants.SMS_TABLET_TERMINAL_TYPE)
                || this.terminalType.equalsIgnoreCase(DebisysConstants.SMS_PRINTER_TERMINAL_TYPE)
                || this.terminalType.equalsIgnoreCase(DebisysConstants.SMS_QUICK_SELL_TERMINAL_TYPE)
                || this.terminalType.equalsIgnoreCase(DebisysConstants.SMS_3G_PRINTER_TERMINAL_TYPE)) {
            // Check if the phone number is already a assigned on the
            // Terminal_Registration table			
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(this.pcTermUsername);
            if (!m.matches()) {
                this.addFieldError("userIdUnique", Languages.getString("com.debisys.terminals.error_terminal_type_SMS_Phone_Valid", sessionData.getLanguage()));
                return false;
            }
            String[] results = new String[4];
            if (this.isSMSPhoneAlreadyAssigned(this.pcTermUsername, results)) {
                this.addFieldError("userIdUnique", Languages.getString("com.debisys.terminals.error_terminal_type_SMS_Phone_Used", sessionData.getLanguage()) + " :  Terminal : " + results[1] + " Merchant : " + results[3]);
                return false;
            }
        }
        if ((this.terminalType.equalsIgnoreCase(DebisysConstants.WEB_SERVICE_TERMINAL_TYPE) || this.terminalType.equalsIgnoreCase(DebisysConstants.WEB_SERVICE_MMSPOS_TERMINAL_TYPE) || this.terminalType.equalsIgnoreCase(DebisysConstants.WEB_SERVICE_KIOSK_TERMINAL_TYPE)) && bWebServiceAuthenticationEnabled) {
            if (!this.isUserIDUnique("")) {
                this.addFieldError("userIdUnique", Languages.getString("jsp.admin.customers.terminal.uniqueuserid", sessionData.getLanguage()));
                return false;
            }
        }

        if ((this.terminalType.equalsIgnoreCase(DebisysConstants.WEB_SERVICE_TERMINAL_TYPE)) && (bWebServiceAuthenticationEnabled) && !this.validatePasswordsTerminal(this.pcTermPassword, sessionData, context)) {
            return false; // Validate passwords
        }
        // Validate if the terminal being added is PC Terminal and then if the
        // provided Registration
        // Code is unique
        Vector vTerminalTypes = Terminal.getTerminalTypes_Combo(request.getParameter("terminalClass"));
        Iterator it = vTerminalTypes.iterator();

        while (it.hasNext()) {
            Vector vTmp = (Vector) it.next();

            if (vTmp.get(0).toString().equals(this.terminalType) && vTmp.get(1).toString().equals("PC Terminal")) {
                if (this.chkManualRegCode && !this.isRegistrationCodeUnique("")) {
                    this.addFieldError("registrationCode", Languages.getString("jsp.admin.customers.terminal.invalidregistrationcode", sessionData.getLanguage()));
                    return false;
                }

                if (!this.isUserIDUnique("")) {
                    this.addFieldError("userIdUnique", Languages.getString("jsp.admin.customers.terminal.uniqueuserid", sessionData.getLanguage()));
                    return false;
                }

                if (!(this.isChkRegCode() || this.isChkCertificate() || this.isChkIPRestriction())) {
                    //this.addFieldError("securityRequired", Languages.getString("jsp.admin.customers.terminal.securityrequired", sessionData.getLanguage()));
                    //return false;
                }

                if (!this.validateRegCodeSendOptions()) {
                    this.addFieldError("sendOptionRequired", Languages.getString("jsp.admin.customers.terminal.enforce_userRegCodeSendOptions", sessionData.getLanguage()));
                    return false;
                }

                if (this.isChkRegCode() && this.isChkConfirmationByEmail() && !ValidateEmail.validateEmail(this.txtConfirmationEMail)) {
                    this.addFieldError("confirmationMailSyntaxError", Languages.getString("jsp.admin.customers.terminal.wrongSyntax_inputConfirmationEmail", sessionData.getLanguage()));
                    return false;
                }

                if (this.isChkRegCode() && this.isChkConfirmationBySMS() && txtConfirmationPhoneNumber != null && !txtConfirmationPhoneNumber.matches("\\d{9,14}")) {
                    this.addFieldError("confirmationSmsSyntaxError", Languages.getString("jsp.admin.customers.terminal.wrongSyntax_inputConfirmationSms", sessionData.getLanguage()));
                    return false;
                }
            }
        }

        boolean valid = true;

        String strProductId = "";
        Hashtable hashBuyRates;

        if (this.isoRatePlanId.length() == 0) {
            hashBuyRates = this.getRepBuyRates(sessionData);
        } else {
            hashBuyRates = this.getIsoBuyRates(sessionData);
        }

        //#BEGIN Added by NM - Release 31
        if (!merchant.isUseRatePlanModel()) {
            for (int i = 0; i < this.productId.length; i++) {
                strProductId = this.productId[i];

                if (hashBuyRates.containsKey(strProductId)) {
                    double buyRate = 0;
                    double repRate = 0;

                    Vector<String> vecTemp = hashRatePlan.get(strProductId);
                    buyRate = Double.parseDouble((String) hashBuyRates.get(strProductId));

                    if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                        repRate = Double.parseDouble(vecTemp.get(0).toString());

                        if (repRate < 0) {
                            this.addFieldError("rep_" + strProductId, Languages.getString("jsp.admin.rates.reprateinvalid", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId));
                            valid = false;
                        }
                    } else {
                        this.addFieldError("rep_" + strProductId, Languages.getString("jsp.admin.rates.reprateinvalid", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId));
                        valid = false;
                    }

                    if (repRate > buyRate) {
                        this.addFieldError("remaining_" + strProductId, Languages.getString("jsp.admin.rates.remainingrateinvalid", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId) + " (" + repRate + " > " + buyRate + ")");
                        valid = false;
                    }
                } else {
                    this.addFieldError("general_" + strProductId, Languages.getString("com.debisys.terminals.error4", sessionData.getLanguage()) + " " + strProductId);
                    valid = false;
                }
            }
        }
        //#END Added by NM - Release 31

        // Validate that the Serial number doesn't exist on MarketPlace already,
        // only if Terminal is
        // MarketPlace type
        if (this.sMarketPlaceTerminal == true) {
            Connection dbConn = null;

            try {
                dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgMarketPlaceDb"));

                if (dbConn == null) {
                    Terminal.cat.error("Can't get MarketPlace database connection");
                    throw new TerminalException();
                }
                PreparedStatement pstmt;
                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("findTerminalMarketPlace"));
                Terminal.cat.debug("Marketplace: " + Terminal.sql_bundle.getString("findTerminalMarketPlace"));
                pstmt.setString(1, this.serialNumber);

                ResultSet rs = pstmt.executeQuery();

                if (rs.next() == true) {
                    Terminal.cat.error("The terminal exists already in MP, SerialNo:" + this.serialNumber);
                    this.addFieldError("TerminalMP", Languages.getString("jsp.admi.customer.terminal.add_terminal_error_exists_Market_Place", sessionData.getLanguage()).replaceAll("_SITEIDACTIVE_", rs.getString("logic_number")));
                    valid = false;
                }
            } catch (Exception ex) {
                Terminal.cat.error("No fue posible verificar la terminal S/N en el marketPlace: " + ex);
                this.addFieldError("TerminalMP", Languages.getString("jsp.admi.customer.terminal.add_terminal_error_marketplace", sessionData.getLanguage()));
                throw new TerminalException();
            } finally {
                try {
                    Torque.closeConnection(dbConn);
                } catch (Exception e) {
                    Terminal.cat.error("Error during release connection", e);
                }
            }
        }

        // bugfix PT 10018374 - reusing delegates created as per PT 7753455
        Map<String, String> badChars = null;
        String[] validateOnlyThese = new String[]{"clerkName", "clerkLastName"};
        if ((badChars = CharsetValidator.validateStringFields(this, context, validateOnlyThese)) != null) {
            valid = false;
            for (String fieldName : badChars.keySet()) {
                this.addFieldError(fieldName, badChars.get(fieldName));
            }
        }

        return valid;
    }

    /**
     * Validate passwords for web service user
     *
     */
    private boolean validatePasswordsTerminal(String pass, SessionData sessionData, ServletContext context) {
        boolean result = false;
        if (!(result = ((RSUtil.validateStr(pass)) && this.regexValidatePasswords(pass, context)))) {
            this.addFieldError("userIdUnique", ((Languages.getString("jsp.admin.customers.terminal.invalid.password", sessionData.getLanguage())) + " " + this.getSpecialCharactersToShow(context)));
        }
        Terminal.cat.error("validatePasswordsTerminal -  PASSWORD IS  " + (result ? "VALID" : "INVALID"));
        return result;
    }

    private boolean regexValidatePasswords(String pass, ServletContext context) {
        try {
            String instance = DebisysConfigListener.getInstance(context);
            String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[" + this.getSpecialCharactersAllowed(instance) + "]).{8," + this.getMaximumLengthPass(instance) + "})";
            Terminal.cat.error("regexValidatePasswords -  REGEX TO VALIDATE PASS : " + PASSWORD_PATTERN);
            Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
            Matcher matcher = pattern.matcher(pass);
            return matcher.matches();
        } catch (Exception e) {
            Terminal.cat.error("regexValidatePasswords - ERROR VALIDATING PASSWORD ", e);
            return false;
        }
    }

    private String getSpecialCharactersAllowed(String instance) {
        try {
            String specialCharactersAllowedPasswordWs = Properties.getPropertieByName(instance, "specialCharactersAllowedPasswordWs");
            return ((RSUtil.validateStr(specialCharactersAllowedPasswordWs)) ? (specialCharactersAllowedPasswordWs.replace(",", "")) : "");
        } catch (Exception e) {
            Terminal.cat.error("getSpecialCharactersAllowed - ERROR RETRIEVING SPECIAL CHARACTERS ALLOWED ", e);
            return "";
        }
    }

    private String getSpecialCharactersToShow(ServletContext context) {
        try {
            String instance = DebisysConfigListener.getInstance(context);
            String specialCharactersAllowedPasswordWs = Properties.getPropertieByName(instance, "specialCharactersAllowedPasswordWs");
            return ((RSUtil.validateStr(specialCharactersAllowedPasswordWs)) ? (specialCharactersAllowedPasswordWs.replace("\\\\", "")) : "");
        } catch (Exception e) {
            Terminal.cat.error("getSpecialCharactersAllowed - ERROR RETRIEVING SPECIAL CHARACTERS ALLOWED ", e);
            return "";
        }
    }

    private Integer getMaximumLengthPass(String instance) {
        try {
            String length = Properties.getPropertieByName(instance, "maxLengthPasswordWs");
            return ((RSUtil.validateStr(length)) ? (Integer.parseInt(length)) : DEFAULT_LENGTH_PASSWORD);
        } catch (Exception e) {
            Terminal.cat.error("getMaximumLengthPass - ERROR RETRIEVING LENGTH PASSWORD ", e);
            return DEFAULT_LENGTH_PASSWORD;
        }
    }

    public boolean validateRegCodeSendOptions() {
        boolean confirmationByEmail = (this.isChkRegCode() && this.isChkConfirmationByEmail());
        boolean confirmationBySMS = (this.isChkRegCode() && this.isChkConfirmationBySMS());
        boolean confirmationByMerchant = (this.isChkRegCode() && this.isChkConfirmationByUser());
        return (!this.isChkRegCode()) || (confirmationByEmail || confirmationBySMS || confirmationByMerchant);
    }

    /**
     * @param context a ServletContext containing a reference to parameters to
     * read from DB properties
     * @return A String containing a space separated list of characters
     * configured to be allowed in input forms
     */
    public String getOtherAcceptedChars(ServletContext context) {
        return CharsetValidator.getAcceptableCharsList(context);
    }

    public Hashtable getRepBuyRates(SessionData sessionData) {
        Connection dbConn = null;
        Hashtable hashRates = new Hashtable();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if (this.repRatePlanId != null) {
            try {
                dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

                if (dbConn == null) {
                    Terminal.cat.error("Can't get database connection");
                    throw new TerminalException();
                }

                String strSQL = "";
                String strAccessLevel = sessionData.getProperty("access_level");
                String strRefId = sessionData.getProperty("ref_id");
                Terminal.cat.debug(strAccessLevel);

                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    strSQL = Terminal.sql_bundle.getString("getRepBuyRatesISO");
                } else if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                    strSQL = Terminal.sql_bundle.getString("getRepBuyRatesREP");
                }

                if (NumberUtil.isNumeric(this.repRatePlanId)) {
                    strSQL = strSQL + " and rrp.rep_rateplan_id=" + this.repRatePlanId;
                } else {
                    return null;
                }

                strSQL = strSQL + " order by p.id";
                Terminal.cat.info(strSQL);
                pstmt = dbConn.prepareStatement(strSQL);

                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.REP)) {
                    pstmt.setDouble(1, Double.parseDouble(strRefId));
                    pstmt.setDouble(2, Double.parseDouble(strRefId));
                } else if (strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setDouble(2, Double.parseDouble(this.repId));
                }

                rs = pstmt.executeQuery();

                while (rs.next()) {
                    if (!hashRates.containsKey(rs.getString("id"))) {
                        hashRates.put(rs.getString("id"), rs.getString("rep_buyrate"));
                    }
                }

                pstmt.close();
                rs.close();
            } catch (Exception e) {
                Terminal.cat.error("Error during getRepBuyRates", e);
            } finally {
                try {
                    Torque.closeConnection(dbConn);
                } catch (Exception e) {
                    Terminal.cat.error("Error during closeConnection", e);
                }
            }
        }

        return hashRates;
    }

    public Hashtable getIsoBuyRates(SessionData sessionData) {
        Connection dbConn = null;
        Hashtable hashRates = new Hashtable();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        if (this.isoRatePlanId != null) {
            try {
                dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

                if (dbConn == null) {
                    Terminal.cat.error("Can't get database connection");
                    throw new TerminalException();
                }

                String strSQL = "";
                String strAccessLevel = sessionData.getProperty("access_level");
                String strRefId = sessionData.getProperty("ref_id");
                String isoId = new com.debisys.users.User().getISOId(strAccessLevel, strRefId);
                Terminal.cat.debug(strAccessLevel);
                strSQL = Terminal.sql_bundle.getString("getIsoBuyRatesISO");
                Terminal.cat.debug(strSQL);
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setDouble(1, Double.parseDouble(isoId));
                pstmt.setDouble(2, Double.parseDouble(this.isoRatePlanId));

                rs = pstmt.executeQuery();

                while (rs.next()) {
                    if (!hashRates.containsKey(rs.getString("id"))) {
                        hashRates.put(rs.getString("id"), rs.getString("rep_buyrate"));
                    }
                }

                pstmt.close();
                rs.close();
            } catch (Exception e) {
                Terminal.cat.error("Error during getRepBuyRates", e);
            } finally {
                try {
                    Torque.closeConnection(dbConn);
                } catch (Exception e) {
                    Terminal.cat.error("Error during closeConnection", e);
                }
            }
        }

        return hashRates;
    }

    public static Vector getTerminalTypes(SessionData sessionData) throws TerminalException {
        return Terminal.getTerminalTypes_Combo((sessionData.getUser().isQcommBusiness()) ? "QComm" : "Debisys");
    }

    /**
     * *
     * DBSY-977 SW Designed to get all of the terminal types to help out DB
     * queries
     *
     * @return
     */
    public static Hashtable<String, String> getTerminalTypes() {
        Hashtable<String, String> termTypeTable = new Hashtable<String, String>();

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getAllTerminalTypes");

            Terminal.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                termTypeTable.put(StringUtil.toString(rs.getString("TerminalType")), StringUtil.toString(rs.getString("Description")));
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalTypes", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }
        return termTypeTable;
    }

    /**
     * @param sessionData
     * @param context
     * @return Only returns terminals for doing terminal rebuild request.
     * @throws TerminalException
     */
    public static Vector getTerminalTypeForRequest(SessionData sessionData, ServletContext context) throws TerminalException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector vecTerminalTypes = new Vector();
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = "";
            strSQL = Terminal.sql_bundle.getString("getTerminalTypeForRequest");
            pstmt = dbConn.prepareStatement(strSQL);

            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                pstmt.setInt(1, 1);
            } else if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
                    pstmt.setInt(1, 2);
                } else {
                    pstmt.setInt(1, 0);
                }
            }

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("terminal_type"));
                vecTemp.add(rs.getString("description"));
                vecTerminalTypes.add(vecTemp);
            }

            pstmt.close();
            rs.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getRepBuyRates", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }
        return vecTerminalTypes;
    }

    public static JSONObject getTerminalTypesAjax(String type, SessionData sessionData) throws TerminalException {
        JSONObject json = new JSONObject();

        try {
            Vector jsonVec = new Vector();

            HashMap map0 = new HashMap();
            map0.put("terminal_type", "");
            map0.put("description", Languages.getString("jsp.admin.customers.merchants_add_terminal.select_terminal_type", sessionData.getLanguage()));
            jsonVec.add(map0);

            Iterator it = Terminal.getTerminalTypes_Combo((type.equals("2")) ? "QComm" : "Debisys").iterator();

            while (it.hasNext()) {
                Vector vTmp = (Vector) it.next();
                HashMap map = new HashMap();
                map.put("terminal_type", vTmp.get(0).toString());
                map.put("description", vTmp.get(1).toString());
                jsonVec.add(map);
            }

            json.put("items", jsonVec);
        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalTypes", e);
            throw new TerminalException();
        }

        return json;
    }

    /*
	 * non-javadoc rbuitrago: This avoid the usage of the SP that has hard-coded
	 * terminal types in it, the terminal_types table was modified to provide
	 * business type for every terminal.
     */
    public static Vector getTerminalTypes_Combo(String vendor_selected) throws TerminalException {
        Vector vecTerminalTypes = new Vector();
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getTerminalTypesQuery");
            pst = dbConn.prepareStatement(strSQL);

            pst.setString(1, vendor_selected);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("terminal_type"));
                vecTemp.add(rs.getString("description"));
                vecTerminalTypes.add(vecTemp);
            }

            rs.close();
            pst.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalTypes_Combo", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecTerminalTypes;
    }

    public static Vector getMarketplaceTerminalType() throws TerminalException {
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getMarketplaceTerminalQuery");
            Terminal.cat.debug("Marketplace: " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("terminalTypeID"));
                vecTemp.add(rs.getString("Description"));
                Terminal.vecMarketplaceTerminalType.add(vecTemp);
            }

            rs.close();
            pst.close();
        } catch (Exception ex) {
            Terminal.cat.error("Error during closeConnection" + ex.getMessage());
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return Terminal.vecMarketplaceTerminalType;
    }

    public static Vector getMarketPlaceGroup_Combo(String vendor_selected) throws TerminalException {
        Vector vecMarketplaceGroup = new Vector();
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getMarketplaceGroupQuery");
            Terminal.cat.debug("Marketplace: " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getInt("AplicationGroupId"));
                vecTemp.add(rs.getString("Description"));
                vecMarketplaceGroup.add(vecTemp);
            }

            rs.close();
            pst.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getMarketplaceGroupQuery", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecMarketplaceGroup;
    }

    public static Vector getMarketPlaceKeys_Combo(String vendor_selected) throws TerminalException {
        Vector vecMarketplaceKey = new Vector();
        Connection dbConn = null;
        PreparedStatement pst = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getMarketplaceKeyQuery");
            Terminal.cat.debug("Marketplace: " + strSQL);
            pst = dbConn.prepareStatement(strSQL);

            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getInt("Keysgroupid"));
                vecTemp.add(rs.getString("Description"));
                vecMarketplaceKey.add(vecTemp);
            }

            rs.close();
            pst.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during vecMarketplaceKey", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecMarketplaceKey;
    }

    // special number generator for site id's
    private String generateRandomNumber2() {
        StringBuffer s = new StringBuffer();
        String[] firstChars = {"2", "3", "4", "5", "8", "9"};

        // number between 0-9 but first digit must not be 0,1,6,7
        int nextInt;
        int index = (int) ((Math.random() * firstChars.length));
        nextInt = Integer.parseInt(firstChars[index]);
        s = s.append(nextInt);

        for (int i = 0; i <= 5; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private String generateRandomNumber() {
        StringBuffer s = new StringBuffer();

        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);

        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    public String generateTerminalId() {
        boolean isUnique = false;
        String strTerminalId = "";

        while (!isUnique) {
            strTerminalId = this.generateRandomNumber();
            isUnique = this.checkTerminalId(strTerminalId);
        }

        return strTerminalId;
    }

    private boolean checkTerminalId(String strTerminalId) {
        Connection dbConn = null;
        boolean isUnique = true;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("checkTerminalId"));
            pstmt.setString(1, strTerminalId);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                // duplicate key found
                Terminal.cat.error("Duplicate Terminal Id found:" + strTerminalId);
                isUnique = false;
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during checkTerminalId", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }

        return isUnique;
    }

    public String generateSiteId() {
        boolean isUnique = false;
        String strSiteId = "";

        while (!isUnique) {
            strSiteId = this.generateRandomNumber2();
            isUnique = this.checkSiteId(strSiteId);
        }

        return strSiteId;
    }

    private boolean checkSiteId(String strSiteId) {
        Connection dbConn = null;
        boolean isUnique = true;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("checkSiteId"));
            pstmt.setString(1, strSiteId);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                // duplicate key found
                Terminal.cat.error("Duplicate Site Id found:" + strSiteId);
                isUnique = false;
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during checkSiteId", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }

        return isUnique;
    }

    /**
     * gets originals rates from ISORate_LineItems tables
     *
     * @param sessionData
     * @return
     */
    private Hashtable getRepPercentages(SessionData sessionData) {
        Connection dbConn = null;
        Hashtable hashPercentages = new Hashtable();
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = "";
            String strAccessLevel = sessionData.getProperty("access_level");
            String strRefId = sessionData.getProperty("ref_id");
            String strIsoId = new com.debisys.users.User().getISOId(strAccessLevel, strRefId);

            if (this.isoRatePlanId.length() > 0) {
                strSQL = Terminal.sql_bundle.getString("getIsoRatesISO");
            } else {
                strSQL = Terminal.sql_bundle.getString("getRatesISO");
            }

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(strIsoId));
            if (this.isoRatePlanId.length() > 0) {
                pstmt.setInt(2, Integer.parseInt(this.isoRatePlanId));
                pstmt.setInt(3, Integer.parseInt(strAccessLevel));
            } else {
                pstmt.setInt(2, Integer.parseInt(this.repRatePlanId));
            }

            Terminal.cat.debug(strSQL);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                // iso.rate iso_rate, agent.rate agent_rate ,subagent.rate
                // subagent_rate
                vecTemp.add(StringUtil.toString(rs.getString("total_buyrate")));
                vecTemp.add(StringUtil.toString(rs.getString("iso_rate")));
                vecTemp.add(StringUtil.toString(rs.getString("agent_rate")));
                vecTemp.add(StringUtil.toString(rs.getString("subagent_rate")));
                hashPercentages.put(StringUtil.toString(rs.getString("id")), vecTemp);
            }

            pstmt.close();
            rs.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getRepPercentages", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return hashPercentages;
    }

    /**
     * @param accessLevel
     * @return
     * @throws TerminalException
     */
    public Vector getTerminalRates(String accessLevel) throws TerminalException {
        Vector vecTerminalRates = new Vector();
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgAlternateDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getTerminalRates");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchantId));
            pstmt.setDouble(2, Double.parseDouble(this.siteId));
            pstmt.setInt(3, Integer.parseInt(accessLevel));

            Terminal.cat.debug("[" + strSQL + " = " + this.merchantId + "," + this.siteId + "," + accessLevel + "] ");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("product_id"));
                vecTemp.add(rs.getString("description"));
                vecTemp.add(rs.getString("total_rate"));
                vecTemp.add(rs.getString("iso_rate"));
                vecTemp.add(rs.getString("agent_rate"));
                vecTemp.add(rs.getString("subagent_rate"));
                vecTemp.add(rs.getString("rep_rate"));
                vecTemp.add(rs.getString("merchant_rate"));
                vecTemp.add(StringUtil.toString(rs.getString("tsorder")));
                vecTemp.add(rs.getString("program_id"));
                vecTerminalRates.add(vecTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalRates", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecTerminalRates;
    }

    public Vector getProductFeeTiers(String product_id, String site_id) throws TerminalException {
        Vector vecProductFT = new Vector();
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgAlternateDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getProductFeeTiers");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, Integer.parseInt(product_id));
            pstmt.setInt(2, Integer.parseInt(product_id));
            pstmt.setDouble(3, Double.parseDouble(site_id));

            Terminal.cat.debug(strSQL + "/* product_id = " + product_id + "*/");

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("vendorName"));
                vecTemp.add(NumberUtil.formatCurrency(rs.getString("productPrice")));
                vecTemp.add(NumberUtil.formatCurrency(rs.getString("brokerFee")));
                vecTemp.add(NumberUtil.formatCurrency(rs.getString("subBroker1Fee")));
                vecTemp.add(NumberUtil.formatCurrency(rs.getString("subBroker2Fee")));
                vecTemp.add(NumberUtil.formatCurrency(rs.getString("subBroker3Fee")));
                vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchantFee")));
                //vecTemp.add(NumberUtil.formatCurrency(rs.getString("minDistributorAmount")));
                //vecTemp.add(NumberUtil.formatCurrency(rs.getString("carrierMax")));
                //vecTemp.add(NumberUtil.formatCurrency(rs.getString("refAgentFee")));
                vecProductFT.add(vecTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getProductFeeTiers", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecProductFT;
    }

    public Vector getTerminalInfo() throws TerminalException {
        Vector vecTerminalInfo = new Vector();
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getTerminal");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchantId));
            pstmt.setDouble(2, Double.parseDouble(this.siteId));

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                vecTerminalInfo.add(rs.getString("millennium_no"));
                vecTerminalInfo.add(rs.getString("terminal_type"));
                this.terminalType = rs.getString("terminal_type");
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("amount")));
                this.softwareVersion = StringUtil.toString(rs.getString("gensar_no"));
                // this.softwareVersion = ((this.softwareVersion.length() ==
                // 0)?"0":this.softwareVersion);
                /* BEGIN MiniRelease 9.0 - Added by YH */
                vecTerminalInfo.add(StringUtil.toString(rs.getString("SerialNumber")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("SIM")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("IMCI")));
                /* END MiniRelease 9.0 - Added by YH */
                // BEGIN Release 16.5 New fields to the terminal. Add by YH
                vecTerminalInfo.add(StringUtil.toString(rs.getString("terminal_version")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("terminal_note")));
                vecTerminalInfo.add((StringUtil.toString(rs.getString("Cached_Receipts"))) == "1" ? true : false);

                // END Release 16.5 Add by YH
                vecTerminalInfo.add(rs.getBoolean("EnablePINCache"));
                vecTerminalInfo.add(rs.getLong("Status"));
                vecTerminalInfo.add(rs.getString("StatusLastChange"));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("RatePlanId")));
                this.MBConfigurationID = rs.getString("MBConfigurationID");
                this.setLastMBConfigurationID(rs.getString("LastMBConfigurationID"));

                vecTerminalInfo.add(StringUtil.toString(rs.getString("description")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("carrierId")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("terminal_label")));
                String receiptCacheType = StringUtil.toString(rs.getString("CacheReceiptType"));
                this.setCacheRecetiptType(receiptCacheType);
                vecTerminalInfo.add(receiptCacheType);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalInfo", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecTerminalInfo;
    }

    public Vector getTerminalInfoMx() throws TerminalException {
        Vector vecTerminalInfo = new Vector();
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getTerminalMx");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchantId));
            pstmt.setDouble(2, Double.parseDouble(this.siteId));

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                vecTerminalInfo.add(rs.getString("millennium_no"));
                vecTerminalInfo.add(rs.getString("terminal_type"));
                this.terminalType = rs.getString("terminal_type");
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Annual_Insurance_Fee")));
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Annual_Insurance_Tax")));
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Annual_Maintenance_Fee")));
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Annual_Maintenance_Tax")));
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Reconnection_Fees_Fee")));
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Reconnection_Fees_Tax")));
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Annual_Affiliation_Fee")));
                vecTerminalInfo.add(NumberUtil.formatAmount(rs.getString("Annual_Affiliation_Tax")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("SerialNumber")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("SIM")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("IMCI")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("terminal_version")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("terminal_note")));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("physicalTerminalId")));

                vecTerminalInfo.add(rs.getInt("marketPlaceKey")); // place 16 of
                // veTerminalInfo
                // is
                // marketplace key
                vecTerminalInfo.add(rs.getInt("marketPlaceGroup")); // place 17
                // of
                // veTerminalInfo
                // is

                vecTerminalInfo.add(rs.getBoolean("EnablePINcache"));

                this.softwareVersion = StringUtil.toString(rs.getString("gensar_no"));
                boolean cache = (StringUtil.toString(rs.getString("Cached_Receipts"))).equals("1") ? true : false;
                this.setEnbCachedRcp(cache);
                vecTerminalInfo.add(cache);

                vecTerminalInfo.add(rs.getLong("Status"));
                vecTerminalInfo.add(rs.getString("StatusLastChange"));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("RatePlanId")));
                this.MBConfigurationID = rs.getString("MBConfigurationID");
                this.setLastMBConfigurationID(rs.getString("LastMBConfigurationID"));
                vecTerminalInfo.add(StringUtil.toString(rs.getString("description")));
                String receiptCacheType = StringUtil.toString(rs.getString("CacheReceiptType"));
                this.setCacheRecetiptType(receiptCacheType);
                vecTerminalInfo.add(receiptCacheType);

                String terminal_Label = StringUtil.toString(rs.getString("terminal_Label"));
                this.setTerminalLabel(terminal_Label);
                vecTerminalInfo.add(terminal_Label);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalInfoMx", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecTerminalInfo;
    }

    public boolean validateTopSellersFeature(String terminalID) throws TerminalException {
        boolean ret = false;
        Connection dbConn = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            PreparedStatement pstmt = null;

            if (terminalID != null) {
                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getTopSellersTerminalType"));
                pstmt.setInt(1, Integer.parseInt(terminalID));
            }

            rs = pstmt.executeQuery();

            if (rs.next()) {
                ret = rs.getBoolean("enabled");
            }

            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error in validateTopSellersFeature", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }

        return ret;
    }

    public void updateTSOrder(SessionData sessionData, Hashtable hashTSOrders) throws TerminalException {
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            PreparedStatement pstmt = null;

            if (!hashTSOrders.isEmpty()) {
                dbConn.setAutoCommit(false);

                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalTopSellers"));

                for (int i = 0; i < this.productId.length; i++) {
                    String strProductId = this.productId[i];
                    int tsorder = 0;

                    if (this.productId[i] != null) {
                        pstmt.setInt(3, Integer.parseInt(this.productId[i]));
                    }

                    if (hashTSOrders.get(strProductId) != null) {
                        if (hashTSOrders.get(strProductId).toString() != "") {
                            if (NumberUtil.isNumeric(hashTSOrders.get(strProductId).toString())) {
                                tsorder = Integer.parseInt(hashTSOrders.get(strProductId).toString());
                                pstmt.setInt(1, tsorder);
                            } else {
                                pstmt.setNull(1, java.sql.Types.INTEGER);
                            }
                        } else {
                            pstmt.setNull(1, java.sql.Types.INTEGER);
                        }
                    }

                    if (this.siteId != null) {
                        pstmt.setInt(2, Integer.parseInt(this.siteId));
                    }

                    pstmt.addBatch();
                }

                pstmt.executeBatch();
                pstmt.close();
                dbConn.commit();
                dbConn.setAutoCommit(true);
            }
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback updateTerminalTS", ex);
            }

            Terminal.cat.error("Error during updateTerminalTS", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    }

    public String getMerchantDBA(String strMerchantId) {
        String strMerchantDBA = "";
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection on GetMerchantDBA");
            } else {
                PreparedStatement pstmt;
                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getMerchantDba"));
                pstmt.setDouble(1, Double.parseDouble(this.merchantId));

                ResultSet rs = pstmt.executeQuery();

                if (rs.next() != false) {
                    strMerchantDBA = rs.getString("dba");
                }
            }
        } catch (Exception ex) {
            Terminal.cat.error("Can't find merchant dba in GetMerchantDBA" + ex);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }

        return strMerchantDBA;
    }

    // If a marketplace terminal already exists its updated, else it is created.
    public void addMarketPlaceTerminal(String generatedSiteId, ServletContext context) throws TerminalException {
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgMarketPlaceDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get MarketPlace database connection");
                throw new TerminalException();
            } else {
                // A validation that the terminal doesn't exist already was made
                // in Validate Add
                // terminal, so I'm just going to create it here
                String strDeploymentMP = DebisysConfigListener.getMarketPlaceDeployName(context);

                PreparedStatement pstmt1;
                pstmt1 = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalMarketPlace"));
                Terminal.cat.debug("Marketplace: " + Terminal.sql_bundle.getString("addTerminalMarketPlace"));
                pstmt1.setString(1, this.serialNumber);
                pstmt1.setString(2, generatedSiteId + "0");
                pstmt1.setString(3, strDeploymentMP);
                pstmt1.setInt(4, this.MarketPlaceGroup);
                pstmt1.setInt(5, this.MarketPlaceKey);
                pstmt1.executeUpdate();
                pstmt1.close();
                Terminal.cat.debug("AddMarketPlaceTerminal: " + Terminal.sql_bundle.getString("addTerminalMarketPlace"));
                Terminal.cat.debug("SN:" + this.serialNumber + " SiteId:" + generatedSiteId + "(0)");
            }
        } catch (Exception ex) {
            Terminal.cat.error("No fue posible crear la terminal en el marketPlace: " + ex);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    }

    // If a specified SiteId already exists in Marketplace the all his values
    // are modified, else a
    // new one is created.
    public void updateMarketPlaceTerminal(ServletContext context) throws TerminalException {
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgMarketPlaceDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get MarketPlace database connection");
                throw new TerminalException();
            }

            PreparedStatement pstmt;
            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("findSiteIdMarketPlace"));
            pstmt.setString(1, this.siteId + "0");

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) // Si ya existe el Site Id en Marketplace, not
            // validating multiple equal
            // SiteIds There (Why would?)
            {
                PreparedStatement pstmt1;
                pstmt1 = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalSiteIdMarketPlace"));
                pstmt1.setString(1, this.serialNumber);

                String strDeploymentMP = DebisysConfigListener.getMarketPlaceDeployName(context);
                pstmt1.setString(2, strDeploymentMP);
                pstmt1.setInt(3, this.MarketPlaceGroup);
                pstmt1.setInt(4, this.MarketPlaceKey);
                pstmt1.setString(5, this.siteId + "0");
                pstmt1.executeUpdate();
                pstmt1.close();
            } else {
                String strDeploymentMP = DebisysConfigListener.getMarketPlaceDeployName(context);
                PreparedStatement pstmt1;
                pstmt1 = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalMarketPlace"));
                pstmt1.setString(1, this.serialNumber);
                pstmt1.setString(2, this.siteId + "0");
                pstmt1.setString(3, strDeploymentMP);
                pstmt1.setInt(4, this.MarketPlaceGroup);
                pstmt1.setInt(5, this.MarketPlaceKey);
                pstmt1.executeUpdate();
                pstmt1.close();
            }
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback updateTerminal in MarketPlace", ex);
            }

            Terminal.cat.error("Error during updateTerminal in MarketPlace", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection with MarketPlace", e);
            }
        }
    }

    /**
     * @param sessionData
     * @param context
     * @return true if the terminal has a registration with CertificateIgnoreInd
     * in true
     * @throws TerminalException
     */
    public boolean hasChkCertificate(SessionData sessionData, ServletContext context) throws TerminalException {
        boolean ok = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        this.validationErrors.clear();
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            } else {
                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("hasChkCertificate"));
                pstmt.setLong(1, Long.parseLong(this.siteId));
                rs = pstmt.executeQuery();
                if (rs.next() != false) {
                    int count = rs.getInt("c");
                    if (count > 0) {
                        ok = true;
                    }
                }
                pstmt.close();
            }
        } catch (Exception e) {
            Terminal.cat.error("Error during Terminal.hasChkCertificate", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection in Terminal.hasChkCertificate", e);
            }
        }
        return ok;
    }

    public void updateTerminal(SessionData sessionData, Hashtable hashRepRates, ServletContext context, HttpServletRequest request) throws TerminalException {
        Connection dbConn = null;
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strAccessLevel = sessionData.getProperty("access_level");
        Boolean merchantUseRatePlanModel = (Boolean) request.getAttribute("merchantUseRatePlanModel");
        LogChanges logChanges = new LogChanges(sessionData);

        logChanges.setContext(context);
        String reason = "";
        reason = "Rate plan updated";

        this.validationErrors.clear();

        try {
            //******************************************************************//
            // The routing number for banking information needs to be limited to
            // 9 characters for all levels.
            //R30 DBSY 714 Jorge Nicol�s Mart�nez Romero
            //Monthly Fee Character Limit on the Add/Edit Terminal Page
            //Max 2 digits
            Pattern patt = Pattern.compile("[0-9]{0,3}(\\.[0-9]{0,2})?");
            Matcher m = patt.matcher(this.monthlyFee);
            if (!m.matches()) {
                this.addFieldError("monthlyFee", Languages.getString("jsp.admin.terminal.monthlyFee.PatternError", sessionData.getLanguage()));
                return;
            }
            // END R30 DBSY 714
            // *****************************************************//

            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }
            dbConn.setAutoCommit(false);

            PreparedStatement pstmt;
            String sqlUpdateTerminal = Terminal.sql_bundle.getString("updateTerminal");

            if (this.rateSelector.equals("1") && this.newproductId != null && this.newproductId.length > 0) {
                if (NumberUtil.isNumeric(this.repRatePlanId) && this.chkRatePlan.equals("REP")) {
                    pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getIsoRepRatePlanId"));
                    pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));

                    ResultSet rs = pstmt.executeQuery();

                    if (rs.next()) {
                        this.isoRatePlanId = rs.getString("iso_rateplan_id");
                    }

                    rs.close();
                    pstmt.close();
                }
                sqlUpdateTerminal = sqlUpdateTerminal + ", iso_rateplan_id=?, carrierId=?,terminal_label=?, CacheReceiptType=? where millennium_no = ?";
            } else {
                if (request.getParameter("EmidaRatePlan") != null && !request.getParameter("EmidaRatePlan").equals("-1")) {
                    sqlUpdateTerminal = sqlUpdateTerminal + ", RatePlanId=?, carrierId=?,terminal_label=? , CacheReceiptType=? where millennium_no = ?";
                } else {
                    sqlUpdateTerminal = sqlUpdateTerminal + ", carrierId=?,terminal_label=? , CacheReceiptType=? where millennium_no = ?";
                }
            }

            pstmt = dbConn.prepareStatement(sqlUpdateTerminal);

            pstmt.setString(1, this.softwareVersion);
            /* BEGIN MiniRelease 9.0 - Added by YH */

            //R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP
            String oldvalue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SERIALNO)) + "";
            if (oldvalue == null || oldvalue.equals("")) {
                oldvalue = "this is the first time adding the serial number.";
            } else {
                oldvalue = "old Serial Number is " + oldvalue;
            }
            //String oldvalue =  logChanges.getSession().getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SERIALNO));
            String reasonToChangeSerialNo = "Terminal Updated: Site ID " + this.siteId + " has new Serial Number: " + this.serialNumber + ";" + oldvalue;
            if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SERIALNO), String.valueOf(this.serialNumber), this.siteId, reasonToChangeSerialNo)) {
            }

            Merchant merchant = new Merchant();
            merchant.addMerchantNote(Long.parseLong(this.merchantId), reasonToChangeSerialNo, sessionData.getUser().getUserLogonID(sessionData));
            //END R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP
            pstmt.setString(2, this.serialNumber);

            pstmt.setString(3, this.getSimData());
            pstmt.setString(4, this.getImciData());
            /* END MiniRelease 9.0 - Added by YH */

            // BEGIN Release 16.5 New fields to the terminal. Add by YH
            pstmt.setString(5, this.termVersion);
            pstmt.setString(6, this.termNote);

            if (this.MarketPlaceKey == 0) {
                pstmt.setNull(7, java.sql.Types.INTEGER);
            } else {
                pstmt.setInt(7, this.MarketPlaceKey);
            }

            if (this.MarketPlaceGroup == 0) {
                pstmt.setNull(8, java.sql.Types.INTEGER);
            } else {
                pstmt.setInt(8, this.MarketPlaceGroup);
            }

            pstmt.setInt(9, this.enablePINCache ? 1 : 0);

            if (this.bCachedReceipts) {
                pstmt.setByte(10, (byte) 1);
            } else {
                pstmt.setByte(10, (byte) 0);
            }

            {// Anonymous block in order to dispose the temp variable used here
                Terminal t2 = new Terminal();
                t2.setMerchantId(this.merchantId);
                t2.setSiteId(this.siteId);
                t2.getTerminalInfo();
                if (Terminal.IsTerminalInMBList(context, t2.getTerminalType())) {
                    pstmt.setString(11, request.getParameter("MBConfigurationID"));
                    if (!StringUtil.toString(t2.getLastMBConfigurationID()).equals(request.getParameter("MBConfigurationID"))) {
                        int nOldConfig = this.getMBConfigurationByTerminal(this.siteId);
                        this.deleteMBConfiguration(nOldConfig);
                        this.addMBConfigurationExt(this.siteId, this.siteId, this.merchantId, request.getParameter("MBConfigurationID"), true);
                    }
                } else {
                    this.MBConfigurationID = null;
                    pstmt.setString(11, this.MBConfigurationID);
                }
            }
            String cachereceiptsTypescontrol = request.getParameter("cachereceiptsTypesSelect");
            // iso_rateplan_id
            if (this.rateSelector.equals("1") && this.newproductId != null && this.newproductId.length > 0) {
                pstmt.setInt(12, Integer.parseInt(this.isoRatePlanId));
                if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
                        && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                    if (this.carrierId != null && this.carrierId.length() > 0 && !this.carrierId.equals("-1")) {
                        pstmt.setDouble(13, Integer.parseInt(this.carrierId));
                    } else {
                        pstmt.setNull(13, java.sql.Types.INTEGER);
                    }
                } else {
                    pstmt.setNull(13, java.sql.Types.INTEGER);
                }
                if (sessionData.checkPermission(DebisysConstants.PERM_ADD_EDIT_TERMINAL_LABEL)) {
                    if (this.terminalLabel != null && !this.terminalLabel.equals("")) {
                        pstmt.setString(14, this.terminalLabel);
                    } else {
                        pstmt.setString(14, "");
                    }
                } else {
                    pstmt.setString(14, "");
                }

                if (cachereceiptsTypescontrol != null && cachereceiptsTypescontrol.length() > 0) {
                    pstmt.setInt(15, Integer.parseInt(cachereceiptsTypescontrol));
                } else {
                    pstmt.setNull(15, java.sql.Types.INTEGER);
                }

                pstmt.setDouble(16, Double.parseDouble(this.siteId));
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_RATEPLAN), String.valueOf(this.isoRatePlanId), this.siteId, reason)) {
                }

            } else {
                String EmidaRatePlan = request.getParameter("EmidaRatePlan");
                if (EmidaRatePlan != null && !EmidaRatePlan.equals("-1")) {
                    pstmt.setInt(12, Integer.parseInt(EmidaRatePlan));
                    if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
                            && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                        if (this.carrierId != null && this.carrierId.length() > 0 && !this.carrierId.equals("-1")) {
                            pstmt.setDouble(13, Integer.parseInt(this.carrierId));
                        } else {
                            pstmt.setNull(13, java.sql.Types.INTEGER);
                        }
                    } else {
                        pstmt.setNull(13, java.sql.Types.INTEGER);
                    }
                    if (sessionData.checkPermission(DebisysConstants.PERM_ADD_EDIT_TERMINAL_LABEL)) {
                        if (this.terminalLabel != null && !this.terminalLabel.equals("")) {
                            pstmt.setString(14, this.terminalLabel);
                        } else {
                            pstmt.setString(14, "");
                        }
                    } else {
                        pstmt.setString(14, "");
                    }
                    if (cachereceiptsTypescontrol != null && cachereceiptsTypescontrol.length() > 0) {
                        pstmt.setInt(15, Integer.parseInt(cachereceiptsTypescontrol));
                    } else {
                        pstmt.setNull(15, java.sql.Types.INTEGER);
                    }
                    pstmt.setDouble(16, Double.parseDouble(this.siteId));
                    if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_RATEPLAN), String.valueOf(EmidaRatePlan), this.siteId, reason)) {
                    }
                } else {
                    if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
                            && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                        if (this.carrierId != null && this.carrierId.length() > 0 && !this.carrierId.equals("-1")) {
                            pstmt.setDouble(12, Integer.parseInt(this.carrierId));
                        } else {
                            pstmt.setNull(12, java.sql.Types.INTEGER);
                        }
                    } else {
                        pstmt.setNull(12, java.sql.Types.INTEGER);
                    }
                    if (sessionData.checkPermission(DebisysConstants.PERM_ADD_EDIT_TERMINAL_LABEL)) {
                        if (this.terminalLabel != null && !this.terminalLabel.equals("")) {
                            pstmt.setString(13, this.terminalLabel);
                        } else {
                            pstmt.setString(13, "");
                        }
                    } else {
                        pstmt.setString(13, "");
                    }

                    if (cachereceiptsTypescontrol != null && cachereceiptsTypescontrol.length() > 0) {
                        pstmt.setInt(14, Integer.parseInt(cachereceiptsTypescontrol));
                    } else {
                        pstmt.setNull(14, java.sql.Types.INTEGER);
                    }
                    pstmt.setDouble(15, Double.parseDouble(this.siteId));
                }
            }

            pstmt.executeUpdate();
            pstmt.close();

            // If this is a marketPlace terminal the go to marketPlace and do
            // stuff.
            if (this.sMarketPlaceTerminal == true) {
                // David Castro Updating the siteId on MarketPlace
                this.updateMarketPlaceTerminal(context);
            }

            boolean blnRatesUpdated = true;
            if (!merchantUseRatePlanModel) {
                if (this.rateSelector.equals("1") && this.newproductId != null && this.newproductId.length > 0) {
                    blnRatesUpdated = this.updateNewTerminalRates(hashRepRates, dbConn, strDistChainType, strAccessLevel, sessionData);
                } else {
                    blnRatesUpdated = this.updateTerminalRates(hashRepRates, dbConn, strDistChainType, strAccessLevel, sessionData);
                }

                if (blnRatesUpdated == true) {
                    CSystemMethods.saveRateChangeLog(sessionData, Terminal.sql_bundle.getString("updateTerminalRates"), Terminal.sql_bundle, Terminal.cat);
                }
            }

            // update terminal amount / monthly fee
            Vector vecTerminalInfo = this.getTerminalInfo();
            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalAmount"));
            pstmt.setDouble(1, Double.parseDouble(NumberUtil.formatAmount(this.monthlyFee)));

            // purchase type = customer provided or FSO Rental
            if ((NumberUtil.isNumeric(this.monthlyFeeThreshold) && (Double.parseDouble(this.monthlyFeeThreshold) > 0)) || (NumberUtil.isNumeric(this.monthlyFee) && (Double.parseDouble(this.monthlyFee) > 0))) {
                pstmt.setInt(2, 15);
            } else {
                pstmt.setInt(2, 5);
            }

            pstmt.setDouble(3, Double.parseDouble(this.siteId));
            pstmt.executeUpdate();
            pstmt.close();

            if (vecTerminalInfo.get(2) != null && !vecTerminalInfo.get(2).toString().equals(NumberUtil.formatAmount(this.monthlyFee))) {
                Log.write(sessionData, "Site id:" + this.siteId + " Terminal amount updated from " + vecTerminalInfo.get(2).toString() + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(this.monthlyFee),
                        DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
            }

            try {
                Merchant m2 = new Merchant();
                m2.setMerchantId(this.merchantId);
                m2.getMerchant(sessionData, context);
                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    m2.getMerchantMx(sessionData, context);
                }
                m2.setMonthlyFeeThreshold(this.monthlyFeeThreshold);
                m2.updateMerchant(sessionData, context, request);
            } catch (Exception e) {
            }

            //DBSY-814 jacuna.
            // This will update in teminal_mappings the providerTerminalID (for
            // BOOST only)
            if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && sessionData.checkPermission(DebisysConstants.PERM_ENABLE_EDIT_SALES_FORCE_ID) && this.providerTerminalID != null
                    && this.providerTerminalID.trim().length() > 0) {
                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalMapping"));
                pstmt.setInt(1, this.providerId); 				//providerId = 110
                pstmt.setInt(2, Integer.parseInt(this.siteId)); //debisysTerminalId
                pstmt.setString(3, this.providerTerminalID);	//providerTerminalID
                Terminal.cat.debug(Terminal.sql_bundle.getString("updateTerminalMapping") + " = " + this.providerId + "," + this.siteId + "," + this.providerTerminalID);
                pstmt.executeUpdate();
                pstmt.close();
            }

            dbConn.commit();
            dbConn.setAutoCommit(true);

            //TODO: fix trackChanges(context, sessionData);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback updateTerminal", ex);
            }

            Terminal.cat.error("Error during updateTerminal", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * @param hashRepRates
     * @param dbConn
     * @param strDistChainType
     * @param strAccessLevel
     * @return
     * @throws TerminalException
     * @throws SQLException
     * @throws NumberFormatException
     */
    private boolean updateTerminalRates(Hashtable hashRepRates, Connection dbConn, String strDistChainType, String strAccessLevel, SessionData sessionData) throws TerminalException, SQLException, NumberFormatException {
        PreparedStatement pstmt;
        Hashtable hashRates = new Hashtable();
        Vector vecTerminalRates = this.getTerminalRates(strAccessLevel);
        Iterator it = vecTerminalRates.iterator();
        boolean blnRatesUpdated = false;

        while (it.hasNext()) {
            Vector vecTemp = null;
            vecTemp = (Vector) it.next();

            if (!hashRates.containsKey(vecTemp.get(0).toString())) {
                hashRates.put(vecTemp.get(0).toString(), vecTemp);
            }
        }

        pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalRates"));
        dbConn.setAutoCommit(false);

        for (int i = 0; i < this.productId.length; i++) {
            String strProductId = this.productId[i];
            double totalRate = 0;
            double newIsoRate = 0;
            double newAgentRate = 0;
            double newSubagentRate = 0;
            double newRepRate = 0;
            double isoRate = 0;
            double agentRate = 0;
            double subagentRate = 0;
            double repRate = 0;
            double merchantRate = 0;

            Vector vecTemp = (Vector) hashRepRates.get(strProductId);

            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                        newIsoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                    }

                    if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                        newAgentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
                    }

                    if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                        newSubagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                    }

                    if (NumberUtil.isNumeric(vecTemp.get(3).toString())) {
                        newRepRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(3).toString()));
                    }
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                        newIsoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                    }

                    if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                        newRepRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
                    }
                }

                vecTemp = (Vector) hashRates.get(strProductId);

                if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                    totalRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(3).toString())) {
                    isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(3).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(4).toString())) {
                    agentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(4).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(5).toString())) {
                    subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(5).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(6).toString())) {
                    repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(6).toString()));
                }

                if ((isoRate != newIsoRate) || (agentRate != newAgentRate) || (subagentRate != newSubagentRate) || (repRate != newRepRate)) {
                    merchantRate = totalRate - (newIsoRate + newAgentRate + newSubagentRate + newRepRate);
                    merchantRate = Double.parseDouble(NumberUtil.formatAmount(Double.toString(merchantRate)));

                    pstmt.setDouble(1, newIsoRate);
                    pstmt.setDouble(2, newAgentRate);
                    pstmt.setDouble(3, newSubagentRate);
                    pstmt.setDouble(4, newRepRate);
                    pstmt.setDouble(5, merchantRate);
                    pstmt.setInt(6, Integer.parseInt(this.siteId));
                    pstmt.setInt(7, Integer.parseInt(strProductId));
                    pstmt.addBatch();
                    blnRatesUpdated = true;
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                    newAgentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                    newSubagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                    newRepRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                }

                vecTemp = (Vector) hashRates.get(strProductId);

                if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                    totalRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(4).toString())) {
                    agentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(4).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(5).toString())) {
                    subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(5).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(6).toString())) {
                    repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(6).toString()));
                }

                if (newAgentRate != agentRate || newSubagentRate != subagentRate || newRepRate != repRate) {
                    merchantRate = totalRate - (newAgentRate + newSubagentRate + newRepRate);
                    merchantRate = Double.parseDouble(NumberUtil.formatAmount(Double.toString(merchantRate)));

                    pstmt.setNull(1, java.sql.Types.NULL);
                    pstmt.setDouble(2, newAgentRate);
                    pstmt.setDouble(3, newSubagentRate);
                    pstmt.setDouble(4, newRepRate);
                    pstmt.setDouble(5, merchantRate);
                    pstmt.setInt(6, Integer.parseInt(this.siteId));
                    pstmt.setInt(7, Integer.parseInt(strProductId));
                    pstmt.addBatch();
                    blnRatesUpdated = true;

                }
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {

                if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                    newSubagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                    newRepRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
                }

                vecTemp = (Vector) hashRates.get(strProductId);

                if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                    totalRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(5).toString())) {
                    subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(5).toString()));
                }

                if (NumberUtil.isNumeric(vecTemp.get(6).toString())) {
                    repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(6).toString()));
                }

                if (newSubagentRate != subagentRate || newRepRate != repRate) {
                    merchantRate = totalRate - (newSubagentRate + newRepRate);
                    merchantRate = Double.parseDouble(NumberUtil.formatAmount(Double.toString(merchantRate)));

                    pstmt.setNull(1, java.sql.Types.NULL);
                    pstmt.setNull(2, java.sql.Types.NULL);
                    pstmt.setDouble(3, newSubagentRate);
                    pstmt.setDouble(4, newRepRate);
                    pstmt.setDouble(5, merchantRate);
                    pstmt.setInt(6, Integer.parseInt(this.siteId));
                    pstmt.setInt(7, Integer.parseInt(strProductId));
                    pstmt.addBatch();
                    blnRatesUpdated = true;

                }
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                    newRepRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                }

                vecTemp = (Vector) hashRates.get(strProductId);

                if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                    totalRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                }

                if (repRate != newRepRate) {
                    merchantRate = totalRate - newRepRate;
                    merchantRate = Double.parseDouble(NumberUtil.formatAmount(Double.toString(merchantRate)));

                    pstmt.setNull(1, java.sql.Types.NULL);
                    pstmt.setNull(2, java.sql.Types.NULL);
                    pstmt.setNull(3, java.sql.Types.NULL);
                    pstmt.setDouble(4, newRepRate);
                    pstmt.setDouble(5, merchantRate);
                    pstmt.setInt(6, Integer.parseInt(this.siteId));
                    pstmt.setInt(7, Integer.parseInt(strProductId));
                    pstmt.addBatch();
                    blnRatesUpdated = true;

                }
            }
            if (blnRatesUpdated) {
                // Log.write
                if ((isoRate != newIsoRate) || (agentRate != newAgentRate) || (subagentRate != newSubagentRate) || (repRate != newRepRate)) {

                    StringBuffer messageBuffer = new StringBuffer();
                    if (isoRate != newIsoRate) {
                        messageBuffer.append(" [OldIsoRate: ").append(isoRate);
                        messageBuffer.append("/NewIsoRate: ").append(newIsoRate).append("]");
                    }
                    if (agentRate != newAgentRate) {
                        messageBuffer.append(" [OldAgentRate: ").append(agentRate);
                        messageBuffer.append("/NewAgentRate: ").append(newAgentRate).append("]");
                    }
                    if (subagentRate != newSubagentRate) {
                        messageBuffer.append(" [OldSubAgentRate: ").append(subagentRate);
                        messageBuffer.append("/NewSubAgentRate: ").append(newSubagentRate).append("]");
                    }
                    if (repRate != newRepRate) {
                        messageBuffer.append(" [OldRepRate: ").append(repRate);
                        messageBuffer.append("/NewRepRate: ").append(newRepRate).append("]");
                    }
                    // CSystemMethods.saveRateChangeLog(sessionData,
                    // sql_bundle.getString("updateTerminalRates"), sql_bundle,
                    // cat);
                    Log.write(sessionData, "Terminal rates updated. Site Id:" + this.siteId + ". ProductID:" + strProductId + ". RatePlan:" + this.getISORatePlanNameFromTerminal() + ". Rates: " + messageBuffer.toString(),
                            DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);

                }
            }
        }// for product

        pstmt.executeBatch();
        pstmt.close();
        return blnRatesUpdated;
    }

    public String getTerminalIdBySite(String strSiteId) {
        Connection dbConn = null;
        boolean isUnique = true;
        String strTerminalId = "";
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getTerminalId"));
            pstmt.setString(1, strSiteId);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                strTerminalId = rs.getString("terminal_id");
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during checkSiteId", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
        return strTerminalId;
    }

    /**
     * @param hashRepRates
     * @param dbConn
     * @param strDistChainType
     * @param strAccessLevel
     * @param sessionData
     * @return
     * @throws TerminalException
     * @throws SQLException
     * @throws NumberFormatException
     */
    private boolean updateNewTerminalRates(Hashtable hashRepRates, Connection dbConn, String strDistChainType, String strAccessLevel, SessionData sessionData) throws TerminalException, SQLException, NumberFormatException {

        Vector vecTerminalRates = this.getTerminalRates(strAccessLevel);
        Iterator it = vecTerminalRates.iterator();
        boolean blnRatesUpdated = false;
        boolean isRepRatePlan = true;
        String strTerminalId = this.getTerminalIdBySite(this.siteId);
        String tempIsoRatePlanId = this.isoRatePlanId;
        String tempRepRatePlanId = this.repRatePlanId;
        if ("ISO".equals(this.chkRatePlan)) {
            isRepRatePlan = false;
            this.repRatePlanId = "";
        } else {
            this.isoRatePlanId = "";
        }

        Hashtable hashTemp = this.getRepPercentages(sessionData);
        this.isoRatePlanId = tempIsoRatePlanId;
        this.repRatePlanId = tempRepRatePlanId;

        String strRefId = sessionData.getProperty("ref_id");

        PreparedStatement pstmt2 = null;
        PreparedStatement pstmt = null;
        String isoRatePlanRepId = "";

        if ((this.saveRates != null) && this.saveRates.equalsIgnoreCase("y")) {
            pstmt2 = dbConn.prepareStatement(Terminal.sql_bundle.getString("getIsoRatePlanRepId"));
            pstmt2.setInt(1, Integer.parseInt(this.repRatePlanId));

            if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.REP)) {
                pstmt2.setDouble(2, Double.parseDouble(strRefId));
            } else if (strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                pstmt2.setDouble(2, Double.parseDouble(this.repId));
            }

            ResultSet rs2 = pstmt2.executeQuery();

            if (rs2.next()) {
                isoRatePlanRepId = rs2.getString("iso_rateplan_rep_id");
                pstmt2 = dbConn.prepareStatement(Terminal.sql_bundle.getString("deleteRepDefaultRates"));
                pstmt2.setInt(1, Integer.parseInt(isoRatePlanRepId));
                pstmt2.executeUpdate();
                pstmt2.close();
            }

            //
        }

        // Call sp for deleting terminal_rate_plans
        pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("deleteTerminalRates"));
        pstmt.setInt(1, Integer.parseInt(this.siteId));
        pstmt.executeUpdate();
        pstmt.close();

        // Add now new terminal
        pstmt2 = dbConn.prepareStatement(Terminal.sql_bundle.getString("addRepDefaultRates"));
        pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalRates"));

        dbConn.setAutoCommit(false);

        for (int i = 0; i < this.newproductId.length; i++) {

            String strProductId = this.newproductId[i];

            double totalRate = 0;
            double isoRate = 0;
            double agentRate = 0;
            double subagentRate = 0;
            double repRate = 0;
            double merchantRate = 0;

            Vector vecTemp = (Vector) hashTemp.get(strProductId);

            if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                totalRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
            }

            if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
            }

            if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                agentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
            }

            if (NumberUtil.isNumeric(vecTemp.get(3).toString())) {
                subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(3).toString()));
            }

            vecTemp = (Vector) hashRepRates.get(strProductId);

            Double tempSus = null;
            String tempSusString = "";
            Double tempSusFinal = null;
            /*
			 * It Doesn't matter if the creation process comes from Iso rate
			 * plan or Rep Rate Plan. It just catches the rep split here. the
			 * others splits comes from isorate_lineitems table.
             */
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    if (isRepRatePlan) {
                        repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                    } else {
                        repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(3).toString()));
                    }

                    if (!isRepRatePlan) {
                        tempSus = new Double((isoRate + agentRate + subagentRate + repRate));
                        tempSusString = NumberUtil.formatAmount(tempSus.toString());
                        tempSusFinal = new Double(tempSusString);
                        merchantRate = totalRate - (tempSusFinal);
                    } else {
                        merchantRate = totalRate - repRate;
                    }
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    if (isRepRatePlan) {
                        repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                    } else {
                        repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
                    }

                    if (!isRepRatePlan) {
                        tempSus = new Double((isoRate + repRate));
                        tempSusString = NumberUtil.formatAmount(tempSus.toString());
                        tempSusFinal = new Double(tempSusString);
                        merchantRate = totalRate - (tempSusFinal);
                    } else {
                        merchantRate = totalRate - repRate;
                    }
                }

            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {

                if (isRepRatePlan) {
                    repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                } else {
                    repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                }

                if (!isRepRatePlan) {
                    tempSus = new Double(agentRate + subagentRate + repRate);
                    tempSusString = NumberUtil.formatAmount(tempSus.toString());
                    tempSusFinal = new Double(tempSusString);
                    merchantRate = totalRate - (tempSusFinal);
                } else {
                    merchantRate = totalRate - repRate;
                }

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {

                if (isRepRatePlan) {
                    repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                } else {
                    repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
                }

                if (!isRepRatePlan) {
                    tempSus = new Double(subagentRate + repRate);
                    tempSusString = NumberUtil.formatAmount(tempSus.toString());
                    tempSusFinal = new Double(tempSusString);
                    merchantRate = totalRate - (tempSusFinal);
                } else {
                    merchantRate = totalRate - repRate;
                }

            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));

                merchantRate = totalRate - repRate;

            }

            if ((totalRate >= 0) && (merchantRate >= 0)) {

                merchantRate = Double.parseDouble(NumberUtil.formatAmount(Double.toString(merchantRate)));

                pstmt.setInt(1, Integer.parseInt(this.siteId));
                pstmt.setInt(2, Integer.parseInt(strProductId));
                pstmt.setDouble(3, Double.parseDouble(strTerminalId));

                pstmt.setDouble(4, isoRate);
                pstmt.setDouble(5, agentRate);
                pstmt.setDouble(6, subagentRate);
                pstmt.setDouble(7, repRate);
                pstmt.setDouble(8, merchantRate);
                pstmt.setInt(9, Integer.parseInt(this.isoRatePlanId));
                pstmt.setString(10, sessionData.getUser().getUsername());
                pstmt.setLong(11, Long.parseLong(this.getMerchantId()));
                pstmt.addBatch();
                blnRatesUpdated = true;
                if (!isoRatePlanRepId.equals("")) {
                    pstmt2.setInt(1, Integer.parseInt(isoRatePlanRepId));
                    pstmt2.setInt(2, Integer.parseInt(strProductId));
                    pstmt2.setDouble(3, repRate);
                    pstmt2.addBatch();
                }
            } else {
                // its an error
                this.addFieldError(strProductId, Languages.getString("jsp.admin.terminal.error_product", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId).replaceAll("_PLAN_", this.isoRatePlanId));
            }

            if (this.getErrors().size() > 0) {
                throw new TerminalException();
            }

            if (!isoRatePlanRepId.equals("")) {
                pstmt2.executeBatch();
            }
            pstmt2.close();

            if (blnRatesUpdated) {
                // Log.write
                StringBuffer messageBuffer = new StringBuffer();

                messageBuffer.append("[NewIsoRate: ").append(isoRate).append("]");
                messageBuffer.append("[NewAgentRate: ").append(agentRate).append("]");
                messageBuffer.append("[NewSubAgentRate: ").append(subagentRate).append("]");
                messageBuffer.append("[NewRepRate: ").append(repRate).append("]");

                // CSystemMethods.saveRateChangeLog(sessionData,
                // sql_bundle.getString("updateTerminalRates"), sql_bundle,
                // cat);
                Log.write(sessionData, "Terminal rates updated. Site Id:" + this.siteId + ". ProductID:" + strProductId + ". NewRatePlan:" + this.getISORatePlanNameFromTerminal() + ". Rates: " + messageBuffer.toString(), DebisysConstants.LOGTYPE_CUSTOMER,
                        this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);

            }
        }// for product

        pstmt.executeBatch();
        pstmt.close();

        return blnRatesUpdated;
    }

    public static void WriteDebug(String line) {
        Terminal.cat.debug(line);
    }

    /* R27-DBSY-602 - Added by JA */
    /**
     * This method returns the current Serial Number formats used in terminals
     *
     * @return vector of vectors with the format and the description
     * @throws TerminalException
     */
    public static Vector getSerialNumberFormats() throws TerminalException {
        Connection dbConn = null;
        Vector vecSerialNumberFormats = null;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            // /mock vector
            vecSerialNumberFormats = new Vector();

            String strSQL = Terminal.sql_bundle.getString("getSerialNumberFormat");

            // Vector element = new Vector();
            // element.addElement("");
            // element.addElement("(No Format)");
            // vecSerialNumberFormats.addElement(element);
            //
            //
            // element = new Vector();
            // element.addElement("8320AB12CD34 1001234567");
            // element.addElement("8320AB12CD34 1001234567 (Nurit)");
            // vecSerialNumberFormats.addElement(element);
            //
            // element = new Vector();
            // element.addElement("%1001234567");
            // element.addElement("1001234567 (Nurit for MarketPlace)");
            // vecSerialNumberFormats.addElement(element);
            //
            //
            // element = new Vector();
            // element.addElement("123-456-789");
            // element.addElement("123-456-789 (Verifone)");
            // vecSerialNumberFormats.addElement(element);
            //
            // element = new Vector();
            // element.addElement("1234AB1234567890");
            // element.addElement("1234AB1234567890 (Sagem)");
            // vecSerialNumberFormats.addElement(element);
            //
            // element = new Vector();
            // element.addElement("%1234567890");
            // element.addElement("1234567890 (Sagem for MarketPlace)");
            // vecSerialNumberFormats.addElement(element);
            //
            // element = new Vector();
            // element.addElement("12345678-12345678");
            // element.addElement("12345678-12345678 (Ingenico)");
            // vecSerialNumberFormats.addElement(element);
            // /end mock
            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL);
            Terminal.cat.debug(strSQL);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.addElement(rs.getString("format"));
                vecTemp.addElement(rs.getString("description"));
                vecSerialNumberFormats.addElement(vecTemp);

            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getSerialNumberFormats", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecSerialNumberFormats;
    }

    /* EBD R27-DBSY-602 - Added by JA */
    public void updateTerminalMx(SessionData sessionData, Hashtable hashRepRates, ServletContext context, HttpServletRequest request) throws TerminalException {
        Connection dbConn = null;
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strAccessLevel = sessionData.getProperty("access_level");
        Boolean merchantUseRatePlanModel = (Boolean) request.getAttribute("merchantUseRatePlanModel");
        String sqlUpdateTerminal = "";
        this.validationErrors.clear();

        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);
        String reason = "";
        reason = "Rate plan updated";

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            dbConn.setAutoCommit(false);

            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                if ((this.termVersion == null) || this.termVersion.equals("")) {
                    this.addFieldError("termVersion", Languages.getString("com.debisys.terminals.error_terminal_version", sessionData.getLanguage()));
                    return;
                }
            }

            //iso_rateplan_id\=? where millennium_no \= ?
            //RatePlanId
            PreparedStatement pstmt;
            sqlUpdateTerminal = Terminal.sql_bundle.getString("updateTerminal");

            if (this.rateSelector.equals("1") && this.newproductId != null && this.newproductId.length > 0) {
                if (NumberUtil.isNumeric(this.repRatePlanId) && this.chkRatePlan.equals("REP")) {
                    pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getIsoRepRatePlanId"));
                    pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));

                    ResultSet rs = pstmt.executeQuery();

                    if (rs.next()) {
                        this.isoRatePlanId = rs.getString("iso_rateplan_id");
                    }

                    rs.close();
                    pstmt.close();
                }
                sqlUpdateTerminal = sqlUpdateTerminal + ", iso_rateplan_id=?, CacheReceiptType=?, terminal_Label=? where millennium_no = ?";
            } else {
                if (request.getParameter("EmidaRatePlan") != null && !request.getParameter("EmidaRatePlan").equals("-1")) {
                    sqlUpdateTerminal = sqlUpdateTerminal + ", RatePlanId=?, CacheReceiptType=?, terminal_Label=? where millennium_no = ?";
                } else {
                    sqlUpdateTerminal = sqlUpdateTerminal + ", CacheReceiptType=? , terminal_Label=? where millennium_no = ?";
                }
            }

            pstmt = dbConn.prepareStatement(sqlUpdateTerminal);

            pstmt.setString(1, (((this.softwareVersion.length() == 0) || (this.softwareVersion.equals("0"))) ? null : this.softwareVersion));
            pstmt.setString(2, this.serialNumber);

            String oldvalue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SERIALNO)) + "";
            if (oldvalue == null || oldvalue.equals("")) {
                oldvalue = "this is the first time adding the serial number.";
            } else {
                oldvalue = "old Serial Number is " + oldvalue;
            }
            //String oldvalue =  logChanges.getSession().getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SERIALNO));
            String reasonToChangeSerialNo = "Terminal Updated: Site ID " + this.siteId + " has new Serial Number: " + this.serialNumber + "; " + oldvalue;
            if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SERIALNO), String.valueOf(this.serialNumber), this.siteId, reasonToChangeSerialNo)) {
            }
            Merchant merchant = new Merchant();
            merchant.addMerchantNote(Long.parseLong(this.merchantId), reasonToChangeSerialNo, sessionData.getUser().getUserLogonID(sessionData));

            pstmt.setString(3, this.simData);
            pstmt.setString(4, this.imciData);

            pstmt.setString(5, this.termVersion);
            pstmt.setString(6, this.termNote);

            if (this.MarketPlaceKey == 0) {
                pstmt.setNull(7, java.sql.Types.INTEGER);
            } else {
                pstmt.setInt(7, this.MarketPlaceKey);
            }

            if (this.MarketPlaceGroup == 0) {
                pstmt.setNull(8, java.sql.Types.INTEGER);
            } else {
                pstmt.setInt(8, this.MarketPlaceGroup);
            }

            pstmt.setInt(9, this.enablePINCache ? 1 : 0);

            if (this.bCachedReceipts) {
                pstmt.setByte(10, (byte) 1);
            } else {
                pstmt.setByte(10, (byte) 0);
            }

            {// Anonymous block in order to dispose the temp variable used here
                Terminal t2 = new Terminal();
                t2.setMerchantId(this.merchantId);
                t2.setSiteId(this.siteId);
                t2.getTerminalInfo();
                if (Terminal.IsTerminalInMBList(context, t2.getTerminalType())) {
                    pstmt.setString(11, request.getParameter("MBConfigurationID"));
                    if (!StringUtil.toString(t2.getLastMBConfigurationID()).equals(request.getParameter("MBConfigurationID"))) {
                        int nOldConfig = this.getMBConfigurationByTerminal(this.siteId);
                        this.deleteMBConfiguration(nOldConfig);
                        this.addMBConfigurationExt(this.siteId, this.siteId, this.merchantId, request.getParameter("MBConfigurationID"), true);
                    }
                } else {
                    this.MBConfigurationID = null;
                    pstmt.setString(11, this.MBConfigurationID);
                }
            }

            String cachereceiptsTypescontrol = request.getParameter("cachereceiptsTypesSelect");
            // iso_rateplan_id
            if (this.rateSelector.equals("1") && this.newproductId != null && this.newproductId.length > 0) {
                pstmt.setInt(12, Integer.parseInt(this.isoRatePlanId));
                if (cachereceiptsTypescontrol != null && cachereceiptsTypescontrol.length() > 0) {
                    pstmt.setInt(13, Integer.parseInt(cachereceiptsTypescontrol));
                } else {
                    pstmt.setNull(13, java.sql.Types.INTEGER);
                }

                pstmt.setString(14, this.terminalLabel);
                pstmt.setDouble(15, Double.parseDouble(this.siteId));
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_RATEPLAN), String.valueOf(this.isoRatePlanId), this.siteId, reason)) {
                }

            } else {
                String EmidaRatePlan = request.getParameter("EmidaRatePlan");
                if (request.getParameter("EmidaRatePlan") != null && !request.getParameter("EmidaRatePlan").equals("-1")) {
                    pstmt.setInt(12, Integer.parseInt(request.getParameter("EmidaRatePlan")));
                    if (cachereceiptsTypescontrol != null && cachereceiptsTypescontrol.length() > 0) {
                        pstmt.setInt(13, Integer.parseInt(cachereceiptsTypescontrol));
                    } else {
                        pstmt.setNull(13, java.sql.Types.INTEGER);
                    }

                    pstmt.setString(14, this.terminalLabel);
                    pstmt.setDouble(15, Double.parseDouble(this.siteId));
                    if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_RATEPLAN), String.valueOf(EmidaRatePlan), this.siteId, reason)) {
                    }
                } else {
                    if (cachereceiptsTypescontrol != null && cachereceiptsTypescontrol.length() > 0) {
                        pstmt.setInt(12, Integer.parseInt(cachereceiptsTypescontrol));
                    } else {
                        pstmt.setNull(12, java.sql.Types.INTEGER);
                    }
                    pstmt.setString(13, this.terminalLabel);
                    pstmt.setDouble(14, Double.parseDouble(this.siteId));
                }
            }

            pstmt.executeUpdate();
            pstmt.close();

            // If this is a marketPlace terminal the go to marketPlace and do
            // stuff.
            if (this.sMarketPlaceTerminal == true) {
                // David Castro Updating the siteId on MarketPlace
                this.updateMarketPlaceTerminal(context);
            }

            boolean blnRatesUpdated = false;
            if (!merchantUseRatePlanModel) {
                if (this.rateSelector.equals("1") && this.newproductId != null && this.newproductId.length > 0) {
                    blnRatesUpdated = this.updateNewTerminalRates(hashRepRates, dbConn, strDistChainType, strAccessLevel, sessionData);
                } else {
                    blnRatesUpdated = this.updateTerminalRates(hashRepRates, dbConn, strDistChainType, strAccessLevel, sessionData);
                }

                if (blnRatesUpdated == true) {
                    CSystemMethods.saveRateChangeLog(sessionData, Terminal.sql_bundle.getString("updateTerminalRates"), Terminal.sql_bundle, Terminal.cat);
                    Log.write(sessionData, "Terminal rates updated. Site Id:" + this.siteId, DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
                }
            }

            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalAmountMx"));
            pstmt.setDouble(1, Double.parseDouble(NumberUtil.formatAmount(this.monthlyFee)));
            pstmt.setDouble(2, Double.parseDouble(NumberUtil.formatAmount(this.annualInsuranceFee)));
            pstmt.setDouble(3, Double.parseDouble(NumberUtil.formatAmount(this.annualInsuranceTax)));
            pstmt.setDouble(4, Double.parseDouble(NumberUtil.formatAmount(this.annualMaintenanceFee)));
            pstmt.setDouble(5, Double.parseDouble(NumberUtil.formatAmount(this.annualMaintenanceTax)));
            pstmt.setDouble(6, Double.parseDouble(NumberUtil.formatAmount(this.reconnectionFee)));
            pstmt.setDouble(7, Double.parseDouble(NumberUtil.formatAmount(this.reconnectionTax)));
            pstmt.setDouble(8, Double.parseDouble(NumberUtil.formatAmount(this.annualAffiliationFee)));
            pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(this.annualAffiliationTax)));
            pstmt.setDouble(10, Double.parseDouble(NumberUtil.formatAmount(this.siteId)));

            pstmt.executeUpdate();
            pstmt.close();

            //TODO: fix trackChanges(context, sessionData);
            // Log.write(sessionData, "Site id:" + this.siteId + " Terminal
            // amount
            // updated from " + vecTerminalInfo.get(2).toString() + " " +
            // Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " +
            // NumberUtil.formatAmount(this.monthlyFee),
            // DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
            // DebisysConstants.PW_REF_TYPE_MERCHANT);
            dbConn.commit();
            dbConn.setAutoCommit(true);
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback updateTerminalMx", ex);
            }

            Terminal.cat.error("Error during updateTerminalMx", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    }

    public void deleteTerminal(SessionData sessionData) throws TerminalException {
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            PreparedStatement pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("deleteTerminalRates"));
            pstmt.setDouble(1, Double.parseDouble(this.terminalId));
            pstmt.executeUpdate();
            pstmt.close();
            Log.write(sessionData, "Terminal id:" + this.terminalId + " deleted", DebisysConstants.LOGTYPE_CUSTOMER, null, null);
            CSystemMethods.saveRateChangeLog(sessionData, Terminal.sql_bundle.getString("deleteTerminalRates"), Terminal.sql_bundle, Terminal.cat);
        } catch (Exception e) {
            Terminal.cat.error("Error during deleteTerminal", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    }

    // returns merchant id for a specific site id
    public String getMerchantId(String strSiteId) {
        Connection dbConn = null;
        String strMerchantId = "";

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getTerminalMerchantId"));
            pstmt.setString(1, strSiteId);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                strMerchantId = rs.getString("merchant_id");
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getMerchantId", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }

        return strMerchantId;
    }

    public void valueBound(HttpSessionBindingEvent event) {
    }

    public void valueUnbound(HttpSessionBindingEvent event) {
    }

    public static void main(String[] args) {
    }

    /* BEGIN MiniRelease 1.0 - Added by YH */
    public void addTerminalMx(ServletContext context, SessionData sessionData, Hashtable hashRepRates, HttpServletRequest request) throws TerminalException {
        Connection dbConn = null;
        String strRefId = sessionData.getProperty("ref_id");
        String strAccessLevel = sessionData.getProperty("access_level");
        String strTerminalId = this.generateTerminalId();
        String strSiteId = this.generateSiteId();
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        boolean bWebServiceAuthenticationEnabled = com.debisys.users.User.isWebServiceAutherticationTerminalsEnabled(sessionData, context);
        Hashtable hashTemp = null;
        boolean isRepRatePlan = true;
        boolean merchantHasNewModelRatePlan = false;

        if (this.isoRatePlanId.length() > 0) {
            isRepRatePlan = false;
        }

        boolean isValidPassword = true;
        if (this.terminalType.equalsIgnoreCase("23") || this.terminalType.equalsIgnoreCase("26")) {
            isValidPassword = validatePasswordsTerminal(this.pcTermPassword, sessionData, context);
        }
        if (isValidPassword) {
            String seeder = (String) context.getAttribute("secretSeed");
            SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
            this.pcTermPassword = cipherService.encrypt(this.pcTermPassword);

            //******************************************************************//
            // The routing number for banking information needs to be limited to 9
            // characters for all levels.
            //R30 DBSY 714 Jorge Nicol�s Mart�nez Romero
            //Monthly Fee Character Limit on the Add/Edit Terminal Page
            //Max 2 digits
            Pattern patt = Pattern.compile("[0-9]{0,3}(\\.[0-9]{0,2})?");
            Matcher m = patt.matcher(this.monthlyFee);
            if (!m.matches()) {
                this.addFieldError("monthlyFee", Languages.getString("jsp.admin.terminal.monthlyFee.PatternError", sessionData.getLanguage()));
                return;
            }
            // END R30 DBSY 714
            // *****************************************************//

            try {
                Merchant m2 = new Merchant();
                m2 = (Merchant) request.getAttribute("merchantDataBaseInfo");
                //m2.setMerchantId(this.merchantId);
                //m2.getMerchant(sessionData);

                if (m2.isUseRatePlanModel()) {
                    merchantHasNewModelRatePlan = true;
                    String EmidaRatePlanValue = request.getParameter("EmidaRatePlanValue");
                    if (!EmidaRatePlanValue.equals("null")) {
                        this.setRatePlan(EmidaRatePlanValue);
                    } else {
                        Terminal.cat.error("CANNOT CREATE TERMINAL BECAUSE: " + Languages.getString("jsp.admin.terminal.error_merchantNotHaveISOManagedRatePlans", sessionData.getLanguage()));
                        this.addFieldError("merchantNotHaveISOManagedRatePlans", Languages.getString("jsp.admin.terminal.error_merchantNotHaveISOManagedRatePlans", sessionData.getLanguage()));
                        //throw new TerminalException();
                        return;
                    }
                }
                if (!DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    m2.getMerchantMx(sessionData, context);
                    m2.setMonthlyFeeThreshold(this.monthlyFeeThreshold);
                    m2.updateMerchant(sessionData, context, request);
                }

            } catch (Exception e) {
                Terminal.cat.error("Error while saving Monthly Fee Threshold:", e);
                this.addFieldError("MonthlyFeeThresholdError", Languages.getString("jsp.admin.terminal.error_MonthlyFeeThreshold", sessionData.getLanguage()));
            }

            if (!merchantHasNewModelRatePlan) {
                hashTemp = this.getRepPercentages(sessionData);
            }

            try {
                dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

                if (dbConn == null) {
                    Terminal.cat.error("Can't get database connection");
                    throw new TerminalException();
                }

                PreparedStatement pstmt2;
                PreparedStatement pstmt = null;

                if (!merchantHasNewModelRatePlan) {
                    if (NumberUtil.isNumeric(this.repRatePlanId)) {
                        pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getIsoRepRatePlanId"));
                        pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));

                        ResultSet rs = pstmt.executeQuery();

                        if (rs.next()) {
                            this.isoRatePlanId = rs.getString("iso_rateplan_id");
                        }

                        rs.close();
                        pstmt.close();
                    }
                }
                dbConn.setAutoCommit(false);

                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalMx"));

                pstmt.setDouble(1, Double.parseDouble(strTerminalId));
                pstmt.setDouble(2, Double.parseDouble(this.merchantId));
                pstmt.setInt(3, Integer.parseInt(strSiteId));
                pstmt.setString(4, this.terminalType);
                // pinpad type
                pstmt.setInt(5, 0);
                // printer type
                pstmt.setInt(6, 0);
                // version
                pstmt.setInt(7, 2);

                // purchase type = customer provided or FSO Rental
                if ((NumberUtil.isNumeric(this.monthlyFeeThreshold) && (Double.parseDouble(this.monthlyFeeThreshold) > 0)) || (NumberUtil.isNumeric(this.monthlyFee) && (Double.parseDouble(this.monthlyFee) > 0))) {
                    pstmt.setInt(8, 15);
                } else {
                    pstmt.setInt(8, 5);
                }

                // amount
                pstmt.setDouble(9, Double.parseDouble(this.monthlyFee));

                if (this.isoRatePlanId.length() > 0) {
                    pstmt.setInt(10, Integer.parseInt(this.isoRatePlanId));
                } else {
                    pstmt.setInt(10, -1);
                }

                pstmt.setInt(11, 1);
                pstmt.setDouble(12, Double.parseDouble(this.annualInsuranceFee));
                pstmt.setDouble(13, Double.parseDouble(this.annualInsuranceTax));
                pstmt.setDouble(14, Double.parseDouble(this.annualMaintenanceFee));
                pstmt.setDouble(15, Double.parseDouble(this.annualMaintenanceTax));
                pstmt.setDouble(16, Double.parseDouble(this.reconnectionFee));
                pstmt.setDouble(17, Double.parseDouble(this.reconnectionTax));
                pstmt.setDouble(18, Double.parseDouble(this.annualAffiliationFee));
                pstmt.setDouble(19, Double.parseDouble(this.annualAffiliationTax));
                pstmt.setString(20, null); // Software version
                /* BEGIN MiniRelease 9.0 - Added by YH */

                pstmt.setString(21, this.serialNumber);
                pstmt.setString(22, this.simData);
                pstmt.setString(23, this.imciData);
                /* END MiniRelease 9.0 - Added by YH */
                // BEGIN Release 16.5 New fields to the terminal. Add by YH
                pstmt.setString(24, this.termVersion);
                pstmt.setString(25, this.termNote);

                // END Release 16.5 Add by YH
                // BEGIN JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE -
                // Asignar
                // los datos correspondientes a terminal fisica, no se valida q sea
                // Mexico
                // xq eso ya se sabe
                if ((this.terminalFisico != null) && Terminal.IsTerminalInPhysicalList(context, this.terminalType)) {
                    pstmt.setLong(26, this.terminalFisico.getTerminalId());
                } else {
                    pstmt.setNull(26, java.sql.Types.INTEGER);
                }

                // END JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE
                // DJC MarketPlace Store Group and Key for marketPlace
                if (this.sMarketPlaceTerminal) {
                    if (this.MarketPlaceKey == 0) {
                        pstmt.setNull(28, java.sql.Types.INTEGER);
                    } else {
                        pstmt.setInt(28, this.MarketPlaceKey);
                    }

                    if (this.MarketPlaceGroup == 0) {
                        pstmt.setNull(27, java.sql.Types.INTEGER);
                    } else {
                        pstmt.setInt(27, this.MarketPlaceGroup);
                    }

                } else {
                    pstmt.setNull(27, java.sql.Types.INTEGER);
                    pstmt.setNull(28, java.sql.Types.INTEGER);
                }
                pstmt.setByte(29, this.bCachedReceipts ? (byte) 1 : (byte) 0);
                pstmt.setLong(30, this.Status);
                pstmt.setString(31, this.RatePlan);
                if (Terminal.IsTerminalInMBList(context, this.terminalType)) {
                    pstmt.setString(32, this.MBConfigurationID);
                } else {
                    this.MBConfigurationID = null;
                    pstmt.setString(32, this.MBConfigurationID);
                }
                if (!this.carrierId.equals("null") && !this.carrierId.equals("-1") && this.carrierId.length() > 0) {
                    pstmt.setString(33, this.carrierId);
                } else {
                    pstmt.setNull(33, java.sql.Types.INTEGER);
                }
                pstmt.setString(34, this.terminalLabel);
                pstmt.executeUpdate();
                pstmt.close();

                //R34 Support Revamp
                String reasonToChangeSerialNo = "Terminal Inserted: Site ID " + this.siteId + " has Serial Number: " + this.serialNumber + ";";
                Merchant merchant = new Merchant();
                merchant.addMerchantNote(Long.parseLong(this.merchantId), reasonToChangeSerialNo, sessionData.getUser().getUserLogonID(sessionData));
                //END R34 Support Revamp

                if (this.terminalType.equals("26")) {
                    // terminal properties for a new Pcterminal

                    String propertiesWebPCTerm = Terminal.sql_bundle.getString("getInsertPropertiesWebPCTerm");
                    pstmt = dbConn.prepareStatement(propertiesWebPCTerm);
                    pstmt.setInt(1, Integer.parseInt(strSiteId));
                    pstmt.setString(2, "PrinterInchSize");
                    pstmt.setString(3, "3");
                    pstmt.addBatch();

                    pstmt.setInt(1, Integer.parseInt(strSiteId));
                    pstmt.setString(2, "ReceiptCopy");
                    pstmt.setString(3, "0");
                    pstmt.addBatch();

                    pstmt.executeBatch();
                    pstmt.close();

                }

                // If the terminal being added is PC Terminal, proceed with the
                // insert queries
                Vector vTerminalTypes = Terminal.getTerminalTypes_Combo(request.getParameter("terminalClass"));
                Iterator it = vTerminalTypes.iterator();
                int idTerminalRegistration = 0;
                while (it.hasNext()) {
                    Vector vTmp = (Vector) it.next();
                    boolean termWithPermAuth = (vTmp.get(1).toString().contains("Web Service")
                            || vTmp.get(1).toString().contains("Web Activation")) && bWebServiceAuthenticationEnabled;
                    if (vTmp.get(0).toString().equals(this.terminalType)
                            && (vTmp.get(1).toString().equals("PC Terminal")
                            || vTmp.get(1).toString().equals("SMS Phone (merchant)")
                            || vTmp.get(1).toString().equals("Kiosk (USSD/SMS)")
                            || vTmp.get(1).toString().contains("SMS")
                            || vTmp.get(1).toString().equals("SMS Phone (subscriber)")
                            || vTmp.get(1).toString().startsWith("Nokia")
                            || vTmp.get(1).toString().equalsIgnoreCase("SmartPhoneAndroid")
                            || vTmp.get(1).toString().equalsIgnoreCase("SmartPhoneIPhone")
                            || vTmp.get(1).toString().equalsIgnoreCase("Smartphone TPS-900")
                            || vTmp.get(1).toString().equals("Tablet")
                            || vTmp.get(1).toString().equals("SMS Printer")
                            || vTmp.get(1).toString().equalsIgnoreCase("3G SMS Printer")
                            || termWithPermAuth
                            || vTmp.get(1).toString().contains("Ingenico T3")
                            || vTmp.get(1).toString().contains("Quick Sell App")
                            || vTmp.get(1).toString().contains("Infopyme Kiosk USSD")
                            || vTmp.get(1).toString().equals("Verifone"))) {
                        if (!this.chkManualRegCode) {
                            this.txtRegistration = strSiteId;
                        }

                        if (this.txtAllowCertificate.length() == 0) {
                            this.txtAllowCertificate = "0";
                        }

                        if (vTmp.get(1).toString().startsWith("Nokia")
                                || vTmp.get(1).toString().equalsIgnoreCase("SmartPhoneAndroid")
                                || vTmp.get(1).toString().equalsIgnoreCase("SmartPhoneIPhone")
                                || vTmp.get(1).toString().equalsIgnoreCase("Ingenico T3")
                                || vTmp.get(1).toString().equalsIgnoreCase("Smartphone TPS-900")) {
                            this.pcTermUsername = strSiteId; //request.getParameter("javaHandSetUserName");
                            this.pcTermPassword = strSiteId; //request.getParameter("javaHandSetPassword");
                            //NOT FOR NOW
                            //this.pcTermBrand = request.getParameter("javaHandSetBranding");
                            this.txtRegistration = TerminalRegistration.getUUID();
                        }

                        String sUpdateInfo = "";
                        sUpdateInfo = "[millennium_no=" + strSiteId + "] [RegistrationCode=" + this.txtRegistration + "] [RegistrationInd=-1] [RegistrationDate=null] ";
                        sUpdateInfo += ("[UserId=" + this.pcTermUsername + "] [Password=" + this.pcTermPassword + "] [RegistrationIp=null] [RegistrationAttemptNum=0] ");
                        sUpdateInfo += ("[RegistrationIgnoreInd=" + (this.chkRegCode ? "-1" : "1") + "] [CustomConfigId=" + this.pcTermBrand + "] ");
                        sUpdateInfo += ("[RestrictTolpAddr=" + this.txtIPAddress + "] [RestrictToSubnetMask=" + this.txtSubnetMask + "] ");
                        sUpdateInfo += ("[ipAddrIgnoreInd=" + (this.chkIPRestriction ? "-1" : "1") + "] [CertificateIgnoreInd=" + (this.chkCertificate ? "-1" : "1") + "] ");
                        sUpdateInfo += ("[CertificateInstallAlowedInd=" + (this.chkAllowCertificate ? "-1" : "1") + "] [CertificateInstallRemainNum=" + this.txtAllowCertificate + "] ");
                        sUpdateInfo += ("[RegistrationByUserIgnoreInd=" + (this.chkConfirmationByUser ? "-1" : "1") + "] [NotifyByEmailIgnoreInd=" + (this.chkConfirmationByEmail ? "-1" : "1") + "]");
                        sUpdateInfo += ("[NotifyBySmsIgnoreInd=" + (this.chkConfirmationBySMS ? "-1" : "1") + "] [NotificationEmail=" + this.txtConfirmationEMail + "]");
                        sUpdateInfo += ("[NotificationPhoneNumber=" + this.txtConfirmationPhoneNumber + "]");

                        pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalRegistration"));
                        pstmt.setInt(1, Integer.parseInt(strSiteId));
                        pstmt.setString(2, this.txtRegistration);
                        // RegistrationInd = -1
                        // RegistrationDate = null
                        pstmt.setString(3, this.pcTermUsername);
                        pstmt.setString(4, this.pcTermPassword);
                        // RegistrationIp = null
                        // RegistrationAttemptNum = 0
                        pstmt.setInt(5, (this.chkRegCode ? (-1) : 1)); // RegistrationIgnoreInd
                        pstmt.setString(6, this.pcTermBrand); // CustomConfigId
                        pstmt.setString(7, this.txtIPAddress); // RestrictToIpAddr
                        pstmt.setString(8, this.txtSubnetMask); // RestrictToSubnetMask
                        pstmt.setInt(9, (this.chkIPRestriction ? (-1) : 1)); // ipAddrIgnoreInd
                        pstmt.setInt(10, (this.chkCertificate ? (-1) : 1)); // CertificateIgnoreInd
                        pstmt.setInt(11, (this.chkAllowCertificate ? (-1) : 1)); // CertificateInstallAlowedInd
                        pstmt.setInt(12, Integer.parseInt(this.txtAllowCertificate)); // CertificateInstallRemainNum
                        pstmt.setTimestamp(13, this.reCalculateDateExpiration(context));
                        pstmt.setInt(14, (this.chkConfirmationByUser ? (-1) : 1)); // RegistrationByUserIgnoreInd

                        pstmt.setInt(15, (this.chkConfirmationByEmail ? (-1) : 1)); // NotifyByEmailIgnoreInd
                        pstmt.setInt(16, (this.chkConfirmationBySMS ? (-1) : 1)); // NotifyBySmsIgnoreInd
                        pstmt.setString(17, this.txtConfirmationEMail); // NotificationEmail
                        pstmt.setString(18, this.txtConfirmationPhoneNumber); // NotificationPhoneNumber
                        pstmt.setBoolean(19, this.chkIsAdmin);

                        ResultSet rsTerminalReg = pstmt.executeQuery();

                        if (rsTerminalReg.next()) {
                            //IF YOU NEED AN AUTONUMERIC ID FOR SOME REASON
                            //YOU CAN TAKE IT HERE
                            idTerminalRegistration = rsTerminalReg.getInt(1);
                            //System.out.println("autonumeric id from terminal_registration: "+idTerminalRegistration);
                        }

                        rsTerminalReg.close();
                        pstmt.close();

                        pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalActivityLog"));
                        // CurrentDate
                        pstmt.setInt(1, Integer.parseInt(strSiteId));
                        pstmt.setString(2, this.txtRegistration);
                        pstmt.setString(3, this.pcTermUsername);
                        pstmt.setString(4, request.getRemoteHost());
                        pstmt.setString(5, request.getLocalAddr());
                        pstmt.setString(6, request.getContextPath());
                        // Class = SupportSite
                        pstmt.setString(7, "addTerminal"); // FunctionName
                        pstmt.setString(8, "ADMIN ADD: " + sUpdateInfo); // Description
                        pstmt.setString(9, "OK"); // ErrorCode
                        pstmt.setString(10, sUpdateInfo); // Error Message
                        pstmt.setString(11, this.pcTermBrand); // CustomConfigId

                        pstmt.executeUpdate();
                        pstmt.close();

                        //TODO: fix trackChanges(context, sessionData);
                        break;
                    }
                } // End of if the terminal being added is PC Terminal, proceed with
                // the insert
                // queries

                // END JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE
                Log.write(sessionData, " Site Id:" + strSiteId + " added.", DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);

                String isoRatePlanRepId = "";

                if ((this.saveRates != null) && this.saveRates.equalsIgnoreCase("y")) {
                    pstmt2 = dbConn.prepareStatement(Terminal.sql_bundle.getString("getIsoRatePlanRepId"));
                    pstmt2.setInt(1, Integer.parseInt(this.repRatePlanId));

                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.REP)) {
                        pstmt2.setDouble(2, Double.parseDouble(strRefId));
                    } else if (strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                        pstmt2.setDouble(2, Double.parseDouble(this.repId));
                    }

                    ResultSet rs2 = pstmt2.executeQuery();

                    if (rs2.next()) {
                        isoRatePlanRepId = rs2.getString("iso_rateplan_rep_id");
                        pstmt2 = dbConn.prepareStatement(Terminal.sql_bundle.getString("deleteRepDefaultRates"));
                        pstmt2.setInt(1, Integer.parseInt(isoRatePlanRepId));
                        pstmt2.executeUpdate();
                        pstmt2.close();
                    }
                }

                if (!merchantHasNewModelRatePlan) {
                    pstmt2 = dbConn.prepareStatement(Terminal.sql_bundle.getString("addRepDefaultRates"));
                    pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalRates"));

                    for (int i = 0; i < this.productId.length; i++) {
                        String strProductId = this.productId[i];

                        double totalRate = 0;
                        double isoRate = 0;
                        double agentRate = 0;
                        double subagentRate = 0;
                        double repRate = 0;
                        double merchantRate = 0;

                        Vector vecTemp = (Vector) hashTemp.get(strProductId);

                        if (NumberUtil.isNumeric(vecTemp.get(0).toString())) {
                            totalRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
                        }

                        if (NumberUtil.isNumeric(vecTemp.get(1).toString())) {
                            isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
                        }

                        if (NumberUtil.isNumeric(vecTemp.get(2).toString())) {
                            agentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
                        }

                        if (NumberUtil.isNumeric(vecTemp.get(3).toString())) {
                            subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(3).toString()));
                        }

                        vecTemp = (Vector) hashRepRates.get(strProductId);

                        Double tempSus = null;
                        String tempSusString = "";
                        Double tempSusFinal = null;
                        /*
					 * It Doesn't matter if the creation process comes from Iso
					 * rate plan or Rep Rate Plan. It just catches the rep split
					 * here. the others splits comes from isorate_lineitems
					 * table.
                         */
                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));

                                if (!isRepRatePlan) {
                                    tempSus = new Double((isoRate + agentRate + subagentRate + repRate));
                                    tempSusString = NumberUtil.formatAmount(tempSus.toString());
                                    tempSusFinal = new Double(tempSusString);
                                    merchantRate = totalRate - (tempSusFinal);
                                } else {
                                    merchantRate = totalRate - repRate;
                                }
                            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));

                                if (!isRepRatePlan) {
                                    tempSus = new Double((isoRate + repRate));
                                    tempSusString = NumberUtil.formatAmount(tempSus.toString());
                                    tempSusFinal = new Double(tempSusString);
                                    merchantRate = totalRate - (tempSusFinal);
                                } else {
                                    merchantRate = totalRate - repRate;
                                }
                            }

                        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {

                            repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));

                            if (!isRepRatePlan) {
                                tempSus = new Double(agentRate + subagentRate + repRate);
                                tempSusString = NumberUtil.formatAmount(tempSus.toString());
                                tempSusFinal = new Double(tempSusString);
                                merchantRate = totalRate - (tempSusFinal);
                            } else {
                                merchantRate = totalRate - repRate;
                            }

                        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {

                            repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));

                            if (!isRepRatePlan) {
                                tempSus = new Double(subagentRate + repRate);
                                tempSusString = NumberUtil.formatAmount(tempSus.toString());
                                tempSusFinal = new Double(tempSusString);
                                merchantRate = totalRate - (tempSusFinal);
                            } else {
                                merchantRate = totalRate - repRate;
                            }

                        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                            repRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));

                            merchantRate = totalRate - repRate;

                        }

                        if ((totalRate >= 0) && (merchantRate >= 0)) {

                            merchantRate = Double.parseDouble(NumberUtil.formatAmount(Double.toString(merchantRate)));

                            pstmt.setInt(1, Integer.parseInt(strSiteId));
                            pstmt.setInt(2, Integer.parseInt(strProductId));
                            pstmt.setDouble(3, Double.parseDouble(strTerminalId));

                            pstmt.setDouble(4, isoRate);
                            pstmt.setDouble(5, agentRate);
                            pstmt.setDouble(6, subagentRate);
                            pstmt.setDouble(7, repRate);
                            pstmt.setDouble(8, merchantRate);
                            pstmt.setInt(9, Integer.parseInt(this.isoRatePlanId));
                            pstmt.setString(10, sessionData.getUser().getUsername());
                            pstmt.setLong(11, Long.parseLong(this.getMerchantId()));
                            pstmt.addBatch();

                            if (!isoRatePlanRepId.equals("")) {
                                pstmt2.setInt(1, Integer.parseInt(isoRatePlanRepId));
                                pstmt2.setInt(2, Integer.parseInt(strProductId));
                                pstmt2.setDouble(3, repRate);
                                pstmt2.addBatch();
                            }
                        } else {
                            // its an error
                            this.addFieldError(strProductId, Languages.getString("jsp.admin.terminal.error_product", sessionData.getLanguage()).replaceAll("_PRODUCT_", strProductId).replaceAll("_PLAN_", this.isoRatePlanId));
                        }
                    }

                    if (this.getErrors().size() > 0) {
                        throw new TerminalException();
                    }

                    pstmt.executeBatch();
                    pstmt.close();

                    if (!isoRatePlanRepId.equals("")) {
                        pstmt2.executeBatch();
                    }
                    pstmt2.close();

                }

                // BEGIN JFM 25-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE -
                // Actualizar
                // el estado de la terminal fisica si hay, para que no siga
                // disponible
                if ((this.terminalFisico != null) && Terminal.IsTerminalInPhysicalList(context, this.terminalType)) {
                    CTerminalStatus assignedStatus = CTerminalStatus.getTerminalStatusByCode(CTerminalStatus.ASSIGN_CODE);

                    if (this.terminalFisico.changeStatus(assignedStatus.getTerminalSatusId())) {
                        if (!CPhysicalTerminal.insertTerminalStatusChange(this.terminalFisico.getTerminalId(), Calendar.getInstance(), assignedStatus.getTerminalSatusId(), strRefId, Languages
                                .getString("jsp.admin.transactions.inventoryTerminals.PhisicalTerminalAssign", sessionData.getLanguage()))) {
                            this.addFieldError("insertTerminalStatusChange", Languages.getString("jsp.admin.terminal.error_inventoryTerminals.PhisicalTerminalAssign", sessionData.getLanguage()));
                            throw new TerminalException();
                        }
                    }
                }

                if (this.sMarketPlaceTerminal) {
                    this.addMarketPlaceTerminal(strSiteId, context);
                }

                //DBSY-814 jacuna.
                // This will update in teminal_mappings the providerTerminalID (for
                // BOOST only)
                if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && sessionData.checkPermission(DebisysConstants.PERM_ENABLE_EDIT_SALES_FORCE_ID) && this.providerTerminalID != null
                        && this.providerTerminalID.trim().length() > 0) {
                    pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalMapping"));
                    pstmt.setInt(1, this.providerId); 				//providerId = 110
                    pstmt.setInt(2, Integer.parseInt(strSiteId)); //debisysTerminalId
                    pstmt.setString(3, this.providerTerminalID);	//providerTerminalID
                    Terminal.cat.debug(Terminal.sql_bundle.getString("updateTerminalMapping") + " = " + this.providerId + "," + this.siteId + "," + this.providerTerminalID);
                    pstmt.executeUpdate();
                    pstmt.close();
                }
                ArrayList<TerminalMapping> listMappings = new ArrayList<TerminalMapping>();
                if (request.getSession().getAttribute("terminalMappings") != null) {
                    listMappings = (ArrayList<TerminalMapping>) request.getSession().getAttribute("terminalMappings");
                    for (TerminalMapping newTerminalMapping : listMappings) {
                        newTerminalMapping.setDebisysTerminalId(Integer.parseInt(strSiteId));
                        newTerminalMapping.createNewTerminalMapping(dbConn);
                    }
                }

                dbConn.commit();
                dbConn.setAutoCommit(true);

                this.insertNewRegCodeInHistory(idTerminalRegistration, this.getRegistrationIdCode());

                CSystemMethods.saveRateChangeLog(sessionData, Terminal.sql_bundle.getString("addRepDefaultRates"), Terminal.sql_bundle, Terminal.cat);
                CSystemMethods.saveRateChangeLog(sessionData, Terminal.sql_bundle.getString("addTerminalRates"), Terminal.sql_bundle, Terminal.cat);

                if (!merchantHasNewModelRatePlan) {
                    Log.write(sessionData, " Site Id:" + strSiteId + " prod : " + this.productId.length, DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
                } else {
                    Log.write(sessionData, " Site Id:" + strSiteId + " EMIDA RATE PLAN: " + this.getRatePlan(), DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
                }
                this.terminalId = strTerminalId;
                this.siteId = strSiteId;
            } catch (SQLException ex) {

                Terminal.cat.error("Error during addTerminal SQLException:", ex);

                try {
                    if (dbConn != null) {
                        dbConn.rollback();
                    }
                } catch (SQLException sqex) {
                    Terminal.cat.error("Error during rollback addTerminal", sqex);
                }

                if (ex.getMessage().indexOf("UserData") > 0 && ex.getMessage().indexOf("pk_UserData") > 0) {
                    // this error comes from EJBCA DataBase other instance of the
                    // SQLServer
                    this.addFieldError("terminal_registration", "EJBCA : " + Languages.getString("com.debisys.users.error_username_duplicate", sessionData.getLanguage()));
                } else {
                    this.addFieldError("terminal_registration", Languages.getString("jsp.admin.terminal.general_add_error", sessionData.getLanguage()));
                }

                throw new TerminalException();
            } catch (Exception e) {
                try {
                    if (dbConn != null) {
                        dbConn.rollback();
                    }
                } catch (SQLException ex) {
                    Terminal.cat.error("Error during rollback addTerminalMx", ex);
                }

                Terminal.cat.error("Error during addTerminal", e);
                throw new TerminalException();
            } finally {
                try {
                    Torque.closeConnection(dbConn);
                } catch (Exception e) {
                    Terminal.cat.error("Error during release connection", e);
                }
            }
        }

    }

    private String getRegistrationIdCode() {
        return ((this.terminalType.equalsIgnoreCase(DebisysConstants.WEB_SERVICE_TERMINAL_TYPE)) ? this.getRegistrationCodeForWebService() : this.txtRegistration);
    }

    private String getRegistrationCodeForWebService() {
        return this.pcTermPassword;
    }

    /* END MiniRelease 1.0 - Added by YH */
    private void trackTrmRegChanges(ServletContext context, SessionData sessionData, int id, String action) {
        /* Do the tracking for the changes */
        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);
        Merchant merchant = new Merchant();
        merchant.setMerchantId(this.getMerchantId(this.getSiteId()));
        try {
            merchant.getMerchant(sessionData, context);
            String reason = action + ": " + this.getSiteId() + ", Terminal Registration ID: " + id;
            String customerNote = "Customer Note: ";
            boolean updated = false;
            try {
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_USERNAME), this.getPcTermUsername(), String.valueOf(id), customerNote + reason + ". Username = " + this.getPcTermUsername())) {
                    updated = true;
                    reason += ". Username = " + this.getPcTermUsername();
                }
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_PASSWORD), this.getPcTermPassword(), String.valueOf(id), customerNote + reason + ". Password = " + this.getPcTermPassword())) {
                    updated = true;
                    reason += ". Password = " + this.getPcTermPassword();
                }
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_ENFORCECERT), String.valueOf(this.isChkCertificate()), String.valueOf(id), customerNote + reason + ". Enforce Cert. = " + String.valueOf(this.isChkCertificate()))) {
                    updated = true;
                    reason += ". Enforce Cert. = " + String.valueOf(this.isChkCertificate());
                }
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CERTINSTALL), String.valueOf(this.isChkAllowCertificate()), String.valueOf(id), customerNote + reason + ". Cert. Install = " + String.valueOf(this.isChkAllowCertificate()))) {
                    updated = true;
                    reason += ". Cert. Install = " + String.valueOf(this.isChkAllowCertificate());
                }
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CERTREMAINING), this.getTxtAllowCertificate(), String.valueOf(id), customerNote + reason + ". Cert. Install Remaining Amount = " + this.getTxtAllowCertificate())) {
                    updated = true;
                    reason += ". Cert. Install Remaining Amount = " + this.getTxtAllowCertificate();
                }
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_ENFORCEREGCODE), String.valueOf(this.isChkRegCode()), String.valueOf(id), customerNote + reason + ". Enforce RegCode = " + String.valueOf(this.isChkRegCode()))) {
                    updated = true;
                    reason += ". Enforce RegCode = " + String.valueOf(this.isChkRegCode());
                }
                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_REGCODE), this.getTxtRegistration(), String.valueOf(id), customerNote + reason + ". RegCode = " + this.getTxtRegistration())) {
                    updated = true;
                    reason += ". RegCode = " + this.getTxtRegistration();
                }
                if (updated) {
                    merchant.addMerchantNote(Long.parseLong(merchant.getMerchantId()), reason, sessionData.getProperty("username"));
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (CustomerException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    /* BEGIN Release 8.1 - Added by LF */
    public String getISORatePlanNameFromTerminal() throws TerminalException {
        String sResult = "";
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getISORatePlanNameFromTerminal");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.siteId));

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                if (rs.getByte(3) == 1) {
                    sResult = rs.getString("Name");
                } else {
                    sResult = rs.getString("RatePlan");
                }
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getISORatePlanNameFromTerminal", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return sResult;
    }

    /* END Release 8.1 - Added by LF */

 /* BEGIN Release 11.0 - Added by LF */
    public boolean IsSerialNumberUnique(String sSerialNumber, String sSiteID) throws TerminalException {
        boolean bResult = false;
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("isSerialNumberUnique");

            if (sSiteID.length() > 0) {
                strSQL += (" AND millennium_no <> '" + sSiteID + "'");
            }

            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, sSerialNumber);

            ResultSet rs = pstmt.executeQuery();
            bResult = !rs.next();
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during IsSerialNumberUnique", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    } // End of function IsSerialNumberUnique

    /* END Release 11.0 - Added by LF */

 /* BEGIN Release 21 - Added by LF */
    public Vector getPCTerminalBrands(String sISO) throws TerminalException {
        Vector vResult = new Vector();
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getPCTerminalBrands");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, sISO);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vTemp = new Vector();
                vTemp.add(rs.getString("CustomConfigId"));
                vTemp.add(rs.getString("Name"));
                vTemp.add(rs.getString("URL"));
                vTemp.add(rs.getString("SecureURL"));
                vResult.add(vTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getISORatePlanNameFromTerminal", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vResult;
    }

    /*
	 * Allows to detect if the provided RegistrationCode already exists in the
	 * database when creating a PC Terminal
     */
    public boolean isRegistrationCodeUnique(String sID) {
        Connection dbConn = null;
        boolean bResult = true;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sSQL = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            sSQL = Terminal.sql_bundle.getString("isRegistrationCodeUnique");

            if (sID.length() > 0) {
                sSQL += (" AND ID <> " + sID);
            }

            pstmt = dbConn.prepareStatement(sSQL);

            if (!this.chkManualRegCode) {
                this.txtRegistration = this.siteId;
            }
            /*            if(this.chkConfirmationByUser){
                this.txtRegistration = StringUtil.randomString(StringUtil.CHARSET_AZ_09, 10);
            }*/

            pstmt.setString(1, this.txtRegistration);
            pstmt.setString(2, this.pcTermUsername);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                bResult = false;
            }

            pstmt.close();
            rs.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during isRegistrationCodeUnique", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    } // End of function isRegistrationCodeUnique

    /*
	 * Allows to detect if the provided UserID already exists when creating a PC
	 * Terminal
     */
    public boolean isUserIDUnique(String sID) {
        Connection dbConn = null;
        boolean bResult = true;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sSQL = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            sSQL = Terminal.sql_bundle.getString("isUserIDUnique");

            if (sID.length() > 0) {
                sSQL += (" AND ID <> " + sID);
            }

            pstmt = dbConn.prepareStatement(sSQL);
            pstmt.setString(1, this.pcTermUsername);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                bResult = false;
            }

            pstmt.close();
            rs.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during isUserIDUnique", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    } // End of function isUserIDUnique

    public Vector getPCTerminalRegistrations(String sID, ServletContext context) throws TerminalException {
        Vector vResult = new Vector();
        Connection dbConn = null;

        String seeder = (String) context.getAttribute("secretSeed");
        SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getPCTerminalRegistrations");

            if (sID.length() > 0) {
                strSQL += (" AND ID = " + sID);
            }

            strSQL += " ORDER BY millennium_no";

            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, this.siteId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vTemp = new Vector();
                vTemp.add(StringUtil.toString(rs.getString("id")));
                vTemp.add(StringUtil.toString(rs.getString("millennium_no")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationCode")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationInd")));
                vTemp.add(DateUtil.formatDateTime(rs.getTimestamp("RegistrationDate")));
                vTemp.add(StringUtil.toString(rs.getString("UserId")));

                if (rs.getBoolean("isEncrypted")) {
                    try {
                        String pw = cipherService.decrypt(rs.getString("Password"));
                        vTemp.add(pw);
                    } catch (Exception e) {
                        vTemp.add("********");
                    }
                } else {
                    vTemp.add(StringUtil.toString(rs.getString("Password")));
                }

//                                vTemp.add(StringUtil.toString(rs.getString("Password")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationIp")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationAttemptNum")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationIgnoreInd")));
                vTemp.add(StringUtil.toString(rs.getString("CustomConfigId")));
                vTemp.add(StringUtil.toString(rs.getString("ipAddrIgnoreInd")));
                vTemp.add(StringUtil.toString(rs.getString("RestrictTolpAddr")));
                vTemp.add(StringUtil.toString(rs.getString("DisabledInd"))); // 13
                vTemp.add(StringUtil.toString(rs.getString("lastLoginAttemptNum")));
                vTemp.add(DateUtil.formatDateTime(rs.getTimestamp("LastLoginAttemptDate")));
                vTemp.add(DateUtil.formatDateTime(rs.getTimestamp("LastLoginSuccessDate")));
                vTemp.add(StringUtil.toString(rs.getString("LastLoginAttemptIpAddr")));
                vTemp.add(StringUtil.toString(rs.getString("LastLoginSuccessIpAddr")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationReuseNum")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationReuseMax")));
                vTemp.add(StringUtil.toString(rs.getString("BypassLoginInd")));
                vTemp.add(StringUtil.toString(rs.getString("LoginTimeRestrictInd")));
                vTemp.add(StringUtil.toString(rs.getString("LoginTimeRestrictFrom")));
                vTemp.add(StringUtil.toString(rs.getString("LoginTimeRestrictTo")));
                vTemp.add(StringUtil.toString(rs.getString("RestrictToSubnetMask")));
                vTemp.add(StringUtil.toString(rs.getString("CertificateIgnoreInd"))); // 26
                vTemp.add(StringUtil.toString(rs.getString("CertificateStatusCode")));
                vTemp.add(DateUtil.formatDateTime(rs.getTimestamp("CertificateStatusDate")));
                vTemp.add(DateUtil.formatDateTime(rs.getTimestamp("CertificateRequestDate")));
                vTemp.add(StringUtil.toString(rs.getString("CertificateInstallAlowedInd")));
                vTemp.add(StringUtil.toString(rs.getString("CertificateInstallRemainNum")));
                vTemp.add(StringUtil.toString(rs.getString("CertificateInstallCompleteNum")));
                vTemp.add(StringUtil.toString(rs.getString("RegistrationByUserIgnoreInd"))); //33
                vTemp.add(StringUtil.toString(rs.getString("NotifyByEmailIgnoreInd")));
                vTemp.add(StringUtil.toString(rs.getString("NotifyBySmsIgnoreInd")));
                vTemp.add(StringUtil.toString(rs.getString("NotificationEmail")));
                vTemp.add(StringUtil.toString(rs.getString("NotificationPhoneNumber"))); //37
                vTemp.add(StringUtil.toString(rs.getString("isAdmin"))); //38                                
                vResult.add(vTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getPCTerminalRegistrations", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vResult;
    } // End of function getPCTerminalRegistrations

    public void addPCTerminalRegistration(ServletContext context, SessionData sessionData, HttpServletRequest request) throws TerminalException {
        Connection dbConn = null;
        ResultSet rs = null;

        try {
            boolean isValidPassword = validatePasswordsTerminal(this.pcTermPassword, sessionData, context);
            if (isValidPassword) {
                String seeder = (String) context.getAttribute("secretSeed");
                SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
                this.pcTermPassword = cipherService.encrypt(this.pcTermPassword);
                dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

                if (dbConn == null) {
                    Terminal.cat.error("Can't get database connection");
                    throw new TerminalException();
                }

                dbConn.setAutoCommit(false);

                if (!this.chkManualRegCode) {
                    this.txtRegistration = this.siteId;
                }
                /*            if(this.chkConfirmationByUser){
                this.txtRegistration = StringUtil.randomString(StringUtil.CHARSET_AZ_09, 10);
            }*/

                if (this.txtAllowCertificate.length() == 0) {
                    this.txtAllowCertificate = "0";
                }

                String sUpdateInfo = "";
                sUpdateInfo = "[millennium_no=" + this.siteId + "] [RegistrationCode=" + this.txtRegistration + "] [RegistrationInd=-1] [RegistrationDate=null] ";
                sUpdateInfo += ("[UserId=" + this.pcTermUsername + "] [Password=" + this.pcTermPassword + "] [RegistrationIp=null] [RegistrationAttemptNum=0] ");
                sUpdateInfo += ("[RegistrationIgnoreInd=" + (this.chkRegCode ? "-1" : "1") + "] [CustomConfigId=" + this.pcTermBrand + "] ");
                sUpdateInfo += ("[RestrictTolpAddr=" + this.txtIPAddress + "] [RestrictToSubnetMask=" + this.txtSubnetMask + "] ");
                sUpdateInfo += ("[ipAddrIgnoreInd=" + (this.chkIPRestriction ? "-1" : "1") + "] [CertificateIgnoreInd=" + (this.chkCertificate ? "-1" : "1") + "] ");
                sUpdateInfo += ("[CertificateInstallAlowedInd=" + (this.chkAllowCertificate ? "-1" : "1") + "] [CertificateInstallRemainNum=" + this.txtAllowCertificate + "] ");
                sUpdateInfo += ("[RegistrationByUserIgnoreInd=" + (this.chkConfirmationByUser ? "-1" : "1") + "] [NotifyByEmailIgnoreInd=" + (this.chkConfirmationByEmail ? "-1" : "1") + "]");
                sUpdateInfo += ("[NotifyBySmsIgnoreInd=" + (this.chkConfirmationBySMS ? "-1" : "1") + "] [NotificationEmail=" + this.txtConfirmationEMail + "]");
                sUpdateInfo += ("[NotificationPhoneNumber=" + this.txtConfirmationPhoneNumber + "]");

                PreparedStatement pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalRegistration"));
                pstmt.setInt(1, Integer.parseInt(this.siteId));
                pstmt.setString(2, this.txtRegistration);
                // RegistrationInd = -1
                // RegistrationDate = null
                pstmt.setString(3, this.pcTermUsername);
                pstmt.setString(4, this.pcTermPassword);
                // RegistrationIp = null
                // RegistrationAttemptNum = 0
                pstmt.setInt(5, (this.chkRegCode ? (-1) : 1)); // RegistrationIgnoreInd
                pstmt.setString(6, this.pcTermBrand); // CustomConfigId
                pstmt.setString(7, this.txtIPAddress); // RestrictToIpAddr
                pstmt.setString(8, this.txtSubnetMask); // RestrictToSubnetMask
                pstmt.setInt(9, (this.chkIPRestriction ? (-1) : 1)); // ipAddrIgnoreInd
                pstmt.setInt(10, (this.chkCertificate ? (-1) : 1)); // CertificateIgnoreInd
                pstmt.setInt(11, (this.chkAllowCertificate ? (-1) : 1)); // CertificateInstallAlowedInd
                pstmt.setInt(12, Integer.parseInt(this.txtAllowCertificate)); // CertificateInstallRemainNum
                pstmt.setTimestamp(13, this.reCalculateDateExpiration(context));
                pstmt.setInt(14, (this.chkConfirmationByUser ? (-1) : 1)); // RegistrationByUserIgnoreInd
/*            if(this.chkConfirmationByUser){
                pstmt.setInt(5, -1); // RegistrationIgnoreInd
            }*/
                pstmt.setInt(15, (this.chkConfirmationByEmail ? (-1) : 1)); // NotifyByEmailIgnoreInd
                pstmt.setInt(16, (this.chkConfirmationBySMS ? (-1) : 1)); // NotifyBySmsIgnoreInd
                pstmt.setString(17, this.txtConfirmationEMail); // NotificationEmail
                pstmt.setString(18, this.txtConfirmationPhoneNumber); // NotificationPhoneNumber
                pstmt.setBoolean(19, this.chkIsAdmin);

                rs = pstmt.executeQuery();
                int id = 0;
                if (rs != null && rs.next()) {
                    id = rs.getInt("newId");
                    this.trackTrmRegChanges(context, sessionData, id, "Adding Terminal Registration:");
                }
                pstmt.close();

                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalActivityLog"));
                // CurrentDate
                pstmt.setInt(1, Integer.parseInt(this.siteId));
                pstmt.setString(2, this.txtRegistration);
                pstmt.setString(3, this.pcTermUsername);
                pstmt.setString(4, request.getRemoteHost());
                pstmt.setString(5, request.getLocalAddr());
                pstmt.setString(6, request.getContextPath());
                // Class = SupportSite
                pstmt.setString(7, "addTerminal"); // FunctionName
                pstmt.setString(8, "ADMIN ADD: " + sUpdateInfo); // Description
                pstmt.setString(9, "OK"); // ErrorCode
                pstmt.setString(10, sUpdateInfo); // Error Message
                pstmt.setString(11, this.pcTermBrand); // CustomConfigId

                pstmt.executeUpdate();
                pstmt.close();

                dbConn.commit();
                dbConn.setAutoCommit(true);

                this.insertNewRegCodeInHistory(id, this.txtRegistration);
            }
        } catch (SQLException ex) {

            Terminal.cat.error("Error during addPCTerminalRegistration SQLException:", ex);

            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException sqex) {
                Terminal.cat.error("Error during rollback addPCTerminalRegistration ", sqex);
            }

            if (ex.getMessage().indexOf("UserData") > 0 && ex.getMessage().indexOf("pk_UserData") > 0) {
                this.addFieldError("terminal_registration", "EJBCA : " + Languages.getString("com.debisys.users.error_username_duplicate", sessionData.getLanguage()));
            } else {
                this.addFieldError("terminal_registration", Languages.getString("jsp.admin.terminal.general_add_reg_error", sessionData.getLanguage()));
            }
            //throw new TerminalException();

        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback addPCTerminalRegistration", ex);
            }

            Terminal.cat.error("Error during addPCTerminalRegistration", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    } // End of function addPCTerminalRegistration

    public void updatePCTerminalRegistration(ServletContext context, SessionData sessionData, HttpServletRequest request) throws TerminalException {
        Connection dbConn = null;
        int id = Integer.parseInt(request.getParameter("edit"));

        try {
            boolean isValidPassword = validatePasswordsTerminal(this.pcTermPassword, sessionData, context);
            if (isValidPassword) {
                String seeder = (String) context.getAttribute("secretSeed");
                SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
                this.pcTermPassword = cipherService.encrypt(this.pcTermPassword);
                dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

                if (dbConn == null) {
                    Terminal.cat.error("Can't get database connection");
                    throw new TerminalException();
                }

                dbConn.setAutoCommit(false);

                if (!this.chkManualRegCode) {
                    this.txtRegistration = this.siteId;
                }

                if (this.txtAllowCertificate.length() == 0) {
                    this.txtAllowCertificate = "0";
                }

                if (this.txtLoginErrorCount.length() == 0) {
                    this.txtLoginErrorCount = null;
                }

                String sUpdateInfo = "";
                sUpdateInfo = "[millennium_no=" + this.siteId + "] [RegistrationCode=" + this.txtRegistration + "] [RegistrationInd=-1] [RegistrationDate=null] ";
                sUpdateInfo += ("[UserId=" + this.pcTermUsername + "] [Password=" + this.pcTermPassword + "] [RegistrationIp=null] [RegistrationAttemptNum=0] ");
                sUpdateInfo += ("[RegistrationIgnoreInd=" + (this.chkRegCode ? "-1" : "1") + "] [CustomConfigId=" + this.pcTermBrand + "] ");
                sUpdateInfo += ("[RestrictTolpAddr=" + this.txtIPAddress + "] [RestrictToSubnetMask=" + this.txtSubnetMask + "] ");
                sUpdateInfo += ("[ipAddrIgnoreInd=" + (this.chkIPRestriction ? "-1" : "1") + "] [CertificateIgnoreInd=" + (this.chkCertificate ? "-1" : "1") + "] ");
                sUpdateInfo += ("[CertificateInstallAlowedInd=" + (this.chkAllowCertificate ? "-1" : "1") + "] [CertificateInstallRemainNum=" + this.txtAllowCertificate + "] [lastLoginAttemptNum=" + this.txtLoginErrorCount + "] [DisabledInd="
                        + this.ddlRecordStatus + "]");
                sUpdateInfo += ("[RegistrationByUserIgnoreInd=" + (this.chkConfirmationByUser ? "-1" : "1") + "] [NotifyByEmailIgnoreInd=" + (this.chkConfirmationByEmail ? "-1" : "1") + "]");
                sUpdateInfo += ("[NotifyBySmsIgnoreInd=" + (this.chkConfirmationBySMS ? "-1" : "1") + "] [NotificationEmail=" + this.txtConfirmationEMail + "]");
                sUpdateInfo += ("[NotificationPhoneNumber=" + this.txtConfirmationPhoneNumber + "]");

                String sqlUpdate = null;
                String currentTxtRegistration = request.getParameter("CurrentTxtRegistration");
                if (!this.txtRegistration.equals(currentTxtRegistration)) {
                    sqlUpdate = Terminal.sql_bundle.getString("updateTerminalRegistrationResetToken");
                } else {
                    sqlUpdate = Terminal.sql_bundle.getString("updateTerminalRegistration");
                }

                boolean existActually = this.existRegistrationCodePrincipalTable(context, sessionData, request);
                if (existActually) {
                    sqlUpdate = sqlUpdate.replace(",RegCodeExpirationDate = ?", "");
                }

                PreparedStatement pstmt = dbConn.prepareStatement(sqlUpdate);
                pstmt.setString(1, this.txtRegistration);
                // RegistrationInd = -1
                // RegistrationDate = null
                pstmt.setString(2, this.pcTermUsername);
                pstmt.setString(3, this.pcTermPassword);
                // RegistrationIp = null
                // RegistrationAttemptNum = 0
                pstmt.setInt(4, (this.chkRegCode ? (-1) : 1)); // RegistrationIgnoreInd
                pstmt.setString(5, this.pcTermBrand); // CustomConfigId
                pstmt.setString(6, this.txtIPAddress); // RestrictToIpAddr
                pstmt.setString(7, this.txtSubnetMask); // RestrictToSubnetMask
                pstmt.setInt(8, (this.chkIPRestriction ? (-1) : 1)); // ipAddrIgnoreInd
                pstmt.setInt(9, (this.chkCertificate ? (-1) : 1)); // CertificateIgnoreInd
                pstmt.setInt(10, (this.chkAllowCertificate ? (-1) : 1)); // CertificateInstallAlowedInd
                pstmt.setInt(11, Integer.parseInt(this.txtAllowCertificate)); // CertificateInstallRemainNum
                pstmt.setString(12, this.txtLoginErrorCount); // lastLoginAttemptNum
                pstmt.setString(13, (this.ddlRecordStatus.length() == 0) ? null : this.ddlRecordStatus); // DisabledInd
                pstmt.setInt(14, (this.chkConfirmationByUser ? (-1) : 1)); // RegistrationByUserIgnoreInd
/*            if(this.chkConfirmationByUser){
                pstmt.setInt(4, -1); // RegistrationIgnoreInd
            }*/
                pstmt.setInt(15, (this.chkConfirmationByEmail ? (-1) : 1)); // NotifyByEmailIgnoreInd
                pstmt.setInt(16, (this.chkConfirmationBySMS ? (-1) : 1)); // NotifyBySmsIgnoreInd
                pstmt.setString(17, this.txtConfirmationEMail); // NotificationEmail
                pstmt.setString(18, this.txtConfirmationPhoneNumber); // NotificationPhoneNumber
                pstmt.setBoolean(19, this.chkIsAdmin); // isAdmin            

                if (!existActually) {
                    pstmt.setTimestamp(20, this.reCalculateDateExpiration(context)); // ID
                    pstmt.setInt(21, id); // ID                                
                } else {
                    pstmt.setInt(20, id); // ID                                
                }

                pstmt.executeUpdate();
                pstmt.close();

                pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalActivityLog"));
                // CurrentDate
                pstmt.setInt(1, Integer.parseInt(this.siteId));
                pstmt.setString(2, this.txtRegistration);
                pstmt.setString(3, this.pcTermUsername);
                pstmt.setString(4, request.getRemoteHost());
                pstmt.setString(5, request.getLocalAddr());
                pstmt.setString(6, request.getContextPath());
                // Class = SupportSite
                pstmt.setString(7, "updateTerminal"); // FunctionName
                pstmt.setString(8, "ADMIN UPDATE: " + sUpdateInfo); // Description
                pstmt.setString(9, "OK"); // ErrorCode
                pstmt.setString(10, sUpdateInfo); // Error Message
                pstmt.setString(11, this.pcTermBrand); // CustomConfigId

                pstmt.executeUpdate();
                pstmt.close();

                if (this.resetAnswers) {
                    pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("resetAnswers"));
                    pstmt.setInt(1, id);
                    pstmt.executeUpdate();
                    pstmt.close();
                }
                dbConn.commit();
                dbConn.setAutoCommit(true);

                this.trackTrmRegChanges(context, sessionData, id, "Updating Terminal Registration:");

                if (!existActually) {
                    this.updateRegCodeHistory(context, sessionData, request);
                }
            }
        } catch (SQLException ex) {
            Terminal.cat.error("Error during updatePCTerminalRegistration SQLException:", ex);

            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException sqex) {
                Terminal.cat.error("Error during rollback updatePCTerminalRegistration", sqex);
            }

            if (ex.getMessage().indexOf("UserData") > 0 && ex.getMessage().indexOf("pk_UserData") > 0) {
                // this error comes from EJBCA DataBase other instance of the
                // SQLServer
                this.addFieldError("terminal_registration", "EJBCA : " + Languages.getString("com.debisys.users.error_username_duplicate", sessionData.getLanguage()));
            } else {
                this.addFieldError("terminal_registration", Languages.getString("jsp.admin.terminal.general_update_error", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback updatePCTerminalRegistration", ex);
            }

            Terminal.cat.error("Error during updatePCTerminalRegistration", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    } // End of function updatePCTerminalRegistration

    /* END Release 21 - Added by LF */
    private Timestamp reCalculateDateExpiration(ServletContext context) {
        int quantityDays = Integer.parseInt(DebisysConfigListener.getDaysBeforeRegistrationCodeExpires(context));
        Calendar regCodeExpirationDate = Calendar.getInstance();
        regCodeExpirationDate.add(Calendar.DAY_OF_MONTH, quantityDays);
        java.util.Date newDate = regCodeExpirationDate.getTime();
        Timestamp timestamp = new Timestamp(newDate.getTime());
        return timestamp;
    }

    /**
     * Verify if exist el regCode in the history
     */
    public boolean existRegistrationCode(ServletContext context, SessionData sessionData, HttpServletRequest request) {
        Connection dbConn = null;
        boolean result = false;
        PreparedStatement pstmt = null;

        String edit = request.getParameter("edit");

        if (edit == null || edit.trim().equals("")) {
            return false;
        }

        int id = Integer.parseInt(edit);

        if (!this.chkManualRegCode) {
            this.txtRegistration = this.siteId;
        }
        /*if(this.chkConfirmationByUser){
            this.txtRegistration = StringUtil.randomString(StringUtil.CHARSET_AZ_09, 10);
        }*/

        if (this.existRegistrationCodePrincipalTable(context, sessionData, request)) {
            return false;
        }

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String sql = Terminal.sql_bundle.getString("getCountTerminalRegistrationRegCodeLog");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.setString(2, this.txtRegistration);
            //System.out.println("QUERY: "+sql+"  "+id+"   "+this.txtRegistration);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                int quantity = rs.getInt(1);
                if (quantity > 0) {
                    result = true;
                }
            }
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during release connection Terminal_Registration_RegCode_Log ", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);

            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    Terminal.cat.error("Error during close PreparedStatement", e);
                }
            }
        }
        return result;
    }

    public boolean existRegistrationCodePrincipalTable(ServletContext context, SessionData sessionData, HttpServletRequest request) {
        Connection dbConn = null;
        boolean result = false;
        PreparedStatement pstmt = null;
        String edit = request.getParameter("edit");

        if (edit == null || edit.trim().equals("")) {
            return false;
        }

        int id = Integer.parseInt(edit);

        if (!this.chkManualRegCode) {
            this.txtRegistration = this.siteId;
        }
        /*        if(this.chkConfirmationByUser){
            this.txtRegistration = StringUtil.randomString(StringUtil.CHARSET_AZ_09, 10);
        }*/

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String sql = Terminal.sql_bundle.getString("getTerminalRegistrationByIdAndRegCode");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.setString(2, this.txtRegistration);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                int quantity = rs.getInt(1);
                if (quantity > 0) {
                    result = true;
                }
            }
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during release connection Terminal_Registration_RegCode_Log ", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    Terminal.cat.error("Error during close PreparedStatement", e);
                }
            }
        }
        return result;
    }

    private void updateRegCodeHistory(ServletContext context, SessionData sessionData, HttpServletRequest request) {

        Connection dbConn = null;
        boolean result = false;
        PreparedStatement pstmt = null;
        int id = Integer.parseInt(request.getParameter("edit"));

        if (!this.chkManualRegCode) {
            this.txtRegistration = this.siteId;
        }
        /*        if(this.chkConfirmationByUser){
            this.txtRegistration = StringUtil.randomString(StringUtil.CHARSET_AZ_09, 10);
        }*/

        int quantityRegCodes = Integer.parseInt(DebisysConfigListener.getQuantityRegistrationCodes(context));

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String sql = Terminal.sql_bundle.getString("getAllTerminalRegistrationRegCodeLog");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, id);

            ResultSet rs = pstmt.executeQuery();
            Vector<TerminalRegistrationRegCodeLog> vec = new Vector<TerminalRegistrationRegCodeLog>();
            int count = 1;

            while (rs.next()) {
                int idR = rs.getInt(1);
                String registrationCode = rs.getString(2);
                java.sql.Date regCodeDate = rs.getDate(3);

                if (count >= quantityRegCodes) {
                    TerminalRegistrationRegCodeLog regCodeLog = new TerminalRegistrationRegCodeLog();
                    regCodeLog.setId(idR);
                    regCodeLog.setRegistrationCode(registrationCode);
                    regCodeLog.setRegCodeDate(regCodeDate);
                    vec.add(regCodeLog);
                }
                count++;
            }

            if (vec.size() > 0) {
                this.deleteOldsRegCodesHistory(id, vec);
            }
            this.insertNewRegCodeInHistory(id, this.txtRegistration);

        } catch (Exception e) {
            Terminal.cat.error("Error during release connection Terminal_Registration_RegCode_Log ", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    Terminal.cat.error("Error during close PreparedStatement", e);
                }
            }
        }
    }

    private boolean insertNewRegCodeInHistory(int id, String txtRegistration) {
        boolean response = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            Calendar regCodeExpirationDate = Calendar.getInstance();
            java.util.Date newDate = regCodeExpirationDate.getTime();
            Timestamp timestamp = new Timestamp(newDate.getTime());
            String sql = "INSERT INTO Terminal_Registration_RegCode_Log (id,registrationCode,RegCodeDate) VALUES (?,?,?)";
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.setString(2, txtRegistration);
            pstmt.setTimestamp(3, timestamp);
            pstmt.executeUpdate();
            response = true;
        } catch (Exception e) {
            //cat.error("Error insert into Terminal_Registration_RegCode_Log (Insert rows history)", e);
        } finally {
            try {
                dbConn.close();
            } catch (Exception e) {
                Terminal.cat.error("Error closing the connection", e);
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    Terminal.cat.error("Error closing the PreparedStatement", e);
                }
            }
            return response;
        }
    }

    private List getTerminalRegCodeLogById(Integer id) {
        List<String> passwords = new ArrayList<String>();
        ResultSet result = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn != null) {
                pstmt = dbConn.prepareStatement(GET_TERMINAL_REGISTRATION_BY_ID);
                pstmt.setInt(1, id);
                result = pstmt.executeQuery();
                while (result.next()) {
                    passwords.add(result.getString("RegistrationCode"));
                }

            } else {
                Terminal.cat.info("getTerminalRegCodeLogById - CONNECTION NOT FOUND ");
            }
        } catch (Exception e) {
            Terminal.cat.error("getTerminalRegCodeLogById - ERROR RETRIEVING FIRST 12 TERMINALS FROM Terminal_Registration_RegCode_Log BY ID ", e);
        } finally {
            if (dbConn != null) {
                try {
                    dbConn.close();
                } catch (Exception e) {
                    Terminal.cat.error("closeConnection - ERROR CLOSING CONNECTION ", e);
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception e) {
                    Terminal.cat.error("closeStatement - ERROR CLOSING STATEMENT ", e);
                }
            }
            return passwords;
        }
    }

    private void deleteOldsRegCodesHistory(int id, Vector<TerminalRegistrationRegCodeLog> vec) {

        Connection dbConn = null;
        PreparedStatement pstmt = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }
            String listRegcodes = " ( ";
            for (TerminalRegistrationRegCodeLog regCodeLog : vec) {
                listRegcodes += "'" + regCodeLog.getRegistrationCode() + "',";
            }
            listRegcodes = listRegcodes.substring(0, listRegcodes.length() - 1);
            listRegcodes += " ) ";

            String sql = Terminal.sql_bundle.getString("deleteTerminalRegistrationRegCodeLog") + listRegcodes;
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, id);
            int resp = pstmt.executeUpdate();
        } catch (Exception e) {
            Terminal.cat.error("Error during release connection Terminal_Registration_RegCode_Log (Deleting rows history)", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    Terminal.cat.error("Error during close PreparedStatement", e);
                }
            }
        }

    }

    // JFM 26-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE - Metodo para
    // asignar
    // un terminal fisico
    public void assignPhysicalTerminal(String strRefId, SessionData sessionData) throws TerminalException {
        // Actualizar la base de datos
        Connection dbConn = null;
        this.validationErrors.clear();

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            PreparedStatement pstmt;
            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("assignPhysicalTerminal"));
            pstmt.setLong(1, this.terminalFisico.getTerminalId());
            pstmt.setString(2, this.terminalFisico.getSerialNumber());
            pstmt.setString(3, this.getMerchantId());
            pstmt.setDouble(4, Double.parseDouble(this.siteId));

            pstmt.executeUpdate();
            pstmt.close();

            // Cambiar el estado de la terminal fisica, pasa a asignada
            CTerminalStatus assignedStatus = CTerminalStatus.getTerminalStatusByCode(CTerminalStatus.ASSIGN_CODE);

            if (this.terminalFisico.changeStatus(assignedStatus.getTerminalSatusId())) {
                CPhysicalTerminal.insertTerminalStatusChange(this.terminalFisico.getTerminalId(), Calendar.getInstance(), assignedStatus.getTerminalSatusId(), strRefId, Languages
                        .getString("jsp.admin.transactions.inventoryTerminals.PhisicalTerminalAssign", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback assignPhysicalTerminal", ex);
            }

            Terminal.cat.error("Error during assignPhysicalTerminal", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     *
     * @param mailHost
     * @param accessLevel
     * @param refId
     * @param companyName
     * @param userName
     * @param dba
     * @param contactEmail
     * @param monthlyFee
     * @param sClass
     * @param sMails
     * @param PCTerm
     * @param URL
     * @param TerminalDesc
     * @param showTerminalClass
     * @param urlDownCert
     * @param isnew
     * @param isJavaHandSetTerm
     * @param isSmartPhoneAndroid
     * @param isSmartPhoneIPhone
     * @param isVerifone
     * @param instance
     * @param sSerialNumberterm
     * @param context
     * @throws Exception
     */
    public void sendMailNotification(String mailHost, String accessLevel, String refId, String companyName, String userName, String dba, String contactEmail,
            String monthlyFee, String sClass, String sMails, Boolean PCTerm, String URL, String TerminalDesc,
            Boolean showTerminalClass, String urlDownCert, boolean isnew,
            boolean isJavaHandSetTerm, boolean isSmartPhoneAndroid, boolean isSmartPhoneIPhone, boolean isVerifone, String instance,
            String sSerialNumberterm, ServletContext context) throws Exception {

        String styleEmail = "BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;";

        String sHeader = "<body bgcolor=\"#ffffff\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #f1f9fe;width:900px;\">";
        if (!isnew) {
            if (urlDownCert != null && !urlDownCert.equals("") && (!isJavaHandSetTerm && !isSmartPhoneAndroid && !isSmartPhoneIPhone && !isVerifone)) {
                sHeader += "<tr><td><b style=\"color:red;\">You have chosen to update your terminal to the newest version: PC Term 2.0.  To update your existing PC terminal you must download your security certificate first! Please go to the URL in the PC Terminal information below to download your certificate.</b><br><br></td></tr>";
            }
        } else {
            if (urlDownCert != null && !urlDownCert.equals("") && (!isJavaHandSetTerm && !isSmartPhoneAndroid && !isSmartPhoneIPhone && !isVerifone)) {
                sHeader += "<tr><td><b style=\"color:red;\">Download Security Certificate First! Please Review PC Terminal Information Below.</b></td></tr>";
            }
        }

        String tit1 = "New Terminal Added";
        String tit2 = "Added";
        String sTerminal = "";
        String sPCTerm = "";
        String sLogin = "";
        String sFooter = "</table></body>";
        if (!isnew) {
            tit1 = "Terminal updated";
            tit2 = "Updated";
            sHeader += "<tr><td><b>" + tit1 + ": [_SITEID_]</b></td></tr><tr><td><b>" + tit2 + " by _ACTOR_: [_USERNAME_] from [_COMPANYNAME_]</b></td></tr>";

            if (!isJavaHandSetTerm && !isSmartPhoneAndroid && !isSmartPhoneIPhone && !isVerifone) {
                sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>PC Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"" + styleEmail + "\">";
            } else {
                sPCTerm = "<table cellpadding=\"0\" cellspacing=\"0\" style=\"" + styleEmail + "\">";
            }

            if (urlDownCert != null && !urlDownCert.equals("") && (!isJavaHandSetTerm && !isSmartPhoneAndroid && !isSmartPhoneIPhone && !isVerifone)) {
                sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b style=\"color:red;\">REQUIRED:<BR> Download Security Certificate First!</b></td><td></td><td style=\"font-size: 11pt;\" nowrap valign=\"top\"><a href=\"" + urlDownCert + "\">"
                        + urlDownCert + "</td><td></td><td></td><td></td><td></td></tr><tr><td colspan=\"7\">&nbsp;</td></tr>";
            }
            sPCTerm += "</table></td></tr>";

        } else {
            sHeader += "<tr><td><b>" + tit1 + ": [_SITEID_]</b></td></tr><tr><td><b>" + tit2 + " by _ACTOR_: [_USERNAME_] from [_COMPANYNAME_]</b></td></tr>";
            sTerminal = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"" + styleEmail + "\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Merchant Name:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_DBA_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Email:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_EMAIL_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Terminal Type:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_TYPE_</td><td></td>"
                    + (showTerminalClass ? "<td style=\"font-size: 11pt;\" nowrap><b>Terminal Class:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CLASS_</td></tr>" : "")
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Terminal Version:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_VERSION_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Monthly Fee:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_FEE_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Serial Number:</b></td><td></td><td style=\"font-size: 11pt;\" colspan=5>_SERIAL_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Notes:</b></td><td></td><td style=\"font-size: 11pt;\" colspan=5>_NOTES_</td></tr>"
                    + "</table></td></tr>";
            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>PC Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"" + styleEmail + "\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Brand:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_BRAND_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Registration Code:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_REGCODE_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>UserName:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMUSERNAME_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Password:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMPASSWORD_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Enforce IP Restriction? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPRESTRICTION_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>IP Address:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPADDRESS_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Subnet Mask:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_SUBNETMASK_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Enforce Certificate? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CERTIFICATE_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Allow Certificate Install? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CERTIFICATEINSTALL_</td><td></td><td style=\"font-size: 11pt;\"><b>Certificate Install Remaining Amount:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CERTINSTALLAMOUNT_</td></tr>";
            if (urlDownCert != null && !urlDownCert.equals("") && (!isJavaHandSetTerm && !isSmartPhoneAndroid && !isSmartPhoneIPhone && !isVerifone)) {
                sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b style=\"color:red;\">REQUIRED: <BR>Download Security Certificate First!</b>&nbsp;</td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap valign=\"top\"><a href=\"" + urlDownCert
                        + "\">" + urlDownCert + "</td><td></td><td></td><td></td><td></td></tr><tr><td colspan=\"7\">&nbsp;</td></tr>";
            }
            sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b>Enforce Registration by User Confirmation? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_USERCONFIRMATION_</td></tr>";
            sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b>Notification EMail:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_NOTIFICATIONEMAIL_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Notification Phone Number:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_NOTIFICATIONPHONENUMBER_</td></tr><tr>";
            sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b>PC Term URL :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_URL_</td></tr></table></td></tr>";
            sLogin = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Login Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"" + styleEmail + "\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Clerk Name:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CLERKNAME_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Clerk Code:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CLERKCODE_</td></tr></table></td></tr>";

        }
        if (!PCTerm) {
            sPCTerm = "";
        }

        //Info for WebService implementation
        if (!PCTerm && (TerminalDesc.indexOf("Web Service") > 0 || TerminalDesc.indexOf("23") > 0 || TerminalDesc.indexOf("79") > 0)) {
            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>WS Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"" + styleEmail + "\"><tr><td style=\"font-size: 11pt;\" nowrap><b>UserName:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMUSERNAME_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Password:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMPASSWORD_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Enforce IP Restriction? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPRESTRICTION_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>IP Address:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPADDRESS_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Subnet Mask:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_SUBNETMASK_</td></tr>";
            sPCTerm += "</table></td></tr>";

        }
        //End WS
        StringBuilder infoJavaHandset = new StringBuilder();
        if (isJavaHandSetTerm || isSmartPhoneAndroid || isSmartPhoneIPhone) {
            String downloadURL = "";
            if (isJavaHandSetTerm) {
                downloadURL = Properties.getPropertyValue(instance, "global", "urlDownloadMobileNokiaS40");
            } else if (isSmartPhoneAndroid) {
                downloadURL = Properties.getPropertyValue(instance, "global", "urlDownloadSmartPhoneAndroid");
            } else if (isSmartPhoneIPhone) {
                downloadURL = Properties.getPropertyValue(instance, "global", "urlDownloadSmartPhoneIPhone");
            }

            infoJavaHandset.append("<tr>");
            if (isJavaHandSetTerm) {
                if (!this.terminalType.equals("106")) {
                    infoJavaHandset.append("<td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Nokia S40 Information</td>");
                }
                //else
                //{
                //    infoJavaHandset.append("<td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Nokia S40 Information</td>");
                //}

            } else if (isSmartPhoneAndroid) {
                infoJavaHandset.append("<td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>SmartPhone Android Information</td>");
            } else if (isSmartPhoneIPhone) {
                infoJavaHandset.append("<td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>SmartPhone IPhone Information</td>");
            }

            infoJavaHandset.append("</tr>");
            infoJavaHandset.append("<tr>");
            infoJavaHandset.append("<td align=center>");
            if (!this.terminalType.equals("106")) {
                infoJavaHandset.append("<table cellpadding=\"0\" cellspacing=\"0\" style=\"" + styleEmail + "\">");
                infoJavaHandset.append("<tr>");
                infoJavaHandset.append(" <td style=\"font-size: 11pt;\" nowrap><b>Url Download Application:</b></td>");
                infoJavaHandset.append(" <td>&nbsp;</td>");
                infoJavaHandset.append(" <td style=\"font-size: 11pt;\" nowrap>" + downloadURL + "</td>");
                infoJavaHandset.append(" <td>&nbsp;&nbsp;</td>");
                infoJavaHandset.append(" <td style=\"font-size: 11pt;\" nowrap><b>Registration Code:</b></td>");
                infoJavaHandset.append(" <td>&nbsp;</td>");
                infoJavaHandset.append(" <td style=\"font-size: 11pt;\" nowrap>" + StringUtil.toString(this.txtRegistration) + "</td>");
                infoJavaHandset.append("</tr>");
                infoJavaHandset.append("</table>");
            }

            infoJavaHandset.append("</td>");
            infoJavaHandset.append("</tr>");
        }

        sHeader = sHeader.replaceAll("_SITEID_", this.siteId).replaceAll("_USERNAME_", userName).replaceAll("_COMPANYNAME_", this.cleanReplace(companyName));
        if (accessLevel.equals(DebisysConstants.ISO)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "ISO");
        } else if (accessLevel.equals(DebisysConstants.AGENT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "AGENT");
        } else if (accessLevel.equals(DebisysConstants.SUBAGENT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "SUBAGENT");
        } else if (accessLevel.equals(DebisysConstants.REP)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "REP");
        } else if (accessLevel.equals(DebisysConstants.MERCHANT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "MERCHANT");
        }

        sTerminal = sTerminal.replaceAll("_DBA_", this.cleanReplace(dba));
        sTerminal = sTerminal.replaceAll("_EMAIL_", contactEmail);
        sTerminal = sTerminal.replaceAll("_CLASS_", (showTerminalClass ? (sClass == null) ? "" : sClass : ""));
        sTerminal = sTerminal.replaceAll("_TYPE_", TerminalDesc);
        sTerminal = sTerminal.replaceAll("_VERSION_", StringUtil.toString(this.termVersion));
        sTerminal = sTerminal.replaceAll("_FEE_", monthlyFee);
        sTerminal = sTerminal.replaceAll("_NOTES_", this.cleanReplace(StringUtil.toString(this.termNote)));
        sTerminal = sTerminal.replaceAll("_SERIAL_", sSerialNumberterm);
                
        sPCTerm = sPCTerm.replaceAll("_BRAND_", StringUtil.toString(this.pcTermBrand));
        sPCTerm = sPCTerm.replaceAll("_REGCODE_", StringUtil.toString(this.txtRegistration));
        sPCTerm = sPCTerm.replaceAll("_PCTERMUSERNAME_", StringUtil.toString(this.pcTermUsername));
        sPCTerm = sPCTerm.replaceAll("_PCTERMPASSWORD_", decodePassword(context));
        sPCTerm = sPCTerm.replaceAll("_IPRESTRICTION_", (this.chkIPRestriction) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_IPADDRESS_", StringUtil.toString(this.txtIPAddress));
        sPCTerm = sPCTerm.replaceAll("_SUBNETMASK_", StringUtil.toString(this.txtSubnetMask));
        sPCTerm = sPCTerm.replaceAll("_CERTIFICATE_", (this.chkCertificate) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_CERTIFICATEINSTALL_", (this.chkAllowCertificate) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_CERTINSTALLAMOUNT_", StringUtil.toString(this.txtAllowCertificate));
        sPCTerm = sPCTerm.replaceAll("_USERCONFIRMATION_", (this.chkConfirmationByUser) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_NOTIFICATIONEMAIL_", StringUtil.toString(this.txtConfirmationEMail));
        sPCTerm = sPCTerm.replaceAll("_NOTIFICATIONPHONENUMBER_", StringUtil.toString(this.txtConfirmationPhoneNumber));

        if (URL != null) {
            sPCTerm = sPCTerm.replaceAll("_URL_", URL.replaceAll(" ", "%20"));
        }

        sLogin = sLogin.replaceAll("_CLERKNAME_", StringUtil.toString(this.clerkName) + " " + StringUtil.toString(this.clerkLastName)).replaceAll("_CLERKCODE_", StringUtil.toString(this.clerkCode));

        java.util.Properties props;
        props = System.getProperties();
        props.put("mail.smtp.host", mailHost);
        Session s = Session.getDefaultInstance(props, null);

        MimeMessage message = new MimeMessage(s);
        message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
        message.setFrom(new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
        if (sMails != null) {
            message.setRecipients(Message.RecipientType.CC, sMails.replaceAll(";", ","));
        }

        if (!isnew) {
            message.setSubject("Terminal Updated - Site Id: [" + this.siteId + "]");
        } else {
            message.setSubject("New Terminal Added - Site Id: [" + this.siteId + "]");
        }

        message.setContent(sHeader + sTerminal + sPCTerm + infoJavaHandset.toString() + sLogin + sFooter, "text/html");
        Transport.send(message);
    }// End of function sendMailNotification

    private String cleanReplace(String sData) {
        String sResult = sData;
        sResult = sResult.replace('$', ' ');

        return sResult;
    }

    /**
     *
     * @param mailHost
     * @param accessLevel
     * @param refId
     * @param companyName
     * @param userName
     * @param dba
     * @param contactEmail
     * @param monthlyFee
     * @param sClass
     * @param sMails
     * @param PCTerm
     * @param URL
     * @param TerminalDesc
     * @param showTerminalClass
     * @param urlDownCert
     * @param isnew
     * @param context
     * @throws Exception
     */
    public void sendMailNotificationUpdatePCTerm(String mailHost, String accessLevel, String refId, String companyName, String userName,
            String dba, String contactEmail, String monthlyFee, String sClass, String sMails, Boolean PCTerm, String URL,
            String TerminalDesc, Boolean showTerminalClass, String urlDownCert, boolean isnew, ServletContext context) throws Exception {
        String sHeader = "<body bgcolor=\"#ffffff\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #f1f9fe;width:900px;\">";
        if (!isnew) {
            if (urlDownCert != null && !urlDownCert.equals("")) {
                sHeader += "<tr><td><b style=\"color:red;\">You have chosen to update your terminal to the newest version: PC Term 2.0.  To update your existing PC terminal you must download your security certificate first! Please go to the URL in the PC Terminal information below to download your certificate.</b><br><br></td></tr>";
            }
        } else {
            if (urlDownCert != null && !urlDownCert.equals("")) {
                sHeader += "<tr><td><b style=\"color:red;\">Download Security Certificate First! Please Review Edited PC Terminal Information Below.</b></td></tr>";
            } else {
                sHeader += "<tr><td><b style=\"color:red;\">Please Review Edited PC Terminal Information Below.</b></td></tr>";
            }
        }

        String tit1 = "Terminal Edited";
        String tit2 = "Edited";
        String sTerminal = "";
        String sPCTerm = "";
        String sLogin = "";
        String sFooter = "</table></body>";
        if (!isnew) {
            tit1 = "Terminal Edited";
            tit2 = "Edited";
            sHeader += "<tr><td><b>" + tit1 + ": [_SITEID_]</b></td></tr><tr><td><b>" + tit2 + " by _ACTOR_: [_USERNAME_] from [_COMPANYNAME_]</b></td></tr>";

            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>PC Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\">";
            if (urlDownCert != null && !urlDownCert.equals("")) {
                sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b style=\"color:red;\">REQUIRED:<BR> Download Security Certificate First!</b></td><td></td><td style=\"font-size: 11pt;\" nowrap valign=\"top\"><a href=\"" + urlDownCert + "\">"
                        + urlDownCert + "</td><td></td><td></td><td></td><td></td></tr><tr><td colspan=\"7\">&nbsp;</td></tr>";
            }
            sPCTerm += "</table></td></tr>";

        } else {
            sHeader += "<tr><td><b>" + tit1 + ": [_SITEID_]</b></td></tr><tr><td><b>" + tit2 + " by _ACTOR_: [_USERNAME_] from [_COMPANYNAME_]</b></td></tr>";
            sTerminal = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Merchant Name:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_DBA_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Email:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_EMAIL_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Terminal Type:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_TYPE_</td><td></td>"
                    + (showTerminalClass ? "<td style=\"font-size: 11pt;\" nowrap><b>Terminal Class:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CLASS_</td></tr>" : "")
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>Terminal Version:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_VERSION_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Monthly Fee:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_FEE_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Serial Number:</b></td><td></td><td style=\"font-size: 11pt;\" colspan=5>_SERIAL_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Notes:</b></td><td></td><td style=\"font-size: 11pt;\" colspan=5>_NOTES_</td>"
                    + "</table></td></tr>";
            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>PC Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Brand:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_BRAND_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Registration Code:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_REGCODE_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>UserName:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMUSERNAME_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Password:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMPASSWORD_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Enforce IP Restriction? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPRESTRICTION_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>IP Address:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPADDRESS_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Subnet Mask:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_SUBNETMASK_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Enforce Certificate? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CERTIFICATE_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Allow Certificate Install? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CERTIFICATEINSTALL_</td><td></td><td style=\"font-size: 11pt;\"><b>Certificate Install Remaining Amount:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_CERTINSTALLAMOUNT_</td></tr>";
            if (urlDownCert != null && !urlDownCert.equals("")) {
                sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b style=\"color:red;\">REQUIRED: <BR>Download Security Certificate First!</b>&nbsp;</td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap valign=\"top\"><a href=\"" + urlDownCert
                        + "\">" + urlDownCert + "</td><td></td><td></td><td></td><td></td></tr><tr><td colspan=\"7\">&nbsp;</td></tr>";
            }
            sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b>PC Term URL :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_URL_</td></tr></table></td></tr>";
            sLogin = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Login Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Clerk Name:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CLERKNAME_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Clerk Code:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CLERKCODE_</td></tr></table></td></tr>";

        }
        if (!PCTerm) {
            sPCTerm = "";
        }

        //Info for WebService implementation
        if (!PCTerm && (TerminalDesc.indexOf("Web Service") > 0 || TerminalDesc.indexOf("23") > 0 || TerminalDesc.indexOf("79") > 0)) {
            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>WS Terminal Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>UserName:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMUSERNAME_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Password:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_PCTERMPASSWORD_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Enforce IP Restriction? :</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPRESTRICTION_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>IP Address:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_IPADDRESS_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Subnet Mask:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_SUBNETMASK_</td></tr>";
            sPCTerm += "</table></td></tr>";

        }
        //End WS

        sHeader = sHeader.replaceAll("_SITEID_", this.siteId).replaceAll("_USERNAME_", userName).replaceAll("_COMPANYNAME_", this.cleanReplace(companyName));
        if (accessLevel.equals(DebisysConstants.ISO)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "ISO");
        } else if (accessLevel.equals(DebisysConstants.AGENT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "AGENT");
        } else if (accessLevel.equals(DebisysConstants.SUBAGENT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "SUBAGENT");
        } else if (accessLevel.equals(DebisysConstants.REP)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "REP");
        } else if (accessLevel.equals(DebisysConstants.MERCHANT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "MERCHANT");
        }

        sTerminal = sTerminal.replaceAll("_DBA_", this.cleanReplace(dba));
        sTerminal = sTerminal.replaceAll("_EMAIL_", contactEmail);
        sTerminal = sTerminal.replaceAll("_CLASS_", (showTerminalClass ? (sClass == null) ? "" : sClass : ""));
        sTerminal = sTerminal.replaceAll("_TYPE_", TerminalDesc);
        sTerminal = sTerminal.replaceAll("_VERSION_", StringUtil.toString(this.termVersion));
        sTerminal = sTerminal.replaceAll("_FEE_", monthlyFee);
        sTerminal = sTerminal.replaceAll("_NOTES_", this.cleanReplace(StringUtil.toString(this.termNote)));

        sPCTerm = sPCTerm.replaceAll("_BRAND_", StringUtil.toString(this.pcTermBrand));
        sPCTerm = sPCTerm.replaceAll("_REGCODE_", StringUtil.toString(this.txtRegistration));
        sPCTerm = sPCTerm.replaceAll("_PCTERMUSERNAME_", StringUtil.toString(this.pcTermUsername));
        sPCTerm = sPCTerm.replaceAll("_PCTERMPASSWORD_", StringUtil.toString(this.pcTermPassword));
        sPCTerm = sPCTerm.replaceAll("_IPRESTRICTION_", (this.chkIPRestriction) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_IPADDRESS_", StringUtil.toString(this.txtIPAddress));
        sPCTerm = sPCTerm.replaceAll("_SUBNETMASK_", StringUtil.toString(this.txtSubnetMask));
        sPCTerm = sPCTerm.replaceAll("_CERTIFICATE_", (this.chkCertificate) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_CERTIFICATEINSTALL_", (this.chkAllowCertificate) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_CERTINSTALLAMOUNT_", StringUtil.toString(this.txtAllowCertificate));

        if (URL != null) {
            sPCTerm = sPCTerm.replaceAll("_URL_", URL.replaceAll(" ", "%20"));
        }

        sLogin = sLogin.replaceAll("_CLERKNAME_", StringUtil.toString(this.clerkName) + " " + StringUtil.toString(this.clerkLastName)).replaceAll("_CLERKCODE_", StringUtil.toString(this.clerkCode));

        java.util.Properties props;
        props = System.getProperties();
        props.put("mail.smtp.host", mailHost);
        Session s = Session.getDefaultInstance(props, null);

        MimeMessage message = new MimeMessage(s);
        message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
        message.setFrom(new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
        if (sMails != null) {
            message.setRecipients(Message.RecipientType.CC, sMails.replaceAll(";", ","));
        }

        if (!isnew) {
            message.setSubject("Terminal Edited - Site Id: [" + this.siteId + "]");
        } else {
            message.setSubject("Terminal Edited - Site Id: [" + this.siteId + "]");
        }

        message.setContent(sHeader + sTerminal + sPCTerm + sLogin + sFooter, "text/html");
        Transport.send(message);
    }

    public void sendMailNotificationUpdatePCTerm1(String mailHost, String accessLevel, String refId, String companyName, String userName,
            String dba, String contactEmail, String monthlyFee, String sClass, String sMails, Boolean PCTerm,
            String URL,
            String TerminalDesc, Boolean showTerminalClass, String urlDownCert, boolean isnew,
            ServletContext context, String languageSession) throws Exception {
        String language = "1";
        if (languageSession.equals("spanish")) {
            language = "2";
        }
        HashMap<String, PropertiesByFeature> propertiesByFeature = PropertiesByFeature.findPropertiesByFeatureHash("SupportSiteEmailTerminalNotification");

        String mustDownloadCertificate = "You have chosen to update your terminal to the newest version: PC Term 2.0. "
                + "To update your existing PC terminal you must download your security certificate first! "
                + "Please go to the URL in the PC Terminal information below to download your certificate.";

        {
            if (propertiesByFeature.get("MUST_DONWLOAD_CERTIFICATE_" + language) != null) {
                mustDownloadCertificate = propertiesByFeature.get("MUST_DONWLOAD_CERTIFICATE_" + language).getValue();
            }
        }
        String downloadCertificateFirst = "Download Security Certificate First! Please Review Edited PC Terminal Information Below.";
        {
            if (propertiesByFeature.get("DONWLOAD_CERTIFICATE_FIRST_" + language) != null) {
                downloadCertificateFirst = propertiesByFeature.get("DONWLOAD_CERTIFICATE_FIRST_" + language).getValue();
            }
        }
        String pleaseReview = "Please Review Edited PC Terminal Information Below.";
        {
            if (propertiesByFeature.get("PLEASE_REVIEW_" + language) != null) {
                pleaseReview = propertiesByFeature.get("PLEASE_REVIEW_" + language).getValue();
            }
        }
        String terminalEdited = "Terminal Edited";
        {
            if (propertiesByFeature.get("TERMINAL_EDITED_" + language) != null) {
                terminalEdited = propertiesByFeature.get("TERMINAL_EDITED_" + language).getValue();
            }
        }
        String edited = "Edited ";
        {
            if (propertiesByFeature.get("EDITED_" + language) != null) {
                edited = propertiesByFeature.get("EDITED_" + language).getValue();
            }
        }
        String by = "by";
        {
            if (propertiesByFeature.get("BY_" + language) != null) {
                by = propertiesByFeature.get("BY_" + language).getValue();
            }
        }
        String from = "from";
        {
            if (propertiesByFeature.get("FROM_" + language) != null) {
                from = propertiesByFeature.get("FROM_" + language).getValue();
            }
        }
        String merchantNameLabel = "Merchant Name :";
        {
            if (propertiesByFeature.get("MERCHANT_NAME_" + language) != null) {
                merchantNameLabel = propertiesByFeature.get("MERCHANT_NAME_" + language).getValue();
            }
        }
        String terminalTypeLabel = "Terminal Type :";
        {
            if (propertiesByFeature.get("TERMINAL_TYPE_" + language) != null) {
                terminalTypeLabel = propertiesByFeature.get("TERMINAL_TYPE_" + language).getValue();
            }
        }
        String terminalClassLabel = "Terminal Class :";
        {
            if (propertiesByFeature.get("TERMINAL_CLASS_" + language) != null) {
                terminalClassLabel = propertiesByFeature.get("TERMINAL_CLASS_" + language).getValue();
            }
        }
        String terminalVersion = "Terminal Version :";
        {
            if (propertiesByFeature.get("TERMINAL_VERSION_" + language) != null) {
                terminalVersion = propertiesByFeature.get("TERMINAL_VERSION_" + language).getValue();
            }
        }
        String monthlyFeeLabel = "Monthly Fee :";
        {
            if (propertiesByFeature.get("MONTHLY_FEE_" + language) != null) {
                monthlyFeeLabel = propertiesByFeature.get("MONTHLY_FEE_" + language).getValue();
            }
        }
        String serialNumberLabel = "Serial Number :";
        {
            if (propertiesByFeature.get("SERIAL_NUMBER_" + language) != null) {
                serialNumberLabel = propertiesByFeature.get("SERIAL_NUMBER_" + language).getValue();
            }
        }

        String notesLabel = "Notes :";
        {
            if (propertiesByFeature.get("NOTES_" + language) != null) {
                notesLabel = propertiesByFeature.get("NOTES_" + language).getValue();
            }
        }
        String terminalInformation = "Terminal Information";
        {
            if (propertiesByFeature.get("TERMINAL_INFORMATION_" + language) != null) {
                terminalInformation = propertiesByFeature.get("TERMINAL_INFORMATION_" + language).getValue();
            }
        }
        String pcTerminalInformation = "PC Terminal Information";
        {
            if (propertiesByFeature.get("PC_TERMINAL_INFORMATION_" + language) != null) {
                pcTerminalInformation = propertiesByFeature.get("PC_TERMINAL_INFORMATION_" + language).getValue();
            }
        }
        String downloadSecurityCertificateFirst = "Download Security Certificate First!";
        {
            if (propertiesByFeature.get("DOWNLOAD_SECURITY_CERT_FIRST_" + language) != null) {
                downloadSecurityCertificateFirst = propertiesByFeature.get("DOWNLOAD_SECURITY_CERT_FIRST_" + language).getValue();
            }
        }
        String required = "REQUIRED";
        {
            if (propertiesByFeature.get("REQUIRED_" + language) != null) {
                required = propertiesByFeature.get("REQUIRED_" + language).getValue();
            }
        }
        String email = "Email :";
        {
            if (propertiesByFeature.get("EMAIL_" + language) != null) {
                email = propertiesByFeature.get("EMAIL_" + language).getValue();
            }
        }
        String enforceIPRestriction = "Enforce IP Restriction? :";
        {
            if (propertiesByFeature.get("ENFORCE_IP_" + language) != null) {
                enforceIPRestriction = propertiesByFeature.get("ENFORCE_IP_" + language).getValue();
            }
        }
        String brand = "Brand :";
        {
            if (propertiesByFeature.get("BRAND_" + language) != null) {
                brand = propertiesByFeature.get("BRAND_" + language).getValue();
            }
        }
        String registrationCode = "Registration Code :";
        {
            if (propertiesByFeature.get("REG_CODE_" + language) != null) {
                registrationCode = propertiesByFeature.get("REG_CODE_" + language).getValue();
            }
        }
        String password = "Password :";
        {
            if (propertiesByFeature.get("PASSWORD_" + language) != null) {
                password = propertiesByFeature.get("PASSWORD_" + language).getValue();
            }
        }
        String IPAddress = "IP Address :";
        {
            if (propertiesByFeature.get("IP_ADDRESS_" + language) != null) {
                IPAddress = propertiesByFeature.get("IP_ADDRESS_" + language).getValue();
            }
        }
        String subnetMask = "Subnet Mask :";
        {
            if (propertiesByFeature.get("SUBNET_MASK_" + language) != null) {
                subnetMask = propertiesByFeature.get("SUBNET_MASK_" + language).getValue();
            }
        }
        String enforceCertificate = "Enforce Certificate ? :";
        {
            if (propertiesByFeature.get("ENFORCE_CERTIFICATE_" + language) != null) {
                enforceCertificate = propertiesByFeature.get("ENFORCE_CERTIFICATE_" + language).getValue();
            }
        }
        String allowCertificateInstall = "Allow Certificate Install ? :";
        {
            if (propertiesByFeature.get("ALLOW_CERTIFICATE_INSTALL_" + language) != null) {
                allowCertificateInstall = propertiesByFeature.get("ALLOW_CERTIFICATE_INSTALL_" + language).getValue();
            }
        }
        String certificateInstallRemainingAmount = "Certificate Install Remaining Amount :";
        {
            if (propertiesByFeature.get("CERTIFICATE_INSTALL_REMAING_AMOUNT_" + language) != null) {
                certificateInstallRemainingAmount = propertiesByFeature.get("CERTIFICATE_INSTALL_REMAING_AMOUNT_" + language).getValue();
            }
        }
        String pcterminalURL = "PC Term URL :";
        {
            if (propertiesByFeature.get("PCTERMINAL_URL_" + language) != null) {
                pcterminalURL = propertiesByFeature.get("PCTERMINAL_URL_" + language).getValue();
            }
        }
        String loginInformation = "Login Information :";
        {
            if (propertiesByFeature.get("LOGIN_INFORMATION_" + language) != null) {
                loginInformation = propertiesByFeature.get("LOGIN_INFORMATION_" + language).getValue();
            }
        }
        String clerkNameLabel = "Clerk Name :";
        {
            if (propertiesByFeature.get("CLERK_NAME_" + language) != null) {
                clerkNameLabel = propertiesByFeature.get("CLERK_NAME_" + language).getValue();
            }
        }
        String clerkCodeLabel = "Clerk Code :";
        {
            if (propertiesByFeature.get("CLERK_CODE_" + language) != null) {
                clerkCodeLabel = propertiesByFeature.get("CLERK_CODE_" + language).getValue();
            }
        }
        String userNameLabel = "User Name :";
        {
            if (propertiesByFeature.get("USER_NAME_" + language) != null) {
                userNameLabel = propertiesByFeature.get("USER_NAME_" + language).getValue();
            }
        }
        String terminalEditedSubject = "Terminal Edited";
        {
            if (propertiesByFeature.get("SUBJECT_" + language) != null) {
                terminalEditedSubject = propertiesByFeature.get("SUBJECT_" + language).getValue();
            }
        }

        String passWordRecovery = "The answers of the recovery password was deleted, once enter to pcterminal must fill it again.";
        {
            if (propertiesByFeature.get("RECOVERY_PASSWORD_" + language) != null) {
                passWordRecovery = propertiesByFeature.get("RECOVERY_PASSWORD_" + language).getValue();
            }
        }

        StringBuilder header = new StringBuilder();
        header.append("<body bgcolor=\"#ffffff\"><table cellpadding=\"0\" cellspacing=\"0\"");
        header.append("style=\"BORDER-RIGHT: #7b9ebd 1px solid;"
                + "BORDER-TOP: #7b9ebd 1px solid;"
                + "BORDER-BOTTOM: #7b9ebd 1px solid;"
                + "BORDER-LEFT: #7b9ebd 1px solid;"
                + "BACKGROUND-COLOR: #f1f9fe;width:900px;\">");

        StringBuilder urlDownCertString = new StringBuilder();
        if (!isnew) {
            if (urlDownCert != null && !urlDownCert.equals("")) {

                urlDownCertString.append("<tr><td><b style=\"color:red;\">");
                header.append(mustDownloadCertificate);
                header.append("</b><br><br></td></tr>");

                header.append(urlDownCertString);
            }
        } else {
            if (urlDownCert != null && !urlDownCert.equals("")) {
                urlDownCertString.append("<tr><td><b style=\"color:red;\">");
                header.append(downloadCertificateFirst);
                header.append("</b></td></tr>");
                header.append(urlDownCertString);
            } else {
                header.append("<tr><td><b style=\"color:red;\">");
                header.append(pleaseReview);
                header.append("</b></td></tr>");
            }
        }
        String sTerminal = "";
        String sPCTerm = "";
        String sLogin = "";
        String sFooter = "</table></body>";

        header.append("<tr><td><b>");
        header.append(terminalEdited);
        header.append(": [_SITEID_]</b></td></tr><tr><td><b>");
        header.append(edited);
        header.append(by);
        header.append(" _ACTOR_: [_USERNAME_] ");
        header.append(from);
        header.append("[_COMPANYNAME_]</b></td></tr>");

        if (!isnew) {
            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>" + pcTerminalInformation + "</td></tr>"
                    + "<tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" "
                    + "style=\"BORDER-RIGHT: #7b9ebd 1px solid;"
                    + "BORDER-TOP: #7b9ebd 1px solid;"
                    + "BORDER-BOTTOM: #7b9ebd 1px solid;"
                    + "BORDER-LEFT: #7b9ebd 1px solid;"
                    + "BACKGROUND-COLOR: #ffffff;width:98%;\">";

            if (urlDownCert != null && !urlDownCert.equals("")) {
                sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b "
                        + "style=\"color:red;\">" + required + "<BR> " + downloadSecurityCertificateFirst + "</b></td><td></td>"
                        + "<td style=\"font-size: 11pt;\" nowrap valign=\"top\"><a href=\"" + urlDownCert + "\">"
                        + urlDownCert + "</td><td></td><td></td><td></td><td></td></tr><tr><td colspan=\"7\">&nbsp;</td></tr>";
            }
            sPCTerm += "</table></td></tr>";

        } else {
            sTerminal = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>" + terminalInformation + "</td></tr><tr><td align=center>"
                    + "<table cellpadding=\"0\" cellspacing=\"0\" style=\""
                    + "BORDER-RIGHT: #7b9ebd 1px solid;"
                    + "BORDER-TOP: #7b9ebd 1px solid;"
                    + "BORDER-BOTTOM: #7b9ebd 1px solid;"
                    + "BORDER-LEFT: #7b9ebd 1px solid;"
                    + "BACKGROUND-COLOR: #ffffff;width:98%;\">"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + merchantNameLabel + "</b></td><td>&nbsp;</td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_DBA_</td>"
                    + "<td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>" + email + "</b></td>"
                    + "<td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_EMAIL_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + terminalTypeLabel + "</b></td>"
                    + "<td></td><td style=\"font-size: 11pt;\" nowrap>_TYPE_</td><td></td>"
                    + (showTerminalClass ? "<td style=\"font-size: 11pt;\" nowrap><b>" + terminalClassLabel + "</b></td>"
                            + "<td></td><td style=\"font-size: 11pt;\" nowrap>_CLASS_</td></tr>" : "")
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + terminalVersion + "</b></td>"
                    + "<td></td><td style=\"font-size: 11pt;\" nowrap>_VERSION_</td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap><b>" + monthlyFeeLabel + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_FEE_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + serialNumberLabel + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" colspan=5>_SERIAL_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + notesLabel + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" colspan=5>_NOTES_</td>"
                    + "</table></td></tr>";

            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>" + pcTerminalInformation + "</td></tr><tr><td align=center>"
                    + "<table cellpadding=\"0\" cellspacing=\"0\" "
                    + "style=\"BORDER-RIGHT: #7b9ebd 1px solid;"
                    + "BORDER-TOP: #7b9ebd 1px solid;"
                    + "BORDER-BOTTOM: #7b9ebd 1px solid;"
                    + "BORDER-LEFT: #7b9ebd 1px solid;"
                    + "BACKGROUND-COLOR: #ffffff;width:98%;\">"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + brand + "</b></td>"
                    + "<td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_BRAND_</td>"
                    + "<td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>" + registrationCode + "</b></td>"
                    + "<td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_REGCODE_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + userNameLabel + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_PCTERMUSERNAME_</td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap><b>" + password + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_PCTERMPASSWORD_</td></tr><tr>"
                    + "<td style=\"font-size: 11pt;\" nowrap><b>" + enforceIPRestriction + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_IPRESTRICTION_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + IPAddress + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_IPADDRESS_</td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap><b>" + subnetMask + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_SUBNETMASK_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + enforceCertificate + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_CERTIFICATE_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + allowCertificateInstall + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_CERTIFICATEINSTALL_</td><td></td>"
                    + "<td style=\"font-size: 11pt;\"><b>" + certificateInstallRemainingAmount + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_CERTINSTALLAMOUNT_</td></tr>";

            if (urlDownCert != null && !urlDownCert.equals("")) {
                sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b style=\"color:red;\">" + required + " <BR>" + downloadSecurityCertificateFirst + "</b>&nbsp;</td>"
                        + "<td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap valign=\"top\"><a href=\"" + urlDownCert
                        + "\">" + urlDownCert + "</td><td></td><td></td><td></td><td></td></tr><tr><td colspan=\"7\">&nbsp;</td></tr>";
            }
            sPCTerm += "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + pcterminalURL + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_URL_</td></tr>";

            if (this.resetAnswers) {
                sPCTerm += "<tr><td colspan=\"3\" style=\"font-size: 11pt;color: red;\" nowrap><b>" + passWordRecovery + "</b></td></tr>";
            }
            sPCTerm += "</table></td></tr>";

            sLogin = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>" + loginInformation + "</td></tr>"
                    + "<tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" "
                    + "style=\"BORDER-RIGHT: #7b9ebd 1px solid;"
                    + "BORDER-TOP: #7b9ebd 1px solid;"
                    + "BORDER-BOTTOM: #7b9ebd 1px solid;"
                    + "BORDER-LEFT: #7b9ebd 1px solid;"
                    + "BACKGROUND-COLOR: #ffffff;width:98%;\">"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + clerkNameLabel + "</b></td><td>&nbsp;</td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_CLERKNAME_</td><td>&nbsp;&nbsp;</td>"
                    + "<td style=\"font-size: 11pt;\" nowrap><b>" + clerkCodeLabel + "</b></td><td>&nbsp;</td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_CLERKCODE_</td></tr></table></td></tr>";

        }
        if (!PCTerm) {
            sPCTerm = "";
        }

        //Info for WebService implementation
        if (!PCTerm && (TerminalDesc.indexOf("Web Service") > 0 || TerminalDesc.indexOf("23") > 0 || TerminalDesc.indexOf("79") > 0)) {

            sPCTerm = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>WS Terminal Information</td></tr>"
                    + "<tr><td align=center>"
                    + "<table cellpadding=\"0\" cellspacing=\"0\" "
                    + "style=\"BORDER-RIGHT: #7b9ebd 1px solid;"
                    + "BORDER-TOP: #7b9ebd 1px solid;"
                    + "BORDER-BOTTOM: #7b9ebd 1px solid;"
                    + "BORDER-LEFT: #7b9ebd 1px solid;"
                    + "BACKGROUND-COLOR: #ffffff;width:98%;\">"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + userNameLabel + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_PCTERMUSERNAME_</td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap><b>" + password + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_PCTERMPASSWORD_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + enforceIPRestriction + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_IPRESTRICTION_</td></tr>"
                    + "<tr><td style=\"font-size: 11pt;\" nowrap><b>" + IPAddress + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_IPADDRESS_</td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap><b>" + subnetMask + "</b></td><td></td>"
                    + "<td style=\"font-size: 11pt;\" nowrap>_SUBNETMASK_</td></tr>";

            sPCTerm += "</table></td></tr>";

        }
        //End WS

        String sHeader = header.toString().replaceAll("_SITEID_", this.siteId).
                replaceAll("_USERNAME_", userName).
                replaceAll("_COMPANYNAME_", this.cleanReplace(companyName));

        if (accessLevel.equals(DebisysConstants.ISO)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "ISO");
        } else if (accessLevel.equals(DebisysConstants.AGENT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "AGENT");
        } else if (accessLevel.equals(DebisysConstants.SUBAGENT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "SUBAGENT");
        } else if (accessLevel.equals(DebisysConstants.REP)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "REP");
        } else if (accessLevel.equals(DebisysConstants.MERCHANT)) {
            sHeader = sHeader.replaceAll("_ACTOR_", "MERCHANT");
        }

        sTerminal = sTerminal.replaceAll("_DBA_", this.cleanReplace(dba));
        sTerminal = sTerminal.replaceAll("_EMAIL_", contactEmail);
        sTerminal = sTerminal.replaceAll("_CLASS_", (showTerminalClass ? (sClass == null) ? "" : sClass : ""));
        sTerminal = sTerminal.replaceAll("_TYPE_", TerminalDesc);
        sTerminal = sTerminal.replaceAll("_VERSION_", StringUtil.toString(this.termVersion));
        sTerminal = sTerminal.replaceAll("_FEE_", monthlyFee);
        sTerminal = sTerminal.replaceAll("_NOTES_", this.cleanReplace(StringUtil.toString(this.termNote)));

        sPCTerm = sPCTerm.replaceAll("_BRAND_", StringUtil.toString(this.pcTermBrand));
        sPCTerm = sPCTerm.replaceAll("_REGCODE_", StringUtil.toString(this.txtRegistration));
        sPCTerm = sPCTerm.replaceAll("_PCTERMUSERNAME_", StringUtil.toString(this.pcTermUsername));
        sPCTerm = sPCTerm.replaceAll("_PCTERMPASSWORD_", StringUtil.toString(this.pcTermPassword));
        sPCTerm = sPCTerm.replaceAll("_IPRESTRICTION_", (this.chkIPRestriction) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_IPADDRESS_", StringUtil.toString(this.txtIPAddress));
        sPCTerm = sPCTerm.replaceAll("_SUBNETMASK_", StringUtil.toString(this.txtSubnetMask));
        sPCTerm = sPCTerm.replaceAll("_CERTIFICATE_", (this.chkCertificate) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_CERTIFICATEINSTALL_", (this.chkAllowCertificate) ? "YES" : "NO");
        sPCTerm = sPCTerm.replaceAll("_CERTINSTALLAMOUNT_", StringUtil.toString(this.txtAllowCertificate));

        if (URL != null) {
            sPCTerm = sPCTerm.replaceAll("_URL_", URL.replaceAll(" ", "%20"));
        }

        sLogin = sLogin.replaceAll("_CLERKNAME_", StringUtil.toString(this.clerkName) + " " + StringUtil.toString(this.clerkLastName)).replaceAll("_CLERKCODE_", StringUtil.toString(this.clerkCode));

        java.util.Properties props;
        props = System.getProperties();
        props.put("mail.smtp.host", mailHost);
        Session s = Session.getDefaultInstance(props, null);

        MimeMessage message = new MimeMessage(s);
        message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
        message.setFrom(new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
        if (sMails != null) {
            message.setRecipients(Message.RecipientType.CC, sMails.replaceAll(";", ","));
        }
        message.setSubject(terminalEditedSubject + " - Site Id: [" + this.siteId + "]");
        message.setContent(sHeader + sTerminal + sPCTerm + sLogin + sFooter, "text/html");
        Transport.send(message);
    }

    public static JSONObject getPhysicalTerminalModels(String sBrandID, SessionData sessionData) throws TerminalException {
        JSONObject json = new JSONObject();

        try {
            Vector jsonVec = new Vector();

            HashMap map0 = new HashMap();
            map0.put("model", "-1");
            map0.put("name", Languages.getString("jsp.admin.transactions.inventoryTerminals.optionAll", sessionData.getLanguage()));
            jsonVec.add(map0);

            CTerminalBrand tb = new CTerminalBrand();
            tb.loadBrand(Long.parseLong(sBrandID));
            tb.loadBrandModels();

            for (CTerminalModel tm : tb.getModelList()) {
                HashMap map = new HashMap();
                map.put("model", Long.toString(tm.getModelId()));
                map.put("name", tm.getModelName());
                jsonVec.add(map);
            }

            json.put("items", jsonVec);
        } catch (Exception e) {
            Terminal.cat.error("Error during getPhysicalTerminalModels", e);
            throw new TerminalException();
        }

        return json;
    } // End of function getPhysicalTerminalModels

    public static boolean IsTerminalInPhysicalList(ServletContext context, String sId) {
        return DebisysConfigListener.getPhysicalTerminals(context).indexOf("," + sId + ",") != -1;
    }

    public void setMarketPlaceKey(Integer marketPlaceKey) {
        this.MarketPlaceKey = marketPlaceKey;
    }

    public Integer getMarketPlaceKey() {
        return this.MarketPlaceKey;
    }

    public void setMarketPlaceGroup(Integer marketPlaceGroup) {
        this.MarketPlaceGroup = marketPlaceGroup;
    }

    public Integer getMarketPlaceGroup() {
        return this.MarketPlaceGroup;
    }

    public boolean isMarketPlaceTerminal(String terminalType) {
        try {
            // Vector vMPTerminalTypes = new Vector();
            // vMPTerminalTypes = getMarketplaceTerminalType();
            String myStr = Terminal.vecMarketplaceTerminalType.toString();

            if (myStr.indexOf("[" + terminalType + ",") != -1) {
                this.setSMarketPlaceTerminal(true);

                return this.isSMarketPlaceTerminal();
            } else {
                this.setSMarketPlaceTerminal(false);

                return this.isSMarketPlaceTerminal();
            }
        } catch (Exception e) {
            Terminal.cat.error("Error during getMarketPlaceModels", e);
        }

        return false;
    }

    public boolean hasFMRTerminal() throws TerminalException {
        // David Castro - This method verifies that the merchant has only 1 terminal of type FMR
        Connection dbConn = null;
        boolean result = false;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get Master database connection");
                throw new TerminalException();
            }
            PreparedStatement pstmt;
            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("findFMRTerminalsByMerchantID"));
            Terminal.cat.debug("FMR execute query:" + Terminal.sql_bundle.getString("findFMRTerminalsByMerchantID"));

            pstmt.setLong(1, Long.parseLong(this.merchantId));
            ResultSet rs = pstmt.executeQuery();

            if (rs.next() == true && this.terminalType.equals("91")) {
                Terminal.cat.error("There is already an FMR terminal for this merchant " + rs.getDouble("millennium_no"));
                result = true;
            }
        } catch (Exception ex) {
            Terminal.cat.error("Error during check FMR terminals", ex);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }

        return result;
    }

    public boolean setSMarketPlaceTerminal(boolean sMarketPlaceTerminal) {
        this.sMarketPlaceTerminal = sMarketPlaceTerminal;

        return sMarketPlaceTerminal;
    }

    public boolean isSMarketPlaceTerminal() {
        return this.sMarketPlaceTerminal;
    }

    public void captureActorsRates(HttpServletRequest request, String strAccessLevel, Hashtable hashTSOrders, String strDistChainType, Hashtable hashRepRates, boolean isEdit) {
        String[] productIds = request.getParameterValues("productId");
        if (productIds != null) {
            String strProductId = "";
            for (int i = 0; i < productIds.length; i++) {
                strProductId = productIds[i];
                if (strProductId != null && !strProductId.equals("")) {
                    if (hashTSOrders != null && !hashTSOrders.containsKey(strProductId) && request.getParameter("termts_" + strProductId) != null) {
                        hashTSOrders.put(strProductId, request.getParameter("termts_" + strProductId));
                    }

                    if (isEdit) {
                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                if (request.getParameter("iso_" + strProductId) != null && request.getParameter("agent_" + strProductId) != null && request.getParameter("subagent_" + strProductId) != null
                                        && request.getParameter("rep_" + strProductId) != null) {
                                    Vector vecRepRates = new Vector();
                                    vecRepRates.add(0, request.getParameter("iso_" + strProductId));
                                    vecRepRates.add(1, request.getParameter("agent_" + strProductId));
                                    vecRepRates.add(2, request.getParameter("subagent_" + strProductId));
                                    vecRepRates.add(3, request.getParameter("rep_" + strProductId));

                                    if (!hashRepRates.containsKey(strProductId)) {
                                        hashRepRates.put(strProductId, vecRepRates);
                                    }
                                }

                            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                if (request.getParameter("iso_" + strProductId) != null && request.getParameter("rep_" + strProductId) != null) {
                                    Vector vecRepRates = new Vector();
                                    vecRepRates.add(0, request.getParameter("iso_" + strProductId));
                                    vecRepRates.add(1, request.getParameter("rep_" + strProductId));

                                    if (!hashRepRates.containsKey(strProductId)) {
                                        hashRepRates.put(strProductId, vecRepRates);
                                    }
                                }
                            }
                        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                            if (request.getParameter("agent_" + strProductId) != null && request.getParameter("subagent_" + strProductId) != null && request.getParameter("rep_" + strProductId) != null) {
                                Vector vecRepRates = new Vector();
                                vecRepRates.add(0, request.getParameter("agent_" + strProductId));
                                vecRepRates.add(1, request.getParameter("subagent_" + strProductId));
                                vecRepRates.add(2, request.getParameter("rep_" + strProductId));

                                if (!hashRepRates.containsKey(strProductId)) {
                                    hashRepRates.put(strProductId, vecRepRates);
                                }
                            }
                        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                            if (request.getParameter("subagent_" + strProductId) != null && request.getParameter("rep_" + strProductId) != null) {
                                Vector vecRepRates = new Vector();
                                vecRepRates.add(0, request.getParameter("subagent_" + strProductId));
                                vecRepRates.add(1, request.getParameter("rep_" + strProductId));
                                if (!hashRepRates.containsKey(strProductId)) {
                                    hashRepRates.put(strProductId, vecRepRates);
                                }
                            }
                        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                            if (request.getParameter("rep_" + strProductId) != null) {
                                Vector vecRepRates = new Vector();
                                vecRepRates.add(0, request.getParameter("rep_" + strProductId));
                                if (!hashRepRates.containsKey(strProductId)) {
                                    hashRepRates.put(strProductId, vecRepRates);
                                }
                            }
                        }

                    } else {
                        // Add action
                        if (request.getParameter("rep_" + strProductId) != null) {
                            Vector vecRepRates = new Vector();
                            vecRepRates.add(0, request.getParameter("rep_" + strProductId));
                            if (!hashRepRates.containsKey(strProductId)) {
                                hashRepRates.put(strProductId, vecRepRates);
                            }
                        }
                    }
                }// product
            }// for
        }// array of products
    }

    public void captureActorsNewRates(HttpServletRequest request, String strAccessLevel, Hashtable hashTSOrders, String strDistChainType, Hashtable hashRepRates, boolean isEdit) {
        String[] productIds = request.getParameterValues("newproductId");
        if (productIds != null) {
            String strProductId = "";
            for (int i = 0; i < productIds.length; i++) {
                strProductId = productIds[i];
                if (strProductId != null && !strProductId.equals("")) {
                    if (hashTSOrders != null && !hashTSOrders.containsKey(strProductId) && request.getParameter("newtermts_" + strProductId) != null) {
                        hashTSOrders.put(strProductId, request.getParameter("newtermts_" + strProductId));
                    }

                    if (isEdit && "ISO".equals(this.chkRatePlan)) {
                        if (strAccessLevel.equals(DebisysConstants.ISO)) {
                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                                if (request.getParameter("newiso_" + strProductId) != null && request.getParameter("newagent_" + strProductId) != null && request.getParameter("newsubagent_" + strProductId) != null
                                        && request.getParameter("newrep_" + strProductId) != null) {
                                    Vector vecRepRates = new Vector();
                                    vecRepRates.add(0, request.getParameter("newiso_" + strProductId));
                                    vecRepRates.add(1, request.getParameter("newagent_" + strProductId));
                                    vecRepRates.add(2, request.getParameter("newsubagent_" + strProductId));
                                    vecRepRates.add(3, request.getParameter("newrep_" + strProductId));

                                    if (!hashRepRates.containsKey(strProductId)) {
                                        hashRepRates.put(strProductId, vecRepRates);
                                    }
                                }

                            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                                if (request.getParameter("newiso_" + strProductId) != null && request.getParameter("newrep_" + strProductId) != null) {
                                    Vector vecRepRates = new Vector();
                                    vecRepRates.add(0, request.getParameter("newiso_" + strProductId));
                                    vecRepRates.add(1, request.getParameter("newrep_" + strProductId));

                                    if (!hashRepRates.containsKey(strProductId)) {
                                        hashRepRates.put(strProductId, vecRepRates);
                                    }
                                }
                            }
                        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                            if (request.getParameter("newagent_" + strProductId) != null && request.getParameter("newsubagent_" + strProductId) != null && request.getParameter("newrep_" + strProductId) != null) {
                                Vector vecRepRates = new Vector();
                                vecRepRates.add(0, request.getParameter("newagent_" + strProductId));
                                vecRepRates.add(1, request.getParameter("newsubagent_" + strProductId));
                                vecRepRates.add(2, request.getParameter("newrep_" + strProductId));

                                if (!hashRepRates.containsKey(strProductId)) {
                                    hashRepRates.put(strProductId, vecRepRates);
                                }
                            }
                        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                            if (request.getParameter("newsubagent_" + strProductId) != null && request.getParameter("newrep_" + strProductId) != null) {
                                Vector vecRepRates = new Vector();
                                vecRepRates.add(0, request.getParameter("newsubagent_" + strProductId));
                                vecRepRates.add(1, request.getParameter("newrep_" + strProductId));
                                if (!hashRepRates.containsKey(strProductId)) {
                                    hashRepRates.put(strProductId, vecRepRates);
                                }
                            }
                        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                            if (request.getParameter("newrep_" + strProductId) != null) {
                                Vector vecRepRates = new Vector();
                                vecRepRates.add(0, request.getParameter("newrep_" + strProductId));
                                if (!hashRepRates.containsKey(strProductId)) {
                                    hashRepRates.put(strProductId, vecRepRates);
                                }
                            }
                        }

                    } else {
                        // Add action
                        if (request.getParameter("newrep_" + strProductId) != null) {
                            Vector vecRepRates = new Vector();
                            vecRepRates.add(0, request.getParameter("newrep_" + strProductId));
                            if (!hashRepRates.containsKey(strProductId)) {
                                hashRepRates.put(strProductId, vecRepRates);
                            }
                        }
                    }
                }// product
            }// for
        }// array of products
    }

    /**
     * @param smsPhoneNumber Phone number to be assigned to the SMS terminal
     * @param retValues If a registration found, it returns the values in order
     * to let the caller to build the LOG message with more info. The returned
     * array has 4 elements which contains the following data : terminal_id,
     * millennium_no, merchant_id, legal_businessname
     * @return True if there is another record on the dbo.Terminal_Registration
     * table with the same phone number (saved as UserId)
     */
    private boolean isSMSPhoneAlreadyAssigned(String smsPhoneNumber, String[] retValues) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rsRegistration = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getTerminalRegistrationByUserId"));
            pstmt.setString(1, smsPhoneNumber);
            rsRegistration = pstmt.executeQuery();
            if (rsRegistration.next()) {
                retValues[0] = rsRegistration.getString("terminal_id");
                retValues[1] = rsRegistration.getString("millennium_no");
                retValues[2] = rsRegistration.getString("merchant_id");
                retValues[3] = rsRegistration.getString("legal_businessname");
                retValue = true;
                rsRegistration.close();
            }
        } catch (Exception localException) {
            Terminal.cat.error("Error during isSMSPhoneAlreadyAssigned", localException);
        } finally {
            try {
                pstmt.close();
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
        return retValue;
    }

    public static boolean IsTerminalInMBList(ServletContext context, String terminalTypeId) throws TerminalException {
        Connection dbConn = null;
        boolean value = false;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }
            String sql = Terminal.sql_bundle.getString("IsTerminalInMBList") + " AND tt.terminal_type=?";
            PreparedStatement pstmt;
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, terminalTypeId);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                value = true;
                cat.info("The terminal and response type supports MicroBrowser Application " + rs.getString(1));
            } else {
                cat.info("The response type do not supports MicroBrowser Application ");
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during IsTerminalInMBList ", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection IsTerminalInMBList ", e);
            }
        }
        return value;
    }

    public static String TerminalMBList() throws TerminalException {
        Connection dbConn = null;
        StringBuilder value = new StringBuilder();
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }
            PreparedStatement pstmt;
            String sql = Terminal.sql_bundle.getString("IsTerminalInMBList");
            pstmt = dbConn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                String terminalTypeId = rs.getString(2);
                value.append(terminalTypeId + ",");
                cat.info("The terminal and response type supports MicroBrowser Application " + rs.getString(1));
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during IsTerminalInMBList ", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection IsTerminalInMBList ", e);
            }
        }
        return value.toString();
    }

    public String getBrandingProperties(String branding, String property) throws TerminalException {
        Connection dbConn = null;
        String value = null;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }
            PreparedStatement pstmt;
            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("getBrandingProperties"));
            pstmt.setString(1, branding);
            pstmt.setString(2, property);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                value = rs.getString("Value");
            } else {
                value = "";
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getBrandingProperties", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
        return value;
    }

    public static Vector<Vector<Object>> getTerminalsBySerialNumbers(SessionData sessionData, String serialNumber, String sSiteID, boolean anyUser, int intPageSize, int intPage) throws TerminalException {
        Connection dbConn = null;
        Vector vecTerminals = new Vector();
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strRefId = sessionData.getProperty("ref_id");

        String strSQL = "select distinct(t.terminal_id)" + ",t.merchant_id  " + ",t.terminal_id as terminalID" + ",t.terminal_type" + ",t.millennium_no" + ",tt.description" + ",r.firstname " + ",r.lastname" + ",r.businessname" + ",r.ISO_id" + ",r.rep_id"
                + ", tr.terminal_id as terminal_status " + " from terminals t with (nolock) " + " inner join merchants m with (nolock) on (m.merchant_id = t.merchant_id)" + " inner join reps r with (nolock) on (m.rep_id = r.rep_id)"
                + " inner join terminal_types tt with (nolock) on   (t.terminal_type=tt.terminal_type) " + " left outer join terminal_rates tr with (nolock) on (t.terminal_id = tr.terminal_id)";

        String strSQLWhere = ("%".equals(serialNumber.substring(0, 1))) ? " where t.SerialNumber like '" + serialNumber + "' " : " where t.SerialNumber = '" + serialNumber + "' ";

        if (sSiteID != null && sSiteID.length() > 0) {
            strSQLWhere += (" AND t.millennium_no <> '" + sSiteID + "' ");
        }

        // if(!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
        if (!sessionData.getUser().getUsername().equalsIgnoreCase("emidadirect") && !anyUser) {
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    // cat.debug(hdr+"ISO DIST_CHAIN_3_LEVEL");
                    strSQLWhere += " AND r.rep_id in (select rep_id from reps with (nolock) where iso_id=" + strRefId + ")";
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    // cat.debug(hdr+"ISO DIST_CHAIN_5_LEVEL");
                    strSQLWhere += " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                // cat.debug(hdr+ "AGENT");
                strSQLWhere += " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " and iso_id = " + strRefId + ")) ";
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                // cat.debug(hdr+"SUBAGENT");
                strSQLWhere += " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                // cat.debug(hdr+"REP");
                strSQLWhere += " and r.rep_id = " + strRefId;
            }
        }

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new ReportException();
            }

            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL + strSQLWhere, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            Terminal.cat.debug(strSQL + strSQLWhere);
            ResultSet rs = pstmt.executeQuery();
            int intRecordCount = DbUtil.getRecordCount(rs);
            // first row is always the count
            Vector vecRowCount = new Vector();
            vecRowCount.add(Integer.valueOf(intRecordCount));
            vecTerminals.add(vecRowCount);

            if (!(intPageSize > 0)) {
                intPageSize = intRecordCount;
            }

            if (intRecordCount > 0) {
                rs.absolute(DbUtil.getRowNumber(rs, intPageSize, intRecordCount, intPage));
                for (int i = 0; i < intPageSize; i++) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(StringUtil.toString(rs.getString("firstname") + " " + rs.getString("lastname")));
                    vecTemp.add(StringUtil.toString(rs.getString("businessname")));
                    vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
                    vecTemp.add(StringUtil.toString(rs.getString("terminalID")));
                    vecTemp.add(StringUtil.toString(rs.getString("description")));
                    if (rs.getString("terminal_status") != null) {
                        vecTemp.add(Languages.getString("jsp.admin.reports.analisys.terminal_enabled", sessionData.getLanguage()));
                    } else {
                        vecTemp.add(Languages.getString("jsp.admin.reports.analisys.terminal_disabled", sessionData.getLanguage()));
                    }
                    vecTemp.add(StringUtil.toString(rs.getString("ISO_id")));
                    vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
                    vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
                    vecTerminals.add(vecTemp);
                    if (!rs.next()) {
                        break;
                    }
                }
            }
            rs.close();
            pstmt.close();

        } catch (Exception e) {
            Terminal.cat.error("Error during getTerminalsBySerialNumbers", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecTerminals;
    }

    public static boolean existsTerminalsBySerialNumbers(SessionData sessionData, String serialNumber) throws TerminalException {
        Connection dbConn = null;
        boolean existsTerminals = false;
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strRefId = sessionData.getProperty("ref_id");

        String strSQL = "select distinct(t.terminal_id)"
                + // ",t.merchant_id "+
                // ",t.terminal_id as terminalID"+
                // ",t.terminal_type"+
                // ",r.firstname "+
                // ",r.lastname"+
                // ",r.businessname"+
                // ", tr.terminal_id as terminal_status "+
                " from terminals t with (nolock) " + " inner join merchants m with (nolock) on (m.merchant_id = t.merchant_id)" + " inner join reps r with (nolock) on (m.rep_id = r.rep_id)";
        // " left outer join terminal_rates tr (nolock) on (t.terminal_id = tr.terminal_id)"+

        String strSQLWhere = ("%".equals(serialNumber.substring(0, 1))) ? " where t.SerialNumber like '" + serialNumber + "' " : " where t.SerialNumber = '" + serialNumber + "' ";

        if (strAccessLevel.equals(DebisysConstants.ISO)) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                // cat.debug(hdr+"ISO DIST_CHAIN_3_LEVEL");
                strSQLWhere += " AND r.rep_id NOT IN (select rep_id from reps with (nolock) where iso_id=" + strRefId + ")";
            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                // cat.debug(hdr+"ISO DIST_CHAIN_5_LEVEL");
                strSQLWhere += " and r.rep_id NOT IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
            }
        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
            // cat.debug(hdr+ "AGENT");
            strSQLWhere += " and r.rep_id NOT IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                    + " and iso_id = " + strRefId + ")) ";
        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
            // cat.debug(hdr+"SUBAGENT");
            strSQLWhere += " and r.rep_id NOT IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
            // cat.debug(hdr+"REP");
            strSQLWhere += " and r.rep_id <> " + strRefId;
        }

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new ReportException();
            }

            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
            Terminal.cat.debug(strSQL + strSQLWhere);
            ResultSet rs = pstmt.executeQuery();
            // int intRecordCount = DbUtil.getRecordCount(rs);
            // first row is always the count
            if (rs.next()) {
                existsTerminals = true;
            }

            rs.close();
            pstmt.close();

        } catch (Exception e) {
            Terminal.cat.error("Error during existsTerminalsBySerialNumbers", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }
        return existsTerminals;
    }

    /**
     * @return true if the SerialNumber is correct, false otherwise
     * @author jacuna
     */
    public boolean validateSerialNumber(SessionData sessionData) {
        boolean valid = true;
        if (this.serialNumber.length() == 0) {
            this.addFieldError("general", Languages.getString("jsp.admin.reports.analisys.error.serialnumber_empty", sessionData.getLanguage()));
            valid = false;
        }

        // if(this.serialNumber.matches("(.*)[\\-\\s](.*)"))
        if (this.serialNumber.matches("(.*)[^a-zA-Z0-9](.*)")) {
            this.addFieldError("general", Languages.getString("jsp.admin.reports.analisys.error.serialnumber_notValid", sessionData.getLanguage()));
            valid = false;
        }

        return valid;
    }

    /**
     * Creates a formated Serial Number depending on the format given
     *
     * @return the formated Serial Number or empty if an error ocurrs
     * @author jacuna
     */
    public String transformSerialNumber(SessionData sessionData) {
        StringBuffer strTransformed = new StringBuffer();
        if (this.serialNumberFormat != null && this.serialNumberFormat.length() > 0 && this.serialNumber != null && this.serialNumber.length() > 0) {
            char[] format = this.serialNumberFormat.toCharArray();
            int letterIndex = 0;
            for (int i = 0; i < format.length; i++) {
                if (letterIndex > this.serialNumber.length() - 1) {
                    this.addFieldError("general", Languages.getString("jsp.admin.reports.analisys.error.serialnumber_lenght", sessionData.getLanguage())); // "Unexpected
                    // lenght
                    // of
                    // Serial
                    // Number
                    // value"
                    strTransformed.delete(0, strTransformed.length());
                    break;
                }

                if (Character.isLetterOrDigit(format[i]) && !Character.isSpaceChar(format[i])) {
                    if ((Character.isLetter(format[i]) && Character.isLetter(this.serialNumber.charAt(letterIndex))) || (Character.isDigit(format[i]) && Character.isDigit(this.serialNumber.charAt(letterIndex)))) {
                        strTransformed.append(this.serialNumber.charAt(letterIndex));
                        letterIndex++;
                    } else {
                        this.addFieldError("general", Languages.getString("jsp.admin.reports.analisys.error.serialnumber_char", sessionData.getLanguage()));// Digit
                        // or
                        // character
                        // not
                        // expected
                        // in
                        // Serial
                        // Number
                        // value
                        strTransformed.delete(0, strTransformed.length());
                        break;
                    }
                } else {
                    strTransformed.append(format[i]);
                }
            }
        } else {
            strTransformed.append(this.serialNumber);
        }

        return strTransformed.toString();
    }

    public String getSerialNumberFormat() {
        return this.serialNumberFormat;
    }

    public void setSerialNumberFormat(String serialNumberFormat) {
        this.serialNumberFormat = serialNumberFormat;
    }

    public boolean removeAllSerialNumbers(SessionData sessionData) throws TerminalException {
        Connection dbConn = null;
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strRefId = sessionData.getProperty("ref_id");
        boolean isSuccessful = true;
        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            String strSQL = "UPDATE terminals SET SerialNumber = NULL ";

            String strSQLWhere = " WHERE SerialNumber = ? ";

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            if (!sessionData.getUser().getUsername().equalsIgnoreCase("emidadirect")) {
                strSQLWhere += " AND merchant_id IN (select merchant_id from merchants with (nolock) where ";
                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                        // cat.debug(hdr+"ISO DIST_CHAIN_3_LEVEL");
                        strSQLWhere += " rep_id in (select rep_id from reps with (nolock) where iso_id=" + strRefId + ")";
                    } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        // cat.debug(hdr+"ISO DIST_CHAIN_5_LEVEL");
                        strSQLWhere += " rep_id IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                                + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
                    }
                } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                    // cat.debug(hdr+ "AGENT");
                    strSQLWhere += " rep_id IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " and iso_id = " + strRefId + ")) ";
                } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                    // cat.debug(hdr+"SUBAGENT");
                    strSQLWhere += " rep_id IN " + "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
                } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                    // cat.debug(hdr+"REP");
                    strSQLWhere += " rep_id = " + strRefId;
                }
                strSQLWhere += ")";
            }

            PreparedStatement pstmt;
            pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
            pstmt.setString(1, this.serialNumber);

            pstmt.executeUpdate();
            pstmt.close();

        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback removeAllSerialNumbers", ex);
            }
            isSuccessful = false;
            Terminal.cat.error("Error during removeAllSerialNumbers", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
                isSuccessful = false;
            }
        }
        return isSuccessful;
    }

    /**
     * @param enablePINCache the enablePINCache to set
     */
    public void setEnablePINCache(boolean enablePINCache) {
        this.enablePINCache = enablePINCache;
    }

    /**
     * @return the enablePINCache
     */
    public boolean isEnablePINCache() {
        return this.enablePINCache;
    }

    public String[] getNewproductId() {
        return this.newproductId;
    }

    public void setNewproductId(String[] newproductId) {
        this.newproductId = newproductId;
    }

    public String getRateSelector() {
        return this.rateSelector;
    }

    public void setRateSelector(String rateSelector) {
        this.rateSelector = rateSelector;
    }

    public Vector getTopSellersForMerchanstUpdate() throws TerminalException {
        Vector vecTerminalTopSellers = new Vector();
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgAlternateDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("topSellerForMerchants");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchantId));
            pstmt.setDouble(2, Double.parseDouble(this.siteId));
            String sql = (strSQL.replaceFirst("\\?", this.merchantId)).replaceFirst("\\?", this.siteId);
            Terminal.cat.debug(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("product_id"));
                vecTemp.add(rs.getString("description"));
                vecTemp.add(StringUtil.toString(rs.getString("tsorder")));
                vecTemp.add(rs.getInt("terminal_Rate_id"));
                vecTerminalTopSellers.add(vecTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getTopSellersForMerchanstUpdate", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecTerminalTopSellers;
    }

    public void updateTSOrderMerchant(HttpServletRequest request) throws TerminalException {
        Connection dbConn = null;
        String value;
        String terminalRate;
        PreparedStatement pstmt = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            int count = Integer.parseInt(request.getParameter("countProducts"));

            dbConn.setAutoCommit(false);

            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateTerminalTopSellersByMerchant"));

            for (int i = 1; i <= count; i++) {
                value = request.getParameter("value_" + i);

                terminalRate = request.getParameter("terminalrate_" + i);
                Terminal.cat.debug(value + " - " + terminalRate + " - " + i);
                if (value == "") {
                    pstmt.setNull(1, java.sql.Types.INTEGER);
                } else {
                    try {
                        Integer valueOrder = new Integer(value);
                        pstmt.setInt(1, Integer.parseInt(value));
                    } catch (Exception e) {
                        pstmt.setNull(1, java.sql.Types.INTEGER);
                    }

                }
                pstmt.setInt(2, Integer.parseInt(terminalRate));
                pstmt.addBatch();

            }

            pstmt.executeBatch();
            pstmt.close();
            dbConn.commit();
            dbConn.setAutoCommit(true);

        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback updateTerminalTS", ex);
            }

            Terminal.cat.error("Error during updateTerminalTS", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
        }
    }

    public boolean updateWebserviceRegistration(ServletContext context, SessionData sessionData, HttpServletRequest request) throws TerminalException {
        String terminalType = ((String) sessionData.getProperty("terminalType"));
        return (((terminalType != null) && terminalType.equalsIgnoreCase(DebisysConstants.WEB_SERVICE_TERMINAL_TYPE)) ? this.insertTerminalHistoryWebService(request, sessionData, context) : this.updateTerminalRegistration(context, sessionData, request));
    }

    private boolean updateTerminalRegistration(ServletContext context, SessionData sessionData, HttpServletRequest request) throws TerminalException {
        Connection dbConn = null;
        boolean result = false;

        String seeder = (String) context.getAttribute("secretSeed");
        SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
        this.pcTermPassword = cipherService.encrypt(this.pcTermPassword);

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            dbConn.setAutoCommit(false);

            if (!this.chkManualRegCode) {
                this.txtRegistration = this.siteId;
            }
            /*            if(this.chkConfirmationByUser){
                this.txtRegistration = StringUtil.randomString(StringUtil.CHARSET_AZ_09, 10);
            }*/

            String sUpdateInfo = "";
            sUpdateInfo = "[millennium_no=" + this.siteId + "] [RegistrationCode=" + this.txtRegistration + "] [RegistrationInd=-1] [RegistrationDate=null] ";
            sUpdateInfo += ("[UserId=" + this.pcTermUsername + "] [Password=" + this.pcTermPassword + "] [RegistrationIp=null] [RegistrationAttemptNum=0] ");
            sUpdateInfo += ("[RegistrationIgnoreInd=" + (this.chkRegCode ? "-1" : "1") + "] [CustomConfigId=" + this.pcTermBrand + "] ");
            sUpdateInfo += ("[RestrictTolpAddr=" + this.txtIPAddress + "] [RestrictToSubnetMask=" + this.txtSubnetMask + "] ");
            sUpdateInfo += ("[ipAddrIgnoreInd=" + (this.chkIPRestriction ? "-1" : "1") + "] [DisabledInd=" + this.ddlRecordStatus + "]");
            sUpdateInfo += ("[RegistrationByUserIgnoreInd=" + (this.chkConfirmationByUser ? "-1" : "1") + "] [NotifyByEmailIgnoreInd=" + (this.chkConfirmationByEmail ? "-1" : "1") + "]");
            sUpdateInfo += ("[NotifyBySmsIgnoreInd=" + (this.chkConfirmationBySMS ? "-1" : "1") + "] [NotificationEmail=" + this.txtConfirmationEMail + "]");
            sUpdateInfo += ("[NotificationPhoneNumber=" + this.txtConfirmationPhoneNumber + "]");

            PreparedStatement pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("updateWebServiceTerminalRegistration"));
            pstmt.setString(1, this.txtRegistration);
            // RegistrationInd = -1
            // RegistrationDate = null
            pstmt.setString(2, this.pcTermUsername);
            pstmt.setString(3, this.pcTermPassword);
            // RegistrationIp = null
            // RegistrationAttemptNum = 0
            pstmt.setInt(4, (this.chkRegCode ? (-1) : 1)); // RegistrationIgnoreInd
            pstmt.setString(5, this.pcTermBrand); // CustomConfigId
            pstmt.setString(6, this.txtIPAddress); // RestrictToIpAddr
            pstmt.setString(7, this.txtSubnetMask); // RestrictToSubnetMask
            pstmt.setInt(8, (this.chkIPRestriction ? (-1) : 1)); // ipAddrIgnoreInd
            pstmt.setString(9, (this.ddlRecordStatus.length() == 0) ? null : this.ddlRecordStatus); // DisabledInd
            pstmt.setInt(10, (this.chkConfirmationByUser ? (-1) : 1)); // RegistrationByUserIgnoreInd
/*            if(this.chkConfirmationByUser){
                pstmt.setInt(4, -1); // RegistrationIgnoreInd
            }*/
            pstmt.setInt(11, (this.chkConfirmationByEmail ? (-1) : 1)); // NotifyByEmailIgnoreInd
            pstmt.setInt(12, (this.chkConfirmationBySMS ? (-1) : 1)); // NotifyBySmsIgnoreInd
            pstmt.setString(13, this.txtConfirmationEMail); // NotificationEmail
            pstmt.setString(14, this.txtConfirmationPhoneNumber); // NotificationPhoneNumber
            pstmt.setInt(15, Integer.parseInt(request.getParameter("edit"))); // ID
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = dbConn.prepareStatement(Terminal.sql_bundle.getString("addTerminalActivityLog"));
            // CurrentDate
            pstmt.setInt(1, Integer.parseInt(this.siteId));
            pstmt.setString(2, this.txtRegistration);
            pstmt.setString(3, this.pcTermUsername);
            pstmt.setString(4, request.getRemoteHost());
            pstmt.setString(5, request.getLocalAddr());
            pstmt.setString(6, request.getContextPath());
            // Class = SupportSite
            pstmt.setString(7, "updateWebServiceTerminal"); // FunctionName
            pstmt.setString(8, "ADMIN UPDATE: " + sUpdateInfo); // Description
            pstmt.setString(9, "OK"); // ErrorCode
            pstmt.setString(10, sUpdateInfo); // Error Message
            pstmt.setString(11, this.pcTermBrand); // CustomConfigId

            pstmt.executeUpdate();
            pstmt.close();

            dbConn.commit();
            dbConn.setAutoCommit(true);
            result = true;
            this.trackTrmRegChanges(context, sessionData, Integer.parseInt(request.getParameter("edit")), "Updating Terminal Registration:");
        } catch (SQLException ex) {
            Terminal.cat.error("Error during updateWebServiceTerminalRegistration SQLException:", ex);

            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException sqex) {
                Terminal.cat.error("Error during rollback updateWebServiceTerminalRegistration", sqex);
            }

            if (ex.getMessage().indexOf("UserData") > 0 && ex.getMessage().indexOf("pk_UserData") > 0) {
                // this error comes from EJBCA DataBase other instance of the
                // SQLServer
                this.addFieldError("terminal_registration", "EJBCA : " + Languages.getString("com.debisys.users.error_username_duplicate", sessionData.getLanguage()));
            } else {
                this.addFieldError("terminal_registration", Languages.getString("jsp.admin.terminal.general_update_error", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            try {
                if (dbConn != null) {
                    dbConn.rollback();
                }
            } catch (SQLException ex) {
                Terminal.cat.error("Error during rollback updateWebServiceTerminalRegistration", ex);
            }

            Terminal.cat.error("Error during updateWebServiceTerminalRegistration", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during release connection", e);
            }
            return result;
        }
    }

    private boolean insertTerminalHistoryWebService(HttpServletRequest request, SessionData sessionData, ServletContext context) throws TerminalException {
        boolean result = false;
        Integer idTerminal = Integer.parseInt(request.getParameter("edit"));

        /*
        String seeder = (String) context.getAttribute("secretSeed");
        SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
        this.pcTermPassword = cipherService.encrypt(this.pcTermPassword);
         */
        if ((idTerminal != null) && ((this.validatePasswordsTerminal(this.pcTermPassword, sessionData, context)) && (this.checkPassUniqueCycle(idTerminal, sessionData)))) {
            result = ((this.updateTerminalRegistration(context, sessionData, request)) && (this.insertNewRegCodeInHistory(idTerminal, this.pcTermPassword)));
        }
        return result;
    }

    private boolean checkPassUniqueCycle(Integer id, SessionData sessionData) {
        return ((this.checkPasswordCyclesPermission(sessionData)) ? !this.checkStringInList(this.pcTermPassword, this.getTerminalRegCodeLogById(id)) : true);
    }

    private boolean checkStringInList(String field, List lsFields) {
        return lsFields.contains(field);
    }

    private boolean checkPasswordCyclesPermission(SessionData sessionData) {
        try {
            return sessionData.checkPermission(DebisysConstants.PERM_ENABLE_PASSWORD_CYCLES_TERMINALS_WS);
        } catch (Exception e) {
            Terminal.cat.error("checkPasswordCyclesPermission - ERROR CHECKING PERMISSION 173 : Password Cycles for Terminals WS ", e);
            return false;
        }
    }

    /**
     * @param status the status to set
     */
    public void setStatus(long status) {
        this.Status = status;
    }

    public void setStatus(String status) {
        if (status.length() > 0) {
            this.Status = Long.parseLong(status);
        } else {
            this.Status = 0;
        }
    }

    /**
     * @return the status
     */
    public long getStatus() {
        return this.Status;
    }

    /**
     * @param statusLastChange the statusLastChange to set
     */
    public void setStatusLastChange(String statusLastChange) {
        this.StatusLastChange = statusLastChange;
    }

    /**
     * @return the statusLastChange
     */
    public String getStatusLastChange() {
        return this.StatusLastChange;
    }

    //#BEGIN Release 31 NM - Rate Plan Rewrite
    /**
     * @param ratePlanId
     * @return
     */
    public static String getTerminalRatePlanName(String ratePlanId, SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String RatePlanName = "";
        if (ratePlanId != null && ratePlanId.length() > 0) {
            try {
                dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Terminal.cat.error("Can't get database connection");
                    throw new CustomerException();
                }

                String strSQL = "";

                strSQL = Terminal.sql_bundle.getString("getTerminalsRatePlanName");

                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setInt(1, Integer.parseInt(ratePlanId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    RatePlanName = rs.getString(1);
                } else {
                    RatePlanName = Languages.getString("jsp.admin.customers.merchants_info.no_rateplan", sessionData.getLanguage());
                }

                rs.close();
                pstmt.close();
            } catch (Exception e) {
                Terminal.cat.error("Error during getTerminalRatePlanName", e);
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    Terminal.cat.error("Error during closeConnection", e);
                }
            }
        } else {
            RatePlanName = Languages.getString("jsp.admin.customers.merchants_info.no_rateplan", sessionData.getLanguage());
        }
        return RatePlanName;
    }

    /**
     * Set to ISO Rate Plan Model.
     *
     * @param ratePlan the ratePlan to set
     */
    public void setRatePlan(String ratePlan) {
        this.RatePlan = ratePlan;
    }

    /**
     * @return the ratePlan
     */
    public String getRatePlan() {
        return this.RatePlan;
    }

    /**
     * @param mBConfigurationID the mBConfigurationID to set
     */
    public void setMBConfigurationID(String mBConfigurationID) {
        this.MBConfigurationID = mBConfigurationID;
    }

    /**
     * @return the mBConfigurationID
     */
    public String getMBConfigurationID() {
        return this.MBConfigurationID;
    }

    /**
     * @return @throws TerminalException
     */
    public Vector getTopSellersTerminal() throws TerminalException {
        this.getTerminalRates("");
        Vector vecTerminalTopSellers = new Vector();
        Connection dbConn = null;

        try {
            dbConn = Torque.getConnection(Terminal.sql_bundle.getString("pkgAlternateDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("topSellerForMerchants");
            PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.merchantId));
            pstmt.setDouble(2, Double.parseDouble(this.siteId));
            String sql = (strSQL.replaceFirst("\\?", this.merchantId)).replaceFirst("\\?", this.siteId);
            Terminal.cat.debug(sql);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("product_id"));
                vecTemp.add(rs.getString("description"));
                vecTemp.add(StringUtil.toString(rs.getString("tsorder")));
                vecTerminalTopSellers.add(vecTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getTopSellersForMerchanstUpdate", e);
            throw new TerminalException();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vecTerminalTopSellers;
    }

    //#END Release 31 NM

    /* BEGIN Release 32 - Added by LF */
    public Vector<Vector<String>> getMBConfigurations() throws TerminalException {
        Vector<Vector<String>> vResult = new Vector<Vector<String>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getMBConfigurations");
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(rs.getString("ConfigurationID"));
                if (rs.getString("Name").equals("Default")) {
                    vecTemp.add(Terminal.sql_bundle.getString("defaultMBConfigurationTitle"));
                } else {
                    vecTemp.add(rs.getString("Name"));
                }
                vecTemp.add(rs.getString("TerminalCount"));
                vResult.add(vecTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getMBConfigurations", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vResult;
    }

    public Vector<Vector<String>> getMBConfigurationData(int nConfID) throws TerminalException {
        Vector<Vector<String>> vResult = new Vector<Vector<String>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getMBConfigurationData");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, nConfID);
            pstmt.setInt(2, nConfID);
            rs = pstmt.executeQuery();

            rs.next();
            Vector<String> vecTemp = new Vector<String>();
            if (rs.getString("Name").equals("Default")) {
                vecTemp.add(Terminal.sql_bundle.getString("defaultMBConfigurationTitle"));
            } else {
                vecTemp.add(rs.getString("Name"));
            }
            vecTemp.add(rs.getString("Description"));
            vResult.add(vecTemp);

            pstmt.getMoreResults();
            rs = pstmt.getResultSet();

            while (rs.next()) {
                vecTemp = new Vector<String>();
                vecTemp.add(rs.getString("ConfigurationDataID"));
                vecTemp.add(rs.getString("Name"));
                vecTemp.add(rs.getString("Value"));
                vResult.add(vecTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getMBConfigurationData", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vResult;
    }

    public boolean saveMBConfigurationData(Vector<Vector<String>> vData) throws TerminalException {
        boolean bResult = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            dbConn.setAutoCommit(false);
            String strSQL = Terminal.sql_bundle.getString("saveMBConfigurationData");
            pstmt = dbConn.prepareStatement(strSQL);
            Iterator<Vector<String>> itData = vData.iterator();
            while (itData.hasNext()) {
                Vector<String> vItem = itData.next();
                pstmt.setString(1, vItem.get(0));
                pstmt.setString(2, vItem.get(1));
                pstmt.setInt(3, Integer.parseInt(vItem.get(2)));
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
            bResult = true;
        } catch (Exception e) {
            Terminal.cat.error("Error during saveMBConfigurationData", e);
            throw new TerminalException();
        } finally {
            try {
                dbConn.setAutoCommit(true);
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    }

    public boolean deleteMBConfigurationData(int nId) throws TerminalException {
        boolean bResult = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("deleteMBConfigurationData");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, nId);
            pstmt.executeUpdate();
            pstmt.close();
            bResult = true;
        } catch (Exception e) {
            Terminal.cat.error("Error during deleteMBConfigurationData", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    }

    public boolean addMBConfigurationData(int nConfId, String sName, String sValue) throws TerminalException {
        boolean bResult = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("addMBConfigurationData");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, nConfId);
            pstmt.setString(2, sName.replaceAll("'", "''"));
            pstmt.setString(3, sValue.replaceAll("'", "''"));
            pstmt.executeUpdate();
            pstmt.close();
            bResult = true;
        } catch (Exception e) {
            Terminal.cat.error("Error during addMBConfigurationData", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    }

    public boolean saveMBConfiguration(int nConfId, String sDesc) throws TerminalException {
        boolean bResult = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("saveMBConfiguration");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, sDesc.replaceAll("'", "''"));
            pstmt.setInt(2, nConfId);
            pstmt.executeUpdate();
            pstmt.close();
            bResult = true;
        } catch (Exception e) {
            Terminal.cat.error("Error during saveMBConfiguration", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    }

    public boolean deleteMBConfiguration(int nId) throws TerminalException {
        boolean bResult = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("deleteMBConfiguration");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setInt(1, nId);
            pstmt.setInt(2, nId);
            pstmt.executeUpdate();
            pstmt.close();
            bResult = true;
        } catch (Exception e) {
            Terminal.cat.error("Error during deleteMBConfiguration", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    }

    public int addMBConfiguration(String sName, String sSiteId, String sMerchantId) throws TerminalException {
        int nResult = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("addMBConfiguration");
            if (sSiteId == null) {
                strSQL = strSQL.replaceAll("_SITEID_", "NULL");
            } else {
                strSQL = strSQL.replaceAll("_SITEID_", sSiteId);
            }
            pstmt = dbConn.prepareStatement(strSQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, sName.replaceAll("'", "''"));
            pstmt.executeUpdate();
            rs = pstmt.getGeneratedKeys();
            rs.next();
            nResult = rs.getInt(1);
            rs.close();
            pstmt.close();

            strSQL = Terminal.sql_bundle.getString("addMBConfigurationData");
            pstmt = dbConn.prepareStatement(strSQL);
            if (sSiteId == null) {
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "ADMPWD");
                pstmt.setString(3, "1234");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "AUTO_JOUR");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "DAY_DB");
                pstmt.setString(3, "0");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "PRINT_REC");
                pstmt.setString(3, "0");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "REPORT_LOG");
                pstmt.setString(3, "0");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "LOGO_APP");
                pstmt.setString(3, "0");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "PREDIAL");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "RCP_COPY");
                pstmt.setString(3, "0");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "DISCONN");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "FLAG_AB");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "FLAG_SC");
                pstmt.setString(3, "0");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "FLAG_AS");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "FLAG_AGP");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "RAND_HRS");
                pstmt.setString(3, "4");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "NIG_CALL_START");
                pstmt.setString(3, "221500");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "MIN_RETRY");
                pstmt.setString(3, "3");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "MBAUTHR");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "FLAG_MD");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "LANG");
                pstmt.setString(3, "ENG");
                pstmt.addBatch();
            } else {
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "NIG_CALL");
                pstmt.setString(3, "2300");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "$NIG_CALL_ID");
                pstmt.setString(3, "0");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "MIN_RETRY");
                pstmt.setString(3, "3");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "MBAUTHR");
                pstmt.setString(3, "1");
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "FLAG_MD");
                {//The following block enables the Dynamic Menu for all the terminals except
                    //those of MB Verifone 3740/50 DialUp/IP.
                    Terminal t = new Terminal();
                    t.setMerchantId(sMerchantId);
                    t.setSiteId(sSiteId);
                    int nType = Integer.parseInt(t.getTerminalInfo().get(1).toString());
                    if (nType >= 69 && nType <= 72) {
                        pstmt.setString(3, "0");
                    } else {
                        pstmt.setString(3, "1");
                    }
                    t = null;
                }
                pstmt.addBatch();
                pstmt.setInt(1, nResult);
                pstmt.setString(2, "LANG");
                pstmt.setString(3, "ENG");
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during addMBConfiguration", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return nResult;
    }

    public int addMBConfigurationExt(String sName, String sSiteId, String sMerchantId, String configID, boolean bAll) throws TerminalException {
        int nResult = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Terminal t = new Terminal();
        int nType = -1;

        try {

            t.setMerchantId(sMerchantId);
            t.setSiteId(sSiteId);
            nType = Integer.parseInt(t.getTerminalInfo().get(1).toString());
            if (((nType >= 69 && nType <= 72) || bAll) && configID != null) {
                Vector<Vector<String>> vData = null;
                vData = this.getMBConfigurationData(Integer.parseInt(configID));

                dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

                if (dbConn == null) {
                    Terminal.cat.error("Can't get database connection");
                    throw new TerminalException();
                }

                String strSQL = Terminal.sql_bundle.getString("addMBConfiguration");
                if (sSiteId == null) {
                    strSQL = strSQL.replaceAll("_SITEID_", "NULL");
                } else {
                    strSQL = strSQL.replaceAll("_SITEID_", sSiteId);
                }
                pstmt = dbConn.prepareStatement(strSQL, Statement.RETURN_GENERATED_KEYS);
                pstmt.setString(1, sName.replaceAll("'", "''"));
                pstmt.executeUpdate();
                rs = pstmt.getGeneratedKeys();
                rs.next();
                nResult = rs.getInt(1);
                rs.close();
                pstmt.close();

                strSQL = Terminal.sql_bundle.getString("addMBConfigurationData");
                pstmt = dbConn.prepareStatement(strSQL);
                if (sSiteId != null) {
                    boolean bNigCall = false;
                    boolean bMinRetry = false;
                    boolean bMBAuthR = false;
                    boolean bLanguage = false;
                    Iterator<Vector<String>> it = vData.iterator();
                    it.next();
                    while (it.hasNext()) {
                        Vector<String> vTmp = it.next();

                        if ((vTmp.get(1).equals("NIG_CALL_START")) || (vTmp.get(1).equals("MIN_RETRY"))
                                || (vTmp.get(1).equals("MBAUTHR")) || (vTmp.get(1).equals("LANG"))) {
                            pstmt.setInt(1, nResult);
                            if (vTmp.get(1).equals("NIG_CALL_START")) {
                                pstmt.setString(2, "NIG_CALL");
                                bNigCall = true;
                            } else {
                                pstmt.setString(2, vTmp.get(1));
                                if (vTmp.get(1).equals("MIN_RETRY")) {
                                    bMinRetry = true;
                                } else if (vTmp.get(1).equals("MBAUTHR")) {
                                    bMBAuthR = true;
                                } else if (vTmp.get(1).equals("LANG")) {
                                    bLanguage = true;
                                }
                            }
                            pstmt.setString(3, vTmp.get(2));
                            pstmt.addBatch();
                        }
                    }
                    it = null;
                    t = null;
                    //The following block enables the Dynamic Menu for all the terminals except
                    //those of MB Verifone 3740/50 DialUp/IP
                    pstmt.setInt(1, nResult);
                    pstmt.setString(2, "FLAG_MD");
                    if (nType >= 69 && nType <= 72) {
                        pstmt.setString(3, "0");
                    } else {
                        pstmt.setString(3, "1");
                    }
                    pstmt.addBatch();
                    pstmt.setInt(1, nResult);
                    pstmt.setString(2, "$NIG_CALL_ID");
                    pstmt.setString(3, "0");
                    pstmt.addBatch();
                    if (!bMinRetry) {
                        pstmt.setInt(1, nResult);
                        pstmt.setString(2, "MIN_RETRY");
                        pstmt.setString(3, "3");
                        pstmt.addBatch();
                    }
                    if (!bMBAuthR) {
                        pstmt.setInt(1, nResult);
                        pstmt.setString(2, "MBAUTHR");
                        pstmt.setString(3, "1");
                        pstmt.addBatch();
                    }
                    if (!bNigCall) {
                        pstmt.setInt(1, nResult);
                        pstmt.setString(2, "NIG_CALL");
                        pstmt.setString(3, "2300");
                        pstmt.addBatch();
                    }
                    if (!bLanguage) {
                        pstmt.setInt(1, nResult);
                        pstmt.setString(2, "LANG");
                        pstmt.setString(3, "ENG");
                        pstmt.addBatch();
                    }
                }
                pstmt.executeBatch();
                pstmt.close();
            }
        } catch (Exception e) {
            Terminal.cat.error("Error during addMBConfigurationExt", e);
            throw new TerminalException();
        } finally {
            try {
                if ((nType >= 69 && nType <= 72) || bAll) {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                }
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return nResult;
    }

    public Vector<Vector<String>> getMBConfigurationDataByTerminal(String sSiteId) throws TerminalException {
        Vector<Vector<String>> vResult = new Vector<Vector<String>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getMBConfigurationDataByTerminal");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, sSiteId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(rs.getString("ConfigurationDataID"));
                vecTemp.add(rs.getString("Name"));
                vecTemp.add(rs.getString("Value"));
                vResult.add(vecTemp);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getMBConfigurationDataByTerminal", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return vResult;
    }

    public int getMBConfigurationByTerminal(String sSiteId) throws TerminalException {
        int nResult = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = Terminal.sql_bundle.getString("getMBConfigurationByTerminal");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setString(1, sSiteId);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                nResult = rs.getInt("ConfigurationID");
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getMBConfigurationByTerminal", e);
            throw new TerminalException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return nResult;
    }

    /* END Release 32 - Added by LF */
    /**
     * @param carrierId the carrierId to set
     */
    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    /**
     * @return the carrierId
     */
    public String getCarrierId() {
        return carrierId;
    }

    /**
     * @return the terminalLabel
     */
    public String getTerminalLabel() {
        return terminalLabel;
    }

    /**
     * @param terminalLabel the terminalLabel to set
     */
    public void setTerminalLabel(String terminalLabel) {
        this.terminalLabel = terminalLabel;
    }

    public void load(SessionData SessionData, ServletContext application) throws TerminalException, CustomerException {
        String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
        if (strCustomConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
            Vector vecTerminalInfo = getTerminalInfoMx();
            if (vecTerminalInfo != null && vecTerminalInfo.size() > 0) {
                setTerminalType(vecTerminalInfo.get(1).toString());
                // David Castro Get Marketplace terminal Info for Group and Key. vecTerminalInfo comes from getTerminalInfoMx which is used in all deployments :S
                if (isMarketPlaceTerminal(getTerminalType()) == true) {
                    setMarketPlaceKey(new Integer(vecTerminalInfo.get(16).toString()));
                    setMarketPlaceGroup(new Integer(vecTerminalInfo.get(17).toString()));
                    setSMarketPlaceTerminal(true);
                }
                setAnnualInsuranceFee(vecTerminalInfo.get(2).toString());
                setAnnualInsuranceTax(vecTerminalInfo.get(3).toString());
                setAnnualMaintenanceFee(vecTerminalInfo.get(4).toString());
                setAnnualMaintenanceTax(vecTerminalInfo.get(5).toString());
                setReconnectionFee(vecTerminalInfo.get(6).toString());
                setReconnectionTax(vecTerminalInfo.get(7).toString());
                setAnnualAffiliationFee(vecTerminalInfo.get(8).toString());
                setAnnualAffiliationTax(vecTerminalInfo.get(9).toString());
                setSerialNumber(StringUtil.toString(vecTerminalInfo.get(10).toString()));
                setSimData(StringUtil.toString(vecTerminalInfo.get(11).toString()));
                setImciData(StringUtil.toString(vecTerminalInfo.get(12).toString()));
                setTermVersion(StringUtil.toString(vecTerminalInfo.get(13).toString()));
                setTermNote(StringUtil.toString(vecTerminalInfo.get(14).toString()));
                setEnablePINCache(Boolean.parseBoolean(vecTerminalInfo.get(18).toString()));
                if (vecTerminalInfo.get(15) != null && !vecTerminalInfo.get(15).equals("")) {
                    CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
                    String strIdTerminalFisico = vecTerminalInfo.get(15).toString();
                    terminalFisico.loadTerminal(Long.parseLong(strIdTerminalFisico));
                    setTerminalFisico(terminalFisico);
                } else {
                    setTerminalFisico(null);
                }
                setStatus(vecTerminalInfo.get(20).toString());
                setStatusLastChange(vecTerminalInfo.get(21).toString());
                setRatePlan(vecTerminalInfo.get(22).toString());
                setTerminalDescription(vecTerminalInfo.get(23).toString());
            } else {
                vecTerminalInfo = getTerminalInfo();
                if (vecTerminalInfo != null && vecTerminalInfo.size() > 0) {
                    setMonthlyFee(vecTerminalInfo.get(2).toString());
                }
            }
            //End of if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
        } else {
            Vector vecTerminalInfo = getTerminalInfo();
            setTerminalType(vecTerminalInfo.get(1).toString());
            setSerialNumber(StringUtil.toString(vecTerminalInfo.get(3).toString()));
            setSimData(StringUtil.toString(vecTerminalInfo.get(4).toString()));
            setImciData(StringUtil.toString(vecTerminalInfo.get(5).toString()));
            setTermVersion(StringUtil.toString(vecTerminalInfo.get(6).toString()));
            setTermNote(StringUtil.toString(vecTerminalInfo.get(7).toString()));
            setEnbCachedRcp(StringUtil.toString(vecTerminalInfo.get(8).toString()) == "true" ? true : false);
            setEnablePINCache(Boolean.parseBoolean(vecTerminalInfo.get(9).toString()));
            setStatus(vecTerminalInfo.get(10).toString());
            setStatusLastChange(vecTerminalInfo.get(11).toString());
            setRatePlan(vecTerminalInfo.get(12).toString());

            SessionData.setProperty("terminalType", vecTerminalInfo.get(1).toString());

            if (vecTerminalInfo != null && vecTerminalInfo.size() > 0) {
                setMonthlyFee(vecTerminalInfo.get(2).toString());
                Merchant m2 = new Merchant();
                m2.setMerchantId(getMerchantId());
                m2.getMerchant(SessionData, application);
                setMonthlyFeeThreshold(m2.getMonthlyFeeThreshold());
            }
            setTerminalDescription(vecTerminalInfo.get(13).toString());
            setCarrierId(vecTerminalInfo.get(14).toString());
            setTerminalLabel(vecTerminalInfo.get(15).toString());
            vecTerminalInfo = getTerminalInfoMx();
            // David Castro Get Marketplace terminal Info for Group and Key. 
            //vecTerminalInfo comes from getTerminalInfoMx which is used in all deployments :S
            if (isMarketPlaceTerminal(getTerminalType()) == true) {
                setMarketPlaceKey(new Integer(vecTerminalInfo.get(16).toString()));
                setMarketPlaceGroup(new Integer(vecTerminalInfo.get(17).toString()));
                setSMarketPlaceTerminal(true);
            }
            if (vecTerminalInfo != null && vecTerminalInfo.size() > 0) {
                if (vecTerminalInfo.get(15) != null && !vecTerminalInfo.get(15).equals("")) {
                    CPhysicalTerminal terminalFisico = new CPhysicalTerminal();
                    String strIdTerminalFisico = vecTerminalInfo.get(15).toString();
                    terminalFisico.loadTerminal(Long.parseLong(strIdTerminalFisico));
                    setTerminalFisico(terminalFisico);
                } else {
                    setTerminalFisico(null);
                } //END JFM 26-06-2008 - [DBSY-32] TERMINAL TRACKING FEATURE
            }
        }

    }

    public static List<Base> getReceiptTypes(String language) {
        int nResult = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<Base> listTypes = new ArrayList<Base>();
        try {
            dbConn = TorqueHelper.getConnection(Terminal.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Terminal.cat.error("Can't get database connection");
                throw new TerminalException();
            }

            String strSQL = "select Id,ReceiptCode,EngDescription,SpaDescription from dbo.ReceiptTypes";
            //Terminal.sql_bundle.getString("getMBConfigurationByTerminal");
            pstmt = dbConn.prepareStatement(strSQL);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Base recType = new Base();
                recType.setId(rs.getString("Id"));
                if (language == "1") {
                    recType.setDescription(rs.getString("EngDescription"));
                } else {
                    recType.setDescription(rs.getString("SpaDescription"));
                }
                listTypes.add(recType);
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Terminal.cat.error("Error during getReceiptTypes", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Terminal.cat.error("Error during closeConnection", e);
            }
        }

        return listTypes;
    }

    /**
     * @return the cacheRecetiptType
     */
    public String getCacheRecetiptType() {
        return cacheRecetiptType;
    }

    /**
     * @param cacheRecetiptType the cacheRecetiptType to set
     */
    public void setCacheRecetiptType(String cacheRecetiptType) {
        this.cacheRecetiptType = cacheRecetiptType;
    }

    public String getTxtConfirmationPhoneNumber() {
        return txtConfirmationPhoneNumber;
    }

    public void setTxtConfirmationPhoneNumber(String txtConfirmationPhoneNumber) {
        this.txtConfirmationPhoneNumber = txtConfirmationPhoneNumber;
    }

    public String getTxtConfirmationEMail() {
        return txtConfirmationEMail;
    }

    public void setTxtConfirmationEMail(String txtConfirmationEMail) {
        this.txtConfirmationEMail = txtConfirmationEMail;
    }

    public boolean isChkConfirmationBySMS() {
        return chkConfirmationBySMS;
    }

    public void setChkConfirmationBySMS(boolean chkConfirmationBySMS) {
        this.chkConfirmationBySMS = chkConfirmationBySMS;
    }

    public boolean isChkConfirmationByEmail() {
        return chkConfirmationByEmail;
    }

    public void setChkConfirmationByEmail(boolean chkConfirmationByEmail) {
        this.chkConfirmationByEmail = chkConfirmationByEmail;
    }

    public boolean isChkConfirmationByUser() {
        return chkConfirmationByUser;
    }

    public void setChkConfirmationByUser(boolean chkConfirmationByUser) {
        this.chkConfirmationByUser = chkConfirmationByUser;
    }

    /**
     * @return the resetAnswers
     */
    public boolean isResetAnswers() {
        return resetAnswers;
    }

    /**
     * @param resetAnswers the resetAnswers to set
     */
    public void setResetAnswers(boolean resetAnswers) {
        this.resetAnswers = resetAnswers;
    }
}
