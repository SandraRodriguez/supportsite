package com.debisys.terminals;

import java.sql.Date;

/**
 * 
 * @author dgarzon
 *
 */
public class TerminalRegistrationRegCodeLog {

	private int id;
	private String registrationCode;
	private Date RegCodeDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegistrationCode() {
		return registrationCode;
	}
	public void setRegistrationCode(String registrationCode) {
		this.registrationCode = registrationCode;
	}
	public Date getRegCodeDate() {
		return RegCodeDate;
	}
	public void setRegCodeDate(Date regCodeDate) {
		RegCodeDate = regCodeDate;
	}
	
	
	
}
