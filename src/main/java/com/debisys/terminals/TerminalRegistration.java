/**
 * 
 */
package com.debisys.terminals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.TerminalException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.eaio.uuid.UUID;

/**
 * @author nmartinez
 *
 */
public class TerminalRegistration {

	static Logger cat = Logger.getLogger(Terminal.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
	
	private Hashtable validationErrors = new Hashtable();
	
	
	/**
	 * @param javaHandSet
	 * @param request
	 * @throws TerminalException
	 */
	public static boolean updatePCTerminalRegistrationNokiaS40(JavaHandSet javaHandSet, HttpServletRequest request,SessionData sessionData) throws TerminalException 
	{
		boolean resultUpdate=false;
		Connection dbConn = null;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new TerminalException();
			}
			int terminalId = Integer.parseInt(javaHandSet.getSiteId());
			
			dbConn.setAutoCommit(false);
			
			String sUpdateInfo = "";
			
			sUpdateInfo = "[millennium_no=" + terminalId + "] [RegistrationCode=" + javaHandSet.getJavaHandSetRegistrationCode() + "] ";
			sUpdateInfo += ("[UserId=" + javaHandSet.getJavaHandSetUserName() + "] [Password=" + javaHandSet.getJavaHandSetPassword() + "] ");
			sUpdateInfo += ("[CustomConfigId=" + javaHandSet.getJavaHandSetBranding() + "] ");
			
			String moreUpdateColumns="";
			String sqlUpdate = sql_bundle.getString("updateTerminalRegistrationJavaNokia");
			
			if (javaHandSet.isReCreateRegistrationCod())
			{
				moreUpdateColumns = ", RegistrationCode=? ";
			}
			sqlUpdate = sqlUpdate.replaceFirst("_REPLACEMORECOLUMS_", moreUpdateColumns);
			cat.info(sqlUpdate);
			
			PreparedStatement pstmt = dbConn.prepareStatement(sqlUpdate);
			
			pstmt.setString(1, javaHandSet.getJavaHandSetBranding());
			
			if (javaHandSet.isReCreateRegistrationCod())
			{
				String uui = TerminalRegistration.getUUID();
				javaHandSet.setJavaHandSetRegistrationCode(uui);
				pstmt.setString(2,javaHandSet.getJavaHandSetRegistrationCode()); 
				pstmt.setInt(3, javaHandSet.getId());
			}
			else
			{
				pstmt.setInt(2, javaHandSet.getId());
			}
			
			pstmt.executeUpdate();
			pstmt.close();		

			dbConn.commit();
			dbConn.setAutoCommit(true);
						
			resultUpdate=true;
		} 
		catch (SQLException ex) 
		{
			cat.error("Error during updatePCTerminalRegistrationNokiaS40 SQLException: ", ex);
			
			try 
			{
				if (dbConn != null) 
				{
					dbConn.rollback();
				}
			} 
			catch (SQLException sqex) 
			{
				cat.error("Error during rollback updatePCTerminalRegistrationNokiaS40 ", sqex);
			}
			
			if (ex.getMessage().indexOf("UserData") > 0 && ex.getMessage().indexOf("pk_UserData") > 0) 
			{
				cat.error("updatePCTerminalRegistrationNokiaS40 DataBase PK: "+Languages.getString("com.debisys.users.error_username_duplicate", sessionData.getLanguage()));
			} 
			else 
			{
			    cat.error("updatePCTerminalRegistrationNokiaS40 DataBase: "+ Languages.getString("jsp.admin.terminal.general_update_error", sessionData.getLanguage()));
			}
		} 
		catch (Exception e) 
		{
			try 
			{
				if (dbConn != null) 
				{
					dbConn.rollback();
				}
			} 
			catch (SQLException ex) 
			{
				cat.error("Error during rollback updatePCTerminalRegistrationNokiaS40 ", ex);
			}
			cat.error("Error during updatePCTerminalRegistrationNokiaS40 ", e);
			throw new TerminalException();
		} 
		finally 
		{
			try 
			{
				Torque.closeConnection(dbConn);
			} 
			catch (Exception e) 
			{
				cat.error("Error during release connection updatePCTerminalRegistrationNokiaS40 ", e);
			}
		}
		return resultUpdate;
	}

	
	
	/**
	 * Sets the validation error against a specific field. Used by
	 * {@link #validateTerminal validateRatePlan}.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @param error
	 *            The error message for the field or null if none is present.
	 */
	public void addFieldError(String fieldname, String error) {
		if (!this.validationErrors.containsKey(fieldname)) {
			this.validationErrors.put(fieldname, error);
		}
	}
	
	/**
	 * @return
	 */
	public static String getUUID()
	{		
		UUID u = new UUID();
		//System.out.println(u);
		String ui = u.toString().replaceAll("-", "");
		//if (isValidRegistrationCode(ui)){
		//	return ui;
		//}
		//else{
		//	return getUUID();
		//}
		return ui.substring(0,10);
	}



	private static boolean isValidRegistrationCode(String ui) {
		Connection dbConn = null;
		boolean result=false;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new TerminalException();
			}
			PreparedStatement pstmt = dbConn.prepareStatement("select RegistrationCode from terminal_registration where RegistrationCode=?");
			pstmt.setString(1, ui);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
			{
				
			}
			
		}
		catch (Exception e) 
		{
			cat.error("Error during release connection updatePCTerminalRegistrationNokiaS40 ", e);
		}
		return result;
	}
	
}
