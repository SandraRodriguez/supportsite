/**
 * 
 */
package com.debisys.terminals;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.TerminalException;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author nmartinez
 *
 */
public class TerminalMapping {

	public final static String InsertTerminalMapping="I";
	public final static String UpdateTerminalMapping="U";
	public final static String DeleteTerminalMapping="D";
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminals.sql");
	static Category cat = Category.getInstance(TerminalMapping.class);
	
	private int providerId;
	private int debisysTerminalId;
	private String providerTerminalId;
	private String providerMerchantId;
	private String ProviderTerminalLocation;
	private String ProviderTerminalData;
	private String action;
	private int currentProviderId;
	
	/**
	 * @return the providerId
	 */
	public int getProviderId() {
		return providerId;
	}
	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(int providerId) {
		this.providerId = providerId;
	}
	/**
	 * @return the debisysTerminalId
	 */
	public int getDebisysTerminalId() {
		return debisysTerminalId;
	}
	/**
	 * @param debisysTerminalId the debisysTerminalId to set
	 */
	public void setDebisysTerminalId(int debisysTerminalId) {
		this.debisysTerminalId = debisysTerminalId;
	}
	/**
	 * @return the providerTerminalId
	 */
	public String getProviderTerminalId() {
		return providerTerminalId;
	}
	/**
	 * @param providerTerminalId the providerTerminalId to set
	 */
	public void setProviderTerminalId(String providerTerminalId) {
		this.providerTerminalId = providerTerminalId;
	}
	/**
	 * @return the providerMerchantId
	 */
	public String getProviderMerchantId() {
		return providerMerchantId;
	}
	/**
	 * @param providerMerchantId the providerMerchantId to set
	 */
	public void setProviderMerchantId(String providerMerchantId) {
		this.providerMerchantId = providerMerchantId;
	}
	/**
	 * @return the providerTerminalLocation
	 */
	public String getProviderTerminalLocation() {
		return ProviderTerminalLocation;
	}
	/**
	 * @param providerTerminalLocation the providerTerminalLocation to set
	 */
	public void setProviderTerminalLocation(String providerTerminalLocation) {
		ProviderTerminalLocation = providerTerminalLocation;
	}
	/**
	 * @return the providerTerminalData
	 */
	public String getProviderTerminalData() {
		return ProviderTerminalData;
	}
	/**
	 * @param providerTerminalData the providerTerminalData to set
	 */
	public void setProviderTerminalData(String providerTerminalData) {
		ProviderTerminalData = providerTerminalData;
	}
	
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param currentProviderId the currentProviderId to set
	 */
	public void setCurrentProviderId(int currentProviderId) {
		this.currentProviderId = currentProviderId;
	}
	/**
	 * @return the currentProviderId
	 */
	public int getCurrentProviderId() {
		return currentProviderId;
	}
	
	
	/**
	 * 
	 * @param siteId
	 * @param refId
	 * @return
	 */
	public static ArrayList<TerminalMapping> getListTerminalMappingBySiteId(int siteId,String refId)
	{
		ArrayList<TerminalMapping> listMappings = new ArrayList<TerminalMapping>();
		Connection dbConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TerminalException();
			}
										
			String strSQL = sql_bundle.getString("getListTerminalMappingBySiteId");
			pst = dbConn.prepareStatement(strSQL);
			
			pst.setInt(1, siteId);
			pst.setString(2, refId);
			
			rs = pst.executeQuery();
			while (rs.next())
			{
				TerminalMapping tm = new TerminalMapping();
				tm.setDebisysTerminalId(siteId);
				tm.setProviderId(rs.getInt(1));
				tm.setProviderMerchantId(rs.getString(4));
				tm.setProviderTerminalData(rs.getString(6));
				tm.setProviderTerminalId(rs.getString(3));
				tm.setProviderTerminalLocation(rs.getString(5));
			
				listMappings.add(tm);
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getListTerminalMappingBySiteId", e);
			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pst, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection in getListTerminalMappingBySiteId", e);
			}
		}
		
		
		return listMappings;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public int save()
	{	    
		Connection dbConn = null;
		PreparedStatement pst = null;
		int result=1;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TerminalException();
			}			
			
			String strSQL="";		
			if ( this.getAction().equals(TerminalMapping.InsertTerminalMapping) )
			{
				strSQL = sql_bundle.getString("addNewProviderTerminalMappingBySiteId");
				pst = dbConn.prepareStatement(strSQL);	
				pst.setInt(1, this.getProviderId());
				pst.setInt(2, this.getDebisysTerminalId());
				pst.setString(3, this.getProviderTerminalId());				
				pst.setString(4, this.getProviderMerchantId());
				pst.setNull(5, Types.VARCHAR);//ProviderTerminalLocation goes null by now
				pst.setString(6, this.getProviderTerminalData());
			}
			else if ( this.getAction().equals(TerminalMapping.UpdateTerminalMapping) )
			{
				strSQL = sql_bundle.getString("updateTerminalMappingBySiteId");
				pst = dbConn.prepareStatement(strSQL);	
				pst.setInt(1, this.getProviderId());
				pst.setString(2, this.getProviderTerminalId());				
				pst.setString(3, this.getProviderMerchantId());
				pst.setString(4, this.getProviderTerminalData());
				pst.setInt(5, this.getCurrentProviderId());
				pst.setInt(6, this.getDebisysTerminalId());
				
			}
			else if ( this.getAction().equals(TerminalMapping.DeleteTerminalMapping) )
			{
				strSQL = sql_bundle.getString("deleteTerminalMappingBySiteId");
				pst = dbConn.prepareStatement(strSQL);
				pst.setInt(1, this.getCurrentProviderId());
				pst.setInt(2, this.getDebisysTerminalId());
			}			
			
			if (pst.executeUpdate()==1)
			{
				cat.info("save executed successfully ["+strSQL+"]");
			}
			else
			{
				result=-2;
			}
			pst.close();
			pst=null;
		}
		catch (Exception e)
		{
			result=-1;
			cat.error("Error during getListTerminalMappingBySiteId", e);			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection in getListTerminalMappingBySiteId", e);
			}
		}
		return result;			
	}
	
	
	/**
	 * 
	 * @param dbConn
	 * @return
	 */
	public int createNewTerminalMapping(Connection dbConn)
	{	    
		PreparedStatement pst = null;
		int result=1;
		
		try
		{
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TerminalException();
			}			
			
			String strSQL="";		
			if ( this.getAction().equals(TerminalMapping.InsertTerminalMapping) )
			{
				strSQL = sql_bundle.getString("addNewProviderTerminalMappingBySiteId");
				pst = dbConn.prepareStatement(strSQL);	
				pst.setInt(1, this.getProviderId());
				pst.setInt(2, this.getDebisysTerminalId());
				pst.setString(3, this.getProviderTerminalId());				
				pst.setString(4, this.getProviderMerchantId());
				pst.setNull(5, Types.VARCHAR);//ProviderTerminalLocation goes null by now
				pst.setString(6, this.getProviderTerminalData());
			}					
			
			if (pst.executeUpdate()==1)
			{
				cat.info("creation executed successfully ["+strSQL+"]");
			}
			else
			{
				result=-2;
			}
			pst.close();
			pst=null;
		}
		catch (Exception e)
		{
			result=-1;
			cat.error("Error during createNewTerminalMapping", e);			
		}
		finally
		{
			try
			{
				if ( pst != null)
				{
					pst.close();
				}
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection in createNewTerminalMapping", e);
			}
		}
		return result;			
	}
	
	
}
