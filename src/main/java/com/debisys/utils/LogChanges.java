package com.debisys.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author
 */
@SuppressWarnings("deprecation")
public class LogChanges {
	
	private SessionData sessionData = null;

	private ServletContext context = null;
	
	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTTYPE = "Payment Type";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTPROCESSOR = "Payment Processor";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ABA = "ABA/Routing Number";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ACCOUNTNUMBER = "Account Number";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_TAXID = "TaxID";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITTYPE = "Sales Limit Type";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITMONDAY = "Sales Limit Monday";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITTUESDAY = "Sales Limit Tuesday";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITWEDNESDAY = "Sales Limit Wednesday";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITTHURSDAY = "Sales Limit Thursday";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITFRIDAY = "Sales Limit Friday";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITSATURDAY = "Sales Limit Saturday";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITSUNDAY = "Sales Limit Sunday";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_SALESLIMITLIABILITYLIMIT = "Sales Limit Liability Limit";
	/**
	 * 
	 */
	public static String LOGCHANGETYPE_REGULATORYFEEID = "Regulatory Fee";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_RATEPLANASSIGNMENT = "RatePlan Assignment";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_RATEPLANPRODUCTADDITION = "RatePlan Product Addition";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_RATEPLANPRODUCTREMOVAL = "RatePlan Product Removal";
	
	
	/**
	 * 	emunoz for audit
	 */
	public static String LOGCHANGETYPE_PAYMENTTYPE_AGENT = "Payment Type Agent";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTPROCESSOR_AGENT = "Payment Processor Agent";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ABA_AGENT = "ABA/Routing Number Agent";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ACCOUNTNUMBER_AGENT = "Account Number Agent";
	/**
	 * 
	 */
	public static String LOGCHANGETYPE_TAXID_AGENT = "TaxID Agent";
	
	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTTYPE_SUBAGENT = "Payment Type SubAgent";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTPROCESSOR_SUBAGENT = "Payment Processor SubAgent";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ABA_SUBAGENT = "ABA/Routing Number SubAgent";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ACCOUNTNUMBER_SUBAGENT = "Account Number SubAgent";
	/**
	 * 
	 */
	public static String LOGCHANGETYPE_TAXID_SUBAGENT = "TaxID SubAgent";
	/**
	 * 
	 */
	
	public static String LOGCHANGETYPE_PAYMENTTYPE_REP = "Payment Type Rep";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTPROCESSOR_REP = "Payment Processor Rep";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ABA_REP = "ABA/Routing Number Rep";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ACCOUNTNUMBER_REP = "Account Number Rep";
	
	
	public static String LOGCHANGETYPE_TAXID_REP = "TaxID Rep";
	
	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTTYPE_ISO = "Payment Type Iso";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_PAYMENTPROCESSOR_ISO = "Payment Processor Iso";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ABA_ISO= "ABA/Routing Number Iso";

	/**
	 * 
	 */
	public static String LOGCHANGETYPE_ACCOUNTNUMBER_ISO = "Account Number Iso";
	
	/**
	 * 
	 */
	public static String LOGCHANGETYPE_TAXID_ISO = "TaxID Iso";
	
	/**
	 * PCTerminal Username
	 */
	public static String LOGCHANGETYPE_TERM_USERNAME = "Terminal Username";
	
	/**
	 * PCTerminal Password
	 */
	public static String LOGCHANGETYPE_TERM_PASSWORD = "Terminal Password";
	
	/**
	 * PCTerminal Enforce Certificate
	 */
	public static String LOGCHANGETYPE_TERM_ENFORCECERT = "Terminal Enforce Certificate";

	/**
	 * PCTerminal Allow Certificate Install
	 */
	public static String LOGCHANGETYPE_TERM_CERTINSTALL = "Terminal Allow Certificate Install";

	/**
	 * PCTerminal Certificate Install Remaining Amount
	 */
	public static String LOGCHANGETYPE_TERM_CERTREMAINING = "Terminal Remaining Amount";

	/**
	 * PCTerminal Registration Enforce Code
	 */
	public static String LOGCHANGETYPE_TERM_ENFORCEREGCODE = "Terminal Enforce RegCode";

	/**
	 * PCTerminal Registration Code
	 */
	public static String LOGCHANGETYPE_TERM_REGCODE = "Terminal RegCode";
	
	/**
	 * Terminal Clerk Code
	 */
	public static String LOGCHANGETYPE_TERM_CLERKCODE = "Terminal Clerk Code";

	/**
	 * Terminal Clerk Name
	 */
	public static String LOGCHANGETYPE_TERM_CLERKNAME = "Terminal Clerk Name";
	
	// DBSY-1072 eAccounts Interface
	public static String LOGCHANGETYPE_ENTITYACCOUNTTYPE_ISO = "Entity Account Type ISO";
	
	public static String LOGCHANGETYPE_ENTITYACCOUNTTYPE_AGENT = "Entity Account Type Agent";
	
	public static String LOGCHANGETYPE_ENTITYACCOUNTTYPE_SUBAGENT = "Entity Account Type SubAgent";
	
	public static String LOGCHANGETYPE_ENTITYACCOUNTTYPE_REP = "Entity Account Type Rep";
	
	public static String LOGCHANGETYPE_ENTITYACCOUNTTYPE_MERCHANT = "Entity Account Type Merchant";
	// END DBSY-1072
	
	//DBSY-1057 Juan Cruz
	public static String LOGCHANGETYPE_RATEPLAN="Rate Plan";
	
	public static String LOGCHANGETYPE_SERIALNO="Serial No";
	//
	

	/**
	 * 
	 */
	static Category cat = Category.getInstance(LogChanges.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
	
	/**
	 * 
	 */
	public LogChanges(){

	}
	
	/**
	 * @param sd
	 */
	public LogChanges(SessionData sd){
		this.sessionData = sd;
	}
	
	/**
	 * @param sd
	 */
	public void setSession(SessionData sd){
		this.sessionData = sd;
	}
	
	public SessionData getSession(){
		return this.sessionData;
	}
	
	/**
	 * @param cntx
	 */
	public void setContext(ServletContext cntx){
		this.context = cntx;
	}
	
	/**
	 * @param log_type
	 * @param value
	 */
	public void set_value_for_check(int log_type, String value){
		this.sessionData.setProperty("check." + log_type, value);
	}

	/**
	 * @param log_type
	 * @param value
	 * @param sid
	 */
	public void set_value_for_check(int log_type, String value, String sid){
		this.sessionData.setProperty("check." + log_type + "." + sid, value);
	}

	/**
	 * @param log_type
	 * @param value
	 */
	public void set_value_for_check(String log_type, String value){
		try {
			this.sessionData.setProperty("check." + getLogChangeIdByName(log_type), value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param log_type
	 * @param value
	 * @param sid
	 */
	public void set_value_for_check(String log_type, String value, String sid){
		try {
			this.sessionData.setProperty("check." + getLogChangeIdByName(log_type) + "." + sid, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// After an update request, check if changed all saved fields in session and log the changes
	/**
	 * @param log_type
	 * @param newvalue
	 * @param key_value
	 * @param reason
	 * @return True if the item was changed, False if not
	 * @throws IOException
	 */
	public boolean was_changed(int log_type, String newvalue, String key_value, String reason) throws IOException{
		String oldvalue = this.sessionData.getProperty("check." + log_type);
		if((oldvalue == null && newvalue != null) || !oldvalue.equals(newvalue)){
			log_changed(log_type, key_value, newvalue, oldvalue, reason);
			this.sessionData.setProperty("check." + log_type, newvalue);
			return true;
		}
		return false;
	}
	
	/**
	 * @param log_type
	 * @param newvalue
	 * @param key_value
	 * @param reason
	 * @return True if the item was changed, False if not
	 * @throws Exception 
	 */
	public boolean was_changed(String log_type, String newvalue, String key_value, String reason) throws Exception{
		return was_changed(getLogChangeIdByName(log_type), newvalue, key_value, reason);
	}
	
	/**
	 * @param log_type
	 * @param newvalue
	 * @param key_value
	 * @param reason
	 * @param sid
	 * @return True if the item was changed, False if not
	 * @throws IOException
	 */
	public boolean was_changed(int log_type, String newvalue, String key_value, String reason, String sid) throws IOException{
		String oldvalue = this.sessionData.getProperty("check." + log_type + "." + sid);
		if((oldvalue == null && newvalue != null) || !oldvalue.equals(newvalue)){
			log_changed(log_type, key_value, newvalue, oldvalue, reason);
			this.sessionData.setProperty("check." + log_type, newvalue);
			return true;
		}
		return false;
	}

	/**
	 * @param log_type
	 * @param newvalue
	 * @param key_value
	 * @param reason
	 * @param sid
	 * @return True if the item was changed, False if not
	 * @throws IOException
	 */
	public boolean was_changed(String log_type, String newvalue, String key_value, String reason, String sid) throws Exception{
		return was_changed(getLogChangeIdByName(log_type), newvalue, key_value, reason, sid);
	}
	
	/**
	 * @param log_type
	 * @param key_value
	 * @param newvalue
	 * @param oldvalue
	 * @param reason
	 * @throws IOException
	 */
	public void log_changed(int log_type, String key_value, String newvalue, String oldvalue, String reason) throws IOException{
			Connection dbConn = null;
			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn != null)
				{
					String strSql = sql_bundle.getString("log_field_change");
					PreparedStatement pstmt = null;
					pstmt = dbConn.prepareStatement(strSql);
					pstmt.setInt(1, log_type);
					pstmt.setString(2, key_value);
					pstmt.setString(3, oldvalue);
					pstmt.setString(4, newvalue);
					pstmt.setString(5, this.sessionData.getProperty("username"));
					pstmt.setString(6, DebisysConfigListener.getInstance(this.context));
					pstmt.setString(7, reason);
					pstmt.executeUpdate();
					cat.debug("[LogChanges] log_type= " + log_type+ ", key_value = " + key_value+ ", oldvalue = " + oldvalue+ ", newvalue = " + newvalue + ", reason = " + reason);
				}
		}catch (Exception e){
			cat.error("Error during checking changes", e);
			throw new IOException();
		}finally{
			try{
				Torque.closeConnection(dbConn);
			}catch (Exception e){
				cat.error("Error during closeConnection", e);
			}
		}		
	}

	/**
	 * @param sLogName
	 * @return LogChangeID
	 * @throws Exception
	 */
	
	@SuppressWarnings("static-access")
	public int getLogChangeIdByName(String sLogName) throws Exception
	{
		int nResult = 0;
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try
		{
			cnx = TorqueHelper.getConnection(LogChanges.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				LogChanges.cat.error("Can't get database connection");
				throw new Exception();
			}

			String sql = LogChanges.sql_bundle.getString("getLogChangeIDByName");
			ps = cnx.prepareStatement(sql);
			ps.setString(1, sLogName);
			rs = ps.executeQuery();

			if (rs.next())
			{
				nResult = rs.getInt("log_change_type_id");
			}
		}
			catch (Exception e)
			{
			LogChanges.cat.error("Error during getLogChangeIdByName", e);
			throw new Exception();
			}
			finally
			{
				try
				{
				TorqueHelper.closeConnection(cnx, ps, rs);
				}
				catch (Exception e)
				{
				LogChanges.cat.error("Error during closeConnection", e);
				}
			}

		return nResult;
        }
        
        
        
        /**
         * 
         * @param sLogName
         * @param tableName
         * @return
         * @throws Exception 
         */
        @SuppressWarnings("static-access")
	public int getLogChangeTypeIdByNameAndTable(String sLogName, String tableName) throws Exception
	{
            int nResult = 0;
            Connection cnx = null;
            PreparedStatement ps = null;
            ResultSet rs = null;

            try
            {
                cnx = TorqueHelper.getConnection(LogChanges.sql_bundle.getString("pkgAlternateDb"));

                if (cnx == null)
                {
                    LogChanges.cat.error("Can't get database connection");
                    throw new Exception();
                }

                String sql = LogChanges.sql_bundle.getString("getLogChangeTypeIdByNameAndTable");
                ps = cnx.prepareStatement(sql);
                ps.setString(1, sLogName);
                ps.setString(2, tableName);
                rs = ps.executeQuery();

                if (rs.next())
                {
                    nResult = rs.getInt("log_change_type_id");
                }
            }
            catch (Exception e)
            {
                LogChanges.cat.error("Error during getLogChangeTypeIdByNameAndTable", e);
                throw new Exception();
            } 
            finally
            {
                DbUtil.closeDatabaseObjects(cnx, ps, rs);
            }
            return nResult;
        }
        
        /**
         * 
         * @param log_type
         * @param key_value
         * @param newvalue
         * @param oldvalue
         * @param reason
         * @param userName
         * @param application
         * @throws IOException 
         */
        public void log_changed(int log_type, String key_value, String newvalue, String oldvalue, String reason, String userName, String application) throws IOException{
            Connection dbConn = null;
            PreparedStatement pstmt = null;
            try
            {
                dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
                if (dbConn != null)
                {
                    String strSql = sql_bundle.getString("log_field_change");
                    pstmt = dbConn.prepareStatement(strSql);
                    pstmt.setInt(1, log_type);
                    pstmt.setString(2, key_value);
                    pstmt.setString(3, oldvalue);
                    pstmt.setString(4, newvalue);
                    pstmt.setString(5, userName);
                    pstmt.setString(6, application);
                    pstmt.setString(7, reason);
                    pstmt.executeUpdate();
                    cat.debug("[LogChanges] log_type= " + log_type+ ", key_value = " + key_value+ ", oldvalue = " + oldvalue+ ", newvalue = " + newvalue + ", reason = " + reason);
                }
            } catch (Exception e){
                cat.error("Error during checking changes", e);
                throw new IOException();
            } finally {
                try{
                	DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
                } catch (Exception e){
                	cat.error("Error during closeConnection", e);
                }
            }		
	}

}	
