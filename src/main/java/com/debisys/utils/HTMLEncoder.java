package com.debisys.utils;


public class HTMLEncoder {

	public static String encode(String s) {
		return htmlSpecialChars(s, true);
	}

	public static String htmlSpecialChars(String s, boolean doQuotes) {
		if (s == null) {
			return s;
		}
		int len = s.length();
		StringBuffer buf = new StringBuffer(len + 50);
		char c = '\0';
		if (doQuotes) {
			for (int i = 0; i < len; i++) {
				c = s.charAt(i);
				if (c == '"')
					buf.append("&quot;");
				else if (c == '\'')
					buf.append("&#039;");
				else if (c == '&')
					buf.append("&amp;");
				else if (c == '<')
					buf.append("&lt;");
				else if (c == '>')
					buf.append("&gt;");
				else
					buf.append(c);
			}

		}
		else {
			for (int i = 0; i < len; i++) {
				c = s.charAt(i);
				if (c == '&')
					buf.append("&amp;");
				else if (c == '<')
					buf.append("&lt;");
				else if (c == '>')
					buf.append("&gt;");
				else
					buf.append(c);
			}

		}
		return buf.toString();
	}
}
