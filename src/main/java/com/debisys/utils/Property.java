/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.utils;

/**
 *
 * @author nmartinez
 */
public class Property {

    private String name;
    private String value;

    /**
     *
     * @param name
     * @param value
     */
    public Property(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    protected void finalize() throws Throwable {
        this.name = null;
        this.value = null;
        super.finalize();
    }

    @Override
    public String toString() {
        return "name-" + name + "; value-" + value;
    }
}
