package com.debisys.utils;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Category;

import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author ldelarosa This klass contains methods to manipulate the TimeZone information
 */
@SuppressWarnings("deprecation")
public class TimeZone
{
	private static Category cat = Category.getInstance(TimeZone.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");

	/**
	 * Retrieves both the CodeName and the DisplayName of the specified TimeZone
	 * @param timeZoneId Id of the TimeZone, if 0 then the zone is GMT+0
	 * @return Zone Name
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static String getTimeZoneName(int timeZoneId) throws Exception
	{
		String sResult = "";
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(TimeZone.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				TimeZone.cat.error("Can't get database connection");
				throw new Exception();
			}
			
			String sql = TimeZone.sql_bundle.getString("getTimeZoneName");
			if ( timeZoneId > 0 )
			{
				sql += " AND TimeZoneID = ?"; 
				ps = cnx.prepareStatement(sql);
				ps.setInt(1, timeZoneId);
			}
			else
			{
				sql += " AND CodeName = 'Coordinated Universal Time'"; 
				ps = cnx.prepareStatement(sql);
			}
			rs = ps.executeQuery();

			if (rs.next())
			{
				sResult = rs.getString("DisplayName") + " [" + rs.getString("CodeName") + "]";
			}
		}
		catch (Exception e)
		{
			TimeZone.cat.error("Error during getTimeZoneName", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				TimeZone.cat.error("Error during closeConnection", e);
			}
		}

		return sResult;
	}

	/**
	 * Returns a list of all TimeZones sorted descendantly by its GMT
	 * 
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static Vector<Vector<String>> getTimeZoneList() throws Exception
	{
		Vector<Vector<String>> vResult = new Vector<Vector<String>>();
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(TimeZone.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				TimeZone.cat.error("Can't get database connection");
				throw new Exception();
			}

			String sql = TimeZone.sql_bundle.getString("getTimeZoneList");
			ps = cnx.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next())
			{
				Vector<String> vTemp = new Vector<String>();
				vTemp.add(rs.getString("TimeZoneID"));
				vTemp.add(rs.getString("CodeName"));
				vTemp.add(rs.getString("DisplayName"));
				vResult.add(vTemp);
			}
		}
		catch (Exception e)
		{
			TimeZone.cat.error("Error during getTimeZoneList", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				TimeZone.cat.error("Error during closeConnection", e);
			}
		}

		return vResult;
	}

	/**
	 * Returns a vector with the default TimeZone which is the default created at deployment time or GMT+0 in case the first doesn't exist
	 * 
	 * @return Vector
	 * 	Each slot has the following info: 1.TimeZoneID
	 * 			   2.CodeName
	 * 			   3.DisplayName
	 * 			   4.JavaTimeZoneId
	 * 			   5.DisplayName
	 * 			   6.Id: Unique Identifier		
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static Vector<String> getDefaultTimeZone() throws Exception
	{
		Vector<String> vResult = new Vector<String>();
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(TimeZone.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				TimeZone.cat.error("Can't get database connection");
				throw new Exception();
			}

			String sql = TimeZone.sql_bundle.getString("getDefaultTimeZone");
			ps = cnx.prepareStatement(sql);
			rs = ps.executeQuery();

			if (rs.next())
			{
				vResult.add(rs.getString("TimeZoneID"));
				vResult.add(rs.getString("CodeName"));
				vResult.add(rs.getString("DisplayName"));
				
				vResult.add(rs.getString("JavaTimeZoneId"));
				vResult.add(rs.getString("DisplayName"));
				vResult.add(rs.getString("Id"));
			}
		}
		catch (Exception e)
		{
			TimeZone.cat.error("Error during getDefaultTimeZone", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				TimeZone.cat.error("Error during closeConnection", e);
			}
		}

		return vResult;
	}

	/**
	 * Returns a vector with the TimeZone of the specified Rep
	 * 
	 * @param repId
	 *        Rep ID
	 * @return Vector
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static Vector<String> getTimeZoneByRep(long repId) throws Exception
	{
		Vector<String> vResult = new Vector<String>();
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(TimeZone.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				TimeZone.cat.error("Can't get database connection");
				throw new Exception();
			}

			String sql = TimeZone.sql_bundle.getString("getTimeZoneByRep");
			ps = cnx.prepareStatement(sql);
			ps.setLong(1, repId);
			rs = ps.executeQuery();

			if (rs.next())
			{
				vResult.add(rs.getString("TimeZoneID"));
				vResult.add(rs.getString("DisplayName"));
				vResult.add(rs.getString("CodeName"));
				
				vResult.add(rs.getString("JavaTimeZoneId"));
				vResult.add(rs.getString("DisplayName"));
				vResult.add(rs.getString("Id"));
				
			}
		}
		catch (Exception e)
		{
			TimeZone.cat.error("Error during getTimeZoneByRep", e);
			throw new Exception();
		}
		finally
		{
			TorqueHelper.closeConnection(cnx, ps, rs);
		}

		return vResult;
	}

	/**
	 * Returns a vector with the TimeZone of the specified Mechant
	 * 
	 * @param merchantId
	 *        Merchant ID
	 * @return Vector
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static Vector<String> getTimeZoneByMerchant(long merchantId) throws Exception
	{
		Vector<String> vResult = new Vector<String>();
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(TimeZone.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				TimeZone.cat.error("Can't get database connection");
				throw new Exception();
			}

			String sql = TimeZone.sql_bundle.getString("getTimeZoneByMerchant");
			ps = cnx.prepareStatement(sql);
			ps.setLong(1, merchantId);
			rs = ps.executeQuery();

			if (rs.next())
			{
				vResult.add(rs.getString("TimeZoneID"));
				vResult.add(rs.getString("DisplayName"));
				vResult.add(rs.getString("CodeName"));
				
				vResult.add(rs.getString("JavaTimeZoneId"));
				vResult.add(rs.getString("DisplayName"));
				vResult.add(rs.getString("Id"));
			}
		}
		catch (Exception e)
		{
			TimeZone.cat.error("Error during getTimeZoneByMerchant", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				TimeZone.cat.error("Error during closeConnection", e);
			}
		}

		return vResult;
	}

	/**
	 * Returns a vector with the TimeZone of the specified Mechant
	 * 
	 * @param sAccessLevel
	 * @param sRefId
	 * @param sStartDate
	 * @param sEndDate
	 * @param bFromServerToClient
	 * @return Vector with both StartDate and EndDate converted to the specified actor's TimeZone
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static Vector<String> getTimeZoneFilterDates(String sAccessLevel, String sRefId, String sStartDate, String sEndDate, boolean bFromServerToClient) throws Exception
	{
		Vector<String> vResult = new Vector<String>();
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(TimeZone.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				TimeZone.cat.error("Can't get database connection");
				throw new Exception();
			}

			//TimeZone.cat.info("TimeZone Input - Start Date: " + sStartDate);
		    //TimeZone.cat.info("TimeZone Input - End Date: " + sEndDate);
			
			String sql = TimeZone.sql_bundle.getString("getTimeZoneFilterDates");
			ps = cnx.prepareStatement(sql);
			ps.setString(1, sAccessLevel);
			ps.setString(2, sRefId);
			ps.setString(3, sStartDate);
			ps.setBoolean(4, bFromServerToClient);
			ps.setString(5, sAccessLevel);
			ps.setString(6, sRefId);
			ps.setString(7, sEndDate);
			ps.setBoolean(8, bFromServerToClient);
			rs = ps.executeQuery();

			if (rs.next())
			{
				SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");//dd/MM/yyyy
			    Timestamp start = rs.getTimestamp("StartDate");
			    Timestamp end = rs.getTimestamp("EndDate");

			    //To send to the database
			    vResult.add(sdfDate.format(start));
				vResult.add(sdfDate.format(end)); 
				
				//For Display purposes
				vResult.add(DateUtil.formatDateTime(rs.getTimestamp("StartDate")));
				vResult.add(DateUtil.formatDateTime(rs.getTimestamp("EndDate"))); 
			}
		}
		catch (Exception e)
		{
			TimeZone.cat.error("Error during getTimeZoneFilterDates", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				TimeZone.cat.error("Error during closeConnection", e);
			}
		}

		return vResult;
	}
	
	/**
	 * Returns a vector with the TimeZone of the specified Mechant
	 * 
	 * @param sAccessLevel
	 * @param sRefId
	 * @param sStartDate
	 * @param sEndDate
	 * @param bFromServerToClient
	 * @return Vector with both StartDate and EndDate converted to the specified actor's TimeZone
	 * @throws Exception
	 */
	@SuppressWarnings("static-access")
	public static String getTimeZoneByDate(String sAccessLevel, String sRefId, String sStartDate, boolean bFromServerToClient) throws Exception
	{		
		String dateTimeZone = "";
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(TimeZone.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				TimeZone.cat.error("Can't get database connection");
				throw new Exception();
			}
			//TimeZone.cat.info("TimeZone Input - Start Date: " + sStartDate);		    			
			String sql = TimeZone.sql_bundle.getString("getTimeZoneByDate");
			ps = cnx.prepareStatement(sql);
			ps.setString(1, sAccessLevel);
			ps.setString(2, sRefId);
			ps.setString(3, sStartDate);
			ps.setBoolean(4, bFromServerToClient);			
			rs = ps.executeQuery();

			if (rs.next())
			{				
				dateTimeZone = DateUtil.formatDateTime(rs.getTimestamp("dateTimeZone"));			     
			}
		}
		catch (Exception e)
		{
			TimeZone.cat.error("Error during getTimeZoneFilterDates", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				TimeZone.cat.error("Error during closeConnection", e);
			}
		}
		return dateTimeZone;
	}
	
	
	
	
	
	/**
	 * @param sessionData
	 * @return
	 * @throws Exception
	 */
	public static Vector<String> getTimeZoneByUserInSession( SessionData sessionData )
	{				
		Vector<String> vTimeZoneData = null;		
		try 
		{
			if ( sessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
			{
				vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(sessionData.getProperty("ref_id")));
			}
			else
			{
				vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(sessionData.getProperty("ref_id")));
			}			
			if ( vTimeZoneData == null)
			{
				vTimeZoneData = TimeZone.getDefaultTimeZone();
			}				
		} 
		catch (Exception e) {
			TimeZone.cat.error("Error during getTimeZoneByUserInSession ", e);
		}
		return vTimeZoneData;
	}
	
	/**
	 * @param date
	 * @param fromTimeZone
	 * @param toTimeZone
	 * @return
	 */
	public static long convertTimes(final long date, final String fromTimeZone, final String toTimeZone) {
        if (fromTimeZone.equalsIgnoreCase(toTimeZone)) {
            return date;
        }
        java.util.TimeZone fTimeZone = java.util.TimeZone.getTimeZone(fromTimeZone);
        java.util.TimeZone tTimeZone = java.util.TimeZone.getTimeZone(toTimeZone);
        long offFrom = fTimeZone == null ? 0 : fTimeZone.getOffset(date);
        long offTo = tTimeZone == null ? 0 : tTimeZone.getOffset(date);
        return date + offTo - offFrom;
    }
	
}
