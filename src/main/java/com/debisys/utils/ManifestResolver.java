package com.debisys.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

public class ManifestResolver {
	
	private static Logger logger = Logger.getLogger(ManifestResolver.class);

	public static String getVersion(ServletContext application){
		String version = null;
		InputStream manifestStream = null;
		try {
			manifestStream = application.getResourceAsStream("META-INF/MANIFEST.MF");
			Manifest manifest = new Manifest(manifestStream);
			Attributes mainAttribs = manifest.getMainAttributes();
            version = mainAttribs.getValue("Specification-Version");
		} catch (FileNotFoundException e) {
			logger.error("Manifest file not-found", e);
		} catch (IOException e) {
			logger.error("Could not extract manifest attributes", e);
		} finally {
			if(manifestStream != null){
				try {
					manifestStream.close();
				} catch (IOException ignore) {
					logger.warn(ignore);
				}
			}
		}
		return version;
	}
}
