/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.utils;

/**
 *
 * @author nmartinez
 */
public class QueryFilter {

    private String sortFieldName;
    private String sortOrder;
    private String startPage;
    private String endPage;

    /**
     *
     * @param sortFieldName
     * @param sortOrder
     * @param startPage
     * @param endPage
     */
    public void setInfoReportData(String sortFieldName, String sortOrder, String startPage, String endPage) {
        this.setSortFieldName(sortFieldName);
        this.setSortOrder(sortOrder);
        this.setStartPage(startPage);
        this.setEndPage(endPage);
    }
    
    /**
     * @return the sortFieldName
     */
    public String getSortFieldName() {
        return sortFieldName;
    }

    /**
     * @param sortFieldName the sortFieldName to set
     */
    public void setSortFieldName(String sortFieldName) {
        this.sortFieldName = sortFieldName;
    }

    /**
     * @return the sortOrder
     */
    public String getSortOrder() {
        return sortOrder;
    }

    /**
     * @param sortOrder the sortOrder to set
     */
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * @return the startPage
     */
    public String getStartPage() {
        return startPage;
    }

    /**
     * @param startPage the startPage to set
     */
    public void setStartPage(String startPage) {
        this.startPage = startPage;
    }

    /**
     * @return the endPage
     */
    public String getEndPage() {
        return endPage;
    }

    /**
     * @param endPage the endPage to set
     */
    public void setEndPage(String endPage) {
        this.endPage = endPage;
    }

}
