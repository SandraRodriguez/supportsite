package com.debisys.utils;

/**
 * @author ?
 */
public interface DebisysConstants {

    public static final int LOGTYPE_LOGIN_LOGOUT = 1;
    public static final int LOGTYPE_CUSTOMER = 2;

    // access levels for website
    public static final String NULL = "0";
    public static final String ISO = "1";
    public static final String AGENT = "2";
    public static final String SUBAGENT = "3";
    public static final String REP = "4";
    public static final String MERCHANT = "5";
    public static final String CLERK_MANAGER = "6";
    public static final String CLERK = "7";
    public static final String REFERRAL = "8";
    public static final String CARRIER = "9";

    // rep_types for reps table
    public static final String REP_TYPE_REP = "1";
    // 3 & 5 level distribution chain
    public static final String REP_TYPE_ISO_3_LEVEL = "2";
    public static final String REP_TYPE_ISO_5_LEVEL = "3";
    public static final String REP_TYPE_AGENT = "4";
    public static final String REP_TYPE_SUBAGENT = "5";
    public static final String REP_TYPE_REFERRAL = "7";
    public static final String REP_TYPE_CARRIER = "11";
    public static final String REP_TYPE_MERCHANT = "8";

    // ref_types for password table
    public static final String PW_REF_TYPE_REP_ISO = "1";
    public static final String PW_REF_TYPE_MERCHANT = "2";
    public static final String PW_REF_TYPE_ADMIN = "3";
    public static final String PW_REF_TYPE_AGENT = "4";
    public static final String PW_REF_TYPE_SUBAGENT = "5";
    public static final String PW_REF_TYPE_REFERRAL = "7";
    public static final String PW_REF_TYPE_CARRIER = "11";

    public static final String DIST_CHAIN_3_LEVEL = "1";
    public static final String DIST_CHAIN_5_LEVEL = "2";

    public static final String DEPLOYMENT_DOMESTIC = "0";
    public static final String DEPLOYMENT_INTERNATIONAL = "1";

    public static final String MERCHANT_TYPE_PREPAID = "1";
    public static final String MERCHANT_TYPE_CREDIT = "2";
    public static final String MERCHANT_TYPE_UNLIMITED = "3";
    public static final String MERCHANT_TYPE_SHARED = "4";

    public static final String REP_CREDIT_TYPE_PREPAID = "1";
    public static final String REP_CREDIT_TYPE_CREDIT = "2";
    public static final String REP_CREDIT_TYPE_UNLIMITED = "3";
    public static final String REP_CREDIT_TYPE_FLEXIBLE = "5";

    
    public static final String AUDIT_PARENT_CHANGE = "PARENT_CHANGE";
    public static final String AUDIT_CREDITTYPE_CHANGE = "CREDITTYPE_CHANGE";
    public static final String AUDIT_SHARED_INDIVIDUAL_CHANGE = "SHARED_INDIVIDUAL_CHANGE";
    public static final String AUDIT_INTERNAL_EXTERNAL_CHANGE = "INTERNAL_EXTERNAL_CHANGE";
    public static final String AUDIT_MERCHANT_CANCELLED="MERCHANT_CANCELLED";
    public static final String AUDIT_MERCHANT_DISABLED="MERCHANT_DISABLED";
    
    // permission types
    // add/edit support site logins
    public static final String PERM_LOGINS = "1";
    // add/edit merchants
    public static final String PERM_MERCHANTS = "2";
    // add/edit terminals
    public static final String PERM_TERMINALS = "3";
    // add/edit rate plans
    public static final String PERM_RATEPLANS = "4";
    // update credit limits
    public static final String PERM_CREDIT_LIMITS = "5";
    // enable/disable merchants
    public static final String PERM_MERCHANT_STATUS = "6";
    // add/remove clerk codes
    public static final String PERM_CLERK_CODES = "7";
    // view merchant notes
    public static final String PERM_NOTES = "8";
    // add/edit reps
    public static final String PERM_REPS = "9";
    // View PIN Information
    public static final String PERM_VIEWPIN = "10";
    // Manage PIN Return Requests
    public static final String PERM_MANAGEPIN = "11";
    // Manage Payment Requests
    public static final String PERM_MANAGEPAYMENT = "12";
    // Manage Inventory
    public static final String PERM_MANAGEINVENTORY = "13";
    // Add PIN Return Requests
    public static final String PERM_ADDPINREQUESTS = "14";

    // Add MULTI - ISOS
    public static final String PERM_MANAGE_SUPPORT_ISOS = "15";

    // Add TRACKING TERMINALS
    public static final String PERM_TRACKING_TERMINALS = "16";

    // View Referral Agent Reports
    public static final String PERM_VIEWREFAGENT_REPORTS = "17";

    // Add Void TopUp Requests
    public static final String PERM_ADDVOIDTOPUP_REQUESTS = "18";

    // View Terminal Rates
    public static final String PERM_VIEW_TERMINAL_RATES = "19";

    // Update Payment Features
    public static final String PERM_UPDATE_PAYMENT_FEATURES = "20";

    // Enable Monthly Inventory Report
    public static final String PERM_MONTHLY_INVENTORY_REPORT = "21";

    // Enable US Prepaid-Prepaid behavior
    // Enable Rep Rate Plans for Terminal creation
    public static final String PERM_REPRATEPLANS_TERMINALCREATION = "23";

    //24	Enable Top Sellers Edition
    public static final String PERM_ENABLED_TOP_SELLERS_EDITION = "24";

    public static final String PERM_MOBILE_AUTHENTICATION = "25";
    // DBSY-568 SW
    // Enable ISO's use merchant bulk import option
    public static final String PERM_MERCHANT_BULK_IMPORT = "26";

    // Enable PIN Remaining Inventory Report
    public static final String PERM_PIN_REMAINING_INVENTORY = "27";

    // Enable ACH Date in Transaction Reports
    public static final String PERM_VIEW_ACH_DATE = "28";

    // Enable Money Transfers Report
    public static final String PERM_MONEY_TRANSFER = "29";

    // Enable Money Transfers Report
    public static final String PERM_BILLING_INT_REPORT = "30";

    // Add/Edit Agents
    public static final String PERM_AGENTS = "31";

    // Add/Edit Sub-Agents
    public static final String PERM_SUBAGENTS = "32";

    // Enable Bonus Threshold Configuration
    public static final String PERM_CONFIGURE_BONUS_THRESHOLDS = "33";

    public static final String PERM_CONFIGURE_BONUS_THRESHOLD_SMS = "34";
	// END DBSY-568

    // Enable Phone Rewards Configuration
    public static final String PERM_CONFIGURE_PHONE_REWARDS = "47";

    public static final String PERM_CONFIGURE_PHONE_REWARDS_SMS = "48";
	// END

    // Enable Add Merchant Notes
    public static final String PERM_ADD_NOTES = "35";

    // Enable Merchants joined by Agent Report
    public static final String PERM_MERCHANT_JOINED = "36";

    // Enable Merchants joined by Agent Report
    public static final String PERM_ENABLE_INVOICE_MERCHANT_REPORT = "37";

    // Enable Detailed Payment Report
    public static final String PERM_ENABLE_DETAILED_PAYMENT_REPORT = "38";

    //Enable PAYMENT BULK IMPORT
    public static final String PERM_ENABLED_PAYMENT_BULK_IMPORT = "39";

    //Enable Search Terminals by Serial Number
    public static final String PERM_ENABLE_SEARCH_TERMINALS_BY_SERIALNUMBER = "40";

    //Enable Modify Rate Plan In Terminal Edition
    public static final String PERM_RATEPLAN_TERMINAL_EDIT = "41";

    //Enable Add/Edit Emida PCTerminal
    public static final String PERM_ENABLED_ADD_EDIT_EMIDA_PCTERMINAL = "42";

    //Enable nFINANSE Report
    public static final String PERM_ENABLED_NFINANSE_REPORT = "43";

    //Enable Tax Calculation in SS Reports   -- DBSYS- 746 NG
    public static final String PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS = "44";

    //Enable WebService Authentication for Terminals   -- DBSYS- 821 NG
    public static final String PERM_ENABLE_WEBSERVICE_AUTHENTICATION_FOR_TERMINALS = "45";

    //Enable Edit Sales Force ID (SFID)    -- DBSYS- 814
    public static final String PERM_ENABLE_EDIT_SALES_FORCE_ID = "46";

    // Write Offs
    public static final String PERM_WRITE_OFF = "49";

    // Brandings
    public static final String PERM_BRANDINGS_SETUP = "50";

    // Carrier User Permissions
    public static final String PERM_MANAGE_CARRIER_USERS = "51";
    public static final String PERM_VIEW_CARRIER_REPORTS = "52";
    public static final String PERM_VIEW_CARRIER_TOOLS = "132";

    // Show Invoice Number Column Permission
    public static final String INVOICE_NUMBER = "57";

    // Web Permission: showing column for additional data in transaction report
    public static final String PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA = "53";

    // Web Permission: ENABLE_MAXIMUM_SALES_LIMIT for ERI Credit Limit R30
    public static final String PERM_ENABLE_MAXIMUM_SALES_LIMIT = "54";

    // Web Permission: Enable Edit Merchant Account Executives ERI
    public static final String PERM_Enable_Edit_Merchant_Account_Executives = "55";

    // Web Permission: Enable Edit Merchant Account Executives ERI
    public static final String PERM_ENABLE_DISABLE_TERMINALS = "56";

    //Billing ACH Report
    public static final String PERM_BILLING_ACH_REPORT = "58";

    // Manage New Rate Plans
    public static final String PERM_MANAGE_NEW_RATEPLANS = "59";

    // Manage Boost Approved - Restricted Products Boost Rationalization Phase 2
    public static final String PERM_MANAGE_APPROVED_RESTRICTED_PRODUCTS = "60";

    //System 2000 Tools
    public static final String SYSTEM_2000_TOOLS = "61";

    // Manage Promos
    public static final String PERM_MANAGE_PROMOS = "62";

    // Manage New Rate Plans
    public static final String PERM_MANAGE_GLOBAL_UPDATE = "63";

    // NSF Report Enabled
    public static final String PERM_NSF_REPORT = "64";

    // Enable Cached Receipts Edition
    public static final String PERM_EDIT_CACHED_RECEIPTS = "65";

    //R31.1 Billing ABernal - Emunoz
    public static final String PERM_REGULATORY_FEE = "66";

    /**
     * Allows to create, edit and delete MicroBrowser parameter configurations
     */
    public static final String PERM_EDIT_MB_CONFIGURATIONS = "67";

    /* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
    public static final String PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT = "68";
    /* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/

    // DBSY-1072
    public static final String PERM_EDIT_ACCOUNT_TYPE = "69";

    //Permission to generate new regcode after terminal has been created.
    public static final String PERM_TO_GENERATE_NEW_REGCODE = "70";

    // Web Permission: Edit Terminal Operations Time for POST R33
    public static final String PERM_EDIT_TERMINAL_OPS_TIME = "71";
    /* DBSY-940 Transaction Origination Report */
    public static final String PERM_VIEW_TRANSACTIONS_ORIGINATION_REPORT = "72";
    /* END DBSY-940 Transaction Origination Report */

    //R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP
    public static final String PERM_TO_DOCUMENT_MANAGEMENT = "73";
	//END R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP

    //R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP
    public static final String PERM_TO_VIEW_ISO_INFORMATION = "74";
	//END R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP

    //Alfred DBSY-1139 - Shows Assigned/External in reports, allows toggling of external rep usage on merchants.
    public static final String PERM_ALLOW_EXTERNAL_REPS = "75";
	//END DBSY-1139

    //Diego Garzon - Shows Assigned/External in reports, allows toggling of external rep usage on merchants.
    public static final String PERM_EDIT_BANKING_INFO_MERCHANT = "78";
    //END;

    /**
     * ***************************************************************************************
     */
    /**
     * ***************************************************************************************
     */
    /*GEOLOCATION FEATURE*/
    public static final String PERM_GEOLOCATION_FEAUTURE = "77";
    /**
     * ***************************************************************************************
     */
    /**
     * ***************************************************************************************
     */
    /*GEOLOCATION FEATURE*/

    // Enable PINMoney Transactions Report
    public static final String PERM_ENABLE_PINMONEY_TRANSACTIONS_REPORT = "79";

    //Jorge Nicol?s Mart?nez Romero - Terminal Mapping Boost requirements.
    public static final String PERM_SETUP_TERMINAL_MAPPING = "80";
	//END;

    //DC - Permission to restrict CS representatives in LIME to access the Transaction Summary Report from the Support Site carrier view
    public static final String PERM_CS_REP_CARRIER_VIEW = "82";

    public static final String PERM_ENABLE_PROVIDER_EXTERNAL_BALANCE_REPORT = "83";

    public static final String PERM_ENABLE_CROSSBORDER_EXCHANGE_RATE_MANAGEMENT = "84";

    //Add by Yecid DTU-229
    public static final String ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMNS_ISLAND_AND_REP = "85";
    public static final String ENABLE_DAILY_LIABILITY_REPORT_EXTRA_COLUMN_LAST_PURCHASE = "86";
	//End Add by Yecid DTU-229

    // Nidhi Gulati DTU-370
    public static final String PERM_MANAGE_PROMO_DETAILS = "87";
	// End DTU-370

    // Parature 5545-10237043 = Jorge Nicol?s Mart?nez Romero
    public static final String PERM_RERCHARGE_DATA_BY_PHONE_NUMBER_REPORT = "88";
	// End Parature 5545-10237043

    //DTU-246: Error Messaging to Device
    public static final String PERM_ENABLE_ERROR_MANAGEMENT = "89";
	//DTU-246: Error Messaging to Device	

    //DTU-526: GRUPO MAS REQUIREMENT
    public static final String PERM_EDIT_BANKING_ACCOUNT_TYPE_FOR_MERCHANT = "90";
	//END DTU-526: GRUPO MAS REQUIREMENT

    //Jorge Nicol?s Mart?nez Romero - Bank-import payments-records status.
    public static final String PERM_Bank_Import_Payments_Records_Status = "81";

    public static final String PERM_ADD_EDIT_TERMINAL_LABEL = "91";

    /* pin cache permissions*/
    public static final String PERM_MANAGE_PINCACHE = "92"; // Manage PinCache Templates
    public static final String PERM_MANAGE_PINCACHE_ADV = "93"; // PinCache Tool

    //DTU-1004 Jorge Nicol?s Mart?nez Romero
    public static final String PERM_ENABLE_FRM_COMMISSIONS = "94"; // FMR Commission Payout Detail Report

    //DTU 896 Heartbeat Project
    public static final String PERM_ENABLE_HEARTBEAT_REPORT = "95"; // Enable Heartbeat Report

    //DTU-480: Low Balance Alert Message SMS and/or email
    public static final String PERM_ENABLE_LOW_BALANCE_NOTIFICATIONS = "96"; // Enable Low Balance Notifications

    //DTU-1006 PIN Stock Report
    public static final String PERM_ENABLE_PIN_STOCK_REPORT = "97"; // Enable PIN Stock Report

    public static final int EXTRA_FILTER_ON_PAYMENT_DETAIL_REPORT = 98;

    //DTU-1149	Belize: Customized Report
    public static final String PERM_ENABLE_TRX_SUMMARY_RECON_REPORT = "99";   // Enable Transaction Summary Reconciliation Report 
    public static final String PERM_ENABLE_TRX_DETAILED_RECON_REPORT = "100"; // Enable Transaction Detailed Reconciliation Report
    //END DTU-1149	Belize: Customized Report

    public static final String FEE_APPROVAL_TOOL = "101";

    //DTU-1281 Facey
    public static final String PERM_ENABLE_VIEW_ADDITIONAL_COLUMNS_TRX_SUMM_PRODUCT_MERCHANT_REPORT = "102";
											//Add Sales/Returns Columns to Trx Summary Product/Merchant Report
    //END DTU-1281 Facey

    //DTU-1204 
    public static final String PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS = "103";//View References Card In Reports
    //END DTU-1204

    public static final String REAL_TIME_TRANSACTION_REPORT = "104";

    //DTU-1051
    public static final String PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT = "105";//View Authorization Number in Transactions Report	
    //END DTU-1051

    //DTU-542
    public static final String PERM_ENABLE_VIEW_SUBAGENT_DETAILS_MERCHANT_ACTIVITY_REPORT = "106";
    public static final String PERM_ENABLE_REMOVE_REP_CONTACT_MERCHANT_ACTIVITY_REPORT = "107";
    public static final String PERM_ENABLE_REP_ACTIVITY_REPORT = "108";
	//END DTU-542

    //DTU-143
    public static final String PERM_VIEW_MICROBROWSER_TOOL_BATCH_UPDATE_VERSION = "109";//MicroBrowser Batch Update Tool

    public static final String GENERATE_REPORT_WITH_TAXES_MX = "110"; // // Generate reports with taxes Mx
    //END DTU-143

    //DTU-1196
    public static final String PERM_ENABLE_CREDIT_BALANCE_REPORT = "111";
	//END DTU-1196

    //DTU-543
    public static final String PERM_ENABLE_SCHEDULE_REPORTS = "112";
	//END DTU-543

    //DTU-1455
    public static final String PERM_CAN_SET_MERCHANT_ASSIGNMENT = "113";
    //END DTU-543

    public static final String PERM_VEW_PIN_HISTORY = "114";

    //DTU-2639
    public static final String PERM_ONE_LOGIN_FOR_MERCHANTS_TERMINALS = "115";

    //DTU-2625
    public static final String PERM_NO_CLERK_DATA_TRANSACTIONS = "116";

    //DTU-2640
    public static final String PERM_SEND_SMS_NOTIFICATION_TO_THE_PAYMENT_CUSTOMER = "117";

    //DTU-2634
    public static final String PERM_SUPPORT_SITE_ACCESS_LINK = "118";

    //DTU-2706 US: PC Term 2.0 Product Flow Change
    public static final String PERM_CARRIERS_LIST = "119";
    
    public static final String PERM_ALLOW_TRANSFER_TO_REPS="121"; 

    //DTU-2536 Multisource in Top Up Products - Phase II
    public static final String PERM_SHOW_MULTISOURCE_INFO = "120";
    
    //Permission to Show accountId to QRCode Transactions
    public static final String PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS = "122";
    
    //Permission to DTU-2845 INTL: Merchant Mapping for BTC.
    public static final String PERM_ALLOW_MERCHANT_MAP_LOCATOR_REPORT = "123";

    //Permission to DTU-2845 INTL: Merchant Mapping for BTC.
    public static final String PERM_ALLOW_LOCALIZE_MERCHANT_ON_MAP = "124";
    
    public static final String PERM_ALLOW_PAYMENT_REQUEST_REPORT = "125";
    
    public static final String PERM_ALLOW_SMS_INVENTORY_MANAGEMENT = "126";
    
    public static final String PERM_ALLOW_AVAILABLE_BALANCE_REPORT = "127";
	
	public static final String PERM_ALLOW_STOCK_LEVELS_THRESHOLD_CONF = "128";
    
    public static final String PERM_ALLOW_STOCK_LEVELS_THRESHOLD_REPORT = "129";
    
    public static final String PERM_ALLOW_SPREAD_BY_PROVIDER_CARRIER_REPORT = "130";
    
    public static final String PERM_ALLOW_AUDIT_REPORT_MANAGEMENT = "131";
    
    public static final String PERM_ALLOW_NO_BALANCE_MERCHANT = "133";
    
    public static final String PERM_ALLOW_FREE_UP_ACTIVATIONS = "134";
    
    public static final String PERM_ALLOW_VIEW_PINS_ON_REPORT = "136";
    
    public static final String PERM_ALLOW_MERCHANTS_PREPAID_USING_CARD_PROCESSOR = "135";
    
    public static final String PERM_VIEW_FILTER_S2K_RATEPLANS = "137";
    
    public static final String PERM_VIEW_BULK_PAYMENT = "138";
    
    public static final String PERM_CWC_FLOW_STREETLIGHT_REPORTS = "139";//CWC Flow StreetLight Reports
    public static final String PERM_CWC_CROSSBORDER_CONSOLIDATED_REPORT = "140";//CWC Cross Border Consolidated Reporting
    public static final String PERM_SHARED_BALANCE_FOR_INTL = "141";//Shared Balance for INTL
    public static final String PERM_TOKEN_SECURITY = "142";
    public static final String PERM_REPORT_PAYMENT_APPLIED_SHARE_CREDIT = "143";
    public static final String PERM_EXCHANGE_RATE_AUDIT_REPORT = "144";
    public static final String PERM_ALLOW_USE_CHAT = "145";
    
    public static final String PERM_ALLOW_ADMIN_TERMINAL_ACTIVITY = "146";
    
    public static final String PERM_CWC_ADDING_FOUNDS_REQUESTER = "147"; // CWC Adding funds (Requester)
    public static final String PERM_CWC_ADDING_FOUNDS_APPROVER = "148"; // CWC Adding funds (APPROVER)
    public static final String PERM_UPDATE_CREDIT_TYPE = "149"; // Update Credit Type
    
    public static final String PERM_ALLOW_MARKET_PLACE_LINK = "150";
    public static final String PERM_DISPERSION_OF_FOUNDS_REQUESTER = "152"; // dispersion of funds (Requester)
    public static final String PERM_MANAGE_PROMOTIONS_BY_ENTITY = "153"; // Manage Promotions by Entities
    public static final String PERM_ALLOW_MANAGE_TERMINAL_ASSOCIATIONS = "154";
	
    public static final String PERM_ALLOW_MANAGEMENT_PIN_STATUS = "156";
    
    public static final String PERM_ALLOW_USE_NOT_USE_1 = "157";
    public static final String PERM_ALLOW_USE_NOT_USE_2 = "158";
    
    public static final String PERM_ALLOW_USE_CUSTOMER_ACCOUNT_MANAGEMENT = "159";
    public static final String PERM_ALLOW_VIEW_ECOMMERCE = "161";
    public static final String PERM_ALLOW_VIEW_COMMISSIONS_DETAIL = "162";
    public static final String PERM_REP_MERCHANTS_BALANCE_SUM = "163";
    public static final String PERM_ALLOW_USE_DOMESTIC_CUSTOMER_ACCOUNT_MANAGEMENT = "164";
    public static final String PERM_ALLOW_PROMO_DATA_COLUMN_IN_TRANSACTIONS_REPORT = "165";  // Data Promo Transactions Reports
    public static final String PERM_ALLOW_VIEW_ACCOUNTS_ON_TOP_UP = "167";
    public static final String PERM_ALLOW_VIEW_DOCUMENT_MANAGEMENT = "168";
    public static final String PERM_SHOW_EXTERNAL_MERCHANT_ID = "169";
    public static final String PERM_ALLOW_ADMIN_DOCUMENT_MANAGEMENT = "170";
    public static final String PERM_ALLOW_PAYMENT_PROCESSOR_MANAGEMENT = "177";    
    
    public static final String PERM_ENABLE_PASSWORD_EXPIRATION_TERMINALS_WS = "171"; // Enable Password Expiration for Terminals WS
    public static final String PERM_ALLOW_PAYMENT_TO_REPS = "172"; // Enabling payment to reps
    public static final String PERM_ENABLE_PASSWORD_CYCLES_TERMINALS_WS = "173"; // Enable Password Cycles for Terminals WS
    public static final String PERM_VIEW_TRX_REPORT_BY_REGION = "176"; // View Transactions Report by region
   
    public static final String PERM_VIEW_FINANCIAL_SUMMARIES_REPORTS = "181"; // Financial Products Summaries Reports
    public static final String PERM_VIEW_CACHED_PINS_REPORTS = "182"; // Cached Pins Reports
    public static final String PERM_VIEW_INTERNAL_REPORTS = "183"; // Internal Reports
    public static final String PERM_VIEW_MANAGED_SCHEDULE_REPORTS = "184"; // Managed Schedule Reports
    public static final String PERM_VIEW_CONVENIENCE_CARDS_SUMMARY_REPORTS = "185"; // Convenience Cards Summary Reports
    //public static final String PERM_VIEW_MULTISOURCE_TRX_REPORTS = "186"; // MultiSource Trx Reports
    public static final String PERM_RESET_CLERK_CODE = "187";
    
    // Indicates what type of Custom configurationis being used
    public static final String CUSTOM_CONFIG_TYPE_DEFAULT = "0";
    public static final String CUSTOM_CONFIG_TYPE_MEXICO = "1";

    // Currency Format
    public static final String CURRENCY_SYMBOL_FORMAT = "0";

    public static final String CURRENCY_ISO_FORMAT = "1";

    // Invoice Types
    public static final String INVOICE_TYPE_NO_INVOICE = "0";
    public static final String INVOICE_TYPE_INVOICE = "1";
    public static final String INVOICE_TYPE_GLOBAL_INVOICE = "2";
    public static final String INVOICE_TYPE_PROCESS = "3";
    public static final String INVOICE_TYPE_OTHER = "4";
    public static final String SMS_PHONE_MERCHANT_TERMINAL_TYPE = "40";
    public static final String SMS_PHONE_KIOSK_MERCHANT_TERMINAL_TYPE = "87";
    public static final String SMS_TABLET_TERMINAL_TYPE = "99";
    public static final String SMS_PRINTER_TERMINAL_TYPE = "100";
    public static final String SMS_3G_PRINTER_TERMINAL_TYPE = "150";
    public static final String SMS_QUICK_SELL_TERMINAL_TYPE = "135";
    public static final String USSD_INFO_PYME_KIOSKO_TERMINAL_TYPE = "136";    

    public static final String SMS_PHONE_SUBSCRIBER_TERMINAL_TYPE = "56";

    public enum ISO_PAYMENT_TYPES {

        DEF, ACH, INV, PRE, CRE
    };

    public static final int WEB_PERMISSION_US_PREPAID = 22;

    //Enable WebService Authentication for Terminals   -- DBSYS- 821 NG
    public static final String WEB_SERVICE_TERMINAL_TYPE = "23";
    public static final String WEB_SERVICE_KIOSK_TERMINAL_TYPE = "86";
    public static final String WEB_SERVICE_MMSPOS_TERMINAL_TYPE = "102";
    public static final String VERIFONE_TERMINAL_TYPE = "17";

    public static final String TRX_TYPE_SALE = "1";

    public static final String TRX_TYPE_RECHARGE = "2";

    public static final String WRITE_OFF_ERROR_INVALID_TRX_TYPE = "880";

    public static final String WRITE_OFF_ERROR_DUPLICATE_TRX = "881";

    public static final String WRITE_OFF_ERROR_TRX_NOT_FOUND = "882";

    public static final String WRITE_OFF_ERROR_GROUPED_ISO = "883";

    // ACL Permissions for Intranet Users
    public static final String INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS = "Edit Emida Master Rate Plans";
    public static final String INTRANET_PERMISSION_MANAGE_PROMOTIONS = "Manage Promotions";
    public static final String INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES = "Manage Rate Plan Global Updates";
    public static final String INTRANET_PERMISSION_ALLOW_EXCEED_RATES = "Allow to Exceed Emida Buy Rate";
    public static final String INTRANET_PERMISSION_MANAGE_BRANDINGS = "Branding Tool";
    public static final String INTRANET_PERMISSION_MANAGE_DOCUMENTS = "Document Approval";
    public static final String INTRANET_PERMISSION_IMTU_REPORT = "IMTU Report";
    public static final String INTRANET_PERMISSION_PUSH_MESSAGING = "Push Messaging";
    public static final String INTRANET_PERMISSION_NO_ACK_PIN_MNG = "No Ack Pin Management";
    public static final String INTRANET_PERMISSION_MULTI_CREDENTIALS = "MultiCredentials";
       
    // SS Validation enabled for merchant balance with respect to rep in MX deployment
    public static final String ENABLE_VALIDATION_MERCHANT_BALANCE = "1";

    public static final String DATA_BASE_MASTER = "MASTER";
    public static final String DATA_BASE_SLAVE = "SLAVE";

    public static final int EXECUTE_REPORT = 0;
    public static final int DOWNLOAD_REPORT = 1;
    public static final int SCHEDULE_REPORT = 2;

    /**
     * This unique variable session by user, means Schedule Session Variable
     * Name
     */
    public static final String SC_SESS_VAR_NAME = "scheduleSessionVariableName";

    /**
     * Identify the agents credit report
     * /admin/customers/agents_credit_history.jsp
     */
    public static final String SC_AGENTS_CREDIT_HISTORY = "AgentsCreditHistory";

    /**
     * Identify the agents information report /admin/customers/agents_info.jsp
     */
    public static final String SC_AGENTS_INFO = "AgentsInfo";

    /**
     * Identify the agents subagents credit history report
     * /admin/customers/agents_subagent_credit_history.jsp
     */
    public static final String SC_AGENT_SUAGENT_CRED_HIST = "AgentsSubagentCreditHistory";

    /**
     * Identify the collections report
     */
    public static final String SC_COLLECTIONS_REPORTS = "CollectionsReport";

    /**
     * Identify the Credit or payment summary by merchant
     */
    public static final String SC_CREDIT_PAYMENT_SUMM_BY_MERCHANT = "CredPaySummByMerch";

    /**
     * Identify the fee approval tool /admin/tools/feeApprovalTool.jsp
     */
    public static final String SC_FEE_APP_TOOL = "FeeApprovalTool";

    /**
     * Identify the merchant credit history report
     * /admin/customers/merchants_credit_history.jsp
     */
    public static final String SC_MER_CRED_HISTORY = "MerchantsCreditHistory";

    /**
     * Identify the merchant external credit history report
     * /admin/customers/merchants_external_credit_history.jsp
     */
    public static final String SC_MER_EXT_CRED_HISTORY = "MerchantsExternalCreditHistory";

    /**
     * Identify the merchants info data /admin/customers/merchants_info.jsp
     */
    public static final String SC_MERC_INFO = "MerchantsInfo";

    /**
     * Identify the NON Transaction Terminals report
     */
    public static final String SC_NON_TRANSAC_TERMINALS = "NonTrxTerminals";

    /**
     * Identify the rep actions /admin/reports/payments/rep_actions.jsp
     */
    public static final String SC_REP_ACTIONS = "RepActions";

    /**
     * Identify the rep actions summary
     * /admin/reports/payments/rep_actions_summary.jsp
     */
    public static final String SC_REP_ACT_SUMMARY = "RepActionsSummary";

    /**
     * Identify the reps credit history /admin/customers/reps_credit_history.jsp
     */
    public static final String SC_REPS_CRED_HIST = "RepsCreditHistory";

    /**
     * Identify the reps info /admin/customers/reps_info.jsp
     */
    public static final String SC_REP_INFO = "RepsInfo";

    /**
     * Identify the reps merchant credit history
     * /admin/customers/reps_merchant_credit_history.jsp
     */
    public static final String SC_REP_MERC_CRED_HIST = "RepsMerchantCreditHistory";

    /**
     * Identify the subagents credit history report
     * /admin/customers/subagents_credit_history.jsp
     */
    public static final String SC_SUBA_CRED_HIST = "SubAgentsCreditHistory";

    /**
     * Sub agent info identifier /admin/customers/subagents_info.jsp
     */
    public static final String SC_SUBA_INFO = "SubagentsInfo";

    /**
     * Sub agents reps credit history identifier
     * /admin/customers/subagents_rep_credit_history.jsp
     */
    public static final String SC_SUBA_REP_CRED_HIST = "SubagentsRepCreditHistory";

    /**
     * Summary FMR commissions pay-out report identifier
     * /admin/reports/ach/summaryFMRCommissionPayout.jsp
     */
    public static final String SC_SUM_FMR_COMM_PAY = "SummaryFMRCommissionPayout";

    /**
     * Identify the Transaction Summary by Agent report
     */
    public static final String SC_TRX_SUMM_BY_AGENT = "TrxSummByAgent";

    /**
     * Identify the Transaction Summary by merchant report
     */
    public static final String SC_TRX_SUMM_BY_MERCHANT = "TrxSummByMer";

    /**
     * Identify the Transaction Summary by rep report
     */
    public static final String SC_TRX_SUMM_BY_REP = "TrxSummByRep";

    /**
     * Identify the Transaction Summary by Subagent report
     */
    public static final String SC_TRX_SUMM_BY_SUBAGENT = "TrxSummBySubAgent";

    /**
     * Identify the Transaction Summary by terminal report
     */
    public static final String SC_TRX_SUMM_BY_TERMINAL = "TrxSummByTerm";

    /**
     * User payments summary report identifier
     * /admin/reports/payments/user_payments_summary.jsp
     */
    public static final String SC_USR_PAYMENT_SUM = "UserPaymentsSummary";

    /**
     * Identfy the user payment totals report
     * /admin/reports/payments/user_payments_totals.jsp
     */
    public static final String SC_USR_PAYMENT_TOTALS = "UserPaymentsTotals";

    /**
     * Identify the transaction error summary by error types
     */
    public static final String SC_TRX_ERROR_SUM_BY_TYPES = "TrxErrorSummByTypes";

    /**
     * Identify the transaction error summary by merchants
     */
    public static final String SC_TRX_ERROR_SUM_BY_MERCHANT = "TrxErrorSummByMerchant";

    /**
     * Identify the transaction error summary by provider
     */
    public static final String SC_TRX_ERROR_SUM_BY_PROVIDERS = "TrxErrorSummByProviders";

    /**
     * Identify the transaction summary by city
     */
    public static final String SC_TRX_SUMM_BY_CITY = "TrxSummByCity";

    /**
     * Identify the payment detail report
     */
    public static final String SC_TRX_PAYMENT_DETAIL_REPORT = "PaymentDetailReport";

    /**
     * Identify the daily liability report
     */
    public static final String SC_TRX_DAILY_LIABILITY_REPORT = "DailyLiabilityReport";

    /**
     * Identify the transaction summary by terminal type
     */
    public static final String SC_TRX_SUM_BY_TERMINAL_TYPE = "TrxSummByTerminalType";

    /**
     * Identify the Top Up Convenience Card Summary By Carrier report
     */
    public static final String SC_TOPUP_CONVENIENCE_CARD_SUM = "TopupConvenienceCardSumCarr";

    /**
     * Identify the VOID TOPUP REQUEST REPORT
     */
    public static final String SC_VOID_TOPUP_REQUESTS = "VoidTopupRequestsReport";

    /**
     * Identify the PIN INVENTORY REPORT
     */
    public static final String SC_PIN_INVENTORY = "PinInventoryReport";

    /**
     * Identify the PIN INVENTORY report
     */
    public static final String SC_PIN_RETURN_REQUESTS = "PinReturnRequestsReport";

    /**
     * Identify the PIN INVENTORY report
     */
    public static final String SC_ISO_ACTIONS_ON_REP_ACCOUNTS = "IsoActionsOnRepAccountsReport";

    /**
     * Identify the PIN INVENTORY report
     */
    public static final String SC_EXTERNAL_CREDIT_OR_PAYMENT = "ExternalCreditOrPaymentReport";

    /**
     * Identify the the transaction summary by phone number report
     */
    public static final String SC_TRX_SUMM_BY_PHONE_NUMBER = "TrxSummByPhoneNumber";

    /**
     * Identify the the transaction summary by amount
     */
    public static final String SC_TRX_SUMM_BY_AMOUNT = "TrxSummByAmount";

    /**
     * Identify the the credit or payment summary by user
     */
    public static final String SC_CRED_PAYMENT_SUMM_BY_USER = "CredPaymentSummByUser";

    /**
     * Identify the PIN INVENTORY report
     */
    public static final String SC_TRANSACTIONS = "TransactionsReport";

    /**
     * Identify the Rep Credit Assignment history report
     */
    public static final String SC_REP_CREDIT_ASSIGNMENT_HIST = "RepCreditAssignmentHistory";

    /**
     * Identify the Subagent Credit Assignment history report
     */
    public static final String SC_SUBAGENT_CREDIT_ASSIGNMENT_HIST = "SubAgentCreditAssignmentHistory";

    /**
     * Identify the Aagent Credit Assignment history report
     */
    public static final String SC_AGENT_CREDIT_ASSIGNMENT_HIST = "AgentCreditAssignmentHistory";

    /**
     * Identify the PIN STICK AVAILABLE
     */
    public static final String SC_PIN_STOCK_AVAILABLE = "PinStockAvailable";

    /**
     * Identify the PIN STICK LOADED
     */
    public static final String SC_PIN_STOCK_LOADED = "PinStockLoaded";

    /**
     * Identify the Carrier summary report
     */
    public static final String SC_CARRIER_SUMMARY_REPORT = "CarrierSummaryReport";

    /**
     * Identify the transactions summary by clerkr
     */
    public static final String SC_TRX_SUMM_BY_CLERK = "TrxSummByClerk";

    /**
     * Identify the transactions summary by product
     */
    public static final String SC_TRX_SUMM_BY_PRODUCT = "TrxSummByProduct";

    /**
     * Identify the transactions summary by product/merchants
     */
    public static final String SC_TRX_SUMM_BY_PRODUCT_MERCHANT = "TrxSummByProductMerchant";

    /**
     * Identify the merchant credits report
     */
    public static final String SC_MERCHANT_CREDIT_REPORT = "MerchantCreditReport";

    /**
     * Identify the Pin Remaining Inventy Report
     */
    public static final String SC_PIN_REMAINING_INVENTORY_REPORT = "PinRemainingInventoryReport";

    /**
     * Identify the Pin Remaining Inventy Report
     */
    public static final String SC_MERCHANT_CREDIT_ACCOUNT_STATUS_REPORT = "MerchantCreditAccountStatusDetail";
    
    /**
     * Identify the Payment Requests Report
     */
    public static final String SC_PAYMENT_REQUEST_REPORT = "PaymentRequestReport";

    /**
     * Identify the Reconciliation Detail Report
     */
    public static final String SC_RECONCILIATION_DETAIL_REPORT = "ReconciliationDetailReport";
    
    /**
     * Identify the Reconciliation Sumary Report
     */
    public static final String SC_RECONCILIATION_SUMARY_REPORT = "ReconciliationSumaryReport";    

    /**
     * Jsp page where setup all about report scheduling parameters.
     */
    public static final String PAGE_TO_SCHEDULE_REPORTS = "/support/admin/reports/scheduleReports/scheduleReports.jsp";
    
    /**
     * If is enable, Password must not contain user Id or user name.
     */
    String PASSWORD_WITHOUT_ID_PARAM  = "validatePasswordWithoutId";

    /**
     * Length consecutive characters not allowed.
     */
    int CHAR_LENGTH_NOT_ALLOWED  = 4;
    
    /**
     * Identify the Rep merchants balance sum, this is not a report but a 
     * calculation whose query needs a limit
     */
    public static final String SC_REP_MERCHANT_BALANCE_SUM = "RepMerchantBalanceSum";    
    
    public static final String PERM_ALLOW_USE_SETTING_ACH_STATEMENT_TEMPLATE = "166";
    
    public static final String PERM_ALLOW_USE_SETTING_INSTANT_SPIFF = "175";
}
