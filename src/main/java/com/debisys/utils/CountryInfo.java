package com.debisys.utils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.HashMap;
import org.apache.log4j.Category;
import com.emida.utils.dbUtils.DbUtils;
import com.emida.utils.dbUtils.InputParameter;
import com.emida.utils.dbUtils.OutputParameter;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author ldelarosa This klass contains methods to manipulate the TimeZone information
 */
@SuppressWarnings("deprecation")
public class CountryInfo
{
	private static Category cat = Category.getInstance(CountryInfo.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");

	/**
	 * Retrieves Country Names available for a specific ISO  
	 * @param ISO ID
	 * @return all country names available for ISO
	 * @throws Exception
	 */
	public static String[][] getAllCountries(String ISO) throws Exception
	{
		cat.debug("getAllCountries "+ ISO);
		String[][] sResult;
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String countryname = "" ,altcountries = "",countryid="";
		try
		{
			cnx = TorqueHelper.getConnection(CountryInfo.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				CountryInfo.cat.error("Can't get database connection");
				throw new Exception();
			}
			 ps = cnx.prepareStatement(sql_bundle.getString("getAllCountries"));

			ps.setDouble(1, Double.parseDouble(ISO));
			rs = ps.executeQuery();

			if (rs.next())
			{
				countryid = rs.getString("country_id");
				countryname =  rs.getString("country");
				altcountries = rs.getString("AltCountry");
				cat.debug("===> "+ countryid); 
				cat.debug("===> "+ countryname); 
				cat.debug("===> "+ altcountries); 
			}
		}
		catch (Exception e)
		{
			CountryInfo.cat.error("Error during getAllCountries", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				CountryInfo.cat.error("Error during closeConnection", e);
			}
		}
		if(countryname  == null && altcountries == null ){
			sResult = new String[1][2];
			sResult[0][0] = "";
			sResult[0][1] = "";
			return sResult;
		}		
		else if(countryname.length() == 0 && altcountries.length() == 0){		
			sResult = new String[1][2];
			sResult[0][0] = "";
			sResult[0][1] = "";
			return sResult;
		}
		else if (altcountries == null){
			sResult = new String[1][2];
			sResult[0][0] = countryid;
			sResult[0][1] = countryname; 
		}
		else if (altcountries.length() == 0){
			sResult = new String[1][2];
			sResult[0][0] = countryid;
			sResult[0][1] = countryname; 
		}
		else {
			HashMap map = getAllCountriesID();
			String[] tokens = altcountries.split(",");
			sResult = new String[tokens.length + 1][2];
			sResult[0][0] = countryid;
			sResult[0][1] = countryname; 
			for (int j=0 ; j<tokens.length ; j++){
				if( map.containsKey( Integer.parseInt(tokens[j]) ) )
				{
					sResult[j+1][0] =  tokens[j].toString();
					sResult[j+1][1] =  map.get(Integer.parseInt(tokens[j])).toString();
				}else{
				//that means there is an error in the database or countries or alter table
				//this will disbale all alternative countries
				sResult = new String[1][2];
				sResult[0][0] = countryid;
				sResult[0][1] = countryname; 
				j= tokens.length;
				}
			}
		}

		cat.debug("size result returned "+ sResult.length); 
		for(int i=0;i<sResult.length;i++)
			cat.debug("result["+i+"] "+ sResult[i][0]+" -> "+sResult[i][1]); 
		return sResult;
	}

	/**
	 * Retrieves Country Names available for a specific ISO  
	 * @param ISO ID
	 * @return all country names available for ISO
	 * @throws Exception
	 */
	public static HashMap getAllCountriesID() throws Exception
	{
		HashMap sResult = new HashMap();
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			cnx = TorqueHelper.getConnection(CountryInfo.sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				CountryInfo.cat.error("Can't get database connection");
				throw new Exception();
			}
			 ps = cnx.prepareStatement(sql_bundle.getString("getAllCountriesID"));

			rs = ps.executeQuery();
			while (rs.next())
			{
				sResult.put(rs.getInt("countryid"),rs.getString("countryname"));
			}
		}
		catch (Exception e)
		{
			CountryInfo.cat.error("Error during getAllCountriesID", e);
			throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				CountryInfo.cat.error("Error during closeConnection", e);
			}
		}
		return sResult;
	}
	/***
	 * Get the top-level ISO ID for any entity ID and an entity type
	 * @param _entityID Rep ID / Merchant ID
	 * @param _entityType Type of the entity (8 = merchant ....)
	 * @return String of the ISO ID
	 */
	public static String getIso(String _entityID, String _entityType) {
		cat.debug("Begin getIso");
		String _isoID = "";
	
		InputParameter[] inParms = new InputParameter[2];
		inParms[0] = new InputParameter(_entityID, "NUMERIC");
		inParms[1] = new InputParameter(_entityType, "INTEGER");
		
		OutputParameter[] outParms = new OutputParameter[1];
		outParms[0] = new OutputParameter("ISOID", "3", "NUMERIC");
		
		Vector<String> spResults = DbUtils.ExecuteStoredProcedure(
				sql_bundle.getString("pkgDefaultDb"), 
				sql_bundle.getString("getIso"), 
				inParms, 
				outParms
			);
		
		_isoID = spResults.get(0);
		cat.debug("End getIso");
		return _isoID;
	}
}
