package com.debisys.utils;

import javax.servlet.http.HttpServletRequest;

public class ConfigHelper {
	private String baseHref;
	private String brandedCompanyName;
	private String domainName;
	private String headerBackground;
	private String imageName;
	private String requestUrl;
	private String serverName;
	private String serverPort;

	public ConfigHelper(HttpServletRequest request){
		serverPort = Integer.toString(request.getServerPort());
		requestUrl = request.getRequestURL().toString();
		serverName = request.getServerName();
		baseHref = serverName;
		domainName = "";
		
		domainName = "";
		if (serverPort != null && !serverPort.equals("") && !serverPort.equals("80") && !serverPort.equals("443")){
			baseHref += ":" + serverPort;
		}

		if ((serverName != null) && (!serverName.equals("")) && !serverName.equalsIgnoreCase("support.debisys.com")){
			int index = serverName.indexOf('.');
			if (index == -1){
				domainName = serverName;
			}else{
				int index2 = serverName.indexOf('.', index + 1);
				if (index2 == -1){
					domainName = serverName;
				}else{
					domainName = serverName.substring(index + 1);
				}
			}
		}else{
			serverName = "support.debisys.com";
			domainName = "debisys.com";	
		}
		
		imageName = "emidaLogo.png";
		headerBackground = "background=\"images/banner.gif\"";
		brandedCompanyName="Emida";

		if (domainName.equalsIgnoreCase("debisys.com")){
			if(serverName.toLowerCase().indexOf("qualitycalling") != -1){
				brandedCompanyName="Quality Calling";
				imageName="quality_calling_logo.jpg";
				headerBackground = "";
				//strProtocol = "http";
			}else if(serverName.toLowerCase().indexOf("giromex") != -1){
				brandedCompanyName="Giromex";
				imageName="giromex_logo.gif";
				headerBackground = "";
				//strProtocol = "http";
			}else if(serverName.toLowerCase().indexOf("support") == -1){
				// only "support.debisys.com" has the ssl cert
				//strProtocol = "http";
			}
		}else if (domainName.equalsIgnoreCase("posalink.com")){
			//support.posalink.com
			brandedCompanyName="POSA Systems";
			imageName="posa.gif";
			headerBackground = "background=\"images/banner.gif\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("econnectprepaid.com")){
			brandedCompanyName="Econnect";
			imageName="EConnectVIPLogoSS_PCTerm.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("imcprepaid.com")){
			brandedCompanyName="IMC";
			imageName="imc.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("payfirst.net")){
			brandedCompanyName="PayFirst";
			imageName="payfirst_logo.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("trueld.com")){
			brandedCompanyName="TrueLD";
			imageName="trueld.jpg";
			headerBackground = "background=\"images/banner.gif\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("candw.ky")){
			brandedCompanyName="Cable and Wireless";
			imageName="caw.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("ppslogin.com")){
			brandedCompanyName="Prepaid Stations";
			imageName="pps_logo.gif";
			headerBackground = "background=\"images/banner.gif\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("americanwireless.com")){
			brandedCompanyName="American Wireless";
			imageName="aw_logo.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("ameritelmobile.com")){
			brandedCompanyName="Ameritel";
			imageName="ameritel_logo.gif";
			headerBackground = "background=\"images/ameritel_banner.gif\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("numoneyaopc.com")){
			brandedCompanyName="NuMoney";
			imageName="numoney_logo.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("mrprepaid.com")){
			brandedCompanyName="MrPrepaid";
			imageName="mrprepaid_logo.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("gpsprepaid.com")){
			brandedCompanyName="GPS Prepaid";
			imageName="gpsprepaid.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("topcommusa.com")){
			brandedCompanyName="TopComm";
			imageName="topwireless_logo.jpg";
			headerBackground = "background=\"images/topwireless_background.jpg\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("skynetposa.com")){
			brandedCompanyName="Skynet";
			imageName="skynet.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("puntoflex.com") || domainName.equalsIgnoreCase("puntoflex.net")){
			brandedCompanyName="RPM";
			imageName="rpm_logo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("ambess.com")){
			brandedCompanyName="Ambess";
			imageName="ambesslogo_tag.JPG";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("sigue.com")){
			brandedCompanyName="Sigue";
			imageName="sigue_logo.jpg";
			headerBackground="background=\"images/sigue_banner.jpg\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("bigcell.net")){
			brandedCompanyName="IT";
			//imageName="bigcell_logo.gif";
			//headerBackground = "background=\"images/bigcell_banner.jpg\"";
			imageName="it_logo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("telem.an")){
			brandedCompanyName="Telcell";
			imageName="telcell.gif";
			headerBackground = "background=\"images/telcell_banner.gif\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("belizetelemedia.net")){
			brandedCompanyName="BTL";
			imageName="belize_logo.jpg";      
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("spectrum-wireless.com")){
			brandedCompanyName="Spectrum Wireless";
			imageName="spectrum_logo.jpg";      
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("c1.bm")){
			brandedCompanyName="Bermuda Digital";
			imageName="bermuda_logo.jpg";      
			headerBackground = "";
			//strProtocol = "https";
		}else if(domainName.equalsIgnoreCase("topupdauphin.com")){
			brandedCompanyName="Dauphin";
			imageName="banner_logiciel.jpg";      
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("superprepaid.net")){
			brandedCompanyName="Super Prepaid";
			imageName="superprepaid_banner.jpg";
			headerBackground = "background=\"images/superprepaid_banner_bg.jpg\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("gpa.net")){
			brandedCompanyName="GPA";
			imageName="GPA_Whls.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("mio.an")){
			brandedCompanyName="MIO";
			imageName="mio_logo.gif";
			headerBackground = "background=\"images/mio_banner.gif\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("bvinet.vg")){
			brandedCompanyName="CCT";
			imageName="cct_logo.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("marcecoltd.com")){
			brandedCompanyName="Marceco";
			imageName="MarcecoLogo.gif";      
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("tci-support.oclgroup.co.nz") != -1){
			brandedCompanyName="TCI";
			imageName="TCICookIslandsSSLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("oclgroup.co.nz")){
			brandedCompanyName="Oceanic";
			//imageName="Oceanic_Logo.gif";
			imageName="OCL.jpg";      
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("emida.net")){
			if(serverName.toLowerCase().indexOf("ghana") != -1){
				brandedCompanyName="GHANA";
				imageName="PayNet_logo.gif";      
				headerBackground = "";
				//strProtocol = "http";
			}
		}else if(domainName.equalsIgnoreCase("skyhonduras.hn")){
			brandedCompanyName="SKY";
			imageName="digicelHonduras.jpg";
			headerBackground = "";
			//strProtocol = "https";
		}else if(domainName.equalsIgnoreCase("skysolutions.com.pa")){
			brandedCompanyName="SKY";
			imageName="SkySolutionsPanama.gif";
			headerBackground = "";
			//strProtocol = "https";
		}else if(domainName.equalsIgnoreCase("localhost")){
			//brandedCompanyName="RPM"; 
			imageName="localhost.png";
			headerBackground = "background=\"images/banner.gif\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("")){ // They never send the domain
			brandedCompanyName="";
			imageName="Digital Communications_logo.JPG";
			headerBackground = "";
			//strProtocol = "http";    
		}else if(requestUrl.indexOf("sopelglobo.terecargamos.com") != -1){
			brandedCompanyName="El Globo Te Tecargamos";
			imageName="ElGloboTeRecargamosSSLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("soppuntorecarga.terecargamos.com") != -1){
			brandedCompanyName="PuntoRecarga";
			imageName="PuntoDeRecargaOtros.png";
			headerBackground = "background=\"images/PuntoDeRecargaBanner.png\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("terecargamos.com")){
			brandedCompanyName="Terecargamos";
			imageName="TeRecargamosLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("tixetradingdr.com")){
			brandedCompanyName="Tixe";
			imageName="TixeLogo.JPG";
			headerBackground = "";
			//strProtocol = "https";
		}else if(domainName.equalsIgnoreCase("rpmmx.net")){
			brandedCompanyName="Procotel";
			imageName="ProcotelLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("sopelglobo.microrecargas.com") != -1){
			brandedCompanyName="El Globo Microrecargas";
			imageName="ElGloboMicrorecargasSSLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("soppuntorecarga.microrecargas.com") != -1){
			brandedCompanyName="PuntoRecarga";
			imageName="PuntoDeRecarga.png";
			headerBackground = "background=\"images/PuntoDeRecargaBanner.png\"";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("microrecargas.com")){
			brandedCompanyName="Microrecargas";
			imageName="MicrorecargasLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("nsiposa.com")){
			brandedCompanyName="NSI";
			imageName="NSILogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("tiemporeal.com.mx")){
			brandedCompanyName="CityClub";
			imageName="CityClubLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("soporte.ccrecargas.com") != -1){
			brandedCompanyName="CityClub";
			imageName="CityClubLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";    	
		}else if(domainName.equalsIgnoreCase("pctermstage.debisys.com")){
			brandedCompanyName="ICE";
			imageName="ICELogo.JPG";
			headerBackground = "background=\"images/IceBackground.JPG\"";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("anguilla.support.faceyetrans.com") != -1){
			brandedCompanyName="Anguilla";
			imageName="AnguillaLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("barbados.support.faceyetrans.com") != -1){
			brandedCompanyName="Barbados";
			imageName="BarbadosLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("jamaica.support.faceyetrans.com") != -1){
			brandedCompanyName="Jamaica";
			imageName="JamaicaLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("svg.support.faceyetrans.com") != -1){
			brandedCompanyName="StVincent";
			imageName="StVincentLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("sulphurtel.com")){
			brandedCompanyName="SulphurTel";
			imageName="SulphurTelLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("superprepaid7.com")){
			brandedCompanyName="Superprepaid";
			imageName="SuperprepaidLogo.gif";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("antigua.support.faceyetrans.com") != -1){
			brandedCompanyName="Antigua";
			imageName="AntiguaLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("dominica.support.faceyetrans.com") != -1){
			brandedCompanyName="Dominica";
			imageName="DominicaLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("grenada.support.faceyetrans.com") != -1){
			brandedCompanyName="Grenada";
			imageName="GrenadaLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("skn.support.faceyetrans.com") != -1){
			brandedCompanyName="StKittsNevis";
			imageName="StKittsNevisLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("stlucia.support.faceyetrans.com") != -1){
			brandedCompanyName="StLucia";
			imageName="StLuciaLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("bermuda.support.faceyetrans.com") != -1){
			brandedCompanyName="Bermuda";
			imageName="BermudaLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("bvi.support.faceyetrans.com") != -1){
			brandedCompanyName="British Vigin Islands";
			imageName="BVILogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("trinidad.support.faceyetrans.com") != -1){
			brandedCompanyName="Trinidad & Tobago";
			imageName="TrinidadTobagoLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("tandc.support.faceyetrans.com") != -1){
			brandedCompanyName="Turks-Caicos";
			imageName="TurksCaicosLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("cayman.support.faceyetrans.com") != -1){
			brandedCompanyName="Cayman";
			imageName="CaymanLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("soporteccotros.tiemporeal.com.mx") != -1){
			brandedCompanyName="City Club";
			imageName="CityClubNoTelcelLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("soporteotros.ccrecargas.com") != -1){
			brandedCompanyName="City Club";
			imageName="CityClubNoTelcelLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";    
		}else if(requestUrl.indexOf("pos.stiprepaid.com") != -1){
			brandedCompanyName="STi Prepaid LLC";
			imageName="STiprepaid-logo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("kingofmobileusa.com")){
			brandedCompanyName="Kace Management";
			imageName="KaceManagementLogo.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("globalpros.net")){
			brandedCompanyName="Global Pros";
			imageName="GlobalProsLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("ez-prepaid.com")){
			brandedCompanyName="EZ Prepaid ";
			imageName="ezp-logo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("emidamex.net")){
			brandedCompanyName="EmidaMex";
			imageName="EmidaMex.png";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("easygowireless.com")){
			brandedCompanyName="Easy Go Wireless";
			imageName="SupportSiteEGP.JPG";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("mymoneytalkz.com")){
			brandedCompanyName="Money Talkz";
			imageName="MoneyTalkz.png";
			headerBackground = "";
			//strProtocol = "http";
		}else if(domainName.equalsIgnoreCase("soporte.mobilmex.com")){
			brandedCompanyName="Mobil Mex";
			imageName="MobilMexLogo.jpg";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("support.mepsgbs.ae") != -1){
			brandedCompanyName="GBS/MINT";
			imageName="GBSMINT_SS_BRANDING.png";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("soporte.mobilmex.com") != -1){
			brandedCompanyName="Mobil Mex";
			imageName="MobilMex.png";
			headerBackground = "";
			//strProtocol = "http";
		}else if(requestUrl.indexOf("wan.net") != -1){
			brandedCompanyName="WAN";
			imageName="WAN.png";
			headerBackground = "";
			//strProtocol = "http";
		}
                else{
			//force them to debisys
			//serverName = "support.debisys.com";
			//strProtocol = "http";
		}		
	}

	/**
	 * @return the baseHref
	 */
	public String getBaseHref() {
		return baseHref;
	}

	/**
	 * @return the brandedCompanyName
	 */
	public String getBrandedCompanyName() {
		return brandedCompanyName;
	}
	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @return the headerBackground
	 */
	public String getHeaderBackground() {
		return headerBackground;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @return the requestUrl
	 */
	public String getRequestUrl() {
		return requestUrl;
	}

	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @return the serverPort
	 */
	public String getServerPort() {
		return serverPort;
	}
	/**
	 * @param baseHref the baseHref to set
	 */
	public void setBaseHref(String baseHref) {
		this.baseHref = baseHref;
	}


	/**
	 * @param brandedCompanyName the brandedCompanyName to set
	 */
	public void setBrandedCompanyName(String brandedCompanyName) {
		this.brandedCompanyName = brandedCompanyName;
	}

	/**
	 * @param domainName the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * @param headerBackground the headerBackground to set
	 */
	public void setHeaderBackground(String headerBackground) {
		this.headerBackground = headerBackground;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * @param requestUrl the requestUrl to set
	 */
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	/**
	 * @param serverPort the serverPort to set
	 */
	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}
}
