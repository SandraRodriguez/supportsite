package com.debisys.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.emida.utils.dbUtils.TorqueHelper;

public class CalendarBussines extends BaseDaysMonths {

	private static Logger cat = Logger.getLogger(CalendarBussines.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
	
	/**
	 * Enum to indicate if search days or months
	 * @author nmartinez
	 *
	 */
	public static enum CALENDAR_OBJECTS_TYPE { DAYS, MONTHS };
	
	
	
	
	
	/**
	 * @param typeObject
	 * @param language
	 * @return
	 * @throws Exception
	 */
	public static ArrayList<BaseDaysMonths> getCalendarObjectsByType(CALENDAR_OBJECTS_TYPE typeObject, String language) throws Exception
	{
		ArrayList<BaseDaysMonths> calendarObject = new ArrayList<BaseDaysMonths>();
		Connection cnx = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
                        if (!language.equals("1") && !language.equals("2")) {
                            language = "1";
                        }
                    
			cnx = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));

			if (cnx == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}
			
			String sql = "";
			if ( typeObject.equals(CalendarBussines.CALENDAR_OBJECTS_TYPE.DAYS) )
			{
				sql = sql_bundle.getString("getdays");				
			}
			else
			{
				sql = sql_bundle.getString("getmonths");				
			}
			ps = cnx.prepareStatement(sql);
			ps.setString(1, language);
			rs = ps.executeQuery();
			
			while ( rs.next() )
			{
				BaseDaysMonths objectCalendar = new BaseDaysMonths();
				objectCalendar.setId( rs.getString("Id"));
				objectCalendar.setCode( rs.getString("code"));
				objectCalendar.setName( rs.getString("name"));
				objectCalendar.setShortName( rs.getString("shortname"));
				objectCalendar.setOrder( rs.getString("orderView") );
				calendarObject.add( objectCalendar );
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getCalendarObjectsByType ", e);
			//throw new Exception();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cnx, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection - getCalendarObjectsByType ", e);
			}
		}
		return calendarObject;
	}
	
	
}
