package com.debisys.utils;

import net.sf.jasperreports.engine.type.CalculationEnum;

/**
 * @author nmartinez
 *
 */
public class ColumnReport {

	
	public static enum CALCULATION {
	    SUM, AVG 
	}
	
	private String sqlName;
	private String languageDescription;
	private Class<?> classNameData;
	private boolean isSumValue = false;	
	private boolean putInTemplate = true;
	private String valueExpressionWhenIsCalculate;
	private CalculationEnum typeTotal;
	
	
	/**
	 * @param sqlColumnName
	 * @param languageDescriptionColumn
	 * @param classNameDataColumn
	 * @param isSummary
	 */
	public ColumnReport(String sqlColumnName,String languageDescriptionColumn,Class<?> classNameDataColumn, boolean isSummary)
	{
		this.sqlName = sqlColumnName;
		this.languageDescription = languageDescriptionColumn;
		this.classNameData = classNameDataColumn;		
		this.isSumValue = isSummary;
		this.putInTemplate = true;
		typeTotal = CalculationEnum.SUM;
	}

	/**
	 * @return the sqlName
	 */
	public String getSqlName() {
		return sqlName;
	}

	/**
	 * @param sqlName the sqlName to set
	 */
	public void setSqlName(String sqlName) {
		this.sqlName = sqlName;
	}	

	/**
	 * @return the languageDescription
	 */
	public String getLanguageDescription() {
		return languageDescription;
	}

	/**
	 * @param languageDescription the languageDescription to set
	 */
	public void setLanguageDescription(String languageDescription) {
		this.languageDescription = languageDescription;
	}

	/**
	 * @param classNameData the classNameData to set
	 */
	public void setClassNameData(Class<?> classNameData) {
		this.classNameData = classNameData;
	}

	/**
	 * @return the classNameData
	 */
	public Class<?> getClassNameData() {
		return classNameData;
	}

	/**
	 * @param isSumValue the isSumValue to set
	 */
	public void setSumValue(boolean isSumValue) {
		this.isSumValue = isSumValue;
	}

	/**
	 * @return the isSumValue
	 */
	public boolean isSumValue() {
		return isSumValue;
	}

	/**
	 * @param putInTemplate the putInTemplate to set
	 */
	public void setPutInTemplate(boolean putInTemplate) {
		this.putInTemplate = putInTemplate;
	}

	/**
	 * @return the putInTemplate
	 */
	public boolean isPutInTemplate() {
		return putInTemplate;
	}

	/**
	 * @param valueExpressionWhenIsCalculate the valueExpressionWhenIsCalculate to set
	 */
	public void setValueExpressionWhenIsCalculate(
			String valueExpressionWhenIsCalculate) {
		this.valueExpressionWhenIsCalculate = valueExpressionWhenIsCalculate;
	}

	/**
	 * @return the valueExpressionWhenIsCalculate
	 */
	public String getValueExpressionWhenIsCalculate() {
		return valueExpressionWhenIsCalculate;
	}

	/**
	 * @param typeTotal the typeTotal to set
	 */
	public void setTypeTotal(CalculationEnum typeTotal) {
		this.typeTotal = typeTotal;
	}

	/**
	 * @return the typeTotal
	 */
	public CalculationEnum getTypeTotal() {
		return typeTotal;
	}

	@Override
	public String toString(){
		return "sqlName: " + sqlName;
	}
	
}
