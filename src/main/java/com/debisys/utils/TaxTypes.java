package com.debisys.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

public class TaxTypes 
{
	static Category cat = Category.getInstance(TaxTypes.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
    
    /***
     * 
     * @return
     */
    public static Hashtable<Integer, String> getTaxTypes()
    {
    	Hashtable<Integer, String> taxTypes = new Hashtable<Integer, String>();
    	Connection dbConn = null;
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
    		if (dbConn == null)
    		{	
    			cat.error("Can't get database connection");
    			throw new Exception();
    		}
    		
    		PreparedStatement pstmt = dbConn.prepareStatement(sql_bundle.getString("getTaxTypes"));
    		ResultSet rs = pstmt.executeQuery();
    		while(rs.next())
    		{
    			taxTypes.put(rs.getInt("type_id"), rs.getString("description"));
    		}

    		rs.close();
    		pstmt.close();
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during getTaxTypes", e);
    	}
    	finally
    	{
    		try
    		{
    			Torque.closeConnection(dbConn);
    		}
    		catch (Exception e)
    		{
    			cat.error("Error during release connection", e);
    		}
    	}
    	return taxTypes;
    }
}
