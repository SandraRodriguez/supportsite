package com.debisys.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;


public class Log{
    //log4j logging
    private static Logger cat = Logger.getLogger(Log.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
    public static void write(SessionData sessionData, String strMessage, int intLogCategoryId, String appliedRefId, String appliedRefType)
    {
      Connection dbConn = null;
      PreparedStatement pstmt = null;
      try
      {

        dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
        if (dbConn == null)
        {
          cat.error("Can't get database connection");
          throw new Exception("Can't get database connection");
        }
        pstmt = dbConn.prepareStatement(sql_bundle.getString("addLog"));
        pstmt.setInt(1, intLogCategoryId);
        pstmt.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
        String strRefId = sessionData.getProperty("ref_id");
        if (strRefId != null && strRefId.trim().length() > 0) {
            pstmt.setDouble(3, Double.parseDouble(strRefId));
        } else {
        	pstmt.setNull(3, java.sql.Types.DOUBLE);
        }
        String strRefType = sessionData.getProperty("ref_type");
        if (strRefType != null && strRefType.trim().length() > 0) {
            pstmt.setInt(4, Integer.parseInt(strRefType));
        } else {
        	pstmt.setNull(4, java.sql.Types.INTEGER);
        }       
        
        if (appliedRefId == null)
        {
          pstmt.setNull(5, java.sql.Types.NUMERIC);
          pstmt.setNull(6, java.sql.Types.INTEGER);
        }
        else
        {
          pstmt.setDouble(5, Double.parseDouble(appliedRefId));
          //hardcode to merchant for now
          pstmt.setInt(6, Integer.parseInt(appliedRefType));
        }
        pstmt.setString(7, sessionData.getProperty("username"));
        pstmt.setString(8, strMessage);

        pstmt.executeUpdate();        
      }
      catch (Exception e)
      {
        cat.error("Error during addLog", e);
      }
      finally
      {
        try
        {
          TorqueHelper.closeConnection(dbConn, pstmt, null);
        }
        catch (Exception e)
        {
          cat.error("Error during release connection", e);
        }
      }

    }


}