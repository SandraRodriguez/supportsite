package com.debisys.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.security.SecureRandom;

public class StringUtil {

    public static final char[] CHARSET_AZ_09 = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

	public static String toString(String strString) {
		if (strString == null) {
			strString = "";
		} else {
			strString = strString.trim();
		}
		return strString;
	}

	public static String arrayToString(String[] a, String separator) {
		StringBuffer result = new StringBuffer();
		if (a != null && a.length > 0) {
			result.append(a[0]);
			for (int i = 1; i < a.length; i++) {
				result.append(separator);
				result.append(a[i]);
			}
		}
		return result.toString();
	}

	/**
	 * Makes sure a string is combination of numbers and letters.
	 * 
	 * @param strString
	 * @return true if string is valid, false otherwise
	 */
	public static boolean isAlphaNumeric(String strString) {
		boolean bValid = true;
		if ((strString != null) && (strString.length() > 0)) {
			StringBuffer sbString = new StringBuffer(strString);
			int intLength = sbString.length();
			for (int intChar = 0; intChar < intLength; intChar++) {
				char charCurrent = sbString.charAt(intChar);
				if (!Character.isLetterOrDigit(charCurrent)) {
					// If not a character or digit, check for allowable symbols
					if ((charCurrent != '.') && (charCurrent != '_')
							&& (charCurrent != '-') && (charCurrent != '@')) {
						bValid = false;
						break;
					}
				}
			}
		} else {
			bValid = false;
		}
		return bValid;
	}

	/**
	 * Makes sure a string is combination of numbers and letters.
	 * 
	 * @param strString
	 * @return true if string is valid, false otherwise
	 */
	public static boolean isAlphaNumericRegex(String strString) {
		boolean bValid = true;

		Pattern pattern = Pattern.compile("[A-Za-z0-9]*");
		// creamos el Matcher a partir del patron, la cadena como parametro
		Matcher encaja = pattern.matcher(strString);
		if ((strString != null) && encaja.matches()) {
			bValid = true;
		} else {
			bValid = false;
		}
		return bValid;
	}

	
	/**
	 * Makes sure a string is combination of numbers and letters and -.
	 * 
	 * @param strString
	 * @return true if string is valid, false otherwise
	 */
	public static boolean isAlphaNumericRec(String strString) {
		boolean bValid = true;

		Pattern pattern = Pattern.compile("[A-Za-z0-9\\-]*");
		// creamos el Matcher a partir del patron, la cadena como parametro
		Matcher encaja = pattern.matcher(strString);
		if ((strString != null) && encaja.matches()) {
			bValid = true;
		} else {
			bValid = false;
		}
		return bValid;
	}

	
	
	
	/**
	 * Makes sure a string is combination of numbers and letters.
	 * 
	 * @param strString
	 * @return true if string is valid, false otherwise
	 */
	public static boolean isNumericRegex(String strString) {
		boolean bValid = true;

		Pattern pattern = Pattern.compile("[0-9]*");
		// creamos el Matcher a partir del patron, la cadena como parametro
		Matcher encaja = pattern.matcher(strString);
		if ((strString != null) && encaja.matches()) {
			bValid = true;
		} else {
			bValid = false;
		}
		return bValid;
	}

	/**
	 * Makes sure a string is combination of letters and spaces.
	 * 
	 * @param strString
	 * @return true if string is valid, false otherwise
	 */
	public static boolean isAlpha(String strString) {
		boolean bValid = true;
		if ((strString != null) && (strString.length() > 0)) {
			StringBuffer sbString = new StringBuffer(strString);
			int intLength = sbString.length();
			for (int intChar = 0; intChar < intLength; intChar++) {
				char charCurrent = sbString.charAt(intChar);
				if (!Character.isLetter(charCurrent)) {
					// If not a character , check for allowable symbols
					if (charCurrent != ' ') {
						bValid = false;
						break;
					}
				}
			}
		} else {
			bValid = false;
		}
		return bValid;
	}

	public static String formatTerminalResponse(String strString) {
		int inputLength = strString.length();
		StringBuffer sb = new StringBuffer();
		int pos = 0;
		int nextPos, endPos;
		char current;
		char lookAhead;
		while (pos < inputLength) {
			current = strString.charAt(pos);
			switch (current) {
			case '%':
				nextPos = pos + 1;
				if (nextPos < inputLength) {
					lookAhead = strString.charAt(nextPos);
					switch (Character.toLowerCase(lookAhead)) {
					case 'j':
					case 'k':
					case 'l':
						sb.append("<br>");
						pos += 2;
						break;
					case 'm':
						sb.append("(merchant)");
						pos += 2;
						break;
					case 'p':
					case 'o':
						sb.append("(pin)");
						pos += 2;
						break;
					case 'c':
						sb.append("(balance)");
						pos += 2;
						break;
					case 'a':
						sb.append("(amount)");
						pos += 2;
						break;
					case 'i':
						sb.append("(invoice)");
						pos += 2;
						break;
					case 'h':
					case 'n':
						sb.append("(account)");
						pos += 2;
						break;
					case 'd':
						sb.append("(date)");
						pos += 2;
						break;
					case 'r':
						sb.append("(trans id)");
						pos += 2;
						break;
					case 'w':
						sb.append("(serial)");
						pos += 2;
						break;
					default:
						sb.append("%");
						pos++;
					}
				} else {
					sb.append(current);
					pos++;
				}
				break;
			case '~':
				pos++;
				break;
			default:
				sb.append(current);
				pos++;
			}
		}
		return sb.toString();
	}

	/**
	 * Inserts &nbsp; at beginning and end of parameter and also converts spaces
	 * to &nbsp; At the end append a Break in order to show the sort arrow below
	 * the text
	 * 
	 * @param sText
	 * @return Modified string
	 */
	public static String formatTableCellTitle(String sText) {
		String sResult = "&nbsp;";
		sResult += sText.replaceAll(" ", "&nbsp;");
		sResult += "&nbsp;<BR>";
		return sResult;
	}// End of function formatTableCellTitle

	
	/** Converts a Vector<String> to a comma-separated String. A typical use is as a parameter 
	 * (not binding) in SQL queries. In such cases, the strings contained in the Vector should be 
	 * quoted (single or double quotes, depending on database and driver) if they represent string 
	 * literals.
	 * @param itemsList Vector<String> 
	 * @return String (comma-separated, single spaced) representing the array of data contained in 
	 * the Vector<String>. Or, empty string, if there is an error reading the Vector<String>.
	 */
	public static String convertVectorToCsv(java.util.Vector<String> itemsList) {
		  String[] tmpArray = itemsList.toArray(new String[itemsList.size()]);	  
		  if (tmpArray != null) {
			  String csv = java.util.Arrays.toString(tmpArray);
			  return csv.substring(1, csv.length() - 1);
		  }
		  return " ";
	  }
	
	
	/**
	 * Escapes a string replacin each ' character with two ''. This is the appropiate method to escape string in MSSQL
	 * @param str string to escape
	 * @return escaped string for sql
	 */
	public static String escape(String str){
		if (str == null) {
			return null;
		}
		return str.replaceAll("'", "''");
	}
	
	public static String vector2CsvString(Vector<String> vToConvert)
	{
		String retValue = null;
		if(vToConvert != null)
		{
	        retValue = "(";
	        for(String currentValue : vToConvert)
	        {
	        	retValue += currentValue  + ",";
	        }
	        retValue = retValue.substring(0,(retValue.length() - 1));
	        retValue += ")";
		}
		return retValue;
	}

    public static String randomString(char[] characterSet, int length) {
        Random random = new SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }
    
    public static String hideXNumberDigits(String text){
        return hideXNumberDigits(text, 4);
    }
    
    /**
     * Hide the text and only show X number of last digits
     * @param text original text (Example: 8553119984)
     * @param digits number of digits to show (Example: 4)
     * @return ******9984
     */
    public static String hideXNumberDigits(String text, int digits){
        if(text == null || text.length() < digits){
            return text;
        }
        String newText = "";
        for (int i = text.length()-1; i >= text.length()-digits; i--) {
            newText = text.charAt(i)+ newText;
        }
        for (int i = 0; i < text.length()-digits; i++) {
            newText = "*"+newText;
        }
        return newText;
    }
	
    
    /**
     * This method converts from a string array to a comma separated string
     *
     * @param source String array containing the values
     * @return String of values separated by comma
     */
    public static String toCsvStringFromStringArray(String[] source) {
        if ((source != null) && (source.length > 0)) {
            StringBuilder sb1 = new StringBuilder();
            for (String val : source) {
                if(!val.trim().isEmpty())
                {
                    sb1.append(val).append(",");
                }
            }
            sb1.deleteCharAt(sb1.length() - 1);
            return sb1.toString();
        } else {
            return "";
        }
    }
    
    /**
     * This method will add SQL wildcards as many times wildcard count states 
     * Is intended for adding space for SQL IN clause (....)
     * @param wildCardCount How many "?" Will be added to the string 
     * @param sqlString sql string lacking parameters
     * @return  original sql string with added parameters
     */
    public static String addSqlParamWhildCards(int wildCardCount, String sqlString){
        if((wildCardCount > 0) && (sqlString != null) && (sqlString.trim().isEmpty())){
            StringBuilder workSql = new StringBuilder();
            workSql.append(sqlString);
            for(int i = 0;i < wildCardCount; i++){
                if(i < (wildCardCount - 1)){
                    workSql.append("?, ");
                }else{
                    workSql.append("?");
                }
            }
            return workSql.toString();
        }else{
            return null;
        }            
    }
    
    
    public static boolean isNotEmptyStr(String strValue){
        return ((strValue != null) && (!strValue.trim().isEmpty()));
    }

    public static String numberListToCsv(List<? extends Number> list){
    	if(list != null && !list.isEmpty()){
			Iterator<? extends Number> it = list.iterator();
			StringBuilder sb = new StringBuilder();
			while(it.hasNext()){
				sb.append(it.next());
				if(it.hasNext()) sb.append(",");
			}
			return sb.toString();
		}
		return null;
	}

    public static String stringListToCsv(List<? extends String> list){
        if(list != null && !list.isEmpty()){
            Iterator<? extends String> it = list.iterator();
            StringBuilder sb = new StringBuilder();
            while(it.hasNext()){
                sb.append(it.next());
                if(it.hasNext()) sb.append(",");
            }
            return sb.toString();
        }
        return null;
    }
    
    public static boolean checkStr(String field) {
        return ((field != null) && (!field.isEmpty()));
    }

}
