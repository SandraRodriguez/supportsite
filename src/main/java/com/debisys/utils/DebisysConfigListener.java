package com.debisys.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import net.emida.supportsite.util.SpringBeanFactory;
import net.emida.supportsite.util.security.AESCipher;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.debisys.customers.Rep;
import com.debisys.exceptions.UserException;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.util.ComboConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Listener that looks up config values when the Web application is first
 * loaded. Stores this name in the servlet context. Various servlets and JSP
 * pages will extract it from that location.
 */
public class DebisysConfigListener implements ServletContextListener {
	static Logger log = Logger.getLogger(DebisysConfigListener.class);
	// New attributes to manage properties from DB //
	private CompositeConfiguration cfg;
	private ComboConfiguration comboCfg;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	/**
	 * Looks up the init parameters and puts them into the servlet context.
	 */

	public void contextInitialized(ServletContextEvent event) {
		BasicConfigurator.configure();
		ServletContext context = event.getServletContext();

		WebApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);

		setInitialAttribute(context, "propertiesFile",
				"/WEB-INF/webapp.properties");

		// Load properties from DB to configure CONTEXT //
		try {
			// Changed the constructor called to use anly DB driven properties
			// aqnd backwards
			// compatibility
            String propertiesFile = context.getRealPath("/") + (String) context.getAttribute("propertiesFile");

            //
            Configuration configuration = setupNewConfiguration(propertiesFile, appCtx);

            comboCfg = new ComboConfiguration(configuration, "properties", "support", "global", "dummyString");
			cfg = comboCfg.getPropertiesConfiguration();


			// monitor the file for changes and reload
            //TODO FVN : This reloading strategy has no sense here because we always need restart the web context and decrypt manually encrypted text
/*			FileChangedReloadingStrategy rls = new FileChangedReloadingStrategy();
			cfg.setReloadingStrategy(rls);*/

			// SW
			// Little prob here with debisys-util.jar
			// db.properties.deployName wasn't getting put in the ComboConfig...
			// hashmap.
			context.setAttribute("instance", comboCfg.getDeployName());
			// context.setAttribute("instance",
			// cfg.getString("db.properties.deployName"));
			context.setAttribute("serverType", "0");
			context.setAttribute("deploymentType", cfg.getString("deploymentType", "0"));
			context.setAttribute("customConfigType", cfg.getString("customConfigType"));
			context.setAttribute("downloadPath", cfg.getString("downloadPath","/admin/download/"));
			context.setAttribute("0workingDir", cfg.getString("0workingDir","c:\\temp\\"));
			context.setAttribute("1workingDir", cfg.getString("1workingDir","/tmp/"));
			context.setAttribute("downloadUrl", cfg.getString("downloadUrl","/support/admin/download/"));
			context.setAttribute("filePrefix", cfg.getString("filePrefix","debisys_"));
			context.setAttribute("achRange", cfg.getString("achRange", "180"));
			context.setAttribute("physicalTerminals",cfg.getString("physicalTerminals",
					"3,9,10,11,12,30,31,32,42,46,47,58,59,60,63,64,43,44,34,35,48,49,57,61,62,67,55"));
			context.setAttribute("mailHost", cfg.getString("mailHost","mail.debisys.com"));
			context.setAttribute("mxValueAddedTax", cfg.getString("mxValueAddedTax", "16"));
			context.setAttribute("MarketPlaceDeploymentName", cfg.getString("MarketPlaceDeploymentName", "DEBISYS_USA"));
			
			// parature tkt 9949440 forcing some rates rounding
			context.setAttribute("ratesDecimalZeroes", cfg.getInteger("ratesDecimalZeroes", 5));
			//For DBSY-719, set the webservice URL
			context.setAttribute("webserviceURL", cfg.getString("webserviceURL","http://192.168.2.79"));
			context.setAttribute("webservicePort", cfg.getString("webservicePort","8080"));
			context.setAttribute("webservicePostPage", cfg.getString("webservicePostPage","/webservice/webservice.asp"));
			context.setAttribute("debugJS", cfg.getString("debugJS","false"));
			context.setAttribute("phoneFormat", cfg.getString("phoneFormat","%s-%s-%s"));
			context.setAttribute("phoneRegExp", cfg.getString("phoneRegExp","[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"));
			context.setAttribute("phoneFormatText", cfg.getString("phoneFormatText","999-999-9999"));
			context.setAttribute("ProcessorDefaultValueId", cfg.getString("ProcessorDefaultValueId","4"));
			context.setAttribute("validateMerchantBalanceMx", cfg.getString("validateMerchantBalanceMx","0"));
			
			//History Quantity RegistrationCodes 
			context.setAttribute("quantityRegistrationCodes", cfg.getString("quantityRegistrationCodes","5"));
			context.setAttribute("daysBeforeRegistrationCodeExpires", cfg.getString("daysBeforeRegistrationCodeExpires","120"));
			
			context.setAttribute("dataBaseDefaultForReport", cfg.getString("dataBaseDefaultForReport","SLAVE"));
			context.setAttribute("reportIntervalLimitDays", cfg.getString("reportIntervalLimitDays","90"));
			
			//DBSY-908
			context.setAttribute("downloadbar", cfg.getString("downloadbar",""));
			context.setAttribute("maxdownload", cfg.getString("maxdownload",""));
			
			context.setAttribute("mandatoryMerchantClassification", cfg.getString("mandatoryMerchantClassification","1"));

			context.setAttribute("SIMInventoryLoadMaxSims", cfg.getString("SIMInventoryLoadMaxSims","100"));
                        
                        context.setAttribute("providerType", cfg.getString("providerType"));
                        context.setAttribute("ftpHost", cfg.getString("ftpHost"));
                        context.setAttribute("ftpPort", cfg.getString("ftpPort"));
                        context.setAttribute("ftpUser", cfg.getString("ftpUser"));
                        context.setAttribute("ftpPassword", cfg.getString("ftpPassword"));
                        context.setAttribute("ftpTargetFolder", cfg.getString("ftpTargetFolder"));

			context.setAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM, cfg.getString(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM,""));
                        
			//DBSY-931 System 2000 - Attribute to control status refresh 
			// value controls Ajax-Asynchronus calls. 
			// value represents Time Interval in milliseconds 
			// The Timeout function will be called every (value) millisecond
			//default value is 5000 milliseconds = 5 seconds
			String temp = "5000";
			if(cfg.getString("System2000TaskListRefresh")!=null && cfg.getString("System2000TaskListRefresh","2000")!=""){
			try{
				temp = cfg.getString("System2000TaskListRefresh","5000");
				//minimum 3 seconds to avoid overhead in refreshing... 
				if(Integer.parseInt(temp) < 3000)
					temp="3000";
			}
			catch(NumberFormatException e){
				temp="5000";
			}
			}
			else
				temp="5000";
			context.setAttribute("System2000TaskListRefresh", temp);
			//END DBSY-931 System 2000
			
			//check for all available static Country ISO
			for (int i = 1;; i++) {
				if (cfg.getString("staticIDCountryISO" + i, "").equals(null)
						|| cfg.getString("staticIDCountryISO" + i, "").equals(
								""))
					break;
				context.setAttribute("staticIDCountryISO" + i, cfg.getString(
						"staticIDCountryISO" + i, ""));
				context.setAttribute("staticIDConstantValue" + i, cfg
						.getString("staticIDConstantValue" + i, ""));
			}

			
			
				if (getCustomConfigType(context).equals("1")) {
				// Make sure RPM (MEXICO) uses Spanish
				context.setAttribute("language", "spanish");
			} else {
				String ln = cfg.getString("language");
				if(ln.equalsIgnoreCase("English") )
					setInitialAttribute(context, "language", "english");
				else if(ln.equalsIgnoreCase("Spanish") ) 
					setInitialAttribute(context, "language", "spanish");
				else //english by default
					setInitialAttribute(context, "language", "english");
					
			}

			//BEGIN RELEASE 31 - ADD BY NM
			context.setAttribute("intranet-site", cfg.getString("intranet-site",""));
			context.setAttribute("intranet-port-site", cfg.getString("intranet-port-site",""));
			context.setAttribute("blackListPermissions", cfg.getString("blackListPermissions",""));
			context.setAttribute("iso_default", cfg.getString("iso_default",""));
			context.setAttribute("prefix_user_intranet", cfg.getString("prefix_user_intranet",""));
                        context.setAttribute("urlBasicIsoToken", cfg.getString("urlBasicIsoToken",""));
                        context.setAttribute("secretSeed", cfg.getString("secretSeed",""));
			//END RELEASE 31
			
			//R34 - SUPPORT EM
			context.setAttribute("documentRepositoryPath", cfg.getString("documentRepositoryPath",""));
			//END R34 - SUPPORT EM			
			
			// put PROPERTIES into the context
			log.info("***********************************************");
			context.setAttribute("properties", cfg);
			log.info("WebApp props loaded from " + propertiesFile);
			log.info("MarketPlaceDeploy: "
					+ cfg.getString("MarketPlaceDeploymentName"));
			log.info("Application: " + cfg.getString("webapp.name"));
			log.info("Description: " + cfg.getString("webapp.description"));
			log.info("    Version: " + cfg.getString("webapp.version"));
			log.info("    downloadbar: " + cfg.getString("downloadbar"));
			log.info("    maxdownloadbar: " + cfg.getString("maxdownloadbar"));
			log.info("Context is initialized!");
			log.info("***********************************************");
		} catch (ConfigurationException e) {
			log
					.fatal(
							"Couldn't initialize configuration... Please Review Properties File / Data Base Properties!",
							e);
		}
		Iterator<String> jasperProperties = cfg.getKeys("jasper");
		if ( jasperProperties.hasNext() )
		{
			log.info("This deploy has JASPER properties setup, now we schedule delete jrxml files job every 5 hours!");
			//String objKey = (String)jasperProperties.next();			
			//System.out.println("param name ="+objKey);
			TaskUtilsSupport taskSupport = new TaskUtilsSupport();
			taskSupport.path = getWorkingDir(context);
			//scheduler.scheduleAtFixedRate( taskSupport, 0, 5, TimeUnit.HOURS);
		}
		
	}

    private Configuration setupNewConfiguration(String propertiesFile, WebApplicationContext webAppCtx) {
	    try {
            PropertiesConfiguration pc = new PropertiesConfiguration(propertiesFile);
            Map<String, String> map = new HashMap<String, String>();
            Iterator keys = pc.getKeys();
            while(keys.hasNext()){
                String key = (String) keys.next();
                String value = pc.getString(key);
                if(value.contains("ENC(")){
                    String encryptedPass = value.substring(4, value.length() - 1);
                    AESCipher cipher = webAppCtx.getBean(AESCipher.class);
                    String decrypted = cipher.decrypt(encryptedPass);
                    map.put(key, decrypted);
                }
            }

            for (Map.Entry<String, String> entry : map.entrySet()) {
                pc.setProperty(entry.getKey(), entry.getValue());
            }

            return pc;
        }catch (Exception ex){
	        log.error("Error loading property file : "+ propertiesFile);
        }
        return null;
    }

    public void contextDestroyed(ServletContextEvent event) {
		// needed to manage funky windoze operation
		// LogManager.getLoggerRepository().shutdown();
	}

	// Looks for a servlet context init parameter with a given name.
	// If it finds it, it puts the value into a servlet context
	// attribute with the same name. If the init parameter is missing,
	// it puts a default value into the servlet context attribute.

	private void setInitialAttribute(ServletContext context,
			String initParamName, String defaultValue) {
		String initialValue = context.getInitParameter(initParamName);
		if (initialValue != null) {
			context.setAttribute(initParamName, initialValue);
		} else {
			context.setAttribute(initParamName, defaultValue);
		}
	}
	
	public static void setLanguageInitialAttribute(ServletContext context,String initParamName,String initialValue){
		context.setAttribute(initParamName, initialValue);
	}

	public static String getWebserviceURL(ServletContext context) {
		String strTemp = (String) context.getAttribute("webserviceURL");
		return (strTemp);
	}	
	public static String getWebservicePort(ServletContext context) {
		String strTemp = (String) context.getAttribute("webservicePort");
		return (strTemp);
	}		
	public static String getWebservicePostPage(ServletContext context) {
		String strTemp = (String) context.getAttribute("webservicePostPage");
		return (strTemp);
	}	
	
	public static String getServerType(ServletContext context) {
		String strTemp = (String) context.getAttribute("serverType");
		return (strTemp);
	}

	public static String getDeploymentType(ServletContext context) {
		String strTemp = (String) context.getAttribute("deploymentType");
		return (strTemp);
	}

	public static String getDownloadPath(ServletContext context) {
		// String strPrefix =(String) context.getAttribute("serverType");
		// String strTemp = (String) context.getAttribute(strPrefix +
		// "downloadPath");
		String strTemp = (String) context.getAttribute("downloadPath");
		strTemp = context.getRealPath("/") + strTemp;
		return (strTemp);
	}

	public static String getWorkingDir(ServletContext context) {
		String strPrefix = (String) context.getAttribute("serverType");
		String strTemp = (String) context
				.getAttribute(strPrefix + "workingDir");
		return (strTemp);
	}

	public static String getDownloadUrl(ServletContext context) {
		String strTemp = (String) context.getAttribute("downloadUrl");
		return (strTemp);
	}
	
	public static String getFilePrefix(ServletContext context) {
		String strTemp = (String) context.getAttribute("filePrefix");
		return (strTemp);
	}

	public static String getLanguage(ServletContext context) {
		String strTemp = (String) context.getAttribute("language");
		return (strTemp);
	}

	public static String getMailHost(ServletContext context) {
		String strTemp = (String) context.getAttribute("mailHost");
		return (strTemp);
	}

	public static String getCustomConfigType(ServletContext context) {
		return context.getAttribute("customConfigType").toString();
	}

	public static double getMxValueAddedTax(ServletContext context) {
		return Double.parseDouble(context.getAttribute("mxValueAddedTax")
				.toString());
	}

	// BEGIN Release 13.0 - Added by YH
	public static int getACHRange(ServletContext context) {
		String strTemp = (String) context.getAttribute("achRange");
		return Integer.parseInt(strTemp);
	}

	// END Release 13.0 - Added by YH

	// BEGIN Release 21 - Added by LF
	public static String getPhysicalTerminals(ServletContext context) {
		String strTemp = (String) context.getAttribute("physicalTerminals");
		strTemp = "," + strTemp.replaceAll(" ", "") + ",";
		return strTemp;
	}

	// END Release 21 - Added by LF

	// MarketPlace Deployment Name
	public static String getMarketPlaceDeployName(ServletContext context) {
		String strTemp = context.getAttribute("MarketPlaceDeploymentName")
				.toString();
		return strTemp;
	}

	public static String getInstance(ServletContext context) {
		String strTemp = (String) context.getAttribute("instance");
		return strTemp;
	}
	

	//DBSY-908
	public static String getdownloadbar(ServletContext context) {
		String strTemp = (String) context.getAttribute("downloadbar");
		return strTemp;
	}

	//DBSY-908
	public static String getmaxdownload(ServletContext context) {
		String strTemp = (String) context.getAttribute("maxdownload");
		return strTemp;
	}

	public static Integer getRatesDecimalZeroes(ServletContext context) {
		int iTemp = (Integer) context.getAttribute("ratesDecimalZeroes");
		return iTemp;
	}
	
	public static boolean getDebugJS(ServletContext context) {
		String s = (String) context.getAttribute("debugJS");
		return new Boolean(s);
	}
	
	public static String getPhoneFormat(ServletContext context)
	{
		String retValue = (String) context.getAttribute("phoneFormat");
		return retValue;
}

	public static String getPhoneRegExp(ServletContext context)
	{
		String retValue = (String) context.getAttribute("phoneRegExp");
		return retValue;
	}

	public static String getPhoneFormatText(ServletContext context)
	{
		String retValue = (String) context.getAttribute("phoneFormatText");
		return retValue;
	}
	
	public static String getProcessDefaultValueId(ServletContext context)
	{
		String retValue = (String) context.getAttribute("ProcessorDefaultValueId");
		return retValue;
	}
	
	public static String getValidateMerchantBalanceMx(ServletContext context)
	{
		String retValue = (String) context.getAttribute("validateMerchantBalanceMx");
		return retValue;
	}
	
	public static String getQuantityRegistrationCodes(ServletContext context)
	{
		String retValue = (String) context.getAttribute("quantityRegistrationCodes");
		return retValue;
	}
	
	public static String getDaysBeforeRegistrationCodeExpires(ServletContext context)
	{
		String retValue = (String) context.getAttribute("daysBeforeRegistrationCodeExpires");
		return retValue;
	}
	
	public static String getDataBaseDefaultForReport(ServletContext context)
	{
		String retValue = (String) context.getAttribute("dataBaseDefaultForReport");
		return retValue;
	}
	public static String getReportIntervalLimitDays(ServletContext context)
	{
		String retValue = (String) context.getAttribute("reportIntervalLimitDays");
		return retValue;
	}
	
	//R34 - SUPPORT EM
	public static String getDocumentRepositoryPath(ServletContext context)
	{
		String retValue = (String) context.getAttribute("documentRepositoryPath");
		return retValue;
	}
	public static String getMandatoryMerchantClassification(ServletContext context)
	{
		String retValue = (String) context.getAttribute("mandatoryMerchantClassification");
		return retValue;
	}
	//END R34 - SUPPORT EM

        public static int getSIMInventoryLoadMaxSims(ServletContext context){
		String retValue = (String) context.getAttribute("SIMInventoryLoadMaxSims");
		return Integer.parseInt(retValue);
	}
        
        public static String getProviderType(ServletContext context) {
		String strTemp = (String) context.getAttribute("providerType");
		return (strTemp);
	}
        
        public static String getFtpHost(ServletContext context) {
		String strTemp = (String) context.getAttribute("ftpHost");
		return (strTemp);
	}
        
        public static String getFtpPort(ServletContext context) {
		String strTemp = (String) context.getAttribute("ftpPort");
		return (strTemp);
	}
        
        public static String getFtpUser(ServletContext context) {
		String strTemp = (String) context.getAttribute("ftpUser");
		return (strTemp);
	}
        
        public static String getFtpPassword(ServletContext context) {
		String strTemp = (String) context.getAttribute("ftpPassword");
		return (strTemp);
	}
        
        public static String getFtpTargetFolder(ServletContext context) {
		String strTemp = (String) context.getAttribute("ftpTargetFolder");
		return (strTemp);
	}
        
        public static String getUrlBasicIsoToken(ServletContext context) {
		String strTemp = (String) context.getAttribute("urlBasicIsoToken");
		return (strTemp);
	}

	private static class TaskUtilsSupport implements Runnable{
		static Logger log = Logger.getLogger(TaskUtilsSupport.class);
		public String path = "";
		
		public TaskUtilsSupport(){}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("Delete template files from "+path);
			File f = new File( path ); 

			FilenameFilter textFilter = new FilenameFilter() 
			{
				public boolean accept(File dir, String name) {
					String lowercaseName = name.toLowerCase();
					if (lowercaseName.endsWith(".jrxml")) {
						return true;
					} else {
						return false;
					}
				}
			};

			File[] files = f.listFiles(textFilter);
			if ( files.length > 0)
			{
				for (File file : files) 
				{
					if ( !file.isDirectory()) 
					{
						try 
						{
							file.delete();
						}
						catch (Exception e) 
						{
							log.info("Exception ",e);
						}
					}					
				}
			}
			else
			{
				log.info("No jrxml template JASPER files found ");
			}
		}
		
	}
	
}
