//R22 branch
package com.debisys.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.debisys.dateformat.DateFormat;


public class DateUtil {
	
    private static final Logger logger = Logger.getLogger(DateUtil.class);
    
    public static final String ODBC_CANONICAL_MS_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    public static String getDateFormatString() {
        return DateFormat.getDateFormat();
    }

    public static Locale getLocale() {
        Locale thisLocale = Locale.US;
        String lang = System.getProperty("user.language"), country = System.getProperty("user.country");
        if (lang != null && lang != "" && country != null && country != "") {
            thisLocale = new Locale(lang, country);
        }
        return thisLocale;
    }

    public static String formatDate(Date dtDate) {
        String dateString = "";
        if (dtDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            dateString = dateFormat.format(dtDate);
        }

        return dateString;
    }

    /**
     * Supports localization.<p>
     * @param dateString A date string with the format yyyy-MM-dd HH:mm:ss.S,
     * e.g: 2009-03-20 00:00:00.0
     * @return a date string with the format as parametrized in DB. Default
     * MM/dd/yyyy, e.g: 03/20/2009 To query the current value for the dateFormat
     * in DB properties exec this:
     *
     * <pre>
     * select value from properties where project = 'support' and module = 'global' and property = 'dateFormat'
     * </pre>
     */
    public static String formatDateTime_DB(String dateString) {
        if (dateString == null || dateString == "") {
            return dateString;
        }
        String inputFormat = "yyyy-MM-dd HH:mm:ss.SSS";
        return formatDateTime(dateString, inputFormat);
    }

    public static String formatDateNoTime_DB(String dateString) {
        if (dateString == null || dateString == "") {
            return dateString;
        }
        String inputFormat = "yyyy-MM-dd HH:mm:ss.SSS";
        return formatDateNoTime(dateString, inputFormat);
    }

    /**
     * *
     * Will return a date string to query against the DB. Can specify a time
     * string in case one does not exist. Expects a date of MM/dd/yyyy for the
     * date search range fields.
     *
     * Returns a String of the format yyyy-MM-dd HH:mm:ss.SSS
     *
     * @param dateString
     * @param timeToConcat
     * @return
     */
    public static String formatDateConcatTime_DB(String dateString, String timeToConcat) {
        boolean noDate = dateString == null || dateString == "", noTime = timeToConcat == null || timeToConcat == "";
        if (noDate || noTime) {
            return dateString;
        }
        String inputFormat = "MM/dd/yyyy HH:mm:ss.SSS";
        String fmtDate = "";
        try {
            SimpleDateFormat idfmt = new SimpleDateFormat(inputFormat);
            java.util.Date idate = new java.util.Date();
            idate = idfmt.parse(dateString + " " + timeToConcat);
            SimpleDateFormat odfmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            fmtDate = odfmt.format(idate);
        } catch (Exception e) {

        }
        return fmtDate;
    }

    /**
     * @param date input date string
     * @param inputFormat the format of the input date. It is used to parse the
     * dateString and generate a java.util.Date object.
     * @return result of a delegate call on {
     * @ DateUtil#formatDateTime(Date)}
     */
    public static String formatDateTime(String date, String inputFormat) {
        boolean noDate = date == null || date == "", noFormat = inputFormat == null || inputFormat == "";
        if (noDate || noFormat) {
            return date;
        }
        String fmtDate = "";
        try {
            SimpleDateFormat idfmt = new SimpleDateFormat(inputFormat);
            java.util.Date idate = new java.util.Date();
            idate = idfmt.parse(date);
            fmtDate = formatDateTime(idate);
        } catch (Exception e) {
        }
        return fmtDate;
    }

    /**
     * *
     * This method is deprecated in favor to {
     *
     * @ DateUtil#formatDateTime(Date)}
     * .<p>
     * ADDED FOR LOCALIZATION - SW Will take either a date of the format:
     * MM/dd/yyyy HH:mm a MM/dd/yyyy yyyy-mm-dd and modify it according to
     * whatever format is in the properties table of the database.
     * @param date
     * @return
     */
    @Deprecated
    public static String formatDateTime(String date) {
        String dateString = "";
        try {
            if (date != null && !date.equals("")) {
                Pattern mmddyyyyhhmma = Pattern.compile("\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}\\s[APap][mM]");
                Pattern mmddyyyy = Pattern.compile("\\d{1,2}/\\d{1,2}/\\d{4}");
                Pattern yyyymmdd = Pattern.compile("\\d{4}-\\d{1,2}-\\d{1,2}");
                Pattern yyyymmddhhmmss = Pattern.compile("\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}.\\d{1,3}");

                Matcher mmddyyyy_Matcher = mmddyyyy.matcher(date);
                Matcher mmddyyyyhhmma_Matcher = mmddyyyyhhmma.matcher(date);
                Matcher yyyymmdd_Matcher = yyyymmdd.matcher(date);
                Matcher yyyymmddhhmmss_Matcher = yyyymmddhhmmss.matcher(date);
                SimpleDateFormat sdf = null;
                SimpleDateFormat sdf1 = null;

                // If 'date' has a time in it
                if (mmddyyyyhhmma_Matcher.matches()) {
                    // Current support site date is in MM/dd/YYYY HH:mm a.
                    sdf = new SimpleDateFormat("MM/dd/yyyy h:mm a");
                    java.util.Date dd = sdf.parse(date);

                    // We will specify a new date format through the database
                    sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
                    dateString = sdf1.format(dd);
                } else if (mmddyyyy_Matcher.matches()) {
                    // Current support site date is in MM/dd/YYYY.
                    sdf = new SimpleDateFormat("MM/dd/yyyy");
                    java.util.Date dd = sdf.parse(date);

                    // We will specify a new date format through the database
                    sdf1 = new SimpleDateFormat(DateFormat.getDateNoTimeFormat());
                    dateString = sdf1.format(dd);
                } else if (yyyymmdd_Matcher.matches()) {
                    // Current support site date is in yyyy-MM-dd
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date dd = sdf.parse(date);

                    // We will specify a new date format through the database
                    sdf1 = new SimpleDateFormat(DateFormat.getDateNoTimeFormat());
                    dateString = sdf1.format(dd);
                } else if (yyyymmddhhmmss_Matcher.matches()) {
                    // Current support site date is in yyyy-MM-dd
                    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    java.util.Date dd = sdf.parse(date);

                    // We will specify a new date format through the database
                    sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
                    dateString = sdf1.format(dd);
                }
            }
        } catch (ParseException ex) {

        }

        return dateString;
    }

    public static String formatDateTime(Date dtDate) {
        String dateString = "";
        if (dtDate != null && !dtDate.equals("")) {
            // For now, format the time based on this dateformat
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormat.getDateFormat(), //Locale.US);
                    getLocale());
            //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm a", Locale.US);
            dateString = dateFormat.format(dtDate);
        }

        return dateString;
    }

    public static String formatDateTimePromo(Date dtDate) {
        String dateString = "";
        if (dtDate != null && !dtDate.equals("")) {
            // For now, format the time based on this dateformat
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm a", Locale.US);
            dateString = dateFormat.format(dtDate);
        }

        return dateString;
    }

    public static String formatDateNoTime(Date date) {
        String dateString = "";
        if (date != null && !date.equals("")) {
            // For now, format the time based on this dateformat
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormat.getDateNoTimeFormat(), //Locale.US);
                    getLocale());
            //SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm a", Locale.US);
            dateString = dateFormat.format(date);
        }

        return dateString;
    }

    /**
     * @param date input date string
     * @param inputFormat the format of the input date. It is used to parse the
     * dateString and generate a java.util.Date object.
     * @return result of a delegate call on {
     * @ DateUtil#formatDateNoTime(Date)}
     */
    public static String formatDateNoTime(String date, String inputFormat) {
        boolean noDate = date == null || date == "", noFormat = inputFormat == null || inputFormat == "";
        if (noDate || noFormat) {
            return date;
        }
        String fmtDate = "";
        try {
            SimpleDateFormat idfmt = new SimpleDateFormat(inputFormat);
            java.util.Date idate = new java.util.Date();
            idate = idfmt.parse(date);
            fmtDate = formatDateNoTime(idate);
        } catch (Exception e) {
        }
        return fmtDate;
    }

    @Deprecated
    public static String formatDateNoTime(String date) {
        Pattern mmddyyyy = Pattern.compile("\\d{1,2}/\\d{1,2}/\\d{4}");
        Matcher mmddyyyy_Matcher = mmddyyyy.matcher(date);
        String dateString = date;
        try {
            if (mmddyyyy_Matcher.matches()) {
                // Current support site date is in MM/dd/YYYY.
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                java.util.Date dd = sdf.parse(date);

                // We will specify a new date format through the database
                SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateNoTimeFormat());
                dateString = sdf1.format(dd);
            }
        } catch (Exception e) {

        }
        return dateString;
    }

    /**
     * @param dateString date string assuming a date format matching EXACTLY
     * [MM/dd/yyyy] OR [yyyy-MM-dd HH:mm:ss.SSS].
     * @return an object ref to an instance of java.sql.Timestamp
     */
    public static java.sql.Timestamp getTimestamp(String dateString) {
        java.sql.Timestamp ts = new java.sql.Timestamp(System.currentTimeMillis()); //XXX better default than 0 anyways 
        if (dateString == null || dateString == "") {
            return null;
        }
        try {
            java.text.DateFormat dfmt = new java.text.SimpleDateFormat("MM/dd/yyyy");
            java.util.Date utilDate = new java.util.Date();
            utilDate = dfmt.parse(dateString);
            ts = new java.sql.Timestamp(utilDate.getTime());
        } catch (Exception e) {
            try {
                java.text.DateFormat dfmt2 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                java.util.Date utilDate2 = new java.util.Date();
                utilDate2 = dfmt2.parse(dateString);
                ts = new java.sql.Timestamp(utilDate2.getTime());
            } catch (Exception x) {
            }
        }
        return ts;
    }

    public static String formatSqlDateTime(Date dtDate) {
        String dateString = "";
        if (dtDate != null && !dtDate.equals("")) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            dateString = dateFormat.format(dtDate);
        }

        return dateString;
    }

    public static String formatSqlDateTimeMillis(Date dtDate) {
        String dateString = "";
        if (dtDate != null && !dtDate.equals("")) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
            dateString = dateFormat.format(dtDate);
        }

        return dateString;
    }

    public static boolean isValid_MMddyyyy(String strDate) {
        return isValid(strDate, "MM/dd/yyyy");
    }

    public static boolean isValid_yyyyMMddHHmmssSSS(String strDate) {
        return isValid(strDate, "yyyy-MM-dd HH:mm:ss.SSS");
    }

    public static boolean isValid(String strDate, String strFmt) {
        boolean valid = false;
        java.text.SimpleDateFormat fmt = null;
        try {
            fmt = new java.text.SimpleDateFormat(strFmt);
            fmt.setLenient(false);
            java.util.Date d = fmt.parse(strDate);
            valid = d != null;
        } catch (Exception e) {
        }
        return valid;
    }

    public static boolean isValid(String strDate) {
        boolean isDate = false;
        try {
            if (strDate != null && strDate.length() >= 8 && strDate.length() <= 10) {
                //added by jay on 4/12/2006
                //make sure date is properly entered
                // or it will cause problems with sql
                String strMDY[] = strDate.split("/");
                if (strMDY.length != 3) {
                    isDate = false;
                } else {
                    String strMonth = strMDY[0];
                    String strDay = strMDY[1];
                    String strYear = strMDY[2];

                    if (strMonth.length() > 2 || strMonth.length() < 1) {
                        isDate = false;
                    } else if (strDay.length() > 2 || strDay.length() < 1) {
                        isDate = false;
                    } else if (strYear.length() != 4) {
                        isDate = false;
                    } else {
                        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                        formatter.setLenient(false);
                        /*Date date = (java.util.Date) */
                        formatter.parse(strDate);
                        isDate = true;
                    }
                }

            } else {
                isDate = false;
            }

        } catch (ParseException e) {
            isDate = false;
        }
        return isDate;
    }

    public static boolean isValidYYYYMMDD(String strDate) {
        boolean isDate = false;
        try {
            if (strDate != null && strDate.length() >= 8 && strDate.length() <= 10) {
                //added by gelm  on 06/20/2008
                //make sure date is properly entered
                // or it will cause problems with sql
                String strMDY[] = strDate.split("-");
                if (strMDY.length != 3) {
                    isDate = false;
                } else {
                    String strMonth = strMDY[1];
                    String strDay = strMDY[2];
                    String strYear = strMDY[0];

                    if (strMonth.length() > 2 || strMonth.length() < 1) {
                        isDate = false;
                    } else if (strDay.length() > 2 || strDay.length() < 1) {
                        isDate = false;
                    } else if (strYear.length() != 4) {
                        isDate = false;
                    } else {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        formatter.setLenient(false);
                        /*Date date = (java.util.Date) */
                        formatter.parse(strDate);
                        isDate = true;
                    }
                }

            } else {
                isDate = false;
            }

        } catch (ParseException e) {
            isDate = false;
        }
        return isDate;
    }

    public static boolean isValidDateTime(String strDateTime) {
        boolean isDate = false;
        try {
            if (strDateTime != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy h:mm a");
                formatter.setLenient(false);
                /*Date date = (java.util.Date) */
                formatter.parse(strDateTime);
                isDate = true;
            } else {
                isDate = false;
            }

        } catch (ParseException e) {
            isDate = false;
        }
        return isDate;
    }

    public static Date addSubtractDays(Date date, int numDays) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, numDays);

        return c.getTime();
    }

    public static String addSubtractMonths(String strDate, int numOfMonths) {
        String returnDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            formatter.setLenient(false);
            Date dtEndDate = /*(Date) */ formatter.parse(strDate);
            //Get the calendar
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(dtEndDate);
            cal.add(Calendar.MONTH, numOfMonths);
            dtEndDate = cal.getTime();
            returnDate = formatter.format(dtEndDate);
        } catch (ParseException e) {
            returnDate = strDate;
        }

        return returnDate;
    }

    public static String addSubtractDays(String strDate, int numOfDays) {
        String returnDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            formatter.setLenient(false);
            Date dtEndDate = /*(Date) */ formatter.parse(strDate);
            //Get the calendar
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(dtEndDate);
            cal.add(Calendar.DATE, numOfDays);
            dtEndDate = cal.getTime();
            returnDate = formatter.format(dtEndDate);
        } catch (ParseException e) {
            returnDate = strDate;
        }

        return returnDate;
    }
    
    /**
     * Calculates the number of days between two calendar days in a manner which
     * is independent of the Calendar type used.
     *
     * @param strDate1 The first date.
     * @param strDate2 The second date.
     *
     * @return The number of days between the two dates. Zero is returned if the
     * dates are the same, one if the dates are adjacent, etc. The order of the
     * dates does not matter, the value returned is always >= 0. If Calendar
     * types of d1 and d2 are different, the result may not be accurate.
     */
    public static int getDaysBetween(String strDate1, String strDate2) {
        int days = 0;
        try {
            Calendar d1 = Calendar.getInstance();
            Calendar d2 = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            formatter.setLenient(false);
            d1.setTime(formatter.parse(strDate1));
            d2.setTime(formatter.parse(strDate2));

            if (d1.after(d2)) {  // swap dates so that d1 is start and d2 is end
                Calendar swap = d1;
                d1 = d2;
                d2 = swap;
            }
            days = d2.get(Calendar.DAY_OF_YEAR)
                    - d1.get(Calendar.DAY_OF_YEAR);
            int y2 = d2.get(java.util.Calendar.YEAR);
            if (d1.get(Calendar.YEAR) != y2) {
                d1 = (Calendar) d1.clone();
                do {
                    days += d1.getActualMaximum(Calendar.DAY_OF_YEAR);
                    d1.add(Calendar.YEAR, 1);
                } while (d1.get(Calendar.YEAR) != y2);
            }

        } catch (ParseException e) {
            days = 0;
        }
        return days;
    }

    public static String dateAddSql(String datepart, int range, Date currentDate0) {

        String returnDate = "";
        Date currentDate = new Date(currentDate0.getTime());
        // 2010-02-12 08:11:13.647
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formatter.setLenient(false);

        // Get the calendar
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(currentDate);
        if (datepart.equalsIgnoreCase("SS")) { // SECONDS
            cal.add(Calendar.SECOND, range);
        } else if (datepart.equalsIgnoreCase("MI")) { // MINUTOS
            cal.add(Calendar.MINUTE, range);
        } else if (datepart.equalsIgnoreCase("HH")) { // HOUR
            cal.add(Calendar.HOUR_OF_DAY, range);
        } else if (datepart.equalsIgnoreCase("WK")) { // WEEK
            cal.add(Calendar.WEEK_OF_YEAR, range);
        }

        currentDate = cal.getTime();
        returnDate = formatter.format(currentDate);

        return returnDate;

    }

    public static Date dateAddSqlDate(String datepart, int range, Date currentDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return formatter.parse(dateAddSql(datepart, range, currentDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Method used to read a date string and return the corresponding Calendar
     * representation
     * @param strDate       String date to be parsed
     * @param dateFormat    String format strDate comes in. If not provided
     *                      the default ODBC canonical format will be used
     * @return  Calendar object with the string date value or null if a parsing
     *          problems happens
     */
    public static Calendar fromStringToCalendar(String strDate, String dateFormat){
        Calendar retValue = null;
        try {
            if((dateFormat == null) || (dateFormat.trim().isEmpty())){
                dateFormat = ODBC_CANONICAL_MS_FORMAT;
            }
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            retValue = Calendar.getInstance();
            retValue.setTime(sdf.parse(strDate));
        } catch (ParseException ex) {
            
        }
        return retValue;
    }
    
    /**
     * Formatting method for Calndar type objects
     * @param calDate       Calendar date to be formatted
     * @param outputFormat  String format to be used for date output string
     * @return              String representation of the calendar date
     */
    public static String formatCalendarDate(Calendar calDate, String outputFormat){        
        if((outputFormat == null) || (outputFormat.trim().isEmpty())){
            outputFormat = ODBC_CANONICAL_MS_FORMAT;
        }        
        SimpleDateFormat sdf = new SimpleDateFormat(outputFormat);
        return sdf.format(calDate.getTime());        
    }

    public static Date atStartOfDay(Date date){
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(Calendar.HOUR, 0);
        instance.set(Calendar.MINUTE, 0);
        instance.set(Calendar.SECOND, 0);
        instance.set(Calendar.MILLISECOND, 0);
        return instance.getTime();
    }

    public static Date atEndOfDay(Date date){
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(Calendar.HOUR, 23);
        instance.set(Calendar.MINUTE, 59);
        instance.set(Calendar.SECOND, 59);
        instance.set(Calendar.MILLISECOND, 0);
        return instance.getTime();
    }

    public static Date parse(String date, String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try{
            return sdf.parse(date);
        }catch (Exception ex){
        	logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static Date plusMinutes(Date date, int minutes){
        Calendar cal = toCalendar(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }

    public static Date plusHours(Date date, int hours){
        Calendar cal = toCalendar(date);
        cal.add(Calendar.HOUR, hours);
        return cal.getTime();
    }

    public static Date plusDays(Date date, int days){
        Calendar cal = toCalendar(date);
        cal.add(Calendar.DAY_OF_MONTH, days);
        return cal.getTime();
    }

    public static Date plusMonths(Date date, int months){
        Calendar cal = toCalendar(date);
        cal.add(Calendar.MONTH, months);
        return cal.getTime();
    }

    public static Date parseStringToDate(String currentDt, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            return simpleDateFormat.parse(currentDt);
        } catch (ParseException ex) {
            return null;
        }
    }

}
