package com.debisys.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.ResourceBundle;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 * 
 * Created for DBSY-1072 eAccounts Interface
 * 
 * @author swright
 *
 */
public class EntityAccountTypes {
	static Category cat = Category.getInstance(EntityAccountTypes.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
    
    private static Hashtable<Integer, String> ht_accountTypes = null;
    
    /***
     * 
     * @param _type
     * @return
     */
    public static String GetEntityAccountTypeDesc(int _type) {
    	if(ht_accountTypes == null)
    		ht_accountTypes = getAccountTypes_Internal();
    	
    	if(ht_accountTypes.containsKey(_type))
    		return ht_accountTypes.get(_type);
    	else
    		return "Unassigned";
    }
    
    /***
     * 
     * @return
     */
    public static Hashtable<Integer, String> getAccountTypes()
    {
    	if(ht_accountTypes == null)
    		ht_accountTypes = getAccountTypes_Internal();
    	
    	return ht_accountTypes;
    }
    
    /***
     * 
     * @return
     */
    private static Hashtable<Integer, String> getAccountTypes_Internal() {
    	Hashtable<Integer, String> accountTypes = new Hashtable<Integer, String>();
    	Connection dbConn = null;
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
    		if (dbConn == null)
    		{	
    			cat.error("Can't get database connection");
    			throw new Exception();
    		}
    		
    		PreparedStatement pstmt = dbConn.prepareStatement(sql_bundle.getString("getEntityAccountTypes"));
    		ResultSet rs = pstmt.executeQuery();
    		while(rs.next())
    		{
    			accountTypes.put(rs.getInt("type_id"), rs.getString("description"));
    		}

    		rs.close();
    		pstmt.close();
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during getAccountTypes", e);
    	}
    	finally
    	{
    		try
    		{
    			Torque.closeConnection(dbConn);
    		}
    		catch (Exception e)
    		{
    			cat.error("Error during release connection", e);
    		}
    	}
    	return accountTypes;
    }
    
    /***
     * 
     * @param repID
     * @return
     */
    public static boolean SubsidiaryCanChange(String repID) {
    	boolean result = true;
		cat.debug("SubsidiaryCanChange of" + repID);
    	Connection dbConn = null;
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
    		if (dbConn == null)
    		{	
    			cat.error("Can't get database connection");
    			throw new Exception();
    		}
    		
    		PreparedStatement pstmt = dbConn.prepareStatement(sql_bundle.getString("parentIsExternal"));
    		pstmt.setDouble(1, Long.parseLong(repID));
    		ResultSet rs = pstmt.executeQuery();
    		if(rs.next())
    		{
    			int count = rs.getInt(1);
    			if(count > 0){
    				result = false;
    			}
    		}

    		rs.close();
    		pstmt.close();
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during ParentIsExternal", e);
    	}
    	finally
    	{
    		try
    		{
    			Torque.closeConnection(dbConn);
    		}
    		catch (Exception e)
    		{
    			cat.error("Error during release connection", e);
    		}
    	}

		cat.debug("result=" + result);
    	return result;
    }
    
    public static int EntityHasInternalChildren(String repID, String _entityLevel) {
    	int result = 0;
    	int level = 3; 

    	if(_entityLevel.equals(DebisysConstants.ISO)){
	    	level = 3;
	    }
	    else if(_entityLevel.equals(DebisysConstants.AGENT)){
	    	level = 4;
	    }
	    else if(_entityLevel.equals(DebisysConstants.SUBAGENT)) {
	    	level = 5;
	    }
	    else if(_entityLevel.equals(DebisysConstants.REP)) {
	    	level = 1;
	    }
    	
    	Connection dbConn = null;
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
    		if (dbConn == null)
    		{	
    			cat.error("Can't get database connection");
    			throw new Exception();
    		}
    		
    		String sql = sql_bundle.getString("getCountChildrenWithInternalAcctType");
    		PreparedStatement pstmt = dbConn.prepareStatement(sql);
    		cat.debug("SQL" + sql);
    		pstmt.setString(1, repID);
    		pstmt.setInt(2, level);
    		cat.debug("Param 1: " + repID);
    		cat.debug("Param 2: " + level);
    		ResultSet rs = pstmt.executeQuery();
    		if(rs.next())
    		{
	    		cat.debug("Count: " + rs.getInt("Count"));
    			if(rs.getInt("Count") > 0) 
    				result = 1;
    		}

    		rs.close();
    		pstmt.close();
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during EntityHasInternalChildren", e);
    	}
    	finally
    	{
    		try
    		{
    			Torque.closeConnection(dbConn);
    		}
    		catch (Exception e)
    		{
    			cat.error("Error during release connection", e);
    		}
    	}
    	
    	return result;
    }
    
    public static int GetEntityAccountTypeRepType(String repID) {
    	int type = -1;
    	Connection dbConn = null;
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
    		if (dbConn == null)
    		{	
    			cat.error("Can't get database connection");
    			throw new Exception();
    		}
    		
    		PreparedStatement pstmt = dbConn.prepareStatement("SELECT EntityAccountType FROM reps WITH (NOLOCK) WHERE rep_id = ?");
    		pstmt.setString(1, repID);
    		ResultSet rs = pstmt.executeQuery();
    		if(rs.next())
    		{
    			type = rs.getInt("EntityAccountType");
    		}

    		rs.close();
    		pstmt.close();
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during ParentIsExternal", e);
    	}
    	finally
    	{
    		try
    		{
    			Torque.closeConnection(dbConn);
    		}
    		catch (Exception e)
    		{
    			cat.error("Error during release connection", e);
    		}
    	}
    	return type;
    }
    
    public static int GetEntityAccountTypeMerchantType(String merchantID) {
    	int type = -1;
    	Connection dbConn = null;
    	try
    	{
    		dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
    		if (dbConn == null)
    		{	
    			cat.error("Can't get database connection");
    			throw new Exception();
    		}
    		
    		PreparedStatement pstmt = dbConn.prepareStatement("SELECT EntityAccountType FROM merchants WITH (NOLOCK) WHERE merchant_id = ?");
    		pstmt.setString(1, merchantID);
    		ResultSet rs = pstmt.executeQuery();
    		if(rs.next())
    		{
    			type = rs.getInt("EntityAccountType");
    		}

    		rs.close();
    		pstmt.close();
    	}
    	catch (Exception e)
    	{
    		cat.error("Error during ParentIsExternal", e);
    	}
    	finally
    	{
    		try
    		{
    			Torque.closeConnection(dbConn);
    		}
    		catch (Exception e)
    		{
    			cat.error("Error during release connection", e);
    		}
    	}
    	return type;
    }
}
