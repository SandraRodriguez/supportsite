/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.utils;

import java.util.HashMap;

/**
 *
 * @author nmartinez
 */
public class QueryReportDataAssoc extends QueryFilter {

    public enum ActionType {
        COUNT,
        LIST
    }

    public enum ReportType {
        CURRENT_TERMINALS,
        SEARCH_INFO,
        GROUPED
    }
    
    private ReportType reportType;
    
    private ActionType reportFeature;
    
    private HashMap dataFilter;

    public QueryReportDataAssoc() {
        dataFilter = new HashMap();
    }

    /**
     * @return the dataFilter
     */
    public HashMap getDataFilter() {
        return dataFilter;
    }

    /**
     * @return the reportFeature
     */
    public ActionType getReportFeature() {
        return reportFeature;
    }

    /**
     * @param reportFeature the reportFeature to set
     */
    public void setReportFeature(ActionType reportFeature) {
        this.reportFeature = reportFeature;
    }

    /**
     * @return the reportType
     */
    public ReportType getReportType() {
        return reportType;
    }

    /**
     * @param reportType the reportType to set
     */
    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }
   

}
