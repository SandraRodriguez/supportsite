/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.utils;

import com.debisys.reports.PropertiesByFeature;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author nmartinez
 */
public class SimControl {
    
    /**
     * 
     * @param propertiesByFeature
     * @param tokenControl
     * @param language
     * @return 
     */
    public static SimControl setUpControlbyTokenHash(HashMap<String, PropertiesByFeature> propertiesByFeature, String tokenControl, String language) {
        SimControl control = null;
        if (propertiesByFeature.get(tokenControl + "REQUIRED") != null) {
            control = new SimControl();
            control.setUrlHelpImage("images/transparent.gif");
            control.setHelpText("");
            control.setValue("");
            control.setControlId(tokenControl);
            PropertiesByFeature label = propertiesByFeature.get(tokenControl + "LABEL_" + language);
            if (label != null) {
                control.setLabel(label.getValue());
            }
            PropertiesByFeature pattern = propertiesByFeature.get(tokenControl + "PATTERN");
            if (pattern != null) {
                control.setPattern(pattern.getValue());
            }
            PropertiesByFeature json = propertiesByFeature.get(tokenControl + "JSON");
            if (json != null) {
                control.setJson(json.getValue());
            }
            PropertiesByFeature length = propertiesByFeature.get(tokenControl + "LENGTH");
            if (length != null) {
                control.setLength(length.getValue());
            }
            PropertiesByFeature required = propertiesByFeature.get(tokenControl + "REQUIRED");
            if (required != null) {
                control.setRequired(Boolean.parseBoolean(required.getValue()));
                PropertiesByFeature validateLength = propertiesByFeature.get(tokenControl + "VALIDATE_LENGTH");
                if (validateLength != null) {
                    //true|>=
                    String[] valData = validateLength.getValue().split("\\|");
                    control.setValidateLength(Boolean.parseBoolean(valData[0]));
                    if (control.getValidateLength()) {
                        control.setOperatorLengthValidator(valData[1]);
                    }
                } else {
                    control.setValidateLength(false);
                }
            }
            PropertiesByFeature help = propertiesByFeature.get(tokenControl + "HELP_" + language);
            if (help != null) {
                control.setHelpText(help.getValue());
                if (help.getValue() != null && help.getValue().length() > 0) {
                    control.setUrlHelpImage("images/help-icon.png");
                }
            }
            {
                PropertiesByFeature property = propertiesByFeature.get(tokenControl + "LEN_VAL_MESSAGE_" + language);
                if (property != null && property.getValue() != null && property.getValue().length() > 0) {
                    control.setMessageValLength(property.getValue());
                }
            }
            PropertiesByFeature options = propertiesByFeature.get(tokenControl + "OPTIONS_" + language);
            if (options != null) {
                //String[] value = options.getValue().split(",");
                control.setValue(options.getValue());
                //control.setOptionsControl(itemsToControl);
            }
        }
        return control;
    }
    
    private String value;
    private String label;
    private String urlHelpImage;
    private String helpText;
    private String controlId;
    private String json;
    private String pattern;
    private String length;
    private Boolean required;
    private Boolean rendered;    
    private Object value1;
    private String styleError;
    private String messageValLength;
    private Boolean validateLength;
    private Boolean enable;
    private String operatorLengthValidator;

    /**
     *
     */
    public SimControl() {
        this.rendered = true;
        this.enable = true;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the urlHelpImage
     */
    public String getUrlHelpImage() {
        return urlHelpImage;
    }

    /**
     * @param urlHelpImage the urlHelpImage to set
     */
    public void setUrlHelpImage(String urlHelpImage) {
        this.urlHelpImage = urlHelpImage;
    }

    /**
     * @return the helpText
     */
    public String getHelpText() {
        return helpText;
    }

    /**
     * @param helpText the helpText to set
     */
    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    /**
     * @return the controlId
     */
    public String getControlId() {
        return controlId;
    }

    /**
     * @param controlId the controlId to set
     */
    public void setControlId(String controlId) {
        this.controlId = controlId;
    }

    /**
     * @return the json
     */
    public String getJson() {
        return json;
    }

    /**
     * @param json the json to set
     */
    public void setJson(String json) {
        this.json = json;
    }

    /**
     * @return the pattern
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * @param pattern the pattern to set
     */
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    /**
     * @return the length
     */
    public String getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * @return the required
     */
    public Boolean getRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(Boolean required) {
        this.required = required;
    }    

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the value1
     */
    public Object getValue1() {
        return value1;
    }

    /**
     * @param value1 the value1 to set
     */
    public void setValue1(Object value1) {
        this.value1 = value1;
    }

    /**
     * @return the rendered
     */
    public Boolean getRendered() {
        return rendered;
    }

    /**
     * @param rendered the rendered to set
     */
    public void setRendered(Boolean rendered) {
        this.rendered = rendered;
    }

    /**
     * @return the styleError
     */
    public String getStyleError() {
        return styleError;
    }

    /**
     * @param styleError the styleError to set
     */
    public void setStyleError(String styleError) {
        this.styleError = styleError;
    }

    /**
     * @return the messageValLength
     */
    public String getMessageValLength() {
        return messageValLength;
    }

    /**
     * @param messageValLength the messageValLength to set
     */
    public void setMessageValLength(String messageValLength) {
        this.messageValLength = messageValLength;
    }

    /**
     * @return the validateLength
     */
    public Boolean getValidateLength() {
        return validateLength;
    }

    /**
     * @param validateLength the validateLength to set
     */
    public void setValidateLength(Boolean validateLength) {
        this.validateLength = validateLength;
    }

    /**
     * @return the enable
     */
    public Boolean getEnable() {
        return enable;
    }

    /**
     * @param enable the enable to set
     */
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    /**
     * @return the operatorLengthValidator
     */
    public String getOperatorLengthValidator() {
        return operatorLengthValidator;
    }

    /**
     * @param operatorLengthValidator the operatorLengthValidator to set
     */
    public void setOperatorLengthValidator(String operatorLengthValidator) {
        this.operatorLengthValidator = operatorLengthValidator;
    }
}
