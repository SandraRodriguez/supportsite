package com.debisys.utils;


import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.dsfactory.AbstractDataSourceFactory;
import org.apache.torque.dsfactory.DataSourceFactory;

import javax.sql.DataSource;


/**
 * @author Franky Villadiego
 */
public class C3P0DataSourceFactory extends AbstractDataSourceFactory implements DataSourceFactory{

    private static Logger log = Logger.getLogger(C3P0DataSourceFactory.class);

    private ComboPooledDataSource ds;


    @Override
    public DataSource getDataSource() throws TorqueException {
        return ds;
    }


    @Override
    public void initialize(Configuration configuration) throws TorqueException {
        super.initialize(configuration);
        configureLog();
        ComboPooledDataSource ds = initJdbc2Pool(configuration);
        this.ds =  ds;

    }

    private ComboPooledDataSource initJdbc2Pool(Configuration configuration) throws TorqueException{
        log.debug("***** Starting C3P0Pool...");
        ComboPooledDataSource ds = new ComboPooledDataSource();
        Configuration c = Torque.getConfiguration();

        if (c == null || c.isEmpty()){
            log.warn("Global Configuration not set, no Default pool data source configured!");
        }else{
            Configuration conf = c.subset(DEFAULT_POOL_KEY);
            applyConfiguration(conf, ds);
        }

        Configuration conf = configuration.subset(POOL_KEY);
        applyConfiguration(conf, ds);
        return ds;
    }

    @Override
    public void close() throws TorqueException {
        try{
            ds.close();
        }catch (Exception e){
            log.error("Exception caught during close()", e);
            throw new TorqueException(e);
        }
        ds = null;
    }

    private void configureLog(){
        System.setProperty("com.mchange.v2.log.MLog","com.mchange.v2.log.FallbackMLog");
        System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL","WARNING");
    }
}
