package com.debisys.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.properties.Properties;

/**
 * This class will handle all email validation.
 */
public class ValidateEmail
{

	static Category cat = Category.getInstance(ValidateEmail.class);
	private String strEmailAddress;
	private String strErrorMessage = "No email address to Validate!";
	private boolean bIsEmailValid = false;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.utils");


	//****DBSY-1049 - Emunoz****
	/**
	 * check the syntax of emails separate by semicolon
	 * @return
	 */
	public boolean validateEmailList(){
		String strd = this.strEmailAddress.trim();
		if(strd.length() > 0){
			String[] email = strd.split(";");
			for (int i = 0; i < email.length; i++) {
				if (!this.isEmailValidList(email[i].trim())) {
					return false;
				}
			}
		}
		return true;
	}
	//****END DBSY-1049 - Emunoz****

	/**
	 * Check the syntax of the current email address.
	 * 
	 * @return
	 */
	public boolean checkSyntax()
	{

		StringBuffer sbEmailAddress = new StringBuffer(this.strEmailAddress);

		if (sbEmailAddress != null)
		{
			if (this.strEmailAddress.length() >= 5)
			{

				// Check for the domain separator
				int indexSeparator = this.strEmailAddress.indexOf('@');
				int indexLastSeparator = this.strEmailAddress.lastIndexOf('@');

				// Make sure there's only one separator
				if ((indexSeparator > 0) && (indexSeparator == indexLastSeparator) && ((indexSeparator + 1) < sbEmailAddress.length()))
				{
					if (this.validateUserName(sbEmailAddress.substring(0, indexSeparator)) && this.validateDomain(sbEmailAddress.substring(indexSeparator + 1)))
					{
						this.bIsEmailValid = true;
						this.strErrorMessage = "Email Address is Valid";
					}
					else
					{
						this.bIsEmailValid = false;
					}
				}
				else
				{
					this.bIsEmailValid = false;
					this.strErrorMessage = "Email Address requires a valid domain";
				}
			}
			else
			{
				this.bIsEmailValid = false;
				this.strErrorMessage = "Email Address is too short";
			}
		}
		else
		{
			this.bIsEmailValid = false;
			this.strErrorMessage = "There is no email to Validate";
		}

		return this.bIsEmailValid;
	}

	/**
	 * Sets the email address field.
	 * 
	 * @param strEmail
	 */
	public void setStrEmailAddress(String strEmail)
	{
		if (strEmail != null)
		{
			this.strEmailAddress = strEmail;
		}
		else
		{
			this.strEmailAddress = "";
		}

	}

	/**
	 * Gets the current email address.
	 * 
	 * @return
	 */
	public String getStrEmailAddress()
	{

		String strReturn;

		if (this.strEmailAddress != null)
		{
			strReturn = this.strEmailAddress;
		}
		else
		{
			strReturn = "";
		}

		return strReturn;
	}

	/**
	 * Gets the error message if email is not valid.
	 * 
	 * @return
	 */
	public String getStrErrorMessage()
	{

		String returnVal;

		if (!this.bIsEmailValid)
		{
			returnVal = this.strErrorMessage;
		}
		else
		{
			returnVal = "Email Address is correct";
		}

		return returnVal;
	}

	//****DBSY-1049 - Emunoz****
	public boolean isEmailValidList(String emails)
	{
		//Pattern p = Pattern.compile(".+@.+\\.[a-z]+");

		Pattern p = Pattern.compile("^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$",Pattern.CASE_INSENSITIVE);

		// Match the given string with the pattern
		Matcher m = p.matcher(emails);

		// check whether match is found
		boolean matchFound = m.matches();

		StringTokenizer st = new StringTokenizer(this.strEmailAddress, ".");
		String lastToken = null;
		while (st.hasMoreTokens())
		{
			lastToken = st.nextToken();
		}

		if (matchFound && lastToken.length() >= 2 && this.strEmailAddress.length() - 1 != lastToken.length())
		{
			//System.err.println("Email addresses with erros.");
			// validate the country code
			return true;
		}
		else
		{
			return false;
		}
	}
	//****DBSY-1049 - Emunoz****

	/**
	 * Returns whether the email address is valid or not.
	 * 
	 * @return
	 */
	public boolean isEmailValid()
	{
		//Pattern p = Pattern.compile(".+@.+\\.[a-z]+");

		Pattern p = Pattern.compile("^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$",Pattern.CASE_INSENSITIVE);

		// Match the given string with the pattern
		Matcher m = p.matcher(this.strEmailAddress);

		// check whether match is found
		boolean matchFound = m.matches();

		StringTokenizer st = new StringTokenizer(this.strEmailAddress, ".");
		String lastToken = null;
		while (st.hasMoreTokens())
		{
			lastToken = st.nextToken();
		}

		if (matchFound && lastToken.length() >= 2 && this.strEmailAddress.length() - 1 != lastToken.length())
		{
			//System.err.println("Email addresses with erros.");
			// validate the country code
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Checks the validity of the user name part of the given email address
	 * 
	 * @param strUserName
	 *            user name part of the email.
	 * @return true if user name is valid, false otherwise
	 */
	private boolean validateUserName(String strUserName)
	{

		boolean bValid = true;

		if ((strUserName != null) && (strUserName.length() > 0))
		{
			StringBuffer sbUserName = new StringBuffer(strUserName);
			int intLength = sbUserName.length();

			for (int intChar = 0; intChar < intLength; intChar++)
			{
				char charCurrent = sbUserName.charAt(intChar);

				if (!Character.isLetterOrDigit(charCurrent))
				{

					// If not a character or digit, check for allowable symbols
					if ((charCurrent != '.') && (charCurrent != '_') && (charCurrent != '-'))
					{
						bValid = false;

						break;
					}
				}
			}
		}
		else
		{
			bValid = false;
		}

		// If not valid, set the error message
		if (!bValid)
		{
			this.strErrorMessage = "Invalid user [" + strUserName + "]";
		}

		return bValid;
	}

	/**
	 * Checks for the validity of a given domain
	 * 
	 * @param strDomain
	 *            the host name (domain) part of the email.
	 * @return true if domain is valid, false otherwise
	 */
	private boolean validateDomain(String strDomain)
	{

		boolean bValid = true;

		if ((strDomain != null) && (strDomain.length() > 0))
		{
			StringBuffer sbDomain = new StringBuffer(strDomain);

			// If surrounded by brackets, the domain is an IP address
			if ((strDomain.charAt(0) == '[') && (strDomain.charAt(sbDomain.length() - 1) == ']'))
			{
				bValid = this.validateIPDomain(sbDomain.substring(1, sbDomain.length() - 2));
			}
			else
			{

				// If not surrounded by brackets, but a valid IP, still return false
				if (this.validateIPDomain(strDomain))
				{
					bValid = false;
				}
				else
				{
					int intLength = sbDomain.length();

					int indexOfDot = sbDomain.toString().indexOf(".");
					// must have a . in the domain
					if (indexOfDot < 0)
					{
						bValid = false;
					}
					else
					{
						for (int intChar = 0; intChar < intLength; intChar++)
						{
							char charCurrent = sbDomain.charAt(intChar);

							if (!Character.isLetterOrDigit(charCurrent))
							{

								// If not a character or digit, check for allowable symbols
								if ((charCurrent != '.') && (charCurrent != '-'))
								{
									bValid = false;

									break;
								}
								else
								{
									// cannot begin with or end with the special char
									if ((intChar == 0) || (intChar == (intLength - 1)))
									{
										bValid = false;
										break;
									}
									else
									{
										// If one of the allowed symbol, make sure the next one is a
										// digit or a number
										char charNext = sbDomain.charAt(intChar + 1);

										if (!Character.isLetterOrDigit(charNext))
										{
											bValid = false;

											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			bValid = false;
		}

		// If not valid, set the error message
		if (!bValid)
		{
			this.strErrorMessage = "Invalid domain [" + strDomain + "]";
		}

		return bValid;
	}

	/**
	 * Checks for validity of IP address
	 * 
	 * @param strIP
	 * @return
	 */
	private boolean validateIPDomain(String strIP)
	{

		boolean bValid = true;

		if ((strIP != null) && (strIP.length() > 0))
		{
			StringBuffer sbIP = new StringBuffer(strIP);
			int intLength = sbIP.length();
			int intChar = 0;
			int intTotalIPSeparators = 0;

			for (intChar = 0; intChar < intLength; intChar++)
			{
				if (sbIP.charAt(intChar) == '.')
				{
					intTotalIPSeparators++;
				}
			}

			// Support IPv6?
			if (intTotalIPSeparators == 3)
			{
				StringBuffer sbTemp = new StringBuffer();

				for (intChar = 0; intChar < intLength; intChar++)
				{
					char charCurrent = sbIP.charAt(intChar);

					if (!Character.isDigit(charCurrent))
					{
						if (charCurrent != '.')
						{
							bValid = false;

							break;
						}
						else
						{
							try
							{
								int intSub = Integer.parseInt(sbTemp.toString());

								if ((intSub >= 0) && (intSub <= 255))
								{

									// Reset the temporary container
									sbTemp = new StringBuffer();
								}
								else
								{
									bValid = false;
								}
							}
							catch (NumberFormatException e)
							{
								bValid = false;
							}
						}
					}
					else
					{
						sbTemp.append(charCurrent);
					}

					if (!bValid)
					{
						break;
					}
				}
			}
			else
			{
				bValid = false;
			}
		}
		else
		{
			bValid = false;
		}

		// If not valid, set the error message
		if (!bValid)
		{
			this.strErrorMessage = "Invalid IP address [" + strIP + "]";
		}

		return bValid;
	}

	/**
	 * @return
	 */
	public static String getSetupMail(ServletContext context)
	{
		String strSetupMail = "";
		/*
		 * try { strSetupMail = sql_bundle.getString("setupMail"); } catch(NumberFormatException
		 * nfe) { strSetupMail = "setups@emida.net"; }
		 */

		Connection dbConn = null;
		PreparedStatement pstmtLogRateChanges = null;
		try
		{
			dbConn = Torque.getConnection(ValidateEmail.sql_bundle.getString("pkgAlternateDb"));
			// dbConn = Torque.getConnection(sql_bundle.getString("setupMail"));
			pstmtLogRateChanges = dbConn.prepareStatement(ValidateEmail.sql_bundle.getString("setupMail"));
			pstmtLogRateChanges.setString(1, DebisysConfigListener.getInstance(context));
			ResultSet rs = pstmtLogRateChanges.executeQuery();
			if (rs.next())
			{
				strSetupMail = rs.getString(1);
			}
			else
			{
				ValidateEmail.cat.error("Error during getSetupMail :no email set up ");
			}
			rs.close();
			pstmtLogRateChanges.close();
		}
		catch (Exception localException)
		{
			// If no log record could be saved, the failure should not affect the operation
			ValidateEmail.cat.error("Error during getSetupMail call : ", localException);
		}
		finally
		{
			try
			{
				if (pstmtLogRateChanges != null)
				{
					pstmtLogRateChanges.close();
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				ValidateEmail.cat.error("Error during release connection", e);
			}
		}

		return strSetupMail;
	}

	/**
	 * @param subject
	 * @param mailHost
	 * @param sMails
	 * @param from
	 * @param textMessage
	 * @param instance
	 * @throws MessagingException
	 */
	public static void sendMail(String subject, String mailHost, String sMails, String from, String textMessage, String instance)
	throws MessagingException
	{
		java.util.Properties props;
		props = System.getProperties();
		props.put("mail.smtp.host", mailHost);
		Session s = Session.getDefaultInstance(props, null);

		MimeMessage message = new MimeMessage(s);

		message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
		message.setFrom(new InternetAddress(from));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(Properties.getPropertieByName(instance, "customerServiceMail")));
		message.setRecipients(Message.RecipientType.CC, sMails.replaceAll(";", ","));
		message.setSubject(subject);
		message.setContent(textMessage, "text/html");
		Transport.send(message);

	}

    public static boolean validateEmail(String emailToValidate){
        Pattern compile = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = compile.matcher(emailToValidate);
        return matcher.matches();

    }

    public boolean isValidEmail(String emailToValidate){
        return validateEmail(emailToValidate);
    }

}
