/**
 * 
 */
package com.debisys.utils;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.lang.management.ManagementFactory;

import org.apache.torque.Torque;

import com.debisys.users.SessionData;



/**
 * @author fernandob
 * This class is a container for system method calls
 */
public class CSystemMethods 
{

	/**
	 * Get current host name
	 * @return The host name if could be obtained, if not an empty string
	 */
	public static String getHostName()
	{
		String retValue = "";
	
		try
		{
			InetAddress   in  = InetAddress.getLocalHost();
			retValue = in.getHostName();
		}
		catch(java.net.UnknownHostException localHostException)
		{
			retValue = "";
		}
		catch(Exception localException)
		{
			retValue = "";
		}
		return retValue;
	}
	
	
	/**
	 * @return A list of all IP addresses of this machine. Null if error happened
	 */
	public static String[] getAllAdresses()
	{
		String[] retValue = null;
		
		try
		{
			InetAddress   in  = InetAddress.getLocalHost();
			
			InetAddress[] all = InetAddress.getAllByName(in.getHostName());
			if(all.length > 0)
			{
				retValue = new String[all.length];
			}
			for (int i=0; i < all.length; i++) 
			{
				retValue[i] = all[i].getHostAddress();
			}

		}
		catch(java.net.UnknownHostException localHostException)
		{
			retValue = null;
		}
		catch(Exception localException)
		{
			retValue = null;
		}		
		return retValue;
	}
	
	/**
	 * @return The main IP addresses of this machine. Empty string if error
	 */
	public static String getIpAdress()
	{
		String retValue = "";
		
		try
		{
			InetAddress   in  = InetAddress.getLocalHost();
			in.getHostAddress();
		}
		catch(java.net.UnknownHostException localHostException)
		{
			retValue = "";
		}
		catch(Exception localException)
		{
			retValue = "";
		}		
		return retValue;
	}
	
	
	/**
	 * @return Current process ID or an empty string if error
	 */
	public static String getProcessId()
	{
		String retValue = "";
		String hostName = "";
		String strPid = "";
		
		try
		{
			hostName = getHostName();
			strPid = ManagementFactory.getRuntimeMXBean().getName();
			strPid = strPid.replace(hostName, "");
			strPid = strPid.replace("@", "");
		}
		catch(Exception localException)
		{
			retValue = "";
		}		
		return retValue;
	}
	
	
	 /**
	 * @param sessionData To get logged user information 
	 */
	public static void saveRateChangeLog(SessionData sessionData, String sqlCommandString, java.util.ResourceBundle sql_bundle,
			org.apache.log4j.Category cat)
	  {
		  Connection dbConn = null;
		  PreparedStatement pstmtLogRateChanges = null;
		  try
		  {
			  dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			  pstmtLogRateChanges = dbConn.prepareStatement(sql_bundle.getString("insertLogRecord"));
			  pstmtLogRateChanges.setString(1, CSystemMethods.getHostName());
			  pstmtLogRateChanges.setString(2, CSystemMethods.getProcessId());
			  pstmtLogRateChanges.setString(3, sqlCommandString);
			  pstmtLogRateChanges.setString(4, CSystemMethods.getIpAdress());
			  pstmtLogRateChanges.setString(5, sessionData.getUser().getUsername());
			  pstmtLogRateChanges.execute();
			  pstmtLogRateChanges.close();
		  }
		  catch(Exception localException)
		  {		  
			  // If no log record could be saved, the failure should not affect the operation
			  cat.error("Error during saveRateChangeLog call : ", localException);
		  }
		  finally
		  {
		      try
		      {
		    	  if(pstmtLogRateChanges != null) pstmtLogRateChanges.close();
		        Torque.closeConnection(dbConn);
		      } 
		      catch (Exception e)
		      {
		        cat.error("Error during release connection", e);
		      }
		  }
	  }

}
