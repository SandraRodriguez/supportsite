package com.debisys.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.TransactionException;
import com.debisys.reports.TransactionReport;
import com.debisys.users.SessionData;

public class Announcements {
	
	private static Logger cat = Logger.getLogger(TransactionReport.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
	
	public static Vector<Vector<String>> getAvailableAnnouncements(HttpServletRequest request)throws TransactionException {
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		Connection dbConn = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try{
			String requestUrl = request.getRequestURL().toString();
			String serverName = request.getServerName();
			String domainName = "";
			if ((serverName != null) && (!serverName.equals("")) && !serverName.equalsIgnoreCase("support.debisys.com")){
				int index = serverName.indexOf('.');
				if (index == -1){
					domainName = serverName;
				}else{
					int index2 = serverName.indexOf('.', index + 1);
					if (index2 == -1){
						domainName = serverName;
					}else{
						domainName = serverName.substring(index + 1);
					}
				}
			}else{
				serverName = "support.debisys.com";
				domainName = "debisys.com";	
			}
			
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null){
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			
			String strSQLGetAgentList = "select n.IdSupportSiteBranding, ssn.id, IdSSNotification,ssn.description,startDate,endDate,sslevel,directionArea, text_notice "+
				" from supportsite_brandings ssb "+
				" INNER JOIN supportsite_post_notification n ON n.IdSupportSiteBranding = ssb.Id "+
				" INNER JOIN supportsite_post_notification_notice ssn ON n.id = ssn.IdSSNotification "+
				" WHERE (ssb.urlBranding like '%"+requestUrl+"%' OR ssb.urlBranding like '%"+domainName+"%') AND (ssn.startDate <= getDate() AND (ssn.endDate is NULL or ssn.endDate >= getDate())) order by ssn.sslevel ";
			
			cat.debug(strSQLGetAgentList + "/* requestUrl = " + requestUrl + " || domainName="+domainName+" */");
			pstmt = dbConn.prepareStatement(strSQLGetAgentList);
			rs = pstmt.executeQuery();
			
			while (rs.next()){
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("directionArea")));
				vecTemp.add(StringUtil.toString(rs.getString("text_notice")));
				vecResultList.add(vecTemp);
			}
			
		}
		catch (Exception e){
			cat.error("Error during getAvailableAnnouncements ", e);
			throw new TransactionException();
		}
		finally{
			try{
				if(rs != null){
					rs.close();
					rs = null;
				}
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e){
				cat.error("Error during closeConnection (getAvailableAnnouncements) ", e);
			}
		}
		return vecResultList;
	}

}
