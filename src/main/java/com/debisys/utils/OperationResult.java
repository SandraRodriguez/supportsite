/*
 * EMIDA all rights reserved 1999-2019.
 */
package com.debisys.utils;

import com.google.gson.Gson;
import java.lang.reflect.Type;
import org.apache.log4j.Logger;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author fernandob
 */
public class OperationResult {
    
    private static final Logger logger = Logger.getLogger(OperationResult.class);
    
    public static final String SUCCESSFUL_ERROR_CODE = "000";
    
    private boolean error;
    private String resultCode;
    private String resultMessage;
    
    public boolean isError() {
        return error;
    }
    
    public void setError(boolean error) {
        this.error = error;
    }
    
    public String getResultCode() {
        return resultCode;
    }
    
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }
    
    public String getResultMessage() {
        return resultMessage;
    }
    
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
    
    public OperationResult() {
        this.error = false;
        this.resultCode = "";
        this.resultMessage = "";
    }
    
    public OperationResult(boolean error, String resultCode, String resultMessage) {
        this.error = error;
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
    }
    
    @Override
    public String toString() {
        return "OperationResult{" + "error=" + error + ", resultCode=" + resultCode + ", resultMessage=" + resultMessage + '}';
    }
    
    public String toJsonString() {
        String retValue = null;
        
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<OperationResult>() {
            }.getType();
            retValue = gson.toJson(this, type);
        } catch (Exception localException) {
            logger.error("Error while converting operation result to JSON", localException);
        }
        return retValue;
    }
    
}
