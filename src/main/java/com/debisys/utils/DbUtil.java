package com.debisys.utils;

import com.debisys.languages.Languages;
import com.debisys.reports.pojo.BankPojo;
import com.debisys.reports.pojo.BasicPojo;
import com.debisys.reports.pojo.PaymentRequestRejectReasonPojo;
import com.debisys.users.SessionData;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.ServletContext;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

public class DbUtil {

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");

    static Category cat = Category.getInstance(DbUtil.class);

    public static int getRecordCount(ResultSet rs) {
        int recordCount;
        try {
            rs.last();
            recordCount = rs.getRow();
        } catch (Exception ex) {
            recordCount = 0;
        }
        return recordCount;

    }

    public static int getRowNumber(ResultSet rs, int intRecordsPerPage, int intRecordCount, int intPageNumber) {
        int rowNumber;
        if ((intRecordsPerPage > intRecordCount) || intPageNumber <= 1) {
            rowNumber = 0;
        } else if (intPageNumber > (intRecordCount / intRecordsPerPage)) {
            // if its not exact add one to page count
            intPageNumber = intRecordCount / intRecordsPerPage;
            if (intRecordCount % intRecordsPerPage > 0) {
                intPageNumber++;
            }

            rowNumber = (intPageNumber - 1) * intRecordsPerPage;
        } else {
            rowNumber = (intPageNumber - 1) * intRecordsPerPage;
        }

        return rowNumber + 1;

    }

    public static String displayNavigation(String baseURL, int intPageSize, int intRecordCount, int intPage,
            SessionData sessionData) {
        StringBuffer navigation = new StringBuffer();
        int intPageCount = (intRecordCount / intPageSize);
        if ((intPageCount * intPageSize) < intRecordCount) {
            intPageCount++;
        }

        if (intPage > 1) {
            navigation.append("<a href=\"" + baseURL + "&page=1\">"
                    + Languages.getString("jsp.admin.first", sessionData.getLanguage()) + "</a>&nbsp;");
            navigation.append("<a href=\"" + baseURL + "&page=" + (intPage - 1) + "\">&lt;&lt;"
                    + Languages.getString("jsp.admin.previous", sessionData.getLanguage()) + "</a>&nbsp;");
        }
        int intLowerLimit = intPage - 12;
        int intUpperLimit = intPage + 12;

        if (intLowerLimit < 1) {
            intLowerLimit = 1;
            intUpperLimit = 25;
        }

        for (int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++) {
            if (i == intPage) {
                navigation.append("<font color=#ff0000>" + i + "</font>&nbsp;");
            } else {
                navigation.append("<a href=\"" + baseURL + "&page=" + i + "\">" + i + "</a>&nbsp;");
            }
        }
        if (intPage < intPageCount) {
            navigation.append("<a href=\"" + baseURL + "&page=" + (intPage + 1) + "\">"
                    + Languages.getString("jsp.admin.next", sessionData.getLanguage()) + "&gt;&gt;</a>&nbsp;");
            navigation.append("<a href=\"" + baseURL + "&page=" + (intPageCount) + "\">"
                    + Languages.getString("jsp.admin.last", sessionData.getLanguage()) + "</a>&nbsp;");
        }

        return navigation.toString();

    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all agents under the 5 level ISO
     */
    public static String sqlSelISO5Agent(String _isoID) {
        return String.format("select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 4", _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all subagents under the 5 level ISO
     */
    public static String sqlSelISO5SubAgent(String _isoID) {
        return String
                .format(
                        "select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 4) and type = 5",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all reps under the 5 level ISO
     */
    public static String sqlSelISO5Rep(String _isoID) {
        return String
                .format(
                        "select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 4) and type = 5) and type = 1",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all merchants under the 5 level ISO
     */
    public static String sqlSelISO5Merch(String _isoID) {
        return String
                .format(
                        "select merchant_id from merchants WITH (NOLOCK) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 4) and type = 5) and type = 1)",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all entities under the ISO (Agent/SubAgent/Rep in
     * descending order except merchants)
     */
    public static String sqlSelAllISO5Entities(String _isoID) {
        return String
                .format(
                        "select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s)) and type != 3",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all entities under the ISO (Agent/SubAgent/Rep in
     * descending order except merchants) but gets their type as well so dont
     * use this with the IN keyword
     */
    public static String sqlSelAllISO5EntitiesWithDetail(String _isoID) {
        return String
                .format(
                        "select rep_id, type, businessname, entityaccounttype from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s)) and type != 3 order by CASE type WHEN 4 THEN 1 WHEN 5 THEN 2 WHEN 1 THEN 3 END",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return
     */
    public static String sqlSelAllISO5MerchWithDetail(String _isoID) {
        return String
                .format(
                        "select merchant_id, dba, entityaccounttype from merchants WITH (NOLOCK) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 4) and type = 5) and type = 1)",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all reps under the 3 level ISO
     */
    public static String sqlSelISO3Rep(String _isoID) {
        return String.format("select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 1", _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return SQL to get all merchants under the 3 level ISO
     */
    public static String sqlSelISO3Merch(String _isoID) {
        return String
                .format(
                        "select merchant_id from merchants WITH (NOLOCK) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 1)",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return
     */
    public static String sqlSelAllISO3EntitiesWithDetail(String _isoID) {
        return String
                .format(
                        "select rep_id, type, businessname, entityaccounttype from reps WITH (NOLOCK) where iso_id = %s and type != 2",
                        _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return
     */
    public static String sqlSelAllISO3Entities(String _isoID) {
        return String.format("select rep_id from reps WITH (NOLOCK) where iso_id = %s and type != 2", _isoID);
    }

    /**
     * *
     *
     * @param _isoID
     * @return
     */
    public static String sqlSelAllISO3MerchWithDetail(String _isoID) {
        return String
                .format(
                        "select merchant_id, dba, entityaccounttype from merchants WITH (NOLOCK) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id = %s and type = 1)",
                        _isoID);
    }

    public static int getReportIntervalLimitDays(String reportCode, ServletContext context) {
        int retValue = Integer.MIN_VALUE;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }

            String sql = DbUtil.sql_bundle.getString("getReportIntervalLimitDays");
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                cat.debug("1:" + reportCode);
                pstmt.setString(1, reportCode);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    retValue = rs.getInt("reportBackwardDateLimit");
                    cat.debug("Result is: " + retValue);
                } else {
                    cat.debug("No result for query: " + sql);
                }
                rs.close();
                pstmt.close();
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            e.printStackTrace();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        if (retValue == Integer.MIN_VALUE) {
            retValue = Integer.valueOf(DebisysConfigListener.getReportIntervalLimitDays(context));
        }
        return retValue;
    }

    public static long getMerchantRep(String merchantId, ServletContext context) {
        long retValue = Long.MIN_VALUE;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }

            String sql = "SELECT [rep_id] FROM dbo.Merchants WHERE merchant_id = " + merchantId;
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                cat.debug("1:" + merchantId);
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    retValue = rs.getLong("rep_id");
                    cat.debug("Result is: " + retValue);
                } else {
                    cat.debug("No result for query: " + sql);
                }
                rs.close();
                pstmt.close();
            } catch (SQLException e) {
                cat.error("Error getting query result: ", e);
                processSqlException(e);
            }
        } catch (TorqueException e) {
            e.printStackTrace();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return retValue;
    }

    public static void processSqlException(SQLException e) {
        cat.error("Error during SQL command");
        do {
            cat.error("JDBC SQL Error: " + e.getMessage());
            cat.error("Vendor code: " + e.getErrorCode());
            cat.error("SQL State: " + e.getSQLState());
        } while ((e = e.getNextException()) != null);
        cat.error("No more errors for this request");
    }
    
    public static boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
        boolean retValue = false;

        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columns = rsmd.getColumnCount();
            for (int x = 1; x <= columns; x++) {
                if (columnName.equals(rsmd.getColumnName(x))) {
                    retValue = true;
                }
            }
        } catch (Exception localException) {
            cat.error("Error checking resultset column existence: ", localException);
        }
        return retValue;
    }
    
    public static ArrayList<BankPojo> getBankList(ServletContext context)
    {
        ArrayList<BankPojo> retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }
            
            String sql = DbUtil.sql_bundle.getString("getEnabledBanks");
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                rs = pstmt.executeQuery();
                retValue = new ArrayList<BankPojo>();
                while (rs.next()) {
                    BankPojo newBank = new BankPojo();
                    newBank.setId(rs.getLong("ID"));
                    newBank.setCode(rs.getString("Code"));
                    newBank.setName(rs.getString("Name"));
                    newBank.setEnabled(true);
                    retValue.add(newBank);
                }
                rs.close();
                pstmt.close();
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            e.printStackTrace();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        if((retValue != null) && (retValue.size() == 0))
        {
            retValue = null;
        }
        return retValue;
    }
    

    /**
     * 
     * @param context
     * @param paymentRequestId
     * @param targetStatus
     * @param user
     * @param reasonId
     * @return 
     */
    public static boolean updatePaymentRequestStatus(ServletContext context,
            long paymentRequestId, int targetStatus, String user, String reasonId) {
        boolean retValue = false;
        Connection dbConn = null;
        CallableStatement cstmt = null;
        
        try {
            dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            
            String sql = DbUtil.sql_bundle.getString("changePaymentRequestStatus");
            cat.debug("Preparing statement for query: " + sql);
            try {
                cstmt = dbConn.prepareCall(sql);
                cstmt.setLong(1, paymentRequestId);
                cstmt.setInt(2, targetStatus);
                cstmt.setString(3, user);
                cstmt.setString(4, reasonId);
                cstmt.execute();                
                retValue = true;
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            cat.error("Error while updating payment request status", e);
        } finally {
            try {
                if(cstmt != null)
                {
                    cstmt.close();
                }
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return retValue;
    }

    public static ArrayList<PaymentRequestRejectReasonPojo> getPaymentRejectReasonList(ServletContext context) {
        ArrayList<PaymentRequestRejectReasonPojo> retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }

            String sql = DbUtil.sql_bundle.getString("getPaymentRequestRejectReasons");
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                rs = pstmt.executeQuery();
                retValue = new ArrayList<PaymentRequestRejectReasonPojo>();
                while (rs.next()) {
                    PaymentRequestRejectReasonPojo newReason = new PaymentRequestRejectReasonPojo();
                    newReason.setCode(rs.getString("code"));
                    newReason.setDescription(rs.getString("description"));
                    newReason.setReasonId(rs.getString("reasonId"));
                    retValue.add(newReason);
                }
                rs.close();
                pstmt.close();
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            cat.error("Error while getting payment request reject reasons", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        if ((retValue != null) && (retValue.isEmpty())) {
            retValue = null;
        }
        return retValue;
    }
    
    /**
     * 
     * @param context
     * @return 
     */
    public static ArrayList<BasicPojo> getPaymentRequestsRejectReasons(ServletContext context)
    {
        ArrayList<BasicPojo> retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }
            
            String sql = DbUtil.sql_bundle.getString("PaymentRequestsRejectReasons");
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                rs = pstmt.executeQuery();
                retValue = new ArrayList<BasicPojo>();
                while (rs.next()) {
                    BasicPojo newBasic = new BasicPojo();
                    newBasic.setId(rs.getString("reasonId"));
                    newBasic.setDescripton(rs.getString("Code"));
                    retValue.add(newBasic);
                }
                rs.close();
                pstmt.close();
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            e.printStackTrace();
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        if((retValue != null) && (retValue.size() == 0))
        {
            retValue = null;
        }
        return retValue;
    }

    /**
     * This code is repeated over and over all over the code.
     * Then this method was created in order to be used when needed
     * @param conn      Database connection object
     * @param statement Usually a prepared statement
     * @param rs        Recordset
     */
    public static void closeDatabaseObjects(Connection conn, Statement statement,
            ResultSet rs){
        if(rs != null){
            try{
                if(!rs.isClosed()){
                    rs.close();
                }
            }catch(SQLException localRsSqlException){
                cat.debug("Resultset already closed");
            }
        }
        if(statement != null){
            try{
                if(!statement.isClosed()){
                    statement.close();                    
                }
            }catch(SQLException localStatementSqlException){
                cat.debug("Statement already closed");
            }
        }
        if(conn != null){
            try{
                Torque.closeConnection(conn);
            }catch(Exception localConnectionSqlException){
                cat.debug("Connection already closed");
            }
        }        
    }
    
    /**
     * This method is intended to print sql strings that has "?" on them with
     * the correspondent parameters replaced
     *
     * @param sqlString Sql string with parameter placeholders "?"
     * @param queryParams Array list of parameter string values
     */
    public static void printSqlString(String sqlString,
            List<String> queryParams) {
        try {
            if ((sqlString != null) && (queryParams != null) && (!queryParams.isEmpty())) {
                cat.debug(String.format("Preparing statement for query: %s.",
                        sqlString));
                int paramNumber = 1;
                for (String currentParam : queryParams) {
                    cat.debug(String.format("\tParam %d : %s", paramNumber, currentParam));
                    paramNumber++;
                }
            }
        } catch (Exception localException) {
            cat.warn("Error while printing sql string content", localException);
        }
    }
    
    
    public static List<String> getRepEntityChildIds(ServletContext context, Long parentEntityId) {
        List<String> retValue = new ArrayList<String>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }

            String sql = DbUtil.sql_bundle.getString("getRepEntityChildIds");
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setLong(1, parentEntityId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    retValue.add(rs.getString("rep_id"));
                }
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            cat.error("Error while rep child ids", e);
        } finally {
            closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }
    
    public static List<String> getAgentRepIds(ServletContext context, Long agentId) {
        List<String> retValue = new ArrayList<String>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }

            String sql = DbUtil.sql_bundle.getString("getAgentRepIds");
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setLong(1, agentId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    retValue.add(rs.getString("rep_id"));
                }
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            cat.error("Error while geting agent rep id's", e);
        } finally {
            closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public static List<String> getRepMerchantIds(ServletContext context, Long repId) {
        List<String> retValue = new ArrayList<String>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(DbUtil.sql_bundle.getString("pkgAlternateDb"));
            }

            String sql = DbUtil.sql_bundle.getString("getRepMerchantIds");
            cat.debug("Preparing statement for query: " + sql);
            try {
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setLong(1, repId);
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    retValue.add(rs.getString("merchant_id"));
                }
            } catch (SQLException e) {
                processSqlException(e);
            }
        } catch (TorqueException e) {
            cat.error("Error while getting rep merchant id's", e);
        } finally {
            closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }


    
}
