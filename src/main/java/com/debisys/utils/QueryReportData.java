/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.utils;

/**
 *
 * @author nmartinez
 */
public class QueryReportData extends QueryFilter{

    private String batchId;
    private String individualSim;
    private String carriers;
    private String products;
    private String simInfo;
    
    private String status;
    private String newCarrier;
    private String newProduct;
    private Boolean expired;
    private String startDate;
    private String endDate;
    private String startExpiredMoment;
    private String endExpiredMoment;
    
    public QueryReportData() {
    }

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the individualSim
     */
    public String getIndividualSim() {
        return individualSim;
    }

    /**
     * @param individualSim the individualSim to set
     */
    public void setIndividualSim(String individualSim) {
        this.individualSim = individualSim;
    }

    /**
     * @return the carriers
     */
    public String getCarriers() {
        return carriers;
    }

    /**
     * @param carriers the carriers to set
     */
    public void setCarriers(String carriers) {
        this.carriers = carriers;
    }

    /**
     * @return the products
     */
    public String getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(String products) {
        this.products = products;
    }

    /**
     * @return the simInfo
     */
    public String getSimInfo() {
        return simInfo;
    }

    /**
     * @param simInfo the simInfo to set
     */
    public void setSimInfo(String simInfo) {
        this.simInfo = simInfo;
    }

    /**
     * @return the newCarrier
     */
    public String getNewCarrier() {
        return newCarrier;
    }

    /**
     * @param newCarrier the newCarrier to set
     */
    public void setNewCarrier(String newCarrier) {
        this.newCarrier = newCarrier;
    }

    /**
     * @return the newProduct
     */
    public String getNewProduct() {
        return newProduct;
    }

    /**
     * @param newProduct the newProduct to set
     */
    public void setNewProduct(String newProduct) {
        this.newProduct = newProduct;
    }

    /**
     * @return the expired
     */
    public Boolean getExpired() {
        return expired;
    }

    /**
     * @param expired the expired to set
     */
    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the startExpiredMoment
     */
    public String getStartExpiredMoment() {
        return startExpiredMoment;
    }

    /**
     * @param startExpiredMoment the startExpiredMoment to set
     */
    public void setStartExpiredMoment(String startExpiredMoment) {
        this.startExpiredMoment = startExpiredMoment;
    }

    /**
     * @return the endExpiredMoment
     */
    public String getEndExpiredMoment() {
        return endExpiredMoment;
    }

    /**
     * @param endExpiredMoment the endExpiredMoment to set
     */
    public void setEndExpiredMoment(String endExpiredMoment) {
        this.endExpiredMoment = endExpiredMoment;
    }

}
