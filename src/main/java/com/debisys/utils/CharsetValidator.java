package com.debisys.utils;

import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CodingErrorAction;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.debisys.languages.Languages;
import com.debisys.properties.Properties;
import com.emida.utils.java.ReflectUtil;

/**
 * Utility class that implements character set validation for input strings
 * 
 * @author Ricardo Buitrago (rbuitrago@emida.net)
 */
public class CharsetValidator {
    
    private static Logger log = Logger.getLogger(CharsetValidator.class);

    private static final String PREFIX = "charset";

    private static Charset charset;
    private static String validationErrorMsg;
    private static HashMap<String, CodingErrorAction> actions;
    private static String malformedInputConfig;
    private static String unmappableCharacterConfig;
    private static Boolean allowAcceptable;
    private static Boolean validationEnabled = true;
    
    private static CharsetEncoder ce;

    private static String[] acceptable = new String[] { "\u00D1", "\u00F1",
	    "\u00E1", "\u00C1", "\u00E9", "\u00C9", "\u00ED", "\u00CD",
	    "\u00F3", "\u00D3", "\u00FA", "\u00DA", "\u00DC", "\u00FC" };
    

    /**
     * Inits properties. Some are always read, others are read at first.
     * @param context
     */
    private static void setup(ServletContext context) {
	String instance = DebisysConfigListener.getInstance(context);
	// init actions
	actions = new HashMap<String, CodingErrorAction>();
	actions.put("REPORT", CodingErrorAction.REPORT);
	actions.put("REPLACE", CodingErrorAction.REPLACE);
	actions.put("IGNORE", CodingErrorAction.IGNORE);
	
	// init validation enabled
	// Turns on/off charset validation. Default is true (added after parature ticket 8749218)
	String propertyValue = Properties.getPropertyValue(instance,
		"validation", PREFIX + ".enabled");
	if (propertyValue != null)
	    validationEnabled = new Boolean(propertyValue);

	// init checkAcceptable
	//If set to true, all "otherAcceptable" and unmappable chars found in the String 
	//will be removed. Other values or even blank will cause all offending chars to be 
	//replaced with '?' when the CharsetEncoder executes. Default value False.
	propertyValue = Properties.getPropertyValue(instance, "validation",
		PREFIX + ".allowAcceptable");
	if (propertyValue != null)
	    allowAcceptable = new Boolean(propertyValue);
	
	// init acceptable excepted characters
	// Specifies characters outside selected charset, that will be accepted 
	// (excluded from validation). Notation is a comma separated list of unicode values. 
	// Default is all spanish characters excluded from US-ASCII.
	propertyValue = Properties.getPropertyValue(instance, "validation",
		PREFIX + ".otherAcceptable");
	if (propertyValue != null) {
	    String[] others = propertyValue.split("\\,");
	    for (int i = 0; i < others.length; i++) {
		others[i] = others[i].trim();  
	    }
	    acceptable = others;
	}
	
	
	if (validationErrorMsg == null) {
	    // init validation error message
	    // Maps to an entry in language property files.
	    validationErrorMsg = Properties.getPropertyValue(instance,
		    "validation", PREFIX + ".encodingErrorMessage");
	}
	
	
	if (malformedInputConfig == null) {
	    //A standard action to be taken if the malformedInput event arises. It can be either 
	    //REPORT, REPLACE or IGNORE. Default value REPORT. 
	    malformedInputConfig = Properties.getPropertyValue(instance,
		    "validation", PREFIX + ".malformedInputAction");
	}
	
	
	if (unmappableCharacterConfig == null) {
	    //A standard action to be taken if the unmappableCharacter event arises. It can be either 
	    //REPORT, REPLACE or IGNORE. Default value REPORT.	
	    unmappableCharacterConfig = Properties.getPropertyValue(instance,
		    "validation", PREFIX + ".unmappableCharacterAction");
	}
	
	if (charset == null) {
	    //init charset
	    //Specifies the charset to be used, as shown in 
	    //http://www.iana.org/assignments/character-sets. Default value ASCII.
	    String charsetname = Properties.getPropertyValue(instance,
		    "validation", PREFIX);
	    try {
		charset = Charset.forName(charsetname);
	    } catch (RuntimeException e) {
		log.warn("[setup] wrong character set name: " 
			+ charsetname + ". Defaulting to ASCII");
		charset = Charset.forName("ASCII");
	    }
	}
	
	if (ce == null) {
	    //init charset encoder
	    ce = configEncoder();
	}
    }

    private static CharsetEncoder configEncoder() {
        CharsetEncoder cc = null;
        try {
            cc = charset.newEncoder();
        } catch (Exception e) { }
    
        if (cc != null) {
    
            if (malformedInputConfig != null
        	    && actions.containsKey(malformedInputConfig))
        	cc.onMalformedInput(actions.get(malformedInputConfig));
    
            if (unmappableCharacterConfig != null
        	    && actions.containsKey(unmappableCharacterConfig))
        	cc.onUnmappableCharacter(actions.get(unmappableCharacterConfig));
        } else {
            cc = charset.newEncoder().onMalformedInput(
        	    CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT);
        }
    
        return cc;
    }

    /**
     * @param word The word to remove the excepted characters from
     * @return the word after stripping all allowed exception in charset
     */
    private static String exceptCharacters(String word) {
		StringBuilder bul = new StringBuilder(word);	
		if (allowAcceptable) {
		    for (int i = 0; i < acceptable.length; i++) {
				String s = acceptable[i];
				if (bul.indexOf(s) != -1) {
				    log.warn("[exceptCharacters] \'" + s + "\' is being excepted and may not " +
				    	"be properly encoded using " + charset.displayName() + " character set");
				    bul.deleteCharAt(bul.indexOf(s));
				}
		    }
		}
		return bul.toString();
    }

    private static String exceptionText(Exception x, String input) {
	String var = "";
	if (validationErrorMsg != null) {
	    Object[] args = new Object[] { input, x.getClass().getName(),
		    x.getMessage() };
	    var = Languages.getString(validationErrorMsg, args,"0");
	} else {
	    var = "Invalid input: " + input + " caused error: "
		    + x.getClass().getName() + " (" + x.getMessage() + ")";
	}
	return var;
    }
    
    /**
     * Validates all String field attributes for an Object, checking compliance
     * to a given encoding. Useful for validation of user input in web forms;
     * disallows non-compliant characters when registering valuable data.
     * 
     * @param <T>
     *                Object Type - POJO with standard JavaBean accessors for
     *                private fields.
     * @param o
     *                An instance of such type
     * 
     * @return A Map containing field names and the error when trying to encode
     *         its contents, or a null reference if no offending character is
     *         found in the field attribute value. Or also will return null if
     *         validationEnabled is set to False (default is true)
     */    
    public static <T extends Object> Map<String, String> validateStringFields(T o, ServletContext context) {
	return validateStringFields(o, context, null);
    }

    /**
     * Validates only listed attributes for an Object, checking compliance
     * to a given encoding. Useful for validation of user input in web forms;
     * disallows non-compliant characters when registering valuable data.
     * 
     * @param <T>
     *                Object Type - POJO with standard JavaBean accessors for
     *                private fields.
     * @param o
     *                An instance of such type
     * @param fields
     *                A list of field names in the object to validate.
     * @return A Map containing field names and the error when trying to encode
     *         its contents, or a null reference if no offending character is
     *         found in the field attribute value. Or also will return null if
     *         validationEnabled is set to False (default is true)
     */    
    public static <T extends Object> Map<String, String> validateStringFields(
	    T o, ServletContext context, String ... fields) {

		// switching properties to DB
		setup(context);
	
		// ticket 8749218 reminded me of adding a mechanism for disabling the whole thing
		if (!validationEnabled) {
		    return null;
		}
		HashMap<String, String> invalidFields = new HashMap<String, String>();
	
		java.lang.reflect.Method[] methods = o.getClass().getDeclaredMethods();
		Hashtable<String, String> data = ReflectUtil.extractAllStringData(o, fields); 
		for (String field : data.keySet()) {
		    String val = data.get(field);
		    try {
				if (!attemptEncoding(val, false)) {
				    if (!attemptEncoding(exceptCharacters(val), true)) {
					log.error("[validateStringFields] cannot encode " + val
						+ " using " + charset.displayName()
						+ " character set!");
					invalidFields.put(field, val);
				    }
				}
		    } catch (Exception c) {
				String exceptionText = exceptionText(c, val);
				log.error("[validateStringFields] cannot encode " + val
					+ " using " + charset.displayName()
					+ " character set: " + exceptionText);
				invalidFields.put(field, exceptionText);
		    }
	
		    
		}
		return invalidFields.isEmpty() ? null : invalidFields;
    }    
    
    private static boolean attemptEncoding(String val, boolean rethrow) throws CharacterCodingException {
	CharBuffer cbuf = CharBuffer.wrap(val.toCharArray());
	String encoded = null;
	try {
	    encoded = new String(ce.encode(cbuf).array());
	} catch (CharacterCodingException e) { if (rethrow) throw e; }
	return encoded != null;
    }
    
    public static String getAcceptableCharsList(ServletContext context) {
	setup(context);
	String asString = "";
	if (validationEnabled && allowAcceptable) {
	    StringBuilder sb = new StringBuilder();
	    for (String a : acceptable) {
		sb.append(a).append(" ");
	    }
	    asString = sb.toString();
	}
	return asString;
    }
    
    public static void main (String ... args) {
//	try {
//	    String word = "Mig\u00FCel";
//	    System.out.println("Encoding " + word);
//		charset = Charset.forName("ISO-8859-1");
//		ce = configEncoder();
//		ce.encode(CharBuffer.wrap(word));
//	    } catch (Exception e) {
//		//charset = Charset.forName("ASCII");
//		System.out.println("No se pudo " + e.getMessage());
//		e.printStackTrace();
//	    } finally {
//		System.out.println(charset.toString());	
//	    }
//	    getAcceptableCharsList(null);
    }
}
