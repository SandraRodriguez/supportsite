package com.debisys.utils;

import java.io.Serializable;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;

import com.debisys.exceptions.TransactionException;
import com.debisys.transactions.TransactionSearch;
import com.debisys.users.SessionData;


public class TaskBean implements Runnable, Serializable {
	  //log4j logging
	static Category cat = Category.getInstance(TaskBean.class);
    private int counter;
    private int sum;
    private boolean started;
    private boolean running;
    private int sleep;
    private boolean mexico;
    private SessionData sessionData;
    private int intSectionPage;
    private int max=-1;
    private ServletContext application;
    private TransactionSearch se;
    private String url;
    
    

    public TaskBean() {
    	cat.debug("TaskBean init()");
        counter = 0;
        sum = 0;
        started = false;
        running = false;
        sleep = 100;
    }
    
    protected void work() throws TransactionException, InterruptedException {
    	cat.debug("work init...");
    	
    	if(this.max!=-1)
    		se.setmax(this.max);
    	if(this.mexico)
    		url = se.downloadMx(this.sessionData, this.intSectionPage, this.application);
    	else
    		url = se.download(this.sessionData, this.intSectionPage, this.application);
    }
    
    public int getPercent() {
    	cat.debug("getpercent " + se.icount);
        return se.icount;
    }
    public void setrecordcount(int count) {
    	sum = count;
    }

    public void setmax(int count) {
    	this.max = count;
    }
 	
    public int getsum() {
    	return sum;
    }
    public String getURL() {
    	cat.debug("getURL " + this.url);
        return this.url;
    }

    public boolean isStarted() {
    	cat.debug("TaskBean started");
        return started;
    }

    public boolean isCompleted() {
        return se.isdone();
    }

    public boolean isRunning() {
    	cat.debug("TaskBean is running");
        return running;
    }

    public void setRunning(boolean running) {
    	cat.debug("TaskBean setRunning" + running);
        this.running = running;
        if (running)
            started = true;
    }
    public void setvar(boolean bmexico,SessionData ssessionData,int iintSectionPage, ServletContext context, TransactionSearch t) {
        this.mexico = bmexico;
        this.sessionData = ssessionData;
        this.intSectionPage = iintSectionPage;
        this.application = context;
        this.se = t;
        
    }
    
    public void run() {
    	cat.debug("TaskBean run() ");
        try {
            setRunning(true);
            while (isRunning() && !isCompleted())
                work();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransactionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			cat.debug("Exception Task Bean");
			e.printStackTrace();
		} finally {
            setRunning(false);
        }
    }

}
