package com.debisys.utils;

import java.text.DecimalFormat;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.debisys.currency.Currency;
import java.text.DecimalFormatSymbols;
import java.util.UUID;

public class NumberUtil
{
    private static Logger log = Logger.getLogger(NumberUtil.class);
    private static ResourceBundle sql_bundle = ResourceBundle
	    .getBundle("com.debisys.utils.utils");

    /**
     * Rounds a small (decimal) rate to a zero value given a parametrized
     * allowance (decimal zeroes).
     * 
     * @param rate
     *                The rate to be checked for rounding
     * @param allowance
     *                positive integer that represents the number of allowed
     *                decimal zeroes for a rate, before considering it a decimal
     *                zero.
     * @return If the absolute rate value is a small decimal so that the
     *         operation
     * 
     * <pre>
     * 	rate * 1E(allowance) &gt; 1
     * </pre>
     * 
     * evaluates to <code>false</code>, the method returns a decimal zero.
     * Otherwise, the method will return the rate as is.
     */
    public static double roundRate(double rate, int allowance) {
	// FIXME comment out sysout lines
	// parature tkt 9949440 forcing some rates rounding
	double multiplier = new Double("1E" + allowance);
	double temp = Math.abs(rate);
	double result = 0D;
	String msg = "[roundRate] for " + temp + " using " + allowance
		+ " decimal zeroes for approximation ..";
	//System.out.println(msg);
	//log.debug(msg);	
	if ((temp * multiplier) >= 1) {
	    result = rate;
	}
	msg = "[roundRate] returning " + result;
	//System.out.println(msg);
	//log.debug(msg);
	return result;
    }
  
  public static String formatCurrency(String strAmount) {
	  return formatCurrency(strAmount, false);
  }
  
  public static String formatCurrency(String strAmount, boolean useParentheses) {
	String strCurrency = "";
	boolean negativeAmt = false;
	if(strAmount == null) strAmount = "0";
	if(strAmount.equals("")) strAmount = "0";
	double damt = new Double(strAmount);
	if (negativeAmt = damt < 0 && useParentheses) {
		damt *= -1;
		strAmount = Double.toString(damt);
	}
    if (strAmount != null && !strAmount.equals("")) {
      try {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        strCurrency = df.format(Double.parseDouble(strAmount));
      } catch (NumberFormatException nfe) {
        strCurrency = "0.00";
      }
    } else {
      strCurrency = "0.00";
    }

    // LOCALIZATION
    // Format the currency to be displayed 
	String formattedAmt = strCurrency;
    
    if (Currency.getCurrencyFormat().equals(DebisysConstants.CURRENCY_SYMBOL_FORMAT)) {
    	formattedAmt = Currency.getCurrencySymbol() + formattedAmt;
    } else if (Currency.getCurrencyFormat().equals(DebisysConstants.CURRENCY_ISO_FORMAT)) {
    	formattedAmt = Currency.getCurrencyCountryCode() + " " + formattedAmt;
    } else {
    	// For now, default to symbol format if incorrect currency format is specified.
    	formattedAmt = Currency.getCurrencySymbol() + formattedAmt;
    }
    
    if (negativeAmt && useParentheses) {
    	formattedAmt = "(" + formattedAmt + ")";
    }
    return formattedAmt;
    // END LOCALIZATION    

    // String strCurrency = "";
    // String strCurrencySymbol = sql_bundle.getString("currencySymbol");
    // if (strAmount != null && !strAmount.equals(""))
    // {
    // try
    // {
    // DecimalFormat df = new DecimalFormat("#,##0.00");
    // strCurrency = df.format(Double.parseDouble(strAmount));
    // }
    // catch(NumberFormatException nfe)
    // {
    // strCurrency = "0.00";
    // }
    // }
    // else
    // {
    // strCurrency = "0.00";
    // }
    // return strCurrencySymbol + strCurrency;
  }
  
  public static String formatPercentage(String strPercentile) {	  
	  String formattedPctg = "0.00%";
	  try {
		  formattedPctg = formatAmount(Double.toString(new Double(strPercentile) * 100)) + "%";
	  }	catch (Exception e)	{ } //NumberFormatException, NullPointer, etc.
	  return formattedPctg;
  }

  public static String formatAmount(String strAmount)
  {
    String strCurrency = "";
    if (strAmount != null && !strAmount.equals(""))
    {
      try
      {
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("###0.00",simbolos);
        strCurrency = df.format(Double.parseDouble(strAmount));
      } catch (NumberFormatException nfe)
      {
        strCurrency = "0.00";
      }
    } else
    {
      strCurrency = "0.00";
    }
    return strCurrency;
  }

  public static String formatExchangeRate(String strAmount)
  {
    String strCurrency = "";
    if (strAmount != null && !strAmount.equals(""))
    {
      try
      {
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("###0.00000000", simbolos);
        strCurrency = df.format(Double.parseDouble(strAmount));
      } catch (NumberFormatException nfe)
      {
        strCurrency = "0.0000";
      }
    } else
    {
      strCurrency = "0.0000";
    }
    return strCurrency;
  }
  
  public static String getCurrencySymbol()
  {
    String strCurrencySymbol = "";
    try
    {
      strCurrencySymbol = NumberUtil.sql_bundle.getString("currencySymbol");
    } catch (NumberFormatException nfe)
    {
      strCurrencySymbol = "$";
    }
    return strCurrencySymbol;
  }

  public static boolean isNumeric(String strNumber)
  {
    try
    {
      Double.parseDouble(strNumber);
      return true;
    } catch (NumberFormatException nfe)
    {
      return false;
    } catch (NullPointerException npe)
    {
      return false;
    }
  }
  
    public static String getUniqueIdentifier() {
        return UUID.randomUUID().toString();
    }
  
}