package com.debisys.utils;

/**
 * @author nmartinez
 *
 */
public class Base {

	private String id;
	private String description;
	
	
	public Base(){}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
