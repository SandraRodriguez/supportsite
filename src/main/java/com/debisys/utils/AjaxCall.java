package com.debisys.utils;

import java.lang.reflect.Method;
import com.debisys.users.SessionData;

public class AjaxCall {

	public static JSONObject callJavaClass(SessionData sessionData, String clazz, String method, String value) {
		try {
			Class c = Class.forName(clazz);
			Class[] parameterTypes = new Class[] { String.class, SessionData.class };
			Method m = c.getMethod(method, parameterTypes);
			Object obj = c.newInstance();
			
			JSONObject json = (JSONObject) m.invoke(obj, value, sessionData);
			return json;
		} catch (Exception  e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
