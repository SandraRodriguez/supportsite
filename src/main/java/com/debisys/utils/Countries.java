package com.debisys.utils;

import java.util.ResourceBundle;
import org.apache.log4j.Category;

public class Countries
{

    private Integer CountryId;
    private String countryCode;
    private String CountryName;
    private String external_id;
    private String internationalCode;
    private String internationalDialCode;
    private Integer ImageId;
    private String IsoAlpha3Code;
    private String IdNew;
        
    static Category cat = Category.getInstance(Countries.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
    /**
     * @return the CountryId
     */
    public Integer getCountryId() {
        return CountryId;
    }

    /**
     * @param CountryId the CountryId to set
     */
    public void setCountryId(Integer CountryId) {
        this.CountryId = CountryId;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the CountryName
     */
    public String getCountryName() {
        return CountryName;
    }

    /**
     * @param CountryName the CountryName to set
     */
    public void setCountryName(String CountryName) {
        this.CountryName = CountryName;
    }

    /**
     * @return the external_id
     */
    public String getExternal_id() {
        return external_id;
    }

    /**
     * @param external_id the external_id to set
     */
    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    /**
     * @return the internationalCode
     */
    public String getInternationalCode() {
        return internationalCode;
    }

    /**
     * @param internationalCode the internationalCode to set
     */
    public void setInternationalCode(String internationalCode) {
        this.internationalCode = internationalCode;
    }

    /**
     * @return the internationalDialCode
     */
    public String getInternationalDialCode() {
        return internationalDialCode;
    }

    /**
     * @param internationalDialCode the internationalDialCode to set
     */
    public void setInternationalDialCode(String internationalDialCode) {
        this.internationalDialCode = internationalDialCode;
    }

    /**
     * @return the ImageId
     */
    public Integer getImageId() {
        return ImageId;
    }

    /**
     * @param ImageId the ImageId to set
     */
    public void setImageId(Integer ImageId) {
        this.ImageId = ImageId;
    }

    /**
     * @return the IsoAlpha3Code
     */
    public String getIsoAlpha3Code() {
        return IsoAlpha3Code;
    }

    /**
     * @param IsoAlpha3Code the IsoAlpha3Code to set
     */
    public void setIsoAlpha3Code(String IsoAlpha3Code) {
        this.IsoAlpha3Code = IsoAlpha3Code;
    }

    /**
     * @return the IdNew
     */
    public String getIdNew() {
        return IdNew;
    }

    /**
     * @param IdNew the IdNew to set
     */
    public void setIdNew(String IdNew) {
        this.IdNew = IdNew;
    }



}
