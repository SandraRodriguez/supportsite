package com.debisys.brandings;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.debisys.exceptions.BrandingException;
import com.debisys.exceptions.RatePlanException;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

public class Branding implements HttpSessionBindingListener {
	
	private static Logger cat = Logger.getLogger(Branding.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.brandings.sql");
	
	private String customConfigId = "";
	
	private String Name;
	
	private String URL;
	
	private String SecureURL;

	public void valueBound(HttpSessionBindingEvent event) {

	}

	public void valueUnbound(HttpSessionBindingEvent event) {

	}

	public String getCustomConfigId() {
		return this.customConfigId;
	}

	public void setCustomConfigId(String customConfigId) {
		this.customConfigId = customConfigId;
	}

	/**
	 * Gets list of reps only for an iso in the next sublevel.
	 * 
	 * @@param sessionData
	 * @@return A vector of results, or null if none found.
	 * @@throws BrandingException Thrown on database errors
	 */

	public Vector<Vector<String>> getIsoReps(SessionData sessionData, String strRepType) throws BrandingException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<String>> vecreps = new Vector<Vector<String>>();
		if (this.customConfigId.length() > 0) {
			try {
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null) {
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}
				this.customConfigId = StringUtil.escape(this.customConfigId);
				String strRefId = sessionData.getProperty("ref_id");
				String strSQL = "SELECT r.businessname, r.rep_id, CASE WHEN b.iso_id IS NULL THEN 0 ELSE 1 END AS checked FROM reps AS r(NOLOCK) LEFT JOIN ISO_PCTerminalBrands AS b(NOLOCK) ON (r.rep_id = b.iso_id AND b.CustomConfigID = '"
				        + this.customConfigId + "') WHERE ";
				String strSQLWhere = "";
				String strAccessLevel = sessionData.getProperty("access_level");
				String strDistChainType = sessionData.getProperty("dist_chain_type");

				strRefId = StringUtil.escape(strRefId);
				// login level
				if (strAccessLevel.equals(DebisysConstants.ISO)) {
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
						if (strRepType.equals(DebisysConstants.AGENT)) {
							strSQLWhere += " type=" + DebisysConstants.REP_TYPE_AGENT + " AND r.iso_id=" + strRefId + " AND r.rep_id <> " + strRefId;
						} else if (strRepType.equals(DebisysConstants.SUBAGENT)) {
							strSQLWhere += " type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND r.iso_id " + "IN (SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_AGENT + " AND iso_id=" + strRefId + ")";
						} else if (strRepType.equals(DebisysConstants.REP)) {
							strSQLWhere += " type=" + DebisysConstants.REP_TYPE_REP + " and "; // subagents
																							   // &
																							   // agents
							strSQLWhere += " (r.iso_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_AGENT
							        + " and iso_id=" + strRefId + "))";
							strSQLWhere = strSQLWhere + ")";
						} else if (strRepType.equals(DebisysConstants.ISO)) {
							strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL + " AND r.iso_id=" + strRefId + " AND r.rep_id = " + strRefId;
						}
					} else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
						strSQLWhere = strSQLWhere + " r.iso_id=" + strRefId + " AND r.rep_id <> " + strRefId;
					}
				} else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
					if (strRepType.equals(DebisysConstants.SUBAGENT)) {
						strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id=" + strRefId;
					} else if (strRepType.equals(DebisysConstants.REP)) {
						strSQLWhere += " type=" + DebisysConstants.REP_TYPE_REP + " AND ";// subagents
						strSQLWhere += " (r.iso_id IN (SELECT rep_id FROM reps WITH(nolock) WHERE type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id= " + strRefId + ")";

						strSQLWhere = strSQLWhere + ")";
					}
				} else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
					if (strRepType.equals(DebisysConstants.REP)) {
						strSQLWhere += " type=" + DebisysConstants.REP_TYPE_REP + " AND " + " iso_id=" + strRefId;
					}
				}
				cat.debug(strSQL + strSQLWhere);
				pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by r.businessname");
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("businessname")));
					vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
					vecTemp.add(StringUtil.toString(rs.getString("checked")));
					vecreps.add(vecTemp);
				}
			} catch (Exception e) {
				cat.error("Error during getIsoReps", e);
				throw new BrandingException();
			} finally {
				try {
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				} catch (Exception e) {
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return vecreps;
	}

	/**
	 * @param sessionData
	 * @return Vector list of brandings allowed in the glue table for the
	 *         current rep or iso
	 * @throws BrandingException
	 */
	public Vector<Vector<String>> getBrandings(SessionData sessionData) throws BrandingException {
		Connection dbConn = null;
		Vector<Vector<String>> vecBrandings = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("getBrandings");
			pstmt = dbConn.prepareStatement(strSQL);
			cat.debug(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("CustomConfigId")));
				vecTemp.add(StringUtil.toString(rs.getString("Name")));
				vecBrandings.add(vecTemp);
			}
		} catch (Exception e) {
			cat.error("Error during getBrandings", e);
			throw new BrandingException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecBrandings;
	}

	/**
	 * @param sessionData
	 * @return Vector list of brandings allowed in the glue table for the
	 *         current rep or iso
	 * @throws BrandingException
	 */
	public Vector<Vector<String>> getISOBrandings(SessionData sessionData) throws BrandingException {
		Connection dbConn = null;
		Vector<Vector<String>> vecBrandings = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("getISOBrandings");
			String strRefId = sessionData.getProperty("ref_id");
			/*
			 * if (strAccessLevel.equals(DebisysConstants.ISO)) {
			 */
			pstmt = dbConn.prepareStatement(strSQL);
			cat.debug(strSQL + "/* " + strRefId + " */ ");
			pstmt.setString(1, strRefId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("CustomConfigId")));
				vecTemp.add(StringUtil.toString(rs.getString("Name")));
				vecBrandings.add(vecTemp);
			}
		} catch (Exception e) {
			cat.error("Error during getISOBrandings", e);
			throw new BrandingException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecBrandings;
	}

	/**
	 * Assign or unassign a brandings to a rep depending on the checked flag
	 * 
	 * @@param customConfigIdP branding ID
	 * @@param rep_id rep ID
	 * @@param checked flag to indicate if assign or unassign
	 * @@return a response string
	 */
	public String assignBranding(SessionData sessionData, String reps, String level) throws BrandingException {
		Connection dbConn = null;
		String str = "";
		PreparedStatement pstmt = null;
		if (this.customConfigId != null && !this.customConfigId.equals("")) {
			this.setCustomConfigId(String.valueOf(this.customConfigId));
			try {
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null) {
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}
				String strDelGlue = "";
				String strInsGlue = "";
				Vector<Vector<String>> vecReps = this.getIsoReps(sessionData, level);
				if (vecReps != null && vecReps.size() > 0) {
					Iterator<Vector<String>> it = vecReps.iterator();
					while (it.hasNext()) {
						Vector<String> vecTemp = it.next();
						String rep_id = vecTemp.get(1);
						if (rep_id.length() > 0) {
							if (reps.contains("|" + rep_id + "|")) {
								if (strDelGlue.length() > 0) {
									strDelGlue += ",";
								}
								strDelGlue += rep_id;
								if (strInsGlue.length() > 0) {
									strInsGlue += " UNION ";
								}
								strInsGlue += "SELECT '" + this.customConfigId.replaceAll("%20", " ") + "' AS customConfigId, " + rep_id + " AS rep_id";
							} else {
								cat.debug("EXEC deleteIsoPcterminalBrand " + rep_id + ", '" + this.customConfigId.replaceAll("%20", " ") + "'");
								pstmt = dbConn.prepareStatement("EXEC deleteIsoPcterminalBrand ?, ?");
								pstmt.setLong(1, Long.parseLong(rep_id));
								pstmt.setString(2, this.customConfigId.replaceAll("%20", " "));
								pstmt.execute();
							}
						}
					}
					if (strInsGlue.length() > 0) {
						strDelGlue = "DELETE FROM ISO_PCTerminalBrands WHERE  CustomConfigID = '" + this.customConfigId.replaceAll("%20", " ") + "' AND iso_id IN (" + strDelGlue + ")";
					}
					if (strInsGlue.length() > 0) {
						strInsGlue = "INSERT INTO ISO_PCTerminalBrands (CustomConfigID, iso_id) " + strInsGlue;
					}
				}
				if (strDelGlue.length() > 0) {
					// DELETE GLUES FOR REPS
					cat.debug(strDelGlue);
					pstmt = dbConn.prepareStatement(strDelGlue);
					pstmt.executeUpdate();
				}
				if (strInsGlue.length() > 0) {
					// INSERT GLUES FOR REPS
					cat.debug(strInsGlue);
					pstmt = dbConn.prepareStatement(strInsGlue);
					pstmt.executeUpdate();
				}
				str = "OK";
			} catch (Exception e) {
				cat.error("Error during assignBranding", e);
				throw new BrandingException();
			} finally {
				try {
					TorqueHelper.closeConnection(dbConn, pstmt, null);
				} catch (Exception e) {
					cat.error("Error during assignBranding", e);
				}
			}
		}
		return str;
	}

	/**
	 * @param sessionData
	 * @return Vector list of all Isos with a flag turned on for the assigned Isos to the given branding
	 * @throws BrandingException
	 */
	public Vector<Vector<String>> getAllISOs(SessionData sessionData) throws BrandingException {
		Connection dbConn = null;
		Vector<Vector<String>> vecBrandings = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("getAllISOsForBranding");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, this.customConfigId);
			cat.debug(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add("" + rs.getBoolean("assigned"));
				vecBrandings.add(vecTemp);
			}
		} catch (Exception e) {
			cat.error("Error during getAllISOs", e);
			throw new BrandingException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecBrandings;
	}

	public boolean saveProperty(String name, String value) throws BrandingException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		boolean ok = false;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = "EXEC sp_setBrandingProperty ?, ?, ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, this.customConfigId);
			pstmt.setString(2, name);
			pstmt.setString(3, (value != null ? value : ""));
			cat.debug("Saving branding property: branding=" + this.customConfigId + ", name=" + name + ", value= " + value);
			pstmt.execute();
			ok = true;
		} catch (Exception e) {
			cat.error("Error during saveProperty", e);
			throw new BrandingException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return ok;
	}
	
	public String getProperty(String name) throws BrandingException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String value = "";
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = "SELECT Value FROM  TerminalBranding_Properties WHERE TerminalBrandingId = ? AND Name = ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, this.customConfigId);
			pstmt.setString(2, name);
			rs = pstmt.executeQuery();
			if(rs.next()){
				value = "" + rs.getString(1);
			}
			cat.debug("Getting branding property: branding=" + this.customConfigId + ", name=" + name + ", value= " + value);
		} catch (Exception e) {
			cat.error("Error during getProperty", e);
			throw new BrandingException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return value;
	}

	public boolean saveAssignment(String iso_id, boolean assign) throws BrandingException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		boolean ok = false;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = "EXEC sp_assignBrandingToIso ?, ?, ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, this.customConfigId);
			pstmt.setString(2, iso_id);
			pstmt.setShort(3, (short) (assign?1:0));
			cat.debug((assign?"Assigning": "Unassigning") +" branding to iso: branding=" + this.customConfigId + ", iso_id=" + iso_id);
			pstmt.execute();
			ok = true;
		} catch (Exception e) {
			cat.error("Error during saveAssignment", e);
			throw new BrandingException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return ok;
	}

	public boolean save() throws BrandingException {
		boolean ok = false;
		if(this.customConfigId != null && this.customConfigId.length()>0){
			if( this.Name != null && this.Name.length() > 0){
				Connection dbConn = null;
				PreparedStatement pstmt = null;
				try {
					dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
					if (dbConn == null) {
						cat.error("Can't get database connection");
						throw new RatePlanException();
					}
					String strSQL = "EXEC sp_saveBranding ?, ?, ?, ?";
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.setString(1, this.customConfigId);
					pstmt.setString(2, this.Name);
					pstmt.setString(3, this.URL);
					pstmt.setString(4, this.SecureURL);
					cat.debug("Saving branding: branding=" + this.customConfigId + ", name=" + this.Name + ", url=" + this.URL + ", name=" + this.SecureURL);
					pstmt.execute();
					ok = true;
				} catch (Exception e) {
					cat.error("Error during saveBranding", e);
					throw new BrandingException();
				} finally {
					try {
						TorqueHelper.closeConnection(dbConn, pstmt, null);
					} catch (Exception e) {
						cat.error("Error during closeConnection", e);
					}
				}
			}else{
				cat.error("Error: Name can't be null or empty");
			}
		}else{
			cat.error("Error: CustomconfigId can't be null or empty");
		}
		return ok;
	}

	public boolean existsBrading(String name){
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		long c = 0;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = "SELECT COUNT(*) AS C FROM Terminal_Branding WHERE CustomConfigId = ? OR Name = ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, name);
			pstmt.setString(2, name);
			rs = pstmt.executeQuery();
			if(rs.next()){
				c  = rs.getLong("C");
			}
		} catch (Exception e) {
			cat.error("Error during Branding.existsName", e);
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return c > 0;
	}
	
	public void load() throws BrandingException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = "SELECT Name, URL, SecureURL FROM  Terminal_Branding WHERE CustomConfigID = ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, this.customConfigId);
			rs = pstmt.executeQuery();
			if(rs.next()){
				this.setName(rs.getString("Name"));
				this.setURL(rs.getString("URL"));
				this.setSecureURL(rs.getString("SecureURL"));
			}
			cat.debug("Getting branding values: branding=" + this.customConfigId );
		} catch (Exception e) {
			cat.error("Error during Branding.load", e);
			throw new BrandingException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
	}

	/**
     * @param name the name to set
     */
    public void setName(String name) {
	    this.Name = name;
    }

	/**
     * @return the name
     */
    public String getName() {
	    return this.Name;
    }

	/**
     * @param uRL the uRL to set
     */
    public void setURL(String uRL) {
	    this.URL = uRL;
    }

	/**
     * @return the uRL
     */
    public String getURL() {
	    return this.URL;
    }

	/**
     * @param secureURL the secureURL to set
     */
    public void setSecureURL(String secureURL) {
	    this.SecureURL = secureURL;
    }

	/**
     * @return the secureURL
     */
    public String getSecureURL() {
	    return this.SecureURL;
    }
}
