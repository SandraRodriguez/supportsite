package com.debisys.rateplans;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.debisys.exceptions.RatePlanException;
import com.debisys.exceptions.TerminalException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.CSystemMethods;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.Log;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * Holds the information for products.
 * <P>
 * 
 * @author Jay Chi
 */

public class RatePlan implements HttpSessionBindingListener
{

	// log4j logging
	// static Category cat = Category.getInstance(RatePlan.class);
	private static Logger cat = Logger.getLogger(RatePlan.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.rateplans.sql");
	private String ratePlanId = "";
	private String repRatePlanId = "";
	private String ratePlanName = "";
	private String repId = "";
	private String[] productId;
	private String[] repIds;

	private Hashtable<String, String> validationErrors = new Hashtable<String, String>();

	/**
	 * @return ratePlanId
	 */
	public String getRatePlanId()
	{
		return this.ratePlanId;
	}

	/**
	 * @param ratePlanId
	 */
	public void setRatePlanId(String ratePlanId)
	{
		this.ratePlanId = ratePlanId;
	}

	/**
	 * @return repRatePlanId
	 */
	public String getRepRatePlanId()
	{
		return StringUtil.toString(this.repRatePlanId);
	}

	/**
	 * @param repRatePlanId
	 */
	public void setRepRatePlanId(String repRatePlanId)
	{
		this.repRatePlanId = repRatePlanId;
	}

	/**
	 * @return ratePlanName
	 */
	public String getRatePlanName()
	{
		return this.ratePlanName;
	}

	/**
	 * @param ratePlanName
	 */
	public void setRatePlanName(String ratePlanName)
	{
		this.ratePlanName = ratePlanName;
	}

	/**
	 * @return productId
	 */
	public String[] getProductId()
	{
		return this.productId;
	}

	/**
	 * @param productId
	 */
	public void setProductId(String[] productId)
	{
		this.productId = productId;
	}

	/**
	 * @return repIds
	 */
	public String[] getRepIds()
	{
		return this.repIds;
	}

	/**
	 * @param repIds
	 */
	public void setRepIds(String[] repIds)
	{
		this.repIds = repIds;
	}

	/**
	 * @return repId
	 */
	public String getRepId()
	{
		return StringUtil.toString(this.repId);
	}

	/**
	 * @param repId
	 */
	public void setRepId(String repId)
	{
		this.repId = repId;
	}

	/**
	 * @param line
	 */
	public static void WriteDebug(String line)
	{
		cat.debug(line);
	}

	/**
	 * Gets list of rate plans for an iso.
	 * 
	 * @param sessionData
	 * @return A vector of results, or null if none found.
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */

	public Vector<Vector<String>> getIsoRatePlans(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecRatePlans = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			String strSQL = sql_bundle.getString("getRatePlans");
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");
			/*
			 * if (strAccessLevel.equals(DebisysConstants.ISO)) {
			 */
			cat.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setDouble(1, Double.parseDouble((new com.debisys.users.User()).getISOId(strAccessLevel, strRefId)));
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("iso_rateplan_id")));
				vecTemp.add(StringUtil.toString(rs.getString("rateplan")));
				vecTemp.add(StringUtil.toString(rs.getString("rateplan_description")));
				vecRatePlans.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlans", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecRatePlans;
	}

	/**
	 * @param strRefId
	 * @param isNewRatePlanModel
	 *        If 1 then only the new Model RatePlans will be retrieved, if 0 then only the old RatePlans will be retrieved, if null then both RatePlans will be
	 *        retrieved
	 * @param sLevel
	 *        Constant number that indicates the type of actor whose RatePlans will be retrieved
	 * @return Vector list of rate plans allowed in the glue table for the current rep or iso
	 * @throws RatePlanException
	 */
	public Vector<Vector<Vector<String>>> getISORatePlansG(String strRefId, String isNewRatePlanModel, String sLevel, String sCarrier) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<Vector<String>>> vecRatePlans = new Vector<Vector<Vector<String>>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = null;
			vecRatePlans.add(new Vector<Vector<String>>());// Initial vector for new-model templates
			vecRatePlans.add(new Vector<Vector<String>>());// Initial vector for new-model rate plans
			vecRatePlans.add(new Vector<Vector<String>>());// Initial vector for old-model rate plans

			if (isNewRatePlanModel == null || isNewRatePlanModel.equals("1"))
			{
				if ( sCarrier == null )
				{
					strSQL = sql_bundle.getString("getISORatePlansGNewModel");
				}
				else
				{
					strSQL = sql_bundle.getString("getISORatePlansGNewModelCarrier").replaceAll("_CARRIERID_", sCarrier);
				}
				pstmt = dbConn.prepareStatement(strSQL);

				switch (Integer.parseInt(sLevel))
				{
					case 1:
						sLevel = "ISO";
						break;
					case 2:
						sLevel = "AGENT";
						break;
					case 3:
						sLevel = "SUBAGENT";
						break;
					case 4:
						sLevel = "REP";
						break;
					case 5:
						sLevel = "MERCHANT";
						break;
				}
				pstmt.setString(1, strRefId);
				pstmt.setString(2, sLevel);
				cat.debug(strSQL + " -- /*" + strRefId + ", " + sLevel + "*/");
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(rs.getString("RatePlanID"));
					vecTemp.add(rs.getString("Name"));
					vecTemp.add(rs.getString("Description"));
					if (rs.getBoolean("IsTemplate"))
					{
						(vecRatePlans.get(0)).add(vecTemp);
					}
					else
					{
						(vecRatePlans.get(1)).add(vecTemp);
					}
				}
				rs.close();
				pstmt.close();
			}

			if (isNewRatePlanModel == null || isNewRatePlanModel.equals("0"))
			{
				if (!sLevel.equals(DebisysConstants.MERCHANT) && !sLevel.equals("MERCHANT"))
				{
					strSQL = sql_bundle.getString("getISORatePlansG");
					pstmt = dbConn.prepareStatement(strSQL);
					cat.debug(strSQL + " -- /*" + strRefId + "*/");
					pstmt.setString(1, strRefId);
					rs = pstmt.executeQuery();
					while (rs.next())
					{
						Vector<String> vecTemp = new Vector<String>();
						vecTemp.add(StringUtil.toString(rs.getString("iso_rateplan_id")));
						vecTemp.add(StringUtil.toString(rs.getString("rateplan")));
						vecTemp.add(StringUtil.toString(rs.getString("rateplan_description")));
						(vecRatePlans.get(2)).add(vecTemp);
					}
					rs.close();
					pstmt.close();
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlans", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecRatePlans;
	}

	/**
	 * Gets list of default rep rates for an iso.
	 * 
	 * @param sessionData
	 * @return A vector of results, or null if none found.
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */

	public Vector<Vector<String>> getIsoRepRatePlans(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecRatePlans = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			String strSQL = sql_bundle.getString("getIsoRepRatePlans");
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRefId));
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("rep_rateplan_id")));
					vecTemp.add(StringUtil.toString(rs.getString("name")));
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecRatePlans.add(vecTemp);
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getRepRatePlans", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecRatePlans;
	}

	/**
	 * Gets list of rate plans for a rep.
	 * 
	 * @param sessionData
	 * @return A vector of results, or null if none found.
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */

	public Vector<Vector<String>> getRepRatePlans(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecRatePlans = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			String strSQL = "";
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{

				strSQL = sql_bundle.getString("getRepRatePlansISO");
				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRefId));

			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL = sql_bundle.getString("getRepRatePlansREP");
				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(this.repId));
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				strSQL = sql_bundle.getString("getRepRatePlansREP");
				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(this.repId));
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSQL = sql_bundle.getString("getRepRatePlansREP");
				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRefId));
			}
			else
			{
				throw new RatePlanException();
			}

			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("rep_rateplan_id")));
				vecTemp.add(StringUtil.toString(rs.getString("name")));
				vecTemp.add(StringUtil.toString(rs.getString("description")));
				vecRatePlans.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getRepRatePlans", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecRatePlans;
	}

	// BEGIN RELEASE 15.2 NM

	// END RELEASE 15.2 NM

	/**
	 * Gets list of products for an iso.
	 * 
	 * @param sessionData
	 * @return A vector of results, or null if none found.
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */
	public Vector<Vector<String>> getIsoProducts(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecISOProducts = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			String strSQL = sql_bundle.getString("getISOProducts");
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (NumberUtil.isNumeric(this.ratePlanId))
				{
					strSQL = strSQL + " and irp.iso_rateplan_id=" + this.ratePlanId;
				}
				strSQL = strSQL + " order by irp.rateplan, p.description";
				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRefId));
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("rateplan")));
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("id")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_rate")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_rate")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_rate")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_rate")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_rate")));
					vecTemp.add(StringUtil.toString(rs.getString("tsorder")));
					vecTemp.add(StringUtil.toString(rs.getString("iso_rateplan_id")));
					vecTemp.add(StringUtil.toString(rs.getString("cached_receipts")));
					vecISOProducts.add(vecTemp);
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getISOProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecISOProducts;
	}

	/**
	 * Gets list of reps only for an iso in the next sublevel.
	 * 
	 * @param sessionData
	 * @return A vector of results, or null if none found.
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */

	@SuppressWarnings("unchecked")
	public Vector<Vector<String>> getIsoReps(SessionData sessionData, String strRepType, boolean isNewRatePlanModel, String sActorFilter, String sCarrier)
			throws RatePlanException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<String>> vecreps = new Vector<Vector<String>>();
		if (this.ratePlanId.length() > 0)
		{
			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}
				this.ratePlanId = StringUtil.escape(this.ratePlanId);
				String strSQL = "";
				if ( sCarrier != null )
				{
					if (strRepType.equals(DebisysConstants.ISO))
					{
						strSQL = sql_bundle.getString("getIsoRepsCarrierISO");
					}
					else if (strRepType.equals(DebisysConstants.AGENT))
					{
						strSQL = sql_bundle.getString("getIsoRepsCarrierAgent");
					}
					else if (strRepType.equals(DebisysConstants.SUBAGENT) || strRepType.equals("ISO3"))
					{
						strSQL = sql_bundle.getString("getIsoRepsCarrierSubAgent");
					}
					else if (strRepType.equals(DebisysConstants.REP))
					{
						strSQL = sql_bundle.getString("getIsoRepsCarrierRep");
					}
					else if (strRepType.equals(DebisysConstants.MERCHANT))
					{
						strSQL = sql_bundle.getString("getIsoRepsCarrierMerchant");
					}
					strSQL = strSQL.replaceAll("_CARRIER_", sCarrier);
				}
				strSQL += "SELECT r.businessname, r.rep_id, CASE WHEN irp.rep_id IS NULL THEN 0 ELSE 1 END AS checked, ";
				strSQL += "(SELECT COUNT(rp.RatePlanID) FROM RatePlan rp WITH (NOLOCK) WHERE r.rep_id = rp.OwnerID AND irp.iso_rate_plan_id = rp.ParentRatePlanID) AS AmountChildren ";
				strSQL += "FROM reps r WITH (NOLOCK) ";
				strSQL += "LEFT JOIN rep_iso_rate_plan_glue irp WITH (NOLOCK) ON (";
				strSQL += "r.rep_id = irp.rep_id AND irp.iso_rate_plan_id = " + this.ratePlanId + " AND ";
				strSQL += "irp.entityType = 'R' AND ";
				strSQL += "irp.isNewRatePlanModel = " + ((isNewRatePlanModel) ? "1" : "0") + ") ";
				if ( sCarrier != null )
				{
					strSQL += "INNER JOIN CarrierActors ca WITH (NOLOCK) ON r.rep_id = ca.rep_id ";
				}
				strSQL += "WHERE ";

				if (strRepType.equals(DebisysConstants.MERCHANT))
				{
					strSQL = "SELECT m.legal_businessname AS businessname, m.merchant_id AS rep_id, CASE WHEN irp.rep_id IS NULL THEN 0 ELSE 1 END AS checked, ";
					strSQL += "0 AS AmountChildren ";
					strSQL += "FROM merchants m WITH (NOLOCK) ";
					strSQL += "LEFT JOIN rep_iso_rate_plan_glue irp WITH (NOLOCK) ON (";
					strSQL += "m.merchant_id = irp.rep_id AND irp.iso_rate_plan_id = " + this.ratePlanId + " AND ";
					strSQL += "irp.entityType = 'M' AND ";
					strSQL += "irp.isNewRatePlanModel = 1";
					strSQL += ") WHERE ";
				}

				String strSQLWhere = "";
				String sOrderBy = "";
				String strRefId = sessionData.getProperty("ref_id");
				String strAccessLevel = sessionData.getProperty("access_level");
				String strDistChainType = sessionData.getProperty("dist_chain_type");

				if (sessionData.getPropertyObj("CurrentRatePlanActor") != null)
				{
					Vector<String> vTmp = (Vector<String>) sessionData.getPropertyObj("CurrentRatePlanActor");
					strRefId = vTmp.get(0);
					strAccessLevel = vTmp.get(1);
					strDistChainType = vTmp.get(2);
				}

				strRefId = StringUtil.escape(strRefId);
				// login level
				if ((sessionData.getUser().isIntranetUser()
						&& sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
								DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
						|| (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && strRefId.equals(sessionData.getUser().getRefId()))
					)
				{
					if (strRepType.equals("ISO3"))
					{
						strSQLWhere += "(r.type = " + DebisysConstants.REP_TYPE_ISO_3_LEVEL + ") ";
					}
					else if (strRepType.equals(DebisysConstants.ISO))
					{
						strSQLWhere += "(r.type = " + DebisysConstants.REP_TYPE_ISO_5_LEVEL + " OR r.type = " + DebisysConstants.REP_TYPE_ISO_3_LEVEL + ") ";
					}
					else if (strRepType.equals(DebisysConstants.AGENT))
					{
						strSQLWhere += "(r.type = " + DebisysConstants.REP_TYPE_AGENT + ") ";
						if (!sActorFilter.equals(""))
						{
							strSQLWhere += "AND r.iso_id = " + sActorFilter + " ";
						}
					}
					else if (strRepType.equals(DebisysConstants.SUBAGENT))
					{
						strSQLWhere += "(r.type = " + DebisysConstants.REP_TYPE_SUBAGENT + ") ";
						if (!sActorFilter.equals(""))
						{
							strSQLWhere += "AND r.iso_id = " + sActorFilter + " ";
						}
					}
					else if (strRepType.equals(DebisysConstants.REP))
					{
						strSQLWhere += "(r.type = " + DebisysConstants.REP_TYPE_REP + ") ";
						if (!sActorFilter.equals(""))
						{
							strSQLWhere += "AND r.iso_id = " + sActorFilter + " ";
						}
					}
					else if (strRepType.equals(DebisysConstants.MERCHANT))
					{
						strSQLWhere += "1 = 1 ";
						if (!sActorFilter.equals(""))
						{
							strSQLWhere += "AND m.rep_id = " + sActorFilter + " ";
						}
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (strRepType.equals(DebisysConstants.AGENT))
					{
						strSQLWhere += " r.type=" + DebisysConstants.REP_TYPE_AGENT + " AND r.iso_id=" + strRefId + " AND r.rep_id <> " + strRefId;
					}
					else if (strRepType.equals(DebisysConstants.SUBAGENT))
					{
						strSQLWhere += " r.type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND r.iso_id " + "IN (SELECT rep_id FROM reps (NOLOCK) WHERE type="
								+ DebisysConstants.REP_TYPE_AGENT + " AND iso_id=" + strRefId + ")";
						if (this.repId != null && !this.repId.equals(""))
						{
							strSQLWhere += " and r.iso_id=" + this.repId;
						}
					}
					else if (strRepType.equals(DebisysConstants.REP))
					{
						if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
						{
							strSQLWhere += " r.type=" + DebisysConstants.REP_TYPE_REP + " and "; // subagents & agents
							strSQLWhere += " (r.iso_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_SUBAGENT
									+ " AND iso_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id="
									+ strRefId + "))";
							if (this.repId != null && !this.repId.equals(""))
							{
								strSQLWhere = strSQLWhere + " AND r.iso_id=" + this.repId;
							}
							strSQLWhere = strSQLWhere + ")";
						}
						else
						{
							strSQLWhere += " r.type=" + DebisysConstants.REP_TYPE_REP + " AND " + " r.iso_id=" + strRefId;
						}
					}
					else if (strRepType.equals(DebisysConstants.MERCHANT))
					{
						if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
						{
							strSQLWhere += " m.rep_id IN (" + "SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id IN ("
									+ "SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id IN (" + "SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = "
									+ strRefId + ")))";
						}
						else
						{
							strSQLWhere += " m.rep_id IN (" + "SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + strRefId + ")";
						}
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					if (strRepType.equals(DebisysConstants.SUBAGENT))
					{
						strSQLWhere = strSQLWhere + " r.type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND r.iso_id=" + strRefId;
					}
					else if (strRepType.equals(DebisysConstants.REP))
					{
						strSQLWhere += " r.type=" + DebisysConstants.REP_TYPE_REP + " AND ";// subagents
						strSQLWhere += " (iso_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id= "
								+ strRefId + ")";

						if (this.repId != null && !this.repId.equals(""))
						{
							strSQLWhere = strSQLWhere + " AND r.iso_id=" + this.repId;
						}
						strSQLWhere = strSQLWhere + ")";
					}
					else if (strRepType.equals(DebisysConstants.MERCHANT))
					{
						strSQLWhere += " m.rep_id IN (" + "SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id IN ("
								+ "SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + strRefId + "))";
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
				{
					if (strRepType.equals(DebisysConstants.REP))
					{
						strSQLWhere += " r.type=" + DebisysConstants.REP_TYPE_REP + " AND " + " r.iso_id=" + strRefId;
					}
					else if (strRepType.equals(DebisysConstants.MERCHANT))
					{
						strSQLWhere += " m.rep_id IN (" + "SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + strRefId + ")";
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.REP))
				{
					if (strRepType.equals(DebisysConstants.MERCHANT))
					{
						strSQLWhere += " m.rep_id = " + strRefId;
					}
				}

				sOrderBy = " ORDER BY r.businessname ";

				if (strRepType.equals(DebisysConstants.MERCHANT))
				{
					sOrderBy = " ORDER BY m.legal_businessname ";
				}

				cat.debug(strSQL + strSQLWhere + sOrderBy);// + " ORDER BY " + strCol + " " + strSort);
				pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + sOrderBy);
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("businessname")));
					vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
					vecTemp.add(StringUtil.toString(rs.getString("checked")));
					vecTemp.add(StringUtil.toString(rs.getString("AmountChildren")));
					vecreps.add(vecTemp);
				}
			}
			catch (Exception e)
			{
				cat.error("Error during getIsoReps", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return vecreps;
	}

	/**
	 * Assign or unassign a rate plan to a rep depending on the checked flag
	 * 
	 * @param rate_plan_id
	 *        rate plan ID
	 * @return a response string
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */
	public String assignRatePlan(SessionData sessionData, ServletContext context, long rate_plan_id, String reps, String unAssignedData, String level,
			boolean isNewRatePlanModel)
			throws RatePlanException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;

		if (rate_plan_id > 0)
		{
			this.setRatePlanId(String.valueOf(rate_plan_id));
			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				if (unAssignedData.length() > 1)
				{
					String[] vUnassigned = unAssignedData.split(",");
					for (String item : vUnassigned)
					{
						cat.debug("EXEC delete_iso_rate_plan_glue " + item + ", " + rate_plan_id);
						pstmt = dbConn.prepareStatement("EXEC delete_iso_rate_plan_glue ?, ?");
						pstmt.setLong(1, Long.parseLong(item));
						pstmt.setLong(2, rate_plan_id);
						pstmt.execute();
					}
				}

				if (reps.length() > 1)
				{
					if (level.equals(DebisysConstants.MERCHANT))
					{
						cat.debug(sql_bundle.getString("insertISORatePlansG_Merchants") + " -- /*" + rate_plan_id + ", " + isNewRatePlanModel + ", [" + reps
								+ "]*/");
						pstmt = dbConn.prepareStatement(sql_bundle.getString("insertISORatePlansG_Merchants").replaceAll("_MERCHANTS_", reps));
					}
					else
					{
						cat
								.debug(sql_bundle.getString("insertISORatePlansG_Reps") + " -- /*" + rate_plan_id + ", " + isNewRatePlanModel + ", [" + reps
										+ "]*/");
						pstmt = dbConn.prepareStatement(sql_bundle.getString("insertISORatePlansG_Reps").replaceAll("_REPS_", reps));
					}
					pstmt.setLong(1, rate_plan_id);
					pstmt.setLong(2, rate_plan_id);
					pstmt.setBoolean(3, isNewRatePlanModel);
					pstmt.setLong(4, rate_plan_id);
					pstmt.setString(5, sessionData.getProperty("username"));
					pstmt.setString(6, DebisysConfigListener.getInstance(context));
					pstmt.setBoolean(7, isNewRatePlanModel);
					pstmt.execute();
				}

				return "OK";
			}
			catch (Exception e)
			{
				cat.error("Error during assignRatePlan", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, null);
				}
				catch (Exception e)
				{
					cat.error("Error during assignRatePlan", e);
				}
			}
		}
		return "";
	}

	/**
	 * Gets list of products for an iso that have a total commission over 0%.
	 * 
	 * @param sessionData
	 * @return A vector of results, or null if none found.
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */
	public Vector<Vector<String>> getQualifyingISOProducts(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecISOProducts = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			String strSQL = sql_bundle.getString("getQualifyingISOProducts");
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (NumberUtil.isNumeric(this.ratePlanId))
				{
					strSQL = strSQL + " and irp.iso_rateplan_id=" + this.ratePlanId;
				}
				else
				{
					return null;
				}
				strSQL = strSQL + " order by p.description, p.id";
				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRefId));
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("id")));
					vecTemp.add(StringUtil.toString(rs.getString("iso_buyrate")));
					vecISOProducts.add(vecTemp);
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getQualifyingISOProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecISOProducts;
	}

	/**
	 * Gets list of selected products and percentages that have a total commission over 0%.
	 * 
	 * @param sessionData
	 * @return A vector of results, or null if none found.
	 * @throws RatePlanException
	 *         Thrown on database errors
	 */
	public Vector<Vector<String>> getSelectedISOProducts(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecProducts = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (this.productId != null)
		{

			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				String strSQL = sql_bundle.getString("getSelectedISOProducts");
				String strAccessLevel = sessionData.getProperty("access_level");
				String strRefId = sessionData.getProperty("ref_id");

				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (NumberUtil.isNumeric(this.ratePlanId))
					{
						strSQL = strSQL + " and irp.iso_rateplan_id=" + this.ratePlanId;
					}
					else
					{
						return null;
					}
					if (this.productId != null && this.productId.length > 0)
					{
						strSQL = strSQL + " and p.id in (" + StringUtil.arrayToString(this.productId, ",") + ")";
					}
					else
					{
						return null;
					}

					strSQL = strSQL + " order by p.description, p.id";
					cat.debug(strSQL);
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.setDouble(1, Double.parseDouble(strRefId));
					rs = pstmt.executeQuery();
					while (rs.next())
					{
						Vector<String> vecTemp = new Vector<String>();
						vecTemp.add(StringUtil.toString(rs.getString("description")));
						vecTemp.add(StringUtil.toString(rs.getString("id")));
						vecTemp.add(StringUtil.toString(rs.getString("iso_buyrate")));
						vecTemp.add(StringUtil.toString(rs.getString("iso_rate")));
						vecTemp.add(StringUtil.toString(rs.getString("agent_rate")));
						vecTemp.add(StringUtil.toString(rs.getString("subagent_rate")));
						vecTemp.add(StringUtil.toString(rs.getString("tsorder")));
						vecTemp.add(StringUtil.toString(rs.getString("iso_rateplan_id")));
						vecProducts.add(vecTemp);
					}
				}
			}
			catch (Exception e)
			{
				cat.error("Error during getSelectedISOProducts", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return vecProducts;
	}

	public Hashtable<String, String> getIsoBuyRates(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Hashtable<String, String> hashBuyRates = new Hashtable<String, String>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (this.productId != null)
		{

			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				String strSQL = sql_bundle.getString("getSelectedISOProducts");
				String strAccessLevel = sessionData.getProperty("access_level");
				String strRefId = sessionData.getProperty("ref_id");

				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (NumberUtil.isNumeric(this.ratePlanId))
					{
						strSQL = strSQL + " and irp.iso_rateplan_id=" + this.ratePlanId;
					}
					else
					{
						return null;
					}
					if (this.productId != null && this.productId.length > 0)
					{
						strSQL = strSQL + " and p.id in (" + StringUtil.arrayToString(this.productId, ",") + ")";
					}
					else
					{
						return null;
					}

					strSQL = strSQL + " order by p.description, p.id";
					cat.debug(strSQL);
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.setDouble(1, Double.parseDouble(strRefId));
					rs = pstmt.executeQuery();
					while (rs.next())
					{
						if (!hashBuyRates.containsKey(StringUtil.toString(rs.getString("id"))))
						{
							hashBuyRates.put(StringUtil.toString(rs.getString("id")), StringUtil.toString(rs.getString("iso_buyrate")));
						}
					}
				}
			}
			catch (Exception e)
			{
				cat.error("Error during getISOBuyRates", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return hashBuyRates;
	}

	/**
	 * Gets a list of reps for an iso
	 * 
	 * @param sessionData
	 * @return Vector
	 * @throws com.debisys.exceptions.RatePlanException
	 *         Thrown on database errors
	 */
	public static Vector<Vector<String>> getRepList(SessionData sessionData) throws RatePlanException
	{
		Vector<Vector<String>> vecRepList = new Vector<Vector<String>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = "";
			String strSQLWhere = "";
			String strAccessLevel = sessionData.getProperty("access_level");
			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strRefId = sessionData.getProperty("ref_id");
			strRefId = StringUtil.escape(strRefId);
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL = sql_bundle.getString("getRepListISO3");
					strSQLWhere = " reps.iso_id=" + strRefId + " and reps.type=" + DebisysConstants.REP_TYPE_REP;
					strSQLWhere = strSQLWhere + " order by reps.businessname";
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL = sql_bundle.getString("getRepListISO5");
					strSQLWhere = " r1.iso_id=" + strRefId + " and r1.type=" + DebisysConstants.REP_TYPE_AGENT + " and r2.iso_id=r1.rep_id and r2.type="
							+ DebisysConstants.REP_TYPE_SUBAGENT + " and   r3.iso_id=r2.rep_id and r3.type=" + DebisysConstants.REP_TYPE_REP;
					strSQLWhere = strSQLWhere + " order by r1.businessname, r2.businessname, r3.businessname";
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL = sql_bundle.getString("getRepListAGENT");
				strSQLWhere = " r1.iso_id=" + strRefId + " and r1.type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and   r2.iso_id=r1.rep_id and r2.type="
						+ DebisysConstants.REP_TYPE_REP;
				strSQLWhere = strSQLWhere + " order by r1.businessname, r2.businessname";

			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				strSQL = sql_bundle.getString("getRepListSUBAGENT");
				strSQLWhere = " reps.iso_id=" + strRefId + " and reps.type=" + DebisysConstants.REP_TYPE_REP;
				strSQLWhere = strSQLWhere + " order by reps.businessname";

			}

			cat.debug(strSQL + strSQLWhere);
			pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
			rs = pstmt.executeQuery();
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{

				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{

					while (rs.next())
					{
						Vector<String> vecTemp = new Vector<String>();
						vecTemp.add(rs.getString("rep_id"));
						vecTemp.add(rs.getString("rep_businessname"));
						vecTemp.add(rs.getString("agent_businessname"));
						vecTemp.add(rs.getString("subagent_businessname"));
						vecRepList.add(vecTemp);
					}
				}
				else
				{
					while (rs.next())
					{
						Vector<String> vecTemp = new Vector<String>();
						vecTemp.add(rs.getString("rep_id"));
						vecTemp.add(rs.getString("businessname"));
						vecRepList.add(vecTemp);
					}

				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				while (rs.next())
				{

					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(rs.getString("rep_id"));
					vecTemp.add(rs.getString("rep_businessname"));
					vecTemp.add(rs.getString("subagent_businessname"));
					vecRepList.add(vecTemp);
				}

			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				while (rs.next())
				{

					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(rs.getString("rep_id"));
					vecTemp.add(rs.getString("businessname"));
					vecRepList.add(vecTemp);
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getRepList", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecRepList;
	}

	/**
	 * Returns a hash of errors.
	 * 
	 * @return Hash of errors.
	 */

	public Hashtable<String, String> getErrors()
	{
		return this.validationErrors;
	}

	public boolean isError()
	{
		return (this.validationErrors.size() > 0);
	}

	/**
	 * Returns a validation error against a specific field. If a field was found to have an error during {@link #validateRepRatePlan validateRatePlan}, the
	 * error message can be accessed via this method.
	 * 
	 * @param fieldname
	 *        The bean property name of the field
	 * @return The error message for the field or null if none is present.
	 */

	public String getFieldError(String fieldname)
	{
		return (this.validationErrors.get(fieldname));
	}

	/**
	 * Sets the validation error against a specific field. Used by {@link #validateRepRatePlan validateRatePlan}.
	 * 
	 * @param fieldname
	 *        The bean property name of the field
	 * @param error
	 *        The error message for the field or null if none is present.
	 */

	public void addFieldError(String fieldname, String error)
	{
		if (!this.validationErrors.containsKey(fieldname))
		{
			this.validationErrors.put(fieldname, error);
		}

	}

	/**
	 * Validates the merchant for missing or invalid fields.
	 * 
	 * @return <code>true</code> if the entity passes the validation checks, otherwise <code>false</code>
	 */

	public boolean validateRepRatePlan(SessionData sessionData, Hashtable<String, Vector<String>> hashRatePlan)

	{
		this.validationErrors.clear();
		boolean valid = true;
		if ((this.ratePlanName == null) || (this.ratePlanName.length() == 0))
		{
			addFieldError("ratePlanName", Languages.getString("com.debisys.rateplans.error1", sessionData.getLanguage()));
			valid = false;
		}
		else if (this.ratePlanName.length() > 50)
		{
			addFieldError("ratePlanName", Languages.getString("com.debisys.rateplans.error2", sessionData.getLanguage()));
			valid = false;
		}

		if (this.productId != null)
		{
			String strProductId = "";
			Hashtable<String, String> hashBuyRates = new Hashtable<String, String>();
			try
			{
				hashBuyRates = this.getIsoBuyRates(sessionData);
			}
			catch (RatePlanException e)
			{
			}

			for (int i = 0; i < this.productId.length; i++)
			{
				strProductId = this.productId[i];
				if (hashBuyRates.containsKey(strProductId))
				{
					if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						double buyRate = 0;
						double isoRate = 0;
						double agentRate = 0;
						double subagentRate = 0;
						double assignedTotal = 0;

						Vector<String> vecTemp = hashRatePlan.get(strProductId);
						buyRate = Double.parseDouble(hashBuyRates.get(strProductId));
						if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
						{
							isoRate = Double.parseDouble(vecTemp.get(0).toString());
							if (isoRate < 0)
							{
								this.addFieldError("iso_" + strProductId, "");
								valid = false;
							}

						}
						else
						{
							this.addFieldError("iso_" + strProductId, "");
							valid = false;
						}

						if (NumberUtil.isNumeric(vecTemp.get(1).toString()))
						{
							agentRate = Double.parseDouble(vecTemp.get(1).toString());
							if (agentRate < 0)
							{
								this.addFieldError("agent_" + strProductId, "");
								valid = false;
							}

						}
						else
						{
							this.addFieldError("agent_" + strProductId, "");
							valid = false;
						}

						if (NumberUtil.isNumeric(vecTemp.get(2).toString()))
						{
							subagentRate = Double.parseDouble(vecTemp.get(2).toString());
							if (subagentRate < 0)
							{
								this.addFieldError("subagent_" + strProductId, "");
								valid = false;
							}
						}
						else
						{
							this.addFieldError("subagent_" + strProductId, "");
							valid = false;
						}

						assignedTotal = isoRate + agentRate + subagentRate;

						if (assignedTotal > buyRate)
						{
							this.addFieldError("remaining_" + strProductId, "");
							valid = false;
						}
					}
					else
					{
						double buyRate = 0;
						double isoRate = 0;
						// double assignedTotal = 0;//Never read

						Vector<String> vecTemp = hashRatePlan.get(strProductId);
						buyRate = Double.parseDouble(hashBuyRates.get(strProductId));
						if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
						{
							isoRate = Double.parseDouble(vecTemp.get(0).toString());
							if (isoRate < 0)
							{
								this.addFieldError("iso_" + strProductId, "");
								valid = false;
							}

						}
						else
						{
							this.addFieldError("iso_" + strProductId, "");
							valid = false;
						}

						if (isoRate > buyRate)
						{
							this.addFieldError("remaining_" + strProductId, "");
							valid = false;
						}

					}

				}
				else
				{
					addFieldError("general", Languages.getString("com.debisys.rateplans.error3", sessionData.getLanguage()) + " " + strProductId);
					valid = false;
				}

			}
		}

		return valid;
	}

	/**
	 * Adds a new rep rate plan in the database
	 * 
	 * @throws RatePlanException
	 *         Thrown if there is an error inserting the record in the database.
	 */

	public void addRepRatePlan(SessionData sessionData, Hashtable<String, Vector<String>> hashRepRates) throws RatePlanException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strRefId = sessionData.getProperty("ref_id");
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			dbConn.setAutoCommit(false);
			pstmt = dbConn.prepareStatement(sql_bundle.getString("addRepRatePlan"), PreparedStatement.RETURN_GENERATED_KEYS);

			pstmt.setDouble(1, Double.parseDouble(this.ratePlanId));
			pstmt.setDouble(2, Double.parseDouble(strRefId));
			pstmt.setString(3, this.ratePlanName);
			pstmt.executeUpdate();
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("addRepRatePlan"), sql_bundle, cat);
			rs = pstmt.getGeneratedKeys();
			String repRatePlanId = "";
			if (rs.next())
			{
				repRatePlanId = rs.getString(1);
			}
			else
			{
				cat.error("Error during addRepRatePlan[Could not get identity]");
				throw new RatePlanException();
			}
			pstmt.close();
			rs.close();
			Log.write(sessionData, "Rep Rate Plan: id:" + repRatePlanId + " name:" + this.ratePlanName + " added.", DebisysConstants.LOGTYPE_CUSTOMER, null,
					null);

			pstmt = dbConn.prepareStatement(sql_bundle.getString("addRepRates"));

			for (int i = 0; i < this.productId.length; i++)
			{
				String strProductId = this.productId[i];
				double isoRate = 0;
				double agentRate = 0;
				double subagentRate = 0;
				if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					Vector<String> vecTemp = hashRepRates.get(strProductId);
					if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
					{
						isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
					}

					if (NumberUtil.isNumeric(vecTemp.get(1).toString()))
					{
						agentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
					}

					if (NumberUtil.isNumeric(vecTemp.get(2).toString()))
					{
						subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
					}
				}
				else
				{
					Vector<String> vecTemp = hashRepRates.get(strProductId);
					if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
					{
						isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
					}
				}
				pstmt.setInt(1, Integer.parseInt(repRatePlanId));
				pstmt.setInt(2, Integer.parseInt(this.productId[i]));
				pstmt.setDouble(3, isoRate);
				pstmt.setDouble(4, agentRate);
				pstmt.setDouble(5, subagentRate);
				pstmt.addBatch();
			}
			pstmt.executeBatch();
			pstmt.close();

			pstmt = dbConn.prepareStatement(sql_bundle.getString("addIsoRatePlanRep"));

			if (this.repIds != null)
			{
				for (int i = 0; i < this.repIds.length; i++)
				{
					String strRepId = this.repIds[i];
					pstmt.setInt(1, Integer.parseInt(repRatePlanId));
					pstmt.setDouble(2, Double.parseDouble(strRepId));
					pstmt.setInt(3, Integer.parseInt(repRatePlanId));
					pstmt.setDouble(4, Double.parseDouble(strRepId));

					pstmt.addBatch();
				}
			}
			// always set the iso as a rep so they can add reps
			pstmt.setInt(1, Integer.parseInt(repRatePlanId));
			pstmt.setDouble(2, Double.parseDouble(strRefId));
			pstmt.setInt(3, Integer.parseInt(repRatePlanId));
			pstmt.setDouble(4, Double.parseDouble(strRefId));
			pstmt.addBatch();

			pstmt.executeBatch();
			pstmt.close();
			dbConn.commit();
			dbConn.setAutoCommit(true);
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("addRepRates"), sql_bundle, cat);
		}
		catch (Exception e)
		{
			try
			{
				if (dbConn != null)
				{
					dbConn.rollback();
				}
			}
			catch (SQLException ex)
			{
				cat.error("Error during rollback addRepRatePlan", ex);
			}
			cat.error("Error during addRepRatePlan", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}

	public void updateRepRatePlan(SessionData sessionData, Hashtable<String, Vector<String>> hashRepRates) throws RatePlanException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String strRefId = sessionData.getProperty("ref_id");
		strRefId = StringUtil.escape(strRefId);
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			dbConn.setAutoCommit(false);
			pstmt = dbConn.prepareStatement(sql_bundle.getString("updateRepRatePlan"));

			pstmt.setString(1, this.ratePlanName);
			pstmt.setDouble(2, Double.parseDouble(this.repRatePlanId));
			pstmt.setDouble(3, Double.parseDouble(strRefId));
			pstmt.executeUpdate();
			pstmt.close();
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("updateRepRatePlan"), sql_bundle, cat);
			Log.write(sessionData, "Rep Rate Plan: id:" + this.repRatePlanId + " name:" + this.ratePlanName + " updated.", DebisysConstants.LOGTYPE_CUSTOMER,
					null, null);

			pstmt = dbConn.prepareStatement(sql_bundle.getString("updateRepRates"));

			for (int i = 0; i < this.productId.length; i++)
			{
				String strProductId = this.productId[i];
				double isoRate = 0;
				double agentRate = 0;
				double subagentRate = 0;

				if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					Vector<String> vecTemp = hashRepRates.get(strProductId);
					if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
					{
						isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
					}
					if (NumberUtil.isNumeric(vecTemp.get(1).toString()))
					{
						agentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
					}
					if (NumberUtil.isNumeric(vecTemp.get(2).toString()))
					{
						subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
					}
				}
				else
				{
					Vector<String> vecTemp = hashRepRates.get(strProductId);
					if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
					{
						isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
					}
				}
				pstmt.setDouble(1, isoRate);
				pstmt.setDouble(2, agentRate);
				pstmt.setDouble(3, subagentRate);
				pstmt.setInt(4, Integer.parseInt(this.repRatePlanId));
				pstmt.setInt(5, Integer.parseInt(this.productId[i]));
				pstmt.addBatch();
			}

			pstmt.executeBatch();
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("updateRepRates"), sql_bundle, cat);
			pstmt.close();

			String strSQL = sql_bundle.getString("deleteRepDefaultRates");
			// delete rep_default_rates where iso_rateplan_rep_id in (select iso_rateplan_rep_id from iso_rateplan_rep where rep_rateplan_id=?)
			String strRepList = StringUtil.arrayToString(this.repIds, ",");
			strRepList = StringUtil.escape(strRepList);
			if (this.repIds != null)
			{
				strSQL = strSQL + " and rep_id not in (" + strRepList + ")";
				strSQL = strSQL + " and rep_id <> " + strRefId + ")";
			}
			else
			{
				strSQL = strSQL + " and rep_id <> " + strRefId + ")";
			}

			pstmt = dbConn.prepareStatement(strSQL);
			cat.debug(strSQL);
			pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
			pstmt.executeUpdate();
			pstmt.close();
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("deleteRepDefaultRates"), sql_bundle, cat);
			strSQL = sql_bundle.getString("deleteIsoRatePlanRep");
			if (this.repIds != null)
			{
				strSQL = strSQL + " and rep_id not in (" + strRepList + ") ";
			}
			strSQL = strSQL + " and rep_id <> " + strRefId;

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
			pstmt.executeUpdate();
			pstmt.close();
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("deleteIsoRatePlanRep"), sql_bundle, cat);

			if (this.repIds != null)
			{

				pstmt = dbConn.prepareStatement(sql_bundle.getString("addIsoRatePlanRep"));

				for (int i = 0; i < this.repIds.length; i++)
				{
					String strRepId = this.repIds[i];
					pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
					pstmt.setDouble(2, Double.parseDouble(strRepId));
					pstmt.setInt(3, Integer.parseInt(this.repRatePlanId));
					pstmt.setDouble(4, Double.parseDouble(strRepId));

					pstmt.addBatch();
				}
				pstmt.executeBatch();
				pstmt.close();
			}
			dbConn.commit();
			dbConn.setAutoCommit(true);
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("addIsoRatePlanRep"), sql_bundle, cat);
		}
		catch (Exception e)
		{
			try
			{
				if (dbConn != null)
				{
					dbConn.rollback();
				}
			}
			catch (SQLException ex)
			{
				cat.error("Error during rollback updateRepRatePlan", ex);
			}
			cat.error("Error during updateRepRatePlan", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}

	public int getCheckedValue(String disabledValues, String strProdID)
	{
		int ret = 0;

		String values[] = disabledValues.split(",");

		for (int i = 0; i <= values.length; i++)
		{
			if (values[i] == strProdID)
			{
				ret = 1;
			}
		}

		return ret;
	}

	/**
	 * @param sessionData  
	 */
	public void updateTSOrder(SessionData sessionData, Hashtable<Integer, Vector<String>> hashTSOrders) throws RatePlanException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			if (!hashTSOrders.isEmpty())
			{
				dbConn.setAutoCommit(false);

				pstmt = dbConn.prepareStatement(sql_bundle.getString("updateIsoTopSellers"));

				int i = 1;
				while (hashTSOrders.get(new Integer(i)) != null)
				{
					int prodid = 0;
					int tsorder = 0;
					int ratetable_id = 0;
					int cachedrecp = 0;

					Vector<String> vecTemp = hashTSOrders.get(new Integer(i));
					if (vecTemp.get(0) != null)
					{
						prodid = Integer.parseInt(vecTemp.get(0).toString());
						pstmt.setInt(4, prodid);
					}
					if (vecTemp.get(1) != null)
					{
						if (vecTemp.get(1) != "")
						{
							if (NumberUtil.isNumeric(vecTemp.get(1).toString()))
							{
								tsorder = Integer.parseInt(vecTemp.get(1).toString());
								pstmt.setInt(1, tsorder);
							}
							else
							{
								pstmt.setNull(1, java.sql.Types.INTEGER);
							}
						}
						else
						{
							pstmt.setNull(1, java.sql.Types.INTEGER);
						}
					}
					if (vecTemp.get(2) != null)
					{
						ratetable_id = Integer.parseInt(vecTemp.get(2).toString());
						pstmt.setInt(3, ratetable_id);
					}
					if (vecTemp.get(3) != null)
					{
						cachedrecp = Integer.parseInt(vecTemp.get(3).toString());
						pstmt.setByte(2, (byte) cachedrecp);

					}

					pstmt.addBatch();
					i++;
				}

				pstmt.executeBatch();
				pstmt.close();
				dbConn.commit();
				dbConn.setAutoCommit(true);
			}
		}
		catch (Exception e)
		{
			try
			{
				if (dbConn != null)
				{
					dbConn.rollback();
				}
			}
			catch (SQLException ex)
			{
				cat.error("Error during rollback updateIsoTS", ex);
			}
			cat.error("Error during updateIsoTS", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}

	public void updateRepRatePlanProducts(SessionData sessionData, Hashtable<String, Vector<String>> hashRepRates) throws RatePlanException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		// String strRefId = sessionData.getProperty("ref_id");//Never read
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			if (this.productId != null)
			{
				dbConn.setAutoCommit(false);
				pstmt = dbConn.prepareStatement(sql_bundle.getString("modifyRepRates"));

				for (int i = 0; i < this.productId.length; i++)
				{
					String strProductId = this.productId[i];
					double isoRate = 0;
					double agentRate = 0;
					double subagentRate = 0;

					if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{

						Vector<String> vecTemp = hashRepRates.get(strProductId);
						if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
						{
							isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
						}

						if (NumberUtil.isNumeric(vecTemp.get(1).toString()))
						{
							agentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(1).toString()));
						}

						if (NumberUtil.isNumeric(vecTemp.get(2).toString()))
						{
							subagentRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(2).toString()));
						}

					}
					else
					{

						Vector<String> vecTemp = hashRepRates.get(strProductId);
						if (NumberUtil.isNumeric(vecTemp.get(0).toString()))
						{
							isoRate = Double.parseDouble(NumberUtil.formatAmount(vecTemp.get(0).toString()));
						}

					}

					pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
					pstmt.setInt(2, Integer.parseInt(this.productId[i]));
					pstmt.setDouble(3, isoRate);
					pstmt.setDouble(4, agentRate);
					pstmt.setDouble(5, subagentRate);
					pstmt.setInt(6, Integer.parseInt(this.repRatePlanId));
					pstmt.setInt(7, Integer.parseInt(this.productId[i]));
					pstmt.setInt(8, Integer.parseInt(this.repRatePlanId));
					pstmt.setInt(9, Integer.parseInt(this.productId[i]));
					pstmt.setDouble(10, isoRate);
					pstmt.setDouble(11, agentRate);
					pstmt.setDouble(12, subagentRate);
					pstmt.addBatch();
				}
				pstmt.executeBatch();
				pstmt.close();

			}
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("modifyRepRates"), sql_bundle, cat);

			String strSQL = sql_bundle.getString("deleteRepRates");
			// deleteRepRates=delete rep_rates where rep_rateplan_id=?
			String strProductList = StringUtil.arrayToString(this.productId, ",");
			if (this.productId != null)
			{
				strSQL = strSQL + " and product_id not in (" + strProductList + ")";
			}

			pstmt = dbConn.prepareStatement(strSQL);
			cat.debug(strSQL);
			pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
			pstmt.executeUpdate();
			pstmt.close();
			dbConn.commit();
			dbConn.setAutoCommit(true);
			CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("deleteRepRates"), sql_bundle, cat);
		}
		catch (Exception e)
		{
			try
			{
				if (dbConn != null)
					dbConn.rollback();
			}
			catch (SQLException ex)
			{
				cat.error("Error during rollback updateRepRatePlanProducts", ex);
			}
			cat.error("Error during updateRepRatePlanProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}

	public void deleteRepRatePlan(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		ResultSet rs = null;
		String strRefId = sessionData.getProperty("ref_id");
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			// firs t check to see if it exists for this iso
			pstmt1 = dbConn.prepareStatement(sql_bundle.getString("getRepRatePlan"));
			pstmt1.setDouble(1, Double.parseDouble(strRefId));
			pstmt1.setDouble(2, Double.parseDouble(this.repRatePlanId));
			rs = pstmt1.executeQuery();

			if (rs.next())
			{
				this.ratePlanName = rs.getString("name");

				pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteRepRates"));
				pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
				pstmt.executeUpdate();
				pstmt.close();
				CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("deleteRepRates"), sql_bundle, cat);
				pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteRepDefaultRates") + ")");
				pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
				pstmt.executeUpdate();
				pstmt.close();
				CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("deleteRepDefaultRates"), sql_bundle, cat);
				pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteIsoRatePlanRep"));
				pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
				pstmt.executeUpdate();
				pstmt.close();
				CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("deleteIsoRatePlanRep"), sql_bundle, cat);
				pstmt = dbConn.prepareStatement(sql_bundle.getString("deleteRepRatePlan"));
				pstmt.setInt(1, Integer.parseInt(this.repRatePlanId));
				pstmt.executeUpdate();
				pstmt.close();
				CSystemMethods.saveRateChangeLog(sessionData, sql_bundle.getString("deleteRepRatePlan"), sql_bundle, cat);
				Log.write(sessionData, "Rep Rate Plan: id:" + this.repRatePlanId + " name:" + this.ratePlanName + " deleted.",
						DebisysConstants.LOGTYPE_CUSTOMER, null, null);
			}
		}
		catch (Exception e)
		{
			cat.error("Error during deleteRepRatePlan", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(null, pstmt1, null);
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
	}

	public Vector<Vector<String>> getRepRatePlan(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecProducts = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (this.repRatePlanId != null)
		{

			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				String strSQL = sql_bundle.getString("getRepRates");
				String strAccessLevel = sessionData.getProperty("access_level");
				String strRefId = sessionData.getProperty("ref_id");

				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (NumberUtil.isNumeric(this.repRatePlanId))
					{
						strSQL = strSQL + " and rrp.rep_rateplan_id=" + this.repRatePlanId;
					}
					else
					{
						return null;
					}

					strSQL = strSQL + " order by p.description, p.id";
					cat.debug(strSQL);
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.setDouble(1, Double.parseDouble(strRefId));
					rs = pstmt.executeQuery();
					while (rs.next())
					{
						Vector<String> vecTemp = new Vector<String>();
						vecTemp.add(StringUtil.toString(rs.getString("description")));
						vecTemp.add(StringUtil.toString(rs.getString("id")));
						vecTemp.add(StringUtil.toString(rs.getString("iso_buyrate")));
						vecTemp.add(StringUtil.toString(rs.getString("iso_rate")));
						vecTemp.add(StringUtil.toString(rs.getString("agent_rate")));
						vecTemp.add(StringUtil.toString(rs.getString("subagent_rate")));
						vecProducts.add(vecTemp);
					}
				}
			}
			catch (Exception e)
			{
				cat.error("Error during getRepRatePlan", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return vecProducts;
	}

	public Vector<Vector<String>> getAvailableRepRatePlanProducts(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecProducts = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strAccessLevel = sessionData.getProperty("access_level");
		String strRefId = sessionData.getProperty("ref_id");

		if (this.repRatePlanId != null && strAccessLevel.equals(DebisysConstants.ISO) && NumberUtil.isNumeric(this.repRatePlanId))
		{

			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				String strSQL = sql_bundle.getString("getAvailableRepRatePlanProducts");

				cat.debug(strSQL);
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRefId));
				pstmt.setDouble(2, Double.parseDouble(this.repRatePlanId));
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("id")));
					vecTemp.add(StringUtil.toString(rs.getString("iso_buyrate")));
					vecTemp.add(StringUtil.toString(rs.getString("iso_rate")));
					vecTemp.add(StringUtil.toString(rs.getString("agent_rate")));
					vecTemp.add(StringUtil.toString(rs.getString("subagent_rate")));
					vecProducts.add(vecTemp);
				}
			}
			catch (Exception e)
			{
				cat.error("Error during getAvailbleRepRatePlanProducts", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return vecProducts;
	}

	public void loadRatePlanReps(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		// Vector vecProducts = new Vector();//Never read
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (this.repRatePlanId != null)
		{

			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				// now load reps
				String strSQL = sql_bundle.getString("getIsoRatePlanRep");
				String strAccessLevel = sessionData.getProperty("access_level");
				String strRefId = sessionData.getProperty("ref_id");

				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (NumberUtil.isNumeric(this.repRatePlanId))
					{
						strSQL = strSQL + this.repRatePlanId;
					}

					cat.debug(strSQL);
					Statement stmt = dbConn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
					rs = stmt.executeQuery(strSQL);
					cat.debug(stmt.getWarnings());
					rs.last();
					String[] aryRepIds = new String[rs.getRow()];
					rs.beforeFirst();
					int i = 0;
					while (rs.next())
					{
						aryRepIds[i] = rs.getString("rep_id");
						i++;
					}
					this.repIds = aryRepIds;
					stmt.close();
					rs.close();

					// now load rate plan name
					strSQL = sql_bundle.getString("getRepRatePlan");
					cat.debug(strSQL);
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.setDouble(1, Double.parseDouble(strRefId));
					pstmt.setInt(2, Integer.parseInt(this.repRatePlanId));
					rs = pstmt.executeQuery();

					if (rs.next())
					{
						this.ratePlanId = rs.getString("iso_rateplan_id");
						this.ratePlanName = rs.getString("name");
					}
				}
			}
			catch (Exception e)
			{
				cat.error("Error during loadRatePlanReps", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
	}

	public Vector<Vector<String>> getMerchantRatePlan(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecProducts = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (this.repRatePlanId != null)
		{

			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				String strAccessLevel = sessionData.getProperty("access_level");
				String strRefId = sessionData.getProperty("ref_id");

				String strSQL = "";

				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					strSQL = sql_bundle.getString("getMerchantRatesISO");
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.setDouble(1, Double.parseDouble(strRefId));
					// pstmt.setDouble(2, Double.parseDouble(strRefId));
					pstmt.setInt(2, Integer.parseInt(this.repRatePlanId));
					cat.info("strRefId=" + strRefId);

				}
				else
				{
					throw new RatePlanException();
				}
				/*
				 * else if (strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) { strSQL =
				 * sql_bundle.getString("getMerchantRatesREP"); pstmt = dbConn.prepareStatement(strSQL); pstmt.setDouble(1, Double.parseDouble(this.repId));
				 * pstmt.setDouble(2, Double.parseDouble(this.repRatePlanId));
				 * 
				 * } else { strSQL = sql_bundle.getString("getMerchantRatesREP"); pstmt = dbConn.prepareStatement(strSQL); pstmt.setDouble(1,
				 * Double.parseDouble(strRefId)); pstmt.setDouble(2, Double.parseDouble(this.repRatePlanId));
				 * 
				 * }
				 */
				//
				cat.debug(strSQL);

				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("id")));
					vecTemp.add(StringUtil.toString(rs.getString("total_buyrate")));
					// Double maxAssing = rs.getDouble("iso_rate") + rs.getDouble("agent_rate") + rs.getDouble("subagent_rate");
					// vecTemp.add(maxAssing.toString());
					vecProducts.add(vecTemp);
				}
			}
			catch (Exception e)
			{
				cat.error("Error during getMerchantRatePlan", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return vecProducts;
	}

	// BEGIN RELEASE 15.2

	public Vector<Vector<String>> getProductsIsoRatePlan(SessionData sessionData) throws RatePlanException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecProducts = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (this.ratePlanId != null)
		{

			try
			{
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new RatePlanException();
				}

				String strAccessLevel = sessionData.getProperty("access_level");
				String strRefId = sessionData.getProperty("ref_id");

				String strSQL = "";

				strSQL = sql_bundle.getString("getProductsIsoRatePlan");
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble((new com.debisys.users.User()).getISOId(strAccessLevel, strRefId)));
				if (this.getRatePlanId().length() == 0)
				{
					this.setRatePlanId("0");
				}
				pstmt.setDouble(2, Double.parseDouble(this.getRatePlanId()));
				pstmt.setInt(3, Integer.parseInt(strAccessLevel));

				cat.debug(strSQL);

				rs = pstmt.executeQuery();
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("id")));
					vecTemp.add(StringUtil.toString(rs.getString("total_buyrate")));
					vecTemp.add(StringUtil.toString(rs.getString("iso_rate")));
					vecTemp.add(StringUtil.toString(rs.getString("agent_rate")));
					vecTemp.add(StringUtil.toString(rs.getString("subagent_rate")));
					vecTemp.add(StringUtil.toString(rs.getString("rep_rate")));
					vecTemp.add(StringUtil.toString(rs.getString("merchant_rate")));
					vecProducts.add(vecTemp);
				}
			}
			catch (Exception e)
			{
				cat.error("Error during getMerchantRatePlan", e);
				throw new RatePlanException();
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return vecProducts;
	}

	// END

	public void valueBound(HttpSessionBindingEvent event)
	{
	}

	public void valueUnbound(HttpSessionBindingEvent event)
	{
	}

	public static void main(String[] args)
	{
	}

	/* BEGIN Release 8.1 - Added by LF */
	public String getISORatePlanNameFromRepRatePlan() throws RatePlanException
	{
		String sResult = "";
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TerminalException();
			}

			String strSQL = sql_bundle.getString("getISORatePlanNameFromRepRatePlan");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setDouble(1, Double.parseDouble(this.repRatePlanId));
			rs = pstmt.executeQuery();

			if (rs.next())
			{
				sResult = rs.getString("RatePlan");
			}
		}
		catch (Exception e)
		{
			cat.error("Error during getISORatePlanNameFromRepRatePlan", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return sResult;
	}

	/* END Release 8.1 - Added by LF */

	/**
  	 * 
  	 */
	public String downloadRatePlans(SessionData sessionData, ServletContext context, String ratePlanID) throws RatePlanException
	{
		Connection dbConn = null;
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		String strFileName = "";
		//String deploymentType = DebisysConfigListener.getDeploymentType(context);
		String strRefId = sessionData.getProperty("ref_id");
		//String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		PreparedStatement pstmt = null;
		//Vector vecISOProducts = new Vector();
		ResultSet rs = null;

		try
		{
			// Build CSV file
			BufferedWriter outputFile = null;
			try
			{
				// Setup CSV file/location
				// downloadUrl=/support/admin/download/
				// downloadPath=C:\Development\apache\Tomcat 4.1.30\webapps\debisys\admin\download\
				// workingDir=c:\temp\
				strFileName = this.generateFileName(context);

				// Create the output file
				File file = new File(workingDir);
				if (!file.exists())
				{
					file.mkdir();
				}

				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
				cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
				outputFile.flush();

				// Database setup
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new TransactionException();
				}

				String strSQL = sql_bundle.getString("getISOProducts");

				cat.debug(this.ratePlanId);
				// Setup query
				if (NumberUtil.isNumeric(ratePlanID))
				{
					strSQL = strSQL + " and irp.iso_rateplan_id=" + ratePlanID;
				}
				strSQL = strSQL + " order by irp.rateplan, p.description";

				// Execute query
				cat.debug(strSQL);
				long startQuery = System.currentTimeMillis();
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRefId));
				rs = pstmt.executeQuery();

				outputFile.write("\"" + Languages.getString("jsp.admin.rateplans.index.rateplan", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.product", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.product_id", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()) + "\",");
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					outputFile.write("\"" + Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()) + "\",");
					outputFile.write("\"" + Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()) + "\",");
				}
				outputFile.write("\"" + Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.rateplans.tsorder", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.rateplans.rateplan_id", sessionData.getLanguage()) + "\",");
				outputFile.write("\r\n");

				// Add rows to CSV
				while (rs.next())
				{
					outputFile.write("\"" + StringUtil.toString(rs.getString("rateplan")) + "\",");
					outputFile.write("\"" + StringUtil.toString(rs.getString("description")) + "\",");
					outputFile.write("\"" + StringUtil.toString(rs.getString("id")) + "\",");
					outputFile.write("\"" + StringUtil.toString(rs.getString("iso_rate")) + "%\",");
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						outputFile.write("\"" + StringUtil.toString(rs.getString("agent_rate")) + "%\",");
						outputFile.write("\"" + StringUtil.toString(rs.getString("subagent_rate")) + "%\",");
					}
					outputFile.write("\"" + StringUtil.toString(rs.getString("rep_rate")) + "%\",");
					outputFile.write("\"" + StringUtil.toString(rs.getString("merchant_rate")) + "%\",");
					outputFile.write("\"" + StringUtil.toString(rs.getString("tsorder")) + "\",");
					outputFile.write("\"" + StringUtil.toString(rs.getString("iso_rateplan_id")) + "\",");
					outputFile.write("\r\n");
				}

				// Close output files/streams
				outputFile.flush();
				outputFile.close();
				pstmt.close();
				rs.close();

				// Setup download
				long endQuery = System.currentTimeMillis();
				cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				cat.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				cat.debug("Download URL: " + downloadUrl);

				try
				{
					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

					// Set the compression ratio
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

					// Add ZIP entry to output stream.
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

					// Transfer bytes from the current file to the ZIP file
					// out.write(buffer, 0, in.read(buffer));

					int len;
					while ((len = in.read(buffer)) > 0)
					{
						out.write(buffer, 0, len);
					}

					// Close the current entry
					out.closeEntry();
					// Close the current file input stream
					in.close();
					out.close();
				}
				catch (IllegalArgumentException iae)
				{
					iae.printStackTrace();
				}
			}
			catch (Exception e)
			{

			}
		}
		catch (Exception e)
		{
			cat.error("Error during getISOProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return downloadUrl;
	}

	public String generateFileName(ServletContext context)
	{
		boolean isUnique = false;
		String strFileName = "";
		while (!isUnique)
		{
			strFileName = this.generateRandomNumber();
			isUnique = this.checkFileName(strFileName, context);
		}
		return strFileName;
	}

	private String generateRandomNumber()
	{
		StringBuffer s = new StringBuffer();
		// number between 1-9 because first digit must not be 0
		int nextInt = (int) ((Math.random() * 9) + 1);
		s = s.append(nextInt);

		for (int i = 0; i <= 10; i++)
		{
			// number between 0-9
			nextInt = (int) (Math.random() * 10);
			s = s.append(nextInt);
		}

		return s.toString();
	}

	private boolean checkFileName(String strFileName, ServletContext context)
	{
		boolean isUnique = true;
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);

		File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
		if (f.exists())
		{
			// duplicate file found
			cat.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
			return false;
		}

		f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
		if (f.exists())
		{
			// duplicate file found
			cat.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
			return false;
		}

		return isUnique;
	}

	/* BEGIN Release 31 - Added by LF */
	public Vector<Vector<String>> getNewRatePlansByActor(String sRefId, int nPageNumber, int nPageSize, String sSort, String sSortDirection,
			String sFilterType, String sFilterId, String sFilterName, String sFilterLevel, String sCarrierId)
			throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			Vector<String> vecTemp = new Vector<String>();
			vecTemp.add("0");
			vResults.add(vecTemp);

			if (!sSortDirection.equals("A") && !sSortDirection.equals("D"))
			{
				sSortDirection = "D";
			}

			sFilterId = sFilterId.replaceAll("%", "").replaceAll("'", "''").replaceAll("[^0-9]", "");
			sFilterName = sFilterName.replaceAll("%", "").replaceAll("'", "''");

			sSQL = new StringBuilder(2500);// This size was chosen because the average size of the whole buffer is ~2100

			if ( sCarrierId != null )
			{
				sSQL.append("WITH RatePlanAncestors AS ( ");
				sSQL.append("SELECT DISTINCT rp.RatePlanID, rp.ParentRatePlanID FROM RatePlan rp WITH (NOLOCK) ");
				sSQL.append("INNER JOIN RatePlanDetail rpd WITH (NOLOCK) ON rp.RatePlanID = rpd.RatePlanID ");
				sSQL.append("INNER JOIN Products p WITH (NOLOCK) ON rpd.ProductID = p.ID ");
				sSQL.append("INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.Carrier_ID = rc.CarrierID ");
				sSQL.append("WHERE rc.RepID = " + sCarrierId + " ");
				sSQL.append("UNION ALL ");
				sSQL.append("SELECT rp.RatePlanID, rp.ParentRatePlanID FROM RatePlan rp WITH (NOLOCK) ");
				sSQL.append("INNER JOIN RatePlanAncestors rpa ON rp.RatePlanID = rpa.ParentRatePlanID) ");
			}

			sSQL.append("SELECT TOP(" + nPageSize + ") * ");
			sSQL.append("FROM (SELECT ");
			if (sSort.equals("Type"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate, rp.OwnerID, rp.TargetEntityType, rp.Name) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.OwnerID, rp.TargetEntityType, rp.Name) AS SortD, ");
			}
			else if (sSort.equals("ID"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.RatePlanID) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.RatePlanID DESC) AS SortD, ");
			}
			else if (sSort.equals("Name"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.Name) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.Name DESC) AS SortD, ");
			}
			else if (sSort.equals("Level"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.TargetEntityType, rp.RatePlanID) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.TargetEntityType DESC, rp.RatePlanID) AS SortD, ");
			}
			else
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate, rp.OwnerID, rp.TargetEntityType, rp.Name) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rp.IsTemplate DESC, rp.OwnerID, rp.TargetEntityType, rp.Name) AS SortD, ");
			}

			sSQL.append("COUNT(*) OVER() AS TotalRows, ");
			sSQL.append("rp.RatePlanID, rp.ParentRatePlanID, rp.OwnerID, rp.Name, ISNULL(r.businessname, 'EMIDA') AS BusinessName, rp.TargetEntityType, ");
			sSQL.append("rp.CreatorLogin, rp.CreationDate, rp.LastUpdate, rp.IsTemplate, ");
			sSQL.append("(SELECT COUNT(DISTINCT rp3.RatePlanID) FROM RatePlan rp3 WITH (NOLOCK) ");
			if ( sCarrierId != null )
			{
				sSQL.append("INNER JOIN RatePlanAncestors rpa ON rp3.RatePlanID = rpa.RatePlanID ");
			}
			sSQL.append("WHERE rp.RatePlanID = rp3.ParentRatePlanID) AS AmountTemplates, ");
			//The following lines are part of a few "CASE WHEN" (one for each Rep type) to count the amount of terminals belonging to the inner joint Actor and that are using the related Plan
			sSQL.append("(CASE WHEN r.type = 3 THEN (SELECT COUNT(t.RatePlanID) FROM Terminals t WITH (NOLOCK) WHERE rp.RatePlanID = t.RatePlanID AND ");
			sSQL.append("t.merchant_id IN (SELECT merchant_id FROM merchants WITH (NOLOCK) WHERE rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE ");
			sSQL.append("iso_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id IN ");
			sSQL.append("(SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + sRefId + "))))) ");
			sSQL.append("ELSE CASE WHEN r.type = 4 THEN (SELECT COUNT(t.RatePlanID) FROM Terminals t WITH (NOLOCK) WHERE rp.RatePlanID = t.RatePlanID AND ");
			sSQL.append("t.merchant_id IN (SELECT merchant_id FROM merchants WITH (NOLOCK) WHERE rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE ");
			sSQL.append("iso_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + sRefId + ")))) ");
			sSQL.append("ELSE CASE WHEN r.type = 5 OR r.type = 2 THEN (SELECT COUNT(t.RatePlanID) FROM Terminals t WITH (NOLOCK) WHERE ");
			sSQL.append("rp.RatePlanID = t.RatePlanID AND t.merchant_id IN (SELECT merchant_id FROM merchants WITH (NOLOCK) WHERE rep_id IN ");
			sSQL.append("(SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + sRefId + "))) ");
			sSQL.append("ELSE CASE WHEN r.type = 1 THEN (SELECT COUNT(t.RatePlanID) FROM Terminals t WITH (NOLOCK) WHERE rp.RatePlanID = t.RatePlanID AND ");
			sSQL.append("t.merchant_id IN (SELECT merchant_id FROM merchants WITH (NOLOCK) WHERE rep_id = " + sRefId + ")) ");
			sSQL.append("ELSE 0 END END END END ) AS AmountTerminals, ");
			sSQL.append("(SELECT COUNT(RatePlanID) FROM Terminals WITH (NOLOCK) WHERE rp.RatePlanID = RatePlanID) AS GlobalAmountTerminals ");
			// Now the rest of the query
			sSQL.append("FROM rep_iso_rate_plan_glue g WITH (NOLOCK) ");
			sSQL.append("INNER JOIN RatePlan rp WITH (NOLOCK) ON g.iso_rate_plan_id = rp.RatePlanID ");
			sSQL.append("LEFT JOIN reps r WITH (NOLOCK) ON rp.OwnerID = r.rep_id ");
			sSQL.append("WHERE g.rep_id = " + sRefId + " AND g.isNewRatePlanModel = 1 ");

			if ( sCarrierId != null )
			{
				sSQL.append(" AND rp.RatePlanID IN (SELECT DISTINCT RatePlanID FROM RatePlanAncestors)");
			}

			if (!sFilterType.equals(""))
			{
				sSQL.append(" AND rp.IsTemplate = " + sFilterType);
			}
			if (!sFilterId.equals(""))
			{
				sSQL.append(" AND rp.RatePlanID LIKE '" + sFilterId + "%'");
			}
			if (!sFilterName.equals(""))
			{
				sSQL.append(" AND rp.Name LIKE '%" + sFilterName + "%'");
			}
			if (!sFilterLevel.equals(""))
			{
				sSQL.append(" AND rp.TargetEntityType = '" + sFilterLevel + "'");
			}
			sSQL.append(") AS Plans WHERE ");

			if (sSortDirection.equals("A"))
			{
				sSQL.append("SortA > " + ((nPageNumber - 1) * nPageSize) + " ORDER BY SortA");
			}
			else
			{
				sSQL.append("SortD > " + ((nPageNumber - 1) * nPageSize) + " ORDER BY SortD");
			}

			ps = cn.prepareStatement(sSQL.toString());
			rs = ps.executeQuery();
			if (rs.next())
			{
				vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("TotalRows"));
				vResults.clear();
				vResults.add(vecTemp);
				do
				{
					vecTemp = new Vector<String>();
					vecTemp.add(rs.getString("RatePlanID"));
					vecTemp.add(StringUtil.toString(rs.getString("ParentRatePlanID")));
					vecTemp.add(StringUtil.toString(rs.getString("OwnerID")));
					vecTemp.add(rs.getString("Name"));
					vecTemp.add(rs.getString("TargetEntityType"));
					vecTemp.add(rs.getString("BusinessName"));
					vecTemp.add(rs.getString("CreatorLogin"));
					vecTemp.add(DateUtil.formatDateNoTime(rs.getTimestamp("CreationDate")));
					vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("LastUpdate")));
					vecTemp.add(rs.getString("IsTemplate"));
					vecTemp.add(rs.getString("AmountTemplates"));
					vecTemp.add(rs.getString("AmountTerminals"));
					vecTemp.add(rs.getString("GlobalAmountTerminals"));
					vResults.add(vecTemp);
				} while (rs.next());
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getNewRatePlansByActor", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public Vector<String> getRatePlanById(String Id) throws RatePlanException
	{
		Connection cn = null;
		Vector<String> vResult = new Vector<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("getRatePlanById");
			// cat.debug(sSQL + " -- /*" + Id + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, Id);
			rs = ps.executeQuery();
			if (rs.next())
			{
				vResult.add(StringUtil.toString(rs.getString("OwnerID")));
				vResult.add(rs.getString("CreatorLogin"));
				vResult.add(rs.getString("CreationDate"));
				vResult.add(rs.getString("Name"));
				vResult.add(rs.getString("Description"));
				vResult.add(rs.getString("IsTemplate"));
				vResult.add(rs.getString("TargetEntityType"));
				vResult.add(rs.getString("AmountChildren"));
				vResult.add(rs.getString("AmountTerminals"));
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanById", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResult;
	}

	public String checkRatePlanName(String Id, String Name) throws RatePlanException
	{
		Connection cn = null;
		String sResult = "-ERROR-";
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("checkRatePlanName");
			// cat.debug(sSQL + " -- /*" + Id + ", " + Name + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, Id);
			ps.setString(2, Name.replaceAll("'", "''"));
			rs = ps.executeQuery();
			if (!rs.next())
			{
				sResult = "-VALID-";
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during checkRatePlanName", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return sResult;
	}

	@SuppressWarnings("unchecked")
	public int addRatePlan(SessionData sessionData, ServletContext context, String parentId, String Name, String Description, String TargetEntityType,
			String PlanType,
			String duplicateId)
			throws RatePlanException
	{
		Connection cn = null;
		int nResult = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("addRatePlan");
			// cat.debug(sSQL + " -- /*" + parentId + ", " + sessionData.getProperty("ref_id") + ", " + sessionData.getProperty("username") + ", [" + Name +
			// "], [" + Description + "], " + TargetEntityType + ", " + PlanType + ", " + duplicateId + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, parentId);
			ps.setString(2, ((Vector<String>) sessionData.getPropertyObj("CurrentRatePlanActor")).get(0));
			ps.setString(3, ((sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))?"CARRIER_":"") + sessionData.getProperty("username"));
			ps.setString(4, Name.replaceAll("'", "''"));
			ps.setString(5, Description.replaceAll("'", "''"));
			ps.setString(6, TargetEntityType);
			ps.setString(7, PlanType);
			ps.setString(8, duplicateId);
			ps.setString(9, DebisysConfigListener.getInstance(context));
			rs = ps.executeQuery();
			if (rs.next())
			{
				nResult = rs.getInt(1);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during addRatePlan", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return nResult;
	}

	public boolean deleteRatePlan(String Id, SessionData sessionData, ServletContext context) throws RatePlanException
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("deleteRatePlan");
			// cat.debug(sSQL + " -- /*" + Id + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, Id);
			ps.setString(2, Id);
			ps.setString(3, Id);
			ps.setString(4, Id);
			ps.setString(5, Id);
			ps.setString(6, sessionData.getProperty("username"));
			ps.setString(7, DebisysConfigListener.getInstance(context));
			if (!ps.execute())
			{
				bResult = ps.getUpdateCount() > 0;
			}
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during deleteRatePlan", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	/**
	 * @param sessionData  
	 */
	public boolean saveRatePlan(SessionData sessionData, String Id, String Name, String Description) throws RatePlanException
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("saveRatePlan");
			if (Name.length() > 64)
			{
				Name = Name.substring(0, 63);
			}
			if (Description.length() > 255)
			{
				Description = Description.substring(0, 254);
			}
			// cat.debug(sSQL + " -- /*" + "[" + Name + "], [" + Description + "], " + Id + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, Name.replaceAll("'", "''"));
			ps.setString(2, Description.replaceAll("'", "''"));
			ps.setString(3, Id);
			if (!ps.execute())
			{
				bResult = ps.getUpdateCount() > 0;
			}
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during saveRatePlan", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	public Vector<Vector<String>> getRatePlanProducts(String sId, String sBaseId, int nPageNumber, int nPageSize, String sSort, String sSortDirection,
			String sFilterId, String sFilterName, String sCarrier) throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			Vector<String> vecTemp = new Vector<String>();
			vecTemp.add("0");
			vResults.add(vecTemp);

			if (!sSortDirection.equals("A") && !sSortDirection.equals("D"))
			{
				sSortDirection = "D";
			}

			sFilterId = sFilterId.replaceAll("%", "").replaceAll("'", "''").replaceAll("[^0-9]", "");
			sFilterName = sFilterName.replaceAll("%", "").replaceAll("'", "''");

			sSQL = new StringBuilder(1000);// This size was chosen because the average size of the whole buffer is ~800

			sSQL.append("SELECT TOP(" + nPageSize + ") ((SortA + SortD) - 1) AS TotalRows, * ");
			sSQL.append("FROM (SELECT ");
			if (sSort.equals("SKU"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rpd.ProductID) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rpd.ProductID DESC) AS SortD, ");
			}
			else if (sSort.equals("Name"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY p.Description) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY p.Description DESC) AS SortD, ");
			}
			else if (sSort.equals("Amount"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY p.Amount, rpd.ProductID) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY p.Amount DESC, rpd.ProductID DESC) AS SortD, ");
			}
			else
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rpd.ProductID) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY rpd.ProductID DESC) AS SortD, ");
			}

			sSQL.append("rpd.ProductID, p.description, p.amount, pv.name AS ProviderName, c.name AS CarrierName, p.NotManagedByRatePlan, p.UseMoneyRates, editableISORatePlan, ");
			sSQL.append("rpd.ISORate, rpd.AgentRate, rpd.SubAgentRate, rpd.RepRate, rpd.MerchantRate, ");
			sSQL.append("(rpd.ISORate + rpd.AgentRate + rpd.SubAgentRate + rpd.RepRate + rpd.MerchantRate) AS SumRates, p.Fixed_Fee, ");
			sSQL.append("rpd.Enabled, rpd.TopSellersOrder, rpd.CachedReceipts,  rpd.CacheReceiptType, ");
			if (sBaseId.equals(""))
			{
				sSQL.append("1 AS ParentEnabled ");
			}
			else
			{
				sSQL.append("(SELECT [Enabled] FROM RatePlanDetail rpd2 WITH (NOLOCK) WHERE rpd2.RatePlanID = " + sBaseId
						+ " AND rpd2.ProductID = rpd.ProductID) AS ParentEnabled ");
			}
			sSQL.append("FROM RatePlanDetail rpd WITH (NOLOCK) ");
			sSQL.append("INNER JOIN Products p WITH (NOLOCK) ON rpd.ProductID = p.id ");
			sSQL.append("LEFT JOIN provider pv WITH (NOLOCK) ON p.program_id = pv.provider_id ");
			sSQL.append("LEFT JOIN carriers c WITH (NOLOCK) ON p.carrier_id = c.id ");
			if ( sCarrier != null )
			{
				sSQL.append("INNER JOIN RepCarriers rc WITH (NOLOCK) ON c.id = rc.CarrierID AND rc.RepID = " + sCarrier + " ");
			}
			sSQL.append("WHERE rpd.RatePlanID = " + sId);

			if (!sFilterId.equals(""))
			{
				sSQL.append(" AND rpd.ProductID LIKE '" + sFilterId + "%'");
			}
			if (!sFilterName.equals(""))
			{
				sSQL.append(" AND p.Description LIKE '%" + sFilterName + "%'");
			}
			sSQL.append(") AS Products WHERE ");

			if (sSortDirection.equals("A"))
			{
				sSQL.append("SortA > " + ((nPageNumber - 1) * nPageSize) + " ORDER BY SortA");
			}
			else
			{
				sSQL.append("SortD > " + ((nPageNumber - 1) * nPageSize) + " ORDER BY SortD");
			}

			ps = cn.prepareStatement(sSQL.toString());
			rs = ps.executeQuery();
			if (rs.next())
			{
				vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("TotalRows"));
				vResults.clear();
				vResults.add(vecTemp);
				do
				{
					vecTemp = new Vector<String>();
					vecTemp.add(rs.getString("ProductID"));
					vecTemp.add(rs.getString("description"));
					vecTemp.add(rs.getString("amount"));
					vecTemp.add(rs.getString("ProviderName"));
					vecTemp.add(rs.getString("CarrierName"));
					vecTemp.add(rs.getString("ISORate"));
					vecTemp.add(rs.getString("AgentRate"));
					vecTemp.add(rs.getString("SubAgentRate"));
					vecTemp.add(rs.getString("RepRate"));
					vecTemp.add(rs.getString("MerchantRate"));
					vecTemp.add(rs.getString("SumRates"));
					vecTemp.add(rs.getString("Enabled"));
					vecTemp.add(rs.getString("NotManagedByRatePlan"));
					vecTemp.add(StringUtil.toString(rs.getString("TopSellersOrder")));
					vecTemp.add(rs.getString("CachedReceipts"));
					vecTemp.add(rs.getString("UseMoneyRates"));
					vecTemp.add(rs.getString("ParentEnabled"));
					vecTemp.add(rs.getString("Fixed_Fee"));
					vecTemp.add(StringUtil.toString(rs.getString("CacheReceiptType")));
                                        vecTemp.add(rs.getString("editableISORatePlan"));
					vResults.add(vecTemp);
				} while (rs.next());
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public Vector<Vector<String>> getRatePlanProductsSearch(String sPlanId, String sParentId, String sCarrier) throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("getRatePlanProductsSearch");
			if ( sCarrier != null )
			{
				sSQL = sSQL.replaceAll("_CARRIERJOIN_", "INNER JOIN RepCarriers rc WITH (NOLOCK) ON c.ID = rc.CarrierID AND rc.RepID = " + sCarrier);
			}
			else
			{
				sSQL = sSQL.replaceAll("_CARRIERJOIN_", "");
			}
			// cat.debug(sSQL + " -- /*" + sParentId + ", " + sPlanId + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sParentId);
			ps.setString(2, sPlanId);
			rs = ps.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("id"));
				vecTemp.add(rs.getString("description"));
				vecTemp.add(rs.getString("amount"));
				vecTemp.add(rs.getString("ProviderName"));
				vecTemp.add(rs.getString("CarrierName"));
				vResults.add(vecTemp);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanProductsSearch", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public boolean addRatePlanProducts(SessionData sessionData, ServletContext context, String sPlanId, String sBaseId, String sProducts)
			throws RatePlanException
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sProducts += "0";
			sSQL = sql_bundle.getString("addRatePlanProducts");
			// cat.debug(sSQL + " -- /*" + sPlanId + ", " + sBaseId + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sPlanId);
			ps.setString(2, sBaseId);
			ps.setString(3, sProducts);
			ps.setString(4, sessionData.getProperty("username"));
			ps.setString(5, DebisysConfigListener.getInstance(context));
			if (!ps.execute())
			{
				bResult = ps.getUpdateCount() > 0;
			}
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during addRatePlanProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	public boolean deleteRatePlanProducts(SessionData sessionData, ServletContext context, String sPlanId, String sProducts) throws RatePlanException
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sProducts += "0";
			sSQL = sql_bundle.getString("deleteRatePlanProducts");
			sSQL = sSQL.replaceAll("_PRODUCTS_", sProducts);
			// cat.debug(sSQL + " -- /*" + sPlanId + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sPlanId);
			ps.setString(2, sPlanId);
			ps.setString(3, sPlanId);
			ps.setString(4, sessionData.getProperty("username"));
			ps.setString(5, DebisysConfigListener.getInstance(context));

			if (!ps.execute())
			{
				bResult = ps.getUpdateCount() > 0;
			}
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during deleteRatePlanProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	public boolean saveRatePlanProducts(String sPlanId, String sData, SessionData sessionData, ServletContext context) throws RatePlanException
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			if ( sData.length() > 0 )
			{
			cn.setAutoCommit(false);

			String[] sRatesData = sData.split("\\|");
			int nCount = 0;
			sSQL = sql_bundle.getString("saveRatePlanProducts");
			ps = cn.prepareStatement(sSQL);
			for (; nCount < sRatesData.length; nCount++)
			{
				String[] sItemData = sRatesData[nCount].split(",");
				// cat.debug(sSQL + " -- /*" + sItemData[1] + ", " + sItemData[2] + ", " + sItemData[3] + ", " + sItemData[4] + ", " + sItemData[5] + ", " +
				// sItemData[6] + ", " + sItemData[7] + ", " + sItemData[8] + ", " + sPlanId + ", " + sItemData[0] + "*/");
				ps.setString(1, sPlanId);
				ps.setString(2, sItemData[1]);
				ps.setString(3, sItemData[2]);
				ps.setString(4, sItemData[3]);
				ps.setString(5, sItemData[4]);
				ps.setString(6, sItemData[5]);
				ps.setString(7, sItemData[6]);
				if (sItemData[7].length() > 0)
				{
					ps.setString(8, sItemData[7]);
				}
				else
				{
					ps.setNull(8, java.sql.Types.VARCHAR);
				}
				ps.setString(9, sItemData[8]);
				if ( sItemData.length == 10 )
					ps.setString(10, sItemData[9]);
				else
					ps.setNull(10, java.sql.Types.VARCHAR);
				ps.setString(11, sPlanId);
				ps.setString(12, sItemData[0]);
				
				ps.setString(13, sessionData.getProperty("username"));
				ps.setString(14, DebisysConfigListener.getInstance(context));
				ps.setString(15, sPlanId);
				ps.setString(16, sItemData[0]);
				ps.addBatch();
			}
			ps.executeBatch();
			ps.close();
			cn.setAutoCommit(true);
			ps = cn.prepareStatement("UPDATE RatePlan SET LastUpdate = GETDATE() WHERE RatePlanID = " + sPlanId);
			// cat.debug("UPDATE RatePlan SET LastUpdate = GETDATE() WHERE RatePlanID = " + sPlanId);
			ps.execute();
			ps.close();
			}
			bResult = true;
		}
		catch (Exception e)
		{
			cat.error("Error during saveRatePlanProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	public Vector<Vector<String>> getRatePlanAssignments(String sPlanId, String sAccessLevel, String sRefId, boolean bIsTemplate, String sCarrier) throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = "";

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			if (sRefId.equals(""))
			{
				sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel0");
				if (sAccessLevel.equals(""))
				{
					sSQL = sSQL.replaceAll("_TYPE_", "-1 AS [type]").replaceAll("_COND_", "r.iso_id = r.rep_id").replaceAll("_GROUPBY_", "")
							+ " UNION ALL "
							+ sSQL.replaceAll("_TYPE_", "[type]").replaceAll("_COND_", "r.type = " + DebisysConstants.REP_TYPE_AGENT).replaceAll("_GROUPBY_",
									"GROUP BY r.type")
							+ " UNION ALL "
							+ sSQL.replaceAll("_TYPE_", "[type]").replaceAll("_COND_", "r.type = " + DebisysConstants.REP_TYPE_SUBAGENT).replaceAll(
									"_GROUPBY_", "GROUP BY r.type")
							+ " UNION ALL "
							+ sSQL.replaceAll("_TYPE_", "[type]").replaceAll("_COND_", "r.type = " + DebisysConstants.REP_TYPE_REP).replaceAll("_GROUPBY_",
									"GROUP BY r.type") + " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsMerchant0");
				}
				else if (sAccessLevel.equals(DebisysConstants.ISO))
				{
					if ( sCarrier == null )
					{
						sSQL = sSQL.replaceAll("_TYPE_", "-1 AS [type]").replaceAll("_COND_", "r.iso_id = r.rep_id").replaceAll("_GROUPBY_", "");
					}
					else
					{
						sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel0Carrier").replaceAll("_CARRIER_", sCarrier) + 
							sSQL.replaceAll("_TYPE_", "-1 AS [type]").replaceAll("_COND_", "r.iso_id = r.rep_id INNER JOIN CarrierISOs c ON r.rep_id = c.rep_id").replaceAll("_GROUPBY_", "OPTION (MAXDOP 1)");
					}
				}
				else if (sAccessLevel.equals(DebisysConstants.AGENT))
				{
					sSQL = sSQL.replaceAll("_TYPE_", "[type]").replaceAll("_COND_", "r.type = " + DebisysConstants.REP_TYPE_AGENT).replaceAll("_GROUPBY_",
							"GROUP BY r.type");
				}
				else if (sAccessLevel.equals(DebisysConstants.SUBAGENT))
				{
					sSQL = sSQL.replaceAll("_TYPE_", "[type]").replaceAll("_COND_", "r.type = " + DebisysConstants.REP_TYPE_SUBAGENT).replaceAll("_GROUPBY_",
							"GROUP BY r.type");
				}
				else if (sAccessLevel.equals(DebisysConstants.REP))
				{
					sSQL = sSQL.replaceAll("_TYPE_", "[type]").replaceAll("_COND_", "r.type = " + DebisysConstants.REP_TYPE_REP).replaceAll("_GROUPBY_",
							"GROUP BY r.type");
				}
				else if (sAccessLevel.equals(DebisysConstants.MERCHANT))
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsMerchant0");
				}
			}
			else if (sAccessLevel.equals(DebisysConstants.ISO))
			{
				if ( sCarrier != null )
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel1Carrier");
					if (!bIsTemplate)
					{
						sSQL += " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsLevel2Carrier")
							+ " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsLevel3Carrier") + " UNION ALL "
							+ sql_bundle.getString("getRatePlanAssignmentsMerchant1Carrier");
					}
					sSQL = sSQL.replaceAll("_CARRIER_", sCarrier);
				}
				else
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel1");
					if (!bIsTemplate)
					{
						sSQL += " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsLevel2")
							+ " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsLevel3") + " UNION ALL "
							+ sql_bundle.getString("getRatePlanAssignmentsMerchant1");
					}
				}
			}
			else if (sAccessLevel.equals(DebisysConstants.AGENT))
			{
				if ( sCarrier != null )
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel1Carrier");
					if (!bIsTemplate)
					{
						sSQL += " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsLevel2Carrier")
							+ " UNION ALL "
								+ sql_bundle.getString("getRatePlanAssignmentsMerchant2Carrier");
					}
					sSQL = sSQL.replaceAll("_CARRIER_", sCarrier);
				}
				else
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel1");
					if (!bIsTemplate)
					{
						sSQL += " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsLevel2")
							+ " UNION ALL "
								+ sql_bundle.getString("getRatePlanAssignmentsMerchant2");
					}
				}
			}
			else if (sAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				if ( sCarrier != null )
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel1Carrier");
					if (!bIsTemplate)
					{
						sSQL += " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsMerchant3Carrier");
					}
					sSQL = sSQL.replaceAll("_CARRIER_", sCarrier);
				}
				else
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsLevel1");
					if (!bIsTemplate)
					{
						sSQL += " UNION ALL " + sql_bundle.getString("getRatePlanAssignmentsMerchant3");
					}
				}
			}
			else if (sAccessLevel.equals(DebisysConstants.REP))
			{
				if ( sCarrier != null )
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsMerchant4Carrier");
					sSQL = sSQL.replaceAll("_CARRIER_", sCarrier);
				}
				else
				{
					sSQL = sql_bundle.getString("getRatePlanAssignmentsMerchant4");
				}
			}
			else if (sAccessLevel.equals(DebisysConstants.CARRIER))
			{
				sSQL = sql_bundle.getString("getRatePlanAssignmentsMerchant0Carrier");
			}
			else
			{
				sSQL = "SELECT 1 FROM Merchants WHERE 1 = 2";//Just a simple query to return no results
			}
			sSQL = sSQL.replaceAll("_REFID_", sRefId).replaceAll("_PLANID_", sPlanId);
			// cat.debug(sSQL);
			ps = cn.prepareStatement(sSQL);
			rs = ps.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				if (rs.getString("type").equals("-1"))
				{
					vecTemp.add("ISO");
				}
				else if (rs.getString("type").equals(DebisysConstants.REP_TYPE_AGENT))
				{
					vecTemp.add("AGENT");
				}
				else if (rs.getString("type").equals(DebisysConstants.REP_TYPE_SUBAGENT))
				{
					vecTemp.add("SUBAGENT");
				}
				else if (rs.getString("type").equals(DebisysConstants.REP_TYPE_REP))
				{
					vecTemp.add("REP");
				}
				else if (rs.getString("type").equals("0"))
				{
					vecTemp.add("MERCHANT");
				}
				if (rs.getString("AmountAssigned").equals("0"))
				{
					continue;
				}
				vecTemp.add(rs.getString("AmountAssigned"));
				vecTemp.add(rs.getString("EnabledMerchants"));
				vResults.add(vecTemp);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanAssignments", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	@SuppressWarnings("unchecked")
	public Hashtable<String, Vector<Vector<String>>> getRatePlanHierarchy(SessionData sessionData, String ratePlanId)
			throws RatePlanException
	{
		Connection cn = null;
		Hashtable<String, Vector<Vector<String>>> htResults = new Hashtable<String, Vector<Vector<String>>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;
		Hashtable<String, Vector<String>> htActors = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			if (sessionData.getPropertyObj("RatePlanActors") != null)
			{
				htActors = (Hashtable<String, Vector<String>>) sessionData.getPropertyObj("RatePlanActors");
			}
			else
			{
				htActors = new Hashtable<String, Vector<String>>();
			}

			sSQL = sql_bundle.getString("getRatePlanHierarchy");
			// cat.debug(sSQL + " -- /*" + ratePlanId + ", " + "*/");
			ps = cn.prepareStatement(sSQL);
			ps.setInt(1, Integer.parseInt(ratePlanId));
			if ( sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
			{
				ps.setLong(2, Long.parseLong(sessionData.getUser().getRefId()));
			}
			else
			{
				ps.setNull(2, java.sql.Types.NUMERIC);
			}
			rs = ps.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("RatePlanID"));
				vecTemp.add(rs.getString("Name"));
				vecTemp.add(StringUtil.toString(rs.getString("ParentRatePlanID")));
				vecTemp.add(rs.getString("TargetEntityType"));
				vecTemp.add(rs.getString("IsTemplate"));
				vecTemp.add(StringUtil.toString(rs.getString("OwnerID")));
				vecTemp.add(StringUtil.toString(rs.getString("BusinessName")));
				switch (Integer.parseInt(StringUtil.toString(rs.getString("Type"))))
				{
					case 1:// Rep
						vecTemp.add(DebisysConstants.REP);
						break;
					case 2:// ISO 3 Levels
						vecTemp.add(DebisysConstants.ISO);
						break;
					case 3:// ISO 5 Levels
						vecTemp.add(DebisysConstants.ISO);
						break;
					case 4:// Agent
						vecTemp.add(DebisysConstants.AGENT);
						break;
					case 5:// SubAgent
						vecTemp.add(DebisysConstants.SUBAGENT);
						break;
					default:
						vecTemp.add(null);// It should never get to this case (except for the root node), but then let's induce a crash just in case
						break;
				}

				switch (Integer.parseInt(rs.getString("ChainLevel")))
				{
					case 2:
						vecTemp.add(DebisysConstants.DIST_CHAIN_3_LEVEL);
						break;
					case 3:
						vecTemp.add(DebisysConstants.DIST_CHAIN_5_LEVEL);
						break;
					default:
						vecTemp.add(null);// It should never get to this case (except for the root node), but then let's induce a crash just in case
						break;
				}
				vecTemp.add(StringUtil.toString(rs.getString("ParentOwnerID")));

				if (!htActors.containsKey(vecTemp.get(5)))
				{
					Vector<String> vT = new Vector<String>();
					vT.add(vecTemp.get(6));// BusinessName
					vT.add(vecTemp.get(7));// Type
					vT.add(vecTemp.get(8));// Chain Level
					vT.add(vecTemp.get(9));// Parent ID
					htActors.put(vecTemp.get(5), vT);
				}

				if (htResults.size() == 0) // If this is the Parent Node
				{
					htResults.put("0", new Vector<Vector<String>>());
					htResults.get("0").add(vecTemp);
				}
				else
				{
					if (htResults.containsKey(vecTemp.get(2)))
					{
						htResults.get(vecTemp.get(2)).add(vecTemp);
					}
					else
					{
						htResults.put(vecTemp.get(2), new Vector<Vector<String>>());
						htResults.get(vecTemp.get(2)).add(vecTemp);
					}
				}
			}
			sessionData.setPropertyObj("RatePlanActors", htActors);
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanHierarchy", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return htResults;
	}

	public String RenderHierarchyItem(String sRefId, Hashtable<String, Vector<Vector<String>>> htData, Vector<Vector<String>> vItems, int nLevel,SessionData sessionData)
	{
		String sResult = "";
		Hashtable<String, String> htLevels = new Hashtable<String, String>();
		htLevels.put(DebisysConstants.ISO, "ISO");
		htLevels.put(DebisysConstants.AGENT, "AGENT");
		htLevels.put(DebisysConstants.SUBAGENT, "SUBAGENT");
		htLevels.put(DebisysConstants.REP, "REP");

		try
		{
			if (vItems != null)
			{
				Iterator<Vector<String>> itTemp = vItems.iterator();
				String sPadding = "";
				for (int i = 1; i < nLevel; i++)
				{
					sPadding += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				}
				if (nLevel >= 1)
				{
					sPadding = sPadding + "<img src=\"/support/images/hierarchy.png\" style=\"vertical-align:middle;\">";
				}
				
				while (itTemp.hasNext())
				{
					String sNameTmp = "";
					Vector<String> vTemp = itTemp.next();
					sResult += "<tr class=\"Hierarchy" + nLevel + "\" style=\"height:25px;\">";
					while (vTemp.get(1).length() > 40)
					{
						sNameTmp += vTemp.get(1).substring(0, 40) + "<br/>";
						vTemp.set(1, vTemp.get(1).substring(40));
					}
					sNameTmp += vTemp.get(1);
					sResult += "<td nowrap>" + sPadding + "[" + vTemp.get(0) + "]&nbsp;" + sNameTmp + "</td>";
					sResult += "<td>&nbsp;"
							+ (vTemp.get(4).equals("1") ? Languages.getString("jsp.admin.rateplans.TypeTemplate", sessionData.getLanguage()) : Languages
									.getString("jsp.admin.rateplans.TypePlan",sessionData.getLanguage())) + "&nbsp;</td>";
					sResult += "<td align=\"center\">&nbsp;" + vTemp.get(3) + "&nbsp;</td>";
					if (vTemp.get(5).equals(""))
					{
						sResult += "<td align=\"center\" nowrap>EMIDA</td>";
					}
					else
					{
						sResult += "<td nowrap>&nbsp;" + htLevels.get(vTemp.get(7)) + "&nbsp;-&nbsp;(" + vTemp.get(5) + ")&nbsp;" + vTemp.get(6)
								+ "&nbsp;</td>";
					}

					if (!sRefId.equals(vTemp.get(5)) && !vTemp.get(5).equals(""))
					{
						sResult += "<td align=\"center\"><a href=\"javascript:DoBrowse(" + vTemp.get(5) + ", " + vTemp.get(7) + ", " + vTemp.get(8) + ", "
								+ vTemp.get(0)
								+ ")\"><img src=\"/support/images/greenarrow.png\" border=\"0\"></a></td>";
					}
					else
					{
						sResult += "<td align=\"center\"><img src=\"/support/images/grayarrow.png\"></td>";
					}

					sResult += "</tr>\n";
					sResult += this.RenderHierarchyItem(sRefId, htData, htData.get(vTemp.get(0)), nLevel + 1,sessionData);
					vTemp = null;
				}
				itTemp = null;
			}
		}
		catch (Exception e)
		{
			cat.error("Error during RenderHierarchyItem", e);
		}

		return sResult;
	}

	public Vector<Vector<String>> getRatePlanChildrenHierarchy(String ownerId, String ratePlanId, String sFilter, String sCarrier) throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("getRatePlanChildrenHierarchy");
			// cat.debug(sSQL + " -- /*" + ratePlanId + "*/");
			if (sFilter.length() > 0)
			{
				sSQL = sSQL.replaceAll("_FILTER_", "'" + sFilter + "'");
			}
			else
			{
				sSQL = sSQL.replaceAll("_FILTER_", "NULL");
			}
			ps = cn.prepareStatement(sSQL);
			if (ownerId.equals("0"))
			{
				ps.setNull(1, java.sql.Types.NUMERIC);
			}
			else
			{
				ps.setLong(1, Long.parseLong(ownerId));
			}
			ps.setInt(2, Integer.parseInt(ratePlanId));
			if ( sCarrier == null )
			{
				ps.setNull(3, java.sql.Types.NUMERIC);
			}
			else
			{
				ps.setLong(3, Long.parseLong(sCarrier));
			}
			rs = ps.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("RatePlanID"));
				vecTemp.add(rs.getString("Name"));
				vecTemp.add(StringUtil.toString(rs.getString("ParentRatePlanID")));
				vecTemp.add(rs.getString("TargetEntityType"));
				vecTemp.add(rs.getString("IsTemplate"));
				vecTemp.add(StringUtil.toString(rs.getString("OwnerID")));
				vecTemp.add(rs.getString("BusinessName"));
				switch (Integer.parseInt(rs.getString("Type")))
				{
					case 1:// Rep
						vecTemp.add(DebisysConstants.REP);
						break;
					case 2:// ISO 3 Levels
						vecTemp.add(DebisysConstants.ISO);
						break;
					case 3:// ISO 5 Levels
						vecTemp.add(DebisysConstants.ISO);
						break;
					case 4:// Agent
						vecTemp.add(DebisysConstants.AGENT);
						break;
					case 5:// SubAgent
						vecTemp.add(DebisysConstants.SUBAGENT);
						break;
					default:
						vecTemp.add(null);// It should never get to this case, but then let's induce a crash just in case
						break;
				}
				vecTemp.add(rs.getString("AmountChildren"));
				vResults.add(vecTemp);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanChildrenHierarchy", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public Vector<Vector<String>> getRatePlanUnionProducts(String ratePlanIDs, int nPageNumber, int nPageSize, String sSort, String sSortDirection,
			String sFilterId, String sFilterName, String sCarrier) throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			Vector<String> vecTemp = new Vector<String>();
			vecTemp.add("0");
			vResults.add(vecTemp);

			if (!sSortDirection.equals("A") && !sSortDirection.equals("D"))
			{
				sSortDirection = "D";
			}

			sFilterId = sFilterId.replaceAll("%", "").replaceAll("'", "''").replaceAll("[^0-9]", "");
			sFilterName = sFilterName.replaceAll("%", "").replaceAll("'", "''");

			sSQL = new StringBuilder(1000);// This size was chosen because the average size of the whole buffer is ~800

			sSQL.append("SELECT TOP(" + nPageSize + ") ((SortA + SortD) - 1) AS TotalRows, * ");
			sSQL.append("FROM (SELECT ");
			if (sSort.equals("SKU"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY ID) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY ID DESC) AS SortD, ");
			}
			else if (sSort.equals("Name"))
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY Description) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY Description DESC) AS SortD, ");
			}
			else
			{
				sSQL.append("ROW_NUMBER() OVER(ORDER BY ID) AS SortA, ");
				sSQL.append("ROW_NUMBER() OVER(ORDER BY ID DESC) AS SortD, ");
			}

			sSQL.append("* FROM (SELECT DISTINCT ");
			sSQL.append("p.ID, p.description, p.editableISORatePlan ");
			sSQL.append("FROM RatePlanDetail rpd WITH (NOLOCK) ");
			sSQL.append("INNER JOIN Products p WITH (NOLOCK) ON rpd.ProductID = p.id ");
			if ( sCarrier != null )
			{
				sSQL.append("INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.Carrier_ID = rc.CarrierID AND rc.RepID = " + sCarrier + " ");
			}
			sSQL.append("WHERE rpd.RatePlanID IN (" + ratePlanIDs + ")");

			if (!sFilterId.equals(""))
			{
				sSQL.append(" AND p.ID LIKE '" + sFilterId + "%'");
			}
			if (!sFilterName.equals(""))
			{
				sSQL.append(" AND p.Description LIKE '%" + sFilterName + "%'");
			}
			sSQL.append(") AS InnerProducts ");
			sSQL.append(") AS Products WHERE ");

			if (sSortDirection.equals("A"))
			{
				sSQL.append("SortA > " + ((nPageNumber - 1) * nPageSize) + " ORDER BY SortA");
			}
			else
			{
				sSQL.append("SortD > " + ((nPageNumber - 1) * nPageSize) + " ORDER BY SortD");
			}

			ps = cn.prepareStatement(sSQL.toString());
			rs = ps.executeQuery();
			if (rs.next())
			{
				vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("TotalRows"));
				vResults.clear();
				vResults.add(vecTemp);
				do
				{
					vecTemp = new Vector<String>();
					vecTemp.add(rs.getString("ID"));
					vecTemp.add(rs.getString("description"));
                                        vecTemp.add(rs.getString("editableISORatePlan"));
					vResults.add(vecTemp);
				} while (rs.next());
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanUnionProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public Vector<Vector<String>> getRatePlanParentUnionProducts(String ratePlanIDs, int nPageNumber, int nPageSize, String sSort, String sSortDirection,
			String sFilterId, String sFilterName, String sCarrier) throws RatePlanException
	{
		String s = "SELECT DISTINCT ParentRatePlanID FROM RatePlan WITH (NOLOCK) WHERE RatePlanID IN (" + ratePlanIDs + ")";
		return this.getRatePlanUnionProducts(s, nPageNumber, nPageSize, sSort, sSortDirection, sFilterId, sFilterName, sCarrier);
	}

	public Vector<Vector<String>> getRatePlanPlatformUnionProducts(String sOwnerId, int nPageNumber, int nPageSize, String sSort, String sSortDirection,
			String sFilterId, String sFilterName, String sCarrier) throws RatePlanException
	{
		String s = "SELECT RatePlanID FROM RatePlan WITH (NOLOCK) WHERE OwnerID" + (sOwnerId.equals("0") ? " IS NULL" : " = " + sOwnerId);
		return this.getRatePlanUnionProducts(s, nPageNumber, nPageSize, sSort, sSortDirection, sFilterId, sFilterName, sCarrier);
	}

	public int generateRatePlanWorkHierarchy(String sessionId, String ratePlanIDs, String productIDs, String sEnableAddedProducts, String sCopyProduct)
			throws RatePlanException
	{
		Connection cn = null;
		int nResult = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("insertRatePlanWorkItem");
			ps = cn.prepareStatement(sSQL);
			for (String item : ratePlanIDs.replaceAll(",0", "").split(","))
			{
				ps.setString(1, sessionId);
				ps.setInt(2, Integer.parseInt(item));
				ps.setNull(3, java.sql.Types.INTEGER);
				ps.setNull(4, java.sql.Types.BIT);
				ps.setNull(5, java.sql.Types.INTEGER);
				ps.setNull(6, java.sql.Types.FLOAT);
				ps.setNull(7, java.sql.Types.FLOAT);
				ps.setNull(8, java.sql.Types.FLOAT);
				ps.setNull(9, java.sql.Types.FLOAT);
				ps.setNull(10, java.sql.Types.FLOAT);
				ps.setNull(11, java.sql.Types.FLOAT);
				ps.setNull(12, java.sql.Types.NUMERIC);
				ps.addBatch();
			}

			if (productIDs.indexOf("|") > -1)
			{
				for (String productItem : productIDs.replaceAll("\\|0", "").split("\\|"))
				{
					String[] vItem = productItem.split(",");
					ps.setString(1, sessionId);
					ps.setNull(2, java.sql.Types.INTEGER);
					ps.setInt(3, Integer.parseInt(vItem[0]));
					if (sEnableAddedProducts.equals("1"))
					{
						ps.setBoolean(4, true);
					}
					else
					{
						ps.setBoolean(4, false);
					}
					if (sCopyProduct.length() > 0)
					{
						ps.setString(5, sCopyProduct);
					}
					else
					{
						ps.setNull(5, java.sql.Types.INTEGER);
					}
					ps.setFloat(6, Float.parseFloat(vItem[1]));
					ps.setFloat(7, Float.parseFloat(vItem[2]));
					ps.setFloat(8, Float.parseFloat(vItem[3]));
					ps.setFloat(9, Float.parseFloat(vItem[4]));
					ps.setFloat(10, Float.parseFloat(vItem[5]));
					ps.setFloat(11, Float.parseFloat(vItem[6]));
					if (vItem[7].length() > 0)
					{
						ps.setString(12, vItem[7]);
					}
					else
					{
						ps.setNull(12, java.sql.Types.NUMERIC);
					}
					ps.addBatch();
				}
			}
			else
			{
				for (String item : productIDs.replaceAll(",0", "").split(","))
				{
					ps.setString(1, sessionId);
					ps.setNull(2, java.sql.Types.INTEGER);
					ps.setInt(3, Integer.parseInt(item));
					if (sEnableAddedProducts.equals("1"))
					{
						ps.setBoolean(4, true);
					}
					else
					{
						ps.setBoolean(4, false);
					}
					ps.setNull(5, java.sql.Types.INTEGER);
					ps.setNull(6, java.sql.Types.FLOAT);
					ps.setNull(7, java.sql.Types.FLOAT);
					ps.setNull(8, java.sql.Types.FLOAT);
					ps.setNull(9, java.sql.Types.FLOAT);
					ps.setNull(10, java.sql.Types.FLOAT);
					ps.setNull(11, java.sql.Types.FLOAT);
					ps.setNull(12, java.sql.Types.NUMERIC);
					ps.addBatch();
				}
			}
			ps.executeBatch();

			sSQL = sql_bundle.getString("generateRatePlanWorkHierarchy");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sessionId);
			rs = ps.executeQuery();
			rs.next();
			nResult = rs.getInt("PlansCount");
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during generateRatePlanWorkHierarchy", e);
			try
			{
				if ( cn != null )
				{
					ps = cn.prepareStatement("DELETE RatePlanWorkTable WHERE SessionID = ?");
					ps.setString(1, sessionId);
					ps.executeUpdate();
				}
			}
			catch (Exception ee)
			{
				cat.error("Error during generateRatePlanWorkHierarchy", ee);
			}
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return nResult;
	}

	public int checkRatePlanWorkTableStatus(String sessionId) throws RatePlanException
	{
		Connection cn = null;
		int nResult = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("checkRatePlanWorkTableCount");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sessionId);
			rs = ps.executeQuery();
			rs.next();
			nResult = rs.getInt("Count");
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during checkRatePlanWorkTableStatus", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return nResult;
	}

	public boolean countRootRatePlans(String ratePlanIDs) throws RatePlanException
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("countRootRatePlans");
			sSQL = sSQL.replaceAll("_PLANS_", ratePlanIDs);
			ps = cn.prepareStatement(sSQL);
			rs = ps.executeQuery();
			rs.next();
			bResult = (rs.getInt("Count") > 0);
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during countRootRatePlans", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	/**
	 * @param action
	 *        Valid values are "ADD", "REMOVE", "UPDATE"
	 */
	public String generateRatePlanGlobalOperation(String sessionId, String action, String sEnableAddedProducts, String sCopyProduct, String sAccessLevel,
			String sExcludePlanType, SessionData sessionData)
			throws RatePlanException
	{
		Connection cn = null;
		StringBuilder sResult = new StringBuilder(10000);// We need to reserve a lot of space because this function returns a huge report
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;
		Hashtable<String, String> htPlans = new Hashtable<String, String>();
		Hashtable<String, String> htProducts = new Hashtable<String, String>();
		Hashtable<String, String> htResults = new Hashtable<String, String>();

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("generateRatePlanGlobalOperation");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sessionId);
			ps.setBoolean(2, (sessionData.getUser().isIntranetUser()
					&& sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
							DebisysConstants.INTRANET_PERMISSION_ALLOW_EXCEED_RATES)));
			rs = ps.executeQuery();
			while (rs.next())
			{
				htPlans.put(rs.getString("RatePlanID"), rs.getString("IsTemplate") + rs.getString("Name"));
			}
			rs.close();

			ps.getMoreResults();
			rs = ps.getResultSet();
			while (rs.next())
			{
				htProducts.put(rs.getString("ProductID"), rs.getString("Description"));
			}
			rs.close();

			ps.getMoreResults();
			rs = ps.getResultSet();
			while (rs.next())
			{
				htResults.put(rs.getString("RatePlanID") + "-" + rs.getString("ProductID"), rs.getString("Enabled") + "," + rs.getString("DontAdd") + ","
						+ rs.getString("ParentEnabled") + "," + rs.getString("IsTemplate"));
			}
			rs.close();
			ps.close();

			sResult.append("<span style=\"width:100%;text-align:left;\">" + Languages.getString("jsp.admin.rateplans.GlobalUpdateSummary", sessionData.getLanguage())
					+ "</span><br/><br/>");
			if (action.equals("ADD"))
			{
				sResult.append("<input type=\"button\" id=\"btnBack\" value=\"&lt;&lt; " + Languages.getString("jsp.admin.rateplans.Back", sessionData.getLanguage())
						+ "\" onclick=\"DoReturnToAddProducts();\">&nbsp;&nbsp;<input type=\"button\" id=\"btnFinish\" value=\""
						+ Languages.getString("jsp.admin.rateplans.Finish", sessionData.getLanguage()) + " &gt;&gt;\" onclick=\"DoAddProducts();\"><br/><br/>");
			}
			else if (action.equals("REMOVE"))
			{
				sResult.append("<input type=\"button\" id=\"btnBack\" value=\"&lt;&lt; " + Languages.getString("jsp.admin.rateplans.Back", sessionData.getLanguage())
						+ "\" onclick=\"DoReturnToRemoveProducts();\">&nbsp;&nbsp;<input type=\"button\" id=\"btnFinish\" value=\""
						+ Languages.getString("jsp.admin.rateplans.Finish", sessionData.getLanguage()) + " &gt;&gt;\" onclick=\"DoRemoveProducts();\"><br/><br/>");
			}
			else if (action.equals("UPDATE"))
			{
				sResult.append("<input type=\"button\" id=\"btnBack\" value=\"&lt;&lt; " + Languages.getString("jsp.admin.rateplans.Back", sessionData.getLanguage())
						+ "\" onclick=\"DoReturnToUpdatePlans();\">&nbsp;&nbsp;<input type=\"button\" id=\"btnFinish\" value=\""
						+ Languages.getString("jsp.admin.rateplans.Finish", sessionData.getLanguage()) + " &gt;&gt;\" onclick=\"DoUpdateRates();\"><br/><br/>");
			}

			Enumeration<String> ePlans = htPlans.keys();
			int nCount = 0;
			while (ePlans.hasMoreElements())
			{
				String sRatePlanID = ePlans.nextElement();// This one will be used in the iteration of products below
				String sFullName = htPlans.get(sRatePlanID).substring(1);// Skip the first char which indicates if the item is a Template or Plan
				String sBreakName = "";
				while (sFullName.length() > 40)
				{
					sBreakName += sFullName.substring(0, 40) + "<br/>";
					sFullName = sFullName.substring(40);
				}
				sBreakName += sFullName;

				sResult.append("<TABLE CELLSPACING=\"1\" CELLPADDING=\"1\" BORDER=\"0\">");
				sResult.append("<TR CLASS=\"SectionTopBorder\"><TD COLSPAN=\"3\" CLASS=\"rowhead2\" STYLE=\"text-align:left;\"><UL><LI>(" + sRatePlanID + ")&nbsp;&nbsp;<B>"
						+ sBreakName + "</B></LI></UL></TD></TR>");
				sResult.append("<TR CLASS=\"SectionTopBorder\"><TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
						+ Languages.getString("jsp.admin.rateplans.SKU", sessionData.getLanguage()) + "</TD><TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
						+ Languages.getString("jsp.admin.rateplans.Name", sessionData.getLanguage()) + "</TD><TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
						+ Languages.getString("jsp.admin.rateplans.Action", sessionData.getLanguage()) + "</TD></TR>");

				Enumeration<String> eProducts = htProducts.keys();
				while (eProducts.hasMoreElements())
				{
					String sProductID = eProducts.nextElement();
					sResult.append("<TR CLASS=\"row" + ((nCount++ % 2) + 1) + "\"><TD>&nbsp;" + sProductID + "&nbsp;</TD><TD NOWRAP>&nbsp;"
							+ htProducts.get(sProductID) + "&nbsp;</TD><TD NOWRAP><B>");

					// If the product exists in the plan being iterated
					if (htResults.get(sRatePlanID + "-" + sProductID) != null)
					{
						if (action.equals("ADD"))
						{
							String[] sFlags = htResults.get(sRatePlanID + "-" + sProductID).split(",");
							// sFlags[0] is the Enabled status of the RatePlanDetail Product
							// sFlags[1] is used when adding a new product from the Platform, 1 means Don't Add
							// sFlags[2] is the Enabled status from the Parent Plan
							// sFlags[3] indicates if the item is a Template or Plan
							if (!sFlags[3].equals(sExcludePlanType))
							{
								if (sFlags[1].equals("1"))
								{
									if (sCopyProduct.length() > 0)
									{
										sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
												+ Languages.getString("jsp.admin.rateplans.ProductCopyNotAdded", sessionData.getLanguage()));
									}
									else
									{
										sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
												+ Languages.getString("jsp.admin.rateplans.ProductNotAdded", sessionData.getLanguage()));
									}
								}
								else
								{
									if (sCopyProduct.length() > 0)
									{
										sResult.append("<img src=\"/support/images/approved-icon.png\" style=\"vertical-align:middle;\">&nbsp;"
												+ Languages.getString("jsp.admin.rateplans.ProductCopyExists", sessionData.getLanguage()));
									}
									else
									{
										sResult.append("<img src=\"/support/images/approved-icon.png\" style=\"vertical-align:middle;\">&nbsp;"
												+ Languages.getString("jsp.admin.rateplans.ProductExists", sessionData.getLanguage()));
										if (!sFlags[0].equals(sEnableAddedProducts) && sFlags[2].equals("1"))
										{
											sResult.append("<br/>" + Languages.getString("jsp.admin.rateplans.EnableStatusUpdate", sessionData.getLanguage()));
										}
									}
								}
							}
							else
							{// Else the user selected to Process Only Templates
								sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
										+ Languages.getString("jsp.admin.rateplans.ProductNotAdded_TemplateOnly", sessionData.getLanguage()));
							}
						}
						else if (action.equals("REMOVE"))
						{
							sResult.append("<img src=\"/support/images/approved-icon.png\" style=\"vertical-align:middle;\">&nbsp;"
									+ Languages.getString("jsp.admin.rateplans.ProductWillBeRemoved", sessionData.getLanguage()));
						}
						else if (action.equals("UPDATE"))
						{
							Vector<Vector<String>> vData = this.generateUpdateRatesChange(sRatePlanID, sProductID, sEnableAddedProducts, sCopyProduct,
									sAccessLevel);
							String sFlag = vData.get(0).get(0);
							if (sFlag.equals("-1"))
							{
								sResult.append(Languages.getString("jsp.admin.rateplans.UnexpectedError", sessionData.getLanguage()));
							}
							else
							{
								if (sFlag.equals("0"))
								{
									sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
											+ Languages.getString("jsp.admin.rateplans.RatesUpdateFAIL", sessionData.getLanguage()));
								}
								else
								{
									sResult.append("<img src=\"/support/images/approved-icon.png\" style=\"vertical-align:middle;\">&nbsp;"
											+ Languages.getString("jsp.admin.rateplans.RatesUpdateOK", sessionData.getLanguage()));
								}

								sResult.append("<BR/><BR/><TABLE CELLSPACING=\"1\" CELLPADDING=\"1\" BORDER=\"0\">");
								sResult.append("<TR CLASS=\"SectionTopBorder\">");
								sResult.append("<TD></TD>");
								if ((sessionData.getUser().isIntranetUser() && 
									sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
									|| sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
									)
								{
									sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
											+ Languages.getString("jsp.admin.rateplans.EmidaRate", sessionData.getLanguage()) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
											+ Languages.getString("jsp.admin.rateplans.ISORate", sessionData.getLanguage()) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
											+ Languages.getString("jsp.admin.rateplans.AgentRate", sessionData.getLanguage()) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT)
										|| sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
											+ Languages.getString("jsp.admin.rateplans.SubAgentRate", sessionData.getLanguage()) + "</TD>");
								}
								sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
										+ Languages.getString("jsp.admin.rateplans.RepRate", sessionData.getLanguage()) + "</TD>");
								sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\">"
										+ Languages.getString("jsp.admin.rateplans.MerchantRate", sessionData.getLanguage()) + "</TD>");
								sResult.append("</TR>");
								// First row is the Old Rates
								sResult.append("<TR CLASS=\"RatePlanRow00\">");
								sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\" NOWRAP>&nbsp;"
										+ Languages.getString("jsp.admin.rateplans.OldRates", sessionData.getLanguage()) + "&nbsp;</TD>");
								if ((sessionData.getUser().isIntranetUser() && 
									sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
									|| sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
									)
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(1).get(0)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(1).get(1)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(1).get(2)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT)
										|| sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(1).get(3)) + "</TD>");
								}
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(1).get(4)) + "</TD>");
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(1).get(5)) + "</TD>");
								sResult.append("</TR>");
								// Second row is the Increments/Decrements
								sResult.append("<TR CLASS=\"RatePlanRow10\">");
								sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\" NOWRAP>&nbsp;"
										+ Languages.getString("jsp.admin.rateplans.ChangeToApply", sessionData.getLanguage()) + "&nbsp;</TD>");
								if ((sessionData.getUser().isIntranetUser() && 
									sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
									|| sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
									)
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(2).get(0)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(2).get(1)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(2).get(2)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT)
										|| sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(2).get(3)) + "</TD>");
								}
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(2).get(4)) + "</TD>");
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(2).get(5)) + "</TD>");
								sResult.append("</TR>");
								// Third row is the New Rates
								sResult.append("<TR CLASS=\"RatePlanRow00\">");
								sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\" NOWRAP>&nbsp;"
										+ Languages.getString("jsp.admin.rateplans.NewRates", sessionData.getLanguage()) + "&nbsp;</TD>");
								if ((sessionData.getUser().isIntranetUser() && 
									sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
									|| sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
									)
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(3).get(0)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(3).get(1)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(3).get(2)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT)
										|| sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(3).get(3)) + "</TD>");
								}
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(3).get(4)) + "</TD>");
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(3).get(5)) + "</TD>");
								sResult.append("</TR>");
								// Fourth row is the Minimum Values
								sResult.append("<TR CLASS=\"RatePlanRow10\">");
								sResult.append("<TD CLASS=\"rowhead2\" STYLE=\"text-align:center;\" NOWRAP>&nbsp;"
										+ Languages.getString("jsp.admin.rateplans.MinimumValue", sessionData.getLanguage()) + "&nbsp;</TD>");
								if ((sessionData.getUser().isIntranetUser() && 
									sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
									|| sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
									)
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + /*NumberUtil.formatAmount(vData.get(4).get(0)) +*/ "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(4).get(1)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(4).get(2)) + "</TD>");
								}
								if (sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT)
										|| sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER))
								{
									sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(4).get(3)) + "</TD>");
								}
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(4).get(4)) + "</TD>");
								sResult.append("<TD STYLE=\"text-align:right;\">" + NumberUtil.formatAmount(vData.get(4).get(5)) + "</TD>");
								sResult.append("</TR>");
								sResult.append("</TABLE>");
							}
							vData = null;
						}
					}
					else
					{
						if (action.equals("ADD"))
						{
							if (sCopyProduct.length() > 0)
							{
								if ( sCopyProduct.equals(sProductID) )
								{
									sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
											+ Languages.getString("jsp.admin.rateplans.ProductCopyCantSelfAdded", sessionData.getLanguage()));
								}
								else
								{
									sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
											+ Languages.getString("jsp.admin.rateplans.ProductCopyNotExists", sessionData.getLanguage()));
								}
							}
							else
							{
								if (!htPlans.get(sRatePlanID).substring(0, 1).equals(sExcludePlanType))
								{
									sResult.append("<img src=\"/support/images/approved-icon.png\" style=\"vertical-align:middle;\">&nbsp;"
											+ Languages.getString("jsp.admin.rateplans.ProductWillBeAdded", sessionData.getLanguage()));
								}
								else
								{// Else the user selected to Process Only Templates
									sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
											+ Languages.getString("jsp.admin.rateplans.ProductNotAdded_TemplateOnly", sessionData.getLanguage()));
								}
							}
						}
						else if (action.equals("REMOVE"))
						{
							sResult.append("<img src=\"/support/images/riskSite.jpg\" style=\"vertical-align:middle;\">&nbsp;"
									+ Languages.getString("jsp.admin.rateplans.ProductNotExists", sessionData.getLanguage()));
						}
						else if (action.equals("UPDATE"))
						{
							sResult.append(Languages.getString("jsp.admin.rateplans.ProductNotExists", sessionData.getLanguage()));
						}
					}
					sResult.append("</B></TD></TR>");
				}
				sResult.append("</TABLE><BR/><BR/>");
			}
		}
		catch (Exception e)
		{
			cat.error("Error during generateRatePlanGlobalOperation", e);
			try
			{
				if ( cn != null )
				{
					ps = cn.prepareStatement("DELETE RatePlanWorkTable WHERE SessionID = ?");
					ps.setString(1, sessionId);
					ps.executeUpdate();
				}
			}
			catch (Exception ee)
			{
				cat.error("Error during generateRatePlanGlobalOperation", ee);
			}
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return sResult.toString();
	}

	public Vector<Vector<String>> getSearchProducts(String sSKU, String sName, String sCarrier) throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSKU = sSKU.replaceAll("%", "") + "%";
			sName = sName.replaceAll("%", "") + "%";
			sSQL = sql_bundle.getString("getSearchProducts");
			if ( sCarrier != null )
			{
				sSQL = sSQL.replaceAll("_CARRIERJOIN_", "INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.Carrier_ID = rc.CarrierID AND rc.RepID = " + sCarrier);
			}
			else
			{
				sSQL = sSQL.replaceAll("_CARRIERJOIN_", "");
			}
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sSKU);
			ps.setString(2, sName);
			rs = ps.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("ID"));
				vecTemp.add(rs.getString("Description"));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("Amount")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("Buy_Rate")));
				vecTemp.add(Integer.toString(rs.getInt("NotManagedByRatePlan")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("StandardISORate")));
				vecTemp.add(Integer.toString(rs.getInt("UseMoneyRates")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("Fixed_Fee")));
				vResults.add(vecTemp);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getSearchProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public Vector<Vector<String>> getSearchCopyProducts(String sSKU, String sName, String ratePlanIDs, String sCarrier) throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSKU = sSKU.replaceAll("%", "") + "%";
			sName = sName.replaceAll("%", "") + "%";
			sSQL = sql_bundle.getString("getSearchCopyProducts");
			sSQL = sSQL.replaceAll("_PLANS_", ratePlanIDs);
			if ( sCarrier != null )
			{
				sSQL = sSQL.replaceAll("_CARRIERJOIN_", "INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.Carrier_ID = rc.CarrierID AND rc.RepID = " + sCarrier);
			}
			else
			{
				sSQL = sSQL.replaceAll("_CARRIERJOIN_", "");
			}
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sSKU);
			ps.setString(2, sName);
			rs = ps.executeQuery();
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("ID"));
				vecTemp.add(rs.getString("Description"));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("Amount")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("Buy_Rate")));
				vResults.add(vecTemp);
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getSearchCopyProducts", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public Vector<Vector<String>> generateUpdateRatesChange(String sPlanId, String sProductId, String sRates, String sMinValues, String sAccessLevel)
			throws RatePlanException
	{
		Connection cn = null;
		Vector<Vector<String>> vResults = new Vector<Vector<String>>();
		Vector<String> vTmp;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;
		float oldEmida, oldISO, oldAgent, oldSubAgent, oldRep, oldMerchant;
		float newEmida, newISO, newAgent, newSubAgent, newRep, newMerchant;
		float deltaISO, deltaAgent, deltaSubAgent, deltaRep, deltaMerchant;
		float minISO, minAgent, minSubAgent, minRep, minMerchant;
		float currentDelta = 0, buyRate, refAgentRate;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("getRatePlanDetailItem");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sPlanId);
			ps.setString(2, sProductId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				newISO = oldISO = rs.getFloat("ISORate");
				newAgent = oldAgent = rs.getFloat("AgentRate");
				newSubAgent = oldSubAgent = rs.getFloat("SubAgentRate");
				newRep = oldRep = rs.getFloat("RepRate");
				newMerchant = oldMerchant = rs.getFloat("MerchantRate");
				refAgentRate = rs.getFloat("RefAgentRate");
				buyRate = rs.getFloat("BuyRate");
				newEmida = oldEmida = buyRate - (newISO + newAgent + newSubAgent + newRep + newMerchant + refAgentRate);
			}
			else
			{
				vTmp = new Vector<String>();
				vTmp.add("-1");
				vResults.add(vTmp);
				rs.close();
				ps.close();
				return vResults;
			}
			rs.close();
			ps.close();

			String[] vItems = sRates.split(",");
			deltaISO = Float.parseFloat(vItems[0]);
			deltaAgent = Float.parseFloat(vItems[1]);
			deltaSubAgent = Float.parseFloat(vItems[2]);
			deltaRep = Float.parseFloat(vItems[3]);
			deltaMerchant = Float.parseFloat(vItems[4]);

			vItems = sMinValues.split(",");
			minISO = Float.parseFloat(vItems[0]);
			minAgent = Float.parseFloat(vItems[1]);
			minSubAgent = Float.parseFloat(vItems[2]);
			minRep = Float.parseFloat(vItems[3]);
			minMerchant = Float.parseFloat(vItems[4]);

			// If we are in a level different to ISO, then reset any delta that might be passed
			// and set the minimum to the actual value so the commission will not be lost
			if (!(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER)))
			{
				deltaISO = 0;
				minISO = newISO;
			}
			if (!(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER)))
			{
				deltaISO = 0;
				minISO = newISO;
				deltaAgent = 0;
				minAgent = newAgent;
			}
			if (!(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER)))
			{
				deltaISO = 0;
				minISO = newISO;
				deltaAgent = 0;
				minAgent = newAgent;
				deltaSubAgent = 0;
				minSubAgent = newSubAgent;
			}

			// First apply the increments because they have no limitations
			if (deltaISO > 0)
			{
				newISO += deltaISO;
			}
			if (deltaAgent > 0)
			{
				newAgent += deltaAgent;
			}
			if (deltaSubAgent > 0)
			{
				newSubAgent += deltaSubAgent;
			}
			if (deltaRep > 0)
			{
				newRep += deltaRep;
			}
			if (deltaMerchant > 0)
			{
				newMerchant += deltaMerchant;
			}

			// Now the first pass from Merchant to ISO applying decrements and considering the minimum value
			if (deltaMerchant < 0)
			{
				currentDelta += deltaMerchant;// Add the desired decrement
			}
			if (currentDelta < 0)
			{
				if (newMerchant > minMerchant)// If the current rate is greater than the desired minimum, then we can steal some commission
				{
					if ((newMerchant - minMerchant) >= Math.abs(currentDelta))// If the diff between current rate and minimum is enough to apply the decrement
					{
						newMerchant += currentDelta;// Decrement the rate
						currentDelta = 0;//Reset the current diff
					}
					else// Else the diff between current rate and minimum is either zero or less than the desired decrement
					{
						currentDelta += (newMerchant - minMerchant);// Decrement the current diff
						newMerchant -= (newMerchant - minMerchant);// Decrement the rate
						// Since the flow caught in this "else", then currentDelta should be < 0 and we need to decrement the next actor rate
					}
				}
			}
			// The following ifs will not be documented since the code is the same only changing the Actor suffix

			if (deltaRep < 0)
			{
				currentDelta += deltaRep;
			}
			if (currentDelta < 0)
			{
				if (newRep > minRep)
				{
					if ((newRep - minRep) >= Math.abs(currentDelta))
					{
						newRep += currentDelta;
						currentDelta = 0;
					}
					else
					{
						currentDelta += (newRep - minRep);
						newRep -= (newRep - minRep);
					}
				}
			}

			if (deltaSubAgent < 0)
			{
				currentDelta += deltaSubAgent;
			}
			if (currentDelta < 0)
			{
				if (newSubAgent > minSubAgent)
				{
					if ((newSubAgent - minSubAgent) >= Math.abs(currentDelta))
					{
						newSubAgent += currentDelta;
						currentDelta = 0;
					}
					else
					{
						currentDelta += (newSubAgent - minSubAgent);
						newSubAgent -= (newSubAgent - minSubAgent);
					}
				}
			}

			if (deltaAgent < 0)
			{
				currentDelta += deltaAgent;
			}
			if (currentDelta < 0)
			{
				if (newAgent > minAgent)
				{
					if ((newAgent - minAgent) >= Math.abs(currentDelta))
					{
						newAgent += currentDelta;
						currentDelta = 0;
					}
					else
					{
						currentDelta += (newAgent - minAgent);
						newAgent -= (newAgent - minAgent);
					}
				}
			}

			if (deltaISO < 0)
			{
				currentDelta += deltaISO;
			}

			// If at this point the "currentDelta" is < 0, then there are pending decrements
			// and we need to do a second pass from ISO to Merchant
			if (currentDelta < 0)
			{
				if (newISO > minISO)// If this actor has some commission to be stolen
				{
					if ((newISO - minISO) >= Math.abs(currentDelta))// If this actor can take all the remaining decrement
					{
						newISO += currentDelta;// Substract the accumulated decrement
						currentDelta = 0;//Reset the current diff
					}
					else//Else this actor can't take all the decrement but we can steal a bit from its commission
					{
						currentDelta += (newISO - minISO);// Substract the part of the decrement that we can apply
						newISO -= (newISO - minISO);
					}
				}
			}

			if (currentDelta < 0)
			{
				if (newAgent > minAgent)
				{
					if ((newAgent - minAgent) >= Math.abs(currentDelta))
					{
						newAgent += currentDelta;
						currentDelta = 0;
					}
					else
					{
						currentDelta += (newAgent - minAgent);
						newAgent -= (newAgent - minAgent);
					}
				}
			}

			if (currentDelta < 0)
			{
				if (newSubAgent > minSubAgent)
				{
					if ((newSubAgent - minSubAgent) >= Math.abs(currentDelta))
					{
						newSubAgent += currentDelta;
						currentDelta = 0;
					}
					else
					{
						currentDelta += (newSubAgent - minSubAgent);
						newSubAgent -= (newSubAgent - minSubAgent);
					}
				}
			}

			if (currentDelta < 0)
			{
				if (newRep > minRep)
				{
					if ((newRep - minRep) >= Math.abs(currentDelta))
					{
						newRep += currentDelta;
						currentDelta = 0;
					}
					else
					{
						currentDelta += (newRep - minRep);
						newRep -= (newRep - minRep);
					}
				}
			}

			if (currentDelta < 0)
			{
				if (newMerchant > minMerchant)
				{
					if ((newMerchant - minMerchant) >= Math.abs(currentDelta))
					{
						newMerchant += currentDelta;
						currentDelta = 0;
					}
					else
					{
						currentDelta += (newMerchant - minMerchant);
						newMerchant -= (newMerchant - minMerchant);
					}
				}
			}

			newEmida = buyRate - (newISO + newAgent + newSubAgent + newRep + newMerchant + refAgentRate);

			// If at this point the "currentDelta" is still < 0, then we are screwed because the Rate Update couldn't be applied,
			// so now build a Vector with the results in order to either show to the user or perform the Global Update
			vTmp = new Vector<String>();
			vTmp.add((currentDelta < 0) ? "0" : "1");
			vResults.add(vTmp);

			vTmp = new Vector<String>();
			vTmp.add(Float.toString(oldEmida));
			vTmp.add(Float.toString(oldISO));
			vTmp.add(Float.toString(oldAgent));
			vTmp.add(Float.toString(oldSubAgent));
			vTmp.add(Float.toString(oldRep));
			vTmp.add(Float.toString(oldMerchant));
			vResults.add(vTmp);

			vTmp = new Vector<String>();
			vTmp.add(Float.toString(newEmida - oldEmida));
			vTmp.add(Float.toString(deltaISO));
			vTmp.add(Float.toString(deltaAgent));
			vTmp.add(Float.toString(deltaSubAgent));
			vTmp.add(Float.toString(deltaRep));
			vTmp.add(Float.toString(deltaMerchant));
			vResults.add(vTmp);

			vTmp = new Vector<String>();
			vTmp.add(Float.toString(newEmida));
			vTmp.add(Float.toString(newISO));
			vTmp.add(Float.toString(newAgent));
			vTmp.add(Float.toString(newSubAgent));
			vTmp.add(Float.toString(newRep));
			vTmp.add(Float.toString(newMerchant));
			vResults.add(vTmp);

			vTmp = new Vector<String>();
			vTmp.add(Float.toString(0));
			vTmp.add(Float.toString(minISO));
			vTmp.add(Float.toString(minAgent));
			vTmp.add(Float.toString(minSubAgent));
			vTmp.add(Float.toString(minRep));
			vTmp.add(Float.toString(minMerchant));
			vResults.add(vTmp);
		}
		catch (Exception e)
		{
			cat.error("Error during generateUpdateRatesChange", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}

	public void generateUpdateRatesWorkTable(String sessionId, String sRates, String sMinValues, String sAccessLevel, SessionData sessionData) throws RatePlanException
	{
		Connection cn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;
		Hashtable<String, String> htPlans = new Hashtable<String, String>();
		Hashtable<String, String> htProducts = new Hashtable<String, String>();
		Hashtable<String, String> htResults = new Hashtable<String, String>();

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("generateRatePlanGlobalOperation");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sessionId);
			ps.setBoolean(2, (sessionData.getUser().isIntranetUser()
					&& sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
							DebisysConstants.INTRANET_PERMISSION_ALLOW_EXCEED_RATES)));
			rs = ps.executeQuery();
			while (rs.next())
			{
				htPlans.put(rs.getString("RatePlanID"), rs.getString("Name"));
			}
			rs.close();

			ps.getMoreResults();
			rs = ps.getResultSet();
			while (rs.next())
			{
				htProducts.put(rs.getString("ProductID"), rs.getString("Description"));
			}
			rs.close();

			ps.getMoreResults();
			rs = ps.getResultSet();
			while (rs.next())
			{
				htResults.put(rs.getString("RatePlanID") + "-" + rs.getString("ProductID"), rs.getString("Enabled") + "," + rs.getString("DontAdd"));
			}
			rs.close();
			ps.close();

			sSQL = sql_bundle.getString("insertRatePlanWorkItem");
			ps = cn.prepareStatement(sSQL);

			Enumeration<String> ePlans = htPlans.keys();
			while (ePlans.hasMoreElements())
			{
				String sRatePlanID = ePlans.nextElement();// This one will be used in the iteration of products below

				Enumeration<String> eProducts = htProducts.keys();
				while (eProducts.hasMoreElements())
				{
					String sProductID = eProducts.nextElement();

					// If the product exists in the plan being iterated
					if (htResults.get(sRatePlanID + "-" + sProductID) != null)
					{
						Vector<Vector<String>> vData = this.generateUpdateRatesChange(sRatePlanID, sProductID, sRates, sMinValues, sAccessLevel);
						Vector<String> vRates = vData.get(3);
						if (vData.get(0).get(0).equals("1"))
						{
							ps.setString(1, sessionId);
							ps.setNull(2, java.sql.Types.INTEGER);
							ps.setInt(3, Integer.parseInt(sProductID));
							ps.setNull(4, java.sql.Types.BIT);
							ps.setString(5, sRatePlanID);
							ps.setFloat(6, Float.parseFloat(vRates.get(1)));
							ps.setFloat(7, Float.parseFloat(vRates.get(2)));
							ps.setFloat(8, Float.parseFloat(vRates.get(3)));
							ps.setFloat(9, Float.parseFloat(vRates.get(4)));
							ps.setFloat(10, Float.parseFloat(vRates.get(5)));
							ps.setNull(11, java.sql.Types.FLOAT);
							ps.setNull(12, java.sql.Types.NUMERIC);
							ps.addBatch();
						}
						vData = null;
					}
				}
			}
			ps.executeBatch();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during generateUpdateRatesWorkTable", e);
			try
			{
				if ( cn != null )
				{
					ps = cn.prepareStatement("DELETE RatePlanWorkTable WHERE SessionID = ?");
					ps.setString(1, sessionId);
					ps.executeUpdate();
				}
			}
			catch (Exception ee)
			{
				cat.error("Error during generateUpdateRatesWorkTable", ee);
			}
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
	}

	public Vector<Double> getRatePlanMinimumRates(String sPlanId) throws RatePlanException
	{
		Connection cn = null;
		Vector<Double> vResults = new Vector<Double>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;
		// Insert default values
		vResults.add(new Double(1));
		vResults.add(new Double(1));
		vResults.add(new Double(1));
		vResults.add(new Double(1));
		vResults.add(new Double(1));

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("getRatePlanMinimumRates");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, sPlanId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				vResults.clear();
				vResults.add(new Double(rs.getDouble("ISORate")));
				vResults.add(new Double(rs.getDouble("AgentRate")));
				vResults.add(new Double(rs.getDouble("SubAgentRate")));
				vResults.add(new Double(rs.getDouble("RepRate")));
				vResults.add(new Double(rs.getDouble("MerchantRate")));
			}
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanMinimumRates", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vResults;
	}
	/* END Release 31 - Added by LF */
	
	/*R33  - BUG 1829*/
	public String getRatePlanId(String siteId) throws RatePlanException
	{
		Connection cn = null;
		Vector<String> vResults = new Vector<String>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;
		// Insert default values
		String ratePlanID="";

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("getRatePlanIDInfo");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, siteId);
			rs = ps.executeQuery();
			if (rs.next())
			{
				vResults.clear();
				vResults.add(rs.getString("ISO_Rateplan_id"));
				vResults.add(rs.getString("RatePlanId"));
				vResults.add(rs.getString("UseRatePlanModel"));
				vResults.add(rs.getString("millennium_no"));
				
			}
			rs.close();
			ps.close();
			
			//Boolean UseRatePlanModel= false;
			if(vResults.size()>0){
				if(vResults.get(2).equals(new Boolean(true))){
					ratePlanID = vResults.get(1);
				}
				else{
					ratePlanID= vResults.get(0);
				}
			}


		}
		catch (Exception e)
		{
			cat.error("Error during getRatePlanId", e);
			throw new RatePlanException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return ratePlanID;
	}
	

	/*END R33  - BUG 1829*/
}
