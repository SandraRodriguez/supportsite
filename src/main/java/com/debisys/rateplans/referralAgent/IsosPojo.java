
package com.debisys.rateplans.referralAgent;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class IsosPojo {
    private BigDecimal isoId;
    private String businesName;

    public IsosPojo(BigDecimal isoId, String businesName) {
        this.isoId = isoId;
        this.businesName = businesName;
    }

    public BigDecimal getIsoId() {
        return isoId;
    }

    public void setIsoId(BigDecimal isoId) {
        this.isoId = isoId;
    }

    public String getBusinesName() {
        return businesName;
    }

    public void setBusinesName(String businesName) {
        this.businesName = businesName;
    }
    
    
}
