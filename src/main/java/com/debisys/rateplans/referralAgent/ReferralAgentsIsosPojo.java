/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.rateplans.referralAgent;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class ReferralAgentsIsosPojo {
    
    private String id;
    private String idReferralAgentAssociation;
    private BigDecimal isoId;
    private String businessName;
    private boolean verified;

    public ReferralAgentsIsosPojo(String id, String idReferralAgentAssociation, BigDecimal isoId, String businessName) {
        this.id = id;
        this.idReferralAgentAssociation = idReferralAgentAssociation;
        this.isoId = isoId;
        this.businessName = businessName;
        verified = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdReferralAgentAssociation() {
        return idReferralAgentAssociation;
    }

    public void setIdReferralAgentAssociation(String idReferralAgentAssociation) {
        this.idReferralAgentAssociation = idReferralAgentAssociation;
    }

    public BigDecimal getIsoId() {
        return isoId;
    }

    public void setIsoId(BigDecimal isoId) {
        this.isoId = isoId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
    
    
}
