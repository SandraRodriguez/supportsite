package com.debisys.rateplans.referralAgent;

import com.debisys.users.SessionData;
import com.debisys.utils.LogChanges;
import com.emida.utils.dbUtils.TorqueHelper;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.ServletContext;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class ReferralAgentConf {

    private static final String INSERT_MODE = "INSERT";
    private static final String UPDATE_MODE = "UPDATE";

    private static final String AUDIT_REFERRAL_AGENT_ENABLE = "Referral Agent Enable";
    private static final String AUDIT_REFERRAL_AGENT = "Referral Agent";
    private static final String AUDIT_REFERRAL_AGENT_DESCRIPTION = "Referral Agent Description";
    private static final String AUDIT_REFERRAL_AGENT_ISOID = "Referral Agent IsoId";
    private static final String AUDIT_REFERRAL_AGENT_PRODUCTID = "Referral Agent ProductId";

    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.rateplans.referralAgent.sql");
    static Category cat = Category.getInstance(ReferralAgentConf.class);

    public static List<ReferralAgentsAssociationPojo> getReferralAgentAssociationList() {
        List<ReferralAgentsAssociationPojo> list = new ArrayList<ReferralAgentsAssociationPojo>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("referralAgentsAssociationAllList");
            String sqlCountIsos = ReferralAgentConf.sql_bundle.getString("referralAgentsIsosCountByParent");
            String sqlCountProducts = ReferralAgentConf.sql_bundle.getString("referralAgentsProductsCountByParent");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                ReferralAgentsAssociationPojo ra = new ReferralAgentsAssociationPojo();
                ra.setId(rs.getString("id"));
                ra.setDescription(rs.getString("description"));
                ra.setReferralAgentId(rs.getBigDecimal("referralAgentId"));
                ra.setReferralAgentName(rs.getString("businessname"));
                ra.setEnabled(rs.getBoolean("enabled"));
                ra.setCountIsos(getCountByQuery(sqlCountIsos, ra.getId()));
                ra.setCountProducts(getCountByQuery(sqlCountProducts, ra.getId()));
                list.add(ra);
            }
        } catch (Exception e) {
            cat.error("Error during getReferralAgentAssociationList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static int getCountByQuery(String sql, String... params) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            pstmt = dbConn.prepareStatement(sql);
            int index = 1;
            for (String c1 : params) {
                pstmt.setString(index, c1);
                index++;
            }
            rs = pstmt.executeQuery();
            return (rs.next()) ? rs.getInt(1) : 0;
        } catch (Exception e) {
            cat.error("Error during getCountByQuery", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return 0;
    }

    public static ReferralAgentsAssociationPojo getReferralAgentAssociation(String idAssociation) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("referralAgentById");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, idAssociation);
            cat.info("getReferralAgentAssociation sql:" + sql + " " + idAssociation);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                cat.info("getReferralAgentAssociation ok");
                ReferralAgentsAssociationPojo ra = new ReferralAgentsAssociationPojo();
                ra.setId(rs.getString("id"));
                ra.setDescription(rs.getString("description"));
                ra.setReferralAgentId(rs.getBigDecimal("referralAgentId"));
                ra.setEnabled(rs.getBoolean("enabled"));
                ra.setIsosList(getReferralAgentIsos(idAssociation));
                ra.setProductsList(getReferralAgentProducts(idAssociation));
                return ra;
            }
        } catch (Exception e) {
            cat.error("Error during getReferralAgentAssociationList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return null;
    }

    private static List<ReferralAgentsIsosPojo> getReferralAgentIsos(String idAssociation) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<ReferralAgentsIsosPojo> list = new ArrayList<ReferralAgentsIsosPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("referralAgentsIsosByParentId");
            cat.info("getReferralAgentIsos sql:" + sql + " " + idAssociation);
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, idAssociation);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(new ReferralAgentsIsosPojo(rs.getString("id"), rs.getString("IdReferralAgentsAssociation"), rs.getBigDecimal("isoId"), getIsoInfo(rs.getString("isoId")).get(0).getBusinesName()));
            }
        } catch (Exception e) {
            cat.error("Error during getReferralAgentIsos", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static List<IsosPojo> getIsoInfo(String filterIso) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<IsosPojo> isoData = new ArrayList<IsosPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("isosByIdOrName");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, filterIso);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                isoData.add(new IsosPojo(rs.getBigDecimal("rep_id"), rs.getString("businessname")));
            }
        } catch (Exception e) {
            cat.error("Error during getIsoInfo", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return isoData;
    }

    private static List<ReferralAgentsProductsPojo> getReferralAgentProducts(String idAssociation) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<ReferralAgentsProductsPojo> list = new ArrayList<ReferralAgentsProductsPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("referralAgentsProductsByParentId");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, idAssociation);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                List<ProductPojo> productInfo = getProductInfo(rs.getInt("productId"));
                list.add(new ReferralAgentsProductsPojo(rs.getString("id"), rs.getString("IdReferralAgentsAssociation"), rs.getInt("productId"), rs.getBigDecimal("rate"),
                        productInfo.get(0).getDescription(),productInfo.get(0).getProviderName()));
            }
        } catch (Exception e) {
            cat.error("Error during getReferralAgentProducts", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static List<ProductPojo> getProductInfo(int productFilter) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<ProductPojo> productsData = new ArrayList<ProductPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("productById");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, productFilter);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                productsData.add(new ProductPojo(rs.getInt("id"), rs.getString("description"),rs.getString("name")));
            }
        } catch (Exception e) {
            cat.error("Error during getProductInfo", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return productsData;
    }

    public static List<ReferralAgentsPojo> getReferralAgentList() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<ReferralAgentsPojo> list = new ArrayList<ReferralAgentsPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("referralAgentsList");
            pstmt = dbConn.prepareStatement(sql);
            cat.info("getReferralAgentList sql:" + sql + " ");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(new ReferralAgentsPojo(rs.getBigDecimal("rep_id"), rs.getString("businessname")));
            }
        } catch (Exception e) {
            cat.error("Error during getReferralAgentList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }

    public static List<IsosPojo> getIsosList() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<IsosPojo> isoData = new ArrayList<IsosPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("isosList");
            cat.info("getIsosList sql:" + sql);
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                isoData.add(new IsosPojo(rs.getBigDecimal("rep_id"), rs.getString("businessname")));
            }
        } catch (Exception e) {
            cat.error("Error during getIsosList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return isoData;
    }

    public static List<ProductPojo> getProductsList(String filter) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<ProductPojo> productData = new ArrayList<ProductPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("productsFilterList");
            cat.info("getProductsList sql:" + sql + " Filter: [" + filter + "]");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setString(1, "%" + filter + "%");
            pstmt.setString(2, "%" + filter + "%");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                productData.add(new ProductPojo(rs.getInt("id"), rs.getString("description"),rs.getString("name")));
            }
        } catch (Exception e) {
            cat.error("Error during getProductsList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return productData;
    }

    public static List<ProvidersPojo> getProvidersList() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<ProvidersPojo> providersData = new ArrayList<ProvidersPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("providersList");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                providersData.add(new ProvidersPojo(rs.getInt("provider_id"), rs.getString("name")));
            }
        } catch (Exception e) {
            cat.error("Error during getProvidersList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return providersData;
    }

    public static List<ProductPojo> getProductsListByProvider(int providerId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<ProductPojo> productData = new ArrayList<ProductPojo>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String sql = ReferralAgentConf.sql_bundle.getString("productListByProvider");
            cat.info("getProductsListByProvider sql:" + sql + " providerId: [" + providerId + "]");
            pstmt = dbConn.prepareStatement(sql);
            pstmt.setInt(1, providerId);
            rs = pstmt.executeQuery();
            while (rs.next()) { 
                productData.add(new ProductPojo(rs.getInt("id"), rs.getString("description"),rs.getString("name")));
            }
        } catch (Exception e) {
            cat.error("Error during getProductsListByProvider", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return productData;
    }

    public static void insertReferralAgents(SessionData sessionData, ServletContext context, ReferralAgentsAssociationPojo raAssociation) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            String uuid = UUID.randomUUID().toString().toUpperCase();
            raAssociation.setId(uuid);
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            strSQL = ReferralAgentConf.sql_bundle.getString("referralAgentsAssociationInsert");
            cat.debug("Insert ReferralAgentsAssociation" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, uuid);
            pst.setString(2, raAssociation.getDescription());
            pst.setBigDecimal(3, raAssociation.getReferralAgentId());
            pst.setBoolean(4, raAssociation.isEnabled());
            pst.executeUpdate();

            // INSERT MerchantsStockLevelsDetail
            insertRaIsos(uuid, raAssociation.getIsosList());
            insertRaProducts(uuid, raAssociation.getProductsList());

            insertReferralAgentsAUDIT(sessionData, context, raAssociation);
        } catch (Exception e) {
            cat.error("Error inserting ReferralAgentsAssociation", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    private static void insertRaIsos(String raAssociationID, List<ReferralAgentsIsosPojo> isosArray) throws Exception {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = ReferralAgentConf.sql_bundle.getString("referralAgentsIsosInsert");
            cat.debug("Insert insertRaIsos" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            for (ReferralAgentsIsosPojo iso : isosArray) {
                pst.setString(1, UUID.randomUUID().toString().toUpperCase());
                pst.setString(2, raAssociationID);
                pst.setBigDecimal(3, iso.getIsoId());
                pst.executeUpdate();
            }
        } catch (Exception e) {
            cat.error("Error inserting insertRaIsos", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    private static void insertRaProducts(String raAssociationID, List<ReferralAgentsProductsPojo> products) throws Exception {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = ReferralAgentConf.sql_bundle.getString("referralAgentsProductsInsert");
            cat.debug("Insert insertRaProducts" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            for (ReferralAgentsProductsPojo prod : products) {
                pst.setString(1, UUID.randomUUID().toString().toUpperCase());
                pst.setString(2, raAssociationID);
                pst.setInt(3, prod.getProductId());
                pst.setBigDecimal(4, prod.getRate());
                pst.executeUpdate();
            }
        } catch (Exception e) {
            cat.error("Error inserting insertRaProducts", e);
            throw new Exception(e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void updateReferralAgents(SessionData sessionData, ServletContext context, ReferralAgentsAssociationPojo raAssociation) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        String strSQL = "";

        try {
            ReferralAgentsAssociationPojo raOld = getReferralAgentAssociation(raAssociation.getId());

            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            strSQL = ReferralAgentConf.sql_bundle.getString("referralAgentsAssociationUpdate");
            cat.debug(" updateReferralAgents" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, raAssociation.getDescription());
            pst.setBigDecimal(2, raAssociation.getReferralAgentId());
            pst.setBoolean(3, raAssociation.isEnabled());
            pst.setString(4, raAssociation.getId());
            
            pst.executeUpdate();

            deleteRaIsos(raAssociation.getId());
            deleteRaProducts(raAssociation.getId());

            // Insert
            insertRaIsos(raAssociation.getId(), raAssociation.getIsosList());
            insertRaProducts(raAssociation.getId(), raAssociation.getProductsList());
            
            updateReferralAgentsAUDIT(sessionData, context, raOld, raAssociation);
        } catch (Exception e) {
            cat.error("Error  updateReferralAgents", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void deleteRaAssociations(SessionData sessionData, ServletContext context, String idRaAssociation) throws Exception {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            ReferralAgentsAssociationPojo raOld = getReferralAgentAssociation(idRaAssociation);
            deleteRaIsos(idRaAssociation);
            deleteRaProducts(idRaAssociation);
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = ReferralAgentConf.sql_bundle.getString("referralAgentsDeleteRaAssociation");
            cat.debug("deleting deleteRaAssociations" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, idRaAssociation);
            pst.executeUpdate();
            
            deleteRAAUDIT(sessionData, context, raOld);
        } catch (Exception e) {
            cat.error("Error deleting deleteRaAssociations", e);
            throw new Exception();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void deleteRaIsos(String idMerchantsStockLevels) throws Exception {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = ReferralAgentConf.sql_bundle.getString("referralAgentsIsosDeleteByParent");
            cat.debug("deleting deleteRaAIsos" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, idMerchantsStockLevels);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error deleteRaAIsos", e);
            throw new Exception();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static void deleteRaProducts(String idMerchantsStockLevels) throws Exception {
        Connection dbConn = null;
        PreparedStatement pst = null;
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            String strSQL = ReferralAgentConf.sql_bundle.getString("referralAgentsProductsDeleteByParent");
            cat.debug("deleting deleteRaProducts" + strSQL);
            pst = dbConn.prepareStatement(strSQL);
            pst.setString(1, idMerchantsStockLevels);
            pst.executeUpdate();
        } catch (Exception e) {
            cat.error("Error deleteRaProducts", e);
            throw new Exception();
        } finally {
            TorqueHelper.closeConnection(dbConn, pst, null);
        }
    }

    public static List<String> verifyAssociationData(String idAssociation, String referralAgent, String[] raIsos, String[] raProducts) {

        List<String> listIsos = Arrays.asList(raIsos);
        List<String> listProducts = Arrays.asList(raProducts);
        String strIsos = listIsos.toString().replace("[", "").replace("]", "");
        String strProducts = listProducts.toString().replace("[", "").replace("]", "");

        String sql = "SELECT raa.id, raa.ReferralAgentId, rai.isoId, rap.productId "
                + "FROM ReferralAgentsAssociation raa WITH(NOLOCK) "
                + "INNER JOIN ReferralAgentsAssociationIsos rai WITH(NOLOCK) ON (raa.id = rai.IdReferralAgentsAssociation) "
                + "INNER JOIN ReferralAgentsAssociationProducts rap WITH(NOLOCK) ON (raa.id = rap.IdReferralAgentsAssociation) "
                + "WHERE  ( rai.isoId IN (" + strIsos + ") AND productId IN (" + strProducts + ")) ";

        if (idAssociation == null || idAssociation.equalsIgnoreCase("NULL")) {
            String sqlCountIsos = ReferralAgentConf.sql_bundle.getString("countIsos");
            String sqlCountProducts = ReferralAgentConf.sql_bundle.getString("countProducts");
            int contIsos = getCountByQuery(sqlCountIsos.replace("?", strIsos));
            int contProducts = getCountByQuery(sqlCountProducts.replace("?", strProducts));

            if (contIsos > 0 && contProducts > 0) {
                return getDuplicateProductsList(ReferralAgentConf.INSERT_MODE, sql);
            }
        } else {
            sql += " AND raa.id != '" + idAssociation + "' ";
            return getDuplicateProductsList(ReferralAgentConf.UPDATE_MODE, sql);
        }
        return new ArrayList<String>();
    }

    private static List<String> getDuplicateProductsList(String mode, String sql) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<String> listData = new ArrayList<String>();
        try {
            dbConn = TorqueHelper.getConnection(ReferralAgentConf.sql_bundle.getString("pkgDefaultDb"));
            cat.info("getDuplicateProductsList sql:" + sql + "");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                listData.add(rs.getString("isoId") + "," + rs.getInt("productId"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            cat.error("Error during getDuplicateProductsList", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return listData;
    }

    private static void insertReferralAgentsAUDIT(SessionData sessionData, ServletContext context, ReferralAgentsAssociationPojo raAssociation) throws Exception {
        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);

        int idLogChangeAssociation = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT);
        int idLogChangeDescription = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_DESCRIPTION);
        int idLogChangeEnable = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ENABLE);
        int idLogChangeIso = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ISOID);
        int idLogChangeProduct = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_PRODUCTID);
        String reason = "Inserted Referral Agent Association with referral agent: " + raAssociation.getReferralAgentId() + "  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        logChanges.log_changed(idLogChangeAssociation, raAssociation.getReferralAgentId().toString(), raAssociation.getReferralAgentId().toString(), "", reason);
        logChanges.log_changed(idLogChangeDescription, raAssociation.getReferralAgentId().toString(), raAssociation.getDescription(), "", reason);
        logChanges.log_changed(idLogChangeEnable, raAssociation.getReferralAgentId().toString(), ""+raAssociation.isEnabled(), "", reason);
        

        String reasonIso = "Inserted ISO [%s] with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        List<ReferralAgentsIsosPojo> isosList = raAssociation.getIsosList();
        for (ReferralAgentsIsosPojo iso : isosList) {
            logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), iso.getIsoId().toString(), "", String.format(reasonIso, iso.getIsoId(), raAssociation.getReferralAgentId().toString()));
        }

        String reasonProduct = "Inserted Product: [%s] with RATE: [%s], referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        List<ReferralAgentsProductsPojo> productsList = raAssociation.getProductsList();
        for (ReferralAgentsProductsPojo p : productsList) {
            logChanges.log_changed(idLogChangeProduct, raAssociation.getReferralAgentId().toString(), p.getProductId() + "_" + p.getRate(), "", String.format(reasonProduct, p.getProductId(), p.getRate(), raAssociation.getReferralAgentId()));
        }
    }

    private static void updateReferralAgentsAUDIT(SessionData sessionData, ServletContext context, ReferralAgentsAssociationPojo raOld, ReferralAgentsAssociationPojo raAssociation) throws Exception {
        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);

        int idLogChangeAssociation = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT);
        int idLogChangeDescription = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_DESCRIPTION);
        int idLogChangeEnable = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ENABLE);
        int idLogChangeIso = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ISOID);
        int idLogChangeProduct = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_PRODUCTID);
        String reason = "Updated Referral Agent Association with referral agent: " + raAssociation.getReferralAgentId() + "  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        if (raOld.getReferralAgentId().doubleValue() != raAssociation.getReferralAgentId().doubleValue()) {
            logChanges.log_changed(idLogChangeAssociation, raAssociation.getReferralAgentId().toString(), raAssociation.getReferralAgentId().toString(), raOld.getReferralAgentId().toString(), reason);
        }
        if (!raOld.getDescription().trim().equals(raAssociation.getDescription())) {
            logChanges.log_changed(idLogChangeDescription, raAssociation.getReferralAgentId().toString(), raAssociation.getDescription(), raOld.getDescription(), reason);
        }
        if ((raOld.isEnabled() && !raAssociation.isEnabled()) || (!raOld.isEnabled() && raAssociation.isEnabled())) {
            logChanges.log_changed(idLogChangeEnable, raAssociation.getReferralAgentId().toString(), ""+raAssociation.isEnabled(), ""+raOld.isEnabled(), reason);
        }

        String reasonIsoDeleted = "Deleted ISO [%s] with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        String reasonIsoInserted = "Inserted ISO [%s] with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        
        boolean exist = false;
        for (ReferralAgentsIsosPojo isoOld : raOld.getIsosList()) {
            for (int i =0; i < raAssociation.getIsosList().size(); i++) {
                if(!exist && isoOld.getIsoId().doubleValue() == raAssociation.getIsosList().get(i).getIsoId().doubleValue()){
                    exist = true;
                    raAssociation.getIsosList().get(i).setVerified(true);
                }
            }
            if(!exist){
                // REPORT ISOS DELETED
                logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), "",isoOld.getIsoId().toString(), String.format(reasonIsoDeleted, isoOld.getIsoId(), raAssociation.getReferralAgentId()));
            }
            exist = false;
        }
        
        // Insert New ISO fiels
        for (ReferralAgentsIsosPojo iso : raAssociation.getIsosList()) {
            if(!iso.isVerified()){
                logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), iso.getIsoId().toString(),"", String.format(reasonIsoInserted, iso.getIsoId(), raAssociation.getReferralAgentId()));
            }
        }
        
        String reasonProductDeleted = "Deleted Product [%s] with Rate[%s], with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        String reasonProductUpdated = "Updated Product [%s] oldRate:[%s], newRate:[%s], with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        String reasonProductInserted = "Inserted Product [%s], with Rate[%s], with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        
        boolean equalsProduct = false;
        boolean equalsRate = false;
        BigDecimal newRate = new BigDecimal(0);
        for (ReferralAgentsProductsPojo productOld : raOld.getProductsList()) {
            for (int i =0; i < raAssociation.getProductsList().size(); i++) {
                if(!exist && productOld.getProductId() == raAssociation.getProductsList().get(i).getProductId()){
                    equalsProduct = true;
                    raAssociation.getProductsList().get(i).setVerified(true);
                    if(!equalsRate && productOld.getRate().doubleValue() == raAssociation.getProductsList().get(i).getRate().doubleValue()){
                        equalsRate = true;
                    }
                    else{
                        newRate = new BigDecimal(raAssociation.getProductsList().get(i).getRate().doubleValue());
                    }
                }
                
            }
            if(!equalsProduct){
                logChanges.log_changed(idLogChangeProduct, raAssociation.getReferralAgentId().toString(), "" , productOld.getProductId()+"_"+productOld.getRate(), String.format(reasonProductDeleted, productOld.getProductId(),productOld.getRate(), raAssociation.getReferralAgentId()));
            }
            else if (equalsProduct && !equalsRate){
                logChanges.log_changed(idLogChangeProduct, raAssociation.getReferralAgentId().toString(), productOld.getProductId()+"_"+newRate.doubleValue() , 
                        productOld.getProductId()+"_"+productOld.getRate(), String.format(reasonProductUpdated, productOld.getProductId(),productOld.getRate(), newRate.doubleValue(), raAssociation.getReferralAgentId()));
            }
            equalsProduct = false;
            equalsRate = false;
            newRate = new BigDecimal(0);
        }
        
        for (ReferralAgentsProductsPojo p: raAssociation.getProductsList()) {
            if(!p.isVerified()){
                logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), p.getProductId()+"_"+p.getRate(),"", String.format(reasonProductInserted, p.getProductId(), p.getRate(), raAssociation.getReferralAgentId()));
            }
        }

    }

    private static void deleteRAAUDIT(SessionData sessionData, ServletContext context, ReferralAgentsAssociationPojo raAssociation) throws Exception {
        LogChanges logChanges = new LogChanges(sessionData);
        logChanges.setContext(context);

        int idLogChangeAssociation = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT);
        int idLogChangeDescription = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_DESCRIPTION);
        int idLogChangeEnable = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ENABLE);
        int idLogChangeIso = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_ISOID);
        int idLogChangeProduct = logChanges.getLogChangeIdByName(AUDIT_REFERRAL_AGENT_PRODUCTID);
        String reason = "Deleted Referral Agent Association with referral agent: " + raAssociation.getReferralAgentId() + "  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        logChanges.log_changed(idLogChangeAssociation, raAssociation.getReferralAgentId().toString(), "", raAssociation.getReferralAgentId().toString(), reason);
        logChanges.log_changed(idLogChangeDescription, raAssociation.getReferralAgentId().toString(), "", raAssociation.getDescription(), reason);
        logChanges.log_changed(idLogChangeEnable, raAssociation.getReferralAgentId().toString(), "", ""+raAssociation.isEnabled(), reason);

        String reasonIso = "Deleted ISO [%s] with referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        List<ReferralAgentsIsosPojo> isosList = raAssociation.getIsosList();
        for (ReferralAgentsIsosPojo iso : isosList) {
            logChanges.log_changed(idLogChangeIso, raAssociation.getReferralAgentId().toString(), "", iso.getIsoId().toString(), String.format(reasonIso, iso.getIsoId(), raAssociation.getReferralAgentId().toString()));
        }

        String reasonProduct = "Deleted Product: [%s] with RATE: [%s], referral agent: [%s]  ReferralAgentsAssociationID [" + raAssociation.getId() + "]";
        List<ReferralAgentsProductsPojo> productsList = raAssociation.getProductsList();
        for (ReferralAgentsProductsPojo p : productsList) {
            logChanges.log_changed(idLogChangeProduct, raAssociation.getReferralAgentId().toString(), "", p.getProductId() + "_" + p.getRate(), String.format(reasonProduct, p.getProductId(), p.getRate(), raAssociation.getReferralAgentId()));
        }
    }
}
