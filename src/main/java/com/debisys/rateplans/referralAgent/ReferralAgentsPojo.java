
package com.debisys.rateplans.referralAgent;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class ReferralAgentsPojo {
    
    private BigDecimal repId;
    private String businessName;

    public ReferralAgentsPojo(BigDecimal repId, String businessName) {
        this.repId = repId;
        this.businessName = businessName;
    }

    public BigDecimal getRepId() {
        return repId;
    }

    public void setRepId(BigDecimal repId) {
        this.repId = repId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

}
