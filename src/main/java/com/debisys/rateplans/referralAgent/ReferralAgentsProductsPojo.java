
package com.debisys.rateplans.referralAgent;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author dgarzon
 */
public class ReferralAgentsProductsPojo {
    private String id;
    private String idReferralAgentAssociation;
    private int productId;
    private BigDecimal rate;
    private String productDescription;
    private String providerName;
    private boolean verified;

    public ReferralAgentsProductsPojo(String id, String idReferralAgentAssociation, int productId, BigDecimal rate, String productDescription, String providerName) {
        this.id = id;
        this.idReferralAgentAssociation = idReferralAgentAssociation;
        this.productId = productId;
        this.rate = rate;
        this.productDescription = productDescription;
        this.providerName = providerName;
        this.verified = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdReferralAgentAssociation() {
        return idReferralAgentAssociation;
    }

    public void setIdReferralAgentAssociation(String idReferralAgentAssociation) {
        this.idReferralAgentAssociation = idReferralAgentAssociation;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public BigDecimal getRate() {
        if(rate != null){
            this.rate = rate.setScale(2, RoundingMode.DOWN);
        }
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }
    
}
