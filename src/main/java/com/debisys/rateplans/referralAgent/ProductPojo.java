
package com.debisys.rateplans.referralAgent;

/**
 *
 * @author dgarzon
 */
public class ProductPojo {
    private int id;
    private String description;
    private String providerName;

    public ProductPojo(int id, String description, String providerName) {
        this.id = id;
        this.description = description;
        this.providerName = providerName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

}
