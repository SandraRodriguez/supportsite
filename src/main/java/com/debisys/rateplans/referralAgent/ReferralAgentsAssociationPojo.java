package com.debisys.rateplans.referralAgent;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dgarzon
 */
public class ReferralAgentsAssociationPojo {

    private String id;
    private String description;
    private BigDecimal referralAgentId;
    private String referralAgentName;
    private boolean enabled;

    private int countIsos;
    private int countProducts;

    private List<ReferralAgentsIsosPojo> isosList;
    private List<ReferralAgentsProductsPojo> productsList;

    public ReferralAgentsAssociationPojo() {
        this.id = "";
        this.description = "";
        this.referralAgentId = new BigDecimal(BigInteger.ZERO);
        this.referralAgentName = "";
        this.isosList = new ArrayList<ReferralAgentsIsosPojo>();
        this.productsList = new ArrayList<ReferralAgentsProductsPojo>();
        this.enabled = true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getReferralAgentId() {
        return referralAgentId;
    }

    public void setReferralAgentId(BigDecimal referralAgentId) {
        this.referralAgentId = referralAgentId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getReferralAgentName() {
        return referralAgentName;
    }

    public void setReferralAgentName(String referralAgentName) {
        this.referralAgentName = referralAgentName;
    }

    public int getCountIsos() {
        return countIsos;
    }

    public void setCountIsos(int countIsos) {
        this.countIsos = countIsos;
    }

    public int getCountProducts() {
        return countProducts;
    }

    public void setCountProducts(int countProducts) {
        this.countProducts = countProducts;
    }

    public List<ReferralAgentsIsosPojo> getIsosList() {
        return isosList;
    }

    public void setIsosList(List<ReferralAgentsIsosPojo> isosList) {
        this.isosList = isosList;
    }

    public List<ReferralAgentsProductsPojo> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<ReferralAgentsProductsPojo> productsList) {
        this.productsList = productsList;
    }

    public void addIsoToList(ReferralAgentsIsosPojo raIso) {
        isosList.add(raIso);
    }

    public void addProductToList(ReferralAgentsProductsPojo raProduct) {
        productsList.add(raProduct);
    }

    public void addIsoToList(List<String> listIsos) {
        for (String iso : listIsos) {
            isosList.add(new ReferralAgentsIsosPojo(null, this.id, new BigDecimal(iso), null));
        }
    }

    public void addProductToList(List<String> listProducts) {
        for (String prod : listProducts) {
            String[] split = prod.split("_");
            productsList.add(new ReferralAgentsProductsPojo(null, this.id, Integer.parseInt(split[0]), new BigDecimal(split[1]), null, null));
        }
    }

}
