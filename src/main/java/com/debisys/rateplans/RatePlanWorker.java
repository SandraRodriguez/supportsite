package com.debisys.rateplans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.RatePlanException;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * This klass creates a disconnected thread that runs the selected GlobalUpdate operation whether is a Remove, Adding or Update Rates The advance of such task
 * is calculated by retrieving pending items in the RatePlanWorkTable DB object by SessionID
 * 
 * @author ldelarosa
 */
public class RatePlanWorker implements Runnable
{
	private String sessionId = null;
	private Actions action = null;
	private static Logger log = Logger.getLogger(RatePlanWorker.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.rateplans.sql");
	private SessionData sessionData;
	private int excludePlanType = 2;
	private boolean allowExceedBuyRate = false;

	/**
	 * Global Update operations to perform by this klass
	 */
	public enum Actions
	{
		/**
		 * Identifier for the Add Product operation
		 */
		ADD,

		/**
		 * Identifier for the Remove Product operation
		 */
		REMOVE,

		/**
		 * Identifier for the Update Rates operation
		 */
		UPDATE
	}

	/**
	 * @param sessionId
	 *        Identifier for this context's session
	 * @param action
	 *        Enumeration identifier for the operation to perform
	 * @param sessionData
	 *        Instance of SessionData used to retrieve the UserName
	 * @param excludePlanType
	 *        0 means exclude Plans and 2 means exclude nothing
	 */
	public RatePlanWorker(String sessionId, Actions action, SessionData sessionData, int excludePlanType, boolean allowExceedBuyRate)
	{
		this.sessionId = sessionId;
		this.action = action;
		this.sessionData = sessionData;
		this.excludePlanType = excludePlanType;
		this.allowExceedBuyRate = allowExceedBuyRate;
		log.debug("A GlobalUpdate operation has been started for the Session:[" + this.sessionId + "] and the operation:" + action);
	}

	/**
	 * Starting point for this object's execution
	 */
	@Override
	public void run()
	{
		try
		{
			int nSleepTimeout = 1000;

			try
			{
				nSleepTimeout = Integer.parseInt(sql_bundle.getString("sleepTimeoutRatePlanWorker"));
			}
			catch (Exception e)
			{
			}

			log.info("Sleep Timeout for GlobalUpdate is: " + nSleepTimeout);
			if (this.action.equals(Actions.ADD))
			{
				while (DoAdd())
				{
					Thread.sleep(nSleepTimeout);
				}
			}
			else if (this.action.equals(Actions.REMOVE))
			{
				while (DoRemove())
				{
					Thread.sleep(nSleepTimeout);
				}
			}
			else if (this.action.equals(Actions.UPDATE))
			{
				while (DoUpdate())
				{
					Thread.sleep(nSleepTimeout);
				}
			}
		}
		catch (Exception e)
		{
			log.error("Error while running GlobalUpdate", e);
		}
		finally
		{
			this.sessionId = null;
			this.action = null;
		}
	}

	private boolean DoAdd()
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;
		String sUserName;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				log.error("Can't get database connection");
				throw new RatePlanException();
			}

			sUserName = this.sessionData.getProperty("username");
			if (sUserName.length() > 32)
			{
				sUserName = sUserName.substring(0, 31);
			}

			sSQL = sql_bundle.getString("doRatePlanAddProducts");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, this.sessionId);
			ps.setString(2, sUserName);
			ps.setInt(3, this.excludePlanType);
			ps.setBoolean(4, this.allowExceedBuyRate);
			rs = ps.executeQuery();
			rs.next();
			bResult = rs.getInt("Count") > 0;
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			log.error("Error during DoAdd", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				log.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	private boolean DoRemove()
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				log.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("doRatePlanRemoveProducts");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, this.sessionId);
			rs = ps.executeQuery();
			rs.next();
			bResult = rs.getInt("Count") > 0;
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			log.error("Error during DoRemove", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				log.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	private boolean DoUpdate()
	{
		Connection cn = null;
		boolean bResult = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sSQL = null;

		try
		{
			cn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cn == null)
			{
				log.error("Can't get database connection");
				throw new RatePlanException();
			}

			sSQL = sql_bundle.getString("doRatePlanUpdateRates");
			ps = cn.prepareStatement(sSQL);
			ps.setString(1, this.sessionId);
			rs = ps.executeQuery();
			rs.next();
			bResult = rs.getInt("Count") > 0;
			rs.close();
			ps.close();
		}
		catch (Exception e)
		{
			log.error("Error during DoUpdate", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(cn, ps, rs);
			}
			catch (Exception e)
			{
				log.error("Error during closeConnection", e);
			}
		}
		return bResult;
	}

	public void setAllowExceedBuyRate(boolean allowExceedBuyRate)
	{
		this.allowExceedBuyRate = allowExceedBuyRate;
	}

	public boolean getAllowExceedBuyRate()
	{
		return allowExceedBuyRate;
	}
}
