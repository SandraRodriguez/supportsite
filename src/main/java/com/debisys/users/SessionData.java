package com.debisys.users;

import java.util.Hashtable;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.debisys.languages.Languages;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.Log;

public class SessionData  implements HttpSessionBindingListener
{
	private Hashtable<String, Object> hashSessionData = new Hashtable<String, Object>();
	//hash to store website permissions
	private Hashtable<String, String> hashPermissions = new Hashtable<String, String>();

	private boolean blnLoggedIn = false;

	private User user;

	private String iniLanguage="1";
	
	private boolean blnChangePassword=false;

	public void setIsChangePassword(boolean blnValue)
	{
		this.blnChangePassword = blnValue;
		
	}

	public boolean isChangePassword()
	{
		return this.blnChangePassword;
	}

	/**
	 * @return the iniLanguage
	 */
	public String getIniLanguage() {
		return this.iniLanguage;
	}

	/**
	 * @param iniLanguage the iniLanguage to set
	 */
	public void setIniLanguage(String iniLanguage) {
		this.iniLanguage = iniLanguage;
	}

	public String getProperty(String strKey)
	{
		String strValue = "";
		if (this.hashSessionData.containsKey(strKey))
		{
			strValue = this.hashSessionData.get(strKey).toString();
		}

		return strValue;
	}

	public Object getPropertyObj(String strKey)
	{
		Object objValue = null;
		if (this.hashSessionData.containsKey(strKey))
		{
			objValue = this.hashSessionData.get(strKey);
		}

		return objValue;
	}

	public void setProperty(String strKey, String strValue)
	{
		if (strValue != null && !this.hashSessionData.containsKey(strKey))
		{
			this.hashSessionData.put(strKey, strValue);
		}
		else if (strValue != null && this.hashSessionData.containsKey(strKey))
		{
			this.hashSessionData.remove(strKey);
			this.hashSessionData.put(strKey, strValue);
		}
	}


	public void setPropertyObj(String strKey, Object objValue)
	{
		if (objValue != null && !this.hashSessionData.containsKey(strKey))
		{
			this.hashSessionData.put(strKey, objValue);
		}
		else if (objValue != null && this.hashSessionData.containsKey(strKey))
		{
			this.hashSessionData.remove(strKey);
			this.hashSessionData.put(strKey, objValue);
		}
	}

	public void clear()
	{
		this.hashSessionData.clear();
	}

	public String getPermission(String strKey)
	{
		String strValue = "";
		if (this.hashPermissions.containsKey(strKey))
		{
			strValue = this.hashPermissions.get(strKey).toString();
		}

		return strValue;
	}
	public boolean checkPermission(String strKey)
	{
		boolean exists = false;
		if (this.hashPermissions.containsKey(strKey))
		{
			exists = true;
		}

		return exists;
	}

	public void setPermission(String strKey)
	{
		if (!this.hashPermissions.containsKey(strKey))
		{
			this.hashPermissions.put(strKey,"");
		}
		else if (this.hashPermissions.containsKey(strKey))
		{
			this.hashPermissions.remove(strKey);
			this.hashPermissions.put(strKey, "");
		}
	}


	public void setIsLoggedIn(boolean blnValue)
	{
		this.blnLoggedIn = blnValue;
		Log.write(this, "Login from " + this.getProperty("ip_address"), DebisysConstants.LOGTYPE_LOGIN_LOGOUT, null, null);
	}

	public boolean isLoggedIn()
	{
		return this.blnLoggedIn;
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event)
	{
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event)
	{
		if (this.blnLoggedIn)
		{
			Log.write(this, "Logout from " + this.getProperty("ip_address"), DebisysConstants.LOGTYPE_LOGIN_LOGOUT, null, null);
		}
		try{
		event.getSession().removeAttribute("vecContacts");
		event.getSession().removeAttribute("merchantInvoicePaymentsArray");
		event.getSession().removeAttribute("toolAccountExecutives");
		}
		catch(Exception e){
			Log.write(this, "valueUnbound ", DebisysConstants.LOGTYPE_LOGIN_LOGOUT, null, null);
		}
		

	}

	/**
	 * Returns the user
	 * @return User
	 */
	public User getUser() {
		return this.user;
	}

	/**
	 * Sets the user value
	 * @param user The user to set
	 */
	public void setUser(User oUser)
	{
		this.user = oUser;
	}

	public int checkUserStatus()
	{
		return (this.user != null)?this.user.checkStatusFromDB():0;
	}

	/**
	 * Remove a property value from Hashtable session.
	 * @param strKey
	 */
	public void removeProperty(String strKey)
	{
		if (strKey != null && this.hashSessionData.containsKey(strKey))
		{
			this.hashSessionData.remove(strKey);
		}
	}

	/**
	 * 
	 * @return the language of the user
	 */
	public String getLanguage() {
		String lang="";

		try
		{
			if (this.getUser() == null)
			{
				lang = Languages.getLanguage();
			}
			else if (this.getUser().getLanguage() != null)
			{
				lang = this.getUser().getLanguage();
			}
		}
		catch (Exception e)
		{
			lang = Languages.getLanguage();
		}
		return lang;
	}

	public void setLanguage(String strLanguage){
		try{
			if(this.getUser().getLanguage()!=null)
			{
				this.getUser().setLanguage(strLanguage);				
				
			}
		}catch(Exception e){
			this.setIniLanguage(strLanguage);
			//e.printStackTrace();
		}


	}

	public String getString(String strKey)
	{
		return Languages.getString(strKey, this.getLanguage());
	}

	public String getString(String strKey, Object[] objArgs)
	{
		return Languages.getString(strKey, objArgs, this.getLanguage());
	}
}
