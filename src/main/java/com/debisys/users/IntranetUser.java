/**
 *
 */
package com.debisys.users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.debisys.languages.Languages;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author nmartinez
 *
 */
public class IntranetUser {

    private String userName;
    private String password;
    private String name;
    private Integer codeError;
    private String IdNew;
    /**
     *
     */
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.users.sql");

    /**
     *
     */
    private static Logger cat = Logger.getLogger(IntranetUser.class);

    /**
     * Validate according IP and user id, on the table acl_user.
     *
     * @param userId
     * @param ip
     * @return
     */
    public static IntranetUser validateInfoByUserIdIntranet(Integer userId, String ip) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        IntranetUser user = new IntranetUser();

        try {
            dbConn = Torque.getConnection(IntranetUser.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new Exception("Can't get database connection");
            }
            pstmt = dbConn.prepareStatement(IntranetUser.sql_bundle.getString("getInfoByUserIdIntranet"));
            pstmt.setInt(1, userId.intValue());
            rs = pstmt.executeQuery();
            if (rs.next()) {
	    	  //if (rs.getString(4)!=null && rs.getString(4).equals(ip))
                //{
                user.setUserName(rs.getString(1));
                user.setName(rs.getString(2) + " " + rs.getString(3));
                user.setCodeError(0);
                user.setIdNew(rs.getString("IdNew"));
	    	  //}	    	     	  
                //else if (rs.getString(4)==null)
                //{
                //	  writeInfo("If we catch here, it is because for any reason somebody try enter directly to support-site instead by Intranet.",5);
                //	  user.setCodeError(5);
                //}
                //else if (!rs.getString(4).equals(ip))
                //{
                //	  writeInfo("Cannot validate the IP, maybe the user do loggin in other instance of the Intranet.",6);
                //	  user.setCodeError(6);
                //}	 
            } else {
                writeInfo("Cannot found info for the userid=[" + userId + "] on the table acl_user.", 7);
                user.setCodeError(7);
            }
        } catch (Exception e) {
            cat.error("Error during getAllPermissionsId", e);
            user.setCodeError(7);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return user;
    }

    /**
     * @param message
     * @param idMessage
     */
    private static void writeInfo(String message, Integer idMessage) {
        cat.info("********************************************************************");
        cat.info(message + " [" + Languages.getString("jsp.message.msg" + idMessage, "0") + "]");
        cat.info("********************************************************************");
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param codeError the codeError to set
     */
    public void setCodeError(Integer codeError) {
        this.codeError = codeError;
    }

    /**
     * @return the codeError
     */
    public Integer getCodeError() {
        return this.codeError;
    }

    /*public IntranetUser getIntranetUserForCarrier()
     {
     Connection dbConn = null;
     PreparedStatement pstmt = null;
     ResultSet rs = null; 
     IntranetUser user = new IntranetUser();
	    
     try
     { 
     dbConn = Torque.getConnection(IntranetUser.sql_bundle.getString("pkgDefaultDb"));
     if (dbConn == null)
     {
     cat.error("Can't get database connection");
     throw new Exception("Can't get database connection");
     }
     pstmt = dbConn.prepareStatement(IntranetUser.sql_bundle.getString("getInfoByUserIdIntranet"));
     pstmt.setInt(1, userId.intValue());
     rs = pstmt.executeQuery();
     if (rs.next())
     {
     if (rs.getString(4)!=null && rs.getString(4).equals(ip))
     {
     user.setUserName(rs.getString(1));
     user.setName(rs.getString(2)+" "+rs.getString(3));
     user.setCodeError(0);
     }	    	     	  
     else if (rs.getString(4)==null)
     {
     writeInfo("If we catch here, it is because for any reason somebody try enter directly to support-site instead by Intranet.",5);
     user.setCodeError(5);
     }
     else if (!rs.getString(4).equals(ip))
     {
     writeInfo("Cannot validate the IP, maybe the user do loggin in other instance of the Intranet.",6);
     user.setCodeError(6);
     }	 
     }
     else
     {
     writeInfo("Cannot found info for the userid=["+userId+"] on the table acl_user.",7);
     user.setCodeError(7);
     }
     }
     catch (Exception e)
     {
     cat.error("Error during getAllPermissionsId", e);
     user.setCodeError(7);
     }
     finally
     {
     try
     {
     TorqueHelper.closeConnection(dbConn, pstmt, rs);
     }
     catch (Exception e)
     {
     cat.error("Error during closeConnection", e);
     }
     }
     return user;
     }*/
    /**
     * @return the IdNew
     */
    public String getIdNew() {
        return IdNew;
    }

    /**
     * @param IdNew the IdNew to set
     */
    public void setIdNew(String IdNew) {
        this.IdNew = IdNew;
    }


    public boolean hasError(){
        return this.getCodeError().intValue() != 0;
    }

}
