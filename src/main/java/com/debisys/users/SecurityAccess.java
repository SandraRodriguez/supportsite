package com.debisys.users;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.debisys.exceptions.UserException;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SecurityAccess {

    private String accessLevel;

    private String distChainType;

    /**
     * to check from an JSP
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section
     * @param section_page
     */
    public void check(HttpServletRequest request, HttpServletResponse response, HttpSession session, SessionData sessionData,
            ServletContext application, int section, int section_page) {
        try {
            if (!sessionData.isLoggedIn()) {
                session.invalidate();
                response.sendRedirect("/support/message.jsp?message=3");
                return;
            } else if (sessionData.checkUserStatus() == 0) {// The user has been disabled!!!
                session.invalidate();
                response.sendRedirect("/support/message.jsp?message=3");
                return;
            } else {
                setAccessLevel(sessionData.getProperty("access_level"));
                setDistChainType(sessionData.getProperty("dist_chain_type"));
                if (getAccessLevel() != null && !getAccessLevel().equals("")) {
                    if (getAccessLevel() == DebisysConstants.NULL) {
                        session.invalidate();
                        response.sendRedirect("/support/message.jsp?message=3");
                        return;
                    } else {

                        switch (section) {
                            case 1: // home
                                break;
                            case 2: // customers section
                                checkCustomers(request, response, session, sessionData, application, section_page);
                                break;
                            case 3: // transactions section
                                checkTransactions(request, response, session, sessionData, application, section_page);
                                break;
                            case 4:
                                checkReports(request, response, session, sessionData, application, section_page);
                                break;
                            case 5: // help
                                check05Section(request, response, session, sessionData, application, section_page);
                                break;
                            case 6: // contact us
                                checkContactUs(request, response, session, sessionData, application, section_page);
                                break;
                            case 7: // rate plans
                                checkRatePlans(request, response, session, sessionData, application, section_page);
                                break;
                            case 8: // ach
                                check08Section(request, response, session, sessionData, application, section_page);
                                break;
                            case 9:// tools
                                checkTools(request, response, session, sessionData, application, section_page);
                                break;
                            case 10:
                                check10Section(request, response, session, sessionData, application, section_page);
                                break;
                            case 11:
                                check11Section(request, response, session, sessionData, application, section_page);
                                break;
                            case 12:
                                check12Section(request, response, session, sessionData, application, section_page);
                                break;
                            case 13:
                                check13Section(request, response, session, sessionData, application, section_page);
                                break;
                            case 14:
                                check14Section(request, response, session, sessionData, application, section_page);
                                break; // Billing ACH Report
                            case 15:
                                checkBillingReports(request, response, session, sessionData, application, section_page);
                                break;
                            case 16:
                                check16Section(request, response, session, sessionData, application, section_page);
                                break;
                            case 17:
                                check17CarrierTools(request, response, session, sessionData, application, section_page);
                                break;
                            default:
                                response.sendRedirect("/support/message.jsp?message=1");
                                return;
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check05Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        switch (section_page) {
            case 1: // index.jsp
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void checkContactUs(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        switch (section_page) {
            case 1: // index.jsp
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check08Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)
                || !(DebisysConfigListener.getDeploymentType(application).equals("0") && !getAccessLevel().equals(DebisysConstants.REFERRAL) && !getAccessLevel()
                .equals(DebisysConstants.CARRIER))) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        switch (section_page) {
            case 1: // summary.jsp
                break;
            case 2: // ach_transactions.jsp
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check10Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        switch (section_page) {
            case 1: // manage multi-iso user
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS)) // iso only and with permission
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check16Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        switch (section_page) {
            case 1:
                if (!(DebisysConfigListener.getDeploymentType(application).equals("0") && !getAccessLevel().equals(DebisysConstants.REFERRAL) && !getAccessLevel()
                        .equals(DebisysConstants.CARRIER))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void checkBillingReports(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        if (!sessionData.checkPermission(DebisysConstants.PERM_BILLING_ACH_REPORT)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check13Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        if (!((getAccessLevel().equals(DebisysConstants.ISO)) && sessionData.checkPermission(DebisysConstants.PERM_BRANDINGS_SETUP))) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check12Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        switch (section_page) {
            case 1: // agents_add.jsp
            case 2: // agents_info.jsp
            case 3: // agents_edit.jsp
            case 4: // agents_update_credit_limit.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_AGENTS)) // iso only and with permission
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 5: // subagents_add.jsp
            case 6: // subagents_info.jsp
            case 7: // subagents_edit.jsp
            case 8: // subagents_update_credit_limit.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_SUBAGENTS)) // iso only and with permission
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 9: // iso_info.jsp
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check11Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        switch (section_page) {
            // admin/tools/bonusPromoTools.jsp
            case 1:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        && sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLDS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin/tools/addBonusThreshold.jsp
            case 2:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        && sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLDS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin/tools/index.jsp
            case 3:
                if (!getAccessLevel().equals(DebisysConstants.ISO)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin/tools/bonusPromoToolsSMS.jsp
            case 4:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        && sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLD_SMS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin/tools/addBonusPromoSMS.jsp
            case 5:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        && sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLD_SMS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin/tools/selectProvider.jsp
            case 6:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        && sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLD_SMS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // CCT phone rewards promo
            case 7:
                if (!getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_PHONE_REWARDS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // CCT phone rewards promo
            case 8:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        && sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_PHONE_REWARDS_SMS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void checkTransactions(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        switch (section_page) {
            case 1: // transactions.jsp
                break;
            case 2: // reps_transactions.jsp
                break;
            case 3: // merchants_transactions.jsp
                break;
            case 4: // download
                break;
            case 5: // agents_transactions.jsp
                break;
            case 6: // subagents_transactions.jsp
                break;
            case 7: // clerks_transactions.jsp
                break;
            case 8: // View PIN information
                // If neither an ISO nor has permission
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void checkCustomers(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        switch (section_page) {
            case 1: // reps.jsp
                if (getAccessLevel().equals(DebisysConstants.REP) || getAccessLevel().equals(DebisysConstants.MERCHANT)) { // iso, agent, subagent
                    // only
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 2: // merchants.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT)) { // everyone except merchants
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 3: // merchants_update_status.jsp
                if (!(sessionData.checkPermission(DebisysConstants.PERM_MERCHANT_STATUS) || getAccessLevel().equals(DebisysConstants.CARRIER))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 4: // merchants_info.jsp
                // all levels can access
                break;
            case 5: // merchants_reset_balance.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_CREDIT_LIMITS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 6: // merchants_update_credit_limit.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_CREDIT_LIMITS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 7: // merchants_clerk_codes.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_CLERK_CODES)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 8: // merchants_edit.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MERCHANTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 9: // merchants_add.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MERCHANTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 10: // rep_selector.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 11: // merchants_terminals_add/edit/delete.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_TERMINALS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 12: // user_logins.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_LOGINS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 13: // merchants_view_rates
                // everyone has access
                break;
            case 14: // reps_add.jsp
            case 15: // reps_info.jsp
            case 16: // reps_edit.jsp
            case 17: // reps_update_credit_limit.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_REPS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 18: // reps_merchant_credit_history.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 19: // merchants_request_rebuild.jsp option
                if (!sessionData.checkPermission(DebisysConstants.PERM_TERMINALS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 20: // iso.jsp option
                if (!sessionData.checkPermission(DebisysConstants.PERM_TO_VIEW_ISO_INFORMATION)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 21: // reptransfer.jsp option
                if (!sessionData.checkPermission(DebisysConstants.PERM_ALLOW_TRANSFER_TO_REPS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void checkRatePlans(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        switch (section_page) {
            case 1: // index.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO)) // iso only
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 2: // rep_rates.jsp
                if (!(sessionData.checkPermission(DebisysConstants.PERM_RATEPLANS) && com.debisys.users.User
                        .isRepRatePlanforTerminalCreationEnabled(sessionData, application))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 3: // assign_rateplans.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) || (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS) && request
                        .getParameter("newrp") != null))) // iso only
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 4: // New RatePlans - rateplans.jsp
                try {
                    if (!((sessionData.getUser().isIntranetUser() && sessionData.getUser().hasIntranetUserPermission(
                            sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS)) || sessionData
                            .checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS))) {
                        response.sendRedirect("/support/message.jsp?message=1");
                        return;
                    }
                } catch (UserException e) {
                    e.printStackTrace();
                }
                break;
            case 5: // New RatePlans - globalupdate.jsp
                try {
                    if (!((sessionData.getUser().isIntranetUser() && sessionData.getUser().hasIntranetUserPermission(
                            sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES)) || sessionData
                            .checkPermission(DebisysConstants.PERM_MANAGE_GLOBAL_UPDATE))) {
                        response.sendRedirect("/support/message.jsp?message=1");
                        return;
                    }
                } catch (UserException e) {
                    e.printStackTrace();
                }
                break;
            case 6: // New RatePlans - globalupdate.jsp
                try {
                    if (!(sessionData.getUser().isIntranetUser() && sessionData.getUser().hasIntranetUserPermission(
                            sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))) {
                        response.sendRedirect("/support/message.jsp?message=1");
                        return;
                    }
                } catch (UserException e) {
                    e.printStackTrace();
                }
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void check14Section(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        switch (section_page) {
            // admin\customers
            case 1: // carrier_users.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 2: // carrier_users_info_delete.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 3: // carrier_users_info_edit.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 4: // carrier_users_info_new.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 5: // carrier_users_products.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 6: // carrier_users_products_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin\reports\carrier_user
            case 7: // index.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin\reports\carrier_user\analysis
            case 8: // index.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 9: // volume_daily.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 10: // volume_monthly.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin\reports\carrier_user\carrier
            case 11: // carrier_report.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 12: // index.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 13: // summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin\reports\carrier_user\crossborder
            case 14: // index.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 15: // roaming_report.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 16: // summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 17: // transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // admin\reports\carrier_user\transactions
            case 18: // index.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 19: // print_transaction_error_detail_by_types.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 20: // print_transaction_error_details.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 21: // print_transaction_error_summary_by_merchants.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 22: // print_transaction_error_summary_by_types.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 23: // products.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 24: // products_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 25: // products_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 26: // terminals.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 27: // terminals_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 28: // terminals_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 29: // transaction_error.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 30: // transaction_error_detail_by_types.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 31: // transaction_error_details.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 32: // transaction_error_summary_by_merchants.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 33: // transaction_error_summary_by_types.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 34: // transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 35: // download_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 36: // admin\reports\carrier_user\transactions\hourly.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 37: // admin\reports\carrier_user\transactions\hourly_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 38: // admin\reports\carrier_user\transactions\agents.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 39: // admin\reports\carrier_user\transactions\agents_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 40: // admin\reports\carrier_user\transactions\merchants.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 41: // admin\reports\carrier_user\transactions\merchants_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 42: // admin\reports\carrier_user\transactions\reps.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 43: // admin\reports\carrier_user\transactions\reps_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 44: // admin\reports\carrier_user\transactions\subagents.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 45: // admin\reports\carrier_user\transactions\subagents_summary.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 46: // admin\reports\carrier_user\transactions\transaction_summary_by_phone.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 47: // admin\reports\carrier_user\transactions\transaction_by_phone.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 48: // admin\reports\carrier_user\payments\daily_liability.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 49: // admin\reports\carrier_user\payments\daily_liability_report.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 50: // admin\reports\carrier_user\transactions\agents_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 51: // admin\reports\carrier_user\transactions\subagents_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 52: // admin\reports\carrier_user\transactions\merchants_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 53: // admin\reports\carrier_user\transactions\print_merchants_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 54: // admin\reports\carrier_user\transactions\reps_transactions.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 55: // admin\reports\carrier_user\transactions\transaction_detail_by_phone.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            default:
                break;
        }
    }

    private void check17CarrierTools(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {

        switch (section_page) {
            // admin\customers
            case 1: // carrier_users.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_TOOLS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            default:
                break;
        }

    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     * @throws IOException
     */
    private void checkTools(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        boolean intraPermission = false;
        int intranetUser = sessionData.getUser().getIntranetUserId();
        boolean intranetUsrc = sessionData.getUser().isIntranetUser();
        switch (section_page) {
            case 1: // pinreturn_search.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT) || !sessionData.checkPermission(DebisysConstants.PERM_MANAGEPIN)) // iso only and with permission
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 2: // paymentrequest_search.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !sessionData.checkPermission(DebisysConstants.PERM_MANAGEPAYMENT)) // iso only and with permission
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // BEGIN MiniRelease 11 - Added by YH */
            case 3: // inventory_management.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MANAGEINVENTORY)) // with permission
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                // END MiniRelease 11 - Added by YH */
                break;
            // BEGIN Release 15 - Added by YH */
            case 4: // pinreturn_request.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT) || !sessionData.checkPermission(DebisysConstants.PERM_ADDPINREQUESTS)) // iso only and with permission
                {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                // END Release 15 - Added by YH */
                break;
            // JFM Junio 09 2008 - [DBSY-32] TERMINAL TRACKING FEATURE
            case 5: // transactions/inventoriesAdministration_menu.jsp - //transactions/inventoryTerminals_menu.jsp
                if (!(sessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // End JFM Junio 09 2008 - [DBSY-32] TERMINAL TRACKING FEATURE
            // END Release 15 - Added by YH */
            case 6: // voidtopup_request.jsp
                if (!((getAccessLevel().equals(DebisysConstants.ISO) || getAccessLevel().equals(DebisysConstants.MERCHANT)) && sessionData
                        .checkPermission(DebisysConstants.PERM_ADDVOIDTOPUP_REQUESTS))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 7:
                // MERCHANT BULK IMPORT ONLY FOR ISO's and permission set up ok
                if (!getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MERCHANT_BULK_IMPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 8:
                // PAYMENT BULK IMPORT ONLY FOR ISO's , Mexico and permission set up ok
                if (!(getAccessLevel().equals(DebisysConstants.ISO))
                        && !DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 9:
                // menu tools.jsp
                break;
            case 10: // write_off_request.jsp
                if (!((getAccessLevel().equals(DebisysConstants.ISO) || getAccessLevel().equals(DebisysConstants.CARRIER)) && sessionData
                        .checkPermission(DebisysConstants.PERM_WRITE_OFF))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 11:
                boolean isDomesticSecurity = DebisysConfigListener.getCustomConfigType(application)
                        .equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
                boolean permERI = sessionData.checkPermission(DebisysConstants.PERM_Enable_Edit_Merchant_Account_Executives);

                if (!isDomesticSecurity || !getAccessLevel().equals(DebisysConstants.ISO) || !permERI) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 12:
                // s2ktools.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 13:
                // s2kstockcode.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 14:
                // s2ktaskscheduler.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 15:
                // s2ksalesmanid.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 16:
                // addedittasks.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 17:
                // Promo Management Console
                try {
                    if (!((getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PROMOS))
                            || (getAccessLevel().equals(DebisysConstants.CARRIER) && sessionData
                            .checkPermission(DebisysConstants.PERM_MANAGE_PROMOS)) || (sessionData.getUser().isIntranetUser() && sessionData
                            .getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
                                    DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS)))) {
                        response.sendRedirect("/support/message.jsp?message=1");
                        return;
                    }
                } catch (UserException e) {
                    e.printStackTrace();
                }
                break;
            case 18:
                // Branding management tool
                try {
                    if (!sessionData.getUser().isIntranetUser()
                            || !sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
                                    DebisysConstants.INTRANET_PERMISSION_MANAGE_BRANDINGS)) {
                        response.sendRedirect("/support/message.jsp?message=1");
                        return;
                    }
                } catch (UserException e) {
                    e.printStackTrace();
                }
                break;
            case 19:
                // Edit MB Configurations
                if (!((sessionData.getUser().isIntranetUser() && request.getParameter("SiteID") == null) || (sessionData
                        .checkPermission(DebisysConstants.PERM_EDIT_MB_CONFIGURATIONS) && request.getParameter("SiteID") != null))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 20:
                // Product Approval for Carrier users
                if (!getAccessLevel().equals(DebisysConstants.CARRIER)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 21:
                // if ( !strAccessLevel.equals(DebisysConstants.ISO) )
                // {

                boolean bBankImportPayments = sessionData.checkPermission(DebisysConstants.PERM_Bank_Import_Payments_Records_Status);
                System.out.println("bBankImportPayments: [" + bBankImportPayments + "]");
                System.out.println("!strAccessLevel.equals(DebisysConstants.ISO): [" + !getAccessLevel().equals(DebisysConstants.ISO) + "]");
                if (!bBankImportPayments || !getAccessLevel().equals(DebisysConstants.ISO)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                // }
                break;
            // DTU-246: Error Messaging to Device
            case 22: // tools/errorManagement.jsp
                if (!(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_ERROR_MANAGEMENT))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // DTU-246: Error Messaging to Device
            case 23: // tools/pincache.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_VIEW_CACHED_PINS_REPORTS))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 24: // tools/pincache.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE_ADV))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 25: // tools/pincache.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !sessionData.checkPermission(DebisysConstants.FEE_APPROVAL_TOOL)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 26:
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !sessionData.checkPermission(DebisysConstants.PERM_VIEW_MICROBROWSER_TOOL_BATCH_UPDATE_VERSION)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 27:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_ONE_LOGIN_FOR_MERCHANTS_TERMINALS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 28:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_NO_CLERK_DATA_TRANSACTIONS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 29:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_SEND_SMS_NOTIFICATION_TO_THE_PAYMENT_CUSTOMER)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 30:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_SUPPORT_SITE_ACCESS_LINK)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 31:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_CARRIERS_LIST)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 32:
                try {
                    intraPermission = sessionData.getUser().hasIntranetUserPermission(intranetUser, 1, DebisysConstants.INTRANET_PERMISSION_PUSH_MESSAGING);
                } catch (UserException e) {
                    e.printStackTrace();
                }
                if (!sessionData.getUser().isIntranetUser() || !intraPermission) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 33:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_NO_BALANCE_MERCHANT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 34:
                boolean isDomestic = DebisysConfigListener.getCustomConfigType(application)
                        .equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
                if (!isDomestic || !accessLevel.equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_FREE_UP_ACTIVATIONS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 36:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_VIEW_PINS_ON_REPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 35:
                boolean isPrepaidUsCardProcessor = DebisysConfigListener.getCustomConfigType(application)
                        .equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
                if (!isPrepaidUsCardProcessor || !accessLevel.equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_MERCHANTS_PREPAID_USING_CARD_PROCESSOR)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 37:
                if (!sessionData.getUser().isIntranetUser()) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 38:
                if (!sessionData.getUser().isIntranetUser()) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 39:
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        || !sessionData.checkPermission(DebisysConstants.PERM_TOKEN_SECURITY)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 40:
                if (!sessionData.getUser().isIntranetUser()) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 41:
                //SIM LOOKUP
                if (!sessionData.getUser().isIntranetUser() || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 42:
                //SIM EDIT PRODUCT
                if (!sessionData.getUser().isIntranetUser() || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 43:
                //ASSING INVENTORY 
                if (!sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 44:
                //SIM INVENTORY MOVE BATCH
                if (!sessionData.getUser().isIntranetUser() || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 45:
                //ISO DOMAIN LIST
                if (!sessionData.getUser().isIntranetUser() || !sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 46:
                //SIM INVENTORY MANAGEMENT
                if (!sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 47:
                boolean intranetUsr = sessionData.getUser().isIntranetUser();
                if (intranetUsr) {
                    break;
                }
                break;
            case 48:        
                if (!intranetUsrc) {
                    break;
                }
                break;
            case 49:
                try {
                    intraPermission = sessionData.getUser().hasIntranetUserPermission(intranetUser, 1, DebisysConstants.INTRANET_PERMISSION_MULTI_CREDENTIALS);
                } catch (UserException ex) {
                    Logger.getLogger(SecurityAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (!intranetUsrc || !intraPermission) {
                    response.sendRedirect("/support/message.jsp?message=1");                    
                }
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param session
     * @param sessionData
     * @param application
     * @param section_page
     */
    private void checkReports(HttpServletRequest request, HttpServletResponse response, HttpSession session,
            SessionData sessionData, ServletContext application, int section_page) throws IOException {
        // reports section
        // Prohibit carrier users from accessing this section
        if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
            response.sendRedirect("/support/message.jsp?message=1");
            return;
        }
        String customConfigType = DebisysConfigListener.getCustomConfigType(application);

        switch (section_page) {
            case 1: // reports/transactions/merchants.jsp
                break;
            case 2: // reports/transactions/reps.jsp
                break;
            case 3: // reports/transactions/products.jsp
                break;
            case 4: // reports/analysis/index.jsp
                break;
            case 5: // reports/analysis/dashboard.jsp
                break;
            case 6: // reports/analysis/top10merchants.jsp
                break;
            case 7: // reports/analysis/top10reps.jsp
                break;
            case 8: // reports/analysis/top10products.jsp
                break;
            case 9: // reports/analysis/volume_*.jsp
                break;
            case 10: // reports/payments/*.jsp
                break;
            case 11: // reports/carrier/*.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) || getAccessLevel().equals(DebisysConstants.REP)
                        || getAccessLevel().equals(DebisysConstants.AGENT) || getAccessLevel().equals(DebisysConstants.SUBAGENT) || getAccessLevel()
                        .equals(DebisysConstants.MERCHANT))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 12: // reports/analysis/merchant_activity.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 13: // reports/crossborder/*.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) || getAccessLevel().equals(DebisysConstants.AGENT)
                        || getAccessLevel().equals(DebisysConstants.SUBAGENT) || getAccessLevel().equals(DebisysConstants.REP) || getAccessLevel()
                        .equals(DebisysConstants.MERCHANT))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 14: // transactions/merchants_transactions.jsp
                break;
            case 15: // transactions/products_transactions.jsp
                break;
            case 16: // reports/transactions/promo.jsp
                break;
            case 17: // transactions/clerks_transactions.jsp
                break;
            case 18: // transactions/transaction_error_summary.jsp
                // not in use
                break;
            case 19: // transactions/transaction_by_phone.jsp
                break;
            case 20: // transactions/terminals_transactions.jsp
                // Removed for DBSY-569 - Allow anyone to view Transaction Summary by Terminal Type
                // if (!(strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.MERCHANT) ||
                // strAccessLevel.equals(DebisysConstants.REP)))
                // {
                // response.sendRedirect("/support/message.jsp?message=1");
                // return;
                // }
                break;
            case 21: // transactions/merchant_credit.jsp
                break;
            case 22: // transactions/liability_limit.jsp
                // no use
                break;
            case 23: // reports/giftcards
                if(!sessionData.checkPermission(DebisysConstants.PERM_VIEW_FINANCIAL_SUMMARIES_REPORTS)){
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                // no use
                break;
            case 24: // reports/analysis/merchant_notification_list.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 25: // reports/transactions/pin_request.jsp
                // BEGIN Release 15 - Added by YH */
                // only iso and merchant
                // if (!(strAccessLevel.equals(DebisysConstants.ISO)||strAccessLevel.equals(DebisysConstants.MERCHANT)))
                // {
                // response.sendRedirect("/support/message.jsp?message=1");
                // return;
                // }
                // END Release 15 - Added by YH */
                break;
            case 26: // reports/transactions/merchant_nopaid.jsp
                // Everybody should be able to view this according to DBSY-569
                // if (!strAccessLevel.equals(DebisysConstants.ISO))
                // {
                // response.sendRedirect("/support/message.jsp?message=1");
                // return;
                // }
                break;
            case 27: // reports/transactions/terminals_non_tx.jsp
                break;
            case 28: // reports/transactions/voidtopup_request.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) || getAccessLevel().equals(DebisysConstants.MERCHANT))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 29: // reports/transactions/merchant_nopaid.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !getAccessLevel().equals(DebisysConstants.REP)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 30: // reports/transactions/merchant_nopaid.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 31: // reports/transactions/pin_inventory.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 32: // reports/analysis/topupphone.jsp
                // Parature 5545-10237043 = Jorge Nicol�s Mart�nez Romero
                boolean perm89 = sessionData.checkPermission(DebisysConstants.PERM_RERCHARGE_DATA_BY_PHONE_NUMBER_REPORT);
                if (!getAccessLevel().equals(DebisysConstants.ISO)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                if (!perm89) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                // End Parature 5545-10237043
                break;
            // BEGIN: R25 - Added by jacuna
            case 33: // reports/transactions/pin_remaining_inventory.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_PIN_REMAINING_INVENTORY))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 34: // reports/payments/deposits_by_merchant.jsp
                break;
            // END: R25 - Added by jacuna
            case 35: // reports/transactions/merchant_credit_accountstatus.jsp
                break;
            case 36:// reports/transactions/money_transfers.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_MONEY_TRANSFER)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 37: // reports/analysis/billingreport.jsp
                if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && getAccessLevel()
                        .equals(DebisysConstants.ISO))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 38: // reports/transactions/transaction_by_city.jsp
                break;
            // R27:DBSY-585. Added by Jacuna
            case 39: // reports/transactions/merchants_joined.jsp
                if (!(sessionData.checkPermission(DebisysConstants.PERM_MERCHANT_JOINED)
                        && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && getAccessLevel()
                        .equals(DebisysConstants.ISO))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            // End. R27:DBSY-585. Added by Jacuna
            case 40: // reports/payments/detailed_payments.jsp
                if (!(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_DETAILED_PAYMENT_REPORT) && DebisysConfigListener
                        .getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;

            case 41: // reports/analysis/terminals_by_serialNumber
                if (!(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_SEARCH_TERMINALS_BY_SERIALNUMBER))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 43: // reports/transactions/nfinanse.jsp
                if (!(getAccessLevel().equals(DebisysConstants.ISO) || getAccessLevel().equals(DebisysConstants.MERCHANT))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 44: // reports/transactions/merchant_detail_site.jsp
                break;
            case 45: // reports/transactions/sales_limit.jsp
                if (getAccessLevel().equals(DebisysConstants.MERCHANT)
                        || !sessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 46: // reports/transactions/products_merchants_summary.jsp
                break;
            case 47: // reports/transactions/getuser.jsp
                break;
            case 49: // reports/transactions/products_merchants.jsp
                break;
            case 50:
                if (!(((getAccessLevel().equals(DebisysConstants.ISO) || getAccessLevel().equals(DebisysConstants.AGENT)
                        || getAccessLevel().equals(DebisysConstants.SUBAGENT) || getAccessLevel().equals(DebisysConstants.REP))) && customConfigType
                        .equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 51: // reports/transactions/reconciliation_summary.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) && !sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 52: // reports/transactions/reconciliation_detail.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO) && !sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 53: // reports/transactions/transactions_org.jsp
                if (!getAccessLevel().equals(DebisysConstants.ISO)
                        && !sessionData.checkPermission(DebisysConstants.PERM_VIEW_TRANSACTIONS_ORIGINATION_REPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 54: // reports/transactions/pinmoney.jsp
                if (!sessionData.checkPermission(DebisysConstants.PERM_ENABLE_PINMONEY_TRANSACTIONS_REPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 55: //FMR Commission Report
                if (!sessionData.checkPermission(DebisysConstants.PERM_ENABLE_FRM_COMMISSIONS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 56: //DTU-1149	Belize: Customized Report
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TRX_SUMMARY_RECON_REPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 57: //DTU-1149	Belize: Customized Report								
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TRX_DETAILED_RECON_REPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 58: //DTU-544 Real-time Transaction Reporting
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !sessionData.checkPermission(DebisysConstants.REAL_TIME_TRANSACTION_REPORT)) {
                    if (!DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) || !DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                        response.sendRedirect("/support/message.jsp?message=1");
                        return;
                    }
                }
                break;
            case 59: // schedule reports
                boolean isInternational = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                        && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
                boolean permissionFlag = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS) 
                                                && sessionData.checkPermission(DebisysConstants.PERM_VIEW_MANAGED_SCHEDULE_REPORTS);
                if (!permissionFlag || !isInternational) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 60: // schedule reports
                isInternational = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                        && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
                if ((getAccessLevel().equals(DebisysConstants.AGENT) || getAccessLevel().equals(DebisysConstants.MERCHANT) || (getAccessLevel().equals(DebisysConstants.SUBAGENT)) && !isInternational)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 61: //Merchant Map Locator
                if (!sessionData.checkPermission(DebisysConstants.PERM_ALLOW_MERCHANT_MAP_LOCATOR_REPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 62: //IMTU report
                int intranetUser = sessionData.getUser().getIntranetUserId();
                boolean intraPermission = false;
                try {
                    intraPermission = sessionData.getUser().hasIntranetUserPermission(intranetUser, 1, DebisysConstants.INTRANET_PERMISSION_IMTU_REPORT);
                    //System.out.println("INTRANET_PERMISSION_IMTU_REPORT [" + intraPermission + "]");
                    //System.out.println("isIntranetUser  [" + sessionData.getUser().isIntranetUser() + "]");
                } catch (UserException e) {
                    e.printStackTrace();
                }
                if (!sessionData.getUser().isIntranetUser() || !intraPermission) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 63: //SIM Inventory Report
                if (!sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 64: //Stock levels threshold Report
                if (!sessionData.checkPermission(DebisysConstants.PERM_ALLOW_STOCK_LEVELS_THRESHOLD_REPORT)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 65: //Emida Balance on Provider report
                intranetUser = sessionData.getUser().getIntranetUserId();
                intraPermission = false;
                try {
                    intraPermission = sessionData.getUser().hasIntranetUserPermission(intranetUser, 1, DebisysConstants.INTRANET_PERMISSION_IMTU_REPORT);
                } catch (UserException e) {

                }
                if (!sessionData.getUser().isIntranetUser() || !intraPermission) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 66:
                intranetUser = sessionData.getUser().getIntranetUserId();
                if (!sessionData.getUser().isIntranetUser()) {
                    response.sendRedirect("/support/message.jsp?message=1");
                }
                break;
            case 67: // Switch Account Report
                if (!getAccessLevel().equals(DebisysConstants.ISO) || !sessionData.checkPermission(DebisysConstants.PERM_VIEW_INTERNAL_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            case 68: // Convenience Card Reports
                 if (!sessionData.checkPermission(DebisysConstants.PERM_VIEW_CONVENIENCE_CARDS_SUMMARY_REPORTS)) {
                    response.sendRedirect("/support/message.jsp?message=1");
                    return;
                }
                break;
            default:
                response.sendRedirect("/support/message.jsp?message=1");
                return;
        }
    }

    /**
     * @return the accessLevel
     */
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * @param accessLevel the accessLevel to set
     */
    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    /**
     * @return the distChainType
     */
    public String getDistChainType() {
        return distChainType;
    }

    /**
     * @param distChainType the distChainType to set
     */
    public void setDistChainType(String distChainType) {
        this.distChainType = distChainType;
    }

}
