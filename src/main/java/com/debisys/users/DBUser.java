/**
 * 
 */
package com.debisys.users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author nmartinez
 *
 */
public class DBUser {
	
	
	/**
	 * 
	 */
	private static ResourceBundle sql_bundle       = ResourceBundle.getBundle("com.debisys.users.sql");
	
	/**
	 * 
	 */
	private static Category               cat              = Category.getInstance(DBUser.class);
	
	 
	
	/**
	 * 
	 * @param blackListPermissions
	 * @return
	 */
	public static ArrayList<String> getAllPermissionsId(String[] blackListPermissions)
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    ResultSet rs = null; 
	    ArrayList<String> permissionsId = new ArrayList<String>(); 
	    try
	    { 
	      dbConn = Torque.getConnection(DBUser.sql_bundle.getString("pkgDefaultDb"));
	      if (dbConn == null)
	      {
	        cat.error("Can't get database connection");
	        throw new Exception("Can't get database connection");
	      }
	      pstmt = dbConn.prepareStatement(DBUser.sql_bundle.getString("getAllPermissionsId"));
	      rs = pstmt.executeQuery();
	      while (rs.next())
	      {
	    	  boolean notFoundPermission=true;
	    	  for(String permission : blackListPermissions)
	    	  {
	    		  if ( permission.equals(rs.getString(1)) )
	    		  {
	    			  notFoundPermission = false;
	    		  }	    		  
	    	  }
	    	  if (notFoundPermission)
	    	  {
	    		  permissionsId.add(rs.getString(1));
	    	  }
	      }
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during getAllPermissionsId", e);
	    }
	    finally
	    {
	      try
	      {
	        TorqueHelper.closeConnection(dbConn, pstmt, rs);
	      }
	      catch (Exception e)
	      {
	        cat.error("Error during closeConnection", e);
	      }
	    }
	    return permissionsId;
	  }
	
	
	
}
