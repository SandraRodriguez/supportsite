package com.debisys.users;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.properties.Properties;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.Log;
import com.debisys.utils.LogChanges;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;
import java.io.IOException;
import net.emida.supportsite.mvc.security.dao.PasswordPojo;
import net.emida.supportsite.util.SpringBeanFactory;
import net.emida.supportsite.util.security.SecurityCipherService;

/**
 * Holds the information for users.
 * <P>
 *
 * @author Jay Chi
 */
public class User implements HttpSessionBindingListener {

    private String username;
    private String password;
    private String email;
    private boolean isEncrypted = false;
    private String lastSuccessfulLogin = "";
    private String encryptedPassword = "";
    private String lastPasswordChange = "";

    private String companyName;
    private String contactName;
    private String creditType;
    private String passwordId = "0";
    private String refType = "0";
    private String refId = "0";
    private String isoId = "0";
    private String accessLevel = "0";
    private String distChainType = "1";
    private String permissionTypeId = "";
    private boolean qcommBusiness;
    private Date qcommBusinessTransitionDate;
    private long status = 0;
    private String statusLastChange = "";
    private Hashtable<String, String> validationErrors = new Hashtable<String, String>();
    private String datasource;
    private boolean intranetUser = false;
    private int intranetUserId = 0;
    private String IdNew;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Calendar cal = null;

    //R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP	
    private String name = "";
    private String language = "0";
    private String languageCode = "0";
    //End R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP

    // log4j logging
    // static Category cat = Category.getInstance(User.class);
    private static Logger cat = Logger.getLogger(User.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.users.sql");

    private boolean supportSiteNgUser = false;

    public boolean isSupportSiteNgUser() {
        return supportSiteNgUser;
    }

    public void setSupportSiteNgUser(boolean supportSiteNgUser) {
        this.supportSiteNgUser = supportSiteNgUser;
    }

    public boolean isError() {
        return (this.validationErrors.size() > 0);
    }

    //R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP
    public String getName() {
        return StringUtil.toString(this.name);
    }

    public void setName(String strName) {
        this.name = strName.trim();
    }

    public String getLanguage() {
        return StringUtil.toString(this.language);
    }

    public void setLanguage(String strLanguage) {
        this.language = strLanguage.trim();
        if (this.language.equalsIgnoreCase("English")) {
            this.languageCode = "1";
        } else if (this.language.equalsIgnoreCase("Spanish")) {
            this.languageCode = "2";
        }
    }

    //End R34 DBSY - DOCUMENT REPOSITORY SUPPORT SITE REVAMP
    public String getUsername() {
        return StringUtil.toString(this.username);
    }

    public void setUsername(String strUsername) {
        this.username = strUsername.trim();
    }

    public String getPassword() {
        return StringUtil.toString(this.password);
    }

    public void setPassword(String strPassword) {
        if (strPassword != null) {
            this.password = strPassword.trim();
        } else {
            this.password = null;
        }
    }

    public String getEmail() {
        return StringUtil.toString(this.email);
    }

    public void setEmail(String strEmail) {
        this.email = strEmail.trim();
    }

    public String getLastSuccessfulLogin() {
        return lastSuccessfulLogin;
    }

    public void setLastSuccessfulLogin(String lastSuccessfulLogin) {
        this.lastSuccessfulLogin = lastSuccessfulLogin;
    }

    public String getRefType() {
        return this.refType;
    }

    public void setRefType(String strRefType) {
        this.refType = strRefType;
    }

    public String getRefId() {
        return this.refId;
    }

    public void setRefId(String strRefId) {
        this.refId = strRefId;
    }

    public String getPasswordId() {
        return this.passwordId;
    }

    public void setpasswordId(String strpasswordId) {
        this.passwordId = strpasswordId;
    }

    public void setCompanyName(String strCompanyName) {
        this.companyName = StringUtil.toString(strCompanyName);
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public void setContactName(String strContactName) {
        this.contactName = StringUtil.toString(strContactName);
    }

    public String getContactName() {
        return this.contactName;
    }
    
    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public void setPermissionTypeId(String strPermissionTypeId) {
        this.permissionTypeId = StringUtil.toString(strPermissionTypeId);
    }

    public String getPermissionTypeId() {
        return this.permissionTypeId;
    }

    public void setPermissions(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // this is a check that gets all iso assignable permissions
            // and makes sure that the user is not assigned any permissions
            // that are not allowed in the assignable permissions
            String strAccessLevel = sessionData.getProperty("access_level");

            Hashtable<String, String> hashAssignablePermissions = new Hashtable<String, String>();
            // Vector vecAssignablePermissions = new Vector();
            Vector<Vector<String>> vecAssignablePermissions = this.getAssignablePermissions(sessionData, strAccessLevel);
            if (vecAssignablePermissions.isEmpty()) {
                User.cat.error("### Seems to be you don't have permissions by cause a Network error. Trying again get them.");
                vecAssignablePermissions = this.getAssignablePermissions(sessionData, strAccessLevel);
                User.cat.error("### Getting permissions list again. size=" + vecAssignablePermissions + " elements");
            }

            Iterator<Vector<String>> it = vecAssignablePermissions.iterator();
            while (it.hasNext()) {
                Vector<String> vecTemp = null;
                vecTemp = it.next();
                if (!hashAssignablePermissions.containsKey(vecTemp.get(0).toString())) {
                    hashAssignablePermissions.put(vecTemp.get(0).toString(), "y");
                }
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getUserPermissions"));
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                // if its an assignable permission for the iso then allow it otherwise delete it
                if (hashAssignablePermissions.containsKey(rs.getString("web_permission_type_id"))) {
                    sessionData.setPermission(rs.getString("web_permission_type_id"));
                } else {
                    // DBSY-890
                    if (rs.getString("web_permission_type_id").equals("52")) {
                        // Set perm 52 in session data
                        sessionData.setPermission(rs.getString("web_permission_type_id"));
                        // Can't really prevent the ISO from disallowing 52 only for ISOs and not carriers
                        // since carrier users don't get their entries in web_iso_permissions
                        // If we remove 52 from web_iso_permissions, all carrier users turn into
                        // normal ISO users
                    } else {
                        // delete it because they shouldnt have this permission
                        // deleteUserPerm=delete from web_user_permissions where password_id=?
                        String strSQL = User.sql_bundle.getString("deleteUserPerm");
                        strSQL = strSQL + " and web_permission_type_id=?";
                        pstmt = dbConn.prepareStatement(strSQL);
                        pstmt.setDouble(1, Double.parseDouble(this.passwordId));
                        pstmt.setDouble(2, Double.parseDouble(rs.getString("web_permission_type_id")));
                        pstmt.executeUpdate();
                    }
                }
            }
        } catch (Exception e) {
            User.cat.error("Error during setPermissions", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
    }

    public Vector<Vector<String>> getAssignablePermissions(SessionData sessionData, String strAccessLevel) {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector<Vector<String>> vecAssignablePermissions = new Vector<Vector<String>>();
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // getAssignablePermissions=select wip.web_permission_type_id, wpt.description from web_iso_permissions wip, web_permission_types wpt where
            // wip.web_permission_type_id=wpt.web_permission_type_id AND iso_id=?
            String strSQL = User.sql_bundle.getString("getAssignablePermissions");
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " AND wip.iso='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " AND wip.agent='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQL = strSQL + " AND wip.subagent='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                strSQL = strSQL + " AND wip.rep='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.REFERRAL)) {
                strSQL = strSQL + " AND wip.ref='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " AND wip.carrier='Y'";
            } else {
                strSQL = strSQL + " AND wip.merchant='Y'";
            }

            strSQL = strSQL + " ORDER BY wip.web_permission_type_id";

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, StringUtils.isNotEmpty(sessionData.getProperty("iso_id")) ? Double.parseDouble(sessionData.getProperty("iso_id")) : -1);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(rs.getString("web_permission_type_id"));
                vecTemp.add(rs.getString("description"));
                vecAssignablePermissions.add(vecTemp);
            }
        } catch (Exception e) {
            User.cat.error("Error during getAssignablePermissions", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return vecAssignablePermissions;
    }

    public int getSequenceLastWebPermissionTypes() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int lastWebPermissionType = 73;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // getAssignablePermissions=select wip.web_permission_type_id, wpt.description from web_iso_permissions wip, web_permission_types wpt where
            // wip.web_permission_type_id=wpt.web_permission_type_id and iso_id=?
            String strSQL = "";//User.sql_bundle.getString("getAssignablePermissions");

            strSQL = "SELECT MAX(wpt.web_permission_type_id) AS web_permission_type_id FROM web_permission_types AS wpt WITH(NOLOCK)";

            pstmt = dbConn.prepareStatement(strSQL);
            //pstmt.setDouble(1, Double.parseDouble(sessionData.getProperty("iso_id")));

            rs = pstmt.executeQuery();

            while (rs.next()) {
                lastWebPermissionType = Integer.parseInt(rs.getString("web_permission_type_id"));
            }
        } catch (Exception e) {
            User.cat.error("Error during getAssignablePermissions", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return lastWebPermissionType;
    }

    public Vector<Vector<String>> getUserPermissions(SessionData sessionData, String strAccessLevel) {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector<Vector<String>> vecAssignablePermissions = new Vector<Vector<String>>();
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // getAssignablePermissions=select wip.web_permission_type_id, wpt.description from web_iso_permissions wip, web_permission_types wpt where
            // wip.web_permission_type_id=wpt.web_permission_type_id AND iso_id=?
            String strSQL = User.sql_bundle.getString("getAssignablePermissions");
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " AND wip.iso='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " AND wip.agent='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQL = strSQL + " AND wip.subagent='Y'";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                strSQL = strSQL + " AND wip.rep='Y'";
            } else {
                strSQL = strSQL + " AND wip.merchant='Y'";
            }

            strSQL = strSQL + " ORDER BY wip.web_permission_type_id";

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(sessionData.getProperty("iso_id")));

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(rs.getString("web_permission_type_id"));
                vecTemp.add(rs.getString("description"));
                vecAssignablePermissions.add(vecTemp);
            }
        } catch (Exception e) {
            User.cat.error("Error during getAssignablePermissions", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return vecAssignablePermissions;
    }

    public void setAccessLevel() throws UserException {
        // 0 -> Default/Invalid
        // 1 -> ISO
        // 2 -> Agent
        // 3 -> Subagent
        // 4 -> Rep
        // 5 -> Merchant

        //MCR ADD
        //9 -> Carrier
        //
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }

            if (this.refType.equals(DebisysConstants.PW_REF_TYPE_CARRIER)) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkRep"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    int intRepType = rs.getInt("type");

                    if (intRepType == Integer.parseInt(DebisysConstants.PW_REF_TYPE_CARRIER)) {
                        // Referral
                        this.setCompanyName(rs.getString("businessname"));
                        this.setContactName(rs.getString("firstname") + " " + rs.getString("lastname"));
                        this.accessLevel = DebisysConstants.CARRIER;
                        this.setDistChainType(DebisysConstants.DIST_CHAIN_5_LEVEL);
                    } else {
                        User.cat.error("Error during setAccessLevel");
                        throw new UserException();
                    }
                }
            } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_MERCHANT)) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkMerchant"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    // Merchant
                    // reset the merchants.rec_id to point to merchants.merchant_id
                    this.setRefId(rs.getString("merchant_id"));
                    this.setCompanyName(rs.getString("dba"));
                    this.setContactName(rs.getString("contact"));
                    this.accessLevel = DebisysConstants.MERCHANT;
                    int intRepType = rs.getInt("type");
                    if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_REP)) {
                        // Rep
                        if (rs.getInt("isotype") == 2) {
                            this.setDistChainType(DebisysConstants.DIST_CHAIN_3_LEVEL);
                        } else {
                            this.setDistChainType(DebisysConstants.DIST_CHAIN_5_LEVEL);
                        }
                    } else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_3_LEVEL)) {
                        // Iso 3 Level
                        this.setDistChainType(DebisysConstants.DIST_CHAIN_3_LEVEL);
                    } else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_5_LEVEL)) {
                        // Iso 5 Level
                        this.setDistChainType(DebisysConstants.DIST_CHAIN_5_LEVEL);
                    } else {
                        User.cat.error("Error during setAccessLevel");
                        throw new UserException();
                    }
                } else {
                    User.cat.error("Error during setAccessLevel");
                    throw new UserException();
                }
            } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_REP_ISO)) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkRep"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    int intRepType = rs.getInt("type");
                    this.setCompanyName(rs.getString("businessname"));
                    this.setContactName(rs.getString("firstname") + " " + rs.getString("lastname"));

                    if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_REP)) {
                        // Rep
                        this.accessLevel = DebisysConstants.REP;
                        if (rs.getInt("isotype") == 2) {
                            this.setDistChainType(DebisysConstants.DIST_CHAIN_3_LEVEL);
                        } else {
                            this.setDistChainType(DebisysConstants.DIST_CHAIN_5_LEVEL);
                        }
                    } else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_3_LEVEL)) {
                        // Iso 3 Level
                        this.accessLevel = DebisysConstants.ISO;
                        this.setDistChainType(DebisysConstants.DIST_CHAIN_3_LEVEL);
                    } else if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_ISO_5_LEVEL)) {
                        // Iso 5 Level
                        this.accessLevel = DebisysConstants.ISO;
                        this.setDistChainType(DebisysConstants.DIST_CHAIN_5_LEVEL);
                    } else {
                        User.cat.error("Error during setAccessLevel");
                        throw new UserException();
                    }

                }
            } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_AGENT)) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkRep"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    int intRepType = rs.getInt("type");
                    if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_AGENT)) {
                        // Agent
                        this.setCompanyName(rs.getString("businessname"));
                        this.setContactName(rs.getString("firstname") + " " + rs.getString("lastname"));
                        this.accessLevel = DebisysConstants.AGENT;
                        this.setDistChainType(DebisysConstants.DIST_CHAIN_5_LEVEL);
                    } else {
                        User.cat.error("Error during setAccessLevel");
                        throw new UserException();
                    }

                }
            } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_SUBAGENT)) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkRep"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    int intRepType = rs.getInt("type");
                    if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT)) {
                        // Agent
                        this.setCompanyName(rs.getString("businessname"));
                        this.setContactName(rs.getString("firstname") + " " + rs.getString("lastname"));
                        this.setCreditType(rs.getString("credit_type"));
                        this.accessLevel = DebisysConstants.SUBAGENT;
                        this.setDistChainType(DebisysConstants.DIST_CHAIN_5_LEVEL);
                    } else {
                        User.cat.error("Error during setAccessLevel");
                        throw new UserException();
                    }

                }
            } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_REFERRAL)) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkRep"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    int intRepType = rs.getInt("type");
                    if (intRepType == Integer.parseInt(DebisysConstants.REP_TYPE_REFERRAL)) {
                        // Referral
                        this.setCompanyName(rs.getString("businessname"));
                        this.setContactName(rs.getString("firstname") + " " + rs.getString("lastname"));
                        this.accessLevel = DebisysConstants.REFERRAL;
                    } else {
                        User.cat.error("Error during setAccessLevel");
                        throw new UserException();
                    }
                }
            }
        } catch (Exception e) {
            User.cat.error("Error during setAccessLevel", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

    }

    public String getAccessLevel() {
        return this.accessLevel;
    }

    public void setDistChainType(String strValue) {
        this.distChainType = StringUtil.toString(strValue);
    }

    public String getDistChainType() {
        return this.distChainType;
    }

    /**
     * Returns a hash of errors.
     *
     * @return Hash of errors.
     */
    public Hashtable<String, String> getErrors() {
        return this.validationErrors;
    }

    /**
     * Returns a validation error against a specific field. If a field was found
     * to have an error during {@link #validateUser validateUser}, the error
     * message can be accessed via this method.
     *
     * @param fieldname The bean property name of the field
     * @return The error message for the field or null if none is present.
     */
    public String getFieldError(String fieldname) {
        return this.validationErrors.get(fieldname);
    }

    /**
     * Sets the validation error against a specific field. Used by
     * {@link #validateUser validateUser}.
     *
     * @param fieldname The bean property name of the field
     * @param error The error message for the field or null if none is present.
     */
    public void addFieldError(String fieldname, String error) {
        this.validationErrors.put(fieldname, error);
    }

    /**
     * Validates the user form for missing or invalid fields.
     *
     * @return <code>true</code> if the entity passes the validation checks,
     * otherwise <code>false</code>
     */
    public boolean validateUser(SessionData sessionData) {
        this.validationErrors.clear();
        boolean valid = true;
        if ((this.username == null) || (this.username.length() == 0)) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.password == null) || (this.password.length() == 0)) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password", sessionData.getLanguage()));
            valid = false;
        }

        return valid;
    }

    /**
     * Validates the add user form for missing or invalid fields.
     *
     * @return <code>true</code> if the entity passes the validation checks,
     * otherwise <code>false</code>
     */
    public boolean validateAddUpdateUser(SessionData sessionData, ServletContext context) {
        this.validationErrors.clear();
        boolean valid = true;
        boolean isPasswordWithoutId = false;
        if ( ((String)context.getAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM)).length()>0 )
            isPasswordWithoutId = ((Integer.parseInt((String) context.getAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM))) == 1) ? true : false;
        
        

        if ((this.username == null) || (this.username.length() == 0)) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.password == null) || (this.password.isEmpty()) || (this.password.length() == 0)) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password", sessionData.getLanguage()));
            valid = false;
        } else {
            Pattern p = Pattern.compile("^(?=.{8,})(?=.*[!@#\\$%\\^&\\*\\(\\)\\+=\\|;'\"{}<>\\.\\?\\-_\\\\/:,~`])(?=.*\\d.*\\d.*\\d)(?=.*[A-Z]).*$");
            Matcher m = p.matcher(this.password);
            boolean matchFound = m.matches();

            if (!matchFound) {
                this.addFieldError("password", Languages.getString("jsp.admin.passwordChangeError", sessionData.getLanguage()));
                valid = false;
            } else {
                if (isPasswordWithoutId
                        && this.username != null && !this.username.isEmpty() && this.username.length() > 1
                        && this.password != null && !this.password.isEmpty() && this.password.length() > 1
                        && ((this.name.isEmpty()) || (this.name != null && !this.name.isEmpty() && this.name.length() > 1))) {

                    if (isIdUserInPassword(this.password, this.username.replaceAll("\\s+", ""), this.name.replaceAll("\\s+", ""))) {
                        this.addFieldError("password", Languages.getString("com.debisys.users.error_password_contains_login_name", sessionData.getLanguage()));
                        valid = false;
                    }
                }
            }
        }

        // check to make sure username >= 3 chracters.
        if (valid && StringUtil.toString(this.username).length() < 3) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username_length", sessionData.getLanguage()));
            valid = false;
        }

        // check to make sure username is alphanumeric
        if (valid && !StringUtil.isAlphaNumeric(this.username)) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username_alpha", sessionData.getLanguage()));
            valid = false;
        }
        // check to make sure password is alphanumeric
        //	if (valid && !StringUtil.isAlphaNumeric(this.password))
        // {
        //  this.addFieldError("password", Languages.getString("com.debisys.users.error_password_alpha", sessionData.getLanguage()));
        //  valid = false;
        // }

        // check to make sure username is unique
        if (valid && this.checkUsername()) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username_duplicate", sessionData.getLanguage()));
            valid = false;
        }

        Pattern pEmail = Pattern.compile("^[_a-zA-Z0-9-\\/]+(\\.[_a-zA-Z0-9-\\/]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$", Pattern.CASE_INSENSITIVE);
        //Pattern.compile("^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$",Pattern.CASE_INSENSITIVE);
        if (this.email != null) {
            // Match the given string with the pattern
            Matcher mEail = pEmail.matcher(this.email);

            // check whether match is found
            boolean foundMatch = mEail.matches();
            if (!foundMatch && (this.email != null) && (this.email.length() != 0)) {
                this.addFieldError("email", Languages.getString("jsp.admin.emailError", sessionData.getLanguage()));
                valid = false;
            }
        }
        return valid;
    }

    public boolean validateAddUpdateMobileUser(SessionData sessionData) {
        this.validationErrors.clear();
        boolean valid = true;
        if ((this.username == null) || (this.username.length() == 0)) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.password == null) || (this.password.length() == 0)) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password", sessionData.getLanguage()));
            valid = false;
        }

        // check to make sure username >= 3 chracters.
        if (valid && StringUtil.toString(this.username).length() < 3) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username_length", sessionData.getLanguage()));
            valid = false;
        }

        // check to make sure password >= 3 chracters.
        if (valid && StringUtil.toString(this.password).length() < 3) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password_length", sessionData.getLanguage()));
            valid = false;
        }

        // check to make sure username is alphanumeric
        if (valid && !StringUtil.isAlphaNumeric(this.username)) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username_alpha", sessionData.getLanguage()));
            valid = false;
        }
        // check to make sure password is alphanumeric
        if (valid && !StringUtil.isAlphaNumeric(this.password)) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password_alpha", sessionData.getLanguage()));
            valid = false;
        }

        // check to make sure username is unique
        if (valid && this.checkUsername()) {
            this.addFieldError("username", Languages.getString("com.debisys.users.error_username_duplicate", sessionData.getLanguage()));
            valid = false;
        }

        Pattern pEmail = Pattern.compile("^[_a-zA-Z0-9-\\/]+(\\.[_a-zA-Z0-9-\\/]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$", Pattern.CASE_INSENSITIVE);
        //Pattern.compile("^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$",Pattern.CASE_INSENSITIVE);

        if (this.email != null) {
            // Match the given string with the pattern
            Matcher mEail = pEmail.matcher(this.email);

            // check whether match is found
            boolean foundMatch = mEail.matches();
            if (!foundMatch && (this.email != null) && (this.email.length() != 0)) {
                this.addFieldError("email", Languages.getString("jsp.admin.emailError", sessionData.getLanguage()));
                valid = false;
            }
        }
        return valid;
    }

    public boolean validateEmail(SessionData sessionData) {
        this.validationErrors.clear();
        boolean valid = true;
        Pattern pEmail = Pattern.compile("^[_a-zA-Z0-9-\\/]+(\\.[_a-zA-Z0-9-\\/]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$", Pattern.CASE_INSENSITIVE);
        //Pattern.compile("^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$",Pattern.CASE_INSENSITIVE);

        // Match the given string with the pattern
        Matcher mEail = pEmail.matcher(this.email);
        boolean foundMatch = mEail.matches();
        if (!foundMatch && (this.email != null) && (this.email.length() != 0)) {
            this.addFieldError("email", Languages.getString("jsp.admin.emailError", sessionData.getLanguage()));
            valid = false;
        }
        return valid;

    }

    /**
     * Validates the add user form for missing or invalid fields.
     *
     * @return <code>true</code> if the entity passes the validation checks,
     * otherwise <code>false</code>
     */
    public boolean validatePassword(SessionData sessionData, ServletContext context, HttpServletRequest request) {
        this.validationErrors.clear();
        boolean valid = true;
        boolean isPasswordWithoutId = ((Integer.parseInt((String) context.getAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM))) == 1) ? true : false;

        if ((this.password == null) || (this.password.isEmpty()) || (this.password.length() == 0)) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password", sessionData.getLanguage()));
            valid = false;
        } else {
            Pattern p = Pattern.compile("^(?=.{8,})(?=.*[!@#\\$%\\^&\\*\\(\\)\\+=\\|;'\"{}<>\\.\\?\\-_\\\\/:,~`])(?=.*\\d.*\\d.*\\d)(?=.*[A-Z]).*$");
            Matcher m = p.matcher(this.password);
            boolean matchFound = m.matches();

            if (!matchFound) {
                this.addFieldError("password", Languages.getString("jsp.admin.passwordChangeError", sessionData.getLanguage()));
                valid = false;
            } else {
                if (isPasswordWithoutId && this.password != null && !this.password.isEmpty() && this.password.length() > 1) {

                    String personId = (request.getParameter("personId") != null && !request.getParameter("personId").isEmpty()) ? request.getParameter("personId").replaceAll("\\s+", "") : "";
                    String personName = (request.getParameter("personName") != null && !request.getParameter("personName").isEmpty()) ? request.getParameter("personName").replaceAll("\\s+", "") : "";

                    if (isIdUserInPassword(this.password, personId, personName)) {
                        this.addFieldError("password", Languages.getString("com.debisys.users.error_password_contains_login_name", sessionData.getLanguage()));
                        valid = false;
                    }
                }
            }
        }
        return valid;
    }

    public boolean validateMobilePassword(SessionData sessionData) {
        this.validationErrors.clear();
        boolean valid = true;

        if ((this.password == null) || (this.password.length() == 0)) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password", sessionData.getLanguage()));
            valid = false;
        }

        if (valid && StringUtil.toString(this.password).length() < 3) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password_length", sessionData.getLanguage()));
            valid = false;
        }

        // check to make sure password is alphanumeric
        if (valid && !StringUtil.isAlphaNumeric(this.password)) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password_alpha", sessionData.getLanguage()));
            valid = false;
        }

        return valid;
    }

    /**
     * Validates username and password
     *
     * @throws UserException Thrown on database errors
     */
    public boolean validateLogin(ServletContext context, SessionData sessionData, boolean isMultiIso) throws UserException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean loggedIn = false;        
        String seeder = (String) context.getAttribute("secretSeed");
        SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("validateLogin"));
            pstmt.setString(1, this.getUsername());
            PasswordPojo passwordPojo = checkPasswordEncryption();
            String passwordTmp = this.password;
            boolean isOldpw = Boolean.FALSE;
            if (passwordPojo.getEncrypted() && !isMultiIso) {
                isOldpw = cipherService.isOldAlgorithm(passwordTmp, passwordPojo.getLogonPassword());                
                //encryptedPassword = encryptedPassword(this.password);                               
                if (isOldpw) {
                    encryptedPassword = cipherService.encryptWithOldAlgorithm(this.password);
                    updatePassword(sessionData, context);
                } else {
                    encryptedPassword = cipherService.encrypt(this.password);                            
                }                
                pstmt.setString(2, encryptedPassword);
            } else {
                pstmt.setString(2, this.password);
            }

            rs = pstmt.executeQuery();
            User.cat.debug(pstmt.toString());
            if (rs.next()) {
                this.status = rs.getLong("Status");
                this.statusLastChange = rs.getString("StatusLastChange");
                if (this.status == 0) {
                    this.addFieldError("login", Languages.getString("com.debisys.users.error_user_disabled", sessionData.getLanguage()));
                    System.out.println("The user is disabled");
                    loggedIn = false;
                } else {
                    this.passwordId = rs.getString("password_id");
                    this.refId = rs.getString("ref_id");
                    this.refType = rs.getString("ref_type");
                    this.qcommBusiness = rs.getBoolean("qcomm_check");
                    this.qcommBusinessTransitionDate = rs.getTimestamp("qcomm_date");
                    this.datasource = Torque.getDefaultDB();
                    if (rs.getString("name") != null) {
                        this.name = rs.getString("name");
                    } else {
                        this.name = "";
                    }
                    this.email = rs.getString("email");
                    this.setIdNew(rs.getString("IdNew"));

                    System.out.println("User logged in, is qcomm business ? " + this.qcommBusiness + "=" + this.qcommBusinessTransitionDate);
                    this.setAccessLevel();
                    loggedIn = true;
                }
            } else {
                this.addFieldError("login", Languages.getString("com.debisys.users.error_login", sessionData.getLanguage()));
                loggedIn = false;
            }
        } catch (Exception e) {
            User.cat.error("Error during validateLogin", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return loggedIn;
    }

    /* BEGIN Release 15.1 - Added by YH - NM */
    public static User getInfoUserMultiIso(String IsoId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        User userMultiIso = new User();
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getInfoUserMultiIso"));
            pstmt.setDouble(1, Double.parseDouble(IsoId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                userMultiIso.setUsername(rs.getString(1));
                userMultiIso.setPassword(rs.getString(2));
            }
        } catch (Exception e) {
            User.cat.error("Error during setPermissions", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return userMultiIso;
    }

    /* END Release 15.1 - Added by YH - NM */
    /**
     * Checks username to make sure its unique if strPasswordId is null then
     * checks to see if there is a user with this username if strPasswordId is
     * not null then check is made to see if the username exists under another
     * user
     */
    public boolean checkUsername() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean userExists = false;

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            if (this.passwordId == null || this.passwordId.equals("")) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkUsername"));
                pstmt.setString(1, this.username);
            } else {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkUsernameExcludePid"));
                pstmt.setString(1, this.username);
                pstmt.setDouble(2, Double.parseDouble(this.passwordId));
            }
            rs = pstmt.executeQuery();
            User.cat.debug(pstmt.toString());
            if (rs.next()) {
                userExists = true;
            }
        } catch (Exception e) {
            User.cat.error("Error during checkUsername", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return userExists;
    }

    /**
     * Checks to see if the password is encrypted to take the decision at login.
     */
    public PasswordPojo checkPasswordEncryption() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean passwordEncrypted = false;
        PasswordPojo model = new PasswordPojo();
        model.setEncrypted(false);

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }

            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkPasswordEncryption"));
            pstmt.setString(1, this.username);

            rs = pstmt.executeQuery();
            User.cat.debug(pstmt.toString());
            model.setLogonId(this.username);
            if (rs.next()) {
                boolean isEncrypted = rs.getBoolean("isEncrypted");
                if (isEncrypted) {
                    passwordEncrypted = true;
                    model.setEncrypted(isEncrypted);
                }
                model.setLogonPassword(rs.getString("Logon_password"));
            }
        } catch (Exception e) {
            User.cat.error("Error during checkPasswordEncryption", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return model;
    }

    /**
     * @param accesslevel
     * @param refid
     * @return
     * @throws UserException
     */
    public String getISOId(String accesslevel, String refid) throws UserException {
        String strIsoId = "";
        // String strAccessLevel = sessionData.getProperty("access_level");
        // String strRefId = sessionData.getProperty("ref_id");
        String strAccessLevel = accesslevel;
        String strRefId = refid;

        User.cat.debug("Access level: " + strAccessLevel);
        User.cat.debug("Ref ID: " + strRefId);
        // String strDistChainType = sessionData.getProperty("dist_chain_type");
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            // get a count of total matches
            String strSQL = User.sql_bundle.getString("getISOId");//FIX: the sql strings must be located in the sql.property file, not here
            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL += " WHERE r1.rep_id = ? ";
            } else if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.REFERRAL)) {
                strSQL += " WHERE r1.rep_id = ? ";
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL += " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id WHERE r2.rep_id = ?";
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQL += " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id WHERE r3.rep_id = ?";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                strSQL += " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id INNER JOIN reps AS r4 WITH(NOLOCK) ON r4.iso_id = r3.rep_id WHERE r4.rep_id = ?";
            } else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                strSQL += " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id INNER JOIN reps AS r4 WITH(NOLOCK) ON r4.iso_id = r3.rep_id INNER JOIN merchants AS m WITH(NOLOCK) ON r4.rep_id = m.rep_id WHERE m.merchant_id = ?";
            }
            User.cat.debug(strSQL + "/* " + strRefId + " */");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, Long.parseLong(strRefId));

            rs = pstmt.executeQuery();
            if (rs.next()) {
                strIsoId = rs.getString("rep_id");
            }
        } catch (Exception e) {
            User.cat.error("Error during getISOId", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        this.isoId = strIsoId;
        return strIsoId;
    }

    /**
     * Gets parent rep business name for given rep_id.
     *
     * @param sessionData
     * @return An iso email address of type string.
     * @throws com.debisys.exceptions.UserException Thrown
     */
    public String getISOEmail(SessionData sessionData) throws UserException {
        String strEmail = "";
        String strAccessLevel = sessionData.getProperty("access_level");
        String strRefId = sessionData.getProperty("ref_id");
        // String strDistChainType = sessionData.getProperty("dist_chain_type");
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            // get a count of total matches
            String strSQL = User.sql_bundle.getString("getISOEmail");
            if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER)) {//FIX: the sql strings must be located in the sql.property file, not here
                strSQL += " WHERE r1.rep_id = ? ";
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL += " INNER JOIN reps AS r2 ON r2.iso_id = r1.rep_id WHERE r2.rep_id= ?";
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQL += " INNER JOIN reps AS r2 ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 ON r3.iso_id = r2.rep_id WHERE r3.rep_id= ?";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                strSQL += " INNER JOIN reps AS r2 ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 ON r3.iso_id = r2.rep_id INNER JOIN reps AS r4 ON r4.iso_id = r3.rep_id WHERE r4.rep_id= ?";
            } else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                strSQL += " INNER JOIN reps AS r2 ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 ON r3.iso_id = r2.rep_id INNER JOIN reps AS r4 ON r4.iso_id = r3.rep_id INNER JOIN merchants AS m On r4.rep_id = m.rep_id WHERE m.merchant_id= ?";
            }
            User.cat.debug(strSQL + "/* " + strRefId + " */");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setLong(1, Long.parseLong(strRefId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                strEmail = rs.getString("email");
            }
        } catch (Exception e) {
            User.cat.error("Error during getISOEmail", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return strEmail;
    }

    // Removed by jay on 10/12/2005 for new permissions structure
    /**
     * Gets parent iso permission for given entity.
     *
     * @param sessionData
     * @return beta_support field from reps table.
     * @throws com.debisys.exceptions.UserException Thrown
     */
    /**
     * Gets a list of users
     *
     * @param sessionData
     *
     * @throws UserException Thrown on database errors
     */
    public Vector<Vector<Object>> getUserList(SessionData sessionData) throws UserException {
        Vector<Vector<Object>> vecUserList = new Vector<Vector<Object>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        // String strAccessLevel = sessionData.getProperty("access_level");
        // String strDistChainType = sessionData.getProperty("dist_chain_type");
        // String strRefId = sessionData.getProperty("ref_id");

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = User.sql_bundle.getString("getUserList");

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.refId));
            pstmt.setInt(2, Integer.parseInt(this.refType));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector<Object> vecTemp = new Vector<Object>();
                vecTemp.add(rs.getString("password_id"));
                vecTemp.add(rs.getString("logon_id"));
                vecTemp.add(rs.getString("logon_password"));
                // attach a hash of user permissions
                Hashtable<String, String> hashPerms = new Hashtable<String, String>();
                strSQL = User.sql_bundle.getString("getUserPermissions");
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setDouble(1, Double.parseDouble(rs.getString("password_id")));
                ResultSet rs2 = pstmt.executeQuery();
                while (rs2.next()) {
                    if (!hashPerms.containsKey(rs2.getString("web_permission_type_id"))) {
                        hashPerms.put(rs2.getString("web_permission_type_id"), "");
                    }
                }
                vecTemp.add(hashPerms);
                vecTemp.add("" + rs.getString("Status"));
                vecTemp.add("" + rs.getString("StatusLastChange"));
                if (rs.getString("name") != null) {
                    vecTemp.add("" + rs.getString("name"));
                } else {
                    vecTemp.add("");
                }
                if (rs.getString("email") != null) {
                    vecTemp.add("" + rs.getString("email"));
                } else {
                    vecTemp.add("");
                }
                vecUserList.add(vecTemp);
            }
        } catch (Exception e) {
            User.cat.error("Error during getUserList", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return vecUserList;
    }

    public void deleteUser(SessionData sessionData, ServletContext context) {
        Connection dbConn = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String instance = DebisysConfigListener.getInstance(context);
        String passwordUniqueCheck = Properties.getPropertyValue(instance, "global", "PasswordUniqueCheck");
        boolean isPasswordUniqueCheck = (passwordUniqueCheck != null && !passwordUniqueCheck.isEmpty() && passwordUniqueCheck.equalsIgnoreCase("true")) ? true : false;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }
            pstmt1 = dbConn.prepareStatement(User.sql_bundle.getString("getUserbyPasswordId"));
            User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("getUserbyPasswordId"));
            pstmt1.setDouble(1, Double.parseDouble(this.passwordId));
            User.cat.debug("Param 1: " + this.passwordId);
            rs = pstmt1.executeQuery();
            String tmpUsername = "";
            String tmpPassword = "";
            if (rs.next()) {
                tmpUsername = StringUtil.toString(rs.getString("logon_id"));
                tmpPassword = StringUtil.toString(rs.getString("logon_password"));

                // delete permissions first
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("deleteUserPerm"));
                User.cat.debug("DeleteUserPerm: " + User.sql_bundle.getString("deleteUserPerm"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                User.cat.debug("Param 1: " + this.passwordId);
                pstmt.executeUpdate();
                pstmt.close();

                boolean bIsMobileAuthenticationOn = false;
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("isMobilePermissionOn"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    bIsMobileAuthenticationOn = true;
                }

                if (!bIsMobileAuthenticationOn && isPasswordUniqueCheck) {
                    pstmt = dbConn.prepareStatement(User.sql_bundle.getString("deleteUserHistory"));
                    User.cat.debug("DeleteUserPerm: " + User.sql_bundle.getString("deleteUserHistory"));
                    pstmt.setInt(1, Integer.parseInt(this.passwordId));
                    User.cat.debug("Param 1: " + this.passwordId);
                    pstmt.executeUpdate();
                    pstmt.close();
                }
                // deleteUser=delete from password WHERE password_id=? and ref_id=? and ref_type=?
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("deleteUser"));
                User.cat.debug("DeleteUserPrm: " + User.sql_bundle.getString("deleteUser"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                User.cat.debug("Param 1: " + this.passwordId);
                pstmt.setDouble(2, Double.parseDouble(this.refId));
                User.cat.debug("Param 2: " + this.refId);
                pstmt.setInt(3, Integer.parseInt(this.refType));
                User.cat.debug("Param 3: " + this.refType);
                pstmt.executeUpdate();
                pstmt.close();

                // log the delete
                Log.write(sessionData, "Deleted web login " + "Username:" + tmpUsername, DebisysConstants.LOGTYPE_LOGIN_LOGOUT,
                        this.refId, this.refType);
                cal = Calendar.getInstance();
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                int permissionId = 0;
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    permissionId = rs.getInt("web_permission_type_id");
                }
                rs.close();
                pstmt.close();
                String levelType = getLevelType();
                if (levelType.equals("ISO") && permissionId == 52) {
                    levelType = "Carrier ISO";
                }

                String entityName = "";
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                pstmt.setDouble(2, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    entityName = rs.getString("name");
                }

                String mailHost = DebisysConfigListener.getMailHost(context);
                String mails = "";
                String bodyMessage = "";
                String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
                String subject = Languages.getString("jsp.admin.customers.user_logins.deleteUser.emailSubject", sessionData.getLanguage());
                mails = Properties.getPropertieByName(instance, "customerServiceMail");
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.deleteUser.CSemailBodyMessage", new Object[]{dateFormat.format(cal.getTime()), tmpUsername, levelType, entityName}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

            }
            pstmt1.close();
        } catch (Exception e) {
            User.cat.error("Error during deleteUser", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }

    }

    public boolean updatePassword(SessionData sessionData, ServletContext context) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String instance = DebisysConfigListener.getInstance(context);
        int passCheckTimes = 0;
        try {
            passCheckTimes = Integer.valueOf(Properties.getPropertyValue(instance, "global", "PasswordReuseTimes"));
        } catch (Exception e) {

        }

        String passwordUniqueCheck = Properties.getPropertyValue(instance, "global", "PasswordUniqueCheck");
        boolean isPasswordUniqueCheck = (passwordUniqueCheck != null && !passwordUniqueCheck.isEmpty() && passwordUniqueCheck.equalsIgnoreCase("true")) ? true : false;

        boolean passwordExists = false;
        if (isPasswordUniqueCheck) {
            passwordExists = this.checkPassword(sessionData, this.passwordId, passCheckTimes, context);
        }

        if (passwordExists) {
            this.addFieldError("password", Languages.getString("com.debisys.users.error_password_match", sessionData.getLanguage()));
            return false;
        } else {
            try {
                dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    User.cat.error("Can't get database connection");
                    throw new Exception();
                }
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getUserbyPasswordId"));
                User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("getUserbyPasswordId"));
                pstmt.setDouble(1, Double.parseDouble(this.passwordId));
                User.cat.debug("Param 1: " + this.passwordId);
                rs = pstmt.executeQuery();
                String tmpUsername = "";
                String tmpPassword = "";
                String tmpEmail = "";
                if (rs.next()) {
                    tmpUsername = StringUtil.toString(rs.getString("logon_id"));
                    tmpPassword = StringUtil.toString(rs.getString("logon_password"));
                    tmpEmail = StringUtil.toString(rs.getString("email"));
                }
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("updatePassword"));
                User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("updatePassword"));
                encryptedPassword = encryptedPassword(this.password, context);
                pstmt.setString(1, encryptedPassword);
                pstmt.setBoolean(2, true);
                User.cat.debug("Param 2: " + true);
                pstmt.setDouble(3, Double.parseDouble(this.passwordId));
                User.cat.debug("Param 3: " + this.passwordId);
                pstmt.setDouble(4, Double.parseDouble(this.refId));
                User.cat.debug("Param 4 " + this.refId);
                pstmt.setInt(5, Integer.parseInt(this.refType));
                User.cat.debug("Param 5: " + this.refType);
                int n = pstmt.executeUpdate();
                pstmt.close();
                Log.write(sessionData, "Updated password for web login " + "Username:" + tmpUsername, DebisysConstants.LOGTYPE_LOGIN_LOGOUT,
                        this.refId, this.refType);

                int numOfPassword = 0;
                boolean bIsMobileAuthenticationOn = false;
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("isMobilePermissionOn"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    bIsMobileAuthenticationOn = true;
                }

                if (!bIsMobileAuthenticationOn && isPasswordUniqueCheck) {
                    pstmt = dbConn.prepareStatement(User.sql_bundle.getString("passwordHistory"));
                    pstmt.setInt(1, Integer.parseInt(this.passwordId));
                    pstmt.setString(2, encryptedPassword(this.password, context));
                    pstmt.executeUpdate();

                    pstmt = dbConn.prepareStatement(User.sql_bundle.getString("countOfPasswordHistory"));
                    pstmt.setInt(1, Integer.parseInt(this.passwordId));
                    rs = pstmt.executeQuery();
                    if (rs.next()) {
                        numOfPassword = rs.getInt("noOfEntry");
                    }
                    if (numOfPassword > passCheckTimes && passCheckTimes != 0) {
                        pstmt = dbConn.prepareStatement(User.sql_bundle.getString("deletePasswordHistory"));
                        pstmt.setInt(1, Integer.parseInt(this.passwordId));
                        pstmt.executeUpdate();
                    }
                }

                cal = Calendar.getInstance();
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                int permissionId = 0;
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    permissionId = rs.getInt("web_permission_type_id");
                }
                rs.close();
                pstmt.close();
                String levelType = getLevelType();
                if (levelType.equals("ISO") && permissionId == 52) {
                    levelType = "Carrier ISO";
                }
                String entityName = "";
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                pstmt.setDouble(2, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    entityName = rs.getString("name");
                }

                String mailHost = DebisysConfigListener.getMailHost(context);
                String mails = "";
                String bodyMessage = "";
                String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
                String subject = Languages.getString("jsp.admin.customers.user_logins.addUser.emailSubjectUpdate", sessionData.getLanguage());
                if (tmpEmail != null && !tmpEmail.equals("")) {
                    mails = tmpEmail;
                    bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangePassword", new Object[]{dateFormat.format(cal.getTime()), tmpUsername, this.password, entityName, levelType}, sessionData.getLanguage());
                    sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

                }

                mails = Properties.getPropertieByName(instance, "customerServiceMail");
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangePasswordCS", new Object[]{dateFormat.format(cal.getTime()), tmpUsername, entityName, levelType}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
                return (n > 0);

            } catch (Exception e) {
                User.cat.error("Error during UpdatePassword", e);
                return false;
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    User.cat.error("Error during release connection", e);
                }
            }
        }

    }

    public void updateEmail(SessionData sessionData, ServletContext context) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getUserbyPasswordId"));
            User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("getUserbyPasswordId"));
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            User.cat.debug("Param 1: " + this.passwordId);
            rs = pstmt.executeQuery();
            String tmpUsername = "";
            String tmpPassword = "";
            String tmpEmail = "";
            if (rs.next()) {
                tmpUsername = StringUtil.toString(rs.getString("logon_id"));
                tmpPassword = StringUtil.toString(rs.getString("logon_password"));
                tmpEmail = StringUtil.toString(rs.getString("email"));
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("updateEmail"));
            User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("updateEmail"));
            pstmt.setString(1, this.email);
            pstmt.setDouble(2, Double.parseDouble(this.passwordId));
            User.cat.debug("Param 2: " + this.passwordId);
            pstmt.setDouble(3, Double.parseDouble(this.refId));
            User.cat.debug("Param 3 " + this.refId);
            pstmt.setInt(4, Integer.parseInt(this.refType));
            User.cat.debug("Param 4: " + this.refType);
            pstmt.executeUpdate();
            pstmt.close();
            Log.write(sessionData, "Updated email for web login " + "Username:" + tmpUsername, DebisysConstants.LOGTYPE_LOGIN_LOGOUT,
                    this.refId, this.refType);
            cal = Calendar.getInstance();
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
            pstmt.setInt(1, Integer.parseInt(this.passwordId));
            int permissionId = 0;
            rs = pstmt.executeQuery();
            if (rs.next()) {
                permissionId = rs.getInt("web_permission_type_id");
            }
            rs.close();
            pstmt.close();
            String levelType = getLevelType();
            if (levelType.equals("ISO") && permissionId == 52) {
                levelType = "Carrier ISO";
            }
            String entityName = "";
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
            pstmt.setDouble(1, Double.parseDouble(this.refId));
            pstmt.setDouble(2, Double.parseDouble(this.refId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                entityName = rs.getString("name");
            }

            String mailHost = DebisysConfigListener.getMailHost(context);
            String instance = DebisysConfigListener.getInstance(context);
            String mails = "";
            String bodyMessage = "";
            String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
            String subject = Languages.getString("jsp.admin.customers.user_logins.addUser.emailSubjectUpdate", sessionData.getLanguage());
            if (tmpEmail != null && !tmpEmail.equals("")) {
                mails = tmpEmail;
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangePassword", new Object[]{dateFormat.format(cal.getTime()), tmpUsername, this.password, entityName, levelType}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

            }

            mails = Properties.getPropertieByName(instance, "customerServiceMail");
            bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangePasswordCS", new Object[]{dateFormat.format(cal.getTime()), tmpUsername, entityName, levelType}, sessionData.getLanguage());
            sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

        } catch (Exception e) {
            User.cat.error("Error during UpdateEmail", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }

    }

    public void deleteUserPermissions() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }

            // delete permissions
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("deleteUserPerm"));
            pstmt.setInt(1, Integer.parseInt(this.passwordId));
            pstmt.executeUpdate();

        } catch (Exception e) {
            User.cat.error("Error during deleteUserPermissions", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }

    }

    public void addUser(SessionData sessionData, ServletContext context, Boolean isMobileOn) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String instance = DebisysConfigListener.getInstance(context);
        String passwordUniqueCheck = Properties.getPropertyValue(instance, "global", "PasswordUniqueCheck");
        boolean isPasswordUniqueCheck = (passwordUniqueCheck != null && !passwordUniqueCheck.isEmpty() && passwordUniqueCheck.equalsIgnoreCase("true")) ? true : false;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }
            cal = Calendar.getInstance();
            // addUser=insert into password (ref_id, ref_type, logon_id, logon_password, admin) values (?,?,?,?,0)
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("addUser"), PreparedStatement.RETURN_GENERATED_KEYS);
            User.cat.debug("Add user: " + User.sql_bundle.getString("addUser"));
            pstmt.setDouble(1, Double.parseDouble(this.refId));
            pstmt.setInt(2, Integer.parseInt(this.refType));
            pstmt.setString(3, StringUtil.toString(this.username));
            encryptedPassword = encryptedPassword(this.password, context);
            pstmt.setString(4, encryptedPassword);
            // pstmt.setString(4, StringUtil.toString(this.password));
            pstmt.setLong(5, this.status);
            pstmt.setString(6, this.name);
            pstmt.setString(7, this.email);
            pstmt.setBoolean(8, true);
            if (isMobileOn) {
                pstmt.setString(9, dateFormat.format(cal.getTime()));
            } else {
                pstmt.setString(9, null);
            }
            User.cat.debug("Params( " + this.refId + ", " + this.refType + ", " + this.username + ", " + this.status + ", " + this.name + ")");
            pstmt.executeUpdate();
            rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                this.passwordId = rs.getString(1);
            } else {
                User.cat.error("Error during addUser[Could not get identity]");
            }
            // log the add
            Log.write(sessionData, "Added web login username:" + this.username, DebisysConstants.LOGTYPE_LOGIN_LOGOUT, this.refId,
                    this.refType);
            int numOfPassword = 0;
            int passCheckTimes = 0;
            try {
                passCheckTimes = Integer.valueOf(Properties.getPropertyValue(instance, "global", "PasswordReuseTimes"));
            } catch (Exception e) {

            }
            if (!isMobileOn && isPasswordUniqueCheck) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("passwordHistory"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                pstmt.setString(2, encryptedPassword(this.password, context));
                pstmt.executeUpdate();

                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("countOfPasswordHistory"));
                pstmt.setInt(1, Integer.parseInt(passwordId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    numOfPassword = rs.getInt("noOfEntry");
                }
                if (numOfPassword > passCheckTimes && passCheckTimes != 0) {
                    pstmt = dbConn.prepareStatement(User.sql_bundle.getString("deletePasswordHistory"));
                    pstmt.setInt(1, Integer.parseInt(passwordId));
                    pstmt.executeUpdate();
                }
            }

            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
            pstmt.setInt(1, Integer.parseInt(this.passwordId));
            int permissionId = 0;
            rs = pstmt.executeQuery();
            if (rs.next()) {
                permissionId = rs.getInt("web_permission_type_id");
            }
            rs.close();
            pstmt.close();
            String levelType = getLevelType();
            if (levelType.equals("ISO") && permissionId == 52) {
                levelType = "Carrier ISO";
            }

            String entityName = "";
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
            pstmt.setDouble(1, Double.parseDouble(this.refId));
            pstmt.setDouble(2, Double.parseDouble(this.refId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                entityName = rs.getString("name");
            }

            String mailHost = DebisysConfigListener.getMailHost(context);
            String mails = "";
            String bodyMessage = "";
            String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
            String subject = Languages.getString("jsp.admin.customers.user_logins.addUser.emailSubjectAdd", sessionData.getLanguage());
            if (this.email != null && this.email != "" && this.email.length() != 0) {
                mails = this.email;
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessage", new Object[]{dateFormat.format(cal.getTime()), this.username, levelType, entityName, this.password}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

            }

            mails = Properties.getPropertieByName(instance, "customerServiceMail");
            bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.CSemailBodyMessage", new Object[]{dateFormat.format(cal.getTime()), this.username, levelType, entityName}, sessionData.getLanguage());
            sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

        } catch (Exception e) {
            User.cat.error("Error during addUser", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }

    }

    private String getLevelType() {
        String levelType = "";
        if (this.refType.equals("1")) {
            levelType = "ISO";
        }
        if (this.refType.equals("2")) {
            levelType = "Merchant";
        }
        if (this.refType.equals("3")) {
            levelType = "Administrator";
        }
        if (this.refType.equals("4")) {
            levelType = "Agent";
        }
        if (this.refType.equals("5")) {
            levelType = "Subagent";
        }
        if (this.refType.equals("7")) {
            levelType = "Referral";
        }
        if (this.refType.equals("11")) {
            levelType = "Carrier";
        }
        return levelType;
    }

    public static void sendMail(String subject, String mailHost, String sMails, String from, String textMessage, String instance) {
        try {
            java.util.Properties props;
            props = System.getProperties();
            props.put("mail.smtp.host", mailHost);
            Session s = Session.getDefaultInstance(props, null);

            MimeMessage message = new MimeMessage(s);

            message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
            message.setFrom(new InternetAddress(from));
            //message.setRecipient(Message.RecipientType.TO, new InternetAddress(Properties.getPropertieByName(instance, "customerServiceMail")));
            message.setRecipients(Message.RecipientType.TO, sMails.replaceAll(";", ","));
            message.setSubject(subject);
            message.setContent(textMessage, "text/html");
            Transport.send(message);
        } catch (MessagingException messageError) {
            User.cat.error("User.messageError - ERROR TRYING TO SEND EMAIL ", messageError);
        } catch (Exception e) {
            User.cat.error("User.messageError - ERROR SEND EMAIL ", e);
        }

    }

    /**
     * Updates the weblogin info
     *
     * @param sessionData Session information
     * @param context Application context
     * @param reason If is changing the state to disable explains why
     *
     * @returns void
	 *
     */
    public void updateUser(SessionData sessionData, ServletContext context, String reason) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean doUpdate = false;
        boolean doUpdate2 = false;
        StringBuffer changes = new StringBuffer();
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }
            // first get old username and pass and compare
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getUserbyPasswordId"));
            User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("getUserbyPasswordId"));
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            User.cat.debug("Param 1: " + this.passwordId);
            rs = pstmt.executeQuery();
            String tmpUsername = "";
            String tmpPassword = "";
            String tmpName = "";
            String tmpEmail = "";
            String log = "";
            if (rs.next()) {
                tmpUsername = StringUtil.toString(rs.getString("logon_id"));
                tmpPassword = StringUtil.toString(rs.getString("logon_password"));
                tmpName = StringUtil.toString(rs.getString("name"));
                tmpEmail = StringUtil.toString(rs.getString("email"));
                if (!tmpEmail.equalsIgnoreCase(this.email) || !tmpName.equalsIgnoreCase(this.name)) {
                    doUpdate = true;
                    if (!tmpName.equalsIgnoreCase(this.name)) {
                        log = log + "from " + tmpName + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + this.name + " ";
                        changes.append(Languages.getString("jsp.admin.customers.user_logins.name", sessionData.getLanguage()) + "<br><br>\n");
                    }
                    if (!tmpEmail.equalsIgnoreCase(this.email)) {
                        if (!log.equals("")) {
                            log = log + "and ";
                        }
                        log = log + "email from " + tmpPassword + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + this.email;
                        changes.append(Languages.getString("jsp.admin.customers.user_logins.email", sessionData.getLanguage()) + "<br><br>\n");
                    }
                }
                long tmpstatus = rs.getLong("Status");
                if (tmpstatus != this.status) {
                    doUpdate2 = true;
                    changes = changes.append(Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.status", sessionData.getLanguage()) + ":" + tmpstatus + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + this.status + "<br><br>\n");
                }
            }
            rs.close();
            pstmt.close();
            if (doUpdate) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("updateUser"));
                User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("updateUser"));
                pstmt.setString(1, StringUtil.toString(this.name));
                User.cat.debug("Param 1: " + this.name);
                pstmt.setString(2, StringUtil.toString(this.email));
                User.cat.debug("Param 1: " + this.email);
                pstmt.setDouble(3, Double.parseDouble(this.passwordId));
                User.cat.debug("Param 3: " + this.passwordId);
                pstmt.setDouble(4, Double.parseDouble(this.refId));
                User.cat.debug("Param 4 " + this.refId);
                pstmt.setInt(5, Integer.parseInt(this.refType));
                User.cat.debug("Param 5: " + this.refType);
                pstmt.executeUpdate();
                pstmt.close();
                // log the update
                Log.write(sessionData, "Updated web login " + log, DebisysConstants.LOGTYPE_LOGIN_LOGOUT, this.refId, this.refType);
            }
            if (doUpdate2) {
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("updateStatusUser"));
                pstmt.setLong(1, this.status);
                pstmt.setDouble(2, Double.parseDouble(this.passwordId));
                pstmt.setDouble(3, Double.parseDouble(this.refId));
                pstmt.setInt(4, Integer.parseInt(this.refType));
                pstmt.executeUpdate();
                pstmt.close();
                User.cat.debug((this.status == 1 ? "Enabling " : "Disabling ") + "web login " + this.username);
                if (this.status == 0) {
                    LogChanges logChanges = new LogChanges(sessionData);
                    logChanges.setContext(context);
                    logChanges.log_changed(logChanges.getLogChangeIdByName("User Status"), this.passwordId, "0", "1", reason);
                }
            }

            if (doUpdate || doUpdate2) {
                cal = Calendar.getInstance();
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                int permissionId = 0;
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    permissionId = rs.getInt("web_permission_type_id");
                }
                rs.close();
                pstmt.close();
                String levelType = getLevelType();
                if (levelType.equals("ISO") && permissionId == 52) {
                    levelType = "Carrier ISO";
                }

                String entityName = "";
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
                pstmt.setDouble(1, Double.parseDouble(this.refId));
                pstmt.setDouble(2, Double.parseDouble(this.refId));
                rs = pstmt.executeQuery();
                while (rs.next()) {
                    entityName = rs.getString("name");
                }

                String mailHost = DebisysConfigListener.getMailHost(context);
                String instance = DebisysConfigListener.getInstance(context);
                String mails = "";
                String bodyMessage = "";
                String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
                String subject = Languages.getString("jsp.admin.customers.user_logins.addUser.emailSubjectUpdate", sessionData.getLanguage());
                if (this.email != null && !this.email.equals("")) {
                    mails = this.email;
                    bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageUpdate", new Object[]{dateFormat.format(cal.getTime()), this.username, levelType, entityName, changes}, sessionData.getLanguage());
                    sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
                }
                mails = Properties.getPropertieByName(instance, "customerServiceMail");
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.CSemailBodyMessageUpdate", new Object[]{dateFormat.format(cal.getTime()), this.username, levelType, entityName, changes}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

            }
        } catch (Exception e) {
            User.cat.error("Error during updateUser", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }
    }

    public void addUserPermission() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }
            // addUserPerm=insert into web_user_permissions (password_id, web_permission_type_id) values (?,?)
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("addUserPerm"));
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            pstmt.setInt(2, Integer.parseInt(this.permissionTypeId));
            pstmt.executeUpdate();
            pstmt.close();

        } catch (Exception e) {
            User.cat.error("Error during addUserPermission", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * Checks permissions for given entity to edit their downline. repType -
     * defines what type of entity the entity is trying to access
     * <p/>
     * refId for merchants is actually the rec_id field and NOT the merchant_id
     * because the password table uses rec_id and NOT merchant_id for merchants
     * only
     *
     * @param sessionData strRepType
     * @return true or false.
     * @throws com.debisys.exceptions.UserException Thrown
     */
    public boolean checkPermissions(SessionData sessionData) throws UserException {
        String strAccessLevel = sessionData.getProperty("access_level");
        String strRefId = sessionData.getProperty("ref_id");
        // String strDistChainType = sessionData.getProperty("dist_chain_type");
        boolean bAllowed = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        if (strRefId.equals(this.refId)) {
            return false;
        }
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = User.sql_bundle.getString("getUserPermission");
            String strSQLWhere = "";
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                // if your an iso you can edit agent, subagent, rep, and merchant
                if (this.refType.equals(DebisysConstants.PW_REF_TYPE_AGENT)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id=r1.rep_id WHERE r1.rep_id= ? and r2.rep_id= ?";
                } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_SUBAGENT)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id WHERE r1.rep_id= ? and r3.rep_id= ?";
                } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_REP_ISO)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id INNER JOIN reps AS r4 WITH(NOLOCK) ON r4.iso_id = r3.rep_id WHERE r1.rep_id= ? and r4.rep_id= ?";
                } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_MERCHANT)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id INNER JOIN reps AS r4 WITH(NOLOCK) ON r4.iso_id = r3.rep_id INNER JOIN  merchants AS m WITH(NOLOCK) ON r4.rep_id = m.rep_id WHERE r1.rep_id= ? and m.rec_id= ?";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                if (this.refType.equals(DebisysConstants.PW_REF_TYPE_SUBAGENT)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id WHERE r1.rep_id= ? and r2.rep_id= ?";
                } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_REP_ISO)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id WHERE r1.rep_id= ? and r3.rep_id= ?";
                } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_MERCHANT)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN reps AS r3 WITH(NOLOCK) ON r3.iso_id = r2.rep_id INNER JOIN merchants AS m WITH(NOLOCK) ON r3.rep_id = m.rep_id WHERE r1.rep_id= ? and m.rec_id= ?";
                }
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                if (this.refType.equals(DebisysConstants.PW_REF_TYPE_REP_ISO)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id WHERE r1.rep_id= ? and r2.rep_id= ?";
                } else if (this.refType.equals(DebisysConstants.PW_REF_TYPE_MERCHANT)) {
                    strSQLWhere = " INNER JOIN reps AS r2 WITH(NOLOCK) ON r2.iso_id = r1.rep_id INNER JOIN merchants AS m WITH(NOLOCK) ON r2.rep_id=m.rep_id WHERE r1.rep_id= ? and m.rec_id= ?";
                }
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                if (this.refType.equals(DebisysConstants.PW_REF_TYPE_MERCHANT)) {
                    strSQLWhere = " INNER JOIN merchants AS m WITH(NOLOCK) ON r1.rep_id = m.rep_id WHERE r1.rep_id= ? and m.rec_id= ?";
                }
            }
            if (strSQLWhere.equals("")) {
                bAllowed = false;
            } else {
                User.cat.debug(strSQL + strSQLWhere);
                pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
                pstmt.setLong(1, Long.parseLong(strRefId));
                pstmt.setLong(2, Long.parseLong(this.refId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    bAllowed = true;
                }
                pstmt.close();
                rs.close();
            }
        } catch (Exception e) {
            User.cat.error("Error during checkPermissions", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return bAllowed;
    }

    /**
     *
     */
    public void valueBound(HttpSessionBindingEvent event) {
    }

    public void valueUnbound(HttpSessionBindingEvent event) {
    }

    /* BEGIN MiniRelease 3.0 - Added by LF */
    public boolean changePassword(String newPassword, String loginName, SessionData sessionData, ServletContext context) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String email = "";
        String passwordId = "";
        int passCheckTimes = 0;
        String instance = DebisysConfigListener.getInstance(context);

        try {
            passCheckTimes = Integer.valueOf(Properties.getPropertyValue(instance, "global", "PasswordReuseTimes"));
            Vector userResults = new Vector();
            userResults = getUserDetails(loginName);
            if (userResults != null && userResults.size() > 0) {
                email = (String) userResults.get(0);
                refId = (String) userResults.get(1);
                refType = (String) userResults.get(2);
                passwordId = (String) userResults.get(3);

            }
        } catch (Exception e) {

        }

        String passwordUniqueCheck = Properties.getPropertyValue(instance, "global", "PasswordUniqueCheck");
        boolean isPasswordUniqueCheck = (passwordUniqueCheck != null && !passwordUniqueCheck.isEmpty() && passwordUniqueCheck.equalsIgnoreCase("true")) ? true : false;

        boolean passwordExists = false;

        if (isPasswordUniqueCheck) {
            passwordExists = this.checkPassword(sessionData, passwordId, passCheckTimes, context);
        }

        if (passwordExists) {
            this.addFieldError("login", Languages.getString("com.debisys.users.error_password_match", sessionData.getLanguage()));
            return false;
        } else {

            try {
                dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    User.cat.error("Can't get database connection");
                    throw new Exception();
                }
                pst = dbConn.prepareStatement(User.sql_bundle.getString("changePassword"));
                encryptedPassword = encryptedPassword(newPassword, context);
                pst.setString(1, new String(encryptedPassword));
                email = getEmailConfigured(loginName);
                pst.setString(2, email);
                pst.setBoolean(3, true);
                // pst.setString(1, newPassword);
                pst.setString(4, loginName);
                int n = pst.executeUpdate();
                pst.close();

                instance = DebisysConfigListener.getInstance(context);

                int numOfPassword = 0;
                boolean bIsMobileAuthenticationOn = false;
                pst = dbConn.prepareStatement(User.sql_bundle.getString("isMobilePermissionOn"));
                pst.setInt(1, Integer.parseInt(passwordId));
                rs = pst.executeQuery();
                if (rs.next()) {
                    bIsMobileAuthenticationOn = true;
                }
                if (!bIsMobileAuthenticationOn && isPasswordUniqueCheck) {
                    pst = dbConn.prepareStatement(User.sql_bundle.getString("passwordHistory"));
                    pst.setInt(1, Integer.parseInt(passwordId));
                    pst.setString(2, encryptedPassword(this.password, context));
                    pst.executeUpdate();

                    pst = dbConn.prepareStatement(User.sql_bundle.getString("countOfPasswordHistory"));
                    pst.setInt(1, Integer.parseInt(passwordId));
                    rs = pst.executeQuery();
                    if (rs.next()) {
                        numOfPassword = rs.getInt("noOfEntry");
                    }
                    if (numOfPassword > passCheckTimes && passCheckTimes != 0) {
                        pst = dbConn.prepareStatement(User.sql_bundle.getString("deletePasswordHistory"));
                        pst.setInt(1, Integer.parseInt(passwordId));
                        pst.executeUpdate();
                    }
                }

                pst = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
                pst.setInt(1, Integer.parseInt(this.passwordId));
                int permissionId = 0;
                rs = pst.executeQuery();
                if (rs.next()) {
                    permissionId = rs.getInt("web_permission_type_id");
                }
                rs.close();
                pst.close();
                String levelType = getLevelType();
                if (levelType.equals("ISO") && permissionId == 52) {
                    levelType = "Carrier ISO";
                }

                String entityName = "";
                pst = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
                pst.setDouble(1, Double.parseDouble(refId));
                pst.setDouble(2, Double.parseDouble(refId));
                rs = pst.executeQuery();
                while (rs.next()) {
                    entityName = rs.getString("name");
                }
                String mailHost = DebisysConfigListener.getMailHost(context);
                String mails = "";
                String bodyMessage = "";
                cal = Calendar.getInstance();
                String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
                String subject = Languages.getString("jsp.admin.customers.user_logins.addUser.emailSubjectUpdate", sessionData.getLanguage());
                if (email != null && !email.equals("")) {
                    mails = email;
                    bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangePassword", new Object[]{dateFormat.format(cal.getTime()), loginName, newPassword, entityName, levelType}, sessionData.getLanguage());
                    sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

                }

                mails = Properties.getPropertieByName(instance, "customerServiceMail");
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangePasswordCS", new Object[]{dateFormat.format(cal.getTime()), loginName, entityName, levelType}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
                return (n > 0);
            } catch (Exception ex) {
                User.cat.error("Error during changePassword", ex);
                return false;
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pst, null);
                } catch (Exception e) {
                    User.cat.error("Error during release connection", e);
                }
            }
        }

    }

    public boolean updatePasswordLoginDate(SessionData sessionData, String newPassword, String email, String loginName, String refId, String refType, ServletContext context, String passwordId) {
        Connection dbConn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String instance = DebisysConfigListener.getInstance(context);
        String passwordUniqueCheck = Properties.getPropertyValue(instance, "global", "PasswordUniqueCheck");
        boolean isPasswordUniqueCheck = (passwordUniqueCheck != null && !passwordUniqueCheck.isEmpty() && passwordUniqueCheck.equalsIgnoreCase("true")) ? true : false;
        int passCheckTimes = 0;
        boolean isPasswordWithoutId = false;
        //( (Integer.parseInt((String)context.getAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM)))  == 1) ? true : false; 

        try {
            passCheckTimes = Integer.valueOf(Properties.getPropertyValue(instance, "global", "PasswordReuseTimes"));
            isPasswordWithoutId = ((Integer.parseInt((String) context.getAttribute(DebisysConstants.PASSWORD_WITHOUT_ID_PARAM))) == 1) ? true : false;
        } catch (Exception e) {

        }
        boolean passwordExists = false;
        if (isPasswordUniqueCheck) {
            passwordExists = this.checkPassword(sessionData, passwordId, passCheckTimes, context);
        }

        if (passwordExists) {
            this.addFieldError("login", Languages.getString("com.debisys.users.error_password_match", sessionData.getLanguage()));
            return false;
        } else {
            try {
                dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    User.cat.error("Can't get database connection");
                    throw new Exception();
                }

                // Validates password must not contain portion userId or Username. If "validatePasswordWithoutId" property is enabled.
                if (isPasswordWithoutId
                        && isIdUserInPassword(newPassword, loginName.replaceAll("\\s+", ""), sessionData.getUser().getName().replaceAll("\\s+", ""))) {
                    return false;
                }

                pst = dbConn.prepareStatement(User.sql_bundle.getString("changePasswordLoginDate"));
                encryptedPassword = encryptedPassword(newPassword, context);
                pst.setString(1, new String(encryptedPassword));
                pst.setString(2, email);
                pst.setBoolean(3, true);
                pst.setString(4, loginName);
                int n = pst.executeUpdate();
                pst.close();
                this.refType = refType;

                int numOfPassword = 0;
                boolean bIsMobileAuthenticationOn = false;
                pst = dbConn.prepareStatement(User.sql_bundle.getString("isMobilePermissionOn"));
                pst.setInt(1, Integer.parseInt(passwordId));
                rs = pst.executeQuery();
                if (rs.next()) {
                    bIsMobileAuthenticationOn = true;
                }

                if (!bIsMobileAuthenticationOn && isPasswordUniqueCheck) {
                    pst = dbConn.prepareStatement(User.sql_bundle.getString("passwordHistory"));
                    pst.setInt(1, Integer.parseInt(passwordId));
                    pst.setString(2, encryptedPassword(this.password, context));
                    pst.executeUpdate();

                    pst = dbConn.prepareStatement(User.sql_bundle.getString("countOfPasswordHistory"));
                    pst.setInt(1, Integer.parseInt(passwordId));
                    rs = pst.executeQuery();
                    if (rs.next()) {
                        numOfPassword = rs.getInt("noOfEntry");
                    }
                    if (numOfPassword > passCheckTimes && passCheckTimes != 0) {
                        pst = dbConn.prepareStatement(User.sql_bundle.getString("deletePasswordHistory"));
                        pst.setInt(1, Integer.parseInt(passwordId));
                        pst.executeUpdate();
                    }
                }

                pst = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
                pst.setInt(1, Integer.parseInt(passwordId));
                int permissionId = 0;
                rs = pst.executeQuery();
                if (rs.next()) {
                    permissionId = rs.getInt("web_permission_type_id");
                }
                rs.close();
                pst.close();
                String levelType = getLevelType();
                if (levelType.equals("ISO") && permissionId == 52) {
                    levelType = "Carrier ISO";
                }

                String entityName = "";
                pst = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
                pst.setDouble(1, Double.parseDouble(refId));
                pst.setDouble(2, Double.parseDouble(refId));
                rs = pst.executeQuery();
                while (rs.next()) {
                    entityName = rs.getString("name");
                }

                String mailHost = DebisysConfigListener.getMailHost(context);
                String mails = "";
                String bodyMessage = "";
                cal = Calendar.getInstance();
                String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
                String subject = Languages.getString("jsp.admin.customers.user_logins.addUser.emailSubjectUpdate", sessionData.getLanguage());
                if (email != null && !email.equals("")) {
                    mails = email;
                    bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageFirstTimeLogin", new Object[]{dateFormat.format(cal.getTime()), loginName, newPassword, entityName, levelType}, sessionData.getLanguage());
                    sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);

                }

                mails = Properties.getPropertieByName(instance, "customerServiceMail");
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageFirstTimeLoginCS", new Object[]{dateFormat.format(cal.getTime()), loginName, entityName, levelType}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
                return (n > 0);

            } catch (Exception ex) {
                User.cat.error("Error during changePassword", ex);
                return false;
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pst, null);
                } catch (Exception e) {
                    User.cat.error("Error during release connection", e);
                }

            }
        }

    }

    private boolean isIdUserInPassword(String stringToEvaluate, String personId, String personName) {
        String partName = "";

        if (stringToEvaluate != null && !stringToEvaluate.isEmpty()
                && stringToEvaluate.length() >= DebisysConstants.CHAR_LENGTH_NOT_ALLOWED) {

            String[] listStringTo = stringToEvaluate.split("");

            for (int i = 0; i <= (listStringTo.length - DebisysConstants.CHAR_LENGTH_NOT_ALLOWED); i++) {
                partName = stringToEvaluate.substring(i, (DebisysConstants.CHAR_LENGTH_NOT_ALLOWED + i));

                if (partName.length() == DebisysConstants.CHAR_LENGTH_NOT_ALLOWED
                        && ((personId != null && !personId.isEmpty() && Pattern.compile(Pattern.quote(partName), Pattern.CASE_INSENSITIVE).matcher(personId).find())
                        || (personName != null && !personName.isEmpty() && Pattern.compile(Pattern.quote(partName), Pattern.CASE_INSENSITIVE).matcher(personName).find()))) {
                    return true;
                }
            }
        }
        return false;
    }

    /* END MiniRelease 3.0 - Added by LF */
    public static void main(String[] args) {
    }

    public String getDatasource() {
        return this.datasource;
    }

    public void setDatasource(String dataSource) {
        this.datasource = dataSource;
    }

    /**
     * Returns the qcommBusiness
     *
     * @return boolean
     */
    public boolean isQcommBusiness() {
        return this.qcommBusiness;
    }

    /**
     * Sets the qcommBusiness value
     *
     * @param qcommbusiness The qcommBusiness to set
     */
    public void setQcommBusiness(boolean qcommbusiness) {
        this.qcommBusiness = qcommbusiness;
    }

    /**
     * Returns the qcommBusinessTransitionDate
     *
     * @return Date
     */
    public Date getQcommBusinessTransitionDate() {
        return this.qcommBusinessTransitionDate;
    }

    /**
     * Sets the qcommBusinessTransitionDate value
     *
     * @param qcommbusinessTransitionDate The qcommBusinessTransitionDate to set
     */
    public void setQcommBusinessTransitionDate(Date qcommbusinessTransitionDate) {
        this.qcommBusinessTransitionDate = qcommbusinessTransitionDate;
    }


    /**
     * @param context
     */
    public static boolean isRepRatePlanforTerminalCreationEnabled(SessionData sessionData, ServletContext context) {
        boolean bResult = false;

        com.debisys.users.User u = new com.debisys.users.User();
        java.util.Iterator<Vector<String>> itPermissions = u.getAssignablePermissions(sessionData, DebisysConstants.ISO).iterator();
        while (itPermissions.hasNext()) {
            Vector<String> vTmp = itPermissions.next();
            if (vTmp.get(0).toString().equals(DebisysConstants.PERM_REPRATEPLANS_TERMINALCREATION)) {
                bResult = true;
                break;
            }
            vTmp = null;
        }
        itPermissions = null;
        u = null;

        return bResult;
    }

    /**
     * @param context
     */
    public static boolean isWebServiceAutherticationTerminalsEnabled(SessionData sessionData, ServletContext context) {
        boolean bResult = false;

        User u = new User();
        java.util.Iterator<Vector<String>> itPermissions = u.getAssignablePermissions(sessionData, DebisysConstants.ISO).iterator();
        while (itPermissions.hasNext()) {
            Vector<String> vTmp = itPermissions.next();
            if (vTmp.get(0).toString().equals(DebisysConstants.PERM_ENABLE_WEBSERVICE_AUTHENTICATION_FOR_TERMINALS)) {
                bResult = true;
                break;
            }
            vTmp = null;
        }
        itPermissions = null;
        u = null;

        return bResult;
    }

    /**
     * @param context
     */
    public static boolean isInvoiceNumberEnabled(SessionData sessionData, ServletContext context) {
        boolean bResult = false;

        com.debisys.users.User u = new com.debisys.users.User();
        java.util.Iterator<Vector<String>> itPermissions = u.getAssignablePermissions(sessionData, DebisysConstants.ISO).iterator();
        while (itPermissions.hasNext()) {
            Vector<String> vTmp = itPermissions.next();
            if (vTmp.get(0).toString().equals(DebisysConstants.INVOICE_NUMBER)) {
                bResult = true;
                break;
            }
            vTmp = null;
        }
        itPermissions = null;
        u = null;

        return bResult;
    }

    public static boolean isExternalRepAllowedEnabled(SessionData sessionData) {
        boolean bResult = false;

        User u = new User();
        java.util.Iterator<Vector<String>> itPermissions = u.getAssignablePermissions(sessionData, DebisysConstants.ISO).iterator();
        while (itPermissions.hasNext()) {
            Vector<String> vTmp = itPermissions.next();
            if (vTmp.get(0).toString().equals(DebisysConstants.PERM_ALLOW_EXTERNAL_REPS)) {
                bResult = true;
                break;
            }
            vTmp = null;
        }
        itPermissions = null;
        u = null;

        return bResult;
    }

    public static boolean isManagePromoDetailsEnabled(SessionData sessionData, ServletContext context) {
        boolean bResult = false;

        com.debisys.users.User u = new com.debisys.users.User();
        Iterator<Vector<String>> itPermissions = u.getAssignablePermissions(sessionData, DebisysConstants.ISO).iterator();
        while (itPermissions.hasNext()) {
            Vector<String> vTmp = itPermissions.next();
            if (vTmp.get(0).toString().equals(DebisysConstants.PERM_MANAGE_PROMO_DETAILS)) {
                bResult = true;
                break;
            }
            vTmp = null;
        }
        itPermissions = null;
        u = null;

        return bResult;
    }

    /**
     * *
     * DBSY-890 SW Sets the carrier user products in session data for easy
     * access when creating reports.
     *
     * @param sessionData
     */
    public void setCarrierUserProducts(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sessdataprods = "";

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }

            Vector<String> vecCarrierUserProducts = new Vector<String>();

            // Get all products assigned to the carrier user
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getCarrierUserProducts"));
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                vecCarrierUserProducts.add(rs.getString("ID"));
            }
            pstmt.close();
            rs.close();

            Enumeration<String> e = vecCarrierUserProducts.elements();

            StringBuffer prods = new StringBuffer("");
            e = vecCarrierUserProducts.elements();
            while (e.hasMoreElements()) {
                prods.append(e.nextElement() + ",");
            }

            // If products exist, remove the last comma
            if (prods.length() > 0) {
                sessdataprods = prods.toString().substring(0, prods.toString().length() - 1);
            }

            // Set a comma separated list of prods in session data
            sessionData.setProperty("carrierUserProds", sessdataprods);
            User.cat.debug("Carrier User Prods: " + sessionData.getPropertyObj("carrierUserProds"));
        } catch (Exception e) {
            User.cat.error("Error during setCarrierUserProducts", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
    }

    /**
     * *
     * DBSY-890 SW
     *
     * @param _passwordID
     * @return
     */
    public Vector<Vector<String>> getProducts() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector<Vector<String>> vecProducts = new Vector<Vector<String>>();
        String strSQL = "";

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new UserException();
            }

            if (this.passwordId != null) {
                strSQL = sql_bundle.getString("getProducts");
            } else {
                strSQL = sql_bundle.getString("getProductsNoPasswordID");
            }

            cat.debug("Product SQL: " + strSQL);
            pstmt = dbConn.prepareStatement(strSQL);

            if (this.passwordId != null) {
                pstmt.setString(1, this.passwordId);
            }

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(StringUtil.toString(rs.getString("ProviderID")));
                vecTemp.add(StringUtil.toString(rs.getString("ProviderName")));
                vecTemp.add(StringUtil.toString(rs.getString("ProductID")));
                vecTemp.add(StringUtil.toString(rs.getString("LongName")));
                vecTemp.add(StringUtil.toString(rs.getString("ShortName")));
                vecTemp.add(StringUtil.toString(rs.getString("Added")));
                vecProducts.add(vecTemp);
            }
            pstmt.close();
            rs.close();
        } catch (Exception e) {
            cat.error("Error during getProducts", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return vecProducts;
    }

    /**
     * *
     * DBSY-890 SW Updates the products for a carrier user
     *
     * @param _products
     */
    public void addCarrierUserProduct(String[] _products, String _passwordID) {
        /**
         * 1) Products we want to save (make sure to allow user to save none) -
         * A 2) Products already allowed by the carrier user (if they exist) - B
         * 3) Delete all products in database & B not in A (all unallowed
         * products) - At the end, A should be the same as B 4) Delete all
         * products in A already in B (Don't want to insert twice. Would break
         * constraint anyway) 5) Insert all products in A (Leftover products in
         * A is what is not yet inserted)
         */
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String saveOldPasswordID = this.passwordId;

        if (_passwordID != null) {
            this.passwordId = _passwordID;
        }

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }

            // 1)
            User.cat.debug("Beginning 1)");
            Vector<String> vecProdsToSave = new Vector<String>();
            Hashtable<String, String> prodsToProviders = new Hashtable<String, String>();

            if (_products != null) {
                for (int i = 0; i < _products.length; i++) {
                    String[] tokens = _products[i].split("_");
                    prodsToProviders.put(tokens[1], tokens[0]);
                    vecProdsToSave.add(tokens[1]);
                    User.cat.debug("Saving prod: " + tokens[1]);
                }
            }

            // 2)
            User.cat.debug("Beginning 2)");
            Vector<String> vecProdsAllowed = new Vector<String>();
            String strSQL1 = sql_bundle.getString("getCarrierUserProducts");

            User.cat.debug("SQL: " + strSQL1);
            pstmt = dbConn.prepareStatement(strSQL1);
            User.cat.debug("Param 1: " + this.passwordId);
            pstmt.setString(1, this.passwordId);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                vecProdsAllowed.add(StringUtil.toString(rs.getString("ID")));
                User.cat.debug("Allowed prod: " + StringUtil.toString(rs.getString("ID")));
            }
            pstmt.close();
            rs.close();

            // 3)
            User.cat.debug("Beginning 3)");
            if (vecProdsAllowed.size() > 0) {
                Iterator<String> it3 = vecProdsAllowed.iterator();
                while (it3.hasNext()) {
                    String prod = "";
                    prod = it3.next();

                    if (!vecProdsToSave.contains(prod)) {
                        // Delete all current carrier user products for that password ID
                        String strSQL2 = "DELETE FROM carrier_user_products WHERE password_id = " + this.passwordId + " AND carrier_user_product_id = " + prod;
                        User.cat.debug("SQL: " + strSQL2);

                        pstmt = dbConn.prepareStatement(strSQL2);
                        pstmt.executeUpdate();
                        User.cat.debug("Removing prod: " + prod);
                    }
                }
                pstmt.close();
            }

            // 4)
            User.cat.debug("Beginning 4)");
            if (vecProdsAllowed.size() > 0) {
                Iterator<String> it4 = vecProdsAllowed.iterator();
                while (it4.hasNext()) {
                    String prod = "";
                    prod = it4.next();

                    if (vecProdsToSave.contains(prod)) {
                        vecProdsToSave.remove(prod);
                        User.cat.debug("Removing prod: " + prod);
                    }
                }
            }
            // 5)
            User.cat.debug("Beginning 5)");
            if (vecProdsToSave.size() > 0) {
                Iterator<String> it5 = vecProdsToSave.iterator();
                while (it5.hasNext()) {
                    String prod = "";
                    prod = it5.next();
                    pstmt = dbConn.prepareStatement(User.sql_bundle.getString("addCarrierUserProduct"));
                    User.cat.debug("SQL: " + strSQL1);
                    pstmt.setDouble(1, Double.parseDouble(this.passwordId));
                    User.cat.debug("Param 1: " + this.passwordId);
                    pstmt.setInt(2, Integer.parseInt(prodsToProviders.get(prod)));
                    User.cat.debug("Param 2: " + prodsToProviders.get(prod));
                    pstmt.setInt(3, Integer.parseInt(prod));
                    User.cat.debug("Param 3: " + prod);
                    pstmt.executeUpdate();
                    pstmt.close();
                    User.cat.debug("Inserting prod: " + prod);
                }
            }
        } catch (Exception e) {
            User.cat.error("Error during addCarrierUserProduct", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }
        this.passwordId = saveOldPasswordID;
    }

    /**
     * *
     * DBSY-890 SW Deletes all products allowed to the specific carrier user
     * before the account itself is deleted from the system.
     *
     * @param sessionData
     */
    public void deleteCarrierUserProducts(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }

            pstmt1 = dbConn.prepareStatement(User.sql_bundle.getString("getUserbyPasswordId"));
            User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("getUserbyPasswordId"));
            pstmt1.setDouble(1, Double.parseDouble(this.passwordId));
            User.cat.debug("Param 1: " + this.passwordId);
            rs = pstmt1.executeQuery();
            //String tmpUsername = "";
            //String tmpPassword = "";

            if (rs.next()) {
                /*tmpUsername = */
                StringUtil.toString(rs.getString("logon_id"));
                /*tmpPassword = */
                StringUtil.toString(rs.getString("logon_password"));

                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("deleteCarrierUserUserProducts"));
                User.cat.debug("DeleteCarrierUserProducts: " + User.sql_bundle.getString("deleteCarrierUserUserProducts"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                User.cat.debug("Param 1: " + this.passwordId);
                pstmt.executeUpdate();
                pstmt.close();
            }
            pstmt1.close();
            rs.close();
        } catch (Exception e) {
            User.cat.error("Error during deleteCarrierUserProducts", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * *
     * DBSY-890 SW Get a logon ID for a password ID
     *
     * @param sessionData
     * @return
     * @throws UserException
     */
    public String getUserLogonID(SessionData sessionData) throws UserException {
        String userLogon = "";
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = User.sql_bundle.getString("getUserLogonID");

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            rs = pstmt.executeQuery();

            if (rs.next()) {
                userLogon = rs.getString("logon_id");
            }
        } catch (Exception e) {
            User.cat.error("Error during getUserLogonID", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return userLogon;
    }

    /**
     * *
     * DBSY-890 SW Get a logon password for a password ID
     *
     * @param sessionData
     * @return
     * @throws UserException
     */
    public String getUserLogonPassword(SessionData sessionData) throws UserException {
        String userLogon = "";
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = User.sql_bundle.getString("getUserLogonPassword");

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            rs = pstmt.executeQuery();

            if (rs.next()) {
                userLogon = rs.getString("logon_password");
            }
        } catch (Exception e) {
            User.cat.error("Error during getUserLogonPassword", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return userLogon;
    }

    public String getUserLogonEmail(SessionData sessionData) throws UserException {
        String userLogon = "";
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = User.sql_bundle.getString("getUserLogonEmail");

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            rs = pstmt.executeQuery();

            if (rs.next()) {
                userLogon = rs.getString("email");
            }
        } catch (Exception e) {
            User.cat.error("Error during getUserLogonEmail", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return userLogon;
    }

    /**
     * @param lStatus the status to set
     */
    public void setStatus(long lStatus) {
        this.status = lStatus;
    }

    /**
     * @return the status
     */
    public long getStatus() {
        return this.status;
    }

    public int checkStatusFromDB() {
        int nStatus = 0;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        if (!this.isIntranetUser()) {
            try {
                dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    cat.error("Can't get database connection");
                    throw new Exception();
                }
                String sqlStr = "SELECT p.Status FROM password AS p WITH (NOLOCK) WHERE p.password_id = ?";
                pstmt = dbConn.prepareStatement(sqlStr); //TODO: sql_bundle.getString("getIsoWebPermission");
                pstmt.setString(1, this.passwordId);
                cat.debug(sqlStr + " /* " + this.passwordId + " */");
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    nStatus = rs.getInt("Status");
                }
            } catch (Exception localException) {
                cat.error("Error during getStatusByRepID", localException);
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    cat.error("Error during release connection", e);
                }
            }
        } else {
            //a intranet user never is disabled
            //for now
            nStatus = 1;
        }
        return nStatus;
    }

    /**
     * @param sStatusLastChange the statusLastChange to set
     */
    public void setStatusLastChange(String sStatusLastChange) {
        this.statusLastChange = sStatusLastChange;
    }

    /**
     * @return the statusLastChange
     */
    public String getStatusLastChange() {
        return this.statusLastChange;
    }

    public void setIntranetUser(boolean intranetUserLogin) {
        this.intranetUser = intranetUserLogin;
    }

    public boolean isIntranetUser() {
        return this.intranetUser;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    /**
     * @param nIntranetUserId the intranetUserId to set
     */
    public void setIntranetUserId(int nIntranetUserId) {
        this.intranetUserId = nIntranetUserId;
    }

    /**
     * @return the intranetUserId
     */
    public int getIntranetUserId() {
        return this.intranetUserId;
    }

    /*
	public void setIntranetUserLogin(String intranetUserlogin) {
		this.intranetUserLogin = intranetUserlogin;
	}

	public String getIntranetUserLogin() {
		return intranetUserLogin;
	}
     */
    public boolean hasIntranetUserPermission(int nIntranetUserId, int groupId, String permissionName) throws UserException {
        Connection dbConn = null;
        boolean bResult = false;
        CallableStatement cst = null;
        String strSQL = "";

        try {
            strSQL = User.sql_bundle.getString("getIntranetUserPermission");// EXEC sp_acl_get_user_permission ?, ?, ?, ?
            dbConn = TorqueHelper.getConnection(User.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new ReportException();
            }

            User.cat.debug(strSQL + " -- /*" + nIntranetUserId + ", " + groupId + ", " + permissionName + ", 0*/");
            cst = dbConn.prepareCall(strSQL);
            cst.setInt(1, nIntranetUserId);
            cst.setInt(2, groupId);
            cst.setString(3, permissionName);
            cst.registerOutParameter(4, Types.INTEGER);
            cst.setInt(4, 0);
            cst.execute();
            bResult = cst.getInt(4) != 0;
        } catch (Exception e) {
            User.cat.error("Error during hasIntranetUserPermission", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, cst, null);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }

        return bResult;
    } // End of function hasIntranetUserPermission

    /**
     * @return the isoId
     */
    public String getIsoId() {
        return this.isoId;
    }

    @Override
    public User clone() {
        User u = new User();
        u.accessLevel = this.accessLevel;
        u.companyName = this.companyName;
        u.contactName = this.contactName;
        u.datasource = this.datasource;
        u.distChainType = this.distChainType;
        u.intranetUser = this.intranetUser;
        u.intranetUserId = this.intranetUserId;
        u.isoId = this.isoId;
        u.name = this.name;
        u.password = this.password;
        u.passwordId = this.passwordId;
        u.permissionTypeId = this.permissionTypeId;
        u.qcommBusiness = this.qcommBusiness;
        u.qcommBusinessTransitionDate = this.qcommBusinessTransitionDate;
        u.refId = this.refId;
        u.refType = this.refType;
        u.status = this.status;
        u.statusLastChange = this.statusLastChange;
        u.username = this.username;
        return u;
    }

    public boolean isFirstTimeLogin(ServletContext context, SessionData sessionData) throws UserException {
        boolean isFirstTimeLogin = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkFirstTimeLogin"));
            pstmt.setString(1, this.getUsername());
            PasswordPojo passwordPojo = checkPasswordEncryption();
            if (passwordPojo.getEncrypted()) {
                //ServletContext servletContext = sessionData.
                encryptedPassword = encryptedPassword(this.password, context);
                pstmt.setString(2, encryptedPassword);
            } else {
                pstmt.setString(2, this.password);
            }

            rs = pstmt.executeQuery();
            User.cat.debug(pstmt.toString());
            if (rs.next()) {
                this.lastSuccessfulLogin = rs.getString("lastSuccessfulLogin");
                this.isEncrypted = rs.getBoolean("isEncrypted");

            }
            if (this.lastSuccessfulLogin == null || this.lastSuccessfulLogin.equals(null) || !this.isEncrypted) {
                isFirstTimeLogin = true;
            }
        } catch (Exception e) {
            User.cat.error("Error during isFirstTimeLogin", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return isFirstTimeLogin;
    }

    public String getEmailConfigured(String username) throws UserException {
        String email = "";
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkEmail"));
            pstmt.setString(1, username);
            rs = pstmt.executeQuery();
            User.cat.debug(pstmt.toString());
            if (rs.next()) {
                email = rs.getString("email");
            }

        } catch (Exception e) {
            User.cat.error("Error during getEmailConfigured", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return email;

    }

    public Vector getUserDetails(String username) throws UserException {
        String email = "";
        String refType = "";
        String refId = "";
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        Vector results = null;
        ResultSet rs = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getUserDetails"));
            pstmt.setString(1, username);
            rs = pstmt.executeQuery();
            User.cat.debug(pstmt.toString());
            if (rs.next()) {
                results = new Vector();
                results.add(rs.getString("email"));
                results.add(rs.getString("ref_id"));
                results.add(rs.getString("ref_type"));
                results.add(rs.getString("password_id"));
            }

        } catch (Exception e) {
            User.cat.error("Error during getEmailConfigured", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return results;
    }

    public String encryptedPassword(String originalPassword, ServletContext context) throws NoSuchAlgorithmException, UnsupportedEncodingException {
//        MessageDigest digest = MessageDigest.getInstance("SHA-1");
//        digest.update(originalPassword.getBytes("UTF-16"));
//        byte rawByte[] = digest.digest();
//        encryptedPassword = org.apache.commons.codec.binary.Base64.encodeBase64String(rawByte);
        //encryptedPassword = (new Base64()).encode(rawByte).toString();
        
        String seeder = (String) context.getAttribute("secretSeed");
        SecurityCipherService cipherService = SecurityCipherService.getInstance(seeder);
        encryptedPassword = cipherService.encrypt(originalPassword);
        return encryptedPassword;
    }

    static public boolean compareSecurityAnswer(String username, int sqID, String answer) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean ok = false;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new UserException();
            }
            pstmt = dbConn.prepareStatement(sql_bundle.getString("getUserAnswerBySqLogonID"));
            pstmt.setString(1, username);
            pstmt.setInt(2, sqID);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                String userAnswer = rs.getString("answer");
                ok = userAnswer.equals(answer);
            }
        } catch (Exception e) {
            cat.error("Error: compareSecurityAnswer", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return ok;
    }

    public boolean forceLoginAndAskToResetPass(SessionData sessionData) throws UserException {
        //lastSuccessfulLogin==null and IsEncrypted=fasle
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean loggedIn = false;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // mark password to reset
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("markPassToReset"));
            pstmt.setString(1, this.getUsername());
            pstmt.execute();

            // load user
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getForcedLogin"));
            pstmt.setString(1, this.getUsername());
            rs = pstmt.executeQuery();
            User.cat.debug(pstmt.toString());
            if (rs.next()) {
                this.status = rs.getLong("Status");
                this.statusLastChange = rs.getString("StatusLastChange");
                if (this.status == 0) {
                    this.addFieldError("login", Languages.getString("com.debisys.users.error_user_disabled", sessionData.getLanguage()));
                    System.out.println("The user is disabled");
                    loggedIn = false;
                } else {
                    this.passwordId = rs.getString("password_id");
                    this.refId = rs.getString("ref_id");
                    this.refType = rs.getString("ref_type");
                    this.qcommBusiness = rs.getBoolean("qcomm_check");
                    this.qcommBusinessTransitionDate = rs.getTimestamp("qcomm_date");
                    this.datasource = Torque.getDefaultDB();
                    if (rs.getString("name") != null) {
                        this.name = rs.getString("name");
                    } else {
                        this.name = "";
                    }

                    System.out.println("User logged in, is qcomm business ? " + this.qcommBusiness + "=" + this.qcommBusinessTransitionDate);
                    this.setAccessLevel();
                    loggedIn = true;
                }
            } else {
                this.addFieldError("login", Languages.getString("com.debisys.users.error_login", sessionData.getLanguage()));
                loggedIn = false;
            }
        } catch (Exception e) {
            User.cat.error("Error during validateLogin", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return loggedIn;
    }

    public void disable() throws UserException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // mark password to reset
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("disableUser"));
            pstmt.setString(1, this.getUsername());
            pstmt.execute();
            User.cat.error("disabled user " + this.getUsername());
        } catch (Exception e) {
            User.cat.error("Error during disable", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
    }

    public static int checkUserStatus(String username) throws UserException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int status = 0;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // mark password to reset
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkUserStatus"));
            pstmt.setString(1, username);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                status = rs.getInt("Status");
            }
        } catch (Exception e) {
            User.cat.error("Error during checkUserStatus", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return status;
    }

    public static boolean hasAnswersConfigured(String username) throws UserException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int count = 0;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // mark password to reset
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("hasAnswersConfigured"));
            pstmt.setString(1, username);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                count = rs.getInt("count");
            }
        } catch (Exception e) {
            User.cat.error("Error during hasAnswersConfigured", e);
            throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return (count > 0);
    }

    public Hashtable<String, String> getAnswers() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Hashtable<String, String> answers = new Hashtable<String, String>();
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            // mark password to reset
            String query = User.sql_bundle.getString("getAnswers");
            pstmt = dbConn.prepareStatement(query);
            pstmt.setString(1, this.passwordId);
            rs = pstmt.executeQuery();
            User.cat.debug(query + " " + this.passwordId);
            while (rs.next()) {
                answers.put(rs.getString("question_id"), rs.getString("answer"));
            }
        } catch (Exception e) {
            User.cat.error("Error during getAnswers", e);
            //throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return answers;
    }

    public boolean saveAnswers(Vector<String> answers) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean ok = false;
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            String query = User.sql_bundle.getString("deleteAnswers");
            pstmt = dbConn.prepareStatement(query);
            pstmt.setString(1, this.passwordId);
            pstmt.execute();
            User.cat.debug(query + " " + passwordId);
            pstmt.close();
            query = User.sql_bundle.getString("saveAnswers");
            pstmt = dbConn.prepareStatement(query);
            int questionId = 0;
            for (String answer : answers) {
                pstmt.setString(1, this.passwordId);
                pstmt.setInt(2, questionId);
                pstmt.setString(3, answer);
                pstmt.execute();
                pstmt.clearParameters();
                User.cat.debug(query + " " + passwordId + " " + questionId + " " + answer);
                questionId++;
            }
            ok = true;
        } catch (Exception e) {
            User.cat.error("Error during getAnswers", e);
            //throw new UserException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return ok;
    }

    public void updateEmailPassword(SessionData sessionData, ServletContext context) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer changes = new StringBuffer();
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new Exception();
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getUserbyPasswordId"));
            User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("getUserbyPasswordId"));
            pstmt.setDouble(1, Double.parseDouble(this.passwordId));
            User.cat.debug("Param 1: " + this.passwordId);
            rs = pstmt.executeQuery();
            String tmpUsername = "";
            String tmpPassword = "";
            String tmpEmail = "";
            String log = "";
            if (rs.next()) {
                tmpUsername = StringUtil.toString(rs.getString("logon_id"));
                tmpPassword = StringUtil.toString(rs.getString("logon_password"));
                tmpEmail = StringUtil.toString(rs.getString("email"));
                if (!tmpEmail.equalsIgnoreCase(this.email)) {
                    if (!log.equals("")) {
                        log = log + "and ";
                    }
                    log = log + "email from " + tmpEmail + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + this.email;
                    changes.append(Languages.getString("jsp.admin.customers.user_logins.email", sessionData.getLanguage()) + "<br><br>\n");
                }
                if (!tmpPassword.equalsIgnoreCase(this.password)) {
                    if (!log.equals("")) {
                        log = log + "and ";
                    }
                    log = log + "password changed";
                }
            }
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("updatePasswordEmail"));
            User.cat.debug("GetUserByPasswordId: " + User.sql_bundle.getString("updatePasswordEmail"));
            encryptedPassword = encryptedPassword(this.password, context);
            pstmt.setString(1, encryptedPassword);
            pstmt.setBoolean(2, true);
            User.cat.debug("Param 2: " + true);
            pstmt.setString(3, this.email);
            pstmt.setDouble(4, Double.parseDouble(this.passwordId));
            User.cat.debug("Param 4: " + this.passwordId);
            pstmt.setDouble(5, Double.parseDouble(this.refId));
            User.cat.debug("Param 5 " + this.refId);
            pstmt.setInt(6, Integer.parseInt(this.refType));
            User.cat.debug("Param 6: " + this.refType);
            pstmt.executeUpdate();
            pstmt.close();
            Log.write(sessionData, "Updated password for web login " + "Username:" + tmpUsername, DebisysConstants.LOGTYPE_LOGIN_LOGOUT,
                    this.refId, this.refType);
            cal = Calendar.getInstance();
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("checkCarrierPermission"));
            pstmt.setInt(1, Integer.parseInt(this.passwordId));
            int permissionId = 0;
            rs = pstmt.executeQuery();
            if (rs.next()) {
                permissionId = rs.getInt("web_permission_type_id");
            }
            rs.close();
            pstmt.close();
            String levelType = getLevelType();
            if (levelType.equals("ISO") && permissionId == 52) {
                levelType = "Carrier ISO";
            }
            String entityName = "";
            pstmt = dbConn.prepareStatement(User.sql_bundle.getString("getEntityName"));
            pstmt.setDouble(1, Double.parseDouble(this.refId));
            pstmt.setDouble(2, Double.parseDouble(this.refId));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                entityName = rs.getString("name");
            }

            String mailHost = DebisysConfigListener.getMailHost(context);
            String instance = DebisysConfigListener.getInstance(context);
            String mails = "";
            String bodyMessage = "";
            String from = Languages.getString("jsp.admin.customers.user_logins.addUser.emailFrom", sessionData.getLanguage());
            String subject = Languages.getString("jsp.admin.customers.user_logins.addUser.emailSubjectUpdate", sessionData.getLanguage());
            if (this.email != null && !this.email.equals("")) {
                mails = this.email;
                bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangeEmailPassword", new Object[]{dateFormat.format(cal.getTime()), tmpUsername, this.password, entityName, levelType}, sessionData.getLanguage());
                sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
            }
            mails = Properties.getPropertieByName(instance, "customerServiceMail");
            bodyMessage = Languages.getString("jsp.admin.customers.user_logins.addUser.emailBodyMessageChangeEmailPasswordCS", new Object[]{dateFormat.format(cal.getTime()), tmpUsername, entityName, levelType}, sessionData.getLanguage());
            sendMail(subject, mailHost, mails, from, bodyMessage.toString(), instance);
        } catch (Exception e) {
            User.cat.error("Error during UpdatePassword", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * @param languageCode the languageCode to set
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * @return the languageCode
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * @return the IdNew
     */
    public String getIdNew() {
        return IdNew;
    }

    /**
     * @param IdNew the IdNew to set
     */
    public void setIdNew(String IdNew) {
        this.IdNew = IdNew;
    }

    public boolean hasPasswordExpired(HttpServletRequest request, SessionData sessionData) throws UserException {
        String instance = DebisysConfigListener.getInstance(request.getSession().getServletContext());
        String enablePasswordExpiration = Properties.getPropertyValue(instance, "global", "EnablePasswordExpiryNotification");
        boolean isEnablePasswordExpiration = (enablePasswordExpiration != null && !enablePasswordExpiration.isEmpty() && enablePasswordExpiration.equalsIgnoreCase("true")) ? true : false;
        int passwordExpiryDays = 60;
        try {
            passwordExpiryDays = Integer.parseInt(Properties.getPropertyValue(instance, "global", "login.passwordExpiryDays"));
        } catch (Exception ex) {
        }

        boolean hasPasswordExpired = false;
        if (isEnablePasswordExpiration) {
            Connection dbConn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try {
                dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    User.cat.error("Can't get database connection");
                    throw new UserException();
                }
//                        pstmt=dbConn.prepareStatement(User.sql_bundle.getString("isMobilePermissionOn"));
//                        pstmt.setString(1,this.getUsername());
//                        rs = pstmt.executeQuery();
//			User.cat.debug(pstmt.toString());
                // if(rs.next())
                boolean bIsMobileAuthenticationOn = false;
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("isMobilePermissionOn"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    bIsMobileAuthenticationOn = true;
                }

                if (bIsMobileAuthenticationOn) {
                    return hasPasswordExpired;
                } else {
                    pstmt = dbConn.prepareStatement(User.sql_bundle.getString("hasPasswordExpired"));
                    pstmt.setString(1, this.getUsername());
                    PasswordPojo passwordPojo = checkPasswordEncryption();
                    if (passwordPojo.getEncrypted()) {
                        ServletContext context = request.getSession().getServletContext();
                        encryptedPassword = encryptedPassword(this.password, context);
                        pstmt.setString(2, encryptedPassword);
                    } else {
                        pstmt.setString(2, this.password);
                    }

                    rs = pstmt.executeQuery();
                    User.cat.debug(pstmt.toString());
                    if (rs.next()) {
                        this.lastPasswordChange = rs.getString("lastPasswordChange");
                    }

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date passwordDateChange = new Date();
                    try {
                        passwordDateChange = simpleDateFormat.parse(lastPasswordChange);
                        User.cat.debug(passwordDateChange);
                    } catch (Exception ex) {
                        System.out.println("Exception " + ex);
                    }
                    java.util.Calendar currentDay = Calendar.getInstance();
                    long difference = currentDay.getTimeInMillis() - passwordDateChange.getTime();
                    System.out.println(difference);
                    long diffDays = difference / (24 * 60 * 60 * 1000);
                    System.out.println(diffDays);
                    if (diffDays >= passwordExpiryDays) {
                        hasPasswordExpired = true;
                    }
                    //request.getSession().setAttribute("expiryDiffDays", diffDays);
                }

            } catch (Exception e) {
                User.cat.error("Error during isFirstTimeLogin", e);
                throw new UserException();
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    User.cat.error("Error during closeConnection", e);
                }
            }

        }

        return hasPasswordExpired;
    }

    public boolean checkPassword(SessionData sessionData, String passwordId, Integer passwordReuseTimes, ServletContext context) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean passwordExists = false;                        
        try {
            dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                User.cat.error("Can't get database connection");
                throw new UserException();
            }
            String sql = User.sql_bundle.getString("checkPassword");
            sql = sql.replaceAll("number", String.valueOf(passwordReuseTimes));
            pstmt = dbConn.prepareStatement(sql);
            // pstmt.setInt(1, passwordReuseTimes);
            pstmt.setInt(1, Integer.valueOf(passwordId));
            pstmt.setString(2, encryptedPassword(password, context));

            rs = pstmt.executeQuery();
            User.cat.info(sql);
            while (rs.next()) {
                passwordExists = true;
            }
        } catch (Exception e) {
            User.cat.error("Error during checkPassword", e);
            User.cat.error(e.getCause());
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                User.cat.error("Error during closeConnection", e);
            }
        }
        return passwordExists;
    }

    public int expiryDaysLeft(HttpServletRequest request, SessionData sessionData) throws UserException {
        int daysLeft = 0;
        String instance = DebisysConfigListener.getInstance(request.getSession().getServletContext());
        String enablePasswordExpiration = Properties.getPropertyValue(instance, "global", "EnablePasswordExpiryNotification");
        boolean isEnablePasswordExpiration = (enablePasswordExpiration != null && !enablePasswordExpiration.isEmpty() && enablePasswordExpiration.equalsIgnoreCase("true")) ? true : false;
        int passwordExpiryDays = 60;
        try {
            passwordExpiryDays = Integer.parseInt(Properties.getPropertyValue(instance, "global", "login.passwordExpiryDays"));
        } catch (Exception ex) {
        }

        if (isEnablePasswordExpiration) {
            Connection dbConn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try {
                dbConn = Torque.getConnection(User.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    User.cat.error("Can't get database connection");
                    throw new UserException();
                }
                boolean bIsMobileAuthenticationOn = false;
                pstmt = dbConn.prepareStatement(User.sql_bundle.getString("isMobilePermissionOn"));
                pstmt.setInt(1, Integer.parseInt(this.passwordId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    bIsMobileAuthenticationOn = true;
                }
                if (bIsMobileAuthenticationOn) {
                    return 0;
                } else {
                    pstmt = dbConn.prepareStatement(User.sql_bundle.getString("hasPasswordExpired"));
                    pstmt.setString(1, this.getUsername());
                    PasswordPojo passwordPojo = checkPasswordEncryption();
                    if (passwordPojo.getEncrypted()) {
                        ServletContext context = request.getSession().getServletContext();
                        encryptedPassword = encryptedPassword(this.password, context);
                        pstmt.setString(2, encryptedPassword);
                    } else {
                        pstmt.setString(2, this.password);
                    }

                    rs = pstmt.executeQuery();
                    User.cat.debug(pstmt.toString());
                    if (rs.next()) {
                        this.lastPasswordChange = rs.getString("lastPasswordChange");
                    }

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date passwordDateChange = new Date();
                    try {
                        passwordDateChange = simpleDateFormat.parse(lastPasswordChange);
                        User.cat.debug(passwordDateChange);
                    } catch (Exception ex) {
                        System.out.println("Exception " + ex);
                    }
                    java.util.Calendar currentDay = Calendar.getInstance();
                    long difference = currentDay.getTimeInMillis() - passwordDateChange.getTime();
                    User.cat.info(difference);
                    int diffDays = (int) difference / (24 * 60 * 60 * 1000);
                    daysLeft = passwordExpiryDays - diffDays;
                    User.cat.info(daysLeft);

                    request.getSession().setAttribute("expiryDiffDays", daysLeft);
                }

            } catch (Exception e) {
                User.cat.error("Error during isFirstTimeLogin", e);
                throw new UserException();
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    User.cat.error("Error during closeConnection", e);
                }
            }

        }

        return daysLeft;
    }

}
