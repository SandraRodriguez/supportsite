package com.debisys.news;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.BannerException;
import com.debisys.exceptions.NewsException;
import com.debisys.exceptions.RatePlanException;
import com.debisys.util.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

@SuppressWarnings("deprecation")
public class News implements HttpSessionBindingListener {
	static Category cat = Category.getInstance(News.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.news.sql");

	private static final String DATE_TIME_FORMAT = "MM/dd/yyyy HH:mm:ss";

	private long newsId;

	private String title;

	private String body;

	private String[] repIds;

	private Date enterDate;

	private Date endDate;

	/**
	 * @param sessionData
	 * @return Vector list of banners allowed in images table
	 * @throws BrandingException
	 */
	public Vector<Vector<String>> getNews() throws NewsException {
		Connection dbConn = null;
		Vector<Vector<String>> vecBanners = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new NewsException();
			}
			String strSQL = sql_bundle.getString("getNews");
			pstmt = dbConn.prepareStatement(strSQL);
			cat.debug(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("Id")));
				setEnterDate(rs.getTimestamp("EnterDate"));
				vecTemp.add(getEnterDate() == null ? "" : getEnterDate());
				setEndDate(rs.getTimestamp("EndDate"));
				vecTemp.add(getEndDate() == null ? "" : getEndDate());
				vecTemp.add(StringUtil.toString(rs.getString("Title")));
				vecTemp.add(StringUtil.toString(rs.getString("Body")));
				vecBanners.add(vecTemp);
			}
		} catch (Exception e) {
			cat.error("Error during getAllISOs", e);
			throw new NewsException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecBanners;
	}

	/**
	 * @return Vector list of all Isos with a flag turned on for the assigned
	 *         Isos to the given branding
	 * @throws BrandingException
	 */
	public Vector<Vector<String>> getAllISOs() throws NewsException {
		Connection dbConn = null;
		Vector<Vector<String>> vecBrandings = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("getAllISOsforNews");
			pstmt = dbConn.prepareStatement(strSQL);
			cat.debug(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecBrandings.add(vecTemp);
			}
		} catch (Exception e) {
			cat.error("Error during getAllISOs", e);
			throw new NewsException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecBrandings;
	}

	public void valueBound(HttpSessionBindingEvent arg0) {

	}

	public void valueUnbound(HttpSessionBindingEvent arg0) {

	}

	/**
	 * @param newsId
	 *            the newsId to set
	 */
	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}

	/**
	 * @return the newsId
	 */
	public long getNewsId() {
		return newsId;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param body
	 *            the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	private void setEndDate(Date date) {
		this.endDate = date;
	}

	/**
	 * @param enterDate
	 *            the enterDate to set
	 */
	public void setEnterDate(Date date) {
		this.enterDate = date;
	}

	/**
	 * @param enterDate
	 *            the enterDate to set
	 */
	public void setEnterDate(String venterDate) {
		enterDate = null;
		if (venterDate != null && !venterDate.equals("")) {
			try {
				this.enterDate = new SimpleDateFormat(DATE_TIME_FORMAT).parse(venterDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the enterDate
	 */
	public String getEnterDate() {
		if (enterDate == null) {
			return null;
		}
		return new SimpleDateFormat(DATE_TIME_FORMAT).format(enterDate);
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(String vendDate) {
		endDate = null;
		if (vendDate != null && !vendDate.equals("")) {
			try {
				this.endDate = new SimpleDateFormat(DATE_TIME_FORMAT).parse(vendDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		if (endDate == null) {
			return null;
		}
		return new SimpleDateFormat(DATE_TIME_FORMAT).format(endDate);
	}

	public boolean save() throws NewsException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean ok = false;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new NewsException();
			}
			String strSQL = sql_bundle.getString(newsId > 0 ? "updateNews" : "createNews");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, title);
			pstmt.setString(2, body);
			pstmt.setString(3, getEnterDate());
			pstmt.setString(4, getEndDate());
			if (newsId > 0) {
				pstmt.setLong(5, newsId);
				pstmt.execute();
				ok = true;
				cat.debug("Updating NewsId= " + newsId);
			} else {
				rs = pstmt.executeQuery();
				if (rs.next()) {
					newsId = rs.getLong("Id");
					ok = true;
				}
				cat.debug("Creating NewsId= " + newsId);
			}
		} catch (Exception e) {
			cat.error("Error during News.save", e);
			throw new NewsException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		if (ok) {
			this.saveAssignedReps();
		}
		return ok;
	}

	public boolean load() throws NewsException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean loaded = false;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("getNewsById");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, newsId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				setTitle(rs.getString("Title"));
				setBody(rs.getString("Body"));
				setEnterDate(rs.getTimestamp("EnterDate"));
				setEndDate(rs.getTimestamp("EndDate"));
				Vector<String> o = getAssignedReps();
				repIds = new String[o.size()];
				o.copyInto(repIds);
				loaded = true;
			}
			cat.debug("Getting news values: news=" + newsId);
		} catch (Exception e) {
			cat.error("Error during News.load", e);
			throw new NewsException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return loaded;
	}

	private void saveAssignedReps() {
		unassignAllReps();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("assignNewsToRep");
			for (String repId : repIds) {
				long rId = Long.parseLong(repId);
				if (rId > 0) {
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.setLong(1, newsId);
					pstmt.setLong(2, rId);
					pstmt.execute();
					cat.debug("Assigning rep " + repId + " to news " + newsId);
				}
			}
		} catch (Exception e) {
			cat.error("Error during News.assignRepId", e);
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
	}

	private void unassignAllReps() {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("unassignAllReps");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, newsId);
			pstmt.execute();
			cat.debug("Removing reps for news=" + newsId);
		} catch (Exception e) {
			cat.error("Error during News.unassignAllReps", e);
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
	}

	private Vector<String> getAssignedReps() throws BannerException {
		Connection dbConn = null;
		Vector<String> reps = new Vector<String>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("getAssignedReps");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, newsId);
			cat.debug(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				reps.add(StringUtil.toString(rs.getString("RepId")));
			}
			cat.debug("Getting all assgined isos for news =" + newsId);
		} catch (Exception e) {
			cat.error("Error during News.getAssignedIsos", e);
			throw new BannerException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return reps;
	}

	/**
	 * @param repIds
	 *            the repIds to set
	 */
	public void setRepIds(String[] repIds) {
		this.repIds = repIds;
	}

	/**
	 * @return the repIds
	 */
	public String[] getRepIds() {
		return repIds;
	}
}
