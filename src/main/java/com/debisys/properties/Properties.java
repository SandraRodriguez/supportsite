package com.debisys.properties;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

@SuppressWarnings("deprecation")
public class Properties {

    private static final Logger log = Logger.getLogger(Properties.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.properties.proper");
    
    public Properties() {
    	super();
    }

    /**
     * @param instance
     * @param propertyName
     * @return
     */
    public static String getPropertieByName(String instance, String propertyName) {
		String strProperty = "";
	
		Connection dbConn = null;
		PreparedStatement pstmtLogRateChanges = null;
		try {
		    dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    pstmtLogRateChanges = dbConn.prepareStatement(sql_bundle.getString("getpropertybyname"));
		    pstmtLogRateChanges.setString(1, instance);
		    pstmtLogRateChanges.setString(2, propertyName);
		    ResultSet rs = pstmtLogRateChanges.executeQuery();
		    if (rs.next()) {
		    	strProperty = rs.getString(1);
		    } else {
		    	log.error("Error during getPropertyValue :no property set up for: " + propertyName);
		    }
		    rs.close();
		    pstmtLogRateChanges.close();
		} catch (Exception localException) {
		    // If no log record could be saved, the failure should not affect
		    // the operation
		    log.error("Error during getPropertyValue call : ", localException);
		} finally {
		    try {
				if (pstmtLogRateChanges != null)
				    pstmtLogRateChanges.close();
				Torque.closeConnection(dbConn);
		    } catch (Exception e) {
		    	log.error("Error during release connection", e);
		    }
		}
	
		return strProperty;
    }

    /**
     * Retrieves a DB-stored property given all parameters
     * 
     * @param instance
     *                deployment instance grouping many projects
     * @param module
     *                specific module within an application where this property
     *                is scoped, or global by default
     * @param propertyName
     *                the property name
     * @return the value of the property or an empty string if it's not found
     */
    public static String getPropertyValue(String instance, String module, String propertyName) {
		if (module == null || module.equals("")) {
		    module = "global";
		    log.debug("[getPropertyValue] defaulting module to \'global\'");
		}	
		String strProperty = "";
		log.debug("[getPropertyValue] instance=" + instance + ", module=" + module + ", propertyName=" + propertyName);
		Connection dbConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
		    dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		    pst = dbConn.prepareStatement(sql_bundle.getString("getPropertyValueAllParams"));
		    pst.setString(1, instance);
		    pst.setString(2, module);
		    pst.setString(3, propertyName);
		    rs = pst.executeQuery();
		    if (rs.next()) {
		    	strProperty = rs.getString(1);
		    } else {
		    	log.error("Error during getPropertyValue: no property set up for: " + propertyName + ". " + "Retuning an empty string..");
		    }
		} catch (Exception e) {
		    // If no log record could be saved, the failure should not affect the operation
		    log.error("Error during getPropertyValue call: ", e);
		} finally {
		    try {
				if (rs != null) {
				    rs.close();
				}
				if (pst != null) {
				    pst.close();
				}
				Torque.closeConnection(dbConn);
		    } catch (Exception e) {
		    	log.error("Error during release connection", e);
		    }
		}

		return strProperty;
    }
}
