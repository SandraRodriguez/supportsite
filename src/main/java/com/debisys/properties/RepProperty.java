package com.debisys.properties;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.emida.utils.dbUtils.TorqueHelper;

public class RepProperty {
	static Logger log = Logger.getLogger(Properties.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.properties.proper");

	public static final String CHURN_REPORTING_OPTIONS_WEEKLY = "churn.reporting.options.weekly";
	public static final String CHURN_REPORTING_OPTIONS_WEEKLY_THRESHOLD = "churn.reporting.options.weekly.threshold";
	public static final String CHURN_REPORTING_OPTIONS_WEEKLY_SENT_TO = "churn.reporting.options.weekly.sentto";
	public static final String CHURN_REPORTING_OPTIONS_MONTHLY = "churn.reporting.options.monthly";
	public static final String CHURN_REPORTING_OPTIONS_MONTHLY_THRESHOLD = "churn.reporting.options.monthly.threshold";
	public static final String CHURN_REPORTING_OPTIONS_MONTHLY_SENT_TO = "churn.reporting.options.monthly.sentto";
	public static final String CHURN_REPORTING_OPTIONS_DISABLED_MERCHANTS = "churn.reporting.options.disabledmerchants";

	public static void saveProperty(long repId, String propName, String propValue) throws Exception {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				log.error("Can't get database connection");
				throw new Exception();
			}
			String strSQL = "EXEC sp_saveRepProperty ?, ?, ?";
			pstmt = dbConn.prepareStatement(strSQL);
			log.debug("Saving repId = "+repId+", " + propName + " = " + propValue);
			pstmt.setLong(1, repId);
			pstmt.setString(2, propName);
			pstmt.setString(3, propValue);
			pstmt.execute();
			pstmt.clearParameters();
		} catch (Exception e) {
			log.error("Error during RepProperty.saveProperty", e);
			throw new Exception();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			} catch (Exception e) {
				log.error("Error during closeConnection", e);
			}
		}
	}

	public static String getProperty(long repId, String propName, String defaultValue) throws Exception {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String propValue = defaultValue;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				log.error("Can't get database connection");
				throw new Exception();
			}
			String strSQL = "SELECT propertyValue FROM RepProperties WHERE repId = ? AND propertyName = ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, repId);
			pstmt.setString(2, propName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				propValue = rs.getString(1);
				if(propValue == null){
					propValue = defaultValue;
				}
			}
			log.debug("Getting rep property (repId = "+repId+", " + propName + " = " + propValue + ")");
		} catch (Exception e) {
			log.error("Error during RepProperty.getProperty", e);
			throw new Exception();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			} catch (Exception e) {
				log.error("Error during closeConnection", e);
			}
		}
		return propValue;
	}

	public static void saveOptionsForChurnReport(long repId, boolean weekly, double weeklyThreshold, String weeklySendTo, boolean monthly, double monthlyThreshold, String monthlySendTo, boolean disabledMerchs) throws Exception {
		saveProperty(repId, CHURN_REPORTING_OPTIONS_WEEKLY, weekly ? "enabled" : "disabled");
		saveProperty(repId, CHURN_REPORTING_OPTIONS_WEEKLY_THRESHOLD, String.valueOf(weeklyThreshold));
		saveProperty(repId, CHURN_REPORTING_OPTIONS_WEEKLY_SENT_TO, weeklySendTo);
		saveProperty(repId, CHURN_REPORTING_OPTIONS_MONTHLY, monthly ? "enabled" : "disabled");
		saveProperty(repId, CHURN_REPORTING_OPTIONS_MONTHLY_THRESHOLD, String.valueOf(monthlyThreshold));
		saveProperty(repId, CHURN_REPORTING_OPTIONS_MONTHLY_SENT_TO, monthlySendTo);
		saveProperty(repId, CHURN_REPORTING_OPTIONS_DISABLED_MERCHANTS, disabledMerchs ? "enabled" : "disabled");
	}
}
