package com.debisys.web.servlet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.Captcha;

import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.properties.Properties;
import com.debisys.users.DBUser;
import com.debisys.users.IntranetUser;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.crypto.TRProxyFactory;
import org.apache.log4j.Logger;

/**
 * @author nmartinez
 */
public class SecurityLogin extends HttpServlet {
        
        private static Logger logger = Logger.getLogger(SecurityLogin.class);
        
	/**
	 * 
	 */
	private static final long serialVersionUID = -7788106812679197256L;

	/**
	 * Constructor of the object.
	 */
	public SecurityLogin() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	@Override
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException 
	{			
		String drep = request.getParameter("drep"); // permission for document repository		
		String userId = request.getParameter("userId");                
		String remoteAddress = request.getRemoteAddr();	
                logger.info("request comes from =["+remoteAddress+"]");
		String userSessionIntranet = TRProxyFactory.dataProtection().r0(userId);		
		IntranetUser intranetUser = IntranetUser.validateInfoByUserIdIntranet(Integer.valueOf(userSessionIntranet),remoteAddress);		
		if (intranetUser.getCodeError().intValue() != 0)
		{
                    response.sendRedirect(request.getContextPath()+"/message.jsp?message="+intranetUser.getCodeError());
		}		
		else
		{                    
                    User user = new User();                    
                    String prefix_user_intranet = (String)request.getSession().getServletContext().getAttribute("prefix_user_intranet");
                    user.setUsername(prefix_user_intranet+intranetUser.getUserName());
                    user.setIntranetUser(true);
                    user.setIntranetUserId(Integer.parseInt(userSessionIntranet));                    
                    user.setIdNew(intranetUser.getIdNew());
                    logger.info("intranetUser Id unique :["+user.getIdNew()+"]");
                    
                    if ( user.getUsername() != null || user.getUsername().length() > 0)
		    {	
                        SessionData sessionSupportSite = (SessionData) request.getSession().getAttribute("SessionData");                        
                        if (sessionSupportSite!=null && sessionSupportSite.isLoggedIn())
                        {
                            sessionSupportSite.setProperty("drep",drep);	
							request.getSession().setAttribute("expiryDiffDays", 0);
                            response.sendRedirect(request.getContextPath()+"/admin/index.jsp");
                            return;  
                        }

                        if (this.validateUserSupportSiteInter(request, response, sessionSupportSite, user,drep))
                        {
							request.getSession().setAttribute("expiryDiffDays", 0);
                            response.sendRedirect(request.getContextPath() + "/admin/index.jsp");
                        }
                        else
                        {
                            response.sendRedirect(request.getContextPath() + "/message.jsp?message=7");
                        }
                    }
		}
	}

	/**
	 * @param request
	 * @param response
	 * @param path
	 * @param sessionSupportSite
	 * @param user
	 */
	private boolean validateUserSupportSiteInter(HttpServletRequest request,HttpServletResponse response, SessionData sessionSupportSite, User user, String drep) 
	{		
		boolean validationResponse=false;
		try
		{    		            
          sessionSupportSite = new SessionData();
          String iso_default = (String)request.getSession().getServletContext().getAttribute("iso_default");
          
          user.setRefId(iso_default);
          user.setRefType(DebisysConstants.PW_REF_TYPE_REP_ISO);
          
          
          sessionSupportSite.setProperty("drep",drep);
          sessionSupportSite.setProperty("username", user.getUsername());
          sessionSupportSite.setProperty("password", user.getPassword());
          sessionSupportSite.setProperty("ref_id", iso_default);
          sessionSupportSite.setProperty("ref_type", user.getRefType());
          //********************************************************************
          //This means that if you enter by this point, all permission will added.
          sessionSupportSite.setProperty("access_level", DebisysConstants.ISO);
          //********************************************************************
          
          //determines always like ISO level
          sessionSupportSite.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_3_LEVEL);
          sessionSupportSite.setProperty("iso_id", iso_default);
          
          //set permissions for this user
          String[] blackListPermissions=null;
          if (request.getSession().getServletContext().getAttribute("blackListPermissions")!=null)
          {
        	  blackListPermissions = ((String)request.getSession().getServletContext().getAttribute("blackListPermissions")).split("\\|");
          }
          
          ArrayList<String> permissionsId = DBUser.getAllPermissionsId(blackListPermissions);
          for(String idPermission : permissionsId)
          {
                sessionSupportSite.setPermission(idPermission);
          }
          
          sessionSupportSite.setIsLoggedIn(true);
          sessionSupportSite.setUser(user);                     
          request.getSession().setAttribute("SessionData", sessionSupportSite);
          validationResponse=true;	              
	       
	  }
	  catch (Exception e)
	  {
		  System.out.println("SecurityException "+e);
	  }
	  return validationResponse;
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String password="";
		String username="";
		String language ="";
		String path = request.getContextPath();
		
		SessionData sessionSupportSite = (SessionData) request.getSession().getAttribute("SessionData");
		
		if (sessionSupportSite.isLoggedIn())
		{
			response.sendRedirect(path+"/admin/");
			return;  
		}
		User user = new User();
		password = request.getParameter("password");
		username = request.getParameter("username");
		user.setPassword(password);
		user.setUsername(username);
		
		language = sessionSupportSite.getLanguage();
		user.setLanguage(language);
		
		if (request.getParameter("submit_login") != null && request.getParameter("submit_login").equals("y")) {
			this.validateUserSupportSite(request, response, path, sessionSupportSite,user);
		}else if (request.getParameter("submit_answers") != null && request.getParameter("submit_answers").equals("y")) {
			this.validateAnswers(request, response, path, sessionSupportSite,user);
		}

	}

	/**
	 * @param request
	 * @param response
	 * @param path
	 * @param sessionSupportSite
	 * @param user
	 */
	private void validateUserSupportSite(HttpServletRequest request,HttpServletResponse response, String path,SessionData sessionSupportSite, User user) {
		Hashtable<String, String> userErrors = null;
		String instance = DebisysConfigListener.getInstance(request.getSession().getServletContext());
        String disableUser = Properties.getPropertyValue(instance, "global", "DisableUser");
        boolean isDisableUser = (disableUser != null && !disableUser.isEmpty() && disableUser.equalsIgnoreCase("true")) ? true : false;
		 request.getSession().setAttribute("expiryDiffDays", 0);
                 ServletContext context = request.getSession().getServletContext();
		try {
			if(validateCaptcha(request)){
				if (user.validateUser(sessionSupportSite)) {
					if (user.validateLogin(context, sessionSupportSite,false)) {
						request.getSession().setAttribute("loginAttempts", 0);
						request.getSession().setAttribute("showCaptcha", false);
						if (sessionSupportSite.isLoggedIn()) {
							request.getSession().invalidate();
							response.sendRedirect("message.jsp?message=2");
						} else {
							setAsLoggedIn(request, response, path, sessionSupportSite, user);
							return;
						}
					} else {
						userErrors = user.getErrors();
						request.getSession().setAttribute("userErrors",userErrors);
						response.sendRedirect(path+"/login.jsp");
					}
				} else {
					userErrors = user.getErrors();
					request.getSession().setAttribute("userErrors",userErrors);
					response.sendRedirect(path+"/login.jsp");
				}
			}else{
				 if(isDisableUser)
                            {
                                user.disable();
                                user.addFieldError("login", Languages.getString("com.debisys.users.error_user_disabled", sessionSupportSite.getLanguage()));
                                userErrors = user.getErrors();
								System.out.println("The user is disabled");
								request.getSession().setAttribute("userErrors",userErrors);
                                request.getSession().setAttribute("loginAttempts", 0);
                                request.getSession().setAttribute("captchaError",false);
								response.sendRedirect(path + "/login.jsp");
                            }
                            else
                            {
                                response.sendRedirect(path + "/login.jsp");
                            }
			}
		} catch (Exception e) {
			System.out.println("SecurityException "+e);
		}
	}

	/**
	 * @param request
	 * @param response
	 * @param path
	 * @param sessionSupportSite
	 * @param user
	 * @throws UserException
	 * @throws IOException
	 */
	private void setAsLoggedIn(HttpServletRequest request, HttpServletResponse response, String path, SessionData sessionSupportSite, User user)
			throws UserException, IOException {
		sessionSupportSite.clear();
		/*R34 Support Site Revamp*/
		sessionSupportSite.setProperty("name", user.getName());
		/*End R34 Support Site Revamp*/
		sessionSupportSite.setProperty("username", user.getUsername());
		sessionSupportSite.setProperty("password", user.getPassword());
		sessionSupportSite.setProperty("ref_id", user.getRefId());
		sessionSupportSite.setProperty("ref_type", user.getRefType());
		sessionSupportSite.setProperty("access_level", user.getAccessLevel());
		//determines 3 level or 5 level layout
		sessionSupportSite.setProperty("dist_chain_type", user.getDistChainType());
		sessionSupportSite.setProperty("company_name", user.getCompanyName());
		sessionSupportSite.setProperty("credit_type", user.getCreditType());
		sessionSupportSite.setProperty("contact_name", user.getContactName());
		sessionSupportSite.setProperty("ip_address", request.getRemoteAddr());
		//SessionData.setProperty("iso_id", User.getISOId(SessionData));
		sessionSupportSite.setProperty("iso_id", user.getISOId(user.getAccessLevel(),user.getRefId()));
		sessionSupportSite.setProperty("datasource", user.getDatasource());
		//System.out.println("sessionSupportSite.getProperty(iso_id): "+sessionSupportSite.getProperty("iso_id"));
		//System.out.println("sessionSupportSite.getProperty(ref_id): "+sessionSupportSite.getProperty("ref_id"));
		//set permissions for this user
		user.setPermissions(sessionSupportSite);

		if(sessionSupportSite.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
			user.setCarrierUserProducts(sessionSupportSite);
		}
		sessionSupportSite.setIsLoggedIn(true);
		sessionSupportSite.setUser(user);
                ServletContext context = request.getSession().getServletContext();
                
		if(user.isFirstTimeLogin(context, sessionSupportSite) || user.hasPasswordExpired(request,sessionSupportSite)) {
			request.getSession().setAttribute("expiryDiffDays", 0);
			response.sendRedirect(path+"/admin/changePassword.jsp");
		}else{
			  String instance = DebisysConfigListener.getInstance(request.getSession().getServletContext());
                        String enablePasswordExpiration = Properties.getPropertyValue(instance, "global", "EnablePasswordExpiryNotification");
                        boolean isEnablePasswordExpiration = (enablePasswordExpiration != null && !enablePasswordExpiration.isEmpty() && enablePasswordExpiration.equalsIgnoreCase("true")) ? true : false;
                        if(isEnablePasswordExpiration)
                        {
                            int daysLeft=user.expiryDaysLeft(request, sessionSupportSite);
                            request.getSession().setAttribute("expiryDiffDays", daysLeft);
                        }
                        else
                        {
                            request.getSession().setAttribute("expiryDiffDays", 0);
                        }
						response.sendRedirect(path+"/admin/index.jsp");
		}
		return;
	}

	private void validateAnswers(HttpServletRequest request,HttpServletResponse response, String path,SessionData session, User user){
		Hashtable<String, String> userErrors = null;
		String param;
		String ans1;
		String ans2;
		String username;
		int attemptsToAnswer = 2;
		boolean ans1ok = false;
		boolean ans2ok = false;
		String userAttempting = (String) request.getSession().getAttribute("answerAttemptsuser");
		Integer sattempts = (Integer) request.getSession().getAttribute("answerAttempts");
		int attempts = 0;
		if(sattempts != null){
			attempts = sattempts;
		}
		request.getSession().setAttribute("userErrors", null);
		
		try{
			username = request.getParameter("username"); 
			if(User.checkUserStatus(username) == 1){
				if(User.hasAnswersConfigured(username)){
					if(userAttempting == null || !userAttempting.equals(username)){
						attempts = 0;
					}
					attempts++;
					if(attempts <= attemptsToAnswer){
						int sq1; 
						int sq2;
						boolean ans1empty = false;
						boolean ans2empty = false;
						param = request.getParameter("sq1");
						sq1 = Integer.parseInt(param);
						param = request.getParameter("sq2");
						sq2 = Integer.parseInt(param);
						ans1 = request.getParameter("answer1");
						ans2 = request.getParameter("answer2");
						ans1empty = !(ans1 != null && !ans1.equals(""));
						ans2empty = !(ans2 != null && !ans2.equals(""));
						if(!ans1empty && !ans2empty){
							ans1ok = User.compareSecurityAnswer(username, sq1, ans1);
							ans2ok = User.compareSecurityAnswer(username, sq2, ans2);
						}
						if(ans1ok && ans2ok){
							user.setUsername(username);
							if(user.forceLoginAndAskToResetPass(session)){
								setAsLoggedIn(request, response, path, session, user);
							}
							request.getSession().setAttribute("answerAttemptsuser", null);
							request.getSession().setAttribute("answerAttempts", null);
							response.sendRedirect(path+"/admin/index.jsp");
						}else{
							userErrors = new Hashtable<String, String>();
							userErrors.put("answers", "jsp.answers.doesnt_match");
							request.getSession().setAttribute("userErrors", userErrors);
							request.getSession().setAttribute("answerAttemptsuser", username);
							request.getSession().setAttribute("answerAttempts", attempts);
							response.sendRedirect(path + "/admin/forgot_password.jsp");
						}
					}else{
						user.disable();
						errorAndClean("jsp.login.user_disabled", request, response, session, path+"/login.jsp");
						return;
					}
				}else{
					errorAndClean("jsp.answers.no_answers", request, response, session, path+"/login.jsp");
					return;					
				}
			}else{
				errorAndClean("jsp.login.user_disabled", request, response, session, path+"/login.jsp");
				return;
			}
		}catch(Exception e){
			// do nothing and try again
			System.out.println("SecurityException "+e);
		}
	}
	
	private void errorAndClean(String msg, HttpServletRequest request, HttpServletResponse response, SessionData session, String redirect) throws IOException{
		Hashtable<String, String> userErrors = new Hashtable<String, String>();
		userErrors.put("login", Languages.getString(msg, session.getLanguage()));
		request.getSession().setAttribute("answerAttemptsuser", null);
		request.getSession().setAttribute("answerAttempts", null);
		request.getSession().setAttribute("userErrors", userErrors);
		response.sendRedirect(redirect);
	}

	/**
	 * captcha validation. <br>
	 * @param request 
	 * @param context 
	 *
	 */
	private boolean validateCaptcha(HttpServletRequest request) {
		boolean ok = false;
		
		String instance = DebisysConfigListener.getInstance(request.getSession().getServletContext());
		int attemptsToCaptcha = 2;
		String disableUser = Properties.getPropertyValue(instance, "global", "DisableUser");
        boolean isDisableUser = (disableUser != null && !disableUser.isEmpty() && disableUser.equalsIgnoreCase("true")) ? true : false;
		try{
			attemptsToCaptcha = Integer.parseInt(Properties.getPropertyValue(instance, "global","login.attemptsToCaptcha"));
		}catch(Exception ex){}

		Integer sattempts = (Integer) request.getSession().getAttribute("loginAttempts");
		Boolean showCaptcha = (Boolean) request.getSession().getAttribute("showCaptcha");
		int attempts = 0;
		if(sattempts != null){
			attempts = sattempts;
		}
		if(showCaptcha != null && showCaptcha){
			Captcha captcha = (Captcha) request.getSession().getAttribute(Captcha.NAME);
			//request.setCharacterEncoding("UTF-8"); // Do this so we can capture non-Latin chars
			String answer = request.getParameter("answer_captcha");
			if (captcha.isCorrect(answer)) {
				ok = true;
			}
		}else{
			ok = true;
		}
		attempts++;
		if(attempts > attemptsToCaptcha){
			if(isDisableUser)
                    {
						showCaptcha = false;
                        ok=false;
                        System.out.println("The user is disabled");
                    }
                    else
                    {
                        showCaptcha = true;
                    }
		}else{
			showCaptcha = false;
		}
		request.getSession().setAttribute("loginAttempts", attempts);
		request.getSession().setAttribute("showCaptcha", showCaptcha);
		request.getSession().setAttribute("captchaError", !ok);
		return ok;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	@Override
	public void init() throws ServletException {
		// Put your code here
	}

	/*private User getCarrierSuperUser(HttpServletRequest request)
	{
		User u = new User();
		String prefix_user_intranet = request.getSession().getServletContext().getAttribute("prefix_user_intranet").toString();
		u.setUsername(prefix_user_intranet + intranetUser.getUserName());
		user.setIntranetUser(true);
		user.setIntranetUserId(Integer.parseInt(userSessionIntranet));
	  String sISODefault = request.getSession().getServletContext().getAttribute("iso_default").toString();
      u.setRefId(sISODefault);
      u.setRefType(DebisysConstants.PW_REF_TYPE_REP_ISO);
      sessionSupportSite.setProperty("username", user.getUsername());
      sessionSupportSite.setProperty("password", user.getPassword());
      sessionSupportSite.setProperty("ref_id", iso_default);
      sessionSupportSite.setProperty("ref_type", user.getRefType());
      //********************************************************************
      //This means that if you enter by this point, all permission will added.
      sessionSupportSite.setProperty("access_level", DebisysConstants.ISO);
      //********************************************************************
      
      //determines always like ISO level
      sessionSupportSite.setProperty("dist_chain_type", DebisysConstants.DIST_CHAIN_3_LEVEL);
      sessionSupportSite.setProperty("iso_id", iso_default);
		
	}*/
	
}
