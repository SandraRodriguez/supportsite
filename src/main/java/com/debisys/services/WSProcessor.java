package com.debisys.services;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URLEncoder;

import javax.servlet.ServletContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Category;
import org.xml.sax.InputSource;

import com.debisys.reports.TransactionReport;
import com.debisys.utils.DebisysConfigListener;

public class WSProcessor
{
	public static final int STUCK_TRANSATION = 3;
	public static final int VOIDED_TRX = 5;
	public static final int RESOLD_TRX = 7;
	public static final int SALE_TRX = 1;
	public static final int DUPLICATE_TRX = 4;	
	public static final int PENDING_FOR_BILLING = 2;
	
	private int _hostTimeOut = 30000;
	public static Category _log = Category.getInstance(TransactionReport.class);
	
	///http://192.168.2.58:2833/webservice/webservice.asp
	private String _hostName = "192.168.2.79";
	private int _hostPort = 8080;
	private String _postPage = "=/pcterm/webservice.asp";

	private long _lastRunTime;
	private String _jobName;
	
	
	public WSProcessor(ServletContext context)
	{
		this._hostName = DebisysConfigListener.getWebserviceURL(context);
		this._hostPort = Integer.parseInt(DebisysConfigListener.getWebservicePort(context));
		this._postPage = DebisysConfigListener.getWebservicePostPage(context);
	}

	public String[] doVoidByTrans(String[] trx)
	{
		StringBuffer strRequest = new StringBuffer();
		
		strRequest.append("32").append(","); //op: void
		strRequest.append(trx[0]).append(","); //siteID
		strRequest.append(trx[1]).append(","); //clerkID
		strRequest.append("v"+trx[2]).append(",,,,,");
		
		strRequest.append("SUPPORT");

		boolean ret = false;
		
		String strUrl;
		try
		{
			strUrl = "?termRequestString=" + URLEncoder.encode( strRequest.toString(), "UTF-8")+ "&hostId=VOIDRETRY";
		}
		catch(Exception ex)
		{
			_log.error("Encoding error in:" + trx[2],ex);
			return new String[]{"false","Encoding error in:" + trx[2]};
		}
			
		String xml = doRequestResponse(strUrl);
		String suc = GetXPathValue("/DATA_OUT/RESPONSE/SUCCESS_IND", xml);
		String res = GetXPathValue("/DATA_OUT/RESPONSE/RESPONSE_CODE", xml);
		String receipt = GetXPathValue("/DATA_OUT/RESPONSE/RESPONSE_MESSAGE", xml);
		
		ret = ((res.compareTo("00") == 0) && (suc.compareTo("1") == 0));
		
		int resultcode;

		try{
			resultcode=Integer.parseInt(res);
		}
		catch(Exception ex)
		{
			resultcode=-1;
		}
		
		if (ret)
		{
			_log.info("Voided Transaction:" + trx[2]);
		}
		else
		{
			_log.info("Error Voiding Transaction:" + trx[2]);
		}

//		return ret;
		return new String[]{String.valueOf(ret), receipt};
	}	

	private String doRequestResponse(String message)
	{
		long connectTime = 0;
		long responseTime = 0;
		String ans = new String();
		Socket socket = null;
		
		// a positive integer for connectTimeout will enable timing;
		int connectTimeout = _hostTimeOut / STUCK_TRANSATION; // arbitrary
		int reqrespTimeout = (_hostTimeOut * PENDING_FOR_BILLING) / STUCK_TRANSATION; // arbitrary
		long startTime = 0;
		
		try
		{
			StringBuffer sb = new StringBuffer();
			
			// connect to far end
			socket = new Socket();
			startTime = System.currentTimeMillis();
			if (connectTimeout > 0)
			{
				_log.debug("Connect timeout = " + connectTimeout + " ms");
				socket.connect(new InetSocketAddress(_hostName, _hostPort), connectTimeout);
			}
			else
			{
				socket.connect(new InetSocketAddress(_hostName, _hostPort));
			}
			_log.debug("Connect time: " + connectTime + " ms");
			
			// get streams
			OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());
			InputStreamReader in = new InputStreamReader(socket.getInputStream());
			
			// send request
			String request = "GET " + _postPage + message + " HTTP/1.1\r\n" + "Host: " + _hostName
					+ "\r\n" + "Connection: close" + "\r\n\r\n";
			if (reqrespTimeout > 0)
			{
				socket.setSoTimeout(reqrespTimeout);
			}
			_log.debug(request);
			out.write(request);
			out.flush();
			
			// read response
			_log.debug("Response timeout = " + reqrespTimeout + " ms");
			socket.setSoTimeout(reqrespTimeout);
			startTime = System.currentTimeMillis();
			while (true)
			{
				int i = in.read();
				if (i == -1)
				{
					break;
				}
				sb.append((char) i);
			}
			ans = sb.toString();
			socket.close();
			_log.debug("Response time: " + responseTime + " ms");
			
			// extract response body
			int pos = ans.indexOf("\r\n\r\n");
			if (pos != -1)
			{
				pos = pos + 4;
				ans = ans.substring(pos);
			}
			_log.debug(ans);
			
		}
		catch (Exception ex)
		{
			_log.warn("Caught exception:", ex);
		}
		finally
		{
			try
			{
				if (socket != null)
				{
					socket.close();
				}
			}
			catch (IOException ex)
			{
			}
		}
		
		return ans;
	}
	
	private String GetXPathValue(String path, String xml)
	{
		
		String temp = "";
		try
		{
			InputSource is = new InputSource(new StringReader(xml));
			XPath xpath = XPathFactory.newInstance().newXPath();
			temp = xpath.evaluate(path, is);
		}
		catch (XPathExpressionException ex)
		{
			_log.info(ex.getCause());
		}
		
		catch (Exception ex)
		{
			_log.warn(ex);
		}
		
		return temp;
		
	}
	
	
	public String[] doWriteOffByTrans(String[] trx)
	{
		StringBuffer strRequest = new StringBuffer();
		
		strRequest.append("81").append(","); //op: write off
		strRequest.append(trx[0]).append(","); //siteID
		strRequest.append(trx[1]).append(",,"); //clerkID
		strRequest.append(trx[2]).append(",,,,,");
		
		strRequest.append("SUPPORT");

		boolean ret = false;
		
		String strUrl;
		try
		{
			strUrl = "?termRequestString=" + URLEncoder.encode( strRequest.toString(), "UTF-8")+ "&hostId=SUPPORT";
		}
		catch(Exception ex)
		{
			_log.error("Encoding error in:" + trx[2],ex);
			return new String[]{"false","Encoding error in:" + trx[2]};
		}
			
		String xml = doRequestResponse(strUrl);
		String suc = GetXPathValue("/DATA_OUT/RESPONSE/SUCCESS_IND", xml);
		String res = GetXPathValue("/DATA_OUT/RESPONSE/RESPONSE_CODE", xml);
		String receipt = GetXPathValue("/DATA_OUT/RESPONSE/RESPONSE_MESSAGE", xml);
		String trxId = GetXPathValue("/DATA_OUT/TRANSACTION/TRANS_ID", xml);
		String hostTrxCode = GetXPathValue("/DATA_OUT/RESPONSE/HOST_TRANSACTION_RESULT_CODE", xml);
		
		ret = ((res.compareTo("0") == 0||res.compareTo("00") == 0) && (suc.compareTo("1") == 0));
		
		if (ret)
		{
			_log.info("Writen off Transaction:" + trx[2]);
		}
		else
		{
			_log.info("Error Writing Transaction off trx:" + trx[2]);
		}

		return new String[]{String.valueOf(ret), trxId, hostTrxCode,receipt};
	}	
	
}
