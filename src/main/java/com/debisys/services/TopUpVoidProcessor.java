package com.debisys.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javax.servlet.ServletContext;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.debisys.exceptions.ReportException;

public class TopUpVoidProcessor
{
	
	  // log4j logging
	  private static Logger       cat        = Logger.getLogger(TopUpVoidProcessor.class);
	  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.services.sql");

	  public static boolean isSKUAutomaticVoidable(String sku) throws ReportException
	{
		Connection cnx = null;
		boolean nResult = false;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String strSQL = "";
		
		try
		{
			strSQL = "SELECT * FROM AutomaticVoid_skus (nolock) WHERE SKU = ?";
			cnx = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cnx == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			cat.debug(strSQL);
			pst = cnx.prepareStatement(strSQL);
			pst.setInt(1, Integer.parseInt(sku));
			rs = pst.executeQuery();
			if (rs.next())
			{
				nResult = true;
			}
		}
		catch (Exception e)
		{
			cat.error("Error getting AutomaticVoid_skus", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				rs.close();
				pst.close();
				Torque.closeConnection(cnx);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return nResult;
	}// End of function isSKUAutomaticVoidable
	
	
	public static String[] processAutVoid(ServletContext context, Integer iSiteID,Integer iTransactionID,String clerkID) throws ReportException
	{
		WSProcessor processor = new WSProcessor(context);
		String[] transParams  = new String[3];
		transParams[0] = iSiteID.toString();
		transParams[1] = clerkID;
		transParams[2] = iTransactionID.toString();

//		String response  = "";
//		if(processor.doVoidByTrans(transParams))
//			response = Languages.getString("jsp.admin.transactions.voidtopup_request.sucessAutVoid");
//		else response = "-1"; 
//		return response;
		return processor.doVoidByTrans(transParams);
	}
	
	
	
}
