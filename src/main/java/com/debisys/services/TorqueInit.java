package com.debisys.services;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.torque.Torque;

public class TorqueInit extends HttpServlet {
    private static boolean inited = false;

    public void init() throws ServletException{
        if (inited) return;
        System.out.println("****** Initing Torque for " + getServletContext().getServletContextName());
        inited = true;
        try {
          String dir = getInitParameter("torque-resource-directory");
          Torque.init(getServletContext().getRealPath("/") + dir);
          System.out.println("****** Torque Initialized - " + Torque.getDefaultDB());
        } catch (Exception e) {
          System.out.print(e.getMessage());
          System.out.println("****** Torque Initialization Failed!");
          throw new ServletException( e.toString() );
        }
    }



public  void doGet(HttpServletRequest req, HttpServletResponse res) {
  }
}
