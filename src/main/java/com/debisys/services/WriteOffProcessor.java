package com.debisys.services;

import javax.servlet.ServletContext;

import com.debisys.exceptions.ReportException;

/**
 * @author ?
 */
public class WriteOffProcessor
{
	
	/**
	 * @param context
	 * @param iSiteID
	 * @param iTransactionID
	 * @param clerkID
	 * @return String[]
	 * @throws ReportException
	 */
	public static String[] processWriteOff(ServletContext context, Integer iSiteID,Integer iTransactionID,String clerkID) throws ReportException
	{
		WSProcessor processor = new WSProcessor(context);
		String[] transParams  = new String[3];
		transParams[0] = iSiteID.toString();
		transParams[1] = clerkID;
		transParams[2] = iTransactionID.toString();

		return processor.doWriteOffByTrans(transParams);
	}
	
	
	
}
