package com.debisys.services;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.PropertyConfigurator;

public class Log4jInit extends HttpServlet {

  public
  void init() {
//    String prefix = DebisysConfigListener.getServerType(getServletContext());
//    String file = getInitParameter(prefix + "log4j-init-file");
    String file = getInitParameter("log4j-init-file");
    // if the log4j-init-file is not set, then no point in trying
    if(file != null) {
//      PropertyConfigurator.configure(file);
      PropertyConfigurator.configure(getServletContext().getRealPath("/") + file);
      System.out.println("****** Log4J Initialized for " + getServletContext().getServletContextName());
    }
    else
    {
	  System.out.println("****** Log4J NOT Initialized");
	}

  }

  public
  void doGet(HttpServletRequest req, HttpServletResponse res) {
  }
}
