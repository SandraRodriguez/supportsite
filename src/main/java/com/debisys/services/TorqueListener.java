package com.debisys.services;

import net.emida.supportsite.util.SpringBeanFactory;
import net.emida.supportsite.util.security.AESCipher;
import org.apache.commons.configuration.*;
import org.apache.torque.Torque;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Franky Villadiego
 */
public class TorqueListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        if (Torque.isInit()) return;

        ServletContext ctx = servletContextEvent.getServletContext();

        WebApplicationContext appCtx = WebApplicationContextUtils.getRequiredWebApplicationContext(ctx);

        System.out.println("****** Initing Torque for " +  ctx.getServletContextName());
        try {
            Object propertiesFile = ctx.getInitParameter("propertiesFile");
            System.out.println("##########PropertiesFile =" + propertiesFile);
            String theFile = ctx.getRealPath("/") + propertiesFile;
            System.out.println("############# propertiesFile = " + theFile);

            Configuration configuration = getConfiguration(theFile);

            setupPasswordProperties(configuration, appCtx);

            String url = configuration.getString("torque.dsfactory.masterDb.connection.url");
            String db = configuration.getString("torque.database.default");
            String factory = configuration.getString("torque.dsfactory.masterDb.factory");
            String checkoutTimeout = configuration.getString("torque.dsfactory.masterDb.pool.checkoutTimeout");

            Torque.init(configuration);
            System.out.println("****** Torque Initialized - " + Torque.getDefaultDB());
        } catch (Exception e) {
            System.out.print(e.getMessage());
            System.out.println("****** Torque Initialization Failed!");

        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }


    private Configuration getConfiguration(String propertiesFile) throws ConfigurationException {
        CompositeConfiguration configuration = new CompositeConfiguration();
        Configuration propertiesConfiguration = new PropertiesConfiguration(propertiesFile);
        Configuration systemConfiguration = new SystemConfiguration();
        configuration.addConfiguration(propertiesConfiguration);
        configuration.addConfiguration(systemConfiguration);
        return configuration;
    }

    private void setupPasswordProperties(Configuration propCfg, WebApplicationContext webAppCtx) {
        try {
            Map<String, String> map = new HashMap<String, String>();
            Iterator keys = propCfg.getKeys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                String value = propCfg.getString(key);
                if (value.contains("ENC(")) {
                    String encrypted = value.substring(4, value.length() - 1);
                    AESCipher cipher = webAppCtx.getBean(AESCipher.class);
                    String decrypted = cipher.decrypt(encrypted);
                    map.put(key, decrypted);
                }
            }
            for (Map.Entry<String, String> entry : map.entrySet()) {
                propCfg.setProperty(entry.getKey(), entry.getValue());
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}
