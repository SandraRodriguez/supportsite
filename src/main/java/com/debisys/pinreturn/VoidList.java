package com.debisys.pinreturn;

import java.util.ArrayList;

public class VoidList extends ArrayList<VoidQueue>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/* (non-Javadoc)
	 * @see java.util.ArrayList#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) 
	{		
		for (int i=0;i<this.size();i++)
		{			
			VoidQueue voidRecord = ((VoidQueue)o);						
		    if ( this.get(i).getVoidTrxNumber().equals( voidRecord.getVoidTrxNumber() ) )  
		    {
		     return true;
		    }
		}
		return false;		
	}

}
