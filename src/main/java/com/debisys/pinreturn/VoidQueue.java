/**
 * 
 */
package com.debisys.pinreturn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;

/**
 * @author nmartinez
 *
 */
public class VoidQueue {

	private static Logger       cat        = Logger.getLogger(VoidQueue.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.pinreturn.sql");
	
	private String id;
	private String VoidTypeId;
	private String VoidTrxNumber;
	private String VoidInfo;
	private String ProviderId;
	private String Retries;
	private String LastResultCode;
	private String ProcessedDate;
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the voidTypeId
	 */
	public String getVoidTypeId() {
		return VoidTypeId;
	}
	/**
	 * @param voidTypeId the voidTypeId to set
	 */
	public void setVoidTypeId(String voidTypeId) {
		VoidTypeId = voidTypeId;
	}
	/**
	 * @return the voidTrxNumber
	 */
	public String getVoidTrxNumber() {
		return VoidTrxNumber;
	}
	/**
	 * @param voidTrxNumber the voidTrxNumber to set
	 */
	public void setVoidTrxNumber(String voidTrxNumber) {
		VoidTrxNumber = voidTrxNumber;
	}
	/**
	 * @return the voidInfo
	 */
	public String getVoidInfo() {
		return VoidInfo;
	}
	/**
	 * @param voidInfo the voidInfo to set
	 */
	public void setVoidInfo(String voidInfo) {
		VoidInfo = voidInfo;
	}
	/**
	 * @return the providerId
	 */
	public String getProviderId() {
		return ProviderId;
	}
	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(String providerId) {
		ProviderId = providerId;
	}
	/**
	 * @return the retries
	 */
	public String getRetries() {
		return Retries;
	}
	/**
	 * @param retries the retries to set
	 */
	public void setRetries(String retries) {
		Retries = retries;
	}
	/**
	 * @return the lastResultCode
	 */
	public String getLastResultCode() {
		return LastResultCode;
	}
	/**
	 * @param lastResultCode the lastResultCode to set
	 */
	public void setLastResultCode(String lastResultCode) {
		LastResultCode = lastResultCode;
	}
	/**
	 * @return the processedDate
	 */
	public String getProcessedDate() {
		return ProcessedDate;
	}
	/**
	 * @param processedDate the processedDate to set
	 */
	public void setProcessedDate(String processedDate) {
		ProcessedDate = processedDate;
	}
	
	
	
	public static VoidList findLastTrxToVoid(ServletContext context){

		VoidList findLastTrxToVoid = new VoidList();	
		Connection cnx = null;		
		PreparedStatement pst = null;
		String strSQL = "";

		try
		{
			strSQL = sql_bundle.getString("findLastTrxToVoid");
			cnx = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cnx == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			int limitDays = 1;
			String instance = DebisysConfigListener.getInstance(context);
			pst = cnx.prepareStatement("SELECT [value] FROM properties WITH (NOLOCK) WHERE project = 'support' AND module = 'global' AND property = 'daysBackToSearchVoidQueue' AND instance = '"+instance+"'");
			ResultSet rs = pst.executeQuery();
			if ( rs.next())
			{
				limitDays = rs.getInt(1);
			}
						
			cat.debug(strSQL);
			pst = cnx.prepareStatement(strSQL);
			pst.setInt(1, limitDays);			
			rs = pst.executeQuery();
			
			while ( rs.next() )
			{
				/*
				1 Id	int
				2 VoidTypeId	int
				3 VoidTrxNumber	int
				4 VoidInfo	varchar
				5 ProviderId	int
				6 Retries	int
				7 LastResultCode	int
				8 ProcessedDate	datetime
				*/
				VoidQueue voidQ = new VoidQueue();
				voidQ.setId(rs.getString(1));
				voidQ.setVoidTypeId(rs.getString(2));
				voidQ.setVoidTrxNumber(rs.getString(3));
				voidQ.setVoidInfo(rs.getString(4));
				voidQ.setProviderId(rs.getString(5));
				voidQ.setRetries(rs.getString(6));
				voidQ.setLastResultCode(rs.getString(7));
				voidQ.setProcessedDate(rs.getString(8));
				
				findLastTrxToVoid.add(voidQ);
				
			}
			rs.close();
			pst.close();
		}
		catch (Exception e)
		{
			cat.error("Error during findLastTrxToVoid ", e);			
		}
		finally
		{
			try
			{
				Torque.closeConnection(cnx);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}		
		return findLastTrxToVoid;
	}
	
	
	public static boolean insertPINReturnRequestCommentACED(int lCaseNumber, int rejectedId, String rec_id, SessionData sessionData)
	{
		Connection cnx = null;
		boolean bResult = false;
		PreparedStatement pst = null;
		String strSQL = "";

		try
		{			
			strSQL = sql_bundle.getString("updatePINRequestStatusACED");
			cnx = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (cnx == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			String messageState = Languages.getString("jsp.admin.pin.return.messageState", sessionData.getLanguage());
			String userId = sessionData.getUser().getUsername();
			String messageStateInsert = String.format( Languages.getString("jsp.admin.pin.return.messageStateInsert", sessionData.getLanguage()), userId);
			
			
			cat.debug(strSQL);
			cnx.setAutoCommit(false);
			pst = cnx.prepareStatement(strSQL);
			
			pst.setInt(1, rejectedId);
			pst.setInt(2, lCaseNumber);
			
			bResult = (pst.executeUpdate() > 0);
			if ( bResult )
			{
				pst.close();
				strSQL = sql_bundle.getString("insertPINReturnRequestCommentACED");
				pst = cnx.prepareStatement(strSQL);
				pst.setInt(1, lCaseNumber);
				pst.setString(2, messageState + " "+messageStateInsert);
				
				bResult = (pst.executeUpdate() > 0);				
				if ( bResult )
				{
					cnx.commit();					
				}
				else
				{
					cnx.rollback();
					cat.debug("Could not insert PIN Request comment case number "+lCaseNumber);
				}
			}
			else
			{
				cat.debug("Could not update PIN Request status case number "+lCaseNumber);
			}
			pst.close();
		}
		catch (Exception e)
		{
			try
			{
				if ( cnx != null)
					cnx.rollback();
			}
			catch (Exception ex)
			{
				cat.error("Error during rollback in insertPINReturnRequestCommentACED ", ex);
			}
			cat.error("Error during insertPINReturnRequestCommentACED ", e);			
		}
		finally
		{			
			try
			{
				cnx.setAutoCommit(true);
				if ( pst !=null)
					pst.close();
				Torque.closeConnection(cnx);
				
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection in insertPINReturnRequestCommentACED ", e);
			}
		}
		return bResult;
	}
	
	
	
}
