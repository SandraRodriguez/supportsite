package com.debisys.document;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.DocumentException;
import com.debisys.exceptions.ImageException;
import com.debisys.exceptions.RatePlanException;
import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.LogChanges;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

public class Document {
	@SuppressWarnings("deprecation")
	static Category cat = Category.getInstance(Document.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.document.sql");

	private String userIntranet="";
	
	private long documentId;
	private String criteria = "";

	private String repId;
	private String name;
	private String description;
	private String user;
	private String lastUpdate;
	private String email;
	private boolean visible;
	private int approvalStatus=2;
	private String path;
	private String category;
	private boolean iso=false;
	private boolean agent=false;
	private boolean subagent=false;
	private boolean rep=false;
	private boolean merchant=false;
	private String uploadDate;
	private int merchantRouteId;
	
	private String businessName;
	private String rep_id_find;
	
	private String strAccessLevel;
	private String strDistChainType;
	private String strRefId;
	private String iso_default;
	
	private String message="";
	
	private SessionData sessionData;
	
	private String reason="";
	
		/**
	 * @return the userIntranet
	 */
	public String getUserIntranet() {
		return userIntranet;
	}

	/**
	 * @param userIntranet the userIntranet to set
	 */
	public void setUserIntranet(String userIntranet) {
		this.userIntranet = userIntranet;
	}
	
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the sessionData
	 */
	public SessionData getSessionData() {
		return sessionData;
	}

	/**
	 * @param sessionData the sessionData to set
	 */
	public void setSessionData(SessionData sessionData) {
		this.sessionData = sessionData;
	}


	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String getIso_default() {
		return iso_default;
	}

	/**
	 * @param strAccessLevel the strAccessLevel to set
	 */
	public void setIso_default(String strIso_default) {
		this.iso_default = strIso_default;
	}
	/**
	 * @return the strAccessLevel
	 */
	public String getStrAccessLevel() {
		return strAccessLevel;
	}

	/**
	 * @param strAccessLevel the strAccessLevel to set
	 */
	public void setStrAccessLevel(String strAccessLevel) {
		this.strAccessLevel = strAccessLevel;
	}

	/**
	 * @return the strDistChainType
	 */
	public String getStrDistChainType() {
		return strDistChainType;
	}

	/**
	 * @param strDistChainType the strDistChainType to set
	 */
	public void setStrDistChainType(String strDistChainType) {
		this.strDistChainType = strDistChainType;
	}

	/**
	 * @return the strRefId
	 */
	public String getStrRefId() {
		return strRefId;
	}

	/**
	 * @param strRefId the strRefId to set
	 */
	public void setStrRefId(String strRefId) {
		this.strRefId = strRefId;
	}

	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return the rep_id_find
	 */
	public String getRep_id_find() {
		return rep_id_find;
	}

	/**
	 * @param repIdFind the rep_id_find to set
	 */
	public void setRep_id_find(String repIdFind) {
		rep_id_find = repIdFind;
	}

	private String[] chain;
	

	private String sort = "";
	private String col = "";
	
	 public String getCol()
	  {
	    return StringUtil.toString(col);
	  }

	  public void setCol(String strCol)
	  {
	    col = strCol;
	  }
	  
	public String getSort()
	{
		return StringUtil.toString(sort);
	}

	public void setSort(String strSort)
	{
		sort = strSort;
	}
	  
	public int getMerchantRouteId() {
		return merchantRouteId;
	}

	public void setMerchantRouteId(int merchantRouteId) {
		this.merchantRouteId = merchantRouteId;
	}
	/**
	 * 
	 * @return
	 */
	public String getCriteria()
	{
		return StringUtil.toString(criteria);
	}

	public void setCriteria(String strCriteria)
	{
		criteria = strCriteria.trim();
	}

	  
	/**
	 * @return the imageId
	 */
	public long getDocumentId() {
		return documentId;
	}

	/**
	 * @param imageId
	 *            the imageId to set
	 */
	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	
	/**
	 * @return the chain
	 */
	public String[] getChain() {
		return chain;
	}

	/**
	 * @param chain the chain to set
	 */
	public void setChain(String[] chain) {
		this.chain = chain;
	}

	/**
	 * @return the uploadDate
	 */
	public String getUploadDate() {
		return uploadDate;
	}

	/**
	 * @param uploadDate the uploadDate to set
	 */
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	/**
	 * @param Id
	 * @return
	 * @throws ImageException
	 */
	public long save() throws DocumentException {
	
		return documentId;
	}

	/**
	 * @return the repId
	 */
	public String getRepId() {
		return repId;
	}

	/**
	 * @param repId the repId to set
	 */
	public void setRepId(String repId) {
		this.repId = repId;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the visible
	 */
	public boolean getVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the approvalStatus
	 */
	public int getApprovalStatus() {
		return approvalStatus;
	}

	/**
	 * @param approvalStatus the approvalStatus to set
	 */
	public void setApprovalStatus(int approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the iso
	 */
	public boolean getIso() {
		return iso;
	}

	/**
	 * @param iso the iso to set
	 */
	public void setIso(boolean iso) {
		this.iso = iso;
	}

	/**
	 * @return the agent
	 */
	public boolean getAgent() {
		return agent;
	}

	/**
	 * @param agent the agent to set
	 */
	public void setAgent(boolean agent) {
		this.agent = agent;
	}

	/**
	 * @return the subagent
	 */
	public boolean getSubagent() {
		return subagent;
	}

	/**
	 * @param subagent the subagent to set
	 */
	public void setSubagent(boolean subagent) {
		this.subagent = subagent;
	}

	/**
	 * @return the rep
	 */
	public boolean getRep() {
		return rep;
	}

	/**
	 * @param rep the rep to set
	 */
	public void setRep(boolean rep) {
		this.rep = rep;
	}

	/**
	 * @return the merchant
	 */
	public boolean getMerchant() {
		return merchant;
	}

	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(boolean merchant) {
		this.merchant = merchant;
	}

	public boolean load() throws DocumentException {
	 
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean loaded = false;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL =""+ sql_bundle.getString("getDocument");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, documentId);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				setDocumentId(rs.getLong("documentId"));
				setRepId(rs.getString("repId"));
				setName(rs.getString("Name"));
				setDescription(rs.getString("description"));
				setUser(rs.getString("userID"));
				setLastUpdate(rs.getString("lastUpdate"));
				setEmail(rs.getString("email"));
				setVisible(rs.getBoolean("visible"));
				setApprovalStatus(rs.getInt("approvalStatus"));
				setPath(rs.getString("documentPath"));
				setCategory(rs.getString("CategoryId"));
				setUploadDate(rs.getString("uploadDate"));
				setIso(rs.getBoolean("iso"));
				setAgent(rs.getBoolean("agent"));
				setSubagent(rs.getBoolean("subagent"));
				setRep(rs.getBoolean("rep"));
				setMerchant(rs.getBoolean("merchant"));
				
				String val[] = new String[5];
				if(rs.getBoolean("iso")) val[0] = "1";
				if(rs.getBoolean("agent")) val[1] = "2";
				if(rs.getBoolean("subagent")) val[2] = "3";
				if(rs.getBoolean("rep")) val[3] = "4";
				if(rs.getBoolean("merchant")) val[4] = "5";
				setChain(val);
				loaded = true;
			}
			cat.debug("Getting documentId values: document=" + documentId);
		} catch (Exception e) {
			cat.error("Error during Document.load", e);
			throw new DocumentException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		
		return true;
	}


	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}





	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param lastUpdate
	 *            the lastUpdate to set
	 */
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @return the lastUpdate
	 */
	public String getLastUpdate() {
		return lastUpdate;
	}

	
	public void saveData() throws DocumentException {

		Connection dbConn = null;
		Vector<Vector<String>> isos = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}

		String strSQL ="";
		if(!userIntranet.equals("true")){
	    	strSQL = sql_bundle.getString(documentId > 0 ? "updateDocumentEntry" : "insertDocumentEntry");
	
			if(user == null){
				user ="";
			}
			
			pstmt = dbConn.prepareStatement(strSQL);

			if(documentId > 0){
				pstmt.setString(1, getName());
				pstmt.setString(2, description);
				pstmt.setString(3, uploadDate);
				pstmt.setString(4, email);
				pstmt.setBoolean(5, visible);
				pstmt.setString(6,category);
				pstmt.setBoolean(7,getIso());
				pstmt.setBoolean(8,getAgent());
				pstmt.setBoolean(9,getSubagent());
				pstmt.setBoolean(10,getRep());
				pstmt.setBoolean(11,getMerchant());
				pstmt.setLong(12, documentId);
			}else{
				pstmt.setString(1, getRepId());
				pstmt.setString(2, getName());
				pstmt.setString(3, description);
				pstmt.setString(4, user);
				pstmt.setString(5, uploadDate);
				pstmt.setString(6, email);
				pstmt.setBoolean(7, visible);
				pstmt.setInt(8, approvalStatus);
				pstmt.setString(9, path);
				pstmt.setString(10,category);
				pstmt.setString(11,uploadDate);
				pstmt.setBoolean(12,getIso());
				pstmt.setBoolean(13,getAgent());
				pstmt.setBoolean(14,getSubagent());
				pstmt.setBoolean(15,getRep());
				pstmt.setBoolean(16,getMerchant());				
			}
		}
		else{
			strSQL = sql_bundle.getString(documentId > 0 ? "updateDocumentEntryIntra" : "insertDocumentEntry");
			if(user == null){
				user ="";
			}
			
			pstmt = dbConn.prepareStatement(strSQL);

			if(documentId > 0){
				pstmt.setString(1, getName());
				pstmt.setString(2, description);
				pstmt.setString(3, uploadDate);
				pstmt.setString(4, email);
				pstmt.setBoolean(5, visible);
				pstmt.setString(6,category);
				pstmt.setLong(7, documentId);
			}else{
				pstmt.setString(1, getRepId());
				pstmt.setString(2, getName());
				pstmt.setString(3, description);
				pstmt.setString(4, user);
				pstmt.setString(5, uploadDate);
				pstmt.setString(6, email);
				pstmt.setBoolean(7, visible);
				pstmt.setInt(8, approvalStatus);
				pstmt.setString(9, path);
				pstmt.setString(10,category);
				pstmt.setString(11,uploadDate);
				pstmt.setBoolean(12,getIso());
				pstmt.setBoolean(13,getAgent());
				pstmt.setBoolean(14,getSubagent());
				pstmt.setBoolean(15,getRep());
				pstmt.setBoolean(16,getMerchant());				
			}				
			
		}
			
			if (documentId > 0) {
				pstmt.execute();
				cat.debug("Updating documentId= " + documentId );
			}else{
				rs = pstmt.executeQuery();
				if(rs.next()){
					documentId = rs.getLong("Id");
				}
				cat.debug("Creating documentId= " + documentId);
			}
			
			
			

		} catch (Exception e) {
			cat.error("Error during saveData", e);
			throw new DocumentException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		
		
	}
	
	
	
	public Vector<Vector<String>> getAllChain() throws DocumentException {
		
		Vector<Vector<String>> v = new Vector<Vector<String>>();
		Vector<String> vecTemp = new Vector<String>();
		
		try {
			if (sessionData != null && sessionData.getUser().isIntranetUser()
					&& sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
							DebisysConstants.INTRANET_PERMISSION_MANAGE_DOCUMENTS)){

	        	vecTemp.add("1");
	    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.iso", sessionData.getLanguage()));
	    		v.add(vecTemp);
	    		vecTemp = new Vector<String>();
	    		vecTemp.add("2");
	    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.agent", sessionData.getLanguage()));
	    		v.add(vecTemp);
	    		vecTemp = new Vector<String>();
	    		vecTemp.add("3");
	    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.subagent", sessionData.getLanguage()));
	    		v.add(vecTemp);
	    		vecTemp = new Vector<String>();
	    		vecTemp.add("4");
	    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.rep", sessionData.getLanguage()));
	    		v.add(vecTemp);
	    		vecTemp = new Vector<String>();
	    		vecTemp.add("5");
	    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.merchant", sessionData.getLanguage()));
	    		v.add(vecTemp);
				
			}
			else{

				if(!strRefId.equals(iso_default)){
					if (strAccessLevel.equals(DebisysConstants.ISO))
				      {
				        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				        {
				        	vecTemp.add("1");
				        	vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.iso", sessionData.getLanguage()));
				        	v.add(vecTemp);
				        	vecTemp = new Vector<String>();
				    		vecTemp.add("4");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.rep", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("5");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.merchant", sessionData.getLanguage()));
				    		v.add(vecTemp);
				        }
				        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				        {
				        	vecTemp.add("1");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.iso", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("2");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.agent", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("3");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.subagent", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("4");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.rep", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("5");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.merchant", sessionData.getLanguage()));
				    		v.add(vecTemp);
				        }
				      }
					else if (strAccessLevel.equals(DebisysConstants.AGENT))
				    {
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("2");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.agent", sessionData.getLanguage()));
				    		v.add(vecTemp);
							vecTemp = new Vector<String>();
				    		vecTemp.add("3");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.subagent", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("4");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.rep", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("5");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.merchant", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    }
				    else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
				    {
							vecTemp = new Vector<String>();
				    		vecTemp.add("3");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.subagent", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("4");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.rep", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("5");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.merchant", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    }
				    else if (strAccessLevel.equals(DebisysConstants.REP))
				    {
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("4");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.rep", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    		vecTemp = new Vector<String>();
				    		vecTemp.add("5");
				    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.merchant", sessionData.getLanguage()));
				    		v.add(vecTemp);
				    }
			    	 
			    }
				else{
		        	vecTemp.add("1");
		    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.iso", sessionData.getLanguage()));
		    		v.add(vecTemp);
		    		vecTemp = new Vector<String>();
		    		vecTemp.add("2");
		    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.agent", sessionData.getLanguage()));
		    		v.add(vecTemp);
		    		vecTemp = new Vector<String>();
		    		vecTemp.add("3");
		    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.subagent", sessionData.getLanguage()));
		    		v.add(vecTemp);
		    		vecTemp = new Vector<String>();
		    		vecTemp.add("4");
		    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.rep", sessionData.getLanguage()));
		    		v.add(vecTemp);
		    		vecTemp = new Vector<String>();
		    		vecTemp.add("5");
		    		vecTemp.add(Languages.getString("jsp.admin.document.documentrepository.merchant", sessionData.getLanguage()));
		    		v.add(vecTemp);
					
					
				}				
				
			}
		} catch (UserException e) {
		
			e.printStackTrace();
		}


		
		return v;
	}
	
	public Vector<Vector<String>> getAllCategory() throws DocumentException {
		Connection dbConn = null;
		Vector<Vector<String>> isos = new Vector<Vector<String>>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new RatePlanException();
			}
			String strSQL = sql_bundle.getString("getAllCategories");
			pstmt = dbConn.prepareStatement(strSQL);
			cat.debug(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("CategoryId")));
				vecTemp.add(StringUtil.toString(rs.getString("description")));
				isos.add(vecTemp);
			}
		} catch (Exception e) {
			cat.error("Error during getAllCategory", e);
			throw new DocumentException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return isos;
	}	
	
	public void sendMailNotification(String mailHost, String sAccessLevel, String companyName, String userName, String option, SessionData sessionData) throws Exception {
		
		//Send Mail
		
		java.util.Properties props;
		props = System.getProperties();
		props.put("mail.smtp.host", mailHost);
		Session s = Session.getDefaultInstance(props, null);

		//InternetAddress[] address = {new InternetAddress(email.replace(";", ","))};
		int p,t = 0;
		
		String val,valini = email;
		val = email.replace(",", ";");
		String newemail="";
		
		while(valini.contains(";")){
			if(valini.contains(";")){
				t+=1;
				valini = valini.substring(valini.indexOf(";")+1, valini.length());
			}
		}
		if(valini.length()>0){
			newemail=valini.substring(0,valini.length());
			t+=1;
		}
		
		InternetAddress[] toAddrs = new InternetAddress[t];
		
		t=0;
		
		while(val.contains(";")){
			if(val.contains(";")){
				newemail=val.substring(0,val.indexOf(";"));
				toAddrs[t] = new InternetAddress(newemail);
				t+=1;
				val = val.substring(val.indexOf(";")+1, val.length());
			}
		}
		if(val.length()>0){
				newemail=val.substring(0,val.length());
				toAddrs[t] = new InternetAddress(newemail);
				t+=1;
		}
		
		InternetAddress[] toAddrsCC = new InternetAddress[1];
		toAddrsCC[0] = new InternetAddress(this.getSetupMail());

		  
		MimeMessage message = new MimeMessage(s);
		message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
		//message.setFrom(new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail()));
		message.setFrom(new InternetAddress(this.getSetupMail()));
		//message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
		message.setRecipients(Message.RecipientType.TO, toAddrs);
		message.setRecipients(Message.RecipientType.CC, toAddrsCC);
		message.setSubject(Languages.getString("jsp.admin.document.documentrepository.mainsubject",sessionData.getLanguage()));
		String content="";
		String rta="";
		content=Languages.getString("jsp.admin.document.documentrepository.yourdocument",sessionData.getLanguage());
		if(option!=null && option.equals("upload")){
			rta=Languages.getString("jsp.admin.document.documentrepository.rtaupload",sessionData.getLanguage());
			message.setContent(content + " <b>" + name + "</b> "+ rta, "text/html");
		}else if(option!=null && option.equals("approved"))	{
			rta=Languages.getString("jsp.admin.document.documentrepository.rtaapproved",sessionData.getLanguage());
			message.setContent(content+ " <b>" + name  + "</b> "+ rta, "text/html");
		}else if(option!=null && option.equals("denied"))	{
			rta=Languages.getString("jsp.admin.document.documentrepository.rtadenied",sessionData.getLanguage());
			rta+=reason;
			message.setContent(content+ "<b> " + name  + "</b> "+ rta, "text/html");
		}
		else if(option!=null && option.equals("updated"))	{
			rta=Languages.getString("jsp.admin.document.documentrepository.rtaupdated",sessionData.getLanguage());
			message.setContent(content+ "<b> " + name  + "</b> "+ rta, "text/html");
		}
		
		
		Transport.send(message);
		
	}
	
	public Vector getCategories() throws DocumentException {
		Vector vecCategories = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Document.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Document.cat.error("Can't get database connection");
				throw new DocumentException();
			}

			String strSQL = Document.sql_bundle.getString("getAllCategories");
			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("CategoryId"));
				vecTemp.add(rs.getString("description"));
				vecTemp.add(rs.getString("Descripcion"));
				vecCategories.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Document.cat.error("Error during Document getCategories", e);
			throw new DocumentException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Document.cat.error("Error during closeConnection", e);
			}
		}

		return vecCategories;
	}
	
	
   public Vector searchDocumentByCategory(SessionData sessionData, ServletContext context,String category) throws DocumentException {
	   
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    java.sql.CallableStatement cs = null;

    Vector vecDocuments = new Vector();
    ResultSet rs = null;
    Vector vecCounts = new Vector();
    
    int intRecordCount = 0;
    try
    {
      dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new DocumentException();
      }
      String strSQL ="";
      if(category !=null && category.equals("")){
    	  strSQL = "exec GetDocumentInfo " + sessionData.getProperty("ref_id") +","+context.getAttribute("iso_default");  
      }
      else if(category !=null && !category.equals("")){
    	  strSQL = "exec GetDocumentInfo " + sessionData.getProperty("ref_id") +","+context.getAttribute("iso_default")+","+"D,"+category;
      }

      
      cs = dbConn.prepareCall(strSQL);//Preparamos el CallableStatement
      //En esta linea registramos a nuestro parámetro de
      //salida con cs.registerOutParameter(1,Types.VARCHAR);
      rs=cs.executeQuery();
      
      while(rs.next()){
    	
    	  Vector vecTemp = new Vector();
    	  if(category !=null && category.equals("")){
        	  vecTemp.add(rs.getString("Categoryid"));
              vecTemp.add(rs.getString("Description"));
              vecTemp.add(rs.getString("Descripcion"));
    	  }
    	  else{
        	  vecTemp.add(rs.getString("documentID"));
              vecTemp.add(rs.getString("Name"));  
              vecTemp.add(rs.getString("Description"));  
              vecTemp.add(rs.getString("documentPath"));  
              vecTemp.add(rs.getString("categoryid"));
              vecTemp.add(rs.getString("category"));
    	  }

          
          vecDocuments.add(vecTemp);
      }

    }
    catch (Exception e)
    {
      cat.error("Error during searchDocumentByCategory", e);
      throw new DocumentException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    return vecDocuments;
   }
   
   
   public Vector searchDocument(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context)
      throws DocumentException
  {
    Connection dbConn = null;
    PreparedStatement pstmt = null;

    Vector vecDocuments = new Vector();
    ResultSet rs = null;
    Vector vecCounts = new Vector();

    int intRecordCount = 0;
    try
    {
      dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new DocumentException();
      }
      
  	  String strSQL = Document.sql_bundle.getString("getDocumentFilter");
      String strSQLWhere = "where dr.approvalStatus not in (3) ";
      String strAccessLevel = sessionData.getProperty("access_level");
      String strRefId = sessionData.getProperty("ref_id");
      String strDistChainType = sessionData.getProperty("dist_chain_type");
      
      String iso_default = context.getAttribute("iso_default").toString();
      
     if(!strRefId.equals(iso_default)){
    	 strSQLWhere+= " and rp.rep_id = '" + strRefId + "' " ;
     }
      
      if (criteria != null && !criteria.equals("")){
    	  strSQLWhere+= " and dr.Name like "+"'"+this.criteria +"%'"+ " ";
      }
      if(category !=null &&  !category.equals("") && !category.equals("-1"))
      {
    	  strSQLWhere+= " and dr.CategoryId="+"'"+this.category +"'"+ " ";
      }
      if(businessName !=null &&  !businessName.equals(""))
      {
    	  strSQLWhere+= " and rp.businessName like "+"'"+this.businessName +"%'"+ " ";
      }	  
      if(rep_id_find !=null &&  !rep_id_find.equals(""))
      {
    	  strSQLWhere+= " and dr.repId like "+"'"+this.rep_id_find +"%'"+ " ";
      }	
      
      if(documentId!=0){
    	  strSQLWhere+= " and dr.documentId = "+this.documentId +" ";
      }
      
      
    	  
  	  
      String drep =  sessionData.getProperty("drep");
      if (!(sessionData.checkPermission(DebisysConstants.PERM_TO_DOCUMENT_MANAGEMENT) || (drep !=null && drep.equals("ok")))){

	      if (strAccessLevel.equals(DebisysConstants.ISO))
	      {
	        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
	        {
	          strSQLWhere = strSQLWhere + " and rp.iso_id = " + strRefId ;
	        }
	        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
	        {
	          strSQLWhere = strSQLWhere + " and rp.rep_id in " +
	              "(select rep_id from reps (nolock) where type=" +DebisysConstants.REP_TYPE_REP+ " and iso_id in " +
	              "(select rep_id from reps (nolock) where type=" +DebisysConstants.REP_TYPE_SUBAGENT+ " and iso_id in " +
	              "(select rep_id from reps (nolock) where type=" +DebisysConstants.REP_TYPE_AGENT+ " and iso_id= " + strRefId + ")))";
	        }
	      }
	      else if (strAccessLevel.equals(DebisysConstants.AGENT))
	      {
	        strSQLWhere = strSQLWhere + " and rp.rep_id in " +
	            "(select rep_id from reps (nolock) where type=" +DebisysConstants.REP_TYPE_REP+ " and iso_id in " +
	             "(select rep_id from reps (nolock) where type=" +DebisysConstants.REP_TYPE_SUBAGENT+ " and iso_id = " + strRefId + "))";
	      }
	      else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
	      {
	        strSQLWhere = strSQLWhere + " and rp.rep_id in " +
	            "(select rep_id from reps (nolock) where type=" +DebisysConstants.REP_TYPE_REP+ " and iso_id=" + strRefId + ")";
	      }
	      else if (strAccessLevel.equals(DebisysConstants.REP))
	      {
	        strSQLWhere = strSQLWhere + " and rp.rep_id = " + strRefId;
	      }
      }
     
      //begin actual query for results
      int intCol = 0;
      int intSort = 0;
      String strCol = "";
      String strSort = "";

      try
      {
        if (col != null && sort != null && !col.equals("") && !sort.equals(""))
        {
          intCol = Integer.parseInt(col);
          intSort = Integer.parseInt(sort);
        }
      }
      catch (NumberFormatException nfe)
      {
        intCol = 0;
        intSort = 0;
      }

      if (intSort == 2)
      {
        strSort = "DESC";
      }
      else
      {
        strSort = "ASC";
      }

      switch (intCol)
      {
        case 1:
          strCol = "dr.repId";
          break;
        case 2:
          strCol = "rp.businessname";
          break;
        case 3:
          strCol = "dc.description";
          break;
        case 4:
          strCol = "dr.name";
          break;
        case 5:
          strCol = "dr.approvalStatus";
          break;
        case 6:
          strCol = "dr.description";
          break;
        case 7:
          strCol = "dr.userID";
          break;
        //case 8:
          //  strCol = "Routes.RouteName";
           // break;          
        default:
          strCol = "dr.repId";
          break;
      }

      
      cat.debug(strSQL + strSQLWhere + " order by " + strCol + " " + strSort);
     
      pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by " +strCol + " " + strSort, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
      rs = pstmt.executeQuery();
      intRecordCount = DbUtil.getRecordCount(rs);
      vecCounts.add(Integer.valueOf(intRecordCount));

      //first row is always the counts of
      //total, active, and record counts
      vecDocuments.add(vecCounts);

      if (intRecordCount > 0)
      {
	rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
	

        for (int i=0;i<intRecordsPerPage;i++)
        {
          Vector vecTemp = new Vector();
          vecTemp.add(rs.getLong("documentId"));
          vecTemp.add(StringUtil.toString(rs.getString("repId")));
           vecTemp.add(StringUtil.toString(rs.getString("Name")));
          vecTemp.add(StringUtil.toString(rs.getString("description")));
          vecTemp.add(StringUtil.toString(rs.getString("userID")));
          vecTemp.add(StringUtil.toString(rs.getString("lastUpdate")));
          vecTemp.add(StringUtil.toString(rs.getString("email")));
          vecTemp.add(StringUtil.toString(Boolean.toString(rs.getBoolean("visible"))));
          vecTemp.add(StringUtil.toString(rs.getString("documentPath")));
          vecTemp.add(StringUtil.toString(rs.getString("CategoryId")));
          vecTemp.add(StringUtil.toString(rs.getString("uploadDate")));
          vecTemp.add(StringUtil.toString(rs.getString("businessname")));
          vecTemp.add(StringUtil.toString(rs.getString("description")));
          vecTemp.add(StringUtil.toString(rs.getString("approvalStatus")));
          vecTemp.add(StringUtil.toString(rs.getString("desccategory")));
          vecDocuments.add(vecTemp);
          //This block replaces the below one because it causes a warning because of an inefficent condition
          //Also because calling isLast represents a performance cost in the engine because of data fetching
          if ( !rs.next() )
          {
          	break;
          }
         }
      }
    }
    catch (Exception e)
    {
      cat.error("Error during searchDocument", e);
      throw new DocumentException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    return vecDocuments;
  }

	
	public void updateDocumentApprovalStatus(SessionData sessionData, ServletContext context, Long strDocumentId, String action, String reason) {
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		cat.info("updateDocumentApprovalStatus " + action + " for " + strDocumentId);
		try
		{
			dbConn = TorqueHelper.getConnection(Document.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Document.cat.error("Can't get database connection");
				throw new Exception();
			}
			LogChanges logChanges = new LogChanges(sessionData);
			logChanges.setContext(context);
			if (action.equals("approved"))
			{// Cancelled
				pstmt = dbConn.prepareStatement(Document.sql_bundle.getString("updateDocumentApprovalStatus"));
				pstmt.setInt(1, 1);
				pstmt.setString(2, reason);
				String newdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date());
				pstmt.setString(3, null);
				pstmt.setString(4, newdate);
				pstmt.setLong(5, strDocumentId);
			}
			else if(action.equals("denied"))
			{// Restored
				pstmt = dbConn.prepareStatement(Document.sql_bundle.getString("updateDocumentApprovalStatus"));
				pstmt.setInt(1, 0);
				pstmt.setString(2, reason);
				String newdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date());
				pstmt.setString(3, newdate);
				pstmt.setString(4, newdate);
				pstmt.setLong(5, strDocumentId);
			}
			else if(action.equals("delete"))
			{// Restored
				pstmt = dbConn.prepareStatement(Document.sql_bundle.getString("updateDocumentApprovalStatus"));
				pstmt.setInt(1, 3);
				pstmt.setString(2, reason);
				String newdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date());
				pstmt.setString(3, newdate);
				pstmt.setString(4, newdate);
				pstmt.setLong(5, strDocumentId);
			}
			pstmt.executeUpdate();
		}
		catch (Exception e)
		{
			Document.cat.error("Error during updateDocumentApprovalStatus", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Document.cat.error("Error during release connection", e);
			}
		}
	}
	
	public void updateDocumentVisible(SessionData sessionData, ServletContext context, Long strDocumentId) {
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		cat.info("updateDocumentVisible change visibility for " + strDocumentId);
		try
		{
			dbConn = TorqueHelper.getConnection(Document.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Document.cat.error("Can't get database connection");
				throw new Exception();
			}
				pstmt = dbConn.prepareStatement(Document.sql_bundle.getString("updateDocumentVisible"));
				pstmt.setBoolean(1, false);
				pstmt.setLong(2, strDocumentId);
	
			pstmt.executeUpdate();
		}
		catch (Exception e)
		{
			Document.cat.error("Error during updateDocumentVisible", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Document.cat.error("Error during release connection", e);
			}
		}
	}
	
	public String returnMailDocumentID(SessionData sessionData, ServletContext context, Long strDocumentId){
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    String mail="";
		try
		{
			dbConn = TorqueHelper.getConnection(Document.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Document.cat.error("Can't get database connection");
				throw new Exception();
			}
		
			String strSQL =""+ sql_bundle.getString("getMailDocumentId");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, documentId);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				mail= rs.getString("email");
			}
	
		}
		catch (Exception e)
		{
			Document.cat.error("Error during returnMailDocumentID", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Document.cat.error("Error during release connection", e);
			}
		}		
		
		
		return mail;
	}
	
	public String returnNameDocumentID(SessionData sessionData, ServletContext context, Long strDocumentId){
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    String name="";
		try
		{
			dbConn = TorqueHelper.getConnection(Document.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Document.cat.error("Can't get database connection");
				throw new Exception();
			}
		
			String strSQL =""+ sql_bundle.getString("getNameDocumentId");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, documentId);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				name= rs.getString("name");
			}
	
		}
		catch (Exception e)
		{
			Document.cat.error("Error during returnNameDocumentID", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Document.cat.error("Error during release connection", e);
			}
		}		
		
		
		return name;
	}
	
	
	public String searchPathDocument(Long strDocumentId){
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    String path="";
		try
		{
			dbConn = TorqueHelper.getConnection(Document.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Document.cat.error("Can't get database connection");
				throw new Exception();
			}
		
			String strSQL =""+ sql_bundle.getString("getPathDocument");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, strDocumentId);
			
			rs = pstmt.executeQuery();
			if (rs.next()) {
				path= rs.getString("documentPath");
			}
	
		}
		catch (Exception e)
		{
			Document.cat.error("Error during searchPathDocument", e);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Document.cat.error("Error during release connection", e);
			}
		}		
		
		return path;
	}
	
	public static String getSetupMail()
	{
		String strSetupMail = "";
		/*
		 * try { strSetupMail = sql_bundle.getString("setupMail"); } catch(NumberFormatException
		 * nfe) { strSetupMail = "setups@emida.net"; }
		 */

		Connection dbConn = null;
		PreparedStatement pstmtLogRateChanges = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			// dbConn = Torque.getConnection(sql_bundle.getString("setupMail"));
			pstmtLogRateChanges = dbConn.prepareStatement(sql_bundle.getString("setupMailDocumentRepository"));
			ResultSet rs = pstmtLogRateChanges.executeQuery();
			if (rs.next())
				strSetupMail = rs.getString(1);
			else
				cat.error("Error during getSetupMail Document Repository:no email set up ");
			rs.close();
			pstmtLogRateChanges.close();
		}
		catch (Exception localException)
		{
			// If no log record could be saved, the failure should not affect the operation
			cat.error("Error during getSetupMail Document Repository call : ", localException);
		}
		finally
		{
			try
			{
				if (pstmtLogRateChanges != null)
					pstmtLogRateChanges.close();
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
		
		return strSetupMail;
	}
	
}

