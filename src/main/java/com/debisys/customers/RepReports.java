package com.debisys.customers;

import java.util.ArrayList;

import javax.servlet.ServletContext;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;


public class RepReports {

	
	
	/**
	 * @param sessionData
	 * @param application
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersRepCreditsAssigmentHistory(SessionData sessionData,ServletContext application)
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add(new ColumnReport("datetime", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.date", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.user", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("legal_businessname", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.merchant_businessname", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.merchant_id", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("description", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.desc", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("amount", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.amount", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("available_credit", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.new_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("prior_available_credit", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.prior_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		return headers;
	}
	
	/**
	 * @param sessionData
	 * @param application
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersSubAgentRepCreditsAssigmentHistory(SessionData sessionData,ServletContext application)
	{		
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add(new ColumnReport("datetime", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.date", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.user", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("rep_id", Languages.getString("jsp.admin.customers.subagents_rep_credit_history.rep_id", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("description", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.desc", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("amount", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.amount", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("available_credit", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.new_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("prior_available_credit", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.prior_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		return headers;
	}
	
	/**
	 * @param sessionData
	 * @param application
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersAgentRepCreditsAssigmentHistory(SessionData sessionData,ServletContext application)
	{		
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add(new ColumnReport("datetime", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.date", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.user", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("subagent_id", Languages.getString("jsp.admin.customers.agents_subagent_credit_history.subagent_id", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("description", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.desc", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("amount", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.amount", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("available_credit", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.new_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("prior_available_credit", Languages.getString("jsp.admin.customers.reps_merchant_credit_history.prior_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		return headers;
	}
	
	
	/**
	 * @param sessionData
	 * @param application
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersRepCreditsHistory(SessionData sessionData,ServletContext application)
	{		
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add(new ColumnReport("datetime", Languages.getString("jsp.admin.customers.reps_credit_history.date", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.customers.reps_credit_history.user", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("description", Languages.getString("jsp.admin.customers.reps_credit_history.desc", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("amount", Languages.getString("jsp.admin.customers.reps_credit_history.payment", sessionData.getLanguage()).toUpperCase(), Double.class, false) );
		headers.add(new ColumnReport("available_credit", Languages.getString("jsp.admin.customers.reps_credit_history.new_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("commission", Languages.getString("jsp.admin.customers.reps_credit_history.commission", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("net_payment", Languages.getString("jsp.admin.customers.reps_credit_history.net_payment", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		return headers;
	}
	
	
	
	
	/**
	 * @param sessionData
	 * @param application
	 * @param bIsExternalRep
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersMerchantsCreditsHistory(SessionData sessionData,ServletContext application, boolean bIsExternalRep)
	{		
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();		
		headers.add(new ColumnReport("log_datetime", Languages.getString("jsp.admin.customers.merchants_credit_history.date", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.customers.merchants_credit_history.user", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.user.dba", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add(new ColumnReport("description", Languages.getString("jsp.admin.customers.merchants_credit_history.desc", sessionData.getLanguage()).toUpperCase(), Double.class, false) );
		headers.add(new ColumnReport("payment", Languages.getString("jsp.admin.customers.merchants_credit_history.payment", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("balance", Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("commission", Languages.getString("jsp.admin.customers.merchants_credit_history.commission", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		headers.add(new ColumnReport("net_payment", Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		if(bIsExternalRep) 
        {   
        	headers.add(new ColumnReport("ExternalDBA", Languages.getString("jsp.admin.reports.extern.RepresentativeType", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
		return headers;
	}
	
	
	/**
	 * @param sessionData
	 * @param application
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersTrxSummaryByAmount(SessionData sessionData,ServletContext application)
	{		
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add( new ColumnReport( "description", "", String.class, false) );
		headers.add(new ColumnReport( "currentPeriod", "", String.class, false) );
		headers.add(new ColumnReport( "previousPeriod", "", String.class, false) );
		headers.add(new ColumnReport( "change", "", String.class, false) );				
		return headers;
	}
	
	
	/**
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersPinStock()
	{		
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add( new ColumnReport( "desc", "", String.class, false) );						
		return headers;
	}
	
	
	/**
	 * @param sessionData
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersAgentCreditHistory(SessionData sessionData)
	{	
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add( new ColumnReport( "datetime", Languages.getString("jsp.admin.customers.agents_credit_history.date", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "logon_id", Languages.getString("jsp.admin.customers.agents_credit_history.user", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "description", Languages.getString("jsp.admin.customers.agents_credit_history.desc", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "amount", Languages.getString("jsp.admin.customers.agents_credit_history.payment", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "available_credit", Languages.getString("jsp.admin.customers.agents_credit_history.new_avail_credit", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "commission", Languages.getString("jsp.admin.customers.agents_credit_history.commission", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "net_payment", Languages.getString("jsp.admin.customers.agents_credit_history.net_payment", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		
		return headers;
	}
	
	
	/**
	 * @param sessionData
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersSubAgentCreditHistory(SessionData sessionData)
	{			
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add( new ColumnReport( "datetime", Languages.getString("jsp.admin.customers.subagents_credit_history.date", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "logon_id", Languages.getString("jsp.admin.customers.subagents_credit_history.user", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "description", Languages.getString("jsp.admin.customers.subagents_credit_history.desc", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "amount", Languages.getString("jsp.admin.customers.subagents_credit_history.payment", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "available_credit", Languages.getString("jsp.admin.customers.subagents_credit_history.new_avail_credit", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "commission", Languages.getString("jsp.admin.customers.subagents_credit_history.commission", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		headers.add( new ColumnReport( "net_payment", Languages.getString("jsp.admin.customers.subagents_credit_history.net_payment", sessionData.getLanguage()).toUpperCase(), String.class, false) );
		
		return headers;
	}
	
	
	
	public static ArrayList<ColumnReport> getHeadersSummaryReportByPeriods()
	{		
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		headers.add( new ColumnReport( "description", "", String.class, false) );
		headers.add(new ColumnReport( "currentPeriod", "", String.class, false) );
		headers.add(new ColumnReport( "previousPeriod", "", String.class, false) );
		headers.add(new ColumnReport( "change", "", String.class, false) );				
		return headers;
	}
	
		
}
