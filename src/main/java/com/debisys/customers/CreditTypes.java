package com.debisys.customers;

import static com.debisys.utils.DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO;
import static com.debisys.utils.DebisysConstants.DEPLOYMENT_DOMESTIC;
import static com.debisys.utils.DebisysConstants.DEPLOYMENT_INTERNATIONAL;
import static com.debisys.utils.DebisysConstants.MERCHANT_TYPE_CREDIT;
import static com.debisys.utils.DebisysConstants.MERCHANT_TYPE_PREPAID;
import static com.debisys.utils.DebisysConstants.MERCHANT_TYPE_SHARED;
import static com.debisys.utils.DebisysConstants.MERCHANT_TYPE_UNLIMITED;
import static com.debisys.utils.DebisysConstants.REP_CREDIT_TYPE_CREDIT;
import static com.debisys.utils.DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE;
import static com.debisys.utils.DebisysConstants.REP_CREDIT_TYPE_PREPAID;
import static com.debisys.utils.DebisysConstants.REP_CREDIT_TYPE_UNLIMITED;
import static com.debisys.utils.DebisysConstants.REP_TYPE_AGENT;
import static com.debisys.utils.DebisysConstants.REP_TYPE_SUBAGENT;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Central point with the credit types logic for support site add/edit actor
 * forms
 * 
 * @author rbuitrago@emida.net
 * 
 */
public class CreditTypes {
	
	private CreditTypes() {}
	
	/**
	 * General purpose function to obtain the Map with options/selected status
	 * for any actor
	 * 
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param isoCredType
	 * @param customConfigType
	 * @param deploymentType
	 * @param selectedOptions
	 * @return a map having the actual option number as key and a selected
	 *         status for the combo option as received as a parameter in the
	 *         selectedOptions array. For the case of Flexible, no selected
	 *         status is returned (blank string), as it is returned alone in
	 *         the map.
	 */
	public static Hashtable<String, String> getAvailableOptionsForAgent(
			String isoCredType, String customConfigType, String deploymentType, String[] selectedOptions) {
		Hashtable<String, String> options = new Hashtable<String, String>();
		if (deploymentType.equals(DEPLOYMENT_DOMESTIC)) {
                    options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
                    options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
		} else {
			if (!customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO)
					&& isoCredType.equals(REP_CREDIT_TYPE_FLEXIBLE)) {
				options.put(REP_CREDIT_TYPE_FLEXIBLE, "");
			} else {
				options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
				options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
				options.put(REP_CREDIT_TYPE_UNLIMITED, selectedOptions[2]);
			}
		}
		return options;
	}

	public static Hashtable<String, String> getAvailableOptionsForSubAgent(
			String isoCredType, String customConfigType, String deploymentType,
			String[] selectedOptions) {
		Hashtable<String, String> options = new Hashtable<String, String>();
		if (deploymentType.equals(DEPLOYMENT_DOMESTIC)) {
                    options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
                    options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
		} else {
			if (!customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO)
					&& isoCredType.equals(REP_CREDIT_TYPE_FLEXIBLE)) {
				options.put(REP_CREDIT_TYPE_FLEXIBLE, "");
			} else {
				options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
				options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
				options.put(REP_CREDIT_TYPE_UNLIMITED, selectedOptions[2]);
			}
		}
		return options;
	}

        
    public static Map<String, String> getAvailableOptionsForRep(
            String isoCredType, String customConfigType, String deploymentType,
            String[] selectedOptions, boolean isEditEntity) {
        Map<String, String> options = new HashMap<String, String>();
        if (deploymentType.equals(DEPLOYMENT_DOMESTIC)) {           
            if (isEditEntity){
                options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
                options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
            }
        } else {
            if (!customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO)
                    && isoCredType.equals(REP_CREDIT_TYPE_FLEXIBLE)) {
                options.put(REP_CREDIT_TYPE_FLEXIBLE, "");
            } else {
                options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
                options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
                options.put(REP_CREDIT_TYPE_UNLIMITED, selectedOptions[2]);
                if (customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO)) {
                    // TODO something about shared?
                }
            }
        }
        return options;
    }

	public static Hashtable<String, String> getAvailableOptionsForMerchant(
			String isoCredType, String customConfigType, String deploymentType,
			String[] selectedOptions) {
		Hashtable<String, String> options = new Hashtable<String, String>();
		if (deploymentType.equals(DEPLOYMENT_DOMESTIC)) {
                    options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
                    options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
		} else {
			if (!customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO)
					&& isoCredType.equals(REP_CREDIT_TYPE_FLEXIBLE)) {
				options.put(REP_CREDIT_TYPE_PREPAID, "");
			} else {
				options.put(REP_CREDIT_TYPE_PREPAID, selectedOptions[0]);
				options.put(REP_CREDIT_TYPE_CREDIT, selectedOptions[1]);
				if (!customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO)) {
					options.put(REP_CREDIT_TYPE_UNLIMITED, selectedOptions[2]);
				}
			}
		}
		return options;
	}
	
	public static List<Boolean> getFormOptionsMerchant(
			String customConfigType, String deploymentType,
			String merchantType, String isoCredType) {

		List<Boolean> options = new ArrayList<Boolean>();
		boolean showApplyPaymentForm = false;
		boolean showPaymentOptions = false;
		boolean showUpdateCreditOption = false;
		boolean showAdditionalPaymentOptions = false;
		boolean disableCreditTypeControls = false;
		boolean hideFormTermSalesResetLimit = false;
		boolean hideBtnChangeType = false;
		boolean showInstructions1 = false;
		boolean hideCreditLimitBox = false;
		
		// we shouldn't show apply payment option if merchant is SHARED; 
		// the Rep will do that task
		boolean selectedCredTypes = merchantType.equals(MERCHANT_TYPE_CREDIT)
			|| merchantType.equals(MERCHANT_TYPE_PREPAID);
		
		boolean selectedConfigs = customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO) 
			|| (deploymentType.equals(DEPLOYMENT_DOMESTIC)) 
			|| deploymentType.equals(DEPLOYMENT_INTERNATIONAL);
		
		// 1st for showin/hidin the ApplyPayment form; 
		if (selectedCredTypes && selectedConfigs) {
			showApplyPaymentForm = true;			
		}
		
		// 2nd for showin/hidin the Banking Info
		if (selectedConfigs) { //same as above
			showPaymentOptions = true;
			selectedConfigs = customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO) 
				|| (deploymentType.equals(DEPLOYMENT_DOMESTIC));
			// 4th for showin basic or additional options in Apply Payment Form
			if (showPaymentOptions && selectedConfigs) {
				showAdditionalPaymentOptions = true;
			}
		}
		
		// 3rd for showin/hidin the Update Credit option.
		selectedCredTypes = merchantType.equals(MERCHANT_TYPE_CREDIT);
		if (selectedCredTypes) {
			showUpdateCreditOption = true;
		}
		
		// 5th 
		// disable btnChangeType when you're a merch under US+PREPAID or INTL+FLEX		
		boolean mex_shared = customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO) 
			&& merchantType.equals(MERCHANT_TYPE_SHARED);
		boolean intl_flex = deploymentType.equals(DEPLOYMENT_INTERNATIONAL) 
			&& !customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO) 
			&& isoCredType.equals(REP_CREDIT_TYPE_FLEXIBLE);
		boolean us_prepaid = deploymentType.equals(DEPLOYMENT_DOMESTIC);
		if (us_prepaid || intl_flex) {
			disableCreditTypeControls = true;
		}
		
		// 6th
		// hide the line with Terminal Sales and reset limit button when MEX + SHARED or UNLIMITED
		selectedCredTypes = merchantType.equals(MERCHANT_TYPE_UNLIMITED);
		if (mex_shared || selectedCredTypes) {
			hideFormTermSalesResetLimit = true;
		}
		
		// 7th 
		// hide btnChangeType if MEX+SHARED
		if (mex_shared) {
			hideBtnChangeType = true;
		}
		
		// 8th
		// show instructions for credit type 
		selectedConfigs = 
			(deploymentType.equals(DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO)) 
			|| (deploymentType.equals(DEPLOYMENT_DOMESTIC))
			|| deploymentType.equals(DEPLOYMENT_DOMESTIC);
		boolean notSharedMerchant = !merchantType.equals(MERCHANT_TYPE_SHARED);
		boolean notUnlimitedMerchant = !merchantType.equals(MERCHANT_TYPE_UNLIMITED);
		
		if (selectedConfigs && notSharedMerchant && notUnlimitedMerchant) {
			showInstructions1 = true;
		}
		
		// 9th hide credit limit box  (only for unlimited)
		if (!notUnlimitedMerchant) {
			hideCreditLimitBox = true;
		}
		
		options.add(0, showApplyPaymentForm);
		options.add(1, showPaymentOptions);
		options.add(2, showUpdateCreditOption);
		options.add(3, showAdditionalPaymentOptions);
		options.add(4, disableCreditTypeControls);
		options.add(5, hideFormTermSalesResetLimit);
		options.add(6, hideBtnChangeType);
		options.add(7, showInstructions1);
		options.add(8, hideCreditLimitBox);
		
		return options;
	}
	
	public static String defaultCreditType( 
			String deploymentType, String repType) {
		String checkedCreditType = repType;
		final boolean invalidCreditType = 
			checkedCreditType == null || checkedCreditType.equals(""); 
		if (deploymentType.equals(DEPLOYMENT_DOMESTIC) && invalidCreditType) {
                    checkedCreditType = REP_CREDIT_TYPE_CREDIT;
		}
		return checkedCreditType;
	}
	
	public static List<Boolean> getFormOptionsReps(
			String customConfigType, String deploymentType, 
                        String repType, String isoCreditType, 
                        String[] selectedOptions, String entityLevel) {

		List<Boolean> options = new ArrayList<Boolean>();
		boolean showApplyPaymentForm = false;
		boolean showPaymentOptions = false;
		boolean showUpdateCreditOption = false;		
		boolean disableApplyPaymentButton = false;
		boolean disableCreditTypeCombo = false;
		boolean hideCreditLimitForm = false;
		
		boolean selectedCredTypes = repType.equals(REP_CREDIT_TYPE_CREDIT)
			|| repType.equals(REP_CREDIT_TYPE_PREPAID)
			|| repType.equals(REP_CREDIT_TYPE_FLEXIBLE);
		
		boolean selectedConfigs = customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO) 
			|| deploymentType.equals(DEPLOYMENT_DOMESTIC) 
			|| deploymentType.equals(DEPLOYMENT_INTERNATIONAL);
		
		// 1st for showin/hidin the ApplyPayment form; 
		if (selectedCredTypes && selectedConfigs) {
			showApplyPaymentForm = true;
		}
		
		// 2nd for showin/hidin the payment options 
		if (selectedConfigs) { 
			showPaymentOptions = true;
			selectedConfigs = customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO);
			// 4th for showin basic or additional options in Apply Payment Form
			if (showPaymentOptions && selectedConfigs) {
				disableApplyPaymentButton = true;
			}
		}
		
		// 3rd for showin/hidin the Update Credit option.
		selectedCredTypes = repType.equals(REP_CREDIT_TYPE_CREDIT);
		if (selectedCredTypes) {
			showUpdateCreditOption = true;
		}
		
		// 5th managing both cases when the combo should be disabled: 
		// case 1: when in mexico and credType Unlimited for Agent and Subagent
		boolean disabledCase1 = customConfigType.equals(CUSTOM_CONFIG_TYPE_MEXICO) 
			&& selectedOptions[2].equals(" selected") 
			&& (entityLevel.equals(REP_TYPE_AGENT) || entityLevel.equals(REP_TYPE_SUBAGENT));
		// case 2: when in international and iso credit type is Flex
		boolean disabledCase2 = isoCreditType.equals(REP_CREDIT_TYPE_FLEXIBLE);
		
		if (disabledCase1 || disabledCase2) {
			disableCreditTypeCombo = true;
		}
		
		// 6th for hiding CreditLimit form
		if (selectedOptions[2].equals(" selected")) {
			hideCreditLimitForm = true;
		}
		
		options.add(0, showApplyPaymentForm);
		options.add(1, showPaymentOptions);
		options.add(2, showUpdateCreditOption);
		options.add(3, disableApplyPaymentButton);
		options.add(4, disableCreditTypeCombo);
		options.add(5, hideCreditLimitForm);
		return options;
	}
}