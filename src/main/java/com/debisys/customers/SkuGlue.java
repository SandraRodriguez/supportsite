package com.debisys.customers;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.log4j.Category;

import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.Log;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;
public class SkuGlue {

	  // log4j logging
	  static Category               cat                 = Category.getInstance(Merchant.class);
	  private static ResourceBundle sql_bundle          = ResourceBundle.getBundle("com.debisys.customers.sql");
	  
	  private String                repId;
	  private String                siteId;
	  private String                merchantId;
	  private String                productId;
	  private int                   merchantRouteId;
	  private String                merchantRouteName;
	  private int 				    status;
	  
	  public String getSiteId()
	  {
	    return StringUtil.toString(this.siteId);
	  }

	  public void setSiteId(String strValue)
	  {
	    this.siteId = strValue;
	  }

		public String getMerchantId() {
			return StringUtil.toString(this.merchantId);
		}
	
		public void setMerchantId(String strValue) {
			this.merchantId = strValue;
		}
	
		public String getProductId() {
			return StringUtil.toString(this.productId);
		}
	
		public void setProductId(String strValue) {
			this.productId = strValue;
		}
	
		public int getMerchantRouteId() {
			return merchantRouteId;
		}
	
		public void setMerchantRouteId(int merchantRouteId) {
			this.merchantRouteId = merchantRouteId;
		}
	
		public String getMerchantRouteName() {
			return StringUtil.toString(this.merchantRouteName);
		}
	
		public void setMerchantRouteName(String strValue) {
			this.merchantRouteName = strValue;
		}
	
		public int getStatus() {
			return status;
		}
	
		public void setStatus(int status) {
			this.status = status;
		}
	  
	  
	public void insertProduct(SessionData sessionData, String strMerchantId, String strTemp,String notes)
	
	 {
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    
	
	    try
	    {
	      dbConn = TorqueHelper.getConnection(SkuGlue.sql_bundle.getString("pkgDefaultDb"));
	
	      if (dbConn == null)
	      {
	        Merchant.cat.error("Can't get database connection");
	        throw new Exception();
	      }
	
	      pstmt = dbConn.prepareStatement(SkuGlue.sql_bundle.getString("insertProductSKUWhiteList"));
	      pstmt.setInt(1,Integer.parseInt(strTemp));
	      pstmt.setLong(2,Long.parseLong(strMerchantId));
	      pstmt.setString(3, sessionData.getUser().getUsername());
	      pstmt.setTimestamp(4, new java.sql.Timestamp(new java.util.Date().getTime()));
	      pstmt.setTimestamp(5, new java.sql.Timestamp(new java.util.Date().getTime()));
	      pstmt.setLong(6,2); // insert product in pending -- boost phase 2
	      
	      pstmt.execute();
	      
	      pstmt = dbConn.prepareStatement(SkuGlue.sql_bundle.getString("insertMerchantNoteProductStatus"));
	      pstmt.setLong(1,Long.parseLong(strMerchantId));
	      pstmt.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
	      pstmt.setString(3, "SUP");
	      pstmt.setString(4, notes);
	      pstmt.setString(5, sessionData.getUser().getUsername());
	      
	      pstmt.execute();
	      
	     
	    }
	    catch (Exception e)
	    {
	      Merchant.cat.error("Error during updateMerchantCancelledStatus", e);
	    }
	    finally
	    {
	      try
	      {
	        TorqueHelper.closeConnection(dbConn, pstmt, null);
	      }
	      catch (Exception e)
	      {
	        Merchant.cat.error("Error during release connection", e);
	      }
	    }
		
	}
		
	  public void updateMerchantProductStatus(SessionData sessionData, String strMerchantId, String strTemp, String chk, String notes)
	  {
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    
	
	    try
	    {
	      dbConn = TorqueHelper.getConnection(SkuGlue.sql_bundle.getString("pkgDefaultDb"));
	
	      if (dbConn == null)
	      {
	        Merchant.cat.error("Can't get database connection");
	        throw new Exception();
	      }
	
	      pstmt = dbConn.prepareStatement(SkuGlue.sql_bundle.getString("updateMerchantProductStatus"));
	      pstmt.setInt(1,Integer.parseInt(chk));
	      pstmt.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
	      pstmt.setLong(3,Long.parseLong(strMerchantId));
	      pstmt.setInt(4,Integer.parseInt(strTemp));

	      pstmt.executeUpdate();
	      
	      pstmt = dbConn.prepareStatement(SkuGlue.sql_bundle.getString("insertMerchantNoteProductStatus"));
	      pstmt.setLong(1,Long.parseLong(strMerchantId));
	      pstmt.setTimestamp(2, new java.sql.Timestamp(new java.util.Date().getTime()));
	      pstmt.setString(3, "SUP");
	      pstmt.setString(4, notes);
	      pstmt.setString(5, sessionData.getUser().getUsername());
	      
	      pstmt.execute();
		 
	      
	    }
	    catch (Exception e)
	    {
	      Merchant.cat.error("Error during updateMerchantCancelledStatus", e);
	    }
	    finally
	    {
	      try
	      {
	        TorqueHelper.closeConnection(dbConn, pstmt, null);
	      }
	      catch (Exception e)
	      {
	        Merchant.cat.error("Error during release connection", e);
	      }
	    }
	  }
	  
	  public void updateTerminalRatesSkuGlue(SessionData sessionData, String strMerchantId, String strTemp, int disabled, String notes)
	  {
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    
	
	    try
	    {
	      dbConn = TorqueHelper.getConnection(SkuGlue.sql_bundle.getString("pkgDefaultDb"));
	
	      if (dbConn == null)
	      {
	        Merchant.cat.error("Can't get database connection");
	        throw new Exception();
	      }
	
	      pstmt = dbConn.prepareStatement(SkuGlue.sql_bundle.getString("updateSkuGlueTerminalRates"));
	           
	      pstmt.setInt(1,disabled);
	      pstmt.setLong(2,Long.parseLong(strMerchantId));
	      pstmt.setInt(3,Integer.parseInt(strTemp));
	      pstmt.setInt(4,Integer.parseInt(strTemp));
	
	      pstmt.executeUpdate();
	
	    }
	    catch (Exception e)
	    {
	      Merchant.cat.error("Error during updateMerchantCancelledStatus", e);
	    }
	    finally
	    {
	      try
	      {
	        TorqueHelper.closeConnection(dbConn, pstmt, null);
	      }
	      catch (Exception e)
	      {
	        Merchant.cat.error("Error during release connection", e);
	      }
	    }
	  }
		
}
