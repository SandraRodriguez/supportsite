package com.debisys.customers;

//import java.sql.CallableStatement; //Never used
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Logger;
import com.debisys.exceptions.CustomerException;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.JSONObject;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Holds the information for customers.
 * <P>
 * 
 * @author Jay Chi
 */

public class CustomerSearch implements HttpSessionBindingListener {

        public static String PROPERTY_SESSION_SQL_ENTITY = "PROPERTY_SESSION_SQL_ENTITY";
        public static String PROPERTY_SESSION_SQL_ENTITY_IDS = "PROPERTY_SESSION_SQL_ENTITY_IDS";
        
	private String criteria = "";
	private String sort = "";
	private String col = "";
	private String rep_id = "";

	// R27-DBSY-585. added by jacuna
	private String agentIds;
	private String subAgentIds;
	private boolean searchNumNewModelsPlans = false;

	// log4j logging
	private static Logger cat = Logger.getLogger(CustomerSearch.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.customers.sql");

	public String getCriteria() {
		return StringUtil.toString(this.criteria);
	}

	public void setCriteria(String strCriteria) {
		this.criteria = strCriteria.trim();
	}

	public String getCol() {
		return StringUtil.toString(this.col);
	}

	public void setCol(String strCol) {
		this.col = strCol;
	}

	public String getSort() {
		return StringUtil.toString(this.sort);
	}

	public void setSort(String strSort) {
		this.sort = strSort;
	}

	public String getRepId() {
		return StringUtil.toString(this.rep_id);
	}

	public void setRepId(String strValue) {
		// double dblRepId;//Not needed
		try {
			/* dblRepId = */Double.parseDouble(strValue);
		} catch (NumberFormatException nfe) {
			strValue = "";
		}
		this.rep_id = strValue;
	}

	/* BEGIN Release 21 - Added by YH */

	private int merchantRouteId;

	public int getMerchantRouteId() {
		return this.merchantRouteId;
	}

	public void setMerchantRouteId(int merchantRouteId) {
		this.merchantRouteId = merchantRouteId;
	}

	private String merchantRouteName;

	public String getMerchantRouteName() {
		return this.merchantRouteName;
	}

	public void setMerchantRouteName(String merchantRouteName) {
		this.merchantRouteName = merchantRouteName;
	}

	/* END Release 21 - Added by YH */

	/**
	 * @param intPageNumber
	 * @param intRecordsPerPage
	 * @param sessionData
	 * @param context
	 * @param agentIds
	 * @param subAgentIds
	 * @return
	 * @throws CustomerException
	 */
	public Vector<Vector<Object>> listJoinedMerchants(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context)
			throws CustomerException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;

		Vector<Vector<Object>> vecCustomers = new Vector<Vector<Object>>();
		ResultSet rs = null;
		Vector<Object> vecCounts = new Vector<Object>();

		// String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strRefId = sessionData.getProperty("ref_id");
		String strSQL = sql_bundle.getString("getJoinedMerchants");
		// strSQL = "select m.*  "+
		// "from merchants m (nolock) "+
		// "inner join reps r (nolock)  on (m.rep_id = r.rep_id) where ";

		String startDate = sessionData.getProperty("start_date");
		String endDate = sessionData.getProperty("end_date");

		StringBuffer strSQLWhere = new StringBuffer();

		if (strAccessLevel.equals(DebisysConstants.ISO)) {
			strSQLWhere.append("where r.rep_id in ");
			strSQLWhere.append("(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in ");
			strSQLWhere.append("(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT);
			strSQLWhere.append(" 	and iso_id in ");
			strSQLWhere.append("	(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT);

			if (this.agentIds != null && !"".equals(this.agentIds))
				strSQLWhere.append(" and rep_id in (" + this.agentIds + ")"); // the List of agents
			else
				strSQLWhere.append(" and iso_id= " + strRefId);// the default of the ISO
			strSQLWhere.append(" )");

			if (this.subAgentIds != null && !"".equals(this.subAgentIds)) {
				strSQLWhere.append("UNION select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT);
				strSQLWhere.append(" and rep_id in (" + this.subAgentIds + ")"); // the List of subagents
			}
			strSQLWhere.append(")) ");
			strSQLWhere.append(" and m.receipt_date >= '").append(startDate).append(" 00:00:00.000' and m.receipt_date < '")
					.append(DateUtil.addSubtractDays(endDate, 1) + "' ");
			strSQLWhere.append("group by m.merchant_id");
			strSQLWhere.append(",r.rep_id");
			strSQLWhere.append(",m.dba");
			strSQLWhere.append(",m.control_number");
			strSQLWhere.append(",m.contact");
			strSQLWhere.append(",m.contact_phone");
			strSQLWhere.append(",m.email");
			strSQLWhere.append(",m.RouteID, Rt.RouteName");
			strSQLWhere.append(", m.DateActivated");
			strSQLWhere.append(", m.DateCancelled");
			strSQLWhere.append(", m.receipt_date");
			strSQLWhere.append(", m.runningliability");
			strSQLWhere.append(", m.liabilitylimit");
			strSQLWhere.append(", r2.rep_id");
			strSQLWhere.append(", r2.businessname");
			strSQLWhere.append(", r3.rep_id");
			strSQLWhere.append(", r3.businessname");
			strSQLWhere.append(", r4.rep_id");
			strSQLWhere.append(", r4.businessname");
			strSQLWhere.append(" order by merch_id");

		}

		int intRecordCount = 0;
		try {
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new CustomerException();
			}

			pstmt = dbConn.prepareStatement(strSQL + strSQLWhere.toString(), ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery();

			intRecordCount = DbUtil.getRecordCount(rs);
			vecCounts.add(Integer.valueOf(intRecordCount));

			if (!(intRecordsPerPage > 0))
				intRecordsPerPage = intRecordCount;

			// first row is always the counts of
			// total, active, and record counts
			vecCustomers.add(vecCounts);

			if (intRecordCount > 0) {
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));

				for (int i = 0; i < intRecordsPerPage; i++) {
					Vector<Object> vecTemp = new Vector<Object>();
					vecTemp.add(StringUtil.toString(rs.getString("sub_agent")));
					vecTemp.add(StringUtil.toString(rs.getString("rep")));
					vecTemp.add(StringUtil.toString(rs.getString("control_number")));
					vecTemp.add(StringUtil.toString(rs.getString("merch_id")));
					// vecTemp.add(StringUtil.toString(rs.getString("merc_rep_id")));
					vecTemp.add(StringUtil.toString(rs.getString("dba")));
					vecTemp.add(StringUtil.toString(rs.getString("RouteName")));
					vecTemp.add(StringUtil.toString(rs.getString("receipt_date")));
					String status = "";
					if (rs.getDate("DateActivated") == null && rs.getDate("DateCancelled") == null)
						status = "Desactivated";
					if (rs.getDate("DateCancelled") != null)
						status = "Cancelled";
					if (rs.getDate("DateActivated") != null)
						status = "Activated";

					vecTemp.add(status);
					vecTemp.add(new Double(rs.getDouble("TotalPayment")));
					vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("last_transaction_date")));
					vecTemp.add(new Integer(rs.getInt("qty")));
					vecTemp.add(new Double(rs.getDouble("total_recharge")));
					vecTemp.add(new Double(rs.getDouble("available_credit")));
					vecTemp.add(StringUtil.toString(rs.getString("contact")));
					vecTemp.add(StringUtil.toString(rs.getString("contact_phone")));

					// vecTemp.add(DateUtil.formatDateNoTime(rs.getDate("DateActivated")));
					// vecTemp.add(DateUtil.formatDateNoTime(rs.getDate("DateCancelled")));
					vecTemp.add(StringUtil.toString(rs.getString("email")));
					vecCustomers.add(vecTemp);
					if (!rs.next()) {
						break;
					}
				}
			}

		} catch (Exception e) {
			cat.error("Error during listJoinedMerchants", e);
			throw new CustomerException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}

		return vecCustomers;
	}

	/**
	 * Gets list of merchants from the merchants table for the logged in user.
	 * 
	 * @param intPageNumber
	 * @return A vector of results, or null if none found.
	 * @throws CustomerException
	 *             Thrown on database errors
	 */

	public Vector<Object> searchMerchant(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context) throws CustomerException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;

		Vector<Object> vecCustomers = new Vector<Object>();
		ResultSet rs = null;
		Vector<Integer> vecCounts = new Vector<Integer>();

		int intRecordCount = 0;
		try {
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new CustomerException();
			}
			// get a count of total matches
			// m.rep_id = r.rep_id and r.iso_id = ? and m.DateCancelled is null
			String strSQLWhere = "";
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");
			String strDistChainType = sessionData.getProperty("dist_chain_type");

			if (strAccessLevel.equals(DebisysConstants.CARRIER)) {				
				strSQLWhere = "where  ta.repid = " + strRefId;

			} else if (strAccessLevel.equals(DebisysConstants.ISO)) {

				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					strSQLWhere = strSQLWhere + " reps.iso_id = " + strRefId;
				} else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
					strSQLWhere = strSQLWhere + " reps.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_REP
							+ " and iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in "
							+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id= " + strRefId + ")))";
				}
			} else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
				strSQLWhere = strSQLWhere + " reps.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_REP
						+ " and iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = "
						+ strRefId + "))";
			} else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
				strSQLWhere = strSQLWhere + " reps.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_REP
						+ " and iso_id=" + strRefId + ")";
			} else if (strAccessLevel.equals(DebisysConstants.REP)) {
				strSQLWhere = strSQLWhere + " reps.rep_id = " + strRefId;
			}

			if (this.criteria == null || this.criteria.equals("")) {
				if (this.rep_id != null && !this.rep_id.equals("")) {
					strSQLWhere = strSQLWhere + " and reps.rep_id=" + this.rep_id;
				}
				if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
					if (this.merchantRouteId > 0) {
						strSQLWhere = strSQLWhere + " and merchants.RouteID = " + this.merchantRouteId;
					}
				}
			} else {
				if (this.rep_id != null && !this.rep_id.equals("")) {
					strSQLWhere = strSQLWhere + " and reps.rep_id=" + this.rep_id;

				}
				if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
					if (this.merchantRouteId > 0) {
						strSQLWhere = strSQLWhere + " and merchants.RouteID = " + this.merchantRouteId;
					}
				}
				strSQLWhere += " and (merchants.merchant_id in (select merchant_id from terminals (nolock) where millennium_no like '%"
						+ this.criteria.replaceAll("'", "''") + "%') or merchants.merchant_id like '%" + this.criteria.replaceAll("'", "''")
						+ "%' or merchants.dba like '%" + this.criteria.replaceAll("'", "''") + "%' or merchants.contact_phone like '%"
						+ this.criteria.replaceAll("'", "''") + "%'";
				// DBSY-814. Perform a search using SFID: a value in terminal mapping
				strSQLWhere += " or merchants.merchant_id in (select merchant_id from terminals (nolock) where millennium_no in (select distinct debisysTerminalId from terminal_mapping (nolock) where providerId = 110 and providerTerminalId like '%"
						+ this.criteria.replaceAll("'", "''") + "%'))";
				//
				strSQLWhere += ")";

			}

			// begin actual query for results
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";

			try {
				if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals("")) {
					intCol = Integer.parseInt(this.col);
					intSort = Integer.parseInt(this.sort);
				}
			} catch (NumberFormatException nfe) {
				intCol = 0;
				intSort = 0;
			}

			if (intSort == 2) {
				strSort = "DESC";
			} else {
				strSort = "ASC";
			}

			switch (intCol) {
			case 1:
				strCol = "merchants.dba";
				break;
			case 2:
				strCol = "merchants.merchant_id";
				break;
			case 3:
				strCol = "merchants.phys_city";
				break;
			case 4:
				strCol = "merchants.contact";
				break;
			case 5:
				strCol = "merchants.contact_phone";
				break;
			case 6:
				strCol = "merchants.datecancelled";
				break;
			case 7:
				strCol = "reps.firstname";
				break;
			case 8:
				strCol = "Routes.RouteName";
				break;
			default:
				strCol = "merchants.dba";
				break;
			}

			String strSQL;

			if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
				strSQL = "SELECT DISTINCT merchants.rec_id, merchants.dba, merchants.merchant_id, merchants.phys_city, merchants.contact, merchants.contact_phone, merchants.datecancelled, reps.firstname, reps.lastname, merchants.cancelled, Routes.RouteName "
						+ "FROM merchants WITH (NOLOCK) "
						+ "INNER JOIN reps WITH (NOLOCK) ON merchants.rep_id = reps.rep_id "
						+ "LEFT JOIN Routes WITH (NOLOCK) ON merchants.RouteID = Routes.RouteID  "
						+ "INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id "
						+ "inner join Repcarriers ta WITH (NOLOCK)   on ta.carrierid = mc.carrierid ";

			} else {
				strSQL = "SELECT merchants.rec_id, merchants.dba, merchants.merchant_id, merchants.phys_city, merchants.contact, merchants.contact_phone, merchants.datecancelled, reps.firstname, reps.lastname, merchants.cancelled, Routes.RouteName "
						+ "FROM merchants (NOLOCK) "
						+ "INNER JOIN reps (NOLOCK) ON merchants.rep_id = reps.rep_id "
						+ "LEFT JOIN Routes (NOLOCK) ON merchants.RouteID = Routes.RouteID WHERE ";
			}
			if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
				if (this.merchantRouteId > 0) {
					strSQL = strSQL + " Routes.RouteID  = " + this.merchantRouteId + " AND ";
				}
			}
			cat.debug(strSQL + strSQLWhere + " order by " + strCol + " " + strSort);
                        if ( sessionData.getProperty(CustomerSearch.PROPERTY_SESSION_SQL_ENTITY).equals("1") )
                        {
                            String ids = sessionData.getProperty(CustomerSearch.PROPERTY_SESSION_SQL_ENTITY_IDS);
                            if ( !ids.equals("") )
                            {
                                strSQLWhere = strSQLWhere + " and reps.rep_id in ("+ids+")";
                            }
                            sessionData.setProperty(CustomerSearch.PROPERTY_SESSION_SQL_ENTITY, strSQL + strSQLWhere + " ORDER BY " + strCol + " " + strSort);
                            return null;
                        }
			pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by " + strCol + " " + strSort, ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery();
			intRecordCount = DbUtil.getRecordCount(rs);
			vecCounts.add(Integer.valueOf(intRecordCount));

			// first row is always the counts of
			// total, active, and record counts
			vecCustomers.add(vecCounts);

			if (intRecordCount > 0) {
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));

				for (int i = 0; i < intRecordsPerPage; i++) {
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("rec_id")));
					vecTemp.add(StringUtil.toString(rs.getString("dba")));
					vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
					vecTemp.add(StringUtil.toString(rs.getString("phys_city")));
					vecTemp.add(StringUtil.toString(rs.getString("contact")));
					vecTemp.add(StringUtil.toString(rs.getString("contact_phone")));
					vecTemp.add(DateUtil.formatDate(rs.getDate("datecancelled")));
					vecTemp.add(StringUtil.toString(rs.getString("firstname")));
					vecTemp.add(StringUtil.toString(rs.getString("lastname")));
					vecTemp.add(StringUtil.toString(rs.getString("cancelled")));

					/* BEGIN Release 21 - Added by YH */
					vecTemp.add(StringUtil.toString(rs.getString("RouteName")));
					/* END Release 21 - Added by YH */

					vecCustomers.add(vecTemp);
					// This block replaces the below one because it causes a warning because of an inefficent condition
					// Also because calling isLast represents a performance cost in the engine because of data fetching
					if (!rs.next()) {
						break;
					}
					/*
					 * if (rs.isLast()) { break; } else { rs.next(); }
					 */
				}
			}
		} catch (Exception e) {
			cat.error("Error during searchMerchant", e);
			throw new CustomerException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecCustomers;
	}

	/**
	 * Gets list of reps from the reps table for the logged in user.
	 * 
	 * @param intPageNumber
	 * @return A vector of results, or null if none found.
	 * @throws CustomerException
	 *             Thrown on database errors
	 */

	public Vector<Object> searchRep(int intPageNumber, int intRecordsPerPage, SessionData sessionData, String strRepType) throws CustomerException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Object> vecCustomers = new Vector<Object>();
		try {
			strRepType = StringUtil.escape(strRepType);
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new CustomerException();
			}
			// get a count of total matches

			String strSQLWhere = "";
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");
			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strSQLCarrier = "";

			String numNewModelsPlans = "";

			String orderByAlias = "reps";

			// sql by default
			String strSQL = "";

			// login level
			cat.debug("strRepType strRepType :: " + strRepType);

			if (this.isSearchNumNewModelsPlans()) {
				numNewModelsPlans = ",(SELECT count(*) FROM rep_iso_rate_plan_glue g with(nolock)  INNER JOIN rateplan b with(nolock) on g.iso_rate_plan_id=b.RatePlanID "
						+ " WHERE g.rep_id=reps.rep_id  and g.isNewRatePlanModel=1 and b.IsTemplate=0) numplans";
			}
			strSQL = "select DISTINCT businessname, rep_id, city, state, firstname, lastname, phone, SharedBalance, Rep_Credit_Type, Type AS RepType "
					+ numNewModelsPlans + " from reps with(nolock) where ";

			if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
				// //////////////////////////////////////////////////////////////////////////////////
				// BEGIN OF CARRIER ACCESS LEVEL
				// //////////////////////////////////////////////////////////////////////////////////
				if (strRepType.equals(DebisysConstants.REP_TYPE_ISO_5_LEVEL)) {
					orderByAlias = "r3";
					strSQLCarrier = strSQLCarrier
							+ "SELECT DISTINCT r3.businessname, r3.rep_id, r3.city, r3.state, r3.firstname, r3.lastname, r3.phone, r3.SharedBalance, r3.Rep_Credit_Type, r3.Type AS RepType "
							+ "FROM  reps AS r WITH (NOLOCK) " + "INNER JOIN reps AS r1 WITH (NOLOCK) ON r.iso_id = r1.rep_id "
							+ "INNER JOIN reps AS r2 WITH (NOLOCK) ON r1.iso_id = r2.rep_id " + "INNER JOIN reps AS r3 WITH (NOLOCK) ON r2.iso_id = r3.rep_id "
							+ "INNER JOIN merchants As m WITH (NOLOCK) ON m.rep_id = r.rep_id "
							+ "INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid = m.merchant_id "
							+ "INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid " + "where  ta.repid = " + strRefId;

					if (this.criteria != null && !this.criteria.equals("")) {
						strSQLCarrier = strSQLCarrier + " and (r3.rep_id like '%" + this.criteria.replaceAll("'", "''") + "%' or r3.businessname like '%"
								+ this.criteria.replaceAll("'", "''") + "%'  or r3.phone like '%" + this.criteria.replaceAll("'", "''") + "%')";
					}

				} else if (strRepType.equals(DebisysConstants.REP_TYPE_AGENT)) {
					orderByAlias = "r2";
					strSQLCarrier = strSQLCarrier
							+ " SELECT DISTINCT r2.businessname, r2.rep_id, r2.city, r2.state, r2.firstname, r2.lastname, r2.phone, r2.SharedBalance, r2.Rep_Credit_Type, r2.Type AS RepType "
							+ "FROM  reps AS r WITH (nolock) " + "INNER JOIN reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id "
							+ "INNER JOIN reps AS r2 WITH (nolock) ON r1.iso_id = r2.rep_id "
							+ "INNER JOIN merchants AS m WITH (NOLOCK) ON m.rep_id = r.rep_id "
							+ "INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid = m.merchant_id "
							+ "inner join Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid " + "where  ta.repid = " + strRefId;

					if (this.rep_id != null && !this.rep_id.equals("")) {
						strSQLCarrier = strSQLCarrier + " and r2.iso_id=" + this.rep_id;
					}

					if (this.criteria != null && !this.criteria.equals("")) {
						strSQLCarrier = strSQLCarrier + " and (r2.rep_id like '%" + this.criteria.replaceAll("'", "''") + "%' or r2.businessname like '%"
								+ this.criteria.replaceAll("'", "''") + "%'  or r2.phone like '%" + this.criteria.replaceAll("'", "''") + "%')";
					}

				} else if (strRepType.equals(DebisysConstants.REP_TYPE_SUBAGENT)) {
					orderByAlias = "r1";
					strSQLCarrier = strSQLCarrier
							+ "SELECT DISTINCT r1.businessname, r1.rep_id, r1.city, r1.state, r1.firstname, r1.lastname, r1.phone, r1.SharedBalance, r1.Rep_Credit_Type, r1.Type AS RepType "
							+ "FROM  reps AS r WITH (nolock) " + "INNER JOIN reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id "
							+ "INNER JOIN merchants AS m WITH (NOLOCK) ON m.rep_id = r.rep_id "
							+ "INNER JOIN Merchantcarriers AS mc WITH (NOLOCK) ON mc.merchantid = m.merchant_id "
							+ "inner join Repcarriers AS ta WITH (NOLOCK) on ta.carrierid = mc.carrierid " + "where ta.repid = " + strRefId;

					if (this.rep_id != null && !this.rep_id.equals("")) {
						strSQLCarrier = strSQLCarrier + " and r1.iso_id=" + this.rep_id;
					}

					if (this.criteria != null && !this.criteria.equals("")) {
						strSQLCarrier = strSQLCarrier + " and (r1.rep_id like '%" + this.criteria.replaceAll("'", "''") + "%' or r1.businessname like '%"
								+ this.criteria.replaceAll("'", "''") + "%'  or r1.phone like '%" + this.criteria.replaceAll("'", "''") + "%')";
					}
				} else if (strRepType.equals(DebisysConstants.REP_TYPE_REP)) {
					orderByAlias = "r";
					strSQLCarrier = strSQLCarrier
							+ "SELECT DISTINCT r.businessname, r.rep_id, r.city, r.state, r.firstname, r.lastname, r.phone, r.SharedBalance, r.Rep_Credit_Type, r.Type AS RepType "
							+ "FROM reps AS r WITH (nolock) " + "INNER JOIN merchants AS m WITH (NOLOCK) ON m.rep_id = r.rep_id "
							+ "INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid = m.merchant_id "
							+ "inner join Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid " + "where ta.repid = " + strRefId;

					if (this.rep_id != null && !this.rep_id.equals("")) {
						strSQLCarrier = strSQLCarrier + " and r.iso_id=" + this.rep_id;
					}

					if (this.criteria != null && !this.criteria.equals("")) {
						strSQLCarrier = strSQLCarrier + " and (r.rep_id like '%" + this.criteria.replaceAll("'", "''") + "%' or r.businessname like '%"
								+ this.criteria.replaceAll("'", "''") + "%'  or r.phone like '%" + this.criteria.replaceAll("'", "''") + "%')";
					}
				}

				// //////////////////////////////////////////////////////////////////////////////////
				// END OF CARRIER ACCESS LEVEL
				// //////////////////////////////////////////////////////////////////////////////////
			} else if (strAccessLevel.equals(DebisysConstants.ISO)) {
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
					if (strRepType.equals(DebisysConstants.REP_TYPE_AGENT)) {
						strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + " and rep_id <> " + strRefId;
					} else if (strRepType.equals(DebisysConstants.REP_TYPE_SUBAGENT)) {
						strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id "
								+ "in (select rep_id from reps with(nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + ")";

						if (this.rep_id != null && !this.rep_id.equals("")) {
							strSQLWhere = strSQLWhere + " and iso_id=" + this.rep_id;
						}

					} else if (strRepType.equals(DebisysConstants.REP_TYPE_REP)) {
						strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_REP + " and ";
						strSQLWhere = strSQLWhere + " (iso_id in " +
						// subagents
								"(select rep_id from reps with(nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
								// agents
								"(select rep_id from reps with(nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + "))";

						if (this.rep_id != null && !this.rep_id.equals("")) {
							strSQLWhere = strSQLWhere + " and iso_id=" + this.rep_id;
						}
						strSQLWhere = strSQLWhere + ")";
					} else if (strRepType.equals(DebisysConstants.REP_TYPE_ISO_5_LEVEL)) {
						strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL + " and iso_id=" + strRefId + " and rep_id = " + strRefId;
					}
				} else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					strSQLWhere = strSQLWhere + " iso_id=" + strRefId + " and rep_id <> " + strRefId;
				}

			} else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
				if (strRepType.equals(DebisysConstants.REP_TYPE_SUBAGENT)) {
					strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id=" + strRefId;
				} else if (strRepType.equals(DebisysConstants.REP_TYPE_REP)) {
					strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_REP + " and ";
					strSQLWhere = strSQLWhere + " (iso_id in " +
					// subagents
							"(select rep_id from reps with(nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id= " + strRefId + ")";

					if (this.rep_id != null && !this.rep_id.equals("")) {
						strSQLWhere = strSQLWhere + " and iso_id=" + this.rep_id;
					}
					strSQLWhere = strSQLWhere + ")";
				}
			} else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
				if (strRepType.equals(DebisysConstants.REP_TYPE_REP)) {
					strSQLWhere = strSQLWhere + " type=" + DebisysConstants.REP_TYPE_REP + " and ";
					strSQLWhere = strSQLWhere + " iso_id=" + strRefId;
				}
			}

			if (this.criteria != null && !this.criteria.equals("")) {
				if (!strRepType.equals(DebisysConstants.REP_TYPE_REP)) {
					strSQLWhere = strSQLWhere + " and (rep_id like '%" + this.criteria.replaceAll("'", "''") + "%' or businessname like '%"
							+ this.criteria.replaceAll("'", "''") + "%'  or phone like '%" + this.criteria.replaceAll("'", "''") + "%')";
				} else {
					strSQLWhere = strSQLWhere + " and (reps.rep_id like '%" + this.criteria.replaceAll("'", "''") + "%' or businessname like '%"
							+ this.criteria.replaceAll("'", "''") + "%'  or phone like '%" + this.criteria.replaceAll("'", "''") + "%')";
				}
			}

			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";

			try {
				if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals("")) {
					intCol = Integer.parseInt(this.col);
					intSort = Integer.parseInt(this.sort);
				}
			} catch (NumberFormatException nfe) {
				intCol = 0;
				intSort = 0;
			}

			if (intSort == 2) {
				strSort = "DESC";
			} else {
				strSort = "ASC";
			}

			switch (intCol) {
			case 1:
				strCol = orderByAlias + ".businessname";
				break;
			case 2:
				strCol = orderByAlias + ".rep_id";
				break;
			case 3:
				strCol = orderByAlias + ".city";
				break;
			case 4:
				strCol = orderByAlias + ".state";
				break;
			case 5:
				strCol = orderByAlias + ".firstname";
				break;
			case 6:
				strCol = orderByAlias + ".phone";
				break;
			default:
				strCol = orderByAlias + ".businessname";
				break;
			}

			// ADDED BY MCR - SPG
			String sql = "";
			if (strAccessLevel.equals(DebisysConstants.CARRIER)) 
                        {
				sql = strSQLCarrier + " order by " + strCol + " " + strSort;
			} 
                        else 
                        {
                            String ids = sessionData.getProperty(CustomerSearch.PROPERTY_SESSION_SQL_ENTITY_IDS);
                            if ( !ids.equals("") )
                            {
                                strSQLWhere = strSQLWhere + " and iso_id in ("+ids+")";
                            }
                            sql = strSQL + (strSQLWhere.equals("") ? " 1=1 " : strSQLWhere);
			}
                        if ( sessionData.getProperty(CustomerSearch.PROPERTY_SESSION_SQL_ENTITY).equals("1") )
                        {
                            sql = sql + " ORDER BY " + strCol + " " + strSort;
                            sessionData.setProperty(CustomerSearch.PROPERTY_SESSION_SQL_ENTITY, sql);
                            return null;
                        }
                        sql = sql + " ORDER BY " + strCol + " " + strSort;
			pstmt = dbConn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			cat.debug(sql);
			rs = pstmt.executeQuery();
			int intRecordCount = DbUtil.getRecordCount(rs);
			// first row is always the count
			vecCustomers.add(Integer.valueOf(intRecordCount));
			if (intRecordCount > 0) {
				if (intRecordsPerPage == -1) {
					intRecordsPerPage = intRecordCount;
				}
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
				for (int i = 0; i < intRecordsPerPage; i++) {
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("businessname")));
					vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
					vecTemp.add(StringUtil.toString(rs.getString("city")));
					vecTemp.add(StringUtil.toString(rs.getString("state")));
					vecTemp.add(StringUtil.toString(rs.getString("firstname")));
					vecTemp.add(StringUtil.toString(rs.getString("lastname")));
					vecTemp.add(StringUtil.toString(rs.getString("phone")));
					vecTemp.add(StringUtil.toString(rs.getString("SharedBalance")));
					vecTemp.add(StringUtil.toString(rs.getString("Rep_Credit_Type")));

					if (this.isSearchNumNewModelsPlans()) {
						vecTemp.add(StringUtil.toString(rs.getString("numplans")));
					}
					vecTemp.add(StringUtil.toString(rs.getString("RepType")));

					vecCustomers.add(vecTemp);
					// This block replaces the below one because it causes a warning because of an inefficent condition
					// Also because calling isLast represents a performance cost in the engine because of data fetching
					if (!rs.next()) {
						break;
					}
				}
			}

		} catch (Exception e) {
			cat.error("Error during searchRep", e);
			throw new CustomerException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecCustomers;
	}

	/**
	 * Gets parent rep business name for given rep_id.
	 * 
	 * @param strRepId
	 * @return A rep business name of type string.
	 * @throws CustomerException
	 *             Thrown on database errors
	 */

	public String getParentRepName(String strRepId) throws CustomerException {
		String strRepName = "";
		if (strRepId != null && !strRepId.equals("")) {
			Connection dbConn = null;
			ResultSet rs = null;
			PreparedStatement pstmt = null;
			try {
				dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
				if (dbConn == null) {
					cat.error("Can't get database connection");
					throw new CustomerException();
				}
				// get a count of total matches

				String strSQL = sql_bundle.getString("getParentRep");
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRepId));
				rs = pstmt.executeQuery();

				if (rs.next()) {
					strRepName = rs.getString("businessname");
				}
			} catch (Exception e) {
				cat.error("Error during GetParentRepName", e);
				throw new CustomerException();
			} finally {
				try {
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				} catch (Exception e) {
					cat.error("Error during closeConnection", e);
				}
			}
		}
		return strRepName;
	}

	public void valueBound(HttpSessionBindingEvent event) {
	}

	public void valueUnbound(HttpSessionBindingEvent event) {
	}

	public static void main(String[] args) {
	}

	public String getListedRepIds(String[] strValue) {
		String strTemp = "";
		if (strValue != null && strValue.length > 0) {
			boolean isFirst = true;
			for (int i = 0; i < strValue.length; i++) {
				if (strValue[i] != null && !strValue[i].equals("")) {
					if (!isFirst) {
						strTemp = strTemp + "," + strValue[i];
					} else {
						strTemp = strValue[i];
						isFirst = false;
					}
				}
			}
		}

		return strTemp;
	}

	public void setAgentIds(String[] strValue) {
		this.agentIds = this.getListedRepIds(strValue);
	}

	public void setSubAgentIds(String[] strValue) {
		this.subAgentIds = this.getListedRepIds(strValue);
	}

	public String getAgentIds() {
		return this.agentIds;
	}

	public void setAgentIds(String agentIds) {
		this.agentIds = agentIds;
	}

	public String getSubAgentIds() {
		return this.subAgentIds;
	}

	public void setSubAgentIds(String subAgentIds) {
		this.subAgentIds = subAgentIds;
	}

	/***
	 * DBSY-890 SW
	 * 
	 * @param intPageNumber
	 * @param intRecordsPerPage
	 * @param _isoID
	 * @return
	 * @throws CustomerException
	 */
	public Vector<Vector<String>> getCarrierUsers(int intPageNumber, int intRecordsPerPage, String _isoID) throws CustomerException {
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<String>> vecCarriers = new Vector<Vector<String>>();

		if (_isoID == null)
			return vecCarriers;

		try {
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new CustomerException();
			}

			// Sorting
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";

			try {
				if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals("")) {
					intCol = Integer.parseInt(this.col);
					intSort = Integer.parseInt(this.sort);
				}
			} catch (NumberFormatException nfe) {
				intCol = 0;
				intSort = 0;
			}

			if (intSort == 2) {
				strSort = "DESC";
			} else {
				strSort = "ASC";
			}

			switch (intCol) {
			case 1:
				strCol = "b.logon_id";
				break;
			default:
				strCol = "b.logon_id";
				break;
			}
			// End sorting

			StringBuffer strSQLBuff = new StringBuffer(sql_bundle.getString("getCarrierUsers"));

			// The stuff from the search box..
			if (this.criteria != null && !this.criteria.equals("")) {
				strSQLBuff.append(" AND b.logon_id like '%" + this.criteria.replaceAll("'", "''") + "%'");
			}

			strSQLBuff.append(" order by " + strCol + " " + strSort);
			cat.debug("Carrier SQL: " + strSQLBuff.toString());
			pstmt = dbConn.prepareStatement(strSQLBuff.toString());
			pstmt.setString(1, _isoID);
			cat.debug("Param 1: " + _isoID);
			rs = pstmt.executeQuery();

			int intRecordCount = 0;

			while (rs.next()) {
				intRecordCount++;
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("PasswordID")));
				vecTemp.add(StringUtil.toString(rs.getString("LogonID")));
				vecTemp.add(StringUtil.toString(rs.getString("LogonPassword")));
				vecCarriers.add(vecTemp);
			}

			// first row is always the count
			Vector<String> vecRecCount = new Vector<String>();
			vecRecCount.add(Integer.toString(intRecordCount));
			vecCarriers.add(0, vecRecCount);
		} catch (Exception e) {
			cat.error("Error during getCarrierUsers", e);
			throw new CustomerException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecCarriers;
	}

	/**
	 * @param searchNumNewModelsPlans
	 *            the searchNumNewModelsPlans to set
	 */
	public void setSearchNumNewModelsPlans(boolean searchNumNewModelsPlans) {
		this.searchNumNewModelsPlans = searchNumNewModelsPlans;
	}

	/**
	 * @return the searchNumNewModelsPlans
	 */
	public boolean isSearchNumNewModelsPlans() {
		return this.searchNumNewModelsPlans;
	}
        
    
    public static Vector executeReport(String sql, ArrayList<String> fieldToRead) throws CustomerException
    {
        Vector entitiesList = new Vector();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            cat.debug("executeReport =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                Vector vecTemp = new Vector();
                /*
                vecTemp.add(rs.getString("rep_id"));
                vecTemp.add(rs.getString("businessname"));
                */
                for( String fieldName : fieldToRead )
                {
                    if ( fieldName.indexOf(".") != -1 )
                    {
                        String[] sField = fieldName.split("\\.");
                        vecTemp.add( rs.getString(sField[1]) );
                    }
                    else
                    {
                        vecTemp.add( rs.getString(fieldName) );
                    }
                }
                entitiesList.add(vecTemp);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during executeReport ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("executeReport Error during closeConnection", e);
            }
        }
        return entitiesList;
    }
    
    /**
     * 
     * @param strAccessLevel
     * @param ids
     * @param strDistChainType
     * @param status
     * @param unitNumber
     * @param period
     * @param trxNumber
     * @return
     * @throws CustomerException 
     */
    public String searchMerchantLocatorReport(String strAccessLevel, String ids, String strDistChainType, 
                                              String status, String unitNumber, String period, String trxNumber, ArrayList<String> fieldsToRead ) throws CustomerException {
		
        String strSQL;
        String strSelect="";
        String strGroupBy="";
        String strHaving="";
        String TOKEN_BY_STATUS = "TOKEN_BY_STATUS";
        
        try 
        {
            String strSQLWhere = "";

            if (strAccessLevel.equals(DebisysConstants.CARRIER)) 
            {				
                strSQLWhere = "where ta.repid in ( " + ids + ") ";
            }             
            
            else if (strAccessLevel.equals(DebisysConstants.REP)) 
            {
                strSQLWhere = strSQLWhere + " and r.rep_id in (" + ids+ ") ";
            }		
            else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) 
            {
                strSQLWhere = strSQLWhere + " and m.merchant_id in ( " + ids+" ) AND m.latitude <>0 AND m.longitude <> 0 ";
            }
            
            if (strAccessLevel.equals(DebisysConstants.CARRIER)) 
            {
                //strSelect = " merchants.dba, merchants.merchant_id, merchants.datecancelled, reps.firstname, reps.lastname, merchants.cancelled "
                //           +" reps.firstname, reps.lastname, merchants.cancelled,  merchants.latitude, merchants.longitude ";
                strSelect = fieldsToRead.toString().replaceAll("\\[", " ").replaceAll("\\]", " ");
                strSQL = "SELECT DISTINCT " +TOKEN_BY_STATUS        
                            + "FROM merchants m WITH (NOLOCK) "
                            + "INNER JOIN reps WITH (NOLOCK) ON m.rep_id = r.rep_id "
                            + "INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid = m.merchant_id "
                            + "inner join Repcarriers ta WITH (NOLOCK)   on ta.carrierid = mc.carrierid ";

            } 
            else 
            {
                //strSelect =  " merchants.dba, merchants.merchant_id, merchants.contact, merchants.contact_phone, merchants.datecancelled, "
                //            +" reps.firstname, reps.lastname, merchants.cancelled,  merchants.latitude, merchants.longitude ";
                strSelect = fieldsToRead.toString().replaceAll("\\[", " ").replaceAll("\\]", " ");
               
                strSQL = "SELECT "+TOKEN_BY_STATUS        
                            + "FROM merchants m WITH (NOLOCK) "
                            + "INNER JOIN reps r WITH (NOLOCK) ON m.rep_id = r.rep_id ";                                   
            }           

            if ( !status.equals("") )
            {
                if ( status.equals("A"))
                {
                    //ACTIVE
                    strSQL = strSQL.replaceAll(TOKEN_BY_STATUS, strSelect+ " ,0 AS trx ");
                    strSQLWhere = strSQLWhere + " AND m.datecancelled IS NULL AND  m.cancelled=0 ";
                }
                else if ( status.equals("C"))
                {
                    //CANCELLED
                    strSQL = strSQL.replaceAll(TOKEN_BY_STATUS, strSelect+ " ,0 AS trx ");
                    strSQLWhere = strSQLWhere + " AND m.datecancelled IS NOT NULL AND  m.cancelled=1 ";
                } 
                else if ( status.equals("D"))
                {
                    //DISABLED
                    strSQL = strSQL.replaceAll(TOKEN_BY_STATUS, strSelect+ " ,0 AS trx ");
                    strSQLWhere = strSQLWhere + " AND m.datecancelled IS NOT NULL AND  m.cancelled=0 ";
                }                
                else if ( status.equals("AT"))
                {
                    //ACTIVE WITH TRANSACTIONS
                    strSQL = strSQL.replaceAll(TOKEN_BY_STATUS, strSelect + " ,count(w.rec_id) trx ");
                    strSQL += " INNER JOIN web_transactions w WITH(NOLOCK) ON w.merchant_id = m.merchant_id ";
                    strSQLWhere = strSQLWhere + " AND w.datetime > dateadd("+period+", -"+unitNumber+", getdate()) ";
                    
                    strGroupBy = " GROUP BY "+strSelect;
                    strHaving  = " HAVING count(w.rec_id) >= "+trxNumber;
                }            
            }    
            else
            {
                strSQL = strSQL.replaceAll(TOKEN_BY_STATUS, strSelect);               
            }
            strSQL += strSQLWhere + strGroupBy + strHaving; // + " order by m.dba ASC ";
			
        } catch (Exception e) {
            cat.error("Error during searchMerchantLocatorReport ", e);
            throw new CustomerException();
        }
        return strSQL;
    }
    
    
    /**
     * 
     * @param sql
     * @param fieldToRead
     * @return
     * @throws CustomerException 
     */
    public static JSONObject searchMerchantsWithLatLong(String sql, ArrayList<String> fieldToRead) throws CustomerException
    {
        JSONObject json = new JSONObject();
        try
        {
            Vector jsonVec = new Vector();
            Iterator it = executeReport(sql, fieldToRead).iterator();

            while (it.hasNext())
            {
                Vector vTmp = (Vector) it.next();
                HashMap map = new HashMap();
                for( int i=0; i< fieldToRead.size(); i++)
                {      
                    String jsonFieldName = fieldToRead.get(i);
                    if ( jsonFieldName.indexOf(".") != -1 )
                    {
                        String[] sField = fieldToRead.get(i).split("\\.");
                        jsonFieldName = sField[1];
                    }
                    if ( vTmp.get(i) != null )
                    {
                        //map.put(jsonFieldName, URLEncoder.encode(vTmp.get(i).toString(), "UTF-8") );
                        map.put(jsonFieldName, vTmp.get(i).toString() );
                    }
                    else 
                    {
                        map.put(jsonFieldName, "" );
                    }
                }                
                jsonVec.add(map);
            }
            json.put("items", jsonVec);
        }
        catch (Exception e)
        {
            Merchant.cat.error("Error during searchMerchantsWithLatLong", e);
            throw new CustomerException();
        }
        return json;
    }
    
}
