/**
 * 
 */
package com.debisys.customers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Category;

import com.debisys.exceptions.CustomerException;
import com.debisys.languages.Languages;
import com.debisys.utils.DbUtil;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.CallableStatement;
import java.sql.SQLException;

/**
 * @author nmartinez
 *
 */
public class MerchantInvoicePayments {
	
	static Category               cat                 = Category.getInstance(MerchantInvoicePayments.class);
	private static ResourceBundle sql_bundle          = ResourceBundle.getBundle("com.debisys.customers.sql");
	  
	  
	private Integer id;
	private Long merchantId ;
	private Integer paymentTypeId;
	private String accountNumber;	
	private Boolean showControls;
	private String description;
	
	public MerchantInvoicePayments(Integer id, Long merchantId, Integer paymentTypeId, String accountNumber, Boolean showControls)
	{
		this.id = id;
		this.merchantId = merchantId;
		this.paymentTypeId = paymentTypeId;
		this.accountNumber = accountNumber;		
		this.showControls = showControls;
	}
	
	public MerchantInvoicePayments()
	{		
	}
	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the merchantId
	 */
	public Long getMerchantId() {
		return merchantId;
	}
	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * @return the paymentTypeId
	 */
	public Integer getPaymentTypeId() {
		return paymentTypeId;
	}
	/**
	 * @param paymentTypeId the paymentTypeId to set
	 */
	public void setPaymentTypeId(Integer paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		if ( accountNumber != null)
			this.accountNumber = accountNumber;
		else
			this.accountNumber = "";
	}

	/**
	 * @param showControls the showControls to set
	 */
	public void setShowControls(Boolean showControls) {
		this.showControls = showControls;
	}


	/**
	 * @return the showControls
	 */
	public Boolean getShowControls() {
		return showControls;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	} 
		
	 
	public static boolean updateInvoicePayment(MerchantInvoicePayments updateMerchantInvoicePayment)
	{
		boolean result = false;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		
		try
		{			
			if ( updateMerchantInvoicePayment != null && updateMerchantInvoicePayment.getMerchantId() != null )
			{
				dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));			
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new CustomerException();
				}
				String strSQL = sql_bundle.getString("updateInvoicePayment");
				cat.debug(strSQL);
						
				pstmt = dbConn.prepareStatement(strSQL);				
				if ( updateMerchantInvoicePayment.getAccountNumber() != null && updateMerchantInvoicePayment.getAccountNumber().length() == 0 )
				{
					pstmt.setNull(1, java.sql.Types.VARCHAR);
				}
				else
				{
					pstmt.setString(1, updateMerchantInvoicePayment.getAccountNumber());
				}
				pstmt.setInt(2, updateMerchantInvoicePayment.getPaymentTypeId());
				pstmt.setLong(3, updateMerchantInvoicePayment.getMerchantId());
				pstmt.setInt(4, updateMerchantInvoicePayment.getId());
				pstmt.executeUpdate();
				result = true;
			}
		}
		catch(Exception e)
		{
			result = false;
			cat.error("Error during updateInvoicePayment ", e);			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection ", e);
			}
		}
		return result;
	}
	
	
	public static boolean addInvoicePayment(MerchantInvoicePayments newMerchantInvoicePayment)
	{
		boolean result = false;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		
		try
		{			
			if ( newMerchantInvoicePayment != null && newMerchantInvoicePayment.getMerchantId() != null )
			{
				dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));			
				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new CustomerException();
				}
				String strSQL = sql_bundle.getString("addMerchantInvoicePayment");
				cat.debug(strSQL);
						
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setLong(1, newMerchantInvoicePayment.getMerchantId());
				pstmt.setInt(2, newMerchantInvoicePayment.getPaymentTypeId());
				
				if ( newMerchantInvoicePayment.getAccountNumber() != null && newMerchantInvoicePayment.getAccountNumber().length() == 0 )
				{
					pstmt.setNull(3, java.sql.Types.VARCHAR);
				}
				else
				{
					pstmt.setString(3, newMerchantInvoicePayment.getAccountNumber());
				}								
				pstmt.executeUpdate();
				result = true;
			}
		}
		catch(Exception e)
		{
			result = false;
			cat.error("Error during addInvoicePayment ", e);			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection ", e);
			}
		}
		return result;
	}
	
	
	public static boolean deleteInvoicePayment(String idToDelete, String merchantId)
	{
		boolean result = false;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		
		try
		{		
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new CustomerException();
			}
			String strSQL = sql_bundle.getString("deletedMerchantInvoicePayment");
			cat.debug(strSQL);
					
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, merchantId );
			pstmt.setString(2, idToDelete);
			pstmt.executeUpdate();	
			result = true;
		}
		catch(Exception e)
		{
			result = false;
			cat.error("Error during deleteInvoicePayment ", e);		
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection ", e);
			}
		}
		return result;		
	}
        
    public static boolean deletedAllMerchantInvoicePayments(String merchantId) {
        boolean retValue = false;
        Connection dbConn = null;
        CallableStatement cstmt = null;

        String sqlDeleteAllInvoicePaymentsByMerchant = sql_bundle.getString("deletedAllMerchantInvoicePayments");
        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            cstmt = dbConn.prepareCall(sqlDeleteAllInvoicePaymentsByMerchant);
            cstmt.setString(1, merchantId);
            cstmt.executeUpdate();
            retValue = true;
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            cat.error("Error while inserting a new sim", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, cstmt, null);
        }
        return retValue;
    }
        
        
	
		
	public static ArrayList<MerchantInvoicePayments> findMerchantInvoicePayments(String merchantId)
	{
		ArrayList<MerchantInvoicePayments> merchantInvoicePaymentsArray = new ArrayList<MerchantInvoicePayments>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try
		{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new CustomerException();
			}
			String strSQL = sql_bundle.getString("findMerchantInvoicePayments");
			
			cat.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setDouble(1, Double.parseDouble(merchantId));
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				Integer id = rs.getInt(1);
				Integer paymentTypeId = rs.getInt(2);
				String accountNumber = rs.getString(3);
				String description= rs.getString(4);
				Long merchat_Id = new Long(merchantId);
				
				MerchantInvoicePayments mMerchantInvoicePayments = new MerchantInvoicePayments();
				mMerchantInvoicePayments.setId(id);
				mMerchantInvoicePayments.setPaymentTypeId(paymentTypeId);
				mMerchantInvoicePayments.setAccountNumber(accountNumber);
				mMerchantInvoicePayments.setDescription(description);
				mMerchantInvoicePayments.setMerchantId(merchat_Id);				
				mMerchantInvoicePayments.setShowControls(false);
				
				merchantInvoicePaymentsArray.add(mMerchantInvoicePayments);				
			}
			rs.close();
			rs = null;
			
		}
		catch (Exception e)
		{
			cat.error("Error during findMerchantInvoicePayments ", e);			
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection ", e);
			}
		}		
		return merchantInvoicePaymentsArray;				
	}

	

	
	

}
