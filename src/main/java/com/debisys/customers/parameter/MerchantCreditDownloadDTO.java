package com.debisys.customers.parameter;

import java.util.List;

import javax.servlet.ServletContext;

import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;

public class MerchantCreditDownloadDTO {
	
	private String startDate;
	private String endDate;
	private SessionData sessionData;
	private ServletContext context;
	private String merchantId;
	private List<ColumnReport> headers;
	private List<String> titles;
	private boolean needsDBAName;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public SessionData getSessionData() {
		return sessionData;
	}
	public void setSessionData(SessionData sessionData) {
		this.sessionData = sessionData;
	}
	public ServletContext getContext() {
		return context;
	}
	public void setContext(ServletContext context) {
		this.context = context;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public List<ColumnReport> getHeaders() {
		return headers;
	}
	public void setHeaders(List<ColumnReport> headers) {
		this.headers = headers;
	}
	public List<String> getTitles() {
		return titles;
	}
	public void setTitles(List<String> titles) {
		this.titles = titles;
	}
	public boolean isNeedsDBAName() {
		return needsDBAName;
	}
	public void setNeedsDBAName(boolean needsDBAName) {
		this.needsDBAName = needsDBAName;
	}
	
}