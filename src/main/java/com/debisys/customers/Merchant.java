package com.debisys.customers;

import static com.debisys.customers.Rep.insertBalanceAudit;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.customers.parameter.MerchantCreditDownloadDTO;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.EntityException;
import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.summarys.DownloadsSummary;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.CharsetValidator;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.CountryInfo;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.EntityAccountTypes;
import com.debisys.utils.JSONObject;
import com.debisys.utils.Log;
import com.debisys.utils.LogChanges;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;
import com.emida.utils.dbUtils.TorqueHelper;
import java.util.UUID;
import net.emida.supportsite.util.security.SecurityCipherService;

/**
 * Holds the information for customers.
 * <P>
 * 
 * @author Jay Chi
 */
public class Merchant implements HttpSessionBindingListener
{
	// Alfred - Bitmask flags used for the merchantPermission column
	public final int flagAllOff = 0;
	public final int flagExternalRepsAllowed = 1;
	
	public final int flagMerchantAssignmentOff=0;
	public final int flagMerchantAssignmentAllowed=1;

	// #END Added by NM - Release 21

	// log4j logging
	static Category cat = Category.getInstance(Merchant.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.customers.sql");

	private static String PHONE_CLEAN_REGEXP = "[^0-9]";

	/* BEGIN MiniRelease 2.0 - Added by LF */
	private String contactCellPhone = "";
	private String contactDepartment = "";
	private Vector vecContacts = new Vector();
	private int contactTypeId = 0;

	/* END MiniRelease 2.0 - Added by LF */
	private String dba;
	private String businessName;
	private String contactName;
	private String contactPhone;
	private String contactFax;

	/* BEGIN MiniRelease 1.0 - Added by LF */
	private String businessTypeId;
	private int merchantSegmentId;
	private String businessHours;
	private String physColony;
	private String physDelegation;
	private String mailColony;
	private String mailDelegation;
	private String mailCounty;
	private String mailCountry;
	private String bankName;
	private int accountTypeId;
	private int creditTypeId;
	private int merchantCreditPaymentType; // added by jacuna. R27-DBSY-586

	/* END MiniRelease 1.0 - Added by LF */

	/* BEGIN MiniRelease 1.0 - Added by YH */
	private String depositDate;
	private String bank;
	private String refNumber;

	/* END MiniRelease 1.0 - Added by YH */
	private String contactEmail;
	private String physAddress;
	private String physCity;
	private String physCounty;
	private String physState;
	private String physZip;
	private String physCountry;
	private String repId;
	private String oldRepId;
	private String salesmanid;
	private String terms;
	private String salesmanname;
	private String repName;
	private String mailAddress;
	private String mailCity;
	private String mailState;
	private String mailZip;
	private String taxId;
	private String routingNumber;
	private String accountNumber;
	private String runningLiability;
	private String liabilityLimit;
	private String creditLimit = "0.00";
	private String paymentAmount;
	private String paymentDescription;
	private String noCreditCheck;
	private String repRatePlanId;
	private Hashtable validationErrors = new Hashtable();
	private String siteId;
	private String terminalType;
	private String merchantId;
	private String[] productId;
	private String startDate;
	private String endDate;
	private String merchantType;
	private String currentMerchantType;
	private String netPaymentAmount;
	private String commission;
	private String monthlyFee = "0.00";
	private String monthlyFeeThreshold = "0.00";
	private String annualInsurance = "0.00";
	private String annualMaintenance = "0.00";
	private String reconnection = "0.00";
	private String achSchedule = "";
	private String achScheduleType = "";

	// added just to flag rep type
	// between a validate and add merchant call
	private String repCreditType = "";

	/* BEGIN Release 9.0 - Added by LF */
	private String sharedBalance = "0";
	private String newMerchantType = "";

	/* END Release 9.0 - Added by LF */

	// #BEGIN Added by NM - Release 21
	private int paymentType;
	private int processType;

	/* END MiniRelease 2.0 - Added by LF */

	/* BEGIN Release 21 - Added by YH */
	private int merchantRouteId;
	private String merchantRouteName;

	/* END Release 21 - Added by YH */

	/* BEGIN Release 14.0 - Added by NM */
	private int region;

	/* R27. DBSY-570. modified by JA. */
	private int invoice;

	/* R27. DBSY-570. added by JA. */
	private int merchantClassification;
	private String controlNumber = "";

	private String merchantClassificationOtherDesc;

	/* R27. DBSY-586. added by JA. */
	private int bank_id;

	private boolean m_isAchAccount = false;

	private int reservedPINCacheBalance = 0;

	private boolean pinCacheEnabled = false;

	private boolean printBillingAddress = false;

	private int timeZoneId; // Reference to the current TimeZone, a value of 0
	// indicates no zone which then maps to GMT+0

	private int executiveAccountId;

	private String salesLimitEnable = ""; // Indicates if the ERI Credit Limit
	// has been enabled

	private String salesLimitType = ""; // Indicates the type of Maximum Credit
	// Limit

	private double salesLimitRunnningLiability = 0;

	private double salesLimitLiabilityLimit = 0;

	private double salesLimitMonday = 0;

	private double salesLimitTuesday = 0;

	private double salesLimitWednesday = 0;

	private double salesLimitThursday = 0;

	private double salesLimitFriday = 0;

	private double salesLimitSaturday = 0;

	private double salesLimitSunday = 0;

	private boolean useRatePlanModel = false;
	private String warningChangeModel;

	// R31.1 Billing ABernal-Emunoz
	private int regulatoryFeeId;
	private int rec_id;
	private String regulatoryFeeName = "";

	// End R31.1 Billing ABernal - Emunoz

	// DBSY-1072
	private int entityAccountType = -1;
	private String dentityAccountType = ""; // determines if the combo is
											// disabled

	// Alfred - DBSY-1139
	private int merchantPermissions = 0;


	private int merchantExternalOutsideHierarchy = 0;
	

	
	private int addressValidationStatus = 0;
	private double latitude = 0;
	private double longitude = 0;

	private String paymentTypeForInvoiceGeneration;
	private String accountNumberForInvoiceGeneration;

	// DTU-369 Payment Notifications
	private boolean paymentNotifications = false;
	private int paymentNotificationType = 0;
	private String smsNotificationPhone = "";
	private String mailNotificationAddress = "";
	private boolean paymentNotificationSMS = false;
	private boolean paymentNotificationMail = false;

	// DTU-1772 Capture of detailed Banking Information for Precash settlement
	private String bankAddress = "";
	private String bankCity = "";
	private String bankState = "";
	private String bankZip = "";
	private String bankContactName = "";
	private String bankContactPhone = "";

	private String merchantOwnerId = "";
	private boolean invoicing;

	// DTU-480: Low Balance Alert Message SMS and/or email
	private boolean balanceNotifications = false;
	private int balanceNotificationType = 0;
	private int balanceNotificationNotType = 0;
	private boolean balanceNotificationTypeValue = false;
	private boolean balanceNotificationTypeDays = false;
	private double balanceNotificationValue = 0;
	private int balanceNotificationDays = 0;
	private boolean balanceNotificationSMS = false;
	private boolean balanceNotificationMail = false;
	private String    strUrlLocation = "";
	private boolean testAccountEnabled = false;
	
	public boolean isBalanceNotifications()
	{
		return balanceNotifications;
	}

	public void setBalanceNotifications(boolean balanceNotifications)
	{
		this.balanceNotifications = balanceNotifications;
	}

	public int getBalanceNotificationType()
	{
		return balanceNotificationType;
	}

	public void setBalanceNotificationType(int balanceNotificationType)
	{
		this.balanceNotificationType = balanceNotificationType;
	}

	public int getBalanceNotificationNotType()
	{
		return balanceNotificationNotType;
	}

	public void setBalanceNotificationNotType(int balanceNotificationNotType)
	{
		this.balanceNotificationNotType = balanceNotificationNotType;
	}

	public boolean isBalanceNotificationTypeValue()
	{
		return balanceNotificationTypeValue;
	}

	public void setBalanceNotificationTypeValue(boolean balanceNotificationTypeValue)
	{
		this.balanceNotificationTypeValue = balanceNotificationTypeValue;
	}

	public boolean isBalanceNotificationTypeDays()
	{
		return balanceNotificationTypeDays;
	}

	public void setBalanceNotificationTypeDays(boolean balanceNotificationTypeDays)
	{
		this.balanceNotificationTypeDays = balanceNotificationTypeDays;
	}

	public double getBalanceNotificationValue()
	{
		return balanceNotificationValue;
	}

	public void setBalanceNotificationValue(double balanceNotificationValue)
	{
		this.balanceNotificationValue = balanceNotificationValue;
	}

	public int getBalanceNotificationDays()
	{
		return balanceNotificationDays;
	}

	public void setBalanceNotificationDays(int balanceNotificationDays)
	{
		this.balanceNotificationDays = balanceNotificationDays;
	}

	public boolean isBalanceNotificationSMS()
	{
		return balanceNotificationSMS;
	}

	public void setBalanceNotificationSMS(boolean balanceNotificationSMS)
	{
		this.balanceNotificationSMS = balanceNotificationSMS;
	}

	public boolean isBalanceNotificationMail()
	{
		return balanceNotificationMail;
	}

	public void setBalanceNotificationMail(boolean balanceNotificationMail)
	{
		this.balanceNotificationMail = balanceNotificationMail;
	}

	public String getBankAddress()
	{
		return bankAddress;
	}

	public void setBankAddress(String bankAddress)
	{
		this.bankAddress = bankAddress;
	}

	public String getBankCity()
	{
		return bankCity;
	}

	public void setBankCity(String bankCity)
	{
		this.bankCity = bankCity;
	}

	public String getBankState()
	{
		return bankState;
	}

	public void setBankState(String bankState)
	{
		this.bankState = bankState;
	}

	public String getBankZip()
	{
		return bankZip;
	}

	public void setBankZip(String bankZip)
	{
		this.bankZip = bankZip;
	}

	public String getBankContactName()
	{
		return bankContactName;
	}

	public void setBankContactName(String bankContactName)
	{
		this.bankContactName = bankContactName;
	}

	public String getBankContactPhone()
	{
		return bankContactPhone;
	}

	public void setBankContactPhone(String bankContactPhone)
	{
		this.bankContactPhone = bankContactPhone;
	}

	public boolean isPaymentNotifications()
	{
		return paymentNotifications;
	}

	public void setPaymentNotifications(boolean paymentNotifications)
	{
		this.paymentNotifications = paymentNotifications;
	}

	public int getPaymentNotificationType()
	{
		return paymentNotificationType;
	}

	public void setPaymentNotificationType(int paymentNotificationType)
	{
		this.paymentNotificationType = paymentNotificationType;
	}

	public String getSmsNotificationPhone()
	{
		return smsNotificationPhone;
	}

	public void setSmsNotificationPhone(String smsNotificationPhone)
	{
		this.smsNotificationPhone = smsNotificationPhone;
	}

	public String getMailNotificationAddress()
	{
		return mailNotificationAddress;
	}

	public void setMailNotificationAddress(String mailNotificationAddress)
	{
		this.mailNotificationAddress = mailNotificationAddress;
	}

	public boolean isPaymentNotificationSMS()
	{
		return paymentNotificationSMS;
	}

	public void setPaymentNotificationSMS(boolean paymentNotificationSMS)
	{
		this.paymentNotificationSMS = paymentNotificationSMS;
	}

	public boolean isPaymentNotificationMail()
	{
		return paymentNotificationMail;
	}

	public void setPaymentNotificationMail(boolean paymentNotificationMail)
	{
		this.paymentNotificationMail = paymentNotificationMail;
	}

	public int getMerchantPermissions()
	{
		return this.merchantPermissions;
	}

	public void setMerchantPermissions(int merchantPermissions)
	{
		this.merchantPermissions = merchantPermissions;
	}

	public void updateMerchantPermissions(boolean addFlag, int flag)
	{
		if (addFlag)
		{ // Add the flag
			this.merchantPermissions |= flag;
		}
		else
		{// Removes the flag
			this.merchantPermissions &= ~flag;
		}
	}

	// #BEGIN Added by NM - Release 21
	public int getPaymentType()
	{
		return this.paymentType;
	}

	/* DBSY-1072 eAccounts Interface */
	public void setDEntityAccountType(String _entityAccountType)
	{
		if (_entityAccountType == null)
		{
			_entityAccountType = "";
		}
		this.dentityAccountType = _entityAccountType;
	}

	public void setPaymentType(int paymentType)
	{
		this.paymentType = paymentType;
	}

	public int getProcessType()
	{
		return this.processType;
	}

	public void setProcessType(int processType)
	{
		this.processType = processType;
	}

	// #END Added by NM - Release 21

	/* BEGIN MiniRelease 2.0 - Added by LF */
	public int getContactTypeId()
	{
		return this.contactTypeId;
	}

	public void setContactTypeId(int contactTypeId)
	{
		this.contactTypeId = contactTypeId;
	}

	public String getContactCellPhone()
	{
		return StringUtil.toString(this.contactCellPhone);
	}

	public void setContactCellPhone(String contactCellPhone)
	{
		this.contactCellPhone = contactCellPhone;
	}

	public String getContactDepartment()
	{
		return StringUtil.toString(this.contactDepartment);
	}

	public void setContactDepartment(String contactDepartment)
	{
		this.contactDepartment = contactDepartment;
	}

	public Vector getVecContacts()
	{
		return this.vecContacts;
	}

	public void setVecContacts(Vector vecContacts)
	{
		this.vecContacts = vecContacts;
	}

	public int getMerchantRouteId()
	{
		return this.merchantRouteId;
	}

	public void setMerchantRouteId(int merchantRouteId)
	{
		this.merchantRouteId = merchantRouteId;
	}

	public String getMerchantRouteName()
	{
		return StringUtil.toString(this.merchantRouteName);
	}

	public void setMerchantRouteName(String merchantRouteName)
	{
		this.merchantRouteName = merchantRouteName;
	}

	public int getRegion()
	{
		return this.region;
	}

	public void setRegion(int region)
	{
		this.region = region;
	}

	public int getInvoice()
	{
		return this.invoice;
	}

	public void setInvoice(int invoice)
	{
		this.invoice = invoice;
	}

	/* END Release 14.0 - Added by NM */
	public String getOldRepId()
	{
		return this.oldRepId;
	}

	public void setOldRepId(String oldRepId)
	{
		this.oldRepId = oldRepId;
	}

	public String getCurrentMerchantType()
	{
		return this.currentMerchantType;
	}

	public void setCurrentMerchantType(String currentMerchantType)
	{
		this.currentMerchantType = currentMerchantType;
	}

	public String getStartDate()
	{
		return StringUtil.toString(this.startDate);
	}

	public void setStartDate(String strValue)
	{
		this.startDate = strValue;
	}

	public String getEndDate()
	{
		return StringUtil.toString(this.endDate);
	}

	public void setEndDate(String strValue)
	{
		this.endDate = strValue;
	}

	public String getTerminalType()
	{
		return this.terminalType;
	}

	public void setTerminalType(String terminalType)
	{
		this.terminalType = terminalType;
	}

	public String[] getProductId()
	{
		return this.productId;
	}

	public void setProductId(String[] productId)
	{
		this.productId = productId;
	}

	public String getDba()
	{
		return StringUtil.toString(this.dba);
	}

	public void setDba(String strValue)
	{
		this.dba = strValue;
	}

	public String getBusinessName()
	{
		return StringUtil.toString(this.businessName);
	}

	public void setBusinessName(String strValue)
	{
		this.businessName = strValue;
	}

	public String getContactName()
	{
		return StringUtil.toString(this.contactName);
	}

	public void setContactName(String strValue)
	{
		this.contactName = strValue;
	}

	public String getContactPhone()
	{
		return StringUtil.toString(this.contactPhone);
	}

	public void setContactPhone(String strValue)
	{
		this.contactPhone = strValue;
	}

	public String getContactFax()
	{
		return StringUtil.toString(this.contactFax);
	}

	public void setContactFax(String strValue)
	{
		this.contactFax = strValue;
	}

	public String getsalesmanname()
	{
		return StringUtil.toString(this.salesmanname);
	}

	public void setsalesmanname(String temp)
	{
		this.salesmanname = temp;
	}

	public String getsalesmanid()
	{
		return StringUtil.toString(this.salesmanid);
	}

	public String getterms()
	{
		return StringUtil.toString(this.terms);
	}

	public String getsalesmannamebyid(String id, String isoId)
	{

		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String vecResults = "";
		String strSQL = Merchant.sql_bundle.getString("getsalesmannamebyid");

		try
		{
			dbConn = Torque.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");

			}

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, id);
			pstmt.setString(2, isoId);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next())
			{
				vecResults = rs.getString("salesmanname");
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("getsalesmannamebyid => Error during gets2kstockcode", e);

		}
		finally
		{
			try
			{
				if (pstmt != null)
				{
					pstmt.close();
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				Merchant.cat.error("getsalesmannamebyid => Error during closeConnection ", e);
			}
		}
		return vecResults;
	}

	public void setsalesmanid(String salesmanid)
	{
		this.salesmanid = salesmanid;
	}

	public void setterms(String terms)
	{
		this.terms = terms;
	}

	public boolean verifyid(String id, String isoId)
	{

		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String strSQL = Merchant.sql_bundle.getString("getsalesmannamebyid");

		try
		{
			dbConn = Torque.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");

			}

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, id);
			pstmt.setString(2, isoId);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next())
			{
				if (id.equals(rs.getString("salesmanid")))
				{
					return true;
				}
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("verifyid => Error ", e);

		}
		finally
		{
			try
			{
				if (pstmt != null)
				{
					pstmt.close();
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				Merchant.cat.error("verifyid => Error during closeConnection ", e);
			}
		}
		return false;
	}

	public String getPhysAddress()
	{
		return StringUtil.toString(this.physAddress);
	}

	public void setPhysAddress(String strValue)
	{
		this.physAddress = strValue;
	}

	public String getPhysCity()
	{
		return StringUtil.toString(this.physCity);
	}

	public void setPhysCity(String strValue)
	{
		this.physCity = strValue;
	}

	public String getPhysCounty()
	{
		return StringUtil.toString(this.physCounty);
	}

	public void setPhysCounty(String strValue)
	{
		this.physCounty = strValue;
	}

	public String getPhysState()
	{
		return StringUtil.toString(this.physState);
	}

	public void setPhysState(String strValue)
	{
		this.physState = strValue;
	}

	public String getPhysZip()
	{
		return StringUtil.toString(this.physZip);
	}

	public void setPhysZip(String strValue)
	{
		this.physZip = strValue;
	}

	public String getMailAddress()
	{
		return StringUtil.toString(this.mailAddress);
	}

	public void setMailAddress(String strValue)
	{
		this.mailAddress = strValue;
	}

	public String getMailCity()
	{
		return StringUtil.toString(this.mailCity);
	}

	public void setMailCity(String strValue)
	{
		this.mailCity = strValue;
	}

	public String getMailState()
	{
		return StringUtil.toString(this.mailState);
	}

	public void setMailState(String strValue)
	{
		this.mailState = strValue;
	}

	public String getMailZip()
	{
		return StringUtil.toString(this.mailZip);
	}

	public void setMailZip(String strValue)
	{
		this.mailZip = strValue;
	}

	public String getRunningLiability()
	{
		return this.runningLiability;
	}

	public void setRunningLiability(String strValue)
	{
		this.runningLiability = strValue;
	}

	public String getLiabilityLimit()
	{
		return this.liabilityLimit;
	}

	public void setLiabilityLimit(String strValue)
	{
		this.liabilityLimit = strValue;
	}

	public String getNoCreditCheck()
	{
		return this.noCreditCheck;
	}

	public void setNoCreditCheck(String strValue)
	{
		this.noCreditCheck = strValue;
	}

	public String getCreditLimit()
	{
		return this.creditLimit;
	}

	public void setCreditLimit(String strValue)
	{
		this.creditLimit = strValue;
	}

	public String getPaymentAmount()
	{
		return this.paymentAmount;
	}

	public void setPaymentAmount(String strValue)
	{
		this.paymentAmount = strValue;
	}

	public String getPaymentDescription()
	{
		return this.paymentDescription;
	}

	public void setPaymentDescription(String strValue)
	{
		this.paymentDescription = strValue;
	}

	public String getContactEmail()
	{
		return StringUtil.toString(this.contactEmail);
	}

	public void setContactEmail(String strValue)
	{
		this.contactEmail = strValue;
	}

	public String getRoutingNumber()
	{
		return StringUtil.toString(this.routingNumber);
	}

	public void setRoutingNumber(String routingNumber)
	{
		this.routingNumber = routingNumber;
	}

	public String getTaxId()
	{
		return StringUtil.toString(this.taxId);
	}

	public void setTaxId(String taxId)
	{
		this.taxId = taxId;
	}

	public String getAccountNumber()
	{
		return StringUtil.toString(this.accountNumber);
	}

	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}

	public void setRepId(String strValue)
	{
		this.repId = strValue;
	}

	public String getRepId()
	{
		return StringUtil.toString(this.repId);
	}

	public void setRepName(String strValue)
	{
		this.repName = strValue;
	}

	public String getRepName()
	{
		return StringUtil.toString(this.repName);
	}

	public void setRepRatePlanId(String strValue)
	{
		this.repRatePlanId = strValue;
	}

	public String getRepRatePlanId()
	{
		return StringUtil.toString(this.repRatePlanId);
	}

	public void setPhysCountry(String strValue)
	{
		this.physCountry = strValue;
	}

	public String getPhysCountry()
	{
		return StringUtil.toString(this.physCountry);
	}

	public boolean isAchAccount()
	{
		return this.m_isAchAccount;
	}

	public void setIsAchAccount(boolean achAccount)
	{
		this.m_isAchAccount = achAccount;
	}

	/**
	 * Returns the unique merchantId which identifies each merchant record.
	 * 
	 * @return merchantId
	 */
	public String getMerchantId()
	{
		return StringUtil.toString(this.merchantId);
	}

	/**
	 * Sets the unique merchantId which identifies each merchant record.
	 */
	public void setMerchantId(String strValue)
	{
		this.merchantId = strValue;
	}

	public String getSiteId()
	{
		return StringUtil.toString(this.siteId);
	}

	public void setSiteId(String strValue)
	{
		this.siteId = strValue;
	}

	public String getMerchantType()
	{
		return StringUtil.toString(this.merchantType);
	}

	public void setMerchantType(String strValue)
	{
		this.merchantType = strValue;
	}

	public String getNetPaymentAmount()
	{
		return this.netPaymentAmount;
	}

	public void setNetPaymentAmount(String strValue)
	{
		this.netPaymentAmount = strValue;
	}

	public String getCommission()
	{
		return this.commission;
	}

	public void setCommission(String strValue)
	{
		this.commission = strValue;
	}

	public String getMonthlyFee()
	{
		return this.monthlyFee;
	}

	public void setMonthlyFee(String monthlyFee)
	{
		this.monthlyFee = monthlyFee;
	}

	public String getMonthlyFeeThreshold()
	{
		return this.monthlyFeeThreshold;
	}

	public void setAnnualInsurance(String var)
	{
		this.annualInsurance = var;
	}

	public String getAnnualInsurance()
	{
		return this.annualInsurance;
	}

	public void setAnnualMaintenance(String var)
	{
		this.annualMaintenance = var;
	}

	public String getAnnualMaintenace()
	{
		return this.annualMaintenance;
	}

	public void setReconnection(String var)
	{
		this.reconnection = var;
	}

	public String getReconnection()
	{
		return this.reconnection;
	}

	public void setMonthlyFeeThreshold(String monthlyFeeThreshold)
	{
		this.monthlyFeeThreshold = monthlyFeeThreshold;
	}

	public String getAchSchedule()
	{
		return StringUtil.toString(this.achSchedule);
	}

	public void setAchSchedule(String achSchedule)
	{
		this.achSchedule = achSchedule;
	}

	public String getAchScheduleType()
	{
		return StringUtil.toString(this.achScheduleType);
	}

	public void setAchScheduleType(String achScheduleType)
	{
		this.achScheduleType = achScheduleType;
	}

	/* BEGIN MiniRelease 1.0 - Added by LF */
	public String getBusinessTypeId()
	{
		return this.businessTypeId;
	}

	public void setBusinessTypeId(String businessTypeId)
	{
		this.businessTypeId = businessTypeId;
	}

	public int getMerchantSegmentId()
	{
		return this.merchantSegmentId;
	}

	public void setMerchantSegmentId(int merchantSegmentId)
	{
		this.merchantSegmentId = merchantSegmentId;
	}

	public String getBusinessHours()
	{
		return StringUtil.toString(this.businessHours);
	}

	public void setBusinessHours(String businessHours)
	{
		this.businessHours = businessHours;
	}

	public String getPhysColony()
	{
		return StringUtil.toString(this.physColony);
	}

	public void setPhysColony(String physColony)
	{
		this.physColony = physColony;
	}

	public String getPhysDelegation()
	{
		return StringUtil.toString(this.physDelegation);
	}

	public void setPhysDelegation(String physDelegation)
	{
		this.physDelegation = physDelegation;
	}

	public String getMailColony()
	{
		return StringUtil.toString(this.mailColony);
	}

	public void setMailColony(String mailColony)
	{
		this.mailColony = mailColony;
	}

	public String getMailDelegation()
	{
		return StringUtil.toString(this.mailDelegation);
	}

	public void setMailDelegation(String mailDelegation)
	{
		this.mailDelegation = mailDelegation;
	}

	public String getMailCounty()
	{
		return StringUtil.toString(this.mailCounty);
	}

	public void setMailCounty(String mailCounty)
	{
		this.mailCounty = mailCounty;
	}

	public String getMailCountry()
	{
		return StringUtil.toString(this.mailCountry);
	}

	public void setMailCountry(String mailCountry)
	{
		this.mailCountry = mailCountry;
	}

	public String getBankName()
	{
		return StringUtil.toString(this.bankName);
	}

	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	public int getAccountTypeId()
	{
		return this.accountTypeId;
	}

	public void setAccountTypeId(int accountTypeId)
	{
		this.accountTypeId = accountTypeId;
	}

	public int getCreditTypeId()
	{
		return this.creditTypeId;
	}

	public void setCreditTypeId(int creditTypeId)
	{
		this.creditTypeId = creditTypeId;
	}

	/* END MiniRelease 1.0 - Added by LF */

	/* BEGIN MiniRelease 1.0 - Added by YH */
	public String getDepositDate()
	{
		return this.depositDate;
	}

	public void setDepositDate(String strValue)
	{
		this.depositDate = strValue;
	}

	public String getBank()
	{
		return this.bank;
	}

	public void setBank(String strValue)
	{
		this.bank = strValue;
	}

	public String getRefNumber()
	{
		return this.refNumber;
	}

	public void setRefNumber(String strValue)
	{
		this.refNumber = strValue;
	}

	/* END MiniRelease 1.0 - Added by YH */

	/* BEGIN Release 9.0 - Added by LF */
	/**
	 * @param sharedBalance
	 *            The sharedBalance to set.
	 */
	public void setSharedBalance(String sharedBalance)
	{
		this.sharedBalance = sharedBalance;
	}

	/**
	 * @return Returns the sharedBalance.
	 */
	public String getSharedBalance()
	{
		return this.sharedBalance;
	}

	/**
	 * @param newMerchantType
	 *            The newMerchantType to set.
	 */
	public void setNewMerchantType(String newMerchantType)
	{
		this.newMerchantType = newMerchantType;
	}

	/**
	 * @return Returns the newMerchantType.
	 */
	public String getNewMerchantType()
	{
		return this.newMerchantType;
	}

	/* END Release 9.0 - Added by LF */

	/* R31.1 Billing ABernal-EMunoz */
	public int getRegulatoryFeeId()
	{
		return this.regulatoryFeeId;
	}

	public void setRegulatoryFeeId(int strValue)
	{
		this.regulatoryFeeId = strValue;
	}

	public int getRecId()
	{
		return this.rec_id;
	}

	public void setRecId(int strValue)
	{
		this.rec_id = strValue;
	}

	public String getRegulatoryFeeName()
	{
		return StringUtil.toString(this.regulatoryFeeName);
	}

	public void setRegulatoryFeeName(String strValue)
	{
		this.regulatoryFeeName = strValue;
	}

	/* End R31.1 Billing ABernal-EMunoz */

	/**
	 * Returns a hash of errors.
	 * 
	 * @return Hash of errors.
	 */
	public Hashtable getErrors()
	{
		return this.validationErrors;
	}

	public boolean isError()
	{
		return (this.validationErrors.size() > 0);
	}

	/**
	 * Returns a validation error against a specific field. If a field was found
	 * to have an error during {@link #validateMerchant validateMerchant}, the
	 * error to have an error during {@link #validateMerchant validateMerchant},
	 * the message can be accessed via this method.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @return The error message for the field or null if none is present.
	 */
	public String getFieldError(String fieldname)
	{
		return ((String) this.validationErrors.get(fieldname));
	}

	/**
	 * Sets the validation error against a specific field. Used by
	 * {@link #validateMerchant validateMerchant}. {@link #validateMerchant
	 * validateMerchant}.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @param error
	 *            The error message for the field or null if none is present.
	 */
	public void addFieldError(String fieldname, String error)
	{
		this.validationErrors.put(fieldname, error);
	}

	public boolean validate(SessionData sessionData, Hashtable hashRepRates, ServletContext context)
	{
		boolean valid = true;
		
		if (!this.validateMerchant(sessionData, context))
		{
			valid = false;
		}

		String deploymentType = DebisysConfigListener.getDeploymentType(context);
		String strAccessLevel = sessionData.getProperty("access_level");
		String strRefId = sessionData.getProperty("ref_id");

		// special here because credit limit is only set on add merchant not
		// update merchant
		if ((this.creditLimit == null) || (!NumberUtil.isNumeric(this.creditLimit)))
		{
			this.addFieldError("creditLimit", Languages.getString("com.debisys.customers.credit_limit_error",
					sessionData.getLanguage()));
			valid = false;
		}

                /*
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && Integer.parseInt(this.creditLimit) == 0
				&& sessionData.getUser().isQcommBusiness())
		{
			this.creditLimit = "0.1";
		}
                */

		// if its international we need to check the rep to make sure rep has
		// enough credit
		if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
			if (!this.checkMerchantControlNumber(sessionData, context))
			{
				this.addFieldError("control_number", Languages.getString(
						"jsp.admin.customers.merchants_control_number_error", sessionData.getLanguage()));
				valid = false;
			}
		}

		// first check the rep to see if we even need to check it
		Rep rep = new Rep();

		try
		{
			if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
					|| strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				rep.setRepId(this.repId);
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				rep.setRepId(strRefId);
			}

			rep.getRep(sessionData, context);
			// this is so I can access this from addMerchant
			this.repCreditType = rep.getRepCreditType();
		}
		catch (Exception e)
		{
		}

		// loaded up with rep info now lets do the check
		if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
		{
			// this means the rep is prepaid or credit based
			// so check to make sure there is available credit
			if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
			{
				// this is an error
				// cant assign an unlimited merchant to a limited rep
				this.addFieldError("merchantType", Languages.getString("com.debisys.customers.rep_credit_error3",
						sessionData.getLanguage()));
				valid = false;
			}
			else
			{
				// check the credit limit to make sure there is enough
				if (Double.parseDouble(rep.getAvailableCredit()) < Double.parseDouble(this.creditLimit))
				{
					this.addFieldError("creditLimit", Languages.getString("com.debisys.customers.rep_credit_error4",
							sessionData.getLanguage())
							+ NumberUtil.formatAmount(rep.getAvailableCredit()));
					valid = false;
				}
			}
		}
		// }

		if ((this.monthlyFeeThreshold == null) || (!NumberUtil.isNumeric(this.monthlyFeeThreshold)))
		{
			this.addFieldError("monthlyFeeThreshold", Languages.getString("com.debisys.customers.rep_credit_error5",
					sessionData.getLanguage()));
			valid = false;
		}

		// bugfix parature 7753455 (String Validation - charset encoding)
		Map<String, String> badChars = null;
		String[] validateOnlyThese = new String[]
		{ "dba", "businessname" };
		if ((badChars = CharsetValidator.validateStringFields(this, context, validateOnlyThese)) != null)
		{
			valid = false;
			for (String fieldName : badChars.keySet())
			{
				this.addFieldError(fieldName, badChars.get(fieldName));
			}
		}

		return valid;
	}

	/**
	 * Validates if the Merchant Control Number entered is valid (apply only for
	 * Mexico)
	 * 
	 * @param sessionData
	 * @param context
	 * @return
	 */
	public boolean checkMerchantControlNumber(SessionData sessionData, ServletContext context)
	{
		boolean blValidControlNumber = true;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (this.controlNumber == null || "".equals(this.controlNumber))
		{
			return true;
		}
		try
		{
			dbConn = Torque.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = "select count(*) from merchants where control_number = '"
					+ StringUtil.escape(this.controlNumber) + "'";
			if (this.merchantId != null && !"".equals(this.merchantId))
			{
				strSQL += " and merchant_id <>" + StringUtil.escape(this.merchantId);
			}

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			rs.next();
			if (rs.getInt(1) > 0)
			{
				blValidControlNumber = false;
			}

		}
		catch (Exception e)
		{
			blValidControlNumber = false;
			Merchant.cat.error("Error during checkMerchantControlNumber", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return blValidControlNumber;
	}

	/**
	 * Validates the merchant for missing or invalid fields.
	 * 
	 * @return <code>true</code> if the entity passes the validation checks,
	 *         otherwise <code>false</code> otherwise <code>false</code>
	 */
	public boolean validateMerchant(SessionData sessionData, ServletContext context)
	{
		String deploymentType = DebisysConfigListener.getDeploymentType(context);
		this.validationErrors.clear();

		boolean valid = true;
		String strAccessLevel = sessionData.getProperty("access_level");

		if ((this.repId == null) || (this.repId.length() == 0))
		{
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				this.addFieldError("repId", Languages.getString("jsp.admin.customers.merchants_edit.error_rep",
						sessionData.getLanguage()));
				valid = false;
			}
		}
		else if (!NumberUtil.isNumeric(this.repId))
		{
			this.addFieldError("repId", Languages.getString("jsp.admin.customers.merchants_edit.error_rep_id",
					sessionData.getLanguage()));
			valid = false;
		}
		else
		{
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				// make sure this is a valid rep for this agent or iso
				if (!this.checkRepPermission(sessionData))
				{
					this.addFieldError("repId", Languages.getString("jsp.admin.customers.merchants_edit.error_rep_id2",
							sessionData.getLanguage()));
					valid = false;
				}
			}
		}

		if ((this.dba == null) || (this.dba.length() == 0))
		{
			this.addFieldError("dba", Languages.getString("jsp.admin.customers.merchants_edit.error_dba", sessionData
					.getLanguage()));
			valid = false;
		}
		else if (this.dba.length() > 50)
		{
			this.addFieldError("dba", Languages.getString("jsp.admin.customers.merchants_edit.error_dba_length",
					sessionData.getLanguage()));
			valid = false;
		}

		if ((this.businessName == null) || (this.businessName.length() == 0))
		{
			this.addFieldError("businessName", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_business_name", sessionData.getLanguage()));
			valid = false;
		}
		else if (this.businessName.length() > 50)
		{
			this.addFieldError("businessName", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_business_name_length", sessionData.getLanguage()));
			valid = false;
		}

		if ((this.physAddress == null) || (this.physAddress.length() == 0))
		{
			this.addFieldError("physAddress", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_street_address", sessionData.getLanguage()));
			valid = false;
		}
		else if (this.physAddress.length() > 50)
		{
			this.addFieldError("physAddress", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_street_address_length", sessionData.getLanguage()));
			valid = false;
		}

		//
		String mandatoryMerchantClassification = DebisysConfigListener.getMandatoryMerchantClassification(context);

		if (mandatoryMerchantClassification.trim().equals("1") && (merchantClassification == 0))
		{
			this.addFieldError("merchantClassification", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_merchantClassification", sessionData.getLanguage()));
			valid = false;
		}
		else if (mandatoryMerchantClassification.trim().equals("1") && (merchantClassification != 0))
		{
			boolean hasMerchantOtherDescription = getMerchantClassificationById(merchantClassification);
			if (hasMerchantOtherDescription
					&& (merchantClassificationOtherDesc == null || merchantClassificationOtherDesc.trim().length() == 0))
			{
				this.addFieldError("merchantClassificationOtherDesc", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_merchantClassificationOtherDesc", sessionData
								.getLanguage()));
				valid = false;
			}
		}

		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
			if ((this.physCity == null) || (this.physCity.length() == 0))
			{
				this.addFieldError("physCity", Languages.getString("jsp.admin.customers.merchants_edit.error_city",
						sessionData.getLanguage()));
				valid = false;
			}
			else if (this.physCity.length() > 50)
			{
				this.addFieldError("physCity", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_city_length", sessionData.getLanguage()));
				valid = false;
			}

			if ((this.physState == null) || (this.physState.length() == 0))
			{
				this.addFieldError("physState", Languages.getString("jsp.admin.customers.merchants_edit.error_state",
						sessionData.getLanguage()));
				valid = false;
			}
			else if (this.physState.length() > 2)
			{
				this.addFieldError("physState", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_state_length", sessionData.getLanguage()));
				valid = false;
			}

			if ((this.physZip == null) || (this.physZip.length() == 0))
			{
				this.addFieldError("physZip", Languages.getString("jsp.admin.customers.merchants_edit.error_zip",
						sessionData.getLanguage()));
				valid = false;
			}
			else if (this.physZip.length() > 50)
			{
				this.addFieldError("physZip", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_zip_length", sessionData.getLanguage()));
				valid = false;
			}

			this.contactPhone = this.contactPhone.replaceAll(Merchant.PHONE_CLEAN_REGEXP, "");
			this.contactPhone = Merchant.formatPhone(context, this.contactPhone);
			if ((this.contactPhone != null) && (this.contactPhone.trim().length() > 0)
					&& (!this.contactPhone.matches(DebisysConfigListener.getPhoneRegExp(context))))
			{
				this.addFieldError("contactPhone", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_phone_format", sessionData.getLanguage())
						+ DebisysConfigListener.getPhoneFormatText(context));
				valid = false;
			}
			if (this.contactFax != null && !this.contactFax.trim().equals(""))
			{
				this.contactFax = this.contactFax.replaceAll(Merchant.PHONE_CLEAN_REGEXP, "");
				this.contactFax = Merchant.formatPhone(context, this.contactFax);
				if ((this.contactFax != null) && (this.contactFax.trim().length() > 0)
						&& (!this.contactFax.matches(DebisysConfigListener.getPhoneRegExp(context))))
				{
					this.addFieldError("contactFax", Languages.getString(
							"jsp.admin.customers.merchants_edit.error_fax_format", sessionData.getLanguage())
							+ DebisysConfigListener.getPhoneFormatText(context));
					valid = false;
				}
			}
			else
			{
				this.contactFax = " ";
			}

		}
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
		{
			if ((this.physCountry == null) || (this.physCountry.length() == 0))
			{
				this.addFieldError("physCountry", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_country", sessionData.getLanguage()));
				valid = false;
			}
			else if (this.physCountry.length() > 50)
			{
				this.addFieldError("physCountry", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_country_length", sessionData.getLanguage()));
				valid = false;
			}
		}

		if ((this.physCounty != null) && (this.physCounty.length() > 20))
		{
			this.addFieldError("physCounty", Languages.getString("jsp.admin.customers.merchants_edit.error_county", sessionData.getLanguage()));
			valid = false;
		}
		else if (this.physCounty == "") //this.physCounty.equals(""))
		{
			this.addFieldError("physCounty", Languages.getString("jsp.admin.customers.merchants_edit.error_county", sessionData.getLanguage()));
			valid = false;
		}

		if (!DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
			if ((this.contactName == null) || (this.contactName.length() == 0))
			{
				this.addFieldError("contactName", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_contact", sessionData.getLanguage()));
				valid = false;
			}
			else if (this.contactName.length() > 50)
			{
				this.addFieldError("contactName", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_contact_length", sessionData.getLanguage()));
				valid = false;
			}

			if ((this.contactPhone == null) || (this.contactPhone.length() == 0))
			{
				this.addFieldError("contactPhone", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_phone", sessionData.getLanguage()));
				valid = false;
			}
			else if (this.contactPhone.length() > 15)
			{
				this.addFieldError("contactPhone", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_phone_length", sessionData.getLanguage()));
				valid = false;
			}
		}
		if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
			if (!this.checkMerchantControlNumber(sessionData, context))
			{
				this.addFieldError("control_number", Languages.getString(
						"jsp.admin.customers.merchants_control_number_error", sessionData.getLanguage()));
				valid = false;
			}
		}

		if ((this.contactFax != null) && (this.contactFax.length() > 15))
		{
			this.addFieldError("contactFax", Languages.getString("jsp.admin.customers.merchants_edit.error_fax_length",
					sessionData.getLanguage()));
			valid = false;
		}

		if ((this.contactEmail != null) && (this.contactEmail.length() > 200))
		{
			this.addFieldError("contactEmail", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_email_length", sessionData.getLanguage()));
			valid = false;
		}
		else if (this.contactEmail != null)
		{
			// commented out because we have multiple emails in a field, i.e.
			// aaa@aaa.com;bbb@bbb.com
			/*
			 * ValidateEmail validateEmail = new ValidateEmail();
			 * validateEmail.setStrEmailAddress(contactEmail); ValidateEmail
			 * validateEmail = new ValidateEmail();
			 * validateEmail.setStrEmailAddress(contactEmail);
			 * validateEmail.checkSyntax(); if (!validateEmail.isEmailValid()) {
			 * addFieldError("contactEmail",
			 * "Please enter a valid email address."); valid = false; }
			 * address."); valid = false; }
			 */
		}                

		// June 6, 2006 - added by jay to make ach info required
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
			if (((this.taxId == null) || this.taxId.equals("")) && 
                            sessionData.checkPermission(DebisysConstants.PERM_EDIT_BANKING_INFO_MERCHANT) )
			{
                            this.addFieldError("taxId", Languages.getString("jsp.admin.customers.merchants_edit.error_tax_id",
                                            sessionData.getLanguage()));
                            valid = false;                                
			}
                        else
                        {
                            Pattern p = Pattern.compile("^(?!(\\d)\\1+$|(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){8}\\d$)\\d{9}$");
                            Matcher match = p.matcher(this.taxId);
                            if (!match.matches()){
                                this.addFieldError("taxId", Languages.getString(
                                                    "jsp.admin.customers.merchants_edit.error_tax_number_length", sessionData.getLanguage()));
                                    valid = false;
                            }
                        }
			if (((this.routingNumber == null) || this.routingNumber.equals("")))
			{
				// addFieldError("routingNumber",
				// Languages.getString("jsp.admin.customers.merchants_edit.error_routing_number",
				// sessionData.getLanguage()));
				// valid = false;
				this.routingNumber = "NA";
			}

			// ********************************************//
			// The routing number for banking information needs to be limited to
			// 9 characters for all levels.
			// R30 Jorge Nicol�s Mart�nez Romero
			// All report headers should be exportable
			// only numbers or letter, min 9 numbers
			// Pattern patt = Pattern.compile("[0-9a-zA-Z]{9}");
			Pattern patt = Pattern.compile("[0-9]{0,9}");
			Matcher m = patt.matcher(this.routingNumber);
			if (!m.matches() && !this.routingNumber.equals("NA"))
			{
				this.addFieldError("routingNumber", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_routing_number", sessionData.getLanguage()));
				valid = false;
			}
			// *****************************************************//

			if (((this.accountNumber == null) || this.accountNumber.equals("")))
			{
				// addFieldError("accountNumber",
				// Languages.getString("jsp.admin.customers.merchants_edit.error_account_number",
				// sessionData.getLanguage()));
				// valid = false;
				this.accountNumber = "NA";
			}
		}

		if ((this.taxId != null) && (this.taxId.length() > 15))
		{
			this.addFieldError("taxId", Languages.getString("jsp.admin.customers.merchants_edit.error_tax_id_length",
					sessionData.getLanguage()));
			valid = false;
		}

		if ((this.routingNumber != null) && (this.routingNumber.length() > 18))
		{
			this.addFieldError("routingNumber", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_routing_number_length", sessionData.getLanguage()));
			valid = false;
		}

		if ((this.accountNumber != null) && (this.accountNumber.length() > 20))
		{
			this.addFieldError("accountNumber", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_account_number_length", sessionData.getLanguage()));
			valid = false;
		}

		if ((this.monthlyFeeThreshold == null) || (!NumberUtil.isNumeric(this.monthlyFeeThreshold)))
		{
			this.addFieldError("monthlyFeeThreshold", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_monthly_fee", sessionData.getLanguage()));
			valid = false;
		}

		if (this.reservedPINCacheBalance < 0 || this.reservedPINCacheBalance > 100)
		{
			this.addFieldError("reservedPINCacheBalance", Languages.getString(
					"jsp.admin.customers.merchants_edit.error_reservedpincachebalance", sessionData.getLanguage()));
			valid = false;
		}
		else
		{
			try
			{
				if (this.reservedPINCacheBalance > this.getGlobalReservedPINCacheBalance())
				{
					this.addFieldError("globalPINCacheBalance", Languages
							.getString("jsp.admin.customers.merchants_edit.error_globalpincachebalance", sessionData
									.getLanguage())
							+ this.getGlobalReservedPINCacheBalance() + "%");
					valid = false;
				}
			}
			catch (Exception ex)
			{
				this.addFieldError("invalidGlobalPINCacheBalance", Languages.getString(
						"jsp.admin.customers.merchants_edit.error_invalidglobalpincachebalance", sessionData
								.getLanguage()));
				valid = false;
			}
		}

		// need to check to make sure reps werent changed
		/*
		 * if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
		 * {
		 */

		// if this is null then its an add or a rep updating
		// should be more sophisticated but ran out of time
		if ((this.oldRepId != null) && !this.oldRepId.equals(this.repId))
		{
			// rep changed so lets make sure new rep can handle it
			try
			{
				Rep oldRep = new Rep();
				oldRep.setRepId(this.oldRepId);
				oldRep.getRep(sessionData, context);

				Rep newRep = new Rep();
				newRep.setRepId(this.repId);
				newRep.getRep(sessionData, context);

				/*
				 * if
				 * (!oldRep.getRepCreditType().equals(newRep.getRepCreditType())
				 * && if
				 * (!oldRep.getRepCreditType().equals(newRep.getRepCreditType())
				 * &&!newRep.getRepCreditType().equals(DebisysConstants.
				 * REP_CREDIT_TYPE_UNLIMITED)) {
				 * //com.debisys.customers.rep_credit_error1=To transfer a
				 * merchant to a new rep, rep types must be the same.
				 * addFieldError("repCredit", merchant to a new rep, rep types
				 * must be the same. addFieldError("repCredit",
				 * Languages.getString
				 * ("com.debisys.customers.rep_credit_error1")); valid = false;
				 * } //new unlimited rep is ok else valid = false; } //new
				 * unlimited rep is ok else
				 */

				Merchant tempMerchant = new Merchant();
				tempMerchant.setMerchantId(this.merchantId);
				tempMerchant.getMerchant(sessionData, context);

				if (!oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
				{

					if (this.creditLimit.equals("")
							&& (DebisysConfigListener.getCustomConfigType(context).equals(
									DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)))
					{
						this.creditLimit = tempMerchant.getLiabilityLimit();
					}

					// old.credit -> new.credit
					if (newRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT))
					{
						// check if rep has enough available
						// and transfer the entire merchant liability limit
						if (Double.parseDouble(newRep.getAvailableCredit()) < Double.parseDouble(tempMerchant
								.getLiabilityLimit()))
						{
							this.addFieldError("repCredit", Languages.getString(
									"com.debisys.customers.rep_credit_error2", sessionData.getLanguage()));
							valid = false;
						}
					}
					// old.prepaid -> new.prepaid
					else if (newRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& !newRep.getSharedBalance().equals("1"))
					{
						// check if rep has enough available
						// and transfer the only merchant's available credit
						double availMerchantCredit = Double.parseDouble(tempMerchant.getLiabilityLimit())
								- Double.parseDouble(tempMerchant.getRunningLiability());

						if (Double.parseDouble(newRep.getAvailableCredit()) < availMerchantCredit)
						{
							this.addFieldError("repCredit", Languages.getString(
									"com.debisys.customers.rep_credit_error2", sessionData.getLanguage()));
							valid = false;
						}
					}
				}

				/*
				 * else { //com.debisys.customers.rep_credit_error1=To transfer
				 * a merchant to a new rep, rep types must be the same.
				 * addFieldError("repCredit", a merchant to a new rep, rep types
				 * must be the same. addFieldError("repCredit",
				 * Languages.getString
				 * ("com.debisys.customers.rep_credit_error1")); valid = false;
				 * } valid = false; }
				 */

				// DBSYS- 613 Avoiding the move from unlimited merchant to
				// limited Rep
				if (oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
				{
					if ((newRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT) || newRep
							.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID))
							&& tempMerchant.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
					{
						this.addFieldError("repCredit", Languages.getString("com.debisys.customers.rep_credit_error7",
								sessionData.getLanguage()));
						valid = false;
					}

				}

				// Validate credit type and credit limit
				if (DebisysConfigListener.getCustomConfigType(context).equals(
						DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
					if (!this.newMerchantType.equals(DebisysConstants.MERCHANT_TYPE_SHARED))
					{
						if (!this.checkRepCreditLimit(sessionData, context))
						{
							this.addFieldError("RepCreditLimit", Languages.getString(
									"jsp.admin.customers.merchant.repchange_notavailablecredit", sessionData
											.getLanguage()));
							valid = false;
						}
					}
				} // End of validate credit type and credit limit
			}
			catch (Exception e)
			{
			}
		}

		/* } */
		// Double exec? this will execute and validate later on validate()
		// bugfix parature 7753455 (String Validation - charset encoding)
		// Map<String, String> badChars = null;
		// if ((badChars = CharsetValidator.validateStringFields(this)) != null)
		// {
		// valid = false;
		// for (String fieldName : badChars.keySet())
		// {
		// addFieldError(fieldName, badChars.get(fieldName));
		// }
		// }
		return valid;
	}

	public JSONObject getMerchantClassificationByIdAjax(String merchantClassificationId, SessionData sessionData)
			throws CustomerException
	{

		JSONObject json = new JSONObject();
		try
		{
			boolean resp = getMerchantClassificationById(Integer.parseInt(merchantClassificationId));
			HashMap map = new HashMap();
			Vector jsonVec = new Vector();
			map.put("hasClassificationDescription", resp);
			jsonVec.add(map);

			json.put("items", jsonVec);
		}
		catch (Exception e)
		{
			cat.error("Error during getMerchantClassificationByIdAjax", e);
			throw new CustomerException();
		}

		return json;
	}

	public boolean getMerchantClassificationById(int merchantClassificationId)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			String strSQL = "SELECT * FROM MerchantClassifications WHERE id = ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setInt(1, merchantClassificationId);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				return rs.getBoolean("hasMerchantOtherDescription");
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantClassificationById", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection (getMerchantClassificationById)", e);
			}
		}
		return false;
	}

	private String getClerkNameForCode(String code, String siteID)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String name = "";
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			String strSQL = Merchant.sql_bundle.getString("getClerkNameForCode");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, code);
			pstmt.setString(2, siteID);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				name = rs.getString("name");
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getClerkNameForCode", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return name;
	}

	/**
	 * Given an merchant_id, populates merchant object populated from the
	 * database, or null if no such merchant_id exists. database, or null if no
	 * such merchant_id exists.
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public void getMerchant(SessionData sessionData, ServletContext context) throws CustomerException
	{
		if (this.checkPermission(sessionData))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				String strAccessLevel = sessionData.getProperty("access_level");
				String strDistChainType = sessionData.getProperty("dist_chain_type");
				String strRefId = sessionData.getProperty("ref_id");

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				String strSQL = Merchant.sql_bundle.getString("getMerchant");

				if (strAccessLevel.equals(DebisysConstants.CARRIER))
				{
					strSQL = strSQL + " INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.Merchant_ID = mc.MerchantID";
					strSQL = strSQL + " INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID";
					strSQL = strSQL + " WHERE rc.RepID = " + StringUtil.escape(strRefId);
					strSQL = strSQL + " AND m.Merchant_ID = " + StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
					{
						strSQL = strSQL + " WHERE r.iso_id = " + StringUtil.escape(strRefId) + " and m.merchant_id = "
								+ StringUtil.escape(this.merchantId);
					}
					else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQL = strSQL + " WHERE r.rep_id in " + "(select rep_id from reps WITH(NOLOCK) where type="
								+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
								+ "(select rep_id from reps WITH(NOLOCK) where type="
								+ DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in "
								+ "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_AGENT
								+ " and iso_id=" + StringUtil.escape(strRefId) + "))) " + "and m.merchant_id = "
								+ StringUtil.escape(this.merchantId);
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQL = strSQL + " WHERE r.rep_id in " + "(select rep_id from reps WITH(NOLOCK) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
							+ "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
							+ " and iso_id= " + StringUtil.escape(strRefId) + "))" + "and m.merchant_id = "
							+ StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
				{
					strSQL = strSQL + " WHERE r.rep_id in " + "(select rep_id from reps WITH(NOLOCK) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")"
							+ " and m.merchant_id = " + StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.REP))
				{
					strSQL = strSQL + " WHERE r.rep_id = " + StringUtil.escape(strRefId) + " and m.merchant_id = "
							+ StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
				{
					strSQL = strSQL + " WHERE m.merchant_id = " + StringUtil.escape(strRefId);
				}
				else
				{
					this.addFieldError("error", "Sorry, you user has not level access.");
				}

				pstmt = dbConn.prepareStatement(strSQL);
				rs = pstmt.executeQuery();

				if (rs.next())
				{
					this.merchantId = rs.getString("merchant_id");
					this.oldRepId = rs.getString("rep_id");
					this.repId = rs.getString("rep_id");
					this.repName = rs.getString("businessname");
					this.dba = rs.getString("dba");
					this.businessName = rs.getString("legal_businessname");
					this.contactName = rs.getString("contact");
					this.contactEmail = rs.getString("email");
					this.contactPhone = (rs.getString("contact_phone") != null) ? rs.getString("contact_phone")
							.replaceAll(Merchant.PHONE_CLEAN_REGEXP, "") : "";
					this.contactFax = (rs.getString("contact_fax") != null) ? rs.getString("contact_fax").replaceAll(
							Merchant.PHONE_CLEAN_REGEXP, "") : "";
					this.physAddress = rs.getString("phys_address");
					this.physCity = rs.getString("phys_city");
					this.physCounty = rs.getString("phys_county");
					this.physState = StringUtil.toString(rs.getString("phys_state"));
					this.physZip = rs.getString("phys_zip");
					this.physCountry = rs.getString("phys_country_id");

					this.mailAddress = rs.getString("mail_address");
					this.mailCity = rs.getString("mail_city");
					this.mailState = StringUtil.toString(rs.getString("mail_state"));
					this.mailZip = rs.getString("mail_zip");
					this.mailCountry = rs.getString("mail_country_id");
					this.runningLiability = NumberUtil.formatAmount(rs.getString("RunningLiability"));
					this.liabilityLimit = NumberUtil.formatAmount(rs.getString("LiabilityLimit"));

					this.taxId = rs.getString("tax_id");
					this.routingNumber = rs.getString("aba");
					this.accountNumber = rs.getString("account_no");
					// cancelled field usurped for credit check flag
					this.noCreditCheck = rs.getString("cancelled");
					this.merchantType = rs.getString("merchant_type");
					this.monthlyFeeThreshold = NumberUtil.formatAmount(rs.getString("rentwaivedatamount"));

					/* BEGIN Release 14.0 - Added by NM */
					this.region = rs.getInt("Region");
					this.invoice = rs.getInt("Invoice");
					/* END Release 14.0 - Added by NM */

					this.printBillingAddress = rs.getBoolean("printBillingAddress");

					// R27. DBSY-570. Added by JA
					this.merchantClassification = rs.getInt("merchant_classification");
					this.controlNumber = rs.getString("control_number");

					// #BEGIN Added by NM - Release 21
					this.paymentType = rs.getInt("payment_type");
					this.processType = rs.getInt("payment_processor");

					// #END Added by NM - Release 21

					this.m_isAchAccount = rs.getBoolean("isAchAccount");

					Integer iRouteId = rs.getInt("RouteID");

					this.reservedPINCacheBalance = rs.getInt("ReservedPINCacheBalance");
					this.pinCacheEnabled = rs.getBoolean("PINCacheEnabled");
					this.timeZoneId = rs.getInt("TimeZoneID");
					this.creditTypeId = rs.getInt("CreditTypeId");
					if (this.timeZoneId == 0)
					{
						this.timeZoneId = Integer.parseInt(TimeZone.getDefaultTimeZone().get(0).toString());
					}

					// DBSY-931 System 2000
					String temp = rs.getString("salesman_id");
					if (StringUtils.isNotEmpty(temp))
					{
						if (this.verifyid(temp.trim(), Rep.getIsoID(strRefId, strAccessLevel)))
						{
							this.salesmanid = temp.trim();
							this.salesmanname = this.getsalesmannamebyid(temp.trim(), strRefId);
						}
					}
					String temp2 = rs.getString("terms");
					if (StringUtils.isNotEmpty(temp2))
					{
						this.terms = temp2;

					}

					this.executiveAccountId = rs.getInt("ExecutiveAccountId");
					this.salesLimitType = rs.getString("SalesLimitType");
					this.salesLimitRunnningLiability = rs.getDouble("SalesLimitRunningLiability");
					this.salesLimitLiabilityLimit = rs.getDouble("SalesLimitLiabilityLimit");
					this.salesLimitMonday = rs.getDouble("SalesLimitMonday");
					this.salesLimitTuesday = rs.getDouble("SalesLimitTuesday");
					this.salesLimitWednesday = rs.getDouble("SalesLimitWednesday");
					this.salesLimitThursday = rs.getDouble("SalesLimitThursday");
					this.salesLimitFriday = rs.getDouble("SalesLimitFriday");
					this.salesLimitSaturday = rs.getDouble("SalesLimitSaturday");
					this.salesLimitSunday = rs.getDouble("SalesLimitSunday");
					this.useRatePlanModel = rs.getBoolean("UseRatePlanModel");
					this.testAccountEnabled = rs.getBoolean("test_account_enabled");

					/* R31.1 Billing ABernal - EMunoz */
					this.regulatoryFeeId = rs.getInt("regulatory_fee_id");
					this.rec_id = rs.getInt("rec_id");
					this.regulatoryFeeName = rs.getString("name");
					/* End R31.1 Billing ABernal - EMunoz */

					// DBSY-1072 eAccounts Interface
					this.entityAccountType = rs.getInt("EntityAccountType");

					// Alfred - DBSY-1139
					this.merchantPermissions = rs.getInt("MerchantPermissions");
					
					this.merchantExternalOutsideHierarchy = rs.getInt("externalOutsideHierarchy");

					this.addressValidationStatus = rs.getInt("AddressValidationStatus");
					this.latitude = rs.getDouble("latitude");
					this.longitude = rs.getDouble("longitude");
					if (rs.getString("PaymentTypeForInvoiceGeneration") != null)
						this.paymentTypeForInvoiceGeneration = rs.getString("PaymentTypeForInvoiceGeneration");
					else
						this.paymentTypeForInvoiceGeneration = "-1";

					if (rs.getString("AccountNumberForInvoiceGeneration") != null)
						this.accountNumberForInvoiceGeneration = rs.getString("AccountNumberForInvoiceGeneration");
					else
						this.accountNumberForInvoiceGeneration = "";

					// DTU-369 Payment Notifications
					this.paymentNotifications = rs.getBoolean("payment_notifications");
					this.paymentNotificationType = rs.getInt("payment_notification_type");
					if (this.paymentNotificationType == 0)
					{
						this.paymentNotificationSMS = true;
					}
					else
					{
						if (this.paymentNotificationType == 1)
						{
							this.paymentNotificationMail = true;
						}
					}

					if (rs.getString("sms_notification_phone") == null)
						this.smsNotificationPhone = "";
					else
						this.smsNotificationPhone = rs.getString("sms_notification_phone");

					if (rs.getString("mail_notification_address") == null)
						this.mailNotificationAddress = "";
					else
						this.mailNotificationAddress = rs.getString("mail_notification_address");

					// DTU-1772 Capture of detailed Banking Information for
					// Precash settlement
					this.bankAddress = rs.getString("bank_address");
					this.bankCity = rs.getString("bank_city");
					this.bankState = rs.getString("bank_state");
					this.bankZip = rs.getString("bank_zip");
					this.bankContactName = rs.getString("bank_contact_name");
					this.bankContactPhone = rs.getString("bank_contact_phone");

					if (rs.getString("merchantOwnerId") != null)
					{
						this.merchantOwnerId = rs.getString("merchantOwnerId");
					}
					else
					{
						this.merchantOwnerId = "";
					}
					this.accountTypeId = rs.getInt("AccountTypeId");

					// DTU-480: Low Balance Alert Message SMS and/or email
					this.balanceNotifications = rs.getBoolean("balance_notifications");
					this.balanceNotificationType = rs.getInt("balance_notification_type");
					if (this.balanceNotificationType == 0)
					{
						this.balanceNotificationTypeValue = true;
					}
					else if (this.balanceNotificationType == 1)
					{
						this.balanceNotificationTypeDays = true;
					}
					else if (this.balanceNotificationType == 2)
					{
						this.balanceNotificationTypeValue = true;
						this.balanceNotificationTypeDays = true;
					}
					this.balanceNotificationValue = rs.getDouble("balance_notification_value");
					this.balanceNotificationDays = rs.getInt("balance_notification_days");
					this.balanceNotificationNotType = rs.getInt("balance_notification_not_type");
					if (this.balanceNotificationNotType == 0)
					{
						this.balanceNotificationSMS = true;
					}
					else if (this.balanceNotificationNotType == 1)
					{
						this.balanceNotificationMail = true;
					}

					this.merchantClassificationOtherDesc = rs.getString("merchantClassificationOtherDesc");

					TorqueHelper.closeStatement(pstmt, rs);

					/* BEGIN Release 31 - Added by NM */
					this.setWarningChangeModel("");

					ArrayList<String> plans = Merchant.getRatePlansByRepId(Long.parseLong(this.repId));
					if (plans.size() == 0)
					{
						this.setWarningChangeModel(Languages.getString(
								"jsp.admin.terminal.error_merchantNotHaveISOManagedRatePlans", sessionData
										.getLanguage()));
					}
					/* END Release 31 - Added by NM */

					/* BEGIN Release 21 - Added by YH */
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getRouteName"));
					pstmt.setInt(1, iRouteId);
					rs = pstmt.executeQuery();

					if (rs.next())
					{
						this.merchantRouteId = rs.getInt("RouteID");
						this.merchantRouteName = rs.getString("RouteName");
					}
					TorqueHelper.closeStatement(pstmt, rs);
					/* END Release 21 - Added by YH */

					// get the ach schedule
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getAchSchedule"));
					pstmt.setDouble(1, Double.parseDouble(this.merchantId));
					rs = pstmt.executeQuery();

					if (rs.next())
					{
						this.achSchedule = rs.getString("schedule");
						this.achScheduleType = rs.getString("schedule_type");
					}
				}
				else
				{
					this.addFieldError("error", "Merchant not found.");
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during getMerchant", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during closeConnection", e);
				}
			}
		}
		else
		{
			this.addFieldError("error", "Permission Denied.");
		}
	}

	/**
	 * Given an merchant_id, populates merchant object populated from the
	 * database, or null if no such merchant_id exists. database, or null if no
	 * such merchant_id exists.
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public void getMerchantMx(SessionData sessionData, ServletContext context) throws CustomerException
	{
		if (this.checkPermission(sessionData))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				String strAccessLevel = sessionData.getProperty("access_level");
				String strDistChainType = sessionData.getProperty("dist_chain_type");
				String strRefId = sessionData.getProperty("ref_id");

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				String strSQL = Merchant.sql_bundle.getString("getMerchantMx");

				if (strAccessLevel.equals(DebisysConstants.CARRIER))
				{
					strSQL = strSQL.substring(0, strSQL.indexOf("WHERE"));
					strSQL = strSQL + " INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.Merchant_ID = mc.MerchantID";
					strSQL = strSQL + " INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID";
					strSQL = strSQL + " WHERE rc.RepID = " + StringUtil.escape(strRefId);
					strSQL = strSQL + " AND m.Merchant_ID = " + StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
					{
						strSQL = strSQL + " r.iso_id = " + StringUtil.escape(strRefId) + " and m.merchant_id = "
								+ StringUtil.escape(this.merchantId);
					}
					else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type="
								+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
								+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
								+ " and iso_id in " + "(select rep_id from reps (nolock) where type="
								+ DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + StringUtil.escape(strRefId)
								+ "))) " + "and m.merchant_id = " + StringUtil.escape(this.merchantId);
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
							+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
							+ " and iso_id= " + StringUtil.escape(strRefId) + "))" + "and m.merchant_id = "
							+ StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
				{
					strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + StringUtil.escape(strRefId) + ")"
							+ " and m.merchant_id = " + StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.REP))
				{
					strSQL = strSQL + " r.rep_id = " + StringUtil.escape(strRefId) + " and m.merchant_id = "
							+ StringUtil.escape(this.merchantId);
				}
				else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
				{
					strSQL = strSQL + " m.merchant_id = " + StringUtil.escape(strRefId);
				}
				else
				{
					strSQL = "";
				}

				pstmt = dbConn.prepareStatement(strSQL);
				rs = pstmt.executeQuery();

				if (rs.next())
				{
					this.physColony = rs.getString("phys_colony");
					this.physDelegation = rs.getString("phys_delegation");
					this.mailColony = rs.getString("mail_colony");
					this.mailDelegation = rs.getString("mail_delegation");
					this.mailCounty = rs.getString("mail_county");
					this.bankName = rs.getString("BankName");
					this.businessHours = rs.getString("BusinessHours");
					this.businessTypeId = rs.getString("sic_code");
					this.mailCountry = rs.getString("mail_country_id");
					this.accountTypeId = rs.getInt("AccountTypeID");
					this.merchantSegmentId = rs.getInt("MerchantSegmentID");
					this.creditTypeId = rs.getInt("CreditTypeID");
					/* R31.1 Billing ABernal - EMunoz */
					this.regulatoryFeeId = rs.getInt("regulatory_fee_id");
					this.rec_id = rs.getInt("rec_id");
					this.regulatoryFeeName = rs.getString("name");
					/* End R31.1 Billing ABernal - EMunoz */
					this.paymentTypeForInvoiceGeneration = rs.getString("PaymentTypeForInvoiceGeneration");
					this.accountNumberForInvoiceGeneration = rs.getString("AccountNumberForInvoiceGeneration");
					this.testAccountEnabled = rs.getBoolean("test_account_enabled");
				}
				else
				{
					this.addFieldError("error", "Merchant not found.");
				}

				TorqueHelper.closeStatement(pstmt, rs);

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getContactsMx"));
				pstmt.setString(1, this.merchantId);
				rs = pstmt.executeQuery();
				this.vecContacts = new Vector();

				while (rs.next())
				{
					Vector vecTemp = new Vector();
					vecTemp.add(rs.getString("ContactTypeID"));
					vecTemp.add(rs.getString("ContactName"));
					vecTemp.add(rs.getString("ContactPhone"));
					vecTemp.add(rs.getString("ContactFax"));
					vecTemp.add(rs.getString("ContactEmail"));
					vecTemp.add(rs.getString("ContactCellPhone"));
					vecTemp.add(rs.getString("ContactDepartment"));
					this.vecContacts.add(vecTemp);
				}

				if (this.vecContacts.size() == 0)
				{
					Vector vecTemp = new Vector();
					vecTemp.add(Merchant.getContactTypeByCode("OTHER", sessionData).get(0).toString());
					vecTemp.add(((this.contactName != null) ? this.contactName : ""));
					vecTemp.add(((this.contactPhone != null) ? this.contactPhone : ""));
					vecTemp.add(((this.contactFax != null) ? this.contactFax : ""));
					vecTemp.add(((this.contactEmail != null) ? this.contactEmail : ""));
					vecTemp.add("");
					vecTemp.add("");
					this.vecContacts.add(vecTemp);
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during getMerchant", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during closeConnection", e);
				}
			}
		}
		else
		{
			this.addFieldError("error", "Permission Denied.");
		}
	}

	/**
	 * Adds a new merchant in the database and sets the entity id of the object
	 * to the newly created record's ID to the newly created record's ID
	 * 
	 * @throws EntityException
	 *             Thrown if there is an error inserting the record in the
	 *             database. database.
	 */
	public void addMerchant(SessionData sessionData, ServletContext context, HttpServletRequest request)
			throws EntityException
	{
		String deploymentType = DebisysConfigListener.getDeploymentType(context);
		boolean hasEntityAccountPermission = sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE);
		boolean hasEditAccountTypeModificationPermission = sessionData
				.checkPermission(DebisysConstants.PERM_EDIT_BANKING_ACCOUNT_TYPE_FOR_MERCHANT);

		ResultSet r = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		CallableStatement cstmt = null;
		
		try
		{

			HashMap map = CountryInfo.getAllCountriesID();
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");
			Merchant.cat.debug("[addMerchant] strAccessLevel=" + strAccessLevel);
			Merchant.cat.debug("[addMerchant] strRefId=" + strRefId);
			String isoid = "";

			long v = 0;
			conn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (conn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new EntityException();
			}
			String SQL = Merchant.sql_bundle.getString("getMerchantISOc");
			Merchant.cat.debug(SQL);
			pstmt = conn.prepareStatement(SQL);
			try
			{
				v = Long.valueOf(this.repId);
				Merchant.cat.debug("[addMerchant] repID=" + v);
			}
			catch (NumberFormatException e)
			{
				Merchant.cat.error("NumberFormatException Parsing ParentID to long" + e);
			}
			pstmt.setLong(1, v);

			r = pstmt.executeQuery();
			while (r.next())
			{
				isoid = r.getString(1).trim();
			}
			Merchant.cat.debug("[addMerchant] ISOID from database  = " + isoid);

			TorqueHelper.closeStatement(pstmt, r); // reuse conn but close
			// statement and result set

			// get ISO ID from RepID
			Merchant.cat.debug("[addMerchant] the staticIDCountryISO1 info is : "
					+ context.getAttribute("staticIDCountryISO1"));
			Merchant.cat.debug("[addMerchant] the staticIDConstantValue1 info is : "
					+ context.getAttribute("staticIDConstantValue1"));
			Vector<String> staticISO = new Vector<String>();
			// check if Country ISO matches Static ISO Conditions
			// Static ISO added to define constant values for a country ISO
			for (int i = 1;; i++)
			{
				if (context.getAttribute("staticIDCountryISO" + i) == null
						|| context.getAttribute("staticIDCountryISO" + i) == "")
				{
					break;
				}
				if (context.getAttribute("staticIDConstantValue" + i) == null
						|| context.getAttribute("staticIDConstantValue" + i) == "")
				{
					break;
				}
				if (isoid.equals(context.getAttribute("staticIDCountryISO" + i)))
				{
					staticISO.add((String) context.getAttribute("staticIDCountryISO" + i));
					staticISO.add((String) context.getAttribute("staticIDConstantValue" + i));
					break;
				}
			}
			if (staticISO.size() < 2)
			{
				this.setMerchantId(this.generateMerchantId());// if no special
																// cases, id
				// is generated randomly
				Merchant.cat.debug("[addMerchant] Vector Static ISO is empty; generated merchantId="
						+ this.getMerchantId());
			}
			else
			{
				this.setMerchantId(this.generatePartiallyStaticId(staticISO).trim());
			}

			cstmt = conn.prepareCall(Merchant.sql_bundle.getString("addMerchant"));

			// merchant id
			cstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));

			// merchant type - not used domestically
			// but just to remain consistent we will set it
			Merchant.cat.debug("[addMerchant] merchantType=" + this.merchantType + ", creditLimit=" + this.creditLimit);

			cstmt.setInt(2, Integer.parseInt(this.merchantType));

			if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
					|| strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				cstmt.setDouble(3, Double.parseDouble(this.repId));
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				cstmt.setDouble(3, Double.parseDouble(strRefId));
			}

			// legacy
			cstmt.setInt(4, 0);
			cstmt.setInt(5, 0);
			cstmt.setInt(6, 0);
			cstmt.setInt(7, 0);
			cstmt.setInt(8, 0);
			cstmt.setInt(9, 0);
			cstmt.setInt(10, 0);
			cstmt.setInt(11, 0);
			cstmt.setInt(12, 0);
			cstmt.setInt(13, 0);

			cstmt.setString(14, this.businessName);
			cstmt.setString(15, this.taxId);
			cstmt.setString(16, this.contactEmail);
			cstmt.setString(17, this.contactName);
			cstmt.setString(18, this.dba);
			cstmt.setString(19, this.contactPhone.replaceAll(Merchant.PHONE_CLEAN_REGEXP, ""));
			if (this.contactFax != null)
			{
				cstmt.setString(20, this.contactFax.replaceAll(Merchant.PHONE_CLEAN_REGEXP, ""));
			}
			else
			{
				cstmt.setString(20, "");
			}

			cstmt.setString(21, this.physAddress);
			cstmt.setString(22, this.physCity);
			cstmt.setString(23, this.physCounty);
			cstmt.setString(24, this.physState);
			cstmt.setString(25, this.physZip);
			cstmt.setString(26, this.physCountry);
			if (map.get(Integer.parseInt(this.physCountry)) != null)
			{
				cstmt.setString(27, map.get(Integer.parseInt(this.physCountry)).toString());
			}
			else
			{
				cstmt.setString(27, "0");
			}

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
					&& (this.mailState != null))
			{
				cstmt.setString(28, this.mailAddress);
				cstmt.setString(29, this.mailCity);
				cstmt.setString(30, this.mailState);
				cstmt.setString(31, this.mailZip);
				cstmt.setString(32, this.mailCountry);
				cstmt.setString(33, map.get(Integer.parseInt(this.mailCountry)).toString());
			}
			else
			{
				cstmt.setString(28, this.physAddress);
				cstmt.setString(29, this.physCity);
				cstmt.setString(30, this.physState);
				cstmt.setString(31, this.physZip);
				cstmt.setString(32, this.physCountry);
				// cstmt.setString(33,
				// map.get(Integer.parseInt(this.physCountry)).toString());
				if (map.get(Integer.parseInt(this.physCountry)) != null)
				{
					cstmt.setString(33, map.get(Integer.parseInt(this.physCountry)).toString());
				}
				else
				{
					cstmt.setString(33, "0");
				}
			}

			// sic_code
			// cstmt.setNull(30,java.sql.Types.VARCHAR);
			cstmt.setString(34, this.businessTypeId);

			cstmt.setString(35, this.routingNumber);
			cstmt.setString(36, this.accountNumber);
			cstmt.setDouble(37, Double.parseDouble(this.monthlyFeeThreshold));

			cstmt.setNull(38, java.sql.Types.INTEGER);
			cstmt.setNull(39, java.sql.Types.FLOAT);
			cstmt.setNull(40, java.sql.Types.DATE);
			cstmt.setNull(41, java.sql.Types.VARCHAR);

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				cstmt.setInt(42, this.getRegion());
			}
			else
			{
				cstmt.setNull(42, java.sql.Types.INTEGER);
			}

			cstmt.setNull(43, java.sql.Types.INTEGER);// Guarantee the data
			// integrity

			if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
			{
				if (this.merchantRouteId > 0)
				{
					cstmt.setInt(44, this.merchantRouteId);
				}
				else
				{
					cstmt.setNull(44, java.sql.Types.INTEGER);
				}
			}
			else
			{
				cstmt.setNull(44, java.sql.Types.INTEGER);
			}

			cstmt.setBoolean(45, this.m_isAchAccount);

			cstmt.setInt(46, this.getMerchantClassification());

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				// cstmt.setInt(46, this.getMerchantClassification());
				cstmt.setString(47, this.getControlNumber());
				cstmt.setInt(48, this.getInvoice());
				cstmt.setBoolean(51, this.getPrintBillingAddress());
			}
			else
			{
				// cstmt.setNull(46, java.sql.Types.INTEGER);
				cstmt.setNull(47, java.sql.Types.VARCHAR);
				cstmt.setNull(48, java.sql.Types.INTEGER);
				cstmt.setNull(51, java.sql.Types.BOOLEAN);
			}
			cstmt.setInt(49, this.reservedPINCacheBalance);
			cstmt.setInt(50, this.pinCacheEnabled ? 1 : 0);
			cstmt.setInt(52, this.timeZoneId);
			// ********************************************//
			// DBSY-783 R30 Jorge Nicol�s Mart�nez Romero
			// - ERI Account Executive
			if (this.executiveAccountId > 0)
			{
				cstmt.setInt(53, this.executiveAccountId);
			}
			else
			{
				cstmt.setNull(53, java.sql.Types.INTEGER);
			}
			// END DBSY-783 R30 Jorge Nicol�s Mart�nez Romero

			/* BEGIN R30 - Added by Luis De La Rosa - ERI Credit Limit */
			if (this.salesLimitEnable.equals("on"))
			{
				cstmt.setString(54, this.salesLimitType);
				cstmt.setDouble(55, this.salesLimitLiabilityLimit);
				cstmt.setDouble(56, this.salesLimitMonday);
				cstmt.setDouble(57, this.salesLimitTuesday);
				cstmt.setDouble(58, this.salesLimitWednesday);
				cstmt.setDouble(59, this.salesLimitThursday);
				cstmt.setDouble(60, this.salesLimitFriday);
				cstmt.setDouble(61, this.salesLimitSaturday);
				cstmt.setDouble(62, this.salesLimitSunday);
			}
			else
			{
				cstmt.setNull(54, java.sql.Types.VARCHAR);
				cstmt.setDouble(55, 0);
				cstmt.setDouble(56, 0);
				cstmt.setDouble(57, 0);
				cstmt.setDouble(58, 0);
				cstmt.setDouble(59, 0);
				cstmt.setDouble(60, 0);
				cstmt.setDouble(61, 0);
				cstmt.setDouble(62, 0);
			}
			/* END R30 - Added by Luis De La Rosa - ERI Credit Limit */

			/* BEGIN R31 - Added by Christian Chabtini DBSY-931 System 2000 */
			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
					&& DebisysConfigListener.getDeploymentType(context).equals(
							DebisysConstants.DEPLOYMENT_INTERNATIONAL) && strAccessLevel.equals(DebisysConstants.ISO)
					&& sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS))
			{
				cstmt.setString(63, this.salesmanid.trim());
				cstmt.setString(64, this.terms);
			}
			else
			{
				cstmt.setNull(63, Types.VARCHAR);
				cstmt.setNull(64, Types.VARCHAR);
			}
			/* END R31 - Added by Christian Chabtini DBSY-931 System 2000 */

			/* BEGIN R31 Added by Nicolas Martinez - ISO Managed Rate Plans */
			if (this.useRatePlanModel)
			{
				cstmt.setBoolean(65, this.useRatePlanModel);
			}
			else
			{
				cstmt.setBoolean(65, Boolean.FALSE);
			}
			/* END R31 - Added by Nicolas Martinez - ISO Managed Rate Plans */

			/* R31.1 Billing Abernal - Emunoz */
			if (sessionData.checkPermission(DebisysConstants.PERM_REGULATORY_FEE))
			{
				cstmt.setInt(66, this.regulatoryFeeId);
			}
			else
			{
				cstmt.setInt(66, 0);// LF - Changed from NULL to 0 because the
				// column doesn't allow NULLS
			}
			/* END R31.1 Billing Abernal - Emunoz */

			/* R31.2 - Added by Diego Garzon - Payment process */
			if (sessionData.checkPermission(DebisysConstants.PERM_UPDATE_PAYMENT_FEATURES))
			{
				cstmt.setInt(67, this.paymentType);
				cstmt.setInt(68, this.processType);
			}
			else
			{
				cstmt.setInt(67, -1);
				cstmt.setInt(68, -1);
			}

			// DBSY-1072 eAccounts Interface
			if (hasEntityAccountPermission)
			{
				if (this.dentityAccountType.equals("1"))
				{
					cstmt.setInt(69, 1);// if Combo is disabled then default to
										// external
				}
				else
				{
					cstmt.setInt(69, this.entityAccountType);
				}

			}
			else
			{
				cstmt.setInt(69, 1);
			}
			// Alfred - DBSY-1139
			cstmt.setInt(70, this.getMerchantPermissions());

			/******************************************************************************************/
			/******************************************************************************************/
			/* GEOLOCATION FEATURE JORGE NICOLAS MARTINEZ ROMERO */
			cstmt.setInt(71, this.getAddressValidationStatus());
			cstmt.setDouble(72, this.getLatitude());
			cstmt.setDouble(73, this.getLongitude());
			/* GEOLOCATION FEATURE JORGE NICOLAS MARTINEZ ROMERO */
			/******************************************************************************************/
			/******************************************************************************************/

			// DTU-369 Payment Notifications
			cstmt.setBoolean(74, this.isPaymentNotifications());
			if (this.isPaymentNotificationSMS())
			{
				this.setPaymentNotificationType(0);
			}
			else
			{
				if (this.isPaymentNotificationMail())
				{
					this.setPaymentNotificationType(1);
				}
			}
			cstmt.setInt(75, this.getPaymentNotificationType());
			cstmt.setString(76, this.getSmsNotificationPhone());
			cstmt.setString(77, this.getMailNotificationAddress());

			// DTU-1772 Capture of detailed Banking Information for Precash
			// settlement
			cstmt.setString(78, this.getBankAddress());
			cstmt.setString(79, this.getBankCity());
			cstmt.setString(80, this.getBankState());
			cstmt.setString(81, this.getBankZip());
			cstmt.setString(82, this.getBankContactName());
			cstmt.setString(83, this.getBankContactPhone());

			if (!this.getMerchantOwnerId().equals("null") && this.getMerchantOwnerId().length() > 0)
			{
				cstmt.setString(84, this.getMerchantOwnerId());
			}
			else
			{
				cstmt.setNull(84, java.sql.Types.VARCHAR);
			}

			if (hasEditAccountTypeModificationPermission)
			{
				cstmt.setInt(85, this.getAccountTypeId());
			}
			else
			{
				cstmt.setInt(85, 1);
			}
			cstmt.setBoolean(86, this.invoicing);

			// DTU-480: Low Balance Alert Message SMS and/or email
			cstmt.setBoolean(87, this.isBalanceNotifications());
			if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays())
			{
				this.setBalanceNotificationType(2);
			}
			else if (this.isBalanceNotificationTypeValue())
			{
				this.setBalanceNotificationType(0);
			}
			else if (this.isBalanceNotificationTypeDays())
			{
				this.setBalanceNotificationType(1);
			}
			cstmt.setInt(88, this.getBalanceNotificationType());
			if (this.isBalanceNotificationSMS())
			{
				this.setBalanceNotificationNotType(0);
			}
			else if (this.isBalanceNotificationMail())
			{
				this.setBalanceNotificationNotType(1);
			}
			cstmt.setInt(89, this.getBalanceNotificationNotType());
			cstmt.setInt(90, this.getBalanceNotificationDays());
			cstmt.setDouble(91, this.getBalanceNotificationValue());
			cstmt.setString(92, this.getMerchantClassificationOtherDesc());
			cstmt.setInt(93, this.getMerchantExternalOutsideHierarchy());
			cstmt.setBoolean(94, this.getTestAccountEnabled());
			
			cstmt.execute();

			TorqueHelper.closeStatement(cstmt, null);

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				if (this.sharedBalance.equals("1"))
				{
					this.merchantType = DebisysConstants.MERCHANT_TYPE_SHARED;
					this.creditLimit = "0";
				}

				pstmt = conn.prepareStatement(Merchant.sql_bundle.getString("updateMerchantMx"));
				pstmt.setString(1, this.physColony);
				pstmt.setString(2, this.physDelegation);
				pstmt.setString(3, this.mailColony);
				pstmt.setString(4, this.mailDelegation);
				pstmt.setString(5, this.mailCounty);
				pstmt.setString(6, this.bankName);
				pstmt.setInt(7, this.accountTypeId);
				pstmt.setInt(8, this.merchantSegmentId);
				pstmt.setString(9, this.businessHours);
				pstmt.setInt(10, Integer.parseInt(this.merchantType));
				pstmt.setString(11, this.merchantType);
				pstmt.setString(12, this.businessTypeId);
				pstmt.setString(13, this.taxId);
				pstmt.setDouble(14, Double.parseDouble(this.merchantId));
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, null);

				Iterator itContacts = this.vecContacts.iterator();

				while (itContacts.hasNext())
				{
					Vector vecTemp = null;
					vecTemp = (Vector) itContacts.next();
					pstmt = conn.prepareStatement(Merchant.sql_bundle.getString("insertContactMx"));
					pstmt.setDouble(1, Double.parseDouble(this.merchantId));
					pstmt.setString(2, vecTemp.get(0).toString());
					pstmt.setString(3, vecTemp.get(1).toString());
					pstmt.setString(4, vecTemp.get(2).toString());
					pstmt.setString(5, vecTemp.get(3).toString());
					pstmt.setString(6, vecTemp.get(4).toString());
					pstmt.setString(7, vecTemp.get(5).toString());
					pstmt.setString(8, vecTemp.get(6).toString());
					pstmt.executeUpdate();

					TorqueHelper.closeStatement(pstmt, null);
				}

				ArrayList<MerchantInvoicePayments> arrayInvoicePayments = (ArrayList<MerchantInvoicePayments>) request
						.getSession().getAttribute("merchantInvoicePaymentsArray");
				if (arrayInvoicePayments != null && arrayInvoicePayments.size() > 0)
				{
					for (MerchantInvoicePayments mMerchantInvoicePayments : arrayInvoicePayments)
					{
						mMerchantInvoicePayments.setMerchantId(new Long(this.getMerchantId()));
						MerchantInvoicePayments.addInvoicePayment(mMerchantInvoicePayments);
						// System.out.println(mMerchantInvoicePayments.getMerchantId()
						// + " / " + mMerchantInvoicePayments.getAccountNumber()
						// + " / " + mMerchantInvoicePayments.getPaymentTypeId()
						// );
					}
				}

			}

			Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.merchant_added", sessionData
					.getLanguage()), DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
					DebisysConstants.PW_REF_TYPE_MERCHANT);

			// updateLiabilityLimit2=update merchants set LiabilityLimit = ?
			// where merchant_id = ?
			pstmt = conn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit2"));
			pstmt.setDouble(1, Double.parseDouble(this.creditLimit));
			pstmt.setDouble(2, Double.parseDouble(this.merchantId));
			pstmt.executeUpdate();

			TorqueHelper.closeStatement(pstmt, null);		

			Rep rep = new Rep();
			rep.setRepId(this.repId);
			rep.getRep(sessionData, context);

			// if the reps not unlimited, log the merchant credit assignment
			// if the merchant credit limit is > 0
			if (!rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
			{

				pstmt = conn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCredit"));
				pstmt.setDouble(1, Double.parseDouble(this.merchantId));
				pstmt.setString(2, sessionData.getProperty("username"));
				pstmt.setString(3, Languages.getString("com.debisys.customers.rep_add_payment", sessionData
						.getLanguage()));
				pstmt.setDouble(4, Double.parseDouble(this.creditLimit));
				pstmt.setDouble(5, Double.parseDouble(this.creditLimit));
				pstmt.setDouble(6, 0);
				pstmt.setDouble(7, 0);
				pstmt.setDouble(8, 0);
				pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
				{
					if (hasEntityAccountPermission)
					{
						Merchant.cat.debug("dentityAccountType(MerchantCredit)=" + this.dentityAccountType);
						if (this.dentityAccountType.equals("1"))
						{
							pstmt.setInt(10, 1);// if Combo is disabled then
												// default to external
						}
						else
						{
							pstmt.setInt(10, this.entityAccountType);
						}
					}
					else
					{
						pstmt.setInt(10, 1);
					}
				}
				/*
				 * if (hasEntityAccountPermission) { pstmt.setInt(10,
				 * EntityAccountTypes
				 * .GetEntityAccountTypeMerchantType(this.merchantId)); } else {
				 * pstmt.setNull(10, java.sql.Types.INTEGER); }
				 */
				pstmt.executeUpdate();
				pstmt.close();

				// DTU-369 Payment Notifications
				if (Rep.GetPaymentNotification(this.merchantId))
				{
					pstmt = conn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
					pstmt.setDouble(1, Double.parseDouble(this.merchantId));
					pstmt.setString(2, sessionData.getProperty("username"));
					pstmt.setString(3, Languages.getString("com.debisys.customers.rep_add_payment", sessionData
							.getLanguage()));
					pstmt.setDouble(4, Double.parseDouble(this.creditLimit));
					pstmt.setDouble(5, Double.parseDouble(this.creditLimit));
					pstmt.setDouble(6, 0);
					pstmt.setDouble(7, 0);
					pstmt.setDouble(8, 0);
					pstmt.setDouble(9, 0);
					pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
					{
						if (hasEntityAccountPermission)
						{
							Merchant.cat.debug("dentityAccountType(MerchantCredit)=" + this.dentityAccountType);
							if (this.dentityAccountType.equals("1"))
							{
								pstmt.setInt(11, 1);// if Combo is disabled then
													// default to external
							}
							else
							{
								pstmt.setInt(11, this.entityAccountType);
							}
						}
						else
						{
							pstmt.setInt(11, 1);
						}
					}
					pstmt.setString(12, this.businessName);
					pstmt.setInt(13, Rep.GetPaymentNotificationType(this.merchantId));
					pstmt.setString(14, Rep.GetSmsNotificationPhone(this.merchantId));
					pstmt.setString(15, Rep.GetMailNotificationAddress(this.merchantId));
					pstmt.executeUpdate();
					pstmt.close();
				}

				// updateRepRunningLiability=update set runningliability=?
				// where rep_id=?
				pstmt = conn.prepareStatement(Merchant.sql_bundle.getString("updateRepRunningLiability"));
				pstmt.setDouble(1, (Double.parseDouble(rep.getRunningLiability()) + Double
						.parseDouble(this.creditLimit)));
				pstmt.setDouble(2, Double.parseDouble(this.repId));
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, null);

				double repCreditLimit = rep.getCurrentRepCreditLimit(sessionData);
				double repAssignedCredit = rep.getAssignedCredit(sessionData);

				// add an entry to show the rep credits being used
				// 1 2 3 4 5 6 7 8
				// (rep_id, merchant_id, logon_id, description, amount,
				// available_credit, prior_available_credit, datetime)
				pstmt = conn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
				pstmt.setDouble(1, Double.parseDouble(this.repId));
				pstmt.setDouble(2, Double.parseDouble(this.merchantId));
				pstmt.setString(3, sessionData.getProperty("username"));
				pstmt.setString(4, "\""
						+ this.dba
						+ "\" "
						+ Languages.getString("com.debisys.customers.Merchant.assigned_credit", sessionData
								.getLanguage()));
				pstmt.setDouble(5, Double.parseDouble(this.creditLimit) * -1);
				pstmt.setDouble(6, repCreditLimit - repAssignedCredit);
				pstmt.setDouble(7, repCreditLimit - (repAssignedCredit - Double.parseDouble(this.creditLimit)));
				pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
				// DBSY-1072 eAccounts Interface
				{
					if (hasEntityAccountPermission)
					{
						Merchant.cat.debug("dentityAccountType(RepMerchantCredit)=" + this.dentityAccountType);
						if (this.dentityAccountType.equals("1"))
						{
							pstmt.setInt(9, rep.getEntityAccountType());
							pstmt.setInt(10, 1);// if Combo is disabled then
												// default to external
						}
						else
						{
							pstmt.setInt(9, rep.getEntityAccountType());
							pstmt.setInt(10, this.entityAccountType);
						}
					}
					else
					{
						pstmt.setInt(9, 1);
						pstmt.setInt(10, 1);
					}
				}
				/*
				 * if (hasEntityAccountPermission) { pstmt.setInt(9,
				 * rep.getEntityAccountType()); pstmt.setInt(10,
				 * EntityAccountTypes
				 * .GetEntityAccountTypeMerchantType(this.merchantId)); } else {
				 * pstmt.setNull(9, java.sql.Types.INTEGER); pstmt.setNull(10,
				 * java.sql.Types.INTEGER); }
				 */
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, null);
			}
			// }
		}catch (SQLException sqle)
		{
                    DbUtil.processSqlException(sqle);
                }
		catch (Exception e)
		{
			Merchant.cat.error("Error during addMerchant", e);
			throw new EntityException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeStatement(cstmt, null);
				DbUtil.closeDatabaseObjects(conn, pstmt, r);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	/**
	 * Updates a merchant in the database
	 * 
	 * @param sessionData
	 * @param context
	 * @param request
	 * @throws EntityException
	 *             Thrown if there is an error inserting the record in the
	 *             database. database.
	 */
	public void updateMerchant(SessionData sessionData, ServletContext context, HttpServletRequest request)
			throws EntityException
	{
		Merchant.cat.debug("updateMerchant");
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean hasEntityAccountPermission = sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE);

		try
		{
			// create a new merchant object to compare changes after update
			Merchant tmpMerchant = new Merchant();
			LogChanges logChanges = new LogChanges(sessionData);
			logChanges.setContext(context);
			tmpMerchant.setMerchantId(this.merchantId);
			tmpMerchant.getMerchant(sessionData, context);

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				tmpMerchant.getMerchantMx(sessionData, context);
			}

			String log = "";

			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new EntityException();
			}

			// Building the query string depending of the fields and if its
			// values are changed
			String sqlStr = Merchant.sql_bundle.getString("updateMerchant");
			String additionalParams = "";
			boolean paymentType_ch = false, processType_ch = false, routingNumber_ch = false, accountNumber_ch = false, regulatoryId_ch = false, taxId_ch = false;
			boolean entityAccountType_ch = false;

			String oldValue = "";
			String reason = "";
			String reasonIni = "Last value: ";
			String reasonEnd = "Actual value: ";
			String changedBy = "Modified by: ";
			String username = sessionData.getProperty("username");

			oldValue = sessionData.getProperty("check."
					+ logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE))
					+ " ";
			reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.paymentType) + " " + changedBy + username
					+ "";

			if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE), String
					.valueOf(this.paymentType), this.merchantId, reason))
			{
				additionalParams += ", payment_type = ?";
				paymentType_ch = true;
			}

			oldValue = sessionData.getProperty("check."
					+ logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR))
					+ " ";
			reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.processType) + " " + changedBy + username
					+ "";

			if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR),
					String.valueOf(this.processType), this.merchantId, reason))
			{
				additionalParams += ",payment_processor = ?";
				processType_ch = true;
			}

			oldValue = sessionData
					.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA))
					+ " ";
			reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.routingNumber) + " " + changedBy + username
					+ "";

			if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA), String
					.valueOf(this.routingNumber), this.merchantId, reason))
			{
				additionalParams += ", aba = ?";
				routingNumber_ch = true;
			}

			oldValue = sessionData.getProperty("check."
					+ logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER))
					+ " ";
			reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.accountNumber) + " " + changedBy + username
					+ "";

			if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER), String
					.valueOf(this.accountNumber), this.merchantId, reason))
			{
				additionalParams += ", account_no = ?";
				accountNumber_ch = true;
			}

			oldValue = sessionData.getProperty("check."
					+ logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID))
					+ " ";
			reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.taxId) + " " + changedBy + username + "";

			if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID), String
					.valueOf(this.taxId), this.merchantId, reason))
			{
				additionalParams += ", tax_id = ?";
				taxId_ch = true;
			}

			/* R31.1 Billing ABernal - Emunoz */
			if (sessionData.checkPermission(DebisysConstants.PERM_REGULATORY_FEE))
			{

				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_REGULATORYFEEID),
						Integer.toString(this.regulatoryFeeId), Integer.toString(this.rec_id), ""))
				{
					additionalParams += ", regulatory_fee_id = ?";
					regulatoryId_ch = true;
				}
			}
			/* End R31.1 Billing ABernal - Emunoz */

			// DBSY-1072 eAccounts Interface
			String tempint;
			if (this.dentityAccountType.equals("1"))
			{
				tempint = "1";
			}
			else
			{
				tempint = Integer.toString(this.entityAccountType);
			}
			oldValue = sessionData.getProperty("check."
					+ logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_MERCHANT))
					+ " ";
			reason = reasonIni + oldValue + reasonEnd + tempint + " " + changedBy + username + "";

			if (hasEntityAccountPermission)
			{
				if (logChanges.was_changed(logChanges
						.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_MERCHANT), tempint,
						this.merchantId, ""))
				{
					additionalParams += ", EntityAccountType = ?";
					entityAccountType_ch = true;
				}
			}
			// END DBSY-1072

			additionalParams += ", merchantClassificationOtherDesc = ?";
			additionalParams += ", externalOutsideHierarchy = ?";
			Merchant.cat.debug("query:" + StringUtil.toString(sqlStr));
			sqlStr = sqlStr.replace("~~addParams~~", additionalParams);
			Merchant.cat.debug("query:" + StringUtil.toString(sqlStr));
			// end of building the query string

			pstmt = dbConn.prepareStatement(sqlStr);

			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");

			if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
					|| strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				pstmt.setDouble(1, Double.parseDouble(this.repId));

			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				pstmt.setDouble(1, Double.parseDouble(strRefId));
			}

			pstmt.setString(2, this.businessName);
			pstmt.setString(3, this.contactEmail);

			pstmt.setString(4, this.contactName);
			pstmt.setString(5, this.dba);
			pstmt.setString(6, this.contactPhone.replaceAll(Merchant.PHONE_CLEAN_REGEXP, ""));

			if (this.contactFax != null)
			{
				pstmt.setString(7, this.contactFax.replaceAll(Merchant.PHONE_CLEAN_REGEXP, ""));
			}
			else
			{
				pstmt.setString(7, "");
			}

			pstmt.setString(8, this.physAddress);
			pstmt.setString(9, this.physCity);
			pstmt.setString(10, this.physCounty);
			pstmt.setString(11, this.physState);

			pstmt.setString(12, this.physZip);
			pstmt.setString(13, this.physCountry);

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
					&& (this.mailState != null))
			{
				pstmt.setString(14, this.mailAddress);
				pstmt.setString(15, this.mailCity);
				pstmt.setString(16, this.mailState);
				pstmt.setString(17, this.mailZip);
				pstmt.setString(18, this.mailCountry);
			}
			else
			{
				pstmt.setString(14, this.physAddress);
				pstmt.setString(15, this.physCity);
				pstmt.setString(16, this.physState);
				pstmt.setString(17, this.physZip);
				pstmt.setString(18, this.physCountry);
			}
			pstmt.setDouble(19, Double.parseDouble(this.monthlyFeeThreshold));
			pstmt.setInt(20, this.region);
			pstmt.setInt(21, this.invoice);
			if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
			{
				pstmt.setInt(22, this.merchantRouteId);
			}
			else
			{
				pstmt.setNull(22, java.sql.Types.INTEGER);
			}
			pstmt.setInt(23, this.getMerchantClassification());
			pstmt.setString(24, this.getControlNumber());
			pstmt.setInt(25, this.reservedPINCacheBalance);
			pstmt.setInt(26, this.pinCacheEnabled ? 1 : 0);
			pstmt.setBoolean(27, this.printBillingAddress);
			pstmt.setInt(28, this.timeZoneId);

			// ********************************************//
			// DBSY-783 R30 Jorge Nicol�s Mart�nez Romero
			// - ERI Account Executive
			if (this.executiveAccountId > 0)
			{
				pstmt.setInt(29, this.executiveAccountId);
			}
			else
			{
				pstmt.setNull(29, java.sql.Types.INTEGER);
			}
			// END DBSY-783 R30 Jorge Nicol�s Mart�nez Romero

			// DBSY-931 System 2000
			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
					&& DebisysConfigListener.getDeploymentType(context).equals(
							DebisysConstants.DEPLOYMENT_INTERNATIONAL))
			{
                            if (this.salesmanid != null)
                               pstmt.setString(30, this.salesmanid.trim());
                            else 
                                pstmt.setNull(30, java.sql.Types.VARCHAR);
                            
                            if (this.terms != null)
				pstmt.setString(31, this.terms);
                            else 
                                pstmt.setNull(31, java.sql.Types.VARCHAR);
			}
			else
			{
				pstmt.setString(30, tmpMerchant.getsalesmanid().trim());// if not
				// international
				// then keep
				// the same
				// value...
				pstmt.setString(31, tmpMerchant.getterms());// if not
				// international
				// then keep the
				// same value...
			}
			pstmt.setInt(32, this.getMerchantPermissions());

			/******************************************************************************************/
			/******************************************************************************************/
			/* GEOLOCATION FEATURE */
			Merchant.cat.debug("latitude:" + Double.toString(this.latitude));
			pstmt.setDouble(33, this.latitude);

			Merchant.cat.debug("longitude:" + Double.toString(this.longitude));
			pstmt.setDouble(34, this.longitude);

			Merchant.cat.debug("addressValidationStatus:" + Integer.toString(this.addressValidationStatus));
			pstmt.setInt(35, this.addressValidationStatus);
			/* GEOLOCATION FEATURE */
			/******************************************************************************************/
			/******************************************************************************************/

			// DTU-369 Payment Notifications
			pstmt.setBoolean(36, this.isPaymentNotifications());
			if (this.isPaymentNotificationSMS())
			{
				this.setPaymentNotificationType(0);
			}
			else
			{
				if (this.isPaymentNotificationMail())
				{
					this.setPaymentNotificationType(1);
				}
			}
			pstmt.setInt(37, this.getPaymentNotificationType());
			if (this.getSmsNotificationPhone() == null || this.getSmsNotificationPhone().length() == 0)
				pstmt.setNull(38, java.sql.Types.VARCHAR);
			else
				pstmt.setString(38, this.getSmsNotificationPhone());

			if (this.getMailNotificationAddress() == null || this.getMailNotificationAddress().length() == 0)
				pstmt.setNull(39, java.sql.Types.VARCHAR);
			else
				pstmt.setString(39, this.getMailNotificationAddress());

			// DTU-1772 Capture of detailed Banking Information for Precash
			// settlement
			pstmt.setString(40, this.getBankAddress());
			pstmt.setString(41, this.getBankCity());
			pstmt.setString(42, this.getBankState());
			pstmt.setString(43, this.getBankZip());
			pstmt.setString(44, this.getBankContactName());
			pstmt.setString(45, this.getBankContactPhone());

			if (!this.getMerchantOwnerId().equals("null") && this.getMerchantOwnerId().length() > 0)
			{
				pstmt.setString(46, this.getMerchantOwnerId());
			}
			else
			{
				pstmt.setNull(46, java.sql.Types.VARCHAR);
			}

			pstmt.setInt(47, this.getAccountTypeId());

			pstmt.setBoolean(48, this.isInvoicing());

			// DTU-480: Low Balance Alert Message SMS and/or email
			pstmt.setBoolean(49, this.isBalanceNotifications());
			if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays())
			{
				this.setBalanceNotificationType(2);
			}
			else if (this.isBalanceNotificationTypeValue())
			{
				this.setBalanceNotificationType(0);
			}
			else if (this.isBalanceNotificationTypeDays())
			{
				this.setBalanceNotificationType(1);
			}

			pstmt.setInt(50, this.getBalanceNotificationType());
			if (this.isBalanceNotificationSMS())
			{
				this.setBalanceNotificationNotType(0);
			}
			else if (this.isBalanceNotificationMail())
			{
				this.setBalanceNotificationNotType(1);
			}
			pstmt.setInt(51, this.getBalanceNotificationNotType());
			pstmt.setInt(52, this.getBalanceNotificationDays());
			pstmt.setDouble(53, this.getBalanceNotificationValue());

			int i = 54;
			if (paymentType_ch)
			{
				Merchant.cat.debug("after paymenttype:" + this.paymentType);
				pstmt.setInt(i, this.paymentType);
				i++;
			}
			if (processType_ch)
			{
				Merchant.cat.debug("after processtype:" + this.processType);
				pstmt.setInt(i, this.processType);
				i++;
			}
			if (routingNumber_ch)
			{
				Merchant.cat.debug("after routingnumber:" + StringUtil.toString(this.routingNumber));
				pstmt.setString(i, this.routingNumber);
				i++;
			}
			if (accountNumber_ch)
			{
				Merchant.cat.debug("after accountnumber:" + StringUtil.toString(this.accountNumber));
				pstmt.setString(i, this.accountNumber);
				i++;
			}
			if (taxId_ch)
			{
				Merchant.cat.debug("after taxid:" + StringUtil.toString(this.taxId));
				pstmt.setString(i, this.taxId);
				i++;
			}

			/* R31.1 Billing ABernal - Emunoz */
			if (regulatoryId_ch)
			{
				Merchant.cat.debug("after regulatory fee:" + Integer.toString(this.regulatoryFeeId));
				pstmt.setInt(i, this.regulatoryFeeId);
				i++;
			}

			if (entityAccountType_ch)
			{
				Merchant.cat.debug("after entityAccountType:" + Integer.toString(this.entityAccountType));
				// DBSY-1072 eAccounts Interface
				if (this.dentityAccountType.equals("1"))
				{
					pstmt.setInt(i, 1);// if Combo is disabled then default to
										// external
				}
				else
				{
					pstmt.setInt(i, this.entityAccountType);
				}
				i++;
			}

			Merchant.cat.debug("after merchantClassificationOtherDesc:" + merchantClassificationOtherDesc);
			pstmt.setString(i, this.merchantClassificationOtherDesc);
			i++;

			Merchant.cat.debug("after merchantExternalOutsideHierarchy:" + merchantExternalOutsideHierarchy);
			pstmt.setInt(i, this.getMerchantExternalOutsideHierarchy());
			i++;
			
			Merchant.cat.debug("after merchantid:" + StringUtil.toString(this.merchantId));
			pstmt.setDouble(i, Double.parseDouble(this.getMerchantId()));
			pstmt.executeUpdate();

			TorqueHelper.closeStatement(pstmt, null);
                        
                        if(entityAccountType_ch){
                            //TODO: INSERTAUDIT GABRIEL    EntityAccountType Internal=0/External=1
                            this.dentityAccountType = (this.dentityAccountType.trim().equals("")) ? "0" : this.dentityAccountType.trim();
                            insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_MERCHANT, this.merchantId,DebisysConstants.AUDIT_INTERNAL_EXTERNAL_CHANGE,
                                    (this.dentityAccountType.equals("1")?"0":"1"), this.dentityAccountType);
                        }

			if (!this.repId.equals(this.oldRepId))
			{
				ArrayList<String> plans = com.debisys.customers.Merchant
						.getRatePlansByRepId(Long.parseLong(this.repId));
				ArrayList<String> terminals = Merchant.getTerminalsByMerchantID(Long.parseLong(this.merchantId));
				if (plans.size() > 0)
				{
					if (terminals.size() > 0)
					{
						// pstmt =
						// dbConn.prepareStatement(Merchant.sql_bundle.getString("updateRatePlanTerminal"));
						boolean foundMatch = false;
						for (String siteInfo : terminals)
						{
							String[] sitePlan = siteInfo.split("\\#");
							String terminal = sitePlan[0];// terminal
							String planTerminal = sitePlan[1]; // plan
							if (!planTerminal.equals("null"))
							{
								for (String plan : plans)
								{
									String[] planInfo = plan.split("\\#");
									if (planInfo[0].trim().equals(planTerminal.trim()))
									{
										foundMatch = true;
									}
								}
								if (!foundMatch)
								{
									// System.out.println(terminal+" site cannot have the rate plan "+planTerminal);
									pstmt = dbConn
											.prepareStatement("update terminals set rateplanid=null where millennium_no="
													+ terminal);
									pstmt.executeUpdate();
									pstmt.close();
								}
							}
						}
					}
				}
				else if (terminals.size() > 0)
				{
					// when the target repId has not ISO Managed Rate Plans
					// assigned and has terminals
					// we must update to null all terminals RatePlanId to null
					// to avoid incorrect information.
					pstmt = dbConn.prepareStatement("update terminals set rateplanid=null where merchant_Id="
							+ this.merchantId);
					pstmt.executeUpdate();
				}
                                
                                //TODO: INSERTAUDIT GABRIEL    DebisysConstants.AUDIT_PARENT_CHANGE
                                insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_MERCHANT, this.merchantId,DebisysConstants.AUDIT_PARENT_CHANGE,
                                    this.oldRepId, this.repId);
			}

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				if (!this.getRepId().equals(tmpMerchant.getRepId()))
				{
					this.merchantType = this.newMerchantType;
				}

				String sqlStrMx = Merchant.sql_bundle.getString("updateMerchantMx");
				Merchant.cat.debug("query:" + StringUtil.toString(sqlStrMx));
				pstmt = dbConn.prepareStatement(sqlStrMx);
				pstmt.setString(1, this.physColony);
				pstmt.setString(2, this.physDelegation);
				pstmt.setString(3, this.mailColony);
				pstmt.setString(4, this.mailDelegation);
				pstmt.setString(5, this.mailCounty);
				pstmt.setString(6, this.bankName);
				pstmt.setInt(7, this.accountTypeId);
				pstmt.setInt(8, this.merchantSegmentId);
				pstmt.setString(9, this.businessHours);
				pstmt.setString(10, this.merchantType);
				pstmt.setInt(11, this.creditTypeId);
				pstmt.setString(12, this.businessTypeId);
				pstmt.setString(13, this.taxId);
				pstmt.setDouble(14, Double.parseDouble(this.merchantId));
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, null);

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("deleteContactsMx"));
				pstmt.setString(1, this.merchantId);
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, null);

				Iterator itContacts = this.vecContacts.iterator();

				while (itContacts.hasNext())
				{
					Vector vecTemp = null;
					vecTemp = (Vector) itContacts.next();
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertContactMx"));
					pstmt.setDouble(1, Double.parseDouble(this.merchantId));
					pstmt.setString(2, vecTemp.get(0).toString());
					pstmt.setString(3, vecTemp.get(1).toString());
					pstmt.setString(4, vecTemp.get(2).toString());
					pstmt.setString(5, vecTemp.get(3).toString());
					pstmt.setString(6, vecTemp.get(4).toString());
					pstmt.setString(7, vecTemp.get(5).toString());
					pstmt.setString(8, vecTemp.get(6).toString());
					pstmt.executeUpdate();

					TorqueHelper.closeStatement(pstmt, null);
				}
			} // End of if when deploying in Mexico

			boolean fieldsChanged = false;

			if (!this.getRepId().equals(tmpMerchant.getRepId()))
			{
				log = log + "rep_id|" + tmpMerchant.getRepId() + "|" + this.getRepId() + ",";
				fieldsChanged = true;

				// if the rep was updated we need
				// to modify credit limits for the rep
				/*
				 * String deploymentType =
				 * DebisysConfigListener.getDeploymentType(context); if String
				 * deploymentType =
				 * DebisysConfigListener.getDeploymentType(context); if
				 * (deploymentType
				 * .equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
				 */

				// rep changed so lets make sure new rep can handle it
				try
				{
					Rep oldRep = new Rep();
					oldRep.setRepId(tmpMerchant.getRepId());
					oldRep.getRep(sessionData, context);

					Rep newRep = new Rep();
					newRep.setRepId(this.repId);
					newRep.getRep(sessionData, context);

					double availMerchantCredit = Double.parseDouble(tmpMerchant.getLiabilityLimit())
							- Double.parseDouble(tmpMerchant.getRunningLiability());
					// olRep -> Individual, newRep -> Individual and Both are
					// Prepaid

					if ((newRep.getSharedBalance().equals("0") && oldRep.getSharedBalance().equals("0")
							&& oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& newRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) && DebisysConfigListener
							.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
							|| (!DebisysConfigListener.getCustomConfigType(context).equals(
									DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && tmpMerchant.getMerchantType()
									.equals("1")))
					{
						double repCreditLimit = oldRep.getCurrentLiabilityLimit();
						double repRunningLiability = oldRep.getCurrentRunningLiability();

						// Inserting old Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(oldRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_reps",
								sessionData.getLanguage()));
						pstmt.setDouble(5, 0);
						pstmt.setDouble(6, repCreditLimit - repRunningLiability);
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, oldRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						repCreditLimit = newRep.getCurrentLiabilityLimit();
						repRunningLiability = newRep.getCurrentRunningLiability();

						// Inserting new Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(newRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_from_rep",
								sessionData.getLanguage()));
						pstmt.setDouble(5, 0);
						pstmt.setDouble(6, repCreditLimit - repRunningLiability);
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, newRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

					}
					// olRep -> Individual, newRep -> Shared and Both are
					// Prepaid
					else if (newRep.getSharedBalance().equals("1")
							&& oldRep.getSharedBalance().equals("0")
							&& oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& newRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& DebisysConfigListener.getCustomConfigType(context).equals(
									DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
					{
						double merchantCurrentCredit = Double.parseDouble(tmpMerchant.getLiabilityLimit());
						double merchantCurrentSales = Double.parseDouble(tmpMerchant.getRunningLiability());

						double repCreditLimit = oldRep.getCurrentLiabilityLimit();
						double repRunningLiability = oldRep.getCurrentRunningLiability();

						// Inserting Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime,
						// rep_entityaccounttype, merchant_entityaccounttype)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(oldRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_reps",
								sessionData.getLanguage()));
						pstmt.setDouble(5, 0);
						pstmt.setDouble(6, repCreditLimit - repRunningLiability);
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, oldRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));

						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						// Updating new Rep Liability Limit adding available
						// merchant credit
						repCreditLimit = newRep.getCurrentLiabilityLimit();
						repRunningLiability = newRep.getCurrentRunningLiability();

						newRep.updateLiabilityLimit((repCreditLimit + merchantCurrentCredit) - merchantCurrentSales);

						// Inserting new Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime,
						// rep_entityaccounttype, merchant_entityaccounttype)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(newRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_from_rep",
								sessionData.getLanguage()));
						pstmt.setDouble(5, merchantCurrentCredit - merchantCurrentSales);
						pstmt.setDouble(6, (repCreditLimit - repRunningLiability + merchantCurrentCredit)
								- merchantCurrentSales);
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, newRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						// Updating Merchant Liability Limit to 0
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit2"));
						pstmt.setDouble(1, 0);
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);
					} // olRep -> Shared, newRep -> Individual and Both are
					// Prepaid

					else if (newRep.getSharedBalance().equals("0")
							&& oldRep.getSharedBalance().equals("1")
							&& oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& newRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& DebisysConfigListener.getCustomConfigType(context).equals(
									DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
					{
						// Setting merchant sales on 0
						double currentRunningLiability = Double.parseDouble(tmpMerchant.getRunningLiability());
						double merchantAssignedCredit = Double.parseDouble(this.getCreditLimit());
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateRunningLiability"));
						pstmt.setDouble(1, 0);
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						// merchant_credits (merchant_id, logon_id, description,
						// payment,
						// balance, prior_terminal_sales,
						// prior_available_credit,
						// commission, log_datetime)
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
						pstmt.setString(2, sessionData.getProperty("username"));
						pstmt.setString(3, Languages.getString("com.debisys.customers.rep_change", sessionData
								.getLanguage())
								+ currentRunningLiability);
						pstmt.setDouble(4, 0);
						pstmt.setDouble(5, merchantAssignedCredit);

						pstmt.setDouble(6, currentRunningLiability);
						pstmt.setDouble(7, 0);
						pstmt.setDouble(8, 0);
						pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
						pstmt.executeUpdate();

						// DTU-369 Payment Notifications
						if (Rep.GetPaymentNotification(this.getMerchantId()))
						{
							pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
							pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
							pstmt.setString(2, sessionData.getProperty("username"));
							pstmt.setString(3, Languages.getString("com.debisys.customers.rep_change", sessionData
									.getLanguage())
									+ currentRunningLiability);
							pstmt.setDouble(4, 0);
							pstmt.setDouble(5, merchantAssignedCredit);
							pstmt.setDouble(6, currentRunningLiability);
							pstmt.setDouble(7, 0);
							pstmt.setDouble(8, 0);
							pstmt.setDouble(9, 0);
							pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
							pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
							pstmt.setString(12, this.businessName);
							pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
							pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
							pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
							pstmt.executeUpdate();
							pstmt.close();
						}

						TorqueHelper.closeStatement(pstmt, null);

						// Updating Merchant Liability Limit to value stated on
						// the interface
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit2"));
						pstmt.setDouble(1, merchantAssignedCredit);
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						// Updating new Rep Running Liability with credit
						// assigned
						// to merchant in the interface
						double repCreditLimit = newRep.getCurrentLiabilityLimit();
						double repRunningLiability = newRep.getCurrentRunningLiability();

						newRep.updateRunningLiability(repRunningLiability + merchantAssignedCredit);

						// Inserting new Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime,
						// rep_entityaccounttype, merchant_entityaccounttype)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(newRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_from_rep",
								sessionData.getLanguage()));
						pstmt.setDouble(5, merchantAssignedCredit * -1);
						pstmt.setDouble(6, repCreditLimit - (repRunningLiability + merchantAssignedCredit));
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, newRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						repCreditLimit = oldRep.getCurrentLiabilityLimit();
						repRunningLiability = oldRep.getCurrentRunningLiability();

						// Inserting old Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime,
						// rep_entityaccounttype, merchant_entityaccounttype)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(oldRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_reps",
								sessionData.getLanguage()));
						pstmt.setDouble(5, 0);
						pstmt.setDouble(6, repCreditLimit - repRunningLiability);
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, oldRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);
					}
					// olRep -> Shared, newRep -> Shared and Both are Prepaid
					else if (newRep.getSharedBalance().equals("1")
							&& oldRep.getSharedBalance().equals("1")
							&& oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& newRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
							&& DebisysConfigListener.getCustomConfigType(context).equals(
									DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
					{
						// pstmt =
						// dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
						// pstmt.setDouble(1,
						// Double.parseDouble(getMerchantId()));

						// rs = pstmt.executeQuery();
						// rs.next();
						// double currentRunningLiability =
						// rs.getDouble("runningliability");//Variable never
						// used
						// double currentLiabilityLimit =
						// rs.getDouble("liabilitylimit");//Variable never used

						TorqueHelper.closeStatement(pstmt, rs);

						double repCreditLimit = newRep.getCurrentLiabilityLimit();
						double repRunningLiability = newRep.getCurrentRunningLiability();

						// Inserting new Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(newRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_from_rep",
								sessionData.getLanguage()));
						pstmt.setDouble(5, 0);
						pstmt.setDouble(6, repCreditLimit - repRunningLiability);
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, newRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						repCreditLimit = oldRep.getCurrentLiabilityLimit();
						repRunningLiability = oldRep.getCurrentRunningLiability();

						// Inserting old Rep Merchant Credit

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(oldRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_reps",
								sessionData.getLanguage()));
						pstmt.setDouble(5, 0);
						pstmt.setDouble(6, repCreditLimit - repRunningLiability);
						pstmt.setDouble(7, repCreditLimit - repRunningLiability);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, oldRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);
					}
					else
					{
						availMerchantCredit = Double.parseDouble(tmpMerchant.getLiabilityLimit())
								- Double.parseDouble(tmpMerchant.getRunningLiability());

						// set the merchants credit to his available
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit"));

						if (newRep.getSharedBalance().equals("0")
								&& DebisysConfigListener.getDeploymentType(context).equals(
										DebisysConstants.DEPLOYMENT_INTERNATIONAL)
								&& ((oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) && newRep
										.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) || (oldRep
										.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) && newRep
										.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)))
								&& tmpMerchant.getMerchantType().equals("1"))
						{
							pstmt.setDouble(1, availMerchantCredit);
						}
						else
						{
							pstmt.setDouble(1, 0);
						}

						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						// set the old rep credit to his credit limit minus what
						// was
						// used by the merchant moving

						boolean creditPrepaidInt = false;

						if (oldRep.getSharedBalance().equals("0")
								&& DebisysConfigListener.getDeploymentType(context).equals(
										DebisysConstants.DEPLOYMENT_INTERNATIONAL)
								&& ((oldRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) && newRep
										.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) || (oldRep
										.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) && newRep
										.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)))
								&& tmpMerchant.getMerchantType().equals("1"))
						{
							creditPrepaidInt = true;
							oldRep.setPaymentAmount("0");
						}
						else if (oldRep.getSharedBalance().equals("0"))
						{
							oldRep.setPaymentAmount(Double.toString(availMerchantCredit));
						}
						else
						{
							oldRep.setPaymentAmount("0");
						}

						oldRep.setPaymentDescription("Merchant moved to another rep.");
						/* BEGIN MiniRelease 9.0 - Added by YH */
						oldRep.makePayment(sessionData, context, creditPrepaidInt);

						/* END MiniRelease 9.0 - Added by YH */
						// set the old rep credit to his credit limit minus what
						// was
						// used by the merchant moving
						if (newRep.getSharedBalance().equals("0"))
						{
							newRep.setPaymentAmount(Double.toString(Double.parseDouble(this.creditLimit) * -1));
						}
						else
						{
							newRep.setPaymentAmount("0");
						}

						newRep.setPaymentDescription("Merchant moved from another rep.");
						/* BEGIN MiniRelease 9.0 - Added by YH */
						newRep.makePayment(sessionData, context, creditPrepaidInt);

						/* END MiniRelease 9.0 - Added by YH */

						// log rep merchant credit to old rep since merchant was
						// removed
						double repCreditLimit = oldRep.getCurrentRepCreditLimit(sessionData);
						double repAssignedCredit = oldRep.getAssignedCredit(sessionData);
						double newRepAvailableCredit = repCreditLimit - repAssignedCredit;

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime,
						// rep_entityaccounttype, merchant_entityaccounttype)
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(oldRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_reps",
								sessionData.getLanguage()));

						if (oldRep.getSharedBalance().equals("0"))
						{
							pstmt.setDouble(5, availMerchantCredit);
						}
						else
						{
							pstmt.setDouble(5, 0);
						}

						pstmt.setDouble(6, newRepAvailableCredit);
						pstmt.setDouble(7, Double.parseDouble(oldRep.getAvailableCredit()));
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, oldRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						repCreditLimit = newRep.getCurrentRepCreditLimit(sessionData);
						repAssignedCredit = newRep.getAssignedCredit(sessionData);
						newRepAvailableCredit = repCreditLimit - repAssignedCredit;
						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime,
						// rep_entityaccounttype, merchant_entityaccounttype)
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(newRep.getRepId()));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.changed_from_rep",
								sessionData.getLanguage()));

						if (newRep.getSharedBalance().equals("0"))
						{
							pstmt.setDouble(5, Double.parseDouble(this.creditLimit) * -1);
						}
						else
						{
							pstmt.setDouble(5, 0);
						}

						pstmt.setDouble(6, newRepAvailableCredit);
						pstmt.setDouble(7, Double.parseDouble(newRep.getAvailableCredit()));
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, newRep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						// Update credit type and credit limit
						/*
						 * if ( if (
						 * DebisysConfigListener.getCustomConfigType(context
						 * ).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
						 * ) { if ( if (
						 * !this.merchantType.equals(DebisysConstants
						 * .MERCHANT_TYPE_SHARED) ) {
						 * this.updateLiabilityLimit2(sessionData, context); }
						 * this.updateLiabilityLimit2(sessionData, context); }
						 * this.updateMerchantType(sessionData); }//End of
						 * update credit type and credit limit update credit
						 * type and credit limit
						 */
					}
				}
				catch (Exception e)
				{
					Merchant.cat.debug("***************************");
					Merchant.cat
							.debug("Exception in Method=[updateMerchant(SessionData sessionData, ServletContext context, HttpServletRequest request)]");
					Merchant.cat.debug("In the try insertRepMerchantCredit");
					Merchant.cat.debug(e);
					Merchant.cat.debug("***************************");
				}

				/* } */
			}

			if (!this.getBusinessName().equals(tmpMerchant.getBusinessName()))
			{
				log = log + "legal_businessname|" + tmpMerchant.getBusinessName() + "|" + this.getBusinessName() + ",";
				fieldsChanged = true;
			}

			if (!this.getTaxId().equals(tmpMerchant.getTaxId()))
			{
				log = log + "tax_id|" + tmpMerchant.getTaxId() + "|" + this.getTaxId() + ",";
				fieldsChanged = true;
			}

			if (!this.getContactEmail().equals(tmpMerchant.getContactEmail()))
			{
				log = log + "email|" + tmpMerchant.getContactEmail() + "|" + this.getContactEmail() + ",";
				fieldsChanged = true;
			}

			if (!this.getContactName().equals(tmpMerchant.getContactName()))
			{
				log = log + "contact|" + tmpMerchant.getContactName() + "|" + this.getContactName() + ",";
				fieldsChanged = true;
			}

			if (!this.getDba().equals(tmpMerchant.getDba()))
			{
				log = log + "dba|" + tmpMerchant.getDba() + "|" + this.getDba() + ",";
				fieldsChanged = true;
			}

			if (!this.getContactPhone().replaceAll(Merchant.PHONE_CLEAN_REGEXP, "").equals(
					tmpMerchant.getContactPhone()))
			{
				log = log + "contact_phone|" + tmpMerchant.getContactPhone() + "|"
						+ this.getContactPhone().replaceAll(Merchant.PHONE_CLEAN_REGEXP, "") + ",";
				fieldsChanged = true;
			}

			if (!this.getContactFax().replaceAll(Merchant.PHONE_CLEAN_REGEXP, "").equals(tmpMerchant.getContactFax()))
			{
				log = log + "contact_fax|" + tmpMerchant.getContactFax() + "|"
						+ this.getContactFax().replaceAll(Merchant.PHONE_CLEAN_REGEXP, "") + ",";
				fieldsChanged = true;
			}

			if (!this.getPhysAddress().equals(tmpMerchant.getPhysAddress()))
			{
				log = log + "phys_address|" + tmpMerchant.getPhysAddress() + "|" + this.getPhysAddress() + ",";
				fieldsChanged = true;
			}

			if (!this.getPhysCity().equals(tmpMerchant.getPhysCity()))
			{
				log = log + "phys_city|" + tmpMerchant.getPhysCity() + "|" + this.getPhysCity() + ",";
				fieldsChanged = true;
			}

			if (!this.getPhysState().equals(tmpMerchant.getPhysState()))
			{
				log = log + "phys_state|" + tmpMerchant.getPhysState() + "|" + this.getPhysState() + ",";
				fieldsChanged = true;
			}

			if (!this.getPhysZip().equals(tmpMerchant.getPhysZip()))
			{
				log = log + "phys_zip|" + tmpMerchant.getPhysZip() + "|" + this.getPhysZip() + ",";
				fieldsChanged = true;
			}

			if (!this.getPhysCounty().equals(tmpMerchant.getPhysCounty()))
			{
				log = log + "phys_county|" + tmpMerchant.getPhysCounty() + "|" + this.getPhysCounty() + ",";
				fieldsChanged = true;
			}

			if (!this.getRoutingNumber().equals(tmpMerchant.getRoutingNumber()))
			{
				log = log + "aba|" + tmpMerchant.getRoutingNumber() + "|" + this.getRoutingNumber() + ",";
				fieldsChanged = true;
			}

			if (!this.getAccountNumber().equals(tmpMerchant.getAccountNumber()))
			{
				log = log + "account_no|" + tmpMerchant.getAccountNumber() + "|" + this.getAccountNumber() + ",";
				fieldsChanged = true;
			}

			// cancelled field usurped for credit check flag
			if (!this.getNoCreditCheck().equals(tmpMerchant.getNoCreditCheck()))
			{
				log = log + "cancelled|" + tmpMerchant.getNoCreditCheck() + "|" + this.getNoCreditCheck() + ",";
				fieldsChanged = true;
			}

			if (!this.getMonthlyFeeThreshold().equals(tmpMerchant.getMonthlyFeeThreshold()))
			{
				log = log + "rentwaivedatamount|" + tmpMerchant.getMonthlyFeeThreshold() + "|"
						+ this.getMonthlyFeeThreshold() + "";
				fieldsChanged = true;
			}

			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				if (!this.getMailAddress().equals(tmpMerchant.getMailAddress()))
				{
					log = log + ",mailAddress|" + tmpMerchant.getMailAddress() + "|" + this.getMailAddress();
					fieldsChanged = true;
				}

				if (!this.getMailCity().equals(tmpMerchant.getMailCity()))
				{
					log = log + ",mailCity|" + tmpMerchant.getMailCity() + "|" + this.getMailCity();
					fieldsChanged = true;
				}

				if (!this.getMailState().equals(tmpMerchant.getMailState()))
				{
					log = log + ",mailState|" + tmpMerchant.getMailState() + "|" + this.getMailState();
					fieldsChanged = true;
				}

				if (!this.getMailZip().equals(tmpMerchant.getMailZip()))
				{
					log = log + ",mailZip|" + tmpMerchant.getMailZip() + "|" + this.getMailZip();
					fieldsChanged = true;
				}

				if (!this.getMailCountry().equals(tmpMerchant.getMailCountry()))
				{
					log = log + ",mailCountry|" + tmpMerchant.getMailCountry() + "|" + this.getMailCountry();
					fieldsChanged = true;
				}

				if (!this.getPhysColony().equals(tmpMerchant.getPhysColony()))
				{
					log = log + ",physColony|" + tmpMerchant.getPhysColony() + "|" + this.getPhysColony();
					fieldsChanged = true;
				}

				if (!this.getPhysDelegation().equals(tmpMerchant.getPhysDelegation()))
				{
					log = log + ",physDelegation|" + tmpMerchant.getPhysDelegation() + "|" + this.getPhysDelegation();
					fieldsChanged = true;
				}

				if (!this.getMailColony().equals(tmpMerchant.getMailColony()))
				{
					log = log + ",mailColony|" + tmpMerchant.getMailColony() + "|" + this.getMailColony();
					fieldsChanged = true;
				}

				if (!this.getMailDelegation().equals(tmpMerchant.getMailDelegation()))
				{
					log = log + ",mailDelegation|" + tmpMerchant.getMailDelegation() + "|" + this.getMailDelegation();
					fieldsChanged = true;
				}

				if (!this.getMailCounty().equals(tmpMerchant.getMailCounty()))
				{
					log = log + ",mailCounty|" + tmpMerchant.getMailCounty() + "|" + this.getMailCounty();
					fieldsChanged = true;
				}

				if (!this.getBankName().equals(tmpMerchant.getBankName()))
				{
					log = log + ",bankName|" + tmpMerchant.getBankName() + "|" + this.getBankName();
					fieldsChanged = true;
				}

				if (this.getAccountTypeId() != tmpMerchant.getAccountTypeId())
				{
					log = log + ",accountTypeId|" + tmpMerchant.getAccountTypeId() + "|" + this.getAccountTypeId();
					fieldsChanged = true;
				}

				if (this.getMerchantSegmentId() != tmpMerchant.getMerchantSegmentId())
				{
					log = log + ",merchantSegmentId|" + tmpMerchant.getMerchantSegmentId() + "|"
							+ this.getMerchantSegmentId();
					fieldsChanged = true;
				}

				if (!this.getBusinessHours().equals(tmpMerchant.getBusinessHours()))
				{
					log = log + ",businessHours|" + tmpMerchant.getBusinessHours() + "|" + this.getBusinessHours();
					fieldsChanged = true;
				}

				if (this.getCreditTypeId() != tmpMerchant.getCreditTypeId())
				{
					log = log + ",creditTypeId|" + tmpMerchant.getCreditTypeId() + "|" + this.getCreditTypeId();
					fieldsChanged = true;
				}

				// DBSY-1072 eAccounts Interface
				if (this.getEntityAccountType() != tmpMerchant.getEntityAccountType())
				{
					log = log + ",entityAccountType|" + tmpMerchant.getEntityAccountType() + "|"
							+ this.getEntityAccountType();
					fieldsChanged = true;
				}
			}

			if (fieldsChanged)
			{
				Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.merchant_updated",
						sessionData.getLanguage())
						+ "[" + log + "]", DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
						DebisysConstants.PW_REF_TYPE_MERCHANT);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateMerchant", e);
			throw new EntityException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	/**
	 * Updates running liability in the merchants table.
	 * 
	 * @throws CustomerException
	 *             Thrown if there is an error updating the record in the
	 *             database. database.
	 */
	public void resetBalance(SessionData sessionData) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
			pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));

			rs = pstmt.executeQuery();
			rs.next();

			double currentRunningLiability = rs.getDouble("runningliability");
			double currentLiabilityLimit = rs.getDouble("liabilitylimit");
			double currentAvailableCredit = currentLiabilityLimit - currentRunningLiability;

			TorqueHelper.closeStatement(pstmt, rs);

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiability"));

			if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
			{
				pstmt.setDouble(1, currentLiabilityLimit);
			}
			else
			{
				pstmt.setDouble(1, currentAvailableCredit);
			}

			pstmt.setDouble(2, Double.parseDouble(this.getMerchantId()));
			pstmt.executeUpdate();

			TorqueHelper.closeStatement(pstmt, rs);

			if ((this.paymentDescription != null) && (this.paymentDescription.length() > 255))
			{
				this.paymentDescription = this.paymentDescription.substring(0, 255);
			}

			// merchant_credits (merchant_id, logon_id, description, payment,
			// balance, prior_terminal_sales, prior_available_credit,
			// commission, log_datetime)
			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCredit"));
			pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
			pstmt.setString(2, sessionData.getProperty("username"));
			pstmt.setString(3, this.paymentDescription);
			pstmt.setDouble(4, 0);

			if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
			{
				pstmt.setDouble(5, currentLiabilityLimit);
			}
			else
			{
				pstmt.setDouble(5, currentAvailableCredit);
			}

			pstmt.setDouble(6, currentRunningLiability);
			pstmt.setDouble(7, currentAvailableCredit);
			pstmt.setDouble(8, 0);
			pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
			pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
			pstmt.executeUpdate();

			// DTU-369 Payment Notifications
			if (Rep.GetPaymentNotification(this.getMerchantId()))
			{
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
				pstmt.setString(2, sessionData.getProperty("username"));
				pstmt.setString(3, this.paymentDescription);
				pstmt.setDouble(4, 0);
				if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
				{
					pstmt.setDouble(5, currentLiabilityLimit);
				}
				else
				{
					pstmt.setDouble(5, currentAvailableCredit);
				}
				pstmt.setDouble(6, currentRunningLiability);
				pstmt.setDouble(7, currentAvailableCredit);
				pstmt.setDouble(8, 0);
				pstmt.setDouble(9, 0);
				pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
				pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
				pstmt.setString(12, this.businessName);
				pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
				pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
				pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
				pstmt.executeUpdate();
				pstmt.close();
			}

			TorqueHelper.closeStatement(pstmt, rs);

			if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
			{
				Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.running_liability",
						sessionData.getLanguage())
						+ NumberUtil.formatCurrency(Double.toString(currentRunningLiability))
						+ Languages.getString("com.debisys.customers.Merchant.Log.running_liability_limit", sessionData
								.getLanguage()) + NumberUtil.formatCurrency(Double.toString(currentLiabilityLimit)),
						DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
			}
			else
			{
				Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.running_liability",
						sessionData.getLanguage())
						+ NumberUtil.formatCurrency(Double.toString(currentRunningLiability))
						+ Languages.getString("com.debisys.customers.Merchant.Log.running_liability_limit", sessionData
								.getLanguage()) + NumberUtil.formatCurrency(Double.toString(currentAvailableCredit)),
						DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during resetBalance", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	/**
	 * Updates running liability in the merchants table. The 2 denotes the old
	 * way of doing this. way of doing this.
	 * 
	 * @throws CustomerException
	 *             Thrown if there is an error updating the record in the
	 *             database. database.
	 */
	public void resetBalance2(SessionData sessionData) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getRunningLiability"));
			pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));

			rs = pstmt.executeQuery();
			rs.next();

			String currentRunningLiability = NumberUtil.formatAmount(rs.getString("runningliability"));

			TorqueHelper.closeStatement(pstmt, rs);

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiability2"));
			pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
			pstmt.executeUpdate();

			TorqueHelper.closeStatement(pstmt, rs);

			Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.running_liability",
					sessionData.getLanguage())
					+ NumberUtil.formatCurrency(currentRunningLiability)
					+ Languages.getString("com.debisys.customers.Merchant.Log.running_liability_limit_to0", sessionData
							.getLanguage()), DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
					DebisysConstants.PW_REF_TYPE_MERCHANT);
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during resetBalance2", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	/**
	 * Updates an liability limit in the merchants table.
	 * 
	 * @throws CustomerException
	 *             Thrown if there is an error updating the record in the
	 *             database. database.
	 */
	public void updateLiabilityLimit(SessionData sessionData, ServletContext context) throws CustomerException
	{
		String deploymentType = DebisysConfigListener.getDeploymentType(context);

		if (NumberUtil.isNumeric(this.getPaymentAmount()))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
				pstmt.setDouble(1, Double.parseDouble(this.merchantId));

				rs = pstmt.executeQuery();
				rs.next();

				double currentRunningLiability = rs.getDouble("runningliability");
				double currentLiabilityLimit = rs.getDouble("liabilitylimit");
				double currentAvailableCredit = currentLiabilityLimit - currentRunningLiability;

				if ((currentAvailableCredit + Double.parseDouble(this.getPaymentAmount())) < 0)
				{
					this.addFieldError("paymentError", "Invalid payment");

					return;
				}

				TorqueHelper.closeStatement(pstmt, rs);

				Merchant.cat.debug(Merchant.sql_bundle.getString("updateLiabilityLimit"));
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit"));
				pstmt.setDouble(1, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
				pstmt.setDouble(2, Double.parseDouble(this.getMerchantId()));
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, rs);

				if ((this.paymentDescription != null) && (this.paymentDescription.length() > 255))
				{
					this.paymentDescription = this.paymentDescription.substring(0, 255);
				}

				if (!NumberUtil.isNumeric(this.commission))
				{
					this.commission = "0";
				}

				double dblCommission = Double.parseDouble(this.commission);

				Merchant.cat.debug(Merchant.sql_bundle.getString("insertMerchantCredit"));
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCredit"));
				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
				pstmt.setString(2, sessionData.getProperty("username"));
				pstmt.setString(3, this.paymentDescription);
				pstmt.setDouble(4, Double.parseDouble(this.getPaymentAmount()));
				pstmt.setDouble(5, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
				pstmt.setDouble(6, currentRunningLiability);
				pstmt.setDouble(7, currentAvailableCredit);
				pstmt.setDouble(8, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
				pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
				pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
				pstmt.executeUpdate();

				// DTU-369 Payment Notifications
				if (Rep.GetPaymentNotification(this.getMerchantId()))
				{
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
					pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
					pstmt.setString(2, sessionData.getProperty("username"));
					pstmt.setString(3, this.paymentDescription);
					pstmt.setDouble(4, Double.parseDouble(this.getPaymentAmount()));
					pstmt.setDouble(5, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
					pstmt.setDouble(6, currentRunningLiability);
					pstmt.setDouble(7, currentAvailableCredit);
					pstmt.setDouble(8, 0);
					pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
					pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
					pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
					pstmt.setString(12, this.businessName);
					pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
					pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
					pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
					pstmt.executeUpdate();
					pstmt.close();
				}

				TorqueHelper.closeStatement(pstmt, rs);

				double dblNewAvailableCredit = currentAvailableCredit + Double.parseDouble(this.getPaymentAmount());
				String strNewAvailableCredit = NumberUtil.formatCurrency(Double.toString(dblNewAvailableCredit));

				Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.liability_limit",
						sessionData.getLanguage())
						+ NumberUtil.formatCurrency(Double.toString(currentLiabilityLimit))
						+ " "
						+ Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + strNewAvailableCredit,
						DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);

				if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
				{
					Rep rep = new Rep();
					rep.setRepId(this.repId);
					rep.getRep(sessionData, context);

					// if the reps not unlimited, log the merchant credit
					// assignment
					if (!rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
					{
						double repCreditLimit = rep.getCurrentRepCreditLimit(sessionData);
						double repAssignedCredit = rep.getAssignedCredit(sessionData);
						double changeInMerchantCredit = dblNewAvailableCredit - currentAvailableCredit;

						// add an entry to show the rep credits being used
						// 1 2 3 4 5 6 7 8 9 10
						// (rep_id, merchant_id, logon_id, description, amount,
						// available_credit, prior_available_credit, datetime,
						// rep_entityaccountype, merchant_entityaccounttype)
						Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
						pstmt.setDouble(1, Double.parseDouble(this.repId));
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
						pstmt.setString(3, sessionData.getProperty("username"));
						pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.credit_updated",
								sessionData.getLanguage()));
						pstmt.setDouble(5, changeInMerchantCredit * -1);
						pstmt.setDouble(6, repCreditLimit - (repAssignedCredit + changeInMerchantCredit));
						pstmt.setDouble(7, repCreditLimit - repAssignedCredit);
						pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(9, rep.getEntityAccountType());
						pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, rs);

						// update reps running liability
						// updateRepRunningLiability=update set
						// runningliability=? where rep_id=?
						Merchant.cat.debug(Merchant.sql_bundle.getString("updateRepRunningLiability"));
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateRepRunningLiability"));
						pstmt.setDouble(1, repAssignedCredit + changeInMerchantCredit);
						pstmt.setDouble(2, Double.parseDouble(this.repId));
						pstmt.executeUpdate();
					}
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during updateLiabilityLimit", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}
	}

	public void changeFromIdividualToShared(SessionData sessionData, ServletContext context) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;

		try
		{
			// String log = "";//Variable never used
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new EntityException();
			}

			Rep rep = new Rep();
			rep.setRepId(this.repId);
			rep.getRep(sessionData, context);

			double repCreditLimit = rep.getCurrentLiabilityLimit();
			double repRunningLiability = rep.getCurrentRunningLiability();

			double merchantCurrentCredit = Double.parseDouble(this.getLiabilityLimit());
			double merchantCurrentSales = Double.parseDouble(this.getRunningLiability());

			// Updating Rep Liability Limit adding merchant
			// liability limit
			rep.updateLiabilityLimit(repCreditLimit + merchantCurrentCredit);

			// Updating Rep Running Liability adding merchant
			// running liability
			rep.updateRunningLiability(repRunningLiability + merchantCurrentSales);

			// Inserting Rep Merchant Credit

			// add an entry to show the rep credits being used
			// 1 2 3 4 5 6 7 8 9 10
			// (rep_id, merchant_id, logon_id, description, amount,
			// available_credit, prior_available_credit, datetime,
			// rep_entityaccounttype, merchant_entityaccounttype)
			Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
			pstmt.setDouble(1, Double.parseDouble(rep.getRepId()));
			pstmt.setDouble(2, Double.parseDouble(this.merchantId));
			pstmt.setString(3, sessionData.getProperty("username"));
			pstmt.setString(4, Languages.getString("com.debisys.customers.merchant_migration", sessionData
					.getLanguage())
					+ this.getDba());
			pstmt.setDouble(5, merchantCurrentCredit - merchantCurrentSales);
			pstmt.setDouble(6, (repCreditLimit - repRunningLiability + merchantCurrentCredit) - merchantCurrentSales);
			pstmt.setDouble(7, repCreditLimit - repRunningLiability);
			pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
			pstmt.setInt(9, rep.getEntityAccountType());
			pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
			pstmt.executeUpdate();

		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during changeFromIdividualToShared", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	public void changeFromSharedToIdividual(SessionData sessionData, ServletContext context) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		try
		{
			// String log = "";//Variable never used
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new EntityException();
			}

			Rep rep = new Rep();
			rep.setRepId(this.repId);
			rep.getRep(sessionData, context);

			double repCreditLimit = rep.getCurrentLiabilityLimit();
			double repRunningLiability = rep.getCurrentRunningLiability();

			double merchantCurrentCredit = this.getCurrentLiabilityLimit();
			double merchantCurrentSales = this.getCurrentRunningLiability();
			double merchantAsignedCredit = Double.parseDouble(this.creditLimit);

			// Update Merchant Liability Limit with value specified by user
			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit2"));
			pstmt.setDouble(1, merchantAsignedCredit);
			pstmt.setDouble(2, Double.parseDouble(this.merchantId));
			pstmt.executeUpdate();

			TorqueHelper.closeStatement(pstmt, null);

			// Update Rep Liability Limit adding merchant
			// liability limit
			rep.updateLiabilityLimit(repCreditLimit + merchantAsignedCredit);

			// Update Rep Running Liability adding merchant
			// liability limit
			rep.updateRunningLiability(repRunningLiability + merchantAsignedCredit);

			// Insert Merchant Credit

			// merchant_credits (merchant_id, logon_id, description,
			// payment, balance, prior_terminal_sales,
			// prior_available_credit, commission, log_datetime)
			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCredit"));
			pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
			pstmt.setString(2, sessionData.getProperty("username"));
			pstmt.setString(3, Languages.getString("com.debisys.customers.rep_change2", sessionData.getLanguage())
					+ merchantCurrentSales);
			pstmt.setDouble(4, 0);
			pstmt.setDouble(5, merchantAsignedCredit);
			pstmt.setDouble(6, merchantCurrentSales);
			pstmt.setDouble(7, merchantCurrentCredit - merchantCurrentSales);
			pstmt.setDouble(8, 0);
			pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
			pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
			pstmt.executeUpdate();
                        
			// DTU-369 Payment Notifications
			if (Rep.GetPaymentNotification(this.getMerchantId()))
			{
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
				pstmt.setString(2, sessionData.getProperty("username"));
				pstmt.setString(3, Languages.getString("com.debisys.customers.rep_change2", sessionData.getLanguage())
						+ merchantCurrentSales);
				pstmt.setDouble(4, 0);
				pstmt.setDouble(5, merchantAsignedCredit);
				pstmt.setDouble(6, merchantCurrentSales);
				pstmt.setDouble(7, merchantCurrentCredit - merchantCurrentSales);
				pstmt.setDouble(8, 0);
				pstmt.setDouble(9, 0);
				pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
				pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
				pstmt.setString(12, this.businessName);
				pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
				pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
				pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
				pstmt.executeUpdate();
				pstmt.close();
			}

		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during changeFromIdividualToShared", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	public double getCurrentLiabilityLimit() throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			// select RunningLiability,LiabilityLimit from merchants (nolock)
			// where merchant_id = ?			
                        double currentLiabilityLimit = 0D;
                        if (this.merchantId != null) {
                            pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
                            pstmt.setDouble(1, Double.parseDouble(this.merchantId));

                            rs = pstmt.executeQuery();
                            rs.next();

                            currentLiabilityLimit = Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));
                            
                        }			
                        return currentLiabilityLimit;
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during Rep's updateRunningLiability", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
	}

	public double getCurrentRunningLiability() throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}
                        
                        double currentLiabilityLimit = 0d;
                        if(this.merchantId != null) {
                            pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
			pstmt.setDouble(1, Double.parseDouble(this.merchantId));

			rs = pstmt.executeQuery();
			rs.next();

			currentLiabilityLimit = Double
					.parseDouble(NumberUtil.formatAmount(rs.getString("RunningLiability")));
                        }		

			return currentLiabilityLimit;
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during Rep's updateRunningLiability", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
	}

	// this was added inorder to support the old way/domestic of setting credit
	// limits
	public void updateLiabilityLimit2(SessionData sessionData, ServletContext context) throws CustomerException,
			UserException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))
		{
                    sessionData.setProperty("iso_id", (new User()).getISOId(DebisysConstants.MERCHANT, this.merchantId));
                    sessionData.setProperty("iso_id", sessionData.getUser().getIsoId());
		}
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilityLimit"));
			pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));

			rs = pstmt.executeQuery();
			rs.next();

			String currentLiabilityLimit = NumberUtil.formatAmount(rs.getString("liabilitylimit"));
                        String runningliability = NumberUtil.formatAmount(rs.getString("runningliability"));
                        int entityAccountType = rs.getInt("entityAccountType");
                        int creditTypeId = rs.getInt("CreditTypeId");
                        

			TorqueHelper.closeStatement(pstmt, rs);
                        
                        Rep.insertBalanceHistory(DebisysConstants.REP_TYPE_MERCHANT, this.getMerchantId(), this.repId,
                                  ""+creditTypeId,this.sharedBalance, entityAccountType, runningliability, ""+currentLiabilityLimit, sessionData.getProperty("username"),true);
                                

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit2"));
			pstmt.setDouble(1, Double.parseDouble(this.creditLimit));
			pstmt.setDouble(2, Double.parseDouble(this.merchantId));
			pstmt.executeUpdate();

			TorqueHelper.closeStatement(pstmt, null);

			Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.liability_limit",
					sessionData.getLanguage())
					+ currentLiabilityLimit
					+ " "
					+ Languages.getString("jsp.admin.to", sessionData.getLanguage())
					+ " " + NumberUtil.formatCurrency(this.creditLimit), DebisysConstants.LOGTYPE_CUSTOMER,
					this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);

			Rep rep = new Rep();
			rep.setRepId(this.repId);
			rep.getRep(sessionData, context);

			// if the reps not unlimited, log the merchant credit assignment
			if (!rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
			{
				double repCreditLimit = rep.getCurrentRepCreditLimit(sessionData);
				double repAssignedCredit = rep.getAssignedCredit(sessionData);
				double changeInMerchantCredit = Double.parseDouble(this.creditLimit)
						- Double.parseDouble(currentLiabilityLimit);

				if (DebisysConfigListener.getCustomConfigType(context).equals(
						DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
						&& sessionData.getProperty("MXEnableSharedBalance").equals("1"))
				{
					changeInMerchantCredit = -(Double.parseDouble(currentLiabilityLimit));
				}

				// add an entry to show the rep credits being used
				// 1 2 3 4 5 6 7 8 9 10
				// (rep_id, merchant_id, logon_id, description, amount,
				// available_credit, prior_available_credit, datetime,
				// rep_entityaccounttype,
				// merchant_entityaccounttype)
				Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
				pstmt.setDouble(1, Double.parseDouble(this.repId));
				pstmt.setDouble(2, Double.parseDouble(this.merchantId));
				pstmt.setString(3, sessionData.getProperty("username"));
				pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.credit_updated", sessionData
						.getLanguage()));
				pstmt.setDouble(5, changeInMerchantCredit * -1);
				pstmt.setDouble(6, repCreditLimit - (repAssignedCredit + changeInMerchantCredit));
				pstmt.setDouble(7, repCreditLimit - repAssignedCredit);
				pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
				Rep parentrep = new Rep();
				parentrep.setRepId(this.repId);
				parentrep.getRep(sessionData, context);
				pstmt.setInt(9, parentrep.getEntityAccountType());
				pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, null);

				// update reps running liability
				// updateRepRunningLiability=update set runningliability=?
				// where rep_id=?
				Merchant.cat.debug(Merchant.sql_bundle.getString("updateRepRunningLiability"));
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateRepRunningLiability"));
				pstmt.setDouble(1, repAssignedCredit + changeInMerchantCredit);
				pstmt.setDouble(2, Double.parseDouble(this.repId));
				pstmt.executeUpdate();
			}
			// }
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateLiabilityLimit2", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	// this was added inorder to support international hard credit limits
	public void updateRunningLiability(SessionData sessionData, ServletContext context) throws CustomerException
	{
		String deploymentType = DebisysConfigListener.getDeploymentType(context);

		if (NumberUtil.isNumeric(this.paymentAmount)
				&& (this.merchantType != null)
				&& (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT) || this.merchantType
						.equals(DebisysConstants.MERCHANT_TYPE_SHARED)))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));

				rs = pstmt.executeQuery();
				rs.next();

				double currentRunningLiability = rs.getDouble("runningliability");
				double currentLiabilityLimit = rs.getDouble("liabilitylimit");
				double currentAvailableCredit = currentLiabilityLimit - currentRunningLiability;
				double dblPaymentAmount = Double.parseDouble(this.paymentAmount);
				double newRunningLiability = Double.parseDouble(NumberUtil.formatAmount(Double
						.toString(currentRunningLiability - dblPaymentAmount)));

				TorqueHelper.closeStatement(pstmt, null);

				if (((dblPaymentAmount <= currentRunningLiability) && ((currentRunningLiability - dblPaymentAmount) <= currentLiabilityLimit))
						|| this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_SHARED))
				{
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateRunningLiability"));
					pstmt.setDouble(1, newRunningLiability);
					pstmt.setDouble(2, Double.parseDouble(this.merchantId));
					pstmt.executeUpdate();

					TorqueHelper.closeStatement(pstmt, null);

					if ((this.paymentDescription != null) && (this.paymentDescription.length() > 255))
					{
						this.paymentDescription = this.paymentDescription.substring(0, 255);
					}

					if (!NumberUtil.isNumeric(this.commission))
					{
						this.commission = "0";
					}

					double dblCommission = Double.parseDouble(this.commission);
					double dblNewAvailableCredit = currentLiabilityLimit - newRunningLiability;

					// merchant_credits (merchant_id, logon_id, description,
					// payment, balance, prior_terminal_sales,
					// prior_available_credit, commission, log_datetime)
					if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_SHARED))
					{
						dblNewAvailableCredit = 0;
						currentAvailableCredit = 0;
					}

					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCredit"));
					pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
					pstmt.setString(2, sessionData.getProperty("username"));
					pstmt.setString(3, this.paymentDescription);
					pstmt.setDouble(4, Double.parseDouble(this.paymentAmount));
					pstmt.setDouble(5, dblNewAvailableCredit);
					pstmt.setDouble(6, currentRunningLiability);
					pstmt.setDouble(7, currentAvailableCredit);
					pstmt.setDouble(8, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
					pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
					pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
					pstmt.executeUpdate();

					// DTU-369 Payment Notifications
					if (Rep.GetPaymentNotification(this.getMerchantId()))
					{
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
						pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
						pstmt.setString(2, sessionData.getProperty("username"));
						pstmt.setString(3, this.paymentDescription);
						pstmt.setDouble(4, Double.parseDouble(this.paymentAmount));
						pstmt.setDouble(5, dblNewAvailableCredit);
						pstmt.setDouble(6, currentRunningLiability);
						pstmt.setDouble(7, currentAvailableCredit);
						pstmt.setDouble(8, 0);
						pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
						pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
						pstmt.setString(12, this.businessName);
						pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
						pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
						pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
						pstmt.executeUpdate();
						pstmt.close();
					}

					TorqueHelper.closeStatement(pstmt, null);

					Log.write(sessionData, Languages.getString(
							"com.debisys.customers.Merchant.Log.running_liability_changed", sessionData.getLanguage())
							+ NumberUtil.formatCurrency(Double.toString(currentRunningLiability))
							+ " "
							+ Languages.getString("jsp.admin.to", sessionData.getLanguage())
							+ " "
							+ NumberUtil.formatCurrency(Double.toString(newRunningLiability)),
							DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);

					if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
					{
						Rep rep = new Rep();
						rep.setRepId(this.repId);
						rep.getRep(sessionData, context);

						// if the reps not unlimited, log the merchant credit
						// assignment
						if (!rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)
								&& !this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_SHARED))
						{
							double repCreditLimit = rep.getCurrentRepCreditLimit(sessionData);
							double repAssignedCredit = rep.getAssignedCredit(sessionData);
							double changeInMerchantCredit = dblNewAvailableCredit - currentAvailableCredit;

							// add an entry to show the rep credits being used
							// 1 2 3 4 5 6 7 8 9 10
							// (rep_id, merchant_id, logon_id, description,
							// amount, available_credit, prior_available_credit,
							// datetime)
							Merchant.cat.debug(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
							pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertRepMerchantCredit"));
							pstmt.setDouble(1, Double.parseDouble(this.repId));
							pstmt.setDouble(2, Double.parseDouble(this.merchantId));
							pstmt.setString(3, sessionData.getProperty("username"));
							pstmt.setString(4, Languages.getString("com.debisys.customers.Merchant.credit_updated",
									sessionData.getLanguage()));
							pstmt.setDouble(5, changeInMerchantCredit * -1);
							pstmt.setDouble(6, repCreditLimit - (repAssignedCredit + changeInMerchantCredit));
							pstmt.setDouble(7, repCreditLimit - repAssignedCredit);
							pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
							pstmt.setInt(9, rep.getEntityAccountType());
							pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.merchantId));
							pstmt.executeUpdate();

							TorqueHelper.closeStatement(pstmt, null);

							// update reps running liability
							// updateRepRunningLiability=update set
							// runningliability=? where rep_id=?
							Merchant.cat.debug(Merchant.sql_bundle.getString("updateRepRunningLiability"));
							pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateRepRunningLiability"));
							pstmt.setDouble(1, repAssignedCredit + changeInMerchantCredit);
							pstmt.setDouble(2, Double.parseDouble(this.repId));
							pstmt.executeUpdate();

							TorqueHelper.closeStatement(pstmt, null);
						}
					}
				}
				else
				{
					if (dblPaymentAmount > currentRunningLiability)
					{
						this.addFieldError("error1", "Payment must be less than terminal sales.");
					}
					else if ((currentRunningLiability - dblPaymentAmount) > currentLiabilityLimit)
					{
						this.addFieldError("error2", "Payment is greater than credit limit.");
					}
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during updateRunningLiability", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}
	} // End of function updateRunningLiability

	public void updateMerchantType(SessionData sessionData, ServletContext context) throws CustomerException
	{
		if (this.checkPermission(sessionData) && NumberUtil.isNumeric(this.merchantType))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));

				rs = pstmt.executeQuery();
				rs.next();

				double currentRunningLiability = rs.getDouble("runningliability");
				double currentLiabilityLimit = rs.getDouble("liabilitylimit");
				double currentAvailableCredit = currentLiabilityLimit - currentRunningLiability;

				TorqueHelper.closeStatement(pstmt, rs);
				// update merchants set merchant_type=?, cancelled=?,
				// runningliability=?,
				// liabilitylimit=? where merchant_id=?
                                
                                Rep.insertBalanceHistory(DebisysConstants.REP_TYPE_MERCHANT, this.getMerchantId(), this.repId,
                                  ""+this.creditTypeId,this.sharedBalance, this.entityAccountType, ""+currentRunningLiability, ""+currentLiabilityLimit, sessionData.getProperty("username"),true);
                                        
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateMerchantType"));
				pstmt.setInt(1, Integer.parseInt(this.merchantType));

				if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
				{
					pstmt.setDouble(2, 0);
					pstmt.setDouble(3, 0);
				}
				else if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_SHARED)
						&& (sessionData.getProperty("MXEnableSharedBalance").equals("1")))
				{
					pstmt.setDouble(2, currentRunningLiability);
					pstmt.setDouble(3, 0);
				}
				else if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_PREPAID)
						&& sessionData.getProperty("MXDisableSharedBalance").equals("1"))
				{
					pstmt.setDouble(2, 0);
					pstmt.setDouble(3, currentLiabilityLimit);
				}
				else
				{
					pstmt.setDouble(2, currentRunningLiability);
					pstmt.setDouble(3, currentLiabilityLimit);
				}

				pstmt.setDouble(4, Double.parseDouble(this.merchantId));
				pstmt.executeUpdate();                              

				TorqueHelper.closeStatement(pstmt, null);

				if ((this.paymentDescription != null) && (this.paymentDescription.length() > 255))
				{
					this.paymentDescription = this.paymentDescription.substring(0, 255);
				}

				if (!((this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_PREPAID) && (sessionData
						.getProperty("MXDisableSharedBalance").equals("1"))) || (this.merchantType
						.equals(DebisysConstants.MERCHANT_TYPE_SHARED) && (sessionData
						.getProperty("MXEnableSharedBalance").equals("1")))))
				{
					
                                        Merchant mTemp = new Merchant();
					mTemp.setMerchantId(this.getMerchantId());
					mTemp.getMerchant(sessionData, context);
                                        Rep.insertBalanceHistory(DebisysConstants.REP_TYPE_MERCHANT, this.getMerchantId(), mTemp.repId,
                                                        ""+mTemp.creditTypeId,mTemp.sharedBalance, mTemp.entityAccountType, mTemp.runningLiability, mTemp.liabilityLimit, sessionData.getProperty("username"),true);
                                        
                                        // merchant_credits (merchant_id, logon_id, description,
					// payment, balance, prior_terminal_sales,
					// prior_available_credit, commission, log_datetime)
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCredit"));
					pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
					pstmt.setString(2, sessionData.getProperty("username"));
					pstmt.setString(3, this.paymentDescription);
					pstmt.setDouble(4, 0);

					if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
					{
						pstmt.setDouble(5, 0);
					}
					else if ((this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_SHARED))
							&& sessionData.getProperty("MXDisableSharedBalance").equals("1"))
					{
						pstmt.setDouble(4, currentRunningLiability);
						pstmt.setDouble(5, Double.parseDouble(NumberUtil.formatAmount(Double
								.toString(currentAvailableCredit)))
								+ currentRunningLiability);
					}
					else if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_PREPAID)
							&& sessionData.getProperty("MXEnableSharedBalance").equals("1"))
					{
						pstmt.setDouble(4, currentRunningLiability);
						pstmt.setDouble(5, 0);
					}
					else
					{
						pstmt.setDouble(5, Double.parseDouble(NumberUtil.formatAmount(Double
								.toString(currentAvailableCredit))));
					}

					pstmt.setDouble(6, currentRunningLiability);
					pstmt.setDouble(7, Double.parseDouble(NumberUtil.formatAmount(Double
							.toString(currentAvailableCredit))));
					pstmt.setDouble(8, 0);
					pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
					pstmt.setInt(10, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
					pstmt.executeUpdate();

					// DTU-369 Payment Notifications
					if (Rep.GetPaymentNotification(this.getMerchantId()))
					{
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
						pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
						pstmt.setString(2, sessionData.getProperty("username"));
						pstmt.setString(3, this.paymentDescription);
						if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED))
						{
							pstmt.setDouble(4, 0);
							pstmt.setDouble(5, 0);
						}
						else if ((this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_SHARED))
								&& sessionData.getProperty("MXDisableSharedBalance").equals("1"))
						{
							pstmt.setDouble(4, currentRunningLiability);
							pstmt.setDouble(5, Double.parseDouble(NumberUtil.formatAmount(Double
									.toString(currentAvailableCredit)))
									+ currentRunningLiability);
						}
						else if (this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_PREPAID)
								&& sessionData.getProperty("MXEnableSharedBalance").equals("1"))
						{
							pstmt.setDouble(4, currentRunningLiability);
							pstmt.setDouble(5, 0);
						}
						else
						{
							pstmt.setDouble(4, 0);
							pstmt.setDouble(5, Double.parseDouble(NumberUtil.formatAmount(Double
									.toString(currentAvailableCredit))));
						}
						pstmt.setDouble(6, currentRunningLiability);
						pstmt.setDouble(7, Double.parseDouble(NumberUtil.formatAmount(Double
								.toString(currentAvailableCredit))));
						pstmt.setDouble(8, 0);
						pstmt.setDouble(9, 0);
						pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
						pstmt.setString(12, this.businessName);
						pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
						pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
						pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
						pstmt.executeUpdate();
						pstmt.close();
					}

					TorqueHelper.closeStatement(pstmt, null);

					Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.merchant_changed",
							sessionData.getLanguage())
							+ this.currentMerchantType
							+ " "
							+ Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + this.merchantType,
							DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
				}

				if (!this.currentMerchantType.equals(this.merchantType))
				{
					insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_MERCHANT, this.merchantId,DebisysConstants.AUDIT_CREDITTYPE_CHANGE,currentMerchantType, this.merchantType);
                                        // Retrieve original data of merchant and check if he has
					// the SalesLimit enabled
					Merchant m2 = new Merchant();
					LogChanges lc = new LogChanges();
					String sNote = null;

					m2.setMerchantId(this.merchantId);
					m2.getMerchant(sessionData, context);

					if (!StringUtil.toString(m2.getSalesLimitType()).equals(""))
					{
						lc.setSession(sessionData);
						lc.setContext(context);
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTYPE), m2
								.getSalesLimitType());
						lc.set_value_for_check(lc
								.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITLIABILITYLIMIT), String
								.valueOf(m2.getSalesLimitLiabilityLimit()));
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITMONDAY),
								String.valueOf(m2.getSalesLimitMonday()));
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTUESDAY),
								String.valueOf(m2.getSalesLimitTuesday()));
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITWEDNESDAY),
								String.valueOf(m2.getSalesLimitWednesday()));
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTHURSDAY),
								String.valueOf(m2.getSalesLimitThursday()));
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITFRIDAY),
								String.valueOf(m2.getSalesLimitFriday()));
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSATURDAY),
								String.valueOf(m2.getSalesLimitSaturday()));
						lc.set_value_for_check(lc.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSUNDAY),
								String.valueOf(m2.getSalesLimitSunday()));
						m2.setSalesLimitType("");
						m2.updateSalesLimits(sessionData, context);
						sNote = Languages.getString("jsp.admin.saleslimit.noteChangeType", sessionData.getLanguage())
								.replaceAll("_DBA_", m2.getDba()).replaceAll("_MID_", m2.getMerchantId());
						m2.addMerchantNote(Long.parseLong(m2.getMerchantId()), sNote, sessionData
								.getProperty("username"));
						m2 = null;
						lc = null;
					}
                                        
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during updateMerchantType", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}
	}

	/**
	 * @param intPageNumber
	 * @param intRecordsPerPage
	 * @param sessionData
	 * @param context
	 * @param needsDBAName
	 * @param scheduleReportType
	 * @param headers
	 * @param titles
	 * @return
	 * @throws CustomerException
	 */
	public Vector getMerchantCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData,
			ServletContext context, boolean needsDBAName, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException
	{
		Connection dbConn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Vector vecMerchantCredits = new Vector();

		try
		{
			String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
			String reportId = sessionData.getProperty("reportId");

			int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);

			if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER))
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			}
			else
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));
			}

			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
                        if ( this.startDate != null && this.endDate != null )
                        {
                            Date date1 = format.parse(this.startDate);
                            Date date2 = format.parse(this.endDate);
                            format.applyPattern("yyyy-MM-dd");
                            this.startDate = format.format(date1);
                            this.endDate = format.format(date2);
                        }
                        else
                        {                                                        
                            this.endDate = format.format(new Date());
                            this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);                                                        
                            
                            Date dt1 = format.parse(this.startDate);
                            Date dt2 = format.parse(this.endDate);
                                               
                            format.applyPattern("yyyy-MM-dd");
                            this.startDate = format.format(dt1);
                            this.endDate = format.format(dt2);
                        }			

			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData
					.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate
					+ " 23:59:59.999", false);

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String instanceName = DebisysConfigListener.getInstance(context);
			String strSQL = Merchant.sql_bundle.getString("getPaymentsHistoryByMerchantId");
			pstmt = dbConn.prepareCall(strSQL);
			Merchant.cat.debug("[getMerchantCredits] Query: " + strSQL);
                        pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));	
                        Merchant.cat.debug("[getMerchantCredits] Parameters 1: " + this.getMerchantId());
                        
			if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate))
			{
                            pstmt.setString(2, vTimeZoneFilterDates.get(0));
                            pstmt.setString(3, vTimeZoneFilterDates.get(1));                            
                            Merchant.cat.debug("[getMerchantCredits] Parameters 2: " + vTimeZoneFilterDates.get(0));
                            Merchant.cat.debug("[getMerchantCredits] Parameters 3: " + vTimeZoneFilterDates.get(1));
			}
			else
			{
                            pstmt.setNull(2, Types.DATE);
                            pstmt.setNull(3, Types.DATE);
                            Merchant.cat.debug("[getMerchantCredits] Parameters 2: " + null);
                            Merchant.cat.debug("[getMerchantCredits] Parameters 3: " + null);
			}
			
			if ( scheduleReportType == DebisysConstants.EXECUTE_REPORT )
			{
				pstmt.setBoolean(4, true);
				pstmt.setNull(5, Types.INTEGER);
				pstmt.setNull(6, Types.INTEGER);
				pstmt.setString(7, instanceName);
	
                                Merchant.cat.debug("[getMerchantCredits] Parameters 4: " + true);
                                Merchant.cat.debug("[getMerchantCredits] Parameters 5: " + null);
                                Merchant.cat.debug("[getMerchantCredits] Parameters 6: " + null);
                                Merchant.cat.debug("[getMerchantCredits] Parameters 7: " + instanceName);
                                
				rs = pstmt.executeQuery();
				int intRecordCount = 0;
				if (rs.next())
				{
                                    intRecordCount = rs.getInt(1);
				}
				// first row is always the count
				vecMerchantCredits.add(new Integer(intRecordCount));
                                Merchant.cat.debug("[getMerchantCredits] intRecordCount : " + intRecordCount);
				if (intRecordCount > 0)
				{
					pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
                                        Merchant.cat.debug("[getMerchantCredits] Parameters 1: " + this.getMerchantId());
					if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate))
					{
                                            pstmt.setString(2, vTimeZoneFilterDates.get(0));
                                            pstmt.setString(3, vTimeZoneFilterDates.get(1));
                                            Merchant.cat.debug("[getMerchantCredits] Parameters 2: " + vTimeZoneFilterDates.get(0));
                                            Merchant.cat.debug("[getMerchantCredits] Parameters 3: " + vTimeZoneFilterDates.get(1));
					}
					else
					{
                                            pstmt.setNull(2, Types.DATE);
                                            pstmt.setNull(3, Types.DATE);
                                            Merchant.cat.debug("[getMerchantCredits] Parameters 2: " + null);
                                            Merchant.cat.debug("[getMerchantCredits] Parameters 3: " + null);
					}
					pstmt.setBoolean(4, false);
					pstmt.setInt(5, intPageNumber);
					pstmt.setInt(6, intRecordsPerPage);
					pstmt.setString(7, instanceName);
                                        
                                        Merchant.cat.debug("[getMerchantCredits] Parameters 4: " + false);
                                        Merchant.cat.debug("[getMerchantCredits] Parameters 5: " + intPageNumber);
                                        Merchant.cat.debug("[getMerchantCredits] Parameters 6: " + intRecordsPerPage);
                                        Merchant.cat.debug("[getMerchantCredits] Parameters 7: " + instanceName);
                                        
					rs = pstmt.executeQuery();
					Merchant.cat.debug("[getMerchantCredits] Send again Query: " + strSQL);
                                        rs.next();
					Double previousBalance = -1D;
					Double balance;
					String exDBA;
					boolean externalPayment;
					for (int i = 0; i < intRecordsPerPage; i++)
					{
						Vector<String> vTimeZoneDate = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), rs.getString("log_datetime"), rs.getString("log_datetime"),true);
						Vector vecTemp = new Vector();
						vecTemp.add(0, vTimeZoneDate.get(2));
						String logon_id = rs.getString("logon_id");
						vecTemp.add(1, StringUtil.toString(logon_id));
						vecTemp.add(2, StringUtil.toString(rs.getString("description")));
	
						if (logon_id.equals("Events Job"))
							vecTemp.add("");
						else
							vecTemp.add(3, NumberUtil.formatCurrency(rs.getString("payment")));
	
						balance = rs.getDouble("balance");
						if ((balance != null && !balance.equals(previousBalance))
								|| (rs.getString("logon_id") != null && !rs.getString("logon_id").equals("Commission") && !rs
										.getString("logon_id").equals("Billing")))
						{
							vecTemp.add(4, NumberUtil.formatCurrency(rs.getString("balance")));
						}
						else
						{
							vecTemp.add("");
						}
	
						if (rs.getString("commission") == null)
						{
							vecTemp.add(5, "0.00");
						}
						else
						{
							vecTemp.add(5, rs.getString("commission"));
						}
	
						if (logon_id.equals("Events Job"))
							vecTemp.add("");
						else
							vecTemp.add(6, NumberUtil.formatCurrency(rs.getString("net_payment")));
	
						vecTemp.add(7, StringUtil.toString(rs.getString("dba")));
	
						// DBSY-1139 ExternalDBA
						exDBA = rs.getString("ExternalDBA");
	
						if (exDBA == null)
						{
							externalPayment = false;
							vecTemp.add(8, "Assigned");
						}
						else
						{
							externalPayment = true;
							if (needsDBAName)
							{
								vecTemp.add(8, exDBA);
							}
							else
							{
								vecTemp.add(8, "External");
							}
	
						}
						if (needsDBAName && externalPayment == false)
						{
							// We don't want to add it to the returned table
						}
						else
						{
							vecMerchantCredits.add(vecTemp);
	
							previousBalance = balance;
						}
						if (!rs.next())
						{
							break;
						}
					}
				}
			}
			else if ( scheduleReportType == DebisysConstants.DOWNLOAD_REPORT )
			{
				pstmt.setBoolean(4, false);
				pstmt.setInt(5, -1);
				pstmt.setNull(6, Types.INTEGER);
				pstmt.setString(7,instanceName);
				rs = pstmt.executeQuery();
				
				DownloadsSummary trxSummary = new DownloadsSummary();
				TransactionReport tr = new TransactionReport(); 
				String strFileName = tr.generateFileName(context);				
				this.setStrUrlLocation(trxSummary.downloadMerchantCredits( rs, sessionData, context, headers, strFileName, titles, 2, needsDBAName ));
				rs.close();
				pstmt.close();
			}
			else if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
			{
				StringBuilder execStore = new StringBuilder();
				execStore.append("DECLARE @startDateRelative DATETIME ");
				execStore.append("DECLARE @endDateRelative DATETIME ");
				execStore.append("SELECT @startDateRelative ="+ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE+" , @endDateRelative = "+ ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE+" ");
				
				String fixedQuery = strSQL.replaceFirst("\\?", this.getMerchantId())
									   .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(0) + "'")
									   .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(1) + "'")
									   .replaceFirst("\\?", "0" )
									   .replaceFirst("\\?", "-1" )
									   .replaceFirst("\\?", "null" )
									   .replaceFirst("\\?", "'"+instanceName+"'" );	
				
				
				String relativeQuery =  strSQL.replaceFirst("\\?", this.getMerchantId())
									   .replaceFirst("\\?", "@startDateRelative" )
									   .replaceFirst("\\?", "@endDateRelative" )
									   .replaceFirst("\\?", "0" )
									   .replaceFirst("\\?", "-1" )
									   .replaceFirst("\\?", "null" )
									   .replaceFirst("\\?", instanceName );	
				execStore.append(relativeQuery);
												
				ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_MER_CRED_HISTORY , 1);
															
				scheduleReport.setRelativeQuery( execStore.toString() );
				
				scheduleReport.setFixedQuery( fixedQuery );				
				TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, titles );				
				sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);								
				return null;
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantCredits", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantCredits;
	}

	public Vector getUserMerchantCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData,
			ServletContext context, boolean needsDBAName) throws CustomerException
	{
		Connection dbConn = null;
		Vector vecMerchantCredits = new Vector();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
			String reportId = sessionData.getProperty("reportId");
			int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);

			if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER))
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			}
			else
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));
			}

			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			int daysBetween = DateUtil.getDaysBetween(this.endDate.toString(), this.startDate.toString());
			if (daysBetween > limitDays)
			{
				this.startDate = DateUtil.addSubtractDays(this.endDate.toString(), -limitDays);
			}
			Date date1 = format.parse(this.startDate);
			Date date2 = format.parse(this.endDate);
			format.applyPattern("yyyy-MM-dd");
			this.startDate = format.format(date1);
			this.endDate = format.format(date2);
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData
					.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate
					+ " 23:59:59.999", false);

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getUserMerchantCredits");

			if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSQL += " INNER JOIN dbo.reps AS R WITH(NOLOCK) ON M.rep_id = R.rep_id WHERE R.rep_id = " + strRefId;
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)
					|| (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType
							.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
			{
				strSQL += " INNER JOIN dbo.reps R WITH(NOLOCK) ON M.rep_id = R.rep_id WHERE R.rep_id IN (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id = "
						+ strRefId + ")";
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL += "INNER JOIN dbo.reps R WITH(NOLOCK) ON M.rep_id = R.rep_id WHERE R.rep_id IN (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id IN"
						+ " (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id = " + strRefId + "))";
			}
			else if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL += " INNER JOIN dbo.reps R WITH (NOLOCK) ON M.rep_id=R.rep_id"
							+ " INNER JOIN dbo.reps I WITH (NOLOCK) ON I.rep_id=R.iso_id AND I.type="
							+ DebisysConstants.REP_TYPE_ISO_3_LEVEL + " AND I.rep_id=I.iso_id AND I.rep_id=" + strRefId;
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL += " INNER JOIN dbo.reps R WITH (NOLOCK) ON M.rep_id=R.rep_id"
							+ " INNER JOIN dbo.reps S WITH (NOLOCK) ON S.rep_id=R.iso_id AND S.type="
							+ DebisysConstants.REP_TYPE_SUBAGENT
							+ " INNER JOIN dbo.reps A WITH (NOLOCK) ON A.rep_id=S.iso_id AND A.type="
							+ DebisysConstants.REP_TYPE_AGENT
							+ " INNER JOIN dbo.reps I WITH (NOLOCK) ON I.rep_id=A.iso_id AND I.type="
							+ DebisysConstants.REP_TYPE_ISO_5_LEVEL + " AND I.rep_id=I.iso_id AND I.rep_id=" + strRefId;
				}
			}

			strSQL += " AND mc.Logon_id = ?";

			if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate))
			{
				strSQL += " AND MC.log_datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND MC.log_datetime <= '"
						+ vTimeZoneFilterDates.get(1) + "'";
			}

			strSQL += " order by mc.id DESC";
			strSQL = strSQL.replace("DATE", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId
					+ ", mc.log_datetime, 1) as log_datetime");
			pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

			pstmt.setString(1, this.getMerchantId());

			Merchant.cat.debug(strSQL);

			rs = pstmt.executeQuery();
			int intRecordCount = DbUtil.getRecordCount(rs);
			// first row is always the count
			vecMerchantCredits.add(new Integer(intRecordCount));

			if (intRecordCount > 0)
			{
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));

				for (int i = 0; i < intRecordsPerPage; i++)
				{
					Vector vecTemp = new Vector();
					vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("log_datetime")));
					vecTemp.add(StringUtil.toString(rs.getString("logon_id")));
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("payment")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("balance")));
					if (rs.getString("commission") == null)
					{
						vecTemp.add(NumberUtil.formatAmount("0.00"));
					}
					else
					{
						vecTemp.add(NumberUtil.formatAmount(rs.getString("commission")));
					}
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("net_payment")));
					vecTemp.add(StringUtil.toString(rs.getString("dba")));
					if (rs.getString("ExternalDBA") == null)
					{
						vecTemp.add("Assigned");
					}
					else
					{
						vecTemp.add("External");
					}
					vecMerchantCredits.add(vecTemp);

					if (!rs.next())
					{
						break;
					}
				}
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantCredits", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantCredits;
	}
	
    public String getUserMerchantCreditDownload(SessionData sessionData, ServletContext context, List<ColumnReport> headers, List<String> titles, boolean needsDBAName) throws CustomerException {
		MerchantReportDAO merchantDAO = new MerchantReportDAO();
		MerchantCreditDownloadDTO merCreditDTO = new MerchantCreditDownloadDTO();
		merCreditDTO.setStartDate(startDate);
		merCreditDTO.setEndDate(endDate);
		merCreditDTO.setSessionData(sessionData);
		merCreditDTO.setContext(context);
		merCreditDTO.setMerchantId(getMerchantId());
		merCreditDTO.setHeaders(headers);
		merCreditDTO.setTitles(titles);
		merCreditDTO.setNeedsDBAName(needsDBAName);
		return merchantDAO.getUserMerchantCreditDownload(merCreditDTO);
    }


	public Vector getMerchantNotes(SessionData sessionData) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector vecMerchantNotes = new Vector();

		// CallableStatement cstmt;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchantNotes");

			String now = (new SimpleDateFormat("MM/dd/yyyy")).format(new java.util.Date());
			if (this.startDate == null)
			{
				this.startDate = now;
			}
			if (this.endDate == null)
			{
				this.endDate = now;
			}
			String s_startDate = DateUtil.formatDateConcatTime_DB(this.startDate, "00:00:00.000");
			String s_endDate = DateUtil.formatDateConcatTime_DB(this.endDate, "23:59:59.999");

			// Calendar cal = new GregorianCalendar();
			// String currentDate = DateUtil.formatDate(cal.getTime());
			// String lastMonth = DateUtil.addSubtractMonths(currentDate, -1);
			Merchant.cat.debug("Start Date:" + s_startDate);
			Merchant.cat.debug("End Date:" + s_endDate);
			Merchant.cat.debug(strSQL);

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, s_startDate);
			pstmt.setString(2, s_endDate);
			pstmt.setDouble(3, Double.parseDouble(this.merchantId));
			pstmt.setString(4, s_startDate);
			pstmt.setString(5, s_endDate);
			pstmt.setDouble(6, Double.parseDouble(this.merchantId));

			rs = pstmt.executeQuery();

			// select website_log_date as datetime, logon_id as username,
			// message from website_log(nolock) where website_log_date >= ? and
			// applied_ref_id =? UNION
			// select datetime, 'SYSTEM' as username , note as message from
			// notes_merchants(nolock) where datetime >= ? and initials='AUT'
			// and merchant_id=? order by
			// datetime DESC
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetimeNote")));
				vecTemp.add(StringUtil.toString(rs.getString("username")));

				String messageNote = "";
				if (rs.getString("message") != null)
				{
					messageNote = rs.getString("message").replaceFirst(
							"customer note",
							Languages.getString("jsp.admin.customers.add_merchant_notes.customerNote", sessionData
									.getLanguage()));
				}
				vecTemp.add(StringUtil.toString(messageNote));
				vecMerchantNotes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantNotes.", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantNotes;
	}

	public static Vector getTerminals(String strMerchantId)
	{
		Vector vecTerminals = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getTerminals"));
			pstmt.setDouble(1, Double.parseDouble(strMerchantId));

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
				vecTemp.add(StringUtil.toString(rs.getString("description")));
				vecTemp.add(StringUtil.toString(rs.getString("Terminal_Type")));
				vecTerminals.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminals", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return vecTerminals;
	}

	public static Vector getTerminalsCountEnabled(String strMerchantId)
	{
		Vector vecTerminals = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getTerminalsCountEnabled"));
			pstmt.setDouble(1, Double.parseDouble(strMerchantId));

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
				vecTemp.add(StringUtil.toString(rs.getString("description")));
				vecTemp.add(StringUtil.toString(rs.getString("Terminal_Type")));
				vecTemp.add(StringUtil.toString(rs.getString("EnabledTerminals")));
				vecTerminals.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalsCountEnabled", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return vecTerminals;
	}

	/*
	 * non-javadoc rbuitrago: this invokes a different query to the ney
	 * terminal_types table to include terminal_of field. terminal_types table
	 * to include terminal_of field.
	 */
	public static Vector<Vector<String>> getTerminalsCountEnabledWithTerminalOf(String strMerchantId)
	{
		Vector<Vector<String>> vecTerminals = new Vector<Vector<String>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getTerminalsCountEnabledWithTerminalOf"));
			pstmt.setDouble(1, Double.parseDouble(strMerchantId));
			Merchant.cat.info("********************************************************************************");
			Merchant.cat.info("****JAVA**** Get terminals list for merchant [" + strMerchantId + "] ****************");
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(0, StringUtil.toString(rs.getString("millennium_no")));
				vecTemp.add(1, StringUtil.toString(rs.getString("description")));
				vecTemp.add(2, StringUtil.toString(rs.getString("Terminal_Type")));
				vecTemp.add(3, StringUtil.toString(rs.getString("EnabledTerminals")));
				vecTemp.add(4, StringUtil.toString(rs.getString("Terminal_of")));
				vecTemp.add(5, StringUtil.toString(rs.getString("Status")));
				vecTemp.add(6, StringUtil.toString(rs.getString("RatePlanID")));
				if (rs.getString("Terminal_Label") != null && !rs.getString("Terminal_Label").equals(""))
				{
					vecTemp.add(7, StringUtil.toString(rs.getString("Terminal_Label")));
				}
				else
				{
					vecTemp.add(7, StringUtil.toString("N/A"));
				}
				vecTemp.add(8, StringUtil.toString(rs.getString("EnablePINCache")));
				Merchant.cat.info("****JAVA**** [millennium_no = " + rs.getString("millennium_no") + "]  "
						+ "[description = " + rs.getString("description") + "]  " + "[Terminal_Type = "
						+ rs.getString("Terminal_Type") + "]  " + "[EnabledTerminals = "
						+ rs.getString("EnabledTerminals") + "]  " + "[Terminal_of = " + rs.getString("Terminal_of")
						+ "]  " + "[Status = " + rs.getString("Status") + "]  " + "[RatePlanID = "
						+ rs.getString("RatePlanID") + "]");

				vecTerminals.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalsCountEnabled", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return vecTerminals;
	}

	public static void printlnLogMsg(String text)
	{
		Merchant.cat.info("****JSP**** " + text);
	}
        
        public static String getHideClerk(String clerk) {
            clerk = clerk.replaceAll("(?s).", "*");
            return clerk;
        }

	public Vector<Vector<String>> getClerkCodes(ServletContext context, SessionData sessionData)
	{
		Vector<Vector<String>> vecClerkCodes = new Vector<Vector<String>>();

		if (this.checkPermission(sessionData))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
                                String seeder = (String) context.getAttribute("secretSeed");
                                net.emida.supportsite.util.security.SecurityCipherService requestBase = new SecurityCipherService(seeder);                                                                

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new Exception();
				}

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getClerkCodes"));
				pstmt.setDouble(1, Double.parseDouble(this.siteId));

				rs = pstmt.executeQuery();

				while (rs.next())
				{
                                    Vector<String> vecTemp = new Vector<String>();                                              
                                    vecTemp.add(StringUtil.toString(rs.getString("password")));
                                    vecTemp.add(StringUtil.toString(rs.getString("name")));
                                    vecTemp.add(Integer.toString(rs.getInt("IsAdmin")));
                                    vecClerkCodes.add(vecTemp);
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during getClerkCodes", e);
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}
		else
		{
			this.addFieldError("error", "Permission Denied.");
		}

		return vecClerkCodes;
	}

	public String generatePartiallyStaticId(Vector<String> v)
	{
		boolean isUnique = false;
		String strEntityNumber = "";
		String partialcode = v.get(1);
		while (!isUnique)
		{
			strEntityNumber = this.generateSRandomNumber(partialcode);
			isUnique = this.checkEntityNumber(strEntityNumber);
		}
		return strEntityNumber;
	}

	private String generateSRandomNumber(String str)
	{
		StringBuffer s = new StringBuffer();
		int nextInt = 0;

		if (str.length() > 7)
		{
			str = str.substring(0, 5);
		}
		else if (str.length() == 0)
		{
			int nextdInt = (int) ((Math.random() * 9) + 1);
			s = s.append(nextdInt);
			str = "1";// adjusting size to 1
		}
		else
		{
			s = s.append(str);
		}

		for (int i = 1; i <= 12 - str.length(); i++)
		{
			// number between 0-9
			nextInt = (int) (Math.random() * 10);
			s = s.append(nextInt);
		}

		return s.toString();
	}

	public String generateMerchantId()
	{
		boolean isUnique = false;
		String strEntityNumber = "";

		while (!isUnique)
		{
			strEntityNumber = this.generateRandomNumber();
			isUnique = this.checkEntityNumber(strEntityNumber);
		}

		return strEntityNumber;
	}

	private String generateRandomNumber()
	{
		StringBuffer s = new StringBuffer();

		// number between 1-9 because first digit must not be 0
		int nextInt = (int) ((Math.random() * 9) + 1);
		s = s.append(nextInt);

		for (int i = 0; i <= 10; i++)
		{
			// number between 0-9
			nextInt = (int) (Math.random() * 10);
			s = s.append(nextInt);
		}

		return s.toString();
	}

	private boolean checkEntityNumber(String strEntityNumber)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean isUnique = true;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkMerchantId"));
			pstmt.setString(1, strEntityNumber);

			rs = pstmt.executeQuery();

			if (rs.next())
			{
				// duplicate key found
				Merchant.cat.error("Duplicate found:" + strEntityNumber);
				isUnique = false;
			}

			TorqueHelper.closeStatement(pstmt, rs);

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkRepId"));
			pstmt.setString(1, strEntityNumber);
			rs = pstmt.executeQuery();

			if (rs.next())
			{
				// duplicate key found
				isUnique = false;
			}

			if (!isUnique)
			{
				Merchant.cat.error("Duplicate found:" + strEntityNumber);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during checkEntityNumber", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return isUnique;
	}

	private Date loadCancelledDate(String strMerchantId)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Date d = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}
			pstmt = dbConn.prepareStatement("SELECT DateCancelled FROM merchants WITH (NOLOCK) WHERE merchant_id = ?");
			pstmt.setDouble(1, Double.parseDouble(strMerchantId.replace("disabled", "")));
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				d = rs.getDate("DateCancelled");
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during loadCancelledDate", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
		return d;
	}

	private boolean loadCancelledBit(String strMerchantId)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Boolean b = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}
			pstmt = dbConn.prepareStatement("SELECT Cancelled FROM merchants WITH (NOLOCK) WHERE merchant_id = ?");
			pstmt.setDouble(1, Double.parseDouble(strMerchantId));
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				b = rs.getBoolean("Cancelled");
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during loadCancelledBit", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
		return b;
	}

	public void updateMerchantStatus(SessionData sessionData, ServletContext context, String strMerchantId,
			boolean blnDisabled, String reason)
	{
		Date d = this.loadCancelledDate(strMerchantId);
		String olddate = (d != null ? new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date()) : "");

		Connection dbConn = null;
		PreparedStatement pstmt = null;
		Merchant.cat.info("updateMerchantStatus " + blnDisabled + " for " + strMerchantId);
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}
			LogChanges logChanges = new LogChanges(sessionData);
			logChanges.setContext(context);
			if (blnDisabled)
			{// disabled
                            
                                insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_MERCHANT, strMerchantId,DebisysConstants.AUDIT_MERCHANT_DISABLED,"0","1");
                            
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateMerchantStatusOn"));
				pstmt.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));
				pstmt.setDouble(2, Double.parseDouble(strMerchantId));
				Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.merchant_disabled",
						sessionData.getLanguage()), DebisysConstants.LOGTYPE_CUSTOMER, strMerchantId,
						DebisysConstants.PW_REF_TYPE_MERCHANT);
				this.addMerchantNote(Long.parseLong(this.getMerchantId()), Languages.getString(
						"jsp.admin.customers.merchants.isdisabled", sessionData.getLanguage())
						+ ". " + reason, sessionData.getUser().getUsername());
				String newdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date());
				logChanges.log_changed(logChanges.getLogChangeIdByName("Merchant Date Cancelled"),
						this.getMerchantId(), newdate, olddate, "[Merchant Disabled] " + reason);
			}
			else
			{// enabled
                                insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_MERCHANT, strMerchantId,DebisysConstants.AUDIT_MERCHANT_DISABLED,"1","0");
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateMerchantStatusOff"));
				pstmt.setDouble(1, Double.parseDouble(strMerchantId));
				if (this.loadCancelledBit(strMerchantId))
				{
					Log.write(sessionData, "Merchant cancelled from website.", DebisysConstants.LOGTYPE_CUSTOMER,
							strMerchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
					this.addMerchantNote(Long.parseLong(this.getMerchantId()), Languages.getString(
							"jsp.admin.customers.merchants.iscancelled", sessionData.getLanguage())
							+ ". " + reason, sessionData.getUser().getUsername());
					String newdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date());
					olddate = (d != null ? new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date())
							: "");
					logChanges.log_changed(logChanges.getLogChangeIdByName("Merchant Cancelled"), this.getMerchantId(),
							"1", "0", "[Merchant Cancelled] " + reason);
					logChanges.log_changed(logChanges.getLogChangeIdByName("Merchant Date Cancelled"), this
							.getMerchantId(), newdate, olddate, "[Merchant Cancelled]" + reason);
				}
				else
				{
					Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.merchant_enabled",
							sessionData.getLanguage()), DebisysConstants.LOGTYPE_CUSTOMER, strMerchantId,
							DebisysConstants.PW_REF_TYPE_MERCHANT);
					this.addMerchantNote(Long.parseLong(this.getMerchantId()), Languages.getString(
							"jsp.admin.customers.merchants.isenabled", sessionData.getLanguage())
							+ ". " + reason, sessionData.getUser().getUsername());
					logChanges.log_changed(logChanges.getLogChangeIdByName("Merchant Date Cancelled"), this
							.getMerchantId(), "", olddate, "[Merchant Enabled] " + reason);
				}
			}
			pstmt.executeUpdate();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateMerchantStatus", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	/**
	 * This method validate id a merchant is saved in tha table
	 * DisabledMerchants
	 * 
	 * @param sessionData
	 * @param context
	 * @param strMerchantId
	 *            By Diego G.
	 */
	public int validateMerchantInDisabledMerchants(SessionData sessionData, ServletContext context, String strMerchantId)
	{

		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int count = -1;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}
			pstmt = dbConn.prepareStatement("SELECT COUNT(1) AS count FROM DisabledMerchants WHERE merchant_id = ? ");
			pstmt.setDouble(1, Double.parseDouble(strMerchantId.replace("disabled", "")));
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				count = rs.getInt("count");
			}
			return count;
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateMerchantStatus", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
		return count;
	}

	// New version to support Cancelled Merchant Status - Release 8
	// updateMerchantStatus was not changed to avoid problems from other parts
	// of Support Site project. DB Schema for Merchants should be updated in the
	// future to allow a more
	// elegant way to represent different types of Status (Example: Temporally
	// disabled (no credit), Manually disabled (by user), Cancelled (no more in
	// business), etc.)
	public void updateMerchantCancelledStatus(SessionData sessionData, ServletContext context, String strMerchantId,
			boolean blnCancelled, String reason)
	{
		Date d = this.loadCancelledDate(strMerchantId);
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		Merchant.cat.info("updateMerchantCancelledStatus " + blnCancelled + " for " + strMerchantId);
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}
			LogChanges logChanges = new LogChanges(sessionData);
			logChanges.setContext(context);
			if (blnCancelled)
			{// Cancelled
                                insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_MERCHANT, strMerchantId,DebisysConstants.AUDIT_MERCHANT_CANCELLED,"0","1");
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateMerchantCancelledStatusOn"));
				pstmt.setTimestamp(1, new java.sql.Timestamp(new java.util.Date().getTime()));
				pstmt.setBoolean(2, true);
				pstmt.setDouble(3, Double.parseDouble(strMerchantId));
				Log.write(sessionData, "Merchant cancelled from website.", DebisysConstants.LOGTYPE_CUSTOMER,
						strMerchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
				this.addMerchantNote(Long.parseLong(this.getMerchantId()), Languages.getString(
						"jsp.admin.customers.merchants.iscancelled", sessionData.getLanguage())
						+ ". " + reason, sessionData.getUser().getUsername());
				String newdate = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date());
				String olddate = (d != null ? new SimpleDateFormat("MM/dd/yyyy hh:mm:ss").format(new java.util.Date())
						: "");
				logChanges.log_changed(logChanges.getLogChangeIdByName("Merchant Cancelled"), this.getMerchantId(),
						"1", "0", "[Merchant Cancelled] " + reason);
				logChanges.log_changed(logChanges.getLogChangeIdByName("Merchant Date Cancelled"),
						this.getMerchantId(), newdate, olddate, "[Merchant Cancelled]" + reason);
			}
			else
			{// Restored
                                insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_MERCHANT, strMerchantId,DebisysConstants.AUDIT_MERCHANT_CANCELLED,"1","0");
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateMerchantCancelledStatusOff"));
				pstmt.setBoolean(1, false);
				pstmt.setDouble(2, Double.parseDouble(strMerchantId));
				Log.write(sessionData, "Merchant restored from website.", DebisysConstants.LOGTYPE_CUSTOMER,
						strMerchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
				this.addMerchantNote(Long.parseLong(this.getMerchantId()), Languages.getString(
						"jsp.admin.customers.merchants.isrestored", sessionData.getLanguage())
						+ ". " + reason, sessionData.getUser().getUsername());
				logChanges.log_changed(logChanges.getLogChangeIdByName("Merchant Cancelled"), this.getMerchantId(),
						"0", "1", "[Merchant Restored] " + reason);

			}
			pstmt.executeUpdate();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateMerchantCancelledStatus", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	public void deleteClerkCode(ServletContext context, SessionData sessionData, String strClerkCode)
	{
		Connection dbConn = null;
		LogChanges logChanges = new LogChanges(sessionData);
		logChanges.setContext(context);
		PreparedStatement pstmt = null;

		if (this.checkPermission(sessionData))
		{
			if (StringUtil.isAlphaNumeric(strClerkCode))
			{
				try
				{
					dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

					if (dbConn == null)
					{
						Merchant.cat.error("Can't get database connection");
						throw new Exception();
					}
					String strClerkName = this.getClerkNameForCode(strClerkCode, this.siteId);

					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("deleteClerkCode"));
					pstmt.setString(1, strClerkCode);
					pstmt.setDouble(2, Double.parseDouble(this.siteId));
					pstmt.executeUpdate();

					TorqueHelper.closeStatement(pstmt, null);

					String reason = "Removed clerk code: " + strClerkCode + ", clerk name: " + strClerkName
							+ ", SiteID = " + this.siteId;
					logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE),
							strClerkCode, this.siteId, reason);

					reason = "Removed clerk name: " + strClerkName + ", clerk code: " + strClerkCode + ", SiteID = "
							+ this.siteId;
					logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKNAME),
							strClerkName, this.siteId, reason);
					this.addMerchantNote(Long.parseLong(this.merchantId), reason, sessionData.getProperty("username"));

					Vector vecClerkCodes = this.getClerkCodes(context, sessionData);

					if ((vecClerkCodes == null) || (vecClerkCodes.size() == 0))
					{
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateClerkCode"));
						pstmt.setInt(1, 0);
						pstmt.setDouble(2, Double.parseDouble(this.siteId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);
					}

					Log.write(sessionData, "Site ID: "
							+ this.siteId
							+ "."
							+ Languages.getString("com.debisys.customers.Merchant.Log.clerk_code", sessionData
									.getLanguage())
							+ " \""
							+ strClerkCode
							+ "\" "
							+ Languages.getString("com.debisys.customers.Merchant.Log.deleted", sessionData
									.getLanguage()) + ".", DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
							DebisysConstants.PW_REF_TYPE_MERCHANT);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during deleteClerkCode", e);
				}
				finally
				{
					try
					{
						DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
					}
					catch (Exception e)
					{
						Merchant.cat.error("Error during release connection", e);
					}
				}
			}
		}
		else
		{
			this.addFieldError("error", "Permission Denied.");
		}
	}
                        
	public void addClerkCode(ServletContext context, SessionData sessionData, String strClerkCode, String strClerkName,
			boolean bIsAdmin)
	{
            String seeder = (String) context.getAttribute("secretSeed");
            net.emida.supportsite.util.security.SecurityCipherService requestBase = new SecurityCipherService(seeder);
            Connection dbConn = null;
            LogChanges logChanges = new LogChanges(sessionData);
            logChanges.setContext(context);
            PreparedStatement pstmt = null;
            ResultSet rs = null;

            if (strClerkCode.length() > 10)
            {
                strClerkCode = strClerkCode.substring(0, 10);
            }

            if (strClerkName.length() > 50)
            {
                strClerkName = strClerkName.substring(0, 50);
            }

            if (this.checkPermission(sessionData))
            {
                if (StringUtil.isAlphaNumeric(strClerkCode))
                {
                    try
                    {
                        dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

                        if (dbConn == null)
                        {
                            Merchant.cat.error("Can't get database connection");
                            throw new Exception();
                        }                                                                        

                        pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkClerkCode"));
                        pstmt.setDouble(1, Double.parseDouble(this.siteId));
                        pstmt.setString(2, strClerkCode);
                        rs = pstmt.executeQuery();
                        boolean returned = rs.next();
                        TorqueHelper.closeStatement(pstmt, rs);
                        if (returned)
                        {
                            this.addFieldError("errorAdd", Languages.getString("jsp.admin.customers.merchants_info.error_1", sessionData.getLanguage()));
                        }
                        else
                        {
                            pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertClerkCode"));
                            pstmt.setDouble(1, Double.parseDouble(this.siteId));
                            pstmt.setString(2, strClerkCode);
                            pstmt.setString(3, strClerkName);
                            pstmt.setBoolean(4, bIsAdmin);
                            pstmt.setString(5, strClerkName);                            
                            pstmt.executeUpdate();

                            String reason = "Added a new clerk code: " + strClerkCode + ", clerk name: " + strClerkName + ", firstname: "+ strClerkName +"SiteID = " + this.siteId;
                            logChanges.set_value_for_check(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE), null);
                            logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE), strClerkCode,this.siteId, reason);

                            reason = "Added a new clerk name: " + strClerkName + ", clerk code: " + strClerkCode+ ", SiteID = " + this.siteId;
                            logChanges.set_value_for_check(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKNAME), null);
                            logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKNAME), strClerkName,this.siteId, reason);

                            this.addMerchantNote(Long.parseLong(this.merchantId), reason, sessionData.getProperty("username"));

                            TorqueHelper.closeStatement(pstmt, null);

                            Merchant.cat.debug("[addClerkCode] insertClerkCode with params: 1=" + this.siteId + ", 2="
                                            + strClerkCode + " ,3=" + strClerkName + ", 4=" + bIsAdmin + " : "
                                            + Merchant.sql_bundle.getString("insertClerkCode"));

                            pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateClerkCode"));
                            pstmt.setInt(1, 1);
                            pstmt.setDouble(2, Double.parseDouble(this.siteId));
                            pstmt.executeUpdate();

                            TorqueHelper.closeStatement(pstmt, null);
                            Merchant.cat.debug("[addClerkCode] updateClerkCode with params: 1=" + 1 + ", 2=" + this.siteId
                                            + " : " + Merchant.sql_bundle.getString("updateClerkCode"));

                            Log.write(sessionData, "Site ID: "
                                    + this.siteId
                                    + "."
                                    + Languages.getString("com.debisys.customers.Merchant.Log.clerk_code", sessionData
                                                    .getLanguage())
                                    + "\""
                                    + strClerkCode
                                    + "\""
                                    + Languages.getString("com.debisys.customers.Merchant.Log.added", sessionData
                                                    .getLanguage()) + ".", DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
                                    DebisysConstants.PW_REF_TYPE_MERCHANT);
                        }
                    }
                    catch (Exception e)
                    {
                        Merchant.cat.error("Error during addClerkCode", e);
                    }
                    finally
                    {
                        try
                        {
                            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
                        }
                        catch (Exception e)
                        {
                            Merchant.cat.error("Error during release connection", e);
                        }
                    }
                }
            }
            else
            {
                this.addFieldError("error", "Permission Denied.");
            }
	}

	// Check to make sure you are a valid rep or iso to edit a merchant
	public boolean checkPermission(SessionData sessionData)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean isAllowed = false;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception("Can't get database connection");
			}

			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");
			boolean knownAccessLevel = true;

			if (strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkCarrier"));
				if (strRefId != null)
				{
					pstmt.setDouble(1, Double.parseDouble(strRefId));
				}
				else
				{
					pstmt.setNull(1, java.sql.Types.DOUBLE);
				}
				if (this.merchantId != null && !this.merchantId.equals(""))
				{
					pstmt.setDouble(2, Double.parseDouble(this.merchantId));
				}
				else
				{
					pstmt.setNull(2, java.sql.Types.DOUBLE);
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkIso3"));
					if (strRefId != null)
					{
						pstmt.setDouble(1, Double.parseDouble(strRefId));
					}
					else
					{
						pstmt.setNull(1, java.sql.Types.DOUBLE);
					}
					if (this.merchantId != null && !this.merchantId.equals(""))
					{
						pstmt.setDouble(2, Double.parseDouble(this.merchantId));
					}
					else
					{
						pstmt.setNull(2, java.sql.Types.DOUBLE);
					}
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkIso5"));
					pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
					pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
					if (strRefId != null)
					{
						pstmt.setDouble(4, Double.parseDouble(strRefId));
					}
					else
					{
						pstmt.setNull(4, java.sql.Types.DOUBLE);
					}
					if (this.merchantId != null && this.merchantId.length() > 0)
					{
						pstmt.setDouble(5, Double.parseDouble(this.merchantId));
					}
					else
					{
						pstmt.setNull(5, java.sql.Types.DOUBLE);
					}
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkAgent"));
				pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
				pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
				if (strRefId != null)
				{
					pstmt.setDouble(3, Double.parseDouble(strRefId));
				}
				else
				{
					pstmt.setNull(3, java.sql.Types.DOUBLE);
				}
				if (this.merchantId != null && this.merchantId.length() > 0)
				{
					pstmt.setDouble(4, Double.parseDouble(this.merchantId));
				}
				else
				{
					pstmt.setNull(4, java.sql.Types.DOUBLE);
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkSubAgent"));
				pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
				if (strRefId != null)
				{
					pstmt.setDouble(2, Double.parseDouble(strRefId));
				}
				else
				{
					pstmt.setNull(2, java.sql.Types.DOUBLE);
				}
				if (this.merchantId != null && this.merchantId.length() > 0)
				{
					pstmt.setDouble(3, Double.parseDouble(this.merchantId));
				}
				else
				{
					pstmt.setNull(3, java.sql.Types.DOUBLE);
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkRep"));
				if (strRefId != null)
				{
					pstmt.setDouble(1, Double.parseDouble(strRefId));
				}
				else
				{
					pstmt.setNull(1, java.sql.Types.DOUBLE);
				}
				if (this.merchantId != null && this.merchantId.length() > 0)
				{
					pstmt.setDouble(2, Double.parseDouble(this.merchantId));
				}
				else
				{
					pstmt.setNull(2, java.sql.Types.DOUBLE);
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkMerchant"));
				if (strRefId != null)
				{
					pstmt.setDouble(1, Double.parseDouble(strRefId));
				}
				else
				{
					pstmt.setNull(1, java.sql.Types.DOUBLE);
				}

				if (strRefId == null || this.merchantId == null || !strRefId.equals(this.merchantId))
				{
					isAllowed = false;
				}

			}
			else
			{
				isAllowed = false;
				knownAccessLevel = false;
			}

			if (knownAccessLevel)
			{
				rs = pstmt.executeQuery();
				if (rs.next())
				{
					isAllowed = true;
				}
				else
				{
					isAllowed = false;
				}
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during checkPermission", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return isAllowed;
	}

	// Check to make sure you are a valid rep for an iso or agent
	// Used for addMerchant to make sure you selected a valid rep in your
	// downline
	private boolean checkRepPermission(SessionData sessionData)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean isAllowed = false;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			String strSQL = Merchant.sql_bundle.getString("checkIsoAgentRep");

			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");

			// iso
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL = strSQL + " r.iso_id = " + strRefId + " and r.rep_id=" + this.repId;
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps WITH(NOLOCK) where type="
							+ DebisysConstants.REP_TYPE_REP + "and rep_id=" + this.repId + " and iso_id in "
							+ "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
							+ " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type="
							+ DebisysConstants.REP_TYPE_AGENT + " and iso_id= " + strRefId + ")))";
				}
			}
			// agent
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps WITH(NOLOCK) where type="
						+ DebisysConstants.REP_TYPE_REP + "and rep_id=" + this.repId + " and iso_id in "
						+ "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type="
						+ DebisysConstants.REP_TYPE_AGENT + " and rep_id= " + strRefId + ")))";
			}
			else
			{
				isAllowed = false;

				return isAllowed;
			}

			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			if (rs.next())
			{
				isAllowed = true;
			}
			else
			{
				isAllowed = false;
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during checkRepPermission", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return isAllowed;
	}

	/**
	 * Gets a list of merchants for a given entity
	 * 
	 * @param sessionData
	 * @param selectiontype
	 *            determines the type of entity
	 * @param
	 */
	public static String getMerchantList(SessionData sessionData, int selectiontype, String selected_ids)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String strTemp = "";
		ResultSet rs = null;
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchantList");
			String strSQLWhere = "";

			if (selectiontype == 6)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id in " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
			}
			else if (selectiontype == 4)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id in " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_AGENT + " and rep_id in (" + selected_ids + ")  ))) ";
			}
			else if (selectiontype == 5)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in " + "( " + selected_ids + " )) ";
			}
			else if (selectiontype == 9)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id = " + strRefId + ")) ";
			}
			else if (selectiontype == 7)
			{
				strSQLWhere = strSQLWhere + " and r.iso_id = " + strRefId;
			}
			else if (selectiontype == 1)
			{
				strSQLWhere = " and r.rep_id in (" + selected_ids + ")";
			}
			else if (selectiontype == 11)
			{
				strSQLWhere = " and r.rep_id = " + strRefId;
			}
			else if (selectiontype == 10)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
			}

			strSQL = strSQL + strSQLWhere;

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			Vector strValue = new Vector();
			while (rs.next())
			{
				strValue.add(rs.getString("merchant_id"));
			}
			if (strValue != null && !strValue.isEmpty())
			{
				boolean isFirst = true;
				for (int i = 0; i < strValue.size(); i++)
				{
					if (strValue.get(i) != null && !strValue.get(i).toString().equals("")
							&& !strValue.get(i).toString().equals("all"))
					{
						if (!isFirst)
						{
							strTemp = strTemp + "," + strValue.get(i).toString();
						}
						else
						{
							strTemp = strValue.get(i).toString();
							isFirst = false;
						}
					}
				}
			}

		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantList", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return strTemp;
	}

	public static String s2kgetMerchantList(String strRefId, int selectiontype, String selected_ids)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String strTemp = "";
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchantList");
			String strSQLWhere = "";

			if (selectiontype == 6)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id in " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
			}
			else if (selectiontype == 4)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id in " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_AGENT + " and rep_id in (" + selected_ids + ")  ))) ";
			}
			else if (selectiontype == 5)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in " + "( " + selected_ids + " )) ";
			}
			else if (selectiontype == 9)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id = " + strRefId + ")) ";
			}
			else if (selectiontype == 7)
			{
				strSQLWhere = strSQLWhere + " and r.iso_id = " + strRefId;
			}
			else if (selectiontype == 1)
			{
				strSQLWhere = " and r.rep_id in (" + selected_ids + ")";
			}
			else if (selectiontype == 11)
			{
				strSQLWhere = " and r.rep_id = " + strRefId;
			}
			else if (selectiontype == 10)
			{
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
			}

			strSQL = strSQL + strSQLWhere;

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			Vector strValue = new Vector();
			while (rs.next())
			{
				strValue.add(rs.getString("merchant_id"));
			}
			if (strValue != null && !strValue.isEmpty())
			{
				boolean isFirst = true;
				for (int i = 0; i < strValue.size(); i++)
				{
					if (strValue.get(i) != null && !strValue.get(i).toString().equals("")
							&& !strValue.get(i).toString().equals("all"))
					{
						if (!isFirst)
						{
							strTemp = strTemp + "," + strValue.get(i).toString();
						}
						else
						{
							strTemp = strValue.get(i).toString();
							isFirst = false;
						}
					}
				}
			}

		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantList", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return strTemp;
	}

	public static Vector<Vector<String>> getMerchantListReports(SessionData sessionData) throws CustomerException
	{
		Vector<Vector<String>> vResult = new Vector<Vector<String>>();
		try
		{
			// This method uses a recursive CTE to optimize the search of
			// children below any actor which as a start enhances the reading of
			// this code
			vResult = TransactionReport.getDescendantActors(Long.valueOf(sessionData.getProperty("ref_id")),
					DebisysConstants.MERCHANT);
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantListReports", e);
			throw new CustomerException();
		}

		return vResult;
	}

	public static Vector getMerchantsList(SessionData sessionData, String[] list) throws CustomerException
	{
		Vector vecMerchantList = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchantList");
			String strSQLWhere = "";
			// cat.debug("Access Level acquired: " + strAccessLevel);
			String hdr = "Access level matches ";
			if (list == null)
			{ // if no list is specified
				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
					{
						// cat.debug(hdr+"ISO DIST_CHAIN_3_LEVEL");
						strSQLWhere = strSQLWhere + " and r.iso_id = " + strRefId;
					}
					else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						// cat.debug(hdr+"ISO DIST_CHAIN_5_LEVEL");
						strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
								+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
								+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
								+ " and iso_id in " + "(select rep_id from reps (nolock) where type="
								+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					// cat.debug(hdr+ "AGENT");
					strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
							+ "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
							+ " and iso_id = " + strRefId + ")) ";
				}
				else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
				{
					// cat.debug(hdr+"SUBAGENT");
					strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps (nolock) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
				}
				else if (strAccessLevel.equals(DebisysConstants.REP))
				{
					// cat.debug(hdr+"REP");
					strSQLWhere = " and r.rep_id = " + strRefId;
				}
				else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
				{
					Merchant.cat.debug(hdr + "MERCHANT");
					strSQLWhere = " and m.merchant_id = " + strRefId;
				}
			}
			strSQL = strSQL + strSQLWhere + " order by m.dba";
			Merchant.cat.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("merchant_id"));
				vecTemp.add(rs.getString("dba"));
				vecMerchantList.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantList", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantList;
	}

	public static Vector getMerchantListSalesLimitReport(SessionData sessionData) throws CustomerException
	{
		Vector vecMerchantList = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchantListSalesLimitReport");
			String strSQLWhere = "";
			// cat.debug("Access Level acquired: " + strAccessLevel);
			String hdr = "Access level matches ";
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					// cat.debug(hdr+"ISO DIST_CHAIN_3_LEVEL");
					strSQLWhere = strSQLWhere + " and r.iso_id = " + strRefId;
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					// cat.debug(hdr+"ISO DIST_CHAIN_5_LEVEL");
					strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
							+ "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
							+ " and iso_id in " + "(select rep_id from reps with (nolock) where type="
							+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				// cat.debug(hdr+ "AGENT");
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id = " + strRefId + ")) ";
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				// cat.debug(hdr+"SUBAGENT");
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				// cat.debug(hdr+"REP");
				strSQLWhere = " and r.rep_id = " + strRefId;
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				Merchant.cat.debug(hdr + "MERCHANT");
				strSQLWhere = " and m.merchant_id = " + strRefId;
			}

			strSQL = strSQL + strSQLWhere + " order by m.dba";
			Merchant.cat.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("merchant_id"));
				vecTemp.add(rs.getString("dba"));
				vecMerchantList.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantListSalesLimitReport", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantList;
	}

	/**
	 * Gets parent rep business name for given rep_id.
	 * 
	 * @param strRepId
	 * @return A rep business name of type string.
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public String getMerchantRepName(String strRepId) throws CustomerException
	{
		String strRepName = "";

		if ((strRepId != null) && !strRepId.equals(""))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				// get a count of total matches

				String strSQL = Merchant.sql_bundle.getString("getMerchantRep");
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setDouble(1, Double.parseDouble(strRepId));

				rs = pstmt.executeQuery();

				if (rs.next())
				{
					strRepName = rs.getString("businessname");
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during getMerchantRepName", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during closeConnection", e);
				}
			}
		}

		return strRepName;
	}

	/**
	 * Gets a list of Business Types
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getBusinessTypes(String customConfigType) throws CustomerException
	{
		Vector vecBusinessTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getBusinessTypes");
			pstmt = dbConn.prepareStatement(strSQL);

			if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				pstmt.setString(1, "SPANISH");
			}
			else
			{
				pstmt.setString(1, "ENGLISH");
			}

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("sic_code"));
				vecTemp.add(rs.getString("description"));
				vecBusinessTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getBusinessTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecBusinessTypes;
	}

	/**
	 * Gets a list of Merchant Segments
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getMerchantSegments() throws CustomerException
	{
		Vector vecMerchantSegments = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchantSegments");
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("merchantsegmentid"));
				vecTemp.add(rs.getString("merchantsegmentcode"));
				vecMerchantSegments.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantSegments", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantSegments;
	}

	/**
	 * Get the state name by the given state id
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static String getStateName(String stateID)
	{
		String stateName = stateID;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getStateName");
			pstmt = dbConn.prepareStatement(strSQL);

			pstmt.setString(1, stateID);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				stateName = rs.getString("StateName");
			}
		}
		catch (Exception e)
		{
			stateName = stateID;
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return stateName;
	}

	/**
	 * Gets a list of States for the Mexico country
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector<Vector<String>> getStates(String customConfigType) throws CustomerException
	{
		Vector<Vector<String>> vecStates = new Vector<Vector<String>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getStates");
			pstmt = dbConn.prepareStatement(strSQL);

			if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				pstmt.setString(1, "MEXICO");
			}
			else
			{
				pstmt.setString(1, "USA");
			}

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("stateid"));
				vecTemp.add(rs.getString("statename"));
				vecTemp.add(rs.getString("statecode"));
				vecStates.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getStates", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecStates;
	}

	/**
	 * Gets a list of Account Types
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getAccountTypes() throws CustomerException
	{
		Vector vecAccountTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getAccountTypes");
			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("accounttypeid"));
				vecTemp.add(rs.getString("accounttypecode"));
				vecAccountTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getAccountTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecAccountTypes;
	}

	/**
	 * Gets a list of Routes
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getRoutes() throws CustomerException
	{
		Vector vecRoutes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getRoutes");
			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("RouteID"));
				vecTemp.add(rs.getString("RouteName"));
				vecRoutes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getRoutes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecRoutes;
	}

	/**
	 * Gets a list of Credit Types
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getCreditTypes(String customConfigType) throws CustomerException
	{
		Vector vecCreditTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = "";

			if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				strSQL = Merchant.sql_bundle.getString("getCreditTypesMx");
			}
			else
			{
				strSQL = Merchant.sql_bundle.getString("getCreditTypes");
			}

			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("credittypeid"));
				vecTemp.add(rs.getString("credittypecode"));
				vecCreditTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getCreditTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecCreditTypes;
	}

	// checks to make the rep is unlimited
	// if the merchant type changes to unlimited
	public boolean checkRepCreditTypeUnlimited(SessionData sessionData, ServletContext context)
	{
		boolean isAllowed = false;
		Rep rep = new Rep();

		try
		{
			rep.setRepId(this.repId);
			rep.getRep(sessionData, context);
		}
		catch (Exception e)
		{
		}

		if (rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
		{
			isAllowed = true;
		}

		return isAllowed;
	}

	// checks to make the rep is unlimited
	// if the merchant type changes to unlimited
	public boolean checkRepCreditLimit(SessionData sessionData, ServletContext context)
	{
		boolean isAllowed = false;
		Rep rep = new Rep();

		try
		{
			rep.setRepId(this.repId);
			rep.getRep(sessionData, context);
		}
		catch (Exception e)
		{
		}

		if (rep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
		{
			// if the rep is unlimited we dont care what the assigned is
			isAllowed = true;
		}
		else
		// check credit limit to make sure rep has enough
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
				pstmt.setDouble(1, Double.parseDouble(this.merchantId));

				rs = pstmt.executeQuery();
				rs.next();

				// double currentRunningLiability =
				// rs.getDouble("runningliability");
				double currentLiabilityLimit = rs.getDouble("liabilitylimit");

				// double currentAvailableCredit = currentLiabilityLimit -
				// currentRunningLiability;

				// limit we are trying to change to
				double merchantCreditIncrease = 0;

				if (!NumberUtil.isNumeric(this.getPaymentAmount()))// this is a
				// credit limit
				// update
				{
					if (this.creditLimit.equals(""))
					{
						this.creditLimit = "0";
					}
					merchantCreditIncrease = Double.parseDouble(this.creditLimit) - currentLiabilityLimit;
				}
				else
				// this is a payment
				{
					merchantCreditIncrease = Double.parseDouble(this.getPaymentAmount());
				}

				// rep limit
				double repAvailableCredit = Double.parseDouble(rep.getAvailableCredit());
				Merchant.cat.debug("Available Credit for Rep:" + repAvailableCredit);

				if (merchantCreditIncrease <= repAvailableCredit)
				{
					isAllowed = true;
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during checkRepCreditLimit", e);
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}

		return isAllowed;
	}

	/* BEGIN MiniRelease 1.0 - Added by YH */
	public void updateRunningLiabilityMx(SessionData sessionData, boolean bCommissionByValue, ServletContext context)
			throws CustomerException
	{
		if (!this.validateDate(this.depositDate))
		{
			this.addFieldError("error3", "Formato de fecha incorrecto. La fecha debe tener el formato \"MM/dd/yyyy\"");

			return;
		}

		if (NumberUtil.isNumeric(this.paymentAmount) && (this.merchantType != null)
				&& this.merchantType.equals(DebisysConstants.MERCHANT_TYPE_CREDIT))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));

				rs = pstmt.executeQuery();
				rs.next();

				double currentRunningLiability = rs.getDouble("runningliability");
				double currentLiabilityLimit = rs.getDouble("liabilitylimit");
				double currentAvailableCredit = currentLiabilityLimit - currentRunningLiability;
				double dblPaymentAmount = Double.parseDouble(this.paymentAmount);
				double newRunningLiability = Double.parseDouble(NumberUtil.formatAmount(Double
						.toString(currentRunningLiability - dblPaymentAmount)));

				TorqueHelper.closeStatement(pstmt, rs);

				if ((dblPaymentAmount <= currentRunningLiability)
						&& ((currentRunningLiability - dblPaymentAmount) <= currentLiabilityLimit))
				{
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateRunningLiability"));
					pstmt.setDouble(1, newRunningLiability);
					pstmt.setDouble(2, Double.parseDouble(this.merchantId));
					pstmt.executeUpdate();

					TorqueHelper.closeStatement(pstmt, null);

					if ((this.paymentDescription != null) && (this.paymentDescription.length() > 255))
					{
						this.paymentDescription = this.paymentDescription.substring(0, 255);
					}

					if (!NumberUtil.isNumeric(this.commission))
					{
						this.commission = "0";
					}

					double dblCommission = Double.parseDouble(this.commission);
					// merchant_credits (merchant_id, logon_id, description,
					// payment, balance, prior_terminal_sales,
					// prior_available_credit, commission, log_datetime)
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCreditMx"));
					pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
					pstmt.setString(2, sessionData.getProperty("username"));
					pstmt.setString(3, this.paymentDescription);
					pstmt.setDouble(4, Double.parseDouble(this.paymentAmount));
					pstmt.setDouble(5, currentLiabilityLimit - newRunningLiability);
					pstmt.setDouble(6, currentRunningLiability);
					pstmt.setDouble(7, currentAvailableCredit);
					pstmt.setDouble(8, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
					pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
					pstmt.setString(10, this.depositDate);
					pstmt.setString(11, this.bank);
					pstmt.setString(12, this.refNumber);

					if (bCommissionByValue)
					{
						pstmt.setString(13, "VALUE-Pago via Support Site");
					}
					else
					{
						pstmt.setString(13, "PERCE-Pago via Support Site");
					}
					// R27-DBSY-586. added by jacuna
					pstmt.setInt(14, this.merchantCreditPaymentType);
					pstmt.setInt(15, this.bank_id);
					// END R27-DBSY-586.

					pstmt.executeUpdate();

					// DTU-369 Payment Notifications
					if (Rep.GetPaymentNotification(this.getMerchantId()))
					{
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
						pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
						pstmt.setString(2, sessionData.getProperty("username"));
						pstmt.setString(3, this.paymentDescription);
						pstmt.setDouble(4, Double.parseDouble(this.paymentAmount));
						pstmt.setDouble(5, currentLiabilityLimit - newRunningLiability);
						pstmt.setDouble(6, currentRunningLiability);
						pstmt.setDouble(7, currentAvailableCredit);
						pstmt.setDouble(8, 0);
						pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
						pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
						pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
						pstmt.setString(12, this.businessName);
						pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
						pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
						pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
						pstmt.executeUpdate();
						pstmt.close();
					}

					TorqueHelper.closeStatement(pstmt, null);

					Log.write(sessionData, Languages.getString(
							"com.debisys.customers.Merchant.Log.running_liability_changed", sessionData.getLanguage())
							+ NumberUtil.formatCurrency(Double.toString(currentRunningLiability))
							+ " "
							+ Languages.getString("jsp.admin.to", sessionData.getLanguage())
							+ " "
							+ NumberUtil.formatCurrency(Double.toString(newRunningLiability)),
							DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
				}
				else
				{
					if (dblPaymentAmount > currentRunningLiability)
					{
						this.addFieldError("error1", "Payment must be less than terminal sales.");
					}
					else if ((currentRunningLiability - dblPaymentAmount) > currentLiabilityLimit)
					{
						this.addFieldError("error2", "Payment is greater than credit limit.");
					}
				}
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during updateRunningLiability", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}
	}

	public void updateLiabilityLimitMx(SessionData sessionData, ServletContext context, boolean bCommissionByValue)
			throws CustomerException
	{
		if (!this.validateDate(this.depositDate))
		{
			this.addFieldError("error3", "Formato de fecha incorrecto. La fecha debe tener el formato \"MM/dd/yyyy\"");

			return;
		}

		if (NumberUtil.isNumeric(this.getPaymentAmount()))
		{
			Connection dbConn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new CustomerException();
				}

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getLiabilities"));
				pstmt.setDouble(1, Double.parseDouble(this.merchantId));

				rs = pstmt.executeQuery();
				rs.next();

				double currentRunningLiability = rs.getDouble("runningliability");
				double currentLiabilityLimit = rs.getDouble("liabilitylimit");
				double currentAvailableCredit = currentLiabilityLimit - currentRunningLiability;

				TorqueHelper.closeStatement(pstmt, rs);

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateLiabilityLimit"));
				pstmt.setDouble(1, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
				pstmt.setDouble(2, Double.parseDouble(this.getMerchantId()));
				pstmt.executeUpdate();

				TorqueHelper.closeStatement(pstmt, null);

				if ((this.paymentDescription != null) && (this.paymentDescription.length() > 255))
				{
					this.paymentDescription = this.paymentDescription.substring(0, 255);
				}

				if (!NumberUtil.isNumeric(this.commission))
				{
					this.commission = "0";
				}

				double dblCommission = Double.parseDouble(this.commission);

				pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertMerchantCreditMx"));
				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
				pstmt.setString(2, sessionData.getProperty("username"));
				pstmt.setString(3, this.paymentDescription);
				pstmt.setDouble(4, Double.parseDouble(this.getPaymentAmount()));
				pstmt.setDouble(5, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
				pstmt.setDouble(6, currentRunningLiability);
				pstmt.setDouble(7, currentAvailableCredit);
				pstmt.setDouble(8, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
				pstmt.setTimestamp(9, new java.sql.Timestamp(new java.util.Date().getTime()));
				pstmt.setString(10, this.depositDate);
				pstmt.setString(11, this.bank);
				pstmt.setString(12, this.refNumber);

				if (bCommissionByValue)
				{
					pstmt.setString(13, "VALUE-Pago via Support Site");
				}
				else
				{
					pstmt.setString(13, "PERCE-Pago via Support Site");
				}
				// R27-DBSY-586. added by jacuna
				pstmt.setInt(14, this.merchantCreditPaymentType);
				pstmt.setInt(15, this.bank_id);
				// END R27-DBSY-586.

				pstmt.executeUpdate();

				// DTU-369 Payment Notifications
				if (Rep.GetPaymentNotification(this.getMerchantId()))
				{
					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertPaymentNotification"));
					pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
					pstmt.setString(2, sessionData.getProperty("username"));
					pstmt.setString(3, this.paymentDescription);
					pstmt.setDouble(4, Double.parseDouble(this.getPaymentAmount()));
					pstmt.setDouble(5, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
					pstmt.setDouble(6, currentRunningLiability);
					pstmt.setDouble(7, currentAvailableCredit);
					pstmt.setDouble(8, 0);
					pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
					pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
					pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeMerchantType(this.getMerchantId()));
					pstmt.setString(12, this.businessName);
					pstmt.setInt(13, Rep.GetPaymentNotificationType(this.getMerchantId()));
					pstmt.setString(14, Rep.GetSmsNotificationPhone(this.getMerchantId()));
					pstmt.setString(15, Rep.GetMailNotificationAddress(this.getMerchantId()));
					pstmt.executeUpdate();
					pstmt.close();
				}

				TorqueHelper.closeStatement(pstmt, null);

				double dblNewLiabilityLimit = currentAvailableCredit + Double.parseDouble(this.getPaymentAmount());
				String strNewLiabilityLimit = NumberUtil.formatCurrency(Double.toString(dblNewLiabilityLimit));

				Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.liability_limit",
						sessionData.getLanguage())
						+ NumberUtil.formatCurrency(Double.toString(currentLiabilityLimit))
						+ " "
						+ Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + strNewLiabilityLimit,
						DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during updateLiabilityLimit", e);
				throw new CustomerException();
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}
	}

	public void deleteClerkCodeMx(ServletContext context, SessionData sessionData, String strClerkCode)
	{
		Connection dbConn = null;
		LogChanges logChanges = new LogChanges(sessionData);
		logChanges.setContext(context);
		PreparedStatement pstmt = null;

		if (this.checkPermission(sessionData))
		{
			if (StringUtil.isAlphaNumeric(strClerkCode))
			{
				try
				{
					dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

					if (dbConn == null)
					{
						Merchant.cat.error("Can't get database connection");
						throw new Exception();
					}
					String strClerkName = this.getClerkNameForCode(strClerkCode, this.siteId);

					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("deleteClerkCode"));
					pstmt.setString(1, strClerkCode);
					pstmt.setDouble(2, Double.parseDouble(this.siteId));
					pstmt.executeUpdate();

					String reason = "Removed clerk code: " + strClerkCode + ", clerk name: " + strClerkName
							+ ", SiteID = " + this.siteId;
					logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE),
							strClerkCode, this.siteId, reason);

					reason = "Removed clerk name: " + strClerkName + ", clerk code: " + strClerkCode + ", SiteID = "
							+ this.siteId;
					logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKNAME),
							strClerkCode, this.siteId, reason);
					this.addMerchantNote(Long.parseLong(this.merchantId), reason, sessionData.getProperty("username"));

					Log.write(sessionData, "Site ID: "
							+ this.siteId
							+ "."
							+ Languages.getString("com.debisys.customers.Merchant.Log.clerk_code", sessionData
									.getLanguage())
							+ "\""
							+ strClerkCode
							+ "\""
							+ Languages.getString("com.debisys.customers.Merchant.Log.deleted", sessionData
									.getLanguage()) + ".", DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
							DebisysConstants.PW_REF_TYPE_MERCHANT);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during deleteClerkCode", e);
				}
				finally
				{
					try
					{
						DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
					}
					catch (Exception e)
					{
						Merchant.cat.error("Error during release connection", e);
					}
				}
			}
		}
		else
		{
			this.addFieldError("error", "Permission Denied.");
		}
	}

	public void addClerkCodeMx(ServletContext context, SessionData sessionData, String strClerkCode,
			String strClerkName, String strClerkLastName, boolean bIsAdmin)
	{
		Connection dbConn = null;
		LogChanges logChanges = new LogChanges(sessionData);
		logChanges.setContext(context);
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		if (!StringUtil.isAlpha(strClerkName))
		{
			this.addFieldError("ErrorValidate", "Error");

			return;
		}

		if ((strClerkLastName.length() > 0) && !StringUtil.isAlpha(strClerkLastName))
		{
			this.addFieldError("ErrorValidate", "Error");

			return;
		}

		if (strClerkCode.length() > 10)
		{
			strClerkCode = strClerkCode.substring(0, 10);
		}

		if (strClerkName.length() > 16)
		{
			strClerkName = strClerkName.substring(0, 16);
		}

		if (strClerkLastName.length() > 24)
		{
			strClerkName = strClerkLastName.substring(0, 24);
		}

		if (this.checkPermission(sessionData))
		{
			if (StringUtil.isAlphaNumeric(strClerkCode))
			{
				try
				{
					dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

					if (dbConn == null)
					{
						Merchant.cat.error("Can't get database connection");
						throw new Exception();
					}

					pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("checkClerkCode"));
					pstmt.setDouble(1, Double.parseDouble(this.siteId));
					pstmt.setString(2, strClerkCode);

					rs = pstmt.executeQuery();
					boolean returned = rs.next();
					TorqueHelper.closeStatement(pstmt, rs);
					if (returned)
					{
						// do nothing
						this.addFieldError("errorAdd", Languages.getString(
								"jsp.admin.customers.merchants_info.error_1", sessionData.getLanguage()));
					}
					else
					{
						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("insertClerkCodeMx"));
						pstmt.setDouble(1, Double.parseDouble(this.siteId));
						pstmt.setString(2, strClerkCode);
						pstmt.setString(3, strClerkName + " " + strClerkLastName);
						pstmt.setString(4, strClerkName);
						pstmt.setString(5, strClerkLastName);
						pstmt.setBoolean(6, bIsAdmin);
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						String reason = "Added a new clerk code: " + strClerkCode + ", clerk name: " + strClerkName
								+ ", SiteID = " + this.siteId;
						logChanges.set_value_for_check(logChanges
								.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE), null);
						logChanges.was_changed(
								logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKCODE), strClerkCode,
								this.siteId, reason);

						reason = "Added a new clerk name: " + strClerkName + ", clerk code: " + strClerkCode
								+ ", SiteID = " + this.siteId;
						logChanges.set_value_for_check(logChanges
								.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKNAME), null);
						logChanges.was_changed(
								logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TERM_CLERKNAME), strClerkName,
								this.siteId, reason);
						this.addMerchantNote(Long.parseLong(this.merchantId), reason, sessionData
								.getProperty("username"));

						Merchant.cat.debug(pstmt.toString().toString());

						pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateClerkCode"));
						pstmt.setInt(1, 1);
						pstmt.setDouble(2, Double.parseDouble(this.siteId));
						pstmt.executeUpdate();

						TorqueHelper.closeStatement(pstmt, null);

						Merchant.cat.debug("[addClerkCodeMx] with params: 1=" + 1 + ", 2=" + this.siteId + ": "
								+ Merchant.sql_bundle.getString("updateClerkCode"));

						Log.write(sessionData, "Site ID: "
								+ this.siteId
								+ "."
								+ Languages.getString("com.debisys.customers.Merchant.Log.clerk_code", sessionData
										.getLanguage())
								+ "\""
								+ strClerkCode
								+ "\""
								+ Languages.getString("com.debisys.customers.Merchant.Log.added", sessionData
										.getLanguage()) + ".", DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId,
								DebisysConstants.PW_REF_TYPE_MERCHANT);
					}
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during addClerkCode", e);
				}
				finally
				{
					try
					{
						DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
					}
					catch (Exception e)
					{
						Merchant.cat.error("Error during release connection", e);
					}
				}
			}
		}
		else
		{
			this.addFieldError("error", "Permission Denied.");
		}
	}

	public boolean validateDate(String start_date)
	{
		this.validationErrors.clear();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		formatter.setLenient(false);
		boolean valid = true;
		if ((start_date == null) || (start_date.length() == 0))
		{
			start_date = formatter.format(new java.util.Date()); // now uses
			// current date
			// as default
			valid = true;
		}
		else if (!DateUtil.isValid(start_date))
		{
			valid = false;
		}

		if (valid)
		{

			try
			{
				formatter.parse(start_date);
			}
			catch (ParseException ex)
			{
				valid = false;
			}
		}

		return valid;
	}

	public Vector getMerchantCreditsMx(int intPageNumber, int intRecordsPerPage, SessionData sessionData,
			ServletContext context, boolean needsDBAName) throws CustomerException
	{
		Connection dbConn = null;
		CallableStatement pstmt = null;
		ResultSet rs = null;
		Vector vecMerchantCredits = new Vector();

		try
		{
			String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
			String reportId = sessionData.getProperty("reportId");
			int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);

			if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER))
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			}
			else
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));
			}

			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			this.endDate = format.format(new Date());
			this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
			Date date1 = format.parse(this.startDate);
			Date date2 = format.parse(this.endDate);
			format.applyPattern("yyyy-MM-dd");
			this.startDate = format.format(date1);
			this.endDate = format.format(date2);
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData
					.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate
					+ " 23:59:59.999", false);
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQLWhere = "";
			String instanceName = DebisysConfigListener.getInstance(context);
			// get a count of total matches

			String strSQL = Merchant.sql_bundle.getString("getPaymentsHistoryByMerchantIdMX");
			Merchant.cat.debug(strSQL);
			pstmt = dbConn.prepareCall(strSQL, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

			Merchant.cat.debug("Merchant Id: " + this.getMerchantId());
			Merchant.cat.debug("Start Date: " + this.startDate);
			Merchant.cat.debug("End Date: " + this.endDate);
			pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
			if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate))
			{
				pstmt.setString(2, vTimeZoneFilterDates.get(0));
				pstmt.setString(3, vTimeZoneFilterDates.get(1));
			}
			else
			{
				pstmt.setNull(2, Types.DATE);
				pstmt.setNull(3, Types.DATE);
			}
			pstmt.setBoolean(4, true);
			pstmt.setNull(5, Types.INTEGER);
			pstmt.setNull(6, Types.INTEGER);
			pstmt.setString(7, instanceName);

			rs = pstmt.executeQuery();
			int intRecordCount = 0;
			if (rs.next())
			{
				intRecordCount = rs.getInt(1);
			}

			// first row is always the count
			vecMerchantCredits.add(new Integer(intRecordCount));
			String exDBA;
			boolean externalPayment;

			if (intRecordCount > 0)
			{

				pstmt.setDouble(1, Double.parseDouble(this.getMerchantId()));
				if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate))
				{
					pstmt.setString(2, vTimeZoneFilterDates.get(0));
					pstmt.setString(3, vTimeZoneFilterDates.get(1));
				}
				else
				{
					pstmt.setNull(2, Types.DATE);
					pstmt.setNull(3, Types.DATE);
				}
				pstmt.setBoolean(4, false);
				pstmt.setInt(5, intPageNumber);
				pstmt.setInt(6, intRecordsPerPage);
				pstmt.setString(7, instanceName);

				rs = pstmt.executeQuery();
				rs.next();
				Double previousBalance = -1D;
				Double balance;
				for (int i = 0; i < intRecordsPerPage; i++)
				{
					Vector<String> vTimeZoneDate = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), rs.getString("log_datetime"), rs.getString("log_datetime"),true);
					boolean bCommissionByValue = false;
					Vector vecTemp = new Vector();
					vecTemp.add(vTimeZoneDate.get(2));
					String logon_id = rs.getString("logon_id");
					vecTemp.add(StringUtil.toString(logon_id));

					String s = StringUtil.toString(rs.getString("PaymentOrigin"));

					if (s.indexOf("VALUE-") >= 0)
					{
						bCommissionByValue = true;
						s = s.replaceAll("VALUE-", "");
					}
					else
					{
						bCommissionByValue = false;
						s = s.replaceAll("PERCE-", "");
					}

					vecTemp.add(2, s);
					vecTemp.add(3, StringUtil.toString(rs.getString("BankName")));
					vecTemp.add(4, StringUtil.toString(rs.getString("ReferenceNumber")));
					vecTemp.add(5, StringUtil.toString(rs.getString("Deposit_date")));
					vecTemp.add(6, StringUtil.toString(rs.getString("description")));

					if (logon_id.equals("Events Job"))
						vecTemp.add("");
					else
						vecTemp.add(7, NumberUtil.formatCurrency(rs.getString("payment")));

					balance = rs.getDouble("balance");
					if ((balance != null && !balance.equals(previousBalance))
							|| (rs.getString("logon_id") != null && !rs.getString("logon_id").equals("Commission") && !rs
									.getString("logon_id").equals("Billing")))
					{
						vecTemp.add(8, NumberUtil.formatCurrency(rs.getString("balance")));
					}
					else
					{
						vecTemp.add("");
					}

					String commission = "";
					if (rs.getString("commission") == null)
					{
						vecTemp.add(9, NumberUtil.formatAmount("0.00"));
						commission = "0.00";
					}
					else
					{
						if (bCommissionByValue)
						{
							vecTemp.add(9, NumberUtil.formatAmount(rs.getString("commission")));
						}
						else
						{
							vecTemp.add(9, NumberUtil.formatAmount(rs.getString("commission")) + "%");
						}
						commission = rs.getString("commission");
					}

					if (logon_id.equals("Events Job"))
					{
						vecTemp.add("");
					}
					else
					{
						if (bCommissionByValue)
						{
							vecTemp.add(10, NumberUtil.formatCurrency(Double.toString(Double.parseDouble(rs
									.getString("payment"))
									- Double.parseDouble(commission))));
						}
						else
						{
							vecTemp.add(10, NumberUtil.formatCurrency(rs.getString("net_payment")));
						}
					}

					vecTemp.add(11, StringUtil.toString(rs.getString("dba")));

					if (rs.getString("PaymentType") == null)
					{
						vecTemp.add(12, " ");
					}
					else
					{
						vecTemp.add(12, StringUtil.toString(rs.getString("PaymentType")));
					}

					// DBSY-1139 ExternalDBA
					exDBA = rs.getString("ExternalDBA");

					if (exDBA == null)
					{
						externalPayment = false;
						vecTemp.add(13, "Assigned");
					}
					else
					{
						externalPayment = true;
						if (needsDBAName)
						{
							vecTemp.add(13, exDBA);
						}
						else
						{
							vecTemp.add(13, "External");
						}

					}

					if (needsDBAName && externalPayment == false)
					{
						// We don't want to add it to the returned table
					}
					else
					{
						vecMerchantCredits.add(vecTemp);

						previousBalance = balance;
					}
					if (!rs.next())
					{
						break;
					}
				}
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantCreditsMX", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantCredits;
	}

	public Vector getUserMerchantCreditsMx(int intPageNumber, int intRecordsPerPage, SessionData sessionData,
			ServletContext context, boolean needsDBAName) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector vecMerchantCredits = new Vector();

		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
			String reportId = sessionData.getProperty("reportId");
			int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);

			if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER))
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			}
			else
			{
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));
			}

			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			// this.endDate = format.format(new Date());
			// Parature 5545-10237043 = Jorge Nicol�s Mart�nez Romero
			int daysBetween = DateUtil.getDaysBetween(this.endDate.toString(), this.startDate.toString());
			if (daysBetween > limitDays)
			{
				this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
			}
			// END Parature 5545-10237043 = Jorge Nicol�s Mart�nez Romero

			Date date1 = format.parse(this.startDate);
			Date date2 = format.parse(this.endDate);
			format.applyPattern("yyyy-MM-dd");
			this.startDate = format.format(date1);
			this.endDate = format.format(date2);
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData
					.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate
					+ " 23:59:59.999", false);
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL_CHAIN_JOINS = "";
			if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSQL_CHAIN_JOINS += " INNER JOIN dbo.reps AS R WITH(NOLOCK) ON M.rep_id = R.rep_id AND R.rep_id = "
						+ strRefId;
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)
					|| (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType
							.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
			{
				strSQL_CHAIN_JOINS += " INNER JOIN dbo.reps R WITH(NOLOCK) ON M.rep_id = R.rep_id AND R.rep_id IN (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id = "
						+ strRefId + ")";
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL_CHAIN_JOINS += " INNER JOIN dbo.reps R WITH(NOLOCK) ON M.rep_id = R.rep_id AND R.rep_id IN (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id IN"
						+ " (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id = " + strRefId + "))";
			}
			else if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL_CHAIN_JOINS += " INNER JOIN dbo.reps R WITH (NOLOCK) ON M.rep_id=R.rep_id"
							+ " INNER JOIN dbo.reps I WITH (NOLOCK) ON I.rep_id=R.iso_id AND I.type="
							+ DebisysConstants.REP_TYPE_ISO_3_LEVEL + " AND I.rep_id=I.iso_id AND I.rep_id=" + strRefId;
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL_CHAIN_JOINS += " INNER JOIN dbo.reps R WITH (NOLOCK) ON M.rep_id=R.rep_id"
							+ " INNER JOIN dbo.reps S WITH (NOLOCK) ON S.rep_id=R.iso_id AND S.type="
							+ DebisysConstants.REP_TYPE_SUBAGENT
							+ " INNER JOIN dbo.reps A WITH (NOLOCK) ON A.rep_id=S.iso_id AND A.type="
							+ DebisysConstants.REP_TYPE_AGENT
							+ " INNER JOIN dbo.reps I WITH (NOLOCK) ON I.rep_id=A.iso_id AND I.type="
							+ DebisysConstants.REP_TYPE_ISO_5_LEVEL + " AND I.rep_id=I.iso_id AND I.rep_id=" + strRefId;
				}
			}

			String strSQLWhere = "";

			// get a count of total matches
			String strSQL = "SELECT M.dba, DATE, MC.logon_id, MC.description, MC.payment, "
					+ "MC.balance,MC.commission,MC.ExternalDBA, MC.Deposit_date, ISNULL(B.Name,'') AS Bank, "
					+ "MC.ReferenceNumber, MC.PaymentOrigin, "
					+ "(MC.payment - (MC.payment * (isnull(MC.commission,0) * .01))) as net_payment, "
					+ "ISNULL(MCPT.PaymentType, 'None') AS PaymentType " + "FROM dbo.Merchant_credits MC WITH(NOLOCK) "
					+ "INNER JOIN dbo.merchants M WITH(NOLOCK) ON MC.merchant_id = M.merchant_id "
					+ "LEFT JOIN dbo.Banks B WITH(NOLOCK) ON MC.Bank_id = B.[ID] "
					+ "LEFT JOIN dbo.Merchant_Credit_Payment_Types MCPT WITH(NOLOCK) ON MC.Payment_Type = MCPT.id";
			strSQL += strSQL_CHAIN_JOINS;
			strSQL += " WHERE	MC.Logon_id = ?";

			if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate))
			{
				strSQLWhere = " and mc.log_datetime >= '" + vTimeZoneFilterDates.get(0) + "' and mc.log_datetime <= '"
						+ vTimeZoneFilterDates.get(1) + "'";
			}
			strSQL = strSQL.replace("DATE", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId
					+ ", mc.log_datetime, 1) as log_datetime");
			String sCompleteSQL = strSQL + strSQLWhere + " order by mc.id DESC";
			Merchant.cat.debug("[getUserMerchantCreditsMx] unprepared SQL statement: " + sCompleteSQL);
			Merchant.cat.debug("[getUserMerchantCreditsMx] parameter 1: getMerchantId()=" + this.getMerchantId());
			pstmt = dbConn
					.prepareStatement(sCompleteSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			pstmt.setString(1, this.getMerchantId());

			rs = pstmt.executeQuery();
			int intRecordCount = DbUtil.getRecordCount(rs);
			// first row is always the count
			vecMerchantCredits.add(new Integer(intRecordCount));

			if (intRecordCount > 0)
			{
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));

				for (int i = 0; i < intRecordsPerPage; i++)
				{
					boolean bCommissionByValue = false;
					Vector vecTemp = new Vector();
					vecTemp.add(0, DateUtil.formatDateTime(rs.getTimestamp("log_datetime")));
					vecTemp.add(1, StringUtil.toString(rs.getString("logon_id")));

					String s = StringUtil.toString(rs.getString("PaymentOrigin"));

					if (s.indexOf("VALUE-") >= 0)
					{
						bCommissionByValue = true;
						s = s.replaceAll("VALUE-", "");
					}
					else
					{
						bCommissionByValue = false;
						s = s.replaceAll("PERCE-", "");
					}

					vecTemp.add(2, s);
					vecTemp.add(3, StringUtil.toString(rs.getString("Bank")));
					vecTemp.add(4, StringUtil.toString(rs.getString("ReferenceNumber")));
					vecTemp.add(5, StringUtil.toString(rs.getString("Deposit_date")));
					vecTemp.add(6, StringUtil.toString(rs.getString("description")));
					vecTemp.add(7, NumberUtil.formatCurrency(rs.getString("payment")));
					vecTemp.add(8, NumberUtil.formatCurrency(rs.getString("balance")));

					String commission = "";
					if (rs.getString("commission") == null)
					{
						vecTemp.add(9, "0.00");
						commission = "0.00";
					}
					else
					{
						if (bCommissionByValue)
						{
							vecTemp.add(9, NumberUtil.formatAmount(rs.getString("commission")));
						}
						else
						{
							vecTemp.add(9, NumberUtil.formatAmount(rs.getString("commission")) + "%");
						}
						commission = rs.getString("commission");
					}

					if (bCommissionByValue)
					{
						vecTemp.add(10, NumberUtil.formatCurrency(Double.toString(Double.parseDouble(rs
								.getString("payment"))
								- Double.parseDouble(commission))));
					}
					else
					{
						vecTemp.add(10, NumberUtil.formatCurrency(rs.getString("net_payment")));
					}
					vecTemp.add(11, StringUtil.toString(rs.getString("dba")));
					vecTemp.add(12, StringUtil.toString(rs.getString("PaymentType")));
					if (rs.getString("ExternalDBA") == null)
					{
						vecTemp.add(13, "Assigned");
					}
					else
					{
						vecTemp.add(13, "External");
					}
					vecMerchantCredits.add(vecTemp);

					if (!rs.next())
					{
						break;
					}
				}
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantCredits", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantCredits;
	}

	/* END MiniRelease 1.0 - Added by YH */
	public void valueBound(HttpSessionBindingEvent event)
	{
	}

	public void valueUnbound(HttpSessionBindingEvent event)
	{
	}

	public static void main(String... args) throws Exception
	{
	}

	/* BEGIN MiniRelease 2.0 - Added by LF */

	/**
	 * Gets a list of Contact Types
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getContactTypes() throws CustomerException
	{
		Vector vecContactTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getContactTypesMx");
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("ContactTypeID"));
				vecTemp.add(rs.getString("ContactTypeCode"));
				vecContactTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getContactTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecContactTypes;
	}

	/**
	 * Gets a specific Contact Type by its Id
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static String getContactTypeById(String contactTypeId, SessionData sessionData) throws CustomerException
	{
		String contactTypeDescription = "";
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getContactTypeByIdMx");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, contactTypeId);

			rs = pstmt.executeQuery();

			if (rs.next())
			{
				contactTypeDescription = Languages.getString("jsp.admin.customers.merchants_edit.contacttype_"
						+ rs.getString("ContactTypeCode"), sessionData.getLanguage());
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getContactTypeById", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return contactTypeDescription;
	}

	/**
	 * Gets a specific Contact Type by its Code
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getContactTypeByCode(String contactTypeCode, SessionData sessionData) throws CustomerException
	{
		Vector vecResult = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getContactTypeByCodeMx");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, contactTypeCode);

			rs = pstmt.executeQuery();

			if (rs.next())
			{
				vecResult.add(rs.getString("ContactTypeID"));
				vecResult.add(Languages.getString("jsp.admin.customers.merchants_edit.contacttype_"
						+ rs.getString("ContactTypeCode"), sessionData.getLanguage()));
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getContactTypeByCode", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecResult;
	}

	/* END MiniRelease 2.0 - Added by LF */

	/* BEGIN Release 9.0 - Added by LF */
	public void doRepMerchantPayment(SessionData sessionData, ServletContext context, String sData)
			throws CustomerException
	{
		try
		{
			String[] sMerchants = sData.split("&");

			// Through merchants of this Rep
			for (int i = 0; i < sMerchants.length; i++)
			{
				if (sMerchants[i].length() > 0)
				{
					String[] sItems = sMerchants[i].split(":");
					Merchant m = new Merchant();
					m.setMerchantId(sItems[0]);
					m.getMerchant(sessionData, context);
					m.setPaymentAmount(sItems[1]);
					m.updateRunningLiability(sessionData, context);
				}
			} // End of for through merchants of this Rep
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during doRepMerchantPayment", e);
			throw new CustomerException();
		}
	} // End of function doRepMerchantPayment

	public Vector doPaymentMX(SessionData sessionData, ServletContext context, boolean bCommissionByValue)
			throws CustomerException
	{
            System.out.println("so::call::doPaymentMX::" + this.bank);
		Connection dbConn = null;
		Vector vResult = new Vector();
		CallableStatement cst = null;
		String strSQL = "";
		String sDeploymentType = DebisysConfigListener.getDeploymentType(context);
		String sCustomConfigType = DebisysConfigListener.getCustomConfigType(context);

		try
		{
			if (this.checkPermission(sessionData))
			{
				strSQL = Merchant.sql_bundle.getString("applyMerchantPayment");// EXEC
				// ApplyMerchantPayment
				// ?,
				// ?,
				// ?,
				// ?,
				// ?,
				// ?,
				// ?,
				// ?,
				// ?,
				// ?,
				// ?,
				// ?
				dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

				if (dbConn == null)
				{
					Merchant.cat.error("Can't get database connection");
					throw new ReportException();
				}

				if (!this.validateDate(this.depositDate))
				{
					String error3Text = Languages.getString("jsp.admin.customers.merchants.applyPayment.checkDate",
							sessionData.getLanguage());
					this.addFieldError("error3", error3Text);
					vResult.add(999); // defaulting to an error condition
					return vResult;
				}

				Merchant.cat.debug(strSQL);
				cst = dbConn.prepareCall(strSQL);
				cst.setString(1, ((Vector) Merchant.getTerminals(this.merchantId).get(0)).get(0).toString());
				cst.setDouble(2, Double.parseDouble(this.paymentAmount));
				cst.setString(3, this.depositDate);
				cst.setDouble(4, Double.parseDouble(this.commission));
				cst.setInt(5, (bCommissionByValue) ? 1 : 0);
				cst.setString(6, this.paymentDescription);
				cst.setString(7, this.bank);
				cst.setString(8, this.refNumber);
				cst.setString(9, sessionData.getProperty("username"));
				cst.setString(10, sDeploymentType);
				cst.setString(11, sCustomConfigType);
				cst.registerOutParameter(12, Types.INTEGER);
				cst.setInt(12, 0);
				cst.registerOutParameter(13, Types.VARCHAR);
				cst.setString(13, "");
				// System.out.println("[[[Merchant::PRINT]]] JavaBean property value="
				// + getMerchantCreditPaymentType());
				cst.setInt(14, this.merchantCreditPaymentType);
				cst.setInt(15, this.bank_id);
                                
                                System.out.println("EXEC ApplyMerchantPayment " 
                                        + ((Vector) Merchant.getTerminals(this.merchantId).get(0)).get(0).toString()
                                        + "," + Double.parseDouble(this.paymentAmount)
                                        + "," + this.depositDate
                                        + "," + Double.parseDouble(this.commission)
                                        + "," + 0 
                                        + "," + this.paymentDescription
                                        + "," + this.bank
                                        + "," + this.refNumber
                                        + "," + sessionData.getProperty("username")
                                        + "," + sDeploymentType
                                        + "," + sCustomConfigType
                                        + "," + 0
                                        + "," + " "
                                        + "," + this.merchantCreditPaymentType
                                        + "," + this.bank_id);
                                
				cst.execute();
				vResult.add(cst.getInt(12));
			}
		}
		catch (Exception e)
		{
                    e.printStackTrace();
			Merchant.cat.error("Error during doPaymentMX", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, cst, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vResult;
	} // End of function doPaymentMX

	/* END Release 9.0 - Added by LF */

	/* BEGIN Release 10.0 - Added by LF */
	public static Vector getTerminalRegistration(String sMillennium_No)
	{
		Vector vResult = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getTerminalRegistration"));
			pstmt.setString(1, sMillennium_No);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("UserID")));
				vResult.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalRegistration", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return vResult;
	} // End of function getTerminalRegistration

	/* END Release 10.0 - Added by LF */

	/* BEGIN Release 13.0 - Added by LF */
	public static String getTerminalRegistrationByMerchant(String sMerchantID)
	{
		String sResult = "";
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getTerminalRegistrationByMerchant"));
			pstmt.setString(1, sMerchantID);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				if (sResult.length() > 0)
				{
					sResult += ", ";
				}

				sResult += StringUtil.toString(rs.getString("UserID"));
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalRegistrationByMerchant", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return sResult;
	} // End of function getTerminalRegistrationByMerchant
	
	public static String getTerminalRegistrationByMerchantAndSiteId(String sMerchantID, String sSiteId)
	{
		String sResult = "";
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("getTerminalRegistrationByMerchantAndSiteId"));
			pstmt.setString(1, sMerchantID);
			pstmt.setString(2, sSiteId);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				if (sResult.length() > 0)
				{
					sResult += ", ";
				}

				sResult += StringUtil.toString(rs.getString("UserID"));
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalRegistrationByMerchant", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}

		return sResult;
	}

	/* END Release 13.0 - Added by LF */

	/**
	 * Gets a list of Regions for the Mexico country
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getRegions(String customConfigType) throws CustomerException
	{
		Vector vecRegions = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getRegions");
			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getInt("id"));
				vecTemp.add(rs.getString("NameRegion"));
				vecRegions.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getRegions", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecRegions;
	}

	// Create getInvoiceTypes
	public static Vector getInvoiceTypes() throws CustomerException
	{
		Vector vecInvoiceTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = "SELECT it.id,it.description, it.Code FROM Invoice_Types it WITH(NOLOCK) WHERE it.status ='true' "; // sql_bundle.getString("getRegions");
			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getInt("id"));
				vecTemp.add(rs.getString("description"));
				vecTemp.add(rs.getString("Code"));
				vecInvoiceTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getRegions", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecInvoiceTypes;
	}

	// Create MerchantClassifications
	public static Vector getMerchantClassifications(SessionData sessionData) throws CustomerException
	{
		Vector vecMerchantClassifications = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = "SELECT mc.id,mc.description, description_eng FROM MerchantClassifications mc WITH(NOLOCK) WHERE mc.Status='true'"; // sql_bundle.getString("getRegions");
			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getInt("id"));
				if (sessionData.getLanguage().equalsIgnoreCase("spanish"))
				{
					vecTemp.add(rs.getString("description"));
				}
				else
				{
					vecTemp.add(rs.getString("description_eng"));
				}

				vecMerchantClassifications.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantClassifications", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantClassifications;
	}

	public void updateTerminalsStatus(SessionData sessionData, String strMillennium_No, String strMerchantId,
			String status)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		Connection dbConn1 = null;
		PreparedStatement pstmt1 = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			pstmt = dbConn.prepareStatement(Merchant.sql_bundle.getString("updateTerminalsStatus"));
			pstmt.setInt(1, Integer.parseInt(status));
			pstmt.setDouble(2, Double.parseDouble(strMillennium_No));
			pstmt.setDouble(3, Double.parseDouble(strMerchantId));

			pstmt.executeUpdate();

			TorqueHelper.closeStatement(pstmt, null);

			String statusChangedTo = null;
			String langPropertyNameRoot = "com.debisys.customers.Merchant.Log.terminalsStatus";
			switch (Integer.parseInt(status))
			{
			case 1:
				statusChangedTo = Languages.getString(langPropertyNameRoot + ".disabled", sessionData.getLanguage());
				break;
			case 0:
				statusChangedTo = Languages.getString(langPropertyNameRoot + ".enabled", sessionData.getLanguage());
				break;
			default:
				statusChangedTo = Languages.getString(langPropertyNameRoot + ".other", sessionData.getLanguage()) + " "
						+ status;
			}
			Log.write(sessionData, "Site ID: " + this.siteId + "."
					+ Languages.getString(langPropertyNameRoot, sessionData.getLanguage()) + " : " + statusChangedTo,
					DebisysConstants.LOGTYPE_CUSTOMER, this.merchantId, DebisysConstants.PW_REF_TYPE_MERCHANT);

			// Disable and enabled marketPlace Terminal if existent
			try
			{
				dbConn1 = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgMarketPlaceDb"));

				if (dbConn1 == null)
				{
					Merchant.cat.error("Can't get MarketPlace database connection on modify Terminal Status");
				}
				else
				{
					if (status.equals("0"))
					{
						pstmt1 = dbConn1.prepareStatement(Merchant.sql_bundle
								.getString("enableMarketPlaceTerminalStatus"));
					}
					else
					{
						pstmt1 = dbConn1.prepareStatement(Merchant.sql_bundle
								.getString("disableMarketPlaceTerminalStatus"));
					}

					pstmt1.setString(1, strMillennium_No);
					pstmt1.executeUpdate();
				}
			}
			catch (Exception ex)
			{
				Merchant.cat.error("Error updating marketplace terminal" + ex);
			}
			finally
			{
				try
				{
					DbUtil.closeDatabaseObjects(dbConn1, pstmt1, null);
				}
				catch (Exception e)
				{
					Merchant.cat.error("Error during release connection", e);
				}
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateTerminalsStatus", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	// #BEGIN Added by NM - Release 21

	/**
	 * Gets a list of Payment Types
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getPaymentTypes() throws CustomerException
	{
		Vector vecPaymentTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getPaymentType");
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("paymenttypeid"));
				vecTemp.add(rs.getString("description"));
				vecPaymentTypes.add(vecTemp);
			}

			//rs.close();
			//pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getPaymentTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecPaymentTypes;
	}

	/**
	 * Gets a list of Processor Types
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public static Vector getProcessorTypes() throws CustomerException
	{
		Vector vecProcessorTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getProcessType");
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("id"));
				vecTemp.add(rs.getString("processor_name"));
				vecProcessorTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getProcessorTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecProcessorTypes;
	}

	public void sendMailNotification(String mailHost, String sAccessLevel, String companyName, String userName,
			String sUserName, String sPassword, String sMails, ServletContext context) throws Exception
	{
            String deploymentType = DebisysConfigListener.getDeploymentType(context);
		String sHeader = "<body bgcolor=\"#ffffff\"><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #f1f9fe;width:700px;\"><tr><td><b>New Merchant: [_DBA_]</b></td></tr><tr><td><b>Added by _ACTOR_: [_USERNAME_] from [_COMPANYNAME_]</b></td></tr>";
		String sCompany = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Company Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Sales Rep:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_REPNAME_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Business Name:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_BUSINESSNAME_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>DBA:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_DBA_</td></tr></table></td></tr>";
		String sAddress = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Address Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Address:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_ADDRESS_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>City:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CITY_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>State:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_STATE_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>ZIP:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_ZIP_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>County:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_COUNTY_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Country:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_COUNTRY_</td></tr></table></td></tr>";
		String sContact = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Contact Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Contact:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CONTACT_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Phone:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_PHONE_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Fax:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_FAX_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>EMail:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_EMAIL_</td></tr></table></td></tr>";
		String sBanking = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Banking Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Tax ID:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_TAXID_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>ABA:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_ABA_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Account #:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_ACCOUNT_</td></tr></table></td></tr>";
		String sLogin = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Login Information</td></tr><tr><td align=center><table cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td style=\"font-size: 11pt;\" nowrap><b>Login:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_LOGIN_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Password:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_PASSWORD_</td></tr></table></td></tr>";
		String sFooter = "</table></body>";

		// String sAccessLevel = sessionData.getProperty("access_level");
		sHeader = sHeader.replaceAll("_DBA_", this.dba).replaceAll("_USERNAME_", userName).replaceAll("_COMPANYNAME_",
				this.cleanReplace(companyName));
		if (sAccessLevel.equals(DebisysConstants.ISO))
		{
			sHeader = sHeader.replaceAll("_ACTOR_", "ISO");
		}
		else if (sAccessLevel.equals(DebisysConstants.AGENT))
		{
			sHeader = sHeader.replaceAll("_ACTOR_", "AGENT");
		}
		else if (sAccessLevel.equals(DebisysConstants.SUBAGENT))
		{
			sHeader = sHeader.replaceAll("_ACTOR_", "SUBAGENT");
		}
		else if (sAccessLevel.equals(DebisysConstants.REP))
		{
			sHeader = sHeader.replaceAll("_ACTOR_", "REP");
		}

		sCompany = sCompany.replaceAll("_REPNAME_", this.cleanReplace(this.getMerchantRepName(this.repId)));
		sCompany = sCompany.replaceAll("_BUSINESSNAME_", this.cleanReplace(this.businessName));
		sCompany = sCompany.replaceAll("_DBA_", this.cleanReplace(this.dba));

		sAddress = sAddress.replaceAll("_ADDRESS_", this.cleanReplace(StringUtil.toString(this.physAddress)));
		sAddress = sAddress.replaceAll("_CITY_", StringUtil.toString(this.physCity));
		sAddress = sAddress.replaceAll("_STATE_", StringUtil.toString(Merchant.getStateName(this.physState)));
		sAddress = sAddress.replaceAll("_ZIP_", StringUtil.toString(this.physZip));
		sAddress = sAddress.replaceAll("_COUNTY_", StringUtil.toString(this.physCounty));
		sAddress = sAddress.replaceAll("_COUNTRY_", StringUtil.toString(this.physCountry));

		sContact = sContact.replaceAll("_CONTACT_", StringUtil.toString(this.contactName));
		sContact = sContact.replaceAll("_PHONE_", StringUtil.toString(this.contactPhone.replaceAll(
				Merchant.PHONE_CLEAN_REGEXP, "")));
                if (this.contactFax!=null){
		sContact = sContact.replaceAll("_FAX_", StringUtil.toString(this.contactFax.replaceAll(
				Merchant.PHONE_CLEAN_REGEXP, "")));
                } else{
                    sContact = sContact.replaceAll("_FAX_", "");
                }
		sContact = sContact.replaceAll("_EMAIL_", StringUtil.toString(this.contactEmail));

		sBanking = sBanking.replaceAll("_TAXID_", StringUtil.toString(this.taxId));
		sBanking = sBanking.replaceAll("_ABA_", StringUtil.toString(this.routingNumber));
		sBanking = sBanking.replaceAll("_ACCOUNT_", StringUtil.toString(this.accountNumber));
                if (!deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)){
                    sLogin = sLogin.replaceAll("_LOGIN_", sUserName);
                    sLogin = sLogin.replaceAll("_PASSWORD_", sPassword);
                } else{
                    sLogin="";
                }

		java.util.Properties props;
		props = System.getProperties();
		props.put("mail.smtp.host", mailHost);
		Session s = Session.getDefaultInstance(props, null);

		MimeMessage message = new MimeMessage(s);
		message.setHeader("Content-Type", "text/html;\r\n\tcharset=iso-8859-1");
		message.setFrom(new InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail(context)));
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(com.debisys.utils.ValidateEmail
				.getSetupMail(context)));

		if (sMails != null)
		{
			message.setRecipients(Message.RecipientType.CC, sMails.replaceAll(";", ","));
		}

		message.setSubject("New Merchant Added: [" + this.dba + "]");
		message.setContent(sHeader + sCompany + sAddress + sContact + sBanking + sLogin + sFooter, "text/html");
		Transport.send(message);
	}// End of function sendMailNotification

	/*
	 * // #END Added by NM - Release 21 public void
	 * sendMailNotification(ServletContext context, SessionData sessionData,
	 * String sUserName, String sPassword, String sMails) throws Exception {
	 * String sHeader = "<body bgcolor=\"#ffffff\"><table cellpadding=\"0\"
	 * cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP:
	 * #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd
	 * 1px solid;BACKGROUND-COLOR: #f1f9fe;width:700px;\"><tr><td><b>New
	 * Merchant: [_DBA_]</b></td></tr><tr><td><b>Added by _ACTOR_: [_USERNAME_]
	 * from [_COMPANYNAME_]</b></td></tr>"; String sCompany = "<tr><td
	 * style=\"FONT-WEIGHT: bold;FONT-SIZE: 12pt;\"><br><br>Company
	 * Information</td></tr><tr><td align=center><table cellpadding=\"0\"
	 * cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px solid;BORDER-TOP:
	 * #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px solid;BORDER-LEFT: #7b9ebd
	 * 1px solid;BACKGROUND-COLOR: #ffffff;width:98%;\"><tr><td
	 * style=\"font-size: 11pt;\" nowrap><b>Sales Rep:</b></td><td></td><td
	 * style=\
	 * "font-size: 11pt;\" nowrap>_REPNAME_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Business Name:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_BUSINESSNAME_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>DBA:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_DBA_</td></tr></table></td></tr>"
	 * ; String sAddress = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE:
	 * 12pt;\"><br><br>Address Information</td></tr><tr><td align=center><table
	 * cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px
	 * solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px
	 * solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR:
	 * #ffffff;width:98%;\
	 * "><tr><td style=\"font-size: 11pt;\" nowrap><b>Address:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_ADDRESS_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>City:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CITY_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>State:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_STATE_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>ZIP:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_ZIP_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>County:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_COUNTY_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>Country:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_COUNTRY_</td></tr></table></td></tr>"
	 * ; String sContact = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE:
	 * 12pt;\"><br><br>Contact Information</td></tr><tr><td align=center><table
	 * cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px
	 * solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px
	 * solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR:
	 * #ffffff;width:98%;\
	 * "><tr><td style=\"font-size: 11pt;\" nowrap><b>Contact:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_CONTACT_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Phone:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_PHONE_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>Fax:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_FAX_</td><td></td><td style=\"font-size: 11pt;\" nowrap><b>EMail:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_EMAIL_</td></tr></table></td></tr>"
	 * ; String sBanking = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE:
	 * 12pt;\"><br><br>Banking Information</td></tr><tr><td align=center><table
	 * cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px
	 * solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px
	 * solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR:
	 * #ffffff;width:98%;\
	 * "><tr><td style=\"font-size: 11pt;\" nowrap><b>Tax ID:</b></td><td></td><td style=\"font-size: 11pt;\" nowrap>_TAXID_</td></tr><tr><td style=\"font-size: 11pt;\" nowrap><b>ABA:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_ABA_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Account #:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_ACCOUNT_</td></tr></table></td></tr>"
	 * ; String sLogin = "<tr><td style=\"FONT-WEIGHT: bold;FONT-SIZE:
	 * 12pt;\"><br><br>Login Information</td></tr><tr><td align=center><table
	 * cellpadding=\"0\" cellspacing=\"0\" style=\"BORDER-RIGHT: #7b9ebd 1px
	 * solid;BORDER-TOP: #7b9ebd 1px solid;BORDER-BOTTOM: #7b9ebd 1px
	 * solid;BORDER-LEFT: #7b9ebd 1px solid;BACKGROUND-COLOR:
	 * #ffffff;width:98%;\
	 * "><tr><td style=\"font-size: 11pt;\" nowrap><b>Login:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_LOGIN_</td><td>&nbsp;&nbsp;</td><td style=\"font-size: 11pt;\" nowrap><b>Password:</b></td><td>&nbsp;</td><td style=\"font-size: 11pt;\" nowrap>_PASSWORD_</td></tr></table></td></tr>"
	 * ; String sFooter = "</table></body>"; String sAccessLevel =
	 * sessionData.getProperty("access_level"); sHeader =
	 * sHeader.replaceAll("_DBA_", this.dba) .replaceAll("_USERNAME_",
	 * sessionData.getProperty("username")) .replaceAll("_COMPANYNAME_",
	 * this.cleanReplace(sessionData.getProperty("company_name"))); if
	 * (sAccessLevel.equals(DebisysConstants.ISO)) { sHeader =
	 * sHeader.replaceAll("_ACTOR_", "ISO"); } else if
	 * (sAccessLevel.equals(DebisysConstants.AGENT)) { sHeader =
	 * sHeader.replaceAll("_ACTOR_", "AGENT"); } else if
	 * (sAccessLevel.equals(DebisysConstants.SUBAGENT)) { sHeader =
	 * sHeader.replaceAll("_ACTOR_", "SUBAGENT"); } else if
	 * (sAccessLevel.equals(DebisysConstants.REP)) { sHeader =
	 * sHeader.replaceAll("_ACTOR_", "REP"); } sCompany =
	 * sCompany.replaceAll("_REPNAME_",
	 * this.cleanReplace(this.getMerchantRepName(this.repId))); sCompany =
	 * sCompany.replaceAll("_BUSINESSNAME_",
	 * this.cleanReplace(this.businessName)); sCompany =
	 * sCompany.replaceAll("_DBA_", this.cleanReplace(this.dba)); sAddress =
	 * sAddress.replaceAll("_ADDRESS_",
	 * this.cleanReplace(StringUtil.toString(this.physAddress))); sAddress =
	 * sAddress.replaceAll("_CITY_", StringUtil.toString(this.physCity));
	 * sAddress = sAddress.replaceAll("_STATE_",
	 * StringUtil.toString(this.physState)); sAddress =
	 * sAddress.replaceAll("_ZIP_", StringUtil.toString(this.physZip)); sAddress
	 * = sAddress.replaceAll("_COUNTY_", StringUtil.toString(this.physCounty));
	 * sAddress = sAddress.replaceAll("_COUNTRY_",
	 * StringUtil.toString(this.physCountry)); sContact =
	 * sContact.replaceAll("_CONTACT_", StringUtil.toString(this.contactName));
	 * sContact = sContact.replaceAll("_PHONE_",
	 * StringUtil.toString(this.contactPhone)); sContact =
	 * sContact.replaceAll("_FAX_", StringUtil.toString(this.contactFax));
	 * sContact = sContact.replaceAll("_EMAIL_",
	 * StringUtil.toString(this.contactEmail)); sBanking =
	 * sBanking.replaceAll("_TAXID_", StringUtil.toString(this.taxId)); sBanking
	 * = sBanking.replaceAll("_ABA_", StringUtil.toString(this.routingNumber));
	 * sBanking = sBanking.replaceAll("_ACCOUNT_",
	 * StringUtil.toString(this.accountNumber)); sLogin =
	 * sLogin.replaceAll("_LOGIN_", sUserName); sLogin =
	 * sLogin.replaceAll("_PASSWORD_", sPassword); java.util.Properties props;
	 * props = System.getProperties(); props.put("mail.smtp.host",
	 * DebisysConfigListener.getMailHost(context)); Session s =
	 * Session.getDefaultInstance(props, null); MimeMessage message = new
	 * MimeMessage(s); message.setHeader("Content-Type",
	 * "text/html;\r\n\tcharset=iso-8859-1"); message.setFrom(new
	 * InternetAddress( com.debisys.utils.ValidateEmail.getSetupMail()));
	 * message.setRecipient(Message.RecipientType.TO, new
	 * InternetAddress(com.debisys.utils.ValidateEmail.getSetupMail()));
	 * message.setRecipients(Message.RecipientType.CC, sMails.replaceAll(";",
	 * ",")); message.setSubject("New Merchant Added: [" + this.dba + "]");
	 * message.setContent(sHeader + sCompany + sAddress + sContact + sBanking +
	 * sLogin + sFooter, "text/html"); Transport.send(message); } // End of
	 * function sendMailNotification
	 */
	private String cleanReplace(String sData)
	{
		String sResult = sData;
		sResult = sResult.replace('$', ' ');
		// String apostrophe = new String(new byte[]{ (byte) 0x27 });
		// String right_single_quotation_mark = new String(new byte[]{ (byte)
		// 0x92 });
		// sResult = sResult.replaceAll(right_single_quotation_mark,
		// apostrophe);
		return sResult;
	}

	/*************************************************************************************************************************************************************
	 * ADDED FOR LOCALIZATION
	 * 
	 * @return
	 */
	public String getStartDateFormatted()
	{
		return this.getDateFormatted(this.startDate);
	}

	public String getEndDateFormatted()
	{
		return this.getDateFormatted(this.endDate);
	}

	private String getDateFormatted(String dateToFormat)
	{
		if (DateUtil.isValid(dateToFormat))
		{
			String date = null;
			try
			{
				// Current support site date is in MM/dd/YYYY.
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				java.util.Date dd = sdf.parse(dateToFormat);

				// We will specify a new date format through the database
				SimpleDateFormat sdf1 = new SimpleDateFormat(com.debisys.dateformat.DateFormat.getDateFormat());
				date = sdf1.format(dd);

				// Since Java SimpleDateFormat does not know whether a datetime
				// contains
				// a time or not, remove the time (usually 12:00:00 AM) from the
				// end of the date ranges
				date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
			}
			catch (ParseException e)
			{
				Merchant.cat.error("Error during TransactionReport date localization ", e);
			}
			return date;
		}
		else
		{
			// Something isn't valid in the date string - don't try to localize
			// it
			return StringUtil.toString(dateToFormat);
		}
	}

	// End localization
	// TODO: change this function to static
	public void addMerchantNote(Long merchantId, String note, String logon)
	{
		Connection dbConn = null;
		CallableStatement cst = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new Exception();
			}

			String strSQL = Merchant.sql_bundle.getString("addMerchantNote");
			Merchant.cat.debug(strSQL);
			cst = dbConn.prepareCall(strSQL);
			note = "Customer Note: " + note;
			cst.setLong(1, merchantId);
			cst.setNull(2, java.sql.Types.DATE);
			cst.setString(3, "SUP");
			cst.setString(4, note);
			cst.setString(5, logon);
			cst.execute();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during addMerchantNote", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, cst, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	// Nidhi Gulati DBSYS- 609
	// Method to get the city list for the Transaction Summary by City Report
	public static Vector getMerchantCityListReports(SessionData sessionData) throws CustomerException
	{
		Vector vecMerchantList = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchantCityList");
			String strSQLWhere = "";
			// cat.debug("Access Level acquired: " + strAccessLevel);
			String hdr = "Access level matches ";
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					// cat.debug(hdr+"ISO DIST_CHAIN_3_LEVEL");
					strSQLWhere = strSQLWhere + " and r.iso_id = " + strRefId;
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					// cat.debug(hdr+"ISO DIST_CHAIN_5_LEVEL");
					strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
							+ "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
							+ " and iso_id in " + "(select rep_id from reps with (nolock) where type="
							+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				// cat.debug(hdr+ "AGENT");
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
						+ " and iso_id = " + strRefId + ")) ";
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				// cat.debug(hdr+"SUBAGENT");
				strSQLWhere = " and r.rep_id IN " + "(select rep_id from reps with (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				// cat.debug(hdr+"REP");
				strSQLWhere = " and r.rep_id = " + strRefId;
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				Merchant.cat.debug(hdr + "MERCHANT");
				strSQLWhere = " and m.merchant_id = " + strRefId;
			}

			strSQL = strSQL + strSQLWhere + " order by m.phys_city";
			Merchant.cat.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("phys_city"));
				vecMerchantList.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantCityList", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerchantList;
	}

	// End of DBSYS-609

	public int getMerchantClassification()
	{
		return this.merchantClassification;
	}

	public void setMerchantClassification(int merchantClassification)
	{
		this.merchantClassification = merchantClassification;
	}

	public String getMerchantClassificationOtherDesc()
	{
		return merchantClassificationOtherDesc;
	}

	public void setMerchantClassificationOtherDesc(String merchantClassificationOtherDesc)
	{
		this.merchantClassificationOtherDesc = merchantClassificationOtherDesc;
	}

	public String getControlNumber()
	{
		return this.controlNumber;
	}

	public void setControlNumber(String controlNumber)
	{
		this.controlNumber = controlNumber;
	}

	/**
	 * This returns a list of the payment types applied to merchant payments
	 * 
	 * @return Vector of payment types
	 * @throws CustomerException
	 */
	public static Vector getMerchantCreditPaymentTypes() throws CustomerException
	{
		Vector vecPaymentTypes = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getMerchPaymentTypes");
			pstmt = dbConn.prepareStatement(strSQL);

			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("id"));
				vecTemp.add(rs.getString("PaymentType"));
				vecPaymentTypes.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchPaymentTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecPaymentTypes;
	}

	public Vector getISOBanks(SessionData sessionData) throws CustomerException
	{
		Vector vecISOBanks = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strISOId = sessionData.getProperty("ref_id");
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getISOBanks");

			String sqlwhere = "where B.Status = 'True' and BI.ISO_id = "
					+ // replace
					// by
					// strISOId
					"(select rep_id from reps (nolock) where type = 4 and rep_id =("
					+ "select iso_id from reps (nolock) where type = 5 and rep_id =("
					+ "select iso_id from reps (nolock) where rep_id= " + this.repId + " )))";
			strSQL += sqlwhere;

			pstmt = dbConn.prepareStatement(strSQL);
			// pstmt.setLong(1, new Long(strISOId));
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("BankID"));
				vecTemp.add(rs.getString("BankCode"));
				vecISOBanks.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchPaymentTypes", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecISOBanks;
	}

	public Vector getDefaultBanks(SessionData sessionData) throws CustomerException
	{
		Vector vecISOBanks = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strISOId = sessionData.getProperty("ref_id");
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = "select B.ID as BankID, B.Code as BankCode, B.Name as BankName from Banks B (nolock)";
			String sqlwhere = " where Status= 1 ";
			strSQL += sqlwhere;

			pstmt = dbConn.prepareStatement(strSQL);
			// pstmt.setLong(1, new Long(strISOId));
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("BankID"));
				vecTemp.add(rs.getString("BankCode"));
				vecISOBanks.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getDefaultBanks", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecISOBanks;
	}

	public int getMerchantCreditPaymentType()
	{
		return this.merchantCreditPaymentType;
	}

	public void setMerchantCreditPaymentType(int merchantCreditPaymentType)
	{
		this.merchantCreditPaymentType = merchantCreditPaymentType;
	}

	public int getBank_id()
	{
		return this.bank_id;
	}

	public void setBank_id(int bank_id)
	{
		this.bank_id = bank_id;
	}

	/* BEGIN Release 25-26 - DBSY-589 - Added by LF */
	/**
	 * Returns a vector of merchants depending the specified actor level and ID
	 * 
	 * @param sAccessLevel
	 *            DebisysConstant of the access level
	 * @param sActorId
	 *            ID of the actor
	 * @param sChainLevel
	 *            DebisysConstant of the Chain level
	 * @return Vector of merchants
	 * @throws CustomerException
	 */
	public static Vector getMerchantsByActor(String sAccessLevel, String sActorId, String sChainLevel)
			throws CustomerException
	{
		Vector vResult = new Vector();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String sSQL = "SELECT m.merchant_id, m.dba FROM merchants m (NOLOCK) ";

			if (sAccessLevel.equals(DebisysConstants.REP))
			{
				sSQL += "INNER JOIN reps r (NOLOCK) ON m.rep_id = r.rep_id WHERE r.rep_id = " + sActorId
						+ " ORDER BY m.dba";
			}
			else if (sAccessLevel.equals(DebisysConstants.SUBAGENT)
					|| (sAccessLevel.equals(DebisysConstants.ISO) && sChainLevel
							.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
			{
				sSQL += "INNER JOIN reps r (NOLOCK) ON m.rep_id = r.rep_id WHERE r.rep_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id = "
						+ sActorId + ") ORDER BY m.dba";
			}
			else if (sAccessLevel.equals(DebisysConstants.AGENT))
			{
				sSQL += "INNER JOIN reps r (NOLOCK) ON m.rep_id = r.rep_id WHERE r.rep_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id IN"
						+ "(SELECT rep_id FROM reps (NOLOCK) WHERE iso_id = " + sActorId + ")) ORDER BY m.dba";
			}
			else if (sAccessLevel.equals(DebisysConstants.ISO)
					&& sChainLevel.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			{
				sSQL += "INNER JOIN reps r (NOLOCK) ON m.rep_id = r.rep_id WHERE r.rep_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id IN"
						+ "(SELECT rep_id FROM reps (NOLOCK) WHERE iso_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id = "
						+ sActorId + "))) ORDER BY m.dba";
			}

			pstmt = dbConn.prepareStatement(sSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("merchant_id"));
				vecTemp.add(rs.getString("dba"));
				vResult.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantsByActor", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vResult;
	}

	/**
	 * Returns a JSON vector wrapping the results of the getMerchantsByActor
	 * method
	 * 
	 * @param sData
	 *            String with AccessLevel and ID of the owning actor
	 * @return JSON vector to be used via Ajax invokation
	 * @throws CustomerException
	 */
	public static JSONObject getMerchantsByActorAjax(String sData, SessionData sessionData) throws CustomerException
	{
		JSONObject json = new JSONObject();

		try
		{
			Vector jsonVec = new Vector();
			Iterator it = Merchant.getMerchantsByActor(sData.split("_")[0], sData.split("_")[1], sData.split("_")[2])
					.iterator();

			while (it.hasNext())
			{
				Vector vTmp = (Vector) it.next();
				HashMap map = new HashMap();
				map.put("merchant_id", vTmp.get(0).toString());
				map.put("dba", URLEncoder.encode(vTmp.get(1).toString(), "UTF-8"));
				jsonVec.add(map);
			}

			json.put("items", jsonVec);
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantsByActorAjax", e);
			throw new CustomerException();
		}

		return json;
	}

	/* END Release 25-26 - DBSY-589 - Added by LF */

	/**
	 * @param reservedPINCacheBalance
	 *            the reservedPINCacheBalance to set
	 */
	public void setReservedPINCacheBalance(int reservedPINCacheBalance)
	{
		this.reservedPINCacheBalance = reservedPINCacheBalance;
	}

	/**
	 * @return the reservedPINCacheBalance
	 */
	public int getReservedPINCacheBalance()
	{
		return this.reservedPINCacheBalance;
	}

	/**
	 * @param pinCacheEnabled
	 *            the pinCacheEnabled to set
	 */
	public void setPinCacheEnabled(boolean pinCacheEnabled)
	{
		this.pinCacheEnabled = pinCacheEnabled;
	}

	/**
	 * @return the pinCacheEnabled
	 */
	public boolean isPinCacheEnabled()
	{
		return this.pinCacheEnabled;
	}

	public int getGlobalReservedPINCacheBalance() throws CustomerException
	{
		int result = 0;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getGlobalReservedPINCacheBalance");
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			if (rs.next())
			{
				result = rs.getInt("value");
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getGlobalReservedPINCacheBalance", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return result;
	}

	public void setMaximumSalesLimit(ServletRequest request)
	{
		String sDayName = new DateFormatSymbols(Locale.US).getWeekdays()[Calendar.getInstance().get(
				Calendar.DAY_OF_WEEK)];
		this.salesLimitLiabilityLimit = Double.parseDouble(request.getParameter("salesLimit" + sDayName));
	}

	public void updateSalesLimits(SessionData sessionData, ServletContext context) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String sql = null;
		LogChanges logChanges = null;
		String sNote = null;
		Merchant m2 = new Merchant(); // Used to retrieve the original values of
		// SalesLimits

		try
		{
			logChanges = new LogChanges(sessionData);
			logChanges.setContext(context);
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			m2.setMerchantId(this.merchantId);
			m2.getMerchant(sessionData, context);

			if (this.salesLimitType.equals("DAILY"))
			{
				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTYPE),
						this.salesLimitType, this.merchantId, ""))
				{
					sNote = Languages.getString("jsp.admin.saleslimit.noteChangeEnabled", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DBA_", this.dba);
					sNote = sNote.replaceAll("_MID_", this.merchantId);
					sNote = sNote.replaceAll("_TYPE_", Languages.getString("jsp.admin.saleslimit.salesLimitTypeDAILY",
							sessionData.getLanguage()));
					this.addMerchantNote(Long.parseLong(this.merchantId), sNote, sessionData.getProperty("username"));

					if (logChanges.was_changed(logChanges
							.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITLIABILITYLIMIT), String
							.valueOf(this.salesLimitLiabilityLimit), this.merchantId, ""))
					{
						sql = Merchant.sql_bundle.getString("updateSalesLimitLiabilityLimit");
						pstmt = dbConn.prepareStatement(sql);
						pstmt.setDouble(1, this.salesLimitLiabilityLimit);
						pstmt.setLong(2, Long.parseLong(this.merchantId));
						pstmt.executeUpdate();
						TorqueHelper.closeStatement(pstmt, null);

						sNote = Languages.getString("jsp.admin.saleslimit.noteChangeSalesLimit", sessionData
								.getLanguage());
						sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
								.getSalesLimitLiabilityLimit())));
						sNote = sNote.replaceAll("_NEW_", NumberUtil.formatAmount(Double
								.toString(this.salesLimitLiabilityLimit)));
						sNote = sNote.replaceAll("_DBA_", this.dba);
						sNote = sNote.replaceAll("_MID_", this.merchantId);
						this.addMerchantNote(Long.parseLong(this.merchantId), sNote, sessionData
								.getProperty("username"));
					}
				}

				sNote = "";
				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITMONDAY),
						String.valueOf(this.salesLimitMonday), this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitMonday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil
							.formatAmount(Double.toString(m2.getSalesLimitMonday())));
					sNote = sNote.replaceAll("_NEW_", NumberUtil.formatAmount(Double.toString(this.salesLimitMonday)));
					sNote += "\r\n";
				}

				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTUESDAY),
						String.valueOf(this.salesLimitTuesday), this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double
							.toString(m2.getSalesLimitTuesday())));
					sNote = sNote.replaceAll("_NEW_", NumberUtil.formatAmount(Double.toString(this.salesLimitTuesday)));
					sNote += "\r\n";
				}

				if (logChanges.was_changed(logChanges
						.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITWEDNESDAY), String
						.valueOf(this.salesLimitWednesday), this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
							.getSalesLimitWednesday())));
					sNote = sNote.replaceAll("_NEW_", NumberUtil
							.formatAmount(Double.toString(this.salesLimitWednesday)));
					sNote += "\r\n";
				}

				if (logChanges.was_changed(
						logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTHURSDAY), String
								.valueOf(this.salesLimitThursday), this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitThursday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
							.getSalesLimitThursday())));
					sNote = sNote
							.replaceAll("_NEW_", NumberUtil.formatAmount(Double.toString(this.salesLimitThursday)));
					sNote += "\r\n";
				}

				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITFRIDAY),
						String.valueOf(this.salesLimitFriday), this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitFriday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil
							.formatAmount(Double.toString(m2.getSalesLimitFriday())));
					sNote = sNote.replaceAll("_NEW_", NumberUtil.formatAmount(Double.toString(this.salesLimitFriday)));
					sNote += "\r\n";
				}

				if (logChanges.was_changed(
						logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSATURDAY), String
								.valueOf(this.salesLimitSaturday), this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
							.getSalesLimitSaturday())));
					sNote = sNote
							.replaceAll("_NEW_", NumberUtil.formatAmount(Double.toString(this.salesLimitSaturday)));
					sNote += "\r\n";
				}

				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSUNDAY),
						String.valueOf(this.salesLimitSunday), this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitSunday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil
							.formatAmount(Double.toString(m2.getSalesLimitSunday())));
					sNote = sNote.replaceAll("_NEW_", NumberUtil.formatAmount(Double.toString(this.salesLimitSunday)));
					sNote += "\r\n";
				}

				if (!sNote.equals(""))
				{
					sNote = Languages.getString("jsp.admin.saleslimit.noteChangeLimits", sessionData.getLanguage())
							.replaceAll("_DBA_", this.dba).replaceAll("_MID_", this.merchantId)
							+ "\r\n" + sNote;
					this.addMerchantNote(Long.parseLong(this.merchantId), sNote, sessionData.getProperty("username"));
				}

				sql = Merchant.sql_bundle.getString("updateSalesLimits");
				// This field was not set as parameter because I needed a mixed
				// value of long and the same database field
				sql = sql.replaceAll("_SALESRUNNING_", "SalesLimitRunningLiability");
				pstmt = dbConn.prepareStatement(sql);
				pstmt.setString(1, this.salesLimitType);
				pstmt.setDouble(2, this.salesLimitMonday);
				pstmt.setDouble(3, this.salesLimitTuesday);
				pstmt.setDouble(4, this.salesLimitWednesday);
				pstmt.setDouble(5, this.salesLimitThursday);
				pstmt.setDouble(6, this.salesLimitFriday);
				pstmt.setDouble(7, this.salesLimitSaturday);
				pstmt.setDouble(8, this.salesLimitSunday);
				pstmt.setLong(9, Long.parseLong(this.merchantId));
			}
			else
			{
				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTYPE),
						"OFF", this.merchantId, ""))
				{
					sNote = Languages.getString("jsp.admin.saleslimit.noteChangeDisabled", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DBA_", this.dba);
					sNote = sNote.replaceAll("_MID_", this.merchantId);
					this.addMerchantNote(Long.parseLong(this.merchantId), sNote, sessionData.getProperty("username"));

					if (logChanges.was_changed(logChanges
							.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITLIABILITYLIMIT), "0.0",
							this.merchantId, ""))
					{
						sql = Merchant.sql_bundle.getString("updateSalesLimitLiabilityLimit");
						pstmt = dbConn.prepareStatement(sql);
						pstmt.setDouble(1, 0);
						pstmt.setLong(2, Long.parseLong(this.merchantId));
						pstmt.executeUpdate();
						TorqueHelper.closeStatement(pstmt, null);

						sNote = Languages.getString("jsp.admin.saleslimit.noteChangeSalesLimit", sessionData
								.getLanguage());
						sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
								.getSalesLimitLiabilityLimit())));
						sNote = sNote.replaceAll("_NEW_", "0.00");
						sNote = sNote.replaceAll("_DBA_", this.dba);
						sNote = sNote.replaceAll("_MID_", this.merchantId);
						this.addMerchantNote(Long.parseLong(this.merchantId), sNote, sessionData
								.getProperty("username"));
					}
				}

				sNote = "";
				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITMONDAY),
						"0.0", this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitMonday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil
							.formatAmount(Double.toString(m2.getSalesLimitMonday())));
					sNote = sNote.replaceAll("_NEW_", "0.00");
					sNote += "\r\n";
				}

				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTUESDAY),
						"0.0", this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitTuesday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double
							.toString(m2.getSalesLimitTuesday())));
					sNote = sNote.replaceAll("_NEW_", "0.00");
					sNote += "\r\n";
				}

				if (logChanges
						.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITWEDNESDAY),
								"0.0", this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitWednesday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
							.getSalesLimitWednesday())));
					sNote = sNote.replaceAll("_NEW_", "0.00");
					sNote += "\r\n";
				}

				if (logChanges.was_changed(
						logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITTHURSDAY), "0.0",
						this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitThursday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
							.getSalesLimitThursday())));
					sNote = sNote.replaceAll("_NEW_", "0.00");
					sNote += "\r\n";
				}

				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITFRIDAY),
						"0.0", this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitFriday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil
							.formatAmount(Double.toString(m2.getSalesLimitFriday())));
					sNote = sNote.replaceAll("_NEW_", "0.00");
					sNote += "\r\n";
				}

				if (logChanges.was_changed(
						logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSATURDAY), "0.0",
						this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitSaturday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil.formatAmount(Double.toString(m2
							.getSalesLimitSaturday())));
					sNote = sNote.replaceAll("_NEW_", "0.00");
					sNote += "\r\n";
				}

				if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITSUNDAY),
						"0.0", this.merchantId, ""))
				{
					sNote += Languages.getString("jsp.admin.saleslimit.noteChangeLimitsDay", sessionData.getLanguage());
					sNote = sNote.replaceAll("_DAY_", Languages.getString("jsp.admin.saleslimit.salesLimitSunday",
							sessionData.getLanguage()));
					sNote = sNote.replaceAll("_OLD_", NumberUtil
							.formatAmount(Double.toString(m2.getSalesLimitSunday())));
					sNote = sNote.replaceAll("_NEW_", "0.00");
					sNote += "\r\n";
				}

				if (!sNote.equals(""))
				{
					sNote = Languages.getString("jsp.admin.saleslimit.noteChangeLimits", sessionData.getLanguage())
							.replaceAll("_DBA_", this.dba).replaceAll("_MID_", this.merchantId)
							+ "\r\n" + sNote;
					this.addMerchantNote(Long.parseLong(this.merchantId), sNote, sessionData.getProperty("username"));
				}

				sql = Merchant.sql_bundle.getString("updateSalesLimits");
				// This field was not set as parameter because I needed a mixed
				// value of long and the same database field
				sql = sql.replaceAll("_SALESRUNNING_", "0");
				pstmt = dbConn.prepareStatement(sql);
				pstmt.setNull(1, java.sql.Types.VARCHAR);
				pstmt.setDouble(2, 0);
				pstmt.setDouble(3, 0);
				pstmt.setDouble(4, 0);
				pstmt.setDouble(5, 0);
				pstmt.setDouble(6, 0);
				pstmt.setDouble(7, 0);
				pstmt.setDouble(8, 0);
				pstmt.setLong(9, Long.parseLong(this.merchantId));
				this.salesLimitType = null;
			}

			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateSalesLimits", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	public void grantTemporaryExtension(SessionData sessionData, ServletContext context, double dAmount)
			throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String sql = null;
		String sNote = null;
		LogChanges logChanges = null;
		Merchant m2 = new Merchant();

		try
		{
			logChanges = new LogChanges(sessionData);
			logChanges.setContext(context);
			m2.setMerchantId(this.merchantId);
			m2.getMerchant(sessionData, context);
			logChanges.set_value_for_check(logChanges
					.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITLIABILITYLIMIT), String.valueOf(m2
					.getSalesLimitLiabilityLimit()));
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			sql = Merchant.sql_bundle.getString("grantTemporaryExtension");
			pstmt = dbConn.prepareStatement(sql);
			pstmt.setDouble(1, dAmount);
			pstmt.setLong(2, Long.parseLong(this.merchantId));
			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);

			m2.getMerchant(sessionData, context);
			logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_SALESLIMITLIABILITYLIMIT),
					String.valueOf(m2.getSalesLimitLiabilityLimit()), this.merchantId, "EXTENSION");
			sNote = Languages.getString("jsp.admin.saleslimit.noteChangeLimitExtension", sessionData.getLanguage());
			sNote = sNote.replaceAll("_AMOUNT_", NumberUtil.formatAmount(Double.toString(dAmount)));
			sNote = sNote.replaceAll("_TOTAL_", NumberUtil.formatAmount(Double.toString(m2
					.getSalesLimitLiabilityLimit())));
			sNote = sNote.replaceAll("_DBA_", this.dba);
			sNote = sNote.replaceAll("_MID_", this.merchantId);
			this.addMerchantNote(Long.parseLong(this.merchantId), sNote, sessionData.getProperty("username"));
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during grantTemporaryExtension", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	public void setPrintBillingAddress(boolean printBillingAdress)
	{
		this.printBillingAddress = printBillingAdress;
	}

	public boolean getPrintBillingAddress()
	{
		return this.printBillingAddress;
	}

	/**
	 * @param timeZoneId
	 *            the timeZoneId to set
	 */
	public void setTimeZoneId(int timeZoneId)
	{
		this.timeZoneId = timeZoneId;
	}

	/**
	 * @return the timeZoneId
	 */
	public int getTimeZoneId()
	{
		return this.timeZoneId;
	}

	/**
	 * @param executiveAccountId
	 *            the executiveAccountId to set
	 */
	public void setExecutiveAccountId(int ExecutiveAccountId)
	{
		this.executiveAccountId = ExecutiveAccountId;
	}

	/**
	 * @return the executiveAccountId
	 */
	public int getExecutiveAccountId()
	{
		return this.executiveAccountId;
	}

	/**
	 * @param salesLimitType
	 *            the salesLimitType to set
	 */
	public void setSalesLimitType(String salesLimitType)
	{
		this.salesLimitType = salesLimitType;
	}

	/**
	 * @return the salesLimitType
	 */
	public String getSalesLimitType()
	{
		return this.salesLimitType;
	}

	/**
	 * @param salesLimitRunnningLiability
	 *            the salesLimitRunnningLiability to set
	 */
	public void setSalesLimitRunnningLiability(double salesLimitRunnningLiability)
	{
		this.salesLimitRunnningLiability = salesLimitRunnningLiability;
	}

	/**
	 * @return the salesLimitRunnningLiability
	 */
	public double getSalesLimitRunnningLiability()
	{
		return this.salesLimitRunnningLiability;
	}

	/**
	 * @param salesLimitLiabilityLimit
	 *            the salesLimitLiabilityLimit to set
	 */
	public void setSalesLimitLiabilityLimit(double salesLimitLiabilityLimit)
	{
		this.salesLimitLiabilityLimit = salesLimitLiabilityLimit;
	}

	/**
	 * @return the salesLimitLiabilityLimit
	 */
	public double getSalesLimitLiabilityLimit()
	{
		return this.salesLimitLiabilityLimit;
	}

	/**
	 * @param salesLimitMonday
	 *            the salesLimitMonday to set
	 */
	public void setSalesLimitMonday(double salesLimitMonday)
	{
		this.salesLimitMonday = salesLimitMonday;
	}

	/**
	 * @return the salesLimitMonday
	 */
	public double getSalesLimitMonday()
	{
		return this.salesLimitMonday;
	}

	/**
	 * @param salesLimitTuesday
	 *            the salesLimitTuesday to set
	 */
	public void setSalesLimitTuesday(double salesLimitTuesday)
	{
		this.salesLimitTuesday = salesLimitTuesday;
	}

	/**
	 * @return the salesLimitTuesday
	 */
	public double getSalesLimitTuesday()
	{
		return this.salesLimitTuesday;
	}

	/**
	 * @param salesLimitWednesday
	 *            the salesLimitWednesday to set
	 */
	public void setSalesLimitWednesday(double salesLimitWednesday)
	{
		this.salesLimitWednesday = salesLimitWednesday;
	}

	/**
	 * @return the salesLimitWednesday
	 */
	public double getSalesLimitWednesday()
	{
		return this.salesLimitWednesday;
	}

	/**
	 * @param salesLimitThursday
	 *            the salesLimitThursday to set
	 */
	public void setSalesLimitThursday(double salesLimitThursday)
	{
		this.salesLimitThursday = salesLimitThursday;
	}

	/**
	 * @return the salesLimitThursday
	 */
	public double getSalesLimitThursday()
	{
		return this.salesLimitThursday;
	}

	/**
	 * @param salesLimitFriday
	 *            the salesLimitFriday to set
	 */
	public void setSalesLimitFriday(double salesLimitFriday)
	{
		this.salesLimitFriday = salesLimitFriday;
	}

	/**
	 * @return the salesLimitFriday
	 */
	public double getSalesLimitFriday()
	{
		return this.salesLimitFriday;
	}

	/**
	 * @param salesLimitSaturday
	 *            the salesLimitSaturday to set
	 */
	public void setSalesLimitSaturday(double salesLimitSaturday)
	{
		this.salesLimitSaturday = salesLimitSaturday;
	}

	/**
	 * @return the salesLimitSaturday
	 */
	public double getSalesLimitSaturday()
	{
		return this.salesLimitSaturday;
	}

	/**
	 * @param salesLimitSunday
	 *            the salesLimitSunday to set
	 */
	public void setSalesLimitSunday(double salesLimitSunday)
	{
		this.salesLimitSunday = salesLimitSunday;
	}

	/**
	 * @return the salesLimitSunday
	 */
	public double getSalesLimitSunday()
	{
		return this.salesLimitSunday;
	}

	/**
	 * @param salesLimitEnable
	 *            the salesLimitEnable to set
	 */
	public void setSalesLimitEnable(String salesLimitEnable)
	{
		this.salesLimitEnable = salesLimitEnable;
	}

	/**
	 * @return the salesLimitEnable
	 */
	public String getSalesLimitEnable()
	{
		return this.salesLimitEnable;
	}

	public void setTerminalStatus(SessionData sessionData, ServletContext context, Long siteId, int status,
			String reason) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}
			int oldstatus = this.getTerminalStatus(siteId);
			if (oldstatus != status)
			{
				String strSQL = "UPDATE terminals SET Status = ?, StatusLastChange = GETDATE() WHERE millennium_no = ?";// Merchant.sql_bundle.getString("setTerminalStatus");
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.setInt(1, status);
				pstmt.setLong(2, siteId);
				pstmt.executeUpdate();
				pstmt.close();
				// log the update
				Merchant.cat.debug((status == 1 ? "Enabling " : "Disabling ") + "siteID:" + siteId);
				String terminalEnabledDisabled = "";
				if (status == 1)
				{
					terminalEnabledDisabled = "[Terminal Enabled] ";
				}
				else
				{
					terminalEnabledDisabled = "[Terminal Disabled] ";
				}
				LogChanges logChanges = new LogChanges(sessionData);
				logChanges.setContext(context);
				logChanges.log_changed(logChanges.getLogChangeIdByName("Terminal Status"), siteId.toString(), String
						.valueOf(status), String.valueOf(oldstatus), terminalEnabledDisabled + reason);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during setTerminalStatus", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
	}

	private int getTerminalStatus(Long siteId)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int status = 0;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}
			String strSQL = "SELECT Status FROM terminals WHERE millennium_no = ?";// Merchant.sql_bundle.getString("setTerminalStatus");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, siteId);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				status = rs.getInt("Status");
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalStatus", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return status;
	}

	/**
	 * @param useRatePlanModel
	 *            the useRatePlanModel to set
	 */
	public void setUseRatePlanModel(boolean useRatePlanModel)
	{
		this.useRatePlanModel = useRatePlanModel;
	}

	/**
	 * @return the useRatePlanModel
	 */
	public boolean isUseRatePlanModel()
	{
		return this.useRatePlanModel;
	}

	/**
	 * @param ratePlanId
	 * @param merchantID
	 * @return
	 */
	public static ArrayList<String> getRatePlansDetailByMerchantId(Integer ratePlanId, String merchantID)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<String> plans = new ArrayList<String>();
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getRatePlansDetailByMerchantId");

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, merchantID);
			pstmt.setInt(2, ratePlanId);
			rs = pstmt.executeQuery();

			Merchant.cat.info("getRatePlansDetailByMerchantId SQL: [" + strSQL + "] ratePlanId:[" + ratePlanId
					+ "] merchantID:[" + merchantID + "]");

			while (rs.next())
			{
				plans.add(rs.getString(1) + "#" + rs.getString(2) + "#" + rs.getString(3) + "#" + rs.getString(4) + "#"
						+ rs.getString(5) + "#" + rs.getString(6) + "#" + rs.getString(7) + "#" + rs.getString(8));
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getRatePlansDetailByMerchantId", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return plans;
	}
        
        public static ArrayList<String> getRatePlansByRepsVector(Vector vecRepList){
            String strRepList = "";
            int count = 0;
            for (Object vecObj : vecRepList) {
                Vector obj = (Vector)vecObj;
                if(count == 0){
                    strRepList += ""+obj.get(0);
                }
                else{
                    strRepList += ","+obj.get(0);
                }
                count++;
            }
            return getRatePlansByRepList(strRepList);
        }
        
        public static ArrayList<String> getRatePlansByRepId(Long repId)
	{
            return getRatePlansByRepList(String.valueOf(repId));
        }
        
	/**
	 * Get the Rate plans assigned in the new Rate Plan model.
	 * 
	 * @param merchantId
	 * @return
	 */
	public static ArrayList<String> getRatePlansByRepList(String repIds)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<String> plans = new ArrayList<String>();
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}
			String strSQL = Merchant.sql_bundle.getString("getRatePlansByMerchantId");
                        
                        strSQL = strSQL.replace("REP_ID_LIST", repIds);

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			Merchant.cat.info("Find ISO Managed Rate Plans for repId " + repIds + " SQL: " + strSQL);
			byte isNotTemplate = 0;
			while (rs.next())
			{
				if (rs.getByte(4) == isNotTemplate)
				{
					plans.add(rs.getString(1) + "#" + rs.getString(2) + "#" + rs.getString(3) + " #");
				}
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getRatePlansByMerchantId", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return plans;
	}
        
    public static String getRatePlansByRepIds(String repIds) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String response = "";
        try {
            dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Merchant.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Merchant.sql_bundle.getString("getRatePlansByReps");
            strSQL = strSQL.replace("REP_ID_LIST", repIds);

            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            Merchant.cat.info("Find ISO Managed Rate Plans (getRatePlansByRepIds) for repId " + repIds + " SQL: " + strSQL);
            byte isNotTemplate = 0;
            while (rs.next()) {
                if (rs.getByte(4) == isNotTemplate) {
                    response = response + rs.getString(1) + "#" + rs.getString(2) + "#" + rs.getString(3) + "|";
                }
            }
        } catch (Exception e) {
            Merchant.cat.error("Error during getRatePlansByRepIds", e);
        } finally {
            try {
                DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
            } catch (Exception e) {
                Merchant.cat.error("Error during closeConnection", e);
            }
        }
        return response;
    }
    
    public static String getRatePlansByMerchants(String merchantsId) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String response = "";
        try {
            dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Merchant.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = "SELECT distinct(a.iso_rate_plan_id), b.name, b.description ,b.IsTemplate " +
                "FROM dbo.rep_iso_rate_plan_glue a with(nolock) " +
                "INNER JOIN rateplan b with(nolock) on a.iso_rate_plan_id=b.RatePlanID " +
                "INNER JOIN terminals t with(nolock) on t.rateplanId = b.RatePlanID " +
                "INNER JOIN merchants m with(nolock) on m.merchant_id=t.merchant_id " +
                "WHERE m.merchant_id IN (MERCHANT_LIST) and a.isNewRatePlanModel=1 ";

            strSQL = strSQL.replace("MERCHANT_LIST", merchantsId);
            Merchant.cat.info("Find ISO Managed Rate Plans (getRatePlansByMerchants) for merchants " + merchantsId + " SQL: " + strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            
            byte isNotTemplate = 0;
            while (rs.next()) {
                if (rs.getByte(4) == isNotTemplate) {
                    response = response + rs.getString(1) + "#" + rs.getString(2) + "#" + rs.getString(3) + "|";
                }
            }
        } catch (Exception e) {
            Merchant.cat.error("Error during getRatePlansByMerchants", e);
        } finally {
            try {
                DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
            } catch (Exception e) {
                Merchant.cat.error("Error during closeConnection", e);
            }
        }
        return response;
    }

	/**
	 * Find the terminals by merchant ID.
	 * 
	 * @param merchantId
	 * @return
	 */
	public static ArrayList<String> getTerminalsByMerchantID(Long merchantId)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<String> terminals = new ArrayList<String>();
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = Merchant.sql_bundle.getString("getTerminalsByMerchantID");

			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, merchantId);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				terminals.add(rs.getString(1) + "#" + rs.getString(2));
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalsWithOutRatePlansByMerchantIdAndModel", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return terminals;
	}

	/**
	 * @param warningChangeModel
	 *            the warningChangeModel to set
	 */
	public void setWarningChangeModel(String warningChangeModel)
	{
		this.warningChangeModel = warningChangeModel;
	}

	/**
	 * @return the warningChangeModel
	 */
	public String getWarningChangeModel()
	{
		return this.warningChangeModel;
	}

	/**
	 * 2010-09-13 FB Ticket 5545-10055275
	 * 
	 * @param context
	 *            Used to know what deployment is being used
	 * @param phoneToFormat
	 *            The phone to be formated
	 * @return If domestic, the phone formatted as specified in the phoneFormat
	 *         DB property
	 */
	public static String formatPhone(ServletContext context, String phoneToFormat)
	{
		String retValue = "";
		if ((DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				&& (DebisysConfigListener.getCustomConfigType(context)
						.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
		{

			String tmpString = phoneToFormat.replaceAll(Merchant.PHONE_CLEAN_REGEXP, "");
			String formatString = DebisysConfigListener.getPhoneFormat(context);
			if (tmpString.length() == 10 && formatString.length() > 4)
			{
				retValue = String.format(formatString, tmpString.substring(0, 3), tmpString.substring(3, 6), tmpString
						.substring(6, 10));
			}

			else
			{
				retValue = tmpString;
			}
		}
		else
		{
			retValue = phoneToFormat;
		}
		return retValue;
	}

	/**
	 * @param terminals
	 * @param merchantId
	 * @param typModel
	 *            1 = New model , 0 Old model
	 */
	public static void updateRatePlanModelAndTerminals(ArrayList<String> terminals, String merchantId,
			String typeModel, String userName, SessionData sessionData)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		StringBuilder notesMerchant = new StringBuilder();
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}
			dbConn.setAutoCommit(false);
			String strSQL = ""; // Merchant.sql_bundle.getString("getTerminalsByMerchantID");

			// jsp.admin.customers.add_merchant_notes.customerNote=Nota Cliente:
			// jsp.admin.customers.add_merchant_notes.customerNote1=El comercio
			// fue cambiado a
			// jsp.admin.customers.add_merchant_notes.customerNote1=Plan
			// asignado
			Languages.getString("jsp.admin.customers.add_merchant_notes.customerNote1", sessionData.getLanguage());

			if (typeModel.equals("1"))
			{
				strSQL = "update merchants set UseRatePlanModel=1 where merchant_id=" + merchantId;
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.executeUpdate();
				pstmt.close();
				notesMerchant.append("customer note "
						+ Languages.getString("jsp.admin.customers.add_merchant_notes.customerNote1", sessionData
								.getLanguage())
						+ " "
						+ Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelISO", sessionData
								.getLanguage()) + ", ");
			}
			else
			{
				strSQL = "update merchants set UseRatePlanModel=0 where merchant_id=" + merchantId;
				pstmt = dbConn.prepareStatement(strSQL);
				pstmt.executeUpdate();
				pstmt.close();
				notesMerchant.append("customer note "
						+ Languages.getString("jsp.admin.customers.add_merchant_notes.customerNote2", sessionData
								.getLanguage())
						+ " "
						+ Languages.getString("jsp.admin.customers.merchants_info.useRatePlanModelEmida", sessionData
								.getLanguage()) + ", ");

				strSQL = "update terminals set rateplanid=null where merchant_Id=" + merchantId;
				pstmt = dbConn.prepareStatement(strSQL);
				int numTerminalUpdates = pstmt.executeUpdate();
				pstmt.close();
				notesMerchant.append(numTerminalUpdates
						+ " "
						+ Languages.getString("jsp.admin.customers.add_merchant_notes.customerNote3", sessionData
								.getLanguage()) + ", ");
			}

			if (terminals.size() > 0)
			{
				for (String sites : terminals)
				{
					String[] sitesInfo = sites.split("\\#");
					strSQL = "update terminals set rateplanId=" + sitesInfo[0] + " where millennium_no=" + sitesInfo[1];
					pstmt = dbConn.prepareStatement(strSQL);
					pstmt.executeUpdate();
					pstmt.close();
					notesMerchant.append(sitesInfo[1]
							+ " "
							+ Languages.getString("jsp.admin.customers.add_merchant_notes.customerNote2", sessionData
									.getLanguage()) + ": [" + sitesInfo[0] + "], ");
				}
			}

			strSQL = Merchant.sql_bundle.getString("addMerchantNote");
			Merchant.cat.debug(strSQL);
			pstmt = dbConn.prepareCall(strSQL);
			pstmt.setLong(1, Long.parseLong(merchantId));
			pstmt.setNull(2, java.sql.Types.DATE);
			pstmt.setString(3, "SUP");
			pstmt.setString(4, notesMerchant.toString());
			pstmt.setString(5, userName);
			pstmt.execute();

			pstmt.close();
			dbConn.commit();
			dbConn.setAutoCommit(true);
		}
		catch (SQLException e)
		{
			try
			{
				if (dbConn != null)
				{
					dbConn.rollback();
				}
			}
			catch (Exception ex)
			{
				Merchant.cat.error("Error during rollback updateRatePlanModelAndTerminals ", ex);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during updateRatePlanModelAndTerminals", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection updateRatePlanModelAndTerminals ", e);
			}
		}

	}

	/* R31.1 Billing ABernal - Emunoz */

	public Vector merchantRegulatoryFees(SessionData sessionData, ServletContext context) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;

		Vector vecMerRegulaFee = new Vector();
		ResultSet rs = null;
		Vector vecCounts = new Vector();

		int intRecordCount = 0;
		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQLWhere = "";

			// begin actual query for results
			String strSQL;

			strSQL = "SELECT regulatory_fee_id as id, name FROM MerchantRegulatoryFees ORDER BY id";

			pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("id")));
				vecTemp.add(StringUtil.toString(rs.getString("name")));
				vecMerRegulaFee.add(vecTemp);
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during merchantRegulatoryFees", e);
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}

		return vecMerRegulaFee;
	}

	public String getProcessDefaultValueId(ServletContext context)
	{
		String processDefaultId = DebisysConfigListener.getProcessDefaultValueId(context);
		return processDefaultId;
	}

	/* End R31.1 Billing ABernal - Emunoz */

	/* DBSY-1072 eAccounts Interface */
	public int getEntityAccountType()
	{
		return this.entityAccountType;
	}

	public void setEntityAccountType(int _entityAccountType)
	{
		this.entityAccountType = _entityAccountType;
	}

	/* END DBSY-1072 eAccounts Interface */

	/* DBSY-887 Merchant Report Dashboard */
	/**
	 * Given an merchant_id, populates merchant object populated from the
	 * database, or null if no such merchant_id exists. database, or null if no
	 * such merchant_id exists ONLY MERCHANT.
	 * 
	 * @throws CustomerException
	 *             Thrown on database errors
	 */
	public Vector getMerchantDashBoard(SessionData sessionData, ServletContext context, String merchantID)
			throws CustomerException
	{
		/* if (checkPermission(sessionData)) { */
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector merchantDashBoard = new Vector();

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = " select m.merchant_id, m.dba,r.rep_id, r.businessname,m.merchant_type,mt.description, "
					+ " m.SalesLimitType, "
					+ " CASE m.SalesLimitType WHEN 'DAILY' THEN SalesLimitType ELSE 'NA' END AS Merchant_Credit_LIMIT, "
					+ " m.SalesLimitMonday,m.SalesLimitTuesday,m.SalesLimitWednesday,m.SalesLimitThursday,m.SalesLimitFriday,m.SalesLimitSaturday,m.SalesLimitSunday,"
					+ " m.LiabilityLimit, " // as credit_limit_amount,"
					+ "m.SalesLimitLiabilityLimit,"
					+ "m.SalesLimitRunningLiability, "
					+ "CASE SalesLimitLiabilityLimit WHEN NULL THEN 'NA' ELSE (SalesLimitLiabilityLimit- SalesLimitRunningLiability)	END as Available_Daily_Credit, "
					+ "m.LiabilityLimit- m.RunningLiability as Current_Available_Credit, m.legal_businessname "
					+ "from  merchants m WITH(NOLOCK) " + "INNER JOIN reps r WITH(NOLOCK) ON m.rep_id = r.rep_id "
					+ "INNER JOIN Merchant_types mt WITH(NOLOCK) ON mt.id = m.merchant_type"
					+ " WHERE m.merchant_id = " + merchantID;

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			if (rs.next())
			{
				merchantDashBoard.add(0, rs.getString("merchant_id"));
				merchantDashBoard.add(1, rs.getString("dba"));
				merchantDashBoard.add(2, rs.getString("rep_id"));
				merchantDashBoard.add(3, rs.getString("businessname"));
				merchantDashBoard.add(4, rs.getString("merchant_type"));
				merchantDashBoard.add(5, rs.getString("description"));
				merchantDashBoard.add(6, rs.getString("SalesLimitType"));
				merchantDashBoard.add(7, rs.getString("Merchant_Credit_LIMIT"));
				merchantDashBoard.add(8, Double.toString(Double.parseDouble(rs.getString("SalesLimitMonday"))));
				merchantDashBoard.add(9, Double.toString(Double.parseDouble(rs.getString("SalesLimitTuesday"))));
				merchantDashBoard.add(10, Double.toString(Double.parseDouble(rs.getString("salesLimitWednesday"))));
				merchantDashBoard.add(11, Double.toString(Double.parseDouble(rs.getString("salesLimitThursday"))));
				merchantDashBoard.add(12, Double.toString(Double.parseDouble(rs.getString("salesLimitFriday"))));
				merchantDashBoard.add(13, Double.toString(Double.parseDouble(rs.getString("salesLimitSaturday"))));
				merchantDashBoard.add(14, Double.toString(Double.parseDouble(rs.getString("salesLimitSunday"))));
				merchantDashBoard.add(15, Double.toString(Double.parseDouble(rs.getString("LiabilityLimit"))));
				merchantDashBoard
						.add(16, Double.toString(Double.parseDouble(rs.getString("salesLimitLiabilityLimit"))));
				merchantDashBoard.add(17, Double.toString(Double
						.parseDouble(rs.getString("SalesLimitRunningLiability"))));
				merchantDashBoard.add(18, Double.toString(Double.parseDouble(rs.getString("Available_Daily_Credit"))));
				merchantDashBoard
						.add(19, Double.toString(Double.parseDouble(rs.getString("Current_Available_Credit"))));
				merchantDashBoard.add(20, rs.getString("legal_businessname"));

			}
			else
			{
				this.addFieldError("error", "Merchant not found.");
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantDashBoard", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return merchantDashBoard;
		/*
		 * } else { addFieldError("error", "Permission Denied."); }
		 */
	}

	public Vector getMerchantDashBoardNSFCount(SessionData sessionData, ServletContext context, String merchantID,
			int days) throws CustomerException
	{
		/* if (checkPermission(sessionData)) { */
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector merchantDashBoardNSFCount = new Vector();

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = " SELECT DISTINCT b.entityid,  r.return_date AS Return_Date, ac.reason AS Reason, m.dba AS company_name, r.amount AS nsfamount, "
					+ " pbe.description AS Description, r.ach_return_id ,b.achfiletracenumber "
					+ " FROM ACHReturns AS r WITH (NOLOCK) "
					+ " INNER JOIN billing AS b WITH (NOLOCK) ON r.rec_id = b.achfiletracenumber "
					+ " LEFT JOIN ACHReturnCodes AS ac WITH (NOLOCK) ON (ac.code = r.return_code) "
					+ " inner join paymentBatchEntity pbe WITH (NOLOCK) on b.paymentBatchEntityId = pbe.paymentBatchEntityId "
					+ " INNER JOIN MERCHANTS AS m WITH(NOLOCK) ON m.merchant_id = b.entityid "
					+ " INNER JOIN ACHReturnCodes AS acrc WITH(NOLOCK) ON acrc.code = r.return_code "
					+ " WHERE b.entityTypeId=8 AND b.entityid = "
					+ merchantID
					+ " AND (r.return_date <= CONVERT(nvarchar(10), GETDATE(), 126)  and r.return_date > CONVERT(nvarchar(10), GETDATE()  -"
					+ days
					+ ", 126) ) "
					+ " AND acrc.code not in ('C01','C02','C03','C04','C05','C06','C07','C10','C13') ";

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			double nsfamount = 0.0;
			long nsfcount = 0;
			String entityId = "";

			while (rs.next())
			{
				entityId = rs.getString("entityid");
				nsfcount += 1;
				nsfamount += Double.parseDouble(rs.getString("nsfamount"));
			}

			merchantDashBoardNSFCount.add(0, entityId);
			merchantDashBoardNSFCount.add(1, nsfcount);
			merchantDashBoardNSFCount.add(2, nsfamount);
			merchantDashBoardNSFCount.add(3, 0);

		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantDashBoardNSFCount", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return merchantDashBoardNSFCount;
		/*
		 * } else { addFieldError("error", "Permission Denied."); }
		 */
	}

	public Vector getMerchantDashBoardNSFCountReg(SessionData sessionData, ServletContext context, String merchantID,
			int days) throws CustomerException
	{
		/* if (checkPermission(sessionData)) { */
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector merchantDashBoardNSFCountReg = new Vector();

		TransactionReport tr = new TransactionReport();

		// tr.setStartDate(strValue);
		// tr.setEndDate(strValue);

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = " SELECT DISTINCT r.ach_return_id,b.entityid,  r.return_date AS Return_Date, ac.reason AS Reason, m.dba AS company_name, r.amount AS commisionamount, pbe.description AS action "
					+ " FROM ACHReturns AS r WITH (NOLOCK) INNER JOIN billing AS b WITH (NOLOCK) ON r.rec_id = b.achfiletracenumber LEFT JOIN ACHReturnCodes AS ac WITH (NOLOCK) ON (ac.code = r.return_code) "
					+ " inner join paymentBatchEntity pbe WITH (NOLOCK) on b.paymentBatchEntityId = pbe.paymentBatchEntityId "
					+ " INNER JOIN MERCHANTS AS m WITH(NOLOCK) ON m.merchant_id = b.entityid "
					+ " INNER JOIN ACHReturnCodes AS acrc WITH(NOLOCK) ON acrc.code = r.return_code"
					+ " WHERE b.entityTypeId=8 AND b.entityid ="
					+ merchantID
					+ " AND (r.return_date <= CONVERT(nvarchar(10), GETDATE(), 126)  and r.return_date > CONVERT(nvarchar(10), GETDATE() -"
					+ days
					+ ", 126) ) "
					+ " AND acrc.code not in ('C01','C02','C03','C04','C05','C06','C07','C10','C13') ";

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(0, rs.getString("ach_return_id"));
				vecTemp.add(1, rs.getString("entityid"));
				vecTemp.add(2, rs.getDate("return_date"));
				vecTemp.add(3, rs.getString("reason"));
				vecTemp.add(4, rs.getString("company_name"));
				vecTemp.add(5, rs.getString("commisionamount"));
				vecTemp.add(6, rs.getString("action"));
				merchantDashBoardNSFCountReg.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantDashBoardNSFCountReg", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return merchantDashBoardNSFCountReg;
		/*
		 * } else { addFieldError("error", "Permission Denied."); }
		 */
	}

	public Vector getMerchantDashBoardNSFSalesVolume30(SessionData sessionData, ServletContext context,
			String merchantID, int days) throws CustomerException
	{
		/* if (checkPermission(sessionData)) { */
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector merchantDashBoardNSFSalesVolume30 = new Vector();

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			String strSQL = " SELECT distinct r.ach_return_id,b.entityid,r.return_date,'' as reason, b.dba as company_name, b.commisionamount "
					+ " FROM ACHReturns AS r WITH(NOLOCK) "
					+ " INNER JOIN Billing b WITH (NOLOCK) ON r.rec_id = b.achfiletracenumber AND b.entityTypeId = 8"
					+ " INNER JOIN ACHReturnCodes AS acrc WITH(NOLOCK) ON acrc.code = r.return_code "
					+ " INNER JOIN MERCHANTS AS m WITH(NOLOCK) ON m.merchant_id = b.entityid"
					+ " WHERE b.entityid = "
					+ merchantID
					+ " AND (r.return_date <= CONVERT(nvarchar(10), GETDATE(), 126) "
					+ " and r.return_date > CONVERT(nvarchar(10), GETDATE() -"
					+ days
					+ ", 126)   )  "
					+ " AND acrc.code not in ('C01','C02','C03','C04','C05','C06','C07','C10','C13') ";

			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(0, rs.getString("ach_return_id"));
				vecTemp.add(1, rs.getString("entityid"));
				vecTemp.add(2, rs.getDate("return_date"));
				vecTemp.add(3, rs.getString("reason"));
				vecTemp.add(4, rs.getString("company_name"));
				vecTemp.add(5, rs.getString("commisionamount"));
				merchantDashBoardNSFSalesVolume30.add(vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getMerchantDashBoardNSFSalesVolume30", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return merchantDashBoardNSFSalesVolume30;
		/*
		 * } else { addFieldError("error", "Permission Denied."); }
		 */
	}

	/* DBSY-887 Merchant Report Dashboard */

	public Hashtable<String, Vector<Integer>> getTerminalOperationsTime() throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String sql = null;
		ResultSet rs = null;
		Hashtable<String, Vector<Integer>> htResult = new Hashtable<String, Vector<Integer>>();
                if (this.merchantId == null) {
                    return htResult;
                }

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			sql = Merchant.sql_bundle.getString("getTerminalOperationsTime");
			pstmt = dbConn.prepareStatement(sql);
			pstmt.setLong(1, Long.parseLong(this.merchantId));
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector<Integer> vecTemp = new Vector<Integer>();
				vecTemp.add(rs.getInt("Enabled"));
				vecTemp.add(rs.getInt("StartHour"));
				vecTemp.add(rs.getInt("StartMinute"));
				vecTemp.add(rs.getInt("EndHour"));
				vecTemp.add(rs.getInt("EndMinute"));
				htResult.put(rs.getString("DayName"), vecTemp);
			}

			if (htResult.size() == 0)
			{
				Vector<Integer> vecTemp = new Vector<Integer>();
				vecTemp.add(0);
				vecTemp.add(0);
				vecTemp.add(0);
				vecTemp.add(0);
				vecTemp.add(0);
				htResult.put("MO", vecTemp);
				htResult.put("TU", vecTemp);
				htResult.put("WE", vecTemp);
				htResult.put("TH", vecTemp);
				htResult.put("FR", vecTemp);
				htResult.put("SA", vecTemp);
				htResult.put("SU", vecTemp);
			}
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getTerminalOperationsTime", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return htResult;
	}

	public void saveTerminalOperationsTime(String sDay, boolean bEnabled, int nStartHour, int nStartMin, int nEndHour,
			int nEndMin) throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String sql = null;

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			sql = Merchant.sql_bundle.getString("saveTerminalOperationsTime");
			pstmt = dbConn.prepareStatement(sql);
			pstmt.setLong(1, Long.parseLong(this.merchantId));
			pstmt.setString(2, sDay);
			pstmt.setLong(3, Long.parseLong(this.merchantId));
			pstmt.setString(4, sDay);
			pstmt.setBoolean(5, bEnabled);
			pstmt.setInt(6, nStartHour);
			pstmt.setInt(7, nStartMin);
			pstmt.setInt(8, nEndHour);
			pstmt.setInt(9, nEndMin);
			pstmt.executeUpdate();
			TorqueHelper.closeStatement(pstmt, null);
		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during saveTerminalOperationsTime", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during release connection", e);
			}
		}
	}

	public static Vector getPaymentTypesForInvoiceGeneration() throws CustomerException
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		String sql = null;
		ResultSet rs = null;
		Vector htResult = new Vector();

		try
		{
			dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));

			if (dbConn == null)
			{
				Merchant.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			sql = Merchant.sql_bundle.getString("getPaymentTypesForInvoiceGeneration");
			pstmt = dbConn.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				if (rs.getInt("status") == 1)
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(rs.getString("id"));
					vecTemp.add(rs.getString("code"));
					vecTemp.add(rs.getString("description"));
					htResult.add(vecTemp);
				}
			}

		}
		catch (Exception e)
		{
			Merchant.cat.error("Error during getPaymentTypesForInvoiceGeneration", e);
			throw new CustomerException();
		}
		finally
		{
			try
			{
				DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				Merchant.cat.error("Error during closeConnection", e);
			}
		}
		return htResult;
	}

	/**
	 * @param addressValidationStatus
	 *            the addressValidationStatus to set
	 */
	public void setAddressValidationStatus(int addressValidationStatus)
	{
		this.addressValidationStatus = addressValidationStatus;
	}

	/**
	 * @return the addressValidationStatus
	 */
	public int getAddressValidationStatus()
	{
		return this.addressValidationStatus;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude()
	{
		return this.latitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude()
	{
		return this.longitude;
	}

	/**
	 * @param paymentTypeForInvoiceGeneration
	 *            the paymentTypeForInvoiceGeneration to set
	 */
	public void setPaymentTypeForInvoiceGeneration(String paymentTypeForInvoiceGeneration)
	{
		this.paymentTypeForInvoiceGeneration = paymentTypeForInvoiceGeneration;
	}

	/**
	 * @return the paymentTypeForInvoiceGeneration
	 */
	public String getPaymentTypeForInvoiceGeneration()
	{
		if (paymentTypeForInvoiceGeneration != null && paymentTypeForInvoiceGeneration.contains("-"))
		{
			String[] arrTemp = paymentTypeForInvoiceGeneration.split("-");
			this.paymentTypeForInvoiceGeneration = arrTemp[0];
		}
		else if (paymentTypeForInvoiceGeneration == null)
		{
			this.paymentTypeForInvoiceGeneration = "";
		}
		return this.paymentTypeForInvoiceGeneration;
	}

	/**
	 * @param accountNumberForInvoiceGeneration
	 *            the accountNumberForInvoiceGeneration to set
	 */
	public void setAccountNumberForInvoiceGeneration(String accountNumberForInvoiceGeneration)
	{
		this.accountNumberForInvoiceGeneration = accountNumberForInvoiceGeneration;
	}

	/**
	 * @return the accountNumberForInvoiceGeneration
	 */
	public String getAccountNumberForInvoiceGeneration()
	{
		return this.accountNumberForInvoiceGeneration;
	}

	// DTU-369 Payment Notifications
	public static boolean GetPaymentNotification(String merchantID)
	{
		boolean type = false;
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}

			PreparedStatement pstmt = dbConn
					.prepareStatement("SELECT payment_notifications FROM merchants WITH (NOLOCK) WHERE merchant_id = ?");
			pstmt.setString(1, merchantID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
			{
				type = rs.getBoolean("payment_notifications");
			}

			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("Error during GetPaymentNotification", e);
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
		return type;
	}

	public static int GetPaymentNotificationType(String merchantID)
	{
		int type = 0;
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}

			PreparedStatement pstmt = dbConn
					.prepareStatement("SELECT payment_notification_type FROM merchants WITH (NOLOCK) WHERE merchant_id = ?");
			pstmt.setString(1, merchantID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
			{
				type = rs.getInt("payment_notification_type");
			}

			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("Error during GetPaymentNotificationType", e);
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
		return type;
	}

	public static String GetSmsNotificationPhone(String merchantID)
	{
		String type = "";
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}

			PreparedStatement pstmt = dbConn
					.prepareStatement("SELECT sms_notification_phone FROM merchants WITH (NOLOCK) WHERE merchant_id = ?");
			pstmt.setString(1, merchantID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
			{
				type = rs.getString("sms_notification_phone");
			}

			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("Error during GetSmsNotificationPhone", e);
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
		return type;
	}

	public static String GetMailNotificationAddress(String merchantID)
	{
		String type = "";
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new Exception();
			}

			PreparedStatement pstmt = dbConn
					.prepareStatement("SELECT mail_notification_address FROM merchants WITH (NOLOCK) WHERE merchant_id = ?");
			pstmt.setString(1, merchantID);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next())
			{
				type = rs.getString("mail_notification_address");
			}

			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("Error during GetMailNotificationAddress", e);
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
		return type;
	}

	/**
	 * @param merchantOwnerId
	 *            the merchantOwnerId to set
	 */
	public void setMerchantOwnerId(String merchantOwnerId)
	{
		this.merchantOwnerId = merchantOwnerId;
	}

	/**
	 * @return the merchantOwnerId
	 */
	public String getMerchantOwnerId()
	{
		return merchantOwnerId;
	}

	/**
	 * @return the invoicing
	 */
	public boolean isInvoicing()
	{
		return invoicing;
	}

	/**
	 * @param invoicing
	 *            the invoicing to set
	 */
	public void setInvoicing(boolean invoicing)
	{
		this.invoicing = invoicing;
	}
	
		public int getMerchantExternalOutsideHierarchy()
	{
		return merchantExternalOutsideHierarchy;
	}

	public void setMerchantExternalOutsideHierarchy(int merchantExternalOutsideHierarchy)
	{
		this.merchantExternalOutsideHierarchy = merchantExternalOutsideHierarchy;
	}

	public void updateMerchantExternalOutsideHierarchy(boolean addFlag, int flag)
	{
		if (addFlag)
		{ // Add the flag
			this.merchantExternalOutsideHierarchy |= flag;
		}
		else
		{// Removes the flag
			this.merchantExternalOutsideHierarchy &= ~flag;
		}
	}

	/**
	 * @return the strUrlLocation
	 */
	public String getStrUrlLocation() {
		return strUrlLocation;
	}

	/**
	 * @param strUrlLocation the strUrlLocation to set
	 */
	public void setStrUrlLocation(String strUrlLocation) {
		this.strUrlLocation = strUrlLocation;
	}

	public boolean getTestAccountEnabled() {
		return testAccountEnabled;
	}

	public void setTestAccountEnabled(boolean testAccountEnabled) {
		this.testAccountEnabled = testAccountEnabled;
	}
        
        public static Vector getMerchants2ksalesman(String isoId) 
	{
	    Connection dbConn = null;
	    PreparedStatement pstmt = null;
	    Vector vecResults = new Vector();
	    String strSQL = sql_bundle.getString("getMerchantSalesmanid");
	    
	    try
	    {
	    	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	    	if (dbConn == null)
	    	{
	    		cat.error("Can't get database connection");
	    		 
	    	}

	    	pstmt = dbConn.prepareStatement(strSQL);
	    	pstmt.setString(1, isoId);
	      	ResultSet rs = pstmt.executeQuery();

	      	while (rs.next())
	      	{
	      		Vector vecTemp = new Vector();
	      		vecTemp.add(rs.getString("salesmanid"));
	      		vecTemp.add(rs.getString("salesmanname"));
	      		vecResults.add(vecTemp);
	      	}
	      	rs.close();
	      	pstmt.close();
	    }
	    catch (Exception e)
	    {
	    	cat.error("gets2kstockcode => Error during gets2kstockcode", e);
	    	 
	    }
	    finally
	    {
	    	try
	    	{
	    		if (pstmt != null)
	    			pstmt.close();
	    		Torque.closeConnection(dbConn);
	    	}
	    	catch (Exception e)
	    	{
	    		cat.error("gets2kstockcode => Error during closeConnection ", e);
	    	}
	    }
	    return vecResults;
	}
        
        /**
         * 
         * @param merchantId
         * @param latitude
         * @param longitude
         * @return 
         */
        public static int updateLatLong(String merchantId, String latitude, String longitude){
            Connection dbConn = null;
            PreparedStatement pstmt = null;
            String sql = null;
            int result = 0;
            try
            {
                dbConn = TorqueHelper.getConnection(Merchant.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null)
                {
                    Merchant.cat.error("Can't get database connection");
                    throw new CustomerException();
                }
                sql = "Update Merchants SET latitude=?, longitude=? WHERE merchant_id=?";
                pstmt = dbConn.prepareStatement(sql);
                pstmt.setDouble(1, Double.parseDouble(latitude));
                pstmt.setDouble(2, Double.parseDouble(longitude));
                pstmt.setDouble(3, Double.parseDouble(merchantId));                
                if (pstmt.executeUpdate() == 1){
                    result = 1;
                }
                TorqueHelper.closeStatement(pstmt, null);
            }
            catch (Exception e)
            {
                Merchant.cat.error("Error during updateLatLong", e);                
            }
            finally
            {
                try
                {
                    DbUtil.closeDatabaseObjects(dbConn, pstmt, null);
                }
                catch (Exception e)
                {
                    Merchant.cat.error("Error during release connection", e);
                }
            }
            return result;
        }

}
