package com.debisys.customers;

import com.debisys.customers.payments.RepLiabilityData;
import java.sql.Types;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;
import java.sql.ResultSet;
import java.util.Iterator;
import java.sql.Timestamp;
import java.util.Calendar;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.math.BigDecimal;
import java.sql.SQLException;
import com.debisys.utils.Log;
import com.debisys.users.User;
import org.apache.log4j.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ResourceBundle;
import org.apache.torque.Torque;
import com.debisys.utils.DbUtil;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import com.debisys.utils.DateUtil;
import com.debisys.utils.TimeZone;
import javax.servlet.ServletContext;
import com.debisys.utils.JSONObject;
import com.debisys.utils.LogChanges;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.users.SessionData;
import com.debisys.utils.CountryInfo;
import com.debisys.utils.ColumnReport;
import com.debisys.languages.Languages;
import com.emida.utils.dbUtils.DbUtils;
import org.apache.torque.TorqueException;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.dto.RepCredit;
import com.debisys.exceptions.UserException;
import com.debisys.utils.EntityAccountTypes;
import com.emida.utils.dbUtils.TorqueHelper;
import com.debisys.reports.TransactionReport;
import com.debisys.exceptions.EntityException;
import com.debisys.exceptions.ReportException;
import com.emida.utils.dbUtils.InputParameter;
import net.emida.supportsite.dao.RepCreditDao;
import com.debisys.utils.DebisysConfigListener;
import com.emida.utils.dbUtils.OutputParameter;
import com.debisys.exceptions.CustomerException;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.reports.summarys.DownloadsSummary;
import net.emida.supportsite.dto.PaymentNotification;
import net.emida.supportsite.dao.PaymentNotificationDao;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import java.util.HashMap;



public class Rep {

    private String businessName;
    private String initials;
    private String contactFirst;
    private String contactMiddle;
    private String contactLast;
    private String contactName;
    private String contactPhone;
    private String contactFax;
    private String contactEmail;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String country;

    private String repId;
    private String repCreditType;
    private String oldRepCreditType;
    private String oldParentRepId;
    private String parentRepId;
    private String parentRepname;

    private String taxId;
    private String routingNumber;
    private String accountNumber;
    private String runningLiability;
    private String availableCredit;
    private String startDate;
    private String endDate;

    private String creditLimit = "0.00";
    private String paymentAmount;
    private String paymentDescription;
    private String netPaymentAmount;
    private String commission;

    private String sharedBalance = "0";

    /* BEGIN Release 9.0 - Added by LF */
    private String contactCellPhone = "";
    private String contactDepartment = "";
    private Vector vecContacts = new Vector();
    private int contactTypeId = 0;
    private String businessTypeId;
    private int merchantSegmentId;
    private String businessHours;
    private String salesmanid;
    private String terms;
    private String salesmanname;
    private String physCounty;
    private String physColony;
    private String physDelegation;
    private String mailAddress;
    private String mailCity;
    private String mailState;
    private String mailZip;
    private String mailColony;
    private String mailDelegation;
    private String mailCounty;
    private String mailCountry;
    private String bankName;
    private int accountTypeId;
    private String currencyCode;
    /* END Release 9.0 - Added by LF */

    private final Hashtable validationErrors = new Hashtable();

    // log4j logging
    private static final Logger cat = Logger.getLogger(Rep.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.customers.sql");

    /* BEGIN Release 14.0 - Added by NM */
    private String legalBusinessname;

    /* END Release 14.0 - Added by NM */
    // #BEGIN Added by NM - Release 21
    private int paymentType;
    private int processType;

    private String agentCreditType = "";
    private String subagentCreditType = "";
    private String subagentId = "";
    private String agentId = "";

    private int timeZoneId; // Reference to the current TimeZone, a value of 0 indicates no zone which then maps to GMT+0

    // DBSY-1072
    private int entityAccountType = -1;
    private String dentityAccountType = ""; // determines if the combo is disabled
    private int internalChildrenExist = 0;

    private int addressValidationStatus = 0;
    private double latitude = 0;
    private double longitude = 0;

    //DTU-369 Payment Notifications
    private boolean paymentNotifications = false;
    private int paymentNotificationType = 0;
    private String smsNotificationPhone = "";
    private String mailNotificationAddress = "";
    private boolean paymentNotificationSMS = false;
    private boolean paymentNotificationMail = false;

    //DTU-480: Low Balance Alert Message SMS and/or email
    private boolean balanceNotifications = false;
    private int balanceNotificationType = 0;
    private int balanceNotificationNotType = 0;
    private boolean balanceNotificationTypeValue = false;
    private boolean balanceNotificationTypeDays = false;
    private double balanceNotificationValue = 0;
    private int balanceNotificationDays = 0;
    private boolean balanceNotificationSMS = false;
    private boolean balanceNotificationMail = false;

    private String strUrlLocation = "";
    private long invoiceType = 0L;

    public boolean isBalanceNotifications() {
        return balanceNotifications;
    }

    public void setBalanceNotifications(boolean balanceNotifications) {
        this.balanceNotifications = balanceNotifications;
    }

    public int getBalanceNotificationType() {
        return balanceNotificationType;
    }

    public void setBalanceNotificationType(int balanceNotificationType) {
        this.balanceNotificationType = balanceNotificationType;
    }

    public int getBalanceNotificationNotType() {
        return balanceNotificationNotType;
    }

    public void setBalanceNotificationNotType(int balanceNotificationNotType) {
        this.balanceNotificationNotType = balanceNotificationNotType;
    }

    public boolean isBalanceNotificationTypeValue() {
        return balanceNotificationTypeValue;
    }

    public void setBalanceNotificationTypeValue(boolean balanceNotificationTypeValue) {
        this.balanceNotificationTypeValue = balanceNotificationTypeValue;
    }

    public boolean isBalanceNotificationTypeDays() {
        return balanceNotificationTypeDays;
    }

    public void setBalanceNotificationTypeDays(boolean balanceNotificationTypeDays) {
        this.balanceNotificationTypeDays = balanceNotificationTypeDays;
    }

    public double getBalanceNotificationValue() {
        return balanceNotificationValue;
    }

    public void setBalanceNotificationValue(double balanceNotificationValue) {
        this.balanceNotificationValue = balanceNotificationValue;
    }

    public int getBalanceNotificationDays() {
        return balanceNotificationDays;
    }

    public void setBalanceNotificationDays(int balanceNotificationDays) {
        this.balanceNotificationDays = balanceNotificationDays;
    }

    public boolean isBalanceNotificationSMS() {
        return balanceNotificationSMS;
    }

    public void setBalanceNotificationSMS(boolean balanceNotificationSMS) {
        this.balanceNotificationSMS = balanceNotificationSMS;
    }

    public boolean isBalanceNotificationMail() {
        return balanceNotificationMail;
    }

    public void setBalanceNotificationMail(boolean balanceNotificationMail) {
        this.balanceNotificationMail = balanceNotificationMail;
    }

    public boolean isPaymentNotifications() {
        return paymentNotifications;
    }

    public void setPaymentNotifications(boolean paymentNotifications) {
        this.paymentNotifications = paymentNotifications;
    }

    public int getPaymentNotificationType() {
        return paymentNotificationType;
    }

    public void setPaymentNotificationType(int paymentNotificationType) {
        this.paymentNotificationType = paymentNotificationType;
    }

    public String getSmsNotificationPhone() {
        return smsNotificationPhone;
    }

    public void setSmsNotificationPhone(String smsNotificationPhone) {
        this.smsNotificationPhone = smsNotificationPhone;
    }

    public String getMailNotificationAddress() {
        return mailNotificationAddress;
    }

    public void setMailNotificationAddress(String mailNotificationAddress) {
        this.mailNotificationAddress = mailNotificationAddress;
    }

    public boolean isPaymentNotificationSMS() {
        return paymentNotificationSMS;
    }

    public void setPaymentNotificationSMS(boolean paymentNotificationSMS) {
        this.paymentNotificationSMS = paymentNotificationSMS;
    }

    public boolean isPaymentNotificationMail() {
        return paymentNotificationMail;
    }

    public void setPaymentNotificationMail(boolean paymentNotificationMail) {
        this.paymentNotificationMail = paymentNotificationMail;
    }

    public int getPaymentType() {

        return this.paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public int getProcessType() {
        return this.processType;
    }

    public void setProcessType(int processType) {
        this.processType = processType;
    }

    // #END Added by NM - Release 21
    public String getStartDate() {
        return StringUtil.toString(this.startDate);
    }

    public void setStartDate(String strValue) {
        this.startDate = strValue;
    }

    public String getEndDate() {
        return StringUtil.toString(this.endDate);
    }

    public void setEndDate(String strValue) {
        this.endDate = strValue;
    }

    public String getOldRepCreditType() {
        return this.oldRepCreditType;
    }

    public void setOldRepCreditType(String oldRepCreditType) {
        this.oldRepCreditType = oldRepCreditType;
    }

    public String getAvailableCredit() {
        return this.availableCredit;
    }

    public void setAvailableCredit(String availableCredit) {
        this.availableCredit = availableCredit;
    }

    public String getRunningLiability() {
        return this.runningLiability;
    }

    public void setRunningLiability(String runningLiability) {
        this.runningLiability = runningLiability;
    }

    public String getParentRepId() {
        return StringUtil.toString(this.parentRepId);
    }

    public void setParentRepId(String parentRepId) {
        this.parentRepId = parentRepId;
    }

    public String getParentRepname() {
        return StringUtil.toString(this.parentRepname);
    }

    public void setParentRepname(String parentRepname) {
        this.parentRepname = parentRepname;
    }

    public String getTaxId() {
        return StringUtil.toString(this.taxId);
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getBusinessName() {
        return StringUtil.toString(this.businessName);
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getInitials() {
        return StringUtil.toString(this.initials);
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getContactFirst() {
        return StringUtil.toString(this.contactFirst);
    }

    public void setContactFirst(String contactFirst) {
        this.contactFirst = contactFirst;
    }

    public String getContactMiddle() {
        return StringUtil.toString(this.contactMiddle);
    }

    public void setContactMiddle(String contactMiddle) {
        this.contactMiddle = contactMiddle;
    }

    public String getContactLast() {
        return StringUtil.toString(this.contactLast);
    }

    public void setContactLast(String contactLast) {
        this.contactLast = contactLast;
    }

    public String getContactName() {
        return StringUtil.toString(this.contactName);
    }

    public void setContactName(String strValue) {
        this.contactName = strValue;
    }

    public String getContactPhone() {
        return StringUtil.toString(this.contactPhone);
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactFax() {
        return StringUtil.toString(this.contactFax);
    }

    public void setContactFax(String contactFax) {
        this.contactFax = contactFax;
    }

    public String getContactEmail() {
        return StringUtil.toString(this.contactEmail);
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getAddress() {
        return StringUtil.toString(this.address);
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return StringUtil.toString(this.city);
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return StringUtil.toString(this.state);
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return StringUtil.toString(this.zip);
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getRepCreditType() {
        return StringUtil.toString(this.repCreditType);
    }

    public void setRepCreditType(String repCreditType) {
        this.repCreditType = repCreditType;
    }

    public String getRoutingNumber() {
        return StringUtil.toString(this.routingNumber);
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return StringUtil.toString(this.accountNumber);
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCountry() {
        return StringUtil.toString(this.country);
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreditLimit() {
        return StringUtil.toString(this.creditLimit);
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getPaymentAmount() {
        return StringUtil.toString(this.paymentAmount);
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getRepId() {
        return StringUtil.toString(this.repId);
    }

    public String getPaymentDescription() {
        return this.paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public String getNetPaymentAmount() {
        return this.netPaymentAmount;
    }

    public void setNetPaymentAmount(String netPaymentAmount) {
        this.netPaymentAmount = netPaymentAmount;
    }

    public String getCommission() {
        return this.commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    /* BEGIN Release 9.0 - Added by LF */
    /**
     * @param sharedBalance The sharedBalance to set.
     */
    public void setSharedBalance(String sharedBalance) {
        this.sharedBalance = sharedBalance;
    }

    /**
     * @return Returns the sharedBalance.
     */
    public String getSharedBalance() {
        return this.sharedBalance;
    }

    public int getContactTypeId() {
        return this.contactTypeId;
    }

    public void setContactTypeId(int contactTypeId) {
        this.contactTypeId = contactTypeId;
    }

    public String getContactCellPhone() {
        return StringUtil.toString(this.contactCellPhone);
    }

    public void setContactCellPhone(String contactCellPhone) {
        this.contactCellPhone = contactCellPhone;
    }

    public String getContactDepartment() {
        return StringUtil.toString(this.contactDepartment);
    }

    public void setContactDepartment(String contactDepartment) {
        this.contactDepartment = contactDepartment;
    }

    public Vector getVecContacts() {
        return this.vecContacts;
    }

    public void setVecContacts(Vector vecContacts) {
        this.vecContacts = vecContacts;
    }

    public String getBusinessTypeId() {
        return this.businessTypeId;
    }

    public void setBusinessTypeId(String businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public int getMerchantSegmentId() {
        return this.merchantSegmentId;
    }

    public void setMerchantSegmentId(int merchantSegmentId) {
        this.merchantSegmentId = merchantSegmentId;
    }

    public String getBusinessHours() {
        return StringUtil.toString(this.businessHours);
    }

    public String getsalesmanid() {
        return StringUtil.toString(this.salesmanid);
    }

    public String getterms() {
        return StringUtil.toString(this.terms);
    }

    public String getsalesmanname() {
        return StringUtil.toString(this.salesmanname);
    }

    public void setsalesmanname(String temp) {
        this.salesmanname = temp;
    }

    /* DBSY-1072 eAccounts Interface */
    public int getEntityAccountType() {
        return this.entityAccountType;
    }

    public void setEntityAccountType(int _entityAccountType) {
        this.entityAccountType = _entityAccountType;
    }

    /* DBSY-1072 eAccounts Interface */
    public String getDEntityAccountType() {
        return this.dentityAccountType;
    }

    public void setDEntityAccountType(String _entityAccountType) {
        if (_entityAccountType == null) {
            _entityAccountType = "";
        }
        this.dentityAccountType = _entityAccountType;
    }

    public int getInternalChildrenExist() {
        return this.internalChildrenExist;
    }

    public void setInternalChildrenExist(int _internalChildrenExist) {
        this.internalChildrenExist = _internalChildrenExist;
    }

    /**
     * @param addressValidationStatus the addressValidationStatus to set
     */
    public void setAddressValidationStatus(int addressValidationStatus) {
        this.addressValidationStatus = addressValidationStatus;
    }

    /**
     * @return the addressValidationStatus
     */
    public int getAddressValidationStatus() {
        return addressValidationStatus;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /* END DBSY-1072 eAccounts Interface */
    public String getsalesmannamebyid(String id, String isoId) {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String vecResults = "";
        String strSQL = Rep.sql_bundle.getString("getsalesmannamebyid");

        try {
            dbConn = Torque.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
            } else {

                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setString(1, id);
                pstmt.setString(2, isoId);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    vecResults = rs.getString("salesmanname");
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            Rep.cat.error("getsalesmannamebyid => Error during gets2kstockcode", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return vecResults;
    }

    public boolean verifyid(String id, String isoId) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String strSQL = Rep.sql_bundle.getString("getsalesmannamebyid");

        try {
            dbConn = Torque.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
            } else {

                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setString(1, id);
                pstmt.setString(2, isoId);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    if (id.equals(rs.getString("salesmanid"))) {
                        retValue = true;
                        break;
                    }
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            Rep.cat.error("verifyid => Error during gets2kstockcode", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public void setsalesmanid(String salesmanid) {
        this.salesmanid = salesmanid;
    }

    public void setterms(String temp) {
        this.terms = temp;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getPhysColony() {
        return StringUtil.toString(this.physColony);
    }

    public void setPhysColony(String physColony) {
        this.physColony = physColony;
    }

    public String getPhysDelegation() {
        return StringUtil.toString(this.physDelegation);
    }

    public void setPhysDelegation(String physDelegation) {
        this.physDelegation = physDelegation;
    }

    public String getMailColony() {
        return StringUtil.toString(this.mailColony);
    }

    public void setMailColony(String mailColony) {
        this.mailColony = mailColony;
    }

    public String getMailDelegation() {
        return StringUtil.toString(this.mailDelegation);
    }

    public void setMailDelegation(String mailDelegation) {
        this.mailDelegation = mailDelegation;
    }

    public String getMailCounty() {
        return StringUtil.toString(this.mailCounty);
    }

    public void setMailCounty(String mailCounty) {
        this.mailCounty = mailCounty;
    }

    public String getMailCountry() {
        return StringUtil.toString(this.mailCountry);
    }

    public void setMailCountry(String mailCountry) {
        this.mailCountry = mailCountry;
    }

    public String getBankName() {
        return StringUtil.toString(this.bankName);
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getAccountTypeId() {
        return this.accountTypeId;
    }

    public void setAccountTypeId(int accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public String getPhysCounty() {
        return StringUtil.toString(this.physCounty);
    }

    public void setPhysCounty(String strValue) {
        this.physCounty = strValue;
    }

    public String getMailAddress() {
        return StringUtil.toString(this.mailAddress);
    }

    public void setMailAddress(String strValue) {
        this.mailAddress = strValue;
    }

    public String getMailCity() {
        return StringUtil.toString(this.mailCity);
    }

    public void setMailCity(String strValue) {
        this.mailCity = strValue;
    }

    public String getMailState() {
        return StringUtil.toString(this.mailState);
    }

    public void setMailState(String strValue) {
        this.mailState = strValue;
    }

    public String getMailZip() {
        return StringUtil.toString(this.mailZip);
    }

    public void setMailZip(String strValue) {
        this.mailZip = strValue;
    }

    /* END Release 9.0 - Added by LF */

 /* BEGIN Release 14.0 - Added by NM */
    public void setLegalBusinessname(String legalBusinessname) {
        this.legalBusinessname = legalBusinessname;
    }

    public String getLegalBusinessname() {
        if (this.legalBusinessname == null) {
            return "";
        } else {
            return this.legalBusinessname;
        }
    }

    /**
     * @return the strUrlLocation
     */
    public String getStrUrlLocation() {
        return strUrlLocation;
    }

    /**
     * @param strUrlLocation the strUrlLocation to set
     */
    public void setStrUrlLocation(String strUrlLocation) {
        this.strUrlLocation = strUrlLocation;
    }


    /* END Release 14.0 - Added by NM */
    /**
     * Returns a hash of errors.
     *
     * @return Hash of errors.
     */
    public Hashtable getErrors() {
        Rep.cat.debug("There are this many friggin errors: " + this.validationErrors.size());
        return this.validationErrors;
    }

    public boolean isError() {
        return (this.validationErrors.size() > 0);
    }

    public long getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(long invoiceType) {
        this.invoiceType = invoiceType;
    }

    /**
     * Returns a validation error against a specific field. If a field was found
     * to have an error during {@link #validate validate}, the error message can
     * be accessed via this method.
     *
     * @param fieldname The bean property name of the field
     * @return The error message for the field or null if none is present.
     */
    public String getFieldError(String fieldname) {
        return ((String) this.validationErrors.get(fieldname));
    }

    /**
     * Sets the validation error against a specific field. Used by
     * {@link #validate validate}.
     *
     * @param fieldname The bean property name of the field
     * @param error The error message for the field or null if none is present.
     */
    public void addFieldError(String fieldname, String error) {
        Rep.cat.debug("ERROR OCCURRED: " + fieldname + " - " + error);
        this.validationErrors.put(fieldname, error);
    }

    public boolean validate(SessionData sessionData, ServletContext context) {
        Rep.cat.debug("Begin validate");
        boolean valid = true;

        try {
            String deploymentType = DebisysConfigListener.getDeploymentType(context);

            String strDistChainType = sessionData.getProperty("dist_chain_type");
            this.validationErrors.clear();
            String strAccessLevel = sessionData.getProperty("access_level");
            if ((this.parentRepId == null) || (this.parentRepId.length() == 0)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                        && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
                    this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep", sessionData.getLanguage()));
                    valid = false;
                }
                // otherwise it must be an subagent which would be the parent for a
                // 5
                // level
                // or it would be the iso in a 3 level

            } else if (!NumberUtil.isNumeric(this.parentRepId)) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep_id", sessionData.getLanguage()));
                valid = false;
            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                    && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
                // make sure this is a valid subagent for this agent or iso
                if (!this.checkSubAgentPermission(sessionData)) {
                    this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep_id2", sessionData.getLanguage()));
                    valid = false;
                }
            }

            if ((this.businessName == null) || (this.businessName.length() == 0)) {
                this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name", sessionData.getLanguage()));
                valid = false;
            } else if (this.businessName.length() > 50) {
                this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.address == null) || (this.address.length() == 0)) {
                this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address", sessionData.getLanguage()));
                valid = false;
            } else if (this.address.length() > 50) {
                this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address_length", sessionData.getLanguage()));
                valid = false;
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                if ((this.city == null) || (this.city.length() == 0)) {
                    this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city", sessionData.getLanguage()));
                    valid = false;
                } else if (this.city.length() > 50) {
                    this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city_length", sessionData.getLanguage()));
                    valid = false;
                }

                if ((this.state == null) || (this.state.length() == 0)) {
                    this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state", sessionData.getLanguage()));
                    valid = false;
                } else if (this.state.length() > 2) {
                    this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state_length", sessionData.getLanguage()));
                    valid = false;
                }

                if ((this.zip == null) || (this.zip.length() == 0)) {
                    this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip", sessionData.getLanguage()));
                    valid = false;
                } else if (this.zip.length() > 50) {
                    this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip_length", sessionData.getLanguage()));
                    valid = false;
                }
            }

            if (this.country != null && this.country.length() > 50) {
                this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country_length", sessionData.getLanguage()));
                valid = false;
            }
            if (!StringUtil.isNotEmptyStr(this.country)) {
                this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country", sessionData.getLanguage()));
                valid = false;
            }
            if ((this.contactFirst == null) || (this.contactFirst.length() == 0)) {
                this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactFirst.length() > 15) {
                this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactLast == null) || (this.contactLast.length() == 0)) {
                this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactLast.length() > 15) {
                this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactMiddle != null) && (this.contactMiddle.length() > 1)) {
                this.addFieldError("contactMiddle", Languages.getString("jsp.admin.customers.reps_add.error_contact_middle", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactPhone == null) || (this.contactPhone.length() == 0)) {
                this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactPhone.length() > 15) {
                this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactFax != null) && (this.contactFax.length() > 15)) {
                this.addFieldError("contactFax", Languages.getString("jsp.admin.customers.reps_add.error_fax_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactEmail != null) && (this.contactEmail.length() > 200)) {
                this.addFieldError("contactEmail", Languages.getString("jsp.admin.customers.reps_add.error_email_length", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactEmail != null) {
                // commented out because we have multiple emails in a field, i.e.
                // aaa@aaa.com;bbb@bbb.com
                /*
				 * ValidateEmail validateEmail = new ValidateEmail(); validateEmail.setStrEmailAddress(contactEmail); validateEmail.checkSyntax(); if
				 * (!validateEmail.isEmailValid()) { addFieldError("contactEmail", "Please enter a valid email address."); valid = false; }
                 */

            }

            if ((this.routingNumber != null) && (this.routingNumber.length() > 18)) {
                this.addFieldError("routingNumber", Languages.getString("jsp.admin.customers.reps_add.error_routing_number", sessionData.getLanguage()));
                valid = false;
            } else if (this.routingNumber == null) {
                this.routingNumber = "NA";
            }

            if ((this.accountNumber != null) && (this.accountNumber.length() > 20)) {
                this.addFieldError("accountNumber", Languages.getString("jsp.admin.customers.reps_add.error_account_number", sessionData.getLanguage()));
                valid = false;
            } else if (this.accountNumber == null) {
                this.accountNumber = "NA";
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                if ((this.taxId == null) || this.taxId.equals("")) {
                    this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id_mandatory",
                            sessionData.getLanguage()));
                    valid = false;
                } else {
                    Pattern p = Pattern.compile("^(?!(\\d)\\1+$|(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){8}\\d$)\\d{9}$");
                    Matcher match = p.matcher(this.taxId);
                    if (!match.matches()) {
                        this.addFieldError("taxId", Languages.getString(
                                "jsp.admin.customers.reps_add.error_tax_length", sessionData.getLanguage()));
                        valid = false;
                    }
                }

            } else if ((this.taxId != null) && (this.taxId.length() > 15)) {
                this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id", sessionData.getLanguage()));
                valid = false;
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)
                        || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)
                        || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE)) {
                    if ((this.creditLimit == null) || (this.creditLimit.length() == 0) || (!NumberUtil.isNumeric(this.creditLimit))) {
                        if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) // credit limit is required for all prepaid and credit based
                        // reps
                        {
                            this.addFieldError("creditLimit", Languages.getString("jsp.admin.customers.reps_add.error_liability_limit1", sessionData.getLanguage()));
                            valid = false;
                        }
                    } else if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                        // check for valid liability limit
                        if (!NumberUtil.isNumeric(this.creditLimit)) {
                            this.addFieldError("creditLimit", Languages.getString("jsp.admin.customers.reps_add.error_liability_limit2", sessionData.getLanguage()));
                            valid = false;
                        }
                    } else {
                        this.creditLimit = "0.00";
                    }
                } else // invalid repType
                {
                    this.addFieldError("repType", Languages.getString("jsp.admin.customers.reps_add.error_rep_type", sessionData.getLanguage()));
                    valid = false;
                }

                // DBSYS-613 Nidhi Gulati
                // Checking added while adding rep to make sure that you are not
                // creating an unlimited rep under credit/prepaid Subagent
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    if ((this.parentRepId != null) && (this.parentRepId.length() != 0)) {
                        Rep subagent = new Rep();

                        try {
                            if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
                                    || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                                subagent.setRepId(this.parentRepId);
                            }

                            subagent.getSubAgent(sessionData, context);
                            // this is so I can access this from addMerchant
                            this.subagentCreditType = subagent.getRepCreditType();
                        } catch (Exception e) {
                        }

                        // loaded up with subagent info now lets do the check
                        if (!this.subagentCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                            // this means the rep is prepaid or credit based
                            // so check to make sure there is available credit
                            if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                                // this is an error
                                // cant assign an unlimited merchant to a limited
                                // rep
                                this.addFieldError("creditType", Languages.getString("com.debisys.customers.subagent_credit_error3", sessionData.getLanguage()));
                                valid = false;
                            } else // check the credit limit to make sure there is
                            // enough
                             if (Double.parseDouble(subagent.getAvailableCredit()) < Double.parseDouble(this.creditLimit)) {
                                    this.addFieldError("creditLimit", Languages.getString("com.debisys.customers.subagent_credit_error4", sessionData.getLanguage())
                                            + NumberUtil.formatAmount(subagent.getAvailableCredit()));
                                    valid = false;
                                }
                        }
                    }
                }

            }// end if intl

            /* BEGIN Release 14.0 - Added by NM */
            if (deploymentType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                if ((this.legalBusinessname != null) && (this.legalBusinessname.length() == 0)) {
                    this.addFieldError("legalBusinessname", Languages.getString("jsp.admin.customers.reps_edit.legal_businessname", sessionData.getLanguage()));
                    valid = false;
                }
            }
            /* END Release 14.0 - Added by NM */
        } catch (Exception e) {
            Rep.cat.error("Exception in validate: " + e);
        }
        Rep.cat.debug("End validate");
        return valid;

    }

    public boolean validateUpdate(SessionData sessionData, ServletContext context) {
        String deploymentType = DebisysConfigListener.getDeploymentType(context);
        String customConfigType = DebisysConfigListener.getCustomConfigType(context);

        String strDistChainType = sessionData.getProperty("dist_chain_type");
        this.validationErrors.clear();
        boolean valid = true;
        String strAccessLevel = sessionData.getProperty("access_level");
        if ((this.parentRepId == null) || (this.parentRepId.length() == 0)) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                    && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep", sessionData.getLanguage()));
                valid = false;
            }
            // otherwise it must be an subagent which would be the parent for a
            // 5
            // level
            // or it would be the iso in a 3 level

        } else if (!NumberUtil.isNumeric(this.parentRepId)) {
            this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep_id", sessionData.getLanguage()));
            valid = false;
        } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
            // make sure this is a valid subagent for this agent or iso
            if (!this.checkSubAgentPermission(sessionData)) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep_id2", sessionData.getLanguage()));
                valid = false;
            }
        }

        if ((this.businessName == null) || (this.businessName.length() == 0)) {
            this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name", sessionData.getLanguage()));
            valid = false;
        } else if (this.businessName.length() > 50) {
            this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.address == null) || (this.address.length() == 0)) {
            this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address", sessionData.getLanguage()));
            valid = false;
        } else if (this.address.length() > 50) {
            this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address_length", sessionData.getLanguage()));
            valid = false;
        }

        // validate city in all deployments/custom configs
        if ((this.city == null) || (this.city.length() == 0)) {
            this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city", sessionData.getLanguage()));
            valid = false;
        } else if (this.city.length() > 15) {
            this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city_length", sessionData.getLanguage()));
            valid = false;
        }

        // same as for zip
        if ((this.zip == null) || (this.zip.length() == 0)) {
            this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip", sessionData.getLanguage()));
            valid = false;
        } else if (this.zip.length() > 10) {
            this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip_length", sessionData.getLanguage()));
            valid = false;
        }

        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
                || (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {
            // for US and INTL only: validate state
            if ((this.state == null) || (this.state.length() == 0)) {
                this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state", sessionData.getLanguage()));
                valid = false;
            } else if (this.state.length() > 2) {
                this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state_length", sessionData.getLanguage()));
                valid = false;
            }
        }

        if (this.country != null && this.country.length() > 50) {
            this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country_length", sessionData.getLanguage()));
            valid = false;
        }

        if (!StringUtil.isNotEmptyStr(this.country)) {
            this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country", sessionData.getLanguage()));
            valid = false;
        }
        if ((this.contactFirst == null) || (this.contactFirst.length() == 0)) {
            this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactFirst.length() > 15) {
            this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactLast == null) || (this.contactLast.length() == 0)) {
            this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactLast.length() > 15) {
            this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactMiddle != null) && (this.contactMiddle.length() > 1)) {
            this.addFieldError("contactMiddle", Languages.getString("jsp.admin.customers.reps_add.error_contact_middle", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactPhone == null) || (this.contactPhone.length() == 0)) {
            this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactPhone.length() > 15) {
            this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactFax != null) && (this.contactFax.length() > 15)) {
            this.addFieldError("contactFax", Languages.getString("jsp.admin.customers.reps_add.error_fax_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactEmail != null) && (this.contactEmail.length() > 200)) {
            this.addFieldError("contactEmail", Languages.getString("jsp.admin.customers.reps_add.error_email_length", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactEmail != null) {
            // commented out because we have multiple emails in a field, i.e.
            // aaa@aaa.com;bbb@bbb.com
            /*
			 * ValidateEmail validateEmail = new ValidateEmail(); validateEmail.setStrEmailAddress(contactEmail); validateEmail.checkSyntax(); if
			 * (!validateEmail.isEmailValid()) { addFieldError("contactEmail", "Please enter a valid email address."); valid = false; }
             */
        }

        if ((this.routingNumber != null) && (this.routingNumber.length() > 18)) {
            this.addFieldError("routingNumber", Languages.getString("jsp.admin.customers.reps_add.error_routing_number", sessionData.getLanguage()));
            valid = false;
        } else if (this.routingNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            this.routingNumber = "NA";
        }

        if ((this.accountNumber != null) && (this.accountNumber.length() > 20)) {
            this.addFieldError("accountNumber", Languages.getString("jsp.admin.customers.reps_add.error_account_number", sessionData.getLanguage()));
            valid = false;
        } else if (this.accountNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            this.accountNumber = "NA";
        }

        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            if ((this.taxId == null) || this.taxId.equals("")) {
                this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id_mandatory",
                        sessionData.getLanguage()));
                valid = false;
            } else {
                Pattern p = Pattern.compile("^(?!(\\d)\\1+$|(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){8}\\d$)\\d{9}$");
                Matcher match = p.matcher(this.taxId);
                if (!match.matches()) {
                    this.addFieldError("taxId", Languages.getString(
                            "jsp.admin.customers.reps_add.error_tax_length", sessionData.getLanguage()));
                    valid = false;
                }

            }

        } else if ((this.taxId != null) && (this.taxId.length() > 15)) {
            this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id", sessionData.getLanguage()));
            valid = false;
        }
        // need to add check to make sure new rep has enough credit and is of
        // the same credit type

        /* BEGIN Release 14.0 - Added by NM */
        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && !customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
            if ((this.legalBusinessname == null) || (this.legalBusinessname.length() == 0)) {
                this.addFieldError("legalBusinessname", Languages.getString("jsp.admin.customers.reps_edit.legal_businessname", sessionData.getLanguage()));
                valid = false;
            }
        }

        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
            if (!this.validateCheckSubAgentChange(sessionData, context)) {
                valid = false;
            }
        }

        /* END Release 14.0 - Added by NM */
        return valid;

    }

    /**
     * Adds a new rep in the database and sets the entity id of the object to
     * the newly created record's ID
     *
     * @param sessionData
     * @param context
     * @throws com.debisys.exceptions.EntityException Thrown if there is an
     * error inserting the record in the database.
     */
    public void addRep(SessionData sessionData, ServletContext context) throws EntityException {

        Connection conn = null;
        PreparedStatement pstmt1 = null;
        ResultSet r = null, resultRepInfo = null;
        try {
            HashMap map = CountryInfo.getAllCountriesID();
            String isoid = sessionData.getProperty("ref_id"), creditType = "";
            long v = 0;
            conn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (conn == null) {
                Rep.cat.error("Can't get database connection");
                throw new EntityException();
            }
            String SQL = Rep.sql_bundle.getString("getaddRepIso");
            Rep.cat.debug(SQL);
            pstmt1 = conn.prepareStatement(SQL);
            try {
                v = Long.valueOf(this.parentRepId);                
            } catch (NumberFormatException e) {
                Rep.cat.error("NumberFormatException Parsing ParentID to long" + e);
            }
            pstmt1.setLong(1, v);

            r = pstmt1.executeQuery();        
            while (r.next()) {
                isoid = r.getString(1).trim();
            }
            Rep.cat.debug("ISOID from database  = " + isoid);

            TorqueHelper.closeStatement(pstmt1, r);
            
            try {
                String sqlRep = Rep.sql_bundle.getString("getCreditIso");
                Rep.cat.debug("Rep.addRep - GETTING REP INFO");
                Rep.cat.debug("Rep.addRep - "+sqlRep);
                pstmt1 = conn.prepareStatement(sqlRep);
                pstmt1.setLong(1, v);
                resultRepInfo = pstmt1.executeQuery();
                
                while (resultRepInfo.next()) {
                    creditType = resultRepInfo.getString(2).trim();
                }
            } catch (Exception e) {
                Rep.cat.error("Rep.addRep - ERROR GETTING DATA REP OF :" + v + " : "+e.getMessage());
            }
            
            TorqueHelper.closeStatement(pstmt1, resultRepInfo);

            // get ISO ID from RepID
            Rep.cat.debug("the staticIDCountryISO1 info is : " + context.getAttribute("staticIDCountryISO1"));
            Rep.cat.debug("the staticIDConstantValue1 info is : " + context.getAttribute("staticIDConstantValue1"));
            Vector<String> staticISO = new Vector<String>();
            // check if Country ISO matches Static ISO Conditions
            // Static ISO added to define constant values for a country ISO
            for (int i = 1;; i++) {
                if (context.getAttribute("staticIDCountryISO" + i) == null || context.getAttribute("staticIDCountryISO" + i) == "") {
                    break;
                }
                if (context.getAttribute("staticIDConstantValue" + i) == null || context.getAttribute("staticIDConstantValue" + i) == "") {
                    break;
                }
                if (isoid.equals(context.getAttribute("staticIDCountryISO" + i))) {
                    staticISO.add((String) context.getAttribute("staticIDCountryISO" + i));
                    staticISO.add((String) context.getAttribute("staticIDConstantValue" + i));
                    break;
                }
            }
            if (staticISO.size() < 2) {
                this.repId = this.generateRepId();// if no special cases, id is generated randomly
                Rep.cat.debug("Vector Static ISO is empty");
            } else {
                this.repId = this.generatePartiallyStaticId(staticISO).trim();
                Rep.cat.debug("Vector Static ISO detected genIDis=" + this.repId);
            }

            String strSQL = Rep.sql_bundle.getString("addRep");
            Rep.cat.debug(strSQL);
            pstmt1 = conn.prepareStatement(strSQL);
            // rep id

            pstmt1.setDouble(1, Double.parseDouble(this.repId));
            pstmt1.setInt(2, 1); // type = 1 = rep
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && strAccessLevel.equals(DebisysConstants.ISO))
                    || (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && strAccessLevel.equals(DebisysConstants.SUBAGENT))) {
                // if you are a 3 level iso or a 5 level subagent, then the parent rep should be the person logged in
                pstmt1.setDouble(3, Double.parseDouble(strRefId));
            } else { // otherwise you must be a 5 level iso or agent
                pstmt1.setDouble(3, Double.parseDouble(this.parentRepId));
            }
            pstmt1.setString(4, this.businessName);
            // initials

            String initials = this.contactFirst.substring(0, 1);
            if (this.contactMiddle == null || this.contactMiddle.trim().equals("")) {
                initials = initials + "_";
            } else {
                initials = initials + this.contactMiddle.substring(0, 1);
            }
            initials = initials + this.contactLast.substring(0, 1);

            pstmt1.setString(5, initials);
            pstmt1.setString(6, this.contactFirst);
            pstmt1.setString(7, this.contactLast);
            pstmt1.setString(8, this.contactMiddle);
            pstmt1.setString(9, this.taxId);
            pstmt1.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
            pstmt1.setString(11, this.address);
            pstmt1.setString(12, this.city);
            pstmt1.setString(13, this.state);
            pstmt1.setString(14, this.zip);
            pstmt1.setString(15, this.contactPhone);
            pstmt1.setString(16, this.contactFax);
            pstmt1.setString(17, this.contactEmail);
            pstmt1.setString(18, this.routingNumber);
            pstmt1.setString(19, this.accountNumber);
            // running liability
            pstmt1.setDouble(20, 0);
            pstmt1.setDouble(21, Double.parseDouble(this.creditLimit));
            if (this.getISOCreditType(this.parentRepId).equals(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE)) {
                this.setRepCreditType(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE);
                Rep.cat.debug("The Credit Type set to: " + this.repCreditType);
            }
            if (this.repCreditType == null) {
                // this is domestic so lets add the rep type for them there is no concept of prepaid domestically so they are credit or unlimited
                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                    this.repCreditType = creditType;
                } else if (Double.parseDouble(this.creditLimit) > 0) {
                    this.repCreditType = DebisysConstants.REP_CREDIT_TYPE_CREDIT;
                } else {
                    this.repCreditType = DebisysConstants.REP_CREDIT_TYPE_UNLIMITED;
                }
            }
            pstmt1.setInt(22, Integer.parseInt(this.repCreditType));
            pstmt1.setString(23, this.country);
            pstmt1.setString(24, this.sharedBalance);
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (this.mailState != null)) {
                pstmt1.setString(25, this.mailAddress);
                pstmt1.setString(26, this.mailCity);
                pstmt1.setString(27, this.mailState);
                pstmt1.setString(28, this.mailZip);
                pstmt1.setString(29, this.mailCountry);
                pstmt1.setString(30, this.mailDelegation);
                pstmt1.setString(31, this.mailCounty);
                pstmt1.setString(32, this.mailColony);
            } else {
                pstmt1.setString(25, this.address);
                pstmt1.setString(26, this.city);
                pstmt1.setString(27, this.state);
                pstmt1.setString(28, this.zip);
                pstmt1.setString(29, this.country);
                pstmt1.setString(30, this.physDelegation);
                pstmt1.setString(31, this.physCounty);
                pstmt1.setString(32, this.physColony);
            }
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                pstmt1.setString(33, this.physCounty);
                pstmt1.setString(34, this.physColony);
                pstmt1.setString(35, this.physDelegation);
                pstmt1.setString(36, this.bankName);
                pstmt1.setString(37, this.businessHours);
                pstmt1.setString(38, this.businessTypeId);
                pstmt1.setInt(39, this.accountTypeId);
                pstmt1.setInt(40, this.merchantSegmentId);
                /* BEGIN Release 14.0 - Added by NM */
                pstmt1.setString(41, this.legalBusinessname);
                /* END Release 14.0 - Added by NM */
            } else {
                pstmt1.setNull(33, Types.VARCHAR);
                pstmt1.setNull(34, Types.VARCHAR);
                pstmt1.setNull(35, Types.VARCHAR);
                pstmt1.setNull(36, Types.VARCHAR);
                pstmt1.setNull(37, Types.VARCHAR);
                pstmt1.setNull(38, Types.VARCHAR);
                pstmt1.setInt(39, 1);
                pstmt1.setNull(40, Types.INTEGER);
                /* BEGIN Release 14.0 - Added by NM */
                pstmt1.setNull(41, Types.VARCHAR);
                /* END Release 14.0 - Added by NM */
            }

            // #BEGIN Added by NM - Release 21
            pstmt1.setInt(42, this.paymentType);
            pstmt1.setInt(43, this.processType);
            // #END Added by NM - Release 21
            pstmt1.setInt(44, this.timeZoneId);
            // pstmt1.setString(45, map.get(Integer.parseInt(this.country)).toString() );
            pstmt1.setString(45, "11");
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                    && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                    && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                pstmt1.setString(46, this.salesmanid);
                pstmt1.setString(47, this.terms);
            } else {
                pstmt1.setNull(46, Types.VARCHAR);
                pstmt1.setNull(47, Types.VARCHAR);
            }

            // DBSY-1072 eAccounts Interface
            if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                Rep.cat.debug("dentityAccountType=" + this.dentityAccountType);
                if (this.dentityAccountType.equals("1")) {
                    pstmt1.setInt(48, 1);// if Combo is disabled then default to external
                    Rep.cat.debug("dentityAccountType=" + this.dentityAccountType);
                } else {
                    pstmt1.setInt(48, this.entityAccountType);
                }
            } else {
                pstmt1.setNull(48, Types.INTEGER);
            }

            //DTU-369 Payment Notifications
            pstmt1.setBoolean(49, this.isPaymentNotifications());
            if (this.isPaymentNotificationSMS()) {
                this.setPaymentNotificationType(0);
            } else if (this.isPaymentNotificationMail()) {
                this.setPaymentNotificationType(1);
            }
            pstmt1.setInt(50, this.getPaymentNotificationType());
            pstmt1.setString(51, this.getSmsNotificationPhone());
            pstmt1.setString(52, this.getMailNotificationAddress());

            //DTU-480: Low Balance Alert Message SMS and/or email
            //DTU-480: Low Balance Alert Message SMS and/or email
            pstmt1.setBoolean(53, this.isBalanceNotifications());
            if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays()) {
                this.setBalanceNotificationType(2);
            } else if (this.isBalanceNotificationTypeValue()) {
                this.setBalanceNotificationType(0);
            } else if (this.isBalanceNotificationTypeDays()) {
                this.setBalanceNotificationType(1);
            }
            pstmt1.setInt(54, this.getBalanceNotificationType());
            if (this.isBalanceNotificationSMS()) {
                this.setBalanceNotificationNotType(0);
            } else if (this.isBalanceNotificationMail()) {
                this.setBalanceNotificationNotType(1);
            }
            pstmt1.setInt(55, this.getBalanceNotificationNotType());
            pstmt1.setInt(56, this.getBalanceNotificationDays());
            pstmt1.setDouble(57, this.getBalanceNotificationValue());
            pstmt1.setLong(58, this.getInvoiceType());

            pstmt1.execute();

            TorqueHelper.closeStatement(pstmt1, null);

            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                Iterator itContacts = this.vecContacts.iterator();
                while (itContacts.hasNext()) {
                    Vector vecTemp = null;
                    vecTemp = (Vector) itContacts.next();
                    pstmt1 = conn.prepareStatement(Rep.sql_bundle.getString("insertContactMx"));
                    pstmt1.setDouble(1, Double.parseDouble(this.repId));
                    pstmt1.setString(2, vecTemp.get(0).toString());
                    pstmt1.setString(3, vecTemp.get(1).toString());
                    pstmt1.setString(4, vecTemp.get(2).toString());
                    pstmt1.setString(5, vecTemp.get(3).toString());
                    pstmt1.setString(6, vecTemp.get(4).toString());
                    pstmt1.setString(7, vecTemp.get(5).toString());
                    pstmt1.setString(8, vecTemp.get(6).toString());
                    pstmt1.executeUpdate();
                    TorqueHelper.closeStatement(pstmt1, null);
                }
            }

            Log.write(sessionData, "Rep added.", DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
            
            // Mexico rep payments on creation will be done 
            if ((!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
                    && (!DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))) {

                RepCredit repCredit = null;
                PaymentNotification paymentNotification = null;

                if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                    //if ((DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                    //	&& (Double.parseDouble(this.creditLimit) > 0))
                    //{
                    strSQL = "select rep_id from reps with (nolock) where rep_id in (select iso_id from reps with (nolock) where rep_id=" + this.repId
                            + ")";
                    Rep.cat.debug(strSQL);
                    pstmt1 = conn.prepareStatement(strSQL);
                    r = pstmt1.executeQuery();
                    if (r.next()) {
                        this.subagentId = r.getString("rep_id");
                    }

                    TorqueHelper.closeStatement(pstmt1, r);
                    Rep subagent = new Rep();
                    subagent.setRepId(this.subagentId);
                    subagent.getSubAgent(sessionData, context);
                    if (!subagent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                        pstmt1 = conn.prepareStatement(Rep.sql_bundle.getString("updateRepRunningLiability"));
                        pstmt1.setDouble(1, (Double.parseDouble(subagent.getRunningLiability()) + Double.parseDouble(this.creditLimit)));
                        pstmt1.setDouble(2, Double.parseDouble(subagent.getRepId()));
                        pstmt1.executeUpdate();
                        TorqueHelper.closeStatement(pstmt1, null);

                        double subagentCreditLimit = subagent.getCurrentSubAgentCreditLimit(sessionData);
                        double subagentAssignedCredit = subagent.getAssignedCreditSubAgent(sessionData);

                        // add an entry to show the rep credits being used
                        // 1 2 3 4 5 6 7 8 9 10
                        // (rep_id, merchant_id, logon_id, description,
                        // amount,
                        // available_credit, prior_available_credit,
                        // datetime, subagent_entityaccounttype, rep_entityaccounttype)
                        pstmt1 = conn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                        pstmt1.setDouble(1, Double.parseDouble(subagent.getRepId()));
                        pstmt1.setDouble(2, Double.parseDouble(this.repId));
                        pstmt1.setString(3, sessionData.getProperty("username"));
                        pstmt1.setString(4, "\"" + this.getRepName(this.getRepId()) + "\" " + Languages.getString("com.debisys.customers.Merchant.assigned_credit", sessionData.getLanguage()));
                        pstmt1.setDouble(5, Double.parseDouble(this.creditLimit) * -1);
                        pstmt1.setDouble(6, subagentCreditLimit - subagentAssignedCredit);
                        pstmt1.setDouble(7, subagentCreditLimit - (subagentAssignedCredit - Double.parseDouble(this.creditLimit)));
                        pstmt1.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                        pstmt1.setInt(9, subagent.getEntityAccountType());
                        {
                            if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                                Rep.cat.debug("dentityAccountType(SubAgentRepCredit)=" + this.dentityAccountType);
                                if (this.dentityAccountType.equals("1")) {
                                    pstmt1.setInt(10, 1);// if Combo is disabled then default to external
                                } else {
                                    pstmt1.setInt(10, this.entityAccountType);
                                }
                            } else {
                                pstmt1.setInt(10, 1);
                            }
                        }
                        /*
						 * Rep childrep = new Rep(); childrep.setRepId(this.repId); childrep.getRep(sessionData, context); pstmt1.setInt(10, childrep.getEntityAccountType());
                         */
                        pstmt1.executeUpdate();
                        TorqueHelper.closeStatement(pstmt1, null);

                        // Add rep credit
                        repCredit = new RepCredit();
                        repCredit.setRepId(Long.valueOf(this.repId));
                        repCredit.setLogonId(sessionData.getProperty("username"));
                        repCredit.setDescription(Languages.getString("com.debisys.customers.rep_add_payment", sessionData.getLanguage()));
                        repCredit.setAmount(new BigDecimal(this.creditLimit));
                        repCredit.setCreditLimit(new BigDecimal(this.creditLimit));
                        repCredit.setPriorCreditLimit(BigDecimal.ZERO);
                        repCredit.setAvailableCredit(new BigDecimal(this.creditLimit));
                        repCredit.setPriorAvailableCredit(BigDecimal.ZERO);
                        repCredit.setCommission(BigDecimal.ZERO);
                        repCredit.setDatetime(Calendar.getInstance());
                        if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                            Rep.cat.debug("dentityAccountType(RepCredit)=" + this.dentityAccountType);
                            if (this.dentityAccountType.equals("1")) {
                                repCredit.setEntityAccountType(1);// if Combo is disabled then default to external
                            } else {
                                repCredit.setEntityAccountType(this.entityAccountType);
                            }
                        } else {
                            repCredit.setEntityAccountType(1);
                        }
                        repCredit.setDepositDate(Calendar.getInstance());
                        //DTU-369 Payment Notifications
                        if (Rep.GetPaymentNotification(this.repId)) {

                            paymentNotification = new PaymentNotification();
                            paymentNotification.setActorId(Long.valueOf(this.repId));
                            paymentNotification.setLogonId(sessionData.getProperty("username"));
                            paymentNotification.setDescription(this.paymentDescription);
                            paymentNotification.setAmount(new BigDecimal(this.creditLimit));
                            paymentNotification.setCreditLimit(new BigDecimal(this.creditLimit));
                            paymentNotification.setPriorCreditLimit(BigDecimal.ZERO);
                            paymentNotification.setAvailableCredit(new BigDecimal(this.creditLimit));
                            paymentNotification.setPriorAvailableCredit(BigDecimal.ZERO);
                            paymentNotification.setCommission(BigDecimal.ZERO);
                            paymentNotification.setDatetime(DateUtil.formatCalendarDate(Calendar.getInstance(), null));
                            if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                                Rep.cat.debug("dentityAccountType(RepCredit)=" + this.dentityAccountType);
                                if (this.dentityAccountType.equals("1")) {
                                    paymentNotification.setEntityAccountType(1);// if Combo is disabled then default to external
                                } else {
                                    paymentNotification.setEntityAccountType(this.entityAccountType);
                                }
                            } else {
                                paymentNotification.setEntityAccountType(1);
                            }
                            paymentNotification.setActorName(this.businessName);
                            paymentNotification.setPaymentNotificationType(Rep.GetPaymentNotificationType(this.repId));
                            paymentNotification.setSmsNotificationPhone(Rep.GetSmsNotificationPhone(this.repId));
                            paymentNotification.setMailNotificationAddress(Rep.GetMailNotificationAddress(this.repId));
                        }
                    }
                } else if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))) {
                    repCredit = new RepCredit();
                    repCredit.setRepId(Long.valueOf(this.repId));
                    repCredit.setLogonId(sessionData.getProperty("username"));
                    repCredit.setDescription(Languages.getString("com.debisys.customers.rep_add_payment", sessionData.getLanguage()));
                    repCredit.setAmount(new BigDecimal(this.creditLimit));
                    repCredit.setCreditLimit(new BigDecimal(this.creditLimit));
                    repCredit.setPriorCreditLimit(BigDecimal.ZERO);
                    repCredit.setAvailableCredit(new BigDecimal(this.creditLimit));
                    repCredit.setPriorAvailableCredit(BigDecimal.ZERO);
                    repCredit.setCommission(BigDecimal.ZERO);
                    repCredit.setDatetime(Calendar.getInstance());
                    repCredit.setEntityAccountType(EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                    repCredit.setDepositDate(Calendar.getInstance());
                    //DTU-369 Payment Notifications
                    if (Rep.GetPaymentNotification(this.repId)) {
                        paymentNotification = new PaymentNotification();
                        paymentNotification.setActorId(Long.valueOf(this.repId));
                        paymentNotification.setLogonId(sessionData.getProperty("username"));
                        paymentNotification.setDescription(Languages.getString("com.debisys.customers.rep_add_payment", sessionData.getLanguage()));
                        paymentNotification.setAmount(new BigDecimal(this.creditLimit));
                        paymentNotification.setCreditLimit(new BigDecimal(this.creditLimit));
                        paymentNotification.setPriorCreditLimit(BigDecimal.ZERO);
                        paymentNotification.setAvailableCredit(new BigDecimal(this.creditLimit));
                        paymentNotification.setPriorAvailableCredit(BigDecimal.ZERO);
                        paymentNotification.setCommission(BigDecimal.ZERO);
                        paymentNotification.setDatetime(DateUtil.formatCalendarDate(Calendar.getInstance(), null));
                        paymentNotification.setEntityAccountType(EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                        paymentNotification.setActorName(this.businessName);
                        paymentNotification.setPaymentNotificationType(Rep.GetPaymentNotificationType(this.repId));
                        paymentNotification.setSmsNotificationPhone(Rep.GetSmsNotificationPhone(this.repId));
                        paymentNotification.setMailNotificationAddress(Rep.GetMailNotificationAddress(this.repId));
                    }
                } // End Else DIST_CHAIN_3_LEVEL

                // ******************  Rep credit and notification insertion ******************
                if (repCredit != null) {
                    RepCreditDao.insertRepCredit(repCredit);
                    if (paymentNotification != null) {
                        PaymentNotificationDao.insertPaymentNotification(paymentNotification);
                    }
                }

                // 1 2 3 4 5 6 7 8 9 10
                // (rep_id, logon_id, description, amount, credit_limit,
                // prior_credit_limit, available_credit, prior_available_credit,
                // commission, datetime)
            }

        } catch (Exception e) {
            Rep.cat.error("Error during addRep", e);
            System.out.println(String.format("Error during addRep : %s", e.toString()));
            throw new EntityException();
        } finally {
            try {
                TorqueHelper.closeConnection(conn, pstmt1, r);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * Updates a rep in the database
     *
     * @param sessionData
     * @param context
     * @throws EntityException Thrown if there is an error inserting the record
     * in the database.
     */
    public void updateRep(SessionData sessionData, ServletContext context) throws EntityException {
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            HashMap map = CountryInfo.getAllCountriesID();

            if (this.checkPermission(sessionData)) {

                // create a new rep object to compare changes after update
                Rep tmpRep = new Rep();
                tmpRep.setRepId(this.repId);
                tmpRep.getRep(sessionData, context);

                if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                    tmpRep.relocateNewRatePlansForActor();
                    //TODO: INSERTAUDIT GABRIEL    DebisysConstants.AUDIT_PARENT_CHANGE   old:tmpRep.getParentRepId()  new: this.getParentRepId()
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_REP, this.repId, DebisysConstants.AUDIT_PARENT_CHANGE,
                            tmpRep.getParentRepId(), this.getParentRepId());
                }

                String log = "";

                // EMUNOZ - AUDIT DBSY-1051
                LogChanges logChanges = new LogChanges(sessionData);
                logChanges.setContext(context);

                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new EntityException();
                }

                // pstmt = dbConn.prepareStatement(sql_bundle.getString("updateRep"));
                String sqlStr = Rep.sql_bundle.getString("updateRep");
                String additionalParams = "";

                boolean paymentType_ch = false, processType_ch = false, routingNumber_ch = false, accountNumber_ch = false, taxId_ch = false;
                boolean entityAccountType_ch = false;

                String oldValue = "";
                String reason = "";
                String reasonIni = "Last value: ";
                String reasonEnd = "Actual value: ";
                String changedBy = "Modified by: ";
                String username = sessionData.getProperty("username");

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE_REP)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.paymentType) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE_REP), String.valueOf(this.paymentType),
                        this.repId, reason)) {
                    additionalParams += ", payment_type = ?";
                    paymentType_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR_REP)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.processType) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR_REP), String.valueOf(this.processType),
                        this.repId, reason)) {
                    additionalParams += ",payment_processor = ?";
                    processType_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA_REP)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.routingNumber) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA_REP), String.valueOf(this.routingNumber), this.repId,
                        reason)) {
                    additionalParams += ", aba = ?";
                    routingNumber_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER_REP)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.accountNumber) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER_REP), String.valueOf(this.accountNumber),
                        this.repId, reason)) {
                    additionalParams += ", account_no = ?";
                    accountNumber_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID_REP)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.taxId) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID_REP), String.valueOf(this.taxId), this.repId, reason)) {
                    additionalParams += ", ssn = ?";
                    taxId_ch = true;
                }

                // DBSY-1072 eAccounts Interface
                String tempacct;
                if (this.dentityAccountType.equals("1")) {
                    tempacct = "1";
                } else {
                    tempacct = String.valueOf(this.entityAccountType);
                }
                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_REP)) + " ";
                reason = reasonIni + oldValue + reasonEnd + tempacct + " " + changedBy + username + "";

                if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {

                    if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_REP), tempacct, this.repId, reason)) {
                        additionalParams += ", EntityAccountType = ?";
                        entityAccountType_ch = true;

                        if (this.internalChildrenExist == 1) {
                            Rep.cat.debug("Doing global update for Rep");
                            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("doGlobalUpdate"));
                            pstmt.setLong(1, Long.parseLong(this.repId));
                            pstmt.setInt(2, 1);
                            pstmt.setString(3, sessionData.getProperty("username"));
                            pstmt.setString(4, "support");
                            pstmt.setString(5, "");
                            pstmt.setString(6, "");
                            Rep.cat.debug("param 1: " + this.repId);
                            Rep.cat.debug("param 2: " + "1");
                            Rep.cat.debug("param 3: " + sessionData.getProperty("username"));
                            Rep.cat.debug("param 4: " + "support");
                            pstmt.execute();
                        }
                    }
                }

                Rep.cat.debug("query:" + StringUtil.toString(sqlStr));
                sqlStr = sqlStr.replace("~~addParams~~", additionalParams);
                Rep.cat.debug("query:" + StringUtil.toString(sqlStr));

                pstmt = dbConn.prepareStatement(sqlStr);

                String strAccessLevel = sessionData.getProperty("access_level");
                String strRefId = sessionData.getProperty("ref_id");

                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                    pstmt.setDouble(1, Double.parseDouble(this.parentRepId));
                } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                    pstmt.setDouble(1, Double.parseDouble(strRefId));
                }
                pstmt.setString(2, this.businessName);

                String initials = this.contactFirst.substring(0, 1);
                if (this.contactMiddle == null || this.contactMiddle.trim().equals("")) {
                    initials = initials + "_";
                } else {
                    initials = initials + this.contactMiddle.substring(0, 1);
                }
                initials = initials + this.contactLast.substring(0, 1);

                // safe handling country data: for missing country data in db
                String safeCountryData = null;
                String safeMailCountryData = null;
                if (this.country != null && map.get(Integer.parseInt(this.country)) != null) {
                    safeCountryData = map.get(Integer.parseInt(this.country)).toString();
                }
                if (this.mailCountry != null && map.get(Integer.parseInt(this.mailCountry)) != null) {
                    safeMailCountryData = map.get(Integer.parseInt(this.mailCountry)).toString();
                }

                pstmt.setString(3, initials);
                pstmt.setString(4, this.contactFirst);
                pstmt.setString(5, this.contactLast);
                pstmt.setString(6, this.contactMiddle);
                pstmt.setString(7, this.address);
                pstmt.setString(8, this.city);
                pstmt.setString(9, this.state);
                pstmt.setString(10, this.zip);
                pstmt.setString(11, this.contactPhone);
                pstmt.setString(12, this.contactFax);
                pstmt.setString(13, this.contactEmail);
                pstmt.setString(14, this.country);
                pstmt.setString(15, this.legalBusinessname);
                pstmt.setInt(16, this.timeZoneId);

                if (safeCountryData != null) {
                    // ideally this shouldn't fail: all reps must have their Country correctly set.
                    pstmt.setString(17, safeCountryData);
                } else {
                    // but we'd better make sure we don't disrupt an update because of this
                    pstmt.setNull(17, java.sql.Types.VARCHAR);
                }
                // DBSY-931 System 2000
                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                        && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                        && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    pstmt.setString(18, this.salesmanid);
                    pstmt.setString(19, this.terms);
                } else {
                    pstmt.setString(18, tmpRep.getsalesmanid());// if not international then keep the same value...
                    pstmt.setString(19, tmpRep.getterms());
                }

                /**
                 * ***************************************************************************************
                 */
                /**
                 * ***************************************************************************************
                 */
                /*GEOLOCATION FEATURE*/
                Rep.cat.debug("latitude:" + Double.toString(this.latitude));
                pstmt.setDouble(20, this.latitude);

                Rep.cat.debug("longitude:" + Double.toString(this.longitude));
                pstmt.setDouble(21, this.longitude);

                Rep.cat.debug("addressValidationStatus:" + Integer.toString(this.addressValidationStatus));
                pstmt.setInt(22, this.addressValidationStatus);
                /*GEOLOCATION FEATURE*/
                /**
                 * ***************************************************************************************
                 */
                /**
                 * ***************************************************************************************
                 */

                //DTU-369 Payment Notifications
                pstmt.setBoolean(23, this.isPaymentNotifications());
                if (this.isPaymentNotificationSMS()) {
                    this.setPaymentNotificationType(0);
                } else if (this.isPaymentNotificationMail()) {
                    this.setPaymentNotificationType(1);
                }
                pstmt.setInt(24, this.getPaymentNotificationType());
                pstmt.setString(25, this.getSmsNotificationPhone());
                pstmt.setString(26, this.getMailNotificationAddress());

                //DTU-480: Low Balance Alert Message SMS and/or email
                pstmt.setBoolean(27, this.isBalanceNotifications());
                if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays()) {
                    this.setBalanceNotificationType(2);
                } else if (this.isBalanceNotificationTypeValue()) {
                    this.setBalanceNotificationType(0);
                } else if (this.isBalanceNotificationTypeDays()) {
                    this.setBalanceNotificationType(1);
                }
                pstmt.setInt(28, this.getBalanceNotificationType());
                if (this.isBalanceNotificationSMS()) {
                    this.setBalanceNotificationNotType(0);
                } else if (this.isBalanceNotificationMail()) {
                    this.setBalanceNotificationNotType(1);
                }
                pstmt.setInt(29, this.getBalanceNotificationNotType());
                pstmt.setInt(30, this.getBalanceNotificationDays());
                pstmt.setDouble(31, this.getBalanceNotificationValue());

                int i = 32;
                if (paymentType_ch) {
                    Rep.cat.debug("after paymenttype:" + this.paymentType);
                    pstmt.setInt(i, this.paymentType);
                    i++;
                }
                if (processType_ch) {
                    Rep.cat.debug("after processtype:" + this.processType);
                    pstmt.setInt(i, this.processType);
                    i++;
                }
                if (routingNumber_ch) {
                    Rep.cat.debug("after routingnumber:" + StringUtil.toString(this.routingNumber));
                    pstmt.setString(i, this.routingNumber);
                    i++;
                }
                if (accountNumber_ch) {
                    Rep.cat.debug("after accountnumber:" + StringUtil.toString(this.accountNumber));
                    pstmt.setString(i, this.accountNumber);
                    i++;
                }
                if (taxId_ch) {
                    Rep.cat.debug("after taxId:" + StringUtil.toString(this.taxId));
                    pstmt.setString(i, this.taxId);
                    i++;
                }

                if (entityAccountType_ch) {
                    if (this.dentityAccountType.equals("1")) {
                        pstmt.setInt(i, 1);// if Combo is disabled then default to external
                        Rep.cat.debug("dentityAccountType=" + this.dentityAccountType);
                    } else {
                        pstmt.setInt(i, this.entityAccountType);
                    }
                    i++;
                }

                Rep.cat.debug("after repId:" + StringUtil.toString(this.repId));
                pstmt.setDouble(i, Double.parseDouble(this.repId));

                pstmt.executeUpdate();
                TorqueHelper.closeStatement(pstmt, null);

                if (entityAccountType_ch) {
                    //TODO: INSERTAUDIT GABRIEL    EntityAccountType Internal=0/External=1
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_REP, this.repId, DebisysConstants.AUDIT_INTERNAL_EXTERNAL_CHANGE,
                            (this.dentityAccountType.equals("1") ? "0" : "1"), this.dentityAccountType);
                }

                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    // When MX update specific fields
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepMx"));
                    pstmt.setString(1, this.sharedBalance);
                    pstmt.setString(2, this.physCounty);
                    pstmt.setString(3, this.physColony);
                    pstmt.setString(4, this.physDelegation);
                    if (this.mailState != null) {
                        pstmt.setString(5, this.mailAddress);
                        pstmt.setString(6, this.mailCity);
                        pstmt.setString(7, this.mailState);
                        pstmt.setString(8, this.mailZip);
                        if (safeMailCountryData != null) {
                            pstmt.setString(9, safeMailCountryData);
                        } else {
                            pstmt.setNull(9, java.sql.Types.VARCHAR);
                        }
                        pstmt.setString(10, this.mailDelegation);
                        pstmt.setString(11, this.mailCounty);
                        pstmt.setString(12, this.mailColony);
                        pstmt.setInt(18, Integer.parseInt(this.mailCountry));
                    } else {
                        pstmt.setString(5, this.address);
                        pstmt.setString(6, this.city);
                        pstmt.setString(7, this.state);
                        pstmt.setString(8, this.zip);
                        if (safeCountryData != null) {
                            pstmt.setString(9, safeCountryData);
                        } else {
                            pstmt.setNull(9, java.sql.Types.VARCHAR);
                        }
                        pstmt.setString(10, this.physDelegation);
                        pstmt.setString(11, this.physCounty);
                        pstmt.setString(12, this.physColony);
                        pstmt.setInt(18, Integer.parseInt(this.country));
                    }
                    pstmt.setString(13, this.bankName);
                    pstmt.setString(14, this.businessHours);
                    pstmt.setString(15, this.businessTypeId);
                    pstmt.setInt(16, this.accountTypeId);
                    pstmt.setInt(17, this.merchantSegmentId);
                    pstmt.setLong(19, this.invoiceType);
                    pstmt.setString(20, this.repId);
                    
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, null);
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("deleteContactsMx"));
                    pstmt.setString(1, this.repId);
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, null);

                    Iterator itContacts = this.vecContacts.iterator();
                    while (itContacts.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) itContacts.next();
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertContactMx"));
                        pstmt.setDouble(1, Double.parseDouble(this.repId));
                        pstmt.setString(2, vecTemp.get(0).toString());
                        pstmt.setString(3, vecTemp.get(1).toString());
                        pstmt.setString(4, vecTemp.get(2).toString());
                        pstmt.setString(5, vecTemp.get(3).toString());
                        pstmt.setString(6, vecTemp.get(4).toString());
                        pstmt.setString(7, vecTemp.get(5).toString());
                        pstmt.setString(8, vecTemp.get(6).toString());
                        pstmt.executeUpdate();
                        TorqueHelper.closeStatement(pstmt, null);
                    }
                }// End of if when MX update specific fields

                boolean fieldsChanged = false;
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                    if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                        log = log + "iso_id|" + tmpRep.getParentRepId() + "|" + this.getParentRepId() + ",";
                        fieldsChanged = true;
                    }
                }

                if (!this.getBusinessName().equals(tmpRep.getBusinessName())) {
                    log = log + "businessname|" + tmpRep.getBusinessName() + "|" + this.getBusinessName() + ",";
                    fieldsChanged = true;
                }

                if (!this.getTaxId().equals(tmpRep.getTaxId())) {
                    log = log + "tax_id|" + tmpRep.getTaxId() + "|" + this.getTaxId() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactEmail().equals(tmpRep.getContactEmail())) {
                    log = log + "email|" + tmpRep.getContactEmail() + "|" + this.getContactEmail() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactFirst().equals(tmpRep.getContactFirst())) {
                    log = log + "contact_first|" + tmpRep.getContactFirst() + "|" + this.getContactFirst() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactLast().equals(tmpRep.getContactLast())) {
                    log = log + "contact_last|" + tmpRep.getContactLast() + "|" + this.getContactLast() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactMiddle().equals(tmpRep.getContactMiddle())) {
                    log = log + "contact_middle|" + tmpRep.getContactMiddle() + "|" + this.getContactMiddle() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactPhone().equals(tmpRep.getContactPhone())) {
                    log = log + "contact_phone|" + tmpRep.getContactPhone() + "|" + this.getContactPhone() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactFax().equals(tmpRep.getContactFax())) {
                    log = log + "contact_fax|" + tmpRep.getContactFax() + "|" + this.getContactFax() + ",";
                    fieldsChanged = true;
                }

                if (!this.getAddress().equals(tmpRep.getAddress())) {
                    log = log + "address|" + tmpRep.getAddress() + "|" + this.getAddress() + ",";
                    fieldsChanged = true;
                }

                if (!this.getCity().equals(tmpRep.getCity())) {
                    log = log + "city|" + tmpRep.getCity() + "|" + this.getCity() + ",";
                    fieldsChanged = true;
                }
                if (!this.getState().equals(tmpRep.getState())) {
                    log = log + "state|" + tmpRep.getState() + "|" + this.getState() + ",";
                    fieldsChanged = true;
                }
                if (!this.getZip().equals(tmpRep.getZip())) {
                    log = log + "zip|" + tmpRep.getZip() + "|" + this.getZip() + ",";
                    fieldsChanged = true;
                }
                if (!this.getCountry().equals(tmpRep.getCountry())) {
                    log = log + "country|" + tmpRep.getCountry() + "|" + this.getCountry() + ",";
                    fieldsChanged = true;
                }
                if (!this.getRoutingNumber().equals(tmpRep.getRoutingNumber())) {
                    log = log + "aba|" + tmpRep.getRoutingNumber() + "|" + this.getRoutingNumber() + ",";
                    fieldsChanged = true;
                }
                if (!this.getAccountNumber().equals(tmpRep.getAccountNumber())) {
                    log = log + "account_no|" + tmpRep.getAccountNumber() + "|" + this.getAccountNumber() + ",";
                    fieldsChanged = true;
                }

                // DBSY-1072 eAccounts Interface
                if (this.getEntityAccountType() != tmpRep.getEntityAccountType()) {
                    log = log + "entityAccountType|" + tmpRep.getEntityAccountType() + "|" + this.getEntityAccountType() + ",";
                    fieldsChanged = true;
                }

                if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                    if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                        log = log + "iso_id|" + tmpRep.getParentRepId() + "|" + this.getRepId() + ",";
                        fieldsChanged = true;

                        // Subagent changed so lets make sure new subagent can
                        // handle it
                        try {
                            Rep oldSubAgent = new Rep();
                            oldSubAgent.setRepId(tmpRep.getParentRepId());
                            oldSubAgent.getSubAgent(sessionData, context);

                            Rep newSubAgent = new Rep();
                            newSubAgent.setRepId(this.parentRepId);
                            newSubAgent.getSubAgent(sessionData, context);
                            if ((oldSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
                                    && newSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
                                    && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                                    || (!DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (tmpRep.getRepCreditType().equals("1") || tmpRep.getRepCreditType().equals("5")))) {
                                double SubAgentCreditLimit = oldSubAgent.getCurrentLiabilityLimit();
                                double SubAgentRunningLiability = oldSubAgent.getCurrentRunningLiability();

                                // Inserting old Rep Merchant Credit
                                // add an entry to show the rep credits being
                                // used
                                // 1 2 3 4 5 6 7 8 9 10
                                // (rep_id, merchant_id, logon_id, description,
                                // amount,
                                // available_credit, prior_available_credit,
                                // datetime, subagent_entityaccounttype, rep_entityaccounttype)
                                Rep.cat.debug(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                                pstmt.setDouble(1, Double.parseDouble(oldSubAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Rep.changed_subagents", sessionData.getLanguage()));
                                pstmt.setDouble(5, 0);
                                pstmt.setDouble(6, SubAgentCreditLimit - SubAgentRunningLiability);
                                pstmt.setDouble(7, SubAgentCreditLimit - SubAgentRunningLiability);
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, oldSubAgent.getEntityAccountType());
                                Rep childrep = new Rep();
                                childrep.setRepId(this.repId);
                                childrep.getRep(sessionData, context);
                                pstmt.setInt(10, childrep.getEntityAccountType());
                                pstmt.executeUpdate();
                                TorqueHelper.closeStatement(pstmt, null);

                                SubAgentCreditLimit = newSubAgent.getCurrentLiabilityLimit();
                                SubAgentRunningLiability = newSubAgent.getCurrentRunningLiability();

                                // Inserting new Rep Merchant Credit
                                // add an entry to show the rep credits being
                                // used
                                // 1 2 3 4 5 6 7 8 9 10
                                // (rep_id, merchant_id, logon_id, description,
                                // amount,
                                // available_credit, prior_available_credit,
                                // datetime, subagent_entityaccounttype, rep_entityaccounttype)
                                Rep.cat.debug(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                                pstmt.setDouble(1, Double.parseDouble(newSubAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Rep.changed_from_subagent", sessionData.getLanguage()));
                                pstmt.setDouble(5, 0);
                                pstmt.setDouble(6, SubAgentCreditLimit - SubAgentRunningLiability);
                                pstmt.setDouble(7, SubAgentCreditLimit - SubAgentRunningLiability);
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, newSubAgent.getEntityAccountType());
                                pstmt.setInt(10, childrep.getEntityAccountType());
                                pstmt.executeUpdate();
                                TorqueHelper.closeStatement(pstmt, null);
                            } else {
                                double availRepCredit = Double.parseDouble(tmpRep.getCreditLimit()) - Double.parseDouble(tmpRep.getRunningLiability());

                                // set the merchants credit to his available
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateSubAgentLiabilityLimit"));

                                boolean creditPrepaidInt = false;
                                // pstmt.setDouble(1, availSubAgentCredit);
                                if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                        && ((oldSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
                                        && newSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
                                        || (oldSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)
                                        && newSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID))) && tmpRep.getRepCreditType().equals("1")) {
                                    creditPrepaidInt = true;
                                    pstmt.setDouble(1, availRepCredit);//0
                                } else {
                                    pstmt.setDouble(1, 0);
                                }
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.executeUpdate();
                                TorqueHelper.closeStatement(pstmt, null);

                                // set the old rep credit to his credit limit
                                // minus what was
                                // used by the merchant moving
                                oldSubAgent.setPaymentAmount(Double.toString(availRepCredit));

                                oldSubAgent.setPaymentDescription("Rep moved to another Subagent.");
                                /* BEGIN MiniRelease 9.0 - Added by YH */
                                oldSubAgent.makeSubAgentPayment(sessionData, context, creditPrepaidInt);

                                /* END MiniRelease 9.0 - Added by YH */
                                // set the old rep credit to his credit limit
                                // minus what was
                                // used by the merchant moving
                                newSubAgent.setPaymentAmount(Double.toString(Double.parseDouble(this.creditLimit) * -1));

                                newSubAgent.setPaymentDescription("Rep moved from another Subagent.");
                                /* BEGIN MiniRelease 9.0 - Added by YH */
                                newSubAgent.makeSubAgentPayment(sessionData, context, creditPrepaidInt);

                                /* END MiniRelease 9.0 - Added by YH */
                                // log rep merchant credit to old rep since
                                // merchant was
                                // removed
                                double subagentCreditLimit = oldSubAgent.getCurrentSubAgentCreditLimit(sessionData);
                                double subagentAssignedCredit = oldSubAgent.getAssignedCreditSubAgent(sessionData);
                                double newSubagentAvailableCredit = subagentCreditLimit - subagentAssignedCredit;

                                // add an entry to show the rep credits being
                                // used
                                // 1 2 3 4 5 6 7 8 9 10
                                // (rep_id, merchant_id, logon_id, description,
                                // amount,
                                // available_credit, prior_available_credit,
                                // datetime, subagent_entityaccounttype, rep_entityaccounttype)
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                                pstmt.setDouble(1, Double.parseDouble(oldSubAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Rep.changed_subagents", sessionData.getLanguage()));
                                pstmt.setDouble(5, availRepCredit);
                                pstmt.setDouble(6, newSubagentAvailableCredit);
                                pstmt.setDouble(7, Double.parseDouble(oldSubAgent.getAvailableCredit()));
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, oldSubAgent.getEntityAccountType());
                                Rep childrep = new Rep();
                                childrep.setRepId(this.repId);
                                childrep.getRep(sessionData, context);
                                pstmt.setInt(10, childrep.getEntityAccountType());
                                pstmt.executeUpdate();
                                TorqueHelper.closeStatement(pstmt, null);

                                subagentCreditLimit = newSubAgent.getCurrentRepCreditLimit(sessionData);
                                subagentAssignedCredit = newSubAgent.getAssignedCredit(sessionData);
                                newSubagentAvailableCredit = subagentCreditLimit - subagentAssignedCredit;
                                // add an entry to show the rep credits being
                                // used
                                // 1 2 3 4 5 6 7 8 9 10
                                // (rep_id, merchant_id, logon_id, description,
                                // amount,
                                // available_credit, prior_available_credit,
                                // datetime, subagent_entityaccounttype, rep_entityaccounttype)
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                                pstmt.setDouble(1, Double.parseDouble(newSubAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Rep.changed_from_subagent", sessionData.getLanguage()));
                                pstmt.setDouble(5, Double.parseDouble(this.creditLimit) * -1);
                                pstmt.setDouble(6, newSubagentAvailableCredit);
                                pstmt.setDouble(7, Double.parseDouble(newSubAgent.getAvailableCredit()));
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, newSubAgent.getEntityAccountType());
                                pstmt.setInt(10, childrep.getEntityAccountType());
                                pstmt.executeUpdate();
                                TorqueHelper.closeStatement(pstmt, null);
                            }
                        } catch (Exception e) {
                            Rep.cat.debug("Error while updating the SubAgent change insertSubAgentRepCredit ", e);
                        }
                    }
                }

                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {// When
                    // MX,
                    // logs
                    // specific
                    // changes
                    if (!this.getSharedBalance().equals(tmpRep.getSharedBalance())) {
                        log = log + "SharedBalance|" + tmpRep.getSharedBalance() + "|" + this.getSharedBalance() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysCounty().equals(tmpRep.getPhysCounty())) {
                        log = log + "phys_county|" + tmpRep.getPhysCounty() + "|" + this.getPhysCounty() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysColony().equals(tmpRep.getPhysColony())) {
                        log = log + "phys_colony|" + tmpRep.getPhysColony() + "|" + this.getPhysColony() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysDelegation().equals(tmpRep.getPhysDelegation())) {
                        log = log + "phys_delegation|" + tmpRep.getPhysDelegation() + "|" + this.getPhysDelegation() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailAddress().equals(tmpRep.getMailAddress())) {
                        log = log + "mailAddress|" + tmpRep.getMailAddress() + "|" + this.getMailAddress() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCity().equals(tmpRep.getMailCity())) {
                        log = log + "mailCity|" + tmpRep.getMailCity() + "|" + this.getMailCity() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailState().equals(tmpRep.getMailState())) {
                        log = log + "mailState|" + tmpRep.getMailState() + "|" + this.getMailState() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailZip().equals(tmpRep.getMailZip())) {
                        log = log + "mailZip|" + tmpRep.getMailZip() + "|" + this.getMailZip() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCountry().equals(tmpRep.getMailCountry())) {
                        log = log + "mailCountry|" + tmpRep.getMailCountry() + "|" + this.getMailCountry() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailDelegation().equals(tmpRep.getMailDelegation())) {
                        log = log + "mailDelegation|" + tmpRep.getMailDelegation() + "|" + this.getMailDelegation() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCounty().equals(tmpRep.getMailCounty())) {
                        log = log + "mailCounty|" + tmpRep.getMailCounty() + "|" + this.getMailCounty() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailColony().equals(tmpRep.getMailColony())) {
                        log = log + "mailColony|" + tmpRep.getMailColony() + "|" + this.getMailColony() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBankName().equals(tmpRep.getBankName())) {
                        log = log + "bankName|" + tmpRep.getBankName() + "|" + this.getBankName() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBusinessHours().equals(tmpRep.getBusinessHours())) {
                        log = log + "businessHours|" + tmpRep.getBusinessHours() + "|" + this.getBusinessHours() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBusinessTypeId().equals(tmpRep.getBusinessTypeId())) {
                        log = log + "businessType|" + tmpRep.getBusinessTypeId() + "|" + this.getBusinessTypeId() + ",";
                        fieldsChanged = true;
                    }
                    if (this.getAccountTypeId() != tmpRep.getAccountTypeId()) {
                        log = log + "accountType|" + tmpRep.getAccountTypeId() + "|" + this.getAccountTypeId() + ",";
                        fieldsChanged = true;
                    }
                    if (this.getMerchantSegmentId() != tmpRep.getMerchantSegmentId()) {
                        log = log + "merchantSegment|" + tmpRep.getMerchantSegmentId() + "|" + this.getMerchantSegmentId() + ",";
                        fieldsChanged = true;
                    }
                }// End of if when MX, logs specific changes
                if (fieldsChanged) {
                    Log.write(sessionData, "Rep updated.[" + log + "]", DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                }
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch(SQLException localSqlExeption){
            DbUtil.processSqlException(localSqlExeption);
            throw new EntityException();
        }
        catch (Exception e) {
            Rep.cat.error("Error during updateRep", e);
            throw new EntityException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * Given a rep_id, populates merchant object populated from the database, or
     * null if no such rep_id exists.
     *
     * @param sessionData
     * @param context
     * @throws com.debisys.exceptions.CustomerException Thrown on database
     * errors
     */
    public void getRep(SessionData sessionData, ServletContext context) throws CustomerException {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = "";
            if (!strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = Rep.sql_bundle.getString("getRep");
            } else {
                strSQL = Rep.sql_bundle.getString("getRepCarrier");
            }
            // getRep=select r.* from reps r (nolock) where r.rep_id=?
            // permissions are built into the queries
            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " INNER JOIN merchants m WITH (NOLOCK) ON m.rep_id = r.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "WHERE r.type = 1 AND rc.RepID = " + strRefId + " AND r.rep_id = ? ";

            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id = " + strRefId;
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id="
                            + strRefId + ")) ";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " and iso_id= " + strRefId + ") ";

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQL = strSQL + " and r.iso_id=" + strRefId;
            } else if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.CARRIER)) {
                // do nothing but dont erase the sql statement
            } else {
                strSQL = "";
            }
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.repId = rs.getString("rep_id");
                this.parentRepId = rs.getString("iso_id");
                this.oldParentRepId = rs.getString("iso_id");
                this.businessName = rs.getString("businessname");
                this.contactFirst = rs.getString("firstname");
                this.contactLast = rs.getString("lastname");
                this.contactMiddle = rs.getString("minitial");
                this.contactEmail = rs.getString("email");
                this.contactPhone = rs.getString("phone");
                this.contactFax = rs.getString("fax");
                this.address = rs.getString("address");
                this.city = rs.getString("city");
                this.state = rs.getString("state");
                this.zip = rs.getString("zip");
                this.country = rs.getString("country_id");

                this.setAddressValidationStatus(rs.getInt("AddressValidationStatus"));
                this.setLatitude(rs.getDouble("latitude"));
                this.setLongitude(rs.getDouble("longitude"));

                this.runningLiability = Double.toString(this.getAssignedCredit(sessionData));
                this.creditLimit = NumberUtil.formatAmount(rs.getString("LiabilityLimit"));
                this.repCreditType = rs.getString("rep_credit_type");
                this.oldRepCreditType = this.repCreditType; // defaulting to avoid Nullpointer when doing a Rep payment in a Merchant rep-switch
                this.taxId = rs.getString("ssn");
                this.routingNumber = rs.getString("aba");
                this.accountNumber = rs.getString("account_no");

                this.availableCredit = Double.toString(Double.parseDouble(this.creditLimit) - Double.parseDouble(this.runningLiability));

                this.physCounty = rs.getString("phys_county");
                this.physColony = rs.getString("phys_colony");
                this.physDelegation = rs.getString("phys_delegation");
                this.mailAddress = rs.getString("mail_address");
                this.mailCity = rs.getString("mail_city");
                this.mailState = rs.getString("mail_state");
                this.mailZip = rs.getString("mail_zip");
                this.mailColony = rs.getString("mail_colony");
                this.mailDelegation = rs.getString("mail_delegation");
                this.mailCounty = rs.getString("mail_county");
                this.mailCountry = rs.getString("mail_country_id");
                this.bankName = rs.getString("BankName");
                this.businessHours = rs.getString("BusinessHours");
                this.businessTypeId = rs.getString("sic_code");
                this.accountTypeId = rs.getInt("AccountTypeID");
                this.merchantSegmentId = rs.getInt("MerchantSegmentID");
                this.sharedBalance = rs.getString("SharedBalance");
                /* BEGIN Release 14.0 - Added by NM */
                this.legalBusinessname = rs.getString("legal_businessname");
                /* END Release 14.0 - Added by NM */

                // #BEGIN Added by NM - Release 21
                this.paymentType = rs.getInt("payment_type");
                this.processType = rs.getInt("payment_processor");
                // #END Added by NM - Release 21
                this.timeZoneId = rs.getInt("TimeZoneID");
                if (this.timeZoneId == 0) {
                    this.timeZoneId = Integer.parseInt(TimeZone.getDefaultTimeZone().get(0).toString());
                }
                // DBSY-931 System 2000
                String temp = rs.getString("salesman_id");
                if (StringUtil.isNotEmptyStr(temp)) {
                    if (this.verifyid(temp.trim(), Rep.getIsoID(strRefId, strAccessLevel))) {
                        this.salesmanid = temp;
                        this.salesmanname = this.getsalesmannamebyid(temp.trim(), strRefId);
                    }
                }
                String temp2 = rs.getString("terms");
                if (StringUtil.isNotEmptyStr(temp2)) {
                    this.terms = temp2;
                }

                this.entityAccountType = rs.getInt("EntityAccountType");

                //DTU-369 Payment Notifications
                this.paymentNotifications = rs.getBoolean("payment_notifications");
                this.paymentNotificationType = rs.getInt("payment_notification_type");
                if (this.paymentNotificationType == 0) {
                    this.paymentNotificationSMS = true;
                } else if (this.paymentNotificationType == 1) {
                    this.paymentNotificationMail = true;
                }
                this.smsNotificationPhone = rs.getString("sms_notification_phone");
                this.mailNotificationAddress = rs.getString("mail_notification_address");

                //DTU-480: Low Balance Alert Message SMS and/or email
                this.balanceNotifications = rs.getBoolean("balance_notifications");
                this.balanceNotificationType = rs.getInt("balance_notification_type");
                switch (this.balanceNotificationType) {
                    case 0:
                        this.balanceNotificationTypeValue = true;
                        break;
                    case 1:
                        this.balanceNotificationTypeDays = true;
                        break;
                    case 2:
                        this.balanceNotificationTypeValue = true;
                        this.balanceNotificationTypeDays = true;
                        break;
                    default:
                        break;
                }
                this.balanceNotificationValue = rs.getDouble("balance_notification_value");
                this.balanceNotificationDays = rs.getInt("balance_notification_days");
                this.balanceNotificationNotType = rs.getInt("balance_notification_not_type");
                if (this.balanceNotificationNotType == 0) {
                    this.balanceNotificationSMS = true;
                } else if (this.balanceNotificationNotType == 1) {
                    this.balanceNotificationMail = true;
                }
                if (DbUtil.hasColumn(rs, "invoiceType")) {
                    this.invoiceType = rs.getLong("invoiceType");
                }
                if (DbUtil.hasColumn(rs, "currencyCode")) {
                    this.currencyCode = rs.getString("currencyCode");
                }

            } else {
                this.addFieldError("error", "Rep not found.");
            }
            rs.close();
            pstmt.close();
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {// When MX
                // retrieve
                // contacts
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getContactsMx"));
                pstmt.setString(1, this.repId);
                rs = pstmt.executeQuery();
                this.vecContacts = new Vector();
                while (rs.next()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(rs.getString("ContactTypeID"));
                    vecTemp.add(rs.getString("ContactName"));
                    vecTemp.add(rs.getString("ContactPhone"));
                    vecTemp.add(rs.getString("ContactFax"));
                    vecTemp.add(rs.getString("ContactEmail"));
                    vecTemp.add(rs.getString("ContactCellPhone"));
                    vecTemp.add(rs.getString("ContactDepartment"));
                    this.vecContacts.add(vecTemp);
                }

                if (this.vecContacts.isEmpty()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(Merchant.getContactTypeByCode("OTHER", sessionData).get(0).toString());
                    vecTemp.add(((this.contactFirst != null) ? this.contactFirst : ""));
                    vecTemp.add(((this.contactPhone != null) ? this.contactPhone : ""));
                    vecTemp.add(((this.contactFax != null) ? this.contactFax : ""));
                    vecTemp.add(((this.contactEmail != null) ? this.contactEmail : ""));
                    vecTemp.add("");
                    vecTemp.add("");
                    this.vecContacts.add(vecTemp);
                }

                rs.close();
                pstmt.close();
            }// End of if when MX retrieve contacts
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            Rep.cat.error("Error during getRep", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public String getRepEmailById(String repId) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String email = "";
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = "SELECT email FROM reps WHERE rep_id = " + repId;
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                email = "" + rs.getString("email");
                if (email.equals("null") || email.equals("NULL")) {
                    email = "";
                }
            } else {
                Rep.cat.error("Error Rep not found.", null);
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            Rep.cat.error("Error during getRepEmailById", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return email;
    }

    public double getAssignedCredit(SessionData sessionData) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double assignedCredit = 0;
        try {

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            // select r.runningliability as assigned_credit from reps r (nolock)
            // where r.rep_id = ?
            String strSQL = "";
            if (!strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = Rep.sql_bundle.getString("getRepAssignedCredit");
            } else {
                strSQL = Rep.sql_bundle.getString("getRepAssignedCreditCarrier");
            }
            // permissions are built into the queries

            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " INNER JOIN merchants m WITH (NOLOCK) ON m.rep_id = r.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "WHERE r.type = 1 AND rc.RepID = " + strRefId + " AND r.rep_id = ? ";
            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id = " + strRefId;
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id="
                            + strRefId + ")) ";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " and iso_id= " + strRefId + ") ";

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQL = strSQL + " and r.iso_id=" + strRefId;
            } else if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.CARRIER)) {
                // do nothing
            } else {
                strSQL = "";
            }

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            Rep.cat.debug(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                assignedCredit = rs.getDouble("assigned_credit");
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_record", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getAssignedCredit", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return assignedCredit;
    }

    public double getCurrentRepCreditLimit(SessionData sessionData) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double creditLimit = 0;
        try {

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getRepLiabilityLimit");
            // permissions are built into the queries
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id = " + strRefId;
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id="
                            + strRefId + ")) ";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " and iso_id= " + strRefId + ") ";

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQL = strSQL + " and r.iso_id=" + strRefId;
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                // do nothing
            } else {
                strSQL = "";
            }

            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                creditLimit = rs.getDouble("liabilitylimit");
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_record", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getCurrentRepCreditLimit", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return creditLimit;
    }

    // Check to make sure you are a valid subagent for an iso or agent
    // Used for addRep to make sure you selected a valid subagent in your
    // downline
    private boolean checkSubAgentPermission(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAllowed = false;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            String strSQL = Rep.sql_bundle.getString("checkIsoAgentRep");
            // String strDistChainType =
            // sessionData.getProperty("dist_chain_type");//Never read
            String strAccessLevel = sessionData.getProperty("access_level");
            String strRefId = sessionData.getProperty("ref_id");
            // iso
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and rep_id="
                        + this.parentRepId + " and iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT
                        + " and iso_id= " + strRefId + "))";
            } // agent
            else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and rep_id="
                        + this.parentRepId + " and iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT
                        + " and rep_id= " + strRefId + "))";

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                // this means the person logged in is the subagent
                strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and rep_id="
                        + strRefId + ")";

            } else {
                isAllowed = false;
                return isAllowed;
            }
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                isAllowed = true;
            } else {
                isAllowed = false;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkSubAgentPermission", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isAllowed;
    }

    public String generatePartiallyStaticId(Vector<String> v) {
        boolean isUnique = false;
        String strEntityNumber = "";
        String partialcode = v.get(1);
        while (!isUnique) {
            strEntityNumber = this.generateSRandomNumber(partialcode);
            isUnique = this.checkEntityNumber(strEntityNumber);
        }
        return strEntityNumber;
    }

    private String generateSRandomNumber(String str) {
        StringBuffer s = new StringBuffer();
        int nextInt = 0;

        if (str.length() > 7) {
            str = str.substring(0, 5);
        } else if (str.length() == 0) {
            int nextdInt = (int) ((Math.random() * 9) + 1);
            s = s.append(nextdInt);
            str = "1";// adjusting size to 1
        } else {
            s = s.append(str);
        }

        for (int i = 1; i <= 12 - str.length(); i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    public String generateRepId() {
        boolean isUnique = false;
        String strEntityNumber = "";
        while (!isUnique) {
            strEntityNumber = this.generateRandomNumber();
            isUnique = this.checkEntityNumber(strEntityNumber);
        }
        return strEntityNumber;
    }

    private String generateRandomNumber() {
        StringBuffer s = new StringBuffer();
        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);

        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private boolean checkEntityNumber(String strEntityNumber) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isUnique = true;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkMerchantId"));
            pstmt.setString(1, strEntityNumber);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                // duplicate key found
                Rep.cat.error("Duplicate found:" + strEntityNumber);
                isUnique = false;
            }
            pstmt.close();
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkRepId"));
            pstmt.setString(1, strEntityNumber);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                // duplicate key found
                Rep.cat.error("Duplicate found:" + strEntityNumber);
                isUnique = false;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkEntityNumber", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isUnique;
    }

    public String getRepName(String strRepId) throws CustomerException {
        String strRepName = "";
        if (strRepId != null && !strRepId.equals("")) {
            Connection dbConn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }
                // get a count of total matches
                String strSQL = Rep.sql_bundle.getString("getRep");
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setDouble(1, Double.parseDouble(strRepId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    strRepName = rs.getString("businessname");
                }
            } catch (Exception e) {
                Rep.cat.error("Error during getRepName", e);
                throw new CustomerException();
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    Rep.cat.error("Error during closeConnection", e);
                }
            }
        }
        return strRepName;
    }
    
    
    public Rep getRepresentantByRepId (String strRepId) throws CustomerException {
        Rep objRep = new Rep();
        if (strRepId != null && !strRepId.equals("")) {
            Connection dbConn = null;
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            try {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }
                // get a count of total matches
                String strSQL = Rep.sql_bundle.getString("getRep");
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setDouble(1, Double.parseDouble(strRepId));
                rs = pstmt.executeQuery();
                if (rs.next()) {
                    objRep.setBusinessName(rs.getString("businessname"));
                    objRep.setRepCreditType(rs.getString("rep_credit_type"));
                }
            } catch (Exception e) {
                Rep.cat.error("Error during getRepName", e);
                return null;
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    Rep.cat.error("Error during closeConnection", e);
                }
            }
        }
        return objRep;
    }
    
    

    public void makePayment(SessionData sessionData, ServletContext context, boolean creditPrepaidInt) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            if (this.checkPermission(sessionData)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) || !NumberUtil.isNumeric(this.paymentAmount)) {
                    this.paymentAmount = "0.00";
                }
                // select r.rep_credit_type, r.LiabilityLimit,
                // r.runningliability from
                // reps r (nolock) where r.rep_id = ?
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                rs.next();
                double currentLiabilityLimit = Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));
                double currentRunningLiability = Double.parseDouble(NumberUtil.formatAmount(rs.getString("runningliability")));
                double dblPaymentAmount = Double.parseDouble(this.paymentAmount);
                // new value for prepaid based reps
                double dblNewCreditLimit = (currentLiabilityLimit - currentRunningLiability) + dblPaymentAmount;
                // new value for credit based reps
                double dblNewRunningLiability = currentRunningLiability - dblPaymentAmount;
                String currentCreditType = this.oldRepCreditType;// rs.getString("rep_credit_type");
                TorqueHelper.closeStatement(pstmt, rs);

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                    // update reps set liabilityLimit = ?, runningliability=?,
                    // rep_credit_type=? where rep_id = ?
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("makePayment"));
                    pstmt.setDouble(1, 0);
                    pstmt.setDouble(2, 0);
                    pstmt.setInt(3, Integer.parseInt(this.repCreditType));
                    pstmt.setDouble(4, Double.parseDouble(this.repId));
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, rs);
                } else if (dblNewRunningLiability < 0 && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                    // addFieldError("error","Payments can not be made when
                    // there is
                    // zero assigned credit");
                    this.addFieldError("error", "9");
                } else if (dblNewRunningLiability > currentLiabilityLimit && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                    // addFieldError("error","Assigned credit exceeds credit
                    // limit");
                    this.addFieldError("error", "8");
                } else if ((dblNewCreditLimit < 0) && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                    // addFieldError("error","Credit limit must be greater
                    // than 0");
                    this.addFieldError("error", "7");
                } else {
                    Rep tmpRep = new Rep();
                    tmpRep.setRepId(this.repId);
                    tmpRep.getRep(sessionData, context);
                    insertBalanceHistory(DebisysConstants.REP_TYPE_REP, this.repId, tmpRep.parentRepId,
                            tmpRep.repCreditType, tmpRep.sharedBalance, tmpRep.entityAccountType, tmpRep.runningLiability, tmpRep.creditLimit, sessionData.getProperty("username"), false);

                    // makePayment=update reps set liabilityLimit = ?,
                    // runningliability=?, rep_credit_type=? where rep_id =
                    // ?
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("makePayment"));
                    if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        pstmt.setDouble(1, currentLiabilityLimit);
                        pstmt.setDouble(2, dblNewRunningLiability);
                    } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                && creditPrepaidInt) {
                            pstmt.setDouble(1, (currentLiabilityLimit - currentRunningLiability));
                        } else {
                            pstmt.setDouble(1, dblNewCreditLimit);
                        }
                        pstmt.setDouble(2, 0);
                    }

                    pstmt.setInt(3, Integer.parseInt(this.repCreditType));
                    pstmt.setDouble(4, Double.parseDouble(this.repId));
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, null);

                    if (this.paymentDescription != null && this.paymentDescription.length() > 255) {
                        this.paymentDescription = this.paymentDescription.substring(0, 255);
                    }

                    if (!NumberUtil.isNumeric(this.commission)) {
                        this.commission = "0";
                    }

                    // 1 2 3 4 5 6 7 8 9 10 11
                    // (rep_id, logon_id, description, amount, credit_limit,
                    // prior_credit_limit, available_credit,
                    // prior_available_credit,
                    // commission, datetime, entityaccounttype)
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertRepCredit"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, sessionData.getProperty("username"));
                    pstmt.setString(3, this.paymentDescription);
                    pstmt.setDouble(4, dblPaymentAmount);
                    if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        pstmt.setDouble(5, currentLiabilityLimit);
                        pstmt.setDouble(7, currentLiabilityLimit - dblNewRunningLiability);
                    } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        pstmt.setDouble(5, dblNewCreditLimit);
                        pstmt.setDouble(7, dblNewCreditLimit);
                    }
                    pstmt.setDouble(6, currentLiabilityLimit);
                    pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                    pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(this.commission)));
                    pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                    pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, null);
                    
                    //DTU-369 Payment Notifications
                    if (Rep.GetPaymentNotification(this.repId)) {
                        PaymentNotification paymentNotification = new PaymentNotification();
                        paymentNotification.setActorId(Long.valueOf(this.repId));
                        paymentNotification.setLogonId(sessionData.getProperty("username"));
                        paymentNotification.setDescription(this.paymentDescription);
                        paymentNotification.setAmount(new BigDecimal(dblPaymentAmount));
                        if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                            paymentNotification.setCreditLimit(new BigDecimal(currentLiabilityLimit));
                            paymentNotification.setAvailableCredit(new BigDecimal(currentLiabilityLimit - dblNewRunningLiability));
                        }else{
                            paymentNotification.setCreditLimit(new BigDecimal(dblNewCreditLimit));
                            paymentNotification.setAvailableCredit(new BigDecimal(dblNewCreditLimit));
                        }
                        paymentNotification.setPriorCreditLimit(new BigDecimal(dblNewCreditLimit));
                        paymentNotification.setPriorAvailableCredit(new BigDecimal(currentLiabilityLimit - currentRunningLiability));                        
                        paymentNotification.setCommission(new BigDecimal(this.commission));                        
                        paymentNotification.setDatetime(DateUtil.formatCalendarDate(Calendar.getInstance(), null));                        
                        paymentNotification.setEntityAccountType(EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));                        
                        paymentNotification.setActorName(this.businessName);
                        paymentNotification.setPaymentNotificationType(Rep.GetPaymentNotificationType(this.repId));
                        paymentNotification.setSmsNotificationPhone(Rep.GetSmsNotificationPhone(this.repId));
                        paymentNotification.setMailNotificationAddress(Rep.GetMailNotificationAddress(this.repId));
                        
                        PaymentNotificationDao.insertPaymentNotification(paymentNotification);
                    }
                    
                }

                if (dblPaymentAmount != 0) {
                    if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        Log.write(sessionData, "Running liability changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewRunningLiability)),
                                DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        Log.write(sessionData, "Liability limit changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewCreditLimit))
                                + ", Running liability changed from " + NumberUtil.formatAmount(Double.toString(currentRunningLiability)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " 0.00", DebisysConstants.LOGTYPE_CUSTOMER, this.repId,
                                DebisysConstants.PW_REF_TYPE_REP_ISO);
                    }
                }

                if (!currentCreditType.equals(this.repCreditType)) {
                    Log.write(sessionData, "Credit type changed from " + currentCreditType + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " "
                            + this.repCreditType, DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_REP, this.repId, DebisysConstants.AUDIT_CREDITTYPE_CHANGE, currentCreditType, this.repCreditType);
                }
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during makePayment", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }

    }

    public void updateLiabilityLimit(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        try {
            if (this.checkPermission(sessionData)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }
                // select r.rep_credit_type, r.LiabilityLimit,
                // r.runningliability from
                // reps r (nolock) where r.rep_id = ?

                if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    rs = pstmt.executeQuery();
                    rs.next();
                    double currentLiabilityLimit = Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));
                    double currentRunningLiability = Double.parseDouble(NumberUtil.formatAmount(rs.getString("runningliability")));
                    double dblNewCreditLimit = Double.parseDouble(this.creditLimit);

                    TorqueHelper.closeStatement(pstmt, rs);

                    // updateRepLiabilityLimit=update reps set liabilityLimit =
                    // ?
                    // where
                    // rep_id = ?
                    Rep repTemp = new Rep();
                    repTemp.setRepId(this.repId);
                    repTemp.getRep(sessionData, context);

                    insertBalanceHistory(DebisysConstants.REP_TYPE_REP, repTemp.repId, repTemp.parentRepId,
                            repTemp.repCreditType, repTemp.sharedBalance, repTemp.entityAccountType, repTemp.runningLiability, String.valueOf(currentLiabilityLimit), sessionData.getProperty("username"), false);

                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepLiabilityLimit"));
                    pstmt.setDouble(1, dblNewCreditLimit);
                    pstmt.setDouble(2, Double.parseDouble(this.repId));
                    pstmt.executeUpdate();

                    TorqueHelper.closeStatement(pstmt, null);

                    // 1 2 3 4 5 6 7 8 9 10 11
                    // (rep_id, logon_id, description, amount, credit_limit,
                    // prior_credit_limit, available_credit,
                    // prior_available_credit,
                    // commission, datetime, entityaccounttype)
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertRepCredit"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, sessionData.getProperty("username"));
                    pstmt.setString(3, Languages.getString("com.debisys.customers.rep_update_credit", sessionData.getLanguage()));
                    pstmt.setDouble(4, dblNewCreditLimit - currentLiabilityLimit);
                    pstmt.setDouble(5, dblNewCreditLimit);
                    pstmt.setDouble(6, currentLiabilityLimit);
                    pstmt.setDouble(7, dblNewCreditLimit - currentRunningLiability);
                    pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                    pstmt.setDouble(9, 0);
                    pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                    pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                    pstmt.executeUpdate();

                    //DTU-369 Payment Notifications
                    if (Rep.GetPaymentNotification(this.repId)) {
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                        pstmt.setDouble(1, Double.parseDouble(this.repId));
                        pstmt.setString(2, sessionData.getProperty("username"));
                        pstmt.setString(3, Languages.getString("com.debisys.customers.rep_update_credit", sessionData.getLanguage()));
                        pstmt.setDouble(4, dblNewCreditLimit - currentLiabilityLimit);
                        pstmt.setDouble(5, dblNewCreditLimit);
                        pstmt.setDouble(6, currentLiabilityLimit);
                        pstmt.setDouble(7, dblNewCreditLimit - currentRunningLiability);
                        pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                        pstmt.setDouble(9, 0);
                        pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                        pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                        pstmt.setString(12, this.businessName);
                        pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                        pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                        pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                        pstmt.executeUpdate();
                    }

                    TorqueHelper.closeStatement(pstmt, null);

                    if (currentLiabilityLimit != dblNewCreditLimit) {
                        Log.write(sessionData, "Liability limit changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewCreditLimit)),
                                DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    }
                }
                if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getSubagentLiabilityLimit"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));

                    rs = pstmt.executeQuery();
                    rs.next();
                    String sLiabilityLimit = rs.getString("liabilitylimit");
                    TorqueHelper.closeStatement(pstmt, rs);
                    String currentLiabilityLimit = NumberUtil.formatAmount(sLiabilityLimit);
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateSubAgentLiabilityLimit2"));
                    pstmt.setDouble(1, Double.parseDouble(this.creditLimit));
                    pstmt.setDouble(2, Double.parseDouble(this.repId));
                    pstmt.executeUpdate();

                    TorqueHelper.closeStatement(pstmt, null);

                    Rep repTemp = new Rep();
                    repTemp.setRepId(this.repId);
                    repTemp.getRep(sessionData, context);

                    insertBalanceHistory(DebisysConstants.REP_TYPE_REP, repTemp.repId, repTemp.parentRepId,
                            repTemp.repCreditType, repTemp.sharedBalance, repTemp.entityAccountType, repTemp.runningLiability, String.valueOf(currentLiabilityLimit), sessionData.getProperty("username"), false);

                    Log.write(sessionData, Languages.getString("com.debisys.customers.Reps.Log.liability_limit", sessionData.getLanguage()) + currentLiabilityLimit + " "
                            + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatCurrency(this.creditLimit), DebisysConstants.LOGTYPE_CUSTOMER,
                            this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    Rep subagent = new Rep();
                    String strSQL = "select rep_id from reps with (nolock) where rep_id in (select iso_id from reps with (nolock) where rep_id=" + this.repId
                            + ")";
                    Rep.cat.debug(strSQL);
                    pstmt = dbConn.prepareStatement(strSQL);
                    rs = pstmt.executeQuery();
                    if (rs.next()) {
                        this.subagentId = rs.getString("rep_id");
                    }

                    TorqueHelper.closeStatement(pstmt, rs);

                    subagent.setRepId(this.subagentId);
                    subagent.getSubAgent(sessionData, context);

                    // if the reps not unlimited, log the merchant credit
                    // assignment
                    if (!subagent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                        double subagentCreditLimit = subagent.getCurrentSubAgentCreditLimit(sessionData);
                        double subagentAssignedCredit = subagent.getAssignedCreditSubAgent(sessionData);
                        double changeInRepCredit = Double.parseDouble(this.creditLimit) - Double.parseDouble(currentLiabilityLimit);
                        // add an entry to show the rep credits being used
                        // 1 2 3 4 5 6 7 8 9 10
                        // (rep_id, merchant_id, logon_id, description, amount, available_credit, prior_available_credit, datetime, subagent_entityaccounttype,
                        // rep_entityaccounttype)
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentRepCredit"));
                        pstmt.setDouble(1, Double.parseDouble(this.subagentId));
                        pstmt.setDouble(2, Double.parseDouble(this.repId));
                        pstmt.setString(3, sessionData.getProperty("username"));
                        pstmt.setString(4, Languages.getString("com.debisys.customers.subagent.credit_updated", sessionData.getLanguage()));
                        pstmt.setDouble(5, changeInRepCredit * -1);
                        pstmt.setDouble(6, subagentCreditLimit - (subagentAssignedCredit + changeInRepCredit));
                        pstmt.setDouble(7, subagentCreditLimit - subagentAssignedCredit);
                        pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                        pstmt.setInt(9, subagent.getEntityAccountType());
                        Rep childrep = new Rep();
                        childrep.setRepId(this.repId);
                        childrep.getRep(sessionData, context);
                        pstmt.setInt(10, childrep.getEntityAccountType());
                        pstmt.executeUpdate();

                        TorqueHelper.closeStatement(pstmt, null);

                        Rep.cat.debug(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));

                        // update reps running liability
                        // updateRepRunningLiability=update set
                        // runningliability=? where rep_id=?
                        Rep.cat.debug(Rep.sql_bundle.getString("updateRepRunningLiability"));
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepRunningLiability"));
                        pstmt.setDouble(1, subagentAssignedCredit + changeInRepCredit);
                        pstmt.setDouble(2, Double.parseDouble(this.subagentId));
                        pstmt.executeUpdate();
                    }

                }

            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during updateLiabilityLimit", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }

    }

    // Check to make sure you are a valid iso, agent, subagent to edit a rep
    public boolean checkPermission(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAllowed = false;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }

            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strAccessLevel = sessionData.getProperty("access_level");
            String strRefId = sessionData.getProperty("ref_id");
            // iso
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkRepIso3"));
                    pstmt.setDouble(1, Double.parseDouble(strRefId));
                    pstmt.setDouble(2, Double.parseDouble(this.repId));
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkRepIso5"));
                    pstmt.setDouble(1, Double.parseDouble(strRefId));
                    pstmt.setDouble(2, Double.parseDouble(this.repId));
                }
            } // agent
            else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkRepAgent"));
                pstmt.setDouble(1, Double.parseDouble(strRefId));
                pstmt.setDouble(2, Double.parseDouble(this.repId));
            } // subagent
            else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                // for query purposes an iso 3 level and subagent are the same
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkRepIso3"));
                pstmt.setDouble(1, Double.parseDouble(strRefId));
                pstmt.setDouble(2, Double.parseDouble(this.repId));
            } else {
                isAllowed = false;
                return isAllowed;
            }

            rs = pstmt.executeQuery();

            if (rs.next()) {
                isAllowed = true;
            } else {
                isAllowed = false;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkPermission", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isAllowed;
    }

    // checks to make sure all merchants are limited merchants
    // before allowing rep change from unlimited to limited type
    // true means all are limited, false means there are unlimited
    // or undefined
    public boolean checkMerchantCreditType() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAllowed = false;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            String strSQL = Rep.sql_bundle.getString("checkMerchantCreditType");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            Rep.cat.debug(strSQL);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                isAllowed = false;
            } else {
                isAllowed = true;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkMerchantCreditType", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isAllowed;
    }

    /**
     * @param intPageNumber
     * @param intRecordsPerPage
     * @param sessionData
     * @param context
     * @param scheduleReportType
     * @param headers
     * @param titles
     * @return
     * @throws CustomerException
     */
    public Vector getRepCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector vecRepCredits = new Vector();
        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
            String reportId = sessionData.getProperty("reportId");
            int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            }

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            this.endDate = format.format(new Date());
            this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
            Date date1 = format.parse(this.startDate);
            Date date2 = format.parse(this.endDate);
            format.applyPattern("yyyy-MM-dd");
            this.startDate = format.format(date1);
            this.endDate = format.format(date2);

            Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate + " 23:59:59.999", false);

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getPaymentsHistoryByRepId");
            String instanceName = DebisysConfigListener.getInstance(context);

            pstmt = dbConn.prepareCall(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.getRepId()));
            if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate)) {
                pstmt.setString(2, vTimeZoneFilterDates.get(0));
                pstmt.setString(3, vTimeZoneFilterDates.get(1));
            } else {
                pstmt.setNull(2, Types.DATE);
                pstmt.setNull(3, Types.DATE);
            }

            if (scheduleReportType == DebisysConstants.EXECUTE_REPORT) {
                pstmt.setBoolean(4, true);
                pstmt.setNull(5, Types.INTEGER);
                pstmt.setNull(6, Types.INTEGER);
                pstmt.setString(7, instanceName);

                rs = pstmt.executeQuery();
                int intRecordCount = 0;
                if (rs.next()) {
                    intRecordCount = rs.getInt(1);
                }

                // first row is always the count
                vecRepCredits.add(Integer.valueOf(intRecordCount));
                if (intRecordCount > 0) {
                    pstmt.setDouble(1, Double.parseDouble(this.getRepId()));
                    if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate)) {
                        pstmt.setString(2, vTimeZoneFilterDates.get(0));
                        pstmt.setString(3, vTimeZoneFilterDates.get(1));
                    } else {
                        pstmt.setNull(2, Types.DATE);
                        pstmt.setNull(3, Types.DATE);
                    }
                    pstmt.setBoolean(4, false);
                    pstmt.setInt(5, intPageNumber);
                    pstmt.setInt(6, intRecordsPerPage);

                    rs = pstmt.executeQuery();
                    rs.next();
                    Double previousAvailableCredit = -1D;
                    Double AvailableCredit;

                    for (int i = 0; i < intRecordsPerPage; i++) {
                        Vector<String> vTimeZoneDate = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), rs.getString("datetime"), rs.getString("datetime"), true);
                        Vector vecTemp = new Vector();
                        String logon_id = rs.getString("logon_id");
                        vecTemp.add(vTimeZoneDate.get(2));
                        vecTemp.add(StringUtil.toString(logon_id));
                        vecTemp.add(StringUtil.toString(rs.getString("description")));

                        if (logon_id.equals("Events Job")) {
                            vecTemp.add("");
                        } else {
                            vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
                        }

                        AvailableCredit = rs.getDouble("available_credit");
                        if ((AvailableCredit != null && !AvailableCredit.equals(previousAvailableCredit))
                                || (rs.getString("logon_id") != null && !rs.getString("logon_id").equals("Commission") && !rs.getString("logon_id").equals("Billing"))) {
                            vecTemp.add(NumberUtil.formatCurrency(rs.getString("available_credit")));
                        } else {
                            vecTemp.add("");
                        }
                        vecTemp.add(NumberUtil.formatAmount(rs.getString("commission")));

                        if (logon_id.equals("Events Job")) {
                            vecTemp.add("");
                        } else {
                            vecTemp.add(NumberUtil.formatCurrency(rs.getString("net_payment")));
                        }

                        vecRepCredits.add(vecTemp);
                        previousAvailableCredit = AvailableCredit;

                        if (!rs.next()) {
                            break;
                        }
                    }
                }
            } else if (scheduleReportType == DebisysConstants.DOWNLOAD_REPORT) {
                pstmt.setBoolean(4, false);
                pstmt.setInt(5, -1);
                pstmt.setNull(6, Types.INTEGER);
                pstmt.setString(7, instanceName);
                rs = pstmt.executeQuery();

                DownloadsSummary trxSummary = new DownloadsSummary();
                TransactionReport tr = new TransactionReport();
                String strFileName = tr.generateFileName(context);
                this.setStrUrlLocation(trxSummary.downloadRepCredits(rs, sessionData, context, headers, strFileName, titles, 2));
                rs.close();
                pstmt.close();
            } else if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                StringBuilder execStore = new StringBuilder();
                execStore.append("DECLARE @startDateRelative DATETIME ");
                execStore.append("DECLARE @endDateRelative DATETIME ");
                execStore.append("SELECT @startDateRelative =" + ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE + " , @endDateRelative = " + ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE + " ");

                String fixedQuery = strSQL.replaceFirst("\\?", this.getRepId())
                        .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(0) + "'")
                        .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(1) + "'")
                        .replaceFirst("\\?", "0")
                        .replaceFirst("\\?", "-1")
                        .replaceFirst("\\?", "null")
                        .replaceFirst("\\?", "'" + instanceName + "'");

                String relativeQuery = strSQL.replaceFirst("\\?", this.getRepId())
                        .replaceFirst("\\?", "@startDateRelative")
                        .replaceFirst("\\?", "@endDateRelative")
                        .replaceFirst("\\?", "0")
                        .replaceFirst("\\?", "-1")
                        .replaceFirst("\\?", "null")
                        .replaceFirst("\\?", instanceName);
                execStore.append(relativeQuery);

                ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_REPS_CRED_HIST, 1);

                scheduleReport.setRelativeQuery(execStore.toString());

                scheduleReport.setFixedQuery(fixedQuery);
                TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
                sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
                return null;
            }

        } catch (Exception e) {
            Rep.cat.error("Error during getRepCredits", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vecRepCredits;
    }

    /**
     * @param intPageNumber
     * @param intRecordsPerPage
     * @param sessionData
     * @param context
     * @param scheduleReportType
     * @param headers
     * @param titles
     * @return
     * @throws CustomerException
     */
    public Vector getRepMerchantCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector vecRepMerchantCredits = new Vector();
        String strAccessLevel = sessionData.getProperty("access_level");
        StringBuilder strBuffSQL = new StringBuilder();
        // jay - 6/19/2006
        // prevent reps from seeing others reps
        // nned to make this more secure but out of time
        if (strAccessLevel.equals(DebisysConstants.REP)) {
            this.repId = sessionData.getProperty("ref_id");
        }

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
            String reportId = sessionData.getProperty("reportId");
            int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            }

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            this.endDate = format.format(new Date());
            this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
            Date date1 = format.parse(this.startDate);
            Date date2 = format.parse(this.endDate);
            format.applyPattern("yyyy-MM-dd");
            this.startDate = format.format(date1);
            this.endDate = format.format(date2);

            Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate + " 23:59:59.999", false);
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQLWhere = "";

            if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                strBuffSQL.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS);
            }

            // get a count of total matches
            String strSQL = "select dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + sessionData.getProperty("ref_id") + ", rmc.datetime, 1) as datetime , rmc.merchant_id, rmc.logon_id, rmc.description, rmc.amount, "
                    + " rmc.available_credit, rmc.prior_available_credit, m.legal_businessname " + strBuffSQL.toString()
                    + " FROM rep_merchant_credits rmc with(nolock) "
                    + "INNER JOIN merchants m WITH(nolock) ON (m.merchant_id = rmc.merchant_id)"
                    + "where rmc.rep_id=?";

            if (scheduleReportType != DebisysConstants.SCHEDULE_REPORT) {
                if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate)) {
                    strSQLWhere = " and rmc.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and rmc.datetime <= '" + vTimeZoneFilterDates.get(1) + "'";
                }
            } else {
                String fixedQuery = strSQL.replaceFirst("\\?", this.repId);
                String relativeQuery = strSQL.replaceFirst("\\?", this.repId);

                relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT;

                fixedQuery += " AND rmc.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND rmc.datetime <= '" + vTimeZoneFilterDates.get(1) + "' ";

                ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_REP_CREDIT_ASSIGNMENT_HIST, 1);
                scheduleReport.setNameDateTimeColumn("rmc.datetime");

                scheduleReport.setRelativeQuery(relativeQuery);

                scheduleReport.setFixedQuery(fixedQuery);
                TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
                sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
                return null;
            }

            Rep.cat.debug(strSQL + strSQLWhere + " order by rmc.id DESC /*" + this.repId + "*/");

            if (scheduleReportType != DebisysConstants.DOWNLOAD_REPORT) {
                pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by rmc.id DESC", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                int intRecordCount = DbUtil.getRecordCount(rs);
                // first row is always the count
                vecRepMerchantCredits.add(Integer.valueOf(intRecordCount));
                if (intRecordCount > 0) {
                    rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
                    for (int i = 0; i < intRecordsPerPage; i++) {

                        Vector vecTemp = new Vector();
                        vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
                        vecTemp.add(StringUtil.toString(rs.getString("logon_id")));
                        vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
                        vecTemp.add(StringUtil.toString(rs.getString("description")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("available_credit")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("prior_available_credit")));
                        vecTemp.add(StringUtil.toString(rs.getString("legal_businessname")));
                        vecRepMerchantCredits.add(vecTemp);
                        if (!rs.next()) {
                            break;
                        }
                    }
                }
            } else {
                pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by rmc.id DESC");
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                DownloadsSummary trxSummary = new DownloadsSummary();
                TransactionReport tr = new TransactionReport();
                String strFileName = tr.generateFileName(context);
                //String totalLabel = Languages.getString("jsp.admin.reports.totals", sessionData.getLanguage() );
                this.setStrUrlLocation(trxSummary.download(rs, sessionData, context, headers, strFileName, titles, null, 2));
                rs.close();
                pstmt.close();
            }

        } catch (Exception e) {
            Rep.cat.error("Error during getRepMerchantCredits", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vecRepMerchantCredits;
    }

    /* BEGIN Release 9.0 - Added by LF */
    public Vector getRepMerchants(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector vResults = new Vector();
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepMerchants"));
            pstmt.setString(1, this.repId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vTemp = new Vector();
                vTemp.add(rs.getString("Merchant_ID"));
                vTemp.add(rs.getString("DBA"));
                vTemp.add(rs.getString("LiabilityLimit"));
                vTemp.add(rs.getString("RunningLiability"));
                vTemp.add(rs.getString("Available"));
                vTemp.add(rs.getString("Merchant_Type"));
                vResults.add(vTemp);
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getRepMerchants", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vResults;
    }

    public void doRepMerchantMigration(SessionData sessionData, ServletContext context, String sData, boolean bShared) throws CustomerException {
        try {
            String sMerchants[] = sData.split("&");

            // Used to establish if all merchants are prepaid
            boolean allMerchantsPrepaid = true;
            double priorRepCreditLimit = this.getCurrentLiabilityLimit();
            double priorRepAvailableCredit = priorRepCreditLimit - this.getCurrentRunningLiability();

            Vector<Merchant> merchants = new Vector<Merchant>();
            String sItems[];
            for (int i = 0; i < sMerchants.length; i++) {
                if (sMerchants[i].length() > 0) {
                    sItems = sMerchants[i].split(":");
                    Merchant m = new Merchant();
                    m.setMerchantId(sItems[0]);
                    m.getMerchant(sessionData, context);
                    m.setCurrentMerchantType(m.getMerchantType());
                    if (!bShared) {
                        m.setCreditLimit(sItems[2]);
                        m.setMerchantType(sItems[1]);
                    }
                    if (!m.getMerchantType().equals(DebisysConstants.MERCHANT_TYPE_PREPAID)) {
                        allMerchantsPrepaid = false;
                    }
                    merchants.add(m);
                }
            }
            // If Rep is going to have Individual Credit and is Prepaid.
            if (this.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) && allMerchantsPrepaid) {
                // Setting 0 RunningLibility and Liability Limit of Rep because
                // it will be updated with Merchants values
                this.updateLiabilityLimit(0);
                this.updateRunningLiability(0);
            }
            // Through merchants of this Rep
            for (Merchant m : merchants) {
                // If Rep is going to have Shared Credit
                if (bShared) {
                    sessionData.setProperty("MXEnableSharedBalance", "1");
                    m.setCreditLimit("0");
                    // If Rep or any of its Merchants is not prepaid
                    if (!this.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) || !allMerchantsPrepaid) {
                        m.updateLiabilityLimit2(sessionData, context);
                    } else {// If Rep and its Merchants are prepaid
                        m.changeFromIdividualToShared(sessionData, context);
                    }
                    m.setMerchantType(DebisysConstants.MERCHANT_TYPE_SHARED);
                    m.updateMerchantType(sessionData, context);
                    sessionData.setProperty("MXEnableSharedBalance", "");
                } else // If Rep is going to have Individual Credit
                {
                    sessionData.setProperty("MXDisableSharedBalance", "1");
                    // If Rep or any of its Merchants is not prepaid
                    if (!this.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) || !allMerchantsPrepaid) {
                        m.updateLiabilityLimit2(sessionData, context);
                        m.updateMerchantType(sessionData, context);
                    } else {// If Rep and its Merchants are prepaid
                        m.changeFromSharedToIdividual(sessionData, context);
                        m.updateMerchantType(sessionData, context);
                    }
                    sessionData.setProperty("MXDisableSharedBalance", "");
                }
            }
            // If Rep and its merchants are prepaid
            if (this.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) && allMerchantsPrepaid) {
                // If Rep is going to have Shared Credit.
                double repCreditLimit = this.getCurrentLiabilityLimit();
                double availableCredit = repCreditLimit - this.getCurrentRunningLiability();

                if ((bShared)) {
                    this.logRepMigration(sessionData, Languages.getString("com.debisys.customers.rep_migration_ind", sessionData.getLanguage()) + priorRepAvailableCredit, 0,
                            repCreditLimit, priorRepCreditLimit, availableCredit, priorRepAvailableCredit);
                } else { // If Rep is going to have Individual Credit.
                    this.logRepMigration(sessionData, Languages.getString("com.debisys.customers.rep_migration_comp", sessionData.getLanguage()) + priorRepAvailableCredit, 0,
                            repCreditLimit, priorRepCreditLimit, availableCredit, priorRepAvailableCredit);
                }
            }

            insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_REP, this.repId, DebisysConstants.AUDIT_SHARED_INDIVIDUAL_CHANGE, (bShared) ? "0" : "1", (bShared) ? "1" : "0");
            this.updateRepSharedBalance(bShared);
        } catch (Exception e) {
            Rep.cat.error("Error during doRepMerchantMigration", e);
            throw new CustomerException();
        }
    }// End of function doRepMerchantMigration

    public double getCurrentLiabilityLimit() throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            rs.next();
            double currentLiabilityLimit = Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));
            return currentLiabilityLimit;
        } catch (Exception e) {
            Rep.cat.error("Error during Rep's updateRunningLiability", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public double getCurrentRunningLiability() throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            rs.next();
            double currentRunningLiability = Double.parseDouble(NumberUtil.formatAmount(rs.getString("runningliability")));
            return currentRunningLiability;
        } catch (Exception e) {
            Rep.cat.error("Error during Rep's updateRunningLiability", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    
    /**
     * This method was added in order to obtain rep liability limit values in 
     * one call, because previous calls get one value or the other separately
     * @return object with rep liability data or null, if rep not found
     */
    public RepLiabilityData getCurrentLiabilityData(){
        RepLiabilityData retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
            } else {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                if(rs.next()){
                    retValue = new RepLiabilityData();
                    retValue.setRepCreditType(rs.getLong("rep_credit_type"));
                    retValue.setLiabilityLimit(rs.getBigDecimal("LiabilityLimit"));
                    retValue.setRunningLiability(rs.getBigDecimal("runningliability"));                    
                }
            }
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception e) {
            Rep.cat.error("Error during Rep's updateRunningLiability", e);
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
        }
        return retValue;
    }

    public void updateRunningLiability(double newRunningLiability) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            Rep.cat.debug(Rep.sql_bundle.getString("updateRepRunningLiability"));
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepRunningLiability"));
            pstmt.setDouble(1, newRunningLiability);
            pstmt.setDouble(2, Double.parseDouble(this.repId));
            pstmt.executeUpdate();
            this.setRunningLiability(String.valueOf(newRunningLiability));
        } catch (Exception e) {
            Rep.cat.error("Error during Rep's updateRunningLiability", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public void updateLiabilityLimit(double newLiabilityLimit) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            rs.next();
            /* double currentLiabilityLimit = */
            Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));// Variable
            // never
            // used
            pstmt.close();
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepLiabilityLimit"));
            pstmt.setDouble(1, newLiabilityLimit);
            pstmt.setDouble(2, Double.parseDouble(this.repId));
            pstmt.executeUpdate();
            this.setCreditLimit(String.valueOf(newLiabilityLimit));
        } catch (Exception e) {
            Rep.cat.error("Error during Rep's updateLiabilityLimit", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public void logRepMigration(SessionData sessionData, String description, double amount, double credit_limit, double prior_credit_limit,
            double available_credit, double prior_available_credit) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            // 1 2 3 4 5 6 7 8 9 10 11
            // (rep_id, logon_id, description, amount, credit_limit,
            // prior_credit_limit, available_credit, prior_available_credit,
            // commission, datetime, entityaccounttype)
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertRepCredit"));
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            pstmt.setString(2, sessionData.getProperty("username"));
            pstmt.setString(3, description);
            pstmt.setDouble(4, amount);
            pstmt.setDouble(5, credit_limit);
            pstmt.setDouble(6, prior_credit_limit);
            pstmt.setDouble(7, available_credit);
            pstmt.setDouble(8, prior_available_credit);
            pstmt.setDouble(9, 0);
            pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
            pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
            pstmt.executeUpdate();

            insertBalanceHistory(DebisysConstants.REP_TYPE_REP, this.repId, this.parentRepId, this.repCreditType, this.sharedBalance,
                    this.entityAccountType, this.runningLiability, this.creditLimit, sessionData.getProperty("username"), false);

            //DTU-369 Payment Notifications
            if (Rep.GetPaymentNotification(this.repId)) {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                pstmt.setString(2, sessionData.getProperty("username"));
                pstmt.setString(3, description);
                pstmt.setDouble(4, amount);
                pstmt.setDouble(5, credit_limit);
                pstmt.setDouble(6, prior_credit_limit);
                pstmt.setDouble(7, available_credit);
                pstmt.setDouble(8, prior_available_credit);
                pstmt.setDouble(9, 0);
                pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                pstmt.setString(12, this.businessName);
                pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                pstmt.executeUpdate();
            }

            TorqueHelper.closeStatement(pstmt, null);
        } catch (Exception e) {
            Rep.cat.error("Error during Rep's logRepMigration", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public void updateRepSharedBalance(boolean bNewSharedBalance) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepSharedBalance"));
            pstmt.setString(1, (bNewSharedBalance) ? "1" : "0");
            pstmt.setString(2, this.repId);
            pstmt.executeUpdate();
        } catch (Exception e) {
            Rep.cat.error("Error during updateRepSharedBalance", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }// End of function updateRepSharedBalance

    public Vector doPaymentMX(SessionData sessionData, ServletContext context) throws CustomerException, UserException {
        Connection cnx = null;
        Vector vResult = new Vector();
        CallableStatement cst = null;
        String strSQL = "";
        String sDeploymentType = DebisysConfigListener.getDeploymentType(context);
        String sCustomConfigType = DebisysConfigListener.getCustomConfigType(context);
        boolean useEntityAccountType = sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE);

        if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)) {
            sessionData.setProperty("iso_id", (new User()).getISOId(DebisysConstants.REP, this.repId));
            sessionData.setProperty("iso_id", sessionData.getUser().getIsoId());
        }

        if (useEntityAccountType) {
            sDeploymentType = DebisysConstants.DEPLOYMENT_INTERNATIONAL;
            sCustomConfigType = DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO;
        }

        try {
            if (this.checkPermission(sessionData)) {
                strSQL = Rep.sql_bundle.getString("applyRepPayment");
                cnx = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (cnx == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new ReportException();
                }

                Rep.cat.debug(strSQL);
                cst = cnx.prepareCall(strSQL);
                cst.setString(1, this.repId);
                cst.setDouble(2, Double.parseDouble(this.paymentAmount));
                cst.setDouble(3, Double.parseDouble(this.commission));
                cst.setString(4, this.paymentDescription);
                cst.setString(5, sessionData.getProperty("username"));
                cst.setString(6, sDeploymentType);
                cst.setString(7, sCustomConfigType);
                cst.registerOutParameter(8, Types.INTEGER);
                cst.setInt(8, 0);
                cst.execute();
                vResult.add(cst.getInt(8));
                Rep.cat.debug(vResult);
            }
        } catch (Exception e) {
            Rep.cat.error("Error during doPaymentMX", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(cnx, cst, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vResult;
    }// End of function doPaymentMX

    /* END Release 9.0 - Added by LF */
    // #BEGIN Added by NM - Release 21
    /**
     * Gets a list of Payment Types
     *
     * @throws CustomerException Thrown on database errors
     */
    public static Vector getPaymentTypes() throws CustomerException {
        Vector vecPaymentTypes = new Vector();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getPaymentType");
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("paymenttypeid"));
                vecTemp.add(rs.getString("description"));
                vecPaymentTypes.add(vecTemp);
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getPaymentTypes", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return vecPaymentTypes;
    }

    /**
     * Gets a list of Processor Types
     *
     * @throws CustomerException Thrown on database errors
     */
    public static Vector getProcessorTypes() throws CustomerException {
        Vector vecProcessorTypes = new Vector();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getProcessType");
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("id"));
                vecTemp.add(rs.getString("processor_name"));
                vecProcessorTypes.add(vecTemp);
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getProcessorTypes", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return vecProcessorTypes;
    }

    // #END Added by NM - Release 21
    // #BEGIN Added by MCR - Release 22
    public Vector addGlobalClerkCode(SessionData sessionData, ServletContext context, String strClerkCode, String strClerkName, String strClerkLastName) {
        Connection cnx = null;
        CallableStatement cst = null;
        String strSQL = "";
        Vector vResult = new Vector();

        if (strClerkCode.length() > 10) {
            strClerkCode = strClerkCode.substring(0, 10);
        }
        if (strClerkName.length() > 50) {
            strClerkName = strClerkName.substring(0, 50);
        }
        if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {// When MX
            if (!StringUtil.isAlpha(strClerkName)) {
                this.addFieldError("ErrorValidate", "Error");
                return null;
            }

            if (!StringUtil.isAlpha(strClerkLastName)) {
                this.addFieldError("ErrorValidate", "Error");
                return null;
            }

            if (strClerkCode.length() > 10) {
                strClerkCode = strClerkCode.substring(0, 10);
            }
            if (strClerkName.length() > 16) {
                strClerkName = strClerkName.substring(0, 16);
            }

            if (strClerkLastName.length() > 24) {
                strClerkName = strClerkLastName.substring(0, 24);
            }
        }
        if (this.checkPermission(sessionData)) {
            if (StringUtil.isAlphaNumeric(strClerkCode)) {
                try {
                    strSQL = Rep.sql_bundle.getString("addGlobalClerkCode");
                    cnx = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                    if (cnx == null) {
                        Rep.cat.error("Can't get database connection");
                        throw new ReportException();
                    }

                    Rep.cat.debug(strSQL);
                    cst = cnx.prepareCall(strSQL);
                    cst.setString(1, this.repId);
                    cst.setString(2, strClerkCode);
                    if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {// When
                        // MX
                        cst.setString(3, strClerkName);
                        cst.setString(4, strClerkLastName);
                        cst.setString(5, strClerkName + " " + strClerkLastName);
                    } else {
                        cst.setString(3, null);
                        cst.setString(4, null);
                        cst.setString(5, strClerkName);
                    }
                    cst.registerOutParameter(6, Types.INTEGER);
                    cst.setInt(6, 0);
                    cst.registerOutParameter(7, Types.INTEGER);
                    cst.setInt(7, 0);
                    cst.execute();
                    vResult.add(cst.getInt(6));
                    vResult.add(cst.getInt(7));
                } catch (Exception e) {
                    Rep.cat.error("Error during addClerkCode", e);
                } finally {
                    try {
                        TorqueHelper.closeConnection(cnx, cst, null);
                    } catch (Exception e) {
                        Rep.cat.error("Error during release connection", e);
                    }
                }
            }
        } else {
            this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
        }
        return vResult;
    }

    public int delGlobalClerkCode(SessionData sessionData, ServletContext context, String strClerkCode) {
        Connection cnx = null;
        CallableStatement cst = null;
        String strSQL = "";
        int result = 0;
        if (this.checkPermission(sessionData)) {
            try {
                strSQL = Rep.sql_bundle.getString("delGlobalClerkCode");
                cnx = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (cnx == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new ReportException();
                }
                Rep.cat.debug(strSQL);
                cst = cnx.prepareCall(strSQL);
                cst.setString(1, this.repId);
                cst.setString(2, strClerkCode);
                cst.setInt(3, Integer.parseInt(DebisysConfigListener.getCustomConfigType(context)));
                cst.registerOutParameter(4, Types.INTEGER);
                cst.setInt(4, 0);
                cst.execute();
                // ResultSet rs = cst.getResultSet();
                result = cst.getInt(4);
            } catch (Exception e) {
                Rep.cat.error("Error during delClerkCode", e);
            } finally {
                try {
                    TorqueHelper.closeConnection(cnx, cst, null);
                } catch (Exception e) {
                    Rep.cat.error("Error during release connection", e);
                }
            }
        } else {
            this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
        }
        return result;
    }

    // #END Added by MCR - Release 22
    // # Begin Added by Nidhi Gulati DBSYS-613
    /**
     * Adds a new Agent in the database and sets the entity id of the object to
     * the newly created record's ID
     *
     * @param sessionData
     * @param context
     * @throws com.debisys.exceptions.EntityException Thrown if there is an
     * error inserting the record in the database.
     */
    public void addAgent(SessionData sessionData, ServletContext context) throws EntityException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            HashMap map = CountryInfo.getAllCountriesID();
            String RefId = sessionData.getProperty("ref_id");
            // rep id
            Rep.cat.debug("the staticIDCountryISO1 info is : " + context.getAttribute("staticIDCountryISO1"));
            Rep.cat.debug("the staticIDConstantValue1 info is : " + context.getAttribute("staticIDConstantValue1"));
            Rep.cat.debug("Compare to ISO ID is : " + RefId);
            Vector<String> staticISO = new Vector<String>();
            // check if Country ISO matches Static ISO Conditions
            // Static ISO added to define constant values for a country ISO
            for (int i = 1;; i++) {
                if (context.getAttribute("staticIDCountryISO" + i) == null || context.getAttribute("staticIDCountryISO" + i) == "") {
                    break;
                }
                if (context.getAttribute("staticIDConstantValue" + i) == null || context.getAttribute("staticIDConstantValue" + i) == "") {
                    break;
                }
                if (RefId.equals(context.getAttribute("staticIDCountryISO" + i))) {
                    staticISO.add((String) context.getAttribute("staticIDCountryISO" + i));
                    staticISO.add((String) context.getAttribute("staticIDConstantValue" + i));
                    break;
                }
            }
            if (staticISO.size() < 2) {
                this.repId = this.generateRepId();// if no special cases, id is
                // generated randomly
                Rep.cat.debug("Generate ID without constants values==>" + this.repId);
            } else {
                this.repId = this.generatePartiallyStaticId(staticISO);
                Rep.cat.debug("partial Approach generatedID==>" + this.repId);
            }

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new EntityException();
            }
            String strSQL = Rep.sql_bundle.getString("addRep");
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            // rep id

            pstmt.setDouble(1, Double.parseDouble(this.repId));
            pstmt.setInt(2, 4); // type = 4 = Agent
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            pstmt.setDouble(3, Double.parseDouble(this.parentRepId));
            pstmt.setString(4, this.businessName);
            // initials

            String initials = this.contactFirst.substring(0, 1);
            if (this.contactMiddle == null || this.contactMiddle.trim().equals("")) {
                initials = initials + "_";
            } else {
                initials = initials + this.contactMiddle.substring(0, 1);
            }
            initials = initials + this.contactLast.substring(0, 1);

            pstmt.setString(5, initials);
            pstmt.setString(6, this.contactFirst);
            pstmt.setString(7, this.contactLast);
            pstmt.setString(8, this.contactMiddle);
            pstmt.setString(9, this.taxId);
            pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
            pstmt.setString(11, this.address);
            pstmt.setString(12, this.city);
            pstmt.setString(13, this.state);
            pstmt.setString(14, this.zip);
            pstmt.setString(15, this.contactPhone);
            pstmt.setString(16, this.contactFax);
            pstmt.setString(17, this.contactEmail);
            pstmt.setString(18, this.routingNumber);
            pstmt.setString(19, this.accountNumber);
            // running liability
            pstmt.setDouble(20, 0);
            pstmt.setDouble(21, Double.parseDouble(this.creditLimit));
            if (this.getISOCreditType(this.parentRepId).equals(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE)) {
                this.setRepCreditType(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE);
                Rep.cat.debug("The Credit Type set to: " + this.repCreditType);
            }
            if (this.repCreditType == null) {
                // this is domestic so lets add the rep type for them
                // there is no concept of prepaid domestically so they
                // are credit or unlimited
                if (Double.parseDouble(this.creditLimit) > 0) {
                    this.repCreditType = DebisysConstants.REP_CREDIT_TYPE_CREDIT;
                } else {
                    this.repCreditType = DebisysConstants.REP_CREDIT_TYPE_UNLIMITED;
                }
            }
            pstmt.setInt(22, Integer.parseInt(this.repCreditType));
            pstmt.setString(23, this.country);
            pstmt.setString(24, this.sharedBalance);
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (this.mailState != null)) {
                pstmt.setString(25, this.mailAddress);
                pstmt.setString(26, this.mailCity);
                pstmt.setString(27, this.mailState);
                pstmt.setString(28, this.mailZip);
                pstmt.setString(29, this.mailCountry);
                pstmt.setString(30, this.mailDelegation);
                pstmt.setString(31, this.mailCounty);
                pstmt.setString(32, this.mailColony);
            } else {
                pstmt.setString(25, this.address);
                pstmt.setString(26, this.city);
                pstmt.setString(27, this.state);
                pstmt.setString(28, this.zip);
                pstmt.setString(29, this.country);
                pstmt.setString(30, this.physDelegation);
                pstmt.setString(31, this.physCounty);
                pstmt.setString(32, this.physColony);
            }
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                pstmt.setString(33, this.physCounty);
                pstmt.setString(34, this.physColony);
                pstmt.setString(35, this.physDelegation);
                pstmt.setString(36, this.bankName);
                pstmt.setString(37, this.businessHours);
                pstmt.setString(38, this.businessTypeId);
                pstmt.setInt(39, this.accountTypeId);
                pstmt.setInt(40, this.merchantSegmentId);
                pstmt.setString(41, this.legalBusinessname);
            } else {
                pstmt.setNull(33, Types.VARCHAR);
                pstmt.setNull(34, Types.VARCHAR);
                pstmt.setNull(35, Types.VARCHAR);
                pstmt.setNull(36, Types.VARCHAR);
                pstmt.setNull(37, Types.VARCHAR);
                pstmt.setNull(38, Types.VARCHAR);
                pstmt.setInt(39, 1);
                pstmt.setNull(40, Types.INTEGER);
                pstmt.setNull(41, Types.VARCHAR);
            }
            pstmt.setInt(42, this.paymentType);
            pstmt.setInt(43, this.processType);
            pstmt.setInt(44, this.timeZoneId);
            pstmt.setString(45, map.get(Integer.parseInt(this.country)).toString());
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                    && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                    && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                pstmt.setString(46, this.salesmanid);
                pstmt.setString(47, this.terms);
            } else {
                pstmt.setNull(46, Types.VARCHAR);
                pstmt.setNull(47, Types.VARCHAR);
            }

            // DBSY-1072 eAccounts Interface
            if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                pstmt.setInt(48, this.entityAccountType);
            } else {
                pstmt.setNull(48, Types.INTEGER);
            }

            //DTU-369 Payment Notifications
            pstmt.setBoolean(49, this.isPaymentNotifications());
            if (this.isPaymentNotificationSMS()) {
                this.setPaymentNotificationType(0);
            } else if (this.isPaymentNotificationMail()) {
                this.setPaymentNotificationType(1);
            }
            pstmt.setInt(50, this.getPaymentNotificationType());
            pstmt.setString(51, this.getSmsNotificationPhone());
            pstmt.setString(52, this.getMailNotificationAddress());

            //DTU-480: Low Balance Alert Message SMS and/or email
            pstmt.setBoolean(53, this.isBalanceNotifications());
            if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays()) {
                this.setBalanceNotificationType(2);
            } else if (this.isBalanceNotificationTypeValue()) {
                this.setBalanceNotificationType(0);
            } else if (this.isBalanceNotificationTypeDays()) {
                this.setBalanceNotificationType(1);
            }
            pstmt.setInt(54, this.getBalanceNotificationType());
            if (this.isBalanceNotificationSMS()) {
                this.setBalanceNotificationNotType(0);
            } else if (this.isBalanceNotificationMail()) {
                this.setBalanceNotificationNotType(1);
            }
            pstmt.setInt(55, this.getBalanceNotificationNotType());
            pstmt.setInt(56, this.getBalanceNotificationDays());
            pstmt.setDouble(57, this.getBalanceNotificationValue());
            pstmt.setLong(58, this.getInvoiceType());

            pstmt.execute();
            pstmt.close();

            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                Iterator itContacts = this.vecContacts.iterator();
                while (itContacts.hasNext()) {
                    Vector vecTemp = null;
                    vecTemp = (Vector) itContacts.next();
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertContactMx"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, vecTemp.get(0).toString());
                    pstmt.setString(3, vecTemp.get(1).toString());
                    pstmt.setString(4, vecTemp.get(2).toString());
                    pstmt.setString(5, vecTemp.get(3).toString());
                    pstmt.setString(6, vecTemp.get(4).toString());
                    pstmt.setString(7, vecTemp.get(5).toString());
                    pstmt.setString(8, vecTemp.get(6).toString());
                    pstmt.executeUpdate();
                    pstmt.close();
                }
            }

            Log.write(sessionData, "Agent added.", DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);

            if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                // 1 2 3 4 5 6 7 8 9 10 11
                // (rep_id, logon_id, description, amount, credit_limit,
                // prior_credit_limit, available_credit, prior_available_credit,
                // commission, datetime, entityaccounttype)
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentCredit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                pstmt.setString(2, sessionData.getProperty("username"));
                pstmt.setString(3, Languages.getString("com.debisys.customers.rep_add_payment", sessionData.getLanguage()));
                pstmt.setDouble(4, Double.parseDouble(this.creditLimit));
                pstmt.setDouble(5, Double.parseDouble(this.creditLimit));
                pstmt.setDouble(6, 0);
                pstmt.setDouble(7, Double.parseDouble(this.creditLimit));
                pstmt.setDouble(8, 0);
                pstmt.setDouble(9, 0);
                pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                pstmt.executeUpdate();

                //DTU-369 Payment Notifications
                if (Rep.GetPaymentNotification(this.repId)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, sessionData.getProperty("username"));
                    pstmt.setString(3, Languages.getString("com.debisys.customers.rep_add_payment", sessionData.getLanguage()));
                    pstmt.setDouble(4, Double.parseDouble(this.creditLimit));
                    pstmt.setDouble(5, Double.parseDouble(this.creditLimit));
                    pstmt.setDouble(6, 0);
                    pstmt.setDouble(7, Double.parseDouble(this.creditLimit));
                    pstmt.setDouble(8, 0);
                    pstmt.setDouble(9, 0);
                    pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                    pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                    pstmt.setString(12, this.businessName);
                    pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                    pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                    pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                    pstmt.executeUpdate();
                }
            }
        } catch (Exception e) {
            Rep.cat.error("Error during addAgent", e);
            throw new EntityException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * Method to validate a Agent while adding
     * @param sessionData
     * @param context
     * @return 
     */
    public boolean validateAgent(SessionData sessionData, ServletContext context) {
        Rep.cat.debug("Begin validateAgent");
        boolean valid = true;

        try {
            String deploymentType = DebisysConfigListener.getDeploymentType(context);

            String strDistChainType = sessionData.getProperty("dist_chain_type");
            this.validationErrors.clear();
            String strAccessLevel = sessionData.getProperty("access_level");
            if ((this.parentRepId == null) || (this.parentRepId.length() == 0)) {
                // Only way to add an agent is via 5 level ISO
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && (strAccessLevel.equals(DebisysConstants.ISO))) {
                    this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.agents_add.error_agent", sessionData.getLanguage()));
                    valid = false;
                }
            } else if (!NumberUtil.isNumeric(this.parentRepId)) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.agents_add.error_agent_id", sessionData.getLanguage()));
                valid = false;

            }

            if ((this.businessName == null) || (this.businessName.length() == 0)) {
                this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name", sessionData.getLanguage()));
                valid = false;
            } else if (this.businessName.length() > 50) {
                this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.address == null) || (this.address.length() == 0)) {
                this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address", sessionData.getLanguage()));
                valid = false;
            } else if (this.address.length() > 50) {
                this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address_length", sessionData.getLanguage()));
                valid = false;
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                if ((this.city == null) || (this.city.length() == 0)) {
                    this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city", sessionData.getLanguage()));
                    valid = false;
                } else if (this.city.length() > 50) {
                    this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city_length", sessionData.getLanguage()));
                    valid = false;
                }

                if ((this.state == null) || (this.state.length() == 0)) {
                    this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state", sessionData.getLanguage()));
                    valid = false;
                } else if (this.state.length() > 2) {
                    this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state_length", sessionData.getLanguage()));
                    valid = false;
                }

                if ((this.zip == null) || (this.zip.length() == 0)) {
                    this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip", sessionData.getLanguage()));
                    valid = false;
                } else if (this.zip.length() > 50) {
                    this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip_length", sessionData.getLanguage()));
                    valid = false;
                }
            }

            if (this.country != null && this.country.length() > 50) {
                this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country_length", sessionData.getLanguage()));
                valid = false;
            }

            if (!StringUtil.isNotEmptyStr(this.country)) {
                this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country", sessionData.getLanguage()));
                valid = false;
            }
            if ((this.contactFirst == null) || (this.contactFirst.length() == 0)) {
                this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactFirst.length() > 15) {
                this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactLast == null) || (this.contactLast.length() == 0)) {
                this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactLast.length() > 15) {
                this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactMiddle != null) && (this.contactMiddle.length() > 1)) {
                this.addFieldError("contactMiddle", Languages.getString("jsp.admin.customers.reps_add.error_contact_middle", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactPhone == null) || (this.contactPhone.length() == 0)) {
                this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactPhone.length() > 15) {
                this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactFax != null) && (this.contactFax.length() > 15)) {
                this.addFieldError("contactFax", Languages.getString("jsp.admin.customers.reps_add.error_fax_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactEmail != null) && (this.contactEmail.length() > 200)) {
                this.addFieldError("contactEmail", Languages.getString("jsp.admin.customers.reps_add.error_email_length", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactEmail != null) {
                // commented out because we have multiple emails in a field, i.e.
                // aaa@aaa.com;bbb@bbb.com
                /*
				 * ValidateEmail validateEmail = new ValidateEmail(); validateEmail.setStrEmailAddress(contactEmail); validateEmail.checkSyntax(); if
				 * (!validateEmail.isEmailValid()) { addFieldError("contactEmail", "Please enter a valid email address."); valid = false; }
                 */

            }

            if ((this.routingNumber != null) && (this.routingNumber.length() > 18)) {
                this.addFieldError("routingNumber", Languages.getString("jsp.admin.customers.reps_add.error_routing_number", sessionData.getLanguage()));
                valid = false;
            } else if (this.routingNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                this.routingNumber = "NA";
            }

            if ((this.accountNumber != null) && (this.accountNumber.length() > 20)) {
                this.addFieldError("accountNumber", Languages.getString("jsp.admin.customers.reps_add.error_account_number", sessionData.getLanguage()));
                valid = false;
            } else if (this.accountNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                this.accountNumber = "NA";
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {

                if ((this.taxId == null) || this.taxId.equals("")) {
                    this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id_mandatory",
                            sessionData.getLanguage()));
                    valid = false;
                } else {
                    Pattern p = Pattern.compile("^(?!(\\d)\\1+$|(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){8}\\d$)\\d{9}$");
                    Matcher match = p.matcher(this.taxId);
                    if (!match.matches()) {
                        this.addFieldError("taxId", Languages.getString(
                                "jsp.admin.customers.reps_add.error_tax_length", sessionData.getLanguage()));
                        valid = false;
                    }
                }

            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {
                if ((this.taxId != null) && (this.taxId.length() > 15)) {
                    this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id", sessionData.getLanguage()));
                    valid = false;
                }
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)
                        || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)
                        || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE)) {
                    if ((this.creditLimit == null) || (this.creditLimit.length() == 0) || (!NumberUtil.isNumeric(this.creditLimit))) {
                        if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) // credit limit is required for all prepaid and credit based
                        // reps
                        {
                            this.addFieldError("creditLimit", Languages.getString("jsp.admin.customers.reps_add.error_liability_limit1", sessionData.getLanguage()));
                            valid = false;
                        }
                    } else if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                        // check for valid liability limit
                        if (!NumberUtil.isNumeric(this.creditLimit)) {
                            this.addFieldError("creditLimit", Languages.getString("jsp.admin.customers.reps_add.error_liability_limit2", sessionData.getLanguage()));
                            valid = false;
                        }
                    } else {
                        this.creditLimit = "0.00";
                    }
                } else // invalid repType
                {
                    this.addFieldError("repType", Languages.getString("jsp.admin.customers.reps_add.error_rep_type", sessionData.getLanguage()));
                    valid = false;
                }

            }// end if intl

            if (deploymentType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                if ((this.legalBusinessname != null) && (this.legalBusinessname.length() == 0)) {
                    this.addFieldError("legalBusinessname", Languages.getString("jsp.admin.customers.reps_edit.legal_businessname", sessionData.getLanguage()));
                    valid = false;
                }
            }

        } catch (Exception e) {
            Rep.cat.error("Exception in validateAgent: " + e);
        }
        Rep.cat.debug("End validateAgent");
        return valid;

    }

    /**
     * Adds a new SubAgent in the database and sets the entity id of the object
     * to the newly created record's ID
     *
     * @param sessionData
     * @param context
     * @throws com.debisys.exceptions.EntityException Thrown if there is an
     * error inserting the record in the database.
     */
    public void addSubAgent(SessionData sessionData, ServletContext context) throws EntityException {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmt1 = null;
        ResultSet r = null;
        ResultSet rs = null;
        try {
            HashMap map = CountryInfo.getAllCountriesID();
            String isoid = sessionData.getProperty("ref_id");
            long v = 0;
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new EntityException();
            }
            String SQL = Rep.sql_bundle.getString("getSubAgentIso");
            Rep.cat.debug(SQL);
            pstmt1 = dbConn.prepareStatement(SQL);
            try {
                v = Long.valueOf(isoid);
            } catch (NumberFormatException e) {
                Rep.cat.error("NumberFormatException Parsing ParentID to long" + e);
            }
            pstmt1.setLong(1, v);

            r = pstmt1.executeQuery();
            while (r.next()) {
                isoid = r.getString(1);
            }
            Rep.cat.debug("ISOID from database  = " + isoid);

            r.close();
            pstmt1.close();

            Rep.cat.debug("the staticIDCountryISO1 info is : " + context.getAttribute("staticIDCountryISO1"));
            Rep.cat.debug("the staticIDConstantValue1 info is : " + context.getAttribute("staticIDConstantValue1"));
            Rep.cat.debug("the ParentID is : " + this.parentRepId);
            Vector<String> staticISO = new Vector<String>();
            // check if Country ISO matches Static ISO Conditions
            // Static ISO added to define constant values for a country ISO
            for (int i = 1;; i++) {
                if (context.getAttribute("staticIDCountryISO" + i) == null || context.getAttribute("staticIDCountryISO" + i) == "") {
                    break;
                }
                if (context.getAttribute("staticIDConstantValue" + i) == null || context.getAttribute("staticIDConstantValue" + i) == "") {
                    break;
                }
                if (isoid.equals(context.getAttribute("staticIDCountryISO" + i))) {
                    staticISO.add((String) context.getAttribute("staticIDCountryISO" + i));
                    staticISO.add((String) context.getAttribute("staticIDConstantValue" + i));
                    break;
                }
            }
            if (staticISO.size() < 2) {
                this.repId = this.generateRepId();// if no special cases, id is
                // generated randomly
                Rep.cat.debug("Generate ID without constants values==>" + this.repId);
            } else {
                this.repId = this.generatePartiallyStaticId(staticISO);
                Rep.cat.debug("partial Approach generatedID==>" + this.repId);
            }

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new EntityException();
            }
            String strSQL = Rep.sql_bundle.getString("addRep");
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            // rep id
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            pstmt.setInt(2, 5); // type = 5 = Subagent
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && strAccessLevel.equals(DebisysConstants.AGENT))) // if you are a 3 level iso or a 5 level subagent, then the
            // parent rep should be the person logged in
            {
                pstmt.setDouble(3, Double.parseDouble(strRefId));
            } else // otherwise you must be a 5 level iso or agent
            {
                pstmt.setDouble(3, Double.parseDouble(this.parentRepId));
            }

            pstmt.setString(4, this.businessName);
            // initials

            String initials = this.contactFirst.substring(0, 1);
            if (this.contactMiddle == null || this.contactMiddle.trim().equals("")) {
                initials = initials + "_";
            } else {
                initials = initials + this.contactMiddle.substring(0, 1);
            }
            initials = initials + this.contactLast.substring(0, 1);

            pstmt.setString(5, initials);
            pstmt.setString(6, this.contactFirst);
            pstmt.setString(7, this.contactLast);
            pstmt.setString(8, this.contactMiddle);
            pstmt.setString(9, this.taxId);
            pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
            pstmt.setString(11, this.address);
            pstmt.setString(12, this.city);
            pstmt.setString(13, this.state);
            pstmt.setString(14, this.zip);
            pstmt.setString(15, this.contactPhone);
            pstmt.setString(16, this.contactFax);
            pstmt.setString(17, this.contactEmail);
            pstmt.setString(18, this.routingNumber);
            pstmt.setString(19, this.accountNumber);
            // running liability
            pstmt.setDouble(20, 0);
            pstmt.setDouble(21, Double.parseDouble(this.creditLimit));
            if (getISOCreditType(this.parentRepId).equals(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE)) {
                this.setRepCreditType(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE);
                Rep.cat.debug("The Credit Type set to: " + this.repCreditType);
            }
            if (this.repCreditType == null) {
                // this is domestic so lets add the rep type for them
                // there is no concept of prepaid domestically so they
                // are credit or unlimited
                if (Double.parseDouble(this.creditLimit) > 0) {
                    this.repCreditType = DebisysConstants.REP_CREDIT_TYPE_CREDIT;
                } else {
                    this.repCreditType = DebisysConstants.REP_CREDIT_TYPE_UNLIMITED;
                }
            }
            pstmt.setInt(22, Integer.parseInt(this.repCreditType));
            pstmt.setString(23, this.country);
            pstmt.setString(24, this.sharedBalance);
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (this.mailState != null)) {
                pstmt.setString(25, this.mailAddress);
                pstmt.setString(26, this.mailCity);
                pstmt.setString(27, this.mailState);
                pstmt.setString(28, this.mailZip);
                pstmt.setString(29, this.mailCountry);
                pstmt.setString(30, this.mailDelegation);
                pstmt.setString(31, this.mailCounty);
                pstmt.setString(32, this.mailColony);
            } else {
                pstmt.setString(25, this.address);
                pstmt.setString(26, this.city);
                pstmt.setString(27, this.state);
                pstmt.setString(28, this.zip);
                pstmt.setString(29, this.country);
                pstmt.setString(30, this.physDelegation);
                pstmt.setString(31, this.physCounty);
                pstmt.setString(32, this.physColony);
            }
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                pstmt.setString(33, this.physCounty);
                pstmt.setString(34, this.physColony);
                pstmt.setString(35, this.physDelegation);
                pstmt.setString(36, this.bankName);
                pstmt.setString(37, this.businessHours);
                pstmt.setString(38, this.businessTypeId);
                pstmt.setInt(39, this.accountTypeId);
                pstmt.setInt(40, this.merchantSegmentId);
                pstmt.setString(41, this.legalBusinessname);
            } else {
                pstmt.setNull(33, Types.VARCHAR);
                pstmt.setNull(34, Types.VARCHAR);
                pstmt.setNull(35, Types.VARCHAR);
                pstmt.setNull(36, Types.VARCHAR);
                pstmt.setNull(37, Types.VARCHAR);
                pstmt.setNull(38, Types.VARCHAR);
                pstmt.setInt(39, 1);
                pstmt.setNull(40, Types.INTEGER);
                pstmt.setNull(41, Types.VARCHAR);
            }

            pstmt.setInt(42, this.paymentType);
            pstmt.setInt(43, this.processType);
            pstmt.setInt(44, this.timeZoneId);
            pstmt.setString(45, map.get(Integer.parseInt(this.country)).toString());
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                    && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                    && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                pstmt.setString(46, this.salesmanid);
                pstmt.setString(47, this.terms);
            } else {
                pstmt.setNull(46, Types.VARCHAR);
                pstmt.setNull(47, Types.VARCHAR);
            }

            // DBSY-1072 eAccounts Interface
            if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                Rep.cat.debug("dentityAccountType=" + this.dentityAccountType);
                if (this.dentityAccountType.equals("1")) {
                    pstmt.setInt(48, 1);// if Combo is disabled then default to external
                    Rep.cat.debug("dentityAccountType=" + this.dentityAccountType);
                } else {
                    pstmt.setInt(48, this.entityAccountType);
                }
            } else {
                pstmt.setInt(48, 1);
            }

            //DTU-369 Payment Notifications
            pstmt.setBoolean(49, this.isPaymentNotifications());
            if (this.isPaymentNotificationSMS()) {
                this.setPaymentNotificationType(0);
            } else if (this.isPaymentNotificationMail()) {
                this.setPaymentNotificationType(1);
            }
            pstmt.setInt(50, this.getPaymentNotificationType());
            pstmt.setString(51, this.getSmsNotificationPhone());
            pstmt.setString(52, this.getMailNotificationAddress());

            //DTU-480: Low Balance Alert Message SMS and/or email
            pstmt.setBoolean(53, this.isBalanceNotifications());
            if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays()) {
                this.setBalanceNotificationType(2);
            } else if (this.isBalanceNotificationTypeValue()) {
                this.setBalanceNotificationType(0);
            } else if (this.isBalanceNotificationTypeDays()) {
                this.setBalanceNotificationType(1);
            }
            pstmt.setInt(54, this.getBalanceNotificationType());
            if (this.isBalanceNotificationSMS()) {
                this.setBalanceNotificationNotType(0);
            } else if (this.isBalanceNotificationMail()) {
                this.setBalanceNotificationNotType(1);
            }
            pstmt.setInt(55, this.getBalanceNotificationNotType());
            pstmt.setInt(56, this.getBalanceNotificationDays());
            pstmt.setDouble(57, this.getBalanceNotificationValue());
            pstmt.setLong(58, this.getInvoiceType());

            pstmt.execute();
            pstmt.close();

            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                Iterator itContacts = this.vecContacts.iterator();
                while (itContacts.hasNext()) {
                    Vector vecTemp = null;
                    vecTemp = (Vector) itContacts.next();
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertContactMx"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, vecTemp.get(0).toString());
                    pstmt.setString(3, vecTemp.get(1).toString());
                    pstmt.setString(4, vecTemp.get(2).toString());
                    pstmt.setString(5, vecTemp.get(3).toString());
                    pstmt.setString(6, vecTemp.get(4).toString());
                    pstmt.setString(7, vecTemp.get(5).toString());
                    pstmt.setString(8, vecTemp.get(6).toString());
                    pstmt.executeUpdate();
                    pstmt.close();
                }
            }

            Log.write(sessionData, "SubAgent added.", DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
            if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                // 1 2 3 4 5 6 7 8 9 10 11
                // (rep_id, logon_id, description, amount, credit_limit,
                // prior_credit_limit, available_credit, prior_available_credit,
                // commission, datetime, entityaccounttype)
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentCredit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                pstmt.setString(2, sessionData.getProperty("username"));
                pstmt.setString(3, Languages.getString("com.debisys.customers.rep_add_payment", sessionData.getLanguage()));
                pstmt.setDouble(4, Double.parseDouble(this.creditLimit));
                pstmt.setDouble(5, Double.parseDouble(this.creditLimit));
                pstmt.setDouble(6, 0);
                pstmt.setDouble(7, Double.parseDouble(this.creditLimit));
                pstmt.setDouble(8, 0);
                pstmt.setDouble(9, 0);
                pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                {
                    if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                        Rep.cat.debug("dentityAccountType(SubAgentCredit)=" + this.dentityAccountType);
                        if (this.dentityAccountType.equals("1")) {
                            pstmt.setInt(11, 1);// if Combo is disabled then default to external
                        } else {
                            pstmt.setInt(11, this.entityAccountType);
                        }
                    } else {
                        pstmt.setInt(11, 1);
                    }
                }
                /*
				 * pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                 */
                pstmt.executeUpdate();
                pstmt.close();

                //DTU-369 Payment Notifications
                if (Rep.GetPaymentNotification(this.repId)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, sessionData.getProperty("username"));
                    pstmt.setString(3, Languages.getString("com.debisys.customers.rep_add_payment", sessionData.getLanguage()));
                    pstmt.setDouble(4, Double.parseDouble(this.creditLimit));
                    pstmt.setDouble(5, Double.parseDouble(this.creditLimit));
                    pstmt.setDouble(6, 0);
                    pstmt.setDouble(7, Double.parseDouble(this.creditLimit));
                    pstmt.setDouble(8, 0);
                    pstmt.setDouble(9, 0);
                    pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                    {
                        if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                            Rep.cat.debug("dentityAccountType(SubAgentCredit)=" + this.dentityAccountType);
                            if (this.dentityAccountType.equals("1")) {
                                pstmt.setInt(11, 1);// if Combo is disabled then default to external
                            } else {
                                pstmt.setInt(11, this.entityAccountType);
                            }
                        } else {
                            pstmt.setInt(11, 1);
                        }
                    }
                    pstmt.setString(12, this.businessName);
                    pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                    pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                    pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                    pstmt.executeUpdate();
                    pstmt.close();
                }

            }
           
            strSQL = "select rep_id from reps with (nolock) where rep_id in (select iso_id from reps with (nolock) where rep_id=" + this.repId + ")";
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.agentId = rs.getString("rep_id");
            }
            rs.close();
            pstmt.close();
            Rep agent = new Rep();
            agent.setRepId(this.agentId);
            agent.getAgent(sessionData, context);

            if (!agent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) || (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE))) {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepRunningLiability"));
                pstmt.setDouble(1, (Double.parseDouble(agent.getRunningLiability()) + Double.parseDouble(this.creditLimit)));
                pstmt.setDouble(2, Double.parseDouble(agent.getRepId()));
                pstmt.executeUpdate();
                pstmt.close();
                double agentCreditLimit = agent.getCurrentAgentCreditLimit(sessionData);
                double agentAssignedCredit = agent.getAssignedCreditAgent(sessionData);

                // add an entry to show the rep credits being used
                // 1 2 3 4 5 6 7 8
                // (rep_id, merchant_id, logon_id, description, amount,
                // available_credit, prior_available_credit, datetime)
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                pstmt.setDouble(1, Double.parseDouble(agent.getRepId()));
                pstmt.setDouble(2, Double.parseDouble(this.repId));
                pstmt.setString(3, sessionData.getProperty("username"));
                pstmt.setString(4, "\"" + this.getRepName(this.getRepId()) + "\" " + Languages.getString("com.debisys.customers.Merchant.assigned_credit", sessionData.getLanguage()));
                pstmt.setDouble(5, Double.parseDouble(this.creditLimit) * -1);
                pstmt.setDouble(6, agentCreditLimit - agentAssignedCredit);
                pstmt.setDouble(7, agentCreditLimit - (agentAssignedCredit - Double.parseDouble(this.creditLimit)));
                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                pstmt.setInt(9, agent.getEntityAccountType());
                {
                    if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                        Rep.cat.debug("dentityAccountType(AgentSubAgentCredit)=" + this.dentityAccountType);
                        if (this.dentityAccountType.equals("1")) {
                            pstmt.setInt(10, 1);// if Combo is disabled then default to external
                        } else {
                            pstmt.setInt(10, this.entityAccountType);
                        }
                    } else {
                        pstmt.setInt(10, 1);
                    }
                }
                /*
				 * Rep childsa = new Rep(); childsa.setRepId(this.repId); childsa.getSubAgent(sessionData, context); pstmt.setInt(10, childsa.getEntityAccountType());
                 */
                pstmt.executeUpdate();
            } // end of if agent credit type
            //}// End of if deployment type
        } catch (Exception e)// Main Try end
        {
            Rep.cat.error("Error during addSubagent", e);
            throw new EntityException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt1, r);
                TorqueHelper.closeStatement(pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
    }

    public boolean validateSubAgent(SessionData sessionData, ServletContext context) {
        Rep.cat.debug("Begin validateSubAgent");

        boolean valid = true;
        try {
            String deploymentType = DebisysConfigListener.getDeploymentType(context);
            
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            this.validationErrors.clear();
            String strAccessLevel = sessionData.getProperty("access_level");
            String strRefId = sessionData.getProperty("ref_id");
            if ((this.parentRepId == null) || (this.parentRepId.length() == 0)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) && (strAccessLevel.equals(DebisysConstants.ISO))) {
                    this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.subagents_add.error_subagent", sessionData.getLanguage()));
                    valid = false;
                }
                // otherwise it must be an subagent which would be the parent for a
                // 5
                // level
                // or it would be the iso in a 3 level

            } else if (!NumberUtil.isNumeric(this.parentRepId)) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.subagents_add.error_subagent_id", sessionData.getLanguage()));
                valid = false;
            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                    && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
                // make sure this is a valid subagent for this agent or iso
                if (!this.checkAgentPermission(sessionData)) {
                    this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.subagents_add.error_subagent_id2", sessionData.getLanguage()));
                    valid = false;
                }
            }

            if ((this.businessName == null) || (this.businessName.length() == 0)) {
                this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name", sessionData.getLanguage()));
                valid = false;
            } else if (this.businessName.length() > 50) {
                this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.address == null) || (this.address.length() == 0)) {
                this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address", sessionData.getLanguage()));
                valid = false;
            } else if (this.address.length() > 50) {
                this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address_length", sessionData.getLanguage()));
                valid = false;
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                if ((this.city == null) || (this.city.length() == 0)) {
                    this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city", sessionData.getLanguage()));
                    valid = false;
                } else if (this.city.length() > 50) {
                    this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city_length", sessionData.getLanguage()));
                    valid = false;
                }

                if ((this.state == null) || (this.state.length() == 0)) {
                    this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state", sessionData.getLanguage()));
                    valid = false;
                } else if (this.state.length() > 2) {
                    this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state_length", sessionData.getLanguage()));
                    valid = false;
                }

                if ((this.zip == null) || (this.zip.length() == 0)) {
                    this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip", sessionData.getLanguage()));
                    valid = false;
                } else if (this.zip.length() > 50) {
                    this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip_length", sessionData.getLanguage()));
                    valid = false;
                }
            }

            if (this.country != null && this.country.length() > 50) {
                this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country_length", sessionData.getLanguage()));
                valid = false;
            }
            if (!StringUtil.isNotEmptyStr(this.country)) {
                this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country", sessionData.getLanguage()));
                valid = false;
            }
            if ((this.contactFirst == null) || (this.contactFirst.length() == 0)) {
                this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactFirst.length() > 15) {
                this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactLast == null) || (this.contactLast.length() == 0)) {
                this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactLast.length() > 15) {
                this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactMiddle != null) && (this.contactMiddle.length() > 1)) {
                this.addFieldError("contactMiddle", Languages.getString("jsp.admin.customers.reps_add.error_contact_middle", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactPhone == null) || (this.contactPhone.length() == 0)) {
                this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactPhone.length() > 15) {
                this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactFax != null) && (this.contactFax.length() > 15)) {
                this.addFieldError("contactFax", Languages.getString("jsp.admin.customers.reps_add.error_fax_length", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.contactEmail != null) && (this.contactEmail.length() > 200)) {
                this.addFieldError("contactEmail", Languages.getString("jsp.admin.customers.reps_add.error_email_length", sessionData.getLanguage()));
                valid = false;
            } else if (this.contactEmail != null) {
                // commented out because we have multiple emails in a field, i.e.
                // aaa@aaa.com;bbb@bbb.com
                /*
				 * ValidateEmail validateEmail = new ValidateEmail(); validateEmail.setStrEmailAddress(contactEmail); validateEmail.checkSyntax(); if
				 * (!validateEmail.isEmailValid()) { addFieldError("contactEmail", "Please enter a valid email address."); valid = false; }
                 */

            }

            if ((this.routingNumber != null) && (this.routingNumber.length() > 18)) {
                this.addFieldError("routingNumber", Languages.getString("jsp.admin.customers.reps_add.error_routing_number", sessionData.getLanguage()));
                valid = false;
            } else if (this.routingNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                this.routingNumber = "NA";
            }

            if ((this.accountNumber != null) && (this.accountNumber.length() > 20)) {
                this.addFieldError("accountNumber", Languages.getString("jsp.admin.customers.reps_add.error_account_number", sessionData.getLanguage()));
                valid = false;
            } else if (this.accountNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                this.accountNumber = "NA";
            }

            if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {

                if ((this.taxId == null) || this.taxId.equals("")) {
                    this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id_mandatory",
                            sessionData.getLanguage()));
                    valid = false;
                } else {
                    Pattern p = Pattern.compile("^(?!(\\d)\\1+$|(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){8}\\d$)\\d{9}$");
                    Matcher match = p.matcher(this.taxId);
                    if (!match.matches()) {
                        this.addFieldError("taxId", Languages.getString(
                                "jsp.admin.customers.reps_add.error_tax_length", sessionData.getLanguage()));
                        valid = false;
                    }
                }

            } else if ((this.taxId != null) && (this.taxId.length() > 15)) {
                this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id", sessionData.getLanguage()));
                valid = false;
            }

            //if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
            //{
            if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID) || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)
                    || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)
                    || this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_FLEXIBLE)) {
                if ((this.creditLimit == null) || (this.creditLimit.length() == 0) || (!NumberUtil.isNumeric(this.creditLimit))) {
                    if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) // credit limit is required for all prepaid and credit based
                    // reps
                    {
                        this.addFieldError("creditLimit", Languages.getString("jsp.admin.customers.reps_add.error_liability_limit1", sessionData.getLanguage()));
                        valid = false;
                    }
                } else if (!this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                    // check for valid liability limit
                    if (!NumberUtil.isNumeric(this.creditLimit)) {
                        this.addFieldError("creditLimit", Languages.getString("jsp.admin.customers.reps_add.error_liability_limit2", sessionData.getLanguage()));
                        valid = false;
                    }
                } else {
                    this.creditLimit = "0.00";
                }
            } else // invalid repType
            {
                this.addFieldError("repType", Languages.getString("jsp.admin.customers.reps_add.error_rep_type", sessionData.getLanguage()));
                valid = false;
            }

            if ((this.parentRepId != null) && (this.parentRepId.length() != 0)) {
                Rep agent = new Rep();

                try {
                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                        agent.setRepId(this.parentRepId);
                    }

                    agent.getAgent(sessionData, context);
                    this.agentCreditType = agent.getRepCreditType();
                } catch (Exception e) {
                }

                // loaded up with Agent info now lets do the check
                if (!this.agentCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                    // this means the agent is prepaid or credit based
                    // so check to make sure there is available credit
                    if (this.repCreditType.equals(DebisysConstants.MERCHANT_TYPE_UNLIMITED)) {
                        // this is an error
                        // cant assign an unlimited subagent to a limited agent
                        this.addFieldError("creditType", Languages.getString("com.debisys.customers.agent_credit_error3", sessionData.getLanguage()));
                        valid = false;
                    } else // check the credit limit to make sure there is enough
                     if (Double.parseDouble(agent.getAvailableCredit()) < Double.parseDouble(this.creditLimit)) {
                            this.addFieldError("creditLimit", Languages.getString("com.debisys.customers.agent_credit_error4", sessionData.getLanguage())
                                    + NumberUtil.formatAmount(agent.getAvailableCredit()));
                            valid = false;
                        }
                }
            }

            //}// end if intl
            if (deploymentType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                if ((this.legalBusinessname != null) && (this.legalBusinessname.length() == 0)) {
                    this.addFieldError("legalBusinessname", Languages.getString("jsp.admin.customers.reps_edit.legal_businessname", sessionData.getLanguage()));
                    valid = false;
                }
            }
        } catch (Exception e) {
            Rep.cat.debug("Exception in validateSubAgent: " + e);
            e.printStackTrace();
        }
        Rep.cat.debug("End validateSubAgent");
        return valid;

    }

    private boolean checkAgentPermission(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAllowed = false;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            String strSQL = Rep.sql_bundle.getString("checkIsoAgentRep");

            // String strDistChainType =
            // sessionData.getProperty("dist_chain_type");//Never read
            String strAccessLevel = sessionData.getProperty("access_level");
            String strRefId = sessionData.getProperty("ref_id");
            // iso
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and rep_id="
                        + this.parentRepId + " and iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL
                        + " and iso_id= " + strRefId + "))";
            } // agent
            else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " r.rep_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and rep_id="
                        + strRefId + ")";
            } else {
                isAllowed = false;
                return isAllowed;
            }
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                isAllowed = true;
            } else {
                isAllowed = false;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkAgentPermission", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isAllowed;
    }

    // ADDED BY MCR - SPG
    /**
     * This method returns all information related to Agent. Only way to see the
     * information is either login as 5 level ISO or Agent.
     * @param sessionData
     * @param context
     * @throws com.debisys.exceptions.CustomerException
     */
    public void getIso(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getRep");
            // getRep=select r.* from reps r (nolock) where r.rep_id=?
            // permissions are built into the queries

            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " AND r.rep_id IN (SELECT DISTINCT  r3.iso_id  from reps r3 WITH (NOLOCK) WHERE r3.rep_id IN ( "
                        + "SELECT DISTINCT  r2.iso_id  from reps r2 WITH (NOLOCK) WHERE r2.rep_id IN ( "
                        + "SELECT DISTINCT r1.iso_id  from reps r1 WITH (NOLOCK) "
                        + "INNER JOIN merchants WITH (NOLOCK) ON merchants.rep_id = r1.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON merchants.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.carrierid = rc.carrierid "
                        + "WHERE r1.type = 1 AND rc.RepID = " + strRefId
                        + "))) AND r.type IN (2,3) ";
            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id = " + strRefId;
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id="
                            + strRefId + ")) ";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " and iso_id= " + strRefId + ") ";

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                //strSQL = strSQL + " and r.iso_id=" + strRefId;
                strSQL = strSQL + " WHERE r.rep_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_REP
                        + " and iso_id = " + strRefId + ") ";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                // do nothing but dont erase the sql statement
                strSQL = strSQL + " and r.iso_id=" + strRefId;
            } else {
                strSQL = "";
            }
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.repId = rs.getString("rep_id");
                this.parentRepId = rs.getString("iso_id");
                this.businessName = rs.getString("businessname");
                this.contactFirst = rs.getString("firstname");
                this.contactLast = rs.getString("lastname");
                this.contactMiddle = rs.getString("minitial");
                this.contactEmail = rs.getString("email");
                this.contactPhone = rs.getString("phone");
                this.contactFax = rs.getString("fax");
                this.address = rs.getString("address");
                this.city = rs.getString("city");
                this.state = rs.getString("state");
                this.zip = rs.getString("zip");
                this.country = rs.getString("country_id");

                this.runningLiability = Double.toString(this.getAssignedCreditISO(sessionData));
                this.creditLimit = NumberUtil.formatAmount(rs.getString("LiabilityLimit"));
                this.repCreditType = rs.getString("rep_credit_type");

                this.taxId = rs.getString("ssn");
                this.routingNumber = rs.getString("aba");
                this.accountNumber = rs.getString("account_no");

                this.availableCredit = Double.toString(Double.parseDouble(this.creditLimit) - Double.parseDouble(this.runningLiability));

                this.physCounty = rs.getString("phys_county");
                this.physColony = rs.getString("phys_colony");
                this.physDelegation = rs.getString("phys_delegation");
                this.mailAddress = rs.getString("mail_address");
                this.mailCity = rs.getString("mail_city");
                this.mailState = rs.getString("mail_state");
                this.mailZip = rs.getString("mail_zip");
                this.mailColony = rs.getString("mail_colony");
                this.mailDelegation = rs.getString("mail_delegation");
                this.mailCounty = rs.getString("mail_county");
                this.mailCountry = rs.getString("mail_country_id");
                this.bankName = rs.getString("BankName");
                this.businessHours = rs.getString("BusinessHours");
                this.businessTypeId = rs.getString("sic_code");
                this.accountTypeId = rs.getInt("AccountTypeID");
                this.merchantSegmentId = rs.getInt("MerchantSegmentID");
                this.sharedBalance = rs.getString("SharedBalance");
                this.legalBusinessname = rs.getString("legal_businessname");
                this.paymentType = rs.getInt("payment_type");
                this.processType = rs.getInt("payment_processor");
                this.timeZoneId = rs.getInt("TimeZoneID");
                if (this.timeZoneId == 0) {
                    this.timeZoneId = Integer.parseInt(TimeZone.getDefaultTimeZone().get(0).toString());
                }

                // DBSY-931 System 2000
                String temp = rs.getString("salesman_id");
                if (temp != null && temp != "") {

                    if (this.verifyid(temp.trim(), Rep.getIsoID(strRefId, strAccessLevel))) {
                        this.salesmanid = temp;
                        this.salesmanname = this.getsalesmannamebyid(temp.trim(), strRefId);
                    }
                }
                String temp2 = rs.getString("terms");
                if (StringUtil.isNotEmptyStr(temp2)) {
                    this.terms = temp2;
                }

                this.entityAccountType = rs.getInt("EntityAccountType");

            } else {
                this.addFieldError("error", "Agent not found.");
            }
            rs.close();
            pstmt.close();
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getContactsMx"));
                pstmt.setString(1, this.repId);
                rs = pstmt.executeQuery();
                this.vecContacts = new Vector();
                while (rs.next()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(rs.getString("ContactTypeID"));
                    vecTemp.add(rs.getString("ContactName"));
                    vecTemp.add(rs.getString("ContactPhone"));
                    vecTemp.add(rs.getString("ContactFax"));
                    vecTemp.add(rs.getString("ContactEmail"));
                    vecTemp.add(rs.getString("ContactCellPhone"));
                    vecTemp.add(rs.getString("ContactDepartment"));
                    this.vecContacts.add(vecTemp);
                }

                if (this.vecContacts.size() == 0) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(Merchant.getContactTypeByCode("OTHER", sessionData).get(0).toString());
                    vecTemp.add(((this.contactFirst != null) ? this.contactFirst : ""));
                    vecTemp.add(((this.contactPhone != null) ? this.contactPhone : ""));
                    vecTemp.add(((this.contactFax != null) ? this.contactFax : ""));
                    vecTemp.add(((this.contactEmail != null) ? this.contactEmail : ""));
                    vecTemp.add("");
                    vecTemp.add("");
                    this.vecContacts.add(vecTemp);
                }
            }// End of if when MX retrieve contacts
        } catch (Exception e) {
            Rep.cat.error("Error during getAgent", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public double getAssignedCreditISO(SessionData sessionData) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double assignedCredit = 0;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            // select r.runningliability as assigned_credit from reps r (nolock)
            // where r.rep_id = ?
            String strSQL = Rep.sql_bundle.getString("getRepAssignedCredit");
            // permissions are built into the queries

            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " AND r.rep_id IN (SELECT DISTINCT  r3.iso_id  from reps r3 WITH (NOLOCK) WHERE r3.rep_id IN ( "
                        + "SELECT DISTINCT  r2.iso_id  from reps r2 WITH (NOLOCK) WHERE r2.rep_id IN ( "
                        + "SELECT DISTINCT r1.iso_id  from reps r1 WITH (NOLOCK) "
                        + "INNER JOIN merchants  WITH (NOLOCK) ON merchants.rep_id = r1.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON merchants.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.carrierid = rc.carrierid "
                        + "WHERE r1.type = 1 AND rc.RepID = " + strRefId
                        + "))) AND r.type IN (2,3) ";
            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id = " + strRefId;
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id="
                            + strRefId + ")) ";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " and iso_id= " + strRefId + ") ";

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                //strSQL = strSQL + " and r.iso_id=" + strRefId;
                strSQL = strSQL + " WHERE r.rep_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_REP
                        + " and iso_id = " + strRefId + ") ";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                // do nothing but dont erase the sql statement
                strSQL = strSQL + " and r.iso_id=" + strRefId;
            } else {
                strSQL = "";
            }

            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));

            rs = pstmt.executeQuery();
            if (rs.next()) {
                assignedCredit = rs.getDouble("assigned_credit");
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_record", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getAssignedCreditAgent", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return assignedCredit;
    }
    // ADDED BY MCR - SPG

    // DBSYS- 613 Nidhi Gulati
    /**
     * This method returns all information related to Agent. Only way to see the
     * information is either login as 5 level ISO or Agent.
     * @param sessionData
     * @param context
     * @throws com.debisys.exceptions.CustomerException
     */
    public void getAgent(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getRep");
            // getRep=select r.* from reps r (nolock) where r.rep_id=?
            // permissions are built into the queries

            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " AND r.rep_id IN "
                        + "( SELECT DISTINCT  r2.iso_id from reps r2 WITH (NOLOCK) "
                        + "WHERE r2.rep_id IN ( SELECT DISTINCT r1.iso_id from reps r1 WITH (NOLOCK) "
                        + "INNER JOIN merchants  WITH (NOLOCK) ON merchants.rep_id = r1.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "WHERE r1.type = 1 AND rc.RepID = " + strRefId + ") AND r2.type=5 )"
                        + " AND r.type=4 ";
            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL
                        + " and iso_id= " + strRefId + ") ";

            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                // do nothing but dont erase the sql statement

            } else {
                strSQL = "";
            }
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.repId = rs.getString("rep_id");
                this.parentRepId = rs.getString("iso_id");
                this.businessName = rs.getString("businessname");
                this.contactFirst = rs.getString("firstname");
                this.contactLast = rs.getString("lastname");
                this.contactMiddle = rs.getString("minitial");
                this.contactEmail = rs.getString("email");
                this.contactPhone = rs.getString("phone");
                this.contactFax = rs.getString("fax");
                this.address = rs.getString("address");
                this.city = rs.getString("city");
                this.state = rs.getString("state");
                this.zip = rs.getString("zip");
                this.country = rs.getString("country_id");

                this.runningLiability = Double.toString(this.getAssignedCreditAgent(sessionData));
                this.creditLimit = NumberUtil.formatAmount(rs.getString("LiabilityLimit"));
                this.repCreditType = rs.getString("rep_credit_type");

                this.taxId = rs.getString("ssn");
                this.routingNumber = rs.getString("aba");
                this.accountNumber = rs.getString("account_no");

                this.availableCredit = Double.toString(Double.parseDouble(this.creditLimit) - Double.parseDouble(this.runningLiability));

                this.physCounty = rs.getString("phys_county");
                this.physColony = rs.getString("phys_colony");
                this.physDelegation = rs.getString("phys_delegation");
                this.mailAddress = rs.getString("mail_address");
                this.mailCity = rs.getString("mail_city");
                this.mailState = rs.getString("mail_state");
                this.mailZip = rs.getString("mail_zip");
                this.mailColony = rs.getString("mail_colony");
                this.mailDelegation = rs.getString("mail_delegation");
                this.mailCounty = rs.getString("mail_county");
                this.mailCountry = rs.getString("mail_country_id");
                this.bankName = rs.getString("BankName");
                this.businessHours = rs.getString("BusinessHours");
                this.businessTypeId = rs.getString("sic_code");
                this.accountTypeId = rs.getInt("AccountTypeID");
                this.merchantSegmentId = rs.getInt("MerchantSegmentID");
                this.sharedBalance = rs.getString("SharedBalance");
                this.legalBusinessname = rs.getString("legal_businessname");
                this.paymentType = rs.getInt("payment_type");
                this.processType = rs.getInt("payment_processor");
                this.timeZoneId = rs.getInt("TimeZoneID");
                if (this.timeZoneId == 0) {
                    this.timeZoneId = Integer.parseInt(TimeZone.getDefaultTimeZone().get(0).toString());
                }

                // DBSY-931 System 2000
                String temp = rs.getString("salesman_id");
                if (temp != null && temp != "") {

                    if (this.verifyid(temp.trim(), Rep.getIsoID(strRefId, strAccessLevel))) {
                        this.salesmanid = temp;
                        this.salesmanname = this.getsalesmannamebyid(temp.trim(), strRefId);
                    }
                }
                String temp2 = rs.getString("terms");
                if (StringUtil.isNotEmptyStr(temp2)) {
                    this.terms = temp2;
                }

                this.entityAccountType = rs.getInt("EntityAccountType");

                //DTU-369 Payment Notifications
                this.paymentNotifications = rs.getBoolean("payment_notifications");
                this.paymentNotificationType = rs.getInt("payment_notification_type");
                if (this.paymentNotificationType == 0) {
                    this.paymentNotificationSMS = true;
                } else if (this.paymentNotificationType == 1) {
                    this.paymentNotificationMail = true;
                }
                this.smsNotificationPhone = rs.getString("sms_notification_phone");
                this.mailNotificationAddress = rs.getString("mail_notification_address");

                //DTU-480: Low Balance Alert Message SMS and/or email
                this.balanceNotifications = rs.getBoolean("balance_notifications");
                this.balanceNotificationType = rs.getInt("balance_notification_type");
                switch (this.balanceNotificationType) {
                    case 0:
                        this.balanceNotificationTypeValue = true;
                        break;
                    case 1:
                        this.balanceNotificationTypeDays = true;
                        break;
                    case 2:
                        this.balanceNotificationTypeValue = true;
                        this.balanceNotificationTypeDays = true;
                        break;
                    default:
                        break;
                }
                this.balanceNotificationValue = rs.getDouble("balance_notification_value");
                this.balanceNotificationDays = rs.getInt("balance_notification_days");
                this.balanceNotificationNotType = rs.getInt("balance_notification_not_type");
                if (this.balanceNotificationNotType == 0) {
                    this.balanceNotificationSMS = true;
                } else if (this.balanceNotificationNotType == 1) {
                    this.balanceNotificationMail = true;
                }
            } else {
                this.addFieldError("error", "Agent not found.");
            }
            rs.close();
            pstmt.close();
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getContactsMx"));
                pstmt.setString(1, this.repId);
                rs = pstmt.executeQuery();
                this.vecContacts = new Vector();
                while (rs.next()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(rs.getString("ContactTypeID"));
                    vecTemp.add(rs.getString("ContactName"));
                    vecTemp.add(rs.getString("ContactPhone"));
                    vecTemp.add(rs.getString("ContactFax"));
                    vecTemp.add(rs.getString("ContactEmail"));
                    vecTemp.add(rs.getString("ContactCellPhone"));
                    vecTemp.add(rs.getString("ContactDepartment"));
                    this.vecContacts.add(vecTemp);
                }

                if (this.vecContacts.isEmpty()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(Merchant.getContactTypeByCode("OTHER", sessionData).get(0).toString());
                    vecTemp.add(((this.contactFirst != null) ? this.contactFirst : ""));
                    vecTemp.add(((this.contactPhone != null) ? this.contactPhone : ""));
                    vecTemp.add(((this.contactFax != null) ? this.contactFax : ""));
                    vecTemp.add(((this.contactEmail != null) ? this.contactEmail : ""));
                    vecTemp.add("");
                    vecTemp.add("");
                    this.vecContacts.add(vecTemp);
                }
            }// End of if when MX retrieve contacts
        } catch (Exception e) {
            Rep.cat.error("Error during getAgent", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public double getAssignedCreditAgent(SessionData sessionData) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double assignedCredit = 0;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            // select r.runningliability as assigned_credit from reps r (nolock)
            // where r.rep_id = ?
            String strSQL = Rep.sql_bundle.getString("getRepAssignedCredit");
            // permissions are built into the queries

            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " AND r.rep_id IN "
                        + "( SELECT DISTINCT  r2.iso_id from reps r2 WITH (NOLOCK) "
                        + "WHERE r2.rep_id IN ( SELECT DISTINCT r1.iso_id from reps r1 WITH (NOLOCK) "
                        + "INNER JOIN merchants  WITH (NOLOCK) ON merchants.rep_id = r1.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "WHERE r1.type = 1 AND rc.RepID = " + strRefId + ") AND r2.type=5 )"
                        + " AND r.type=4 ";

            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL
                        + " and iso_id= " + strRefId + ") ";
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                // Do Nothing
            } else {
                strSQL = "";
            }

            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));

            rs = pstmt.executeQuery();
            if (rs.next()) {
                assignedCredit = rs.getDouble("assigned_credit");
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_record", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getAssignedCreditAgent", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return assignedCredit;
    }

    public void getSubAgent(SessionData sessionData, ServletContext context) throws CustomerException {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getRep");
            // getRep=select r.* from reps r (nolock) where r.rep_id=?
            // permissions are built into the queries
            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " AND r.rep_id "
                        + "IN ( SELECT DISTINCT r1.iso_id from reps r1 WITH (NOLOCK) "
                        + "INNER JOIN merchants  WITH (NOLOCK) ON merchants.rep_id = r1.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "WHERE r1.type = 1 AND rc.RepID = " + strRefId + ")  "
                        + "AND r.type=5   ";
            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {

                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id in "
                        + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL + " and iso_id=" + strRefId + ")) ";

            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id=" + strRefId;

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                // Do nothing but dont erase the sql
            } else {
                strSQL = "";
            }
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.repId = rs.getString("rep_id");
                this.parentRepId = rs.getString("iso_id");
                this.oldParentRepId = rs.getString("iso_id");
                this.businessName = rs.getString("businessname");
                this.contactFirst = rs.getString("firstname");
                this.contactLast = rs.getString("lastname");
                this.contactMiddle = rs.getString("minitial");
                this.contactEmail = rs.getString("email");
                this.contactPhone = rs.getString("phone");
                this.contactFax = rs.getString("fax");
                this.address = rs.getString("address");
                this.city = rs.getString("city");
                this.state = rs.getString("state");
                this.zip = rs.getString("zip");
                this.country = rs.getString("country_id");

                this.runningLiability = Double.toString(this.getAssignedCreditSubAgent(sessionData));
                this.creditLimit = NumberUtil.formatAmount(rs.getString("LiabilityLimit"));
                this.repCreditType = rs.getString("rep_credit_type");

                this.taxId = rs.getString("ssn");
                this.routingNumber = rs.getString("aba");
                this.accountNumber = rs.getString("account_no");

                this.availableCredit = Double.toString(Double.parseDouble(this.creditLimit) - Double.parseDouble(this.runningLiability));

                this.physCounty = rs.getString("phys_county");
                this.physColony = rs.getString("phys_colony");
                this.physDelegation = rs.getString("phys_delegation");
                this.mailAddress = rs.getString("mail_address");
                this.mailCity = rs.getString("mail_city");
                this.mailState = rs.getString("mail_state");
                this.mailZip = rs.getString("mail_zip");
                this.mailColony = rs.getString("mail_colony");
                this.mailDelegation = rs.getString("mail_delegation");
                this.mailCounty = rs.getString("mail_county");
                this.mailCountry = rs.getString("mail_country_id");
                this.bankName = rs.getString("BankName");
                this.businessHours = rs.getString("BusinessHours");
                this.businessTypeId = rs.getString("sic_code");
                this.accountTypeId = rs.getInt("AccountTypeID");
                this.merchantSegmentId = rs.getInt("MerchantSegmentID");
                this.sharedBalance = rs.getString("SharedBalance");
                this.legalBusinessname = rs.getString("legal_businessname");
                this.paymentType = rs.getInt("payment_type");
                this.processType = rs.getInt("payment_processor");
                this.timeZoneId = rs.getInt("TimeZoneID");
                // DBSY-931 System 2000
                String temp = rs.getString("salesman_id");
                if (StringUtil.isNotEmptyStr(temp)) {
                    if (this.verifyid(temp.trim(), Rep.getIsoID(strRefId, strAccessLevel))) {
                        this.salesmanid = temp;
                        this.salesmanname = this.getsalesmannamebyid(temp.trim(), strRefId);
                    }
                }
                String temp2 = rs.getString("terms");

                if (StringUtil.isNotEmptyStr(temp2)) {
                    this.terms = temp2;
                }

                if (this.timeZoneId == 0) {
                    this.timeZoneId = Integer.parseInt(TimeZone.getDefaultTimeZone().get(0));
                }

                this.entityAccountType = rs.getInt("EntityAccountType");

                //DTU-369 Payment Notifications
                this.paymentNotifications = rs.getBoolean("payment_notifications");
                this.paymentNotificationType = rs.getInt("payment_notification_type");
                if (this.paymentNotificationType == 0) {
                    this.paymentNotificationSMS = true;
                } else if (this.paymentNotificationType == 1) {
                    this.paymentNotificationMail = true;
                }
                this.smsNotificationPhone = rs.getString("sms_notification_phone");
                this.mailNotificationAddress = rs.getString("mail_notification_address");

                //DTU-480: Low Balance Alert Message SMS and/or email
                this.balanceNotifications = rs.getBoolean("balance_notifications");
                this.balanceNotificationType = rs.getInt("balance_notification_type");
                switch (this.balanceNotificationType) {
                    case 0:
                        this.balanceNotificationTypeValue = true;
                        break;
                    case 1:
                        this.balanceNotificationTypeDays = true;
                        break;
                    case 2:
                        this.balanceNotificationTypeValue = true;
                        this.balanceNotificationTypeDays = true;
                        break;
                    default:
                        break;
                }
                this.balanceNotificationValue = rs.getDouble("balance_notification_value");
                this.balanceNotificationDays = rs.getInt("balance_notification_days");
                this.balanceNotificationNotType = rs.getInt("balance_notification_not_type");
                if (this.balanceNotificationNotType == 0) {
                    this.balanceNotificationSMS = true;
                } else if (this.balanceNotificationNotType == 1) {
                    this.balanceNotificationMail = true;
                }
            } else {
                this.addFieldError("error", "SubAgent not found.");
            }
            TorqueHelper.closeStatement(pstmt, rs);
            if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getContactsMx"));
                pstmt.setString(1, this.repId);
                rs = pstmt.executeQuery();
                this.vecContacts = new Vector();
                while (rs.next()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(rs.getString("ContactTypeID"));
                    vecTemp.add(rs.getString("ContactName"));
                    vecTemp.add(rs.getString("ContactPhone"));
                    vecTemp.add(rs.getString("ContactFax"));
                    vecTemp.add(rs.getString("ContactEmail"));
                    vecTemp.add(rs.getString("ContactCellPhone"));
                    vecTemp.add(rs.getString("ContactDepartment"));
                    this.vecContacts.add(vecTemp);
                }

                if (this.vecContacts.isEmpty()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(Merchant.getContactTypeByCode("OTHER", sessionData).get(0).toString());
                    vecTemp.add(((this.contactFirst != null) ? this.contactFirst : ""));
                    vecTemp.add(((this.contactPhone != null) ? this.contactPhone : ""));
                    vecTemp.add(((this.contactFax != null) ? this.contactFax : ""));
                    vecTemp.add(((this.contactEmail != null) ? this.contactEmail : ""));
                    vecTemp.add("");
                    vecTemp.add("");
                    this.vecContacts.add(vecTemp);
                }
            }// End of if when MX retrieve contacts
        } catch (Exception e) {
            Rep.cat.error("Error during getSubAgent", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }

    public double getAssignedCreditSubAgent(SessionData sessionData) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double assignedCredit = 0;
        try {

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            // select r.runningliability as assigned_credit from reps r (nolock)
            // where r.rep_id = ?
            String strSQL = Rep.sql_bundle.getString("getRepAssignedCredit");
            // permissions are built into the queries

            if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
                strSQL = strSQL + " AND r.rep_id "
                        + "IN ( SELECT DISTINCT r1.iso_id from reps r1 WITH (NOLOCK) "
                        + "INNER JOIN merchants  WITH (NOLOCK) ON merchants.rep_id = r1.rep_id "
                        + "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID "
                        + "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
                        + "WHERE r1.type = 1 AND rc.RepID = " + strRefId + ")  "
                        + "AND r.type=" + DebisysConstants.REP_TYPE_SUBAGENT;
            } else if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_AGENT
                        + " and iso_id in " + "(select rep_id from reps WITH(NOLOCK) where type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL + " and iso_id="
                        + strRefId + ")) ";

            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id=" + strRefId;

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                // Do nothing
            } else {
                strSQL = "";
            }

            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                assignedCredit = rs.getDouble("assigned_credit");
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_record", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getAssignedCreditSubAgent", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return assignedCredit;
    }

    public boolean validateUpdateAgent(SessionData sessionData, ServletContext context) {
        String deploymentType = DebisysConfigListener.getDeploymentType(context);
        String customConfigType = DebisysConfigListener.getCustomConfigType(context);

        String strDistChainType = sessionData.getProperty("dist_chain_type");
        this.validationErrors.clear();
        boolean valid = true;
        String strAccessLevel = sessionData.getProperty("access_level");
        if ((this.parentRepId == null) || (this.parentRepId.length() == 0)) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                    && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep", sessionData.getLanguage()));
                valid = false;
            }

        } else if (!NumberUtil.isNumeric(this.parentRepId)) {
            this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep_id", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.businessName == null) || (this.businessName.length() == 0)) {
            this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name", sessionData.getLanguage()));
            valid = false;
        } else if (this.businessName.length() > 50) {
            this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.address == null) || (this.address.length() == 0)) {
            this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address", sessionData.getLanguage()));
            valid = false;
        } else if (this.address.length() > 50) {
            this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address_length", sessionData.getLanguage()));
            valid = false;
        }

        // validate city and zip for all deployments/custom config
        if ((this.city == null) || (this.city.length() == 0)) {
            this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city", sessionData.getLanguage()));
            valid = false;
        } else if (this.city.length() > 15) {
            this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.zip == null) || (this.zip.length() == 0)) {
            this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip", sessionData.getLanguage()));
            valid = false;
        } else if (this.zip.length() > 10) {
            this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip_length", sessionData.getLanguage()));
            valid = false;
        }

        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
                || (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {
            // validate state for US and INTL only
            if ((this.state == null) || (this.state.length() == 0)) {
                this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state", sessionData.getLanguage()));
                valid = false;
            } else if (this.state.length() > 2) {
                this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state_length", sessionData.getLanguage()));
                valid = false;
            }
        }

        if (this.country != null && this.country.length() > 50) {
            this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country_length", sessionData.getLanguage()));
            valid = false;
        }

        if (!StringUtil.isNotEmptyStr(this.country)) {
            this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactFirst == null) || (this.contactFirst.length() == 0)) {
            this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactFirst.length() > 15) {
            this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactLast == null) || (this.contactLast.length() == 0)) {
            this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactLast.length() > 15) {
            this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactMiddle != null) && (this.contactMiddle.length() > 1)) {
            this.addFieldError("contactMiddle", Languages.getString("jsp.admin.customers.reps_add.error_contact_middle", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactPhone == null) || (this.contactPhone.length() == 0)) {
            this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactPhone.length() > 15) {
            this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactFax != null) && (this.contactFax.length() > 15)) {
            this.addFieldError("contactFax", Languages.getString("jsp.admin.customers.reps_add.error_fax_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactEmail != null) && (this.contactEmail.length() > 200)) {
            this.addFieldError("contactEmail", Languages.getString("jsp.admin.customers.reps_add.error_email_length", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactEmail != null) {
            // commented out because we have multiple emails in a field, i.e.
            // aaa@aaa.com;bbb@bbb.com
            /*
			 * ValidateEmail validateEmail = new ValidateEmail(); validateEmail.setStrEmailAddress(contactEmail); validateEmail.checkSyntax(); if
			 * (!validateEmail.isEmailValid()) { addFieldError("contactEmail", "Please enter a valid email address."); valid = false; }
             */

        }

        if ((this.routingNumber != null) && (this.routingNumber.length() > 18)) {
            this.addFieldError("routingNumber", Languages.getString("jsp.admin.customers.reps_add.error_routing_number", sessionData.getLanguage()));
            valid = false;
        } else if (this.routingNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            this.routingNumber = "NA";
        }

        if ((this.accountNumber != null) && (this.accountNumber.length() > 20)) {
            this.addFieldError("accountNumber", Languages.getString("jsp.admin.customers.reps_add.error_account_number", sessionData.getLanguage()));
            valid = false;
        } else if (this.accountNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            this.accountNumber = "NA";
        }

        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {

            if ((this.taxId == null) || this.taxId.equals("")) {
                this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id_mandatory",
                        sessionData.getLanguage()));
                valid = false;
            } else {
                Pattern p = Pattern.compile("^(?!(\\d)\\1+$|(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){8}\\d$)\\d{9}$");
                Matcher match = p.matcher(this.taxId);
                if (!match.matches()) {
                    this.addFieldError("taxId", Languages.getString(
                            "jsp.admin.customers.reps_add.error_tax_length", sessionData.getLanguage()));
                    valid = false;
                }
            }

        } else if ((this.taxId != null) && (this.taxId.length() > 15)) {
            this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id", sessionData.getLanguage()));
            valid = false;
        }

        // need to add check to make sure new rep has enough credit and is of
        // the same credit type
        if (deploymentType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && !customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
            if ((this.legalBusinessname == null) || (this.legalBusinessname.length() == 0)) {
                this.addFieldError("legalBusinessname", Languages.getString("jsp.admin.customers.reps_edit.legal_businessname", sessionData.getLanguage()));
                valid = false;
            }
        }

        return valid;

    }

    public void updateAgent(SessionData sessionData, ServletContext context) throws EntityException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            HashMap map = CountryInfo.getAllCountriesID();

            if (this.checkUpdateAgentPermission(sessionData)) {

                // create a new rep object to compare changes after update
                Rep tmpRep = new Rep();
                tmpRep.setRepId(this.repId);
                tmpRep.getAgent(sessionData, context);
                String log = "";

                if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                    //TODO: INSERTAUDIT GABRIEL    DebisysConstants.AUDIT_PARENT_CHANGE  SUBGETN old:tmpRep.getParentRepId()  new: this.getParentRepId()
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_AGENT, this.repId, DebisysConstants.AUDIT_PARENT_CHANGE,
                            tmpRep.getParentRepId(), this.getParentRepId());
                }

                // EMUNOZ - AUDIT DBSY-1051
                LogChanges logChanges = new LogChanges(sessionData);
                logChanges.setContext(context);

                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new EntityException();
                }

                // pstmt = dbConn.prepareStatement(sql_bundle.getString("updateRep"));
                String sqlStr = Rep.sql_bundle.getString("updateRep");
                String additionalParams = "";

                boolean paymentType_ch = false, processType_ch = false, routingNumber_ch = false, accountNumber_ch = false, taxId_ch = false, entityAccountType_ch = false;
                String oldValue = "";
                String reason = "";
                String reasonIni = "Last value: ";
                String reasonEnd = "Actual value: ";
                String changedBy = "Modified by: ";
                String username = sessionData.getProperty("username");

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE_AGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.paymentType) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE_AGENT), String.valueOf(this.paymentType),
                        this.repId, reason)) {
                    additionalParams += ", payment_type = ?";
                    paymentType_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR_AGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.processType) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR_AGENT), String.valueOf(this.processType),
                        this.repId, reason)) {
                    additionalParams += ",payment_processor = ?";
                    processType_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA_AGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.routingNumber) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA_AGENT), String.valueOf(this.routingNumber), this.repId,
                        reason)) {
                    additionalParams += ", aba = ?";
                    routingNumber_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER_AGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.accountNumber) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER_AGENT), String.valueOf(this.accountNumber),
                        this.repId, reason)) {
                    additionalParams += ", account_no = ?";
                    accountNumber_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID_AGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.taxId) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID_AGENT), String.valueOf(this.taxId), this.repId,
                        reason)) {
                    additionalParams += ", ssn = ?";
                    taxId_ch = true;
                }

                // DBSY-1072 eAccounts Interface
                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_AGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.entityAccountType) + " " + changedBy + username + "";

                if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {
                    if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_AGENT), String
                            .valueOf(this.entityAccountType), this.repId, reason)) {
                        additionalParams += ", EntityAccountType = ?";
                        entityAccountType_ch = true;

                        if (this.internalChildrenExist == 1) {
                            Rep.cat.debug("Doing global update for Agent");
                            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("doGlobalUpdate"));
                            pstmt.setLong(1, Long.parseLong(this.repId));
                            pstmt.setInt(2, 4);
                            pstmt.setString(3, sessionData.getProperty("username"));
                            pstmt.setString(4, "support");
                            pstmt.setString(5, "");
                            pstmt.setString(6, "");
                            Rep.cat.debug("param 1: " + this.repId);
                            Rep.cat.debug("param 2: " + "4");
                            Rep.cat.debug("param 3: " + sessionData.getProperty("username"));
                            Rep.cat.debug("param 4: " + "support");
                            pstmt.execute();
                        }
                    }
                }
                // END DBSY-1072

                Rep.cat.debug("query:" + StringUtil.toString(sqlStr));
                sqlStr = sqlStr.replace("~~addParams~~", additionalParams);
                Rep.cat.debug("query:" + StringUtil.toString(sqlStr));

                pstmt = dbConn.prepareStatement(sqlStr);

                String strAccessLevel = sessionData.getProperty("access_level");
                String strRefId = sessionData.getProperty("ref_id");
                pstmt.setDouble(1, Double.parseDouble(this.parentRepId));
                pstmt.setString(2, this.businessName);

                String initials = this.contactFirst.substring(0, 1);
                if (this.contactMiddle == null || this.contactMiddle.trim().equals("")) {
                    initials = initials + "_";
                } else {
                    initials = initials + this.contactMiddle.substring(0, 1);
                }
                initials = initials + this.contactLast.substring(0, 1);

                // safe handling country data: for missing country data in db
                String safeCountryData = null;
                String safeMailCountryData = null;
                if (this.country != null && map.get(Integer.parseInt(this.country)) != null) {
                    safeCountryData = map.get(Integer.parseInt(this.country)).toString();
                }
                if (this.mailCountry != null && map.get(Integer.parseInt(this.mailCountry)) != null) {
                    safeMailCountryData = map.get(Integer.parseInt(this.mailCountry)).toString();
                }

                pstmt.setString(3, initials);
                pstmt.setString(4, this.contactFirst);
                pstmt.setString(5, this.contactLast);
                pstmt.setString(6, this.contactMiddle);

                // pstmt.setString(7, this.taxId);
                pstmt.setString(7, this.address);

                pstmt.setString(8, this.city);
                pstmt.setString(9, this.state);
                pstmt.setString(10, this.zip);
                pstmt.setString(11, this.contactPhone);
                pstmt.setString(12, this.contactFax);
                pstmt.setString(13, this.contactEmail);

                // pstmt.setString(15, this.routingNumber);
                // pstmt.setString(16, this.accountNumber);
                // pstmt.setString(17, this.country);
                // pstmt.setString(18, this.legalBusinessname);
                pstmt.setString(14, this.country);
                pstmt.setString(15, this.legalBusinessname);

                // pstmt.setInt(19, this.paymentType);
                // pstmt.setInt(20, this.processType);
                // pstmt.setInt(21, this.timeZoneId);
                pstmt.setInt(16, this.timeZoneId);

                if (safeCountryData != null) {
                    pstmt.setString(17, safeCountryData);
                } else {
                    pstmt.setNull(17, java.sql.Types.VARCHAR);
                }

                // DBSY-931 System 2000
                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                        && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                        && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    pstmt.setString(18, this.salesmanid);
                    pstmt.setString(19, this.terms);
                } else {
                    pstmt.setString(18, tmpRep.getsalesmanid());// if not international then keep the same value...
                    pstmt.setString(19, tmpRep.getterms());// if not international then keep the same value...
                }

                /**
                 * ***************************************************************************************
                 */
                /**
                 * ***************************************************************************************
                 */
                /*GEOLOCATION FEATURE*/
                pstmt.setNull(20, java.sql.Types.DOUBLE);
                pstmt.setNull(21, java.sql.Types.DOUBLE);
                pstmt.setInt(22, 0);//Feature not validated
                /*GEOLOCATION FEATURE*/
                /**
                 * ***************************************************************************************
                 */
                /**
                 * ***************************************************************************************
                 */

                //DTU-369 Payment Notifications
                pstmt.setBoolean(23, this.isPaymentNotifications());
                if (this.isPaymentNotificationSMS()) {
                    this.setPaymentNotificationType(0);
                } else if (this.isPaymentNotificationMail()) {
                    this.setPaymentNotificationType(1);
                }
                pstmt.setInt(24, this.getPaymentNotificationType());
                pstmt.setString(25, this.getSmsNotificationPhone());
                pstmt.setString(26, this.getMailNotificationAddress());

                //DTU-480: Low Balance Alert Message SMS and/or email
                pstmt.setBoolean(27, this.isBalanceNotifications());
                if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays()) {
                    this.setBalanceNotificationType(2);
                } else if (this.isBalanceNotificationTypeValue()) {
                    this.setBalanceNotificationType(0);
                } else if (this.isBalanceNotificationTypeDays()) {
                    this.setBalanceNotificationType(1);
                }
                pstmt.setInt(28, this.getBalanceNotificationType());
                if (this.isBalanceNotificationSMS()) {
                    this.setBalanceNotificationNotType(0);
                } else if (this.isBalanceNotificationMail()) {
                    this.setBalanceNotificationNotType(1);
                }
                pstmt.setInt(29, this.getBalanceNotificationNotType());
                pstmt.setInt(30, this.getBalanceNotificationDays());
                pstmt.setDouble(31, this.getBalanceNotificationValue());

                int i = 32;

                if (paymentType_ch) {
                    Rep.cat.debug("after paymenttype:" + this.paymentType);
                    pstmt.setInt(i, this.paymentType);
                    i++;
                }
                if (processType_ch) {
                    Rep.cat.debug("after processtype:" + this.processType);
                    pstmt.setInt(i, this.processType);
                    i++;
                }
                if (routingNumber_ch) {
                    Rep.cat.debug("after routingnumber:" + StringUtil.toString(this.routingNumber));
                    pstmt.setString(i, this.routingNumber);
                    i++;
                }
                if (accountNumber_ch) {
                    Rep.cat.debug("after accountnumber:" + StringUtil.toString(this.accountNumber));
                    pstmt.setString(i, this.accountNumber);
                    i++;
                }
                if (taxId_ch) {
                    Rep.cat.debug("after taxId:" + StringUtil.toString(this.taxId));
                    pstmt.setString(i, this.taxId);
                    i++;
                }

                // DBSY-1072 eAccounts Interface
                if (entityAccountType_ch) {
                    Rep.cat.debug("after entityAccountType:" + this.entityAccountType);
                    pstmt.setInt(i, this.entityAccountType);
                    i++;
                }

                Rep.cat.debug("after repId:" + StringUtil.toString(this.repId));
                pstmt.setDouble(i, Double.parseDouble(this.repId));

                // pstmt.setDouble(25, Double.parseDouble(this.repId));
                pstmt.executeUpdate();
                pstmt.close();

                if (entityAccountType_ch) {
                    //TODO: INSERTAUDIT GABRIEL    EntityAccountType Internal=0/External=1
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_AGENT, this.repId, DebisysConstants.AUDIT_INTERNAL_EXTERNAL_CHANGE,
                            (this.entityAccountType == 1 ? "0" : "1"), "" + this.entityAccountType);
                }

                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepMx"));
                    pstmt.setString(1, this.sharedBalance);
                    pstmt.setString(2, this.physCounty);
                    pstmt.setString(3, this.physColony);
                    pstmt.setString(4, this.physDelegation);
                    if (this.mailState != null) {
                        pstmt.setString(5, this.mailAddress);
                        pstmt.setString(6, this.mailCity);
                        pstmt.setString(7, this.mailState);
                        pstmt.setString(8, this.mailZip);
                        if (safeMailCountryData != null) {
                            pstmt.setString(9, safeMailCountryData);
                        } else {
                            pstmt.setNull(9, java.sql.Types.VARCHAR);
                        }
                        pstmt.setString(10, this.mailDelegation);
                        pstmt.setString(11, this.mailCounty);
                        pstmt.setString(12, this.mailColony);
                        pstmt.setInt(18, Integer.parseInt(this.mailCountry));
                    } else {
                        pstmt.setString(5, this.address);
                        pstmt.setString(6, this.city);
                        pstmt.setString(7, this.state);
                        pstmt.setString(8, this.zip);
                        if (safeCountryData != null) {
                            pstmt.setString(9, safeCountryData);
                        } else {
                            pstmt.setNull(9, java.sql.Types.VARCHAR);
                        }
                        pstmt.setString(10, this.physDelegation);
                        pstmt.setString(11, this.physCounty);
                        pstmt.setString(12, this.physColony);
                        pstmt.setInt(18, Integer.parseInt(this.country));
                    }
                    pstmt.setString(13, this.bankName);
                    pstmt.setString(14, this.businessHours);
                    pstmt.setString(15, this.businessTypeId);
                    pstmt.setInt(16, this.accountTypeId);
                    pstmt.setInt(17, this.merchantSegmentId);
                    pstmt.setLong(19, this.invoiceType);
                    pstmt.setString(20, this.repId);
                    
                    pstmt.executeUpdate();
                    pstmt.close();
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("deleteContactsMx"));
                    pstmt.setString(1, this.repId);
                    pstmt.executeUpdate();
                    pstmt.close();

                    Iterator itContacts = this.vecContacts.iterator();
                    while (itContacts.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) itContacts.next();
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertContactMx"));
                        pstmt.setDouble(1, Double.parseDouble(this.repId));
                        pstmt.setString(2, vecTemp.get(0).toString());
                        pstmt.setString(3, vecTemp.get(1).toString());
                        pstmt.setString(4, vecTemp.get(2).toString());
                        pstmt.setString(5, vecTemp.get(3).toString());
                        pstmt.setString(6, vecTemp.get(4).toString());
                        pstmt.setString(7, vecTemp.get(5).toString());
                        pstmt.setString(8, vecTemp.get(6).toString());
                        pstmt.executeUpdate();
                        pstmt.close();
                    }
                }// End of if when MX update specific fields

                boolean fieldsChanged = false;
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                    if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                        log = log + "iso_id|" + tmpRep.getParentRepId() + "|" + this.getParentRepId() + ",";
                        fieldsChanged = true;
                    }
                }

                if (!this.getBusinessName().equals(tmpRep.getBusinessName())) {
                    log = log + "businessname|" + tmpRep.getBusinessName() + "|" + this.getBusinessName() + ",";
                    fieldsChanged = true;
                }

                if (!this.getTaxId().equals(tmpRep.getTaxId())) {
                    log = log + "tax_id|" + tmpRep.getTaxId() + "|" + this.getTaxId() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactEmail().equals(tmpRep.getContactEmail())) {
                    log = log + "email|" + tmpRep.getContactEmail() + "|" + this.getContactEmail() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactFirst().equals(tmpRep.getContactFirst())) {
                    log = log + "contact_first|" + tmpRep.getContactFirst() + "|" + this.getContactFirst() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactLast().equals(tmpRep.getContactLast())) {
                    log = log + "contact_last|" + tmpRep.getContactLast() + "|" + this.getContactLast() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactMiddle().equals(tmpRep.getContactMiddle())) {
                    log = log + "contact_middle|" + tmpRep.getContactMiddle() + "|" + this.getContactMiddle() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactPhone().equals(tmpRep.getContactPhone())) {
                    log = log + "contact_phone|" + tmpRep.getContactPhone() + "|" + this.getContactPhone() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactFax().equals(tmpRep.getContactFax())) {
                    log = log + "contact_fax|" + tmpRep.getContactFax() + "|" + this.getContactFax() + ",";
                    fieldsChanged = true;
                }

                if (!this.getAddress().equals(tmpRep.getAddress())) {
                    log = log + "address|" + tmpRep.getAddress() + "|" + this.getAddress() + ",";
                    fieldsChanged = true;
                }

                if (!this.getCity().equals(tmpRep.getCity())) {
                    log = log + "city|" + tmpRep.getCity() + "|" + this.getCity() + ",";
                    fieldsChanged = true;
                }
                if (!this.getState().equals(tmpRep.getState())) {
                    log = log + "state|" + tmpRep.getState() + "|" + this.getState() + ",";
                    fieldsChanged = true;
                }
                if (!this.getZip().equals(tmpRep.getZip())) {
                    log = log + "zip|" + tmpRep.getZip() + "|" + this.getZip() + ",";
                    fieldsChanged = true;
                }
                if (!this.getCountry().equals(tmpRep.getCountry())) {
                    log = log + "country|" + tmpRep.getCountry() + "|" + this.getCountry() + ",";
                    fieldsChanged = true;
                }
                if (!this.getRoutingNumber().equals(tmpRep.getRoutingNumber())) {
                    log = log + "aba|" + tmpRep.getRoutingNumber() + "|" + this.getRoutingNumber() + ",";
                    fieldsChanged = true;
                }
                if (!this.getAccountNumber().equals(tmpRep.getAccountNumber())) {
                    log = log + "account_no|" + tmpRep.getAccountNumber() + "|" + this.getAccountNumber() + ",";
                    fieldsChanged = true;
                }

                // DBSY-1072 eAccounts Interface
                if (this.getEntityAccountType() != tmpRep.getEntityAccountType()) {
                    log = log + "entityAccountType|" + tmpRep.getEntityAccountType() + "|" + this.getEntityAccountType() + ",";
                    fieldsChanged = true;
                }

                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    if (!this.getSharedBalance().equals(tmpRep.getSharedBalance())) {
                        log = log + "SharedBalance|" + tmpRep.getSharedBalance() + "|" + this.getSharedBalance() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysCounty().equals(tmpRep.getPhysCounty())) {
                        log = log + "phys_county|" + tmpRep.getPhysCounty() + "|" + this.getPhysCounty() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysColony().equals(tmpRep.getPhysColony())) {
                        log = log + "phys_colony|" + tmpRep.getPhysColony() + "|" + this.getPhysColony() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysDelegation().equals(tmpRep.getPhysDelegation())) {
                        log = log + "phys_delegation|" + tmpRep.getPhysDelegation() + "|" + this.getPhysDelegation() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailAddress().equals(tmpRep.getMailAddress())) {
                        log = log + "mailAddress|" + tmpRep.getMailAddress() + "|" + this.getMailAddress() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCity().equals(tmpRep.getMailCity())) {
                        log = log + "mailCity|" + tmpRep.getMailCity() + "|" + this.getMailCity() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailState().equals(tmpRep.getMailState())) {
                        log = log + "mailState|" + tmpRep.getMailState() + "|" + this.getMailState() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailZip().equals(tmpRep.getMailZip())) {
                        log = log + "mailZip|" + tmpRep.getMailZip() + "|" + this.getMailZip() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCountry().equals(tmpRep.getMailCountry())) {
                        log = log + "mailCountry|" + tmpRep.getMailCountry() + "|" + this.getMailCountry() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailDelegation().equals(tmpRep.getMailDelegation())) {
                        log = log + "mailDelegation|" + tmpRep.getMailDelegation() + "|" + this.getMailDelegation() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCounty().equals(tmpRep.getMailCounty())) {
                        log = log + "mailCounty|" + tmpRep.getMailCounty() + "|" + this.getMailCounty() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailColony().equals(tmpRep.getMailColony())) {
                        log = log + "mailColony|" + tmpRep.getMailColony() + "|" + this.getMailColony() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBankName().equals(tmpRep.getBankName())) {
                        log = log + "bankName|" + tmpRep.getBankName() + "|" + this.getBankName() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBusinessHours().equals(tmpRep.getBusinessHours())) {
                        log = log + "businessHours|" + tmpRep.getBusinessHours() + "|" + this.getBusinessHours() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBusinessTypeId().equals(tmpRep.getBusinessTypeId())) {
                        log = log + "businessType|" + tmpRep.getBusinessTypeId() + "|" + this.getBusinessTypeId() + ",";
                        fieldsChanged = true;
                    }
                    if (this.getAccountTypeId() != tmpRep.getAccountTypeId()) {
                        log = log + "accountType|" + tmpRep.getAccountTypeId() + "|" + this.getAccountTypeId() + ",";
                        fieldsChanged = true;
                    }
                    if (this.getMerchantSegmentId() != tmpRep.getMerchantSegmentId()) {
                        log = log + "merchantSegment|" + tmpRep.getMerchantSegmentId() + "|" + this.getMerchantSegmentId() + ",";
                        fieldsChanged = true;
                    }
                }// End of if when MX, logs specific changes
                if (fieldsChanged) {
                    Log.write(sessionData, "Agent updated.[" + log + "]", DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                }
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during updateAgent", e);
            throw new EntityException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
    }

    public boolean validateUpdateSubAgent(SessionData sessionData, ServletContext context) {
        String deploymentType = DebisysConfigListener.getDeploymentType(context);
        String customConfigType = DebisysConfigListener.getCustomConfigType(context);

        String strDistChainType = sessionData.getProperty("dist_chain_type");
        this.validationErrors.clear();
        boolean valid = true;
        String strAccessLevel = sessionData.getProperty("access_level");
        if ((this.parentRepId == null) || (this.parentRepId.length() == 0)) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                    && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep", sessionData.getLanguage()));
                valid = false;
            }

        } else if (!NumberUtil.isNumeric(this.parentRepId)) {
            this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep_id", sessionData.getLanguage()));
            valid = false;
        } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)
                && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT))) {
            // make sure this is a valid subagent for this agent or iso
            if (!this.checkAgentPermission(sessionData)) {
                this.addFieldError("parentRepId", Languages.getString("jsp.admin.customers.reps_add.error_rep_id2", sessionData.getLanguage()));
                valid = false;
            }
        }

        if ((this.businessName == null) || (this.businessName.length() == 0)) {
            this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name", sessionData.getLanguage()));
            valid = false;
        } else if (this.businessName.length() > 50) {
            this.addFieldError("businessName", Languages.getString("jsp.admin.customers.reps_add.error_business_name_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.address == null) || (this.address.length() == 0)) {
            this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address", sessionData.getLanguage()));
            valid = false;
        } else if (this.address.length() > 50) {
            this.addFieldError("address", Languages.getString("jsp.admin.customers.reps_add.error_street_address_length", sessionData.getLanguage()));
            valid = false;
        }

        // validate city and zip for all deployments/custom config
        if ((this.city == null) || (this.city.length() == 0)) {
            this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city", sessionData.getLanguage()));
            valid = false;
        } else if (this.city.length() > 15) {
            this.addFieldError("city", Languages.getString("jsp.admin.customers.reps_add.error_city_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.zip == null) || (this.zip.length() == 0)) {
            this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip", sessionData.getLanguage()));
            valid = false;
        } else if (this.zip.length() > 10) {
            this.addFieldError("zip", Languages.getString("jsp.admin.customers.reps_add.error_zip_length", sessionData.getLanguage()));
            valid = false;
        }

        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
                || (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {
            // validate state for US and INTL only
            if ((this.state == null) || (this.state.length() == 0)) {
                this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state", sessionData.getLanguage()));
                valid = false;
            } else if (this.state.length() > 2) {
                this.addFieldError("state", Languages.getString("jsp.admin.customers.reps_add.error_state_length", sessionData.getLanguage()));
                valid = false;
            }
        }

        if (this.country != null && this.country.length() > 50) {
            this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country_length", sessionData.getLanguage()));
            valid = false;
        }
        if (!StringUtil.isNotEmptyStr(this.country)) {
            this.addFieldError("country", Languages.getString("jsp.admin.customers.reps_add.error_country", sessionData.getLanguage()));
            valid = false;
        }
        if ((this.contactFirst == null) || (this.contactFirst.length() == 0)) {
            this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactFirst.length() > 15) {
            this.addFieldError("contactFirst", Languages.getString("jsp.admin.customers.reps_add.error_contact_first_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactLast == null) || (this.contactLast.length() == 0)) {
            this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactLast.length() > 15) {
            this.addFieldError("contactLast", Languages.getString("jsp.admin.customers.reps_add.error_contact_last_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactMiddle != null) && (this.contactMiddle.length() > 1)) {
            this.addFieldError("contactMiddle", Languages.getString("jsp.admin.customers.reps_add.error_contact_middle", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactPhone == null) || (this.contactPhone.length() == 0)) {
            this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactPhone.length() > 15) {
            this.addFieldError("contactPhone", Languages.getString("jsp.admin.customers.reps_add.error_phone_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactFax != null) && (this.contactFax.length() > 15)) {
            this.addFieldError("contactFax", Languages.getString("jsp.admin.customers.reps_add.error_fax_length", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.contactEmail != null) && (this.contactEmail.length() > 200)) {
            this.addFieldError("contactEmail", Languages.getString("jsp.admin.customers.reps_add.error_email_length", sessionData.getLanguage()));
            valid = false;
        } else if (this.contactEmail != null) {
            // commented out because we have multiple emails in a field, i.e.
            // aaa@aaa.com;bbb@bbb.com
            /*
			 * ValidateEmail validateEmail = new ValidateEmail(); validateEmail.setStrEmailAddress(contactEmail); validateEmail.checkSyntax(); if
			 * (!validateEmail.isEmailValid()) { addFieldError("contactEmail", "Please enter a valid email address."); valid = false; }
             */

        }

        if ((this.routingNumber != null) && (this.routingNumber.length() > 18)) {
            this.addFieldError("routingNumber", Languages.getString("jsp.admin.customers.reps_add.error_routing_number", sessionData.getLanguage()));
            valid = false;
        } else if (this.routingNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            this.routingNumber = "NA";
        }

        if ((this.accountNumber != null) && (this.accountNumber.length() > 20)) {
            this.addFieldError("accountNumber", Languages.getString("jsp.admin.customers.reps_add.error_account_number", sessionData.getLanguage()));
            valid = false;
        } else if (this.accountNumber == null && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
            this.accountNumber = "NA";
        }

        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {

            if ((this.taxId == null) || this.taxId.equals("")) {
                this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id_mandatory",
                        sessionData.getLanguage()));
                valid = false;
            } else {
                Pattern p = Pattern.compile("^(?!(\\d)\\1+$|(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){8}\\d$)\\d{9}$");
                Matcher match = p.matcher(this.taxId);
                if (!match.matches()) {
                    this.addFieldError("taxId", Languages.getString(
                            "jsp.admin.customers.reps_add.error_tax_length", sessionData.getLanguage()));
                    valid = false;
                }
            }

        } else if ((this.taxId != null) && (this.taxId.length() > 15)) {
            this.addFieldError("taxId", Languages.getString("jsp.admin.customers.reps_add.error_tax_id", sessionData.getLanguage()));
            valid = false;
        }

        // need to add check to make sure new rep has enough credit and is of
        // the same credit type
        if (deploymentType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && !customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
            if ((this.legalBusinessname == null) || (this.legalBusinessname.length() == 0)) {
                this.addFieldError("legalBusinessname", Languages.getString("jsp.admin.customers.reps_edit.legal_businessname", sessionData.getLanguage()));
                valid = false;
            }
        }

        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
            // Checking to see if the agent changed.
            if (!this.validateCheckAgentChange(sessionData, context)) {
                valid = false;
            }
        }

        return valid;

    }

    public double getCurrentAgentCreditLimit(SessionData sessionData) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double creditLimit = 0;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getRepLiabilityLimit");
            // permissions are built into the queries
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL
                        + " and iso_id= " + strRefId + ") ";

            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                // Do Nothing
            } else {
                strSQL = "";
            }

            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                creditLimit = rs.getDouble("liabilitylimit");
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_record", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getCurrentAgentCreditLimit", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return creditLimit;
    }

    public double getCurrentSubAgentCreditLimit(SessionData sessionData) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double creditLimit = 0;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strRefId = sessionData.getProperty("ref_id");
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = Rep.sql_bundle.getString("getRepLiabilityLimit");
            // permissions are built into the queries
            if (strAccessLevel.equals(DebisysConstants.ISO)) {

                strSQL = strSQL + " and r.iso_id in " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id in "
                        + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_ISO_5_LEVEL + " and iso_id=" + strRefId + ")) ";

            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQL = strSQL + " and r.iso_id=" + strRefId;

            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                // Do Nothing
            } else {
                strSQL = "";
            }

            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                creditLimit = rs.getDouble("liabilitylimit");
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_record", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getCurrentSubAgentCreditLimit", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return creditLimit;
    }

    public void updateSubAgent(SessionData sessionData, ServletContext context) throws EntityException {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            HashMap map = CountryInfo.getAllCountriesID();
            if (this.checkUpdateSubAgentPermission(sessionData)) {

                // create a new rep object to compare changes after update
                Rep tmpRep = new Rep();
                tmpRep.setRepId(this.repId);
                tmpRep.getSubAgent(sessionData, context);

                if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                    tmpRep.relocateNewRatePlansForActor();
                    //TODO: INSERTAUDIT GABRIEL    DebisysConstants.AUDIT_PARENT_CHANGE  SUBGETN old:tmpRep.getParentRepId()  new: this.getParentRepId()
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_SUBAGENT, this.repId, DebisysConstants.AUDIT_PARENT_CHANGE,
                            tmpRep.getParentRepId(), this.getParentRepId());
                }

                String log = "";

                // EMUNOZ - AUDIT DBSY-1051
                LogChanges logChanges = new LogChanges(sessionData);
                logChanges.setContext(context);

                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new EntityException();
                }

                // pstmt = dbConn.prepareStatement(sql_bundle.getString("updateRep"));
                String sqlStr = Rep.sql_bundle.getString("updateRep");
                String additionalParams = "";

                boolean paymentType_ch = false, processType_ch = false, routingNumber_ch = false, accountNumber_ch = false, taxId_ch = false;
                boolean entityAccountType_ch = false;

                String oldValue = "";
                String reason = "";
                String reasonIni = "Last value: ";
                String reasonEnd = "Actual value: ";
                String changedBy = "Modified by: ";
                String username = sessionData.getProperty("username");

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE_SUBAGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.paymentType) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTTYPE_SUBAGENT), String.valueOf(this.paymentType),
                        this.repId, reason)) {
                    additionalParams += ", payment_type = ?";
                    paymentType_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR_SUBAGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.processType) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_PAYMENTPROCESSOR_SUBAGENT), String
                        .valueOf(this.processType), this.repId, reason)) {
                    additionalParams += ",payment_processor = ?";
                    processType_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA_SUBAGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.routingNumber) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ABA_SUBAGENT), String.valueOf(this.routingNumber),
                        this.repId, reason)) {
                    additionalParams += ", aba = ?";
                    routingNumber_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER_SUBAGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.accountNumber) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ACCOUNTNUMBER_SUBAGENT),
                        String.valueOf(this.accountNumber), this.repId, reason)) {
                    additionalParams += ", account_no = ?";
                    accountNumber_ch = true;
                }

                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID_SUBAGENT)) + " ";
                reason = reasonIni + oldValue + reasonEnd + String.valueOf(this.taxId) + " " + changedBy + username + "";

                if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_TAXID_SUBAGENT), String.valueOf(this.taxId), this.repId,
                        reason)) {
                    additionalParams += ", ssn = ?";
                    taxId_ch = true;
                }

                // DBSY-1072 eAccounts Interface
                oldValue = sessionData.getProperty("check." + logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_SUBAGENT)) + " ";
                String tempacct;
                if (this.dentityAccountType.equals("1")) {
                    tempacct = "1";
                } else {
                    tempacct = String.valueOf(this.entityAccountType);
                }
                reason = reasonIni + oldValue + reasonEnd + tempacct + " " + changedBy + username + "";

                if (sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE)) {

                    if (logChanges.was_changed(logChanges.getLogChangeIdByName(LogChanges.LOGCHANGETYPE_ENTITYACCOUNTTYPE_SUBAGENT), tempacct, this.repId,
                            reason)) {
                        additionalParams += ", EntityAccountType = ?";
                        entityAccountType_ch = true;

                        if (this.internalChildrenExist == 1) {
                            Rep.cat.debug("Doing global update for SubAgent");
                            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("doGlobalUpdate"));
                            pstmt.setLong(1, Long.parseLong(this.repId));
                            pstmt.setInt(2, 5);
                            pstmt.setString(3, sessionData.getProperty("username"));
                            pstmt.setString(4, "support");
                            pstmt.setString(5, "");
                            pstmt.setString(6, "");
                            Rep.cat.debug("param 1: " + this.repId);
                            Rep.cat.debug("param 2: " + "5");
                            Rep.cat.debug("param 3: " + sessionData.getProperty("username"));
                            Rep.cat.debug("param 4: " + "support");
                            pstmt.execute();
                        }
                    }
                }

                Rep.cat.debug("query:" + StringUtil.toString(sqlStr));
                sqlStr = sqlStr.replace("~~addParams~~", additionalParams);
                Rep.cat.debug("query:" + StringUtil.toString(sqlStr));

                pstmt = dbConn.prepareStatement(sqlStr);

                String strAccessLevel = sessionData.getProperty("access_level");
                String strRefId = sessionData.getProperty("ref_id");

                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                    pstmt.setDouble(1, Double.parseDouble(this.parentRepId));
                } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                    pstmt.setDouble(1, Double.parseDouble(strRefId));
                }
                pstmt.setString(2, this.businessName);

                String initials = this.contactFirst.substring(0, 1);
                if (this.contactMiddle == null || this.contactMiddle.trim().equals("")) {
                    initials = initials + "_";
                } else {
                    initials = initials + this.contactMiddle.substring(0, 1);
                }
                initials = initials + this.contactLast.substring(0, 1);

                // Safe handling country data: for missing country data in db
                String safeCountryData = null;
                String safeMailCountryData = null;
                if (this.country != null && map.get(Integer.parseInt(this.country)) != null) {
                    safeCountryData = map.get(Integer.parseInt(this.country)).toString();
                }

                if (this.mailCountry != null && map.get(Integer.parseInt(this.mailCountry)) != null) {
                    safeCountryData = map.get(Integer.parseInt(this.mailCountry)).toString();
                }

                pstmt.setString(3, initials);
                pstmt.setString(4, this.contactFirst);
                pstmt.setString(5, this.contactLast);
                pstmt.setString(6, this.contactMiddle);

                // pstmt.setString(7, this.taxId);
                pstmt.setString(7, this.address);

                pstmt.setString(8, this.city);
                pstmt.setString(9, this.state);
                pstmt.setString(10, this.zip);
                pstmt.setString(11, this.contactPhone);
                pstmt.setString(12, this.contactFax);
                pstmt.setString(13, this.contactEmail);

                // pstmt.setString(15, this.routingNumber);
                // pstmt.setString(16, this.accountNumber);
                pstmt.setString(14, this.country);
                pstmt.setString(15, this.legalBusinessname);

                // pstmt.setInt(19, this.paymentType);
                // pstmt.setInt(20, this.processType);
                pstmt.setInt(16, this.timeZoneId);

                if (safeCountryData != null) {
                    pstmt.setString(17, safeCountryData);
                } else {
                    pstmt.setNull(17, java.sql.Types.VARCHAR);
                }
                // DBSY-931 System 2000
                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
                        && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                        && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)) {
                    pstmt.setString(18, this.salesmanid);
                    pstmt.setString(19, this.terms);
                } else {
                    pstmt.setString(18, tmpRep.getsalesmanid());// if not international then keep the same value...
                    pstmt.setString(19, tmpRep.getterms());// if not international then keep the same value...
                }
                

                /**
                 * ***************************************************************************************
                 */
                /**
                 * ***************************************************************************************
                 */
                /*GEOLOCATION FEATURE*/
                pstmt.setNull(20, java.sql.Types.DOUBLE);
                pstmt.setNull(21, java.sql.Types.DOUBLE);
                pstmt.setInt(22, 0);//Feature not validated
                /*GEOLOCATION FEATURE*/
                /**
                 * ***************************************************************************************
                 */
                /**
                 * ***************************************************************************************
                 */

                //DTU-369 Payment Notifications
                pstmt.setBoolean(23, this.isPaymentNotifications());
                if (this.isPaymentNotificationSMS()) {
                    this.setPaymentNotificationType(0);
                } else if (this.isPaymentNotificationMail()) {
                    this.setPaymentNotificationType(1);
                }
                pstmt.setInt(24, this.getPaymentNotificationType());
                pstmt.setString(25, this.getSmsNotificationPhone());
                pstmt.setString(26, this.getMailNotificationAddress());

                //DTU-480: Low Balance Alert Message SMS and/or email
                pstmt.setBoolean(27, this.isBalanceNotifications());
                if (this.isBalanceNotificationTypeValue() && this.isBalanceNotificationTypeDays()) {
                    this.setBalanceNotificationType(2);
                } else if (this.isBalanceNotificationTypeValue()) {
                    this.setBalanceNotificationType(0);
                } else if (this.isBalanceNotificationTypeDays()) {
                    this.setBalanceNotificationType(1);
                }
                pstmt.setInt(28, this.getBalanceNotificationType());
                if (this.isBalanceNotificationSMS()) {
                    this.setBalanceNotificationNotType(0);
                } else if (this.isBalanceNotificationMail()) {
                    this.setBalanceNotificationNotType(1);
                }
                pstmt.setInt(29, this.getBalanceNotificationNotType());
                pstmt.setInt(30, this.getBalanceNotificationDays());
                pstmt.setDouble(31, this.getBalanceNotificationValue());

                int i = 32;
                if (paymentType_ch) {
                    Rep.cat.debug("after paymenttype:" + this.paymentType);
                    pstmt.setInt(i, this.paymentType);
                    i++;
                }
                if (processType_ch) {
                    Rep.cat.debug("after processtype:" + this.processType);
                    pstmt.setInt(i, this.processType);
                    i++;
                }
                if (routingNumber_ch) {
                    Rep.cat.debug("after routingnumber:" + StringUtil.toString(this.routingNumber));
                    pstmt.setString(i, this.routingNumber);
                    i++;
                }
                if (accountNumber_ch) {
                    Rep.cat.debug("after accountnumber:" + StringUtil.toString(this.accountNumber));
                    pstmt.setString(i, this.accountNumber);
                    i++;
                }
                if (taxId_ch) {
                    Rep.cat.debug("after taxId:" + StringUtil.toString(this.taxId));
                    pstmt.setString(i, this.taxId);
                    i++;
                }

                // DBSY-1072 eAccounts Interface
                if (entityAccountType_ch) {
                    Rep.cat.debug("after entityAccountType:" + this.entityAccountType);
                    // DBSY-1072 eAccounts Interface
                    Rep.cat.debug("dentityAccountType=" + this.dentityAccountType);
                    if (this.dentityAccountType.equals("1")) {
                        pstmt.setInt(i, 1);// if Combo is disabled then default to external
                        Rep.cat.debug("dentityAccountType=" + this.dentityAccountType);
                    } else {
                        pstmt.setInt(i, this.entityAccountType);
                    }
                    i++;
                }

                Rep.cat.debug("after repId:" + StringUtil.toString(this.repId));

                // pstmt.setDouble(25, Double.parseDouble(this.repId));
                pstmt.setDouble(i, Double.parseDouble(this.repId));

                pstmt.executeUpdate();
                pstmt.close();

                if (entityAccountType_ch) {
                    //TODO: INSERTAUDIT GABRIEL    EntityAccountType Internal=0/External=1
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_SUBAGENT, this.repId, DebisysConstants.AUDIT_INTERNAL_EXTERNAL_CHANGE,
                            (this.entityAccountType == 1 ? "0" : "1"), "" + this.entityAccountType);
                }

                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepMx"));
                    pstmt.setString(1, this.sharedBalance);
                    pstmt.setString(2, this.physCounty);
                    pstmt.setString(3, this.physColony);
                    pstmt.setString(4, this.physDelegation);
                    if (this.mailState != null) {
                        pstmt.setString(5, this.mailAddress);
                        pstmt.setString(6, this.mailCity);
                        pstmt.setString(7, this.mailState);
                        pstmt.setString(8, this.mailZip);
                        if (safeMailCountryData != null) {
                            pstmt.setString(9, safeMailCountryData);
                        } else {
                            pstmt.setNull(9, java.sql.Types.VARCHAR);
                        }
                        pstmt.setString(10, this.mailDelegation);
                        pstmt.setString(11, this.mailCounty);
                        pstmt.setString(12, this.mailColony);
                        pstmt.setInt(18, Integer.parseInt(this.mailCountry));
                    } else {
                        pstmt.setString(5, this.address);
                        pstmt.setString(6, this.city);
                        pstmt.setString(7, this.state);
                        pstmt.setString(8, this.zip);
                        if (safeCountryData != null) {
                            pstmt.setString(9, safeCountryData);
                        } else {
                            pstmt.setNull(9, java.sql.Types.VARCHAR);
                        }
                        pstmt.setString(10, this.physDelegation);
                        pstmt.setString(11, this.physCounty);
                        pstmt.setString(12, this.physColony);
                        pstmt.setInt(18, Integer.parseInt(this.country));
                    }
                    pstmt.setString(13, this.bankName);
                    pstmt.setString(14, this.businessHours);
                    pstmt.setString(15, this.businessTypeId);
                    pstmt.setInt(16, this.accountTypeId);
                    pstmt.setInt(17, this.merchantSegmentId);
                    pstmt.setLong(19, this.invoiceType);
                    pstmt.setString(20, this.repId);
                    
                    pstmt.executeUpdate();
                    pstmt.close();
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("deleteContactsMx"));
                    pstmt.setString(1, this.repId);
                    pstmt.executeUpdate();
                    pstmt.close();

                    Iterator itContacts = this.vecContacts.iterator();
                    while (itContacts.hasNext()) {
                        Vector vecTemp = null;
                        vecTemp = (Vector) itContacts.next();
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertContactMx"));
                        pstmt.setDouble(1, Double.parseDouble(this.repId));
                        pstmt.setString(2, vecTemp.get(0).toString());
                        pstmt.setString(3, vecTemp.get(1).toString());
                        pstmt.setString(4, vecTemp.get(2).toString());
                        pstmt.setString(5, vecTemp.get(3).toString());
                        pstmt.setString(6, vecTemp.get(4).toString());
                        pstmt.setString(7, vecTemp.get(5).toString());
                        pstmt.setString(8, vecTemp.get(6).toString());
                        pstmt.executeUpdate();
                        pstmt.close();
                    }
                }// End of if when MX update specific fields

                boolean fieldsChanged = false;
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                    if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                        log = log + "iso_id|" + tmpRep.getParentRepId() + "|" + this.getParentRepId() + ",";
                        fieldsChanged = true;
                    }
                }

                if (!this.getBusinessName().equals(tmpRep.getBusinessName())) {
                    log = log + "businessname|" + tmpRep.getBusinessName() + "|" + this.getBusinessName() + ",";
                    fieldsChanged = true;
                }

                if (!this.getTaxId().equals(tmpRep.getTaxId())) {
                    log = log + "tax_id|" + tmpRep.getTaxId() + "|" + this.getTaxId() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactEmail().equals(tmpRep.getContactEmail())) {
                    log = log + "email|" + tmpRep.getContactEmail() + "|" + this.getContactEmail() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactFirst().equals(tmpRep.getContactFirst())) {
                    log = log + "contact_first|" + tmpRep.getContactFirst() + "|" + this.getContactFirst() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactLast().equals(tmpRep.getContactLast())) {
                    log = log + "contact_last|" + tmpRep.getContactLast() + "|" + this.getContactLast() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactMiddle().equals(tmpRep.getContactMiddle())) {
                    log = log + "contact_middle|" + tmpRep.getContactMiddle() + "|" + this.getContactMiddle() + ",";
                    fieldsChanged = true;
                }

                if (!this.getContactPhone().equals(tmpRep.getContactPhone())) {
                    log = log + "contact_phone|" + tmpRep.getContactPhone() + "|" + this.getContactPhone() + ",";
                    fieldsChanged = true;
                }
                if (!this.getContactFax().equals(tmpRep.getContactFax())) {
                    log = log + "contact_fax|" + tmpRep.getContactFax() + "|" + this.getContactFax() + ",";
                    fieldsChanged = true;
                }

                if (!this.getAddress().equals(tmpRep.getAddress())) {
                    log = log + "address|" + tmpRep.getAddress() + "|" + this.getAddress() + ",";
                    fieldsChanged = true;
                }

                if (!this.getCity().equals(tmpRep.getCity())) {
                    log = log + "city|" + tmpRep.getCity() + "|" + this.getCity() + ",";
                    fieldsChanged = true;
                }
                if (!this.getState().equals(tmpRep.getState())) {
                    log = log + "state|" + tmpRep.getState() + "|" + this.getState() + ",";
                    fieldsChanged = true;
                }
                if (!this.getZip().equals(tmpRep.getZip())) {
                    log = log + "zip|" + tmpRep.getZip() + "|" + this.getZip() + ",";
                    fieldsChanged = true;
                }
                if (!this.getCountry().equals(tmpRep.getCountry())) {
                    log = log + "country|" + tmpRep.getCountry() + "|" + this.getCountry() + ",";
                    fieldsChanged = true;
                }
                if (!this.getRoutingNumber().equals(tmpRep.getRoutingNumber())) {
                    log = log + "aba|" + tmpRep.getRoutingNumber() + "|" + this.getRoutingNumber() + ",";
                    fieldsChanged = true;
                }
                if (!this.getAccountNumber().equals(tmpRep.getAccountNumber())) {
                    log = log + "account_no|" + tmpRep.getAccountNumber() + "|" + this.getAccountNumber() + ",";
                    fieldsChanged = true;
                }

                // DBSY-1072 eAccounts Interface
                if (this.getEntityAccountType() != tmpRep.getEntityAccountType()) {
                    log = log + "entityAccountType|" + tmpRep.getEntityAccountType() + "|" + this.getEntityAccountType() + ",";
                    fieldsChanged = true;
                }

                String strDistChainType = sessionData.getProperty("dist_chain_type");
                if ((strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                    if (!this.getParentRepId().equals(tmpRep.getParentRepId())) {
                        log = log + "iso_id|" + tmpRep.getParentRepId() + "|" + this.getRepId() + ",";
                        fieldsChanged = true;
                        // Agent changed so lets make sure new agent can handle
                        // it
                        try {
                            Rep oldAgent = new Rep();
                            oldAgent.setRepId(tmpRep.getParentRepId());
                            oldAgent.getAgent(sessionData, context);

                            Rep newAgent = new Rep();
                            newAgent.setRepId(this.parentRepId);
                            newAgent.getAgent(sessionData, context);

                            if ((oldAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
                                    && newAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
                                    && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                                    || (!DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && (tmpRep.getRepCreditType().equals("1") || tmpRep.getRepCreditType().equals("5")))) {
                                double agentCreditLimit = oldAgent.getCurrentLiabilityLimit();
                                double agentRunningLiability = oldAgent.getCurrentRunningLiability();

                                // Inserting old Agent Subagent Credit
                                Rep.cat.debug(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                                pstmt.setDouble(1, Double.parseDouble(oldAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Subagent.changed_agents", sessionData.getLanguage()));
                                pstmt.setDouble(5, 0);
                                pstmt.setDouble(6, agentCreditLimit - agentRunningLiability);
                                pstmt.setDouble(7, agentCreditLimit - agentRunningLiability);
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, oldAgent.getEntityAccountType());
                                Rep childsa = new Rep();
                                childsa.setRepId(this.repId);
                                childsa.getSubAgent(sessionData, context);
                                pstmt.setInt(10, childsa.getEntityAccountType());
                                pstmt.executeUpdate();
                                pstmt.close();

                                agentCreditLimit = newAgent.getCurrentLiabilityLimit();
                                agentRunningLiability = newAgent.getCurrentRunningLiability();

                                // Inserting new Rep Merchant Credit
                                // add an entry to show the rep credits being
                                // used
                                // 1 2 3 4 5 6 7 8
                                // (rep_id, merchant_id, logon_id, description,
                                // amount,
                                // available_credit, prior_available_credit,
                                // datetime)
                                Rep.cat.debug(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                                pstmt.setDouble(1, Double.parseDouble(newAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Subagent.changed_from_agent", sessionData.getLanguage()));
                                pstmt.setDouble(5, 0);
                                pstmt.setDouble(6, agentCreditLimit - agentRunningLiability);
                                pstmt.setDouble(7, agentCreditLimit - agentRunningLiability);
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, newAgent.getEntityAccountType());
                                pstmt.setInt(10, childsa.getEntityAccountType());
                                pstmt.executeUpdate();
                                pstmt.close();
                            } else {
                                double availSubAgentCredit = Double.parseDouble(tmpRep.getCreditLimit()) - Double.parseDouble(tmpRep.getRunningLiability());

                                // set the merchants credit to his available
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateSubAgentLiabilityLimit"));

                                boolean creditPrepaidInt = false;
                                // pstmt.setDouble(1, availSubAgentCredit);
                                if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                        && ((oldAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)
                                        && newAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED))
                                        || (oldAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)
                                        && newAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID))) && tmpRep.getRepCreditType().equals("1")) {
                                    creditPrepaidInt = true;
                                    pstmt.setDouble(1, availSubAgentCredit);//0
                                } else {
                                    pstmt.setDouble(1, 0);
                                }
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.executeUpdate();
                                pstmt.close();

                                // set the old rep credit to his credit limit
                                // minus what was
                                // used by the merchant moving
                                oldAgent.setPaymentAmount(Double.toString(availSubAgentCredit));

                                oldAgent.setPaymentDescription("Subagent moved to another Agent.");
                                /* BEGIN MiniRelease 9.0 - Added by YH */
                                oldAgent.makeAgentPayment(sessionData, context, creditPrepaidInt);

                                /* END MiniRelease 9.0 - Added by YH */
                                // set the old rep credit to his credit limit
                                // minus what was
                                // used by the merchant moving
                                newAgent.setPaymentAmount(Double.toString(Double.parseDouble(this.creditLimit) * -1));

                                newAgent.setPaymentDescription("Subagent moved from another Agent.");
                                /* BEGIN MiniRelease 9.0 - Added by YH */
                                newAgent.makeAgentPayment(sessionData, context, creditPrepaidInt);

                                /* END MiniRelease 9.0 - Added by YH */
                                // log rep merchant credit to old rep since
                                // merchant was
                                // removed
                                double agentCreditLimit = oldAgent.getCurrentAgentCreditLimit(sessionData);
                                double agentAssignedCredit = oldAgent.getAssignedCreditAgent(sessionData);
                                double newAgentAvailableCredit = agentCreditLimit - agentAssignedCredit;

                                // add an entry to show the rep credits being
                                // used
                                // 1 2 3 4 5 6 7 8
                                // (rep_id, merchant_id, logon_id, description,
                                // amount,
                                // available_credit, prior_available_credit,
                                // datetime)
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                                pstmt.setDouble(1, Double.parseDouble(oldAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Subagent.changed_agents", sessionData.getLanguage()));
                                pstmt.setDouble(5, availSubAgentCredit);
                                pstmt.setDouble(6, newAgentAvailableCredit);
                                pstmt.setDouble(7, Double.parseDouble(oldAgent.getAvailableCredit()));
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, oldAgent.getEntityAccountType());
                                Rep childsa = new Rep();
                                childsa.setRepId(this.repId);
                                childsa.getSubAgent(sessionData, context);
                                pstmt.setInt(10, childsa.getEntityAccountType());
                                pstmt.executeUpdate();
                                pstmt.close();

                                agentCreditLimit = newAgent.getCurrentRepCreditLimit(sessionData);
                                agentAssignedCredit = newAgent.getAssignedCredit(sessionData);
                                newAgentAvailableCredit = agentCreditLimit - agentAssignedCredit;
                                // add an entry to show the rep credits being
                                // used
                                // 1 2 3 4 5 6 7 8
                                // (rep_id, merchant_id, logon_id, description,
                                // amount,
                                // available_credit, prior_available_credit,
                                // datetime)
                                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                                pstmt.setDouble(1, Double.parseDouble(newAgent.getRepId()));
                                pstmt.setDouble(2, Double.parseDouble(this.repId));
                                pstmt.setString(3, sessionData.getProperty("username"));
                                pstmt.setString(4, Languages.getString("com.debisys.customers.Subagent.changed_from_agent", sessionData.getLanguage()));
                                pstmt.setDouble(5, Double.parseDouble(this.creditLimit) * -1);
                                pstmt.setDouble(6, newAgentAvailableCredit);
                                pstmt.setDouble(7, Double.parseDouble(newAgent.getAvailableCredit()));
                                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                                pstmt.setInt(9, newAgent.getEntityAccountType());
                                pstmt.setInt(10, childsa.getEntityAccountType());
                                pstmt.executeUpdate();
                                pstmt.close();
                            }

                        } catch (Exception e) {
                            Rep.cat.error("Error while updating the Agent change", e);
                        }

                        /* } */
                    }
                }

                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {// When
                    // MX,
                    // logs
                    // specific
                    // changes
                    if (!this.getSharedBalance().equals(tmpRep.getSharedBalance())) {
                        log = log + "SharedBalance|" + tmpRep.getSharedBalance() + "|" + this.getSharedBalance() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysCounty().equals(tmpRep.getPhysCounty())) {
                        log = log + "phys_county|" + tmpRep.getPhysCounty() + "|" + this.getPhysCounty() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysColony().equals(tmpRep.getPhysColony())) {
                        log = log + "phys_colony|" + tmpRep.getPhysColony() + "|" + this.getPhysColony() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getPhysDelegation().equals(tmpRep.getPhysDelegation())) {
                        log = log + "phys_delegation|" + tmpRep.getPhysDelegation() + "|" + this.getPhysDelegation() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailAddress().equals(tmpRep.getMailAddress())) {
                        log = log + "mailAddress|" + tmpRep.getMailAddress() + "|" + this.getMailAddress() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCity().equals(tmpRep.getMailCity())) {
                        log = log + "mailCity|" + tmpRep.getMailCity() + "|" + this.getMailCity() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailState().equals(tmpRep.getMailState())) {
                        log = log + "mailState|" + tmpRep.getMailState() + "|" + this.getMailState() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailZip().equals(tmpRep.getMailZip())) {
                        log = log + "mailZip|" + tmpRep.getMailZip() + "|" + this.getMailZip() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCountry().equals(tmpRep.getMailCountry())) {
                        log = log + "mailCountry|" + tmpRep.getMailCountry() + "|" + this.getMailCountry() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailDelegation().equals(tmpRep.getMailDelegation())) {
                        log = log + "mailDelegation|" + tmpRep.getMailDelegation() + "|" + this.getMailDelegation() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailCounty().equals(tmpRep.getMailCounty())) {
                        log = log + "mailCounty|" + tmpRep.getMailCounty() + "|" + this.getMailCounty() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getMailColony().equals(tmpRep.getMailColony())) {
                        log = log + "mailColony|" + tmpRep.getMailColony() + "|" + this.getMailColony() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBankName().equals(tmpRep.getBankName())) {
                        log = log + "bankName|" + tmpRep.getBankName() + "|" + this.getBankName() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBusinessHours().equals(tmpRep.getBusinessHours())) {
                        log = log + "businessHours|" + tmpRep.getBusinessHours() + "|" + this.getBusinessHours() + ",";
                        fieldsChanged = true;
                    }
                    if (!this.getBusinessTypeId().equals(tmpRep.getBusinessTypeId())) {
                        log = log + "businessType|" + tmpRep.getBusinessTypeId() + "|" + this.getBusinessTypeId() + ",";
                        fieldsChanged = true;
                    }
                    if (this.getAccountTypeId() != tmpRep.getAccountTypeId()) {
                        log = log + "accountType|" + tmpRep.getAccountTypeId() + "|" + this.getAccountTypeId() + ",";
                        fieldsChanged = true;
                    }
                    if (this.getMerchantSegmentId() != tmpRep.getMerchantSegmentId()) {
                        log = log + "merchantSegment|" + tmpRep.getMerchantSegmentId() + "|" + this.getMerchantSegmentId() + ",";
                        fieldsChanged = true;
                    }
                }// End of if when MX, logs specific changes
                if (fieldsChanged) {
                    Log.write(sessionData, "Rep updated.[" + log + "]", DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                }
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during updateSubAgent", e);
            throw new EntityException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * @param intPageNumber
     * @param intRecordsPerPage
     * @param sessionData
     * @param context
     * @param scheduleReportType
     * @param headers
     * @param titles
     * @return
     * @throws CustomerException
     */
    public Vector getAgentCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException {
        Connection dbConn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        Vector vecAgentCredits = new Vector();
        StringBuilder strBuffSQL = new StringBuilder();
        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
            String reportId = sessionData.getProperty("reportId");
            int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            }

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            this.endDate = format.format(new Date());
            this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
            Date date1 = format.parse(this.startDate);
            Date date2 = format.parse(this.endDate);
            format.applyPattern("yyyy-MM-dd");
            this.startDate = format.format(date1);
            this.endDate = format.format(date2);

            Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate + " 23:59:59.999", false);

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQLWhere = "";
            String strSQLOrderBy = " order by agent_credits.id DESC ";

            if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                strBuffSQL.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS);
            }

            // get a count of total matches
            String strSQL = "SELECT dbo.GetLocalTimeByAccessLevel(" + sessionData.getProperty("access_level") + ", " + sessionData.getProperty("ref_id") + ", agent_credits.datetime, 1) as datetime, "
                    + " agent_credits.logon_id, agent_credits.description, agent_credits.amount, agent_credits.commission, agent_credits.available_credit, "
                    + " (agent_credits.amount - (agent_credits.amount * (isnull(agent_credits.commission,0) * .01))) as net_payment " + strBuffSQL.toString()
                    + " FROM agent_credits WITH(NOLOCK) WHERE rep_id=?";

            if (scheduleReportType != DebisysConstants.SCHEDULE_REPORT) {
                if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate)) {
                    strSQLWhere = " and datetime >= '" + vTimeZoneFilterDates.get(0) + "' and datetime <= '" + vTimeZoneFilterDates.get(1) + "'";
                }
            } else {
                String fixedQuery = strSQL.replaceFirst("\\?", this.repId);
                String relativeQuery = strSQL.replaceFirst("\\?", this.repId);

                relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " " + strSQLOrderBy;

                fixedQuery += " AND datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND datetime <= '" + vTimeZoneFilterDates.get(1) + "' " + strSQLOrderBy;

                ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_AGENTS_CREDIT_HISTORY, 1);
                scheduleReport.setNameDateTimeColumn("datetime");

                scheduleReport.setRelativeQuery(relativeQuery);

                scheduleReport.setFixedQuery(fixedQuery);
                TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
                sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
                return null;
            }

            strSQL = strSQL + strSQLWhere + strSQLOrderBy;
            Rep.cat.debug("SQL: " + strSQL);
            if (scheduleReportType != DebisysConstants.DOWNLOAD_REPORT) {
                pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                int intRecordCount = DbUtil.getRecordCount(rs);

                // first row is always the count
                vecAgentCredits.add(Integer.valueOf(intRecordCount));
                if (intRecordCount > 0) {
                    rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
                    for (int i = 0; i < intRecordsPerPage; i++) {

                        Vector vecTemp = new Vector();                        
                        String logon_id = rs.getString("logon_id");
                        vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
                        if (logon_id == null) {
                            logon_id = "";
                        }
                        vecTemp.add(StringUtil.toString(logon_id));
                        String description = rs.getString("description");
                        if (description == null) {
                            description = "";
                        }
                        vecTemp.add(StringUtil.toString(description));
                        if (logon_id.equals("Events Job")) {
                            vecTemp.add("");
                        } else {
                            String amount = rs.getString("amount");
                            if (amount == null) {
                                amount = "0";
                            }
                            vecTemp.add(NumberUtil.formatCurrency(amount));
                        }
                        String availableCredit = rs.getString("available_credit");
                        if (availableCredit == null) {
                            availableCredit = "0";
                        }
                        vecTemp.add(NumberUtil.formatCurrency(availableCredit));
                        
                        String commission = rs.getString("commission");
                        if(commission == null) {
                            commission = "0";
                        }
                        vecTemp.add(NumberUtil.formatAmount(commission));
                        if (logon_id.equals("Events Job")) {
                            vecTemp.add("");
                        } else {
                            String netPayment = rs.getString("net_payment");
                            if (netPayment == null) {
                                netPayment = "0";
                            }
                            vecTemp.add(NumberUtil.formatCurrency(netPayment));
                        }
                        vecAgentCredits.add(vecTemp);
                        if (!rs.next()) {
                            break;
                        }
                    }
                }

            } else {
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                DownloadsSummary trxSummary = new DownloadsSummary();
                TransactionReport tr = new TransactionReport();
                String strFileName = tr.generateFileName(context);
                this.setStrUrlLocation(trxSummary.download(rs, sessionData, context, headers, strFileName, titles, null, 2));
                rs.close();
                pstmt.close();
            }

        } catch (Exception e) {
            Rep.cat.error("Error during getAgentCredits", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vecAgentCredits;
    }

    /**
     * @param intPageNumber
     * @param intRecordsPerPage
     * @param sessionData
     * @param context
     * @param scheduleReportType
     * @param headers
     * @param titles
     * @return
     * @throws CustomerException
     */
    public Vector getAgentSubagentCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector vecAgentSubagentCredits = new Vector();
        String strAccessLevel = sessionData.getProperty("access_level");
        StringBuilder strBuffSQL = new StringBuilder();
        // prevent reps from seeing others reps
        // nned to make this more secure but out of time
        if (strAccessLevel.equals(DebisysConstants.REP)) {
            this.repId = sessionData.getProperty("ref_id");
        }

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
            String reportId = sessionData.getProperty("reportId");
            int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            }

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            this.endDate = format.format(new Date());
            this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
            Date date1 = format.parse(this.startDate);
            Date date2 = format.parse(this.endDate);
            format.applyPattern("yyyy-MM-dd");
            this.startDate = format.format(date1);
            this.endDate = format.format(date2);

            Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate + " 23:59:59.999", false);

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQLWhere = "";

            if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                strBuffSQL.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS);
            }

            // get a count of total matches
            String strSQL = "SELECT dbo.GetLocalTimeByAccessLevel(" + sessionData.getProperty("access_level") + ", " + sessionData.getProperty("ref_id")
                    + ", ac.datetime, 1) as datetime , ac.logon_id, ac.subagent_id, ac.description, "
                    + " ac.amount, ac.available_credit, ac.prior_available_credit " + strBuffSQL.toString()
                    + " FROM agent_subagent_credits ac WITH(NOLOCK) WHERE ac.agent_id=?";

            if (scheduleReportType != DebisysConstants.SCHEDULE_REPORT) {
                if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate)) {
                    strSQLWhere = " and ac.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and ac.datetime <= '" + vTimeZoneFilterDates.get(1) + "' ";
                }
            } else {
                String fixedQuery = strSQL.replaceFirst("\\?", this.repId);
                String relativeQuery = strSQL.replaceFirst("\\?", this.repId);

                relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT;

                fixedQuery += " AND ac.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND ac.datetime <= '" + vTimeZoneFilterDates.get(1) + "' ";

                ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_AGENT_CREDIT_ASSIGNMENT_HIST, 1);
                scheduleReport.setNameDateTimeColumn("ac.datetime");

                scheduleReport.setRelativeQuery(relativeQuery);

                scheduleReport.setFixedQuery(fixedQuery);
                TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
                sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
                return null;

            }

            Rep.cat.debug(strSQL + strSQLWhere + " order by ac.id DESC");
            if (scheduleReportType != DebisysConstants.DOWNLOAD_REPORT) {
                pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by ac.id DESC", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                int intRecordCount = DbUtil.getRecordCount(rs);
                // first row is always the count
                vecAgentSubagentCredits.add(Integer.valueOf(intRecordCount));
                if (intRecordCount > 0) {
                    rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
                    for (int i = 0; i < intRecordsPerPage; i++) {

                        Vector vecTemp = new Vector();
                        vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
                        vecTemp.add(StringUtil.toString(rs.getString("logon_id")));
                        vecTemp.add(StringUtil.toString(rs.getString("subagent_id")));
                        vecTemp.add(StringUtil.toString(rs.getString("description")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("available_credit")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("prior_available_credit")));
                        vecAgentSubagentCredits.add(vecTemp);
                        if (!rs.next()) {
                            break;
                        }
                    }
                }
            } else {
                pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by ac.id DESC");
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                DownloadsSummary trxSummary = new DownloadsSummary();
                TransactionReport tr = new TransactionReport();
                String strFileName = tr.generateFileName(context);
                this.setStrUrlLocation(trxSummary.download(rs, sessionData, context, headers, strFileName, titles, null, 2));
                rs.close();
                pstmt.close();
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getAgentSubagentCredits", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vecAgentSubagentCredits;
    }

    public void updateAgentLiabilityLimit(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            if (this.checkUpdateAgentPermission(sessionData)) {

                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }

                // select r.rep_credit_type, r.LiabilityLimit,
                // r.runningliability from
                // reps r (nolock) where r.rep_id = ?
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                ResultSet rs = pstmt.executeQuery();
                rs.next();
                double currentLiabilityLimit = Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));
                double currentRunningLiability = Double.parseDouble(NumberUtil.formatAmount(rs.getString("runningliability")));
                double dblNewCreditLimit = Double.parseDouble(this.creditLimit);
                pstmt.close();

                // updateRepLiabilityLimit=update reps set liabilityLimit = ?
                // where
                // rep_id = ?
                Rep agentTemp = new Rep();
                agentTemp.setRepId(this.repId);
                agentTemp.getAgent(sessionData, context);
                insertBalanceHistory(DebisysConstants.REP_TYPE_AGENT, agentTemp.repId, agentTemp.parentRepId,
                        agentTemp.repCreditType, agentTemp.sharedBalance, agentTemp.entityAccountType, agentTemp.runningLiability, String.valueOf(currentLiabilityLimit), sessionData.getProperty("username"), false);

                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepLiabilityLimit"));
                pstmt.setDouble(1, dblNewCreditLimit);
                pstmt.setDouble(2, Double.parseDouble(this.repId));
                pstmt.executeUpdate();
                pstmt.close();

                // 1 2 3 4 5 6 7 8 9 10 11
                // (rep_id, logon_id, description, amount, credit_limit,
                // prior_credit_limit, available_credit, prior_available_credit,
                // commission, datetime, entityaccounttype)
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentCredit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                pstmt.setString(2, sessionData.getProperty("username"));
                pstmt.setString(3, Languages.getString("com.debisys.customers.agent_update_credit", sessionData.getLanguage()));
                pstmt.setDouble(4, dblNewCreditLimit - currentLiabilityLimit);
                pstmt.setDouble(5, dblNewCreditLimit);
                pstmt.setDouble(6, currentLiabilityLimit);
                pstmt.setDouble(7, dblNewCreditLimit - currentRunningLiability);
                pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                pstmt.setDouble(9, 0);
                pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));

                pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                pstmt.executeUpdate();
                pstmt.close();

                //DTU-369 Payment Notifications
                if (Rep.GetPaymentNotification(this.repId)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, sessionData.getProperty("username"));
                    pstmt.setString(3, Languages.getString("com.debisys.customers.agent_update_credit", sessionData.getLanguage()));
                    pstmt.setDouble(4, dblNewCreditLimit - currentLiabilityLimit);
                    pstmt.setDouble(5, dblNewCreditLimit);
                    pstmt.setDouble(6, currentLiabilityLimit);
                    pstmt.setDouble(7, dblNewCreditLimit - currentRunningLiability);
                    pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                    pstmt.setDouble(9, 0);
                    pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                    pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                    pstmt.setString(12, this.businessName);
                    pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                    pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                    pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                    pstmt.executeUpdate();
                }

                if (currentLiabilityLimit != dblNewCreditLimit) {
                    Log.write(sessionData, "Liability limit changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                            + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewCreditLimit)),
                            DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                }

            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during updateAgentLiabilityLimit", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }

    }

    public boolean checkChildCreditType() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAllowed = false;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            String strSQL = Rep.sql_bundle.getString("checkChildCreditType");
            pstmt = dbConn.prepareStatement(strSQL);
            pstmt.setDouble(1, Double.parseDouble(this.repId));
            Rep.cat.debug(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                isAllowed = false;
            } else {
                isAllowed = true;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkChildCreditType", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isAllowed;
    }

    public void makeAgentPayment(SessionData sessionData, ServletContext context, boolean creditPrepaidInt) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            if (this.checkUpdateAgentPermission(sessionData)) {

                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) || !NumberUtil.isNumeric(this.paymentAmount)) {
                    this.paymentAmount = "0.00";
                }
                // select r.rep_credit_type, r.LiabilityLimit,
                // r.runningliability from
                // reps r (nolock) where r.rep_id = ?
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                rs.next();
                double currentLiabilityLimit = Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));
                double currentRunningLiability = Double.parseDouble(NumberUtil.formatAmount(rs.getString("runningliability")));
                double dblPaymentAmount = Double.parseDouble(this.paymentAmount);
                // new value for prepaid based reps
                double dblNewCreditLimit = (currentLiabilityLimit - currentRunningLiability) + dblPaymentAmount;
                // new value for credit based reps
                double dblNewRunningLiability = currentRunningLiability - dblPaymentAmount;
                String currentCreditType = rs.getString("rep_credit_type");//this.oldRepCreditType;
                TorqueHelper.closeStatement(pstmt, rs);

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                    // update reps set liabilityLimit = ?, runningliability=?,
                    // rep_credit_type=? where rep_id = ?
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("makePayment"));
                    pstmt.setDouble(1, 0);
                    pstmt.setDouble(2, 0);
                    pstmt.setInt(3, Integer.parseInt(this.repCreditType));
                    pstmt.setDouble(4, Double.parseDouble(this.repId));
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, null);
                } else {

                    Rep tmpAgent = new Rep();
                    tmpAgent.setRepId(this.repId);
                    tmpAgent.getAgent(sessionData, context);
                    insertBalanceHistory(DebisysConstants.REP_TYPE_AGENT, this.repId, tmpAgent.parentRepId,
                            tmpAgent.repCreditType, tmpAgent.sharedBalance, tmpAgent.entityAccountType, tmpAgent.runningLiability, tmpAgent.creditLimit, sessionData.getProperty("username"), false);

                    if (dblNewRunningLiability < 0 && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        // addFieldError("error","Payments can not be made when
                        // there is
                        // zero assigned credit");
                        this.addFieldError("error", "9");
                    } else if (dblNewRunningLiability > currentLiabilityLimit && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        // addFieldError("error","Assigned credit exceeds credit
                        // limit");
                        this.addFieldError("error", "8");
                    } else if ((dblNewCreditLimit < 0) && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        // addFieldError("error","Credit limit must be greater
                        // than 0");
                        this.addFieldError("error", "7");
                    } else {

                        // makePayment=update reps set liabilityLimit = ?,
                        // runningliability=?, rep_credit_type=? where rep_id =
                        // ?
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("makePayment"));
                        if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                            pstmt.setDouble(1, currentLiabilityLimit);
                            pstmt.setDouble(2, dblNewRunningLiability);
                        } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                            if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                    && creditPrepaidInt) {
                                pstmt.setDouble(1, (currentLiabilityLimit - currentRunningLiability));
                            } else {
                                pstmt.setDouble(1, dblNewCreditLimit);
                            }
                            pstmt.setDouble(2, 0);
                        }
                        pstmt.setInt(3, Integer.parseInt(this.repCreditType));
                        pstmt.setDouble(4, Double.parseDouble(this.repId));
                        pstmt.executeUpdate();
                        TorqueHelper.closeStatement(pstmt, null);

                        if (this.paymentDescription != null && this.paymentDescription.length() > 255) {
                            this.paymentDescription = this.paymentDescription.substring(0, 255);
                        }

                        if (!NumberUtil.isNumeric(this.commission)) {
                            this.commission = "0";
                        }

                        // 1 2 3 4 5 6 7 8 9 10 11
                        // (rep_id, logon_id, description, amount, credit_limit,
                        // prior_credit_limit, available_credit,
                        // prior_available_credit,
                        // commission, datetime, entityaccounttype)
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentCredit"));
                        pstmt.setDouble(1, Double.parseDouble(this.repId));
                        pstmt.setString(2, sessionData.getProperty("username"));
                        pstmt.setString(3, this.paymentDescription);
                        pstmt.setDouble(4, dblPaymentAmount);
                        if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                            pstmt.setDouble(5, currentLiabilityLimit);
                            pstmt.setDouble(7, currentLiabilityLimit - dblNewRunningLiability);
                        } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                            pstmt.setDouble(5, dblNewCreditLimit);
                            pstmt.setDouble(7, dblNewCreditLimit);
                        }
                        pstmt.setDouble(6, currentLiabilityLimit);
                        pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                        pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(this.commission)));
                        pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                        pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                        pstmt.executeUpdate();

                        //DTU-369 Payment Notifications
                        if (Rep.GetPaymentNotification(this.repId)) {
                            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                            pstmt.setDouble(1, Double.parseDouble(this.repId));
                            pstmt.setString(2, sessionData.getProperty("username"));
                            pstmt.setString(3, this.paymentDescription);
                            pstmt.setDouble(4, dblPaymentAmount);
                            if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                                pstmt.setDouble(5, currentLiabilityLimit);
                                pstmt.setDouble(7, currentLiabilityLimit - dblNewRunningLiability);
                            } else {
                                pstmt.setDouble(5, dblNewCreditLimit);
                                pstmt.setDouble(7, dblNewCreditLimit);
                            }
                            pstmt.setDouble(6, currentLiabilityLimit);
                            pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                            pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(this.commission)));
                            pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                            pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                            pstmt.setString(12, this.businessName);
                            pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                            pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                            pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                            pstmt.executeUpdate();
                        }

                        TorqueHelper.closeStatement(pstmt, null);
                    }
                }

                if (dblPaymentAmount != 0) {
                    if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        Log.write(sessionData, "Running liability changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewRunningLiability)),
                                DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        Log.write(sessionData, "Liability limit changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewCreditLimit))
                                + ", Running liability changed from " + NumberUtil.formatAmount(Double.toString(currentRunningLiability)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " 0.00", DebisysConstants.LOGTYPE_CUSTOMER, this.repId,
                                DebisysConstants.PW_REF_TYPE_REP_ISO);
                    }
                }

                if (!currentCreditType.equals(this.repCreditType)) {
                    Log.write(sessionData, "Credit type changed from " + currentCreditType + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " "
                            + this.repCreditType, DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_AGENT, this.repId, DebisysConstants.AUDIT_CREDITTYPE_CHANGE, currentCreditType, this.repCreditType);
                }
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during makeAgentPayment", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }

    }

    public Vector doAgentPaymentMX(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection cnx = null;
        Vector vResult = new Vector();
        CallableStatement cst = null;
        String strSQL = "";
        String sDeploymentType = DebisysConfigListener.getDeploymentType(context);
        String sCustomConfigType = DebisysConfigListener.getCustomConfigType(context);

        sDeploymentType = DebisysConstants.DEPLOYMENT_INTERNATIONAL;
        sCustomConfigType = DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO;

        try {
            if (this.checkUpdateAgentPermission(sessionData)) {
                strSQL = Rep.sql_bundle.getString("applyAgentPayment");
                cnx = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (cnx == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new ReportException();
                }

                Rep.cat.debug(strSQL);
                cst = cnx.prepareCall(strSQL);
                cst.setString(1, this.repId);
                cst.setDouble(2, Double.parseDouble(this.paymentAmount));
                cst.setDouble(3, Double.parseDouble(this.commission));
                cst.setString(4, this.paymentDescription);
                cst.setString(5, sessionData.getProperty("username"));
                cst.setString(6, sDeploymentType);
                cst.setString(7, sCustomConfigType);
                cst.registerOutParameter(8, Types.INTEGER);
                cst.setInt(8, 0);
                cst.execute();
                vResult.add(cst.getInt(8));
                cst.close();
            }
        } catch (Exception e) {
            Rep.cat.error("Error during doAgentPaymentMX", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(cnx, cst, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vResult;
    }// End of function doPaymentMX

    // checks to make the rep is unlimited
    // if the merchant type changes to unlimited
    public boolean checkAgentCreditLimit(SessionData sessionData, ServletContext context) {
        boolean isAllowed = false;
        Rep agent = new Rep();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = "select rep_id from reps with (nolock) where rep_id in (select iso_id from reps with (nolock) where rep_id=" + this.repId + ")";
            Rep.cat.debug("[checkAgentCreditLimit]: " + strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.agentId = rs.getString("rep_id");
            }
            TorqueHelper.closeStatement(pstmt, rs);
            agent.setRepId(this.agentId);
            agent.getAgent(sessionData, context);

            if (agent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                // if the rep is unlimited we dont care what the assigned is
                isAllowed = true;
            } else // check credit limit to make sure rep has enough
            {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getSubagentLiabilities"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                rs.next();
                // double currentRunningLiability =
                // rs.getDouble("runningliability");
                double currentLiabilityLimit = rs.getDouble("liabilitylimit");

                // double currentAvailableCredit = currentLiabilityLimit -
                // currentRunningLiability;
                // limit we are trying to change to
                double subagentCreditIncrease = 0;

                if (!NumberUtil.isNumeric(this.getPaymentAmount()))// this is a
                // credit limit
                // update
                {
                    subagentCreditIncrease = Double.parseDouble(this.creditLimit) - currentLiabilityLimit;
                } else // this is a payment
                {
                    subagentCreditIncrease = Double.parseDouble(this.getPaymentAmount());
                }

                // rep limit
                double agentAvailableCredit = Double.parseDouble(agent.getAvailableCredit());
                Rep.cat.debug(" Available Credit for Agent:" + agentAvailableCredit);

                if (subagentCreditIncrease <= agentAvailableCredit) {
                    isAllowed = true;
                }
                TorqueHelper.closeStatement(pstmt, rs);
            }
        } catch (TorqueException e1) {
            Rep.cat.error("[checkAgentCreditLimit] TorqueException: " + e1.getMessage(), e1);
        } catch (CustomerException e1) {
            // FIXME this feature should be throwing CustomerException!!!
            Rep.cat.error("[checkAgentCreditLimit] CustomerException: " + e1.getMessage(), e1);
        } catch (SQLException e1) {
            Rep.cat.error("[checkAgentCreditLimit] SQLException: " + e1.getMessage(), e1);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }

        return isAllowed;
    }

    public void updateSubagentLiabilityLimit2(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getSubagentLiabilityLimit"));
            pstmt.setDouble(1, Double.parseDouble(this.repId));

            rs = pstmt.executeQuery();
            rs.next();
            String sLiabilityLimit = rs.getString("liabilitylimit");
            TorqueHelper.closeStatement(pstmt, rs);

            Rep subAgentTemp = new Rep();
            subAgentTemp.setRepId(this.repId);
            subAgentTemp.getSubAgent(sessionData, context);
            insertBalanceHistory(DebisysConstants.REP_TYPE_SUBAGENT, subAgentTemp.repId, subAgentTemp.parentRepId,
                    subAgentTemp.repCreditType, subAgentTemp.sharedBalance, subAgentTemp.entityAccountType, subAgentTemp.runningLiability, sLiabilityLimit, sessionData.getProperty("username"), false);

            String currentLiabilityLimit = NumberUtil.formatAmount(sLiabilityLimit);
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateSubAgentLiabilityLimit2"));
            pstmt.setDouble(1, Double.parseDouble(this.creditLimit));
            pstmt.setDouble(2, Double.parseDouble(this.repId));
            pstmt.executeUpdate();

            TorqueHelper.closeStatement(pstmt, rs);

            Log.write(sessionData, Languages.getString("com.debisys.customers.Reps.Log.liability_limit", sessionData.getLanguage()) + currentLiabilityLimit + " "
                    + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatCurrency(this.creditLimit), DebisysConstants.LOGTYPE_CUSTOMER, this.repId,
                    DebisysConstants.PW_REF_TYPE_REP_ISO);

            String deploymentType = DebisysConfigListener.getDeploymentType(context);

            //if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
            //{
            Rep agent = new Rep();
            String strSQL = "select rep_id from reps with (nolock) where rep_id in (select iso_id from reps with (nolock) where rep_id=" + this.repId + ")";
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.agentId = rs.getString("rep_id");
            }
            TorqueHelper.closeStatement(pstmt, rs);

            agent.setRepId(this.agentId);
            agent.getAgent(sessionData, context);

            // if the reps not unlimited, log the merchant credit assignment
            if (!agent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                double subagentCreditLimit = agent.getCurrentAgentCreditLimit(sessionData);
                double subagentAssignedCredit = agent.getAssignedCreditAgent(sessionData);
                double changeInSubagentCredit = Double.parseDouble(this.creditLimit) - Double.parseDouble(currentLiabilityLimit);
                // add an entry to show the rep credits being used
                // 1 2 3 4 5 6 7 8 9 10
                // (rep_id, merchant_id, logon_id, description, amount,
                // available_credit, prior_available_credit, datetime, agent_entityaccounttype, subagent_entityaccounttype)
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                pstmt.setDouble(1, Double.parseDouble(this.agentId));
                pstmt.setDouble(2, Double.parseDouble(this.repId));
                pstmt.setString(3, sessionData.getProperty("username"));
                pstmt.setString(4, Languages.getString("com.debisys.customers.subagent.credit_updated", sessionData.getLanguage()));
                pstmt.setDouble(5, changeInSubagentCredit * -1);
                pstmt.setDouble(6, subagentCreditLimit - (subagentAssignedCredit + changeInSubagentCredit));
                pstmt.setDouble(7, subagentCreditLimit - subagentAssignedCredit);
                pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                pstmt.setInt(9, agent.getEntityAccountType());
                Rep subagent = new Rep();
                subagent.setRepId(this.repId);
                subagent.getSubAgent(sessionData, context);
                pstmt.setInt(10, subagent.getEntityAccountType());
                pstmt.executeUpdate();

                TorqueHelper.closeStatement(pstmt, rs);

                Rep.cat.debug(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));

                // update reps running liability
                // updateRepRunningLiability=update set runningliability=?
                // where rep_id=?
                Rep.cat.debug(Rep.sql_bundle.getString("updateRepRunningLiability"));
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepRunningLiability"));
                pstmt.setDouble(1, subagentAssignedCredit + changeInSubagentCredit);
                pstmt.setDouble(2, Double.parseDouble(this.agentId));
                pstmt.executeUpdate();
            }
            //}
        } catch (Exception e) {
            Rep.cat.error("Error during updateSubAgentLiabilityLimit2", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
    }

    /**
     * @param intPageNumber
     * @param intRecordsPerPage
     * @param sessionData
     * @param context
     * @param scheduleReportType
     * @param headers
     * @param titles
     * @return
     * @throws CustomerException
     */
    public Vector getSubagentCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector vecSubAgentCredits = new Vector();
        StringBuilder strBuffSQL = new StringBuilder();
        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
            String reportId = sessionData.getProperty("reportId");
            int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);

            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            }

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            this.endDate = format.format(new Date());
            this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
            Date date1 = format.parse(this.startDate);
            Date date2 = format.parse(this.endDate);
            format.applyPattern("yyyy-MM-dd");
            this.startDate = format.format(date1);
            this.endDate = format.format(date2);
            Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate + " 23:59:59.999", false);

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQLWhere = "";
            String strSQLOrderBy = " order by subagent_credits.id DESC ";

            if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                strBuffSQL.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS);
            }

            // get a count of total matches
            String strSQL = "SELECT dbo.GetLocalTimeByAccessLevel(" + sessionData.getProperty("access_level") + ", " + sessionData.getProperty("ref_id") + ", subagent_credits.datetime, 1) as datetime , "
                    + " subagent_credits.logon_id, subagent_credits.description, subagent_credits.amount, subagent_credits.commission, subagent_credits.available_credit, "
                    + " (subagent_credits.amount - (subagent_credits.amount * (isnull(subagent_credits.commission,0) * .01))) as net_payment " + strBuffSQL.toString()
                    + " FROM subagent_credits WITH(NOLOCK) WHERE subagent_id=?";

            if (scheduleReportType != DebisysConstants.SCHEDULE_REPORT) {
                if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate)) {
                    strSQLWhere = " and datetime >= '" + vTimeZoneFilterDates.get(0) + "' and datetime <= '" + vTimeZoneFilterDates.get(1) + "'";
                }
            } else {
                String fixedQuery = strSQL.replaceFirst("\\?", this.repId);
                String relativeQuery = strSQL.replaceFirst("\\?", this.repId);

                relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " " + strSQLOrderBy;

                fixedQuery += " AND datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND datetime <= '" + vTimeZoneFilterDates.get(1) + "' " + strSQLOrderBy;

                ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_SUBA_CRED_HIST, 1);
                scheduleReport.setNameDateTimeColumn("datetime");

                scheduleReport.setRelativeQuery(relativeQuery);

                scheduleReport.setFixedQuery(fixedQuery);
                TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
                sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
                return null;
            }

            strSQL = strSQL + strSQLWhere + strSQLOrderBy;

            if (scheduleReportType != DebisysConstants.DOWNLOAD_REPORT) {
                pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                int intRecordCount = DbUtil.getRecordCount(rs);

                // first row is always the count
                vecSubAgentCredits.add(Integer.valueOf(intRecordCount));
                if (intRecordCount > 0) {
                    rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
                    for (int i = 0; i < intRecordsPerPage; i++) {

                        Vector vecTemp = new Vector();
                        String logon_id = rs.getString("logon_id");
                        if (logon_id == null) {
                            logon_id = "";
                        }
                        vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
                        vecTemp.add(StringUtil.toString(logon_id));
                        String description = rs.getString("description");
                        if (description == null) {
                            description = "";
                        }
                        vecTemp.add(StringUtil.toString(description));
                        if (logon_id.equals("Events Job")) {
                            vecTemp.add("");
                        } else {
                            String amount = rs.getString("amount");
                            if (amount == null) {
                                amount = "0";                                
                            }
                            vecTemp.add(NumberUtil.formatCurrency(amount));
                        }
                        String available_credit = rs.getString("available_credit");
                        if (available_credit == null) {
                            available_credit = "0";
                        }
                        vecTemp.add(NumberUtil.formatCurrency(available_credit));
                        String commission = rs.getString("commission");
                        if (commission == null) {
                            commission = "0";
                        }
                        vecTemp.add(NumberUtil.formatAmount(commission));
                        if (logon_id.equals("Events Job")) {
                            vecTemp.add("");
                        } else {
                            String net_payment = rs.getString("net_payment");
                            if (net_payment == null) {
                                net_payment = "0";
                            }
                            vecTemp.add(NumberUtil.formatCurrency(net_payment));
                        }
                        vecSubAgentCredits.add(vecTemp);
                        if (!rs.next()) {
                            break;
                        }
                    }
                }

            } else {
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                DownloadsSummary trxSummary = new DownloadsSummary();
                TransactionReport tr = new TransactionReport();
                String strFileName = tr.generateFileName(context);
                this.setStrUrlLocation(trxSummary.download(rs, sessionData, context, headers, strFileName, titles, null, 2));
                rs.close();
                pstmt.close();
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getSubAgentCredits", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vecSubAgentCredits;
    }

    /**
     * @param intPageNumber
     * @param intRecordsPerPage
     * @param sessionData
     * @param context
     * @param scheduleReportType
     * @param headers
     * @param titles
     * @return
     * @throws CustomerException
     */
    public Vector getSubagentRepCredits(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Vector vecSubagentRepCredits = new Vector();
        String strAccessLevel = sessionData.getProperty("access_level");
        StringBuilder strBuffSQL = new StringBuilder();
        // prevent reps from seeing others reps
        // nned to make this more secure but out of time
        if (strAccessLevel.equals(DebisysConstants.REP)) {
            this.repId = sessionData.getProperty("ref_id");
        }

        try {
            String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
            String reportId = sessionData.getProperty("reportId");
            int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
            if (dataBase.equals(DebisysConstants.DATA_BASE_MASTER)) {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            } else {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));
            }

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            this.endDate = format.format(new Date());
            this.startDate = DateUtil.addSubtractDays(this.endDate, -limitDays);
            Date date1 = format.parse(this.startDate);
            Date date2 = format.parse(this.endDate);
            format.applyPattern("yyyy-MM-dd");
            this.startDate = format.format(date1);
            this.endDate = format.format(date2);
            Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), this.startDate, this.endDate + " 23:59:59.999", false);

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQLWhere = "";

            if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                strBuffSQL.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS);
            }

            // get a count of total matches
            String strSQL = "select dbo.GetLocalTimeByAccessLevel(" + sessionData.getProperty("access_level") + ", " + sessionData.getProperty("ref_id") + ", sarc.datetime, 1) as datetime , "
                    + " sarc.logon_id, sarc.rep_id, sarc.description, sarc.amount, sarc.available_credit, "
                    + " sarc.prior_available_credit " + strBuffSQL.toString()
                    + " from subagent_rep_credits sarc WITH(NOLOCK) where sarc.subagent_id=?";

            if (scheduleReportType != DebisysConstants.SCHEDULE_REPORT) {
                if (DateUtil.isValidYYYYMMDD(this.startDate) && DateUtil.isValidYYYYMMDD(this.endDate)) {
                    strSQLWhere = " and sarc.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and sarc.datetime <= '" + vTimeZoneFilterDates.get(1) + "'";
                }
            } else {
                String fixedQuery = strSQL.replaceFirst("\\?", this.repId);
                String relativeQuery = strSQL.replaceFirst("\\?", this.repId);

                relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT;

                fixedQuery += " AND rmc.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND rmc.datetime <= '" + vTimeZoneFilterDates.get(1) + "' ";

                ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_SUBAGENT_CREDIT_ASSIGNMENT_HIST, 1);
                scheduleReport.setNameDateTimeColumn("sarc.datetime");

                scheduleReport.setRelativeQuery(relativeQuery);

                scheduleReport.setFixedQuery(fixedQuery);
                TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
                sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
                return null;

            }

            Rep.cat.debug(strSQL + strSQLWhere + " order by sarc.id DESC");

            if (scheduleReportType != DebisysConstants.DOWNLOAD_REPORT) {
                pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by sarc.id DESC", ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                int intRecordCount = DbUtil.getRecordCount(rs);
                // first row is always the count
                vecSubagentRepCredits.add(Integer.valueOf(intRecordCount));
                if (intRecordCount > 0) {
                    rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
                    for (int i = 0; i < intRecordsPerPage; i++) {

                        Vector vecTemp = new Vector();
                        vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
                        vecTemp.add(StringUtil.toString(rs.getString("logon_id")));
                        vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
                        vecTemp.add(StringUtil.toString(rs.getString("description")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("available_credit")));
                        vecTemp.add(NumberUtil.formatCurrency(rs.getString("prior_available_credit")));
                        vecSubagentRepCredits.add(vecTemp);
                        if (!rs.next()) {
                            break;
                        }
                    }
                }
            } else {
                pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by sarc.id DESC");
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                DownloadsSummary trxSummary = new DownloadsSummary();
                TransactionReport tr = new TransactionReport();
                String strFileName = tr.generateFileName(context);
                this.setStrUrlLocation(trxSummary.download(rs, sessionData, context, headers, strFileName, titles, null, 2));
                rs.close();
                pstmt.close();
            }

        } catch (Exception e) {
            Rep.cat.error("Error during getSubagentRepCredits", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vecSubagentRepCredits;
    }

    public Vector doPaymentSubagentMX(SessionData sessionData, ServletContext context) throws CustomerException {
        Connection cnx = null;
        Vector vResult = new Vector();
        CallableStatement cst = null;
        String strSQL = "";
        String sDeploymentType = DebisysConfigListener.getDeploymentType(context);
        String sCustomConfigType = DebisysConfigListener.getCustomConfigType(context);
        boolean useEntityAccountType = sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE);

        if (useEntityAccountType) {
            sDeploymentType = DebisysConstants.DEPLOYMENT_INTERNATIONAL;
            sCustomConfigType = DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO;
        }

        try {
            if (this.checkUpdateSubAgentPermission(sessionData)) {
                strSQL = Rep.sql_bundle.getString("applySubAgentPayment");
                cnx = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));

                if (cnx == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new ReportException();
                }

                Rep.cat.debug(strSQL);
                cst = cnx.prepareCall(strSQL);
                cst.setString(1, this.repId);
                cst.setDouble(2, Double.parseDouble(this.paymentAmount));
                cst.setDouble(3, Double.parseDouble(this.commission));
                cst.setString(4, this.paymentDescription);
                cst.setString(5, sessionData.getProperty("username"));
                cst.setString(6, sDeploymentType);
                cst.setString(7, sCustomConfigType);
                cst.registerOutParameter(8, Types.INTEGER);
                cst.setInt(8, 0);
                cst.execute();
                vResult.add(cst.getInt(8));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during doPaymentSubAgentMX", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(cnx, cst, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        Rep.cat.debug("result" + vResult.get(0));
        return vResult;
    } // End of function doPaymentMX

    public void updateSubagentLiabilityLimit(SessionData sessionData, ServletContext context) throws CustomerException {
        String deploymentType = DebisysConfigListener.getDeploymentType(context);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        if (NumberUtil.isNumeric(this.getPaymentAmount())) {
            Connection dbConn = null;

            try {
                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));

                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }

                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getSubagentLiabilities"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));

                rs = pstmt.executeQuery();
                rs.next();

                double currentRunningLiability = rs.getDouble("runningliability");
                double currentLiabilityLimit = rs.getDouble("liabilitylimit");
                double currentAvailableCredit = currentLiabilityLimit - currentRunningLiability;

                if ((currentAvailableCredit + Double.parseDouble(this.getPaymentAmount())) < 0) {
                    this.addFieldError("paymentError", Languages.getString("com.debisys.customers.rep_invalid_payment", sessionData.getLanguage()));

                    return;
                }

                TorqueHelper.closeStatement(pstmt, rs);

                Rep.cat.debug(Rep.sql_bundle.getString("updateSubAgentLiabilityLimit"));
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateSubAgentLiability"));
                pstmt.setDouble(1, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
                pstmt.setDouble(2, Double.parseDouble(this.repId));
                pstmt.executeUpdate();

                TorqueHelper.closeStatement(pstmt, rs);

                if ((this.paymentDescription != null) && (this.paymentDescription.length() > 255)) {
                    this.paymentDescription = this.paymentDescription.substring(0, 255);
                }

                if (!NumberUtil.isNumeric(this.commission)) {
                    this.commission = "0";
                }

                double dblCommission = Double.parseDouble(this.commission);
                Rep.cat.debug(Rep.sql_bundle.getString("insertSubAgentCredit"));
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentCredit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                pstmt.setString(2, sessionData.getProperty("username"));
                pstmt.setString(3, this.paymentDescription);
                pstmt.setDouble(4, Double.parseDouble(this.getPaymentAmount()));
                pstmt.setDouble(5, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
                pstmt.setDouble(6, currentLiabilityLimit);
                pstmt.setDouble(7, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));

                pstmt.setDouble(8, currentAvailableCredit);
                pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
                pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                pstmt.executeUpdate();

                //DTU-369 Payment Notifications
                if (Rep.GetPaymentNotification(this.repId)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, sessionData.getProperty("username"));
                    pstmt.setString(3, this.paymentDescription);
                    pstmt.setDouble(4, Double.parseDouble(this.getPaymentAmount()));
                    pstmt.setDouble(5, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
                    pstmt.setDouble(6, currentLiabilityLimit);
                    pstmt.setDouble(7, currentAvailableCredit + Double.parseDouble(this.getPaymentAmount()));
                    pstmt.setDouble(8, currentAvailableCredit);
                    pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(Double.toString(dblCommission))));
                    pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                    pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                    pstmt.setString(12, this.businessName);
                    pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                    pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                    pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                    pstmt.executeUpdate();
                }

                TorqueHelper.closeStatement(pstmt, rs);

                double dblNewAvailableCredit = currentAvailableCredit + Double.parseDouble(this.getPaymentAmount());
                String strNewAvailableCredit = NumberUtil.formatCurrency(Double.toString(dblNewAvailableCredit));

                Log.write(sessionData, Languages.getString("com.debisys.customers.Merchant.Log.liability_limit", sessionData.getLanguage())
                        + NumberUtil.formatCurrency(Double.toString(currentLiabilityLimit)) + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " "
                        + strNewAvailableCredit, DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_MERCHANT);

                if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {

                    try {
                        String strSQL = "select rep_id from reps with (nolock) where rep_id in (select iso_id from reps with (nolock) where rep_id="
                                + this.repId + ")";
                        Rep.cat.debug(strSQL);
                        pstmt = dbConn.prepareStatement(strSQL);
                        rs = pstmt.executeQuery();
                        if (rs.next()) {
                            this.agentId = rs.getString("rep_id");
                        }

                        TorqueHelper.closeStatement(pstmt, rs);
                    } catch (Exception e) {
                    }

                    Rep agent = new Rep();
                    agent.setRepId(this.agentId);
                    agent.getAgent(sessionData, context);

                    // if the reps not unlimited, log the merchant credit
                    // assignment
                    if (!agent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                        double agentCreditLimit = agent.getCurrentAgentCreditLimit(sessionData);
                        double agentAssignedCredit = agent.getAssignedCreditAgent(sessionData);
                        double changeInSubAgentCredit = dblNewAvailableCredit - currentAvailableCredit;

                        // add an entry to show the rep credits being used
                        // 1 2 3 4 5 6 7 8
                        // (rep_id, merchant_id, logon_id, description, amount,
                        // available_credit, prior_available_credit, datetime)
                        Rep.cat.debug(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertAgentSubAgentCredit"));
                        pstmt.setDouble(1, Double.parseDouble(this.agentId));
                        pstmt.setDouble(2, Double.parseDouble(this.repId));
                        pstmt.setString(3, sessionData.getProperty("username"));
                        pstmt.setString(4, Languages.getString("com.debisys.customers.subagent.credit_updated", sessionData.getLanguage()));
                        pstmt.setDouble(5, changeInSubAgentCredit * -1);
                        pstmt.setDouble(6, agentCreditLimit - (agentAssignedCredit + changeInSubAgentCredit));
                        pstmt.setDouble(7, agentCreditLimit - agentAssignedCredit);
                        pstmt.setTimestamp(8, new java.sql.Timestamp(new java.util.Date().getTime()));
                        pstmt.setInt(9, agent.getEntityAccountType());
                        Rep childsa = new Rep();
                        childsa.setRepId(this.repId);
                        childsa.getSubAgent(sessionData, context);
                        pstmt.setInt(10, childsa.getEntityAccountType());
                        pstmt.executeUpdate();

                        TorqueHelper.closeStatement(pstmt, rs);

                        // update reps running liability
                        // updateRepRunningLiability=update set
                        // runningliability=? where rep_id=?
                        Rep.cat.debug(Rep.sql_bundle.getString("updateRepRunningLiability"));
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("updateRepRunningLiability"));
                        pstmt.setDouble(1, agentAssignedCredit + changeInSubAgentCredit);
                        pstmt.setDouble(2, Double.parseDouble(this.agentId));
                        pstmt.executeUpdate();

                    }
                }
            } catch (Exception e) {
                Rep.cat.error("Error during updateSubagentLiabilityLimit", e);
                throw new CustomerException();
            } finally {
                try {
                    TorqueHelper.closeConnection(dbConn, pstmt, rs);
                } catch (Exception e) {
                    Rep.cat.error("Error during release connection", e);
                }
            }
        }
    }

    public boolean checkAgentCreditType(SessionData sessionData, ServletContext context) {
        boolean isAllowed = false;
        Rep agent = new Rep();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String strSQL = "select * from reps where rep_id in (select iso_id from reps where rep_id=" + this.repId + ")";
            Rep.cat.debug("[checkAgentCreditType]: " + strSQL);

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("[checkAgentCreditType] Can't get database connection");
                throw new EntityException();
            }
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.agentId = rs.getString("rep_id");
            }
        } catch (TorqueException e1) {
            Rep.cat.error("[checkAgentCreditType] TorqueException: " + e1.getMessage(), e1);
        } catch (EntityException e1) {
            // FIXME this feat should throw EntityException
            Rep.cat.error("[checkAgentCreditType] EntityException: " + e1.getMessage(), e1);
        } catch (SQLException e1) {
            Rep.cat.error("[checkAgentCreditType] SQLException: " + e1.getMessage(), e1);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }

        try {
            agent.setRepId(this.agentId);
            agent.getAgent(sessionData, context);
        } catch (Exception e) {
            // FIXME is this correct?
        }

        if (agent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
            isAllowed = true;
        }

        return isAllowed;
    }

    public void makeSubAgentPayment(SessionData sessionData, ServletContext context, boolean creditPrepaidInt) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            if (this.checkUpdateSubAgentPermission(sessionData)) {

                dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
                if (dbConn == null) {
                    Rep.cat.error("Can't get database connection");
                    throw new CustomerException();
                }

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED) || !NumberUtil.isNumeric(this.paymentAmount)) {
                    this.paymentAmount = "0.00";
                }
                // select r.rep_credit_type, r.LiabilityLimit,
                // r.runningliability from
                // reps r (nolock) where r.rep_id = ?
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepLiabilityLimit"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                rs.next();
                double currentLiabilityLimit = Double.parseDouble(NumberUtil.formatAmount(rs.getString("liabilitylimit")));
                double currentRunningLiability = Double.parseDouble(NumberUtil.formatAmount(rs.getString("runningliability")));
                double dblPaymentAmount = Double.parseDouble(this.paymentAmount);
                // new value for prepaid based reps
                double dblNewCreditLimit = (currentLiabilityLimit - currentRunningLiability) + dblPaymentAmount;
                // new value for credit based reps
                double dblNewRunningLiability = currentRunningLiability - dblPaymentAmount;
                String currentCreditType = this.oldRepCreditType;// rs.getString("rep_credit_type");
                TorqueHelper.closeStatement(pstmt, rs);

                if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                    // update reps set liabilityLimit = ?, runningliability=?,
                    // rep_credit_type=? where rep_id = ?
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("makePayment"));
                    pstmt.setDouble(1, 0);
                    pstmt.setDouble(2, 0);
                    pstmt.setInt(3, Integer.parseInt(this.repCreditType));
                    pstmt.setDouble(4, Double.parseDouble(this.repId));
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, null);
                } else if (dblNewRunningLiability < 0 && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                    // addFieldError("error","Payments can not be made when
                    // there is
                    // zero assigned credit");
                    this.addFieldError("error", "9");
                } else if (dblNewRunningLiability > currentLiabilityLimit && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                    // addFieldError("error","Assigned credit exceeds credit
                    // limit");
                    this.addFieldError("error", "8");
                } else if ((dblNewCreditLimit < 0) && this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                    // addFieldError("error","Credit limit must be greater
                    // than 0");
                    this.addFieldError("error", "7");
                } else {
                    Rep tmpSubAgent = new Rep();
                    tmpSubAgent.setRepId(this.repId);
                    tmpSubAgent.getSubAgent(sessionData, context);
                    insertBalanceHistory(DebisysConstants.REP_TYPE_SUBAGENT, this.repId, tmpSubAgent.parentRepId,
                            tmpSubAgent.repCreditType, tmpSubAgent.sharedBalance, tmpSubAgent.entityAccountType, tmpSubAgent.runningLiability, tmpSubAgent.creditLimit, sessionData.getProperty("username"), false);

                    // makePayment=update reps set liabilityLimit = ?,
                    // runningliability=?, rep_credit_type=? where rep_id =
                    // ?
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("makePayment"));
                    if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        pstmt.setDouble(1, currentLiabilityLimit);
                        pstmt.setDouble(2, dblNewRunningLiability);
                    } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && creditPrepaidInt) {

                            pstmt.setDouble(1, (currentLiabilityLimit - currentRunningLiability));
                        } else {
                            pstmt.setDouble(1, dblNewCreditLimit);
                        }
                        pstmt.setDouble(2, 0);
                    }
                    pstmt.setInt(3, Integer.parseInt(this.repCreditType));
                    pstmt.setDouble(4, Double.parseDouble(this.repId));
                    pstmt.executeUpdate();
                    TorqueHelper.closeStatement(pstmt, null);

                    if (this.paymentDescription != null && this.paymentDescription.length() > 255) {
                        this.paymentDescription = this.paymentDescription.substring(0, 255);
                    }

                    if (!NumberUtil.isNumeric(this.commission)) {
                        this.commission = "0";
                    }

                    // 1 2 3 4 5 6 7 8 9 10 11
                    // (rep_id, logon_id, description, amount, credit_limit,
                    // prior_credit_limit, available_credit,
                    // prior_available_credit,
                    // commission, datetime, entityaccounttype)
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertSubAgentCredit"));
                    pstmt.setDouble(1, Double.parseDouble(this.repId));
                    pstmt.setString(2, sessionData.getProperty("username"));
                    pstmt.setString(3, this.paymentDescription);
                    pstmt.setDouble(4, dblPaymentAmount);
                    if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        pstmt.setDouble(5, currentLiabilityLimit);
                        pstmt.setDouble(7, currentLiabilityLimit - dblNewRunningLiability);
                    } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        pstmt.setDouble(5, dblNewCreditLimit);
                        pstmt.setDouble(7, dblNewCreditLimit);
                    }
                    pstmt.setDouble(6, currentLiabilityLimit);
                    pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                    pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(this.commission)));
                    pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                    pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                    pstmt.executeUpdate();

                    //DTU-369 Payment Notifications
                    if (Rep.GetPaymentNotification(this.repId)) {
                        pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertPaymentNotification"));
                        pstmt.setDouble(1, Double.parseDouble(this.repId));
                        pstmt.setString(2, sessionData.getProperty("username"));
                        pstmt.setString(3, this.paymentDescription);
                        pstmt.setDouble(4, dblPaymentAmount);
                        if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                            pstmt.setDouble(5, currentLiabilityLimit);
                            pstmt.setDouble(7, currentLiabilityLimit - dblNewRunningLiability);
                        } else {
                            pstmt.setDouble(5, dblNewCreditLimit);
                            pstmt.setDouble(7, dblNewCreditLimit);
                        }
                        pstmt.setDouble(6, currentLiabilityLimit);
                        pstmt.setDouble(8, currentLiabilityLimit - currentRunningLiability);
                        pstmt.setDouble(9, Double.parseDouble(NumberUtil.formatAmount(this.commission)));
                        pstmt.setTimestamp(10, new java.sql.Timestamp(new java.util.Date().getTime()));
                        pstmt.setInt(11, EntityAccountTypes.GetEntityAccountTypeRepType(this.repId));
                        pstmt.setString(12, this.businessName);
                        pstmt.setInt(13, Rep.GetPaymentNotificationType(this.repId));
                        pstmt.setString(14, Rep.GetSmsNotificationPhone(this.repId));
                        pstmt.setString(15, Rep.GetMailNotificationAddress(this.repId));
                        pstmt.executeUpdate();
                    }

                    TorqueHelper.closeStatement(pstmt, null);
                }

                if (dblPaymentAmount != 0) {
                    if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        Log.write(sessionData, "Running liability changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewRunningLiability)),
                                DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    } else if (this.repCreditType.equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        Log.write(sessionData, "Liability limit changed from " + NumberUtil.formatAmount(Double.toString(currentLiabilityLimit)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " " + NumberUtil.formatAmount(Double.toString(dblNewCreditLimit))
                                + ", Running liability changed from " + NumberUtil.formatAmount(Double.toString(currentRunningLiability)) + " "
                                + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " 0.00", DebisysConstants.LOGTYPE_CUSTOMER, this.repId,
                                DebisysConstants.PW_REF_TYPE_REP_ISO);
                    }
                }

                if (!currentCreditType.equals(this.repCreditType)) {
                    Log.write(sessionData, "Credit type changed from " + currentCreditType + " " + Languages.getString("jsp.admin.to", sessionData.getLanguage()) + " "
                            + this.repCreditType, DebisysConstants.LOGTYPE_CUSTOMER, this.repId, DebisysConstants.PW_REF_TYPE_REP_ISO);
                    insertBalanceAudit(sessionData, DebisysConstants.REP_TYPE_SUBAGENT, this.repId, DebisysConstants.AUDIT_CREDITTYPE_CHANGE, currentCreditType, this.repCreditType);
                }
            } else {
                this.addFieldError("error", Languages.getString("com.debisys.customers.rep_no_permission", sessionData.getLanguage()));
            }
        } catch (Exception e) {
            Rep.cat.error("Error during makeSubAgentPayment", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }

    }

    public boolean checkSubAgentCreditType(SessionData sessionData, ServletContext context) {
        boolean isAllowed = false;
        Rep subagent = new Rep();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String strSQL = "select * from reps where rep_id in (select iso_id from reps where rep_id=" + this.repId + ")";
            Rep.cat.debug(strSQL);

            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("[checkSubAgentCreditType] Can't get database connection");
                throw new EntityException();
            }
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.subagentId = rs.getString("rep_id");
            }
        } catch (TorqueException e1) {
            Rep.cat.error("[checkSubAgentCreditType] TorqueException: " + e1.getMessage(), e1);
        } catch (EntityException e1) {
            // FIXME This one should throw EntityException
            Rep.cat.error("[checkSubAgentCreditType] EntityException: " + e1.getMessage(), e1);
        } catch (SQLException e1) {
            Rep.cat.error("[checkSubAgentCreditType] SQLException: " + e1.getMessage(), e1);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }

        try {
            subagent.setRepId(this.subagentId);
            subagent.getSubAgent(sessionData, context);
        } catch (Exception e) {
            // FIXME IS THIS CORRECT?
        }

        if (subagent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
            isAllowed = true;
        }

        return isAllowed;
    }

    // checks to make the rep is unlimited
    // if the merchant type changes to unlimited
    public boolean checkSubAgentCreditLimit(SessionData sessionData, ServletContext context) {
        boolean isAllowed = false;
        Rep subagent = new Rep();
        Connection dbConn = null;
        ResultSet rs = null;
        PreparedStatement pstmt = null;

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String strSQL = "select rep_id from reps with (nolock) where rep_id in (select iso_id from reps with (nolock) where rep_id=" + this.repId + ")";
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                this.subagentId = rs.getString("rep_id");
            }
            TorqueHelper.closeStatement(pstmt, rs);
            subagent.setRepId(this.subagentId);
            subagent.getSubAgent(sessionData, context);
            if (subagent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                // if the rep is unlimited we dont care what the assigned is
                isAllowed = true;
            } else {
                // check credit limit to make sure rep has enough
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getSubagentLiabilities"));
                pstmt.setDouble(1, Double.parseDouble(this.repId));
                rs = pstmt.executeQuery();
                rs.next();

                // double currentRunningLiability =
                // rs.getDouble("runningliability");
                double currentLiabilityLimit = rs.getDouble("liabilitylimit");

                // double currentAvailableCredit = currentLiabilityLimit -
                // currentRunningLiability;
                // limit we are trying to change to
                double repCreditIncrease = 0;
                // this is a credit limit update
                if (!NumberUtil.isNumeric(this.getPaymentAmount())) {
                    repCreditIncrease = Double.parseDouble(this.creditLimit) - currentLiabilityLimit;
                } else {
                    // this is a payment
                    repCreditIncrease = Double.parseDouble(this.getPaymentAmount());
                }

                // rep limit
                double subagentAvailableCredit = Double.parseDouble(subagent.getAvailableCredit());
                Rep.cat.debug("Available Credit for SubAgent:" + subagentAvailableCredit);

                if (repCreditIncrease <= subagentAvailableCredit) {
                    isAllowed = true;
                }
                TorqueHelper.closeStatement(pstmt, rs);
            }

        } catch (TorqueException e1) {
            Rep.cat.error("[checkSubAgentCreditLimit] TorqueException: " + e1.getMessage(), e1);
        } catch (CustomerException e1) {
            // FIXME This feat should be throwing CustomerException
            Rep.cat.error("[checkSubAgentCreditLimit] CustomerException: " + e1.getMessage(), e1);
        } catch (SQLException e1) {
            Rep.cat.error("[checkSubAgentCreditLimit] SQLException: " + e1.getMessage(), e1);
        } finally {
            try {

                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }

        }
        return isAllowed;
    }

    public String getOldParentRepId() {
        return this.oldParentRepId;
    }

    public void setOldParentRepId(String oldParentRepId) {
        this.oldParentRepId = oldParentRepId;
    }

    public boolean validateCheckSubAgentChange(SessionData sessionData, ServletContext context) {
        boolean valid = true;
        if ((this.oldParentRepId != null) && !this.oldParentRepId.equals(this.parentRepId)) {
            // rep changed so lets make sure new rep can handle it
            try {
                Rep oldSubAgent = new Rep();
                oldSubAgent.setRepId(this.oldParentRepId);
                oldSubAgent.getSubAgent(sessionData, context);

                Rep newSubAgent = new Rep();
                newSubAgent.setRepId(this.parentRepId);
                newSubAgent.getSubAgent(sessionData, context);
                Rep tempRep = new Rep();
                tempRep.setRepId(this.repId);
                tempRep.getRep(sessionData, context);
                if (!oldSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {

                    if (this.creditLimit.equals("")
                            && (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))) {
                        this.creditLimit = tempRep.getCreditLimit();
                    }

                    // old.credit -> new.credit
                    if (newSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        // check if rep has enough available
                        // and transfer the entire merchant liability limit
                        if (Double.parseDouble(newSubAgent.getAvailableCredit()) < Double.parseDouble(tempRep.getCreditLimit())) {
                            this.addFieldError("repCredit", Languages.getString("com.debisys.customers.subagent_credit_error2", sessionData.getLanguage()));
                            valid = false;
                        }
                    } // old.prepaid -> new.prepaid
                    else if (newSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        // check if rep has enough available
                        // and transfer the only merchant's available credit
                        double availRepCredit = Double.parseDouble(tempRep.getCreditLimit()) - Double.parseDouble(tempRep.getRunningLiability());

                        if (Double.parseDouble(newSubAgent.getAvailableCredit()) < availRepCredit) {
                            this.addFieldError("repCredit", Languages.getString("com.debisys.customers.subagent_credit_error2", sessionData.getLanguage()));
                            valid = false;
                        }
                    }
                }

                if (oldSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                    if ((newSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT) || newSubAgent.getRepCreditType().equals(
                            DebisysConstants.REP_CREDIT_TYPE_PREPAID))
                            && tempRep.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                        this.addFieldError("repCredit", Languages.getString("com.debisys.customers.subagent_credit_error6", sessionData.getLanguage()));
                        valid = false;
                    }

                }
                // Validate credit type and credit limit
                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    if (!this.checkSubAgentCreditLimit(sessionData, context)) {
                        this.addFieldError("RepCreditLimit", Languages.getString("jsp.admin.customers.rep.subagentchange_notavailablecredit", sessionData.getLanguage()));
                        valid = false;
                    }
                } // End of validate credit type and credit limit
            } catch (Exception e) {
                Rep.cat.error("Error while validating the Subagent change from one to another", e);
            }
        }
        return valid;
    }

    public boolean validateCheckAgentChange(SessionData sessionData, ServletContext context) {
        boolean valid = true;
        if ((this.oldParentRepId != null) && !this.oldParentRepId.equals(this.parentRepId)) {
            // rep changed so lets make sure new rep can handle it
            try {
                Rep oldAgent = new Rep();
                oldAgent.setRepId(this.oldParentRepId);
                oldAgent.getAgent(sessionData, context);

                Rep newAgent = new Rep();
                newAgent.setRepId(this.parentRepId);
                newAgent.getAgent(sessionData, context);

                Rep tempSubAgent = new Rep();
                tempSubAgent.setRepId(this.repId);
                tempSubAgent.getSubAgent(sessionData, context);
                if (!oldAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {

                    if (this.creditLimit.equals("")
                            && (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))) {
                        this.creditLimit = tempSubAgent.getCreditLimit();
                    }

                    // old.credit -> new.credit
                    if (newAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT)) {
                        // check if rep has enough available
                        // and transfer the entire merchant liability limit
                        if (Double.parseDouble(newAgent.getAvailableCredit()) < Double.parseDouble(tempSubAgent.getCreditLimit())) {
                            this.addFieldError("repCredit", Languages.getString("com.debisys.customers.agent_credit_error2", sessionData.getLanguage()));
                            valid = false;
                        }
                    } // old.prepaid -> new.prepaid
                    else if (newAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_PREPAID)) {
                        // check if rep has enough available
                        // and transfer the only merchant's available credit
                        double availRepCredit = Double.parseDouble(tempSubAgent.getCreditLimit()) - Double.parseDouble(tempSubAgent.getRunningLiability());

                        if (Double.parseDouble(newAgent.getAvailableCredit()) < availRepCredit) {
                            this.addFieldError("repCredit", Languages.getString("com.debisys.customers.agent_credit_error2", sessionData.getLanguage()));
                            valid = false;
                        }
                    }
                }

                if (oldAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                    if ((newAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_CREDIT) || newAgent.getRepCreditType().equals(
                            DebisysConstants.REP_CREDIT_TYPE_PREPAID))
                            && tempSubAgent.getRepCreditType().equals(DebisysConstants.REP_CREDIT_TYPE_UNLIMITED)) {
                        this.addFieldError("repCredit", Languages.getString("com.debisys.customers.agent_credit_error6", sessionData.getLanguage()));
                        valid = false;
                    }

                }
                // Validate credit type and credit limit
                if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                    if (!this.checkAgentCreditLimit(sessionData, context)) {
                        this.addFieldError("RepCreditLimit", Languages.getString("jsp.admin.customers.rep.agentchange_notavailablecredit", sessionData.getLanguage()));
                        valid = false;
                    }
                } // End of validate credit type and credit limit
            } catch (Exception e) {
                Rep.cat.error("Error while validating the Agent change from one to another", e);
            }
        }
        return valid;
    }

    // Check to make sure you are a valid iso to edit a subagent
    public boolean checkUpdateAgentPermission(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAllowed = false;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strAccessLevel = sessionData.getProperty("access_level");
            String strRefId = sessionData.getProperty("ref_id");
            // iso
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkAgentIso5"));
                    pstmt.setDouble(1, Double.parseDouble(strRefId));
                    pstmt.setDouble(2, Double.parseDouble(this.repId));
                }
            } else {
                isAllowed = false;
                return isAllowed;
            }
            rs = pstmt.executeQuery();
            if (rs.next()) {
                isAllowed = true;
            } else {
                isAllowed = false;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkUpdateAgentPermission", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isAllowed;
    }

    // Check to make sure you are a valid iso, agent to edit a subagent
    public boolean checkUpdateSubAgentPermission(SessionData sessionData) {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean isAllowed = false;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            String strAccessLevel = sessionData.getProperty("access_level");
            String strRefId = sessionData.getProperty("ref_id");
            // iso
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkSubagentIso5"));
                    pstmt.setDouble(1, Double.parseDouble(strRefId));
                    pstmt.setDouble(2, Double.parseDouble(this.repId));
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("checkSubagentAgent"));
                pstmt.setDouble(1, Double.parseDouble(strRefId));
                pstmt.setDouble(2, Double.parseDouble(this.repId));
            } else {
                isAllowed = false;
                return isAllowed;
            }
            rs = pstmt.executeQuery();
            if (rs.next()) {
                isAllowed = true;
            } else {
                isAllowed = false;
            }
        } catch (Exception e) {
            Rep.cat.error("Error during checkUpdateSubagentPermission", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return isAllowed;
    }

    // # End DBSYS-613 Nidhi Gulati
    /**
     * @param repId Identity from the rep whose ISO will be retrived.
     * @return If found, the rep ISO record for the given rep_id
     */
    public static Rep getRepIso(String repId) {
        Rep retValue = null;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getRepIso"));
            pstmt.setDouble(1, Double.parseDouble(repId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = new Rep();
                retValue.setAccountNumber(rs.getString("account_no"));
                retValue.setAccountTypeId(rs.getInt("AccountTypeID"));
                retValue.setAddress(rs.getString("address"));

                retValue.setBankName(rs.getString("BankName"));
                retValue.setBusinessHours(rs.getString("BusinessHours"));
                retValue.setBusinessName(rs.getString("businessname"));
                retValue.setBusinessTypeId(rs.getString("sic_code"));
                retValue.setCity(rs.getString("city"));
                // retValue.setContactDepartment(rs.getString("")
                // contactDepartment);
                retValue.setContactEmail(rs.getString("email"));
                retValue.setContactFax(rs.getString("fax"));
                retValue.setContactFirst(rs.getString("firstname"));
                retValue.setContactLast(rs.getString("lastname"));
                retValue.setContactMiddle(rs.getString("minitial"));
                retValue.setContactName(retValue.getContactFirst() + " " + retValue.getContactLast());
                retValue.setContactPhone(rs.getString("phone"));
                // retValue.setContactTypeId(rs.getInt(""));
                retValue.setCountry(rs.getString("country"));
                retValue.setCreditLimit(NumberUtil.formatAmount(rs.getString("LiabilityLimit")));
                retValue.setEndDate(rs.getString("termdate"));
                retValue.setInitials(rs.getString("initials"));
                retValue.setLegalBusinessname(rs.getString("legal_businessname"));
                retValue.setMailAddress(rs.getString("mail_address"));
                retValue.setMailCity(rs.getString("mail_city"));
                retValue.setMailColony(rs.getString("mail_colony"));
                retValue.setMailCountry(rs.getString("mail_country"));
                retValue.setMailDelegation(rs.getString("mail_delegation"));
                retValue.setMailState(rs.getString("mail_state"));
                retValue.setMailZip(rs.getString("mail_zip"));
                retValue.setMerchantSegmentId(rs.getInt("MerchantSegmentID"));
                // retValue.setNetPaymentAmount(rs.getString(""));
                retValue.setOldParentRepId(rs.getString("iso_id"));
                // retValue.setOldRepCreditType(rs.getString(""));
                retValue.setParentRepId(rs.getString("iso_id"));
                // retValue.setParentRepname(rs.getString(""));
                // retValue.setPaymentAmount(rs.getString(""));
                // retValue.setPaymentDescription(rs.getString(""));
                retValue.setPaymentType(rs.getInt("payment_type"));
                retValue.setPhysColony(rs.getString("phys_colony"));
                retValue.setPhysCounty(rs.getString("phys_county"));
                retValue.setPhysDelegation(rs.getString("phys_delegation"));
                retValue.setProcessType(rs.getInt("payment_processor"));
                retValue.setRepCreditType(rs.getString("Rep_credit_type"));
                retValue.setRepId(rs.getString("rep_id"));
                retValue.setRoutingNumber(rs.getString("aba"));
                retValue.setRunningLiability(rs.getString("RunningLiability"));
                retValue
                        .setAvailableCredit(Double.toString(Double.parseDouble(retValue.getCreditLimit()) - Double.parseDouble(retValue.getRunningLiability())));
                retValue.setSharedBalance(rs.getString("SharedBalance"));
                retValue.setStartDate(rs.getString("hiredate"));
                retValue.setState(rs.getString("state"));
                retValue.setTaxId(rs.getString("ssn"));
                // retValue.setVecContacts(vecContacts);
                retValue.setZip(rs.getString("zip"));
            }
        } catch (Exception localException) {
            Rep.cat.error("Error during getRepIso", localException);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return retValue;
    }

    /**
     * *
     * Get the top-level ISO ID for any entity ID and an entity type
     *
     * @param _entityID Rep ID / Merchant ID
     * @param _entityType Type of the entity (8 = merchant ....)
     * @return String of the ISO ID
     */
    public static String getIso(String _entityID, String _entityType) {
        Rep.cat.debug("Begin getIso");
        String _isoID = "";

        InputParameter[] inParms = new InputParameter[2];
        inParms[0] = new InputParameter(_entityID, "NUMERIC");
        inParms[1] = new InputParameter(_entityType, "INTEGER");

        OutputParameter[] outParms = new OutputParameter[1];
        outParms[0] = new OutputParameter("ISOID", "3", "NUMERIC");

        Vector<String> spResults = DbUtils.ExecuteStoredProcedure(Rep.sql_bundle.getString("pkgDefaultDb"), Rep.sql_bundle.getString("getIso"), inParms, outParms);

        _isoID = spResults.get(0);
        Rep.cat.debug("End getIso");
        return _isoID;
    }

    /**
     * *
     * Get the top-level ISO ID for any entity ID and an entity type
     *
     * @param _entityID
     * @param _entitylevel
     * @param strrefid Rep ID / Merchant ID
     * @param strAccessLevel Type of the entity
     * @return String of the ISO ID
     */
    public static String getIsoID(String _entityID, String _entitylevel) {
        Rep.cat.debug("Begin getIso");
        String _isoID = "";
        String level = "";

        if (_entitylevel.equals(DebisysConstants.ISO)) {
            // returning ISO ID since entity is already an ISO
            return _entityID;
        } else if (_entitylevel.equals(DebisysConstants.AGENT)) {
            level = "4";
        } else if (_entitylevel.equals(DebisysConstants.SUBAGENT)) {
            level = "5";
        } else if (_entitylevel.equals(DebisysConstants.REP)) {
            level = "1";
        } else if (_entitylevel.equals(DebisysConstants.MERCHANT)) {
            level = "8";
        }
        if (StringUtil.isNotEmptyStr(level)) {
            InputParameter[] inParms = new InputParameter[2];
            inParms[0] = new InputParameter(_entityID, "NUMERIC");
            inParms[1] = new InputParameter(level, "INTEGER");

            OutputParameter[] outParms = new OutputParameter[1];
            outParms[0] = new OutputParameter("ISOID", "3", "NUMERIC");

            Vector<String> spResults = DbUtils.ExecuteStoredProcedure(Rep.sql_bundle.getString("pkgDefaultDb"), Rep.sql_bundle.getString("getIso"), inParms, outParms);

            _isoID = spResults.get(0);
            Rep.cat.debug("End getIso");
            // returning ISO ID
            return _isoID;
        }
        // / wrong parameters were passed
        return null;
    }

    /**
     * *
     * Get the credit type of the specified ISO IS
     *
     * @param _isoID ISO ID you're looking for.
     * @return Credit type
     */
    public static String getISOCreditType(String _isoID) {
        Rep.cat.debug("Begin getISOCreditType");
        String _credType = "";

        Vector<Vector<String>> qResults = DbUtils.ExecuteQuery(Rep.sql_bundle.getString("pkgDefaultDb"), Rep.sql_bundle.getString("getISOCreditType"),
                new String[]{_isoID});

        // defaulting to Credit: temporary fix for QComm Migrated Merchants.
        _credType = DbUtils.ExpectSingleResult(qResults);
        _credType = _credType == null ? DebisysConstants.REP_CREDIT_TYPE_CREDIT : _credType;

        Rep.cat.debug("End getISOCreditType");
        return _credType;
    }

    /**
     * @param IsoId Iso identifier
     * @return The
     */
    public static String getIsoPaymentType(String IsoId) {
        String retValue = "";
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            IsoId = IsoId.trim();
            if (IsoId.equals("")) {
                Rep.cat.error("Empty IsoId passed as parameter");
                throw new Exception();
            }
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getIsoPaymentType"));
            pstmt.setDouble(1, Double.parseDouble(IsoId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = rs.getString("code");
            }
        } catch (Exception localException) {
            Rep.cat.error("Error during getIsoPaymentType", localException);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return retValue;
    }

    /**
     * @param webPermission
     * @param IsoId Iso identifier
     * @return True
     */
    public static boolean getIsoWebPermission(int webPermission, String IsoId) {
        boolean retValue = false;
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            IsoId = IsoId.trim();
            if (IsoId.equals("")) {
                Rep.cat.error("Empty IsoId passed as parameter");
                throw new Exception();
            }
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new Exception();
            }
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getIsoWebPermission"));
            pstmt.setInt(1, webPermission);
            pstmt.setDouble(2, Double.parseDouble(IsoId));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                retValue = true;
            }
        } catch (Exception localException) {
            Rep.cat.error("Error during getIsoWebPermission", localException);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection", e);
            }
        }
        return retValue;
    }

    /**
     * @param timeZoneId the timeZoneId to set
     */
    public void setTimeZoneId(int timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    /**
     * @return the timeZoneId
     */
    public int getTimeZoneId() {
        return this.timeZoneId;
    }

    /**
     * Returns a JSON vector wrapping the results of the getRepsByActor method
     *
     * @param sData String with AccessLevel and ID of the owning actor
     * @return JSON vector to be used via Ajax invokation
     * @throws CustomerException
     */
    public static JSONObject getRepsByActorAjax(String sData, SessionData sessionData) throws CustomerException {
        JSONObject json = new JSONObject();
        try {
            Vector jsonVec = new Vector();
            Iterator it = Rep.getRepsByActor(sData.split("_")[0], sData.split("_")[1], sData.split("_")[2]).iterator();

            while (it.hasNext()) {
                Vector vTmp = (Vector) it.next();
                Hashtable map = new Hashtable();
                map.put("rep_id", vTmp.get(0).toString());
                map.put("businessname", URLEncoder.encode(vTmp.get(1).toString(), "UTF-8"));

                jsonVec.add(map);
            }

            json.put("items", jsonVec);
        } catch (Exception e) {
            Rep.cat.error("Error during getMerchantsByActorAjax", e);
            throw new CustomerException();
        }

        return json;
    }

    /**
     * Returns a vector of reps depending the specified actor level and ID
     *
     * @param sAccessLevel DebisysConstant of the access level
     * @param sActorId ID of the actor
     * @param sChainLevel DebisysConstant of the Chain level
     * @return Vector of reps
     * @throws CustomerException
     */
    public static Vector getRepsByActor(String sAccessLevel, String sActorId, String sChainLevel) throws CustomerException {
        Vector vResult = new Vector();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            String sSQL = "SELECT re.rep_id, re.businessname FROM reps re (NOLOCK) ";

            if (sAccessLevel.equals(DebisysConstants.REP)) {
                sSQL += "INNER JOIN reps r (NOLOCK) ON m.rep_id = r.rep_id WHERE r.rep_id = " + sActorId + " ORDER BY re.businessname";
            } else if (sAccessLevel.equals(DebisysConstants.SUBAGENT)
                    || (sAccessLevel.equals(DebisysConstants.ISO) && sChainLevel.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))) {
                sSQL += "INNER JOIN reps r (NOLOCK) ON re.rep_id = r.rep_id WHERE r.rep_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id = " + sActorId
                        + ") ORDER BY re.businessname";
            } else if (sAccessLevel.equals(DebisysConstants.AGENT)) {
                sSQL += "INNER JOIN reps r (NOLOCK) ON re.rep_id = r.rep_id WHERE r.rep_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id IN"
                        + "(SELECT rep_id FROM reps (NOLOCK) WHERE iso_id = " + sActorId + ")) ORDER BY re.businessname";
            } else if (sAccessLevel.equals(DebisysConstants.ISO) && sChainLevel.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                sSQL += "INNER JOIN reps r (NOLOCK) ON re.rep_id = r.rep_id WHERE r.rep_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id IN"
                        + "(SELECT rep_id FROM reps (NOLOCK) WHERE iso_id IN (SELECT rep_id FROM reps (NOLOCK) WHERE iso_id = " + sActorId
                        + "))) ORDER BY re.businessname";
            }

            Rep.cat.debug("getRepsByActor " + sSQL);
            pstmt = dbConn.prepareStatement(sSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("rep_id"));
                vecTemp.add(rs.getString("businessname"));

                vResult.add(vecTemp);
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getRepsByActor", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }

        return vResult;
    }

    /**
     * Returns a JSON vector wrapping the results of the getRepsByActor method
     *
     * @param sData String with AccessLevel and ID of the owning actor
     * @param sessionData
     * @return JSON vector to be used via Ajax invokation
     * @throws CustomerException
     */
    public static JSONObject getEntityListByIsoAjax(String sData, SessionData sessionData) throws CustomerException {
        JSONObject json = new JSONObject();
        try {
            Vector jsonVec = new Vector();
            Iterator it = Rep.getEntityListByIso(sData.split("_")[0], sData.split("_")[1], sData.split("_")[2]).iterator();

            while (it.hasNext()) {
                Vector vTmp = (Vector) it.next();
                Hashtable map = new Hashtable();
                map.put("rep_id", vTmp.get(0).toString());
                map.put("businessname", URLEncoder.encode(vTmp.get(1).toString(), "UTF-8"));

                jsonVec.add(map);
            }

            json.put("items", jsonVec);
        } catch (Exception e) {
            Rep.cat.error("Error during getEntityListByIsoAjax", e);
            throw new CustomerException();
        }

        return json;
    }

    public static Vector getEntityListByIso(String sAccessLevel, String sActorId, String sChainLevel) throws CustomerException {
        Vector vResult = new Vector();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            String sSQL = "";

            if (sAccessLevel.equals(DebisysConstants.AGENT)) {
                sSQL += "SELECT a.rep_id, a.businessname  "
                        + " FROM reps a (NOLOCK) "
                        + " INNER JOIN reps i (NOLOCK) ON i.rep_id = a.iso_id "
                        + " WHERE i.rep_id =  ? and a.type = 4 ORDER BY a.businessname ";
            } else if (sAccessLevel.equals(DebisysConstants.SUBAGENT) || (sAccessLevel.equals(DebisysConstants.ISO) && !sChainLevel.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))) {
                sSQL += "SELECT s.rep_id, s.businessname "
                        + " FROM reps s (NOLOCK) "
                        + " INNER JOIN reps a (NOLOCK) ON a.rep_id = s.iso_id "
                        + " INNER JOIN reps i (NOLOCK) ON i.rep_id = a.iso_id  "
                        + " WHERE i.rep_id =  ? and s.type = 5 ORDER BY s.businessname ";
            } else if (sAccessLevel.equals(DebisysConstants.REP)) {
                sSQL += "SELECT r.rep_id, r.businessname "
                        + " FROM reps r (NOLOCK) "
                        + " INNER JOIN reps s (NOLOCK) ON s.rep_id = r.iso_id "
                        + " INNER JOIN reps a (NOLOCK) ON a.rep_id = s.iso_id "
                        + " INNER JOIN reps i (NOLOCK) ON i.rep_id = a.iso_id  "
                        + " WHERE i.rep_id =  ? and r.type = 1 ORDER BY r.businessname";
            } else if (sAccessLevel.equals(DebisysConstants.MERCHANT)) {
                sSQL += "SELECT m.merchant_id, m.dba  "
                        + " FROM merchants m (NOLOCK) "
                        + " INNER JOIN reps r (NOLOCK) ON r.rep_id = m.rep_id "
                        + " INNER JOIN reps s (NOLOCK) ON s.rep_id = r.iso_id "
                        + " INNER JOIN reps a (NOLOCK) ON a.rep_id = s.iso_id "
                        + " INNER JOIN reps i (NOLOCK) ON i.rep_id = a.iso_id "
                        + " WHERE i.rep_id =  ? ORDER BY m.dba";
            }

            Rep.cat.debug("getEntityListByIso " + sSQL + "/*" + sActorId + "*/");
            pstmt = dbConn.prepareStatement(sSQL);
            pstmt.setString(1, sActorId);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString(1));
                vecTemp.add(rs.getString(2));

                vResult.add(vecTemp);
            }
        } catch (Exception e) {
            Rep.cat.error("Error during getEntityListByActor", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection (getEntityListByActor)", e);
            }
        }

        return vResult;
    }

    public static Vector getRepListReports(SessionData sessionData) throws CustomerException {

        Vector vecRepList = new Vector();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strRefId = sessionData.getProperty("ref_id");

        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgAlternateDb"));

            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            String strSQL = Rep.sql_bundle.getString("getRepList");

            String strSQLWhere = "";
            String hdr = "Access level matches ";

            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                    strSQLWhere = strSQLWhere + " r.iso_id = " + strRefId;
                } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    strSQLWhere = " r.rep_id IN " + "(SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type=" + DebisysConstants.REP_TYPE_REP
                            + " AND r1.iso_id in " + "(SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type=" + DebisysConstants.REP_TYPE_SUBAGENT
                            + " AND r2.iso_id IN " + "(SELECT r3.rep_id FROM reps AS r3 WITH(NOLOCK) WHERE r3.type=" + DebisysConstants.REP_TYPE_AGENT
                            + " AND r3.iso_id = " + strRefId + "))) ";
                }
            } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                strSQLWhere = " r.rep_id IN " + "(SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type=" + DebisysConstants.REP_TYPE_REP
                        + " AND r1.iso_id IN " + "(SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type=" + DebisysConstants.REP_TYPE_SUBAGENT
                        + " AND r2.iso_id = " + strRefId + ")) ";
            } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                strSQLWhere = " r.rep_id IN " + "(SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type=" + DebisysConstants.REP_TYPE_REP
                        + " AND r1.iso_id = " + strRefId + ")";
            } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                // cat.debug(hdr+"REP");
                strSQLWhere = "  r.rep_id = " + strRefId;
            }
            /*
			 * else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) { Merchant.cat.debug(hdr + "MERCHANT"); strSQLWhere = " and m.merchant_id = " +
			 * strRefId; }
             */

            strSQL = strSQL + strSQLWhere + " ORDER BY r.businessname";
            Rep.cat.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL);

            rs = pstmt.executeQuery();
            while (rs.next()) {
                Vector vecTemp = new Vector();
                vecTemp.add(rs.getString("rep_id"));
                vecTemp.add(rs.getString("businessname"));
                vecRepList.add(vecTemp);
            }
        } catch (Exception e) {
            Merchant.cat.error("Error during getRepList", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Merchant.cat.error("Error during closeConnection", e);
            }
        }

        return vecRepList;
    }

    public static Vector<Vector<String>> getReferralAgents() throws CustomerException {
        Connection cn = null;
        Vector<Vector<String>> vResults = new Vector<Vector<String>>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sSQL = null;

        try {
            cn = Torque.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (cn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            sSQL = Rep.sql_bundle.getString("getReferralAgents");
            ps = cn.prepareStatement(sSQL);
            rs = ps.executeQuery();
            while (rs.next()) {
                Vector<String> vecTemp = new Vector<String>();
                vecTemp.add(rs.getString("rep_id"));
                vecTemp.add(rs.getString("businessname"));
                vResults.add(vecTemp);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            Rep.cat.error("Error during getReferralAgents", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(cn, ps, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vResults;
    }

    public boolean hasNewRatePlanMerchants() throws CustomerException {
        Connection cn = null;
        boolean bResult = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sSQL = null;

        try {
            cn = Torque.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (cn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            sSQL = Rep.sql_bundle.getString("hasNewRatePlanMerchants");
            ps = cn.prepareStatement(sSQL);
            ps.setString(1, this.repId);
            rs = ps.executeQuery();
            if (rs.next()) {
                bResult = rs.getInt("MerchantCount") > 0;
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            Rep.cat.error("Error during hasNewRatePlanMerchants", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(cn, ps, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return bResult;
    }

    public boolean relocateNewRatePlansForActor() // throws CustomerException
    {
        Connection cn = null;
        boolean bResult = false;
        PreparedStatement ps = null;
        String sSQL = null;

        try {
            cn = Torque.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (cn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            sSQL = Rep.sql_bundle.getString("relocateNewRatePlansForActor");
            ps = cn.prepareStatement(sSQL);
            ps.setString(1, this.repId);
            ps.execute();
            bResult = true;
            ps.close();
        } catch (Exception e) {
            Rep.cat.error("Error during relocateNewRatePlansForActor", e);
            // throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(cn, ps, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return bResult;
    }

    //DTU-369 Payment Notifications
    public static boolean GetPaymentNotification(String repID) {
        boolean type = false;
        Connection dbConn = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement("SELECT payment_notifications FROM reps WITH (NOLOCK) WHERE rep_id = ?");
            pstmt.setString(1, repID);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                type = rs.getBoolean("payment_notifications");
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during GetPaymentNotification", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }
        return type;
    }

    public static int GetPaymentNotificationType(String repID) {
        int type = 0;
        Connection dbConn = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement("SELECT payment_notification_type FROM reps WITH (NOLOCK) WHERE rep_id = ?");
            pstmt.setString(1, repID);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                type = rs.getInt("payment_notification_type");
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during GetPaymentNotificationType", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }
        return type;
    }

    public static String GetSmsNotificationPhone(String repID) {
        String type = "";
        Connection dbConn = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement("SELECT sms_notification_phone FROM reps WITH (NOLOCK) WHERE rep_id = ?");
            pstmt.setString(1, repID);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                type = rs.getString("sms_notification_phone");
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during GetSmsNotificationPhone", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }
        return type;
    }

    public static String GetMailNotificationAddress(String repID) {
        String type = "";
        Connection dbConn = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new Exception();
            }

            PreparedStatement pstmt = dbConn.prepareStatement("SELECT mail_notification_address FROM reps WITH (NOLOCK) WHERE rep_id = ?");
            pstmt.setString(1, repID);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                type = rs.getString("mail_notification_address");
            }

            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during GetMailNotificationAddress", e);
        } finally {
            try {
                Torque.closeConnection(dbConn);
            } catch (Exception e) {
                cat.error("Error during release connection", e);
            }
        }
        return type;
    }

    public static Vector getRepsForISO(long isoId) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Vector vResult = null;
        Connection dbConn = null;
        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new Exception();
            }
            ps = dbConn.prepareStatement(sql_bundle.getString("getIsoPrepaidReps"));
            ps.setLong(1, isoId);
            cat.debug("Executing getIsoReps SP: [" + sql_bundle.getString("getIsoPrepaidReps") + "]");
            rs = ps.executeQuery();
            vResult = new Vector();
            while (rs.next()) {
                Vector vTmp = new Vector();

                vTmp.add(rs.getLong("rep_id"));
                vTmp.add(rs.getString("businessname").trim());
                vResult.add(vTmp);
            } // End while(rs.next())
        } catch (Exception ex) {
            if (vResult != null) {
                vResult.clear();
            }
            vResult = null;
            cat.debug("Error when retrieving getIsoForReps data: " + ex.toString());
        } finally {
            DbUtil.closeDatabaseObjects(dbConn, ps, rs);
        }
        return vResult;
    }

    public Vector doRepTransfer(SessionData sessionData, ServletContext context, String destinationID) throws CustomerException, UserException {
        Connection cnx = null;
        Vector vResult = new Vector();
        CallableStatement cst = null;
        if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)) {
            sessionData.setProperty("iso_id", (new User()).getISOId(DebisysConstants.REP, this.repId));
            sessionData.setProperty("iso_id", sessionData.getUser().getIsoId());
        }       

        try {

            String strSQL = Rep.sql_bundle.getString("applyRepTransferEntityPayment");
            cnx = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (cnx == null) {
                Rep.cat.error("Can't get database connection");
                throw new ReportException();
            }

            Rep.cat.debug(strSQL);
            cst = cnx.prepareCall(strSQL);
            cst.setString(1, this.repId);
            cst.setString(2, destinationID);
            cst.setString(3, sessionData.getProperty("username"));
            cst.setDouble(4, Double.parseDouble(this.paymentAmount));
            cst.setString(5, sessionData.getProperty("ip_address"));
            cst.setString(6, this.paymentDescription);
            cst.registerOutParameter(7, Types.INTEGER);
            cst.setInt(7, 0);
            cst.registerOutParameter(8, Types.LONGVARCHAR);
            cst.setString(8, "");
            cst.execute();
            vResult.add(String.valueOf(cst.getInt(7)));
            vResult.add(cst.getString(8));
            Rep.cat.debug(vResult);

        } catch (Exception e) {
            Rep.cat.error("Error during doPaymentMX", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(cnx, cst, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
        return vResult;
    }// End of func

    protected static void insertBalanceHistory(String entityType, String entityID, String parentRepId, String repCreditType, String sharedBalance, int entityAccountType, String runningLiability, String creditLimit, String userName, boolean isMerchant) throws CustomerException {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new ReportException();
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateInit = simpleDateFormat.format(new Date());
            Date parseInit = simpleDateFormat.parse(dateInit);
            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getBalanceHistoryByEntity"));
            pstmt.setDouble(1, Double.parseDouble(entityID));
            pstmt.setTimestamp(2, new Timestamp(parseInit.getTime()));
            rs = pstmt.executeQuery();
            if (!rs.next()) {
                if (isMerchant) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getValidMerchant"));
                } else {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("getValidRep"));
                }
                pstmt.setDouble(1, Double.parseDouble(entityID));
                rs2 = pstmt.executeQuery();

                if (rs2.next()) {
                    pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertBalanceHistory"));
                    pstmt.setString(1, UUID.randomUUID().toString().toUpperCase());
                    pstmt.setTimestamp(2, new Timestamp(new Date().getTime()));
                    pstmt.setTimestamp(3, new Timestamp(new Date().getTime()));
                    pstmt.setInt(4, Integer.parseInt(entityType));
                    pstmt.setDouble(5, (entityID == null) ? null : Double.parseDouble(entityID));
                    pstmt.setDouble(6, (parentRepId == null) ? null : Double.parseDouble(parentRepId));
                    pstmt.setInt(7, (repCreditType == null) ? null : Integer.parseInt(repCreditType));
                    pstmt.setBoolean(8, sharedBalance.trim().equals("1"));
                    pstmt.setInt(9, entityAccountType);
                    pstmt.setDouble(10, (runningLiability == null) ? null : Double.parseDouble(runningLiability));
                    pstmt.setDouble(11, (creditLimit == null) ? null : Double.parseDouble(creditLimit));
                    pstmt.setString(12, userName);
                    pstmt.executeUpdate();
                }
            }
        } catch (Exception e) {
            Rep.cat.error("Error during insertBalanceHistory", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(null, null, rs2);
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                Rep.cat.error("Error during release connection (insertBalanceHistory)", e);
            }
        }
    }

    /**
     * Log credit type changes to BalanceAudit Table
     *
     * @param sessionData
     * @param entityAccountType DebisysConstants.XXX. A constant for this change
     * type
     * @param entityId repId for representatives, merchant_id for merchants.
     * @param changeType DebisysConstants.AUDIT_CREDITTYPE_CHANGE. A constant
     * for this change type.
     * @param oldValue
     * @param newValue
     * @throws CustomerException
     */
    public static void insertBalanceAudit(SessionData sessionData, String entityAccountType, String entityId, String changeType, String oldValue, String newValue) throws CustomerException {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        try {
            dbConn = TorqueHelper.getConnection(Rep.sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                Rep.cat.error("Can't get database connection");
                throw new CustomerException();
            }

            Timestamp now = new java.sql.Timestamp(new java.util.Date().getTime());

            pstmt = dbConn.prepareStatement(Rep.sql_bundle.getString("insertBalanceAudit"));
            pstmt.setTimestamp(1, now); //insertDate
            pstmt.setTimestamp(2, now); //date
            pstmt.setString(3, sessionData.getProperty("username")); //Username
            pstmt.setString(4, entityAccountType); //entytyType
            pstmt.setString(5, entityId); //entityId
            pstmt.setString(6, changeType); //changeType
            pstmt.setString(7, oldValue); //old Value
            pstmt.setString(8, newValue); //new Value
            pstmt.executeUpdate();
        } catch (Exception e) {
            Rep.cat.error("Error during log insertion", e);
            throw new CustomerException();
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, null);
            } catch (Exception e) {
                Rep.cat.error("Error during closeConnection", e);
            }
        }
    }
}
