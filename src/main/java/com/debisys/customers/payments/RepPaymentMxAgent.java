/*
 * EMIDA all rights reserved 1999-2019.
 */
package com.debisys.customers.payments;

import org.apache.log4j.Logger;
import com.debisys.customers.Rep;
import com.debisys.utils.DateUtil;
import com.debisys.languages.Languages;
import com.debisys.utils.OperationResult;
import net.emida.supportsite.dto.RepCredit;
import net.emida.supportsite.dao.RepCreditDao;
import net.emida.supportsite.dto.RepCreditPaymentData;

/**
 *
 * @author fernandob
 */
public class RepPaymentMxAgent {

    private static final Logger logger = Logger.getLogger(RepPaymentMxAgent.class);

    public static OperationResult doPayment(Rep rep, RepCredit repCredit,
            RepCreditPaymentData paymentData, int deploymentType, 
            int customConfigType, String language
            ) {
        OperationResult retValue;

        // 1) Apply rep payment
        if ((rep != null) && (rep.getRepId() != null) && (!rep.getRepId().isEmpty())) {

            long repId = Long.valueOf(rep.getRepId());

            long repPaymentId = RepCreditDao.applyRepPayment(repCredit.getRepId(), repCredit.getAmount(), repCredit.getCommission(),
                    repCredit.getDescription(), repCredit.getLogonId(), deploymentType, customConfigType,
                    DateUtil.formatCalendarDate(repCredit.getDepositDate(), null));
            if (repPaymentId != -1) {
                logger.info(String.format("Rep payment added for rep : %d", repId));
                // 2) Get Rep Payment id new
                RepCredit newRepCredit = RepCreditDao.getRepCreditById(repPaymentId);
                // 3) Insert additional payment data
                paymentData.setRepCreditId(newRepCredit.getIdNew());
                if (RepCreditDao.insertRepCreditPaymentData(paymentData)) {
                    logger.info(String.format("Rep credit data added for rep : %d", repId));
                    retValue = new OperationResult();

                    retValue.setError(false);
                    retValue.setResultCode(OperationResult.SUCCESSFUL_ERROR_CODE);
                    retValue.setResultMessage(Languages.getString("jsp.admin.customers.reps.payment.msgs.successfulPayment", language));
                } else {
                    retValue = new OperationResult();
                    retValue.setError(true);
                    retValue.setResultCode("002");
                    retValue.setResultMessage(Languages.getString("jsp.admin.customers.reps.payment.msgs.error.repCreditAdditionalDataNotCreated", language));
                }
            } else {
                retValue = new OperationResult();
                retValue.setError(true);
                retValue.setResultCode("001");
                retValue.setResultMessage(Languages.getString("jsp.admin.customers.reps.payment.msgs.error.repCreditNotCreated", language));
            }
        } else {
            retValue = new OperationResult();
            retValue.setError(true);
            retValue.setResultCode("003");
            retValue.setResultMessage(Languages.getString("jsp.admin.customers.reps.payment.msgs.error.noRepIdFound", language));
        }

        return retValue;
    }

}
