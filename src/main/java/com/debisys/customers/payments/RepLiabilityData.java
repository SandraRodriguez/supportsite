/*
 * EMIDA all rights reserved 1999-2019.
 */
package com.debisys.customers.payments;

import java.math.BigDecimal;

/**
 *
 * @author fernandob
 */
public class RepLiabilityData {
    private long repCreditType;
    private BigDecimal liabilityLimit;
    private BigDecimal runningLiability;

    public long getRepCreditType() {
        return repCreditType;
    }

    public void setRepCreditType(long repCreditType) {
        this.repCreditType = repCreditType;
    }

    public BigDecimal getLiabilityLimit() {
        return liabilityLimit;
    }

    public void setLiabilityLimit(BigDecimal liabilityLimit) {
        this.liabilityLimit = liabilityLimit;
    }

    public BigDecimal getRunningLiability() {
        return runningLiability;
    }

    public void setRunningLiability(BigDecimal runningLiability) {
        this.runningLiability = runningLiability;
    }

    public RepLiabilityData() {
        this.repCreditType = 0L;
        this.liabilityLimit = BigDecimal.ZERO;
        this.runningLiability = BigDecimal.ZERO;
    }

    public RepLiabilityData(long repCreditType, BigDecimal liabilityLimit, BigDecimal runningLiability) {
        this.repCreditType = repCreditType;
        this.liabilityLimit = liabilityLimit;
        this.runningLiability = runningLiability;
    }

    @Override
    public String toString() {
        return "RepLiabilityData{" + "repCreditType=" + repCreditType + ", liabilityLimit=" + liabilityLimit + ", runningLiability=" + runningLiability + '}';
    }
    
    
    
}
