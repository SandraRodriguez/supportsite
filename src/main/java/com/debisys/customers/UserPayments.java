package com.debisys.customers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.dateformat.DateFormat;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.summarys.DownloadsSummary;
import com.debisys.schedulereports.JasperVariables;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;
import com.emida.utils.dbUtils.TorqueHelper;

public class UserPayments
{

  private String start_date = "";
  private String end_date   = "";
  private String strUserIds;
  private String strRepIds;
  private String userId;
  private final String entityType="";
  private String sqlUsersList;
  private String urlLocation;
  
  
  /**
 * @return the sqlUsersList
 */
public String getSqlUsersList()
{
	return sqlUsersList;
}

/**
 * @param sqlUsersList the sqlUsersList to set
 */
public void setSqlUsersList(String sqlUsersList)
{
	this.sqlUsersList = sqlUsersList;
}

public String getStrRepIds()
  {
    return strRepIds;
  }

  public void setStrRepIds(String strRepIds)
  {
    this.strRepIds = strRepIds;
  }

  public void setStrRepIds(String strValue[])
  {
    String strTemp = "";
    if (strValue != null && strValue.length > 0)
    {
      boolean isFirst = true;
      for (int i = 0; i < strValue.length; i++)
      {
        if (strValue[i] != null && !strValue[i].equals(""))
        {
          if (!isFirst)
          {
            strTemp = strTemp + "," + strValue[i];
          }
          else
          {
            strTemp = strValue[i];
            isFirst = false;
          }
        }
      }
    }
    this.strRepIds = strTemp;
  }

  public String getUserId()
  {
    return userId;
  }

  public void setUserId(String userId)
  {
    this.userId = userId;
  }

  public String getStart_date()
  {
    return start_date;
  }

  public void setStart_date(String start_date)
  {
    this.start_date = start_date;
  }

  public String getEnd_date()
  {
    return end_date;
  }

  public void setEnd_date(String end_date)
  {
    this.end_date = end_date;
  }

  public void setStrUserIds(String[] strValue)
  {
    String strTemp = "";
    if (strValue != null && strValue.length > 0)
    {
      boolean isFirst = true;
      for (int i = 0; i < strValue.length; i++)
      {
        if (strValue[i] != null && !strValue[i].equals(""))
        {
          if (!isFirst)
          {
            strTemp = strTemp + ", '" + strValue[i] + "' ";
          }
          else
          {
            strTemp = " '" + strValue[i] + "'  ";
            isFirst = false;
          }
        }
      }
    }
    this.strUserIds = strTemp;
  }

  public void setStrUserIds(String csvUserIds)
  {
    this.strUserIds = csvUserIds;
  }

  public String getStrUserIds()
  {
    return this.strUserIds;
  }

  public void setStrUserNames(String[] strValue)
  {
    String strTemp = "";
    if (strValue != null && strValue.length > 0)
    {
      boolean isFirst = true;
      for (int i = 0; i < strValue.length; i++)
      {
        if (strValue[i] != null && !strValue[i].equals(""))
        {
          if (!isFirst)
          {
            strTemp = strTemp + ", '" + strValue[i] + "' ";
          }
          else
          {
            strTemp = " '" + strValue[i] + "'  ";
            isFirst = false;
          }
        }
      }
    }
    this.strUserIds = strTemp;
  }

  private static Category       cat        = Category.getInstance(UserPayments.class);
  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.customers.sql");

  public Vector getUsersList()
  {

    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Vector vec = new Vector();

    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("Can't get database connection");
        throw new CustomerException();
      }
      pstmt = dbConn.prepareStatement(UserPayments.sql_bundle.getString("getUserList"));
      rs = pstmt.executeQuery();

      while (rs.next())
      {
        Vector vecValues = new Vector();
        vecValues.add(rs.getString(1));
        vecValues.add(rs.getString(2));
        vec.add(vecValues);
      }
    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getUsersList", e);
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during release connection", e);
      }
    }

    return vec;
  }

  public Vector getSummaryReport(SessionData sessionData, ServletContext application, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles)
  {

    Connection dbConn = null;
    PreparedStatement pstmt = null;    
    ResultSet rs = null;
    Vector vecPaymentSummary = new Vector();
    
    String strSQLSummaryFixed = "  SELECT mc.logon_id, COUNT(mc.id) as qty, SUM(payment) as total_payments " +
        " FROM merchant_credits mc WITH(NOLOCK) INNER JOIN merchants m WITH(NOLOCK) on (m.merchant_id = mc.merchant_id) WHERE ";
    
    String strSQLSummaryRelative = " SELECT mc.logon_id, COUNT(mc.id) as qty, SUM(payment) as total_payments " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS +
    	" FROM merchant_credits mc WITH(NOLOCK) INNER JOIN merchants m WITH(NOLOCK) on (m.merchant_id = mc.merchant_id) WHERE ";
        
    
    String strSQLWhere = "";
    String strAccessLevel = sessionData.getProperty("access_level");
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    String strRefId = sessionData.getProperty("ref_id");
    
    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("Can't get database connection");
        throw new ReportException();
      }
      Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"),this.start_date, this.end_date + " 23:59:59.999", false);
      // get a count of total matches
      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
        {
          strSQLWhere += "  m.merchant_id in (select merchant_id from merchants (nolock) where rep_id in (select rep_id from reps (nolock) where iso_id = "
              + strRefId + ")) ";
        }
        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
        {
          strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants WITH(NOLOCK) where rep_id in "
              + "(select rep_id from reps WITH(NOLOCK) where type= " + DebisysConstants.REP_TYPE_REP + " and iso_id in "
              + "(select rep_id from reps WITH(NOLOCK) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in "
              + "(select rep_id from reps WITH(NOLOCK) where type= " + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + ")))) ";
        }
      }
      else if (strAccessLevel.equals(DebisysConstants.AGENT))
      {
        strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants (nolock) where rep_id in "
            + "(select rep_id from reps WITH(NOLOCK) where type= " + DebisysConstants.REP_TYPE_REP + " and iso_id in "
            + "(select rep_id from reps WITH(NOLOCK) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = " + strRefId + "))) ";
      }
      else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
      {
        strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants WITH(NOLOCK) where rep_id in "
            + "(select rep_id from reps WITH(NOLOCK) where type= " + DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")) ";
      }
      else if (strAccessLevel.equals(DebisysConstants.REP))
      {
        strSQLWhere += " m.rep_id = " + strRefId + " ";
      }
      else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
      {
        strSQLWhere += " mc.merchant_id = " + strRefId + " ";
      }

      String rangeDateTimesWhenRelativeHostTrx = "_RangeDateTimesWhenRelativeMerchantCredits_";				
	  HashMap<String,String> hashTokensToReplaceWhenRelativeDates = new HashMap<String,String>();
	  hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "mc.log_datetime");
	  
	  String strWhereFixed = " (mc.log_datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  mc.log_datetime <= '" + vTimeZoneFilterDates.get(1) + "')"; ;
	  
	  String strWhereRelative = " "+rangeDateTimesWhenRelativeHostTrx+" ";
           

      if ( this.strUserIds != null && this.strUserIds.trim().equals("\'ALL\'") )
      {
    	 this.getUserList(sessionData, 1);    	 
         strSQLWhere = strSQLWhere + " AND mc.Logon_id in (" + this.sqlUsersList + ") ";
         strWhereFixed  = strWhereFixed + " AND "+strSQLWhere; 
         
      }
      else if (this.strUserIds != null && !this.strUserIds.equals(""))
      {
          strSQLWhere = strSQLWhere + " AND mc.Logon_id in (" + this.strUserIds + ") ";
          strWhereFixed  = strWhereFixed + " AND "+strSQLWhere;
      }	  
      
      String strGroupOrder = " GROUP BY mc.Logon_id  ORDER BY mc.Logon_id ";
      
      strSQLSummaryFixed = strSQLSummaryFixed + strWhereFixed + strGroupOrder;

      strSQLSummaryRelative = strSQLSummaryRelative + strWhereRelative + " AND "+strSQLWhere + strGroupOrder;
      
      UserPayments.cat.debug( strSQLSummaryFixed );      
      
      if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
      {
    	  UserPayments.cat.debug( strSQLSummaryRelative );    
    	  ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_CRED_PAYMENT_SUMM_BY_USER , 1);
		  scheduleReport.setFixedQuery( strSQLSummaryFixed );	
		  scheduleReport.setRelativeQuery( strSQLSummaryRelative );
		  TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, titles );	
		  scheduleReport.setHastTokensRelativeDates(hashTokensToReplaceWhenRelativeDates);
		  sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);
    	  return null;
      }
      
      int totalQty = 0;
      double totalPayments = 0;
      pstmt = dbConn.prepareStatement( strSQLSummaryFixed );
      rs = pstmt.executeQuery();
      
      if ( scheduleReportType == DebisysConstants.DOWNLOAD_REPORT )
      {
    	  TransactionReport trxReport = new TransactionReport();
    	  String strFileName = trxReport.generateFileName( application );
    	  String totalLabel = Languages.getString("jsp.admin.reports.totals", sessionData.getLanguage() );
    	  DownloadsSummary download = new DownloadsSummary();
    	  this.urlLocation = download.download(rs, sessionData, application, headers, strFileName, titles, totalLabel, 1 );
      }      
      else
      {
	      while (rs.next())
	      {    	
	    	totalQty +=  rs.getInt("qty");
	    	totalPayments += rs.getDouble("total_payments");
	        
	    	Vector vecTemp = new Vector();
	        vecTemp.add(StringUtil.toString(rs.getString("Logon_id")));
	        vecTemp.add(rs.getString("qty"));
	        vecTemp.add(NumberUtil.formatAmount(rs.getString("total_payments")));
	        vecPaymentSummary.add(vecTemp);
	      }
	      
	      if ( totalQty > 0)
	      {
	        Vector vecTemp = new Vector();
	        vecTemp.add( String.valueOf( totalQty ) );
	        vecTemp.add( NumberUtil.formatAmount( Double.toString( totalPayments ) ) );
	        vecPaymentSummary.add(0, vecTemp);
	      }
      }
      
    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getSummaryReport", e);
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during closeConnection", e);
      }
    }
    return vecPaymentSummary;
  }

  public Vector getTotalUserReport()
  {

    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Vector vec = new Vector();

    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("Can't get database connection");
        throw new CustomerException();
      }
      String sql = UserPayments.sql_bundle.getString("getTotalUserReport");
      sql += " where id = " + getUserId() + " ";

      pstmt = dbConn.prepareStatement(sql);
      rs = pstmt.executeQuery();

      while (rs.next())
      {
        Vector vecValues = new Vector();
        vecValues.add(rs.getString(1));
        vecValues.add(rs.getString(2));
        vecValues.add(rs.getString(3));
        vecValues.add(rs.getString(4));
        vecValues.add(rs.getString(5));
        vecValues.add(rs.getString(6));
        vecValues.add(rs.getString(7));
        vecValues.add(rs.getString(8));
        vec.add(vecValues);
      }
    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getUsersList", e);
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during release connection", e);
      }
    }

    return vec;
  }

  /*
  public Vector getUserIsoList(SessionData sessionData) throws ReportException
  {
    Vector vecRepList = new Vector();
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("Can't get database connection");
        throw new ReportException();
      }
      String strSQL = UserPayments.sql_bundle.getString("getUserIsoList");
      String strSQLWhere = "";
      String strAccessLevel = sessionData.getProperty("access_level");
      String strDistChainType = sessionData.getProperty("dist_chain_type");
      String strRefId = sessionData.getProperty("ref_id");
      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
        {
          strSQLWhere = " reps.iso_id = " + strRefId + " and type=" + DebisysConstants.REP_TYPE_REP;
        }
        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
        {
          strSQLWhere = " reps.rep_id IN " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in "
              + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in "
              + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";
        }
      }
      else if (strAccessLevel.equals(DebisysConstants.AGENT))
      {
        strSQLWhere = " reps.rep_id IN " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in "
            + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = " + strRefId + ")) ";
      }
      else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
      {
        strSQLWhere = " reps.rep_id IN " + "(select rep_id from reps (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ")";
      }
      else if (strAccessLevel.equals(DebisysConstants.REP))
      {
        strSQLWhere = " reps.rep_id = " + strRefId;
      }
      else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
      {
        strSQLWhere = " m.merchant_id = " + strRefId;
      }

      strSQLWhere = strSQLWhere + " order by pas.Logon_id";

      pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
      rs = pstmt.executeQuery();
      while (rs.next())
      {
        Vector vecTemp = new Vector();
        vecTemp.add(rs.getString("Password_id"));
        vecTemp.add(rs.getString("Logon_id"));
        vecRepList.add(vecTemp);
      }
    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getUserIsoList", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during closeConnection", e);
      }
    }

    return vecRepList;
  }
  */

  public Vector getUserMerchantCredits(SessionData sessionData) throws CustomerException
  {
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Vector vecMerchantCredits = new Vector();
    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("Can't get database connection");
        throw new CustomerException();
      }
      String strSQLWhere = " WHERE ";

      // get a count of total matches
      
      String strSQL = "select reps.businessname, " + "mc.log_datetime, " + "mc.logon_id, " + "mc.description, " + "mc.payment, " + "mc.balance, "
          + "mc.commission, " + "(mc.payment - (mc.payment * (isnull(mc.commission,0) * .01))) as net_payment "
          + "from merchant_credits mc WITH (NOLOCK) inner join merchants m WITH (NOLOCK) on (mc.merchant_id = m.merchant_id)  inner join reps WITH (NOLOCK) on (m.rep_id = reps.rep_id) ";

      String strAccessLevel = sessionData.getProperty("access_level");
      String strDistChainType = sessionData.getProperty("dist_chain_type");
      String strRefId = sessionData.getProperty("ref_id");
      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
        {
          strSQLWhere += " m.merchant_id in (select merchant_id from merchants WITH (nolock) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id = " + strRefId
              + ")  ) ";
        }
        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
        {
          strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants WITH (nolock) where rep_id in "
              + "(select rep_id from reps WITH (nolock) where type= " + DebisysConstants.REP_TYPE_REP + " and iso_id in "
              + "(select rep_id from reps WITH (nolock) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in "
              + "(select rep_id from reps WITH (nolock) where type= " + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + ")))) ";
        }
      }

      if (strUserIds != null && !strUserIds.equals(""))
      {
        strSQLWhere += " AND mc.logon_id in (" + strUserIds + ") ";
      }

      if (strRepIds != null && !strRepIds.equals(""))
      {
        strSQLWhere += " AND mc.merchant_id in ( " + " select merchant_id " + " from merchants WITH (nolock) " + " where rep_id in ( " + " select rep_id "
            + " from reps WITH (nolock) " + " where type= " + DebisysConstants.REP_TYPE_REP + " and rep_id in (" + strRepIds + ") " + " ) " + " )	";
      }

      if (DateUtil.isValid(start_date) && DateUtil.isValid(end_date))
      {
        strSQLWhere += " and mc.log_datetime >= '" + start_date + "' and mc.log_datetime <= '" + end_date + " " + "23:59:59.999' ";
      }

      String sql = strSQL + strSQLWhere + " order by mc.id DESC";
      UserPayments.cat.debug("sql:" + sql);
      pstmt = dbConn.prepareStatement(sql);
      rs = pstmt.executeQuery();

      while (rs.next())
      {
        Vector vecTemp = new Vector();
        vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("log_datetime")));
        vecTemp.add(StringUtil.toString(rs.getString("logon_id")));
        vecTemp.add(StringUtil.toString(rs.getString("description")));
        vecTemp.add(NumberUtil.formatCurrency(rs.getString("payment")));
        vecTemp.add(NumberUtil.formatCurrency(rs.getString("balance")));
        vecTemp.add(NumberUtil.formatAmount(rs.getString("commission")));
        vecTemp.add(NumberUtil.formatCurrency(rs.getString("net_payment")));
        vecTemp.add(StringUtil.toString(rs.getString("businessname")));
        vecMerchantCredits.add(vecTemp);
      }

    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getMerchantCredits", e);
      throw new CustomerException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during closeConnection", e);
      }
    }
    return vecMerchantCredits;
  }

  public Vector getMerchantsPayments(SessionData sessionData) throws CustomerException
  {
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Vector vecMerchantPayments = new Vector();
    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("can't get database connection");
        throw new CustomerException();
      }

      String strSQL = "SELECT distinct merchants.merchant_id, merchants.dba, Merchant_credits.logon_id,"
          + "Merchant_credits.description, Merchant_credits.payment, " + "Merchant_credits.balance, Merchant_credits.prior_available_credit, "
          + "Merchant_credits.commission, Merchant_credits.PaymentOrigin, "
          + "Merchant_credits.Deposit_date, Merchant_credits.log_datetime FROM merchants (nolock) INNER JOIN terminals (nolock) "
          + "ON merchants.merchant_id = terminals.merchant_id "
          + "INNER JOIN Merchant_credits (nolock) ON merchants.merchant_id = Merchant_credits.merchant_id "
          + "INNER JOIN ISO_RatePlan (nolock) ON terminals.ISO_Rateplan_id = ISO_RatePlan.ISO_RatePlan_id " + " WHERE ISO_RatePlan.ISO_id = ? "
          + " and Merchant_credits.log_datetime >= ? and Merchant_credits.log_datetime <= ? " + "and logon_id not in('commission','tax','billing','Events Job') ";

      if (this.getStrRepIds().length() > 0)
        strSQL += " and Merchant_credits.merchant_id in ( " + this.getStrRepIds() + " ) ";

      

      String strRefId = sessionData.getProperty("ref_id");
      String strStartDate = this.getStart_date();
      String strEndDate = this.getEnd_date();

      UserPayments.cat.debug("sql:" + strSQL);
      pstmt = dbConn.prepareStatement(strSQL);
      pstmt.setDouble(1, Double.parseDouble(strRefId));
      pstmt.setString(2, strStartDate);
      pstmt.setString(3, strEndDate + " 23:59:59.999");

      rs = pstmt.executeQuery();

      while (rs.next())
      {
        Vector vecTemp = new Vector();
        vecTemp.add(0, rs.getString(1)); // merchant_id
        vecTemp.add(1, rs.getString(2)); // dba
        vecTemp.add(2, rs.getString(3)); // logon_id
        vecTemp.add(3, (rs.getString(4) != null) ? rs.getString(4) : Languages.getString("jsp.admin.reports.payments.depositsByMerchant.descriptionNull", sessionData.getLanguage())); // description
        vecTemp.add(4, NumberUtil.formatAmount(rs.getString(5))); // payment
        vecTemp.add(5, NumberUtil.formatAmount(rs.getString(6))); // balance
        vecTemp.add(6, NumberUtil.formatAmount(rs.getString(7))); // prior_available_credit
        vecTemp.add(7, rs.getString(10)); // Deposit_date
        vecTemp.add(8, rs.getString(11)); // log_datetime
        vecMerchantPayments.add(vecTemp);
      }
    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getMerchantsPayments", e);
      throw new CustomerException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during closeConnection", e);
      }

    }
    return vecMerchantPayments;
  }

  public Vector getMerchantsPaymentsDownload(SessionData sessionData) throws CustomerException
  {
    Vector vecResultsDownload = new Vector();

    Vector vecSearchResults = getMerchantsPayments(sessionData);
    Iterator it = vecSearchResults.iterator();

    if (it.hasNext())
    {
      Vector vecTemp = new Vector();
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.merchant_id", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.dba", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.balance", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.Deposit_date", sessionData.getLanguage()));
      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.log_datetime", sessionData.getLanguage()));
      vecResultsDownload.add(vecTemp);
      while (it.hasNext())
      {
        vecTemp = new Vector();
        vecTemp = (Vector) it.next();
        vecTemp.set(4, NumberUtil.formatCurrency(vecTemp.get(4).toString()));
        vecTemp.set(5, NumberUtil.formatCurrency(vecTemp.get(5).toString()));
        vecTemp.set(6, NumberUtil.formatCurrency(vecTemp.get(6).toString()));
        vecResultsDownload.add(vecTemp);
      }
    }

    return vecResultsDownload;
  }

  public Vector getUserRepsMerchantCredits(SessionData sessionData,ServletContext context) throws CustomerException
  {
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Vector<Vector<String>> vecMerchantCredits = new Vector<Vector<String>>();
    try
    {      
      
		String dataBase = DebisysConfigListener.getDataBaseDefaultForReport(context);
		String reportId = sessionData.getProperty("reportId");
		int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
		
		if(dataBase.equals(DebisysConstants.DATA_BASE_MASTER)){
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
		}
		else{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
		}
      
      if (dbConn == null)
      {
        UserPayments.cat.error("Can't get database connection");
        throw new CustomerException();
      }
      String strSQLWhere = " WHERE ";
      String strSQLWhere2 = " WHERE ";

      // get a count of total matches
      

      String strSQL = "SELECT     reps.businessname AS businessname," + "mc.datetime AS datetime," + "mc.logon_id AS logon_id, "
          + "mc.description AS description," + "mc.amount AS payment, " + "mc.available_credit AS balance," + "0 AS commission, " + "mc.amount AS net_payment "
          + "FROM rep_merchant_credits AS mc " + "INNER JOIN merchants AS m ON mc.merchant_id = m.merchant_id " + "INNER JOIN reps ON m.rep_id = reps.rep_id ";

      String strSQL2 = "SELECT r.businessname AS businessname," + "rc.datetime AS datetime," + "rc.logon_id AS logon_id," + "rc.description AS description,"
          + "rc.amount AS payment," + "rc.available_credit AS balance," + "rc.commission AS commission,"
          + "(rc.amount - (rc.amount * (isnull(rc.commission,0) * .01))) AS net_payment " + "FROM rep_credits rc (nolock) "
          + "INNER JOIN reps r on (rc.rep_id = r.rep_id) ";

      String strAccessLevel = sessionData.getProperty("access_level");
      String strDistChainType = sessionData.getProperty("dist_chain_type");
      String strRefId = sessionData.getProperty("ref_id");

      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
        {
          strSQLWhere += " m.merchant_id IN (SELECT merchant_id FROM merchants (nolock) WHERE rep_id IN (SELECT rep_id FROM reps WHERE iso_id = " + strRefId
              + ")  ) ";
          strSQLWhere2 += " rc.rep_id IN (SELECT rep_id FROM reps AS reps_2 WHERE (iso_id =" + strRefId + ")  ) ";

        }
        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
        {
          String strTheChain = "(SELECT rep_id FROM reps (nolock) WHERE type= " + DebisysConstants.REP_TYPE_REP + " and iso_id IN "
              + "(SELECT rep_id FROM reps (nolock) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id IN "
              + "(SELECT rep_id FROM reps (nolock) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))) ";

          strSQLWhere += " m.merchant_id IN (SELECT merchant_id FROM merchants (nolock) WHERE rep_id IN " + strTheChain + ")";
          strSQLWhere2 += " rc.rep_id IN " + strTheChain ;
        }
      }

      if (strUserIds != null && !strUserIds.equals(""))
      {
        strSQLWhere += " AND mc.logon_id IN (" + strUserIds + ") ";
        strSQLWhere2 += " AND rc.logon_id IN (" + strUserIds + ") ";
      }

      if (strRepIds != null && !strRepIds.equals(""))
      {
        strSQLWhere += " AND mc.merchant_id IN ( " + " SELECT merchant_id " + " FROM merchants (nolock) " + " WHERE rep_id IN ( " + " SELECT rep_id "
            + " FROM reps (nolock) " + " WHERE type= " + DebisysConstants.REP_TYPE_REP + " and rep_id IN (" + strRepIds + ") " + " ) " + " )	";
        strSQLWhere2 += " AND rc.rep_id IN(" + strRepIds + ") ";
      }
      
      int numDays = DateUtil.getDaysBetween(this.start_date, this.end_date);
      if(numDays > limitDays){
    	  this.end_date = DateUtil.addSubtractDays(start_date,limitDays);
      }

      if (DateUtil.isValid(start_date) && DateUtil.isValid(end_date))
      {
        strSQLWhere += " and mc.datetime >= '" + start_date + "' and mc.datetime <= '" + end_date + " " + "23:59:59.999' ";
        strSQLWhere2 += " and rc.datetime >= '" + start_date + "' and rc.datetime <= '" + end_date + " " + "23:59:59.999' ";
      }

      String sql = "(" + strSQL + strSQLWhere + ")UNION ALL(" + strSQL2 + strSQLWhere2 + ")" + " ORDER BY datetime DESC";
      UserPayments.cat.debug("sql:" + sql);
      pstmt = dbConn.prepareStatement(sql);
      rs = pstmt.executeQuery();

      while (rs.next())
      {
        Vector<String> vecTemp = new Vector<String>();
        vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
        vecTemp.add(StringUtil.toString(rs.getString("logon_id")));
        vecTemp.add(StringUtil.toString(rs.getString("description")));
        vecTemp.add(NumberUtil.formatCurrency(rs.getString("payment")));
        vecTemp.add(NumberUtil.formatCurrency(rs.getString("balance")));
        vecTemp.add(NumberUtil.formatAmount(rs.getString("commission")));
        vecTemp.add(NumberUtil.formatCurrency(rs.getString("net_payment")));
        vecTemp.add(StringUtil.toString(rs.getString("businessname")));
        vecMerchantCredits.add(vecTemp);
      }

    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getUserRepsMerchantCredits", e);
      throw new CustomerException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during closeConnection", e);
      }
    }
    return vecMerchantCredits;
  }

  public Vector<String> getUserList(SessionData sessionData, int flagSQLQueryUserList) throws ReportException
  {
    Vector<String> vecRepList = new Vector<String>();
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("Can't get database connection");
        throw new ReportException();
      }
      String strSQLWhere = "";
      String strAccessLevel = sessionData.getProperty("access_level");
      String strDistChainType = sessionData.getProperty("dist_chain_type");
      String strRefId = sessionData.getProperty("ref_id");
      String strCurrentUsername = sessionData.getProperty("username");
      
      String strSQL = "";

      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
        {
          strSQL = "select distinct password.logon_id " + "from password inner join merchants m " + "on ref_id =m.rep_id " + "where m.rep_id in ( "
              + "select rep_id from reps where iso_id in " + "( select rep_id from reps where iso_id in ( " + "select  rep_id from reps where iso_id = "
              + strRefId + " and iso_id <> rep_id))) " + "union  " + "select distinct password.logon_id " + "from password  inner join  reps r3  "
              + "on ref_id = r3.rep_id " + "where  iso_id = " + strRefId + " and iso_id <> rep_id " + "union  " + "select distinct password.logon_id "
              + "from password  inner join  reps r2 " + "on  r2.rep_id =ref_id " + "where iso_id = " + strRefId + " and iso_id= r2.rep_id ";

        }
        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
        {
          strSQL = "SELECT DISTINCT pw.logon_id  " 
        	  + " FROM password pw WITH (NOLOCK) INNER JOIN reps r WITH (NOLOCK) ON pw.ref_id=r.rep_id"
        	  + " WHERE r.type=1 AND r.iso_id IN"
        	  + " (SELECT rep_id FROM reps r WITH (NOLOCK) WHERE type=5 AND iso_id IN "
        	  + " (SELECT rep_id FROM reps r WITH (NOLOCK) WHERE type=4 AND iso_id IN "
         	  + " (SELECT rep_id FROM reps r WITH (NOLOCK) WHERE type=3 AND rep_id=" + strRefId + ")))"
        	  + " UNION " 
			  + " SELECT DISTINCT pw.logon_id "
              + " FROM password pw WITH (NOLOCK) INNER JOIN reps s ON s.rep_id=pw.ref_id " 
              + " WHERE s.type=5 AND s.iso_id IN"
        	  + " (SELECT rep_id FROM reps r WITH (NOLOCK) WHERE type=4 AND iso_id IN "
         	  + " (SELECT rep_id FROM reps r WITH (NOLOCK) WHERE type=3 AND rep_id=" + strRefId + "))"
        	  + " UNION "
        	  + " SELECT DISTINCT pw.logon_id "
        	  + " FROM password pw WITH (NOLOCK) INNER JOIN reps a WITH (NOLOCK) ON pw.ref_id=a.rep_id "
			  + " WHERE a.type=4 AND a.iso_id IN "
			  + " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=3 AND rep_id=" + strRefId + ") " 			  
              + " UNION "
              + " SELECT DISTINCT pw.logon_id " 
              + " FROM password pw WITH (NOLOCK) INNER JOIN reps i ON i.rep_id=pw.ref_id " 
              + " WHERE i.type=3 AND i.rep_id=" + strRefId
                  
                  + " UNION " 
                  + "SELECT DISTINCT mc.logon_id " 
                  + "FROM   password pw WITH (nolock) " 
                  + "INNER JOIN reps r WITH (nolock) " 
                  + "ON pw.ref_id IN (select DISTINCT rec_id from merchants where merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed')) " 
                  + "INNER JOIN merchant_credits mc ON mc.merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed') "
                  + "AND mc.description like '%PAY-%'" 
                  + "WHERE  r.type = 1 "
                  + "and r.rep_id in (select DISTINCT rep_id from merchants where merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed'))"
                  + "AND r.iso_id IN (SELECT rep_id " 
                  + "FROM   reps r WITH (nolock) WHERE  type = 5 AND iso_id IN (SELECT rep_id " 
                  + "FROM   reps r WITH (nolock) " 
                  + "WHERE  type = 4 " 
                  + "AND iso_id IN (SELECT rep_id FROM reps r WITH (nolock) " 
                  + "WHERE type = 3  " 
                  + "AND rep_id = " + strRefId + ")))";
        }
      }
      else if (strAccessLevel.equals(DebisysConstants.REP))
      {
        strSQL = "select distinct password.logon_id " + "from password  inner join  reps r3  " + "on ref_id = r3.rep_id " + "where iso_id in  " + "( "
            + strRefId + ") "
                + "UNION "
                + "SELECT DISTINCT mc.logon_id FROM   password pw WITH (nolock)"
                + "INNER JOIN merchant_credits mc ON mc.merchant_id IN (select DISTINCT merchant_id from merchants where rep_id = " + strRefId + " "
                + "AND merchant_id in ( SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed') AND mc.description like '%PAY-%')";
      }
      else if (strAccessLevel.equals(DebisysConstants.AGENT))
      {
        strSQL = "select distinct password.logon_id " + "from password inner join merchants m " + "on ref_id =m.rep_id " + "where m.rep_id in ( "
            + "		select rep_id from reps where iso_id in " + "		( select rep_id from reps where iso_id in ( " + "select  rep_id from reps where iso_id = "
            + strRefId
            + " and iso_id <> rep_id))) "
            + "union  "
            + "select distinct password.logon_id "
            + "from password  inner join  reps r3  "
            + "on ref_id = r3.rep_id "
            + "	where iso_id in  "
            + "	( select rep_id from reps where iso_id in ( "
            + "			select  rep_id from reps where iso_id = "
            + strRefId
            + " and iso_id <> rep_id)) "
            + "union  "
            + "select distinct password.logon_id "
            + "	from password  inner join  reps r1 "
            + "	on ref_id = r1.rep_id where iso_id in ( "
            + "			select  rep_id from reps where iso_id = "
            + strRefId
            + " and iso_id <> rep_id) "
            + "union  "
            + "	select distinct password.logon_id "
            + "	from password  inner join  reps r2 "
            + "  on  r2.rep_id =ref_id "
            + "where iso_id = "
            + strRefId
            + " and iso_id <> r2.rep_id "
            + "UNION SELECT DISTINCT mc.logon_id FROM password pw WITH (nolock) INNER JOIN reps r WITH (nolock) ON pw.ref_id "
                + "IN (select DISTINCT rec_id from merchants where merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) "
                + "WHERE  state = 'completed')) INNER JOIN merchant_credits mc ON mc.merchant_id IN (SELECT DISTINCT merchantId "
                + "FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed') AND mc.description like '%PAY-%'"
                + "WHERE  r.type = 1 "
                + "and r.rep_id in (select DISTINCT rep_id from merchants where merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed'))"
                + "AND r.iso_id IN (SELECT rep_id FROM   reps r WITH (nolock) "
                + "WHERE  type = 5 AND iso_id IN (SELECT rep_id FROM   reps r WITH (nolock) "
                + "WHERE  type = 4 AND rep_id = " + strRefId + "))";
      }
      else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
      {
        strSQL = "select distinct password.logon_id " + "from password inner join merchants m " + "on ref_id =m.rep_id " + "where m.rep_id in  "
            + "		( select rep_id from reps where iso_id in ( " + "		select rep_id from reps where iso_id in " + "(select  rep_id from reps where iso_id = "
            + strRefId + " and iso_id <> rep_id))) " + "union  " + "select distinct password.logon_id " + "from password  inner join  reps r3  "
            + "on ref_id = r3.rep_id " + "	where iso_id in  " + "	( select rep_id from reps where iso_id in ( " + "			select  rep_id from reps where iso_id = "
            + strRefId + " and iso_id <> rep_id)) " + "union  " + "	select distinct password.logon_id " + "	from password  inner join  reps r2 "
            + "  on  r2.rep_id =ref_id " + "where iso_id = " + strRefId + " and iso_id <> r2.rep_id "
            + "UNION SELECT DISTINCT mc.logon_id FROM   password pw WITH (nolock) INNER JOIN reps r WITH (nolock) ON pw.ref_id IN (select DISTINCT rec_id "
                + "from merchants where merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed')) "
                + "INNER JOIN merchant_credits mc ON mc.merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed')   "
                + "AND mc.description like '%PAY-%' WHERE  r.type = 1 "
                + "and r.rep_id in (select DISTINCT rep_id from merchants where merchant_id IN (SELECT DISTINCT merchantId FROM   paymentprocessortransactions WITH (nolock) WHERE  state = 'completed'))"                
                + "AND r.iso_id IN (SELECT rep_id FROM   reps r WITH (nolock) WHERE  type = 5 AND r.rep_id = " + strRefId + ")";
      }

      //strSQL += " union select ('Commission')  union select ('Billing') union select ('Tax') UNION SELECT \'" + strCurrentUsername + "\'";
      strSQL += " UNION (SELECT su.UserCode FROM SpecialUsers su WHERE su.ShowInCombos=1) UNION SELECT \'" + strCurrentUsername + "\'";
      if ( flagSQLQueryUserList == 1 )
      {
    	  //THIS FLAG IS INTENDED TO HAVE ONE SQL QUERY TO FIND THE USERS LOGINS
    	  //IN THE SCHEDULE REPORTS FEATURES
    	  sqlUsersList = strSQL;
    	  return null;
      }
      
      strSQLWhere = " order by Logon_id";

      pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
      
      UserPayments.cat.info("[getUserList] SQL: " + strSQL + strSQLWhere);
      
      rs = pstmt.executeQuery();
      while (rs.next())
      {
        vecRepList.add(rs.getString("Logon_id"));
      }
    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getUserIsoList", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during closeConnection", e);
      }
    }

    return vecRepList;
  }

  private String getDateFormatted(String dateToFormat)
  {
    if (DateUtil.isValid(dateToFormat))
    {
      String date = null;
      try
      {
        // Current support site date is in MM/dd/YYYY.
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        java.util.Date dd = sdf.parse(dateToFormat);

        // We will specify a new date format through the database
        SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
        date = sdf1.format(dd);

        // Since Java SimpleDateFormat does not know whether a datetime contains
        // a time or not, remove the time (usually 12:00:00 AM) from the end of the date
        // ranges
        date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
      }
      catch (ParseException e)
      {
        UserPayments.cat.error("Error during TransactionReport date localization ", e);
      }
      return date;
    }
    else
    {
      // Something isn't valid in the date string - don't try to localize it
      return StringUtil.toString(dateToFormat);
    }
  }

  public String getStartDateFormatted()
  {
    return getDateFormatted(this.start_date);
  }

  public String getEndDateFormatted()
  {
    return getDateFormatted(this.end_date);
  }
  
 
// DBSYS- 746 Nidhi Gulati
  public Vector getPayments(SessionData sessionData,ServletContext context, String entityType, String itemSelected, String[] strMerchantIds,
		  					int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws CustomerException
  {
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Vector vecPayments = new Vector();
    String strAccessLevel = sessionData.getProperty("access_level");
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    String strRefId = sessionData.getProperty("ref_id");
    boolean bExternalRep=com.debisys.users.User.isExternalRepAllowedEnabled(sessionData);
    String extraFilter = getPermissionFaceyJAMGrpPaymentDetailsReport(sessionData);
    try
    {
      dbConn = Torque.getConnection(UserPayments.sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        UserPayments.cat.error("can't get database connection");
        throw new CustomerException();
      }
     
      String strSQL = "";
      String strSQLFixedQueryWhere = "";
      String rangeDateTimesWhenRelativeHostTrx = "_RangeDateTimesWhenRelativeCreditsTrx_";
	  HashMap<String,String> hashTokensToReplaceWhenRelativeDates = new HashMap<String,String>();
	  
	  
	  String descrWhenNull = Languages.getString("jsp.admin.reports.payments.depositsByMerchant.descriptionNull", sessionData.getLanguage());
	  
      StringBuilder strSQLAgents = new StringBuilder();
      
      //getAgentPayment
      strSQLAgents.append(" SELECT distinct dbo.reps.rep_id, dbo.reps.businessname, Agent_credits.logon_id, ISNULL(Agent_credits.description,'"+descrWhenNull+"') description, Agent_credits.amount, ");
      strSQLAgents.append(" Agent_credits.prior_available_credit,DATEPARAMS  ");
      if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
	  {
    	  strSQLAgents.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
	  }
      strSQLAgents.append(" FROM dbo.reps WITH (NOLOCK), dbo.Agent_credits WITH (NOLOCK) ");
      strSQLAgents.append(" WHERE  ");
      strSQLAgents.append("   reps.iso_id in (select rep_id from reps WITH (nolock) where type=3 and iso_id ="+strRefId+")  ");
      strSQLAgents.append("   and dbo.reps.rep_id=dbo.agent_credits.rep_id and ");
      
      
      
      //getSubAgentPayment
      StringBuilder strSQLSubAgents = new StringBuilder();
      strSQLSubAgents.append(" SELECT distinct s.rep_id, s.businessname, sc.logon_id, ISNULL(sc.description,'"+descrWhenNull+"' ) description, sc.amount, sc.prior_available_credit, DATEPARAMS  PARAMETERS   ");
      if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
	  {
    	  strSQLSubAgents.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
	  }
      strSQLSubAgents.append(" FROM SubAgent_credits sc WITH (NOLOCK)  ");
      
      
      //getRepPayment
      StringBuilder strSQLReps = new StringBuilder();
      strSQLReps.append(" SELECT distinct r.rep_id, r.businessname, rc.logon_id, ISNULL(rc.description,'"+descrWhenNull+"') description, rc.amount, rc.prior_available_credit, DATEPARAMS  PARAMETERS  ");
      if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
	  {
    	  strSQLReps.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
	  }
      strSQLReps.append(" FROM rep_credits rc WITH (NOLOCK)  ");      
      
      //getMerchantPayment
      StringBuilder strSQLMerchants = new StringBuilder();
      strSQLMerchants.append(" SELECT distinct m.merchant_id, m.dba, mc.logon_id, ISNULL(mc.description,'"+descrWhenNull+"') description, mc.payment, mc.balance, mc.prior_available_credit, mc.commission, mc.PaymentOrigin, "); 
      strSQLMerchants.append(" DATEPARAMS1,DATEPARAMS2, ExternalDBA = CASE ISNULL(mc.ExternalDBA,'1') WHEN '1' then 'Assigned' else 'External' end PARAMETERS ");
      if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
	  {
    	  strSQLMerchants.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
	  }
      strSQLMerchants.append(" FROM merchant_credits mc WITH (NOLOCK) INNER JOIN merchants m WITH (NOLOCK) ON m.merchant_id =  mc.merchant_id  ");
      	  
      String strStartDate = this.getStart_date();
      String strEndDate = this.getEnd_date();
      String sqlWhere= "";
      Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"),strStartDate, strEndDate+" 23:59:59.999", false);
      
      
      if(entityType.equalsIgnoreCase("Agent"))
      {
    	   if (strAccessLevel.equals(DebisysConstants.ISO))
 	       {
 	    	   if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		       {
		           strSQL = strSQLAgents.toString();
		           strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", Agent_credits.datetime, 1) as datetime");
		           strSQLFixedQueryWhere = " Agent_credits.datetime >= '"+vTimeZoneFilterDates.get(0).toString()+"' and Agent_credits.datetime <= '"+vTimeZoneFilterDates.get(1).toString()+"'  ";
		           if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
		     	   {
		         	 hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "Agent_credits.datetime");
		         	 strSQL = strSQL + " "+ rangeDateTimesWhenRelativeHostTrx;
		     	   }
		           else
		           {		        	   
		        	   strSQL = strSQL + strSQLFixedQueryWhere;
		           }      
		           strSQL = strSQL + " and Agent_credits.logon_id not like 'commission%'  ";
		           strSQL = strSQL + " and Agent_credits.logon_id not like 'billing%'  ";
		           strSQL = strSQL + " and Agent_credits.logon_id not like 'Events Job' ";
		           if(itemSelected != null && !itemSelected.equals("0"))
		           {
		        	   strSQL += " and reps.rep_id ="+itemSelected;
		           }		           
		       }
 	       }
      }
      else if(entityType.equalsIgnoreCase("SubAgent"))
      {
		  strSQL = strSQLSubAgents.toString();
		  if(!extraFilter.equals("1")){
			  strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", sc.datetime, 1) as datetime");
			  strSQL = strSQL.replace("PARAMETERS", "");
		  }
		  if (strAccessLevel.equals(DebisysConstants.ISO)){
			  strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", sc.datetime, 1) as datetime");
			  strSQL = strSQL.replace("PARAMETERS", ", a.businessname as agentName");
    		  strSQL += " INNER JOIN reps s WITH (nolock) ON s.rep_id = sc.subagent_id" +
    		  		    " INNER JOIN reps a WITH (nolock) ON a.rep_id = s.iso_id AND a.type=4 "+
    			  		" INNER JOIN reps i WITH (nolock) ON i.rep_id = a.iso_id AND i.type = 3 AND i.iso_id="+strRefId;
 	      }
		  else if (strAccessLevel.equals(DebisysConstants.AGENT)){
			  strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", sc.datetime, 1) as datetime");
			  strSQL = strSQL.replace("PARAMETERS", ", a.businessname as agentName");
			  strSQL += " INNER JOIN reps s WITH (nolock) ON s.rep_id = sc.subagent_id " +
			  		    " INNER JOIN reps a WITH (nolock) ON a.rep_id = s.iso_id AND a.rep_id="+strRefId ;
		  }
		  strSQL += " WHERE ";
		  strSQLFixedQueryWhere = " sc.datetime >= '"+vTimeZoneFilterDates.get(0).toString()+"' and sc.datetime <= '"+vTimeZoneFilterDates.get(1).toString()+"' ";
		  if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
		  {
	    	  hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "sc.datetime");
	    	  strSQL = strSQL + rangeDateTimesWhenRelativeHostTrx ;
		  }
	      else
	      {	    	  
	    	  strSQL= strSQL+ strSQLFixedQueryWhere;	    	  
	      }		
		  strSQL = strSQL +" and sc.logon_id not like 'commission%' and sc.logon_id not like 'billing%' and sc.logon_id not like 'Events Job' ";
		  
		  if(itemSelected != null && !itemSelected.equals("0")){
       	   strSQL += " and s.rep_id = "+itemSelected;
          }		  
		   
      }
      else if(entityType.equalsIgnoreCase("Rep"))
      {
		  strSQL = strSQLReps.toString();
		  if(!extraFilter.equals("1")){
			  strSQL = strSQL.replace("DATEPARAMS","dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", rc.datetime, 1) as datetime");
			  strSQL = strSQL.replace("PARAMETERS", "");
		  }
		  
	      if (strAccessLevel.equals(DebisysConstants.ISO)){
	    		 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
	    			 strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", rc.datetime, 1) as datetime");
	    			 strSQL = strSQL.replace("PARAMETERS", ""); 
	    			 strSQL += " INNER JOIN reps r WITH (nolock) ON r.rep_id = rc.rep_id AND r.iso_id = "+strRefId;
			     }
	    		 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			     {
	    			 strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", rc.datetime, 1) as datetime");
	    			 strSQL = strSQL.replace("PARAMETERS", ", s.businessname as subAgentName, a.businessname as agentName ");
	    			 strSQL += " INNER JOIN reps r WITH (nolock) ON r.rep_id = rc.rep_id " +
	    			 		   " INNER JOIN reps s WITH (nolock) ON s.rep_id = r.iso_id AND s.type= "+DebisysConstants.REP_TYPE_SUBAGENT +" "+
	    			 	       " INNER JOIN reps a WITH (nolock) ON a.rep_id = s.iso_id AND a.type = " + DebisysConstants.REP_TYPE_AGENT + " AND a.iso_id = "+strRefId;
			     }
		   }
	       else if(strAccessLevel.equals(DebisysConstants.AGENT))
    	   {
	    	   strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", rc.datetime, 1) as datetime");
	    	   strSQL = strSQL.replace("PARAMETERS", ", s.businessname as subAgentName, a.businessname as agentName ");
	    	   strSQL += " INNER JOIN reps r WITH (nolock) ON r.rep_id = rc.rep_id " +
	    	   		     " INNER JOIN reps s WITH (nolock) ON s.rep_id = r.iso_id AND s.type= "+DebisysConstants.REP_TYPE_SUBAGENT +" AND s.iso_id= "+strRefId +
	    	             " INNER JOIN reps a WITH (nolock) ON a.rep_id = s.iso_id ";
    	   }
	       else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
	       {
	    	   strSQL = strSQL.replace("DATEPARAMS", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", rc.datetime, 1) as datetime");
	    	   strSQL = strSQL.replace("PARAMETERS", ", s.businessname as subAgentName, a.businessname as agentName ");
	    	   strSQL += " INNER JOIN reps r WITH (nolock) ON r.rep_id = rc.rep_id AND  r.iso_id = " + strRefId +
	    	   		     " INNER JOIN reps s WITH (nolock) ON s.rep_id = r.iso_id "+ 
		 	             " INNER JOIN reps a WITH (nolock) ON a.rep_id = s.iso_id "; 
	       } 
	      
	      strSQLFixedQueryWhere = " rc.datetime >= '"+vTimeZoneFilterDates.get(0).toString()+"' and rc.datetime <= '"+vTimeZoneFilterDates.get(1).toString()+"' ";
	      if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
		  {
	    	  hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "rc.datetime");
	    	  strSQL = strSQL + " WHERE "+rangeDateTimesWhenRelativeHostTrx ;
		  }
	      else
	      {	    	   
	    	  strSQL = strSQL + " WHERE "+strSQLFixedQueryWhere;	    	  
	      }	
	      strSQL = strSQL + " and logon_id not like 'commission%' and logon_id not like 'billing%' and logon_id not like 'Events Job' ";
	      
	      if(itemSelected != null && !itemSelected.equals("0")){
	       	   strSQL += " and r.rep_id = "+itemSelected;
	          }
	      
	   }
       else if(entityType.equalsIgnoreCase("Merchant"))
       {
		   strSQL = strSQLMerchants.toString();    		   
		   if(!extraFilter.equals("1")){
			   strSQL = strSQL.replace("DATEPARAMS1","dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.Deposit_date, 1) as Deposit_date");
			   strSQL = strSQL.replace("DATEPARAMS2", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.log_datetime, 1) as log_datetime");
				  strSQL = strSQL.replace("PARAMETERS", "");
		   }
		   if (strAccessLevel.equals(DebisysConstants.ISO))
	 	   {
    		 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
    			 strSQL = strSQL.replace("DATEPARAMS1","dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.Deposit_date, 1) as Deposit_date");
    			 strSQL = strSQL.replace("DATEPARAMS2", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.log_datetime, 1) as log_datetime");
    			 strSQL = strSQL.replace("PARAMETERS", ", r.businessname AS repName "); 
    			 strSQL += " INNER JOIN reps r WITH (NOLOCK) ON r.rep_id= m.rep_id AND r.iso_id =  "+strRefId;
    		 }
    		 else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
    		 {
    			 strSQL = strSQL.replace("DATEPARAMS1","dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.Deposit_date, 1) as Deposit_date");
    			   strSQL = strSQL.replace("DATEPARAMS2", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.log_datetime, 1) as log_datetime");
    			 strSQL = strSQL.replace("PARAMETERS", ", r.businessname as repName, s.businessname as subAgentName, a.businessname as agentName ");
    			 strSQL += " INNER JOIN reps r WITH (NOLOCK) ON r.rep_id= m.rep_id AND r.type = "+ DebisysConstants.REP_TYPE_REP +
    			 " INNER JOIN reps s WITH (NOLOCK) ON s.rep_id= r.iso_id AND s.type = " + DebisysConstants.REP_TYPE_SUBAGENT + 
    			 " INNER JOIN reps a WITH (NOLOCK) ON a.rep_id= s.iso_id AND a.type = " + DebisysConstants.REP_TYPE_AGENT +" AND a.iso_id="+strRefId;
    			 
    		 }
	 	   }
		   else if(strAccessLevel.equals(DebisysConstants.AGENT)){
			   strSQL = strSQL.replace("DATEPARAMS1","dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.Deposit_date, 1) as Deposit_date");
			   strSQL = strSQL.replace("DATEPARAMS2", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.log_datetime, 1) as log_datetime");
			   strSQL = strSQL.replace("PARAMETERS", ", r.businessname as repName, s.businessname as subAgentName, a.businessname as agentName ");
			   strSQL += " INNER JOIN reps r WITH (NOLOCK) ON r.rep_id= m.rep_id AND r.type = "+ DebisysConstants.REP_TYPE_REP +
    			         " INNER JOIN reps s WITH (NOLOCK) ON s.rep_id= r.iso_id AND s.type = " + DebisysConstants.REP_TYPE_SUBAGENT +" AND s.iso_id= "+ strRefId + 
    			         " INNER JOIN reps a WITH (NOLOCK) ON a.rep_id= s.iso_id ";
		   }
		   else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
			   strSQL = strSQL.replace("DATEPARAMS1","dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.Deposit_date, 1) as Deposit_date");
			   strSQL = strSQL.replace("DATEPARAMS2", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.log_datetime, 1) as log_datetime");
			   strSQL = strSQL.replace("PARAMETERS", ", r.businessname as repName, s.businessname as subAgentName, a.businessname as agentName ");
			   strSQL += " INNER JOIN reps r WITH (NOLOCK) ON r.rep_id= m.rep_id AND r.type = "+ DebisysConstants.REP_TYPE_REP +" AND r.iso_id=  "+ strRefId + 
    			         " INNER JOIN reps s WITH (NOLOCK) ON s.rep_id= r.iso_id "+ 
    			         " INNER JOIN reps a WITH (NOLOCK) ON a.rep_id= s.iso_id ";
		   }
		   else if (strAccessLevel.equals(DebisysConstants.REP)){
			   strSQL = strSQL.replace("DATEPARAMS1","dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.Deposit_date, 1) as Deposit_date");
			   strSQL = strSQL.replace("DATEPARAMS2", "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", mc.log_datetime, 1) as log_datetime");
			   strSQL = strSQL.replace("PARAMETERS", ", r.businessname as repName, s.businessname as subAgentName, a.businessname as agentName ");
			   strSQL += " INNER JOIN reps r WITH (NOLOCK) ON r.rep_id= m.rep_id AND r.rep_id= "+ strRefId + 
    			         " INNER JOIN reps s WITH (NOLOCK) ON s.rep_id= r.iso_id "+ 
    			         " INNER JOIN reps a WITH (NOLOCK) ON a.rep_id= s.iso_id ";
		   }
		   strSQL = strSQL + " WHERE mc.logon_id not in('Commission','Tax','Billing','Events Job') AND ";	
		   
		   strSQLFixedQueryWhere = " mc.log_datetime >= '"+vTimeZoneFilterDates.get(0).toString()+"' and mc.log_datetime <= '"+vTimeZoneFilterDates.get(1).toString()+"' ";
		   if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
		   {
		   	  hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "mc.log_datetime");
		   	  strSQL = strSQL + " "+rangeDateTimesWhenRelativeHostTrx ;
		   }
		   else
		   {			   
		       strSQL = strSQL + strSQLFixedQueryWhere;	    	  
		   }			   
		   	   
		   if(strMerchantIds != null)
		   {
			   strSQL += " AND " + getListToStr_SQL_IN("m.merchant_id", strMerchantIds);
	       }		   
    	}
      	      	
        if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
	    {
        	String fixedQuery = strSQL.replaceAll( rangeDateTimesWhenRelativeHostTrx , strSQLFixedQueryWhere ).replaceFirst("\\?", strRefId);
        	fixedQuery = fixedQuery.replaceFirst("\\?", "'"+ vTimeZoneFilterDates.get(0).toString() +"'");
        	fixedQuery = fixedQuery.replaceFirst("\\?", "'"+ vTimeZoneFilterDates.get(1).toString() +"'");
        	
        	ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_TRX_PAYMENT_DETAIL_REPORT , 1);
        	strSQL = strSQL.replaceFirst("\\?", strRefId);
        	
        	scheduleReport.setRelativeQuery( strSQL );			
			scheduleReport.setFixedQuery( fixedQuery );
			TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, titles );
			scheduleReport.setHastTokensRelativeDates(hashTokensToReplaceWhenRelativeDates);
			sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);
			return null;
	    }        
        
	    UserPayments.cat.debug("sql:" + strSQL+" /*"+strRefId+" */ strAccessLevel:"+strAccessLevel+"  entityType: "+entityType);
      	pstmt = dbConn.prepareStatement(strSQL);
        
        rs = pstmt.executeQuery();
        while (rs.next())
        {
        Vector vecTemp = new Vector();
        vecTemp.add(0, rs.getString(1)); // rep_id
        vecTemp.add(1, rs.getString(2)); // dba
        vecTemp.add(2, rs.getString(3)); // logon_id
        vecTemp.add(3, (rs.getString(4) != null) ? rs.getString(4) : descrWhenNull ); // description
        if(entityType.equalsIgnoreCase("Merchant"))
        {
        	vecTemp.add(4, NumberUtil.formatAmount(rs.getString(5))); // payment
            vecTemp.add(5, NumberUtil.formatAmount(rs.getString(6))); // balance
            vecTemp.add(6, NumberUtil.formatAmount(rs.getString(7))); // prior_available_credit
            vecTemp.add(7, rs.getString(10)); // Deposit_date
            vecTemp.add(8, rs.getString(11)); // log_datetime
            if(bExternalRep){
            	vecTemp.add(9, rs.getString(12) );
	            //if(rs.getString(12) == null){
	            //	vecTemp.add(9, "Assigned");
	            //}else{
	            //	vecTemp.add(9, "External");
	            //}
	            if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
		            vecTemp.add(10, rs.getString(13)); // repName
		            vecTemp.add(11, rs.getString(14)); // subAgentName
		            vecTemp.add(12, rs.getString(15)); // agentName
	            }
	            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
	            	vecTemp.add(10, rs.getString(13)); // repName
	            }
            }
            else{
            	if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
		            vecTemp.add(9, rs.getString(13)); // repName
		            vecTemp.add(10, rs.getString(14)); // subAgentName
		            vecTemp.add(11, rs.getString(15)); // agentName
            	}
            	if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
	            	vecTemp.add(9, rs.getString(13)); // repName
	            }
            	
            }
            
        }
        else
        {
	        vecTemp.add(4, NumberUtil.formatAmount(rs.getString(5))); // amount
	        vecTemp.add(5, NumberUtil.formatAmount(rs.getString(6))); // prior_available_credit
	        vecTemp.add(6, rs.getString(7)); // Deposit_date
	        if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && entityType.equalsIgnoreCase("Rep") && extraFilter.equals("1")){
		        vecTemp.add(7, rs.getString(8)); // subAgentName
		        vecTemp.add(8, rs.getString(9)); // agentName
	        }
	        else if(entityType.equalsIgnoreCase("SubAgent") && extraFilter.equals("1")){
	        	vecTemp.add(7, rs.getString(8)); // agentName
	        }
	 
        }
        vecPayments.add(vecTemp);
      }
    }
    catch (Exception e)
    {
      UserPayments.cat.error("Error during getPayments", e);
      e.printStackTrace();
      throw new CustomerException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        UserPayments.cat.error("Error during closeConnection", e);
      }

    }
    return vecPayments;
  }
  
  public static String getListToStr_SQL_IN(String colIn, String[] list) {
      String strSQL = "";
      if (list != null && list.length >0) 
      {
          strSQL += " " + colIn + " IN (";
          int i=0;
          for (String merchant : list) {
              strSQL += "'"+merchant+"',";
          }
          strSQL = strSQL.substring(0, strSQL.length() - 1) + ")";
      }
      return strSQL;
  }

	public static void setPreparedStatementFromListSQLINObject(PreparedStatement pstmt, String[] list, int i) throws SQLException {
		for (String obj : list) {
			pstmt.setDouble(i,Double.parseDouble(obj));
			i++;
		}
	}
  
  public Vector getPaymentsDownload(SessionData sessionData,ServletContext context,  String entityType,String itemSelected, String[] strMerchantIds) throws CustomerException
  {
	
    Vector vecResultsDownload = new Vector();
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    boolean bExternalRep=com.debisys.users.User.isExternalRepAllowedEnabled(sessionData);
    Vector vecSearchResults = getPayments(sessionData,context, entityType, itemSelected, strMerchantIds, DebisysConstants.EXECUTE_REPORT, null, null);
    Iterator it = vecSearchResults.iterator();
    String extraFilter = getPermissionFaceyJAMGrpPaymentDetailsReport(sessionData);

    if (it.hasNext())
    {
      Vector vecTemp = new Vector();
      if(entityType.equalsIgnoreCase("Merchant"))
      {
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.merchant_id", sessionData.getLanguage()));
	      if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
	    	  vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",sessionData.getLanguage()));
	    	  vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",sessionData.getLanguage()));
	    	  vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",sessionData.getLanguage()));
         }
         else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){
        	 vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",sessionData.getLanguage()));
         }
	      
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.dba", sessionData.getLanguage()));
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id", sessionData.getLanguage()));
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description", sessionData.getLanguage()));
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment", sessionData.getLanguage()));
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.balance", sessionData.getLanguage()));
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit", sessionData.getLanguage()));
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.Deposit_date", sessionData.getLanguage()));
	      vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.log_datetime", sessionData.getLanguage()));
	      if(bExternalRep){
	    	  vecTemp.add(Languages.getString("jsp.admin.reports.extern.RepresentativeType", sessionData.getLanguage()));
	      }
      }
      else{
    	  //vecTemp.add(Languages.getString("jsp.admin.reports.payments.deposits.entityid", sessionData.getLanguage()));
    	  
    	  String entityId = "ENTITY "+Languages.getString("jsp.admin.reports.payments.deposits.entityid",sessionData.getLanguage());
    	  boolean wasValidated = false;
    	  if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){    		  
		        if (entityType.equalsIgnoreCase("SubAgent")){
		        	wasValidated = true;
		        	entityId = entityId.replace("ENTITY", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",sessionData.getLanguage()));
		        	vecTemp.add(entityId);
		        	vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",sessionData.getLanguage()));
		        }
		        else if (entityType.equalsIgnoreCase("Rep")){
		        	wasValidated = true;
		        	entityId = entityId.replace("ENTITY", Languages.getString("jsp.admin.reports.payments.depositsByMerchant.rep",sessionData.getLanguage()));
		        	vecTemp.add(entityId);
		        	vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.agent",sessionData.getLanguage()));
		        	vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.subAgent",sessionData.getLanguage()));
		        }
         }
    	  if(!wasValidated){
         		entityId = entityId.replace("ENTITY", "");
         		vecTemp.add(entityId);
         }
          vecTemp.add(Languages.getString("jsp.admin.reports.payments.deposits.dba", sessionData.getLanguage()));
          vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.logon_id", sessionData.getLanguage()));
          vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.description", sessionData.getLanguage()));
          vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.payment", sessionData.getLanguage()));
          vecTemp.add(Languages.getString("jsp.admin.reports.payments.depositsByMerchant.prior_available_credit", sessionData.getLanguage()));
          vecTemp.add(Languages.getString("jsp.admin.reports.payments.deposits.datetime", sessionData.getLanguage()));
      }
      vecResultsDownload.add(vecTemp);
      while (it.hasNext())
      {
        vecTemp = new Vector();
        vecTemp = (Vector) it.next();
        
	        if(entityType.equalsIgnoreCase("Merchant") && !strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && extraFilter.equals("1")){        	
	        	
	        	
	        	if(bExternalRep){
	        		vecTemp.add(1, vecTemp.get(12));
	        		vecTemp.remove(13);
		        	vecTemp.add(2, vecTemp.get(12));
		        	vecTemp.remove(13);
		        	vecTemp.add(3, vecTemp.get(12));
		        	vecTemp.remove(13);
		        	
	        	}
	        	else{
	        		vecTemp.add(1, vecTemp.get(11));
	        		vecTemp.remove(12);
		        	vecTemp.add(2, vecTemp.get(11));
		        	vecTemp.remove(12);
		        	vecTemp.add(3, vecTemp.get(11));
		        	vecTemp.remove(12);
	        	}
	        	
	        	vecTemp.set(7, NumberUtil.formatCurrency(vecTemp.get(7).toString()));
	            vecTemp.set(8, NumberUtil.formatCurrency(vecTemp.get(8).toString()));
	            vecTemp.set(9, NumberUtil.formatCurrency(vecTemp.get(9).toString()));
	        }
	        else if(entityType.equalsIgnoreCase("Merchant") && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)&& extraFilter.equals("1")){
	        	vecTemp.add(1, ((bExternalRep)?vecTemp.get(10):vecTemp.get(9)));
	        	vecTemp.set(5, NumberUtil.formatCurrency(vecTemp.get(5).toString()));
	            vecTemp.set(6, NumberUtil.formatCurrency(vecTemp.get(6).toString()));
	            vecTemp.set(7, NumberUtil.formatCurrency(vecTemp.get(7).toString()));
	        	if(bExternalRep){
	        		vecTemp.remove(11);
	        	}
	        	else{
	        		vecTemp.remove(10);
	        	}
	        }
	        
	        
	        	if (!strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && entityType.equalsIgnoreCase("Rep") && extraFilter.equals("1")){
			        vecTemp.add(1, vecTemp.get(8));// agent
			        vecTemp.remove(9);
			        vecTemp.add(2, vecTemp.get(8));// like move the subagent is 8 position
			        vecTemp.remove(9);
			        vecTemp.set(6, NumberUtil.formatCurrency(vecTemp.get(6).toString()));
		            vecTemp.set(7, NumberUtil.formatCurrency(vecTemp.get(7).toString()));
			        
			        
		        }
		        else if(entityType.equalsIgnoreCase("SubAgent") && extraFilter.equals("1")){
		        	vecTemp.add(1, vecTemp.get(7));
		        	vecTemp.set(5, NumberUtil.formatCurrency(vecTemp.get(5).toString()));
		            vecTemp.set(6, NumberUtil.formatCurrency(vecTemp.get(6).toString()));
			        vecTemp.remove(8);
		        }
	        	
		        if(!extraFilter.equals("1")){
		        	vecTemp.set(4, NumberUtil.formatCurrency(vecTemp.get(4).toString()));
		            vecTemp.set(5, NumberUtil.formatCurrency(vecTemp.get(5).toString()));
		        }

           if(entityType.equalsIgnoreCase("Merchant") && !extraFilter.equals("1")){
        	   vecTemp.set(6, NumberUtil.formatCurrency(vecTemp.get(6).toString()));
           }
           
        vecResultsDownload.add(vecTemp);
      }
    }

    return vecResultsDownload;
  }
  
  public static String getPermissionFaceyJAMGrpPaymentDetailsReport(SessionData sessionData){
		
		User tmpUser = new User();
		String strIsoId = "1";
		String strAccessLevel = sessionData.getProperty("access_level");
		try {
			strIsoId = tmpUser.getISOId(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"));
			
		} catch (UserException e) {
			e.printStackTrace();
		}
		boolean isExtraFilter = getIsoWebPermission(DebisysConstants.EXTRA_FILTER_ON_PAYMENT_DETAIL_REPORT, strIsoId, strAccessLevel);
		
		boolean userHasPermission = getWebUserPermission(DebisysConstants.EXTRA_FILTER_ON_PAYMENT_DETAIL_REPORT, sessionData.getProperty("ref_id"));
	
		if(isExtraFilter && userHasPermission){
			return "1";
		}
		return "0";
  }
  
  public static boolean getIsoWebPermission(int webPermission, String isoId, String strAccessLevel) {
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			isoId = isoId.trim();
			if (isoId.equals("")){
				cat.error("Empty IsoId passed as parameter");
				throw new Exception();
			}
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null){
				cat.error("Can't get database connection");
				throw new Exception();
			}
			pstmt = dbConn.prepareStatement("SELECT * FROM web_iso_permissions WITH (NOLOCK) WHERE web_permission_type_id = ? AND iso_id = ?");
			pstmt.setInt(1, webPermission);
			pstmt.setDouble(2, Double.parseDouble(isoId));
			rs = pstmt.executeQuery();
			if (rs.next()){
				if(rs.getString("iso") != null && rs.getString("iso").trim().equalsIgnoreCase("Y")  && strAccessLevel.equals(DebisysConstants.ISO)){
					retValue = true;
				}
				else if(rs.getString("agent") != null && rs.getString("agent").trim().equalsIgnoreCase("Y")  && strAccessLevel.equals(DebisysConstants.AGENT)){
					retValue = true;
				}
				else if(rs.getString("subagent") != null && rs.getString("subagent").trim().equalsIgnoreCase("Y")  && strAccessLevel.equals(DebisysConstants.SUBAGENT)){
					retValue = true;
				}
				else if(rs.getString("rep") != null && rs.getString("rep").trim().equalsIgnoreCase("Y")  && strAccessLevel.equals(DebisysConstants.REP)){
					retValue = true;
				}
				else{
					retValue = false;
				}
				
			}
		}
		catch (Exception localException)
		{
			cat.error("Error during getIsoWebPermission", localException);
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during release connection", e);
			}
		}
		return retValue;
	}
  
  public static boolean getWebUserPermission(int webPermission, String ref_id) {
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			ref_id = ref_id.trim();
			if (ref_id.equals("")){
				cat.error("Empty ref_id passed as parameter");
				throw new Exception();
			}
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null){
				cat.error("Can't get database connection");
				throw new Exception();
			}
			
			String sqlQuery = "select wu.* "+ 
					" FROM web_user_permissions wu "+
					" INNER JOIN password p ON p.password_id = wu.password_id "+
					" WHERE p.ref_id = ? AND wu.web_permission_type_id = ?";
			pstmt = dbConn.prepareStatement(sqlQuery); 
			
			cat.info("getWebUserPermission SQL: "+sqlQuery+" /* "+ref_id+", "+webPermission+" */");
					
			pstmt.setDouble(1, Double.parseDouble(ref_id));
			pstmt.setInt(2, webPermission);
			rs = pstmt.executeQuery();
			if (rs.next()){
				retValue = true;
			}
		}
		catch (Exception localException){
			cat.error("Error during getIsoWebPermission", localException);
		}
		finally{
			try{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e){
				cat.error("Error during release connection", e);
			}
		}
		return retValue;
	}

/**
 * @param urlLocation the urlLocation to set
 */
public void setUrlLocation(String urlLocation) {
	this.urlLocation = urlLocation;
}

/**
 * @return the urlLocation
 */
public String getUrlLocation() {
	return urlLocation;
}

}
