package com.debisys.customers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.debisys.customers.parameter.MerchantCreditDownloadDTO;
import com.debisys.exceptions.CustomerException;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.summarys.DownloadsSummary;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.TimeZone;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * MerchantReportDAO is a class that delegates the responsibility of producing User merchant credit payment reports
 * 
 * @author cmercado
 *
 */
public class MerchantReportDAO {
	
	private static final Logger logger = Logger.getLogger(MerchantReportDAO.class);
	private static final ResourceBundle sqlBundle = ResourceBundle.getBundle("com.debisys.customers.sql");

	public String getUserMerchantCreditDownload(MerchantCreditDownloadDTO merCreditDTO) throws CustomerException {
		String startDate = merCreditDTO.getStartDate();
		String endDate = merCreditDTO.getEndDate();
		SessionData sessionData = merCreditDTO.getSessionData();
		ServletContext context = merCreditDTO.getContext();
		String urlLocation = "";
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
        		String strSQL = getUserMerchantCreditSQL(startDate, endDate, sessionData);
			String reportId = sessionData.getProperty("reportId");
			int limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);

            dbConn = TorqueHelper.getConnection(sqlBundle.getString("pkgAlternateDb"));

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            int daysBetween = DateUtil.getDaysBetween(endDate, startDate);
            if (daysBetween > limitDays) {
                startDate = DateUtil.addSubtractDays(endDate, -limitDays);
            }
            Date date1 = format.parse(startDate);
            Date date2 = format.parse(endDate);
            format.applyPattern("yyyy-MM-dd");
            startDate = format.format(date1);
            endDate = format.format(date2);

            if (dbConn == null) {
                Merchant.cat.error("Can't get database connection");
                throw new CustomerException();
            }
            logger.debug(strSQL);
            pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            pstmt.setString(1, merCreditDTO.getMerchantId());
            rs = pstmt.executeQuery();
            
            DownloadsSummary trxSummary = new DownloadsSummary();
			TransactionReport tr = new TransactionReport(); 
			String strFileName = tr.generateFileName(context);				
			urlLocation = trxSummary.downloadMerchantCredits( rs, sessionData, context, merCreditDTO.getHeaders(), strFileName, merCreditDTO.getTitles(), 2, merCreditDTO.isNeedsDBAName() );
        } catch (Exception e) {
        		logger.error(e.getMessage(), e);
        		throw new CustomerException(e.getMessage(), e);
        } finally {
        		TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return urlLocation;
	}

	public String getUserMerchantCreditSQL(String startDate, String endDate, SessionData sessionData) throws Exception {
		String strAccessLevel = sessionData.getProperty("access_level");
		String strRefId = sessionData.getProperty("ref_id");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		StringBuilder strSQL = new StringBuilder(sqlBundle.getString("getUserMerchantCredits"));
		
		if (strAccessLevel.equals(DebisysConstants.REP)) {
			strSQL.append(" INNER JOIN dbo.reps AS R WITH(NOLOCK) ON M.rep_id = R.rep_id WHERE R.rep_id = ").append(strRefId);
		}
		else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)
				|| (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))) {
			strSQL.append(" INNER JOIN dbo.reps R WITH(NOLOCK) ON M.rep_id = R.rep_id WHERE R.rep_id IN (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id = ").append(strRefId).append(")");
		}
		else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
			strSQL.append(
					"INNER JOIN dbo.reps R WITH(NOLOCK) ON M.rep_id = R.rep_id WHERE R.rep_id IN (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id IN (SELECT rep_id FROM dbo.reps WITH(NOLOCK) WHERE iso_id = ");
			strSQL.append(strRefId).append("))");
		} else if (strAccessLevel.equals(DebisysConstants.ISO)) {
			if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
				strSQL.append(" INNER JOIN dbo.reps R WITH (NOLOCK) ON M.rep_id=R.rep_id");
				strSQL.append(" INNER JOIN dbo.reps I WITH (NOLOCK) ON I.rep_id=R.iso_id AND I.type=").append(DebisysConstants.REP_TYPE_ISO_3_LEVEL);
				strSQL.append(" AND I.rep_id=I.iso_id AND I.rep_id=").append(strRefId);
			} else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
				strSQL.append(" INNER JOIN dbo.reps R WITH (NOLOCK) ON M.rep_id=R.rep_id");
				strSQL.append(" INNER JOIN dbo.reps S WITH (NOLOCK) ON S.rep_id=R.iso_id AND S.type=").append(DebisysConstants.REP_TYPE_SUBAGENT);
				strSQL.append(" INNER JOIN dbo.reps A WITH (NOLOCK) ON A.rep_id=S.iso_id AND A.type=").append(DebisysConstants.REP_TYPE_AGENT);
				strSQL.append(" INNER JOIN dbo.reps I WITH (NOLOCK) ON I.rep_id=A.iso_id AND I.type=").append(DebisysConstants.REP_TYPE_ISO_5_LEVEL);
				strSQL.append(" AND I.rep_id=I.iso_id AND I.rep_id=").append(strRefId);
			}
		}

		strSQL.append(" AND mc.Logon_id = ?");
		
		Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), startDate, endDate + " 23:59:59.999", false);

		//if (DateUtil.isValidYYYYMMDD(startDate) && DateUtil.isValidYYYYMMDD(endDate)) {
			strSQL.append(" AND MC.log_datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND MC.log_datetime <= '" + vTimeZoneFilterDates.get(1) + "'");
		//}

		strSQL.append(" order by mc.id DESC");

		String sql = strSQL.toString();
		sql = sql.replace("DATE", " m.receipt_date as log_datetime ");
		return sql;
	}
}
