package com.debisys.customers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Logger;
import com.debisys.exceptions.CustomerException;
import com.debisys.users.SessionData;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

public class SkuGlueSearch implements HttpSessionBindingListener{
	
	  private String criteria = "";
	  private String sort = "";
	  private String col = "";
	  private String rep_id = "";
	  private String merchantId = "";
	  private String productId ="";
	  
	  //log4j logging
	  private static Logger cat = Logger.getLogger(CustomerSearch.class);
	  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.customers.sql");
	  
	  
	  public String getProductId()
	  {
	    return StringUtil.toString(this.productId);
	  }
	  
	  public void setProductId(String strProductId)
	  {
		  this.productId = strProductId.trim();
	  }
	  
	  public String getMerchantId()
	  {
	    return StringUtil.toString(this.merchantId);
	  }
	  
	  public void setMerchantId(String strMerchantId)
	  {
		  this.merchantId = strMerchantId.trim();
	  }
	  
	  
	  public String getCriteria()
	  {
	    return StringUtil.toString(this.criteria);
	  }
	   

	  public void setCriteria(String strCriteria)
	  {
	    this.criteria = strCriteria.trim();
	  }

	  public String getCol()
	  {
	    return StringUtil.toString(this.col);
	  }

	  public void setCol(String strCol)
	  {
	    this.col = strCol;
	  }

	  public String getSort()
	  {
	    return StringUtil.toString(this.sort);
	  }

	  public void setSort(String strSort)
	  {
	    this.sort = strSort;
	  }
	  public String getRepId()
	  {
	    return StringUtil.toString(this.rep_id);
	  }

	  public void setRepId(String strValue)
	  {
	    //double dblRepId;//Not needed
	    try
	    {
	      /*dblRepId = */Double.parseDouble(strValue);
	    }
	    catch (NumberFormatException nfe)
	    {
	      strValue = "";
	    }
	    this.rep_id = strValue;
	  }
	  
	public void valueBound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void valueUnbound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	 private int merchantRouteId;
	  public int getMerchantRouteId() 
	  {
	      return this.merchantRouteId;
	  }

	  public void setMerchantRouteId(int merchantRouteId) 
	  {
	      this.merchantRouteId = merchantRouteId;
	  }  
	  
	  private String merchantRouteName;
	  public String getMerchantRouteName() 
	  {
	      return this.merchantRouteName;
	  }

	  public void setMerchantRouteName(String merchantRouteName) 
	  {
	      this.merchantRouteName = merchantRouteName;
	  } 

	  /**
	 * @param sessionData  
	 * @param context 
	 */
	public Vector<Vector<String>> searchSkuNoWLProductsDesc(int intPageNumber, int intRecordsPerPage,SessionData sessionData,ServletContext context) throws CustomerException
	  {
		  Connection dbConn = null;
		   PreparedStatement pstmt = null;
		
		   Vector<Vector<String>> vecSKUGlue = new Vector<Vector<String>>();
		   ResultSet rs = null;
		   //Vector vecCounts = new Vector();
		
		   int intRecordCount = 0;
		   try
		   {
		     dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
		     if (dbConn == null)
		     {
		       cat.error("Can't get database connection");
		       throw new CustomerException();
		     }
		     //String strSQLWhere = "";
		     //String strAccessLevel = sessionData.getProperty("access_level");
		     //String strRefId = sessionData.getProperty("ref_id");
		     //String strDistChainType = sessionData.getProperty("dist_chain_type");
		    
		     //begin actual query for results
		     //int intCol = 0;
		     //int intSort = 0;
		     //String strCol = "";
		     //String strSort = "";
		
		     /*
		     try
		     {
		       if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
		       {
		         intCol = Integer.parseInt(this.col);
		         intSort = Integer.parseInt(this.sort);
		       }
		     }
		     catch (NumberFormatException nfe)
		     {
		       intCol = 0;
		       intSort = 0;
		     }

		    switch (intCol)
		     {
		       case 1:
		         strCol = "id";
		         break;
		       case 2:
		         strCol = "description";
		         break;
		       default:
		         strCol = "id";
		         break;
		     }
		     */
		
		     String strSQL;
		     
		     strSQL = " SELECT DISTINCT id, description " +
		     		  " FROM products (NOLOCK) WHERE id = " +Integer.parseInt(this.productId) ;
		     		
		     
		     pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		     rs = pstmt.executeQuery();
		     intRecordCount = DbUtil.getRecordCount(rs);
		    		
		     if (intRecordCount > 0)
		     {
			rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
			
		
		       for (int i=0;i<intRecordsPerPage;i++)
		       {
		         Vector<String> vecTemp = new Vector<String>();
		         vecTemp.add(StringUtil.toString(rs.getString("id")));
		         vecTemp.add(StringUtil.toString(rs.getString("description")));
		         vecSKUGlue.add(vecTemp);
		         //This block replaces the below one because it causes a warning because of an inefficent condition
		         //Also because calling isLast represents a performance cost in the engine because of data fetching
		         if ( !rs.next() )
		         {
		         	break;
		         }
		        
		       }
		     }
		   }
		   catch (Exception e)
		   {
		     cat.error("Error during searchSkuNoWLProductsDesc", e);
		     throw new CustomerException();
		   }
		   finally
		   {
		     try
		     {
		       TorqueHelper.closeConnection(dbConn, pstmt, rs);
		     }
		     catch (Exception e)
		     {
		       cat.error("Error during closeConnection", e);
		     }
		   }
		   return vecSKUGlue;

	  }
	  
	 /**
	 * @param context  
	 */
	public Vector<Vector<String>> searchSkuNoWLProducts(int intPageNumber, int intRecordsPerPage,SessionData sessionData,ServletContext context)  throws CustomerException
	 {
		 Connection dbConn = null;
		   PreparedStatement pstmt = null;
		
		   Vector<Vector<String>> vecSKUGlue = new Vector<Vector<String>>();
		   ResultSet rs = null;
		   //Vector vecCounts = new Vector();
		
		   int intRecordCount = 0;
		   try
		   {
		     dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
		     if (dbConn == null)
		     {
		       cat.error("Can't get database connection");
		       throw new CustomerException();
		     }
		     //String strSQLWhere = "";
		     String strAccessLevel = sessionData.getProperty("access_level");
		     //String strRefId = sessionData.getProperty("ref_id");
			String strRefId = "";
			if (strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				strRefId = sessionData.getUser().getISOId(DebisysConstants.MERCHANT, this.merchantId);
			}
			else
			{
				strRefId = sessionData.getUser().getISOId(strAccessLevel, sessionData.getProperty("ref_id"));
			}

		     //String strDistChainType = sessionData.getProperty("dist_chain_type");
		    /*
		     //begin actual query for results
		     int intCol = 0;
		     int intSort = 0;
		     String strCol = "";
		     String strSort = "";
		
		     try
		     {
		       if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
		       {
		         intCol = Integer.parseInt(this.col);
		         intSort = Integer.parseInt(this.sort);
		       }
		     }
		     catch (NumberFormatException nfe)
		     {
		       intCol = 0;
		       intSort = 0;
		     }
		
		    switch (intCol)
		     {
		       case 1:
		         strCol = "products.id";
		         break;
		       case 2:
		         strCol = "products.description";
		         break;
		       default:
		         strCol = "products.id";
		         break;
		     }
		     */
		
		     String strSQL;
		     
		     strSQL = " SELECT DISTINCT products.id,  products.description " +
		     		  " FROM approvalIsoSkuGlue sku " +
		     		  " LEFT JOIN products (NOLOCK) ON products.id = sku.product_id " +
		     		  " WHERE sku.isoid = "+strRefId +" and products.requireApproval = 1 and sku.product_id not in " + 
					  "(SELECT productId FROM approvalWhiteList WHERE merchantID = "+this.merchantId+") ORDER BY products.id";
		     
		     
		     pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		     rs = pstmt.executeQuery();
		     intRecordCount = DbUtil.getRecordCount(rs);

		
		     if (intRecordCount > 0)
		     {
			rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
			
		
		       for (int i=0;i<intRecordsPerPage;i++)
		       {
		         Vector<String> vecTemp = new Vector<String>();
		         vecTemp.add(StringUtil.toString(rs.getString("id")));
		         vecTemp.add(StringUtil.toString(rs.getString("description")));
		         vecSKUGlue.add(vecTemp);
		         //This block replaces the below one because it causes a warning because of an inefficent condition
		         //Also because calling isLast represents a performance cost in the engine because of data fetching
		         if ( !rs.next() )
		         {
		         	break;
		         }
		        
		       }
		     }
		   }
		   catch (Exception e)
		   {
		     cat.error("Error during searchSkuNoWLProducts", e);
		     throw new CustomerException();
		   }
		   finally
		   {
		     try
		     {
		       TorqueHelper.closeConnection(dbConn, pstmt, rs);
		     }
		     catch (Exception e)
		     {
		       cat.error("Error during closeConnection", e);
		     }
		   }
		   return vecSKUGlue;
		     
		     
	 
	 }
	  
	  
	 /**
	 * @param context  
	 */
	public Vector<Object> searchSkuGlue(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context)
     throws CustomerException
	 {
	   Connection dbConn = null;
	   PreparedStatement pstmt = null;
	
	   Vector<Object> vecSKUGlue = new Vector<Object>();
	   ResultSet rs = null;
	   Vector<Integer> vecCounts = new Vector<Integer>();
	
	   int intRecordCount = 0;
	   try
	   {
	     dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
	     if (dbConn == null)
	     {
	       cat.error("Can't get database connection");
	       throw new CustomerException();
	     }
	    
	     String strSQLWhere = "";
	     String strAccessLevel = sessionData.getProperty("access_level");
	    
	     String strRefId = "";
			if (strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				strRefId = sessionData.getUser().getISOId(DebisysConstants.MERCHANT, this.merchantId);
			}
			else
			{
				strRefId = sessionData.getUser().getISOId(strAccessLevel, sessionData.getProperty("ref_id"));
			}
	    
	     //String strDistChainType = sessionData.getProperty("dist_chain_type");
	     
	  	
	     //begin actual query for results
	     int intCol = 0;
	     int intSort = 0;
	     String strCol = "";
	     String strSort = "";
	
	     try
	     {
	       if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
	       {
	         intCol = Integer.parseInt(this.col);
	         intSort = Integer.parseInt(this.sort);
	       }
	     }
	     catch (NumberFormatException nfe)
	     {
	       intCol = 0;
	       intSort = 0;
	     }
	
	     if (intSort == 2)
	     {
	       strSort = "DESC";
	     }
	     else
	     {
	       strSort = "ASC";
	     }
	
	     switch (intCol)
	     {
	       case 1:
	         strCol = "products.id";
	         break;
	       case 2:
	         strCol = "products.description";
	         break;
	       case 3:
	         strCol = "approvalWhiteList.approvalStatusBit";
	         break;      
	       default:
	         strCol = "products.id";
	         break;
	     }
	
	     String strSQL;

	     strSQL =  "SELECT distinct products.id, products.description, apwl.approvalStatusBit " +
	    	 	   "FROM merchants (NOLOCK) "+
	     		   "INNER JOIN reps (NOLOCK) ON merchants.rep_id = reps.rep_id "+
	     		   "INNER JOIN approvalWhiteList apwl (NOLOCK) ON apwl.merchantId = merchants.merchant_id and apwl.merchantId = "+ this.merchantId +
	     		   " INNER JOIN products (NOLOCK) ON products.id = apwl.productid ";
	     if ( sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
	     {
	    	 strSQL += "INNER JOIN Carriers c WITH (NOLOCK) ON products.carrier_id = c.id " + 
	    	 			"INNER JOIN RepCarriers rc WITH (NOLOCK) ON c.id = rc.CarrierID " + 
	    	 			"WHERE rc.RepID = " + sessionData.getUser().getRefId();
	     }
	     else
	     {
	    	 strSQL += "WHERE products.id in (select distinct product_id from approvalIsoSkuGlue where isoid = " + strRefId + ")  ";
	     }

	     cat.debug(strSQL + strSQLWhere + " order by " + strCol + " " + strSort);
	     pstmt = dbConn.prepareStatement(strSQL + strSQLWhere + " order by " +strCol + " " + strSort, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
	     rs = pstmt.executeQuery();
	     intRecordCount = DbUtil.getRecordCount(rs);
	     vecCounts.add(Integer.valueOf(intRecordCount));

	     //first row is always the counts of
	     //total, active, and record counts
	     vecSKUGlue.add(vecCounts);

	     if (intRecordCount > 0)
	     {
		rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
		
	
	       for (int i=0;i<intRecordsPerPage;i++)
	       {
	         Vector<String> vecTemp = new Vector<String>();
	         vecTemp.add(StringUtil.toString(rs.getString("id")));
	         vecTemp.add(StringUtil.toString(rs.getString("description")));
	         vecTemp.add(StringUtil.toString(rs.getString("approvalStatusBit")));
	    
	         vecSKUGlue.add(vecTemp);
	         //This block replaces the below one because it causes a warning because of an inefficent condition
	         //Also because calling isLast represents a performance cost in the engine because of data fetching
	         if ( !rs.next() )
	         {
	         	break;
	         }
	       
	       }
	     }
	   }
	   catch (Exception e)
	   {
	     cat.error("Error during searchMerchant", e);
	     throw new CustomerException();
	   }
	   finally
	   {
	     try
	     {
	       TorqueHelper.closeConnection(dbConn, pstmt, rs);
	     }
	     catch (Exception e)
	     {
	       cat.error("Error during closeConnection", e);
	     }
	   }
	   return vecSKUGlue;
	 }


}

