package com.debisys.ach;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.debisys.dateformat.DateFormat;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;

/**
 * Holds the information for ach transactions.
 * <P>
 * 
 * @author Jay Chi
 */

public class TransactionSearch implements HttpSessionBindingListener
{
	
	private String rep_id = "";
	private String merchant_id = "";
	private String start_date = "";
	private String end_date = "";
	private Hashtable<String, String> validationErrors = new Hashtable<String, String>();
	
	// log4j logging
	private static Logger cat = Logger.getLogger(TransactionSearch.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.ach.sql");
	
	/***********************************************************************************************
	 * ADDED FOR LOCALIZATION
	 * 
	 * @return
	 */
	public String getStartDateFormatted()
	{
		return this.getDateFormatted(this.start_date);
	}
	
	/***********************************************************************************************
	 * ADDED FOR LOCALIZATION
	 * 
	 * @return
	 */
	public String getEndDateFormatted()
	{
		return this.getDateFormatted(this.end_date);
	}
	
	/***********************************************************************************************
	 * ADDED FOR LOCALIZATION
	 * 
	 * @param dateToFormat
	 * @return
	 */
	private String getDateFormatted(String dateToFormat)
	{
		if (DateUtil.isValid(dateToFormat))
		{
			String date = null;
			try
			{
				// Current support site date is in MM/dd/YYYY.
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				java.util.Date dd = sdf.parse(dateToFormat);
				
				// We will specify a new date format through the database
				SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
				
				date = sdf1.format(dd);
				
				// Since Java SimpleDateFormat does not know whether a datetime contains
				// a time or not, remove the time (usually 12:00:00 AM) from the end of the date
				// ranges
				date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
			}
			catch (ParseException e)
			{
				cat.error("Error during TransactionReport date localization ", e);
			}
			return date;
		}
		// Something isn't valid in the date string - don't try to localize it
		return StringUtil.toString(this.start_date);
	}
	
	public String getStartDate()
	{
		return StringUtil.toString(this.start_date);
	}
	
	public void setStartDate(String strValue)
	{
		this.start_date = strValue;
	}
	
	public String getEndDate()
	{
		return StringUtil.toString(this.end_date);
	}
	
	public void setEndDate(String strValue)
	{
		this.end_date = strValue;
	}
	
	public String getRepId()
	{
		return StringUtil.toString(this.rep_id);
	}
	
	public void setRepId(String strValue)
	{
		// double dblRepId;
		try
		{
			/* dblRepId = */java.lang.Double.parseDouble(strValue);
		}
		catch (NumberFormatException nfe)
		{
			strValue = "";
		}
		this.rep_id = strValue;
	}
	
	public String getMerchantId()
	{
		return StringUtil.toString(this.merchant_id);
	}
	
	public void setMerchantId(String strValue)
	{
		// double dblMerchantId;
		try
		{
			/* dblMerchantId = */java.lang.Double.parseDouble(strValue);
		}
		catch (NumberFormatException nfe)
		{
			strValue = "";
		}
		this.merchant_id = strValue;
	}
	
	public static Vector<Vector<String>> getAgentList(SessionData sessionData, String strIsoID)
			throws TransactionException
	{
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			if ((strIsoID.equals("-1")) || (strIsoID.equals("-2")) || (strIsoID.trim().equals("")))
			{
				strIsoID = sessionData.getProperty("ref_id");
			}
			String strSQLGetAgentList = "SELECT r.rep_id, r.businessname FROM reps AS r WITH(NOLOCK) WHERE r.iso_id = ? AND r.rep_id <> r.iso_id ORDER BY r.businessname";
			cat.debug(strSQLGetAgentList + "/* iso_id = " + strIsoID + "*/");
			pstmt = dbConn.prepareStatement(strSQLGetAgentList);
			pstmt.setLong(1, Long.parseLong(strIsoID));
			ResultSet rsGetAgentList = pstmt.executeQuery();
			while (rsGetAgentList.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rsGetAgentList.getString("rep_id")));
				vecTemp.add(StringUtil.toString(rsGetAgentList.getString("businessname")));
				vecResultList.add(vecTemp);
			}
			rsGetAgentList.close();
			pstmt.close();
			pstmt = null;
			
		}
		catch (Exception e)
		{
			cat.error("Error during getAgentList", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}
	
	public static Vector<Vector<String>> getSubAgentList(SessionData sessionData, String strAgentID)
			throws TransactionException
	{
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			String strSQLGetSubAgentList = "";
			if ((strAgentID.equals("-1")) || (strAgentID.equals("-2")) || (strAgentID.trim().equals("")))
			{
				strAgentID = sessionData.getProperty("ref_id");
				String strAccessLevel = sessionData.getProperty("access_level");
				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					strSQLGetSubAgentList = "SELECT rep_id, businessname FROM reps AS r WITH(NOLOCK) WHERE  iso_id IN (SELECT rep_id FROM reps (nolock) WHERE  iso_id = ? AND rep_id <> iso_id ) ORDER BY businessname";
				}
				else
				{
					strSQLGetSubAgentList = "SELECT rep_id, businessname FROM reps AS r WITH(NOLOCK) WHERE  iso_id = ? ORDER BY businessname";
				}
			}
			else
			{
				strSQLGetSubAgentList = "SELECT rep_id, businessname FROM reps AS r WITH(NOLOCK) WHERE  iso_id = ? ORDER BY businessname";
			}
			cat.debug(strSQLGetSubAgentList + "/* "+ strAgentID +" */");
			pstmt = dbConn.prepareStatement(strSQLGetSubAgentList);
			pstmt.setLong(1, Long.parseLong(strAgentID));
			ResultSet rsGetAgentList = pstmt.executeQuery();
			while (rsGetAgentList.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rsGetAgentList.getString("rep_id")));
				vecTemp.add(StringUtil.toString(rsGetAgentList.getString("businessname")));
				vecResultList.add(vecTemp);
			}
			rsGetAgentList.close();
			pstmt.close();
			pstmt = null;
			
		}
		catch (Exception e)
		{
			cat.error("Error during getAgentList", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}
	
	public static Vector<Vector<String>> getRepList(SessionData sessionData, String strSubAgentID)
			throws TransactionException
	{
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			String strSQLGetRep = "";
			if ((strSubAgentID.equals("-1")) || (strSubAgentID.equals("-2")) || (strSubAgentID.trim().equals(""))){
				strSubAgentID = sessionData.getProperty("ref_id");
				String strAccessLevel = sessionData.getProperty("access_level");
				String strDistChainType = sessionData.getProperty("dist_chain_type");
				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLGetRep = "SELECT r.rep_id, r.businessname FROM reps AS r WITH(NOLOCK) WHERE r.type = " + DebisysConstants.REP_TYPE_REP
								+ " AND r.iso_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type = " + DebisysConstants.REP_TYPE_SUBAGENT
								+ " AND r1.iso_id IN ( SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type = " + DebisysConstants.REP_TYPE_AGENT 
								+ " AND r2.iso_id = ? AND r2.rep_id <> r2.iso_id ) ) ORDER BY r.businessname";
					}
					else
					{
						strSQLGetRep = "SELECT r.rep_id, r.businessname FROM reps AS r WITH(NOLOCK) WHERE r.type=" + DebisysConstants.REP_TYPE_REP + " AND r.iso_id = ? ORDER BY r.businessname";
					}
				}
				else
				{
					if (strAccessLevel.equals(DebisysConstants.AGENT))
					{
						strSQLGetRep = "SELECT r.rep_id, r.businessname FROM reps AS r WITH(NOLOCK) WHERE r.type=" + DebisysConstants.REP_TYPE_REP
								+ " AND r.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type=" + DebisysConstants.REP_TYPE_SUBAGENT 
								+ " AND r2.iso_id = ? AND r2.rep_id <> r2.iso_id ) ORDER BY r.businessname";
					}
					else
					{
						strSQLGetRep = "SELECT r.rep_id, r.businessname FROM reps AS r WITH(NOLOCK) WHERE r.type=" + DebisysConstants.REP_TYPE_REP
								+ " AND r.iso_id = ? ORDER BY r.businessname";
					}
				}
			}
			else
			{
				strSQLGetRep = "SELECT r.rep_id, r.businessname FROM reps AS r WITH(NOLOCK) WHERE r.iso_id = ? ORDER BY r.businessname";
			}
			cat.debug(strSQLGetRep + "/* " + strSubAgentID + " */");
			pstmt = dbConn.prepareStatement(strSQLGetRep);
			pstmt.setLong(1, Long.parseLong(strSubAgentID));
			ResultSet rsGetRepList = pstmt.executeQuery();
			while (rsGetRepList.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rsGetRepList.getString("rep_id")));
				vecTemp.add(StringUtil.toString(rsGetRepList.getString("businessname")));
				vecResultList.add(vecTemp);
			}
			rsGetRepList.close();
			pstmt.close();
			pstmt = null;
			
		}
		catch (Exception e)
		{
			cat.error("Error during getAgentList", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}
	
	public static Vector<Vector<String>> getMerchantList(SessionData sessionData, String strRepID)
			throws TransactionException
	{
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			String strSQLGetMerchant = "";
			if ((strRepID.equals("-1")) || (strRepID.equals("-2")) || (strRepID.trim().equals("")))
			{
				strRepID = sessionData.getProperty("ref_id");
				String strAccessLevel = sessionData.getProperty("access_level");
				String strDistChainType = sessionData.getProperty("dist_chain_type");
				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLGetMerchant = "SELECT m.merchant_id, m.dba FROM merchants AS m WITH(NOLOCK) WHERE m.rep_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.iso_id IN ( SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.iso_id IN ( SELECT r3.rep_id FROM reps AS r3 WITH(NOLOCK) WHERE r3.iso_id = ? AND r3.rep_id <> r3.iso_id ) ) ) ORDER BY m.dba";
					}
					else
					{
						strSQLGetMerchant = "SELECT m.merchant_id, m.dba FROM merchants AS m WITH(NOLOCK) WHERE m.rep_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.iso_id = ? AND r1.rep_id <> r1.iso_id ) ORDER BY m.dba";
					}
				}
				else
				{
					if (strAccessLevel.equals(DebisysConstants.AGENT))
					{
						strSQLGetMerchant = "SELECT m.merchant_id, m.dba FROM merchants As M WITH(NOLOCK) WHERE m.rep_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.iso_id IN ( SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.iso_id = ? AND r2.rep_id <> r2.iso_id )) ORDER BY m.dba";
					}
					else
					{
						if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
						{
							strSQLGetMerchant = "SELECT m.merchant_id, m.dba FROM merchants AS m WITH(NOLOCK) WHERE m.rep_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.iso_id = ? AND r1.rep_id <> r1.iso_id ) ORDER BY m.dba";
						}
						else
						{
							strSQLGetMerchant = "SELECT m.merchant_id, m.dba FROM merchants AS m WITH(NOLOCK) WHERE m.rep_id = ? ORDER BY m.dba";
						}
					}
				}
			}
			else
			{
				strSQLGetMerchant = "SELECT m.merchant_id, m.dba FROM merchants AS m WITH(NOLOCK) WHERE m.rep_id = ? ORDER BY m.dba";
			}
			cat.debug(strSQLGetMerchant + "/* "+ strRepID +" */");
			pstmt = dbConn.prepareStatement(strSQLGetMerchant);
			pstmt.setLong(1, Long.parseLong(strRepID));
			ResultSet rsGetRepList = pstmt.executeQuery();
			while (rsGetRepList.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rsGetRepList.getString("merchant_id")));
				vecTemp.add(StringUtil.toString(rsGetRepList.getString("dba")));
				vecResultList.add(vecTemp);
			}
			rsGetRepList.close();
			pstmt.close();
			pstmt = null;
			
		}
		catch (Exception e)
		{
			cat.error("Error during getAgentList", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}
	
	public static String getIsoName(SessionData sessionData) throws TransactionException
	{
		Connection dbConn = null;
		String IsoName = "";
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRepMerchantID = sessionData.getProperty("ref_id");
			String strRepID = "";
			if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				strRepID = getMerchantRepID(strRepMerchantID);
			}
			else
			{
				strRepID = strRepMerchantID;
			}
			
			String IsoId = "";
			String RepId = strRepID;
			String IsoRepTemp = strRepID;
			while (!RepId.equals(IsoId))
			{
				IsoId = getIsoRepID(IsoRepTemp, true);
				RepId = getIsoRepID(IsoRepTemp, false);
				IsoRepTemp = IsoId;
			}
			String strSQL = "SELECT r.businessname FROM reps AS r WITH(NOLOCK) WHERE r.rep_id = ?";
			cat.debug(strSQL + "/* "+ IsoId +" */");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, Long.parseLong(IsoId));
			ResultSet rsGetReport = pstmt.executeQuery();
			while (rsGetReport.next())
			{
				IsoName = StringUtil.toString(rsGetReport.getString("businessname"));
			}
			rsGetReport.close();
			pstmt.close();
			pstmt = null;
		}
		catch (Exception e)
		{
			cat.error("Error during getIsoName", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return IsoName;
	}
	
	private static String getMerchantRepID(String strMerchantID) throws TransactionException
	{
		Connection dbConn = null;
		String MerchantID = "";
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			String strSQL = "SELECT m.rep_id FROM merchants AS m WITH(NOLOCK) WHERE m.merchant_id = ?";
			cat.debug(strSQL + "/* "+strMerchantID+" */");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, Long.parseLong(strMerchantID));
			ResultSet rsGetReport = pstmt.executeQuery();
			while (rsGetReport.next())
			{
				MerchantID = StringUtil.toString(rsGetReport.getString("rep_id"));
			}
			rsGetReport.close();
			pstmt.close();
			pstmt = null;
		}
		catch (Exception e)
		{
			cat.error("Error during getMerchantRepID", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return MerchantID;
	}
	
	private static String getIsoRepID(String strRepID, boolean IsoRep) throws TransactionException
	{
		Connection dbConn = null;
		String IsoRepID = "";
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			String strSQL = "SELECT r.iso_id, r.rep_id FROM reps AS r WITH(NOLOCK) WHERE r.rep_id = ?";
			cat.debug(strSQL + "/* "+strRepID+" */");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, Long.parseLong(strRepID));
			ResultSet rsGetReport = pstmt.executeQuery();
			while (rsGetReport.next())
			{
				if (IsoRep)
				{
					IsoRepID = StringUtil.toString(rsGetReport.getString("iso_id"));
				}
				else
				{
					IsoRepID = StringUtil.toString(rsGetReport.getString("rep_id"));
				}
			}
			rsGetReport.close();
			pstmt.close();
			pstmt = null;
		}
		catch (Exception e)
		{
			cat.error("Error during getIsoRepID", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return IsoRepID;
	}
	
	public static Vector<Vector<String>> getReportACH(SessionData sessionData, String IsoID,
			String AgentID, String SubAgentID, String RepID, String MerchantID, String StartDate,
			String EndDate) throws TransactionException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		try
		{
			IsoID = StringUtil.escape(IsoID);
			AgentID = StringUtil.escape(AgentID);
			SubAgentID = StringUtil.escape(SubAgentID);
			RepID = StringUtil.escape(RepID);
			MerchantID = StringUtil.escape(MerchantID);
			StartDate = StringUtil.escape(StartDate);
			EndDate = StringUtil.escape(EndDate);
			
			if (IsoID.trim().equals("") && AgentID.trim().equals("")
					&& SubAgentID.trim().equals("") && RepID.trim().equals("")
					&& MerchantID.trim().equals(""))
			{
				return vecResultList;
			}
			if (IsoID.equals("-2") && AgentID.equals("-2") && SubAgentID.equals("-2")
					&& RepID.equals("-2") && MerchantID.equals("-2"))
			{
				return vecResultList;
			}
			
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			
			String strIDs = "";
			String strIsoID = "";
			String strAgentID = "";
			String strSubAgentID = "";
			String strRepID = "";
			String strMerchantID = "";
			
			if ((IsoID.equals("-2")) || (IsoID.trim().equals("")))
			{
				strIsoID = "";
			}
			else
			{
				strIsoID = IsoID;
			}
			if ((AgentID.equals("-2")) || (AgentID.trim().equals("")))
			{
				strAgentID = "";
			}
			else
				if (AgentID.equals("-1"))
				{
					strAgentID = "";
					Vector<Vector<String>> vecAgents = getAgentList(sessionData, "");
					Iterator<Vector<String>> itAL = vecAgents.iterator();
					while (itAL.hasNext())
					{
						Vector<String> vecTempAL = null;
						vecTempAL = itAL.next();
						strAgentID += vecTempAL.get(0) + ",";
					}
					if (!strAgentID.equals(""))
						strAgentID = strAgentID.substring(0, (strAgentID.length() - 1));
				}
				else
				{
					strAgentID = AgentID;
				}
			if ((SubAgentID.equals("-2")) || (SubAgentID.trim().equals("")))
			{
				strSubAgentID = "";
			}
			else
				if (SubAgentID.equals("-1"))
				{
					strSubAgentID = "";
					Vector<Vector<String>> vecSubAgentID = null;
					if (AgentID.equals("-1"))
					{
						vecSubAgentID = getSubAgentList(sessionData, "");
					}
					else
					{
						vecSubAgentID = getSubAgentList(sessionData, strAgentID);
					}
					Iterator<Vector<String>> itSAL = vecSubAgentID.iterator();
					while (itSAL.hasNext())
					{
						Vector<String> vecTempSAL = null;
						vecTempSAL = itSAL.next();
						strSubAgentID += vecTempSAL.get(0) + ",";
					}
					if (!strSubAgentID.equals(""))
						strSubAgentID = strSubAgentID.substring(0, (strSubAgentID.length() - 1));
				}
				else
				{
					strSubAgentID = SubAgentID;
				}
			if ((RepID.equals("-2")) || (RepID.trim().equals("")))
			{
				strRepID = "";
			}
			else
				if (RepID.equals("-1"))
				{
					Vector<Vector<String>> vecRepID = null;
					if (SubAgentID.equals("-1"))
					{
						vecRepID = getRepList(sessionData, "");
					}
					else
					{
						vecRepID = getRepList(sessionData, strSubAgentID);
					}
					strRepID = "";
					Iterator<Vector<String>> itRL = vecRepID.iterator();
					while (itRL.hasNext())
					{
						Vector<String> vecTempRL = null;
						vecTempRL = itRL.next();
						strRepID += vecTempRL.get(0) + ",";
					}
					if (!strRepID.equals(""))
						strRepID = strRepID.substring(0, (strRepID.length() - 1));
				}
				else
				{
					strRepID = RepID;
				}
			if ((MerchantID.equals("-2")) || (MerchantID.trim().equals("")))
			{
				strMerchantID = "";
			}
			else
				if (MerchantID.equals("-1"))
				{
					Vector<Vector<String>> vecMerchantID = null;
					if (RepID.equals("-1"))
					{
						vecMerchantID = getMerchantList(sessionData, "");
					}
					else
					{
						vecMerchantID = getMerchantList(sessionData, strRepID);
					}
					strMerchantID = "";
					Iterator<Vector<String>> itML = vecMerchantID.iterator();
					while (itML.hasNext())
					{
						Vector<String> vecTempML = null;
						vecTempML = itML.next();
						strMerchantID += vecTempML.get(0) + ",";
					}
					if (!strMerchantID.equals(""))
						strMerchantID = strMerchantID.substring(0, (strMerchantID.length() - 1));
				}
				else
				{
					strMerchantID = MerchantID;
				}
			
			if (!strIsoID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strIsoID) + ",";
			}
			if (!strAgentID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strAgentID) + ",";
			}
			if (!strSubAgentID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strSubAgentID) + ",";
			}
			if (!strRepID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strRepID) + ",";
			}
			if (!strMerchantID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strMerchantID) + ",";
			}
			if (!strIDs.equals(""))
				strIDs = strIDs.substring(0, (strIDs.length() - 1));
			
			String strSQLGetReport = sql_bundle.getString("getACHSummary");
			
			// SELECT rec_id,
			// datetime statementDate,
			// merchant_Id,
			// entity_name = CASE WHEN (SELECT businessname FROM reps (nolock) WHERE rep_id =
			// a.merchant_id) IS null
			// THEN (SELECT dba FROM merchants (nolock) WHERE merchant_id = a.merchant_id)
			// ELSE (SELECT businessname FROM reps (nolock) WHERE rep_id = a.merchant_id) END,
			// entity_type = CASE (SELECT type FROM reps (nolock) WHERE rep_id = a.merchant_id)
			// WHEN '1' THEN 'Rep'
			// WHEN '2' THEN 'Iso'
			// WHEN '3' THEN 'Iso'
			// WHEN '4' THEN 'Agent'
			// WHEN '5' THEN 'SubAgent'
			// ELSE 'Merchant' END,
			// startDate,
			// endDate,
			// amount,
			// description,
			// ach_status = CASE WHEN transmit_date is null THEN 'ACH Pending'
			// ELSE 'ACH Sent' END
			// FROM ach_transactions a (nolock)
			// WHERE datetime >= '' AND datetime <= ''
			// AND merchant_id IN ( '' )
			// ORDER BY entity_type, startdate
			
			String refidSwitchParam = " entity_type = CASE (SELECT p.type FROM reps AS p (NOLOCK) WHERE p.rep_id = a.merchant_id) "
					+ "WHEN \'1\' THEN \'Rep\' "
					+ "WHEN \'2\' THEN \'Iso\' "
					+ "WHEN \'3\' THEN \'Iso\' "
					+ "WHEN \'4\' THEN \'Agent\' "
					+ "WHEN \'5\' THEN \'SubAgent\' " + "ELSE \'Merchant\' END ";
			
			String achTypeSwithParam = " ach_status = CASE "
					+ "WHEN a.transmit_date is null THEN \'ACH Pending\' " + "ELSE \'ACH Sent\' END ";
			
			String strWhereParam = "";
			if (!DateUtil.isValid_MMddyyyy(StartDate) || !DateUtil.isValid_MMddyyyy(EndDate))
			{
				cat.warn("GetACH summary - Wrong date format! Check " + StartDate + " or "
						+ EndDate + " (It was expected MM/dd/yyyy). Defaulting to NULL.");
				// we imply with this that rowset returned should be empty in this case
				strWhereParam = " isnull(a.isPaidByOLdBilling,1)=1 and datetime IS NULL ";
			}
			else
			{
				strWhereParam = " isnull(a.isPaidByOLdBilling,1)=1 and a.datetime >= \'" + StartDate + "\' AND a.datetime <= \'" + EndDate 	+ "\' ";
			}
			
			if (!strIDs.equals(""))
			{
				strWhereParam += " AND a.merchant_id IN ( " + strIDs + " ) ";
			}
			
			Object[] args = new Object[]
			{ refidSwitchParam, achTypeSwithParam, strWhereParam };
			strSQLGetReport = java.text.MessageFormat.format(strSQLGetReport, args);
			cat.debug("GetACH summary sql query: " + strSQLGetReport);
			
			pstmt = dbConn.prepareStatement(strSQLGetReport);
			ResultSet rsGetReport = pstmt.executeQuery();
			while (rsGetReport.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(0, rsGetReport.getString("statementDate"));
				vecTemp.add(1, StringUtil.toString(rsGetReport.getString("entity_type")));
				vecTemp.add(2, StringUtil.toString(rsGetReport.getString("entity_name")));// used
																							// in
																							// summary_result_details.jsp
				String sStartDate = DateUtil.formatDate(rsGetReport.getTimestamp("startDate"));
				String sEndDate = DateUtil.formatDate(rsGetReport.getTimestamp("endDate"));
				String period = Languages.getString("jsp.admin.ach.summary.from", sessionData.getLanguage()) + " "
						+ sStartDate + " " + Languages.getString("jsp.admin.ach.summary.to", sessionData.getLanguage()) + " "
						+ sEndDate;
				vecTemp.add(3, period);
				vecTemp.add(4, rsGetReport.getString("amount"));// used in
																// summary_result_details.jsp
				vecTemp.add(5, StringUtil.toString(rsGetReport.getString("merchant_id")));// used
																							// in
																							// summary_result_details.jsp
				vecTemp.add(6, DateUtil.formatDate(rsGetReport.getTimestamp("startDate")));// used
																							// in
																							// summary_result_details.jsp
				vecTemp.add(7, DateUtil.formatDate(rsGetReport.getTimestamp("endDate")));// used
																							// in
																							// summary_result_details.jsp
				vecTemp.add(8, StringUtil.toString(rsGetReport.getString("description")));// used
																							// in
																							// summary_result_details.jsp
				vecTemp.add(9, StringUtil.toString(rsGetReport.getString("ach_status")));

				vecResultList.add(vecTemp);
			}
			rsGetReport.close();
			pstmt.close();
			pstmt = null;
		}
		catch (Exception e)
		{
			cat.error("Error during getReportACH", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}
	public static Vector<Vector<String>> getReportACHBilling(SessionData sessionData, String IsoID,
			String AgentID, String SubAgentID, String RepID, String MerchantID, String StartDate,
			String EndDate) throws TransactionException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		try
		{
			IsoID = StringUtil.escape(IsoID);
			AgentID = StringUtil.escape(AgentID);
			SubAgentID = StringUtil.escape(SubAgentID);
			RepID = StringUtil.escape(RepID);
			MerchantID = StringUtil.escape(MerchantID);
			StartDate = StringUtil.escape(StartDate);
			EndDate = StringUtil.escape(EndDate);
			
			
			SimpleDateFormat fmrt = new SimpleDateFormat("MM/dd/yyyy");
			Date endDate = fmrt.parse(EndDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(endDate);
	        cal.add(Calendar.DATE, 1);
	        EndDate = fmrt.format(cal.getTime());
	        
			if (IsoID.trim().equals("") && AgentID.trim().equals("")
					&& SubAgentID.trim().equals("") && RepID.trim().equals("")
					&& MerchantID.trim().equals(""))
			{
				return vecResultList;
			}
			if (IsoID.equals("-2") && AgentID.equals("-2") && SubAgentID.equals("-2")
					&& RepID.equals("-2") && MerchantID.equals("-2"))
			{
				return vecResultList;
			}
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			
			String strIDs = "";
			String strIsoID = "";
			String strAgentID = "";
			String strSubAgentID = "";
			String strRepID = "";
			String strMerchantID = "";
			
			if ((IsoID.equals("-2")) || (IsoID.trim().equals("")))
			{
				strIsoID = "";
			}
			else
			{
				strIsoID = IsoID;
			}
			if ((AgentID.equals("-2")) || (AgentID.trim().equals("")))
			{
				strAgentID = "";
			}
			else
				if (AgentID.equals("-1"))
				{
					strAgentID = "";
					Vector<Vector<String>> vecAgents = getAgentList(sessionData, "");
					Iterator<Vector<String>> itAL = vecAgents.iterator();
					while (itAL.hasNext())
					{
						Vector<String> vecTempAL = null;
						vecTempAL = itAL.next();
						strAgentID += vecTempAL.get(0) + ",";
					}
					if (!strAgentID.equals(""))
						strAgentID = strAgentID.substring(0, (strAgentID.length() - 1));
				}
				else
				{
					strAgentID = AgentID;
				}
			if ((SubAgentID.equals("-2")) || (SubAgentID.trim().equals("")))
			{
				strSubAgentID = "";
			}
			else
				if (SubAgentID.equals("-1"))
				{
					strSubAgentID = "";
					Vector<Vector<String>> vecSubAgentID = null;
					if (AgentID.equals("-1"))
					{
						vecSubAgentID = getSubAgentList(sessionData, "");
					}
					else
					{
						vecSubAgentID = getSubAgentList(sessionData, strAgentID);
					}
					Iterator<Vector<String>> itSAL = vecSubAgentID.iterator();
					while (itSAL.hasNext())
					{
						Vector<String> vecTempSAL = null;
						vecTempSAL = itSAL.next();
						strSubAgentID += vecTempSAL.get(0) + ",";
					}
					if (!strSubAgentID.equals(""))
						strSubAgentID = strSubAgentID.substring(0, (strSubAgentID.length() - 1));
				}
				else
				{
					strSubAgentID = SubAgentID;
				}
			if ((RepID.equals("-2")) || (RepID.trim().equals("")))
			{
				strRepID = "";
			}
			else
				if (RepID.equals("-1"))
				{
					Vector<Vector<String>> vecRepID = null;
					if (SubAgentID.equals("-1"))
					{
						vecRepID = getRepList(sessionData, "");
					}
					else
					{
						vecRepID = getRepList(sessionData, strSubAgentID);
					}
					strRepID = "";
					Iterator<Vector<String>> itRL = vecRepID.iterator();
					while (itRL.hasNext())
					{
						Vector<String> vecTempRL = null;
						vecTempRL = itRL.next();
						strRepID += vecTempRL.get(0) + ",";
					}
					if (!strRepID.equals(""))
						strRepID = strRepID.substring(0, (strRepID.length() - 1));
				}
				else
				{
					strRepID = RepID;
				}
			if ((MerchantID.equals("-2")) || (MerchantID.trim().equals("")))
			{
				strMerchantID = "";
			}
			else
				if (MerchantID.equals("-1"))
				{
					Vector<Vector<String>> vecMerchantID = null;
					if (RepID.equals("-1"))
					{
						vecMerchantID = getMerchantList(sessionData, "");
					}
					else
					{
						vecMerchantID = getMerchantList(sessionData, strRepID);
					}
					strMerchantID = "";
					Iterator<Vector<String>> itML = vecMerchantID.iterator();
					while (itML.hasNext())
					{
						Vector<String> vecTempML = null;
						vecTempML = itML.next();
						strMerchantID += vecTempML.get(0) + ",";
					}
					if (!strMerchantID.equals(""))
						strMerchantID = strMerchantID.substring(0, (strMerchantID.length() - 1));
				}
				else
				{
					strMerchantID = MerchantID;
				}
			
			if (!strIsoID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strIsoID) + ",";
			}
			if (!strAgentID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strAgentID) + ",";
			}
			if (!strSubAgentID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strSubAgentID) + ",";
			}
			if (!strRepID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strRepID) + ",";
			}
			if (!strMerchantID.trim().equals(""))
			{
				strIDs += StringUtil.escape(strMerchantID) + ",";
			}
			if (!strIDs.equals(""))
				strIDs = strIDs.substring(0, (strIDs.length() - 1));
			
			String strSQLGetReport = sql_bundle.getString("getACHBillingSummary");
			
			// SELECT rec_id,
			// datetime statementDate,
			// merchant_Id,
			// entity_name = CASE WHEN (SELECT businessname FROM reps (nolock) WHERE rep_id =
			// a.merchant_id) IS null
			// THEN (SELECT dba FROM merchants (nolock) WHERE merchant_id = a.merchant_id)
			// ELSE (SELECT businessname FROM reps (nolock) WHERE rep_id = a.merchant_id) END,
			// entity_type = CASE (SELECT type FROM reps (nolock) WHERE rep_id = a.merchant_id)
			// WHEN '1' THEN 'Rep'
			// WHEN '2' THEN 'Iso'
			// WHEN '3' THEN 'Iso'
			// WHEN '4' THEN 'Agent'
			// WHEN '5' THEN 'SubAgent'
			// ELSE 'Merchant' END,
			// startDate,
			// endDate,
			// amount,
			// description,
			// ach_status = CASE WHEN transmit_date is null THEN 'ACH Pending'
			// ELSE 'ACH Sent' END
			// FROM ach_transactions a (nolock)
			// WHERE datetime >= '' AND datetime <= ''
			// AND merchant_id IN ( '' )
			// ORDER BY entity_type, startdate
			
			String refidSwitchParam = " entity_type = CASE (SELECT type FROM reps (nolock) WHERE rep_id = a.entityId) "
					+ "WHEN \'1\' THEN \'Rep\' "
					+ "WHEN \'2\' THEN \'Iso\' "
					+ "WHEN \'3\' THEN \'Iso\' "
					+ "WHEN \'4\' THEN \'Agent\' "
					+ "WHEN \'5\' THEN \'SubAgent\' " + "ELSE \'Merchant\' END ";
			
			
			String achTypeSwithParam = " ach_status = CASE(SELECT COUNT(b.billingId) FROM billing AS b WHERE b.paymentBatchEntityId = a.paymentBatchEntityId AND b.transmitDate IS NULL) "
					+ "WHEN 0 THEN \'ACH Sent\' " + "ELSE \'ACH Pending\' END,";
			
			String statementSwitchParam = " statement_status = CASE "
				+ "WHEN a.generationDate IS NULL THEN \'Statement Pending\' " + "ELSE \'Statement Sent\' END ";
			
			String strWhereParam = "(p.paymenTypeId = 2 or p.paymenTypeId = 8 OR p.paymenTypeId = 6 OR p.paymenTypeId = 9)";
			if (!DateUtil.isValid_MMddyyyy(StartDate) || !DateUtil.isValid_MMddyyyy(EndDate))
			{
				cat.warn("GetACH summary - Wrong date format! Check " + StartDate + " or "
						+ EndDate + " (It was expected MM/dd/yyyy). Defaulting to NULL.");
				// we imply with this that rowset returned should be empty in this case
				strWhereParam = strWhereParam + " AND datetime IS NULL ";
			}
			else
			{
				strWhereParam = strWhereParam + " AND a.processEndDate >= \'" + StartDate + "\' AND a.processEndDate <= \'" + EndDate 	+ "\' ";
			}
			
			if (!strIDs.equals(""))
			{
				strWhereParam += " AND a.entityId IN ( " + strIDs + " ) ";
			}
			
			Object[] args = new Object[]
			{ refidSwitchParam, achTypeSwithParam, statementSwitchParam, strWhereParam };
			strSQLGetReport = java.text.MessageFormat.format(strSQLGetReport, args);
			cat.debug("GetACHBilling summary sql query: " + strSQLGetReport);
			
			pstmt = dbConn.prepareStatement(strSQLGetReport);
			ResultSet rsGetReport = pstmt.executeQuery();
			while (rsGetReport.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(0, rsGetReport.getString("statementDate"));
				vecTemp.add(1, StringUtil.toString(rsGetReport.getString("entity_type")));
				vecTemp.add(2, StringUtil.toString(rsGetReport.getString("entity_name")));// used
																							// in
																							// summary_result_details.jsp
				String sStartDate = DateUtil.formatDate(rsGetReport.getTimestamp("startDate"));
				String sEndDate = DateUtil.formatDate(rsGetReport.getTimestamp("endDate"));
				String period = Languages.getString("jsp.admin.ach.summary.from", sessionData.getLanguage()) + " "
						+ sStartDate + " " + Languages.getString("jsp.admin.ach.summary.to", sessionData.getLanguage()) + " "
						+ sEndDate;
				vecTemp.add(3, period);
				vecTemp.add(4, rsGetReport.getString("amount"));// used in summary_result_details.jsp
				vecTemp.add(5, StringUtil.toString(rsGetReport.getString("merchant_id")));// used in summary_result_details.jsp
				vecTemp.add(6, DateUtil.formatDate(rsGetReport.getTimestamp("startDate")));// used in summary_result_details.jsp
				vecTemp.add(7, DateUtil.formatDate(rsGetReport.getTimestamp("endDate")));// used in summary_result_details.jsp
				vecTemp.add(8, StringUtil.toString(rsGetReport.getString("description")));// used in summary_result_details.jsp
				vecTemp.add(9, StringUtil.toString(rsGetReport.getString("ach_status")));
				
				vecTemp.add(10, StringUtil.toString(rsGetReport.getString("statement_status")));

				vecTemp.add(11, StringUtil.toString(rsGetReport.getString("paymentBatchEntityId")));// used in summary_result_details.jsp
				vecTemp.add(12, StringUtil.toString(rsGetReport.getString("paymentGrouping"))); // used in summary_result_details.jsp
				vecResultList.add(vecTemp);
			}
			rsGetReport.close();
			pstmt.close();
			pstmt = null;
		}
		catch (Exception e)
		{
			cat.error("Error during getReportACH", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}//getReportACHBilling
	
	@Deprecated
	public static Vector<Vector<String>> getACHTransactionType(String MerchantID, String StartDate, String EndDate)
			throws TransactionException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		try
		{
			MerchantID = StringUtil.escape(MerchantID);
			StartDate = StringUtil.escape(StartDate);
			EndDate = StringUtil.escape(EndDate);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			
			String strSQLGetReport = sql_bundle.getString("getACHTransactionType");
			
			cat.debug(strSQLGetReport + ";/* MerchantID = " + MerchantID + ", startdate = '" + StartDate + ", enddate = '" + EndDate + "'");
			pstmt = dbConn.prepareStatement(strSQLGetReport);
			pstmt.setString(1, MerchantID);
			pstmt.setString(2, StartDate);
			pstmt.setString(3, EndDate);
			ResultSet rsGetReport = pstmt.executeQuery();
			while (rsGetReport.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rsGetReport.getString("transaction_type")));
				vecTemp.add(StringUtil.toString(rsGetReport.getString("viewtype")));
				vecResultList.add(vecTemp);
			}
			rsGetReport.close();
			pstmt.close();
			pstmt = null;
		}
		catch (Exception e)
		{
			cat.error("Error during getReportACH", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}
	
	public static Hashtable<String, Vector<Vector<String>>> getReportDetailACH(String MerchantID,
			String description, String processDate, String startDate, String endDate)
			throws TransactionException
	{
		Connection dbConn = null;
		// this should regroup all ach_transaction_audit entries given their viewtype and
		// transaction type:
		// the values in the hashtable are the vector of entries having the same viewtype and
		// transaction type.
		Hashtable<String, Vector<Vector<String>>> htResultList = new Hashtable<String, Vector<Vector<String>>>();
		try
		{
			
			MerchantID = StringUtil.escape(MerchantID);
			description = StringUtil.escape(description);
			processDate = StringUtil.escape(processDate);
			startDate = StringUtil.escape(startDate);
			endDate = StringUtil.escape(endDate);
			
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			PreparedStatement pstmt = null;
			
			String strSQLGetReport = sql_bundle.getString("getReportDetailACH");
			
			// SELECT
			// ata.iso_name,
			// transaction_type = CASE WHEN ata.transaction_type IS NULL THEN 'NULL'
			// ELSE ata.transaction_type END,
			// ata.datetime,
			// ata.merch_name,
			// ata.sku,
			// ata.proddesc,
			// ata.amount,
			// ata.discount,
			// ata.ach_amount,
			// viewtype = CASE WHEN ata.viewtype IS NULL THEN 0
			// ELSE ata.viewtype END,
			// ata.Process_date,
			// ata.entity_id
			// FROM ach_transaction_audit ata (nolock)
			// WHERE ata.entity_id = '<ENTITYID>'
			// AND ata.process_date = '<DATE>' -- NO further filter for weekly fees;
			// AND ata.viewtype IN (11, 12, 13) -- for monthly fees
			// --OR THIS FILTER FOR OTHER CASES: AND ata.startDate='<startDate>' AND
			// ata.endDate='<endDate>'
			// ORDER BY ata.TRANSACTION_TYPE, ata.datetime
			
			String transtypeParam = " transaction_type = CASE WHEN ata.transaction_type IS NULL THEN \'NULL\' ELSE ata.transaction_type END ";
			
			String filterParam = "";
			if (!DateUtil.isValid_yyyyMMddHHmmssSSS(processDate))
			{
				cat.warn("GetACH summary details - Wrong date format! Check " + processDate
						+ " (It was expected yyyy-MM-dd HH:mm:ss.SSS). Defaulting to NULL.");
				filterParam = " ata.process_date IS NULL ";
			}
			else
			{
				filterParam = " ata.process_date = \'" + processDate + "\' ";
			}
			// Conditional filter depending on [ach_transactions].[description]:
			// for Weekly Fees leave only [process_date] as filter;
			// for Monthly Fees also filter using [viewtype];
			// Other cases filter by [startDate] and [endDate].
			//
			// Why? start/end dates are different for monthly fees; and using start/end dates
			// or description won't retrieve weekly fees. All other cases work with start/end dates.
			if (description != null)
			{
				if (description.indexOf("Weekly Fee") != -1)
				{
					// nothing to do, process_date seems good enough filter in these cases
				}
				else
					if (description.indexOf("Monthly Fee") != -1)
					{
						// filterParam += " AND description LIKE \'" + description.trim() + "%\' ";
						filterParam += " AND ata.viewtype IN (11, 12, 13) ";
					}
					else
						if (description.indexOf("PPC ISO Buy Rate") != -1
								|| description.indexOf("Debisys Buy Rate") != -1
								|| description.indexOf("ISO Commission Rate") != -1)
						{
							filterParam += " AND ata.description like \'" + description + "%\' ";
						}
						else
						{
							if (!DateUtil.isValid_MMddyyyy(startDate)
									|| !DateUtil.isValid_MMddyyyy(endDate))
							{
								cat.warn("GetACH summary details - Wrong date format! Check "
										+ startDate + " or " + endDate
										+ " (It was expected MM/dd/yyyy). Defaulting to NULL.");
								filterParam += " AND ata.startDate IS NULL ";
							}
							else
							{
								filterParam += " AND ata.startDate=\'" + startDate
										+ "\' AND ata.endDate=\'" + endDate + "\' ";
							}
							
							if (description.indexOf("PPC Merchant Buy Rate") != -1 || description.indexOf("PPC Merchant MTCorpus") != -1)
							{
								filterParam += "AND ata.description like '%" + description.trim() + "%' "; 
							}
						}
			}
			
			Object[] args = new Object[]
			{ transtypeParam, filterParam };
			strSQLGetReport = java.text.MessageFormat.format(strSQLGetReport, args);
			cat.debug("getReportDetailACH SQL query: " + strSQLGetReport);
			
			pstmt = dbConn.prepareStatement(strSQLGetReport);
			pstmt.setString(1, MerchantID);
			
			ResultSet rsGetReport = pstmt.executeQuery();
			while (rsGetReport.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(0, StringUtil.toString(rsGetReport.getString("iso_name")));
				String transactionType = StringUtil.toString(rsGetReport
						.getString("transaction_type"));
				vecTemp.add(1, transactionType);
				vecTemp.add(2, StringUtil.toString(rsGetReport.getString("datetime")));
				vecTemp.add(3, StringUtil.toString(rsGetReport.getString("merch_name")));
				vecTemp.add(4, StringUtil.toString(rsGetReport.getString("sku")));
				vecTemp.add(5, StringUtil.toString(rsGetReport.getString("proddesc")));		        
				vecTemp.add(6, rsGetReport.getString("amount"));
				vecTemp.add(7, rsGetReport.getString("discount"));
				vecTemp.add(8, rsGetReport.getString("ACH_amount"));
				String viewType = StringUtil.toString(rsGetReport.getString("viewtype"));
				vecTemp.add(9, viewType);
				vecTemp.add(10, StringUtil.toString(rsGetReport.getString("Process_Date")));
				vecTemp.add(11, StringUtil.toString(rsGetReport.getString("entity_id")));
				if (rsGetReport.getString("rec_id") != null)
				{
					vecTemp.add(12, StringUtil.toString(rsGetReport.getString("rec_id")));
				}else{
					vecTemp.add(12, "N/A");
				}
				
				// RB: no need to pack following 3 figures again if we already have'em unformatted
				// for some more math
				// vecTemp.add(12, StringUtil.toString(rsGetReport.getString("amount")));
				// vecTemp.add(13, StringUtil.toString(rsGetReport.getString("discount")));
				// vecTemp.add(14, StringUtil.toString(rsGetReport.getString("ACH_amount")));
				
				Vector<Vector<String>> entriesPerViewType = new Vector<Vector<String>>();
				String htkey = viewType + "_" + transactionType;
				if (htResultList.containsKey(htkey))
				{
					entriesPerViewType = htResultList.get(htkey);
				}
				entriesPerViewType.add(vecTemp);
				htResultList.put(htkey, entriesPerViewType);
			}
			// cat.debug("getReportDetailACH search returned " + htResultList.size() + " entries.");
			rsGetReport.close();
			pstmt.close();
			pstmt = null;
		}
		catch (Exception e)
		{
			cat.error("Error during getReportACH", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return htResultList;
	}
	
	public Vector<Object> getTransactions(int intPageNumber, int intRecordsPerPage,
			SessionData sessionData, int transactionType) throws TransactionException
	{
		Connection dbConn = null;
		Vector<Object> vecTransactions = new Vector<Object>();
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			// get a count of total matches
			PreparedStatement pstmt = null;
			String strRefId = StringUtil.escape(sessionData.getProperty("ref_id"));
			String strAccessLevel = sessionData.getProperty("access_level");
			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strSQL = "SELECT a.rec_id, a.transmit_date, a.description, at.description AS type, (a.amount * -1) AS amount, a.startdate, a.enddate, ";
			if (transactionType == 5){
				strSQL += " mr.merchant_id, mr.dba ";
			}else{
				strSQL += " r.rep_id, r.businessname ";
			}
			strSQL += " FROM ach_transactions AS a WITH(NOLOCK) INNER JOIN ach_types WITH(nolock) ON a.type = ach_types.type ";
			String strSQLWhere = "";
			switch (transactionType){
				case 1:// iso
					if (strAccessLevel.equals(DebisysConstants.ISO)){
						strSQLWhere = " a.merchant_id =" + strRefId;
					}
					strSQL += " INNER JOIN reps AS r WITH(NOLOCK) ON a.merchant_id = reps.rep_id ";
					break;
				case 2:// agent
					if (strAccessLevel.equals(DebisysConstants.ISO)){
						strSQLWhere = " a.merchant_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type=" + DebisysConstants.REP_TYPE_AGENT + " AND r1.iso_id=" + strRefId + ") ";
					}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
						strSQLWhere = " a.merchant_id =" + strRefId;
					}
					strSQL += " INNER JOIN reps AS r WITH(NOLOCK) ON a.merchant_id = r.rep_id ";
					break;
				
				case 3:// subagent
					if (strAccessLevel.equals(DebisysConstants.ISO)){
						strSQLWhere = " a.merchant_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND r1.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type = " + DebisysConstants.REP_TYPE_AGENT + " AND r2.iso_id=" + strRefId + "))";
					}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
						strSQLWhere = " a.merchant_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND r1.iso_id = " + strRefId + ")";
					}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
						strSQLWhere = " a.merchant_id=" + strRefId;
					}
					strSQL += " INNER JOIN reps AS r WITH(NOLOCK) ON a.merchant_id = r.rep_id ";
					break;
				
				case 4:// rep
					if (strAccessLevel.equals(DebisysConstants.ISO)){
						if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
							strSQLWhere = strSQLWhere + " a.merchant_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type = " + DebisysConstants.REP_TYPE_REP + " AND r1.iso_id= " + strRefId + ")";
						}else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
							strSQLWhere = strSQLWhere + " a.merchant_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type = " + DebisysConstants.REP_TYPE_REP + " AND r1.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND r2.iso_id IN (SELECT r3.rep_id FROM reps AS r3 WITH(NOLOCK) WHERE r3.type = " + DebisysConstants.REP_TYPE_AGENT + " AND r3.iso_id=" + strRefId + ")))";
						}
					}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
						strSQLWhere = strSQLWhere + " a.merchant_id IN (SELECT r1.rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type=" + DebisysConstants.REP_TYPE_REP + " AND r1.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND r2.iso_id = " + strRefId + "))";
					}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
						strSQLWhere = strSQLWhere + " a.merchant_id IN (SELECT rep_id FROM reps AS r1 WITH(NOLOCK) WHERE r1.type=" + DebisysConstants.REP_TYPE_REP + " AND r1.iso_id=" + strRefId + ")";
					}else if (strAccessLevel.equals(DebisysConstants.REP)){
						strSQLWhere = " a.merchant_id=" + strRefId;
					}
					strSQL += " INNER JOIN reps AS r WITH(NOLOCK) ON a.merchant_id = r.rep_id ";
					break;
				case 5:
					// merchant
					if (strAccessLevel.equals(DebisysConstants.ISO)){
						if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
							strSQLWhere = " a.merchant_id IN (SELECT m.merchant_id FROM reps AS r1 WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r.rep_id WHERE r.type = " + DebisysConstants.REP_TYPE_REP + " AND r.iso_id= " + strRefId + ")";
						}else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
							strSQLWhere = " a.merchant_id IN (SELECT m.merchant_id FROM reps AS r1 WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r1.rep_id WHERE r1.type = " + DebisysConstants.REP_TYPE_REP + " AND r1.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND r2.iso_id IN (SELECT r3.rep_id FROM reps AS r3 WITH(NOLOCK) WHERE r3.type = " + DebisysConstants.REP_TYPE_AGENT + " AND r3.iso_id = " + strRefId + ")))";
						}
					}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
						strSQLWhere = " a.merchant_id IN (SELECT m.merchant_id FROM reps AS r1 WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r1.rep_id WHERE r1.type=" + DebisysConstants.REP_TYPE_REP + " AND r1.iso_id IN (SELECT rep_id FROM reps AS r2 WITH(NOLOCK) WHERE r2.type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND r2.iso_id=" + strRefId + "))";
					}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
						strSQLWhere = " a.merchant_id IN (SELECT m.merchant_id FROM reps AS r1 WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r1.rep_id WHERE r1.type=" + DebisysConstants.REP_TYPE_REP + " AND r1.iso_id = " + strRefId + ")";
					}else if (strAccessLevel.equals(DebisysConstants.REP)){
						strSQLWhere = " a.merchant_id IN (SELECT m.merchant_id FROM reps AS r1 WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r1.rep_id WHERE r1.type=" + DebisysConstants.REP_TYPE_REP + " AND r1.rep_id = " + strRefId + ")";
					}else if (strAccessLevel.equals(DebisysConstants.MERCHANT)){
						strSQLWhere = " a.merchant_id= " + strRefId;
					}
					strSQL += " INNER JOIN merchants AS mr WITH(NOLOCK) ON a.merchant_id = mr.merchant_id ";
					break;
			}
			
			strSQLWhere += " WHERE a.startdate = '" + this.start_date + "' AND a.enddate = '" + this.end_date + "' AND a.status=1 ORDER BY " + (transactionType == 5 ? "dba" :"businessname") + ", a.rec_id";
			pstmt = dbConn.prepareStatement(strSQL + strSQLWhere, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			cat.debug(strSQL + strSQLWhere);
			ResultSet rs = pstmt.executeQuery();
			int intRecordCount = DbUtil.getRecordCount(rs);
			// first row is always the count
			vecTransactions.add(Integer.valueOf(intRecordCount));
			if (intRecordCount > 0)
			{
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount,
						intPageNumber));
				for (int i = 0; i < intRecordsPerPage; i++)
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("rec_id")));
					vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("transmit_date")));
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("type")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
					vecTemp.add(DateUtil.formatDate(rs.getTimestamp("startdate")));
					vecTemp.add(DateUtil.formatDate(rs.getTimestamp("enddate")));
					if (transactionType == 5)
					{
						vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
						vecTemp.add(StringUtil.toString(rs.getString("dba")));
					}
					else
					{
						vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
						vecTemp.add(StringUtil.toString(rs.getString("businessname")));
						
					}
					
					vecTransactions.add(vecTemp);
					if (rs.isLast())
					{
						break;
					}
					rs.next();
				}
			}
			pstmt.close();
			rs.close();
			
		}
		catch (Exception e)
		{
			cat.error("Error during search", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecTransactions;
	}
public static Hashtable<String, Vector<Vector<String>>> getReportDetailACHBilling(long paymentBatchEntityId)
throws TransactionException
{
Connection dbConn = null;
// this should regroup all billing rows given their statementType and paymentBatchEntity:
// the values in the hashtable are the vector of entries having the same statementType and
// transaction type.
Hashtable<String, Vector<Vector<String>>> htResultList = new Hashtable<String, Vector<Vector<String>>>();
try
{
	
	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	if (dbConn == null)
	{
		cat.error("Can't get database connection");
		throw new TransactionException();
	}
	PreparedStatement pstmt = null;
	
	String strSQLGetReport = sql_bundle.getString("getReportDetailACHBilling");
	
	String transtypeParam = " transaction_type = CASE WHEN b.lineTypeItemId=8 THEN 'FMR' WHEN w.transaction_type IS NULL THEN \'NULL\' ELSE w.transaction_type END ";
	String productDescParam = "proddesc = CASE WHEN b.lineTypeItemId=8 THEN \'FMR Commission\' WHEN p.description IS NULL THEN b.description ELSE p.description END";
	String transtypeDescParam = " transaction_type_desc = CASE WHEN b.lineTypeItemId=8 THEN 'FMR' WHEN tt.description IS NULL THEN \'NULL\' ELSE tt.description END ";
	
	Object[] args = new Object[]
	{ transtypeParam, productDescParam,transtypeDescParam };
	strSQLGetReport = java.text.MessageFormat.format(strSQLGetReport, args);
	cat.debug("getReportDetailACHBilling SQL query: " + strSQLGetReport);
	cat.debug("paymentBatchEntityId: " + paymentBatchEntityId);
	
	pstmt = dbConn.prepareStatement(strSQLGetReport);
	pstmt.setLong(1, paymentBatchEntityId);
	
	ResultSet rsGetReport = pstmt.executeQuery();
	while (rsGetReport.next())
	{
		Vector<String> vecTemp = new Vector<String>();
		String viewType = StringUtil.toString(rsGetReport.getString("statementTypeId"));
		vecTemp.add(0, StringUtil.toString(rsGetReport.getString("iso_name")));
                String transactionType = rsGetReport.getString("detailType");
		vecTemp.add(1, transactionType);
                
		vecTemp.add(2, StringUtil.toString(rsGetReport.getString("datetime")));
		vecTemp.add(3, StringUtil.toString(rsGetReport.getString("merch_name")));
		vecTemp.add(4, StringUtil.toString(rsGetReport.getString("sku")));
		vecTemp.add(5, StringUtil.toString(rsGetReport.getString("proddesc"))
				+ " " + StringUtil.toString(rsGetReport.getString("statementLabel")));		        
		vecTemp.add(6, rsGetReport.getString("amount"));
		vecTemp.add(7, rsGetReport.getString("discount"));
		vecTemp.add(8, rsGetReport.getString("ACH_amount"));
		vecTemp.add(9, viewType);
		vecTemp.add(10, StringUtil.toString(rsGetReport.getString("Process_Date")));
		vecTemp.add(11, StringUtil.toString(rsGetReport.getString("entity_id")));
		if (rsGetReport.getString("rec_id") != null)
		{
			vecTemp.add(12, StringUtil.toString(rsGetReport.getString("rec_id")));
		}else{
			vecTemp.add(12, "N/A");
		}
		vecTemp.add(13, StringUtil.toString(rsGetReport.getString("transactionCount")));
                vecTemp.add(14, rsGetReport.getString("sim"));
                vecTemp.add(15, rsGetReport.getString("esn"));
                
		Vector<Vector<String>> entriesPerViewType = new Vector<Vector<String>>();
		String htkey = viewType + "_" + transactionType;
		if (htResultList.containsKey(htkey))
		{
			entriesPerViewType = htResultList.get(htkey);
		}
		entriesPerViewType.add(vecTemp);
		htResultList.put(htkey, entriesPerViewType);
	}
	cat.debug("getReportDetailACHBilling search returned " + htResultList.size() + " entries.");
	rsGetReport.close();
	pstmt.close();
	pstmt = null;
}
catch (Exception e)
{
	cat.error("Error during getReportACH", e);
	throw new TransactionException();
}
finally
{
	try
	{
		Torque.closeConnection(dbConn);
	}
	catch (Exception e)
	{
		cat.error("Error during closeConnection", e);
	}
}
return htResultList;
}

public static Hashtable<String, Vector<Vector<String>>> getReportGroupedDetailACHBilling(long paymentBatchEntityId)
throws TransactionException
{
	Connection dbConn = null;
	// this should regroup all billing rows given their statementType and paymentBatchEntity:
	// the values in the hashtable are the vector of entries having the same statementType and
	// transaction type.
	Hashtable<String, Vector<Vector<String>>> htResultList = new Hashtable<String, Vector<Vector<String>>>();
	try
	{
	
	dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
	if (dbConn == null)
	{
		cat.error("Can't get database connection");
		throw new TransactionException();
	}
	PreparedStatement pstmt = null;
	PreparedStatement pstmt1 = null;
	
	String strSQLGetReport = sql_bundle.getString("getPaymentBatchEntity");
	
	cat.debug("getPaymentBatchEntity SQL query: " + strSQLGetReport);
	pstmt = dbConn.prepareStatement(strSQLGetReport);
	cat.debug("PaymentBatchEntityId: " + paymentBatchEntityId);
	pstmt.setLong(1, paymentBatchEntityId);
	
	ResultSet rsGetReport = pstmt.executeQuery();
	
	strSQLGetReport = sql_bundle.getString("getReportGroupedDetailACHBilling");
	cat.debug("getReportGroupedDetailACHBilling SQL query: " + strSQLGetReport);
	rsGetReport.next();
	pstmt1 = dbConn.prepareStatement(strSQLGetReport);
	cat.debug("billingBatchId: " + rsGetReport.getLong("billingBatchId"));
	pstmt1.setLong(1, rsGetReport.getLong("billingBatchId"));
	cat.debug("paymentTypeId: " + rsGetReport.getInt("paymenTypeId"));
	pstmt1.setInt(2, rsGetReport.getInt("paymenTypeId"));
	cat.debug("entityId: " + rsGetReport.getLong("entityId"));
	pstmt1.setLong(3, rsGetReport.getLong("entityId"));
	cat.debug("entityTypeId: " + rsGetReport.getInt("entityTypeId"));
	pstmt1.setInt(4, rsGetReport.getInt("entityTypeId"));
	
	
	
	rsGetReport = pstmt1.executeQuery();
	while (rsGetReport.next())
	{
		Vector<String> vecTemp = new Vector<String>();
		String transactionType = StringUtil.toString(rsGetReport
				.getString("detailType"));
		vecTemp.add(0, transactionType);
		vecTemp.add(1, transactionType);
		vecTemp.add(2, StringUtil.toString(rsGetReport.getString("dateTime")));
		vecTemp.add(3, StringUtil.toString(rsGetReport.getString("name")));
		vecTemp.add(4, StringUtil.toString(rsGetReport.getString("productId")));
		vecTemp.add(5, StringUtil.toString(rsGetReport.getString("productDescription") 
				+  " " + StringUtil.toString(rsGetReport.getString("statementLabel"))));		        
		vecTemp.add(6, rsGetReport.getString("originalTransactionAmount"));
		vecTemp.add(7, rsGetReport.getString("feeValue"));
		vecTemp.add(8, rsGetReport.getString("commisionAmount"));
		String viewType = StringUtil.toString(rsGetReport.getString("statementTypeId"));
		vecTemp.add(9, viewType);
		vecTemp.add(10, StringUtil.toString(rsGetReport.getString("entityId")));
		vecTemp.add(11, StringUtil.toString(rsGetReport.getString("entityId")));
		if (rsGetReport.getString("rec_id") != null)
		{
			vecTemp.add(12, StringUtil.toString(rsGetReport.getString("rec_id")));
		}else{
			vecTemp.add(12, "N/A");
		}
		vecTemp.add(13, StringUtil.toString(rsGetReport.getString("transactionCount")));
                vecTemp.add(14, rsGetReport.getString("sim"));
                vecTemp.add(15, rsGetReport.getString("esn"));

		// RB: no need to pack following 3 figures again if we already have'em unformatted
		// for some more math
		// vecTemp.add(12, StringUtil.toString(rsGetReport.getString("amount")));
		// vecTemp.add(13, StringUtil.toString(rsGetReport.getString("discount")));
		// vecTemp.add(14, StringUtil.toString(rsGetReport.getString("ACH_amount")));
		
		Vector<Vector<String>> entriesPerViewType = new Vector<Vector<String>>();
		String htkey = viewType + "_" + transactionType;
		if (htResultList.containsKey(htkey))
		{
			entriesPerViewType = htResultList.get(htkey);
		}
		entriesPerViewType.add(vecTemp);
		htResultList.put(htkey, entriesPerViewType);
	}
	// cat.debug("getReportDetailACH search returned " + htResultList.size() + " entries.");
	rsGetReport.close();
	pstmt.close();
	pstmt = null;
	pstmt1.close();
	pstmt1 = null;
	}
	catch (Exception e)
	{
	cat.error("Error during getReportGroupedDetailACHBilling", e);
	throw new TransactionException();
	}
	finally
	{
	try
	{
		Torque.closeConnection(dbConn);
	}
	catch (Exception e)
	{
		cat.error("Error during closeConnection", e);
	}
	}
	return htResultList;
}

	public Vector<Vector<String>> getSummary(SessionData sessionData, int summaryType) throws TransactionException
	{
		Connection dbConn = null;
		Vector<Vector<String>> vecSummary = new Vector<Vector<String>>();
		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new TransactionException();
			}
			
			PreparedStatement pstmt;
			String strSQLTotal = sql_bundle.getString("getTotal");
			String strSQLSummary = sql_bundle.getString("getSummary");
			String strSQLMerchantIds = sql_bundle.getString("getMerchantIds");
			String strSQLWhere = "";
			switch (summaryType)
			{
				
				case 1:
					// iso
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						strSQLWhere = " t.merchant_id=" + strRefId;
					}
					break;
				
				case 2:
					// agent
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						strSQLWhere = " t.merchant_id IN (SELECT r.rep_id FROM reps AS r WITH(NOLOCK) WHERE r.type = "
								+ DebisysConstants.REP_TYPE_AGENT
								+ " AND r.iso_id="
								+ strRefId
								+ ") ";
					}
					else
						if (strAccessLevel.equals(DebisysConstants.AGENT))
						{
							strSQLWhere = " t.merchant_id=" + strRefId;
						}
					break;
				
				case 3:
					// subagent
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						strSQLWhere = " t.merchant_id IN "
								+ "(SELECT e.rep_id FROM reps AS e WITH(NOLOCK) WHERE e.type="
								+ DebisysConstants.REP_TYPE_SUBAGENT + " AND e.iso_id IN "
								+ "(SELECT e.rep_id FROM reps AS p WITH(NOLOCK) WHERE p.type="
								+ DebisysConstants.REP_TYPE_AGENT + " AND p.iso_id=" + strRefId
								+ "))";
					}
					else
						if (strAccessLevel.equals(DebisysConstants.AGENT))
						{
							strSQLWhere = " t.merchant_id in "
									+ "(SELECT r.rep_id FROM reps AS r WITH(NOLOCK) WHERE r.type="
									+ DebisysConstants.REP_TYPE_SUBAGENT + " AND r.iso_id="
									+ strRefId + ")";
						}
						else
							if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
							{
								strSQLWhere = " t.merchant_id = " + strRefId;
							}
					break;
				
				case 4:
					// rep
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
						{
							strSQLWhere = strSQLWhere + " t.merchant_id in "
									+ "(SELECT r.rep_id FROM reps AS r WITH(NOLOCK) WHERE r.type="
									+ DebisysConstants.REP_TYPE_REP + " AND r.iso_id= " + strRefId
									+ ")";
						}
						else
							if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
							{
								strSQLWhere = strSQLWhere + " t.merchant_id in "
										+ "(SELECT r.rep_id FROM reps AS r WITH(NOLOCK) where r.type="
										+ DebisysConstants.REP_TYPE_REP + " AND r.iso_id in "
										+ "(SELECT e.rep_id FROM reps AS e WITH(NOLOCK) where e.type="
										+ DebisysConstants.REP_TYPE_SUBAGENT + " AND e.iso_id in "
										+ "(SELECT p.rep_id FROM reps AS p WITH(NOLOCK) where p.type="
										+ DebisysConstants.REP_TYPE_AGENT + " AND p.iso_id="
										+ strRefId + ")))";
							}
					}
					else
						if (strAccessLevel.equals(DebisysConstants.AGENT))
						{
							strSQLWhere = strSQLWhere + " t.merchant_id in "
									+ "(SELECT r.rep_id FROM reps AS r WITH(NOLOCK) WHERE r.type="
									+ DebisysConstants.REP_TYPE_REP + " AND r.iso_id IN "
									+ "(SELECT e.rep_id FROM reps AS e WITH(NOLOCK) WHERE e.type="
									+ DebisysConstants.REP_TYPE_SUBAGENT + " AND e.iso_id="
									+ strRefId + "))";
						}
						else
							if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
							{
								strSQLWhere = strSQLWhere + " t.merchant_id IN "
										+ "(SELECT r.rep_id FROM reps AS r WITH(NOLOCK) WHERE r.type="
										+ DebisysConstants.REP_TYPE_REP + " AND r.iso_id=" + strRefId
										+ ")";
							}
							else
								if (strAccessLevel.equals(DebisysConstants.REP))
								{
									strSQLWhere = " t.merchant_id = " + strRefId;
								}
					
					break;
				
				case 5:
					// merchant
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						
						if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
						{
							strSQLWhere = " t.merchant_id IN "
									+ "(SELECT m.merchant_id FROM reps AS r WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r.rep_id WHERE r.type="
									+ DebisysConstants.REP_TYPE_REP + " AND r.iso_id= " + strRefId
									+ ")";
						}
						else
							if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
							{
								strSQLWhere = " t.merchant_id IN "
										+ "(SELECT m.merchant_id FROM reps AS r WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r.rep_id WHERE r.type="
										+ DebisysConstants.REP_TYPE_REP + " AND r.iso_id IN "
										+ "(SELECT e.rep_id FROM reps AS e WITH(NOLOCK) where e.type="
										+ DebisysConstants.REP_TYPE_SUBAGENT + " AND e.iso_id IN "
										+ "(SELECT p.rep_id FROM reps AS p WITH(NOLOCK) where p.type="
										+ DebisysConstants.REP_TYPE_AGENT + " AND p.iso_id="
										+ strRefId + ")))";
							}
					}
					else
						if (strAccessLevel.equals(DebisysConstants.AGENT))
						{
							strSQLWhere = " t.merchant_id IN "
									+ "(SELECT m.merchant_id FROM reps AS r WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r.rep_id WHERE r.type="
									+ DebisysConstants.REP_TYPE_REP + " AND r.iso_id IN "
									+ "(SELECT e.rep_id FROM reps AS e WITH(NOLOCK) WHERE e.type="
									+ DebisysConstants.REP_TYPE_SUBAGENT + " AND e.iso_id="
									+ strRefId + "))";
						}
						else
							if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
							{
								strSQLWhere = " t.merchant_id IN "
										+ "(SELECT m.merchant_id FROM reps AS r WITH(NOLOCK) INNER JOIN merchants AS m WITH(NOLOCK) ON m.rep_id = r.rep_id WHERE r.type="
										+ DebisysConstants.REP_TYPE_REP + " and r.iso_id = "
										+ strRefId + ")";
							}
							else
								if (strAccessLevel.equals(DebisysConstants.REP))
								{
									strSQLWhere = " t.merchant_id IN "
											+ "(SELECT m.merchant_id FROM reps AS r WITH(NOLOCK) INNER JOIN  merchants AS m WITH(NOLOCK) ON m.rep_id = r.rep_id WHERE r.type="
											+ DebisysConstants.REP_TYPE_REP + " and r.rep_id = "
											+ strRefId + ")";
								}
								else
									if (strAccessLevel.equals(DebisysConstants.MERCHANT))
									{
										strSQLWhere = " t.merchant_id = " + strRefId;
									}
					
					break;
			}
			
			strSQLWhere = strSQLWhere + " AND t.startdate >= '" + this.start_date
					+ "' AND t.startdate <= '" + this.end_date + "' AND status=1 ";
			strSQLTotal = strSQLTotal + strSQLWhere;
			strSQLMerchantIds = strSQLMerchantIds + strSQLWhere;
			
			pstmt = dbConn.prepareStatement(strSQLTotal);
			cat.debug(strSQLTotal);
			ResultSet rs = pstmt.executeQuery();
			int recordCount = 0;
			if (rs.next())
			{
				recordCount = rs.getInt("count");
				if (recordCount > 0)
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(DateUtil.formatDate(rs.getDate("startdate")));
					vecTemp.add(DateUtil.formatDate(rs.getDate("enddate")));
					vecTemp.add(StringUtil.toString(rs.getString("count")));
					vecTemp.add(rs.getString("total"));
					
					// create a string of merchant ids
					// to pass to the merchant summary report
					pstmt = dbConn.prepareStatement(strSQLMerchantIds);
					cat.debug(strSQLMerchantIds);
					rs = pstmt.executeQuery();
					String strTemp = "";
					int count = 0;
					while (rs.next())
					{
						if (count == 0)
						{
							strTemp = rs.getString("merchant_id");
						}
						else
						{
							strTemp = strTemp + "," + rs.getString("merchant_id");
						}
						count++;
					}
					
					vecTemp.add(strTemp);
					vecSummary.add(vecTemp);
					
				}
			}
			
			pstmt.close();
			rs.close();
			
			if (recordCount > 0)
			{
				strSQLSummary = strSQLSummary + strSQLWhere
						+ " GROUP BY t.startdate, t.enddate ORDER BY t.startdate ";
				pstmt = dbConn.prepareStatement(strSQLSummary);
				cat.debug(strSQLSummary);
				rs = pstmt.executeQuery();
				
				while (rs.next())
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(DateUtil.formatDate(rs.getDate("startdate")));
					vecTemp.add(DateUtil.formatDate(rs.getDate("enddate")));
					vecTemp.add(StringUtil.toString(rs.getString("count")));
					vecTemp.add(rs.getString("total"));
					vecSummary.add(vecTemp);
				}
				
				pstmt.close();
				rs.close();
			}
			
		}
		catch (Exception e)
		{
			cat.error("Error during search", e);
			throw new TransactionException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecSummary;
	}
	
	/**
	 * Returns a hash of errors.
	 * 
	 * @return Hash of errors.
	 */
	
	public Hashtable<String, String> getErrors()
	{
		return this.validationErrors;
	}
	
	/**
	 * Returns a validation error against a specific field. If a field was found to have an error
	 * during {@link #validateDateRange validateDateRange}, the error message can be accessed via
	 * this method.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @return The error message for the field or null if none is present.
	 */
	
	public String getFieldError(String fieldname)
	{
		return (this.validationErrors.get(fieldname));
	}
	
	/**
	 * Sets the validation error against a specific field. Used by
	 * {@link #validateDateRange validateDateRange}.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @param error
	 *            The error message for the field or null if none is present.
	 */
	
	public void addFieldError(String fieldname, String error)
	{
		this.validationErrors.put(fieldname, error);
	}
	
	/**
	 * Validates for missing or invalid fields.
	 * 
	 * @return <code>true</code> if the entity passes the validation checks, otherwise
	 *         <code>false</code>
	 */
	
	public boolean validateDateRange(SessionData sessionData)
	{
		this.validationErrors.clear();
		boolean valid = true;
		
		if ((this.start_date == null) || (this.start_date.length() == 0))
		{
			this.addFieldError("startdate", Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()));
			valid = false;
		}
		else
			if (!DateUtil.isValid(this.start_date))
			{
				this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
				valid = false;
			}
		
		if ((this.end_date == null) || (this.end_date.length() == 0))
		{
			this.addFieldError("enddate", Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()));
			valid = false;
		}
		else
			if (!DateUtil.isValid(this.end_date))
			{
				this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
				valid = false;
			}
		
		if (valid)
		{
			
			Date dtStartDate = new Date();
			Date dtEndDate = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			formatter.setLenient(false);
			try
			{
				dtStartDate = formatter.parse(this.start_date);
			}
			catch (ParseException ex)
			{
				this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
				valid = false;
			}
			try
			{
				dtEndDate = formatter.parse(this.end_date);
			}
			catch (ParseException ex)
			{
				this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
				valid = false;
			}
			if (dtStartDate.after(dtEndDate))
			{
				this.addFieldError("date", "Start date must be before end date.");
				valid = false;
			}
			
		}
		
		return valid;
	}
	
	public static String validateDateRange(String start_date, String end_date,SessionData sessionData)
	{
		String strResult = "";
		boolean valid = true;
		if ((start_date == null) || (start_date.length() == 0))
		{
			strResult += Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()) + "<br>";
			valid = false;
		}
		else
			if (!DateUtil.isValid(start_date))
			{
				strResult += Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()) + "<br>";
				valid = false;
			}
		
		if ((end_date == null) || (end_date.length() == 0))
		{
			strResult += Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()) + "<br>";
			valid = false;
		}
		else
			if (!DateUtil.isValid(end_date))
			{
				strResult += Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()) + "<br>";
				valid = false;
			}
		
		if (valid)
		{
			
			Date dtStartDate = new Date();
			Date dtEndDate = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			formatter.setLenient(false);
			try
			{
				dtStartDate = formatter.parse(start_date);
			}
			catch (ParseException ex)
			{
				strResult += Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()) + "<br>";
			}
			try
			{
				dtEndDate = formatter.parse(end_date);
			}
			catch (ParseException ex)
			{
				strResult += Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()) + "<br>";
			}
			if (dtStartDate.after(dtEndDate))
			{
				strResult += Languages.getString("com.debisys.reports.error5", sessionData.getLanguage()) + "<br>";
			}
			
		}
		
		return strResult;
	}
	
	public void valueBound(HttpSessionBindingEvent event)
	{
	}
	
	public void valueUnbound(HttpSessionBindingEvent event)
	{
	}
	
	public static void main(String[] args)
	{
	}
	
}
