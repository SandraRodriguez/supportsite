/**
 * 
 */
package com.debisys.mb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.languages.Languages;
import com.debisys.utils.Base;
import com.debisys.utils.DebisysConstants;

/**
 * @author nmartinez
 *
 */
public class MicroBrowser {

	
	/**
	 * to log this specific class 
	 */
	private static Logger cat = Logger.getLogger(MicroBrowser.class);
	
	
	/**
	 *  file with sql statements
	 */
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.mb.sql");
	
	

	/**
	 * @param accessLevelId
	 * @param agentId
	 * @param subagentId
	 * @param repId
	 * @param merchantId
	 * @param currenAppGroupId
	 * @return ArrayList of TerminalsMB objects 
	 */
	public static ArrayList<TerminalsMB> findTerminals(String accessLevelId, String agentId, String subagentId, 
													   String repId, String merchantId, String currenAppGroupId){
		
		String strSQL = "";
		ArrayList<TerminalsMB> arrTerminals = new ArrayList<TerminalsMB>();
		
		if ( accessLevelId.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		{
			if ( merchantId!=null && merchantId.length()>0 && !merchantId.equals("-1") )
			{
				strSQL = sql_bundle.getString("findTerminalsMerchant");
				arrTerminals = findTerminals(merchantId,currenAppGroupId,strSQL);
			}
			else if ( repId != null && repId.length()>0 && !repId.equals("-1"))
			{
				strSQL = sql_bundle.getString("findTerminalsRep");
				arrTerminals = findTerminals(repId,currenAppGroupId,strSQL);
			}	
			else if ( subagentId != null && subagentId.length()>0 && !subagentId.equals("-1") )
			{
				strSQL = sql_bundle.getString("findTerminalsSubAgent");
				arrTerminals = findTerminals(subagentId,currenAppGroupId,strSQL);
			}
			else if ( agentId != null && agentId.length()>0 && !agentId.equals("-1") )
			{
				strSQL = sql_bundle.getString("findTerminalsAgent");
				arrTerminals = findTerminals(agentId,currenAppGroupId,strSQL);
			}
			else
			{
				cat.info("****** select At least select one Rep!!! ***** ");
			}
		}
		else
		{
			if ( merchantId!=null && merchantId.length()>0 && !merchantId.equals("-1") )
			{
				strSQL = sql_bundle.getString("findTerminalsMerchant");
				arrTerminals = findTerminals(merchantId,currenAppGroupId,strSQL);
			}
			else if ( repId != null && repId.length()>0 && !repId.equals("-1"))
			{
				strSQL = sql_bundle.getString("findTerminalsRep");
				arrTerminals = findTerminals(repId,currenAppGroupId,strSQL);
			}
			else
			{
				cat.info("****** select At least select one Rep!!! ***** ");
			}
		}
		return arrTerminals;		
	}
	
	
	/**
	 * @param entityId
	 * @param currenAppGroupId
	 * @param sqlTerminals
	 * @return ArrayList of TerminalsMB objects
	 */
	private static ArrayList<TerminalsMB> findTerminals(String entityId, String currenAppGroupId, String sqlTerminals) {
		ArrayList<TerminalsMB> arrTerminals = new ArrayList<TerminalsMB>();
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
			}
			cat.info("***** findTerminals SQL: "+sqlTerminals );
			cat.info("***** findTerminals Entity: "+entityId );
			cat.info("***** findTerminals Current GroupId: "+currenAppGroupId );
			pstmt = dbConn.prepareStatement(sqlTerminals);
			pstmt.setString(1, entityId);
			pstmt.setString(2, currenAppGroupId);
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				TerminalsMB terminal = new TerminalsMB();
				terminal.setSiteId(rs.getString("millennium_no"));
				terminal.setMerchantDba(rs.getString("merchant"));
				terminal.setMerchantId(rs.getString("merchant_id"));
				arrTerminals.add( terminal );
			}
			
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.info("findTerminals ", e);
		}
		finally
		{
			try
			{
				if (pstmt != null)
				{
					pstmt.close();
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.info("findTerminals closeConnection ", e);
			}
		}		
		return arrTerminals;
	}


	/**
	 * @return ArrayList of ApplicationsGroups objects
	 */
	public static ArrayList<ApplicationsGroups> findApplicationsGroups(){
		String strSQL = sql_bundle.getString("findApplicationsGroups");
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ArrayList<ApplicationsGroups> arrApplications = new ArrayList<ApplicationsGroups>(); 
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
			}
			pstmt = dbConn.prepareStatement(strSQL);
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				arrApplications.add(new ApplicationsGroups(rs.getString(1), rs.getString(2)));
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("findApplicationsGroups ", e);
		}
		finally
		{
			try
			{
				if (pstmt != null)
				{
					pstmt.close();
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("findApplicationsGroups closeConnection ", e);
			}
		}
		return arrApplications;
	}
		
	
	
	/**
	 * @param siteId
	 * @param applicactionGroupId
	 * @param language
	 * @return A simple object with Id=1 successful update and description message
	 */
	public static Base updateTerminalMBApplicationGroup(String siteId, String applicactionGroupId, String language){
		
		Base result = new Base();
		result.setId("-1");
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;		
		Connection dbConnMarketPlace = null;
		PreparedStatement pstmtMarketPlace = null;
		
		String updateDebisys = sql_bundle.getString("updateTerminalSiteIdDebisys");
		String updateMarketPlace = sql_bundle.getString("updateTerminalSiteIdMarketPlace");
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if ( dbConn != null )
			{
				dbConn.setAutoCommit(false);
				pstmt = dbConn.prepareStatement(updateDebisys);
				pstmt.setString(1, applicactionGroupId);
				pstmt.setString(2, siteId);
				int rowsAffected = pstmt.executeUpdate();				
				if ( rowsAffected == 1 )
				{
					dbConnMarketPlace = Torque.getConnection(sql_bundle.getString("marketPlace"));
					if ( dbConnMarketPlace != null )
					{
						dbConnMarketPlace.setAutoCommit( false );
						pstmtMarketPlace = dbConnMarketPlace.prepareStatement( updateMarketPlace );
						pstmtMarketPlace.setString( 1,applicactionGroupId );
						pstmtMarketPlace.setString( 2, siteId+"0" );
						rowsAffected = pstmtMarketPlace.executeUpdate();
						
						if ( rowsAffected == 1 )
						{
							dbConnMarketPlace.commit();
							dbConn.commit();
							result.setId("1");
							result.setDescription( Languages.getString("jsp.tools.dtu143.okTerminal", language) );
							cat.info(">>> OK SiteId = "+siteId+" updated to application group ("+applicactionGroupId+") <<<");
						}
						else
						{
							dbConnMarketPlace.rollback();
							dbConn.rollback();
							result.setDescription( Languages.getString("jsp.tools.dtu143.errorUpdateMarketTerminal", language) );
							cat.info("******** ERROR SiteId = "+siteId+" Not found on MarkePlace database *******");
						}
						pstmtMarketPlace.close();
					}
					else
					{
						result.setDescription( Languages.getString("jsp.tools.dtu143.errorUpdateMarketNotAvailable", language) );
						cat.info("******** ERROR MarkePlace database NOT AVAILABLE *******");
						dbConn.rollback();
					}
				}
				else 
				{
					result.setDescription( Languages.getString("jsp.tools.dtu143.errorUpdateDebisysTerminal", language) );					
					cat.info("******** ERROR SiteId = "+siteId+" Not found on Debisys database *******");					
				}				
				pstmt.close();				
			}
			else
			{
				result.setDescription( Languages.getString("jsp.tools.dtu143.errorUpdateDebisysNotAvailable", language) );
				cat.info("******** ERROR Debisys database NOT AVAILABLE *******");
			}			
		}
		catch (Exception e)
		{
			result.setDescription( Languages.getString("jsp.tools.dtu143.errorGeneral", language) );			
			cat.info("updateTerminalMBApplicationGroup ", e);
			if ( dbConn != null )
			{
				try {
					dbConn.rollback();
				} catch (SQLException e1) {
					cat.info("findApplicationsGroups cannot rollback Debisys ", e);
				}
			}
			if ( dbConnMarketPlace != null )
			{
				try {
					dbConnMarketPlace.rollback();
				} catch (SQLException e1) {
					cat.info("findApplicationsGroups cannot rollback MarketPlace ", e);
				}
			}
		}
		finally
		{
			try
			{
				if ( pstmt != null )
					pstmt.close();
				if ( dbConn != null )
				{
					dbConn.setAutoCommit(true);
					Torque.closeConnection(dbConn);
				}
			}
			catch (Exception e)
			{
				cat.info("findApplicationsGroups closeConnection Debisys ", e);
			}
			try
			{
				if ( pstmtMarketPlace != null )
					pstmtMarketPlace.close();
				
				if ( dbConnMarketPlace != null )
				{
					dbConnMarketPlace.setAutoCommit(true);
					Torque.closeConnection(dbConnMarketPlace);
				}
			}
			catch (Exception e)
			{
				cat.info("findApplicationsGroups closeConnection MarketPlace ", e);
			}
		}		
		return result;
	}
	
}
