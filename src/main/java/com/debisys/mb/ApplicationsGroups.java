/**
 * 
 */
package com.debisys.mb;

/**
 * @author nmartinez
 *
 */
public class ApplicationsGroups {

	private String aplicationGroupId;
	private String description;
	
	
	public ApplicationsGroups(String applicationId, String description){
		this.aplicationGroupId = applicationId;
		this.description = description;
	}
	
	
	/**
	 * @return the aplicationGroupId
	 */
	public String getAplicationGroupId() {
		return aplicationGroupId;
	}
	/**
	 * @param aplicationGroupId the aplicationGroupId to set
	 */
	public void setAplicationGroupId(String aplicationGroupId) {
		this.aplicationGroupId = aplicationGroupId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
