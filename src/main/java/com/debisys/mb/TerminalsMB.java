package com.debisys.mb;

public class TerminalsMB {

	private String siteId;
	private String merchantId;
	private String merchantDba;
	
	
	public TerminalsMB(){
		
	}
	
	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}
	/**
	 * @param siteId the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}	
	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * @return the merchantDba
	 */
	public String getMerchantDba() {
		return merchantDba;
	}
	/**
	 * @param merchantDba the merchantDba to set
	 */
	public void setMerchantDba(String merchantDba) {
		this.merchantDba = merchantDba;
	}
	
	
	
}
