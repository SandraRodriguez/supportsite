/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.debisys.dateformat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import com.debisys.exceptions.UserException;
import com.debisys.utils.DebisysConfigListener;

/**
 * ADDED FOR LOCALIZATION
 * @author swright
 */
public class DateFormat extends HttpServlet
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static boolean inited = false;
    private static Logger cat = Logger.getLogger(DateFormat.class);

    private static String dateFormat = "MM/dd/yyyy"; //default
    
    // This one is for the results that dont really display a time and are really difficult
    // to figure out what exactly is going on.
    private static String dateFormatNoTime = "MM/dd/yyyy"; //default
    
    //private static String[] acceptedDateFormats = { "MM/dd/yyyy","EEEE MMMM dd yyyy","dd/MM/yyyy", "MM/dd/yy", "dd/MM/yy" };
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.dateformat.sql");

    private static void initialize(ServletContext context) 
    {
        Connection dbConn = null;

        try
        {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new UserException();
            }
            PreparedStatement pstmt = dbConn.prepareStatement(sql_bundle.getString("getProperty"));
            pstmt.setString(1, "support");
            pstmt.setString(2, "global");
            pstmt.setString(3, DebisysConfigListener.getInstance(context));
            pstmt.setString(4, "dateFormat");
            ResultSet rs = pstmt.executeQuery();
            if (rs.next())
            {
            	cat.debug("Got dateFormat from DB");
                dateFormat = rs.getString("value");
            }
            else
            	cat.debug("Using default value for dateFormat");
            
            
            // Get rid of the time format for date results stored as strings in DB without a time
            if(dateFormat.contains("H") || dateFormat.contains("h"))
            {
                int indexOfH = dateFormat.indexOf("H");
                int indexOfh = dateFormat.indexOf("h");
                
                if(indexOfH > 0)
                    dateFormatNoTime = dateFormat.substring(0, indexOfH - 1);
                else
                    dateFormatNoTime = dateFormat.substring(0, indexOfh - 1);
            }

            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during currency initialize", e);
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }
        }
    }
    
    @Override
	public void init() throws ServletException
    {
        cat.info("Initializing date settings");
        if (inited) 
            return;
        inited = true;
        
        try
        {
            DateFormat.initialize(this.getServletContext());
            cat.info("Date format initialized to '" + dateFormat + "'");
            cat.info("Date format (no time) initialized to " + dateFormatNoTime + "'");
        } 
        catch (Exception e)
        {
            cat.error("Date format initialization failed!");
            throw new ServletException( e.toString() );
        }
    }
    
    public static String getDateFormat()
    {
        return dateFormat;
    }
    
    public static String getDateNoTimeFormat()
    {
        return dateFormatNoTime;
    }
}
