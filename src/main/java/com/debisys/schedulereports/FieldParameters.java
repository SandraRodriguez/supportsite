package com.debisys.schedulereports;

public class FieldParameters {
	private String name;
    private Class clazz;
    private String expression;
    
    /**
	 * @return the expression
	 */
	public String getExpression() {
		return expression;
	}

	/**
	 * @param expression the expression to set
	 */
	public void setExpression(String expression) {
		this.expression = expression;
	}

	public FieldParameters(String name) {
        this.name = name;
        this.clazz = String.class;
    }

    public FieldParameters(String name, Class clazz, String expression) {
        this.name = name;
        this.clazz = clazz;
        this.expression = expression;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
