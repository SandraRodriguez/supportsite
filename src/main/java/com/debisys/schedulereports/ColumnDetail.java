package com.debisys.schedulereports;

import java.math.BigDecimal;

public class ColumnDetail {
	private String name;
    private Class clazzType;
    private String pattern;
    private boolean isSumValue;
    private boolean putInTemplate = true;
    private String valueExpressionWhenIsCalculate;
    
    public ColumnDetail(String name) {
        this.name = name;
        this.clazzType = String.class;
    }
    public ColumnDetail(String name, Class clazzType, boolean isSumm) {
        this.name = name;
        this.clazzType = clazzType;
        this.setSumValue(isSumm);
    }
    
    public boolean isNumber(){
        if(clazzType == BigDecimal.class || clazzType == Integer.class || clazzType == double.class || clazzType == int.class || clazzType == float.class ||
                clazzType == Float.class || clazzType == Long.class || clazzType == long.class){
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getClazzType() {
        return clazzType;
    }

    public void setClazzType(Class clazzType) {
        this.clazzType = clazzType;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
	/**
	 * @param isSumValue the isSumValue to set
	 */
	public void setSumValue(boolean isSumValue) {
		this.isSumValue = isSumValue;
	}
	/**
	 * @return the isSumValue
	 */
	public boolean isSumValue() {
		return isSumValue;
	}
	/**
	 * @param putInTemplate the putInTemplate to set
	 */
	public void setPutInTemplate(boolean putInTemplate) {
		this.putInTemplate = putInTemplate;
	}
	/**
	 * @return the putInTemplate
	 */
	public boolean isPutInTemplate() {
		return putInTemplate;
	}
	/**
	 * @param valueExpressionWhenIsCalculate the valueExpressionWhenIsCalculate to set
	 */
	public void setValueExpressionWhenIsCalculate(
			String valueExpressionWhenIsCalculate) {
		this.valueExpressionWhenIsCalculate = valueExpressionWhenIsCalculate;
	}
	/**
	 * @return the valueExpressionWhenIsCalculate
	 */
	public String getValueExpressionWhenIsCalculate() {
		return valueExpressionWhenIsCalculate;
	}
}
