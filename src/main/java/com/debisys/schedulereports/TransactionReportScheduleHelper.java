package com.debisys.schedulereports;

import java.util.ArrayList;

import net.sf.jasperreports.engine.type.CalculationEnum;

import com.debisys.utils.ColumnReport;


public class TransactionReportScheduleHelper {

	
	
	public static void addMetaDataReport(ArrayList<ColumnReport> headersInfo, ScheduleReport scheduleReport, ArrayList<String> titles) 
	{	
		if ( titles!=null && titles.size()>0 )
		{				
			scheduleReport.setListTitles(titles);
		}
		for ( ColumnReport column : headersInfo)
		{			
			String columnName = column.getSqlName();
			String columnLabel = column.getLanguageDescription();
			Class<?> dataType = column.getClassNameData();	
			
			if ( column.isPutInTemplate() )
				scheduleReport.addHeader( columnLabel );
						
			ColumnDetail detail = new ColumnDetail("$F{"+ columnName +"}", dataType, column.isSumValue() );
			detail.setPutInTemplate( column.isPutInTemplate() );
			
			if ( column.getValueExpressionWhenIsCalculate() != null )
			{
				String expression = column.getValueExpressionWhenIsCalculate();
				JasperVariables calculateVariable = new JasperVariables( "tot"+columnName ,java.lang.Double.class, expression , "###0.00", false,CalculationEnum.NOTHING);
				scheduleReport.addVariable( calculateVariable );
				//detail.setValueExpressionWhenIsCalculate( expression );
			}
			//else
			//{
			scheduleReport.addField(new FieldParameters(columnName, dataType, "" ));
			//}				
			
			scheduleReport.addColumnDetail( detail );
			
			if ( column.isSumValue() )
			{				
				String varColumnName = "tot"+columnName;
				if ( column.getClassNameData().equals(java.lang.Integer.class) )
				{
					JasperVariables sumQty = new JasperVariables( varColumnName ,java.lang.Integer.class,"$F{"+ columnName +"}", "#,##0", false, CalculationEnum.SUM);
					scheduleReport.addVariable( sumQty );
				}
				else
				{
					JasperVariables sumQty = new JasperVariables( varColumnName,java.lang.Double.class,"$F{"+ columnName +"}", "#,##0.00", false, CalculationEnum.SUM);
					scheduleReport.addVariable( sumQty );
				}				
			}			
		}						
	}	

	
}