/**
 * 
 */
package com.debisys.schedulereports;

/**
 * @author nmartinez
 *
 */
public class JobUI {

	private long Id;
	private String label;
	private String scheduleDescription;
	private String errorDescription;
	private String groupReport;
	private String creationDate;
	private String[] emails;

	/**
	 * 
	 */
	public JobUI(){
		errorDescription="";
	}
	
	
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}


	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}


	/**
	 * @return the groupReport
	 */
	public String getGroupReport() {
		return groupReport;
	}


	/**
	 * @param groupReport the groupReport to set
	 */
	public void setGroupReport(String groupReport) {
		this.groupReport = groupReport;
	}

	
	/**
	 * @return the id
	 */
	public long getId() {
		return Id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		Id = id;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the scheduleDescription
	 */
	public String getScheduleDescription() {
		return scheduleDescription;
	}
	/**
	 * @param scheduleDescription the scheduleDescription to set
	 */
	public void setScheduleDescription(String scheduleDescription) {
		this.scheduleDescription = scheduleDescription;
	}


	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	/**
	 * @return the creationDate
	 */
	public String getCreationDate() {
		return creationDate;
	}


	/**
	 * @param emails the emails to set
	 */
	public void setEmails(String[] emails) {
		this.emails = emails;
	}


	/**
	 * @return the emails
	 */
	public String[] getEmails() {
		return emails;
	}
	
	
}
