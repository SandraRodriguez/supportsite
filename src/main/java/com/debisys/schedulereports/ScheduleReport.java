package com.debisys.schedulereports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author nmartinez
 *
 */
public class ScheduleReport extends ScheduleReportAbstract{

	
	private String name;
	private String relativeQuery;
	private String fixedQuery;
	private int rowNumberStart;
	private List<FieldParameters> listParameters;
	private List<FieldParameters> listFieldsQuery;
	private List<JasperVariables> listVariables;
	private List<String> listHeaders;
	private List<ColumnDetail> listDetail;
	private List<String> listTitles;
	private String nameDateTimeColumn;
	private String titleName;
	private String additionalData;
	private HashMap<String, String> hashTokensToReplaceWhenRelativeDates;
	


	/**
	 * @return the nameDateTimeColumn
	 */
	public String getNameDateTimeColumn() {
		return nameDateTimeColumn;
	}



	/**
	 * @param nameDateTimeColumn the nameDateTimeColumn to set
	 */
	public void setNameDateTimeColumn(String nameDateTimeColumn) {
		this.nameDateTimeColumn = nameDateTimeColumn;
	}



	/**
	 * @param name
	 */
	public ScheduleReport(String name){
		super(name);
		relativeQuery = "";
		fixedQuery = "";
		rowNumberStart = 1;
		this.name = name;
		init();
	}
	
	
	
	/**
	 * @param name
	 * @param rowNumberStart
	 */
	public ScheduleReport(String name, int rowNumberStart){
		super(name);
		relativeQuery = "";
		fixedQuery = "";
		this.name = name;
		this.rowNumberStart = rowNumberStart;
		init();
	}
	
	
	/**
	 * 
	 */
	private void init(){
		listTitles = new ArrayList<String>();
		listParameters = new ArrayList<FieldParameters>();
		listFieldsQuery = new ArrayList<FieldParameters>();
		listVariables = new ArrayList<JasperVariables>();
		listHeaders = new ArrayList<String>();
		listDetail = new ArrayList<ColumnDetail>();
	}
	
	public void addParameter(FieldParameters parameter){
		listParameters.add(parameter);
	}
	public void addField(FieldParameters field){
		listFieldsQuery.add(field);
	}
	public void addVariable(JasperVariables variable){
		listVariables.add(variable);
	}
	public void addHeader(String header){
		listHeaders.add(header);
	}
	public void addTitle(String title){
		listTitles.add(title);
	}
	public void addColumnDetail(ColumnDetail columnDetail){
		listDetail.add(columnDetail);
	}	
	
	public String getRelativeQuery() {
		return relativeQuery;
	}
	public void setRelativeQuery(String query) {
		this.relativeQuery = query;
	}
	public String getFixedQuery() {
		return fixedQuery;
	}
	public void setFixedQuery(String query) {
		this.fixedQuery = query;
	}
	public List<FieldParameters> getListParameters() {
		return listParameters;
	}
	public void setListParameters(List<FieldParameters> listParameters) {
		this.listParameters = listParameters;
	}
	public List<FieldParameters> getListFieldsQuery() {
		return listFieldsQuery;
	}
	public void setListFieldsQuery(List<FieldParameters> listFieldsQuery) {
		this.listFieldsQuery = listFieldsQuery;
	}
	public List<JasperVariables> getListVariables() {
		return listVariables;
	}
	public void setListVariables(List<JasperVariables> listVariables) {
		this.listVariables = listVariables;
	}
	public List<String> getListHeaders() {
		return listHeaders;
	}
	public void setListHeaders(List<String> listHeaders) {
		this.listHeaders = listHeaders;
	}
	public List<ColumnDetail> getListDetail() {
		return listDetail;
	}
	public void setListDetail(List<ColumnDetail> listDetail) {
		this.listDetail = listDetail;
	}
	public int getRowNumberStart() {
		return rowNumberStart;
	}
	public void setRowNumberStart(int rowNumberStart) {
		this.rowNumberStart = rowNumberStart;
	}



	/**
	 * @param listTitles the listTitles to set
	 */
	public void setListTitles(List<String> listTitles) {
		this.listTitles = listTitles;
	}

	/**
	 * @return the listTitles
	 */
	public List<String> getListTitles() {
		return listTitles;
	}

	/**
	 * @param titleName the titleName to set
	 */
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	/**
	 * @return the titleName
	 */
	public String getTitleName() {
		return titleName;
	}

	/**
	 * @param additionalData the additionalData to set
	 */
	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	/**
	 * @return the additionalData
	 */
	public String getAdditionalData() {
		return additionalData;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @param hashTokensToReplaceWhenRelativeDates
	 */
	public void setHastTokensRelativeDates( HashMap<String, String> hashTokensToReplaceWhenRelativeDates) {
		this.hashTokensToReplaceWhenRelativeDates = hashTokensToReplaceWhenRelativeDates;
	}
	
	/**
	 * @return
	 */
	public HashMap<String, String> getHastTokensRelativeDates( ) {
		return this.hashTokensToReplaceWhenRelativeDates;
	}
	
}
