/**
 * 
 */
package com.debisys.schedulereports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;

/**
 * @author nmartinez
 *
 */
public class RelativeOptions {

	private static Logger       cat          = Logger.getLogger(RelativeOptions.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.schedulereports.sql");

	
	private String id;
	private String code;
	private String startFunction;
	private String endFunction;
	private String captureData;
	private String shortDescrption;
	private String longDescription;
	private String canIncludeCurrent;
	
	
	/**
	 * @param language
	 * @return
	 */
	public static ArrayList<RelativeOptions> findRelativeOptions(String language)
	{
		
		Connection dbConn = null;
		ArrayList<RelativeOptions> arrRelative = new ArrayList<RelativeOptions>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			String sql = sql_bundle.getString("findRelativeOptions");
			
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement( sql );
			pstmt.setString(1, language);
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next())
			{			
				RelativeOptions relativeOptions = new RelativeOptions();				
				String id = rs.getString("id");
				String StartDateFunc = rs.getString("StartDateFunc");
				String EndDateFunc = rs.getString("EndDateFunc");
				String capture = rs.getString("CaptureAdditionalData");
				String ShortDescription = rs.getString("ShortDescription");
				String LongDescription = rs.getString("LongDescription");
				String codeR = rs.getString("Code");
				String canIncludeCurrent = rs.getString("CanIncludeCurrent");
				
				relativeOptions.setId( id );
				relativeOptions.setCaptureData( capture );
				relativeOptions.setEndFunction( EndDateFunc );
				relativeOptions.setStartFunction( StartDateFunc );
				relativeOptions.setShortDescrption( ShortDescription );
				relativeOptions.setLongDescription( LongDescription );				
				relativeOptions.setCode( codeR );
				relativeOptions.setCanIncludeCurrent( canIncludeCurrent );
				
				arrRelative.add(relativeOptions);
				
			}
			rs.close();
			pstmt.close();
			
		}
		catch(Exception e)
		{
			cat.info("Error-Exception findRelativeOptions ", e);
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.info("Error during closeConnection - findRelativeOptions ", e);
			}
		}		
		return arrRelative;
	}
	
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}



	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the startFunction
	 */
	public String getStartFunction() {
		return startFunction;
	}

	/**
	 * @param startFunction the startFunction to set
	 */
	public void setStartFunction(String startFunction) {
		this.startFunction = startFunction;
	}

	/**
	 * @return the endFunction
	 */
	public String getEndFunction() {
		return endFunction;
	}

	/**
	 * @param endFunction the endFunction to set
	 */
	public void setEndFunction(String endFunction) {
		this.endFunction = endFunction;
	}

	/**
	 * @return the captureData
	 */
	public String getCaptureData() {
		return captureData;
	}

	/**
	 * @param captureData the captureData to set
	 */
	public void setCaptureData(String captureData) {
		this.captureData = captureData;
	}

	/**
	 * @return the shortDescrption
	 */
	public String getShortDescrption() {
		return shortDescrption;
	}

	/**
	 * @param shortDescrption the shortDescrption to set
	 */
	public void setShortDescrption(String shortDescrption) {
		this.shortDescrption = shortDescrption;
	}

	/**
	 * @return the longDescription
	 */
	public String getLongDescription() {
		return longDescription;
	}

	/**
	 * @param longDescription the longDescription to set
	 */
	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}


	/**
	 * @param canIncludeCurrent the canIncludeCurrent to set
	 */
	public void setCanIncludeCurrent(String canIncludeCurrent) {
		this.canIncludeCurrent = canIncludeCurrent;
	}


	/**
	 * @return the canIncludeCurrent
	 */
	public String getCanIncludeCurrent() {
		return canIncludeCurrent;
	}


	

}
