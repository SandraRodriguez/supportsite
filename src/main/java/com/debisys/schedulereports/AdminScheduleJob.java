/**
 * 
 */
package com.debisys.schedulereports;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;

import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.BaseDaysMonths;
import com.debisys.utils.CalendarBussines;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.jaspersoft.jasperserver.ws.scheduling.CalendarDaysType;
import com.jaspersoft.jasperserver.ws.scheduling.IntervalUnit;
import com.jaspersoft.jasperserver.ws.scheduling.Job;
import com.jaspersoft.jasperserver.ws.scheduling.JobCalendarTrigger;
import com.jaspersoft.jasperserver.ws.scheduling.JobSimpleTrigger;
import com.jaspersoft.jasperserver.ws.scheduling.JobSummary;
import com.jaspersoft.jasperserver.ws.scheduling.ReportSchedulerSoapBindingStub;

/**
 * @author nmartinez
 *
 */
public class AdminScheduleJob {
	
	private static Logger cat = Logger.getLogger(AdminScheduleJob.class);
	
	
	/**
	 * @param context
	 * @param jobId
	 */
	public static void deleteJobById(ServletContext context, String jobId)
	{
		String instance 			   = DebisysConfigListener.getInstance( context );
		SchedulerProperties properties = SchedulerProperties.findPropertiesScheduler(instance);
		
		if ( properties != null )
		{			
			 String url 	   = properties.getUrl();
			 String user       = properties.getUser();
			 String password   = properties.getPassword();				 
			 String repository = properties.getReportscheduler();
			 
	     	java.net.URL urlJasper;
			try 
			{
				urlJasper = new java.net.URL( url+ repository );
				ReportSchedulerSoapBindingStub scheduler = new ReportSchedulerSoapBindingStub(urlJasper, null);
		        ((Stub)scheduler).setUsername( user );
		         scheduler.setPassword( password );
		         scheduler.deleteJob( Long.parseLong(jobId) );
			}
			catch (Exception e) 
			{
				cat.info("Cannot delete job "+jobId+ " "+e);
			}         
	        
		}    
	}
	
	
	/**
	 * @param sessionData
	 * @param context
	 * @param request
	 * @param userId
	 * @return
	 */
	public static ArrayList<JobUI> listJobByUserId(SessionData sessionData, ServletContext context,ServletRequest request, String userId )
	{
		String instance 	= DebisysConfigListener.getInstance( context );
		ArrayList<JobUI> jobsList = new ArrayList<JobUI>();
		String languageFlag = sessionData.getUser().getLanguageCode();
		try
	    {
	    	 SchedulerProperties properties = SchedulerProperties.findPropertiesScheduler(instance);
			 if ( properties != null)
			 {			
				 String url = properties.getUrl();
				 String user = properties.getUser();
				 String password = properties.getPassword();				 
				 String repository = properties.getReportscheduler();
				 String formatDate = properties.getFormatDateUI();
				 
		     	 java.net.URL urlJasper = new java.net.URL( url+ repository );
		     	 
	             ReportSchedulerSoapBindingStub scheduler = new ReportSchedulerSoapBindingStub(urlJasper, null);
		         scheduler.setUsername( user );
		         scheduler.setPassword( password );
		         
		    	 com.jaspersoft.jasperserver.ws.scheduling.JobSummary[] jobsAll = scheduler.getAllJobs();
		         for( com.jaspersoft.jasperserver.ws.scheduling.JobSummary jos : jobsAll )
		     	 {		        	
		        	System.out.println( jos.getReportUnitURI() + " "+ jos.getLabel()   );
		     	 	if ( jos.getLabel() != null)
		     	 	{
		     	 		String jobInfo[] = jos.getLabel().split("_");
		     	 		//System.out.println( jobInfo[0]+ " "+jobInfo[1] + " "+ jobInfo[2]  );
		     	 		if ( jobInfo.length == 4 && jobInfo[1].equals( userId ) )
		     	 		{	    
		     	 			JobUI jobUI = new JobUI();
		     	 			parseCreationJobDate(jobInfo, jobUI, formatDate);
		     	 			
		     	 			long jobId = jos.getId();		     	 			
		     	 			Job schedulejob = findJobById(scheduler, jobId ); 
	     	 				if ( schedulejob != null )
	     	 				{
			     	 			defineReportGroup(sessionData, jos, jobUI);			     	 						     	 			
			     	 			jobUI.setId(jobId);
			     	 			jobUI.setLabel(jos.getLabel());		     	 			
			     	 			try
			     	 			{
			     	 				String endLabel = Languages.getString("jsp.admin.reports.scheduleReport.until", sessionData.getLanguage());
			     	 				String startLabel = Languages.getString("jsp.admin.reports.scheduleReport.startSchedule", sessionData.getLanguage());
			     	 				
			     	 				String carrierReturn = "<br/>";
			     	 				StringBuilder reportDescription = new StringBuilder();
			     	 				jobsList.add(jobUI);
			     	 				
			     	 				if ( schedulejob.getCalendarTrigger() != null )
				    				{
			     	 					ArrayList<BaseDaysMonths> months = CalendarBussines.getCalendarObjectsByType(CalendarBussines.CALENDAR_OBJECTS_TYPE.MONTHS, languageFlag);
			     	 					ArrayList<BaseDaysMonths> days = CalendarBussines.getCalendarObjectsByType(CalendarBussines.CALENDAR_OBJECTS_TYPE.DAYS, languageFlag);
			     	 					
			     	 					JobCalendarTrigger trigger = schedulejob.getCalendarTrigger();
			     	 					
			     	 					if ( trigger.getStartDate() != null)
			     	 						reportDescription.append( startLabel + ": " + convertCalendarToFormatDate( trigger.getStartDate(), formatDate ) + carrierReturn );
			     	 					
			     	 					if ( trigger.getMonths() != null )
			     	 					{
			     	 						int countMonths = 0;
			     	 						//reportDescription.append("Months: ");
			     	 						StringBuilder monthsSets = new StringBuilder();
			     	 						for(int monthNumber : trigger.getMonths() )
			     	 						{
			     	 							for(BaseDaysMonths objMonth : months)
			     	 							{
			     	 								if ( objMonth.getOrder().equals(String.valueOf(monthNumber)) )
			     	 								{
			     	 									monthsSets.append(objMonth.getShortName()+" " );
			     	 								}
			     	 							}			     	 							
				     	 						countMonths++;
			     	 						}
			     	 						String selectedMonths = Languages.getString("jsp.admin.reports.scheduleReport.selectedMonths", sessionData.getLanguage());
			     	 						String allMonths = Languages.getString("jsp.admin.reports.scheduleReport.eachMonth", sessionData.getLanguage());
			     	 						if (countMonths == 12)
			     	 							reportDescription.append(allMonths + carrierReturn );
			     	 						else
			     	 							reportDescription.append(selectedMonths+": "+monthsSets + carrierReturn);
			     	 					}
			     	 					
				    					
			     	 					if ( trigger.getDaysType() != null && trigger.getDaysType().equals(CalendarDaysType.ALL) )
			     	 					{
			     	 						reportDescription.append(Languages.getString("jsp.admin.reports.managescheduleteports.allDays", sessionData.getLanguage())+ carrierReturn );
			     	 					}		     	 						
			     	 					else if ( trigger.getDaysType() != null && trigger.getDaysType().equals(CalendarDaysType.WEEK) )
			     	 					{
			     	 						reportDescription.append(Languages.getString("jsp.admin.reports.managescheduleteports.daysWeek", sessionData.getLanguage())+ ": " );
			     	 						if ( trigger.getWeekDays() != null)
			     	 						{
			     	 							StringBuilder daysWeek = new StringBuilder();
			     	 							for(int dayNumber : trigger.getWeekDays() )
				     	 						{
			     	 								for(BaseDaysMonths objDay : days)
				     	 							{
				     	 								if ( objDay.getOrder().equals(String.valueOf(dayNumber)) )
				     	 								{
				     	 									daysWeek.append(objDay.getShortName()+" " );
				     	 								}
				     	 							}			     	 												     	 						
				     	 						}
			     	 							reportDescription.append(daysWeek + carrierReturn );
			     	 						}			     	 							     	 						
			     	 					}		     	 					
			     	 					else if ( trigger.getDaysType() != null && trigger.getDaysType().equals(CalendarDaysType.MONTH) )
			     	 					{
			     	 						reportDescription.append(Languages.getString("jsp.admin.reports.managescheduleteports.daysMonth", sessionData.getLanguage()) + ": " );
			     	 						if ( trigger.getMonthDays() != null)
					     	 					reportDescription.append(trigger.getMonthDays() + carrierReturn );	
			     	 					}	     	 						
			     	 					
			     	 					String timeLabel = Languages.getString("jsp.admin.reports.scheduleReport.time", sessionData.getLanguage());
			     	 					if ( trigger.getHours() != null)
			     	 						reportDescription.append(timeLabel +": "+ trigger.getHours() + ":"+trigger.getMinutes() + carrierReturn );
			     	 					
			     	 					if ( trigger.getEndDate() != null)
			     	 						reportDescription.append(endLabel+ ": " + convertCalendarToFormatDate( trigger.getEndDate() , formatDate ) + carrierReturn );
			     	 					
				    				}
				    				else if ( schedulejob.getSimpleTrigger() != null )
				    				{
				    					JobSimpleTrigger trigger = schedulejob.getSimpleTrigger();
				    					
				    					if ( trigger.getStartDate() != null)
				    					{			    						
				    						reportDescription.append(startLabel + ": " + convertCalendarToFormatDate( trigger.getStartDate() , formatDate ) + carrierReturn );
				    					}			    					
				    					if ( trigger.getOccurrenceCount() != -1 )
				    					{
				    						String ocurrenceCLabel = Languages.getString("jsp.admin.reports.managescheduleteports.ocurrenceCount", sessionData.getLanguage());
				    						
				     	 					reportDescription.append(ocurrenceCLabel+ ": " +trigger.getOccurrenceCount() + carrierReturn );
				    					}
			     	 					if ( trigger.getRecurrenceInterval() != null)
			     	 					{
			     	 						String unitLabel = Languages.getString("jsp.admin.reports.managescheduleteports.unit", sessionData.getLanguage());
			     	 						String repeatEveryLabel = Languages.getString("jsp.admin.reports.scheduleReport.repeatEvery", sessionData.getLanguage());
			     	 						reportDescription.append( repeatEveryLabel + ": " + trigger.getRecurrenceInterval() + carrierReturn );
			     	 						if ( trigger.getRecurrenceIntervalUnit() != null)
			     	 						{
			     	 							if ( trigger.getRecurrenceIntervalUnit().equals( IntervalUnit.DAY  ) )
			     	 								reportDescription.append(unitLabel+ ": " + Languages.getString("jsp.admin.reports.managescheduleteports.unitDay", sessionData.getLanguage()) + carrierReturn );
			     	 							else if ( trigger.getRecurrenceIntervalUnit().equals( IntervalUnit.HOUR  ) )
			     	 								reportDescription.append(unitLabel+ ": " + Languages.getString("jsp.admin.reports.managescheduleteports.unitHour", sessionData.getLanguage()) + carrierReturn );
			     	 							else if ( trigger.getRecurrenceIntervalUnit().equals( IntervalUnit.MINUTE  ) )
			     	 								reportDescription.append(unitLabel+ ": " + Languages.getString("jsp.admin.reports.managescheduleteports.unitMinute", sessionData.getLanguage()) + carrierReturn );
			     	 							else if ( trigger.getRecurrenceIntervalUnit().equals( IntervalUnit.WEEK  ) )
			     	 								reportDescription.append(unitLabel+ ": " + Languages.getString("jsp.admin.reports.managescheduleteports.unitWeek", sessionData.getLanguage()) + carrierReturn );
			     	 						}		     	 							
			     	 					}				    				
				    					if ( trigger.getEndDate() != null)
				    					{			    						
				    						reportDescription.append(endLabel+ ": " + convertCalendarToFormatDate( trigger.getEndDate() , formatDate ) + carrierReturn );
				    					}
				    				}
			     	 				jobUI.setScheduleDescription( reportDescription.toString() );
			     	 				jobUI.setEmails( schedulejob.getMailNotification().getToAddresses() );
			     	 			}		     	 			
			     	 			catch(Exception e)
			     	 		    {
			     	 				String errorSearching = Languages.getString("jsp.admin.reports.managescheduleteports.errorSearching", sessionData.getLanguage());
	     	 						jobUI.setErrorDescription( errorSearching );
			     	 		    	cat.info( errorSearching + ": "+e);
			     	 		    }
	     	 				}
	     	 				else
	     	 				{
	     	 					cat.info( "Cannot find job id"+jobId );
	     	 				}
		    			}
		    		}		    			   
		    	 }
	    	 } 	 
	    	  
	    }
	    catch(Exception e)
	    {
	    	cat.info(e.getMessage());     
	    }
	    return jobsList;
	    
	}


	private static void parseCreationJobDate(String[] jobInfo, JobUI jobUI, String formatUI){
		SimpleDateFormat ff = new SimpleDateFormat("yyyyddMM HHmmss");
		Date d;
		try 
		{
			d = ff.parse(jobInfo[2]+" "+ jobInfo[3]);
			ff.applyPattern(formatUI); //"yyyy-dd-MM hh:mm:ss a");
			String rta = ff.format(d);
			jobUI.setCreationDate(rta);
		} 
		catch (ParseException e) {
			cat.info( "Error parseCreationJobDate "+e );
		}
	}

	
	/**
	 * @param scheduler
	 * @param jobId
	 * @return
	 */
	private static Job findJobById(ReportSchedulerSoapBindingStub scheduler,	long jobId) 
	{		
		Job job = null;
		try
		{
			job = scheduler.getJob(jobId);
		}
		catch(Exception e)
		{
			cat.info("CANNOT GET JOB BY ID ",e);    
		}
		return job;
	}


	/**
	 * @param date
	 * @param format
	 * @return
	 */
	private static String convertCalendarToFormatDate(Calendar date, String format)
	{		
		DateFormat formatter = new SimpleDateFormat(format); 
		//Calendar calendar = Calendar.getInstance();
		return formatter.format(date.getTime());
	}

	/**
	 * @param sessionData
	 * @param jos
	 * @param jobUI
	 */
	private static void defineReportGroup(SessionData sessionData, JobSummary jos, JobUI jobUI) 
	{
		String unitURI = jos.getReportUnitURI();
		String unitInfo[] = unitURI.split("/");
		if ( unitInfo != null && unitInfo.length>0) 
		{
			String unitReport = unitInfo[1];
			if ( unitReport.equals(DebisysConstants.SC_TRX_SUMM_BY_AGENT) )
			{
				unitReport = Languages.getString("jsp.admin.reports.transaction.agent_summary", sessionData.getLanguage() );
			}
			else if ( unitReport.equals(DebisysConstants.SC_TRX_SUMM_BY_SUBAGENT) )
			{		     	 					
				unitReport = Languages.getString("jsp.admin.reports.transaction.subagent_summary", sessionData.getLanguage() );
			}
			else if ( unitReport.equals(DebisysConstants.SC_TRX_SUMM_BY_REP) )
			{
				unitReport = Languages.getString("jsp.admin.reports.transaction.rep_summary", sessionData.getLanguage() );
			}
			else if ( unitReport.equals(DebisysConstants.SC_TRX_SUMM_BY_MERCHANT) )
			{
				unitReport = Languages.getString("jsp.admin.reports.transaction.merchant_summary", sessionData.getLanguage() );
			}
			else if ( unitReport.equals(DebisysConstants.SC_CREDIT_PAYMENT_SUMM_BY_MERCHANT) )
			{
				unitReport = Languages.getString("jsp.admin.reports.title6", sessionData.getLanguage() );
			}
			else if ( unitReport.equals( DebisysConstants.SC_TRX_SUMM_BY_TERMINAL ) )
			{
				unitReport = Languages.getString("jsp.admin.reports.transaction.merchant_summarybyterminal", sessionData.getLanguage() );
			}
			else if ( unitReport.equals( DebisysConstants.SC_TRX_SUM_BY_TERMINAL_TYPE ) )
			{
				unitReport = Languages.getString("jsp.admin.reports.title36", sessionData.getLanguage() );
			}
			else if ( unitReport.equals( DebisysConstants.SC_TRX_ERROR_SUM_BY_MERCHANT ) )
			{
				unitReport = Languages.getString("jsp.admin.reports.title33", sessionData.getLanguage() );
			}
			else if ( unitReport.equals( DebisysConstants.SC_TRX_SUMM_BY_PHONE_NUMBER ) )
			{
				unitReport = Languages.getString("jsp.admin.reports.title34", sessionData.getLanguage() );
			}
			else if ( unitReport.equals( DebisysConstants.SC_TRX_SUMM_BY_CITY ) )
			{
				unitReport = Languages.getString("jsp.admin.reports.title44", sessionData.getLanguage() );
			}
			else if ( unitReport.equals( DebisysConstants.SC_CRED_PAYMENT_SUMM_BY_USER ) )
			{
				unitReport = Languages.getString("jsp.admin.reports.title40", sessionData.getLanguage() );
			}
			jobUI.setGroupReport(unitReport);
		}
	}
	
	

}

