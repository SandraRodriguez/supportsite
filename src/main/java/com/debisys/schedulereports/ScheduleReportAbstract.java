package com.debisys.schedulereports;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.debisys.utils.DebisysConstants;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignParameter;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JRDesignVariable;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.CalculationEnum;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.type.PositionTypeEnum;

/**
 * 
 * @author dgarzon
 *
 */
public abstract class ScheduleReportAbstract {

	/**
	 * 
	 */
	static Logger cat = Logger.getLogger(ScheduleReportAbstract.class);
	
	/**
	 * Every object that set relative dates must be have in the sql statement this token, this will be change at the moment of the schedule.
	 */
	public static final String RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT = "RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT";
	
	
	/**
	 * Every object that set relative dates must be have in the sql statement this token, this will be change at the moment of the schedule.
	 */
	public static final String WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS = "FROM_COMMODIN_TO_SCHEDULER_COMPONENT";
	
	
	
	/**
	 * Every object that set relative dates must be have in the sql statement this token, this will be change at the moment of the schedule.
	 */
	public static final String WILDCARD_END_DATE_FROM_CLAUSE = "endDate";
	
	
	/**
	 * Every object that set relative dates must be have in the sql statement this token, this will be change at the moment of the schedule.
	 */
	public static final String WILDCARD_START_DATE_FROM_CLAUSE = "startDate";
	
	
	/**
	 * 
	 */
	public static final String WILDCARD_HEADER_CLAUSE_TO_SCHEDULE_REPORTS = "FROM_HEADER_TO_SCHEDULER_COMPONENT";
	
	
	/**
	 * 
	 */
	public static final String WILDCARD_FOOTER_CLAUSE_TO_SCHEDULE_REPORTS = "FROM_FOOTER_TO_SCHEDULER_COMPONENT";
	
	/**
	 * 
	 */
	public static final String WILDCARD_STORE_PROCEDURE_TO_START_DATE = "WILDCARD_STORE_PROCEDURE_TO_START_DATE";
	
	/**
	 * 
	 */
	public static final String WILDCARD_STORE_PROCEDURE_TO_END_DATE = "WILDCARD_STORE_PROCEDURE_TO_END_DATE";
	
	//private JasperDesign jasperDesignMain;
	/**
	 * Main class provider by jasper to design a report
	 */
	private JasperDesign jasperDesign;
	private final int CELL_WIDTH = 180;
    private final int CELL_HEIGHT = 20;
    private int queryType = 0;    
    private boolean alreadyCompiled=false;
    
    
    
    
    
	public ScheduleReportAbstract()  {
		init(getName());
	}
	
	public ScheduleReportAbstract(String name)  {
		init(name);
	}
	
	public void init(String name)  {
		try {
			jasperDesign = new JasperDesign();
			jasperDesign.setName(name);
			jasperDesign.setPageWidth(2800);
			jasperDesign.setPageHeight(60000);
			jasperDesign.setColumnWidth(515);
			jasperDesign.setColumnSpacing(0);
			jasperDesign.setLeftMargin(40);
			jasperDesign.setRightMargin(40);
			jasperDesign.setTopMargin(50);
			jasperDesign.setBottomMargin(50);
			jasperDesign.addStyle(getBoldStyle()); // Import Bold Style
		} catch (JRException e) {
			cat.error("Error in ScheduleReportFactory",e); 
		}
	}

	public void buildParameters() throws JRException {
		List<FieldParameters> listParameters = getListParameters();
		for (FieldParameters parameters : listParameters) {
			JRDesignParameter parameter = new JRDesignParameter();
			parameter.setName(parameters.getName());
			parameter.setValueClass(parameters.getClazz());
			if ( parameters.getExpression() != null )
			{
				parameter.setDefaultValueExpression( new JRDesignExpression(parameters.getExpression()) );
			}				
			jasperDesign.addParameter(parameter);
		}
	}

	public void buildQuery() {
		JRDesignQuery query = new JRDesignQuery();
		if ( this.queryType == 1 ){			
			query.setText(getRelativeQuery());
		}			
		else
		{
			query.setText(getFixedQuery());
		}					
		jasperDesign.setQuery(query);
	}
    
	public void buildFields() throws JRException {
		List<FieldParameters> listFields = getListFieldsQuery();
		for (FieldParameters fields : listFields) 
		{			
			JRDesignField field = new JRDesignField();
			field.setName(fields.getName());
			field.setValueClass(fields.getClazz());
			try{
				jasperDesign.addField(field);
			}
			catch(Exception e){
				System.out.println("FIELD ALREADY EXIST IN THE REPORT!!!!!");
			}			
		}
		if ( this.queryType == 1 || this.getName().equals( DebisysConstants.SC_TRX_DAILY_LIABILITY_REPORT) )
		{
			JRDesignField fieldStartDate = new JRDesignField();
			fieldStartDate.setName( WILDCARD_START_DATE_FROM_CLAUSE );
			fieldStartDate.setValueClass(java.lang.String.class);
			jasperDesign.addField( fieldStartDate );
			
			JRDesignField fieldEndDate = new JRDesignField();
			fieldEndDate.setName( WILDCARD_END_DATE_FROM_CLAUSE );
			fieldEndDate.setValueClass(java.lang.String.class);
			jasperDesign.addField( fieldEndDate );
		}
				
	}
	
	public void buildVariables() throws JRException{
		
		List<JasperVariables> listVariables = getListVariables();
		for (JasperVariables jasperVariables : listVariables) 
		{
			JRDesignVariable variableSum = new JRDesignVariable();
			String name = jasperVariables.getName();
	        variableSum.setName( name );
	        variableSum.setValueClass(jasperVariables.getClazz());
	        variableSum.setExpression(new JRDesignExpression(jasperVariables.getExpression()));
	        System.out.println("jasperVariables :"+jasperVariables.getName());
	        if ( !jasperVariables.getIsCalculate() )
	        {
	        	if ( jasperVariables.getCalculationType() == null || jasperVariables.getCalculationType().equals(CalculationEnum.SUM) )
	        		variableSum.setCalculation(CalculationEnum.SUM);
	        	else if ( jasperVariables.getCalculationType().equals(CalculationEnum.AVERAGE) )
	        		variableSum.setCalculation(CalculationEnum.AVERAGE);
	        	else
	        		variableSum.setCalculation(CalculationEnum.NOTHING);
	        }	        	
	        else
	        {
	        	variableSum.setCalculation(CalculationEnum.NOTHING);
	        }
	        jasperDesign.addVariable(variableSum);	          
		}
		System.out.println("END");
		
	}
	
	public void buildHeaders(){		
		JRDesignBand bandHeader = new JRDesignBand();
		int x = 0;
        int rowStart = getRowNumberStart();		
		List<String> listHeaders = getListHeaders();		
        bandHeader.setHeight(CELL_HEIGHT+1);
        for (String header : listHeaders) {
            JRDesignStaticText staticTextHeader1 = new JRDesignStaticText();
            staticTextHeader1.setX(x);
            staticTextHeader1.setY(rowStart);
            staticTextHeader1.setWidth(CELL_WIDTH);
            staticTextHeader1.setHeight(CELL_HEIGHT);
            staticTextHeader1.setMode(ModeEnum.OPAQUE);
            staticTextHeader1.setHorizontalAlignment(HorizontalAlignEnum.LEFT);
            staticTextHeader1.setStyle(getBoldStyle());
            staticTextHeader1.setText(header);
            staticTextHeader1.getLineBox().getLeftPen().setLineWidth(1);
            staticTextHeader1.getLineBox().getTopPen().setLineWidth(1);
            staticTextHeader1.getLineBox().setLeftPadding(10);
            bandHeader.addElement(staticTextHeader1);
            x += CELL_WIDTH;
        }
        //jasperDesign.setPageHeader(bandHeader);
        jasperDesign.setColumnHeader(bandHeader);
	}
	
	public void buildDetails()
        {
            JRDesignBand bandFooter = new JRDesignBand();
            List<ColumnDetail> listDetail = getListDetail();
            JRDesignBand bandDetail = new JRDesignBand();
            bandDetail.setHeight(CELL_HEIGHT+1);
            int x = 0;
            int rowStart = getRowNumberStart();
            HashMap<String, Integer> xPositions = new HashMap<String, Integer>();
            for (ColumnDetail columnDetail : listDetail) 
            {	
        	if ( columnDetail.isPutInTemplate() )
        	{
	        	String name = columnDetail.getName();
	            JRDesignTextField textField = new JRDesignTextField();
	            textField.setKey( name );
	            textField.setX(x);
	            textField.setY(rowStart);
	            textField.setWidth(CELL_WIDTH);
	            textField.setHeight(CELL_HEIGHT);
	            textField.setStretchWithOverflow(true);
	            textField.getLineBox().getTopPen().setLineWidth(1);
	            textField.getLineBox().getRightPen().setLineWidth(1);
	            textField.getLineBox().setLeftPadding(10);
	            textField.setBlankWhenNull(true);
	            if (columnDetail.isNumber()) {
	                textField.setHorizontalAlignment(HorizontalAlignEnum.RIGHT);                
	            } else {
	                textField.setPositionType(PositionTypeEnum.FLOAT);
	            }            
	            if  ( columnDetail.getClazzType().equals(java.lang.Double.class) )
	            	textField.setPattern("#,##0.00");            
	            
	            JRDesignExpression expression = null;
	            if ( columnDetail.getValueExpressionWhenIsCalculate() != null)
	        	{
	            	expression = new JRDesignExpression( columnDetail.getValueExpressionWhenIsCalculate() );
	        	}
	            else
	            {
	            	expression = new JRDesignExpression( name );	
	            }
	            
	            textField.setExpression(expression);
	            bandDetail.addElement( textField );           
	            xPositions.put(name, x);
	            x += CELL_WIDTH;
        	}
        }
               
        ((JRDesignSection) jasperDesign.getDetailSection()).addBand(bandDetail);

        x = 0;
        rowStart = getRowNumberStart();
        List<String> listTitles = getListTitles();
        JRDesignBand bandHeader = new JRDesignBand();		
        for ( String title : listTitles )
        {
            JRDesignStaticText textStaticTitle = new JRDesignStaticText();
            String tmptitle = title.replaceAll("&nbsp;", "");
            textStaticTitle.setText(tmptitle);
            textStaticTitle.setX(0);
            textStaticTitle.setY(x);
            textStaticTitle.setWidth(CELL_WIDTH*5);
            textStaticTitle.setHeight(CELL_HEIGHT);
            bandHeader.addElement( textStaticTitle );
            x += CELL_HEIGHT;
        }
				
        String startDateExpression = "$F{"+WILDCARD_START_DATE_FROM_CLAUSE+"}";
        String endDateExpression = "$F{"+WILDCARD_END_DATE_FROM_CLAUSE+"}";
		
        if ( this.queryType == 0 && !this.getName().equals(DebisysConstants.SC_TRX_DAILY_LIABILITY_REPORT) )
        {
            //IF FIXED DATES WAS SELECTED
            startDateExpression  = this.getStartDateFixedQuery();
            endDateExpression = this.getEndDateFixedQuery();

            JRDesignStaticText textStaticTitleStartDate = new JRDesignStaticText();
            textStaticTitleStartDate.setText( startDateExpression );
            textStaticTitleStartDate.setX(0);
            textStaticTitleStartDate.setY(x);
            textStaticTitleStartDate.setWidth(CELL_WIDTH*5);
            textStaticTitleStartDate.setHeight(CELL_HEIGHT);
            bandHeader.addElement( textStaticTitleStartDate );
            x += CELL_HEIGHT;
			
            JRDesignStaticText textStaticTitleEndDate = new JRDesignStaticText();
            textStaticTitleEndDate.setText( endDateExpression );
            textStaticTitleEndDate.setX(0);
            textStaticTitleEndDate.setY(x);
            textStaticTitleEndDate.setWidth(CELL_WIDTH*5);
            textStaticTitleEndDate.setHeight(CELL_HEIGHT);
            bandHeader.addElement( textStaticTitleEndDate );
            x += CELL_HEIGHT;
			
        }
        else
        {
            ///////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////		
            JRDesignTextField textFieldStartDate = new JRDesignTextField();
            textFieldStartDate.setX(0);
            textFieldStartDate.setY(x);
            x += CELL_HEIGHT;
            textFieldStartDate.setWidth(CELL_WIDTH);
            textFieldStartDate.setHeight(CELL_HEIGHT);        
            JRDesignExpression expressionStartDate = new JRDesignExpression( startDateExpression );        
            textFieldStartDate.setExpression( expressionStartDate );
            textFieldStartDate.setBlankWhenNull(true);
            bandHeader.addElement( textFieldStartDate );
            /////////////////////////////////////////////////////////////////////////////////////       
            JRDesignTextField textFieldEndDate = new JRDesignTextField();
            textFieldEndDate.setX(0);
            textFieldEndDate.setY(x);
            x += CELL_HEIGHT;
            textFieldEndDate.setWidth(CELL_WIDTH);
            textFieldEndDate.setHeight(CELL_HEIGHT);  
            textFieldEndDate.setBlankWhenNull(true);
            JRDesignExpression expressionEndDate = new JRDesignExpression( endDateExpression );        
            textFieldEndDate.setExpression( expressionEndDate );        
            bandHeader.addElement( textFieldEndDate );        
            /////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////
        }
		
        bandHeader.setHeight(x+1);
        jasperDesign.setPageHeader( bandHeader );
        
        try
        {				
            List<JasperVariables> listVariables = getListVariables();
            for (JasperVariables jasperVariables : listVariables) 
            {
                String name = jasperVariables.getName();
                String nameDetailColumn = jasperVariables.getExpression();
                String pattern = jasperVariables.getPattern();

                JRElement detailField = bandDetail.getElementByKey( nameDetailColumn );
                if  ( detailField != null )
                {	        		
                    int xDetailValue = detailField.getX();
                    int yDetailValue = detailField.getY();

                    JRDesignTextField textFieldSumm = new JRDesignTextField();
                    textFieldSumm.setX(xDetailValue);
                    textFieldSumm.setY(yDetailValue);
                    textFieldSumm.setWidth(CELL_WIDTH);
                    textFieldSumm.setHeight(CELL_HEIGHT);		            
                    JRDesignExpression expression = new JRDesignExpression( "$V{"+name+"}" );		            
                    textFieldSumm.setExpression( expression );
                    textFieldSumm.setPattern( pattern );		            
                    bandFooter.setHeight(CELL_HEIGHT+1);
                    bandFooter.addElement( textFieldSumm );		            
                }
                else if ( jasperVariables.getIsCalculate() )
                {
                    detailField = bandDetail.getElementByKey( "$F{"+name+"}" );
                    if ( detailField == null)
                    {
                            String nameRealColumn = name.substring(3);
                            detailField = bandDetail.getElementByKey( "$F{"+nameRealColumn+"}" );
                    }
                    if ( detailField != null)
                    {
                        int xDetailValue = detailField.getX();
                        int yDetailValue = detailField.getY();

                        JRDesignTextField textFieldSumm = new JRDesignTextField();
                        textFieldSumm.setX(xDetailValue);
                        textFieldSumm.setY(yDetailValue);
                        textFieldSumm.setWidth(CELL_WIDTH);
                        textFieldSumm.setHeight(CELL_HEIGHT);		            
                        //String newName = name.replaceFirst("F", "V");
                        JRDesignExpression expression = new JRDesignExpression( "$V{"+name+"}" );		            
                        textFieldSumm.setExpression( expression );
                        textFieldSumm.setPattern( pattern );		            
                        bandFooter.setHeight(CELL_HEIGHT+1);
                        bandFooter.addElement( textFieldSumm );
                    }		        	
                }
                else{
                    detailField = bandDetail.getElementByKey( "$F{"+name.substring(3)+"}" );                    
                    int xDetailValue = detailField.getX();
                    int yDetailValue = detailField.getY();

                    JRDesignTextField textFieldSumm = new JRDesignTextField();
                    textFieldSumm.setX(xDetailValue);
                    textFieldSumm.setY(yDetailValue);
                    textFieldSumm.setWidth(CELL_WIDTH);
                    textFieldSumm.setHeight(CELL_HEIGHT);		            
                    JRDesignExpression expression = new JRDesignExpression( jasperVariables.getExpression() );		            
                    textFieldSumm.setExpression( expression );
                    textFieldSumm.setPattern( pattern );		            
                    bandFooter.setHeight(CELL_HEIGHT+1);
                    bandFooter.addElement( textFieldSumm );
                }
                    
            }	
            jasperDesign.setPageFooter( bandFooter  );
        }
        catch( Exception e)
        {
            cat.info(e);
        }                
    }
	
	public boolean compileAndExport(String pathFile, String pathMain) {
		boolean result = false;
		try 
		{
			if ( !alreadyCompiled )
			{
				buildParameters();
				buildQuery();
				buildFields();
				buildVariables();
				buildHeaders();
				buildDetails();
				alreadyCompiled = true;			
			}		
			JasperReport jreport = JasperCompileManager.compileReport(jasperDesign);
		
			DynamicJasperHelper.generateJRXML(jreport, "UTF-8", pathFile);
			result = true;
		} 
		catch (Exception e) 
		{
			cat.info("Error compiling "+pathFile+" "+e);
		}	
		return result;		
	}
	
	public void setTypeQuery(int queryType){
		this.queryType = queryType;
	}

	protected JRDesignStyle getBoldStyle() {
		JRDesignStyle boldStyle = new JRDesignStyle();
		boldStyle.setName("Arial_Bold");
		boldStyle.setFontName("Arial");
		boldStyle.setFontSize(11);
		boldStyle.setBold(true);
		return boldStyle;
	}

	public abstract String getName();

	public abstract List<FieldParameters> getListParameters();

	public abstract String getRelativeQuery();
	
	public abstract String getFixedQuery();
	
	public abstract List<FieldParameters> getListFieldsQuery();
	
	public abstract List<JasperVariables> getListVariables();
	
	public abstract List<String> getListHeaders();
	
	public abstract int getRowNumberStart();
	
	public abstract List<ColumnDetail> getListDetail();
	
	public abstract List<String> getListTitles();
	
	
	private String startDateFixedQuery;
	private String endDateFixedQuery;
	

	/**
	 * @return the startDateFixedQuery
	 */
	public String getStartDateFixedQuery() {
		return startDateFixedQuery;
	}



	/**
	 * @param startDateFixedQuery the startDateFixedQuery to set
	 */
	public void setStartDateFixedQuery(String startDateFixedQuery) {
		this.startDateFixedQuery = startDateFixedQuery;
	}



	/**
	 * @return the endDateFixedQuery
	 */
	public String getEndDateFixedQuery() {
		return endDateFixedQuery;
	}



	/**
	 * @param endDateFixedQuery the endDateFixedQuery to set
	 */
	public void setEndDateFixedQuery(String endDateFixedQuery) {
		this.endDateFixedQuery = endDateFixedQuery;
	}
}
