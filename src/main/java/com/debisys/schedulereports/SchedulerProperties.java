/**
 * 
 */
package com.debisys.schedulereports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;

/**
 * @author nmartinez
 *
 */
public class SchedulerProperties {

	private static Logger       cat        = Logger.getLogger(SchedulerProperties.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.schedulereports.sql");

	private String url;
	private String user;
	private String password;
	private String repository;
	private String reportscheduler;
	private String subject;
	private String body;
	private String codeSelectedRelativeDate;
	private String formatDateUI;
	private String databaseName;
	

	/**
	 * @param instance
	 * @return
	 */
	public static SchedulerProperties findPropertiesScheduler(String instance)
	{
		SchedulerProperties properties = null;
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			String sql = sql_bundle.getString("findProperties");
			
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement( sql );
			pstmt.setString(1, instance);
			
			ResultSet rs = pstmt.executeQuery();
			properties = new SchedulerProperties();
			while (rs.next())
			{				
				String property = rs.getString("property");
				String value = rs.getString("value");				
				if ( property.equals("jasper.url") )
				{
					properties.setUrl( value );
				}
				else if  ( property.equals("jasper.user") )
				{
					properties.setUser( value );
				}
				else if  ( property.equals("jasper.password") )
				{
					properties.setPassword( value );
				}
				else if  ( property.equals("jasper.repository"))
				{
					properties.setRepository( value );
				}
				else if  ( property.equals("jasper.reportscheduler"))
				{
					properties.setReportscheduler( value );
				}
				else if  ( property.equals("jasper.subject"))
				{
					properties.setSubject( value );
				}
				else if  ( property.equals("jasper.body"))
				{
					properties.setBody( value );
				}
				else if  ( property.equals("jasper.selectedCodeRelativeDate"))
				{
					properties.setCodeSelectedRelativeDate( value );
				}				
				else if  ( property.equals("jasper.formatdate"))
				{
					properties.setFormatDateUI( value );
				}
				else if  ( property.equals("jasper.databaseName"))
				{
					properties.setDatabaseName( value );
				}
			}
			rs.close();
			pstmt.close();
			
		}
		catch(Exception e)
		{
			cat.info("Error-Exception during closeConnection ", e);
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.info("Error during closeConnection ", e);
			}
		}		
		return properties;
	}
	
	
	/**
	 * @return the codeSelectedRelativeDate
	 */
	public String getCodeSelectedRelativeDate() {
		return codeSelectedRelativeDate;
	}


	/**
	 * @param codeSelectedRelativeDate the codeSelectedRelativeDate to set
	 */
	public void setCodeSelectedRelativeDate(String codeSelectedRelativeDate) {
		this.codeSelectedRelativeDate = codeSelectedRelativeDate;
	}

	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}


	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}


	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}


	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	
	/**
	 * @return the reportscheduler
	 */
	public String getReportscheduler() {
		return reportscheduler;
	}
	/**
	 * @param reportscheduler the reportscheduler to set
	 */
	public void setReportscheduler(String reportscheduler) {
		this.reportscheduler = reportscheduler;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the repository
	 */
	public String getRepository() {
		return repository;
	}
	/**
	 * @param repository the repository to set
	 */
	public void setRepository(String repository) {
		this.repository = repository;
	}


	/**
	 * @param formatDateUI the formatDateUI to set
	 */
	public void setFormatDateUI(String formatDateUI) {
		this.formatDateUI = formatDateUI;
	}


	/**
	 * @return the formatDateUI
	 */
	public String getFormatDateUI() {
		return formatDateUI;
	}


	/**
	 * @param databaseName the databaseName to set
	 */
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}


	/**
	 * @return the databaseName
	 */
	public String getDatabaseName() {
		return databaseName;
	}
	
	//public 
	
}
