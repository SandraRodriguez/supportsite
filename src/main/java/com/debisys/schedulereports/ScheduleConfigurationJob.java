package com.debisys.schedulereports;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;

import org.apache.log4j.Logger;

import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.TimeZone;
import com.jaspersoft.jasperserver.api.metadata.xml.domain.impl.ResourceDescriptor;
import com.jaspersoft.jasperserver.api.metadata.xml.domain.impl.ResourceProperty;
import com.jaspersoft.jasperserver.irplugin.JServer;
import com.jaspersoft.jasperserver.irplugin.wsclient.WSClient;
import com.jaspersoft.jasperserver.ws.scheduling.CalendarDaysType;
import com.jaspersoft.jasperserver.ws.scheduling.IntervalUnit;
import com.jaspersoft.jasperserver.ws.scheduling.Job;
import com.jaspersoft.jasperserver.ws.scheduling.JobCalendarTrigger;
import com.jaspersoft.jasperserver.ws.scheduling.JobMailNotification;
import com.jaspersoft.jasperserver.ws.scheduling.JobRepositoryDestination;
import com.jaspersoft.jasperserver.ws.scheduling.JobSimpleTrigger;
import com.jaspersoft.jasperserver.ws.scheduling.ReportSchedulerSoapBindingStub;
import com.jaspersoft.jasperserver.ws.scheduling.ResultSendType;

public class ScheduleConfigurationJob {

    private static Logger cat = Logger.getLogger(ScheduleConfigurationJob.class);

    public static final String SCHEDULE_TYPE_NONE = "0";
    public static final String SCHEDULE_TYPE_SIMPLE = "1";
    public static final String SCHEDULE_TYPE_CALENDAR = "2";

    public static final String REPORT_NAME_PARAMETER = "parameter_ReportName";
    public static final String REPORT_DESCRIPTION_PARAMETER = "parameter_ReportDescription";
    public static final String START_REPORT_PARAMETER = "parameter_startReport";
    public static final String START_DATE_REPORT_PARAMETER = "parameter_startDateReport";

    /**
     * Define the schedule type None, Simple or Calendar
     */
    public static final String SCHEDULE_TYPE_ID_PARAMETER = "parameter_scheduleTypeId";

    public static final String END_DATE_REPORT_PARAMETER = "parameter_endScheduleDate";
    public static final String TIME_ZONE_REPORT_PARAMETER = "parameter_timeZone";
    public static final String RELATIVE_VALUE_PARAMETER = "parameter_relativeValue";
    public static final String RELATIVE_TYPE_ID_PARAMETER = "parameter_relativeTypeId";
    public static final String RELATIVE_INCLUDING_TODAY = "parameter_includingToday";

    //SIMPLE CONTROL NAMES
    public static final String SIMPLE_REPEAT_EVERY_PARAMETER_UNIT = "parameter_simpleRepeatEveryUnit";
    public static final String SIMPLE_REPEAT_EVERY_VALUE_PARAMETER = "parameter_simpleRepeatEveryValue";
    public static final String SIMPLE_RADIO_CONTROL_PARAMETER = "parameter_simpleRadioControl";
    public static final String SIMPLE_TIMES_PARAMETER = "parameter_simpleTimes";

    //CALENDAR CONTROL NAMES
    public static final String CALENDAR_MONTH_EACH_PARAMETER = "parameter_calendarMonthEach";
    public static final String CALENDAR_TRIGGER_MONTHS_PARAMETER = "parameter_calendarTriggerMonths";
    public static final String CALENDAR_DAYS_EACH_PARAMETER = "parameter_calendarDaysEach";
    public static final String CALENDAR_TRIGGER_DAYS_PARAMETER = "parameter_calendarTriggerDays";
    public static final String CALENDAR_DAYS_NUMBER_PARAMETER = "parameter_calendarDaysNumbers";
    public static final String CALENDAR_TIME_EXECUTIONR_PARAMETER = "parameter_calendarTimeExecution";

    public static final String EMAILS_NOTIFICATIONS_PARAMETER = "parameter_emailsNotification";

    private HashMap<String, String> parameterValues = new HashMap<String, String>();

    private SchedulerProperties properties;

    private String urlBack;
    //private static String FORMAT_UI_DATE_CONTROL_CALENDARS = "MM/dd/yyyy HH:mm";
    private static String FORMAT_UI_DATE_CONTROL_UNTIL = "MM/dd/yyyy";

    private boolean captureValuesParameters(javax.servlet.ServletRequest req) {
        boolean result = false;

        String emails = req.getParameter(EMAILS_NOTIFICATIONS_PARAMETER);
        parameterValues.put(EMAILS_NOTIFICATIONS_PARAMETER, emails);

        String scheduleTypeId = req.getParameter(SCHEDULE_TYPE_ID_PARAMETER);
        if (scheduleTypeId == null) {
            scheduleTypeId = SCHEDULE_TYPE_NONE;
        }

        //Set the stardate job, if At is selected
        String startReport = req.getParameter(START_REPORT_PARAMETER);
        String startDateReport = req.getParameter(START_DATE_REPORT_PARAMETER);
        //set the end date
        String endDateSchedule = req.getParameter(END_DATE_REPORT_PARAMETER);

        parameterValues.put(START_REPORT_PARAMETER, startReport);

        if (startReport.equals("a") && startDateReport.length() > 0)//fire the schedule
        {
            //At specfic date time enter by the user			
            parameterValues.put(START_DATE_REPORT_PARAMETER, startDateReport);
        }

        //time zone of the user
        String timeZone = req.getParameter(TIME_ZONE_REPORT_PARAMETER);
        parameterValues.put(TIME_ZONE_REPORT_PARAMETER, timeZone);

        //type of schedule
        parameterValues.put(SCHEDULE_TYPE_ID_PARAMETER, scheduleTypeId);

        if (scheduleTypeId.equals(SCHEDULE_TYPE_SIMPLE)) //schedule simple
        {
            if (endDateSchedule != null && endDateSchedule.length() > 0)//end date schedule
            {
                parameterValues.put(END_DATE_REPORT_PARAMETER, endDateSchedule);
            }

            String simpleRepeatEveryUnit = req.getParameter(SIMPLE_REPEAT_EVERY_PARAMETER_UNIT);
            parameterValues.put(SIMPLE_REPEAT_EVERY_PARAMETER_UNIT, simpleRepeatEveryUnit);

            String simpleRepeatValue = req.getParameter(SIMPLE_REPEAT_EVERY_VALUE_PARAMETER);
            parameterValues.put(SIMPLE_REPEAT_EVERY_VALUE_PARAMETER, simpleRepeatValue);

			//String simpleRadioControl = req.getParameter( SIMPLE_RADIO_CONTROL_PARAMETER );
            //parameterValues.put( SIMPLE_RADIO_CONTROL_PARAMETER , simpleRadioControl);
            //if ( simpleRadioControl.equals("t") ) //time
            //{
            //	String calMonthEach = req.getParameter( SIMPLE_TIMES_PARAMETER );
            //	parameterValues.put( SIMPLE_TIMES_PARAMETER , calMonthEach);
            //}				
            result = true;
        } else if (scheduleTypeId.equals(SCHEDULE_TYPE_CALENDAR)) //schedule calendar
        {
            if (endDateSchedule != null && endDateSchedule.length() > 0)//end date schedule
            {
                parameterValues.put(END_DATE_REPORT_PARAMETER, endDateSchedule);
            }

            String calendarMonthEach = req.getParameter(CALENDAR_MONTH_EACH_PARAMETER);
            parameterValues.put(CALENDAR_MONTH_EACH_PARAMETER, calendarMonthEach);
            if (calendarMonthEach.equals("s")) {
                String[] calendarMonthSelected = req.getParameterValues(CALENDAR_TRIGGER_MONTHS_PARAMETER);
                StringBuilder builder = new StringBuilder();
                for (String s : calendarMonthSelected) {
                    builder.append(s + ",");
                }
                parameterValues.put(CALENDAR_TRIGGER_MONTHS_PARAMETER, builder.toString());
            }
            String calendarDaysEach = req.getParameter(CALENDAR_DAYS_EACH_PARAMETER);
            parameterValues.put(CALENDAR_DAYS_EACH_PARAMETER, calendarDaysEach);
            if (calendarDaysEach.equals("s")) {
                String[] calendarDaysSelected = req.getParameterValues(CALENDAR_TRIGGER_DAYS_PARAMETER);
                StringBuilder builder = new StringBuilder();
                for (String s : calendarDaysSelected) {
                    builder.append(s + ",");
                }
                parameterValues.put(CALENDAR_TRIGGER_DAYS_PARAMETER, builder.toString());
            } else if (calendarDaysEach.equals("d")) {
                String calendarDaysNumber = req.getParameter(CALENDAR_DAYS_NUMBER_PARAMETER);
                if (calendarDaysNumber != null && calendarDaysNumber.length() > 0) {
                    parameterValues.put(CALENDAR_DAYS_NUMBER_PARAMETER, calendarDaysNumber);
                } else {
                    parameterValues.put(CALENDAR_DAYS_NUMBER_PARAMETER, "1");
                }
            }
            String calendarTime = req.getParameter(CALENDAR_TIME_EXECUTIONR_PARAMETER);
            parameterValues.put(CALENDAR_TIME_EXECUTIONR_PARAMETER, calendarTime);
            result = true;
        }
        result = true;
        return result;
    }

    /**
     * @param sessionData
     * @param context
     * @param pathTemplate
     * @param request
     */
    public void saveSchedule(SessionData sessionData, ServletContext context, String pathTemplate, ServletRequest request) {
        boolean result = false;
        String userName = sessionData.getUser().getUsername();
        String uniqueId = sessionData.getUser().getPasswordId();
        String language = sessionData.getUser().getLanguageCode();
        String strRefId = sessionData.getProperty("ref_id");
        String strAccessLevel = sessionData.getProperty("access_level");
        String dateBehaviour = request.getParameter("dateBehaviour");
        String relativeTypeIdParam = request.getParameter(RELATIVE_TYPE_ID_PARAMETER);
        String relativeValueParam = request.getParameter(RELATIVE_VALUE_PARAMETER);
        String includeToday = request.getParameter(RELATIVE_INCLUDING_TODAY);

        ScheduleReport scheduleReport = (ScheduleReport) sessionData.getPropertyObj(DebisysConstants.SC_SESS_VAR_NAME);
        String jobName = scheduleReport.getName();

        boolean parametersOk = captureValuesParameters(request);

        String tmpRelativeQuery = "";
        String tmpFixedQuery = "";

        if (scheduleReport != null && parametersOk) {
            tmpRelativeQuery = scheduleReport.getRelativeQuery();
            tmpFixedQuery = scheduleReport.getFixedQuery();

            if (dateBehaviour == null || dateBehaviour.equals("fixed")) {
                String tmpFixed = tmpFixedQuery.replaceAll(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS, "");
                tmpFixed = tmpFixed.replaceAll(ScheduleReport.WILDCARD_HEADER_CLAUSE_TO_SCHEDULE_REPORTS, "");
                tmpFixed = tmpFixed.replaceAll(ScheduleReport.WILDCARD_FOOTER_CLAUSE_TO_SCHEDULE_REPORTS, "");
                scheduleReport.setFixedQuery(tmpFixed);
                scheduleReport.setTypeQuery(0);
            } else {
                scheduleReport.setTypeQuery(1);
                if (relativeValueParam == null || relativeValueParam.length() == 0) {
                    relativeValueParam = "1";
                }
                buildRelativeQueryByOption(scheduleReport, relativeTypeIdParam, relativeValueParam, includeToday, language, strRefId, strAccessLevel);
            }

            try {
                String formatDate = "ddMMyyyyhhmmssSSS";
                formatDate = "yyyyddMM_HHmmss";
                String uniName = userName.replaceAll("_", "-") + "_" + uniqueId + "_" + convertSecondsToDate(System.currentTimeMillis(), formatDate);
                String templateName = pathTemplate + uniName + ".jrxml";
                String templateNameMain = pathTemplate + uniName + "Main.jrxml";
                cat.info("Starting compiling report...");
                result = scheduleReport.compileAndExport(templateName, templateNameMain);
                if (result) {
                    result = uploadFile(scheduleReport.getName(), templateName, uniName);
                    if (result) {
                        cat.info("Starting scheduled report...");
                        result = scheduleReport(sessionData, scheduleReport.getName(), uniName);
                        if (result) {
                            cat.info("Scheduled report success");
                        } else {
                            cat.info("Cannot scheduled report " + uniName);
                        }
                    } else {
                        cat.info("Cannot scheduled report..." + uniName);
                    }
                } else {
                    cat.info("Cannot compile report..." + uniName);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                scheduleReport.setRelativeQuery(tmpRelativeQuery);
                scheduleReport.setFixedQuery(tmpFixedQuery);
            }

        } else {
            cat.info("No session variable " + DebisysConstants.SC_SESS_VAR_NAME);
        }

        definePageToBack(jobName);
    }

    private void definePageToBack(String jobName) {
        if (jobName.equals(DebisysConstants.SC_TRX_SUMM_BY_AGENT)) {
            this.urlBack = "/support/admin/reports/transactions/agents.jsp";
        } else if (jobName.equals(DebisysConstants.SC_TRX_SUMM_BY_SUBAGENT)) {
            this.urlBack = "/support/admin/reports/transactions/subagents.jsp";
        } else if (jobName.equals(DebisysConstants.SC_TRX_SUMM_BY_REP)) {
            this.urlBack = "/support/admin/reports/transactions/reps.jsp";
        } else if (jobName.equals(DebisysConstants.SC_TRX_SUMM_BY_MERCHANT)) {
            this.urlBack = "/support/admin/reports/transactions/merchants.jsp";
        } else {
            this.urlBack = "/support/admin/index.jsp";
        }

    }

    /**
     *
     * @param scheduleReport
     * @param parameterValueRelative
     * @param value
     * @param includeToday
     * @param languageFlag
     * @param isoId
     * @param accessLevel
     */
    private void buildRelativeQueryByOption(ScheduleReport scheduleReport, String parameterValueRelative, String value,
            String includeToday, String languageFlag, String isoId, String accessLevel) {
        ArrayList<RelativeOptions> arrRelative = new ArrayList<RelativeOptions>();
        arrRelative = RelativeOptions.findRelativeOptions(languageFlag);
        String[] paramsV = parameterValueRelative.split("-");
        String clientTimeZone = " dbo.GetLocalTimeByAccessLevel( " + accessLevel + ", " + isoId + ", %s, 0) ";
        StringBuilder sqlWhere = new StringBuilder();
        String startFunc = "";
        String endFunc = "";

        if (paramsV.length == 4) {
            String idRelativeOption = paramsV[0];
            String needAdditionalData = paramsV[1];
            String nameColumnDateTime = scheduleReport.getNameDateTimeColumn();

            for (RelativeOptions opt : arrRelative) {
                if (opt.getId().equals(idRelativeOption)) {
                    startFunc = opt.getStartFunction();
                    endFunc = opt.getEndFunction();
                    if (includeToday != null) {
                        if (includeToday.equals("1")) {
                            endFunc = endFunc.replaceFirst("UNTIL_NOW", "1");
                        } else {
                            endFunc = endFunc.replaceFirst("UNTIL_NOW", "0");
                        }
                    }
                    ////////////////////////////////////////////////////////
                    //to control any case
                    endFunc = endFunc.replaceFirst("UNTIL_NOW", "0");
                    ////////////////////////////////////////////////////////

                    if (opt.getCaptureData().equals(needAdditionalData) && opt.getCaptureData().equals("1")) {
                        startFunc = startFunc.replaceFirst("NUMBER_CAPTURE", value);

                        sqlWhere.append(" " + nameColumnDateTime + " > ").append(String.format(clientTimeZone, startFunc)).append(" AND ")
                                .append(" " + nameColumnDateTime + " < ").append(String.format(clientTimeZone, endFunc));
                    } else {
                        sqlWhere.append(" " + nameColumnDateTime + " > ").append(String.format(clientTimeZone, startFunc)).append(" AND ")
                                .append(" " + nameColumnDateTime + " <").append(String.format(clientTimeZone, endFunc));
                    }
                    break;
                }
            }
        }

        String finalQuery = scheduleReport.getRelativeQuery().replaceAll(ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT, sqlWhere.toString());
        finalQuery = finalQuery.replaceAll(ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE, String.format(clientTimeZone, startFunc))
                .replaceAll(ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE, String.format(clientTimeZone, endFunc));

        if (scheduleReport.getHastTokensRelativeDates() != null) {
            HashMap<String, String> hashTokens = scheduleReport.getHastTokensRelativeDates();
            Set<Map.Entry<String, String>> entries = hashTokens.entrySet();

            for (Map.Entry<String, String> entry : entries) {
                String key = entry.getKey();
                String valueSet = entry.getValue();
                finalQuery = finalQuery.replaceAll(key, " " + valueSet + " > " + startFunc + " AND " + valueSet + " < " + endFunc);
            }
        }

        String startDateConvert = "CONVERT(VARCHAR(20)," + startFunc + ",121)";
        String endDateConvert = "CONVERT(VARCHAR(20)," + endFunc + ",121)";

        finalQuery = finalQuery.replaceAll(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS, " ," + startDateConvert + " " + ScheduleReport.WILDCARD_START_DATE_FROM_CLAUSE + " , " + endDateConvert + " " + ScheduleReport.WILDCARD_END_DATE_FROM_CLAUSE + " ");

        finalQuery = finalQuery.replaceAll(ScheduleReport.WILDCARD_HEADER_CLAUSE_TO_SCHEDULE_REPORTS, "," + ScheduleReport.WILDCARD_START_DATE_FROM_CLAUSE + "," + ScheduleReport.WILDCARD_END_DATE_FROM_CLAUSE);
        finalQuery = finalQuery.replaceAll(ScheduleReport.WILDCARD_FOOTER_CLAUSE_TO_SCHEDULE_REPORTS, "," + ScheduleReport.WILDCARD_START_DATE_FROM_CLAUSE + "," + ScheduleReport.WILDCARD_END_DATE_FROM_CLAUSE);

        cat.info("********************************************");
        cat.info("THIS IS THE FINAL QUERY THAT WILL BE EXECUTED IN JASPERSERVER");
        cat.info(finalQuery);
        cat.info("********************************************");
        scheduleReport.setRelativeQuery(finalQuery);
    }

    /**
     * Upload a template to the server and saved in a specific path
     *
     * @param reportName
     * @param templateFile
     */
    private boolean uploadFile(String reportName, String templateFile, String ownerReport) {
        File jrxml = null;
        boolean bresult = false;
        try {
            JServer server = new JServer();
            String url = this.properties.getUrl();
            String repository = this.properties.getRepository();
            String user = this.properties.getUser();
            String password = this.properties.getPassword();
            String dataBaseName = this.properties.getDatabaseName();

            server.setUrl(url + repository);
            server.setUsername(user);
            server.setPassword(password);

            bresult = createFolderReportMaster(server, reportName);

            if (bresult) {
                ResourceDescriptor newFolder = new ResourceDescriptor();
                newFolder.setName(ownerReport);
                newFolder.setResourceType(ResourceDescriptor.TYPE_FOLDER);
                newFolder.setLabel(ownerReport);
                newFolder.setParentFolder("/" + reportName);
                newFolder.setUriString(newFolder.getParentFolder() + "/" + newFolder.getName());
                newFolder.setWsType(ResourceDescriptor.TYPE_FOLDER);
                newFolder.setIsNew(true);
                ResourceDescriptor resultfolder = server.getWSClient().addOrModifyResource(newFolder, null);

                ResourceDescriptor rdReportUnit = new ResourceDescriptor();
                rdReportUnit.setWsType(ResourceDescriptor.TYPE_REPORTUNIT);
                rdReportUnit.setUriString("/" + reportName + "/" + ownerReport);
                //rdReportUnit.setName(reportName+"_"+ownerReport+"_Unit");
                rdReportUnit.setName(ownerReport + "_Unit");
                rdReportUnit.setLabel(ownerReport);

                ////////////////////////////////////////////
                //CHANGE HERE THE REPORT DESCRIPTION
                rdReportUnit.setDescription("");
                ////////////////////////////////////////////

                rdReportUnit.setResourceProperty(new ResourceProperty(ResourceDescriptor.PROP_PARENT_FOLDER, "/" + reportName + "/" + ownerReport));
                rdReportUnit.setIsNew(true);

                ResourceDescriptor rdDataSource = new ResourceDescriptor();
                rdDataSource.setWsType(ResourceDescriptor.TYPE_DATASOURCE);
                rdDataSource.setResourceProperty(new ResourceProperty(ResourceDescriptor.PROP_FILERESOURCE_REFERENCE_URI, "/datasources/" + dataBaseName));
                rdDataSource.setResourceProperty(new ResourceProperty(ResourceDescriptor.PROP_FILERESOURCE_IS_REFERENCE, "true"));

                ResourceDescriptor rdJrxml = buildResourceDescriptorJRXML(reportName, ownerReport);

                List<ResourceDescriptor> children = new ArrayList<ResourceDescriptor>();
                children.add(rdDataSource);
                children.add(rdJrxml);
                rdReportUnit.setChildren(children);
                jrxml = new File(templateFile);

                cat.info("DELETING FILE " + templateFile);
                server.getWSClient().addOrModifyResource(rdReportUnit, jrxml);
                if (jrxml.delete()) {
                    cat.info("DELETED FILE " + templateFile);
                } else {
                    cat.info("**********CANNOT DELETE FILE " + templateFile + " *************************************");
                }
                jrxml = null;
                bresult = true;
            } else {
                cat.info("Cannot create the master folder for " + reportName);
            }
        } catch (Exception ex) {
            cat.info("***********EXCEPTION " + ex);
        }
        return bresult;
    }

    /**
     * @param reportName
     * @param ownerReport
     * @return
     */
    private ResourceDescriptor buildResourceDescriptorJRXML(String reportName, String ownerReport) {
        ResourceDescriptor rdJrxml = new ResourceDescriptor();
        rdJrxml.setWsType(ResourceDescriptor.TYPE_JRXML);
        rdJrxml.setIsNew(true);
        rdJrxml.setHasData(true);
        rdJrxml.setMainReport(true);
        rdJrxml.setLabel(reportName + "Main jrxml");
        rdJrxml.setDescription(reportName + "Main jrxml");
        rdJrxml.setUriString("/" + reportName + "/" + ownerReport + "_emida");
        return rdJrxml;
    }

    /**
     * @param server
     * @param reportName
     * @return
     */
    private boolean createFolderReportMaster(JServer server, String reportName) {
        boolean result = false;
        ResourceDescriptor findFolder = new ResourceDescriptor();
        findFolder.setWsType(ResourceDescriptor.TYPE_FOLDER);
        findFolder.setUriString("/" + reportName);
        findFolder.setIsNew(false);
        try {
            List<?> list = server.getWSClient().list(findFolder);

            // CREATE FOLDER 
            if (list.isEmpty()) {
                ResourceDescriptor newFolder = new ResourceDescriptor();
                newFolder.setName(reportName);
                newFolder.setResourceType(ResourceDescriptor.TYPE_FOLDER);
                newFolder.setLabel(reportName);
                newFolder.setDescription(reportName);
                newFolder.setUriString(newFolder.getParentFolder() + "/" + newFolder.getName());
                newFolder.setWsType(ResourceDescriptor.TYPE_FOLDER);
                newFolder.setIsNew(true);
                ResourceDescriptor resultfolder = server.getWSClient().addOrModifyResource(newFolder, null);
                //System.out.println("FOLDER CREATED ************" + resultfolder.toString());
                result = true;
            } else {
                result = true;
            }
        } catch (Exception e) {
            cat.info("createFolderReportMaster " + e);
            //TODO:
            //Fixing to the nex release
            cat.info("--------Fixing to the nex release--------");
            cat.info("--------Fixing to the nex release--------");
            cat.info("--------Fixing to the nex release--------");
            cat.info("--------Fixing to the nex release--------");

            result = true;
        }
        return result;
    }

    /**
     * @param sessionData
     * @param reportName
     * @param ownerReport
     * @throws Exception
     */
    private boolean scheduleReport(SessionData sessionData, String reportName, String ownerReport) {
        boolean result = false;
        ReportSchedulerSoapBindingStub scheduler = null;

        try {
            String url = this.properties.getUrl();
            String repositoryScheduler = this.properties.getReportscheduler();
            String user = this.properties.getUser();
            String password = this.properties.getPassword();

            scheduler = new ReportSchedulerSoapBindingStub(new URL(url + repositoryScheduler), null);

            scheduler.setUsername(user);
            scheduler.setPassword(password);

            Job job = new Job();

            job.setReportUnitURI("/" + reportName + "/" + ownerReport + "/" + ownerReport + "_Unit");
            job.setLabel(ownerReport);
            job.setDescription("JOB");
            job.setBaseOutputFilename(reportName);

            String[] arrFormats = {"CSV"}; // PDF, DOCX, XLSX, RTF HTML
            job.setOutputFormats(arrFormats);

            String scheduleTypeId = getValueByParameterName(SCHEDULE_TYPE_ID_PARAMETER);
            Calendar calStartReport = new GregorianCalendar();
            calStartReport.setTimeInMillis(System.currentTimeMillis());
            String startParameterVal = getValueByParameterName(START_REPORT_PARAMETER);
            String startDate = getValueByParameterName(START_DATE_REPORT_PARAMETER);
            SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy HH:mm");

            if (startParameterVal.equals("a")) {
                if (!startDate.equals("")) {
                    Date date = formatDate.parse(startDate + ":00");
                    calStartReport.setTime(date);
                }
            } else {
                calStartReport.add(Calendar.MINUTE, 5);
            }

            createTriggerByType(job, scheduleTypeId, calStartReport, sessionData);

            JobRepositoryDestination destination = new JobRepositoryDestination();
            destination.setFolderURI("/" + reportName + "/" + ownerReport);
            destination.setSequentialFilenames(true);
            destination.setOverwriteFiles(true);
            job.setRepositoryDestination(destination);

            JobMailNotification jobMailNotification = new JobMailNotification();
            String emails = parameterValues.get(EMAILS_NOTIFICATIONS_PARAMETER);

            String[] arrToAddress = emails.split(",");
            jobMailNotification.setToAddresses(arrToAddress);
            jobMailNotification.setSubject(this.properties.getSubject() + reportName + " " + ownerReport);
            jobMailNotification.setMessageText(this.properties.getBody());
            jobMailNotification.setResultSendType(ResultSendType.SEND_ATTACHMENT);

            job.setMailNotification(jobMailNotification);

            Job jobCreated = scheduler.scheduleJob(job);
            long jobId = jobCreated.getId();
            jobCreated.getMailNotification().setSubject(this.properties.getSubject() + " (" + jobId + ") " + reportName + " " + ownerReport);
            scheduler.updateJob(jobCreated);
            result = true;

        } catch (Exception e) {
            cat.info(e);
        }
        return result;
    }

    /**
     * @param job
     * @param scheduleTypeId
     * @param calStartReport
     */
    private void createTriggerByType(Job job, String scheduleTypeId, Calendar calStartReport, SessionData sessionData) {
        Vector<String> vTimeZoneData = TimeZone.getTimeZoneByUserInSession(sessionData);
        String timeZoneUserLogged = (String) vTimeZoneData.get(3);

        if (scheduleTypeId.equals(SCHEDULE_TYPE_SIMPLE)) {
            //SINGLE OPTION
            JobSimpleTrigger simpleTrigger = new JobSimpleTrigger();

            simpleTrigger.setStartDate(calStartReport);
            String unitRepeatEvery = getValueByParameterName(SIMPLE_REPEAT_EVERY_PARAMETER_UNIT);
            if (unitRepeatEvery.equals("DAY")) {
                simpleTrigger.setRecurrenceIntervalUnit(IntervalUnit.DAY);
            } else if (unitRepeatEvery.equals("HOUR")) {
                simpleTrigger.setRecurrenceIntervalUnit(IntervalUnit.HOUR);
            } else if (unitRepeatEvery.equals("MINUTE")) {
                simpleTrigger.setRecurrenceIntervalUnit(IntervalUnit.MINUTE);
            } else if (unitRepeatEvery.equals("WEEK")) {
                simpleTrigger.setRecurrenceIntervalUnit(IntervalUnit.WEEK);
            }

            String repeatEveryValue = getValueByParameterName(SIMPLE_REPEAT_EVERY_VALUE_PARAMETER);
            simpleTrigger.setRecurrenceInterval(Integer.parseInt(repeatEveryValue));

            /*
             String radioControl = getValueByParameterName( SIMPLE_RADIO_CONTROL_PARAMETER );	   
             if ( radioControl.equals("t") )
             {
             String simpleTimes = getValueByParameterName( SIMPLE_TIMES_PARAMETER );
             simpleTrigger.setOccurrenceCount( Integer.parseInt(simpleTimes) );
             }	            
             else
             {
             simpleTrigger.setOccurrenceCount( -1 );
             }	
             */
            simpleTrigger.setOccurrenceCount(-1);
            String endDate = getValueByParameterName(END_DATE_REPORT_PARAMETER);
            SimpleDateFormat formatDate = new SimpleDateFormat(FORMAT_UI_DATE_CONTROL_UNTIL);

            Calendar calEndDateReport = new GregorianCalendar();
            calEndDateReport.setTimeInMillis(System.currentTimeMillis());
            Date date;
            try {
                date = formatDate.parse(endDate);
                calEndDateReport.setTime(date);
                simpleTrigger.setEndDate(calEndDateReport);
            } catch (Exception e) {
                cat.info("End date cannot setup " + e);
            }

            simpleTrigger.setTimezone(timeZoneUserLogged);
            job.setSimpleTrigger(simpleTrigger);

        } else if (scheduleTypeId.equals(SCHEDULE_TYPE_CALENDAR)) {
            //CALENDAR OPTION
            JobCalendarTrigger calendarTrigger = new JobCalendarTrigger();
            calendarTrigger.setStartDate(calStartReport);
            String calendarMonthEach = getValueByParameterName(CALENDAR_MONTH_EACH_PARAMETER);
            int[] months = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
            calendarTrigger.setMonths(months);

            if (calendarMonthEach.equals("s")) {
                String calendarMonthSelected = getValueByParameterName(CALENDAR_TRIGGER_MONTHS_PARAMETER);
                calendarMonthSelected = calendarMonthSelected.substring(0, calendarMonthSelected.length() - 1);

                String[] monthsS = calendarMonthSelected.split(",");
                int lenghtMonths = monthsS.length;
                int[] arrayMonths = new int[monthsS.length];
                for (int i = 0; i < lenghtMonths; i++) {
                    arrayMonths[i] = Integer.parseInt(monthsS[i]);
                }
                calendarTrigger.setMonths(arrayMonths);
            }

            String calendarDaysEach = getValueByParameterName(CALENDAR_DAYS_EACH_PARAMETER);
            if (calendarDaysEach.equals("e")) {
                calendarTrigger.setDaysType(CalendarDaysType.ALL);
            } else if (calendarDaysEach.equals("s")) {
                calendarTrigger.setDaysType(CalendarDaysType.WEEK);
                String calendarDaysSelected = getValueByParameterName(CALENDAR_TRIGGER_DAYS_PARAMETER);
                calendarDaysSelected = calendarDaysSelected.substring(0, calendarDaysSelected.length() - 1);
                String[] days = calendarDaysSelected.split(",");
                int lenghtDays = days.length;
                int[] arrayDays = new int[lenghtDays];
                for (int i = 0; i < lenghtDays; i++) {
                    arrayDays[i] = Integer.parseInt(days[i]);
                }
                calendarTrigger.setWeekDays(arrayDays);
            } else if (calendarDaysEach.equals("d")) {
                calendarTrigger.setDaysType(CalendarDaysType.MONTH);
                String calendarDaysNumber = getValueByParameterName(CALENDAR_DAYS_NUMBER_PARAMETER);
                calendarTrigger.setMonthDays(calendarDaysNumber);
            }

            String calendarTime = getValueByParameterName(CALENDAR_TIME_EXECUTIONR_PARAMETER);
            String[] calendarTimeArr = calendarTime.split(":");
            calendarTrigger.setHours(calendarTimeArr[0]); // execution hour
            calendarTrigger.setMinutes(calendarTimeArr[1]); //execution minutes

            calendarTrigger.setEndDate(new GregorianCalendar(2015, 1, 13));
            String endDate = getValueByParameterName(END_DATE_REPORT_PARAMETER);
            SimpleDateFormat formatDate = new SimpleDateFormat(FORMAT_UI_DATE_CONTROL_UNTIL);

            Calendar calEndDateReport = new GregorianCalendar();
            calEndDateReport.setTimeInMillis(System.currentTimeMillis());
            Date date;
            try {
                date = formatDate.parse(endDate);
                calEndDateReport.setTime(date);
                calendarTrigger.setEndDate(calEndDateReport);
            } catch (Exception e) {
                cat.info("End date cannot setup " + e);
            }
            job.setCalendarTrigger(calendarTrigger);
            calendarTrigger.setTimezone(timeZoneUserLogged);
        } else {
            //###################################################
            // NONE OPTION
            JobSimpleTrigger noneTrigger = new JobSimpleTrigger();
            noneTrigger.setStartDate(calStartReport);
            noneTrigger.setOccurrenceCount(1); // for this option is always 1
            //noneTrigger.setTimezone( timeZoneUserLogged );
            job.setSimpleTrigger(noneTrigger);
            //################################################### 
        }

    }

    /**
     * @param parameterName
     * @return
     */
    public String getValueByParameterName(String parameterName) {
        String valueParameter = "";
        if (parameterValues != null && parameterValues.size() > 0) {
            valueParameter = parameterValues.get(parameterName);
        }
        return valueParameter;
    }

    /**
     * @param seconds
     * @param dateFormat
     * @return
     */
    private String convertSecondsToDate(long seconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(seconds);
        return formatter.format(calendar.getTime());
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(SchedulerProperties properties) {
        this.properties = properties;
    }

    /**
     * @return the urlBack
     */
    public String getUrlBack() {
        return urlBack;
    }

}
