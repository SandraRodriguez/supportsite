package com.debisys.schedulereports;

import net.sf.jasperreports.engine.type.CalculationEnum;

public class JasperVariables {
	private String name;
    private Class clazz;
    private String expression;
    private String pattern;
    private boolean isCalculate;
    
    private CalculationEnum calculationType;
    
    /**
     * @param name
     * @param clazz
     * @param expression
     * @param pattern
     * @param isCalc
     * @param calculationType
     */
    public JasperVariables(String name, Class clazz, String expression, String pattern, boolean isCalc, CalculationEnum calculationType ) {
        this.name = name;
        this.clazz = clazz;
        this.expression = expression;
        this.setPattern(pattern);
        this.isCalculate = isCalc;
        this.calculationType = calculationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * @param isCalculate the isCalculate to set
	 */
	public void setIsCalculate(boolean isCalculate) {
		this.isCalculate = isCalculate;
	}

	/**
	 * @return the isCalculate
	 */
	public boolean getIsCalculate() {
		return isCalculate;
	}

	/**
	 * @param calculationType the calculationType to set
	 */
	public void setCalculationType(CalculationEnum calculationType) {
		this.calculationType = calculationType;
	}

	/**
	 * @return the calculationType
	 */
	public CalculationEnum getCalculationType() {
		return calculationType;
	}
    

}
