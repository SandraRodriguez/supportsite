package com.debisys.presentation;

import com.debisys.exceptions.ImageException;
import com.debisys.images.Image;
import com.debisys.presentation.html.HtmlComponent;
import com.debisys.presentation.html.HtmlField;
import com.debisys.presentation.html.HtmlFieldType;
import com.debisys.users.SessionData;
import net.emida.supportsite.dao.ImagesDao;
import net.emida.supportsite.dto.ImageType;

/**
 * @author Juan Carlos Cruz
 *
 */
public class ImageComponent extends HtmlComponent {
    
    private Image image;
    
    public ImageComponent(final Image vimage) {
        super("jsp.admin.image", vimage);
    }
    
    @Override
    public void loadDefaults() {
        //setValue("ImageId", "");
    }
    
    @Override
    public void loadMetadata(Object vimage) {
        image = (Image) vimage;
        addField(new HtmlField("imageId", HtmlFieldType.IMAGE_FILE));
    }
    
    public void loadFromDB() {
        clearValues();
        try {
            image.load();
            if (image.getImageId() > 0) {
                HtmlField p = getMetadata("imageId");
                if (p != null) {
                    String val = String.valueOf(image.getImageId());
                    if (val != null && !val.equals("")) {
                        load(p, this);
                        setValue("imageId_type", image.getType());
                    } else {
                        setValue(p.name, "");
                    }
                }
            }
        } catch (ImageException e1) {
            LOG.error("loadFromDB error", e1);
        }
    }
    
    public boolean save() {
        HtmlField p = getMetadata("imageId");
        if (p != null) {
            return (save(p, this) > 0);
        }
        return false;
    }
    
    @Override
    protected boolean onCheckImageField(HtmlField p) {
        return true;
    }
    
    @Override
    protected String onGetImageField(HtmlField p, String value, SessionData sessionData) {
        return getImageField(p, value, "/support/images/logo_sample.png", 0, 0, 0, true, sessionData);
    }
    
    public long save(HtmlField p, HtmlComponent comp) {
        try {
            String name = comp.getValue(p.name + "_file_name");
            name = name.substring(name.lastIndexOf("/") + 1);
            name = name.substring(name.lastIndexOf("\\") + 1);
            if (name != null && !name.equals("")) {
                String val = comp.getValue(p.name + "_id");
                long id = Long.parseLong((val != null && !val.equals("") ? val : "0"));
                boolean isnew = false;
                if (id == 0) {
                    isnew = true;
                }
                image.setImageId(id);
                image.setType(comp.getValue(p.name + "_type"));
                image.setAlternativeText(comp.getValue(p.name + "_alt"));
                image.setDescription(comp.getValue(p.name + "_desc"));
                image.setName(name);
                if (image.getType().isEmpty()) {
                    image.setType("BANNER");
                }
                ImageType imageType = ImagesDao.getImageTypeByCode(image.getType());
                image.setImageTypeId(imageType.getId());
                id = image.save();
                if (id > 0 && isnew) {
                    name = id + "_" + name;
                    image.setName(name);
                    image.save();
                    image.setData(comp.getValue(p.name + "_file"));
                    image.saveData();
                }
                return image.getImageId();
            }
        } catch (ImageException e) {
            //e.printStackTrace();
            this.errors.put(e.getErrorMessage(), e.getErrorMessage());
        }
        return 0;
    }
    
    public void load(HtmlField p, HtmlComponent comp) {
        try {
            if (image.load()) {
                comp.setValue(p.name + "_id", "" + image.getImageId());
                comp.setValue(p.name + "_type", "" + image.getType());
                comp.setValue(p.name + "_file_name", image.getName());
                String srcaux = image.getData();
                if (srcaux != null && srcaux.length() > 0 && !image.getName().equals("")) {
                    int dot = image.getName().lastIndexOf(".");
                    String ext = image.getName().substring(dot + 1);
                    srcaux = "data:image/" + ext + ";base64," + srcaux;
                    comp.setValue(p.name + "_data", srcaux);
                }
                comp.setValue(p.name + "_alt", image.getAlternativeText());
                comp.setValue(p.name + "_desc", image.getDescription());
            } else {
                comp.setValue(p.name, "");
            }
        } catch (ImageException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected boolean onCheckDocumentField(HtmlField p) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    protected String onGetDocumentField(HtmlField p, String value, SessionData sessionData) {
        // TODO Auto-generated method stub
        return null;
    }
}
