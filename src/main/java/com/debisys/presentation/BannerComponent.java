package com.debisys.presentation;

import com.debisys.exceptions.BannerException;
import com.debisys.images.Banner;
import com.debisys.images.Image;
import com.debisys.presentation.html.*;
import com.debisys.users.SessionData;

/**
 * @author Juan Carlos Cruz
 * 
 */

public class BannerComponent extends HtmlComponent {

	private Banner banner;

	private static final int MAX_BANNER_HEIGHT = 80;

	private static final int MAX_BANNER_WIDTH = 1024;

	public static final int MAX_MEMORY_SIZE = 3145728;

	private static final String BANNER_SAMPLE = "/support/images/banner_sample.png";

	public BannerComponent(final Banner vbanner){
		super("jsp.admin.banner", vbanner);
	}

	@Override
	public void loadDefaults() {
		// setValue("ImageId", "");
		setValue("ImageId_type", "BANNER");
		// setValue("repId", "");
		// setValue("DisplayTime", "");
		// setValue("EnterDate", "");
		// setValue("EndDate", "");
		// setValue("Description", "");
		// setValue("Link", "");
	}

	@Override
	public void loadMetadata(Object vbanner) {
		banner = (Banner)vbanner;
		addField(new HtmlField("bannerId", HtmlFieldType.HIDDEN));
		addField(new HtmlField("ImageId", HtmlFieldType.IMAGE_FILE));
		addField(new HtmlField("DisplayTime", HtmlFieldType.INTEGER));
		addField(new HtmlField("EnterDate", HtmlFieldType.DATE_TIME));
		addField(new HtmlField("EndDate", HtmlFieldType.DATE_TIME, 1));
		addField(new HtmlField("Description", HtmlFieldType.TEXT));
		addField(new HtmlField("Link", HtmlFieldType.TEXT, 500, 500));
		if (banner != null) {
			try {
				addField(new HtmlField("RepId", HtmlFieldType.CHECK_LIST, 0, banner.getAllISOs()));
			} catch (BannerException e) {
				e.printStackTrace();
			}
		}
	}

	public void loadFromDB() {
		clearValues();
		try {
			if(banner.load() && banner.getBannerId() > 0) {
				setValue("bannerId", String.valueOf(banner.getBannerId()));
				setValue("DisplayTime", ""+banner.getDisplayTime());
				setValue("EnterDate", banner.getEnterDate());
				setValue("EndDate", banner.getEndDate());
				setValue("Description", banner.getDescription());
				setValue("Link", banner.getLink());
				HtmlField p = getMetadata("ImageId");
				if (p != null){
					String val = String.valueOf(banner.getImageId());
					if (val != null && !val.equals("")) {
						long imgid = Long.parseLong(val);
						Image im = new Image();
						im.setImageId(imgid);
						ImageComponent ic = new ImageComponent(im);
						ic.load(p, this);
						setValue("ImageId_type", "BANNER");
					} else {
						setValue(p.name, "");
					}
				}
				setValue("RepId", banner.getRepIds());
			}
		} catch (BannerException e1) {
			e1.printStackTrace();
		}
	}

	public boolean save() {
		try {
			String val = getValue("DisplayTime");
			banner.setDisplayTime(val == null || val.equals("") ? 0 : Long.parseLong(val));
			banner.setDescription(getValue("Description"));
			banner.setLink(getValue("Link"));
			banner.setEnterDate(getValue("EnterDate"));
			banner.setEndDate(getValue("EndDate"));
			banner.setRepIds(getStringArray("RepId"));
			HtmlField p = getMetadata("ImageId");
			if(p != null){
				ImageComponent ic = new ImageComponent(new Image());
				setValue("ImageId_type", "BANNER");
				long id = ic.save(p, this);
				if (id > 0) {
					banner.setImageId(id);
					banner.save();
					return true;
				}
			}
		} catch (BannerException e) {
			e.printStackTrace();
		}
		return false;
	}

        public boolean delete() {            			
            return  banner.delete();                				            
        }        
        
	@Override
	protected String onGetImageField(HtmlField p, String value,SessionData sessionData) {
		return getImageField(p, value, BANNER_SAMPLE, MAX_BANNER_WIDTH, MAX_BANNER_HEIGHT, MAX_MEMORY_SIZE, false,sessionData);
	}

	@Override
	protected boolean onCheckImageField(HtmlField p) {
		return checkImageSize(p.name, MAX_BANNER_WIDTH, MAX_BANNER_HEIGHT, MAX_MEMORY_SIZE);
	}

	@Override
	protected boolean onCheckDocumentField(HtmlField p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String onGetDocumentField(HtmlField p, String value,SessionData sessionData) {
		// TODO Auto-generated method stub
		return null;
	}
}