package com.debisys.presentation;

import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;

import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.mvc.UrlPaths;

// TODO: create instead a treeview class
public class ToolGroup {

    String urlLink = null;
    String label = null;
    String blockName = null;
    private Boolean useModalForm = false;
    private String functionName;

    Vector<ToolGroup> subReports = null;

    public ToolGroup(String vlab, String vurl, String div, SessionData sessionData) {
        if (vlab != null) {
            label = Languages.getString(vlab, sessionData.getLanguage());
        }
        urlLink = vurl;
        blockName = div;
    }

    public void add(ToolGroup r) {
        if (r != null) {
            if (subReports == null) {
                subReports = new Vector<ToolGroup>();
            }
            subReports.add(r);
        }
    }

    /**
     * Indicates if the inner vector has items and therefore this item should be
     * rendered in the page
     *
     * @return True if the inner vector has items, otherwise is false
     */
    public boolean hasItems() {
        if (this.subReports != null) {
            return this.subReports.size() > 0;
        }
        return false;
    }

    public void showItem(JspWriter out) {
        try {
            // System.out.println("label:" + label);
            if (label != null) {
                out.print("<li class=\"" + ((urlLink == null) ? "group-closed" : "report") + "\">");
            }
            if (label == null) {
                out.print((urlLink == null) ? "<span class=main2>" : "");
            }
            if (urlLink != null) {
                if (!this.getUseModalForm()) {
                    if (label != null) {
                        out.print("<a href=\"" + urlLink + "\"> " + label + "</a>");
                    }
                } else {
                    out.print("<a href=\"#\" onclick='return " + this.getFunctionName() + "'> " + label + "</a>");
                }
            } else {
                if (label != null) {
                    out.print("<span class=\"group-label\">" + label + " (" + subReports.size() + ")</span>");
                }
                String name = (blockName != null) ? "name=\"" + blockName + "\" id=\"" + blockName + "\"" : "";
                out.print("<ul class=\"sublevel\" " + name + ">");
                if (subReports != null) {
                    for (ToolGroup r : subReports) {
                        r.showItem(out);
                    }
                }
                out.print("</ul>");
            }
            if (label == null) {
                out.print((urlLink == null) ? "</span2>" : "");
            }
            if (label != null) {
                out.print("</li>");
            }
        } catch (Exception ex) {
            System.out.println("Error in ToolGroup.showItem()");
        }
    }

    /**
     * @param sessionData
     * @param context
     * @param strAccessLevel
     * @param strDistChainType
     * @param deploymentType
     * @param customConfigType
     * @return
     * @throws UserException
     */
    public static ToolGroup createTransactionsGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType,
            String deploymentType, String customConfigType) throws UserException {
        ToolGroup groups = new ToolGroup(null, null, "main", sessionData);
        // Adding TransactionSummaries)
        boolean isIntranetUser = sessionData.getUser().isIntranetUser();
        boolean hasMultiCredentials = sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
                DebisysConstants.INTRANET_PERMISSION_MULTI_CREDENTIALS);
        {
            ToolGroup r = new ToolGroup("jsp.includes.menu.tools.h2hFeatures", null, "divH2HSetUp", sessionData);
            if (isIntranetUser && hasMultiCredentials) {
                String url = UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH_CREDENTIAL + UrlPaths.CREDENTIALS_PROVIDER;
                r.add(new ToolGroup("jsp.includes.menu.tools.h2hFeatures.credentials", url, null, sessionData));                
                
                //url = UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH_CREDENTIAL + UrlPaths.CREDENTIALS_PROVIDER;
                //r.add(new ToolGroup("jsp.includes.menu.tools.h2hFeatures.credentialsByRegion", url, null, sessionData));                
                
            }
            if (r.hasItems()) {
                groups.add(r);
            }
        }
        {
            boolean isDomestic = com.debisys.utils.DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
            //Activatoin request sumary
            if (isDomestic && sessionData.checkPermission(DebisysConstants.PERM_ALLOW_FREE_UP_ACTIVATIONS)) {
                ToolGroup activationRequestGroup = new ToolGroup("jsp.admin.tools.smsInventory.activationVerizonReport", null, "divActivationTransaction", sessionData);
                activationRequestGroup.add(new ToolGroup("jsp.admin.tools.smsInventory.activationVerizonReport", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.ACTIVATION_VERIZON_REPORT_FILTER, null, sessionData));
                groups.add(activationRequestGroup);
            }

            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SMS_INVENTORY_MANAGEMENT)) {
                ToolGroup r = new ToolGroup("jsp.includes.menu.tools.SmsInventory", null, "divSmsInventory", sessionData);
                r.add(new ToolGroup("jsp.admin.tools.smsInventory.simInventoryManagement", "admin/tools/smsInventory/simInventoryManagement.jsp##", null, sessionData));
                r.add(new ToolGroup("jsp.admin.tools.smsInventory.simInventoryEdit", "admin/tools/smsInventory/assignInventory.jsp?editOption=Y", null, sessionData));
                r.add(new ToolGroup("jsp.admin.tools.smsInventory.assingInventory", "admin/tools/smsInventory/assignInventory.jsp##", null, sessionData));
                r.add(new ToolGroup("jsp.admin.tools.smsInventory.inventoryReporting", "admin/tools/smsInventory/inventoryReporting.jsp##", null, sessionData));
                if ((isIntranetUser)) {
                    r.add(new ToolGroup("jsp.admin.tools.smsInventory.moveBatchIsoToIso", "admin/tools/smsInventory/simInventoryMoveBatch.jsp##", null, sessionData));
                    r.add(new ToolGroup("jsp.admin.tools.smsInventory.titleIsoDomain", "admin/tools/smsInventory/isoDomainList.jsp##", null, sessionData));
                    r.add(new ToolGroup("jsp.admin.tools.simInventory.productEdit", "admin/tools/smsInventory/simEditProduct.jsp##", null, sessionData));
                    r.add(new ToolGroup("jsp.admin.tools.simInventory.simLookup", "admin/tools/smsInventory/simLookup.jsp##", null, sessionData));
                }
                groups.add(r);
            }

            boolean permissionEnableEditMerchantAccountExecutives = sessionData.checkPermission(DebisysConstants.PERM_Enable_Edit_Merchant_Account_Executives);

            if (strAccessLevel.equals(DebisysConstants.ISO) && permissionEnableEditMerchantAccountExecutives) {
                groups.add(new ToolGroup("jsp.admin.reports.tools.accountexecutives.title", "admin/tools/accountexecutives.jsp##", null, sessionData));
            }

            {
                ToolGroup rx = new ToolGroup("jsp.admin.brandings.tool", null, "divBrandings", sessionData);
                if ((isIntranetUser)
                        && sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
                                DebisysConstants.INTRANET_PERMISSION_MANAGE_BRANDINGS)) {
                    rx.add(new ToolGroup("jsp.admin.brandings.manage", "admin/brandings/branding_list.jsp" + "##", null, sessionData));
                    rx.add(new ToolGroup("jsp.admin.banners.manage", "admin/banners/banners_list.jsp" + "##", null, sessionData));
                    rx.add(new ToolGroup("jsp.admin.news.manage", "admin/news/news_list.jsp" + "##", null, sessionData));
                    rx.add(new ToolGroup("jsp.admin.images.add", "admin/images/image_edit.jsp?action=add" + "##", null, sessionData));
                }
                if (rx.hasItems()) {
                    groups.add(rx);
                }

            }

            {
                if (sessionData.checkPermission(DebisysConstants.PERM_TO_DOCUMENT_MANAGEMENT)) {
                    ToolGroup r = new ToolGroup("jsp.includes.menu.tools.documentrepository", null, "divDocumentRepository", sessionData);
                    r.add(new ToolGroup("jsp.includes.menu.tools.documentManagement", "admin/documents/documentApproval.jsp##", null, sessionData));
                    groups.add(r);
                }
            }

            if (isIntranetUser) {
                groups.add(new ToolGroup("jsp.admin.customer.terminal.EditMBConfiguration", "admin/tools/mbconfigurations.jsp##", null, sessionData));
            }

            if ((isIntranetUser && sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1,
                    DebisysConstants.INTRANET_PERMISSION_MANAGE_PROMOTIONS))
                    || (strAccessLevel.equals(DebisysConstants.ISO) && (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PROMOS)))) {

                groups.add(new ToolGroup("jsp.includes.menu.manageproms_request", "admin/tools/managepromos.jsp?repId=" + sessionData.getProperty("ref_id")
                        + "##", null, sessionData));

            } else if (strAccessLevel.equals(DebisysConstants.CARRIER)/* &&(sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PROMOS)) */) {

                groups.add(new ToolGroup("jsp.includes.menu.manageproms_request", "admin/tools/managepromos.jsp?carrierId=" + sessionData.getProperty("ref_id")
                        + "##", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_CROSSBORDER_EXCHANGE_RATE_MANAGEMENT)) {
                groups.add(new ToolGroup("jsp.includes.menu.xBorderExchangeRate.title", "admin/tools/xBorderExchangeRate.jsp?repId="
                        + sessionData.getProperty("ref_id") + "##", null, sessionData));
            }

            if (isIntranetUser) {
                ToolGroup toolDepositTypes = new ToolGroup("jsp.admin.tools.depositTypes.title", null, "divDepositTypesAdmin", sessionData);

                toolDepositTypes.add(new ToolGroup("jsp.admin.tools.depositTypes.title", "admin/tools/depositTypes/depositTypesList.jsp##", null, sessionData));

                toolDepositTypes.add(new ToolGroup("jsp.admin.tools.banksDepositTypes.title", "admin/tools/depositTypes/banksDepositTypes.jsp##", null, sessionData));

                toolDepositTypes.add(new ToolGroup("jsp.admin.tools.paymentWizard.title", "admin/tools/depositTypes/paymentWizardList.jsp##", null, sessionData));

                groups.add(toolDepositTypes);
            }

            // Adding divMerchantAdmin
            {
                ToolGroup r = new ToolGroup("jsp.includes.menu.tools.merchantadmin", null, "divMerchantAdmin", sessionData);

                if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MERCHANT_BULK_IMPORT)) {

                    r.add(new ToolGroup("jsp.includes.menu.bulkmerchants", "admin/tools/bulkmerchant.jsp##", null, sessionData));
                }
                if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_ALLOW_STOCK_LEVELS_THRESHOLD_CONF)) {
                    r.add(new ToolGroup("jsp.includes.menu.stockLevelsConfig", "admin/tools/stockLevelsThreshold/stockLevelsThreshold.jsp##", null, sessionData));
                }
                if (com.debisys.utils.DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                    if (sessionData.checkPermission(DebisysConstants.PERM_MANAGEINVENTORY)) {
                        r.add(new ToolGroup("jsp.includes.menu.manageinventory", "admin/transactions/inventory_management.jsp##", null, sessionData));
                    }
                }

                if (sessionData.checkPermission(DebisysConstants.PERM_TRACKING_TERMINALS)) {

                    r.add(new ToolGroup("jsp.includes.menu.inventoriesAdministration.title", "admin/transactions/inventoriesAdministration_menu.jsp##", null,
                            sessionData));
                }

                if (r.hasItems()) {
                    groups.add(r);
                }
            }

            // Adding divNotifications
            {
                ToolGroup r = new ToolGroup("jsp.includes.menu.tools.notifications", null, "divNotifications", sessionData);
                if (!strAccessLevel.equals(DebisysConstants.MERCHANT) && !strAccessLevel.equals(DebisysConstants.CARRIER)) {
                    r.add(new ToolGroup("jsp.admin.reports.churn.toolstitle", "admin/customers/churn_reporting_options.jsp?tools=1&repId="
                            + sessionData.getProperty("ref_id") + "##", null, sessionData));
                }

                if ((isIntranetUser)
                        && sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_PUSH_MESSAGING)) {
                    r.add(new ToolGroup("jsp.admin.tools.pushMessaging.reportTitle", "reports/pushmessaging/historical", null, sessionData));
                }
                //DTU-4395
                r.add(new ToolGroup("jsp.tool.notification.pull.message", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.PULL_MESSAGE_NOTIFY, null, sessionData));

                // DTU-4841
                if (isIntranetUser) {
                    r.add(new ToolGroup("jsp.admin.tools.notification.messagesTitle", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.PULL_MESSAGE_NOTIFY + UrlPaths.PULL_QUEUE_MESSAGE + UrlPaths.PULL_QUEUE_VIEW, null, sessionData));
                }

                if (r.hasItems()) {
                    groups.add(r);
                }
            }

            {
                ToolGroup r = new ToolGroup("jsp.admin.reports.bankpayments.groupTitle", null, "divPayments", sessionData);
                if (sessionData.checkPermission(DebisysConstants.PERM_MANAGEPAYMENT)) {
                    r.add(new ToolGroup("jsp.includes.menu.paymentrequest_search", "admin/reports/transactions/paymentrequest_search.jsp##", null, sessionData));
                }
                if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_Bank_Import_Payments_Records_Status)) {
                    if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)) {
                        r.add(new ToolGroup("jsp.admin.reports.bankpayments.title", "admin/tools/banks/bankPayments.jsp", null, sessionData));
                    }
                }

                String strRefId = sessionData.getProperty("ref_id");
                String iso_default = context.getAttribute("iso_default").toString();
                if (strRefId.equals(iso_default)) {
                    r.add(new ToolGroup("jsp.admin.reports.spiffProcessHistoryReport.reportTitle",
                            UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.SPIFF_PROCESSING_HISTORY_REPORT_PATH, null, sessionData));
                }

                if (r.hasItems()) {
                    groups.add(r);
                }
            }

            {	//FEE DAEMON
                ToolGroup r = new ToolGroup("jsp.admin.reports.tool.fee.commissionsTitle", null, "divFee", sessionData);
                if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.FEE_APPROVAL_TOOL)) {
                    r.add(new ToolGroup("jsp.admin.reports.tool.fee.feeApprovalTool.title", "admin/tools/feeApprovalTool.jsp", null, sessionData));
                }
                if (r.hasItems()) {
                    groups.add(r);
                }
            }

            ToolGroup r = new ToolGroup("jsp.includes.menu.tools.pinmanagement", null, "divPinRequest", sessionData);
            if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
                    || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP)) {

                if (sessionData.checkPermission(DebisysConstants.PERM_MANAGEPIN)) {
                    r.add(new ToolGroup("jsp.includes.menu.pinreturn_search", "admin/reports/transactions/pinreturn_search.jsp##", null, sessionData));
                }

                if (sessionData.checkPermission(DebisysConstants.PERM_ADDPINREQUESTS)) {
                    r.add(new ToolGroup("jsp.includes.menu.pinreturn_request", "admin/transactions/pinreturn_request.jsp##", null, sessionData));
                }

                if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
                    r.add(new ToolGroup("jsp.admin.pincache.templates", "admin/pincache/template.jsp", null, sessionData));
                }

                if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE_ADV)) {
                    r.add(new ToolGroup("jsp.admin.pincache.goadmin", "admin/pincache/manage_pins.jsp", null, sessionData));
                }
                //DTU-3911
                if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_MANAGEMENT_PIN_STATUS)) {
                    r.add(new ToolGroup("jsp.includes.menu.tools.pinmanagement.status.title", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.PIN_MANAGEMENT_STATUS, null, sessionData));
                }
            }

            if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                if (sessionData.checkPermission(DebisysConstants.PERM_ADDVOIDTOPUP_REQUESTS)) {
                    r.add(new ToolGroup("jsp.includes.menu.voidtopup_request", "admin/transactions/voidtopup_request.jsp##", null, sessionData));
                }
            }

            if (isIntranetUser && sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_NO_ACK_PIN_MNG)) {
                r.add(new ToolGroup("mv.acktransactions.title", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.NO_ACK_PIN_MANAGEMENT, null, sessionData));
            }
            // r.add(new ToolGroup("jsp.admin.customer.terminal.EditMBConfiguration", "admin/tools/mbconfigurations.jsp##", null));
            if (r.hasItems()) {
                groups.add(r);
            }
        }

        {
            boolean isInternational = com.debisys.utils.DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                    && com.debisys.utils.DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
            boolean system2000 = sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS);
            if (system2000 && isInternational) {

                ToolGroup ra = new ToolGroup("jsp.admin.reports.transactions.sys2ktitle", null, "divSys2000", sessionData);

                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
                        || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP)) {

                    ra.add(new ToolGroup("jsp.admin.tools.s2k.tools.title3", "admin/tools/s2ktaskscheduler.jsp##", null, sessionData));
                    ra.add(new ToolGroup("jsp.admin.tools.s2k.tools.title2", "admin/tools/s2kstockcode.jsp##", null, sessionData));
                    ra.add(new ToolGroup("jsp.admin.tools.s2k.tools.title1", "admin/tools/s2ksalesmanid.jsp##", null, sessionData));
                }

                if (ra.hasItems()) {
                    groups.add(ra);
                }
            }
        }

        // Adding divVoidsWriteOff
        {
            ToolGroup r = new ToolGroup("jsp.includes.menu.tools.voidswriteoff", null, "divVoidsWriteOff", sessionData);

            if (sessionData.checkPermission(DebisysConstants.PERM_ADDVOIDTOPUP_REQUESTS)) {
                r.add(new ToolGroup("jsp.includes.menu.voidtopup_request", "admin/transactions/voidtopup_request.jsp##", null, sessionData));

            }
            if (sessionData.checkPermission(DebisysConstants.PERM_WRITE_OFF)
                    && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                r.add(new ToolGroup("jsp.includes.menu.write_off_request", "admin/transactions/write_off_request.jsp##", null, sessionData));
            }

            if (r.hasItems()) {
                groups.add(r);
            }
        }

        // DTU-246: Error Messaging to Device
        // Adding divErrorManagement
        {
            ToolGroup rem = new ToolGroup("jsp.includes.menu.tools.errormanagement", null, "divErrorManagement", sessionData);
            if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_ERROR_MANAGEMENT)) {
                rem.add(new ToolGroup("jsp.includes.menu.errormanagement", "admin/tools/errorManagement.jsp##", null, sessionData));
            }
            if (rem.hasItems()) {
                groups.add(rem);
            }
        }

        if (strAccessLevel.equals(DebisysConstants.CARRIER)) {
            ToolGroup r = new ToolGroup("jsp.carrier.tools.title", null, "divProductApproval", sessionData);
            r.add(new ToolGroup("jsp.productapproval.title", "admin/tools/productapproval_search.jsp", null, sessionData));

            if (r.hasItems()) {
                groups.add(r);
            }
        }

        {
            // Adding divMBTools all concern with MicroBrowser terminals.
            ToolGroup r = new ToolGroup("jsp.tools.dtu143.masterTitleTools", null, "divMBTools", sessionData);
            if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_MICROBROWSER_TOOL_BATCH_UPDATE_VERSION)) {
                r.add(new ToolGroup("jsp.tools.dtu143.masterTitle", "admin/mb/updatemb.jsp", null, sessionData));
            }
            if (r.hasItems()) {
                groups.add(r);
            }
        }

        {
            //DTU-2639 eternal login.
            ToolGroup r = new ToolGroup("jsp.tools.terminals.permissions.link", null, "divTerminalsBehaviour", sessionData);
            if (sessionData.checkPermission(DebisysConstants.PERM_ONE_LOGIN_FOR_MERCHANTS_TERMINALS)) {
                String resourceByPerm = "jsp.tools.dtu2639." + DebisysConstants.PERM_ONE_LOGIN_FOR_MERCHANTS_TERMINALS;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/oncelogin.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_NO_CLERK_DATA_TRANSACTIONS)) {
                String resourceByPerm = "jsp.tools.dtu2625." + DebisysConstants.PERM_NO_CLERK_DATA_TRANSACTIONS;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/noclerk.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_SEND_SMS_NOTIFICATION_TO_THE_PAYMENT_CUSTOMER)) {
                String resourceByPerm = "jsp.tools.dtu2640." + DebisysConstants.PERM_SEND_SMS_NOTIFICATION_TO_THE_PAYMENT_CUSTOMER;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/smsnotification.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_SUPPORT_SITE_ACCESS_LINK)) {
                String resourceByPerm = "jsp.tools.dtu2634." + DebisysConstants.PERM_SUPPORT_SITE_ACCESS_LINK;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/supportaccess.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_CARRIERS_LIST)) {
                String resourceByPerm = "jsp.tools.dtu2706." + DebisysConstants.PERM_CARRIERS_LIST;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/combocarriers.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_NO_BALANCE_MERCHANT)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_NO_BALANCE_MERCHANT;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/nobalancemerchant.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_VIEW_PINS_ON_REPORT)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_VIEW_PINS_ON_REPORT;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/viewpin.jsp", null, sessionData));
            }

            if (isIntranetUser) {
                r.add(new ToolGroup("jsp.admin.tools.isoLogin.title", "admin/tools/isoLogin/isoLoginList.jsp##", null, sessionData));
            }

            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_MERCHANTS_PREPAID_USING_CARD_PROCESSOR)) {
                String resourceByPerm = "jsp.tools.dtu3165." + DebisysConstants.PERM_ALLOW_MERCHANTS_PREPAID_USING_CARD_PROCESSOR;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/prepaidWithCardProcessor.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_TOKEN_SECURITY)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_TOKEN_SECURITY;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/tokensecurity.jsp", null, sessionData));
            }

            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_USE_CHAT)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_USE_CHAT;
                r.add(new ToolGroup(resourceByPerm, "admin/tools/terminals/chat.jsp", null, sessionData));
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_MARKET_PLACE_LINK)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_MARKET_PLACE_LINK;
                ToolGroup market = new ToolGroup(resourceByPerm, "admin/tools/terminals/marketPlace.jsp", null, sessionData);
                r.add(market);
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_USE_CUSTOMER_ACCOUNT_MANAGEMENT)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_USE_CUSTOMER_ACCOUNT_MANAGEMENT;
                ToolGroup market = new ToolGroup(resourceByPerm, "admin/tools/terminals/customerAccountManagement.jsp", null, sessionData);
                r.add(market);
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_USE_DOMESTIC_CUSTOMER_ACCOUNT_MANAGEMENT)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_USE_DOMESTIC_CUSTOMER_ACCOUNT_MANAGEMENT;
                ToolGroup market = new ToolGroup(resourceByPerm, "admin/tools/terminals/customerAccountManagement.jsp?domestic=true", null, sessionData);
                r.add(market);
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_VIEW_ECOMMERCE)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_VIEW_ECOMMERCE;
                ToolGroup market = new ToolGroup(resourceByPerm, "admin/tools/terminals/ecommerce.jsp", null, sessionData);
                r.add(market);
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_VIEW_COMMISSIONS_DETAIL)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_VIEW_COMMISSIONS_DETAIL;
                ToolGroup market = new ToolGroup(resourceByPerm, "admin/tools/terminals/viewcommissions.jsp", null, sessionData);
                r.add(market);
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_VIEW_ACCOUNTS_ON_TOP_UP)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_VIEW_ACCOUNTS_ON_TOP_UP;
                ToolGroup market = new ToolGroup(resourceByPerm, "admin/tools/terminals/viewaccountsOnTopUp.jsp", null, sessionData);
                r.add(market);
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_VIEW_DOCUMENT_MANAGEMENT)) {
                String resourceByPerm = "jsp.tools.title." + DebisysConstants.PERM_ALLOW_VIEW_DOCUMENT_MANAGEMENT;
                ToolGroup market = new ToolGroup(resourceByPerm, "admin/tools/terminals/viewdocumentManagement.jsp", null, sessionData);
                r.add(market);
            }
            if (r.hasItems()) {
                groups.add(r);
            }
        }

        {
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_VIEW_DOCUMENT_MANAGEMENT)) {
                ToolGroup ra = new ToolGroup("jsp.tools.adminManagementDocuments.title", null, "divAdminManagementDocuments", sessionData);
                ToolGroup documents = new ToolGroup("jsp.tools.adminManagementDocuments", "#", null, sessionData);
                documents.setUseModalForm(Boolean.TRUE);
                documents.setFunctionName("managementDocumentsFlowTool();");
                ra.add(documents);
                if (ra.hasItems()) {
                    groups.add(ra);
                }
            }
        }

        {
            if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_PAYMENT_PROCESSOR_MANAGEMENT) || isIntranetUser) {                                
                //DTU-dtu3165 MERCHANTS_PREPAID_USING_CARD_PROCESSOR.            
                ToolGroup r = new ToolGroup("jsp.tools.paymentoption.permissions.link", null, "divPaymentsBehaviour", sessionData);
                String prepaidUseCardProcessor = "jsp.tools.dtu3165.135.config";
                r.add(new ToolGroup(prepaidUseCardProcessor, UrlPaths.CONTEXT_PATH + UrlPaths.ADMIN_PATH + UrlPaths.TOOLS_PATH + UrlPaths.PAYMENTS_OPTIONS, null, sessionData));
                if (r.hasItems()) {
                    groups.add(r);
                }
            }
        }

        {
            if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_BULK_PAYMENT)) {
                //DTU-dtu3582 BULK_PAYMENT_PROCESS.            
                ToolGroup r = new ToolGroup("jsp.tools.bulkpayment.permissions.link", null, "divBulkPaymenttsBehaviour", sessionData);
                r.add(new ToolGroup("jsp.tools.bulkpayment.permissions.title", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + "/bulkPayment?message=", null, sessionData));
                if (r.hasItems()) {
                    groups.add(r);
                }
            }
        }
        {
            if (isIntranetUser) {
                //DTU-3905 - Contratos y Aviso de Privacidad                       
                ToolGroup r = new ToolGroup("jsp.tools.notice.contracts.link", null, "divNoticeBehaviour", sessionData);
                r.add(new ToolGroup("jsp.tools.notice.contracts.add", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.CONTRACTS_PRIVACY_NOTICER, null, sessionData));
                if (r.hasItems()) {
                    groups.add(r);
                }
            }

        }
        {
            if (isIntranetUser) {
                ToolGroup r = new ToolGroup("jsp.tools.bundlingActivation.title.link", null, "divNoticeBehaviour", sessionData);
                r.add(new ToolGroup("jsp.tools.bundlingActivation.title.sub1", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.BUNDLING_ACTIVATION_INIT, null, sessionData));
                if (r.hasItems()) {
                    groups.add(r);
                }
            }
        }

        {
            if (isIntranetUser) {
                ToolGroup r = new ToolGroup("jsp.tools.menu.fileExport.subMenuTitle", null, "divTerminalsBehaviour", sessionData);
                {

                    r.add(new ToolGroup("jsp.tools.conciliationExporter.toolTitle", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.CONCILIATION_EXPORT, null, sessionData));
                    if (r.hasItems()) {
                        groups.add(r);
                    }

                }
            }
        }

        {
            if (isIntranetUser) {
                groups.add(new ToolGroup("sort.products.title", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.SORTABLE_PRODUCTS, null, sessionData));
            }
        }
        
        {
            if (isIntranetUser) {
                groups.add(new ToolGroup("instant.spiff.main.title", UrlPaths.CONTEXT_PATH + UrlPaths.TOOLS_PATH + UrlPaths.INSTANT_SPIFF_SETTING, null, sessionData));
            }
        }

        return groups;
    }

    /**
     * @return the useModalForm
     */
    public Boolean getUseModalForm() {
        return useModalForm;
    }

    /**
     * @param useModalForm the useModalForm to set
     */
    public void setUseModalForm(Boolean useModalForm) {
        this.useModalForm = useModalForm;
    }

    /**
     * @return the functionName
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * @param functionName the functionName to set
     */
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
}
