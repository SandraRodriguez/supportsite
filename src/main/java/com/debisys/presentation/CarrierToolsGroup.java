/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.presentation;

import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;
import net.emida.supportsite.mvc.UrlPaths;

/**
 *
 * @author dgarzon
 */
public class CarrierToolsGroup {

    String urlLink = null;
    String label = null;
    String blockName = null;
    Vector<ToolGroup> subReports = null;

    public CarrierToolsGroup(String vlab, String vurl, String div, SessionData sessionData) {
        if (vlab != null) {
            label = Languages.getString(vlab, sessionData.getLanguage());
        }
        urlLink = vurl;
        blockName = div;
    }

    public void add(ToolGroup r) {
        if (r != null) {
            if (subReports == null) {
                subReports = new Vector<ToolGroup>();
            }
            subReports.add(r);
        }
    }

    /**
     * Indicates if the inner vector has items and therefore this item should be
     * rendered in the page
     *
     * @return True if the inner vector has items, otherwise is false
     */
    public boolean hasItems() {
        if (this.subReports != null) {
            return this.subReports.size() > 0;
        }
        return false;
    }

    public void showItem(JspWriter out) {
        try {
            // System.out.println("label:" + label);
            if (label != null) {
                out.print("<li class=\"" + ((urlLink == null) ? "group-closed" : "report") + "\">");
            }
            if (label == null) {
                out.print((urlLink == null) ? "<span class=main2>" : "");
            }
            if (urlLink != null) {
                if (label != null) {
                    out.print("<a href=\"" + urlLink + "\"> " + label + "</a>");
                }
            } else {
                if (label != null) {
                    out.print("<span class=\"group-label\">" + label + " (" + subReports.size() + ")</span>");
                }
                String name = (blockName != null) ? "name=\"" + blockName + "\"" : "";
                out.print("<ul class=\"sublevel\" " + name + ">");
                if (subReports != null) {
                    for (ToolGroup r : subReports) {
                        r.showItem(out);
                    }
                }
                out.print("</ul>");
            }
            if (label == null) {
                out.print((urlLink == null) ? "</span2>" : "");
            }
            if (label != null) {
                out.print("</li>");
            }
        } catch (Exception ex) {
            System.out.println("Error in ToolGroup.showItem()");
        }
    }

    /**
     * @param sessionData
     * @param context
     * @param strAccessLevel
     * @param strDistChainType
     * @param deploymentType
     * @param customConfigType
     * @return
     * @throws UserException
     */
    public static CarrierToolsGroup createCarrierToolGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType,
            String deploymentType, String customConfigType) throws UserException {
        CarrierToolsGroup groups = new CarrierToolsGroup(null, null, "main", sessionData);
        
        ToolGroup toolLoyaltyProgramConf2 = new ToolGroup("jsp.includes.menu.tools.loyaltyProgram.title", "loyalty/loyaltyProgram/historical", null, sessionData);
        groups.add(toolLoyaltyProgramConf2);

        return groups;
    }
}
