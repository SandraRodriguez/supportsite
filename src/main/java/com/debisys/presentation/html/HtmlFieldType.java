package com.debisys.presentation.html;

public enum HtmlFieldType {
	TEXT,
	CHECKBOX,
	BOOL_CHECKBOX,
	COLOR,
	IMAGE_FILE,
	SINGLE_SELECT,
	PHONE,
	INTEGER,
	DATE_TIME,
	DATE,
	TIME, 
	TEXTAREA, 
	TEXT_STATIC, 
	HIDDEN, 
	CHECK_LIST,
	FILE,
	LABEL
}
