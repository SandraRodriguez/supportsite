package com.debisys.presentation.html;

/**
 * @author Juan Carlos Cruz
 *
 */
public class HtmlAttrib {
	public static final String ATTRIB_TYPE = "type";
	
	public static final String ATTRIB_VALUE = "value";
	
	public static final String ATTRIB_NAME = "name";
	
	public static final String ATTRIB_ID = "id";
	
	public static final String ATTRIB_CLASS = "class";
	
	public static final String ATTRIB_SRC = "src";
	
	public static final String ATTRIB_ALT = "alt";
	
	public static final String ATTRIB_STYLE = "style";
	
	public static final String ATTRIB_COLSPAN = "colspan";
	
	public static final String ATTRIB_MAXLENGTH = "maxlength";

	public static final String ATTRIB_SIZE = "size";

	public static final String ATTRIB_READONLY = "readonly";

	public static final String ATTRIB_ROWS = "rows";
	
	public static final String ATTRIB_COLS = "cols";

	public static final String ATTRIB_ACCEPT = "accept";

	public static final String ATTRIB_CHECKED = "checked";
	
	public static final String ATTRIB_ONCHANGE = "onchange";

	protected static final String ATTRIB_SELECTED = "selected";
	
	public String at_name;
	
	public String at_value;
	
	/**
	 * @param name Name of the attribute
	 * @param value Value of the attribute
	 */
	public HtmlAttrib(String vname, String vvalue){
		at_name = vname;
		at_value = vvalue;
	}

	public String toString() {
		if(at_name != null){
			return " " + at_name + "=\"" + (at_value==null?"":at_value) + "\"";
		}else{
			return "";
		}
	}
}