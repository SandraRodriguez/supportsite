package com.debisys.presentation.html;

public class HtmlField {
	public boolean enabled = true;
	public String name;
	public HtmlFieldType type;
	public int maxLegth = 0;
	public long rules;
	public Object data = null;

	/**
	 * @param name name of the field
	 * @param type type of the field
	 * @param maxLength maximum length for the value
	 */
	public HtmlField(String vname, HtmlFieldType vtype, long vrules, int maxLen) {
		name = vname;
		type = vtype;
		rules = vrules;
		maxLegth = maxLen;
	};

	/**
	 * @param name name of the field
	 * @param type type of the field
	 * @param rules validation rules
	 * @param data
	 */
	public HtmlField(String vname, HtmlFieldType vtype, long vrules, Object vdata) {
		name = vname;
		type = vtype;
		rules = vrules;
		data = vdata;
	};

	/**
	 * @param name name of the field
	 * @param type type of the field
	 * @param rules validation rules
	 */
	public HtmlField(String vname, HtmlFieldType vtype, long vrules) {
		name = vname;
		type = vtype;
		rules = vrules;
	};

	/**
	 * @param name name of the field
	 * @param type type of the field
	 */
	public HtmlField(String vname, HtmlFieldType vtype) {
		name = vname;
		type = vtype;
		rules = 0;
	};
}
