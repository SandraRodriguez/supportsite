package com.debisys.presentation.html;

public enum HtmlFieldRule {
	NO_RULE,
	NOT_NULL,
	NOT_EMPTY,
	INTEGER,
	CURRENCY,
	NAME,
	EMAIL,
	PHONE,
	DATE,
	DATE_TIME,
	TIME
}
