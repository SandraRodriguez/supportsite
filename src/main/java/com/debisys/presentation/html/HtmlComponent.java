package com.debisys.presentation.html;

import com.debisys.images.Banner;
import com.debisys.images.Image;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;
import net.emida.supportsite.dao.ImagesDao;
import net.emida.supportsite.dto.ImageType;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Category;
import org.apache.log4j.Logger;

/**
 * @author Juan Carlos Cruz
 *
 */
@SuppressWarnings({"serial", "deprecation"})
public abstract class HtmlComponent {
    
    protected static final Logger LOG = Logger.getLogger(HtmlComponent.class);

    protected String brandingGroup;

    private static final int MAX_TEXT_SIZE = 80;

    static Category cat = Category.getInstance(Banner.class);

    protected List<HtmlField> metadata = new ArrayList<>();

    protected HashMap<String, Object> values = new HashMap<>();

    protected HashMap<String, String> errors = new HashMap<>();

    protected String langPrefix = null;

    protected static final String TAG_OPTION = "option";

    protected static final String TAG_IMG = "img";

    protected static final String TAG_INPUT = "input";

    protected static final String TAG_SELECT = "select";

    protected static final String TAG_SPAN = "span";

    protected static final String TAG_DIV = "div";

    protected static final String TAG_TABLE = "table";

    protected static final String TAG_TH = "th";

    protected static final String TAG_TR = "tr";

    protected static final String TAG_TD = "td";

    protected static final String TAG_THEAD = "thead";

    protected static final String TAG_TBODY = "tbody";

    protected static final String TAG_CAPTION = "caption";

    protected static final String TAG_STRONG = "strong";

    protected static final String TAG_EM = "em";

    protected static final String TAG_FIELDSET = "fieldset";

    protected static final String TAG_LEGEND = "legend";

    private static final String TAG_SCRIPT = "script";

    private static final String TAG_TEXTAREA = "textarea";

    private static final String TAG_P = "p";

    private static final String TAG_LI = "li";

    private static final String TAG_UL = "ul";

    protected abstract boolean onCheckImageField(HtmlField p);

    protected abstract boolean onCheckDocumentField(HtmlField p);

    abstract protected String onGetImageField(HtmlField p, String value, SessionData sessionData);

    abstract protected String onGetDocumentField(HtmlField p, String value, SessionData sessionData);

    public HtmlComponent(String languagePrefix, Object obj) {
        this.langPrefix = languagePrefix;
        this.loadMetadata(obj);
        this.clearValues();
        this.loadDefaults();
    }

    public abstract void loadMetadata(Object obj);

    public abstract void loadDefaults();

    public void addField(HtmlField field) {
        if (field != null) {
            this.metadata.add(field);
        }
    }

    private byte[] b;
    //private static final String workingDir  = DebisysConfigListener.getWorkingDir(application);

    /**
     * @param name name of the element
     * @param attribs array of attribs of the element
     * @param content sets the content of the element
     * @param single determines if the tag is a single tag or not
     * @return the generated element as string
     */
    public String getTag(String name, List<HtmlAttrib> attribs, String content, boolean single) {
        String tag = "<" + name;
        if (attribs != null) {
            for (final HtmlAttrib at : attribs) {
                tag += at.toString();
            }
        }
        tag += single ? " />" : ">" + (content == null ? "" : content) + "</" + name + ">";
        return tag;
    }

    public HtmlField getMetadata(String string) {
        for (HtmlField p : this.metadata) {
            if (p.name.equals(string)) {
                return p;
            }
        }
        return null;
    }

    public void enableField(String fieldName) {
        for (HtmlField p : this.metadata) {
            if (p.name.equals(fieldName)) {
                p.enabled = true;
                return;
            }
        }
    }

    public boolean isEnabled(String fieldName) {
        for (HtmlField p : this.metadata) {
            if (p.name.equals(fieldName)) {
                return p.enabled;
            }
        }
        return false;
    }

    public void disableField(String fieldName) {
        for (HtmlField p : this.metadata) {
            if (p.name.equals(fieldName)) {
                p.enabled = false;
                return;
            }
        }
    }

    /**
     * @param type
     * @param name
     * @param value
     * @return the generated element as string
     */
    public String getInput(final String type, final String name, final String value) {
        return this.getTag(HtmlComponent.TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, type));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, value));
            }
        }, null, true);
    }

    /**
     * @param name
     * @param src
     * @param alt
     * @return the generated element as string
     */
    public String getImage(final String name, final String src, final String alt) {
        return this.getTag(HtmlComponent.TAG_IMG, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SRC, src));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ALT, alt));
            }
        }, null, true);
    }

    public String getSelect(final String name, List<List<String>> options, final List<String> selected, boolean multiple) {
        return getSelect(name, options, selected, multiple, null);
    }

    public String getSelect(final String name, List<List<String>> options, final List<String> selected, boolean multiple, final String onchange) {
        String content = "";
        if (options != null) {
            for (final List<String> op : options) {
                content += this.getTag(HtmlComponent.TAG_OPTION, new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, op.get(0)));
                        if (selected != null && selected.contains(op.get(0))) {
                            this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SELECTED, HtmlAttrib.ATTRIB_SELECTED));
                        }
                    }
                }, op.get(1), false);
            }
        }
        String select = this.getTag(HtmlComponent.TAG_SELECT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, name));
                if (onchange != null) {
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ONCHANGE, onchange));
                }
            }
        }, content, false);
        return select;
    }

    /**
     * @param name name of the element
     * @param value value of the element
     * @return the generated element as string
     */
    public String getTextField(String name, String value) {
        return this.getInput("text", name, value);
    }

    /**
     * @param name name of the element
     * @param value color value for the element
     * @return the generated element as string
     */
    public String getColorField(final String name, final String value) {
        return this.getTag(HtmlComponent.TAG_DIV, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "ColorField"));
            }
        }, this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, value));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_MAXLENGTH, "7"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SIZE, "7"));
                //add(new HtmlAttrib(HtmlAttrib.ATTRIB_READONLY, HtmlAttrib.ATTRIB_READONLY));
            }
        }, null, false), false);
    }

    /**
     * @param name name of the element
     * @param imagePath path of the image to display
     * @param helpContent tooltip content to display when mouse rolls over the
     * image
     * @return the generated element as string
     */
    public String getHelpToolTip(final String name, String imagePath, String helpContent) {
        if (helpContent != null && helpContent.length() > 0) {
            String img = this.getImage(name + "_helpimage", imagePath, "HelpToolTip");
            String tagP = this.getTag(HtmlComponent.TAG_P, null, helpContent, false);
            return this.getTag(HtmlComponent.TAG_SPAN,
                    new ArrayList<HtmlAttrib>() {
                {
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "helpToolTip"));
                }
            },
                    img
                    + this.getTag(HtmlComponent.TAG_DIV, new ArrayList<HtmlAttrib>() {
                        {
                            this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, name + "_helpToolTipBox"));
                            this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "helpToolTipBox"));
                        }
                    }, tagP, false),
                    false
            );
        }
        return "";
    }

    @SuppressWarnings({"unchecked"})
    public String getField(final HtmlField p, String value, SessionData sessionData) {
        final String s = (value != null ? value : "");
        switch (p.type) {
            case IMAGE_FILE:
                return this.getImageField(p, s, sessionData);
            case FILE:
                return this.getDocumentField(p, s, sessionData);
            case INTEGER:
                return this.getIntegerField(p.name, s);
            case COLOR:
                return this.getColorField(p.name, s);
            case TEXT:
                return this.getTextField(p, s);
            case TEXT_STATIC:
                return this.getStaticText(p, s);
            case TEXTAREA:
                return this.getTextArea(p, s);
            case BOOL_CHECKBOX:
                return this.getBoolCheckBox(p, s);
            case DATE:
                return this.getDateField(p, s);
            case DATE_TIME:
                return this.getDateTimeField(p, s);
            case TIME:
                return this.getTimeField(p, s);
            case PHONE:
                return this.getPhoneField(p.name, s);
            case SINGLE_SELECT:
                return this.getSelect(p.name, (List<List<String>>) p.data, new ArrayList<String>() {
                    {
                        this.add(s);
                    }
                }, false);
            case HIDDEN:
                return this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, p.name));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "hidden"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, HtmlComponent.this.getValue(p.name)));
                    }
                }, null, true);
            default:
                return "";
        }
    }

    /**
     * @param name
     * @param options
     * @param selected
     * @param multiple
     * @return the generated element as string
     */
    public String getCheckList(final String name, List<List<String>> options, final List<String> selected, boolean multiple) {
        String content = "";
        if (options != null) {
            for (final List<String> op : options) {
                content += this.getTag(HtmlComponent.TAG_LI,
                        null,
                        this.getTag(HtmlComponent.TAG_INPUT, new ArrayList<HtmlAttrib>() {
                            {
                                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "checkbox"));
                                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, name));
                                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, op.get(0)));
                                if (selected != null && selected.contains(op.get(0))) {
                                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CHECKED, HtmlAttrib.ATTRIB_CHECKED));
                                }
                            }
                        }, null, true)
                        + op.get(1),
                        false);
            }
        }
        String select = this.getTag(HtmlComponent.TAG_UL, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "checkList"));
            }
        }, content, false);
        return select;
    }

    /**
     * @param s
     */
    private String getIntegerField(final String name, String s) {
        return this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "integer"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, HtmlComponent.this.getValue(name)));
            }
        }, null, true);
    }

    /**
     * @param s
     */
    private String getPhoneField(final String name, String s) {
        return this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SIZE, "10"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_MAXLENGTH, "10"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "phone"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, HtmlComponent.this.getValue(name)));
            }
        }, null, true);
    }

    /**
     * @param p
     * @param value
     * @return
     */
    private String getBoolCheckBox(final HtmlField p, final String value) {
        return this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, "1"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "checkbox"));
                if (value != null && (value.equals("1") || value.toLowerCase().equals("true") || value.toLowerCase().equals("yes"))) {
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CHECKED, HtmlAttrib.ATTRIB_CHECKED));
                }
            }
        }, null, true);
    }

    private String getTextArea(final HtmlField p, final String value) {
        return this.getTag(TAG_TEXTAREA, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                if (p.maxLegth > 0) {
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_MAXLENGTH, String.valueOf(p.maxLegth)));
                }
            }
        }, value, false);
    }

    private String getStaticText(final HtmlField p, final String value) {
        return this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, value));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_READONLY, HtmlAttrib.ATTRIB_READONLY));
                if (p.maxLegth > 0) {
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_MAXLENGTH, String.valueOf(p.maxLegth)));
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SIZE, String.valueOf((p.maxLegth > MAX_TEXT_SIZE ? MAX_TEXT_SIZE : p.maxLegth))));
                }
            }
        }, null, true);
    }

    private String getTextField(final HtmlField p, final String value) {
        return this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, value));
                if (p.maxLegth > 0) {
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_MAXLENGTH, String.valueOf(p.maxLegth)));
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SIZE, String.valueOf((p.maxLegth > MAX_TEXT_SIZE ? MAX_TEXT_SIZE : p.maxLegth))));
                }
            }
        }, null, true);
    }

    protected String getImageField(final HtmlField p, final String value, SessionData sessionData) {
        return this.onGetImageField(p, value, sessionData);
    }

    protected String getDocumentField(final HtmlField p, final String value, SessionData sessionData) {
        return this.onGetDocumentField(p, value, sessionData);
    }

    protected String getDocumentField(final HtmlField p, final String value, final String defaultImage, final int maxWidth,
            final int maxHeight, int maxSize, final boolean showType, SessionData sessionData) {
        return this.getTag(TAG_FIELDSET, null, this.getTag(TAG_LEGEND, null, Languages.getString(this.langPrefix + ".label." + p.name, sessionData.getLanguage()), false)
                + Languages.getString(this.langPrefix + ".desc." + p.name, sessionData.getLanguage())
                + "<br><br>File: " + this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
                    {
                        //add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name + "_file"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name + ""));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ACCEPT, "application/pdf"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ACCEPT, "application/doc"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ACCEPT, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ACCEPT, "application/xls"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ACCEPT, "application/docx"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "file"));
                        //add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "image_file"));
                    }
                }, null, true) + this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                //add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name + "_file_name"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "hidden"));
                //add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, getValue(p.name+ "_file_name")));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, HtmlComponent.this.getValue(p.name)));
            }
        }, null, true), false);
    }

    protected String getImageField(final HtmlField p, final String value, final String defaultImage, final int maxWidth, final int maxHeight, int maxSize, final boolean showType, SessionData sessionData) {
        String typeField;
        if (showType) {
            
            Map<String, ImageType> imagesTypesMap = ImagesDao.getAllImageTypes();
            
            List<List<String>> selectOptions = new ArrayList<>();
            if(!imagesTypesMap.isEmpty()){
                for(ImageType currentImgType:imagesTypesMap.values()){
                    List<String> newOption = new ArrayList<>();
                    newOption.add(currentImgType.getCode());
                    newOption.add(currentImgType.getDescription());
                    selectOptions.add(newOption);
                }
            }
            
            typeField = "<br><br>" + this.getSelect(p.name + "_type", selectOptions, new ArrayList<String>() {
                {
                    this.add(HtmlComponent.this.getValue(p.name + "_type"));
                }
            }, false, "onchangeDivInfo(this)");
        } else {
            typeField = this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
                {
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name + "_type"));
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "hidden"));
                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, HtmlComponent.this.getValue(p.name + "_type")));
                }
            }, null, true);
        }

        List<HtmlAttrib> attributesFieldSet = new ArrayList<>();
        attributesFieldSet.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "brandingFieldSet"));
        List<HtmlAttrib> attributesLegend = new ArrayList<>();
        attributesLegend.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "brandingLegend"));

        return this.getTag(TAG_FIELDSET, attributesFieldSet, this.getTag(TAG_LEGEND, attributesLegend, Languages.getString(this.langPrefix + ".label." + p.name, sessionData.getLanguage()), false)
                + Languages.getString(this.langPrefix + ".desc." + p.name, sessionData.getLanguage())
                + (maxWidth > 0 && maxHeight > 0 ? "<br><br>Recommended size: " + maxWidth + "x" + maxHeight + " pixels, " : "")
                + "<br><br>Allowed file formats: GIF, PNG, JPG"
                + "<br><br>Description: " + this.getTextField(p.name + "_desc", this.getValue(p.name + "_desc"))
                + "<br><br>Alternate text: " + this.getTextField(p.name + "_alt", this.getValue(p.name + "_alt"))
                + "<br><br>File: " + this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name + "_file"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ACCEPT, "image/*"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "file"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "image_file"));
                    }
                }, null, true)
                + this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name + "_file_name"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "hidden"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, HtmlComponent.this.getValue(p.name + "_file_name")));
                    }
                }, null, true) + typeField + this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name + "_id"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, p.name + "_id"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_TYPE, "hidden"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, HtmlComponent.this.getValue(p.name + "_id")));
            }
        }, null, true)
                + "<br><br>"
                + this.getTag(TAG_DIV, new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, "div_info"));
                    }
                }, "<b id=\"b_tag_info\">  </b>", false),
                 false);

        //Recommended size 531x72 px
    }

    /**
     * @param p
     * @param value
     * @return
     */
    private String getDateField(final HtmlField p, final String value) {
        return this.getTag(TAG_SCRIPT, null, "", false)
                + this.getTag(TAG_INPUT,
                        new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, p.name));
                        this.add(new HtmlAttrib("chidden", "0"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, value));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "dateField"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_READONLY, HtmlAttrib.ATTRIB_READONLY));
                    }
                },
                         null, true)
                + this.getTag(TAG_IMG,
                        new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib("idc", p.name));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "dateButton"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SRC, "/support/images/calendar.png"));
                    }
                },
                         null, true);
    }

    /**
     * @param p
     * @param value
     * @return
     */
    private String getDateTimeField(final HtmlField p, final String value) {
        return this.getTag(TAG_INPUT,
                new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, p.name));
                this.add(new HtmlAttrib("chidden", "0"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, value));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "datetimeField"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_READONLY, HtmlAttrib.ATTRIB_READONLY));
            }
        },
                 null, true)
                + this.getTag(TAG_IMG,
                        new ArrayList<HtmlAttrib>() {
                    {
                        this.add(new HtmlAttrib("idc", p.name));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "datetimeButton"));
                        this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SRC, "/support/images/calendar.png"));
                    }
                },
                         null, true);
    }

    /**
     * @param p
     * @param value
     * @return
     */
    private String getTimeField(final HtmlField p, final String value) {
        return this.getTag(TAG_INPUT, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_NAME, p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_ID, p.name));
                this.add(new HtmlAttrib("chidden", "0"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_VALUE, value));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "timeField"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_READONLY, HtmlAttrib.ATTRIB_READONLY));
            }
        }, null, true) + this.getTag(TAG_IMG, new ArrayList<HtmlAttrib>() {
            {
                this.add(new HtmlAttrib("idc", p.name));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "timeButton"));
                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_SRC, "/support/images/time.png"));
            }
        }, null, true);
    }

    public String getValue(String key) {
        Object o = this.values.get(key);
        if (o != null) {
            return o.toString();
        }
        return null;
    }

    public String getValues(String key) {
        Object o = this.values.get(key);

        if (o != null) {
            return o.toString();
        }
        return null;
    }

    public byte[] getBytes() {

        return this.b;
    }

    public void setBytes(byte[] multipart) {
        this.b = multipart;
    }

    public Object getObject(String key) {
        return this.values.get(key);
    }

    public String[] getStringArray(String string) {
        Object o = this.getObject(string);
        if (o != null && o.getClass().isArray()) {
            return (String[]) o;
        }
        return new String[]{(String) o};
    }

    public void setValue(String key, Object val) {
        HtmlField q = this.getMetadata(key);
        if (q != null) {
            if (q.type == HtmlFieldType.BOOL_CHECKBOX) {
                if (val != null && (val.equals("1") || val.toString().toLowerCase().equals("true") || val.toString().toLowerCase().equals("yes"))) {
                    val = "true";
                } else {
                    val = "false";
                }
            }
        }
        this.values.put(key, val);
    }

    public void clearValues() {
        Object brandingGroupTmp = this.values.get("brandingGroup");
        this.values.clear();
        if (brandingGroupTmp != null) {
            this.values.put("brandingGroup", brandingGroupTmp.toString());
        }
        this.errors.clear();
        for (HtmlField p : this.metadata) {
            if (p.type == HtmlFieldType.BOOL_CHECKBOX) {
                this.setValue(p.name, "false");
            }
        }
    }

    /**
     * @param request
     * @param workingDir
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
    public String uploadFile(javax.servlet.http.HttpServletRequest request, String workingDir, String key) {
        String saveFileName = "";
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                Iterator itr = fileItemsList.iterator();
                while (itr.hasNext()) {
                    FileItem item = (FileItem) itr.next();
                    if (!item.isFormField()) {
                        File fullFile = new File(item.getName());
                        saveFileName = fullFile.getName();
                        File fNew = new File(workingDir, saveFileName);
                        item.write(fNew);
                        cat.debug("HtmlComponent.uploadFile: " + workingDir + saveFileName);
                    }
                }
            } catch (Exception e) {
                cat.error("Error uploading file in  HtmlComponent.uploadFile: " + e.getMessage());
            }
        } else {
            cat.error("HtmlComponent.uploadFile error: No MultipartContent");
        }
        return saveFileName;
    }

    @SuppressWarnings("unchecked")
    public void showFields(JspWriter out, SessionData sessionData) {
        try {
            String t = "";
            for (HtmlField p : this.metadata) {
                if (p.enabled) {
                    String errorMsg = this.errors.get(p.name);
                    if (errorMsg != null && errorMsg.length() > 0) {
                        t += this.getTag(TAG_TR, null, this.getTag(TAG_TD, new ArrayList<HtmlAttrib>() {
                            {
                                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_COLSPAN, "2"));
                                this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_CLASS, "errorMsg"));
                            }
                        }, errorMsg, false), false);
                    }
                    if (null == p.type) {
                        String tagTd = this.getTag(TAG_TD, null, this.getField(p, this.getValue(p.name), sessionData), false);
                        String languagesDesc = Languages.getString(this.langPrefix + ".desc." + p.name, sessionData.getLanguage());
                        String helpToolTip = this.getHelpToolTip(p.name, "/support/images/questionmark.png", languagesDesc);
                        String languageLabel = Languages.getString(this.langPrefix + ".label." + p.name, sessionData.getLanguage());
                        final String attr = "width: 30%;";
                        String tagTh = this.getTag(TAG_TH, new ArrayList<HtmlAttrib>() {
                            {
                                add(new HtmlAttrib(HtmlAttrib.ATTRIB_STYLE, attr));

                            }
                        }, languageLabel + ":" + helpToolTip, false);
                        t += this.getTag(TAG_TR, null, tagTh + tagTd, false);
                    } else switch (p.type) {
                        case IMAGE_FILE:
                            t += this.getTag(TAG_TR, null, this.getTag(TAG_TD, new ArrayList<HtmlAttrib>() {
                                {
                                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_COLSPAN, "2"));
                                }
                            }, this.getField(p, this.getValue(p.name), sessionData), false), false);
                            break;
                        case FILE:
                            t += this.getTag(TAG_TR, null, this.getTag(TAG_TD, new ArrayList<HtmlAttrib>() {
                                {
                                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_COLSPAN, "2"));
                                }
                            }, this.getField(p, this.getValue(p.name), sessionData), false), false);
                            break;
                        case HIDDEN:
                            t += this.getField(p, this.getValue(p.name), sessionData);
                            break;
                        case CHECK_LIST:
                            List<String> l = null;
                            String[] o = this.getStringArray(p.name);
                            if (o != null) {
                                l = Arrays.asList(o);
                            }   t += this.getTag(TAG_TR, null, this.getTag(TAG_TD, new ArrayList<HtmlAttrib>() {
                                {
                                    this.add(new HtmlAttrib(HtmlAttrib.ATTRIB_COLSPAN, "2"));
                                }
                            },
                                    this.getTag(TAG_STRONG, null, Languages.getString(this.langPrefix + ".label." + p.name, sessionData.getLanguage()) + ":" + this.getHelpToolTip(p.name, "/support/images/questionmark.png", Languages.getString(this.langPrefix + ".desc." + p.name, sessionData.getLanguage())), false)
                                            + "<br>"
                                            + this.getCheckList(p.name, (List<List<String>>) p.data, l, false),
                                    false), false);
                            break;
                        default:
                            String tagTd = this.getTag(TAG_TD, null, this.getField(p, this.getValue(p.name), sessionData), false);
                            String languagesDesc = Languages.getString(this.langPrefix + ".desc." + p.name, sessionData.getLanguage());
                            String helpToolTip = this.getHelpToolTip(p.name, "/support/images/questionmark.png", languagesDesc);
                            String languageLabel = Languages.getString(this.langPrefix + ".label." + p.name, sessionData.getLanguage());
                            final String attr = "width: 30%;";
                            String tagTh = this.getTag(TAG_TH, new ArrayList<HtmlAttrib>() {
                                {
                                    add(new HtmlAttrib(HtmlAttrib.ATTRIB_STYLE, attr));
                                    
                                }
                            }, languageLabel + ":" + helpToolTip, false);
                            t += this.getTag(TAG_TR, null, tagTh + tagTd, false);
                            break;
                    }
                }
            }
            out.print(t);
        } catch (IOException e) {
            LOG.error("Show fields error", e);
        }
    }

    public void loadFromRequest(HttpServletRequest request, HttpServletResponse response, ServletContext context, SessionData sessionData) throws FileUploadException {
        this.clearValues();

        if (ServletFileUpload.isMultipartContent(request)) {
            this.parseRequest(request, sessionData);
        } else {
            //FIX:  here you must load ALL fields not only by metadata
            for (HtmlField p : this.metadata) {
                Object o = request.getParameterValues(p.name);
                if (o != null) {
                    if (o.getClass().isArray()) {
                        if (((Object[]) o).length == 1) {
                            o = ((Object[]) o)[0];
                        }
                    }
                }
                this.setValue(p.name, o);
            }
        }
    }

    /**
     * @param fieldName
     * @param width
     * @param height
     * @param size
     * @return
     */
    protected boolean checkImageSize(String fieldName, int width, int height, int size) {
        Image im = new Image();
        im.setData(this.getValue(fieldName + "_file"));
        byte[] data = im.getDataBytes();
        boolean ok = true;
        if (data != null) {
            String msg = "";
            try {
                if (size > 0 && (/*data == null || -> Always yield false */data.length > size)) {
                    cat.error("HtmlComponent.checkImageSize error: invalid image size");
                    msg += "Invalid image size. ";
                    ok = false;
                }
                double measureThrshld = 0.2;
                InputStream in = new ByteArrayInputStream(data);
                BufferedImage image = ImageIO.read(in);
                if (image != null) {
                    if (width > 0 && (image.getWidth() < (width * (1 - measureThrshld)) || image.getWidth() > (width * (1 + measureThrshld)))) {
                        cat.error("HtmlComponent.checkImageSize error: invalid image width");
                        msg += "Invalid image width. ";
                        ok = false;
                    }
                    if (height > 0 && (image.getHeight() < (height * (1 - measureThrshld)) || image.getHeight() > (height * (1 + measureThrshld)))) {
                        cat.error("HtmlComponent.checkImageSize error: invalid image height");
                        msg += "Invalid image height. ";
                        ok = false;
                    }
                }
            } catch (IOException e) {
                cat.error("HtmlComponent.checkImageSize error: " + e.getMessage());
                ok = false;
            }
            if (msg.length() > 0) {
                this.errors.put(fieldName, msg);
            }
        }
        return ok;
    }

    public boolean validateFields() {
        boolean ok = true;
        for (HtmlField p : this.metadata) {
            switch (p.type) {
                case IMAGE_FILE:
                    ok = ok && this.onCheckImageField(p);
                    break;
                case FILE:
                    ok = ok && this.onCheckDocumentField(p);
                    break;
                case COLOR:
                    break;
                case TEXT:
                    break;
                case TEXT_STATIC:
                    break;
                case TEXTAREA:
                    break;
                case BOOL_CHECKBOX:
                    break;
                case DATE:
                    break;
                case DATE_TIME:
                    break;
                case TIME:
                    break;
                case SINGLE_SELECT:
                    break;
                default:
                    break;
            }
        }
        return ok;
    }

    /**
     * Parse the given HttpServletRequest. If the request is a multipart
     * request, then all multipart request items will be processed, else the
     * request will be returned unchanged. During the processing of all
     * multipart request items, the name and value of each regular form field
     * will be added to the parameterMap of the HttpServletRequest. The name and
     * File object of each form file field will be added as attribute of the
     * given HttpServletRequest. If a FileUploadException has occurred when the
     * file size has exceeded the maximum file size, then the
     * FileUploadException will be added as attribute value instead of the
     * FileItem object.
     *
     * @param request The HttpServletRequest to be checked and parsed as
     * multipart request.
     * @param sessionData
     * @throws FileUploadException
     */
    // ServletFileUpload#parseRequest() does not return generic type.
    @SuppressWarnings("unchecked")
    public void parseRequest(HttpServletRequest request, SessionData sessionData) throws FileUploadException {
        //if (!ServletFileUpload.isMultipartContent(request)) {
        long maxFileSize = 10485760; // 10 MB
        //long maxFileSize = 10; // 10 MB
        this.clearValues();
        List<FileItem> multipartItems = null;
        String msg = "";
        try {
            multipartItems = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            // Note: we could use ServletFileUpload#setFileSizeMax() here, but that would throw a FileUploadException immediately without processing the other	
            // fields. So we're checking the file size only if the items are already parsed. See processFileField().	
        } catch (FileUploadException e) {
            cat.error("ERROR:  parseRequest. Cannot parse multipart request: " + e.getMessage());
            throw e;//In order to avoid possible null in the For below
        }
        // Loop through multipart request items.
        HashMap<String, Object> fixes = new HashMap<>();
        for (FileItem multipartItem : multipartItems) {
            if (multipartItem.isFormField()) {
                // Process regular form field (input type="text|radio|checkbox|etc", select, etc).	
                String name = multipartItem.getFieldName();
                String value = multipartItem.getString();
                Object values = this.getObject(name);
                HtmlField q = this.getMetadata(name);
                if (values == null || (q != null && q.type != HtmlFieldType.CHECK_LIST)) {// Not in parameter map yet, so add as new value OR the value is an scalar and NOT an array
                    this.setValue(name, value);
                } else {//If its an array
                    String[] newValues;
                    if (values.getClass().isArray()) {// Multiple field values, so add new value to existing array.	
                        int length = ((String[]) values).length;
                        newValues = new String[length + 1];
                        System.arraycopy((String[])values, 0, newValues, 0, length);
                        newValues[length] = value;
                    } else {
                        newValues = new String[]{(String) values, value};
                    }
                    this.setValue(name, newValues);
                }
            } else {
                // Process form file field (input type="file").
                maxFileSize = 5242880; // 5MB
                String name = multipartItem.getFieldName();
                //String value = multipartItem.getString(); //Never read	
                //Object values = this.getObject(name); //Never read
                HtmlField q = this.getMetadata(name);
                if (multipartItem.getName().length() <= 0) {// No file uploaded.	
                    request.setAttribute(multipartItem.getFieldName(), null);
                    msg = "No file uploaded";
                } else if (maxFileSize > 0 && multipartItem.getSize() > maxFileSize) {// File size exceeds maximum file size.	
                    request.setAttribute(multipartItem.getFieldName(), new FileUploadException("File size exceeds maximum file size of " + maxFileSize + " bytes."));
                    multipartItem.delete();// Immediately delete temporary file to free up memory and/or disk space.
                    msg = "File size exceeds maximum file size of " + maxFileSize + " bytes.";
                } else {// File uploaded with good size.	
                    request.setAttribute(multipartItem.getFieldName(), multipartItem);

                    if (!this.onCheckDocumentField(q)) {
                        //if(multipartItem.getContentType().contains("image")){
                        this.setValue(multipartItem.getFieldName(), new sun.misc.BASE64Encoder().encode(multipartItem.get()));
                        fixes.put(multipartItem.getFieldName() + "_name", multipartItem.getName());
                        //}	
                    } else {
                        if (multipartItem.getContentType().contains("application/msword") || multipartItem.getContentType().contains("application/octet-stream")
                                || multipartItem.getContentType().contains("application/vnd.openxmlformats-officedocument.presentationml.presentation")
                                || multipartItem.getContentType().contains("application/vnd.ms-powerpoint")
                                || multipartItem.getContentType().contains("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                                || multipartItem.getContentType().contains("application/vnd.ms-excel")
                                || multipartItem.getContentType().contains("application/pdf")
                                || multipartItem.getContentType().contains("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                            this.setBytes(multipartItem.get());

                        } else {
                            msg = Languages.getString("jsp.admin.document.label.formatInformation", sessionData.getLanguage());
                        }
                        fixes.put(multipartItem.getFieldName() + "_name", multipartItem.getName());
                    }

                    //fixes.put(multipartItem.getFieldName()+"_name",  multipartItem.getName());
                }

                if (msg.length() > 0) {
                    this.errors.put(name, msg);
                }

            }
        }
        Set<String> keys = fixes.keySet();
        for (String key : keys) {
            this.setValue(key, fixes.get(key));
        }
    }

    /**
     *
     * @return
     */
    public HashMap<String, String> getErrors() {
        return this.errors;
    }

}
