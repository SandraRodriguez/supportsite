package com.debisys.presentation;

public class MenuOption {
	
	public MenuOption(String label, String url, String sclass) {
		super();
		this.label = label;
		this.url = url;
		this.sclass = sclass;
	}

	public MenuOption(String label, String url) {
		super();
		this.label = label;
		this.url = url;
		this.sclass = "";
	}

	private String label;
	
	private String url;

	private String sclass;
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the sclass
	 */
	public String getSclass() {
		return sclass;
	}

	/**
	 * @param sclass the sclass to set
	 */
	public void setSclass(String sclass) {
		this.sclass = sclass;
	}
	
}
