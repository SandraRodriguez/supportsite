package com.debisys.presentation;

import com.debisys.exceptions.NewsException;
import com.debisys.news.News;
import com.debisys.presentation.html.*;
import com.debisys.users.SessionData;

/**
 * @author Juan Carlos Cruz
 *
 */
public class NewsComponent extends HtmlComponent {
	
	private News news;
	
	public NewsComponent(final News vnews){
		super("jsp.admin.news", vnews);
	}

	@Override
	public void loadDefaults() {
		//setValue("repId", "");
		//setValue("Title", "");
		//setValue("Body", "");
		//setValue("EnterDate", "");
		//setValue("EndDate", "");
	}

	@Override
	public void loadMetadata(Object vnews) {
		news = (News)vnews;
		addField(new HtmlField("newsId", HtmlFieldType.HIDDEN));
		addField(new HtmlField("Title", HtmlFieldType.TEXT, 50));
		addField(new HtmlField("Body", HtmlFieldType.TEXTAREA));
		addField(new HtmlField("EnterDate", HtmlFieldType.DATE_TIME));
		addField(new HtmlField("EndDate", HtmlFieldType.DATE_TIME, 1));
		if (news != null) {
			try {
				addField(new HtmlField("RepId", HtmlFieldType.CHECK_LIST, 0,  news.getAllISOs()));
			} catch (NewsException e) {
				e.printStackTrace();
			}
		}
	}

	public void loadFromDB() {
		clearValues();
		try {
			news.load();
			if (news.getNewsId() > 0) {
				setValue("newsId", String.valueOf(news.getNewsId()));
				setValue("Title", news.getTitle());
				setValue("Body", news.getBody());
				setValue("EnterDate", news.getEnterDate());
				setValue("EndDate", news.getEndDate());
				setValue("RepId", news.getRepIds());
			}
		} catch (NewsException e1) {
			e1.printStackTrace();
		}
	}

	public boolean save() {
		try {
			news.setTitle(getValue("Title"));
			news.setBody(getValue("Body"));
			news.setEnterDate(getValue("EnterDate"));
			news.setEndDate(getValue("EndDate"));
			news.setRepIds(getStringArray("RepId"));
			return news.save();
		} catch (NewsException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	protected boolean onCheckImageField(HtmlField p) {
		return false;
	}

	@Override
	protected String onGetImageField(HtmlField p, String value,SessionData sessionData) {
		return null;
	}

	@Override
	protected boolean onCheckDocumentField(HtmlField p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String onGetDocumentField(HtmlField p, String value,SessionData sessionData) {
		// TODO Auto-generated method stub
		return null;
	}
}