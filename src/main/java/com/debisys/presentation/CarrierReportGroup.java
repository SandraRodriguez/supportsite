package com.debisys.presentation;

import java.util.Vector;



import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;

// TODO: create instead a treeview class
public class CarrierReportGroup{
	String urlLink = null;
	String label = null;
	String blockName = null;
	Vector<CarrierReportGroup> subReports = null;
	static Boolean isLimitedCSrepresentative = true;
	
	public CarrierReportGroup(String vlab, String vurl, String div,SessionData sessionData){
		if(vlab != null){
			label = Languages.getString(vlab,sessionData.getLanguage());
		}
		urlLink = vurl;
		blockName = div;
	}
	
	public void add(CarrierReportGroup r){
		if(r != null){
			if(subReports == null){
				subReports = new Vector<CarrierReportGroup>();
			}
			subReports.add(r);
		}
	}

	/**
	 * Indicates if the inner vector has items and therefore this item should be rendered in the page
	 * 
	 * @return True if the inner vector has items, otherwise is false
	 */
	public boolean hasItems()
	{
		if ( this.subReports != null )
		{
			return this.subReports.size() > 0;
		}
		return false;
	}

	public void showItem(JspWriter out){
		try{
			//System.out.println("label:" + label);
			
			if(label != null){
				out.print("<li class=\""+((urlLink == null)?"group-closed":"report")+"\">");
			}
			
			if(label == null){
				out.print((urlLink == null)?"<span class=main2>":"");
			}
			
			if(urlLink != null){
				if(label != null){
					out.print("<a href=\"" + urlLink + "\"> " + label + "</a>");
				}
			}else{
				if(label != null){
					out.print("<span class=\"group-label\">" + label + " (" + subReports.size() + ")</span>");
				}
				String name = (blockName!=null)?"name=\""+ blockName +"\"":"";
				out.print("<ul class=\"sublevel\" "+ name.trim() +">");
				
				if(subReports != null){
					for(CarrierReportGroup r: subReports){
						r.showItem(out);
					}
				}
				out.print("</ul>");
			}
			if(label == null){
			out.print((urlLink == null)?"</span2>":"");
			}
			if(label != null){
				out.print("</li>");
			}
		}catch(Exception ex){
			System.out.println("Error in ReportGroup.showItem()");
		}
	}

	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static CarrierReportGroup createAnalysisGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType, int page){

		CarrierReportGroup groups = new CarrierReportGroup(null, null, "main",sessionData);

		if(page==0 || page==2){
		// Adding VOLUME -----------------------------------------------------------------
		{
				CarrierReportGroup r = new CarrierReportGroup("jsp.admin.reports.group.titleTV", null, "divTotalVolume",sessionData);
				r.add(new CarrierReportGroup("jsp.admin.reports.title9", "admin/reports/carrier_user/analysis/volume_daily.jsp", null,sessionData));
				r.add(new CarrierReportGroup("jsp.admin.reports.title11","admin/reports/carrier_user/analysis/volume_monthly.jsp", null,sessionData));
				groups.add(r);
		}
		}

		return groups;
	}
	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static CarrierReportGroup createCarrierGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType, int page){
		CarrierReportGroup groups = new CarrierReportGroup(null, null, "main",sessionData);

		if(page==0 || page==3)
			{
				groups.add(new CarrierReportGroup("jsp.admin.reports.carrier_user.carrier.carrier_summ", "admin/reports/carrier_user/carrier/carrier_report.jsp", null,sessionData));
				
				
				// to modify and add multiple reports:
				//ReportGroup r = new ReportGroup("jsp.admin.reports.carrier_user.carrier", null, "divCarrier",sessionData);
				//r.add(new ReportGroup("jsp.admin.reports.carrier_user.carrier.carrier_summ", "admin/reports/carrier/index.jsp", null,sessionData));
				//groups.add(r);
			}
			
		return groups;
	}

	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static CarrierReportGroup createRoamingGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType, int page){
		CarrierReportGroup groups = new CarrierReportGroup(null, null, "main",sessionData);
		if(page==0 || page==4)
			{
				groups.add(new CarrierReportGroup("jsp.admin.reports.title28", "admin/reports/carrier_user/crossborder/roaming_report.jsp", null,sessionData));
				// to modify and add multiple reports:
				//ReportGroup r = new ReportGroup("jsp.includes.menu.roaming_etopups", null, "divRoaming",sessionData);
				//r.add(new ReportGroup("jsp.admin.reports.title28", "admin/reports/crossborder/index.jsp", null,sessionData));
				//groups.add(r);
			}
			
		return groups;
	}
	/**
	 * @param sessionData
	 * @param context
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @param customConfigType
	 * @return
	 */
	public static CarrierReportGroup createTransactionsGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType, String customConfigType, int page){
		CarrierReportGroup groups = new CarrierReportGroup(null, null, "main",sessionData);
		
		// Adding TransactionSummaries
		if(page==0 || page==1){
		{
			
			CarrierReportGroup r = new CarrierReportGroup("jsp.admin.reports.group.titleTS", null, "divTransactionSummaries",sessionData);
			if(!sessionData.checkPermission(DebisysConstants.PERM_CS_REP_CARRIER_VIEW)) // The new permission should only show the transaction summary report for the carrier view
			{
				
			
			if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{   
				
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						//by agent
						r.add(new CarrierReportGroup("jsp.admin.reports.title1", "admin/reports/carrier_user/transactions/agents.jsp", null,sessionData));
					}
					if (!strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						// by subagent
						r.add(new CarrierReportGroup("jsp.admin.reports.title2", "admin/reports/carrier_user/transactions/subagents.jsp", null,sessionData));
					}
				} 
				//by rep
				r.add(new CarrierReportGroup("jsp.admin.reports.title3", "admin/reports/carrier_user/transactions/reps.jsp", null,sessionData));
			}
				// by merchants
				r.add(new CarrierReportGroup("jsp.admin.reports.title4", "admin/reports/carrier_user/transactions/merchants.jsp", null,sessionData));
			
			if (!strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				// by phone
				r.add(new CarrierReportGroup("jsp.admin.reports.title34", "admin/reports/carrier_user/transactions/transaction_by_phone.jsp", null,sessionData));
			}
			//by product
			r.add(new CarrierReportGroup("jsp.admin.reports.carrier_user.transactions.trans_summ_by_prod", "admin/reports/carrier_user/transactions/products.jsp", null,sessionData));

			// by Terminals 
			r.add(new CarrierReportGroup("jsp.admin.reports.carrier_user.transactions.trans_summ_by_term_type", "admin/reports/carrier_user/transactions/terminals.jsp", null,sessionData));

			// Error Summary Report 
			r.add(new CarrierReportGroup("jsp.admin.reports.carrier_user.transactions.trans_err_summ_report", "admin/reports/carrier_user/transactions/transaction_error.jsp?filter=merchants", null,sessionData));

			// hourly summary Report
			r.add(new CarrierReportGroup("jsp.admin.reports.carrier_user.transactions.hour.title", "admin/reports/carrier_user/transactions/hourly.jsp", null,sessionData));
			}

			// Trx summary Report
			r.add(new CarrierReportGroup("jsp.admin.reports.carrier_user.transactions.trans_summ_report", "admin/reports/carrier_user/transactions/transactions.jsp", null,sessionData));
			
			if (r.hasItems())
			{
				groups.add(r);
			}
		}
		// Adding CreditOrPaymentSummaries

		if(!sessionData.checkPermission(DebisysConstants.PERM_CS_REP_CARRIER_VIEW)) // The new permission should only show the transaction summary report for the carrier view
		
			
		{
			CarrierReportGroup r = new CarrierReportGroup("jsp.admin.reports.group.titleCPS", null, "divCreditOrPaymentSummaries",sessionData);
			
			if (!strAccessLevel.equals(DebisysConstants.CARRIER))
			{
					r.add(new CarrierReportGroup("jsp.admin.reports.title38", "admin/reports/carrier_user/payments/daily_liability.jsp", null,sessionData));
				
			}
			if ( r.hasItems() )
			{
				groups.add(r);
			}
		}}
		return groups;
	}
}
