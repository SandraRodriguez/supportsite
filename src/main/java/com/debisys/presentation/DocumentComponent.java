package com.debisys.presentation;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.debisys.exceptions.BannerException;
import com.debisys.exceptions.DocumentException;
import com.debisys.exceptions.ImageException;
import com.debisys.document.Document;
import com.debisys.presentation.html.HtmlComponent;
import com.debisys.presentation.html.HtmlField;
import com.debisys.presentation.html.HtmlFieldType;
import com.debisys.promotions.ISO;
import com.debisys.terminalTracking.CFileLoad;
import com.debisys.terminalTracking.CResultMessage;
import com.debisys.users.SessionData;
import com.debisys.util.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.StringUtil;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.debisys.images.Image;
import com.debisys.languages.Languages;


/**
 * @author Edwin Mu�oz
 *
 */

public class DocumentComponent extends HtmlComponent {

	private Document doc;
	String[] chain;
	
	String userIntranet="";

	/**
	 * @return the userIntranet
	 */
	public String getUserIntranet() {
		return userIntranet;
	}

	/**
	 * @param userIntranet the userIntranet to set
	 */
	public void setUserIntranet(String userIntranet) {
		this.userIntranet = userIntranet;
	}


	public DocumentComponent(final Document docu) {
		super("jsp.admin.document", docu);
		
	}

	@Override
	public void loadDefaults() {
		try {
			if( doc.load() && doc.getDocumentId() > 0) {
				setValue("name", String.valueOf(doc.getName()));
				setValue("description", String.valueOf(doc.getDescription()));
				setValue("email", String.valueOf(doc.getEmail()));
				setValue("visible", doc.getVisible());
				String[] a= doc.getChain();
				setValue("chain", doc.getChain());
				setValue("id", doc.getDocumentId());
			}
			else if( doc.load() && doc.getDocumentId() > 0) {
				setValue("name", String.valueOf(doc.getName()));
				setValue("description", String.valueOf(doc.getDescription()));
				setValue("email", String.valueOf(doc.getEmail()));
				setValue("visible", doc.getVisible());
				String[] a= doc.getChain();
				setValue("chain", doc.getChain());
				setValue("id", doc.getDocumentId());
			}
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	@SuppressWarnings("serial")
	@Override
	public void loadMetadata(Object docu) {
		doc = (Document)docu;

		addField(new HtmlField("name", HtmlFieldType.TEXT, 50, 50));
		addField(new HtmlField("description", HtmlFieldType.TEXT,100,100));
		addField(new HtmlField("email", HtmlFieldType.TEXT,50,50));
		
		if (doc != null) {
			try {
				
				addField(new HtmlField("category", HtmlFieldType.SINGLE_SELECT, 0, doc.getAllCategory()));
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
		addField(new HtmlField("visible", HtmlFieldType.BOOL_CHECKBOX));
		
		if (doc != null) {
			try {
		addField(new HtmlField("chain", HtmlFieldType.CHECK_LIST, 0, doc.getAllChain()));
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		addField(new HtmlField("doc", HtmlFieldType.FILE));
	}

	public void loadFromDB() {
		clearValues();
		try {
			if(doc.load() && doc.getDocumentId() > 0) {
				setValue("name", String.valueOf(doc.getName()));
				setValue("description", String.valueOf(doc.getDescription()));
				setValue("email", String.valueOf(doc.getEmail()));
				setValue("visible", doc.getVisible());
				String[] a= doc.getChain();
				setValue("chain", doc.getChain());
				setValue("id", doc.getDocumentId());
				setValue("status", doc.getApprovalStatus());
				setValue("category", doc.getCategory());
			}
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	}

	public void load(HtmlField p, HtmlComponent comp) {
		try {
			if (doc.load()) {
				comp.setValue(p.name , "" + doc.getName());
			
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected boolean onCheckDocumentField(HtmlField p) {
		return true;
	}

	@Override
    protected String onGetDocumentField(HtmlField p, String value,SessionData sessionData) {
		return getDocumentField(p, value, null, 0, 0, 0, true,sessionData);
    }
	
	public long save(SessionData sessionData) {
		
		String user = getValue("user");
		if(user != null && !user.equals("")){
			doc.setUser(user);
		}
		else{
			doc.setUser("");
		}
		
		String repId =getValue("repId");
		if(repId ==null){
			repId = doc.getRepId();
		}
		String name = getValue("name");  //nombre del documento
		if(name == null){
			name = doc.getName();
		}
		String description = getValue("description");
		if(description ==null){
			description = doc.getDescription();
		}
		String email = getValue("email");
		if(email ==null){
			email = doc.getEmail();
		}
		String visible = getValue("visible");
		
		if(visible==null){			
			boolean b = doc.getVisible();
			doc.setVisible(b);
		}
		if(visible.equals("true")){
			doc.setVisible(true);
		}else{
			doc.setVisible(false);
		}
		
		String[] b =  doc.getChain();
		if(b == null){
			doc.setChain(getStringArray("chain"));
		}
		
		String category = getValue("category");
		if(category==null){
			category =doc.getCategory();
		}
		
		if (name!=null && !name.equals("")){
			doc.setName(name);
		}
		else{
			doc.setName("");
			errors.put("name", Languages.getString("jsp.admin.document.documentrepository.errorname",sessionData.getLanguage()));				
		}
		if (description!=null && !description.equals("")){
			doc.setDescription(description);
		}
		else{
			doc.setDescription("");
			errors.put("description", Languages.getString("jsp.admin.document.documentrepository.errordescription",sessionData.getLanguage()));
		}
		
		
		if (email!=null && !email.equals("")){
			if(email.contains("@")){
				doc.setEmail(email);	
			}
			else{
				errors.put("email", Languages.getString("jsp.admin.document.documentrepository.erroremailvalid",sessionData.getLanguage()));
			}
			
		}
		else{
			doc.setEmail("");
			errors.put("email", Languages.getString("jsp.admin.document.documentrepository.erroremail",sessionData.getLanguage()));
		}
		
		//setValue("name", String.valueOf(doc.getName()));
		//setValue("description", String.valueOf(doc.getDescription()));
		//setValue("email", String.valueOf(doc.getEmail()));
		//setValue("chain", doc.getChain());
	if(!userIntranet.equals("true")){
		
		if(doc.getChain() != null && doc.getChain().length>0){
			//setValue("chain", chain);
			int ln = doc.getChain().length;
			String op="";
			String num ="";
			
			for (int i =0; i<ln;i++){
				op= doc.getChain()[i];
				if(op != null){
					num+=op + ",";
					if(op.equals("1")){
						doc.setIso(true);
					}else if(op.equals("2")){
						doc.setAgent(true);
					}else if(op.equals("3")){
						doc.setSubagent(true);
					}else if(op.equals("4")){
						doc.setRep(true);
					}else if(op.equals("5")){
						doc.setMerchant(true);
					}	
				}else{
					errors.put("chain", Languages.getString("jsp.admin.document.documentrepository.errorchain",sessionData.getLanguage()));
				}
			}
		}else{
			errors.put("chain", Languages.getString("jsp.admin.document.documentrepository.errorchain",sessionData.getLanguage()));
		}
	}
		
		//setValue("id", doc.getDocumentId());
		//errors.put(name, msg);
		doc.setCategory(category);
		doc.setRepId(repId);
		//doc.setDescription(description);
		//doc.setEmail(email);
		
		Calendar cal = Calendar.getInstance(); 
		Date date = cal.getTime();
		//DateUtil util =  new DateUtil();
		String uploadDate = DateUtil.formatSqlDateTime(date);
		doc.setUploadDate(uploadDate);
		
		int status = doc.getApprovalStatus();
		
		doc.setApprovalStatus(status);

		
		if(!(errors.size()>0)){	
			 try {
				if(isEnabled("doc")){
					String filename = saveData();
					String path = getValue("contextPath");
					doc.setPath("/"+path+"/"+filename);
				}
				doc.setUserIntranet(userIntranet);
				doc.saveData();
				return 1;
			} catch (DocumentException e) {

				e.printStackTrace();
			}
		}
		else{
			return 0;
		}
		return 0;
	}

	private String saveData(){
		try {
			String folderPath = getValue("folderPath");
			File folder = new File(folderPath);
			if(!folder.exists()){
				folder.mkdir();
			}
										
			Date actualDate = new Date();
			SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy HH:mm:ss ");

			String dateStr = "_"+format.format(actualDate).trim().replace(":", "");
			
			File f = new File( getValue("doc_name") );
			String finalName = f.getName().substring(0, f.getName().lastIndexOf("."));
			String ext = f.getName().substring(f.getName().lastIndexOf("."), f.getName().length()).trim();
			finalName = finalName + dateStr + ext;
			
			FileOutputStream fos = new FileOutputStream(folderPath + "/" + finalName);
			
			//fos.write(getBytes("doc"));
			fos.write(getBytes());
			fos.flush();
			fos.close();
			return finalName;
		}catch (Exception e) {
			return "";
		}		
	}

	
	@Override
	protected boolean onCheckImageField(HtmlField p) {
		return false;
	}

	@Override
	protected String onGetImageField(HtmlField p, String value,SessionData sessionData) {
		return null;
	}
}

