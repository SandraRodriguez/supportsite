package com.debisys.presentation;

import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;

import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.reports.ResourceReport;
import com.debisys.reports.TransactionReport;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import net.emida.supportsite.mvc.UrlPaths;

import static net.emida.supportsite.mvc.UrlPaths.*;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

// TODO: create instead a treeview class
public class ReportGroup{
	String urlLink = null;
	String label = null;
	String blockName = null;
	Vector<ReportGroup> subReports = null;

	public ReportGroup(String vlab, String vurl, String div,SessionData sessionData){
		if(vlab != null){
			this.label = Languages.getString(vlab,sessionData.getLanguage());
		}
		this.urlLink = vurl;
		this.blockName = div;
	}
        
        /**
         * 
         * @param vlab
         * @param vurl
         * @param div 
         */
        public ReportGroup(String vlab, String vurl, String div){
            this.label = vlab;
            this.urlLink = vurl;
            this.blockName = div;
	}

	public void add(ReportGroup r){
		if(r != null){
			if(this.subReports == null){
				this.subReports = new Vector<ReportGroup>();
			}
			this.subReports.add(r);
		}
	}

	/**
	 * Indicates if the inner vector has items and therefore this item should be rendered in the page
	 * 
	 * @return True if the inner vector has items, otherwise is false
	 */
	public boolean hasItems()
	{
		if ( this.subReports != null )
		{
			return this.subReports.size() > 0;
		}
		return false;
	}

	public void showItem(JspWriter out){
		try{
			//System.out.println("label:" + label);

			if(this.label != null){
				out.print("<li class=\""+((this.urlLink == null)?"group-closed":"report")+"\">");
			}

			if(this.label == null){
				out.print((this.urlLink == null)?"<span class=main2>":"");
			}

			if(this.urlLink != null){
				if(this.label != null){
					out.print("<a id=\""+this.urlLink+"\" href=\"" + this.urlLink + "\"> " + this.label + "</a>");
				}
			}else{
				if(this.label != null){
					out.print("<span class=\"group-label\">" + this.label + " (" + this.subReports.size() + ")</span>");
				}
				String name = (this.blockName!=null)?"name=\""+ this.blockName +"\"":"";
				out.print("<ul class=\"sublevel\" "+ name.trim() +">");

				if(this.subReports != null){
					for(ReportGroup r: this.subReports){
						r.showItem(out);
					}
				}
				out.print("</ul>");
			}
			if(this.label == null){
				out.print((this.urlLink == null)?"</span2>":"");
			}
			if(this.label != null){
				out.print("</li>");
			}
		}catch(Exception ex){
			System.out.println("Error in ReportGroup.showItem()");
		}
	}

	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static ReportGroup createAnalysisGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);
                String sessionLanguage = sessionData.getUser().getLanguageCode(); 
                if ( sessionLanguage.equals("0"))
                {
                    sessionLanguage="1";
                }
		if (!strAccessLevel.equals(DebisysConstants.CARRIER))
		{
			groups.add(new ReportGroup("jsp.admin.reports.title8", "admin/reports/analysis/dashboard.jsp", null,sessionData));
                        if ( sessionData.checkPermission(DebisysConstants.PERM_ALLOW_MERCHANT_MAP_LOCATOR_REPORT) )
                        {
                            groups.add(new ReportGroup("jsp.admin.reports.titleMML", "admin/reports/analysis/merchant_locat.jsp", null,sessionData));
                        }
                        if ( sessionData.checkPermission(DebisysConstants.PERM_ALLOW_STOCK_LEVELS_THRESHOLD_REPORT) ){
                            groups.add(new ReportGroup("jsp.admin.reports.titleMStockLevel", "admin/reports/analysis/merchantStockLevelsReport.jsp", null,sessionData));
                            ReportGroup remainingMappingRep = new ReportGroup("jsp.admin.reports.balanceMapping.merchantRemainingBalanceReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_PATH+"?start=1&sortField=merchant&sortOrder=ASC&rows=25", null,sessionData);
                            groups.add(remainingMappingRep);
                        }
                         
			// Adding VOLUME -----------------------------------------------------------------
			{
				ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleTV", null, "divTotalVolume",sessionData);
				r.add(new ReportGroup("jsp.admin.reports.title9", "admin/reports/analysis/volume_daily.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.title10", "admin/reports/analysis/volume_weekly.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.title11", "admin/reports/analysis/volume_monthly.jsp", null,sessionData));
				groups.add(r);
			}
			// Adding TOP 10 -----------------------------------------------------------------
			{
				ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleTop10", null, "divTop10",sessionData);
				if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)
						|| strAccessLevel.equals(DebisysConstants.SUBAGENT))
				{// only iso, agents, subagents
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						if (strAccessLevel.equals(DebisysConstants.ISO))
						{
							r.add(new ReportGroup("jsp.admin.reports.title12", "admin/reports/analysis/top10agents.jsp", null,sessionData));
						}
						if (!strAccessLevel.equals(DebisysConstants.SUBAGENT))
						{
							r.add(new ReportGroup("jsp.admin.reports.title13", "admin/reports/analysis/top10subagents.jsp", null,sessionData));
						}
					}
					r.add(new ReportGroup("jsp.admin.reports.title14", "admin/reports/analysis/top10reps.jsp", null,sessionData));
				}
				if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
				{// only isos and reps
					r.add(new ReportGroup("jsp.admin.reports.title15", "admin/reports/analysis/top10merchants.jsp", null,sessionData));
				}
				r.add(new ReportGroup("jsp.admin.reports.title16", "admin/reports/analysis/top10products.jsp", null,sessionData));
				groups.add(r);
			}
		}
		// Adding M ACTIVITY ----------------------------------------------------------------
		{
                    if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
                    {// everyone but the merchant
                        ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleMNL", null,"divMNL",sessionData);
                        r.add(new ReportGroup("jsp.admin.reports.churn.title", "admin/reports/analysis/churn_reporting.jsp", null,sessionData));
                        if (!strAccessLevel.equals(DebisysConstants.CARRIER))
                        {
                            if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
                            {
                                r.add(new ReportGroup("jsp.admin.reports.title17", "admin/reports/analysis/merchant_activity.jsp", null,sessionData));
                                r.add(new ReportGroup("jsp.admin.reports.merchant_notification_list", "admin/reports/analysis/merchant_notification_list.jsp", null,sessionData));
                            }
                            else
                            {
                                r.add(new ReportGroup("jsp.admin.reports.title17_international", "admin/reports/analysis/merchant_activity.jsp", null,sessionData));
                                r.add(new ReportGroup("jsp.admin.reports.merchant_notification_list_international",
                                                    "admin/reports/analysis/merchant_notification_list.jsp", null,sessionData));
                            }
                            r.add(new ReportGroup("jsp.admin.reports.analisys.merchant_detail_siteid.title",
                                            "admin/reports/analysis/merchant_detail_siteId.jsp", null,sessionData));
                            if (sessionData.checkPermission(DebisysConstants.PERM_NSF_REPORT) && com.debisys.utils.DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
                            {
                                r.add(new ReportGroup("jsp.admin.reports.nsf.title", "admin/reports/analysis/nsf.jsp", null,sessionData));
                            }

                        }
                        groups.add(r);
                    }
		}
		// ----------------------------------------------------------------
		if (!strAccessLevel.equals(DebisysConstants.CARRIER))
		{
			// Parature 5545-10237043 = Jorge Nicol?s Mart?nez Romero
			boolean permRechargeDataByPhoneNumber = sessionData.checkPermission(DebisysConstants.PERM_RERCHARGE_DATA_BY_PHONE_NUMBER_REPORT);
			if ( strAccessLevel.equals(DebisysConstants.ISO) && permRechargeDataByPhoneNumber )
			{				
				if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
				{
					groups.add(new ReportGroup("jsp.admin.reports.analysis.topedupphones", "admin/reports/analysis/topedupphones.jsp", null,sessionData));
				}
				else 
				{
					groups.add(new ReportGroup("jsp.admin.reports.analysis.topedupphones_international", "admin/reports/analysis/topedupphones.jsp", null,sessionData));
				}
			}
			// Parature 5545-10237043 = Jorge Nicol?s Mart?nez Romero	
			
			if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && strAccessLevel.equals(DebisysConstants.ISO)
					&& sessionData.checkPermission(DebisysConstants.PERM_BILLING_INT_REPORT))
			{
				groups.add(new ReportGroup("jsp.admin.reports.analysis.billingreport", "admin/reports/analysis/billingreport.jsp", null,sessionData));
			}
			// ----------------------------------------------------------------
			if (!strAccessLevel.equals(DebisysConstants.MERCHANT) && sessionData.checkPermission(DebisysConstants.PERM_ENABLE_SEARCH_TERMINALS_BY_SERIALNUMBER))
			{
				groups.add(new ReportGroup("jsp.admin.reports.analisys.terminal_search.title", "admin/reports/analysis/terminals_by_serialNumber.jsp", null,sessionData));
			}
			// ----------------------------------------------------------------
			if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_VIEW_TRANSACTIONS_ORIGINATION_REPORT))
			{
				groups.add(new ReportGroup("jsp.admin.reports.trans_origination_title", "admin/reports/transactions/transactions_org.jsp", null,sessionData));
			}
			if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_ENABLE_HEARTBEAT_REPORT))
			{
				groups.add(new ReportGroup("jsp.admin.reports.heartbeat_title", "admin/reports/analysis/heartbeat.jsp", null,sessionData));
			}
			if(!strAccessLevel.equals(DebisysConstants.MERCHANT) && sessionData.checkPermission(DebisysConstants.PERM_ENABLE_REP_ACTIVITY_REPORT))
			{
				groups.add(new ReportGroup("jsp.admin.reports.analysis.rep_activity_report", "admin/reports/analysis/rep_activity.jsp", null,sessionData));
			}
		}
                //if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                //{s
                    int intranetUser = sessionData.getUser().getIntranetUserId();
                    boolean intraPermission = false;
                    try
                    {
                        intraPermission = sessionData.getUser().hasIntranetUserPermission(intranetUser, 1, DebisysConstants.INTRANET_PERMISSION_IMTU_REPORT);
                        if ( sessionData.getUser().isIntranetUser() && intraPermission )
                        {
                            try 
                            {
                                 ArrayList<ResourceReport> resources = ResourceReport.findResourcesByReport( ResourceReport.RESOURCE_IMTU_FEATURE, sessionLanguage,"title");
                                 String title = ResourceReport.RESOURCE_IMTU_FEATURE;
                                 if ( resources != null && resources.size() == 1 )
                                 {
                                     title = resources.get(0).getMessage();
                                 }
                                 groups.add(new ReportGroup(title, "admin/reports/analysis/intranet/imtu_providers_by_product.jsp", null));
                             } catch (Exception ex) {
                                 Logger.getLogger(ReportGroup.class.getName()).log(Level.SEVERE, null, ex);
                             }
                        }
                    } 
                    catch (UserException e) 
                    {
                        Logger.getLogger(ReportGroup.class.getName()).log(Level.SEVERE, null, e);
                    }
                   
                    
                //}
			
		return groups;
	}

	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static ReportGroup createCarrierGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);

		{
			groups.add(new ReportGroup("jsp.admin.reports.carrier_user.carrier.carrier_summ", "admin/reports/carrier/index.jsp", null,sessionData));

			//ReportGroup r = new ReportGroup("jsp.admin.reports.carrier_user.carrier", null, "divCarrier",sessionData);
			//r.add(new ReportGroup("jsp.admin.reports.carrier_user.carrier.carrier_summ", "admin/reports/carrier/index.jsp", null,sessionData));
			//groups.add(r);
		}

		return groups;
	}

	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static ReportGroup createRoamingGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);

		{
			groups.add(new ReportGroup("jsp.admin.reports.title28", "admin/reports/crossborder/index.jsp", null,sessionData));
			//ReportGroup r = new ReportGroup("jsp.includes.menu.roaming_etopups", null, "divRoaming",sessionData);
			//r.add(new ReportGroup("jsp.admin.reports.title28", "admin/reports/crossborder/index.jsp", null,sessionData));
			//groups.add(r);
		}

		return groups;
	}

	/**
	 * @param sessionData
	 * @param context
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @param customConfigType
	 * @return
	 */
	public static ReportGroup createTransactionsGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType, String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);
		boolean bExternalRep=com.debisys.users.User.isExternalRepAllowedEnabled(sessionData);
		boolean showInfoMultiSource = sessionData.checkPermission(DebisysConstants.PERM_SHOW_MULTISOURCE_INFO);
                
                // Adding TransactionSummaries)
		{
			ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleTS", null, "divTransactionSummaries",sessionData);
			if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_PINMONEY_TRANSACTIONS_REPORT))
			{
				r.add(new ReportGroup("jsp.admin.reports.PINMoney_Transactions_Report.title", "admin/reports/transactions/pinmoney.jsp", null, sessionData));
			}

			if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				//only iso, agents, subagents
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						r.add(new ReportGroup("jsp.admin.reports.title1", "admin/reports/transactions/agents.jsp", null,sessionData));
					}
					if (!strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						r.add(new ReportGroup("jsp.admin.reports.title2", "admin/reports/transactions/subagents.jsp", null,sessionData));
					}
				}
				r.add(new ReportGroup("jsp.admin.reports.title3", "admin/reports/transactions/reps.jsp", null,sessionData));
			}
			if (!strAccessLevel.equals(DebisysConstants.MERCHANT) && !strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				r.add(new ReportGroup("jsp.admin.reports.title4", "admin/reports/transactions/merchants", null,sessionData));
			}
			if (!strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				r
				.add(new ReportGroup("jsp.admin.reports.TransactionMerchantSummaryByTerminal",
						"admin/reports/transactions/transaction_mercsumbytrm.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.title30", "admin/reports/transactions/clerks.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.title5", "admin/reports/transactions/products.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.title46", "admin/reports/transactions/products_merchants.jsp", null,sessionData));
				if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
				{
					r.add(new ReportGroup("jsp.admin.reports.title36", "admin/reports/transactions/terminals.jsp", null,sessionData));
					r.add(new ReportGroup("jsp.admin.reports.title7", "admin/reports/transactions/promo.jsp", null,sessionData));
				}
				r.add(new ReportGroup("jsp.admin.reports.title34", "admin/reports/transactions/transaction_by_phone.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.title32", "admin/reports/transactions/transaction_error.jsp?filter=merchants", null,sessionData));

				if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
					r.add(new ReportGroup("jsp.admin.reports.title44", "admin/reports/transactions/transaction_by_city.jsp", null,sessionData));
				}
				else
				{
					r.add(new ReportGroup("jsp.admin.reports.title44_international", "admin/reports/transactions/transaction_by_city.jsp", null,sessionData));
				}


				if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
				{
					r.add(new ReportGroup("jsp.admin.reports.merchant_nopaid_title", "admin/reports/transactions/merchant_nopaid.jsp", null,sessionData));
				}
				r.add(new ReportGroup("jsp.admin.reports.terminals_non_tx_title", "admin/reports/transactions/terminals_non_tx.jsp", null,sessionData));
                                boolean permConvenienceCardReport = sessionData.checkPermission(DebisysConstants.PERM_VIEW_CONVENIENCE_CARDS_SUMMARY_REPORTS);
                                if (permConvenienceCardReport){
                                    r.add(new ReportGroup("jsp.admin.reports.title48", "admin/reports/transactions/convenience_card_carriers.jsp", null,sessionData));
                                }
				//DTU-1149	Belize: Customized Report
				boolean permTrxReconSummReport = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TRX_SUMMARY_RECON_REPORT);
				if ( strAccessLevel.equals(DebisysConstants.ISO) && permTrxReconSummReport )
				{
					r.add(new ReportGroup("jsp.admin.reports.reconciliation.trxsummreconreport", "admin/reports/reconciliation/trxsummaryreconreport.jsp", null,sessionData));
				}
				boolean permTrxReconDetailedReport = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TRX_DETAILED_RECON_REPORT);
				if ( strAccessLevel.equals(DebisysConstants.ISO) && permTrxReconDetailedReport )
				{
					r.add(new ReportGroup("jsp.admin.reports.reconciliation.trxdetconreport", "admin/reports/reconciliation/trxdetailedreconreport.jsp", null,sessionData));
				}
				//END DTU-1149	Belize: Customized Report
				if ( strAccessLevel.equals(DebisysConstants.ISO) && showInfoMultiSource )
				{
                                    r.add(new ReportGroup("jsp.tools.dtu2536.report.main", "admin/reports/transactions/multisource.jsp?reportType=1", null,sessionData));
                                    r.add(new ReportGroup("jsp.tools.dtu2536.report.mainProvider", "admin/reports/transactions/multisource.jsp?reportType=2", null,sessionData));
				}
                                if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_TRX_REPORT_BY_REGION)){
                                    String url = UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.TRX_BY_REGION_PATH;
                                    r.add(new ReportGroup("jsp.tools.dtu5636.report.RegionTrxReport", url, null,sessionData));
                                }
			}
      			if ((strAccessLevel.equals(DebisysConstants.ISO))  && sessionData.checkPermission(DebisysConstants.PERM_ALLOW_SPREAD_BY_PROVIDER_CARRIER_REPORT))
			{
				r.add(new ReportGroup("jsp.admin.reports.commissionSpreadByProvCarReport.reportTitle", 
                                        UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.SPREAD_BY_PROVIDER_CARRIER_REPORT_PATH, null, sessionData));
			}

			if (r.hasItems())
			{
				groups.add(r);
			}
			
		}
		// Adding CreditOrPaymentSummaries
		{
			ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleCPS", null, "divCreditOrPaymentSummaries",sessionData);
			if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					&& !strAccessLevel.equals(DebisysConstants.MERCHANT) && !strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				r.add(new ReportGroup("jsp.admin.reports.title45", "admin/reports/payments/deposits.jsp", null,sessionData));
			}
			TransactionReport t = new TransactionReport();
			try {
				if (t.getISoProductPermision(sessionData,169) && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)){
					r.add(new ReportGroup("jsp.admin.reports.GM5.Title", "admin/reports/transactions/gm5.jsp", null,sessionData));
				}
			} catch (ReportException e) {
				e.printStackTrace();
			}
			if (!strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
				{
					if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
					{
						r.add(new ReportGroup("jsp.admin.reports.title6", "admin/reports/payments/index.jsp", null,sessionData));
					}
					else
					{
						r.add(new ReportGroup("jsp.admin.reports.title6_international", "admin/reports/payments/index.jsp", null,sessionData));
					}

					if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
					{
						if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
						{
							r.add(new ReportGroup("jsp.admin.reports.title40", "admin/reports/payments/user_payments.jsp", null,sessionData));
						}
						else
						{
							r.add(new ReportGroup("jsp.admin.reports.title40_international", "admin/reports/payments/user_payments.jsp", null,sessionData));
						}
					}
				}
				
                                r.add(new ReportGroup("jsp.admin.reports.title37", "admin/reports/transactions/merchant_credit.jsp?", null,sessionData));
                                r.add(new ReportGroup("jsp.admin.reports.merchant_credit_accountstatus", "admin/reports/transactions/merchant_credit_accountstatus.jsp", null,sessionData));
                                r.add(new ReportGroup("jsp.admin.reports.title38", "admin/reports/transactions/daily_liability.jsp", null,sessionData));
                                r.add(new ReportGroup("jsp.admin.reports.title.externalCreditOrPayment", "admin/reports/payments/external_credit_payment.jsp", null, sessionData));
												
				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
					{
						r.add(new ReportGroup("jsp.admin.reports.title42", "admin/reports/payments/deposits_by_merchant.jsp", null,sessionData));
					}
					else
					{
						r.add(new ReportGroup("jsp.admin.reports.title42_international", "admin/reports/payments/deposits_by_merchant.jsp", null,sessionData));
					}

				}
			}
			
			if (strAccessLevel.equals(DebisysConstants.ISO) && DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO.equals(deploymentType) 
					&& sessionData.checkPermission(DebisysConstants.PERM_ALLOW_AVAILABLE_BALANCE_REPORT)){
				r.add(new ReportGroup("jsp.admin.reports.titleABR", "admin/reports/transactions/merchant_balance_availability.jsp", null, sessionData));
				r.add(new ReportGroup("jsp.admin.reports.AFBR.title", CONTEXT_PATH + REPORT_PATH + REPS_AFBR_REPORT_PATH, null, sessionData));
			}

			//This is detailed shared credit payment report by rep
			if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_DETAILED_PAYMENT_REPORT) )
			{
				if( customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
					r.add(new ReportGroup("jsp.admin.reports.payments.detailedrep_payments.title", "admin/reports/payments/detailed_shared_payments.jsp", null,sessionData));
					if(!strAccessLevel.equals(DebisysConstants.MERCHANT)){
						r.add(new ReportGroup("jsp.admin.reports.payments.detailed_payments.title", "admin/reports/payments/detailed_payments.jsp", null,sessionData));
					}
				}
			}

			if (!strAccessLevel.equals(DebisysConstants.CARRIER) && !strAccessLevel.equals(DebisysConstants.MERCHANT)
					&& sessionData.checkPermission(DebisysConstants.PERM_ENABLE_MAXIMUM_SALES_LIMIT))
			{
				r.add(new ReportGroup("jsp.admin.saleslimit.reportTitle", "admin/reports/transactions/sales_limit.jsp", null,sessionData));
			}

			if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_INVOICE_MERCHANT_REPORT)
					&& customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) && strAccessLevel.equals(DebisysConstants.ISO)) {
				r.add(new ReportGroup("jsp.admin.reports.invoice_merchants.title", "admin/reports/transactions/merchants_invoice_data.jsp", null,sessionData));
			}

			if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_EDIT_ACCOUNT_TYPE))
			{
				r.add(new ReportGroup("jsp.admin.reports.transactions.reconcile_summary.title", "admin/reports/transactions/reconciliation_summary.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.transactions.reconcile_detail.title", "admin/reports/transactions/reconciliation_detail.jsp", null,sessionData));
			}
			if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_ENABLE_CREDIT_BALANCE_REPORT))
			{
				r.add(new ReportGroup("jsp.admin.reports.credit.balance.title", "admin/reports/payments/credit_balance.jsp", null,sessionData));			
			}
                        
                        if((deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) && strAccessLevel.equals(DebisysConstants.REP) && sessionData.checkPermission(DebisysConstants.PERM_ALLOW_TRANSFER_TO_REPS))
                                || (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) && strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_ALLOW_TRANSFER_TO_REPS)))
                        {
                            r.add(new ReportGroup("jsp.admin.reports.rep.transfers.title", "admin/reports/payments/rep_Transfers_Report.jsp", null,sessionData));
                        }
                        if (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_PAYMENT_REQUEST_REPORT)){
                            r.add(new ReportGroup("jsp.admin.reports.paymentRequestReport.title", "admin/reports/payments/paymentRequestReport.jsp", null, sessionData));
                        }
                        
                        //DTU-4208 - consolidated monthly sales
                        if(sessionData.getUser().isIntranetUser()){
                            r.add(new ReportGroup("jsp.report.consolidate.monthly.sales.title",  UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.CONSOLIDATE_MONTHLY_SALES_REPORT, null, sessionData));
			}
                        
			if ( r.hasItems() )
			{
				groups.add(r);
			}
		}
		// Adding ISOActionsOnAccounts
		{
			ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleIAOA", null, "divISOActionsOnAccounts",sessionData);
			if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel
					.equals(DebisysConstants.MERCHANT))
					&& (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) && !strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				if(strAccessLevel.equals(DebisysConstants.ISO) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
					r.add(new ReportGroup("jsp.admin.reports.title41", "admin/reports/payments/rep_actions.jsp", null,sessionData));
				}
			}

			if ( r.hasItems() )
			{
				groups.add(r);
			}
		}
		//Adding PINInventoryAndReturnRequests
		{
			ReportGroup r = new ReportGroup("jsp.admin.reports.group.titlePIRR", null, "divPINInventoryAndReturnRequests",sessionData);
			if (!strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				r.add(new ReportGroup("jsp.admin.reports.pin_request_report", "admin/reports/transactions/pin_request.jsp", null,sessionData));
			}
			if ( strAccessLevel.equals(DebisysConstants.ISO) && (sessionData.checkPermission(DebisysConstants.PERM_WRITE_OFF)) &&
					(DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)))
			{
				r.add(new ReportGroup("jsp.admin.reports.write_off_request_report", "admin/reports/transactions/write_off_request.jsp", null,sessionData));
			}
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				r.add(new ReportGroup("jsp.admin.reports.voidtopup_request_report", "admin/reports/transactions/voidtopup_request.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.pininventory.report", "admin/reports/transactions/pin_inventory.jsp", null,sessionData));
			}
			if(strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_PIN_REMAINING_INVENTORY))
			{
				r.add(new ReportGroup("jsp.admin.reports.pin_remaining_inventory.report", "admin/reports/transactions/pin_remaining_inventory.jsp", null,sessionData));
			}
			if(sessionData.checkPermission(DebisysConstants.PERM_VEW_PIN_HISTORY))
			{
				r.add(new ReportGroup("jsp.includes.menu.pinTransactions_search", "admin/reports/transactions/pinTransactions.jsp", null, sessionData));
			}
			if (r.hasItems())
			{
				groups.add(r);
			}
		}
		// Adding FinancialReports
		{
                    if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_FINANCIAL_SUMMARIES_REPORTS)){
				
			ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleGCS", null, "divFinancialReports",sessionData);
			if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.MERCHANT)){
				r.add(new ReportGroup("jsp.admin.reports.giftcards.gc_lookup", "admin/reports/giftcards/gc_search.jsp", null,sessionData));
			}
			if (!strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				r.add(new ReportGroup("jsp.admin.reports.giftcards.gc_summary_report.title", "admin/reports/giftcards/gc_summary.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.giftcards.gc_product_summary.title", "admin/reports/giftcards/gc_product.jsp", null,sessionData));
				r.add(new ReportGroup("jsp.admin.reports.giftcards.gc_deactivation.title", "admin/reports/giftcards/gc_deactivation.jsp", null,sessionData));
			}
			if (sessionData.checkPermission(DebisysConstants.PERM_ENABLED_NFINANSE_REPORT)
					&& (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.MERCHANT)))
			{
				r.add(new ReportGroup("jsp.admin.reports.nfinanse.title", "admin/reports/transactions/nfinanse.jsp", null,sessionData));
			}
			if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				if(sessionData.checkPermission(DebisysConstants.PERM_MONEY_TRANSFER) && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
					r.add(new ReportGroup("jsp.admin.reports.money_transfers.title", "admin/reports/transactions/money_transfers.jsp", null,sessionData));
				}
			}
			if (r.hasItems())
			{
				groups.add(r);
			}
                    }
		}
		// Adding Balance Report dtu-4
		{
			if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_PROVIDER_EXTERNAL_BALANCE_REPORT) && deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
					customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) && (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)))
			{
				ReportGroup r = new ReportGroup("jsp.admin.reports.group.titleBalance", null, "divBalanceReports",sessionData);
				r.add(new ReportGroup("jsp.admin.reports.provider.externalBalance.report.title", "admin/reports/providerBalance/providerExternalBalance_summary.jsp", null,sessionData));
				if (r.hasItems())
				{
					groups.add(r);
				}
			}
		}
		if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
		{
			if (sessionData.checkPermission(DebisysConstants.PERM_MERCHANT_JOINED) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)
					&& strAccessLevel.equals(DebisysConstants.ISO))
			{
				groups.add(new ReportGroup("jsp.admin.reports.merchants_joined.title", "admin/reports/transactions/merchants_joined.jsp", null,sessionData));
			}
		}
		
		if (strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.REAL_TIME_TRANSACTION_REPORT)) {
			if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
				groups.add(new ReportGroup("jsp.admin.reports.realTimeTransactionReport.title", "admin/reports/transactions/real_time_transaction_report.jsp", null, sessionData));
			}
		}

		return groups;
	}
	
	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static ReportGroup createPincacheGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);

		{
			groups.add(new ReportGroup("jsp.admin.pincache.reportpins", "admin/reports/pincache/pins.jsp", null,sessionData));
			groups.add(new ReportGroup("jsp.admin.pincache.reportsync", "admin/reports/pincache/sync.jsp", null,sessionData));
		}

		return groups;
	}	
	
	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static ReportGroup createACHGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);

		{
			if ( sessionData.checkPermission(DebisysConstants.PERM_ENABLE_FRM_COMMISSIONS) )
			{
				groups.add(new ReportGroup("jsp.admin.reports.ach.title.fmr_commission_payout", "admin/reports/ach/fmrCommissionPayout.jsp", null,sessionData));
			}			
		}

		return groups;
	}	
	
	
	/**
	 * @param sessionData
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param deploymentType
	 * @return
	 */
	public static ReportGroup createPINStockGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);

		{			
				groups.add(new ReportGroup("jsp.admin.reports.pinstock.title.pinStockLoadedReport", "admin/reports/pinStock/pinStockLoaded.jsp", null,sessionData));
				groups.add(new ReportGroup("jsp.admin.reports.pinstock.title.pinStockAvailableReport", "admin/reports/pinStock/pinStockAvailable.jsp", null,sessionData));			
		}

		return groups;
	}	
	
	public static ReportGroup createScheduleToolGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);

		{		
			//TODO add permission code
			groups.add(new ReportGroup("jsp.admin.reports.scheduleReport.manageSchedule", "admin/reports/scheduleReports/manageschedule.jsp", null,sessionData));
							
		}

		return groups;
	}

	public static ReportGroup createSpiffGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
		ReportGroup groups = new ReportGroup(null, null, "main",sessionData);

		//check permission or/and level

		ReportGroup spiffDetailRep = new ReportGroup("jsp.admin.reports.spiffReports.spiffDetailReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.SPIFF_DETAIL_REPORT_PATH, null,sessionData);
		groups.add(spiffDetailRep);
		
		ReportGroup ActivationDetailRep = new ReportGroup("jsp.admin.reports.spiffReports.activationDetailReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.ACTIVATION_DETAIL_REPORT_PATH, null,sessionData);
		groups.add(ActivationDetailRep);


		return groups;
	}
        
        public static ReportGroup createBalanceHistoryGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
          
            ReportGroup groups = new ReportGroup(null, null, "main",sessionData);
            if (strAccessLevel.equals(DebisysConstants.ISO) &&  sessionData.checkPermission(DebisysConstants.PERM_ALLOW_AUDIT_REPORT_MANAGEMENT)){
                
                //check permission or/and level
                ReportGroup spiffDetailRep = new ReportGroup("jsp.admin.reports.balanceAudit.balanceAuditReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.BALANCE_AUDIT_HISTORICAL_REPORT_PATH, null,sessionData);
                groups.add(spiffDetailRep);

                ReportGroup spiffDetailRep2 = new ReportGroup("jsp.admin.reports.balancehistory.balanceHistoryReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.BALANCE_HISTORY_HISTORICAL_REPORT_PATH, null,sessionData);
                groups.add(spiffDetailRep2);        
                
                ReportGroup spiffDetailRep3 = new ReportGroup("jsp.admin.reports.balancehistory.agendBalanceHistoryReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.SCHEDULER_HISTORIAL_BALANCE, null,sessionData);
                groups.add(spiffDetailRep3);

                ReportGroup spiffDetailRep4 = new ReportGroup("jsp.admin.reports.balancehistory.balanceHistoryByRepsReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.BALANCE_HISTORY_BY_REPS_HISTORICAL_REPORT_PATH, null,sessionData);
                groups.add(spiffDetailRep4); 
                
                if(sessionData.getUser().isIntranetUser()){
                    ReportGroup spiffDetailRep5 = new ReportGroup("jsp.report.balance.emidaBalanceOnProvider.title", 
                            UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.EMIDA_BALANCE_ON_PROVIDER, null,sessionData);
                    groups.add(spiffDetailRep5); 
                }
            }
            
            return groups;
         }  

        /*public static ReportGroup createMerchantRemainingBalanceMappingGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType,String customConfigType){
            ReportGroup groups = new ReportGroup(null, null, "main",sessionData);
            if (strAccessLevel.equals(DebisysConstants.ISO) &&  sessionData.checkPermission(DebisysConstants.PERM_ALLOW_AUDIT_REPORT_MANAGEMENT)){    
                //check permission or/and level
                ReportGroup remainingMappingRep = new ReportGroup("jsp.admin.reports.balanceMapping.merchantRemainingBalanceReport", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.MERCHANT_REMAINING_BALANCE_MAPPING_REPORT_PATH, null,sessionData);
                groups.add(remainingMappingRep);  
            }
            return groups;
         } */
    /**
     *
     * @param sessionData
     * @param context
     * @param strAccessLevel
     * @param strDistChainType
     * @param deploymentType
     * @param customConfigType
     * @return
     */
    public static ReportGroup createInternalReportsGroup(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType, String customConfigType) {
        ReportGroup groups = new ReportGroup(null, null, "main", sessionData);            
        if(sessionData.getUser().isIntranetUser()) {
            groups.add(new ReportGroup("jsp.tools.internal.report.tx.err.label", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + "/internalReport/transactionErr/transactionErr", null, sessionData));
            groups.add(new ReportGroup("noticeAcceptanceContracts.title", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.ACCEPTANCE_CONTRACTS, null, sessionData));
        }                
        //DTU-3842
        if (strAccessLevel.equals(DebisysConstants.ISO) &&  sessionData.checkPermission(DebisysConstants.PERM_ALLOW_ADMIN_TERMINAL_ACTIVITY)){
            groups.add(new ReportGroup("terminal.registration.records.title", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.TERMINAL_REGISTRATION, null, sessionData));
            groups.add(new ReportGroup("jsp.report.login.activity.main.title", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.LOGIN_ACTIVITY, null, sessionData));
        }
        //DTU-5398
        if (strAccessLevel.equals(DebisysConstants.ISO)){
            groups.add(new ReportGroup("jsp.reports.internalReport.switchAccount.main.title", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT + UrlPaths.REPORTS_INTERNAL_REPORT_SWITCH_ACCOUNT_VIEW, null, sessionData));
        }
        //DTU-4346
        if(sessionData.getUser().isIntranetUser()){
            groups.add(new ReportGroup("jsp.includes.menu.report.ppi.title", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.PAYMENT_PROCESSOR_INFORMATION, null, sessionData));
        }
        return groups;
    }

    /**
     *
     * @param sessionData
     * @param context
     * @param strAccessLevel
     * @param strDistChainType
     * @param deploymentType
     * @param customConfigType
     * @return
     */
    public static ReportGroup createFlowBusinessIntelligence(SessionData sessionData, ServletContext context, String strAccessLevel, String strDistChainType, String deploymentType, String customConfigType) {
        ReportGroup groups = new ReportGroup(null, null, "main", sessionData);
        if (strAccessLevel.equals(DebisysConstants.ISO) &&  sessionData.checkPermission(DebisysConstants.PERM_CWC_FLOW_STREETLIGHT_REPORTS) ){
            ReportGroup streetLightSnapshotReport = new ReportGroup("jsp.admin.reports.flow.streetlightSnapshotReport", "admin/reports/streetlightSnapshot/streetlightSnapshotForm.jsp", null, sessionData);        
            groups.add(streetLightSnapshotReport);

            ReportGroup streetLightTrendReport = new ReportGroup("jsp.admin.reports.flow.streetlightTrend.title", "admin/reports/streetlightSnapshot/streetLightTrendForm.jsp", null, sessionData);
            groups.add(streetLightTrendReport);

            ReportGroup streetLightTrendDetail = new ReportGroup("jsp.admin.reports.flow.streetlightTrendDetail.title", UrlPaths.CONTEXT_PATH + UrlPaths.REPORT_PATH + UrlPaths.STREETLIGHT_TREND_DETAIL_PATH, null, sessionData);
            groups.add(streetLightTrendDetail);
        }
        return groups;
    }

}