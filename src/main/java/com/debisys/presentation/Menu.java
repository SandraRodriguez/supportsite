/**
 * @author Juan Carlos Cruz
 */

package com.debisys.presentation;

import java.util.ArrayList;

import javax.servlet.ServletContext;

import com.debisys.exceptions.UserException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;

public class Menu {
	
	private ArrayList<MenuOption> subMenuOptions;
	private ArrayList<MenuOption> menuOptions;
	private int offSet;
	
	public Menu(ServletContext context, SessionData sessionData, int section, String strDistChainType, int section_page, String deploymentType, String serverName){
		if (sessionData.isLoggedIn()){
			// If the user is a carrier user, show a custom menu.
			// Easier than going through and filtering certain menus, less headaches.
			// Should only show home screen, custom carrier reports section, help, contact us, and logout
			if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS) || (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS) && sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_TOOLS))) {
				menuOptions = new ArrayList<MenuOption>();
				menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.home",sessionData.getLanguage()), "admin/index.jsp", (section==1 ? "gcol":"")));
				//menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.reports",sessionData.getLanguage()), "admin/reports/carrier_user/index.jsp", (section==14 ? "gcol":"")));
				menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.reports",sessionData.getLanguage()), "admin/reports/carrier_user/index.jsp", (section==14 ? "gcol":"")));
                                
                                if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_TOOLS)){
                                    menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.tools",sessionData.getLanguage()), "admin/tools/carrierUser/index.jsp", ""));
                                }
				menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.help",sessionData.getLanguage()), "javascript:void(0);", "helpLink"));
				menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.logout",sessionData.getLanguage()), "logout.jsp"));
				
				subMenuOptions = new ArrayList<MenuOption>();
				if (section==14) {
					subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.transactions",sessionData.getLanguage()), "admin/reports/carrier_user/transactions/index.jsp"));
					if(!sessionData.checkPermission(DebisysConstants.PERM_CS_REP_CARRIER_VIEW)){ // The new permission should only show the transaction summary report for the carrier view
						subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.analysis",sessionData.getLanguage()), "admin/reports/carrier_user/analysis/index.jsp"));
						subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.carrier",sessionData.getLanguage()), "admin/reports/carrier_user/carrier/carrier_report.jsp"));
						subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.roaming_etopups",sessionData.getLanguage()), "admin/reports/carrier_user/crossborder/roaming_report.jsp"));
					}		
					//<!--end reports submenu-->
				}
			}else if(!sessionData.isChangePassword()){
				menuOptions = new ArrayList<MenuOption>();
				String strAccessLevel = sessionData.getProperty("access_level");
				prepareMenu(sessionData, section, strDistChainType, deploymentType, strAccessLevel);
				prepareSubMenu(context, sessionData, section, strDistChainType, section_page, deploymentType, strAccessLevel);
				if (serverName.equals("localhost")){
					
					String s = sessionData.getProperty("datasource") + " - DT(" + DebisysConfigListener.getDeploymentType(context) + ") - CCT(" + DebisysConfigListener.getCustomConfigType(context) + ")";
					menuOptions.add(new MenuOption(s, "javascrip:void(0)", ""));
				}
				// Do as normal, not a carrier user
			}
		} //end if logged in	
	}
	
	/**
	 * @param sessionData
	 * @param section
	 * @param strDistChainType
	 * @param deploymentType
	 * @param strAccessLevel
	 */
	private void prepareMenu(SessionData sessionData, int section, String strDistChainType, String deploymentType, String strAccessLevel) {
		/* Menu Options*/
		
		menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.home",sessionData.getLanguage()), "admin/index.jsp", (section==1 ? "gcol":"")));
		
		//<!--end multi isos  Release 15.1--><!--customers-->
		if (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS)){ 
			//<!--multi isos  Release 15.1-->
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.manageiso",sessionData.getLanguage()), "admin/multiiso/multi.jsp", (section==10 ? "gcol":"")));
			//<!--end multi isos  Release 15.1--><!--customers-->
		} 
		//<!--end multi isos  Release 15.1-->
		//<!--customers-->
		if (!strAccessLevel.equals(DebisysConstants.MERCHANT) && !strAccessLevel.equals(DebisysConstants.REFERRAL)){
			String custUrl = "";
			if (strAccessLevel.equals(DebisysConstants.CARRIER)){
				custUrl = "admin/customers/iso.jsp?search=y";
			}else if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
				custUrl = "admin/customers/agents.jsp?search=y";
			}else if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
				custUrl = "admin/customers/reps.jsp?search=y";
			}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
				custUrl = "admin/customers/subagents.jsp?search=y";
			}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				custUrl = "admin/customers/reps.jsp?search=y";
			}else if (strAccessLevel.equals(DebisysConstants.REP)){
				custUrl = "admin/customers/merchants.jsp?search=y";
			}
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.customers",sessionData.getLanguage()), custUrl, (section==2 ? "gcol":"")));
			//<!--end multi isos  Release 15.1--><!--customers-->
			//<!--end customers tab-->
		}
		if (strAccessLevel.equals(DebisysConstants.MERCHANT)){
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.merchant_info",sessionData.getLanguage()), "admin/customers/merchants_info.jsp", (section==2 ? "gcol":"")));
			//<!--end multi isos  Release 15.1--><!--customers-->
		}
		if ( !strAccessLevel.equals(DebisysConstants.REFERRAL)){
			String refUrl = "";
			if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.CARRIER)){
				refUrl = "admin/transactions";
			}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
				refUrl = "admin/transactions/agents_transactions.jsp";
			}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				refUrl = "admin/transactions/subagents_transactions.jsp";
			}else if (strAccessLevel.equals(DebisysConstants.REP)){
				refUrl = "admin/transactions/reps_transactions.jsp";
			}else if (strAccessLevel.equals(DebisysConstants.MERCHANT)){
				refUrl = "admin/transactions/merchants_transactions.jsp";
			}
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.transactions",sessionData.getLanguage()), refUrl, (section==3 ? "gcol":"")));
			//<!--end multi isos  Release 15.1--><!--customers-->
		}

		try {
			if ( strAccessLevel.equals(DebisysConstants.ISO) || sessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS) || (sessionData.getUser().isIntranetUser() && 
							sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS)))
			{
				//<!--RatePlans-->
				if ( (sessionData.getUser().isIntranetUser() && sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
						|| sessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS))
				{
					menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.rateplan_manage",sessionData.getLanguage()), "admin/rateplans/rateplans.jsp", (section==7 ? "gcol":"")));
				}else if ( (sessionData.getUser().isIntranetUser() &&  sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
						|| sessionData.checkPermission(DebisysConstants.PERM_MANAGE_GLOBAL_UPDATE))
				{
					menuOptions.add(new MenuOption(Languages.getString("jsp.admin.rateplans.GlobalUpdate",sessionData.getLanguage()), "admin/rateplans/globalupdate.jsp", (section==7 ? "gcol":"")));
				}else if ( strAccessLevel.equals(DebisysConstants.ISO) ){
					menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.rate_plans",sessionData.getLanguage()), "admin/rateplans/index.jsp", (section==7 ? "gcol":"")));
				}
				//<!--end multi isos  Release 15.1--><!--customers-->
			}
		} catch (UserException e1) {
			e1.printStackTrace();
		}

		if(strAccessLevel.equals(DebisysConstants.ISO)){
			if (sessionData.checkPermission(DebisysConstants.PERM_BRANDINGS_SETUP)){
				int default_level = 0;
				if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
					default_level = Integer.parseInt(DebisysConstants.AGENT); 
				}
				if (((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))){
					if(default_level == 0) default_level = Integer.parseInt(DebisysConstants.SUBAGENT); 
				}
				if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
					if(default_level == 0) default_level = Integer.parseInt(DebisysConstants.REP);
				}
				sessionData.setProperty("default_level", String.valueOf(default_level) );
				//<!--products-->
				menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.brandings",sessionData.getLanguage()), "admin/brandings/assig_brandings.jsp", (section==12 ? "gcol":"")));
				//<!--end multi isos  Release 15.1--><!--customers-->
			}
		}
		if ( !strAccessLevel.equals(DebisysConstants.REFERRAL)){
			//<!--reports-->
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.reports",sessionData.getLanguage()), "admin/reports/index.jsp", (section==4 ? "gcol":"")));
			//<!--end multi isos  Release 15.1--><!--customers-->
		}
		//<!--reports-->
		// only show for domestic
		if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && !strAccessLevel.equals(DebisysConstants.REFERRAL) && !strAccessLevel.equals(DebisysConstants.CARRIER)){
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.achBilling",sessionData.getLanguage()), "admin/achBilling/summary.jsp", (section==16 ? "gcol":"")));
		}
		//<!--Tools-->
		//Previous if lines here were deleted because they were useless considering there's a direct assignment below
		menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.tools",sessionData.getLanguage()), "admin/tools/tools.jsp", (section==15 ? "gcol":"")));
						
		//<!--end multi isos  Release 15.1--><!--customers-->
		//<!--Promo/Bonus Tools Tab-->
		// Only let in ISOs that have the appropriate permissions to use this stuff.
		if(strAccessLevel.equals(DebisysConstants.ISO) && (sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLDS)
				|| sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLD_SMS) || sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_PHONE_REWARDS)
				|| sessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_PHONE_REWARDS_SMS)))
		{
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.bonusPromoTools",sessionData.getLanguage()), "admin/tools/index.jsp", (section==11 ? "gcol":"")));
			//<!--end multi isos  Release 15.1--><!--customers-->
		} // End of PromoBonus Tab
		if (!strAccessLevel.equals(DebisysConstants.CARRIER)) {
			//<!--help-->
			menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.help",sessionData.getLanguage()), "javascript:void(0);", "helpLink"));
			//<!--end multi isos  Release 15.1--><!--customers-->
			//<!--help-->
		}
		//<!--contact-->
		menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.contact_us",sessionData.getLanguage()), "admin/contact/index.jsp", (section==6 ? "gcol":"")));
		//<!--end multi isos  Release 15.1--><!--customers-->
		//<!--logout-->
		menuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.logout",sessionData.getLanguage()), "logout.jsp", ""));
		//<!--end multi isos  Release 15.1--><!--customers-->
			//<!--end multi isos  Release 15.1-->
		//<!--customers-->
	}

	/**
	 * @param context
	 * @param sessionData
	 * @param section
	 * @param strDistChainType
	 * @param section_page
	 * @param deploymentType
	 * @param strAccessLevel
	 */
	private void prepareSubMenu(ServletContext context, SessionData sessionData, int section, String strDistChainType, int section_page, String deploymentType, String strAccessLevel) {
		
		boolean isInternational = DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
									DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
		
		/* SubMenu Options*/
		
		subMenuOptions = new ArrayList<MenuOption>();
		
		if ((section==2 || section == 14) && (!strAccessLevel.equals(DebisysConstants.MERCHANT))){ 
			setOffSet(121);
			// User is ISO
			// User has 'Manage Carrier Users' permission
			if(strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_CARRIER_USERS)){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.carrier_user",sessionData.getLanguage()), "admin/customers/carrier_users.jsp?search=y"));
			}
			if (strAccessLevel.equals(DebisysConstants.CARRIER) ){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.iso",sessionData.getLanguage()), "admin/customers/iso.jsp?search=y"));
			}
			if (((strAccessLevel.equals(DebisysConstants.CARRIER) || strAccessLevel.equals(DebisysConstants.ISO)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.agents",sessionData.getLanguage()), "admin/customers/agents.jsp?search=y"));
				}
			if (((strAccessLevel.equals(DebisysConstants.CARRIER) || strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.sub_agents",sessionData.getLanguage()), "admin/customers/subagents.jsp?search=y"));
			}
			if (strAccessLevel.equals(DebisysConstants.CARRIER) || strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.representatives",sessionData.getLanguage()), "admin/customers/reps.jsp?search=y"));
			}
			if (!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.merchants",sessionData.getLanguage()), "admin/customers/merchants.jsp?search=y"));
			}
				
			/* PIN Cache */
			/*if (!strAccessLevel.equals(DebisysConstants.ISO)){// && sessionData.checkPermission(DebisysConstants.PERM_MANAGE_PINCACHE)) {
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.pincache",sessionData.getLanguage()), "admin/pincache/template.jsp"));
			}*/
		}
		
			//rateplan submenu
		if (section==7) {
			setOffSet(296);
			if (section_page == 3){
				int default_level = 0;
				if (strAccessLevel.equals(DebisysConstants.CARRIER)){
					default_level = Integer.parseInt(DebisysConstants.ISO); 
				}else if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
					default_level = Integer.parseInt(DebisysConstants.AGENT); 
				}else if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
					default_level = Integer.parseInt(DebisysConstants.REP); 
				}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
					default_level = Integer.parseInt(DebisysConstants.SUBAGENT);
				}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
					default_level = Integer.parseInt(DebisysConstants.REP);
				}else if (strAccessLevel.equals(DebisysConstants.REP)){
					default_level = Integer.parseInt(DebisysConstants.MERCHANT);
				}
				try {
					if ( sessionData.getUser().isIntranetUser() && sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS) ){
						default_level = Integer.parseInt(DebisysConstants.ISO);
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (UserException e) {
					e.printStackTrace();
				}
				sessionData.setProperty("default_level", String.valueOf(default_level) );
			}
			try {
				if (
					(sessionData.getUser().isIntranetUser() && sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					|| sessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS)){
					subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.rateplan_manage",sessionData.getLanguage()), "admin/rateplans/rateplans.jsp"));
				}
			} catch (UserException e) {
				e.printStackTrace();
			}
			try {
				if (
					(sessionData.getUser().isIntranetUser() && 
							sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
					|| sessionData.checkPermission(DebisysConstants.PERM_MANAGE_GLOBAL_UPDATE)){
					subMenuOptions.add(new MenuOption(Languages.getString("jsp.admin.rateplans.GlobalUpdate",sessionData.getLanguage()), "admin/rateplans/globalupdate.jsp"));
				}
			} catch (UserException e) {
				e.printStackTrace();
			}
			if ( strAccessLevel.equals(DebisysConstants.ISO) ){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.iso_rate_plans",sessionData.getLanguage()), "admin/rateplans/index.jsp"));
			}
			if (sessionData.checkPermission(DebisysConstants.PERM_RATEPLANS) && com.debisys.users.User.isRepRatePlanforTerminalCreationEnabled(sessionData, context)){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.rep_rate_plans",sessionData.getLanguage()), "admin/rateplans/rep_rates.jsp"));
			}
		}//end rateplan submenu
		
		//reports submenu
		if (section==4) {
			if (strAccessLevel.equals(DebisysConstants.ISO)){
				setOffSet(552);
			}else{
				setOffSet(121);
			}
			if ( !strAccessLevel.equals(DebisysConstants.CARRIER) ){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.transactions",sessionData.getLanguage()), "admin/reports/transactions/index.jsp"));
			}
			subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.analysis",sessionData.getLanguage()), "admin/reports/analysis/index.jsp"));
			// SW DBSY-569 - Added in Agent and Subagent
			if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.MERCHANT)) && deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.carrier",sessionData.getLanguage()), "admin/reports/carrier/index.jsp"));
			}
			// SW DBSY-569 - Added in Agent and Subagent
			if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.MERCHANT)) && deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.includes.menu.roaming_etopups",sessionData.getLanguage()), "admin/reports/crossborder/index.jsp"));
			}
			
			if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS) && isInternational ){
				subMenuOptions.add(new MenuOption(Languages.getString("jsp.admin.reports.scheduleReport.manageSchedule",sessionData.getLanguage()), "admin/reports/scheduleReports/manageschedule.jsp"));
			}
		}//end reports submenu
	}

	/**
	 * @return the subMenuOptions
	 */
	public ArrayList<MenuOption> getSubMenuOptions() {
		return subMenuOptions;
	}

	/**
	 * @param subMenuOptions the subMenuOptions to set
	 */
	public void setSubMenuOptions(ArrayList<MenuOption> subMenuOptions) {
		this.subMenuOptions = subMenuOptions;
	}

	/**
	 * @return the menuOptions
	 */
	public ArrayList<MenuOption> getMenuOptions() {
		return menuOptions;
	}

	/**
	 * @param menuOptions the menuOptions to set
	 */
	public void setMenuOptions(ArrayList<MenuOption> menuOptions) {
		this.menuOptions = menuOptions;
	}

	/**
	 * @return the offSet
	 */
	public int getOffSet() {
		return offSet;
	}

	/**
	 * @param offSet the offSet to set
	 */
	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}
}
