package com.debisys.presentation;

import com.debisys.brandings.Branding;
import com.debisys.exceptions.BrandingException;
import com.debisys.images.Image;
import com.debisys.presentation.html.*;
import com.debisys.users.SessionData;
import java.util.Vector;

/**
 * @author Juan Carlos Cruz
 *
 */
@SuppressWarnings("serial")
public class BrandingComponent extends HtmlComponent {

    public static final String COMMONS = "COMMONS";
    public static final String PCTERM2 = "PCTERM2";
    public static final String PCTERM4 = "PCTERM4";
    
	private Branding branding;
	
	private static final short MAX_LOGO_HEIGHT = 47;

	private static final short MAX_LOGO_WIDTH = 152;

	public static final int MAX_MEMORY_SIZE = 1048576;

	private static final String LOGO_SAMPLE = "/support/images/logo_sample.png";

        
        
	/**
	 */
	public BrandingComponent(Branding vbranding) {
            super("jsp.admin.branding", vbranding);                
	}
	
        @Override
	public void loadMetadata(Object vbranding){
		branding = (Branding)vbranding;                
                
		addField(new HtmlField("customConfigId", HtmlFieldType.HIDDEN));
                /*
		
                */              
	}

        private void loadControlByGroup(){
            String group = this.getValue("brandingGroup");
            if (group.equalsIgnoreCase( PCTERM2 ) ){       
                addField(new HtmlField("brandingGroup", HtmlFieldType.HIDDEN));
                addField(new HtmlField("Name", HtmlFieldType.TEXT, 0, 20));
                addField(new HtmlField("PCTermURL", HtmlFieldType.TEXT, 0, 256));
                addField(new HtmlField("PCTermSecureURL", HtmlFieldType.TEXT, 0, 256));
                addField(new HtmlField("AvailableCreditColor", HtmlFieldType.COLOR));
                addField(new HtmlField("BarCodePIN", HtmlFieldType.BOOL_CHECKBOX));
                addField(new HtmlField("BarCodeProduct", HtmlFieldType.BOOL_CHECKBOX));
                addField(new HtmlField("BarCodeTransaction", HtmlFieldType.BOOL_CHECKBOX));
                addField(new HtmlField("CurrencySymbol", HtmlFieldType.TEXT, 0, 1));
                addField(new HtmlField("Line1", HtmlFieldType.COLOR));
                addField(new HtmlField("Line2", HtmlFieldType.COLOR));
                addField(new HtmlField("MainLeftTextColor", HtmlFieldType.COLOR));
                addField(new HtmlField("PanelSupportLinkColor", HtmlFieldType.COLOR));
                addField(new HtmlField("PanelSupportLinkTextColor", HtmlFieldType.COLOR));
                addField(new HtmlField("SupportLinkTextColor", HtmlFieldType.COLOR));                                      
                addField(new HtmlField("TabCategoryListProductsButtonStartColor", HtmlFieldType.COLOR));
                addField(new HtmlField("TabCategoryListProductsButtonEndColor", HtmlFieldType.COLOR));
                addField(new HtmlField("TabCategoryListProductsButtonColorSHov", HtmlFieldType.COLOR));
                addField(new HtmlField("TabCategoryListProductsButtonColorEHover", HtmlFieldType.COLOR));
                addField(new HtmlField("NewsTextColor", HtmlFieldType.COLOR));
                addField(new HtmlField("NewsBackGroundColor", HtmlFieldType.COLOR));
                addField(new HtmlField("OptionSelectColor", HtmlFieldType.COLOR));
                addField(new HtmlField("TitleLeft", HtmlFieldType.COLOR));
                addField(new HtmlField("TitleSelectColor", HtmlFieldType.COLOR));
                addField(new HtmlField("TitleUnselectColor", HtmlFieldType.COLOR));
                addField(new HtmlField("ColorTextSubmitButtons", HtmlFieldType.COLOR));            
            } else if (group.equalsIgnoreCase( COMMONS ) ) {
                addField(new HtmlField("brandingGroup", HtmlFieldType.HIDDEN));
                addField(new HtmlField("Language", HtmlFieldType.SINGLE_SELECT, 0, new Vector<Vector<String>>() {
			{
				add(new Vector<String>() {
					{
						add("1");
						add("English");
					}
				});
				add(new Vector<String>() {
					{
						add("2");
						add("Spanish");
					}
				});
			}
		}));
		addField(new HtmlField("SupportPhone", HtmlFieldType.TEXT, 0, 50));
		addField(new HtmlField("SupportEmail", HtmlFieldType.TEXT, 150, 256));
		addField(new HtmlField("SupportSiteUrl", HtmlFieldType.TEXT, 150, 256));
		addField(new HtmlField("LogoImage", HtmlFieldType.IMAGE_FILE));
                
            }else if (group.equalsIgnoreCase( PCTERM4 ) ) {
                
                addField(new HtmlField("brandingGroup", HtmlFieldType.HIDDEN));
                addField(new HtmlField("NavbarBrand", HtmlFieldType.BOOL_CHECKBOX));
                addField(new HtmlField("BaseColor", HtmlFieldType.COLOR));
                addField(new HtmlField("ReverseColor", HtmlFieldType.COLOR));
                addField(new HtmlField("LogoImageWhite", HtmlFieldType.IMAGE_FILE));
                addField(new HtmlField("LogoImageBlack", HtmlFieldType.IMAGE_FILE));
                
            }
        }
        
        
        @Override
	public void loadDefaults() {	
            /*
            setValue("AvailableCreditColor", "#000000");
            setValue("BarCodePIN", "0");
            setValue("BarCodeProduct", "0");
            setValue("BarCodeTransaction", "0");
            setValue("CurrencySymbol", "$");
            setValue("Line1", "#000000");
            setValue("Line2", "#000000");
            setValue("NewsTextColor", "#000000");
            setValue("NewsBackGroundColor", "#000000");
            setValue("OptionSelectColor", "#000000");
            setValue("TitleLeft", "#000000");
            setValue("TitleSelectColor", "#000000");
            setValue("TitleUnselectColor", "#000000");
            setValue("Language", "0");		
            setValue("LogoImage_type", "BRAND");		
            */
	}
	
	public void loadFromDB() {            
                loadControlByGroup();
		clearValues();
		String customConfigId = branding.getCustomConfigId();
		try {
			branding.load();
			if (customConfigId != null && !customConfigId.equals("")) {
				setValue("customConfigId", customConfigId);
				setValue("Name", branding.getName());
				for (HtmlField p : metadata) {
					if (p.name.equals("customConfigId") 
                                                || p.name.equals("Name") 
                                                    || p.name.equals("brandingGroup") ) {                                            
						// do nothing
                                                System.out.println("***");
					} else if (p.name.contains("LogoImage")) {
						String val = branding.getProperty(p.name);
						if (val != null && !val.equals("")) {
							long imgid = Long.parseLong(val);
							Image im = new Image();
							im.setImageId(imgid);
							ImageComponent ic = new ImageComponent(im);
							ic.load(p, this);
							setValue("ImageId_type", "BRAND");
						} else {
							setValue(p.name, "");
						}
					} else {
						setValue(p.name, branding.getProperty(p.name));
					}
				}
			}
		} catch (BrandingException e) {
			e.printStackTrace();
		}
	}

	public boolean save() {
		try {
			branding.setName(getValue("Name"));
			if(branding.getCustomConfigId() == null || branding.getCustomConfigId().length() == 0){
				if(branding.existsBrading(branding.getName())){
					//log.error("BrandingComponet.save: Duplicated Name or CustomConfigId");
					errors.put("Name", "Duplicated Name.");
					return false;
				}else{
					branding.setCustomConfigId(branding.getName());
				}
			}
			if(validateFields()){
                                String group = this.getValue("brandingGroup");
                                boolean flagNoPCT2 = true;
                                if ( group.equalsIgnoreCase(PCTERM2) ){
                                    branding.setURL(getValue("PCTermURL"));
                                    branding.setSecureURL(getValue("PCTermSecureURL"));
                                    flagNoPCT2 = branding.save();
                                }
				if (flagNoPCT2) {
                                    loadControlByGroup();
					for (HtmlField p : metadata) {
						if (p.name.equals("customConfigId") || p.name.equals("Name") || p.name.startsWith("LogoImage_") ||
                                                        p.name.equals("brandingGroup")) {
							// do nothing
						} else if (p.name.contains("LogoImage")) {
							ImageComponent ic = new ImageComponent(new Image());
							long id = ic.save(p, this);
							if (id > 0) {
                                                            branding.saveProperty(p.name, String.valueOf(id));
							}
						} else {
							branding.saveProperty(p.name, getValue(p.name));
						}
					}
					return true;
				}
			}else{
				return false;
			}
		} catch (BrandingException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	protected String onGetImageField(HtmlField p, String value,SessionData sessionData) {
		return getImageField(p, value, LOGO_SAMPLE, MAX_LOGO_WIDTH, MAX_LOGO_HEIGHT, MAX_MEMORY_SIZE, false,sessionData);
	}

	@Override
	protected boolean onCheckImageField(HtmlField p){
		return checkImageSize(p.name, MAX_LOGO_WIDTH, MAX_LOGO_HEIGHT, MAX_MEMORY_SIZE);
	}

	@Override
	protected boolean onCheckDocumentField(HtmlField p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected String onGetDocumentField(HtmlField p, String value,SessionData sessionData) {
		// TODO Auto-generated method stub
		return null;
	}
}