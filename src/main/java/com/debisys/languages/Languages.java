package com.debisys.languages;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.emida.utils.dbUtils.TorqueHelper;

public class Languages extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static boolean inited = false;
    private static Logger cat = Logger.getLogger(Languages.class);
    /** Resource bundle. */
    private static ResourceBundle language_bundle = null;
    private static String DEFAULT_VALUE = "default";
    //private static String language = "english";  //default
    private static String language = "";  //default
    private static String codeLanguage = "0";  //default

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.languages.sql");
	
    //private static ServletContext servletContext;
	private static ResourceBundle language_budle_spanish=null;
	private static ResourceBundle language_budle_english=null;
	
    /**
     * @return 0 for english, 1 for spanish
     */
    public static String getCodeLanguage() {
		return codeLanguage;
	}


	@Override
	public void init() throws ServletException{
      cat.info("Initializing language file");
      if (inited) return;
        inited = true;
        try
        {
          //servletContext= this.getServletContext();
          language = DebisysConfigListener.getLanguage(this.getServletContext());
         // Languages.initialize("com.debisys.languages." + language);
          Languages.initialize();
          cat.info("Language initialized to " + language);
          cat.info("Languages initialized  ");
          
        } catch (Exception e) {
          cat.error("Language initialization failed!");
          throw new ServletException( e.toString() );
        }
    }


  /**
   * Initializes Language with the given resource file.
   *
   * @param strResourceFile
   */
 //public static void initialize(String strResourceFile)
 // {
 //   try
 //   {
 //     cat.debug(strResourceFile) ;
 //     language_bundle = ResourceBundle.getBundle(strResourceFile);
 //   }
 //   catch (Exception ex)
 //   {
 //     /* Just null out the missing bundle if that's the case. */
 //     System.out.println("exception");
 //     language_bundle = null;
 //   }
 // }
  /**
   * Initialize All Languages of the Support Site  
   */
  public static void initialize(){
    try
    {
	      String spanish = "com.debisys.languages.spanish";
	      String english = "com.debisys.languages.english";
	      cat.debug(spanish) ;
	      language_budle_spanish = ResourceBundle.getBundle(spanish);
	      cat.debug(english);
	      language_budle_english = ResourceBundle.getBundle(english);
	      //language bundle by default
	      System.out.println("language ** " + language);
	      language_bundle=ResourceBundle.getBundle("com.debisys.languages."+language);
    }
    catch (Exception ex)
    {
      /* Just null out the missing bundle if that's the case. */
	    	ex.printStackTrace();
      System.out.println("exception");
	      language_budle_spanish = null;
	      language_budle_english = null;
      language_bundle = null;
    }
  }

  /**
 * @return the language_budle_spanish
 */
public static ResourceBundle getLanguage_budle_spanish() {
	return language_budle_spanish;
}


/**
 * @param languageBudleSpanish the language_budle_spanish to set
 */
public static void setLanguage_budle_spanish(ResourceBundle languageBudleSpanish) {
	language_budle_spanish = languageBudleSpanish;
}


/**
 * @return the language_budle_english
 */
public static ResourceBundle getLanguage_budle_english() {
	return language_budle_english;
}


/**
 * @param languageBudleEnglish the language_budle_english to set
 */
public static void setLanguage_budle_english(ResourceBundle languageBudleEnglish) {
	language_budle_english = languageBudleEnglish;
}


/**
   * Gets a string based upon a resource key.
   *
   * @param strKey resource key in resources file
   * @return a string from the current locale's resource file
   */
//  public static String getString(String strKey)
//  {
//    String strValue = null;
//    boolean bFound = true;
//
//    try
//    {
//      strValue = language_bundle.getString(strKey);
//    }
//    catch (Exception ex)
//    {
//      bFound = false;
//    }

    /** Last resort, return the default value defined */
//    if (!bFound)
//    {
//      strValue = DEFAULT_VALUE;
//      cat.info("Key:" +strKey+ " does not exist in the languages file, please update.");
//    }

//    return strValue;

//  }

public static String getString(String strKey, String language)
  {
    String strValue = null;
    boolean bFound = true;

    try
    {
		if(language!=null && !language.equals(""))
		{
			if(language.equals("spanish")){
				 strValue = language_budle_spanish.getString(strKey);
			}
			else if(language.equals("english")){
				strValue = language_budle_english.getString(strKey);
			}
			else if(language.equals("0")){
		      strValue = language_bundle.getString(strKey);
		    }
			else if(language.equals("1")){
			      strValue = language_budle_english.getString(strKey);
			}
		}
		else
		{
			strValue = language_bundle.getString(strKey);
		}
    }
    catch (Exception ex)
    {
      bFound = false;
    }

    /** Last resort, return the default value defined */
    if (!bFound)
    {
      strValue = DEFAULT_VALUE;
      cat.info("Key:" +strKey+ " does not exist in the languages file, please update.");
    }

    return strValue;

  }


  /**
   * Version of getString that handles formatting messages with variables
   * using java.text.MessageFormat.
   *
   * //pass arguments like this
   * Object[] arguments = {string1, string2};
   * //this will return a string with the variables added
   * getString("whatever", arguments)
   *
   * @param strKey
   * @param objArgs
   */
  /*public static String getString(String strKey, Object[] objArgs)
  {
    return MessageFormat.format(getString(strKey), objArgs);
  }*/
  
  public static String getString(String strKey, Object[] objArgs, String language)
  {
    return MessageFormat.format(getString(strKey,language), objArgs);
  }

  public static String getLanguage()
  {
    return language;
  }


	/**
	 * @param SessionData  
	 */
	public static void reloadLanguage(String language,SessionData SessionData) throws ServletException{
	      cat.info("Initializing language file");
	       try
	        {
	          if(language!=null && !language.equals("")){
	        	  //ResourceBundle language_bundle_cha=null;
	        	  /*language_bundle_cha = */ResourceBundle.getBundle("com.debisys.languages." + language);
	        	  //SessionData.setProperty("language_sesion", strValue)
	        	 // Languages.initialize("com.debisys.languages." + language);  
	        	  //servletContext.setAttribute("language_change", language);
	          }
	          
	          /*cat.info("Language initialized to " + language);
	          
	          if (language=="spanish"){
	        	  codeLanguage = "1";
	          }
	          else{
	        	  codeLanguage = "0";
	          }*/
	          
	        } catch (Exception e) {
	          cat.error("Language initialization failed!");
	          throw new ServletException( e.toString() );
	        }
    }
	
public static Vector<Vector<String>> searchLanguages(){
	
    Connection dbConn = null;
    PreparedStatement pstmt = null;

    Vector<Vector<String>> vecLanguages = new Vector<Vector<String>>();
    ResultSet rs = null;

    try
    {
      dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new Exception("Can't get database connection");
      }
      
      String strSQL = Languages.sql_bundle.getString("searchLanguages");
      pstmt = dbConn.prepareStatement(strSQL);
      rs = pstmt.executeQuery();
      while(rs.next()){
      	  Vector<String> vecTemp = new Vector<String>();
    	  vecTemp.add(rs.getString("id"));
          vecTemp.add(rs.getString("language"));
          vecTemp.add(rs.getString("languageesp"));
          vecLanguages.add(vecTemp);
      }
      
    }
    catch (Exception e)
    {
      cat.error("Error during searchLanguages", e);
      
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
	return vecLanguages;
}


@Override
public  void doGet(HttpServletRequest req, HttpServletResponse res)
{
	String language = req.getParameter("lan");
	String page = req.getParameter("page");
	
	if(language!= null && !language.equals("") && page !=null && !page.equals("")){
		try {
			SessionData sessionSupportSite = (SessionData) req.getSession().getAttribute("SessionData");
			sessionSupportSite.setLanguage(language);
	        res.sendRedirect(page);
		} //catch (ServletException e) {
			//e.printStackTrace();
		//}
		catch (IOException e) {
			e.printStackTrace();
}
}
}
}


