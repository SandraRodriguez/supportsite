/**
 * 
 */
package com.debisys.terminalTracking;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.text.*;
import com.debisys.languages.Languages;

import com.debisys.users.SessionData;
import com.debisys.utils.StringUtil;
import com.debisys.utils.DbUtil;

import org.apache.torque.Torque;
/**
 * @author fernandob
 * Sim cards specific info 
 */
public class CSimCard extends CTerminalPart 
{

	// ------------------------------------  attributes ------------------------------------------------
	private String m_MobileSuscriberId; // IMSI
	private String m_PhoneNumber;
	private CProvider m_Provider;
	private String m_ActivationDate;
	private String m_ContractNumber;
	private String m_ContractDate;
	private long m_RowNumber;
	private String m_ChangeReason;
	
	// following two properties are for sorting model implementation
	private static String sort = "";
	private static String col = "";
	
	// filtering properties
	private String mobileSubscriberIdH="";
	private String lotNumberH="";
	private String phoneNumberH="";
	private String carrierIdH="";
	private String statusIdH="";
	private String startDateH="";
	private String endDateH="";
	

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_MobileSuscriberId
	 */
	public String getMobileSuscriberId() 
	{
		return m_MobileSuscriberId;
	}
	/**
	 * @param mobileSuscriberId the m_MobileSuscriberId to set
	 */
	public void setMobileSuscriberId(String mobileSuscriberId) 
	{
		m_MobileSuscriberId = mobileSuscriberId;
	}
	/**
	 * @return the m_PhoneNumber
	 */
	public String getPhoneNumber() 
	{
		return m_PhoneNumber;
	}
	/**
	 * @param phoneNumber the m_PhoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) 
	{
		m_PhoneNumber = phoneNumber;
	}
	/**
	 * @return the m_Carrier
	 */
	public CProvider getProvider() 
	{
		return m_Provider;
	}
	/**
	 * @param carrier the m_Carrier to set
	 */
	public void setProvider(CProvider provider) 
	{
		m_Provider = provider;
	}
	/**
	 * @return the m_ActivationDate
	 */
	public String getActivationDate(boolean viewFormat) 
	{
		String retValue =  m_ActivationDate;
	
		if(viewFormat)
		{
			try
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
				SimpleDateFormat viewDateFormat = new SimpleDateFormat(DEFAULT_VIEW_FORMAT);
				Calendar tmpDate = Calendar.getInstance();
				tmpDate.setTime(dateFormat.parse(m_ActivationDate));
				retValue = viewDateFormat.format(tmpDate.getTime());
			}
			catch(Exception localException)
			{
				retValue =  m_ActivationDate;
			}
		}
		return retValue;
	}
	/**
	 * @param activationDate the m_ActivationDate to set
	 */
	public void setActivationDate(String activationDate) 
	{
		m_ActivationDate = activationDate;
	}
	/**
	 * @return the m_ContractNumber
	 */
	public String getContractNumber() 
	{
		return m_ContractNumber;
	}
	/**
	 * @param contractNumber the m_ContractNumber to set
	 */
	public void setContractNumber(String contractNumber) 
	{
		m_ContractNumber = contractNumber;
	}
	/**
	 * @return the m_ContractDate
	 */
	public String getContractDate(boolean viewFormat) 
	{
		String retValue =  m_ContractDate;
		
		if(viewFormat)
		{
			try
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
				SimpleDateFormat viewDateFormat = new SimpleDateFormat(DEFAULT_VIEW_FORMAT);
				Calendar tmpDate = Calendar.getInstance();
				tmpDate.setTime(dateFormat.parse(m_ContractDate));
				retValue = viewDateFormat.format(tmpDate.getTime());
			}
			catch(Exception localException)
			{
				retValue =  m_ContractDate;
			}
		}
		return retValue;
	}
	/**
	 * @param contractDate the m_ContractDate to set
	 */
	public void setContractDate(String contractDate) 
	{
		m_ContractDate = contractDate;
	}
	
	
	/**
	 * @return the m_RowNumber
	 */
	public long getRowNumber() 
	{
		return m_RowNumber;
	}
	/**
	 * @param rowNumber the m_RowNumber to set
	 */
	public void setRowNumber(long rowNumber) 
	{
		m_RowNumber = rowNumber;
	}
	
	public String getKey()
	{
		String retValue = String.valueOf(m_PartId);
		return retValue;
	}
	
	/**
	 * @return the m_ChangeReason
	 */
	public String getChangeReason() 
	{
		return m_ChangeReason;
	}
	/**
	 * @param changeReason the m_ChangeReason to set
	 */
	public void setChangeReason(String changeReason) 
	{
		m_ChangeReason = changeReason;
	}
	
	public String getSort() {
		return StringUtil.toString(col);
	}
	
	public void setSort(String _sort) {
		sort = _sort;
	}
	
	public String getCol() {
		return StringUtil.toString(col);
	}
	
	public void setCol(String _col) {
		col = _col;
	}
	
	public String getMobileSubscriberIdH() {
		return mobileSubscriberIdH;
	}
	
	public void setMobileSubscriberIdH(String _f_IMSI) {
		mobileSubscriberIdH = _f_IMSI;
	}
	
	public String getLotNumberH() {
		return lotNumberH;
	}
	
	public void setLotNumberH(String _f_Lot) {
		lotNumberH = _f_Lot;
	}
	
	public String getPhoneNumberH() {
		return phoneNumberH;
	}
	
	public void setPhoneNumberH(String _f_PhoneNumber) {
		phoneNumberH = _f_PhoneNumber;
	}
	
	public String getCarrierIdH() {
		return carrierIdH;
	}
	
	public void setCarrierIdH(String _f_Provider) {
		carrierIdH = _f_Provider;
	}
	
	public String getStatusIdH() {
		return statusIdH;
	}
	
	public void setStatusIdH(String _f_Status) {
		statusIdH = _f_Status;
	}
	
	public String getStartDateH() {
		return startDateH;
	}
	
	public void setStartDateH(String _f_StartDate) {
		startDateH = _f_StartDate;
	}
	
	public String getEndDateH() {
		return endDateH;
	}
	
	public void setEndDateH(String _f_EndDate) {
		endDateH = _f_EndDate;
	}
	
	// ------------------------------------  methods  --------------------------------------------------
	/**
	 * @param simCardId Part identification
	 * @return true if part load could be done
	 */
	public boolean loadSimCard(long simCardId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPart = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPart"));
            ps.setLong(1, simCardId);
            ps.setNull(2, java.sql.Types.INTEGER);
            ps.setNull(3, java.sql.Types.INTEGER);
            ps.setNull(4, java.sql.Types.INTEGER);
            rsPart = ps.executeQuery();
            if((rsPart != null) && (rsPart.next()))
            {
            	this.m_PartId = rsPart.getLong("partId");
            	this.m_PartType = new CTerminalPartType();
            	this.m_PartType.setPartTypeId(rsPart.getLong("partType"));
            	this.m_PartType.setPartTypeCode(rsPart.getString("partTypeCode"));
            	this.m_PartType.setPartDescription(rsPart.getString("partTypeDescription"));
            	this.m_MobileSuscriberId = (rsPart.getString("mobileSuscriberId") != null)?rsPart.getString("mobileSuscriberId"):"";
            	this.m_PhoneNumber = (rsPart.getString("phoneNumber") != null)?rsPart.getString("phoneNumber"):"";
            	this.m_Provider = new CProvider();
            	this.m_Provider.loadProvider(rsPart.getLong("providerId"));
            	SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
            	this.m_ActivationDate = rsPart.getString("activationDate");
            	this.m_LotNumber = (rsPart.getString("lotNumber") != null)?rsPart.getString("lotNumber"):"";
            	this.m_ContractNumber = (rsPart.getString("contractNumber") != null)?rsPart.getString("contractNumber"):"";
            	this.m_ContractDate = rsPart.getString("contractDate");
            	this.m_Status = new CTerminalPartStatus();
            	this.m_Status.setPartStatusId(rsPart.getLong("currentStatus"));
            	this.m_Status.setPartStatusCode(rsPart.getString("statusCode"));
            	this.m_Status.setPartStatusDescription(rsPart.getString("statusDescription"));
            	this.m_LastChangeStatusDate = Calendar.getInstance();
            	this.m_LastChangeStatusDate.setTime(dateFormat.parse(rsPart.getString("lastChangeStatusDate")));
            	this.m_RowNumber = 1;
            	retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsPart != null )
    		{
    			try { rsPart.close(); }
    			catch (Exception ex) {}
    		}
    		rsPart = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	
	
	/**
	 * @param mobileSuscriberId 	Use -1 if null
	 * @param phoneNumber			Use -1 if null
	 * @param lotNumber				Use -1 if null
	 * @param carrierId				Use -1 if null
	 * @param statusId				Use -1 if null
	 * @param startAdmitanceDate	Start limit to get parts
	 * @param endAdmitanceDate		End limit to get parts
	 * @param startRecordIndex		Use -1 if null WEB paging start offset
	 * @param pageLength			Use -1 if null WEB paging page length. if null default is 100
	 * @return An array with the parts found when using the parameters given. Those records are paged
	 */
	public CTerminalPart[] getPartList(long mobileSuscriberId, long phoneNumber, long lotNumber, 
			long carrierId, long statusId, String startAdmitanceDate, String endAdmitanceDate, 
			int intRecordsPerPage, int intPageNumber) {//long startRecordIndex, long pageLength
		CTerminalPart[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPart = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		ArrayList<CSimCard> tmpList = new ArrayList<CSimCard>();
		
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() ) {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null) {
	          throw new Exception("Can't get database connection");
	        }
	        
	        String strSQL = sql_bundle.getString("getPartsList");
            
	        // Configure prepared statement parameters dynamically
	        java.util.Hashtable<String, Integer> selectFilters = new java.util.Hashtable<String, Integer>();
	        int filterIndex = 1;
	        if (mobileSuscriberId != -1 || (mobileSubscriberIdH != null && !mobileSubscriberIdH.equals("") && !mobileSubscriberIdH.equals("-1"))) {
	        	strSQL += (selectFilters.isEmpty()? " where": " and") + " mobileSuscriberId=?";
	        	selectFilters.put("mobileSuscriberId", new Integer(filterIndex++));
	        }
	        
	        if (phoneNumber != -1 || (phoneNumberH != null && !phoneNumberH.equals("") && !phoneNumberH.equals("-1"))) {
	        	strSQL += (selectFilters.isEmpty()? " where": " and") + " phoneNumber=?";
	        	selectFilters.put("phoneNumber", new Integer(filterIndex++));
	        }
	        
	        if (lotNumber != -1 || (lotNumberH != null && !lotNumberH.equals("") && !lotNumberH.equals("-1"))) {
	        	strSQL += (selectFilters.isEmpty()? " where": " and") + " lotNumber=?";
	        	selectFilters.put("lotNumber", new Integer(filterIndex++));
	        }
	        
	        if ((carrierId != -1 && carrierId != 0) || (carrierIdH != null && !carrierIdH.equals("") && !carrierIdH.equals("0"))) {
	        	strSQL += (selectFilters.isEmpty()? " where": " and") + " providerId=?";
	        	selectFilters.put("carrierId", new Integer(filterIndex++));
	        }
	        
	        if ((statusId != -1 && statusId != 0) || (statusIdH != null && !statusIdH.equals("") && !statusIdH.equals("0"))) {
	        	strSQL += (selectFilters.isEmpty()? " where": " and") + " currentStatus=?";
	        	selectFilters.put("statusId", new Integer(filterIndex++));
	        }
	        
	        if (((startAdmitanceDate != null) && (!startAdmitanceDate.equals(""))) || (startDateH != null && !startDateH.equals("") && !startDateH.equals("-1"))) {
	        	strSQL += (selectFilters.isEmpty()? " where": " and") + " activationDate>=?";
	        	selectFilters.put("startAdmitanceDate", new Integer(filterIndex++));
	        }
	        
	        if (((endAdmitanceDate != null) && (!endAdmitanceDate.equals(""))) || (endDateH != null && !endDateH.equals("") && !endDateH.equals("-1"))) {
	        	strSQL += (selectFilters.isEmpty()? " where": " and") + " activationDate<=?";
	        	selectFilters.put("endAdmitanceDate", new Integer(filterIndex++));
	        }
	        
	        // config the sorting
            int intCol = 0;
            int intSort = 0;
            try {
              if (col != null && sort != null && !col.equals("") && !sort.equals("")) {
                intCol = Integer.parseInt(col);
                intSort = Integer.parseInt(sort);
              }
            } catch (NumberFormatException nfe) {
              intCol = 0;
              intSort = 0;
            }
            
            String strSort = (intSort == 2)? "DESC": "ASC";            
            String strCol = "";
            switch (intCol) {
	            case 1: strCol = "partId"; break;
	            case 2: strCol = "partTypeDescription"; break;
	            case 3: strCol = "mobileSuscriberId"; break;
	            case 4: strCol = "phoneNumber"; break;
	            case 5: strCol = "providerId"; break;
	            case 6: strCol = "activationDate"; break;
	            case 7: strCol = "lotNumber"; break;
	            case 8: strCol = "contractNumber"; break;
	            case 9: strCol = "contractDate"; break;
	            case 10: strCol = "statusDescription"; break;
	            case 11: strCol = "lastChangeStatusDate"; break;
	            default: strCol = "RowNumber"; break;
            }
            
            strSQL += " order by " + strCol + " " + strSort;
            //logger.debug("*** SQL sentence for SIM inventory search: "+ strSQL);
            ps = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            
	        if (mobileSuscriberId != -1 || (mobileSubscriberIdH != null && !mobileSubscriberIdH.equals("") && !mobileSubscriberIdH.equals("-1"))) {
	        	ps.setString(selectFilters.get("mobileSuscriberId"), mobileSubscriberIdH);
	        	logger.debug("*** mobileSuscriberId must be set: "+selectFilters.get("mobileSuscriberId")+", "+mobileSubscriberIdH);
	        }
	        
	        if (phoneNumber != -1 || (phoneNumberH != null && !phoneNumberH.equals("") && !phoneNumberH.equals("-1"))) {
	        	ps.setString(selectFilters.get("phoneNumber"), phoneNumberH);
	        	logger.debug("*** phoneNumber must be set: "+selectFilters.get("phoneNumber")+", "+phoneNumberH);
	        }
	        
	        if (lotNumber != -1 || (lotNumberH != null && !lotNumberH.equals("") && !lotNumberH.equals("-1"))) {
	        	ps.setString(selectFilters.get("lotNumber"), lotNumberH);
	        	logger.debug("*** lotNumber must be set: "+selectFilters.get("lotNumber")+", "+lotNumberH);
	        }
	        
	        if ((carrierId != -1 && carrierId != 0) || (carrierIdH != null && !carrierIdH.equals("") && !carrierIdH.equals("0"))) {
	        	ps.setString(selectFilters.get("carrierId"), carrierIdH);
	        	logger.debug("*** carrierId must be set: "+selectFilters.get("carrierId")+", "+carrierIdH);
	        }
	        
	        if ((statusId != -1 && statusId != 0) || (statusIdH != null && !statusIdH.equals("") && !statusIdH.equals("0"))) {
	        	ps.setString(selectFilters.get("statusId"), statusIdH);
	        	logger.debug("*** statusId must be set: "+selectFilters.get("statusId")+", "+statusIdH);
	        }
	        
	        if (((startAdmitanceDate != null) && (!startAdmitanceDate.equals(""))) || (startDateH != null && !startDateH.equals("") && !startDateH.equals("-1"))) {
	        	ps.setString(selectFilters.get("startAdmitanceDate"), startDateH);
	        	logger.debug("*** startAdmitanceDate must be set: "+selectFilters.get("startAdmitanceDate")+", "+startDateH);
	        }
	        
	        if (((endAdmitanceDate != null) && (!endAdmitanceDate.equals(""))) || (endDateH != null && !endDateH.equals("") && !endDateH.equals("-1"))) {
	        	ps.setString(selectFilters.get("endAdmitanceDate"), endDateH);
	        	logger.debug("*** endAdmitanceDate must be set: "+selectFilters.get("endAdmitanceDate")+", "+endDateH);
	        }
	        
	        logger.debug("*** SQL sentence for SIM inventory search: "+ strSQL);
            rsPart = ps.executeQuery();
            
            //2. Find out how many records did the query return (and make sure to return it)
            int intRecordCount = DbUtil.getRecordCount(rsPart);            
            
            //3. Calculate row number to position cursor and #records to return based on a parameter
            
            int rowNumber = DbUtil.getRowNumber(rsPart, intRecordsPerPage, intRecordCount, intPageNumber);
            logger.debug("*** calculating row position in the resultset. RecordsPerPage=" + intRecordsPerPage + " RecordCount=" + intRecordCount + " PageNumber=" + intPageNumber + " rowAssigned=" + rowNumber);
            rsPart.absolute(rowNumber);
            
            // Get Detail
            // 4. Loop within limits until max records per page reached, or, end of resultset. In the meantime, add results to the returnable array.
        	for (int i = 0; i < intRecordsPerPage; i++) {
        		CSimCard newSimCard = new CSimCard();
            	newSimCard.m_PartId = rsPart.getLong("partId");
            	newSimCard.m_PartType = new CTerminalPartType();
            	newSimCard.m_PartType.setPartTypeId(rsPart.getLong("partType"));
            	newSimCard.m_PartType.setPartTypeCode(rsPart.getString("partTypeCode"));
            	newSimCard.m_PartType.setPartDescription(rsPart.getString("partTypeDescription"));
            	newSimCard.m_MobileSuscriberId = (rsPart.getString("mobileSuscriberId") != null)?rsPart.getString("mobileSuscriberId"):"";
            	newSimCard.m_PhoneNumber = (rsPart.getString("phoneNumber") != null)? rsPart.getString("phoneNumber"):"";
            	newSimCard.m_Provider = new CProvider();
            	newSimCard.m_Provider.loadProvider(rsPart.getLong("providerId"));
            	newSimCard.m_ActivationDate = rsPart.getString("activationDate");
            	newSimCard.m_LotNumber = (rsPart.getString("lotNumber") != null)?rsPart.getString("lotNumber"):"";
            	newSimCard.m_ContractNumber = rsPart.getString("contractNumber");
            	newSimCard.m_ContractDate = rsPart.getString("contractDate");
            	newSimCard.m_Status = new CTerminalPartStatus();
            	newSimCard.m_Status.setPartStatusId(rsPart.getLong("currentStatus"));
            	newSimCard.m_Status.setPartStatusCode(rsPart.getString("statusCode"));
            	newSimCard.m_Status.setPartStatusDescription(rsPart.getString("statusDescription"));
            	newSimCard.m_LastChangeStatusDate = Calendar.getInstance();
            	newSimCard.m_LastChangeStatusDate.setTime(dateFormat.parse(rsPart.getString("lastChangeStatusDate")));
            	newSimCard.m_RowNumber = rsPart.getLong("RowNumber");
            	newSimCard.m_TotalRecords = intRecordCount;
            	tmpList.add(newSimCard);
            	if (!rsPart.next()) {
            		break;
            	}
        	} // end while more results
        	retValue = new CTerminalPart[tmpList.size()];
        	retValue = tmpList.toArray(retValue);
        	tmpList.clear();
        	tmpList = null;
		} catch(Exception localException) {
			logger.error(localException.toString());
			retValue = null;
		} finally {
    		if ( rsPart != null ) {
    			try { 
    				rsPart.close(); 
    			} catch (Exception ex) {
    				logger.error(ex.getMessage());
    			}
    		}
    		rsPart = null;
    		if ( ps != null ) {
    			try { 
    				ps.close(); 
    			} catch (Exception ex) {
    				logger.error(ex.getMessage());
    			}
    		}
    		ps = null;
    		if ( dbConn != null ) {
    			try { 
    				dbConn.close(); 
    			} catch (Exception ex) {
    				logger.error(ex.getMessage());
    			}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	

	
	/**
	 * @param userId	from password table
	 * @return			True if new part could be inserted
	 * Insertion fields are obtained from class attributes, they must be filled first
	 */
	public boolean insertNewPart(String userId,SessionData sessionData)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("insertPart"));
	        ps.setLong(1, this.m_PartType.getPartTypeId());
	        ps.setString(2, this.m_MobileSuscriberId);
			if(this.m_PhoneNumber.equals(""))
			{
				ps.setNull(3, java.sql.Types.VARCHAR);
			}
			else
			{		
				ps.setString(3, this.m_PhoneNumber);
			}	        
	        ps.setLong(4, this.m_Provider.getProviderId());
	        ps.setString(5, this.m_ActivationDate);
	        ps.setString(6, this.m_ActivationDate);
			if((this.m_LotNumber == null) || (this.m_LotNumber.trim().equals("")))
			{
				ps.setNull(7, java.sql.Types.VARCHAR);
			}
			else
			{		
				ps.setString(7, this.m_LotNumber);
			}
			if((this.m_ContractNumber == null) || (this.m_ContractNumber.trim().equals("")))
			{
				ps.setNull(8, java.sql.Types.VARCHAR);
			}
			else
			{		
				ps.setString(8, this.m_ContractNumber);
			}
	        ps.setString(9, this.m_ContractDate);
	        ps.setLong(10, this.m_Status.getPartStatusId());
	        ps.setString(11, dateFormat.format(Calendar.getInstance().getTime()));
            ps.executeUpdate();
            Long partId = CSimCard.partExists(this.m_PartType.getPartTypeId(), this.m_MobileSuscriberId);
            if(partId != null)
            {
            	this.m_PartId = partId.longValue();
            	CTerminalPart.insertPartStatusChange(this.m_PartId, Calendar.getInstance(), this.m_Status.getPartStatusId(), userId, Languages.getString("jsp.admin.transactions.inventorySimsInsertReason",sessionData.getLanguage()));
            	retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;		
	}

	
	/**
	 * @return True if part could be updated. 
	 * Update fields are obtained from class attributes, they must be filled first
	 */
	public boolean updatePart(String userId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        CSimCard originalCard = new CSimCard();
	        if(originalCard.loadSimCard(this.m_PartId))
	        {
	        	// If imsi change but there is no other part duplicated or if imsi is not changed the update is allowed
		        ps = dbConn.prepareStatement(sql_bundle.getString("updatePart"));
		        ps.setLong(1, this.m_PartType.getPartTypeId());
		        ps.setString(2, this.m_MobileSuscriberId);
		        ps.setString(3, this.m_PhoneNumber);
		        ps.setLong(4, this.m_Provider.getProviderId());
		        ps.setString(5, this.m_ActivationDate);
		        ps.setString(6, this.m_ActivationDate);
		        ps.setString(7, this.m_LotNumber);
		        ps.setString(8, this.m_ContractNumber);
		        ps.setString(9, this.m_ContractDate);
		        ps.setLong(10, this.m_Status.getPartStatusId());
		        ps.setString(11, dateFormat.format(Calendar.getInstance().getTime()));
		        ps.setLong(12, this.m_PartId);
		        ps.executeUpdate();
		        if(this.m_Status.getPartStatusId() != originalCard.getStatus().getPartStatusId())
		        {
		        	CSimCard.insertPartStatusChange(this.m_PartId, Calendar.getInstance(), this.getStatus().getPartStatusId(), userId, this.m_ChangeReason);
		        }
	            retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;		
	}

	
	/**
	 * @param partTypeId			This two filed are part of the part primary key
	 * @param mobileSubscriverId	
	 * @return						True if a part with those values is found
	 */
	public static Long partExists(long partTypeId, String mobileSubscriverId)
	{
		Long retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPart = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("partExists"));
            ps.setLong(1, partTypeId);
            ps.setString(2, mobileSubscriverId);
            rsPart = ps.executeQuery();
            if((rsPart != null) && (rsPart.next()))
            {
            	retValue = new Long(rsPart.getLong("partId"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPart != null )
    		{
    			try { rsPart.close(); }
    			catch (Exception ex) {}
    		}
    		rsPart = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;

	}
	
	
	
	/**
	 * @return	A list of current part changes
	 */
	public CPartChange[] getPartChanges()
	{
		CPartChange[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPartChanges = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		ArrayList<CPartChange> tmpList = new ArrayList<CPartChange>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPartStatusChanges"));
	        ps.setLong(1, this.m_PartId);	        
            rsPartChanges = ps.executeQuery();
            if((rsPartChanges != null))
            {
            	while(rsPartChanges.next())
            	{
            		CPartChange newPartChange = new CPartChange();
            		newPartChange.setPartId(this.m_PartId);
                        Date fechaCambio = dateFormat.parse(rsPartChanges.getString("lastChangeDate"));
                        newPartChange.setLastChangeDate(fechaCambio);
            		newPartChange.setStatusId(rsPartChanges.getLong("newStatusId"));
            		newPartChange.setStatusDescription(rsPartChanges.getString("statusDescription"));
            		newPartChange.setChangeReason(rsPartChanges.getString("changeReason"));
            		newPartChange.setUserId(rsPartChanges.getString("userId"));
	            	tmpList.add(newPartChange);
            	}
            	retValue = new CPartChange[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPartChanges != null )
    		{
    			try { rsPartChanges.close(); }
    			catch (Exception ex) {}
    		}
    		rsPartChanges = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	/**
	 * @return	True if current phone number is being used by other part
	 */
	public static long getPhonesInUse(String phoneNumber)
	{
		long retValue = -1;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPart = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getDuplicatePhoneUse"));
            ps.setString(1, phoneNumber);
            rsPart = ps.executeQuery();
            if((rsPart != null) && (rsPart.next()))
            {
            	retValue = rsPart.getLong("phoneCount");
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
		}	
    	finally
    	{
    		if ( rsPart != null )
    		{
    			try { rsPart.close(); }
    			catch (Exception ex) {}
    		}
    		rsPart = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	
	
	
	
	
}
