package com.debisys.terminalTracking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.StringUtil;

public class CFileLoad 
{
	// ------------------------------------  constants -------------------------------------------------
	public static final int FIELDS_PER_PART = 7;
	public static final int DEFAULT_ALPHA_LENGHT = 50;

	// ------------------------------------  attributes ------------------------------------------------

	// Attributes for terminal validation
	private static HashMap<String, CCommunicationType> communicationTypes;
	private static HashMap<String, CTerminalBrand> terminalBrands;
	private static HashMap<String, CApplicationType> applicationTypes;
	private static HashMap<String, String> licenseTemp = new HashMap<String, String>();
	private static HashMap<String, String> imeiTemp = new HashMap<String, String>();
	// this attribute is going to be for keep the temporal serial number
	private static Logger logger = Logger.getLogger(CPhysicalTerminal.class);
	// Attributes for part validation
	private static Hashtable<String, CTerminalPartType>partTypes;
	private static Hashtable<String, CProvider>partProviders;
	private static Hashtable<String, String>loadedIMSIs = new Hashtable<String, String>();
	private static Hashtable<String, String>loadedPhones = new Hashtable<String, String>();
	
	// ------------------------------------  methods  --------------------------------------------------


	/*
	 * This method made a load data from the database to have this in memory for
	 * performance in the moment to find any information about terminal
	 */
	private static boolean loadTerminalsBasicData() {
		CTerminalBrand[] tb = CTerminalBrand.getBrandList();
		boolean result = true;
		communicationTypes = new HashMap<String, CCommunicationType>();
		terminalBrands = new HashMap<String, CTerminalBrand>();
		applicationTypes = new HashMap<String, CApplicationType>();

		for (int i = 0; i < tb.length; i++) {
			CTerminalBrand vbrand = (CTerminalBrand) tb[i];
			if (vbrand.loadBrandModels()) {
				terminalBrands.put(vbrand.getBrandName().toUpperCase(), vbrand);
			} else {
				result = false;
				break;
			}
		}
		// application Types
		CApplicationType[] at = CApplicationType.getAllApplicationTypes();
		for (int i = 0; i < at.length; i++) {
			CApplicationType vApplicationType = (CApplicationType) at[i];
			applicationTypes.put(vApplicationType.getApplicationTypeCode()
					.toUpperCase(), vApplicationType);
		}
		// comunication Types
		CCommunicationType[] ct = CCommunicationType.getAllCommunicationTypes();
		for (int i = 0; i < ct.length; i++) {
			CCommunicationType vCCommunicationType = (CCommunicationType) ct[i];
			communicationTypes.put(vCCommunicationType
					.getCommunicationTypeCode(), vCCommunicationType);
		}
		return result;
	}

	
	/**
	 * @return True if part types and providers could be loaded from DB.
	 * Those lists will be used for part files record validation 
	 */
	private static boolean loadPartsBasicData() 
	{
		boolean retValue = false;
		try
		{
			// Clear resources
			if(partTypes != null)
			{
				partTypes.clear();
				partTypes = null;
			}
			if(partProviders != null)
			{
				partProviders.clear();
				partProviders = null;
			}
			// Object creation
			partTypes = new Hashtable<String,CTerminalPartType>();
			partProviders = new Hashtable<String, CProvider>();
			// Obtain information
			CTerminalPartType[] partTypeList = CTerminalPartType.getAllTerminalPartTypes();
			CProvider[] providerList = CProvider.getProviderList();
			// Assign information
			for(CTerminalPartType currentPartType:partTypeList)
			{
				partTypes.put(currentPartType.getPartTypeCode(), currentPartType);
			}
			for(CProvider currentProvider:providerList)
			{
				partProviders.put(currentProvider.getCode(), currentProvider);
			}
			retValue = true;
		}
		catch(Exception localException)
		{
			logger.error("Parts reference data could not be loaded : " + localException.toString());
		}
		return retValue;
	}

	
	
	
	/**
	 * this method made a validation for one line in the file of terminal
	 * 
	 * @param tokens
	 *            each token means a field that compose one line.
	 */
	private CResultMessage validateFileLine(String[] tokens, int line, CPhysicalTerminal terminal,SessionData sessionData) 
	{
		CResultMessage resultmsg = null;
		int ind = 1;

		String msg = null;
		HashMap<String, CCommunicationType> commTypeObjs = null;
		boolean flagContinue = true;
		while (ind <= tokens.length && flagContinue) 
		{
			String token = tokens[ind-1];
			//System.out.println("token " + token);
			switch (ind) 
			{
				case 1: // serial
					if (!StringUtil.isAlphaNumericRec(token)) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.serialValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						terminal.setSerialNumber(token);
					}
				break;
				case 2: // brand
					if (!terminalBrands.containsKey(token.toUpperCase())) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.brandValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						terminal.setBrand(terminalBrands.get(token.toUpperCase()));
					}
					break;
				case 3: // model
					ArrayList<CTerminalModel> models = terminal.getBrand().getModelList();
					boolean existModel = false;
					for(CTerminalModel terminalModel:models)
					{
						if (terminalModel.getModelName().equalsIgnoreCase(token)) 
						{
							existModel = true;
							terminal.setModel(terminalModel);
							break;
						}
					}
					if (!existModel) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.modelValidation", sessionData.getLanguage());
						flagContinue = false;
					}
					break;
				case 4: // application Type
					if (!applicationTypes.containsKey(token.toUpperCase())) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.applicationTypeValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						terminal.setApplicationType(applicationTypes.get(token.toUpperCase()));
					}
					break;
				case 5: // softwareLicense
					if (terminal.getApplicationType().getApplicationTypeCode().equalsIgnoreCase(CApplicationType.MICROBROWSER_CODE)) 
					{
						if (!StringUtil.isAlphaNumericRec(token)) 
						{
							resultmsg = new CResultMessage();
							msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.licenseFormatValidation", sessionData.getLanguage());
							flagContinue = false;
						} 
						else 
						{
							terminal.setSoftwareLicense(token);
						}
					} 
					else 
					{
						terminal.setSoftwareLicense("");
					}
					break;
				case 6:
					// communicationType
					StringTokenizer st = new StringTokenizer(token, ";");
					//String[] commTypeIds;
	
					if (st.hasMoreTokens()) 
					{
						commTypeObjs = new HashMap<String, CCommunicationType>();
						int i = 0;
						while (st.hasMoreTokens()) 
						{
							String comTypeToken = st.nextToken();
							if (!communicationTypes.containsKey(comTypeToken.toUpperCase())) 
							{
								resultmsg = new CResultMessage();
								msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.commTypeValidation", sessionData.getLanguage());
								flagContinue = false;
								break;
							} 
							else 
							{
								CCommunicationType ct = CCommunicationType.getCommTypeByCode(comTypeToken.toUpperCase());
								terminal.getCommunicationTypeSet().add(ct);
								commTypeObjs.put(comTypeToken.toUpperCase(), ct);
							}
							i++;
						}
					} 
					else // at least one communication type is needed
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.commTypeValidation", sessionData.getLanguage());
						flagContinue = false;
					}
					break;
				case 7: // is PrimaryCommType in case 6
					if (!commTypeObjs.containsKey(token.toUpperCase())) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.primaryCommTypeValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						terminal.setPrimaryCommType(commTypeObjs.get(token.toUpperCase()).getCommunicationTypeId());
					}
					break;
				case 8: // IMEI
					if (terminal.getPrimaryCommType().getCommunicationTypeCode().equalsIgnoreCase(CCommunicationType.GPRS_CODE)) 
					{
						if (!StringUtil.isNumericRegex(token) && token.length() > 15) 
						{
							resultmsg = new CResultMessage();
							msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.imeiFormatValidation", sessionData.getLanguage());
							flagContinue = false;
						} 
						else 
						{
							terminal.setMobileEquipmentId(token);
						}
					} 
					else 
					{
						terminal.setMobileEquipmentId("");
					}
					break;
				case 9: // PURCHASEORDER
					if (!StringUtil.isAlphaNumericRegex(token) && token.length() > DEFAULT_ALPHA_LENGHT) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.purchaseOrdenValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						terminal.setPurchaseOrder(token);
					}
					break;
				case 10: // inventory Admitance Data
					Date dtEndDate = new Date();
					Date dtStartDate = null;
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					if (!DateUtil.isValidYYYYMMDD(token)) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.admitanceDateValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						try 
						{
							dtStartDate = formatter.parse(token);
						} 
						catch (ParseException ex) {
						}
						if (dtStartDate.after(dtEndDate)) 
						{
							resultmsg = new CResultMessage();
							msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.admitanceDateFutureValidation", sessionData.getLanguage());
							flagContinue = false;
						} 
						else 
						{
							terminal.setInventoryAdmitanceDate(token);
						}
					}
					break;
				case 11: // lotNumber
					if (!StringUtil.isAlphaNumericRegex(token) && token.length() > DEFAULT_ALPHA_LENGHT) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.lotNumberValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						terminal.setLotNumber(token);
					}
					break;
				default:
					break;
			}
			ind++;
		}

		// extra validations
		if (flagContinue) 
		{	
			//Check if terminal already exists
			if ((CPhysicalTerminal.terminalExists(terminal.getSerialNumber(), terminal.getBrand().getBrandId()) != null) ||
				(!CPhysicalTerminal.IsSerialNumberUnique(terminal.getSerialNumber())))
			{
				resultmsg = new CResultMessage();
				msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.terminalExistValidation", sessionData.getLanguage());
				flagContinue = false;
			}
			
			if (!terminal.getMobileEquipmentId().equalsIgnoreCase("") && imeiTemp.containsKey(terminal.getMobileEquipmentId())) 
			{
				resultmsg = new CResultMessage();
				msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.imeiExistValidation", sessionData.getLanguage());
				flagContinue = false;
			}
			else
			{
				imeiTemp.put(terminal.getMobileEquipmentId(), terminal.getMobileEquipmentId());
			}
			
			if (!terminal.getSoftwareLicense().equalsIgnoreCase("") && licenseTemp.containsKey(terminal.getSoftwareLicense())) 
			{ // Current license already passed in a previous record
				resultmsg = new CResultMessage();
				msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.licenseExistValidation", sessionData.getLanguage());
				flagContinue = false;
			}
			else
			{
				licenseTemp.put(terminal.getSoftwareLicense(), terminal.getSoftwareLicense());
			}
			
			if ((flagContinue && terminal.getPrimaryCommType().getCommunicationTypeCode().equalsIgnoreCase(CCommunicationType.GPRS_CODE)|| 
			   (terminal.getApplicationType().getApplicationTypeCode().equalsIgnoreCase(CApplicationType.MICROBROWSER_CODE)))) 
			{
				
				// validate the field, imei, software licence
				CPhysicalTerminal[] terminalsTarget = CPhysicalTerminal.searchTerminal(null, null, null, null, null, 
						terminal.getSoftwareLicense(), terminal.getMobileEquipmentId(), null, null);

				if (terminalsTarget.length > 0) 
				{
					for (int i = 0; i < terminalsTarget.length; i++) 
					{
						CPhysicalTerminal refenceTerminal = terminalsTarget[i];
						if (terminal.getPrimaryCommType().getCommunicationTypeCode().equalsIgnoreCase(CCommunicationType.GPRS_CODE)) 
						{
							if (refenceTerminal.getMobileEquipmentId().equalsIgnoreCase(terminal.getMobileEquipmentId())) 
							{	// IMEI exist
								resultmsg = new CResultMessage();
								msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.imeiExistValidation", sessionData.getLanguage());
								flagContinue = false;
								break;
							}
						}
						if ((terminal.getApplicationType() != null) && (terminal.getApplicationType().getApplicationTypeCode().equalsIgnoreCase(CApplicationType.MICROBROWSER_CODE))) 
						{
							if ((refenceTerminal.getSoftwareLicense() != null) && (refenceTerminal.getSoftwareLicense().equalsIgnoreCase(terminal.getSoftwareLicense())))
							{	//Software license exist
								resultmsg = new CResultMessage();
								msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.licenseExistValidation", sessionData.getLanguage());
								flagContinue = false;
								break;
							}
						}
					}
				}
			}
			
		}

		if (flagContinue == false) {
			resultmsg.setNumberLine(line);
			resultmsg.setMessage(msg);
			resultmsg.setError(true);
		}
		return resultmsg;
	}

	public CResultMessage validateFileTerminal(ServletContext context, String fileName, String userId,SessionData sessionData) 
	{
		CResultMessage resultmsg = null;
		boolean continueFlag = true;
		if (!loadTerminalsBasicData()) 
		{
			resultmsg = new CResultMessage();
			resultmsg.setMessage(Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.dataNotfound", sessionData.getLanguage()));
			resultmsg.setNumberLine(0);
			resultmsg.setError(true);
			return resultmsg;
		}
		try {
			imeiTemp.clear();
			licenseTemp.clear();
			String workingDir = DebisysConfigListener.getDownloadPath(context);
			File file = new File(workingDir + "/" + fileName);
			if (file.exists()) {
				BufferedReader inBr = new BufferedReader(new FileReader(file));
				String line = inBr.readLine();

				int numLine = 0;
				ArrayList<CPhysicalTerminal> totalTerminal = new ArrayList<CPhysicalTerminal>();
				while (line != null && !line.equalsIgnoreCase("") && continueFlag) 
				{
					// System.out.println("line " + line);
					numLine++;
					// validate structure for each line
					// each line from the file need 11 fields
					
					String[] tokens = line.split(",");
			        
					if (tokens.length != 11) {
						resultmsg = new CResultMessage();
						resultmsg.setMessage(Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.badstructure", sessionData.getLanguage()));
						resultmsg.setNumberLine(numLine);
						resultmsg.setError(true);
						continueFlag = false;
					} 
					else 
					{
						CPhysicalTerminal terminal = new CPhysicalTerminal();
						CTerminalStatus[] tstatus = CPhysicalTerminal.getAllowedStatus(-1, null);
						// is new
						terminal.setCurrentStatus(tstatus[0]);
						resultmsg = validateFileLine(tokens, numLine, terminal,sessionData);
						if (resultmsg != null) 
						{
							continueFlag = false;
							terminal = null;
							totalTerminal = null;
							break;
						} 
						else 
						{
							totalTerminal.add(terminal);
						}
					}
					line = inBr.readLine();
				}
				if (continueFlag) {
					// save the terminals;
					numLine = 0;
					for(CPhysicalTerminal physicalTerminal:totalTerminal)
					{
						if (physicalTerminal.createNewTerminal(false, userId,sessionData)) {
							numLine++;
						}
					}
					resultmsg = new CResultMessage();
					resultmsg.setMessage(Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.filesuccess", sessionData.getLanguage()));
					resultmsg.setTotalRecord(numLine);
					resultmsg.setError(false);
				}
				// delete file
				file.delete();
				inBr = null;
				line = null;
				totalTerminal = null;
			} else {
				logger.error("FileLoad: file doesn't exist");
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return resultmsg;
	}

	
	
	
	
	/**
	 * @param tokens	An array of fields values from a file part line
	 * @param line		Current file line number
	 * @param simCard	Current object to be filled
	 * @return			Successful or fail message object depending on the line processing results 
	 */
	private CResultMessage validatePartLine(String[] tokens, int line, CSimCard simCard,SessionData sessionData) 
	{
		CResultMessage resultmsg = null;
		int ind = 1;

		String msg = null;
		boolean flagContinue = true;
		Date dtEndDate = new Date();
		Date dtStartDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		
		while (ind <= tokens.length && flagContinue) 
		{
			String token = tokens[ind-1];
			switch (ind) 
			{
				case 1: // International Mobile Subscriber ID - IMSI
					if ((!StringUtil.isNumericRegex(token)) || (token.length() > 20)) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.IMSIValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						simCard.setMobileSuscriberId(token);
					}
				break;
				case 2: // Phone number
					if((!token.trim().equals("")) && ((!StringUtil.isNumericRegex(token)) || (token.length() > 15)))
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.PhoneValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						simCard.setPhoneNumber(token.trim());
					}
					break;
				case 3: // Provider Code
					if (!partProviders.containsKey(token)) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.ProviderValidation", sessionData.getLanguage());
						flagContinue = false;
					}
					else
					{
						simCard.setProvider(partProviders.get(token));
					}
					break;
				case 4: // Activation Date
					dtEndDate = new Date();
					dtStartDate = null;
					if (!DateUtil.isValidYYYYMMDD(token)) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.ActivationDateValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						try 
						{
							dtStartDate = formatter.parse(token);
						} 
						catch (ParseException ex) {
						}
						if (dtStartDate.after(dtEndDate)) 
						{
							resultmsg = new CResultMessage();
							msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.ActivationDateValidation", sessionData.getLanguage());
							flagContinue = false;
						} 
						else 
						{
							simCard.setActivationDate(token);
						}
					}
					break;
				case 5: // lotNumber
					if (!StringUtil.isAlphaNumericRegex(token) || token.length() > DEFAULT_ALPHA_LENGHT) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.LotValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						simCard.setLotNumber(token);
					}
					break;
				case 6: // Contract number
					if (!StringUtil.isAlphaNumericRegex(token) || token.length() > DEFAULT_ALPHA_LENGHT) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.ContractValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						simCard.setContractNumber(token);
					}
					break;
				case 7: // Activation Date
					dtEndDate = new Date();
					dtStartDate = null;
					if (!DateUtil.isValidYYYYMMDD(token)) 
					{
						resultmsg = new CResultMessage();
						msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.ContractDateValidation", sessionData.getLanguage());
						flagContinue = false;
					} 
					else 
					{
						try 
						{
							dtStartDate = formatter.parse(token);
						} 
						catch (ParseException ex) {
						}
						if (dtStartDate.after(dtEndDate)) 
						{
							resultmsg = new CResultMessage();
							msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.ContractDateValidation", sessionData.getLanguage());
							flagContinue = false;
						} 
						else 
						{
							simCard.setContractDate(token);
						}
					}
					break;
				default:
					break;
			}
			ind++;
		}

		// extra validations
		if (flagContinue) 
		{	
			//Check if part already exists on DB and previous loaded parts
			if ((loadedIMSIs.containsKey(simCard.getMobileSuscriberId()) ||
				(CSimCard.partExists(simCard.getPartType().getPartTypeId(), simCard.getMobileSuscriberId()) != null)))
			{
				resultmsg = new CResultMessage();
				msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.PartExistsValidation", sessionData.getLanguage());
				flagContinue = false;
			}
			else
			{
				loadedIMSIs.put(simCard.getMobileSuscriberId(), simCard.getMobileSuscriberId());
			}
			if((simCard.getPhoneNumber() != null) && (!simCard.getPhoneNumber().equals("")) &&
				((CSimCard.getPhonesInUse(simCard.getPhoneNumber()) != 0) || (loadedPhones.containsKey(simCard.getPhoneNumber()))))
			{
				resultmsg = new CResultMessage();
				msg = Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.PhoneExistsDateValidation", sessionData.getLanguage());
				flagContinue = false;				
			}
			else
			{
				loadedPhones.put(simCard.getPhoneNumber(), simCard.getPhoneNumber());
			}
		}

		if (flagContinue == false) 
		{
			resultmsg.setNumberLine(line);
			resultmsg.setMessage(msg);
			resultmsg.setError(true);
		}
		return resultmsg;
	}

	
	/**
	 * @param context	Session context used to get shared data
	 * @param fileName	Current file name that is been processed
	 * @param userId	Current session user 
	 * @return			Successful or fail message object depending on file processing results 
	 */
	public CResultMessage validatePartsFile(ServletContext context, String fileName, String userId,SessionData sessionData) 
	{
		CResultMessage resultmsg = null;
		boolean continueFlag = true;
		if (!loadPartsBasicData()) 
		{
			resultmsg = new CResultMessage();
			resultmsg.setMessage(Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.dataNotfound", sessionData.getLanguage()));
			resultmsg.setNumberLine(0);
			resultmsg.setError(true);
			return resultmsg;
		}
		try {
			loadedIMSIs.clear();
			loadedPhones.clear();
			String workingDir = DebisysConfigListener.getDownloadPath(context);
			File file = new File(workingDir + "/" + fileName);
			if (file.exists()) 
			{
				BufferedReader inBr = new BufferedReader(new FileReader(file));
				String line = inBr.readLine();

				int numLine = 0;
				ArrayList<CSimCard> totalSims = new ArrayList<CSimCard>();
				while ((line != null) && (!line.equalsIgnoreCase("")) && (continueFlag)) 
				{
					numLine++;
					// validate structure for each line
					
					String[] tokens = line.split(",");
			        
					if (tokens.length != FIELDS_PER_PART) 
					{
						resultmsg = new CResultMessage();
						resultmsg.setMessage(Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.badstructure", sessionData.getLanguage()));
						resultmsg.setNumberLine(numLine);
						resultmsg.setError(true);
						continueFlag = false;
					} 
					else 
					{
						CSimCard newSim = new CSimCard();
						CTerminalPartStatus[] statusNew = CTerminalPart.getAllowedStatus(null, null);
						newSim.setStatus(statusNew[0]);
						CTerminalPartType simType = CTerminalPartType.getTerminalPartTypeByCode(CTerminalPartType.SIMCARD_CODE);
						newSim.setPartType(simType);
						resultmsg = validatePartLine(tokens, numLine, newSim,sessionData);
						if (resultmsg != null) 
						{
							continueFlag = false;
							newSim = null;
							totalSims = null;
							break;
						} 
						else 
						{
							totalSims.add(newSim);
						}
					}
					line = inBr.readLine();
				}
				if (continueFlag) 
				{
					// save parts
					numLine = 0;
					for(CSimCard currentSim:totalSims)
					{
						if (currentSim.insertNewPart(userId,sessionData)) 
						{
							numLine++;
						}
					}
					resultmsg = new CResultMessage();
					resultmsg.setMessage(Languages.getString("jsp.admin.transactions.inventoryTerminalsLoadLotFile.filesuccess", sessionData.getLanguage()));
					resultmsg.setTotalRecord(numLine);
					resultmsg.setError(false);
				}
				// delete file
				file.delete();
				inBr = null;
				line = null;
				totalSims = null;
			} 
			else 
			{
				logger.error("FileLoad: file doesn't exist");
			}
		} 
		catch (Exception e) 
		{
			logger.error(e.toString());
		}
		return resultmsg;
	}
}
