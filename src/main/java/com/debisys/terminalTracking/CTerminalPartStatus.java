/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 * Terminal Part status information container
 */
public class CTerminalPartStatus 
{
	// ------------------------------------  constants -------------------------------------------------
	public static final String NEW_CODE = "NEW";			//	New
	public static final String READY_CODE = "READY";		//	Ready
	public static final String ASSIGN_CODE = "ASSIGN";		//	Assigned
	public static final String LOST_CODE = "LOST_CODE";		//	Lost
	public static final String DISABLE_CODE = "DISABLE";	//	Disabled
	// ------------------------------------  attributes ------------------------------------------------
	private long m_PartStatusId;
	private String m_PartStatusCode;
	private String m_PartStatusDescription;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CTerminalPartStatus.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_PartStatusId
	 */
	public long getPartStatusId() 
	{
		return m_PartStatusId;
	}
	/**
	 * @param partStatusId the m_PartStatusId to set
	 */
	public void setPartStatusId(long partStatusId) 
	{
		m_PartStatusId = partStatusId;
	}
	/**
	 * @return the m_PartStatusCode
	 */
	public String getPartStatusCode() 
	{
		return m_PartStatusCode;
	}
	/**
	 * @param partStatusCode the m_PartStatusCode to set
	 */
	public void setPartStatusCode(String partStatusCode) 
	{
		m_PartStatusCode = partStatusCode;
	}
	/**
	 * @return the m_PartStatusDescription
	 */
	public String getPartStatusDescription() 
	{
		return m_PartStatusDescription;
	}
	/**
	 * @param partStatusDescription the m_PartStatusDescription to set
	 */
	public void setPartStatusDescription(String partStatusDescription) 
	{
		m_PartStatusDescription = partStatusDescription;
	}
	
	// ------------------------------------  methods  --------------------------------------------------
	
	/**
	 * @return Return the list of all terminal status
	 */
	public static CTerminalPartStatus[] getAllPartStatus()
	{
		CTerminalPartStatus[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPartStatus = null;
		ArrayList<CTerminalPartStatus> tmpList = new ArrayList<CTerminalPartStatus>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getAllPartStatus"));
	        rsPartStatus = ps.executeQuery();
            if(rsPartStatus != null)
            {
            	while(rsPartStatus.next())
            	{
            		CTerminalPartStatus newPartStatus = new CTerminalPartStatus();
            		newPartStatus.setPartStatusId(rsPartStatus.getLong("terminalPartStatusId"));
            		newPartStatus.setPartStatusCode(rsPartStatus.getString("code"));
            		newPartStatus.setPartStatusDescription(rsPartStatus.getString("description"));
            		tmpList.add(newPartStatus);
            	}	
            	retValue = new CTerminalPartStatus[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPartStatus != null )
    		{
    			try { rsPartStatus.close(); }
    			catch (Exception ex) {}
    		}
    		rsPartStatus = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	/**
	 * @param partStatusCode	
	 * @return					The part status object which code is the same as the parameter given 
	 */
	public static CTerminalPartStatus getPartStatusByCode(String partStatusCode)
	{
		CTerminalPartStatus retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPartStatus = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPartStatusByCode"));
	        ps.setString(1, partStatusCode);
	        rsPartStatus = ps.executeQuery();
            if((rsPartStatus != null) && (rsPartStatus.next()))
            {
        		retValue = new CTerminalPartStatus();
        		retValue.setPartStatusId(rsPartStatus.getLong("terminalPartStatusId"));
        		retValue.setPartStatusCode(rsPartStatus.getString("code"));
        		retValue.setPartStatusDescription(rsPartStatus.getString("description"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPartStatus != null )
    		{
    			try { rsPartStatus.close(); }
    			catch (Exception ex) {}
    		}
    		rsPartStatus = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	

	/**
	 * @param partStatusId
	 * @return				An status object if found.
	 */
	public static CTerminalPartStatus getPartStatusById(long partStatusId)
	{
		CTerminalPartStatus retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPartStatus = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPartStatusById"));
	        ps.setLong(1, partStatusId);
	        rsPartStatus = ps.executeQuery();
            if((rsPartStatus != null) && (rsPartStatus.next()))
            {
        		retValue = new CTerminalPartStatus();
        		retValue.setPartStatusId(rsPartStatus.getLong("terminalPartStatusId"));
        		retValue.setPartStatusCode(rsPartStatus.getString("code"));
        		retValue.setPartStatusDescription(rsPartStatus.getString("description"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPartStatus != null )
    		{
    			try { rsPartStatus.close(); }
    			catch (Exception ex) {}
    		}
    		rsPartStatus = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

}
