/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 * Base abstract class for different terminal parts
 */
public abstract class CTerminalPart 
{
	// ------------------------------------  constants -------------------------------------------------
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_SMALLDATE_FORMAT = "yyyy-MM-dd";
	public static final String DEFAULT_VIEW_FORMAT = "MM/dd/yyyy";
	// ------------------------------------  attributes ------------------------------------------------
	protected long m_PartId;
	protected CTerminalPartType m_PartType;
	protected String m_LotNumber;
	protected CTerminalPartStatus m_Status;
	protected Calendar m_LastChangeStatusDate;
	protected long m_TotalRecords; // If current object belongs to a record set, this filed indicates the total set count
	protected static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	protected static Logger logger = Logger.getLogger(CTerminalPart.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_PartId
	 */
	public long getPartId() 
	{
		return m_PartId;
	}
	/**
	 * @param partId the m_PartId to set
	 */
	public void setPartId(long partId) 
	{
		m_PartId = partId;
	}
	/**
	 * @return the m_PartType
	 */
	public CTerminalPartType getPartType() 
	{
		return m_PartType;
	}
	/**
	 * @param partType the m_PartType to set
	 */
	public void setPartType(CTerminalPartType partType) 
	{
		m_PartType = partType;
	}
	/**
	 * @return the m_LotNumber
	 */
	public String getLotNumber() 
	{
		return m_LotNumber;
	}
	/**
	 * @param lotNumber the m_LotNumber to set
	 */
	public void setLotNumber(String lotNumber) 
	{
		m_LotNumber = lotNumber;
	}
	/**
	 * @return the m_Status
	 */
	public CTerminalPartStatus getStatus() 
	{
		return m_Status;
	}
	/**
	 * @param status the m_Status to set
	 */
	public void setStatus(CTerminalPartStatus status) 
	{
		m_Status = status;
	}
	/**
	 * @return the m_LastChangeStatusDate
	 */
	public Calendar getLastChangeStatusDate() 
	{
		return m_LastChangeStatusDate;
	}
	/**
	 * @param lastChangeStatusDate the m_LastChangeStatusDate to set
	 */
	public void setLastChangeStatusDate(Calendar lastChangeStatusDate) 
	{
		m_LastChangeStatusDate = lastChangeStatusDate;
	}
	
	/**
	 * @return the m_TotalRecords
	 */
	public long getTotalRecords() {
		return m_TotalRecords;
	}
	/**
	 * @param totalRecords the m_TotalRecords to set
	 */
	public void setTotalRecords(long totalRecords) {
		m_TotalRecords = totalRecords;
	}

	
	// ------------------------------------  methods  --------------------------------------------------
	
	/**
	 * @param partId
	 * @param lastChangeDate
	 * @param newSatatusId
	 * @param userId
	 * @param changeReason
	 * @return				True if a new record could be created on the terminalPartsStatusChanges table
	 */
	public static boolean insertPartStatusChange(long partId, Calendar lastChangeDate, long newSatatusId,
			String userId, String changeReason)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		SimpleDateFormat smallDateFormat = new SimpleDateFormat(DEFAULT_SMALLDATE_FORMAT);
		
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("insertPartStatusChange"));
	        ps.setLong(1, partId);
	        ps.setString(2, dateFormat.format(lastChangeDate.getTime()));
	        ps.setString(3, smallDateFormat.format(lastChangeDate.getTime()));
	        ps.setLong(4, newSatatusId);
	        ps.setString(5, userId);
	        ps.setString(6, changeReason);
	        ps.executeUpdate();
            retValue = true; 
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;		
	}
	
	
	/**
	 * @param currentStatusId		Use -1 if no status ID is available
	 * @param currentStatusCode		Use null if no status code is available
	 * If both parameters are "null" there wont be any results. No default values are assumed
	 * @return	An array of allowed status to go from the current status given
	 */
	public static CTerminalPartStatus[] getAllowedStatus(Long currentStatusId, String currentStatusCode)
	{
		CTerminalPartStatus[] retValue = null;
		Connection dbConn = null;
		ResultSet rsStatusAllowed = null;
		PreparedStatement ps = null;
		ArrayList<CTerminalPartStatus> tmpList = new ArrayList<CTerminalPartStatus>();

		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPartAllowedStatus"));
	        
			if ((currentStatusCode != null) && (!currentStatusCode.equals(""))) 
			{
				ps.setString(1, currentStatusCode);
			} 
			else 
			{
				ps.setNull(1, java.sql.Types.VARCHAR);
			}
			if (currentStatusId != null) 
			{
				ps.setLong(2, currentStatusId);
			} 
			else 
			{
				ps.setNull(2, java.sql.Types.INTEGER);
			}
	        rsStatusAllowed = ps.executeQuery();
	        if(rsStatusAllowed != null)
	        {
	        	while(rsStatusAllowed.next())
	        	{
	        		CTerminalPartStatus newStatus = new CTerminalPartStatus();
	        		newStatus.setPartStatusId(rsStatusAllowed.getLong("targetStatusId"));
	        		newStatus.setPartStatusCode(rsStatusAllowed.getString("statusCode"));
	        		newStatus.setPartStatusDescription(rsStatusAllowed.getString("statusDescription"));
	        		tmpList.add(newStatus);
	        	}
	        	retValue = new CTerminalPartStatus[tmpList.size()];
	        	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
	        }	        
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsStatusAllowed != null )
    		{
    			try { rsStatusAllowed.close(); }
    			catch (Exception ex) {}
    		}
    		rsStatusAllowed = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	/**
	 * @param targetStatusId
	 * @return
	 */
	public boolean changeStatus(long targetStatusId)
	{
		boolean retValue = false;
		
		CTerminalPartStatus[] statusAllowed = CTerminalPart.getAllowedStatus(this.m_Status.getPartStatusId(), null);
		if(statusAllowed != null)
		{
			for(CTerminalPartStatus currentStatus:statusAllowed)
			{
				if(currentStatus.getPartStatusId() == targetStatusId)
				{
					Connection dbConn = null;
					PreparedStatement ps = null;
				
					try
					{
						dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
						
			            if ( !Torque.isInit() )
			            {
			                throw new Exception("Torque is not initialized");
			            }

				        if (dbConn == null)
				        {
				          throw new Exception("Can't get database connection");
				        }
				        
				        ps = dbConn.prepareStatement(sql_bundle.getString("updatePartStatus"));
				        ps.setLong(1, targetStatusId);
				        ps.setLong(2, this.m_PartId);
				        ps.executeUpdate();
				        this.m_Status = CTerminalPartStatus.getPartStatusById(targetStatusId);
				        retValue = true;
					}
					catch(java.lang.Exception localException)
					{
						logger.error(localException.toString());
						retValue = false;
					}
			    	finally
			    	{
			    		if ( ps != null )
			    		{
			    			try { ps.close(); }
			    			catch (Exception ex) {}
			    		}
			    		ps = null;
			    		if ( dbConn != null )

			                {
			    			try { dbConn.close(); }
			    			catch (Exception ex) {}
			    		}
			    		dbConn = null;
			    	}
			    	break; // Out from the loop when a valid target status is found  
				}// End If
			}
		}		
		return retValue;
	}
	
	
	/**
	 * @return Unique part identifier
	 */
	public abstract String getKey();


	/**
	 * @return	Parent terminalId
	 */
	public Long getParentTerminal()
	{
		Long retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPart = null;
		
		try
		{
                    dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getParentTerminal"));
            ps.setLong(1, this.m_PartId);
            rsPart = ps.executeQuery();
            if((rsPart != null) && (rsPart.next()))
            {
            	retValue = new Long(rsPart.getLong("terminalId"));
            }
                    if ( !Torque.isInit() )
                    {
                        throw new Exception("Torque is not initialized");
                    }

                    if (dbConn == null)
                    {
                      throw new Exception("Can't get database connection");
                    }
                    ps = dbConn.prepareStatement(sql_bundle.getString("getParentTerminal"));
                    ps.setLong(1, this.m_PartId);
                    rsPart = ps.executeQuery();
                    if((rsPart != null) && (rsPart.next()))
                    {
                        retValue = new Long(rsPart.getLong("terminalId"));
                    }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPart != null )
    		{
    			try { rsPart.close(); }
    			catch (Exception ex) {}
    		}
    		rsPart = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	
}
