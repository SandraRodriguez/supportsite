/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 * This class saves all spscific part type information
 */
public class CTerminalPartType 
{
	// ------------------------------------  constants -------------------------------------------------
	public static final String SIMCARD_CODE = "SIMCARD";
	public static final String CONLESS_CODE = "CONLESS";	//Contact Less
	public static final String PINPAD_CODE = "PINPAD";		//Pin Pad
	// ------------------------------------  attributes ------------------------------------------------
	private long m_PartTypeId;
	private String m_PartTypeCode;
	private String m_PartDescription;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CTerminalPartType.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_PartTypeId
	 */
	public long getPartTypeId() 
	{
		return m_PartTypeId;
	}
	/**
	 * @param partTypeId the m_PartTypeId to set
	 */
	public void setPartTypeId(long partTypeId) 
	{
		m_PartTypeId = partTypeId;
	}
	/**
	 * @return the m_PartTypeCode
	 */
	public String getPartTypeCode() 
	{
		return m_PartTypeCode;
	}
	/**
	 * @param partTypeCode the m_PartTypeCode to set
	 */
	public void setPartTypeCode(String partTypeCode) 
	{
		m_PartTypeCode = partTypeCode;
	}
	/**
	 * @return the m_PartDescription
	 */
	public String getPartDescription() 
	{
		return m_PartDescription;
	}
	/**
	 * @param partDescription the m_PartDescription to set
	 */
	public void setPartDescription(String partDescription) 
	{
		m_PartDescription = partDescription;
	}
	
	// ------------------------------------  methods  --------------------------------------------------

	/**
	 * @return Return the list of all terminal types
	 */
	public static CTerminalPartType[] getAllTerminalPartTypes()
	{
		CTerminalPartType[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPartTypes = null;
		ArrayList<CTerminalPartType> tmpList = new ArrayList<CTerminalPartType>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPartTypes"));
	        rsPartTypes = ps.executeQuery();
            if(rsPartTypes != null)
            {
            	while(rsPartTypes.next())
            	{
            		CTerminalPartType newPartType = new CTerminalPartType();
            		newPartType.setPartTypeId(rsPartTypes.getLong("terminalPartTypeId"));
            		newPartType.setPartTypeCode(rsPartTypes.getString("code"));
            		newPartType.setPartDescription(rsPartTypes.getString("description"));
            		tmpList.add(newPartType);
            	}	
            	retValue = new CTerminalPartType[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPartTypes != null )
    		{
    			try { rsPartTypes.close(); }
    			catch (Exception ex) {}
    		}
    		rsPartTypes = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;

	}


	/**
	 * @return	A terminal type code  which code is the same as the given
	 */
	public static CTerminalPartType getTerminalPartTypeByCode(String partTypeCode)
	{
		CTerminalPartType retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPartTypes = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPartTypeByCode"));
	        ps.setString(1, partTypeCode);
	        rsPartTypes = ps.executeQuery();
            if((rsPartTypes != null) && (rsPartTypes.next()))
            {
        		retValue = new CTerminalPartType();
        		retValue.setPartTypeId(rsPartTypes.getLong("terminalPartTypeId"));
        		retValue.setPartTypeCode(rsPartTypes.getString("code"));
        		retValue.setPartDescription(rsPartTypes.getString("description"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPartTypes != null )
    		{
    			try { rsPartTypes.close(); }
    			catch (Exception ex) {}
    		}
    		rsPartTypes = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;

	}

	
	public static CTerminalPartType getTerminalPartTypeById(long partTypeId)
	{
		CTerminalPartType retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsPartTypes = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getPartTypeById"));
	        ps.setLong(1, partTypeId);
	        rsPartTypes = ps.executeQuery();
            if((rsPartTypes != null) && (rsPartTypes.next()))
            {
        		retValue = new CTerminalPartType();
        		retValue.setPartTypeId(rsPartTypes.getLong("terminalPartTypeId"));
        		retValue.setPartTypeCode(rsPartTypes.getString("code"));
        		retValue.setPartDescription(rsPartTypes.getString("description"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsPartTypes != null )
    		{
    			try { rsPartTypes.close(); }
    			catch (Exception ex) {}
    		}
    		rsPartTypes = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;

	}

	
}
