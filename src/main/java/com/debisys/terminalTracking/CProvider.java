/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 *
 */
public class CProvider 
{
	// ------------------------------------  attributes ------------------------------------------------
	private long m_ProviderId;
	private String m_ProviderCode;
	private String m_ProviderDescription;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CProvider.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_CarrierId
	 */
	public long getProviderId() 
	{
		return m_ProviderId;
	}
	/**
	 * @param ProviderId the m_ProviderId to set
	 */
	public void setProviderId(long providerId) 
	{
		m_ProviderId = providerId;
	}
	
	/**
	 * @return the m_Description
	 */
	public String getCode() 
	{
		return m_ProviderCode;
	}
	/**
	 * @param code the m_Description to set
	 */
	public void setCode(String code) 
	{
		m_ProviderCode = code;
	}
	
	
	/**
	 * @return the m_Description
	 */
	public String getDescription() 
	{
		return m_ProviderDescription;
	}
	/**
	 * @param description the m_Description to set
	 */
	public void setDescription(String description) 
	{
		m_ProviderDescription = description;
	}
	/**
	 * @return the m_ShortName
	 */
	
	// ------------------------------------  methods  --------------------------------------------------
	/**
	 * @param providerId	Provider ID from DB
	 * 
	 * Loads object information from DB
	 */
	public boolean loadProvider(long providerId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsProvider = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getProviderById"));
            ps.setLong(1, providerId);
            rsProvider = ps.executeQuery();
            if(rsProvider != null)
            {
            	if(rsProvider.next())
            	{
            		this.m_ProviderId = rsProvider.getLong("terminalPartProviderId");
            		this.m_ProviderCode = rsProvider.getString("code");
            		this.m_ProviderDescription = rsProvider.getString("description");
            		retValue = true;
            	}	
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsProvider != null )
    		{
    			try { rsProvider.close(); }
    			catch (Exception ex) {}
    		}
    		rsProvider = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	
	/**
	 * @return The complete list of carriers
	 */
	public static CProvider[] getProviderList()
	{
		CProvider[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsProvider = null;
		ArrayList<CProvider> tmpList = new ArrayList<CProvider>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getProviderList"));
            rsProvider = ps.executeQuery();
            if(rsProvider != null)
            {
            	while(rsProvider.next())
            	{
            		CProvider newProvider = new CProvider();
            		newProvider.setProviderId(rsProvider.getLong("terminalPartProviderId"));
            		newProvider.setCode(rsProvider.getString("code"));
            		newProvider.setDescription(rsProvider.getString("description"));
            		tmpList.add(newProvider);
            	}	
            	retValue = new CProvider[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsProvider != null )
    		{
    			try { rsProvider.close(); }
    			catch (Exception ex) {}
    		}
    		rsProvider = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	/**
	 * @param providerId
	 * @return				Returns the provider whose id is the same as the given parameter
	 */
	public static CProvider getProviderById(long providerId)
	{
		CProvider retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsProvider = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getProviderById"));
	        ps.setLong(1, providerId);
            rsProvider = ps.executeQuery();
            if((rsProvider != null) && (rsProvider.next()))
            {
            	retValue = new CProvider();
            	retValue.setProviderId(rsProvider.getLong("terminalPartProviderId"));
            	retValue.setCode(rsProvider.getString("code"));
            	retValue.setDescription(rsProvider.getString("description"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsProvider != null )
    		{
    			try { rsProvider.close(); }
    			catch (Exception ex) {}
    		}
    		rsProvider = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	/**
	 * @param carrierId
	 * @return				Returns the carried whose id is the same as the given parameter
	 */
	public static CProvider getProviderByName(String providerName)
	{
		CProvider retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsProvider = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getProviderByName"));
	        ps.setString(1, providerName);
            rsProvider = ps.executeQuery();
            if((rsProvider != null) && (rsProvider.next()))
            {
            	retValue = new CProvider();
            	retValue.setProviderId(rsProvider.getLong("terminalPartProviderId"));
            	retValue.setCode(rsProvider.getString("code"));
            	retValue.setDescription(rsProvider.getString("description"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsProvider != null )
    		{
    			try { rsProvider.close(); }
    			catch (Exception ex) {}
    		}
    		rsProvider = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
}
