/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 * This class contains information about one type of communication for physical terminals
 */
public class CCommunicationType 
{
	// ------------------------------------  constants -------------------------------------------------
	public static final String DIALUP_CODE = "DIALUP";
	public static final String LAN_CODE = "LAN";
	public static final String GPRS_CODE = "GPRS";	
	// ------------------------------------  attributes ------------------------------------------------
	private long m_CommunicationTypeId;
	private String m_CommunicationTypeCode;
	private String m_CommunicationTypeDescription;
	private boolean m_IsPrimaryCommType;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CCommunicationType.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_CommunicationTypeId
	 */
	public long getCommunicationTypeId() 
	{
		return m_CommunicationTypeId;
	}
	/**
	 * @param communicationTypeId the m_CommunicationTypeId to set
	 */
	public void setCommunicationTypeId(long communicationTypeId) 
	{
		m_CommunicationTypeId = communicationTypeId;
	}
	/**
	 * @return the m_CommunicationTypeCode
	 */
	public String getCommunicationTypeCode() 
	{
		return m_CommunicationTypeCode;
	}
	/**
	 * @param communicationTypeCode the m_CommunicationTypeCode to set
	 */
	public void setCommunicationTypeCode(String communicationTypeCode) 
	{
		m_CommunicationTypeCode = communicationTypeCode;
	}
	/**
	 * @return the m_CommunicationTypeDescription
	 */
	public String getCommunicationTypeDescription() 
	{
		return m_CommunicationTypeDescription;
	}
	/**
	 * @param communicationTypeDescription the m_CommunicationTypeDescription to set
	 */
	public void setCommunicationTypeDescription
	(
			String communicationTypeDescription) {
		m_CommunicationTypeDescription = communicationTypeDescription;
	}
	/**
	 * @return the m_IsPrimaryCommType
	 */
	public boolean getIsPrimaryCommType() 
	{
		return m_IsPrimaryCommType;
	}
	/**
	 * @param isPrimaryCommType the m_IsPrimaryCommType to set
	 */
	public void setIsPrimaryCommType(boolean isPrimaryCommType) 
	{
		m_IsPrimaryCommType = isPrimaryCommType;
	}
	
	
	
	// ------------------------------------  methods  --------------------------------------------------
	
	/**
	 * @return The complete list of communication types
	 */
	public static CCommunicationType[] getAllCommunicationTypes()
	{
		CCommunicationType[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsCommTypes = null;
		ArrayList<CCommunicationType> tmpList = new ArrayList<CCommunicationType>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getCommunicationTypes"));
	        rsCommTypes = ps.executeQuery();
            if(rsCommTypes != null)
            {
            	while(rsCommTypes.next())
            	{
            		CCommunicationType newComType = new CCommunicationType();
            		newComType.setCommunicationTypeId(rsCommTypes.getLong("communicationTypeId"));
            		newComType.setCommunicationTypeCode(rsCommTypes.getString("code"));
            		newComType.setCommunicationTypeDescription(rsCommTypes.getString("description"));
            		newComType.setIsPrimaryCommType(false);
            		tmpList.add(newComType);
            	}	
            	retValue = new CCommunicationType[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsCommTypes != null )
    		{
    			try { rsCommTypes.close(); }
    			catch (Exception ex) {}
    		}
    		rsCommTypes = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	/**
	 * @param commTypeId	What record is going to be loaded
	 * @return				True if info could be obtained from DB
	 */
	public boolean loadCommType(long commTypeId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsCommType = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        ps = dbConn.prepareStatement(sql_bundle.getString("getCommTypeById"));
            ps.setLong(1, commTypeId);
            rsCommType = ps.executeQuery();
            if((rsCommType != null) && (rsCommType.next()))
            {
            	this.m_CommunicationTypeId = rsCommType.getLong("communicationTypeId");
            	this.m_CommunicationTypeCode = rsCommType.getString("code");
            	this.m_CommunicationTypeDescription = rsCommType.getString("description");;
            	this.m_IsPrimaryCommType = false;
            	
            	retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsCommType != null )
    		{
    			try { rsCommType.close(); }
    			catch (Exception ex) {}
    		}
    		rsCommType = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	
	
	
	public static CCommunicationType getCommTypeByCode(String commTypeCode)
	{
		CCommunicationType retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsCommType = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getComTypeByCode"));
	        ps.setString(1, commTypeCode);
	        rsCommType = ps.executeQuery();
            if((rsCommType != null) && (rsCommType.next()))
            {
            	retValue = new CCommunicationType();
            	retValue.setCommunicationTypeId(rsCommType.getLong("communicationTypeId"));
            	retValue.setCommunicationTypeCode(rsCommType.getString("code"));
            	retValue.setCommunicationTypeDescription(rsCommType.getString("description"));
            	retValue.setIsPrimaryCommType(false);
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsCommType != null )
    		{
    			try { rsCommType.close(); }
    			catch (Exception ex) {}
    		}
    		rsCommType = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	
}
