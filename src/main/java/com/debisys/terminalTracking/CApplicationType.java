/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 * This class models the application types that terminals can run	
 */
public class CApplicationType 
{
	// ------------------------------------  constants -------------------------------------------------
	public static final String MICROBROWSER_CODE = "MICRCLI";
	public static final String DEBISYSCLIENT_CODE = "DBSYCLI";
	public static final String QCOMMCLIENT_CODE = "QCOMCLI";
	// ------------------------------------  attributes ------------------------------------------------
	private long m_ApplicationTypeId;
	private String m_ApplicationTypeCode;
	private String m_ApplicationTypeDescription;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CApplicationType.class);

	// ------------------------------------  properties  -----------------------------------------------
	
	/**
	 * @return the m_ApplicationTypeId
	 */
	public long getApplicationTypeId() 
	{
		return m_ApplicationTypeId;
	}
	/**
	 * @param applicationTypeId the m_ApplicationTypeId to set
	 */
	public void setApplicationTypeId(long applicationTypeId) 
	{
		m_ApplicationTypeId = applicationTypeId;
	}
	/**
	 * @return the m_ApplicationTypeCode
	 */
	public String getApplicationTypeCode() 
	{
		return m_ApplicationTypeCode;
	}
	/**
	 * @param applicationTypeCode the m_ApplicationTypeCode to set
	 */
	public void setApplicationTypeCode(String applicationTypeCode) 
	{
		m_ApplicationTypeCode = applicationTypeCode;
	}
	/**
	 * @return the m_ApplicationTypeDescription
	 */
	public String getApplicationTypeDescription() 
	{
		return m_ApplicationTypeDescription;
	}
	/**
	 * @param applicationTypeDescription the m_ApplicationTypeDescription to set
	 */
	public void setApplicationTypeDescription(String applicationTypeDescription) 
	{
		m_ApplicationTypeDescription = applicationTypeDescription;
	}
	
	// ------------------------------------  methods  --------------------------------------------------
	
	public boolean loadAppType(long appTypeId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsAppType = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getApplicationTypeById"));
            ps.setLong(1, appTypeId);
            rsAppType = ps.executeQuery();
            if((rsAppType != null) && (rsAppType.next()))
            {
        		this.m_ApplicationTypeId = rsAppType.getLong("applicationTypeId");
        		this.m_ApplicationTypeCode = rsAppType.getString("code");
        		this.m_ApplicationTypeDescription = rsAppType.getString("description");
        		retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsAppType != null )
    		{
    			try { rsAppType.close(); }
    			catch (Exception ex) {}
    		}
    		rsAppType = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	
	
	/**
	 * @return Complete list of application types
	 */
	public static CApplicationType[] getAllApplicationTypes()
	{
		CApplicationType[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsAppTypes = null;
		ArrayList<CApplicationType> tmpList = new ArrayList<CApplicationType>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getApplicationTypes"));
	        rsAppTypes = ps.executeQuery();
            if(rsAppTypes != null)
            {
            	while(rsAppTypes.next())
            	{
            		CApplicationType newAppType = new CApplicationType();
            		newAppType.setApplicationTypeId(rsAppTypes.getLong("applicationTypeId"));
            		newAppType.setApplicationTypeCode(rsAppTypes.getString("code"));
            		newAppType.setApplicationTypeDescription(rsAppTypes.getString("description"));
            		tmpList.add(newAppType);
            	}	
            	retValue = new CApplicationType[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsAppTypes != null )
    		{
    			try { rsAppTypes.close(); }
    			catch (Exception ex) {}
    		}
    		rsAppTypes = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

}
