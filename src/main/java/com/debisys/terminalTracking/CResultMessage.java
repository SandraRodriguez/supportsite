package com.debisys.terminalTracking;

public class CResultMessage {
	private boolean error = true;
	private String message;
	private long numberLine;
	private long totalRecord;
	public boolean isError() {
		return error;
	}
	public void setError(boolean type) {
		this.error = type;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getNumberLine() {
		return numberLine;
	}
	public void setNumberLine(long numberLine) {
		this.numberLine = numberLine;
	}
	public long getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(long totalRecord) {
		this.totalRecord = totalRecord;
	}
	

}
