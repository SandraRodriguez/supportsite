/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 *
 */
public class CTerminalModel 
{
	// ------------------------------------  attributes ------------------------------------------------
	private long m_ModelId;
	private String m_ModelName;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CTerminalModel.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_ModelId
	 */
	public long getModelId() {
		return m_ModelId;
	}
	/**
	 * @param modelId the m_ModelId to set
	 */
	public void setModelId(long modelId) {
		m_ModelId = modelId;
	}
	/**
	 * @return the m_ModelName
	 */
	public String getModelName() {
		return m_ModelName;
	}
	/**
	 * @param modelName the m_ModelName to set
	 */
	public void setModelName(String modelName) {
		m_ModelName = modelName;
	}
	// ------------------------------------  methods  --------------------------------------------------
	
	
	/**
	 * @param brandId
	 * @param modelId
	 * @return			True if desired model could be loaded
	 */
	public boolean loadModel(long brandId, long modelId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsModel = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getModel"));
            ps.setLong(1, brandId);
            ps.setLong(1, modelId);
            rsModel = ps.executeQuery();
            if((rsModel != null) && (rsModel.next()))
            {
        		this.m_ModelId = rsModel.getLong("terminalModelId");
        		this.m_ModelName = rsModel.getString("modelName");
        		retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsModel != null )
    		{
    			try { rsModel.close(); }
    			catch (Exception ex) {}
    		}
    		rsModel = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	
	
	/**
	 * @param brandId	Parent models brand
	 * @param modelId	
	 * @return			Null if no model could be retrived
	 */
	public static CTerminalModel getTerminalModel(long brandId, long modelId)
	{
		CTerminalModel retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsModel = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        ps = dbConn.prepareStatement(sql_bundle.getString("getModel"));
            ps.setLong(1, brandId);
            ps.setLong(2, modelId);
            rsModel = ps.executeQuery();
            if((rsModel != null) && (rsModel.next()))
            {
            	retValue = new CTerminalModel();
            	retValue.setModelId(modelId);
            	retValue.setModelName(rsModel.getString("modelName"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsModel != null )
    		{
    			try { rsModel.close(); }
    			catch (Exception ex) {}
    		}
    		rsModel = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;		
	}
}
