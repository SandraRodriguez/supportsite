/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 * This class contains a Terminal status information
 */
public class CTerminalStatus 
{
	// ------------------------------------  constants -------------------------------------------------
	public static final String NEW_CODE = "NEW";					//	New
	public static final String READY_CODE = "READY";				//	Ready
	public static final String ASSIGN_CODE = "ASSIGN";				//	Assigned
	public static final String TO_MERCHANT_CODE = "TO_MERCHANT";	//	In Transit to Merchant
	public static final String IN_MERCHANT_CODE = "IN_MERCHANT";	//	In Merchant Location
	public static final String PENDING_CODE = "PENDING";			//	Pending
	public static final String DAMAGED_CODE = "DAMAGED";			//	Damaged
	public static final String RETURNED_CODE = "RETURNED";			//	Returned
	public static final String TO_OFFICE_CODE = "TO_OFFICE";		//	In Transit To Office
	public static final String DISCONT_CODE = "DISCONT";			//	Discontinued
	public static final String TO_REPAIR_CODE = "TO_REPAIR";		//	In Transit to Repair
	public static final String IN_REPAIR_CODE = "IN_REPAIR";		//	In Repair
	public static final String LOST_CODE = "LOST";					//	Lost
	// ------------------------------------  attributes ------------------------------------------------
	private long m_TerminalSatusId;
	private String m_TerminalSatusCode;
	private String m_TerminalStatusDescription;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CTerminalStatus.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_TerminalSatusId
	 */
	public long getTerminalSatusId() 
	{
		return m_TerminalSatusId;
	}
	/**
	 * @param terminalSatusId the m_TerminalSatusId to set
	 */
	public void setTerminalSatusId(long terminalSatusId) 
	{
		m_TerminalSatusId = terminalSatusId;
	}
	/**
	 * @return the m_TerminalSatusCode
	 */
	public String getTerminalSatusCode() 
	{
		return m_TerminalSatusCode;
	}
	/**
	 * @param terminalSatusCode the m_TerminalSatusCode to set
	 */
	public void setTerminalSatusCode(String terminalSatusCode) 
	{
		m_TerminalSatusCode = terminalSatusCode;
	}
	/**
	 * @return the m_TerminalStatusDescription
	 */
	public String getTerminalStatusDescription() 
	{
		return m_TerminalStatusDescription;
	}
	/**
	 * @param terminalStatusDescription the m_TerminalStatusDescription to set
	 */
	public void setTerminalStatusDescription(String terminalStatusDescription) 
	{
		m_TerminalStatusDescription = terminalStatusDescription;
	}
	
	// ------------------------------------  methods  --------------------------------------------------
	
	public boolean load(long statusId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsStatus = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getTerminalStatusById"));
            ps.setLong(1, statusId);
            rsStatus = ps.executeQuery();
            if((rsStatus != null) && (rsStatus.next()))
            {
        		this.m_TerminalSatusId= rsStatus.getLong("terminalStatusId");
        		this.m_TerminalSatusCode = rsStatus.getString("code");
        		this.m_TerminalStatusDescription = rsStatus.getString("description");
        		retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsStatus != null )
    		{
    			try { rsStatus.close(); }
    			catch (Exception ex) {}
    		}
    		rsStatus = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	
	
	/**
	 * @return Return the list of all terminal status
	 */
	public static CTerminalStatus[] getAllTerminalStatus()
	{
		CTerminalStatus[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminalStatus = null;
		ArrayList<CTerminalStatus> tmpList = new ArrayList<CTerminalStatus>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getTerminalStatus"));
	        rsTerminalStatus = ps.executeQuery();
            if(rsTerminalStatus != null)
            {
            	while(rsTerminalStatus.next())
            	{
            		CTerminalStatus newTerminalStatus = new CTerminalStatus();
            		newTerminalStatus.setTerminalSatusId(rsTerminalStatus.getLong("terminalStatusId"));
            		newTerminalStatus.setTerminalSatusCode(rsTerminalStatus.getString("code"));
            		newTerminalStatus.setTerminalStatusDescription(rsTerminalStatus.getString("description"));
            		tmpList.add(newTerminalStatus);
            	}	
            	retValue = new CTerminalStatus[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsTerminalStatus != null )
    		{
    			try { rsTerminalStatus.close(); }
    			catch (Exception ex) {}
    		}
    		rsTerminalStatus = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;

	}

	
	/**
	 * @param partStatusCode	
	 * @return					The part status object which code is the same as the parameter given 
	 */
	public static CTerminalStatus getTerminalStatusByCode(String statusCode)
	{
		CTerminalStatus retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminalStatus = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getTerminalStatusByCode"));
	        ps.setString(1, statusCode);
	        rsTerminalStatus = ps.executeQuery();
            if((rsTerminalStatus != null) && (rsTerminalStatus.next()))
            {
        		retValue = new CTerminalStatus();
        		retValue.setTerminalSatusId(rsTerminalStatus.getLong("terminalStatusId"));
        		retValue.setTerminalSatusCode(rsTerminalStatus.getString("code"));
        		retValue.setTerminalStatusDescription(rsTerminalStatus.getString("description"));
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsTerminalStatus != null )
    		{
    			try { rsTerminalStatus.close(); }
    			catch (Exception ex) {}
    		}
    		rsTerminalStatus = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

}
