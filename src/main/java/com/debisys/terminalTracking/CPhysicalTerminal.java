/**
 * 
 */
package com.debisys.terminalTracking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.TerminalException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DbUtil;

/**
 * @author fernandob 2008-06-11 This class contains all information related to a
 *         physical terminal. Not to be confused to logical terminals
 */
public class CPhysicalTerminal 
{
	// ------------------------------------ constants -------------------------------------------------
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DEFAULT_SMALLDATE_FORMAT = "yyyy-MM-dd";
	// ------------------------------------ attributes ------------------------------------------------
	private long m_TerminalId;
	private String m_SerialNumber;
	private CTerminalBrand m_Brand;
	private CTerminalModel m_Model;
	private CApplicationType m_ApplicationType;
	private String m_SoftwareLicense;
	private ArrayList<CCommunicationType> m_CommunicationTypeSet = new ArrayList<CCommunicationType>();
	private CCommunicationType m_PrimaryCommType; // Points to one of the
													// previous set elements
	private String m_MobileEquipmentId;
	private String m_PurchaseOrder;
	private String m_InventoryAdmitanceDate;
	private CTerminalStatus m_CurrentStatus;
	private String m_LastChangeStatusDate;
	private String m_LotNumber;
	private long m_RowNumber;
	private String m_StatusChangeReason;
	private String m_PartUnassignReason;
	private Hashtable<String, CTerminalPart> m_PartsAssigned = new Hashtable<String, CTerminalPart>();
	private static ResourceBundle sql_bundle = ResourceBundle
			.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CPhysicalTerminal.class);
	
	// paging properties
	private int m_RecordCount;
	private static String sort;
	private static String col;
	
	// filtering properties
	private String serialNumberH = "";
	private String lotNumberH = "";
	private String applicationTypeH = "";
	private String brandIdH = "";
	private String modelIdH = "";
	private String statusIdH = "";
	private String startDateH = "";
	private String endDateH = "";

	// ------------------------------------ properties 	-----------------------------------------------
	/**
	 * @return the m_TerminalId
	 */
	public long getTerminalId() {
		return m_TerminalId;
	}

	/**
	 * @param terminalId
	 *            the m_TerminalId to set
	 */
	public void setTerminalId(long terminalId) {
		m_TerminalId = terminalId;
	}

	/**
	 * @return the m_SerialNumber
	 */
	public String getSerialNumber() {
		return m_SerialNumber;
	}

	/**
	 * @param serialNumber
	 *            the m_SerialNumber to set
	 */
	public void setSerialNumber(String serialNumber) {
		m_SerialNumber = serialNumber;
	}

	/**
	 * @return the m_BrandName
	 */
	public CTerminalBrand getBrand() {
		return m_Brand;
	}

	/**
	 * @param brandName
	 *            the m_BrandName to set
	 */
	public void setBrand(CTerminalBrand brand) {
		m_Brand = brand;
	}

	/**
	 * @return the m_Model
	 */
	public CTerminalModel getModel() {
		return m_Model;
	}

	/**
	 * @param model
	 *            the m_Model to set
	 */
	public void setModel(CTerminalModel model) {
		m_Model = model;
	}

	/**
	 * @return the m_ApplicationType
	 */
	public CApplicationType getApplicationType() {
		return m_ApplicationType;
	}

	/**
	 * @param applicationType
	 *            the m_ApplicationType to set
	 */
	public void setApplicationType(CApplicationType applicationType) {
		m_ApplicationType = applicationType;
	}

	/**
	 * @return the m_SoftwareLicense
	 */
	public String getSoftwareLicense() {
		return m_SoftwareLicense;
	}

	/**
	 * @param softwareLicense
	 *            the m_SoftwareLicense to set
	 */
	public void setSoftwareLicense(String softwareLicense) {
		m_SoftwareLicense = softwareLicense;
	}

	/**
	 * @return an array of supported communication types
	 */
	public ArrayList<CCommunicationType> getCommunicationTypeSet() {
        return m_CommunicationTypeSet;
	}


	/**
	 * @return the m_PrimaryCommType
	 */
	public CCommunicationType getPrimaryCommType() {
		return m_PrimaryCommType;
	}

	/**
	 * @return the m_MobileEquipmentId
	 */
	public String getMobileEquipmentId() {
		return m_MobileEquipmentId;
	}

	/**
	 * @param mobileEquipmentId
	 *            the m_MobileEquipmentId to set
	 */
	public void setMobileEquipmentId(String mobileEquipmentId) {
		m_MobileEquipmentId = mobileEquipmentId;
	}

	/**
	 * @return the m_PurchaseOrder
	 */
	public String getPurchaseOrder() {
		return m_PurchaseOrder;
	}

	/**
	 * @param purchaseOrder
	 *            the m_PurchaseOrder to set
	 */
	public void setPurchaseOrder(String purchaseOrder) {
		m_PurchaseOrder = purchaseOrder;
	}

	/**
	 * @return the m_InventoryAdmitanceDate
	 */
	public String getInventoryAdmitanceDate() {
		return m_InventoryAdmitanceDate;
	}

	/**
	 * @param inventoryAdmitanceDate
	 *            the m_InventoryAdmitanceDate to set
	 */
	public void setInventoryAdmitanceDate(String inventoryAdmitanceDate) {
		m_InventoryAdmitanceDate = inventoryAdmitanceDate;
	}

	/**
	 * @return the m_CurrentStatus
	 */
	public CTerminalStatus getCurrentStatus() {
		return m_CurrentStatus;
	}

	/**
	 * @param currentStatus
	 *            the m_CurrentStatus to set
	 */
	public void setCurrentStatus(CTerminalStatus currentStatus) {
		m_CurrentStatus = currentStatus;
	}

	/**
	 * @return the m_LastChangeStatusDate
	 */
	public String getLastChangeStatusDate() {
		return m_LastChangeStatusDate;
	}

	/**
	 * @param lastChangeStatusDate
	 *            the m_LastChangeStatusDate to set
	 */
	public void setLastChangeStatusDate(String lastChangeStatusDate) {
		m_LastChangeStatusDate = lastChangeStatusDate;
	}

	/**
	 * @return the m_LotNumber
	 */
	public String getLotNumber() {
		return m_LotNumber;
	}

	/**
	 * @param lotNumber
	 *            the m_LotNumber to set
	 */
	public void setLotNumber(String lotNumber) {
		m_LotNumber = lotNumber;
	}

	/**
	 * @return the m_RowNumber
	 */
	public long getRowNumber() {
		return m_RowNumber;
	}

	/**
	 * @param rowNumber
	 *            the m_RowNumber to set
	 */
	public void setRowNumber(long rowNumber) {
		m_RowNumber = rowNumber;
	}

	/**
	 * @return an array of assigned parts
	 */
	public Hashtable<String,CTerminalPart> getAssignedParts() 
	{
		return this.m_PartsAssigned;
	}

	/**
	 * @return the m_StatusChangeReason
	 */
	public String getStatusChangeReason() {
		return m_StatusChangeReason;
	}

	/**
	 * @param statusChangeReason
	 *            the m_StatusChangeReason to set
	 */
	public void setStatusChangeReason(String statusChangeReason) {
		m_StatusChangeReason = statusChangeReason;
	}

	/**
	 * @return the m_PartUnassignReason
	 */
	public String getPartUnassignReason() {
		return m_PartUnassignReason;
	}

	/**
	 * @param partUnassignReason
	 *            the m_PartUnassignReason to set
	 */
	public void setPartUnassignReason(String partUnassignReason) {
		m_PartUnassignReason = partUnassignReason;
	}
	
	public void setSort(String _sort) {
		sort = _sort;
	}
	
	public String getSort() {
		return sort;
	}
	
	public void setCol(String _col) {
		col = _col;
	}
	
	public String getCol() {
		return col;
	}
	
	public int getRecordCount() {
		return m_RecordCount;
	}
	
	public String getSerialNumberH() {
		return serialNumberH;
	}
	
	public void setSerialNumberH(String s) {
		serialNumberH = s;
	}
	
	public String getLotNumberH() {
		return lotNumberH;
	}
	
	public void setLotNumberH(String s) {
		lotNumberH = s;
	}
	
	public String getApplicationTypeH() {
		return applicationTypeH;
	}
	
	public void setApplicationTypeH(String s) {
		applicationTypeH = s;
	}
	
	public String getBrandIdH() {
		return brandIdH;
	}
	
	public void setBrandIdH(String s) {
		brandIdH = s;
	}
	
	public String getModelIdH() {
		return modelIdH;
	}
	
	public void setModelIdH(String s) {
		modelIdH = s;
	}
	
	public String getStatusIdH() {
		return statusIdH;
	}
	
	public void setStatusIdH(String s) {
		statusIdH = s;
	}
	
	public String getStartDateH() {
		return startDateH;
	}
	
	public void setStartDateH(String s) {
		startDateH = s;
	}
	
	public String getEndDateH() {
		return endDateH;
	}
	
	public void setEndDateH(String s) {
		endDateH = s;
	}

	// ------------------------------------ methods --------------------------------------------------

	/**
	 * @param terminalId
	 *            Terminal ID to get all information to fill the object
	 * @return True if all the information could be obtained
	 */
	public boolean loadTerminal(long terminalId) {
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn.prepareStatement(sql_bundle.getString("getTerminal"));
			ps.setLong(1, terminalId);
			ps.setNull(2, java.sql.Types.VARCHAR);
			ps.setNull(3, java.sql.Types.VARCHAR);
			rsTerminal = ps.executeQuery();
			if ((rsTerminal != null) && (rsTerminal.next())) {
				this.m_TerminalId = terminalId;
				this.m_SerialNumber = rsTerminal.getString("serialNumber");
				this.m_Brand = new CTerminalBrand();
				if (!this.m_Brand.loadBrand(rsTerminal.getLong("brandId"))) {
					throw new Exception("Terminal brand could not be obtained");
				}
				this.m_Model = CTerminalModel.getTerminalModel(this.m_Brand
						.getBrandId(), rsTerminal.getLong("modelId"));
				if (this.m_Model == null) {
					throw new Exception("Terminal model could not be obtained");
				}
				this.m_ApplicationType = new CApplicationType();
				this.m_ApplicationType.setApplicationTypeId(rsTerminal.getLong("applicationTypeId"));
				this.m_ApplicationType.setApplicationTypeCode(rsTerminal.getString("applicationTypeCode"));
				this.m_ApplicationType.setApplicationTypeDescription(rsTerminal.getString("applicationTypeDescription"));
				this.m_SoftwareLicense = (rsTerminal.getString("softwareLicense") != null)?rsTerminal.getString("softwareLicense"):"";
				this.m_SoftwareLicense = (this.m_SoftwareLicense == null)?"":this.m_SoftwareLicense;
				if (!this.getTerminalCommTypes(this.m_TerminalId)) throw new Exception("Terminal communication types could not be obtained");
				this.m_MobileEquipmentId = (rsTerminal.getString("mobileEquipmentId") != null)?rsTerminal.getString("mobileEquipmentId"):"";
				this.m_PurchaseOrder = rsTerminal.getString("purchaseOrder");
				this.m_InventoryAdmitanceDate = rsTerminal.getString("inventoryAdmitanceDate");
				this.m_CurrentStatus = new CTerminalStatus();
				this.m_CurrentStatus.setTerminalSatusId(rsTerminal.getLong("currentStatus"));
				this.m_CurrentStatus.setTerminalSatusCode(rsTerminal.getString("statusCode"));
				this.m_CurrentStatus.setTerminalStatusDescription(rsTerminal.getString("statusDescription"));
				this.m_LastChangeStatusDate = rsTerminal.getString("lastChangeStatusDate");
				this.m_LotNumber = rsTerminal.getString("lotNumber");
				this.m_RowNumber = 1;
				this.loadTerminalParts();

				retValue = true;
			}
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (rsTerminal != null) {
				try {
					rsTerminal.close();
				} catch (Exception ex) {
				}
			}
			rsTerminal = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @return True if communication types assigned to this terminal could be
	 *         retrived
	 */
	public boolean getTerminalCommTypes(long terminalId) {
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsCommTypes = null;

		if (this.m_CommunicationTypeSet != null) {
			this.m_CommunicationTypeSet.clear();
		} else {
			this.m_CommunicationTypeSet = new ArrayList<CCommunicationType>();
		}
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn.prepareStatement(sql_bundle
					.getString("getTerminalCommTypes"));
			ps.setLong(1, terminalId);
			rsCommTypes = ps.executeQuery();
			if (rsCommTypes != null) {
				while (rsCommTypes.next()) {
					CCommunicationType newCommType = new CCommunicationType();
					newCommType.setCommunicationTypeId(rsCommTypes
							.getLong("communicationTypeId"));
					newCommType.setCommunicationTypeCode(rsCommTypes
							.getString("code"));
					newCommType.setCommunicationTypeDescription(rsCommTypes
							.getString("description"));
					newCommType.setIsPrimaryCommType(rsCommTypes
							.getBoolean("isPrimaryCommType"));
					this.m_CommunicationTypeSet.add(newCommType);
					if (newCommType.getIsPrimaryCommType())
						this.m_PrimaryCommType = newCommType;
				}
				retValue = true;
			}
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (rsCommTypes != null) {
				try {
					rsCommTypes.close();
				} catch (Exception ex) {
				}
			}
			rsCommTypes = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @return True if new terminal could be created
	 */
	public boolean createNewTerminal(boolean automaticSimInsertion,	String userId,SessionData sessionData) {
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}

			ps = dbConn.prepareStatement(sql_bundle.getString("insertTerminal"));
			ps.setString(1, this.m_SerialNumber);
			ps.setLong(2, this.m_Brand.getBrandId());
			ps.setLong(3, this.m_Model.getModelId());
			ps.setLong(4, this.m_ApplicationType.getApplicationTypeId());
			if((this.m_SoftwareLicense == null) || (this.m_SoftwareLicense.trim().equals("")))
			{
				ps.setNull(5, java.sql.Types.VARCHAR);
			}
			else
			{		
				ps.setString(5, this.m_SoftwareLicense);
			}
			if((this.m_MobileEquipmentId == null) || (this.m_MobileEquipmentId.trim().equals("")))
			{
				ps.setNull(6, java.sql.Types.VARCHAR);
			}
			else
			{		
				ps.setString(6, this.m_MobileEquipmentId);
			}
			if((this.m_PurchaseOrder == null) || (this.m_PurchaseOrder.trim().equals("")))
			{
				ps.setNull(7, java.sql.Types.VARCHAR);
			}
			else
			{		
				ps.setString(7, this.m_PurchaseOrder);
			}
			ps.setString(8, this.m_InventoryAdmitanceDate);
			ps.setString(9, this.m_InventoryAdmitanceDate);
			ps.setLong(10, this.m_CurrentStatus.getTerminalSatusId());
			ps.setString(11, dateFormat.format(Calendar.getInstance().getTime()));
			if((this.m_LotNumber == null) || (this.m_LotNumber.trim().equals("")))
			{
				ps.setNull(12, java.sql.Types.VARCHAR);
			}
			else
			{		
				ps.setString(12, this.m_LotNumber);
			}

			if (automaticSimInsertion)
			{
				this.addPart(this.getReadySimCard());
				if(this.m_PartsAssigned.size() < 1)
				{
					logger.warn(Languages.getString("jsp.admin.transactions.inventoryTerminalsAdd.partNotAssigned", sessionData.getLanguage()));
				}
			}
			ps.executeUpdate();
			Long newTerminalId = terminalExists(this.m_SerialNumber, this.m_Brand.getBrandId());

			if (newTerminalId != null) 
			{
				this.m_TerminalId = newTerminalId.longValue();
				retValue = this.createCommTypeRelations()
						&& this.assignTerminalParts()
						&& this.changeAssignedPartsStatus(userId,sessionData)
						&& CPhysicalTerminal.insertTerminalStatusChange(this.m_TerminalId,Calendar.getInstance(),this.m_CurrentStatus
												.getTerminalSatusId(),
										userId,
										Languages.getString("jsp.admin.transactions.inventoryTerminalsInsertReason", sessionData.getLanguage()));
			}
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @return True if all existing communication types could be related to
	 *         current terminal
	 */
	private boolean createCommTypeRelations() {
		boolean retValue = false;

		if ((this.m_CommunicationTypeSet != null)
				&& (this.m_CommunicationTypeSet.size() > 0)) {
			Connection dbConn = null;
			PreparedStatement ps = null;
			try {
				dbConn = Torque.getConnection(sql_bundle
						.getString("pkgDefaultDb"));

				if (!Torque.isInit()) {
					throw new Exception("Torque is not initialized");
				}

				if (dbConn == null) {
					throw new Exception("Can't get database connection");
				}
				ps = dbConn.prepareStatement(sql_bundle
						.getString("addTerminalCommType"));
				for (CCommunicationType currentCommType : this.m_CommunicationTypeSet) {
					ps.setLong(1, this.m_TerminalId);
					ps.setLong(2, currentCommType.getCommunicationTypeId());
					ps.setBoolean(3, currentCommType.getIsPrimaryCommType());
					ps.executeUpdate();
				}
				retValue = true;
			} catch (java.lang.Exception localException) {
				logger.error(localException.toString());
				retValue = false;
			} finally {
				if (ps != null) {
					try {
						ps.close();
					} catch (Exception ex) {
					}
				}
				ps = null;
				if (dbConn != null)

				{
					try {
						dbConn.close();
					} catch (Exception ex) {
					}
				}
				dbConn = null;
			}
		}
		return retValue;
	}

	/**
	 * @param automaticSimInsertion		If applies, a sim card in READY status will be assigned to the current terminal 
	 * @param userId					Session user that is doing the changes
	 * @param partIdToRemove			A given part ID to be removed from current part list. This one 
	 * @return							True if terminal update could be done
	 */
	public boolean updateCurrentTerminal(boolean automaticSimInsertion, String userId, Long partIdToRemove,SessionData sessionData) 
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		PreparedStatement psDeleteComms = null;
		PreparedStatement psDeleteParts = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

		try 
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) 
			{
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) 
			{
				throw new Exception("Can't get database connection");
			}
			CPhysicalTerminal originalTerminal = new CPhysicalTerminal();
			originalTerminal.loadTerminal(this.m_TerminalId); // This must be done before deleting relations
			psDeleteComms = dbConn.prepareStatement(sql_bundle.getString("removeTerminalCommTypes"));
			psDeleteComms.setLong(1, this.m_TerminalId);
			psDeleteParts = dbConn.prepareStatement(sql_bundle.getString("deleteTerminalParts"));
			psDeleteParts.setLong(1, this.m_TerminalId);
            // Remove communication types from DB             
            psDeleteComms.execute();
            psDeleteComms.close();
            
            // Delete part assignment records from DB
            psDeleteParts.execute();
            psDeleteParts.close();

            // If a part ID is given for remove, the part will be deleted from the list - current list already contains assigned parts
            if((partIdToRemove != null) && (this.m_PartsAssigned != null))
            {            	
                String partKey = null;
                for(CTerminalPart currentPart:this.m_PartsAssigned.values())
                {
                    if(currentPart.getPartId() == partIdToRemove)
                    {
                        partKey = currentPart.getKey();
                        break;
                    }
                }
                if(partKey != null) this.m_PartsAssigned.remove(partKey);
            }
            // Terminal record update
            ps = dbConn.prepareStatement(sql_bundle.getString("updateTerminal"));
            ps.setString(1, this.m_SerialNumber);
            ps.setLong(2, this.m_Brand.getBrandId());
            ps.setLong(3, this.m_Model.getModelId());
            ps.setLong(4, this.m_ApplicationType.getApplicationTypeId());
            if((this.m_SoftwareLicense == null) || (this.m_SoftwareLicense.equalsIgnoreCase("")))
            {
            	ps.setNull(5, java.sql.Types.VARCHAR);
            }
            else
            {
            	ps.setString(5, this.m_SoftwareLicense);
            }
            ps.setString(6, this.m_MobileEquipmentId);
            if((this.m_PurchaseOrder == null) || (this.m_PurchaseOrder.equalsIgnoreCase("")))
            {
            	ps.setNull(7, java.sql.Types.VARCHAR);
            }
            else
            {            
            	ps.setString(7, this.m_PurchaseOrder);
            }
            ps.setString(8, this.m_InventoryAdmitanceDate);
            ps.setString(9, this.m_InventoryAdmitanceDate);
            ps.setLong(10, this.m_CurrentStatus.getTerminalSatusId());
            ps.setString(11, dateFormat.format(Calendar.getInstance().getTime()));
            if((this.m_LotNumber == null) || (this.m_LotNumber.equalsIgnoreCase("")))
            {
            	ps.setNull(12, java.sql.Types.VARCHAR);
            }
            else
            {            
            	ps.setString(12, this.m_LotNumber);
            }
            ps.setLong(13, this.m_TerminalId);
            ps.executeUpdate();
            // Assign a new SIM card if asked to, but if terminal type apply
            if (automaticSimInsertion) this.addPart(this.getReadySimCard());
            // If status changed, a new historic record is inserted
            if (this.m_CurrentStatus.getTerminalSatusId() != originalTerminal.getCurrentStatus().getTerminalSatusId()) 
            {
                CPhysicalTerminal.insertTerminalStatusChange(this.m_TerminalId, Calendar.getInstance(),
                                this.m_CurrentStatus.getTerminalSatusId(), userId,
                                this.m_StatusChangeReason);
            }

            // Go to previous assigned parts and change their status, they will be deleted from this terminal relation
            CTerminalPartStatus readyStatus = CTerminalPartStatus.getPartStatusByCode(CTerminalPartStatus.READY_CODE);
            for (CTerminalPart currentPart : originalTerminal.m_PartsAssigned.values()) 
            {    
            	// If original part assignment is not included at present part assignment, that part must be returned to ready status 
                if (!this.m_PartsAssigned.containsKey(currentPart.getKey())) 
                {
                    currentPart.changeStatus(readyStatus.getPartStatusId());
                    CTerminalPart.insertPartStatusChange(currentPart.getPartId(), Calendar.getInstance(),
                                    readyStatus.getPartStatusId(), userId, Languages.getString("jsp.admin.transactions.inventorySimsAutoUnAssignReason", sessionData.getLanguage()));
                }
            }

            // Go through current assigned parts, and for those new ones a new historic record should be added
            CTerminalPartStatus assignedStatus = CTerminalPartStatus.getPartStatusByCode(CTerminalPartStatus.ASSIGN_CODE);
            for (CTerminalPart currentPart : this.m_PartsAssigned.values()) 
            {
                if (!originalTerminal.m_PartsAssigned.containsKey(currentPart.getKey())) 
                {
                    currentPart.changeStatus(assignedStatus.getPartStatusId());
                    CTerminalPart.insertPartStatusChange(currentPart.getPartId(), Calendar.getInstance(),
                                    assignedStatus.getPartStatusId(), userId,Languages.getString("jsp.admin.transactions.inventorySimsAutoAssignReason", sessionData.getLanguage()));
                }
            }

            retValue = this.createCommTypeRelations() && this.assignTerminalParts();

		} 
		catch (java.lang.Exception localException) 
		{
			logger.error(localException.toString());
			retValue = false;
		} 
		finally 
		{
			if (psDeleteComms != null) {
				try {
					psDeleteComms.close();
				} catch (Exception ex) {
				}
			}
			psDeleteComms = null;
			if (psDeleteParts != null) {
				try {
					psDeleteParts.close();
				} catch (Exception ex) {
				}
			}
			psDeleteParts = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @return True if current terminal assigned parts could be obtained
	 */
	public boolean loadTerminalParts() {
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsParts = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);

		if (this.m_PartsAssigned != null) {
			this.m_PartsAssigned.clear();
		}
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn.prepareStatement(sql_bundle.getString("getTerminalParts"));
			ps.setLong(1, this.m_TerminalId);
			rsParts = ps.executeQuery();
			if (rsParts != null) {
				int rowIndex = 1;
				while (rsParts.next()) {
					CSimCard newSimCard = new CSimCard();
					newSimCard.setPartId(rsParts.getLong("partId"));
					CTerminalPartType newPartType = new CTerminalPartType();
					newPartType.setPartTypeId(rsParts.getLong("partType"));
					newPartType.setPartTypeCode(rsParts
							.getString("partTypeCode"));
					newPartType.setPartDescription(rsParts
							.getString("partTypeDescription"));
					newSimCard.setPartType(newPartType);
					newSimCard.setMobileSuscriberId(rsParts.getString("mobileSuscriberId"));
					newSimCard.setPhoneNumber(rsParts.getString("phoneNumber"));
					newSimCard.setProvider(new CProvider());
					newSimCard.getProvider().loadProvider(
							rsParts.getLong("providerId"));
					newSimCard.setActivationDate(rsParts
							.getString("activationDate"));
					newSimCard.setLotNumber(rsParts.getString("lotNumber"));
					newSimCard.setContractNumber(rsParts
							.getString("contractNumber"));
					newSimCard.setContractDate(rsParts
							.getString("contractDate"));
					newSimCard.setStatus(new CTerminalPartStatus());
					newSimCard.getStatus().setPartStatusId(
							rsParts.getLong("currentStatus"));
					newSimCard.getStatus().setPartStatusCode(
							rsParts.getString("statusCode"));
					newSimCard.getStatus().setPartStatusDescription(
							rsParts.getString("statusDescription"));
					newSimCard.setLastChangeStatusDate(Calendar.getInstance());
					newSimCard.getLastChangeStatusDate().setTime(
							dateFormat.parse(rsParts
									.getString("lastChangeStatusDate")));
					newSimCard.setRowNumber(rowIndex++);
					this.m_PartsAssigned.put(newSimCard.getKey(), newSimCard);
				}
				retValue = true;
			}
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (rsParts != null) {
				try {
					rsParts.close();
				} catch (Exception ex) {
				}
			}
			rsParts = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @param terminalId
	 *            Terminal that status is going to be changed
	 * @param lastChangeDate
	 *            Current date
	 * @param newSatatusId
	 *            New valid status. No additional validations are done here
	 * @param userId
	 *            password id from passwords table
	 * @param changeReason
	 *            Text indicating the change reason
	 * @return True if new terminal status change record could be inserted
	 */
	public static boolean insertTerminalStatusChange(long terminalId,
			Calendar lastChangeDate, long newSatatusId, String userId,
			String changeReason) {
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		SimpleDateFormat smallDateFormat = new SimpleDateFormat(
				DEFAULT_SMALLDATE_FORMAT);

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}

			ps = dbConn.prepareStatement(sql_bundle
					.getString("insertTerminalStatusChange"));
			ps.setLong(1, terminalId);
			ps.setString(2, dateFormat.format(lastChangeDate.getTime()));
			ps.setString(3, smallDateFormat.format(lastChangeDate.getTime()));
			ps.setLong(4, newSatatusId);
			ps.setString(5, userId);
			ps.setString(6, changeReason);
			ps.executeUpdate();
			retValue = true;
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @param currentStatusId
	 *            Use -1 if no status ID is available
	 * @param currentStatusCode
	 *            Use null if no status code is available If both parameters are
	 *            "null" there wont be any results. No default values are
	 *            assumed
	 * @return An array of allowed status to go from the current status given
	 */
	public static CTerminalStatus[] getAllowedStatus(long currentStatusId,
			String currentStatusCode) {
		CTerminalStatus[] retValue = null;
		Connection dbConn = null;
		ResultSet rsStatusAllowed = null;
		PreparedStatement ps = null;
		ArrayList<CTerminalStatus> tmpList = new ArrayList<CTerminalStatus>();

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}

			ps = dbConn.prepareStatement(sql_bundle
					.getString("getTerminalAllowedStatus"));
			if ((currentStatusCode != null) && (!currentStatusCode.equals(""))) 
			{
				ps.setString(1, currentStatusCode);
			} 
			else 
			{
				ps.setNull(1, java.sql.Types.VARCHAR);
			}
			if (currentStatusId != -1) 
			{
				ps.setLong(2, currentStatusId);
			} 
			else 
			{
				ps.setNull(2, java.sql.Types.INTEGER);
			}
			rsStatusAllowed = ps.executeQuery();
			if (rsStatusAllowed != null) {
				while (rsStatusAllowed.next()) {
					CTerminalStatus newStatus = new CTerminalStatus();
					newStatus.setTerminalSatusId(rsStatusAllowed
							.getLong("targetStatusId"));
					newStatus.setTerminalSatusCode(rsStatusAllowed
							.getString("statusCode"));
					newStatus.setTerminalStatusDescription(rsStatusAllowed
							.getString("statusDescription"));
					tmpList.add(newStatus);
				}
				retValue = new CTerminalStatus[tmpList.size()];
				retValue = tmpList.toArray(retValue);
				tmpList.clear();
				tmpList = null;
			}
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = null;
		} finally {
			if (rsStatusAllowed != null) {
				try {
					rsStatusAllowed.close();
				} catch (Exception ex) {
				}
			}
			rsStatusAllowed = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @param serialNumber
	 * @param applicationTypeId
	 * @param lotNumber
	 * @param brandName
	 *            Builder's name
	 * @param model
	 * @param statusId
	 *            Terminal current status
	 * @param startAdmitanceDate
	 *            Start time limit to select records
	 * @param endAdmitanceDate
	 *            End time limit to select records
	 * @param startRecordIndex
	 *            Paging offset
	 * @param pageLength
	 *            How many records per page
	 * @return A physical terminal list based on the given parameters
	 */
	public CPhysicalTerminal[] getTerminalList(String serialNumber,
			Long applicationTypeId, String lotNumber, Long brandId,
			Long modelId, Long statusId, String startAdmitanceDate,
			String endAdmitanceDate, int pageSize, int page) { //Long startRecordIndex, Long pageLength,Long recordCount
		logger.debug("*** getting Terminal list with parameters: serialNumber="+serialNumber+", applicationTypeId="+applicationTypeId+
				", lotNumber="+lotNumber+", brandId="+brandId+", modelId="+modelId+", statusId="+statusId+", startAdmitanceDate="+startAdmitanceDate+
				", endAdmitanceDate="+endAdmitanceDate+", pageSize="+pageSize+", page="+page);
		CPhysicalTerminal[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;
		ArrayList<CPhysicalTerminal> tmpList = new ArrayList<CPhysicalTerminal>();

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			
			String strSQL = sql_bundle.getString("getPhysicalTerminalsListQuery");
			
			// Configure prepared statement parameters dynamically
			java.util.Hashtable<String, Integer> selectFilters = new java.util.Hashtable<String, Integer>();
			int filterIndex = 1;
			if (serialNumber != null || (serialNumberH != null && !serialNumberH.equals("") && !serialNumberH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " serialNumber=?";
				selectFilters.put("serialNumber", new Integer(filterIndex++));
			}
			if (applicationTypeId != -1 || (applicationTypeH != null && !applicationTypeH.equals("") && !applicationTypeH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " PT.applicationTypeId=?";
				selectFilters.put("applicationTypeId", new Integer(filterIndex++));
			}
			if (lotNumber != null || (lotNumberH != null && !lotNumberH.equals("") && !lotNumberH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " lotNumber=?";
				selectFilters.put("lotNumber", new Integer(filterIndex++));
			}
			if (brandId != -1 || (brandIdH != null && !brandIdH.equals("") && !brandIdH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " brandId=?";
				selectFilters.put("brandId", new Integer(filterIndex++));
			}
			if (modelId != -1 || (modelIdH != null && !modelIdH.equals("") && !modelIdH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " modelId=?";
				selectFilters.put("modelId", new Integer(filterIndex++));
			}
			if (statusId != -1 || (statusIdH != null && !statusIdH.equals("") && !statusIdH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " terminalStatusId=?";
				selectFilters.put("statusId", new Integer(filterIndex++));
			}
			if (((startAdmitanceDate != null) && (!startAdmitanceDate.equals(""))) || (startDateH != null && !startDateH.equals("") && !startDateH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " inventoryAdmitanceDate>=?";
				selectFilters.put("startAdmitanceDate", new Integer(filterIndex++));
			}
			if (((endAdmitanceDate != null) && (!endAdmitanceDate.equals(""))) || (endDateH != null && !endDateH.equals("") && !endDateH.equals("-1"))) {
				strSQL += (selectFilters.isEmpty()? " where": " and") + " inventoryAdmitanceDate<=?";
				selectFilters.put("endAdmitanceDate", new Integer(filterIndex++));
			}
			// config the sorting
			int intCol = 0;
			int intSort = 0;
			try {
			  if (col != null && sort != null && !col.equals("") && !sort.equals("")) {
			    intCol = Integer.parseInt(col);
			    intSort = Integer.parseInt(sort);
			  }
			} catch (NumberFormatException nfe) {
			  intCol = 0;
			  intSort = 0;
			}

			String strSort = (intSort == 2)? "DESC": "ASC";
			String strCol = "";
			switch (intCol) {
			   case 1: strCol = "serialNumber"; break;
			   case 2: strCol = "brandId"; break;
			   case 3: strCol = "modelId"; break;
			   case 4: strCol = "applicationTypeDescription"; break;
			   case 5: strCol = "commTypeDescription"; break;
			   case 6: strCol = "inventoryAdmitanceDate"; break;
			   case 7: strCol = "lotNumber"; break;
			   case 8: strCol = "statusDescription"; break;
			   default: strCol = "RowNumber"; break;
			}

			strSQL += " order by " + strCol + " " + strSort;
			
			ps = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			if (serialNumber != null || (serialNumberH != null && !serialNumberH.equals("") && !serialNumberH.equals("-1"))) {
				ps.setString(selectFilters.get("serialNumber"), serialNumberH);
				logger.debug("*** serialNumber must be set: "+selectFilters.get("serialNumber")+", "+serialNumberH);
			}
			if (applicationTypeId != -1 || (applicationTypeH != null && !applicationTypeH.equals("") && !applicationTypeH.equals("-1"))) {
				ps.setString(selectFilters.get("applicationTypeId"), applicationTypeH);
				logger.debug("*** applicationTypeId must be set: "+selectFilters.get("applicationTypeId")+", "+applicationTypeH);
			}
			if (lotNumber != null || (lotNumberH != null && !lotNumberH.equals("") && !lotNumberH.equals("-1"))) {
				ps.setString(selectFilters.get("lotNumber"), lotNumberH);
				logger.debug("*** lotNumber must be set: "+selectFilters.get("lotNumber")+", "+lotNumberH);
			}
			if (brandId != -1 || (brandIdH != null && !brandIdH.equals("") && !brandIdH.equals("-1"))) {
				ps.setString(selectFilters.get("brandId"), brandIdH);
				logger.debug("*** brandId must be set: "+selectFilters.get("brandId")+", "+brandIdH);
			}
			if (modelId != -1 || (modelIdH != null && !modelIdH.equals("") && !modelIdH.equals("-1"))) {
				ps.setString(selectFilters.get("modelId"), modelIdH);
				logger.debug("*** modelId must be set: "+selectFilters.get("modelId")+", "+modelIdH);
			}
			if (statusId != -1 || (statusIdH != null && !statusIdH.equals("") && !statusIdH.equals("-1"))) {
				ps.setString(selectFilters.get("statusId"), statusIdH);
				logger.debug("*** statusId must be set: "+selectFilters.get("statusId")+", "+statusIdH);
			}
			if (((startAdmitanceDate != null) && (!startAdmitanceDate.equals(""))) || (startDateH != null && !startDateH.equals("") && !startDateH.equals("-1"))) {
				ps.setString(selectFilters.get("startAdmitanceDate"), startDateH);
				logger.debug("*** startAdmitanceDate must be set: "+selectFilters.get("startAdmitanceDate")+", "+startDateH);
			}
			if (((endAdmitanceDate != null) && (!endAdmitanceDate.equals(""))) || (endDateH != null && !endDateH.equals("") && !endDateH.equals("-1"))) {
				ps.setString(selectFilters.get("endAdmitanceDate"), endDateH);
				logger.debug("*** endAdmitanceDate must be set: "+selectFilters.get("endAdmitanceDate")+", "+endDateH);
			}
			logger.debug("*** SQL sentence for Terminal inventory search: "+ strSQL);
			rsTerminal = ps.executeQuery();
			// Get total record count
			int intRecordCount = DbUtil.getRecordCount(rsTerminal);
			
			int rowNumber = DbUtil.getRowNumber(rsTerminal, pageSize, intRecordCount, page);
			logger.debug("*** calculating row position in the resultset. RecordsPerPage=" + pageSize + " RecordCount=" + intRecordCount + " PageNumber=" + page + " rowAssigned=" + rowNumber);
			if (intRecordCount > 0) {
			    rsTerminal.absolute(rowNumber);
			    // Get Detail
			    for (int i = 0; i < pageSize; i++) {
				CPhysicalTerminal newTerminal = new CPhysicalTerminal();
				newTerminal.setTerminalId(rsTerminal
					.getLong("terminalId"));
				newTerminal.setSerialNumber(rsTerminal
					.getString("serialNumber"));
				CTerminalBrand currentBrand = new CTerminalBrand();
				currentBrand.loadBrand(rsTerminal
					.getLong("brandId"));
				newTerminal.setBrand(currentBrand);
				newTerminal.setModel(CTerminalModel
					.getTerminalModel(currentBrand
						.getBrandId(), rsTerminal
						.getLong("modelId")));
				newTerminal
					.setApplicationType(new CApplicationType());
				newTerminal
					.getApplicationType()
					.setApplicationTypeId(
						rsTerminal
							.getLong("applicationTypeId"));
				newTerminal
					.getApplicationType()
					.setApplicationTypeCode(
						rsTerminal
							.getString("applicationTypeCode"));
				newTerminal
					.getApplicationType()
					.setApplicationTypeDescription(
						rsTerminal
							.getString("applicationTypeDescription"));
				newTerminal.setSoftwareLicense(rsTerminal
					.getString("softwareLicense"));
				if (!newTerminal
					.getTerminalCommTypes(newTerminal
						.getTerminalId()))
				    throw new Exception(
					    "Terminal communication types could not be obtained");
				newTerminal.setMobileEquipmentId(rsTerminal
					.getString("mobileEquipmentId"));
				newTerminal.setPurchaseOrder(rsTerminal
					.getString("purchaseOrder"));
				newTerminal
					.setInventoryAdmitanceDate(rsTerminal
						.getString("inventoryAdmitanceDate"));
				newTerminal
					.setCurrentStatus(new CTerminalStatus());
				newTerminal
					.getCurrentStatus()
					.setTerminalSatusId(
						rsTerminal
							.getLong("currentStatus"));
				newTerminal
					.getCurrentStatus()
					.setTerminalSatusCode(
						rsTerminal
							.getString("statusCode"));
				newTerminal
					.getCurrentStatus()
					.setTerminalStatusDescription(
						rsTerminal
							.getString("statusDescription"));
				newTerminal.setLastChangeStatusDate(rsTerminal
					.getString("lastChangeStatusDate"));
				newTerminal.setLotNumber(rsTerminal
					.getString("lotNumber"));
				newTerminal.setRowNumber(rsTerminal
					.getLong("RowNumber"));
				newTerminal.m_RecordCount = intRecordCount;
				tmpList.add(newTerminal);
				if (!rsTerminal.next()) {
				    break;
				}
			    } 
			} else {
			    logger.info("[getTerminalList] No records found, no paging operations performed.");
			}
			    retValue = new CPhysicalTerminal[tmpList.size()];
			    retValue = tmpList.toArray(retValue);
			    tmpList.clear();
			    tmpList = null;
			
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = null;
		} finally {
			if (rsTerminal != null) {
				try {
					rsTerminal.close();
				} catch (Exception ex) {
				}
			}
			rsTerminal = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * 
	 * @param terminalId
	 * @param serialNumber
	 * @param brandId
	 * @param modelId
	 * @param applicationTypeId
	 * @param softwareLicense
	 * @param mobileEquipmentId
	 * @param purchaseOrder
	 * @param lotNumber
	 * @return A list of terminals whose fields values can be one of the given
	 * parameters. For DB query the provided values are "OR"ed to get
	 * results
	 */

	public static CPhysicalTerminal[] searchTerminal(Long terminalId,
		String serialNumber, Long brandId, Long modelId,
		Long applicationTypeId, String softwareLicense,
		String mobileEquipmentId, String purchaseOrder, String lotNumber) 
	{

		CPhysicalTerminal[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;
		ArrayList<CPhysicalTerminal> tmpList = new ArrayList<CPhysicalTerminal>();

		try 
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (!Torque.isInit()) {

				throw new Exception("Torque is not initialized");
			}
			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}

			ps = dbConn.prepareStatement(sql_bundle.getString("searchTerminal"));

			if (terminalId != null) {
				ps.setLong(1, terminalId);
			} 
			else 
			{
				ps.setNull(1, java.sql.Types.INTEGER);
			}
			if (serialNumber != null) 
			{
				ps.setString(2, serialNumber);
			} 
			else 
			{
				ps.setNull(2, java.sql.Types.VARCHAR);
			}

			if (brandId != null) 
			{
				ps.setLong(3, brandId);
			} 
			else 
			{
				ps.setNull(3, java.sql.Types.INTEGER);
			}

			if (modelId != null) {
				ps.setLong(4, modelId);
			} 
			else 
			{
				ps.setNull(4, java.sql.Types.INTEGER);
			}

			if (applicationTypeId != null) 
			{
				ps.setLong(5, applicationTypeId);
			} 
			else 
			{
				ps.setNull(5, java.sql.Types.INTEGER);
			}

			if (softwareLicense != null) 
			{
				ps.setString(6, softwareLicense);

			} 
			else 
			{
				ps.setNull(6, java.sql.Types.VARCHAR);
			}
			if (mobileEquipmentId != null) {

				ps.setString(7, mobileEquipmentId);
			} 
			else 
			{
				ps.setNull(7, java.sql.Types.VARCHAR);
			}
			if (purchaseOrder != null) 
			{
				ps.setString(8, purchaseOrder);
			} 
			else 
			{
				ps.setNull(8, java.sql.Types.VARCHAR);
			}
			if (lotNumber != null) 
			{
				ps.setString(9, lotNumber);
			} 
			else 
			{
				ps.setNull(9, java.sql.Types.VARCHAR);
			}
			rsTerminal = ps.executeQuery();
			if (rsTerminal != null) 
			{
				while (rsTerminal.next()) 
				{
					CPhysicalTerminal newTerminal = new CPhysicalTerminal();
					newTerminal.setTerminalId(rsTerminal.getLong("terminalId"));
					newTerminal.setSerialNumber(rsTerminal.getString("serialNumber"));
					CTerminalBrand currentBrand = new CTerminalBrand();
					currentBrand.loadBrand(rsTerminal.getLong("brandId"));
					newTerminal.setBrand(currentBrand);
					newTerminal.setModel(CTerminalModel.getTerminalModel(currentBrand.getBrandId(), rsTerminal.getLong("modelId")));
					newTerminal.setApplicationType(new CApplicationType());
					newTerminal.getApplicationType().setApplicationTypeId(rsTerminal.getLong("applicationTypeId"));
					newTerminal.getApplicationType().setApplicationTypeCode(rsTerminal.getString("applicationTypeCode"));
					newTerminal.getApplicationType().setApplicationTypeDescription(rsTerminal.getString("applicationTypeDescription"));
					newTerminal.setSoftwareLicense(rsTerminal.getString("softwareLicense"));

					if (!newTerminal.getTerminalCommTypes(newTerminal.getTerminalId()))	throw new Exception("Terminal communication types could not be obtained");
					newTerminal.setMobileEquipmentId(rsTerminal.getString("mobileEquipmentId"));
					newTerminal.setPurchaseOrder(rsTerminal.getString("purchaseOrder"));
					newTerminal.setInventoryAdmitanceDate(rsTerminal.getString("inventoryAdmitanceDate"));
					newTerminal.setCurrentStatus(new CTerminalStatus());
					newTerminal.getCurrentStatus().setTerminalSatusId(rsTerminal.getLong("currentStatus"));
					newTerminal.getCurrentStatus().setTerminalSatusCode(rsTerminal.getString("statusCode"));
					newTerminal.getCurrentStatus().setTerminalStatusDescription(rsTerminal.getString("statusDescription"));
					newTerminal.setLastChangeStatusDate(rsTerminal.getString("lastChangeStatusDate"));
					newTerminal.setLotNumber(rsTerminal.getString("lotNumber"));
					tmpList.add(newTerminal);
				}
				retValue = new CPhysicalTerminal[tmpList.size()];
				retValue = tmpList.toArray(retValue);
				tmpList.clear();
				tmpList = null;
			}

		} 
		catch (java.lang.Exception localException) 
		{
			logger.error(localException.toString());
			retValue = null;
		} 
		finally 
		{
			if (rsTerminal != null) 
			{
				try 
				{
					rsTerminal.close();
				} 
				catch (Exception ex) 
				{
				}
			}
			rsTerminal = null;
			if (ps != null) 
			{
				try 
				{
					ps.close();
				} 
				catch (Exception ex) 
				{
				}
			}
			ps = null;
			if (dbConn != null)
			{
				try 
				{
					dbConn.close();
				} 
				catch (Exception ex) 
				{
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @param commTypeId
	 *            A communication ID from the assigned communication types list
	 * @return True if a primary communication type could be set. Precondition
	 *         Communication Types Set must be loaded first
	 */
	public boolean setPrimaryCommType(long commTypeId) {
		boolean retValue = false;
		if ((this.m_CommunicationTypeSet != null) && (!this.m_CommunicationTypeSet.isEmpty())) 
		{
			for (CCommunicationType comType : this.m_CommunicationTypeSet) 
			{
				if (comType.getCommunicationTypeId() == commTypeId) 
				{
					this.m_PrimaryCommType = comType;
					comType.setIsPrimaryCommType(true);
					retValue = true;
				} 
				else 
				{
					comType.setIsPrimaryCommType(false);
				}
			}
		}
		return retValue;
	}

	/**
	 * @param modelId
	 *            Model ID from the brand that is going to be set at the current
	 *            terminal model
	 * @return True if terminal model could be set
	 * 
	 * Pre: this.m_Brand != null
	 */
	public boolean setTerminalModel(long modelId) {
		boolean retValue = false;
		if ((this.m_Brand != null) && (this.m_Brand.getModelList() != null)
				&& (this.m_Brand.getModelList().size() > 0)) {
			for (CTerminalModel currentModel : this.m_Brand.getModelList()) {
				if (currentModel.getModelId() == modelId) {
					this.m_Model = currentModel;
					retValue = true;
					break;
				}
			}
		}
		return retValue;
	}

	/**
	 * @param serialNumber
	 *            This two filed are part of the terminal primary key
	 * @param brandId
	 * @param modelId
	 * @return True if a terminal with those values is found
	 */
	public static Long terminalExists(String serialNumber, long brandId) 
	{
		Long retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn
					.prepareStatement(sql_bundle.getString("terminalExists"));
			ps.setString(1, serialNumber);
			ps.setLong(2, brandId);
			rsTerminal = ps.executeQuery();
			if ((rsTerminal != null) && (rsTerminal.next())) {
				retValue = rsTerminal.getLong("terminalId");
			}
		} catch (java.lang.Exception localException) {
			logger.error(localException.toString());
			retValue = null;
		} finally {
			if (rsTerminal != null) {
				try {
					rsTerminal.close();
				} catch (Exception ex) {
				}
			}
			rsTerminal = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;

	}

	/**
	 * @param commTypeIds
	 *            An array of communication types ISs as strings
	 * @return True if operation could be completed
	 */
	public boolean setCommunicationTypeSet(String[] commTypeIds) {
		boolean retValue = false;
		if ((commTypeIds != null) && (commTypeIds.length > 0)) {
			for (String currentId : commTypeIds) {
				if ((currentId != null) && (!currentId.equals(""))) {
					long lngCurrentId = Long.valueOf(currentId);
					CCommunicationType newCommType = new CCommunicationType();
					if (newCommType.loadCommType(lngCurrentId)) {
						this.m_CommunicationTypeSet.add(newCommType);
					}
				}
			}
			if (this.m_CommunicationTypeSet.size() > 0)
				retValue = true;
		}
		return retValue;
	}

	/**
	 * @return 	True if all parts from this terminal could be added to DB
	 * 			No new historic record is added in this method, because when editing parts assignment
	 * 			there could be parts that remains the same, so no new historic record should be added
	 */
	private boolean assignTerminalParts() 
	{
		boolean retValue = true; // No part assigned can happen

		if ((this.m_PartsAssigned != null) && (this.m_PartsAssigned.size() > 0)) {
			Connection dbConn = null;
			PreparedStatement ps = null;
			try {
				dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

				if (!Torque.isInit()) 
				{
					throw new Exception("Torque is not initialized");
				}

				if (dbConn == null) 
				{
					throw new Exception("Can't get database connection");
				}
				ps = dbConn.prepareStatement(sql_bundle.getString("assignPartToTerminal"));
				for (CTerminalPart currentPart : this.m_PartsAssigned.values()) {
					CSimCard currentSim = (CSimCard) currentPart;
					ps.setLong(1, this.m_TerminalId);
					ps.setLong(2, currentSim.getPartId());
					ps.executeUpdate();
				}
			} catch (java.lang.Exception localException) {
				logger.error(localException.toString());
				retValue = false;
			} finally {
				if (ps != null) {
					try {
						ps.close();
					} catch (Exception ex) {
					}
				}
				ps = null;
				if (dbConn != null)

				{
					try {
						dbConn.close();
					} catch (Exception ex) {
					}
				}
				dbConn = null;
			}
		}
		return retValue;
	}

	/**
	 * @param userId
	 * @return True if assigned parts could be changed from READY to ASSIGNED.
	 *         Precondition This one must be used just when adding terminals
	 */
	private boolean changeAssignedPartsStatus(String userId,SessionData sessionData) {
		boolean retValue = true;

		if ((this.m_PartsAssigned != null) && (this.m_PartsAssigned.size() > 0)) 
		{
			CTerminalPartStatus assignedStatus = CTerminalPartStatus.getPartStatusByCode(CTerminalPartStatus.ASSIGN_CODE);
			if (assignedStatus != null) 
			{
				String defaultMessage = Languages.getString("jsp.admin.transactions.inventorySimsAutoAssignReason", sessionData.getLanguage());
				for (CTerminalPart currentPart : this.m_PartsAssigned.values()) 
				{
					CSimCard currentSim = (CSimCard) currentPart;
					retValue = currentSim.changeStatus(assignedStatus.getPartStatusId()) && 
						CTerminalPart.insertPartStatusChange(currentSim.getPartId(), Calendar.getInstance(), 
					            		assignedStatus.getPartStatusId(), userId, defaultMessage);
				}
			}
		}
		return retValue;
	}

	/**
	 * @return A sim card object from a record ready to be assigned
	 */
	public CSimCard getReadySimCard() {
		CSimCard retValue = null;

		if ((this.m_PrimaryCommType != null)
				&& (this.m_PrimaryCommType.getCommunicationTypeCode()
						.equals(CCommunicationType.GPRS_CODE))) {
			Connection dbConn = null;
			PreparedStatement ps = null;
			ResultSet rsPart = null;
			long partId = -1;
			try {
				dbConn = Torque.getConnection(sql_bundle
						.getString("pkgDefaultDb"));

				if (!Torque.isInit()) {
					throw new Exception("Torque is not initialized");
				}

				if (dbConn == null) {
					throw new Exception("Can't get database connection");
				}
				ps = dbConn.prepareStatement(sql_bundle
						.getString("getReadySimCard"));
				rsPart = ps.executeQuery();
				if ((rsPart != null) && (rsPart.next())) {
					partId = rsPart.getLong("partId");
					rsPart.close();
				}

				retValue = new CSimCard();
				if (!retValue.loadSimCard(partId)) {
					retValue = null;
				}
			} catch (java.lang.Exception localException) {
				logger.error(localException.toString());
				retValue = null;
			} finally {
				if (rsPart != null) {
					try {
						rsPart.close();
					} catch (Exception ex) {
					}
				}
				rsPart = null;

				if (ps != null) {
					try {
						ps.close();
					} catch (Exception ex) {
					}
				}
				ps = null;
				if (dbConn != null)

				{
					try {
						dbConn.close();
					} catch (Exception ex) {
					}
				}
				dbConn = null;
			}
		}
		return retValue;

	}

	/**
	 * @param newSimCard
	 * @return
	 */
	public boolean addPart(CSimCard newSimCard) {
		boolean retValue = false;
		if (newSimCard != null) {
			this.m_PartsAssigned.put(newSimCard.getKey(), newSimCard);
			retValue = true;
		}
		return retValue;
	}

	
	/**
	 * @return	A list of current terminal changes
	 */
	public CTerminalChange[] getTerminalChanges()
	{
		CTerminalChange[] retValue = null;
		Connection dbConn = null;    
		PreparedStatement ps = null;
		ResultSet rsTerminalChanges = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		ArrayList<CTerminalChange> tmpList = new ArrayList<CTerminalChange>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getTerminalStatusChanges"));
	        ps.setLong(1, this.m_TerminalId);	        
            rsTerminalChanges = ps.executeQuery();
            if((rsTerminalChanges != null))
            {
            	while(rsTerminalChanges.next())
            	{
            		CTerminalChange newPartChange = new CTerminalChange();
            		newPartChange.setTerminalId(this.m_TerminalId);
                        Date date = dateFormat.parse(rsTerminalChanges.getString("lastChangeDate"));
                        newPartChange.setLastChangeDate(date);
                        newPartChange.setStatusId(rsTerminalChanges.getLong("newStatusId"));
            		newPartChange.setStatusDescription(rsTerminalChanges.getString("statusDescription"));
            		newPartChange.setChangeReason(rsTerminalChanges.getString("changeReason"));
            		newPartChange.setUserId(rsTerminalChanges.getString("userId"));
	            	tmpList.add(newPartChange);
            	}
            	retValue = new CTerminalChange[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsTerminalChanges != null )
    		{
    			try { rsTerminalChanges.close(); }
    			catch (Exception ex) {}
    		}
    		rsTerminalChanges = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	/**
	 * @return A terminal object from a record ready to be assigned
	 */
	public static CPhysicalTerminal getReadyTerminal() 
	{
		CPhysicalTerminal retValue = null;

		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;
		long terminalId = -1;
		try 
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (!Torque.isInit()) 
			{
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn.prepareStatement(sql_bundle.getString("getReadyTerminal"));
			rsTerminal = ps.executeQuery();
			if ((rsTerminal != null) && (rsTerminal.next()) && (rsTerminal.getLong("terminalId") != 0)) 
			{
				terminalId = rsTerminal.getLong("terminalId");
				rsTerminal.close();
                                retValue = new CPhysicalTerminal();
                                retValue.loadTerminal(terminalId);                                
			}

		} 
		catch (java.lang.Exception localException) 
		{
			logger.error(localException.toString());
			retValue = null;
		} 
		finally 
		{
			if (rsTerminal != null) 
			{
				try 
				{
					rsTerminal.close();
				}
				catch (Exception ex) 
				{
				}
			}
			rsTerminal = null;

			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	
	/**
	 * @param targetStatusId
	 * @return				True if new status could be assigned
	 */
	public boolean changeStatus(long targetStatusId)
	{
		boolean retValue = false;
		
		CTerminalStatus[] statusAllowed = CPhysicalTerminal.getAllowedStatus(this.m_CurrentStatus.getTerminalSatusId(), null);
		if(statusAllowed != null)
		{
			for(CTerminalStatus currentStatus:statusAllowed)
			{
				if(currentStatus.getTerminalSatusId() == targetStatusId)
				{
					Connection dbConn = null;
					PreparedStatement ps = null;
				
					try
					{
						dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
						
			            if ( !Torque.isInit() )
			            {
			                throw new Exception("Torque is not initialized");
			            }

				        if (dbConn == null)
				        {
				          throw new Exception("Can't get database connection");
				        }
				        
				        ps = dbConn.prepareStatement(sql_bundle.getString("updateTerminalStatus"));
				        ps.setLong(1, targetStatusId);
				        ps.setLong(2, this.m_TerminalId);
				        ps.executeUpdate();
				        this.m_CurrentStatus = new CTerminalStatus();
				        this.m_CurrentStatus.load(targetStatusId);
				        retValue = true;
					}
					catch(java.lang.Exception localException)
					{
						logger.error(localException.toString());
						retValue = false;
					}
			    	finally
			    	{
			    		if ( ps != null )
			    		{
			    			try { ps.close(); }
			    			catch (Exception ex) {}
			    		}
			    		ps = null;
			    		if ( dbConn != null )

			                {
			    			try { dbConn.close(); }
			    			catch (Exception ex) {}
			    		}
			    		dbConn = null;
			    	}
			    	break; // Out from the loop when a valid target status is found  
				}// End If
			}
		}		
		return retValue;
	}

	
	/**
	 * @param serialNumber
	 * @return				True if a just one record is found with the given serial number 
	 */
	public static boolean serialNumberExists(String serialNumber) 
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn.prepareStatement(sql_bundle.getString("serialNumberExists"));
			ps.setString(1, serialNumber);
			rsTerminal = ps.executeQuery();
			if ((rsTerminal != null) && (rsTerminal.next())) 
			{
				retValue = true;
			}
		} 
		catch (java.lang.Exception localException) 
		{
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (rsTerminal != null) {
				try {
					rsTerminal.close();
				} catch (Exception ex) {
				}
			}
			rsTerminal = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	/**
	 * @param softwareLicense
	 * @return					True if software license is already in use
	 */
	public static boolean softwareLicenseExists(String softwareLicense) 
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn.prepareStatement(sql_bundle.getString("softwareLicenseExists"));
			ps.setString(1, softwareLicense);
			rsTerminal = ps.executeQuery();
			if ((rsTerminal != null) && (rsTerminal.next()) && (rsTerminal.getLong("licenseCount") > 0)) 
			{
				retValue = true;
			}
		} 
		catch (java.lang.Exception localException) 
		{
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (rsTerminal != null) {
				try {
					rsTerminal.close();
				} catch (Exception ex) {
				}
			}
			rsTerminal = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}


	
	/**
	 * @param imei
	 * @return			True if there are records with the given imei
	 */
	public static boolean imeiExists(String imei) 
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsTerminal = null;

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));

			if (!Torque.isInit()) {
				throw new Exception("Torque is not initialized");
			}

			if (dbConn == null) {
				throw new Exception("Can't get database connection");
			}
			ps = dbConn.prepareStatement(sql_bundle.getString("imeiExists"));
			ps.setString(1, imei);
			rsTerminal = ps.executeQuery();
			if ((rsTerminal != null) && (rsTerminal.next()) && (rsTerminal.getLong("imeiCount") > 0)) 
			{
				retValue = true;
			}
		} 
		catch (java.lang.Exception localException) 
		{
			logger.error(localException.toString());
			retValue = false;
		} finally {
			if (rsTerminal != null) {
				try {
					rsTerminal.close();
				} catch (Exception ex) {
				}
			}
			rsTerminal = null;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			ps = null;
			if (dbConn != null)

			{
				try {
					dbConn.close();
				} catch (Exception ex) {
				}
			}
			dbConn = null;
		}
		return retValue;
	}

	
	
	/**
	 * @param sSerialNumber			Serial number to search for in the terminal table 
	 * @return						True if there is no duplicate serial						
	 */
	public static boolean IsSerialNumberUnique(String sSerialNumber)
	{
		
		boolean bResult = false;
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null)
			{
				 logger.error("Can't get database connection");
				 throw new TerminalException();
			}  
			String strSQL = sql_bundle.getString("isSerialNumberUnique");
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, sSerialNumber);
			ResultSet rs = pstmt.executeQuery();
			bResult = !rs.next();
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			 logger.error("Error during IsSerialNumberUnique", e);
			 bResult = false;
		}
		finally
		{
		  try
		  {
			  Torque.closeConnection(dbConn);
		  }
		  catch (Exception e)
		  {
			  logger.error("Error during closeConnection", e);
		  }
		}
		return bResult;
	}//End of function IsSerialNumberUnique
	
}
