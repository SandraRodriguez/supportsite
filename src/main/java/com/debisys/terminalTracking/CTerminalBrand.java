/**
 * 
 */
package com.debisys.terminalTracking;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 * @author fernandob
 *
 */
public class CTerminalBrand 
{
	// ------------------------------------  attributes ------------------------------------------------
	private long m_BrandId;
	private String m_BrandName;
	private ArrayList<CTerminalModel>m_ModelList;
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.terminalTracking.sql");
	private static Logger logger = Logger.getLogger(CTerminalBrand.class);

	// ------------------------------------  properties  -----------------------------------------------
	/**
	 * @return the m_BrandId
	 */
	public long getBrandId() 
	{
		return m_BrandId;
	}
	/**
	 * @param brandId the m_BrandId to set
	 */
	public void setBrandId(long brandId) 
	{
		m_BrandId = brandId;
	}
	/**
	 * @return the m_BrandName
	 */
	public String getBrandName() 
	{
		return m_BrandName;
	}
	/**
	 * @param brandName the m_BrandName to set
	 */
	public void setBrandName(String brandName) 
	{
		m_BrandName = brandName;
	}
	/**
	 * @return the m_ModelList
	 */
	public ArrayList<CTerminalModel> getModelList() 
	{
		return m_ModelList;
	}
	
	// ------------------------------------  methods  --------------------------------------------------
	/**
	 * @param brandId	Brand ID from DB
	 * 
	 * Loads object information from DB
	 */
	public boolean loadBrand(long brandId)
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsBrand = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getBrandById"));
            ps.setLong(1, brandId);
            rsBrand = ps.executeQuery();
            if(rsBrand != null)
            {
            	if(rsBrand.next())
            	{
            		this.m_BrandId = rsBrand.getLong("terminalBrandId");
            		this.m_BrandName = rsBrand.getString("brandName");
            		retValue = true;
            	}	
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsBrand != null )
    		{
    			try { rsBrand.close(); }
    			catch (Exception ex) {}
    		}
    		rsBrand = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}
	
	/**
	 * @return The complete list of brands
	 */
	public static CTerminalBrand[] getBrandList()
	{
		CTerminalBrand[] retValue = null;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsBrand = null;
		ArrayList<CTerminalBrand> tmpList = new ArrayList<CTerminalBrand>();
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getAllBrands"));
            rsBrand = ps.executeQuery();
            if(rsBrand != null)
            {
            	while(rsBrand.next())
            	{
            		CTerminalBrand newBrand = new CTerminalBrand();
            		newBrand.setBrandId(rsBrand.getLong("terminalBrandId"));
            		newBrand.setBrandName(rsBrand.getString("brandName"));
            		tmpList.add(newBrand);
            	}	
            	retValue = new CTerminalBrand[tmpList.size()];
            	retValue = tmpList.toArray(retValue);
            	tmpList.clear();
            	tmpList = null;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = null;
		}	
    	finally
    	{
    		if ( rsBrand != null )
    		{
    			try { rsBrand.close(); }
    			catch (Exception ex) {}
    		}
    		rsBrand = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;
	}

	
	/**
	 * @return True if current brand models could be obtained from DB. Current brand id is used to get model records
	 */
	public boolean loadBrandModels()
	{
		boolean retValue = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rsBrand = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			
            if ( !Torque.isInit() )
            {
                throw new Exception("Torque is not initialized");
            }

	        if (dbConn == null)
	        {
	          throw new Exception("Can't get database connection");
	        }
	        
	        if(this.m_ModelList == null)
	        {
	        	this.m_ModelList = new ArrayList<CTerminalModel>();
	        }
	        else
	        {
	        	this.m_ModelList.clear();
	        }
	        
	        ps = dbConn.prepareStatement(sql_bundle.getString("getBrandModels"));
            ps.setLong(1, this.m_BrandId);
            rsBrand = ps.executeQuery();
            if(rsBrand != null)
            {
            	while(rsBrand.next())
            	{
            		CTerminalModel newModel = new CTerminalModel();
            		newModel.setModelId(rsBrand.getLong("terminalModelId"));
            		newModel.setModelName(rsBrand.getString("modelName"));
            		this.m_ModelList.add(newModel);
            	}	
            	if(this.m_ModelList.size() > 0) retValue = true;
            }
		}
		catch(java.lang.Exception localException)
		{
			logger.error(localException.toString());
			retValue = false;
		}	
    	finally
    	{
    		if ( rsBrand != null )
    		{
    			try { rsBrand.close(); }
    			catch (Exception ex) {}
    		}
    		rsBrand = null;
    		if ( ps != null )
    		{
    			try { ps.close(); }
    			catch (Exception ex) {}
    		}
    		ps = null;
    		if ( dbConn != null )

                {
    			try { dbConn.close(); }
    			catch (Exception ex) {}
    		}
    		dbConn = null;
    	}
    	return retValue;		
	}

	
	
}
