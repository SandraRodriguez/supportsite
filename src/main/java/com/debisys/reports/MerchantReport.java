package com.debisys.reports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.exceptions.ReportException;
import com.debisys.reports.pojo.MerchantDetailPojo;
import com.debisys.terminals.Terminal;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;

public class MerchantReport {

	private static final Logger cat = Logger.getLogger(ClerkReport.class);
	private static final String MERCHANT_INFO_SQL = "SELECT m.dba, m.phys_address, m.phys_city, m.phys_state, m.phys_zip, m.merchant_id "
			+ " FROM merchants m with(nolock) inner join terminals t with(nolock) on t.merchant_id=m.merchant_id "
			+ "where t.millennium_no = ?";
	private static final String TRANSACTION_SQL = "SELECT w.millennium_no,w.datetime, w.clerk,  w.clerk_name,  w.description,  w.amount,  "
			+ "(w.merchant_rate / 100 * (CASE p.ach_type WHEN 6 THEN ISNULL(w.Fixed_trans_fee,0) + ISNULL(w.transFixedFeeAmt,0) ELSE (" + 
			"	   		CASE p.ach_type WHEN 6 \r\n" + 
			"	   		THEN ISNULL(w.Fixed_trans_fee,0) + ISNULL(w.transFixedFeeAmt,0) \r\n" + 
			"	   		ELSE (CASE p.isRegulatoryFee WHEN 1\r\n" + 
			"	   			  THEN w.amount - p.Fixed_fee\r\n" + 
			"	   			  ELSE w.amount\r\n" + 
			"	   			   END) \r\n" + 
			"	   		END) END)) as merchant_commission "
			+ "FROM web_transactions w with(nolock) INNER JOIN products p ON p.id = w.id  "
			+ "WHERE w.merchant_id= ? and w.datetime >= ? AND w.datetime <= ? "
			+ "order by w.millennium_no,w.datetime";
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	private String startDateTime;
	private String endDateTime;
	private String clerkId;
	private String siteId;

	private String merchantId;
	private String merchantName;
	private String merchantAddress;

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getMerchantAddress() {
		return merchantAddress;
	}

	public void setMerchantAddress(String merchantAddress) {
		this.merchantAddress = merchantAddress;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getClerkId() {
		return clerkId;
	}

	public void setClerkId(String clerkId) {
		this.clerkId = clerkId;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public ArrayList<MerchantDetailPojo> getMerchantDetailTransactionReport(SessionData sessionData) throws ReportException {
		String strAccessLevel = sessionData.getProperty("access_level");
		Connection dbConn = null;
		ArrayList<MerchantDetailPojo> transactions = new ArrayList<MerchantDetailPojo>();

		if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK) || strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK_MANAGER)) {
			this.siteId = sessionData.getProperty("siteId");
			if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK)) {
				this.clerkId = sessionData.getProperty("clerkId");
			}
		} else {
			// validate permissions
			Terminal terminal = new Terminal();
			String merchantId = terminal.getMerchantId(siteId);

			// check to make sure this user has
			// permission to access this resource.
			Merchant merchant = new Merchant();
			merchant.setMerchantId(merchantId);
			if (merchantId.equals("")) {
				return null;
			} else if (!merchant.checkPermission(sessionData)) {
				// this means merchant/terminal does
				// not belong to logged in user
				return null;
			}
		}

		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			String strSQL = MERCHANT_INFO_SQL;

			cat.debug(String.format("SQL: %s [%s]", strSQL, this.getSiteId()));

			PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, this.getSiteId());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {

				this.setMerchantName(rs.getString(1));
				this.setMerchantId(rs.getString(6));

				String phys_address = "";
				String phys_city = "";
				String phys_state = "";
				String phys_zip = "";

				if (rs.getString(2) != null) {
					phys_address = rs.getString(2);
				}

				if (rs.getString(3) != null) {
					phys_city = rs.getString(3);
				}
				if (rs.getString(4) != null) {
					phys_state = rs.getString(4);
				}
				if (rs.getString(5) != null) {
					phys_zip = rs.getString(5);
				}
				pstmt.close();
				rs.close();

				this.setMerchantAddress(phys_address + "," + phys_city + "," + phys_state + "," + phys_zip);

				strSQL = TRANSACTION_SQL;

				cat.debug(String.format("SQL: %s [%s, %s, %s]", strSQL, this.getMerchantId(), this.getStartDateTime(), this.endDateTime));
				pstmt = dbConn.prepareStatement(strSQL);
				Date start = DateUtil.parse(this.getStartDateTime(), "MM/dd/yyyy hh:mm a");
				Date end = DateUtil.parse(this.getEndDateTime(), "MM/dd/yyyy hh:mm a");
				pstmt.setString(1,  this.getMerchantId());
				pstmt.setTimestamp(2, new Timestamp(start.getTime()));
				pstmt.setTimestamp(3, new Timestamp(end.getTime()));
				rs = pstmt.executeQuery();

				while (rs.next()) {
					MerchantDetailPojo transaction = new MerchantDetailPojo();

					// site Id is required
					if (rs.getString(1) != null) {

						transaction.setSiteId(rs.getString(1));
						transaction.setDatetime(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
						if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals("")) {
							transaction.setClerk_name(StringUtil.toString(rs.getString("clerk_name")));
						} else {
							transaction.setClerk_name(StringUtil.toString(rs.getString("clerk")));
						}

						transaction.setDescription(StringUtil.toString(rs.getString("description")));
						transaction.setAmount(rs.getString("amount"));
						transaction.setComission(rs.getString("merchant_commission"));

						transactions.add(transaction);
					}
				}
				pstmt.close();
				rs.close();
			} else {
				pstmt.close();
				rs.close();
			}

		} catch (Exception e) {
			cat.error("Error during getMerchantDetailTransactionReport", e);
			throw new ReportException();
		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return transactions;
	}
}
