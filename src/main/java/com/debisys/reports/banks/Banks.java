/**
 *
 */
package com.debisys.reports.banks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.debisys.exceptions.CustomerException;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author nmartinez
 *
 */
public class Banks {

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.banks.sql");
    private static Logger cat = Logger.getLogger(Banks.class);

    private long Id;
    private String code;
    private String Name;
    private boolean status;

    /**
     *
     * @param id
     * @param code
     * @param name
     * @param status
     */
    public Banks(long id, String code, String name, boolean status) {
        this.setId(id);
        this.setCode(code);
        this.setName(name);
        this.setStatus(status);
    }

    /**
     *
     * @return
     */
    public static ArrayList<Banks> findBanks() {
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        ArrayList<Banks> banks = new ArrayList<Banks>();

        try {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));

            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            sql.append(sql_bundle.getString("getBanks"));
            cat.debug(sql.toString());
            pstmt = dbConn.prepareStatement(sql.toString());

            rs = pstmt.executeQuery();

            while (rs.next()) {
                banks.add(new Banks(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getBoolean(4)));
            }
            rs.close();
            pstmt.close();
        } catch (Exception e) {
            cat.error("Error during findBanks ", e);
        } finally {
            try {
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            } catch (Exception e) {
                cat.error("Error during closeConnection", e);
            }
        }
        return banks;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return the status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @return the id
     */
    public long getId() {
        return Id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        Id = id;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return Name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        Name = name;
    }

}
