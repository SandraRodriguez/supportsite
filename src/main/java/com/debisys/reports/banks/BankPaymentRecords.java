/**
 * 
 */
package com.debisys.reports.banks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.debisys.exceptions.CustomerException;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

import com.debisys.languages.Languages;

/**
 * @author nmartinez
 *
 */
public class BankPaymentRecords {

	 private String amount;
	 private String bank;
	 private String referenceNumber;
	 private String paymentDate;
	 private String transactionCode;
	 private String dba;
	 private String millennium_no;
	 private long recordId;
	 private String documentId;
	 private String requestPaymentDate;
	 private String statusDescription;
	 
	 
	 private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.banks.sql");
	 private static Logger cat = Logger.getLogger(BankPaymentRecords.class);
	 
	 /**
	  * 
	  * @param amount
	  * @param bank
	  * @param referenceNumber
	  * @param paymentDate
	  * @param transactionCode
	  * @param dba
	  * @param millennium_no
	  */
	 public BankPaymentRecords(long recordId,
			 				   String amount,
			 				   String bank, 
			 				   String referenceNumber, 
			 				   String paymentDate, 
			 				   String transactionCode, 
			 				   String dba, 
			 				   String millennium_no, 
			 				   String documentId,
			 				   String requestPaymentDate,
			 				   String status)
	 {
		 this.setAmount(amount);
		 this.setBank(bank);
		 this.setDba(dba);
		 this.setMillennium_no(millennium_no);
		 this.setPaymentDate(paymentDate);
		 this.setReferenceNumber(referenceNumber);
		 this.setRecordId(recordId);
		 this.setDocumentId(documentId);
		 this.setRequestPaymentDate(requestPaymentDate);
		 this.setStatusDescription(status);
	 }
	 
	 
	 
	
	 
	 
	/**
	 *  
	 * @param dateIni
	 * @param dateEnd
	 * @param idDocument
	 * @param amount
	 * @param banks
	 * @param status
	 * @param merchants
	 * @param terminalId
	 * @return
	 */
	public static ArrayList<BankPaymentRecords> findBankPayments(String dateIni,
																 String dateEnd,
																 String idDocument, 
																 String amount,
																 String banks, 
																 String status, 
																 String merchants, 
																 String terminalId, 
																 String idRequest,
																 SessionData sessionData)
	{
		 ArrayList<BankPaymentRecords> payments = new ArrayList<BankPaymentRecords>();
		 StringBuilder sql = new StringBuilder();		
		 Connection dbConn = null;
		 PreparedStatement pstmt = null;
		 ResultSet rs = null;
		 ArrayList<String> sqlsByStatus = new ArrayList<String>();
		 
		 try
		 {
				dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));

				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new CustomerException();
				}
				
				String[] merchantsIds = null;
				if ( merchants.length()>0)
					merchantsIds = merchants.split("\\,");
				
				String[] banksIds = null;
				if ( banks.length() > 0)
					banksIds = banks.split("\\,");
				
				String[] statusId = null;
				if ( status.length() > 0)
				{
					statusId = status.split("\\,");
					if ( (statusId.length==1) && statusId[0].equals("0") )
					{
						//all status
						sqlsByStatus.add(defineSQLAppliedStatus(dateIni,dateEnd,idRequest,terminalId,merchantsIds,amount,banksIds));
						sqlsByStatus.add(defineSQLOtherStatus(dateIni,dateEnd,idDocument,amount,banksIds,"NAPL"));
						sqlsByStatus.add(defineSQLOtherStatus(dateIni,dateEnd,idDocument,amount,banksIds,"DISC"));
					}
					else
					{
						for(String currentStatus : statusId)
						{
							if ( currentStatus.equals("APLY"))
							{
								sqlsByStatus.add(defineSQLAppliedStatus(dateIni,dateEnd,idRequest,terminalId,merchantsIds,amount,banksIds));
							}
							else if ( currentStatus.equals("NAPL")  )
							{
								sqlsByStatus.add(defineSQLOtherStatus(dateIni,dateEnd,idDocument,amount,banksIds,"NAPL"));
							}	
							//else if ( currentStatus.equals("DISC")  )
							//{
							//	sqlsByStatus.add(defineSQLOtherStatus(dateIni,dateEnd,idDocument,amount,banksIds,"DISC"));
							//}					
						}	
					}					
				}
				else
				{
					//all status
					sqlsByStatus.add(defineSQLAppliedStatus(dateIni,dateEnd,idRequest,terminalId,merchantsIds,amount,banksIds));
					sqlsByStatus.add(defineSQLOtherStatus(dateIni,dateEnd,idDocument,amount,banksIds,"NAPL"));
					//sqlsByStatus.add(defineSQLOtherStatus(dateIni,dateEnd,idDocument,amount,banksIds,"DISC"));
				}
				
				int sizeSQLs=sqlsByStatus.size();
				for(int i=0; i<sizeSQLs;i++)
				{					
					if (i<sizeSQLs-1)
					{
						sql.append(sqlsByStatus.get(i)+" UNION ALL ");
					}
					else
					{
						sql.append(sqlsByStatus.get(i));
					}
				}				
				sql.append(" ORDER BY bp.PaymentDate, prs.StatusCode  ");
				
				cat.debug(sql.toString());
				pstmt = dbConn.prepareStatement(sql.toString());

				rs = pstmt.executeQuery();
				int i=1;
				while (rs.next())
				{
					String dateRequestDate="";
					String siteId="";
					String requestId="";
					String statusDesc="";
					
					if ( rs.getTimestamp("RequestDate") !=null )
					{
						dateRequestDate= DateUtil.formatDateTime(rs.getTimestamp("RequestDate"));						
					}
					
					if ( rs.getInt("millennium_no") != 0 )
					{
						siteId = rs.getString("millennium_no");
					}
					if ( rs.getInt("ID") != 0 )
					{
						requestId = rs.getString("ID");
					}
					
					///////////////////////////////////////////////////////////////
					///////////////////////////////////////////////////////////////
					//**************STATUS DESCRIPTION LOGIC*********************//
					if ( rs.getString("StatusCode").equals("APLY") )
					{
						statusDesc = Languages.getString("jsp.admin.reports.bankpayments.Applied",sessionData.getLanguage());
					}
					else if ( rs.getString("StatusCode").equals("DISC") )
					{
						statusDesc = Languages.getString("jsp.admin.reports.bankpayments.Expired",sessionData.getLanguage());
					}
					else if ( rs.getString("StatusCode").equals("LOAD") )
					{
						statusDesc = "*";
					}
					else if ( rs.getString("StatusCode").equals("NAPL") )
					{
						statusDesc = Languages.getString("jsp.admin.reports.bankpayments.Pend",sessionData.getLanguage());
					}
					///////////////////////////////////////////////////////////////
					///////////////////////////////////////////////////////////////
					
					payments.add(new BankPaymentRecords(
														i,
													    NumberUtil.formatAmount(rs.getString("amount")), 
														rs.getString("Name"), 
														rs.getString("ReferenceNumber"), 
														DateUtil.formatDateTime(rs.getTimestamp("PaymentDate")),
														rs.getString("TransactionCode"), 
														rs.getString("legal_businessname"), 
														siteId,
														requestId,
														dateRequestDate,
														statusDesc
														)
								);
					i++;				
				}
				rs.close();
				pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("Error during findBankPayments ", e);				
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}		
		return payments;
	 }
	
	
	/**
	 * 
	 * @return
	 */
	public static int findPropertyMaxDays(String instance)
	{
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder();
		Integer value= new Integer(50);
		
		 try
		 {
				dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));

				if (dbConn == null)
				{
					cat.error("Can't get database connection");
					throw new CustomerException();
				}
				sql.append(sql_bundle.getString("getPropertyDays"));				
				cat.debug(sql.toString());
				pstmt = dbConn.prepareStatement(sql.toString());

				rs = pstmt.executeQuery();
				
				if (rs.next())
				{
					value = rs.getInt(1);		
				}
				rs.close();
				pstmt.close();
			}
			catch (Exception e)
			{
				cat.error("Error during findPropertyMaxDays ", e);				
			}
			finally
			{
				try
				{
					TorqueHelper.closeConnection(dbConn, pstmt, rs);
				}
				catch (Exception e)
				{
					cat.error("Error during closeConnection", e);
				}
			}	
			return value;		
	}
	
	/**
	 * 
	 * @param dateIni
	 * @param dateEnd
	 * @param idDocument
	 * @param amount
	 * @param banks
	 * @return
	 */
	private static String defineSQLOtherStatus(String dateIni, String dateEnd, String idDocument, String amount, String[] banks, String statusCode)
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT bp.amount, bp.BankId, bp.ReferenceNumber, bp.PaymentDate, bp.TransactionCode, bp.RecordId, b.Name, ");
		sql.append(" isnull(m.legal_businessname,'') legal_businessname,");
		sql.append(" isnull(t.millennium_no,'') millennium_no,");
		sql.append(" isnull(pq.ID,'') ID,");
		sql.append(" pq.RequestDate, prs.StatusCode, b.code codeBank ");
		sql.append(" FROM bankpayments bp WITH(NOLOCK) ");
		sql.append(" LEFT JOIN PaymentRequests pq WITH(NOLOCK) ON bp.Amount=pq.Amount "); 
		sql.append("										   and bp.BankId= pq.BankID "); 
		sql.append("										   and bp.ReferenceNumber = pq.DocumentNumber "); 
		sql.append("										   and bp.PaymentDate = pq.DocumentDate ");
		sql.append(" LEFT JOIN terminals t WITH(NOLOCK) ON t.millennium_no=pq.millennium_no ");
		sql.append(" LEFT JOIN merchants m WITH(NOLOCK) ON t.merchant_id=m.merchant_id ");
		sql.append(" LEFT JOIN banks b WITH(NOLOCK) ON b.ID = bp.BankId ");
		sql.append(" LEFT JOIN PaymentRecordStatus prs  WITH(NOLOCK) ON prs.StatusId=bp.PaymentStatus ");
		sql.append(" WHERE ");
		sql.append(" bp.PaymentDate>'"+dateIni+"' AND bp.PaymentDate<'"+dateEnd+"' ");
		sql.append(" AND pq.ID IS NULL ");
		sql.append(" AND prs.StatusCode='"+statusCode+"'");
		
		
		if ( idDocument.length() > 0)
		{
			//id returned by the bank when a person has a payment, MUST BE UNIQUE BY BANK!!!!
			sql.append(" AND bp.ReferenceNumber = '"+idDocument+"'");
		}
		
		if (amount.length()>0)
		{
			sql.append(" AND bp.amount = "+amount+" ");
		}	
		
		if ( banks!=null && banks.length >= 1)
		{
			StringBuilder bankBuilder = new StringBuilder();
			for(String bankId : banks)
			{
				bankBuilder.append(bankId+",");	
			}
			//remove the last ,
			String chanisBanks = bankBuilder.toString().substring(0,bankBuilder.toString().length()-1);
			sql.append(" AND bp.BankId in ("+chanisBanks+")");
		}			
		return sql.toString();
	}
	
	
	/**
	 * 
	 * @param dateIni
	 * @param dateEnd
	 * @param idRequest
	 * @param terminalId
	 * @param merchantsIds
	 * @param amount
	 * @param banks
	 * @return
	 */
	private static String defineSQLAppliedStatus(String dateIni, String dateEnd, String idRequest, String terminalId, String[] merchantsIds, String amount, String[] banks)
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT bp.amount, bp.BankId, bp.ReferenceNumber, bp.PaymentDate, bp.TransactionCode, bp.RecordId, b.Name, ");
		sql.append(" isnull(m.legal_businessname,'') legal_businessname,");
		sql.append(" isnull(t.millennium_no,'') millennium_no,");
		sql.append(" isnull(pq.ID,'') ID,");
		sql.append(" pq.RequestDate, prs.StatusCode, b.code codeBank ");
		sql.append(" FROM bankpayments bp WITH(NOLOCK) ");
		sql.append(" INNER JOIN PaymentRequests pq WITH(NOLOCK) ON bp.Amount=pq.Amount "); 
		sql.append("										   and bp.BankId= pq.BankID "); 
		sql.append("										   and bp.ReferenceNumber = pq.DocumentNumber "); 
		sql.append("										   and bp.PaymentDate = pq.DocumentDate ");
		sql.append(" INNER JOIN terminals t WITH(NOLOCK) ON t.millennium_no=pq.millennium_no ");
		sql.append(" INNER JOIN merchants m WITH(NOLOCK) ON t.merchant_id=m.merchant_id ");
		sql.append(" INNER JOIN banks b WITH(NOLOCK) ON b.ID = bp.BankId ");
		sql.append(" INNER JOIN PaymentRecordStatus prs  WITH(NOLOCK) ON prs.StatusId=bp.PaymentStatus ");
		sql.append(" WHERE bp.PaymentDate>'"+dateIni+"' AND bp.PaymentDate<'"+dateEnd+"' ");
		
		if ( idRequest.length() > 0)
		{
			//id returned when a customer do a payment request it is UNIQUE
			sql.append(" AND pq.ID = "+idRequest);
		}
		else if ( terminalId.length()>0)
		{
			//we look up for all terminals so sometimes the person who reported a issue payment just know the terminal Id
			//but the problem is with other terminal Id
			sql.append(" AND m.merchant_id=(select merchant_id from terminals tt where tt.millennium_no="+terminalId+")");
		}
		else if ( merchantsIds !=null && merchantsIds.length>0 )
		{
			StringBuilder mercStrBuilder = new StringBuilder();
			for(String merchId : merchantsIds)
			{
				mercStrBuilder.append(merchId+",");	
			}
			//remove the last ,
			String chanisMerchants = mercStrBuilder.toString().substring(0,mercStrBuilder.toString().length()-1);
			sql.append(" AND m.merchant_id in ("+chanisMerchants+")");			
		}
		
		if ( banks!=null && banks.length >= 1)
		{
			StringBuilder bankBuilder = new StringBuilder();
			for(String bankId : banks)
			{
				bankBuilder.append(bankId+",");	
			}
			//remove the last ,
			String chanisBanks = bankBuilder.toString().substring(0,bankBuilder.toString().length()-1);
			sql.append(" AND bp.BankId in ("+chanisBanks+")");
		}
		
		if (amount.length()>0)
		{
			sql.append(" AND bp.amount = "+amount+" ");
		}		
		return sql.toString();
	}
	
	
	
	/**
	 * 
	 * @param context
	 * @param sessionData
	 * @param bankPayments
	 * @return
	 */
	public static String downloadBankPayments(ServletContext context, SessionData sessionData, ArrayList<BankPaymentRecords> bankPayments)
	{
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
	    String downloadPath = DebisysConfigListener.getDownloadPath(context);
	    String workingDir = DebisysConfigListener.getWorkingDir(context);
	    String filePrefix = DebisysConfigListener.getFilePrefix(context);
		String strFileName = generateFileName(context);    
	    String fileSourceToDownload="";
		try 
		{			
			fileSourceToDownload = workingDir + filePrefix + strFileName + ".csv";
			FileChannel fc = new RandomAccessFile(fileSourceToDownload, "rw").getChannel();
			writetofile(fc,"\"" + "#" + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.paymentrequest_search.dba", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.bankpayments.amount", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.bankpayments.bankCode", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.paymentrequest_search.siteid", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.bankpayments.IdDocumento", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.paymentrequest_search.requestnumber", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.bankpayments.PaymentDate", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.payments.detailedrep_payments.report_date", sessionData.getLanguage()) + "\",");
			writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.paymentrequest_search.status", sessionData.getLanguage()) + "\",");
			
			
			writetofile(fc,"\n");
			
			int icount=1;
			for(BankPaymentRecords bankPayment : bankPayments)
			{
				writetofile(fc,"\"" + icount + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getDba()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getAmount()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getBank()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getMillennium_no()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getReferenceNumber()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getDocumentId()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getPaymentDate()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getRequestPaymentDate()) + "\",");
				writetofile(fc,"\"" + StringUtil.toString(bankPayment.getStatusDescription()) + "\",");
				writetofile(fc,"\n");
				icount++;				
			}
			fc.close();
			
			fileSourceToDownload = compressFile(workingDir, filePrefix, strFileName, downloadPath, downloadUrl);
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fileSourceToDownload;
	}
	 
	
	/**
	 * 
	 * @param workingDir
	 * @param filePrefix
	 * @param strFileName
	 * @param downloadPath
	 * @param downloadUrl
	 * @return
	 */
	private static String compressFile(String workingDir, String filePrefix, String strFileName, String downloadPath, String downloadUrl) 
	{
		File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        cat.debug("Zip File Name: " + zipFileName);
        String downloadUrlFinal = downloadUrl + filePrefix + strFileName + ".zip";
        cat.debug("Download URL: " + downloadUrl);

        try
        {
          ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

          // Set the compression ratio
          out.setLevel(Deflater.DEFAULT_COMPRESSION);
          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

          // Add ZIP entry to output stream.
          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

          // Transfer bytes from the current file to the ZIP file
          //out.write(buffer, 0, in.read(buffer));

          int len;
          while ((len = in.read(buffer)) > 0)
          {
            out.write(buffer, 0, len);
          }

          // Close the current entry
          out.closeEntry();
          // Close the current file input stream
          in.close();
          out.close();
        }
        catch (IllegalArgumentException iae)
        {
          iae.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
          fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
        sourceFile.delete();
        return downloadUrlFinal;
	}



	/**
	 * 
	 * @param fc
	 * @param c
	 * @throws IOException
	 */
	private static void writetofile(FileChannel fc,String c) throws IOException
	{
	      fc.write(ByteBuffer.wrap(c.getBytes()));
	}
	
	
	/**
	 * 
	 * @param context
	 * @return
	 */
	public static String generateFileName(ServletContext context)
	{
	    boolean isUnique = false;
	    String strFileName = "";
	    while (!isUnique)
	    {
	      strFileName = generateRandomNumber();
	      isUnique = checkFileName(strFileName, context);
	    }
	    return strFileName;
	}
	
	/**
	 * 
	 * @return
	 */
	private static String generateRandomNumber()
	{
	    StringBuffer s = new StringBuffer();
	    // number between 1-9 because first digit must not be 0
	    int nextInt = (int) ((Math.random() * 9) + 1);
	    s = s.append(nextInt);

	    for (int i = 0; i <= 10; i++)
	    {
	      //number between 0-9
	      nextInt = (int) (Math.random() * 10);
	      s = s.append(nextInt);
	    }

	    return s.toString();
	}
	
	
	/**
	 * 
	 * @param strFileName
	 * @param context
	 * @return
	 */
	private static boolean checkFileName(String strFileName, ServletContext context)
	  {
	    boolean isUnique = true;
	    String downloadPath = DebisysConfigListener.getDownloadPath(context);
	    String workingDir = DebisysConfigListener.getWorkingDir(context);
	    String filePrefix = DebisysConfigListener.getFilePrefix(context);

	    File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
	    if (f.exists())
	    {
	      //duplicate file found
	      cat.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
	      return false;
	    }

	    f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
	    if (f.exists())
	    {
	      //duplicate file found
	      cat.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
	      return false;
	    }

	    return isUnique;
	  }
	 
	
	
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the bank
	 */
	public String getBank() {
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	public void setBank(String bank) {
		this.bank = bank;
	}

	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the paymentDate
	 */
	public String getPaymentDate() {
		return paymentDate;
	}

	/**
	 * @param paymentDate the paymentDate to set
	 */
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	/**
	 * @return the transactionCode
	 */
	public String getTransactionCode() {
		return transactionCode;
	}

	/**
	 * @param transactionCode the transactionCode to set
	 */
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	/**
	 * @return the dba
	 */
	public String getDba() {
		return dba;
	}

	/**
	 * @param dba the dba to set
	 */
	public void setDba(String dba) {
		this.dba = dba;
	}

	/**
	 * @return the millennium_no
	 */
	public String getMillennium_no() {
		return millennium_no;
	}

	/**
	 * @param millenniumNo the millennium_no to set
	 */
	public void setMillennium_no(String millenniumNo) {
		millennium_no = millenniumNo;
	}
	
	/**
	 * @param recordId the recordId to set
	 */
	public void setRecordId(long recordId) {
		this.recordId = recordId;
	}



	/**
	 * @return the recordId
	 */
	public long getRecordId() {
		return recordId;
	}



	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}



	/**
	 * @return the documentId
	 */
	public String getDocumentId() {
		return documentId;
	}



	/**
	 * @param requestPaymentDate the requestPaymentDate to set
	 */
	public void setRequestPaymentDate(String requestPaymentDate) {
		this.requestPaymentDate = requestPaymentDate;
	}



	/**
	 * @return the requestPaymentDate
	 */
	public String getRequestPaymentDate() {
		return requestPaymentDate;
	}



	/**
	 * @param statusDescription the statusDescription to set
	 */
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}



	/**
	 * @return the statusDescription
	 */
	public String getStatusDescription() {
		return statusDescription;
	}
	 
}
