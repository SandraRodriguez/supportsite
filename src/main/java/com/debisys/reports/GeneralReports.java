package com.debisys.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.reports.pojo.MerchantBySiteId;
import com.debisys.reports.pojo.MultiSurceByProviderPojo;
import com.debisys.reports.pojo.MultiSurcePojo;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * @author nmartinez
 *
 */
public class GeneralReports {

	//static Category cat = Category.getInstance(GeneralReports.class);
	private static Logger cat = Logger.getLogger(GeneralReports.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	private String merchant_ids = "";
	private String terminalTypes_ids = "";
	private String ratePlanId = "";

	/**
	 * 
	 */
	public static String NOT_AVALAIBLE_DESCRIPTION="N/A";
	
	/**
	 * @return the merchant_ids
	 */
	public String getMerchant_ids() {
		return this.merchant_ids;
	}

	/**
	 * @return the terminalTypes_ids
	 */
	/**
	 * @return terminalTypes_ids
	 */
	public String getTerminalTypes_ids() {
		return this.terminalTypes_ids;
	}

	/**
	 * @param strValue
	 */
	public void setTerminalTypesIds(String[] strValue) {
		String strTemp = "";
		if (strValue != null && strValue.length > 0) {
			boolean isFirst = true;
			for (int i = 0; i < strValue.length; i++) {
				if (strValue[i] != null && !strValue[i].equals("")) {
					if (!isFirst) {
						strTemp = strTemp + "," + strValue[i];
					} else {
						strTemp = strValue[i];
						isFirst = false;
					}
				}
			}
		}

		this.terminalTypes_ids = strTemp;
	}

	/**
	 * @param strValue
	 */
	public void setMerchantIds(String[] strValue) {
		String strTemp = "";
		if (strValue != null && strValue.length > 0) {
			boolean isFirst = true;
			for (int i = 0; i < strValue.length; i++) {
				if (strValue[i] != null && !strValue[i].equals("")) {
					if (!isFirst) {
						strTemp = strTemp + "," + strValue[i];
					} else {
						strTemp = strValue[i];
						isFirst = false;
					}
				}
			}
		}

		this.merchant_ids = strTemp;
	}

	/**
	 * @param typeTerminal
	 * @return isContained
	 */
	public boolean containsTerminalSelected(String typeTerminal) {
		boolean isContained = false;
		String[] typesTerm = this.getTerminalTypes_ids().split(",");

		for (String typeT : typesTerm) {
			if (typeT.equals(typeTerminal)) {
				isContained = true;
				break;
			}
		}

		return isContained;

	}

	/**
	 * @param sessionData
	 * @return
	 * @throws CustomerException
	 */
	/**
	 * @param sessionData
	 * @param request
	 * @return downloadRows
	 */
	public boolean getSitesIdByMerchantList(SessionData sessionData, ServletContext context, HttpServletRequest request) 
	{
		ArrayList<MerchantBySiteId> vecMerchantList = new ArrayList<MerchantBySiteId>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		boolean downloadRows = false;

		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		String searchFinal = "";
		String strSQL = GeneralReports.sql_bundle.getString("getMerchantList");
		String whereTerminalTypes = "";

		if (!this.terminalTypes_ids.equals("ALL")) {
			whereTerminalTypes = " b.terminal_type in ("+ this.terminalTypes_ids + ")";
		}

		try {
			dbConn = TorqueHelper.getConnection(GeneralReports.sql_bundle
					.getString("pkgAlternateDb"));

			if (dbConn == null) {
				GeneralReports.cat.error("Can't get database connection");
				throw new CustomerException();
			}

			StringBuffer search = new StringBuffer();
			search.append("select a.rep_id,a.merchant_id,a.dba,a.phys_address,a.phys_city,a.phys_state,a.phys_zip,a.phys_country,a.contact_phone,");
			search.append(" a.contact_fax,b.millennium_no,");
			if (this.ratePlanId.equals(""))
			{
				search.append(" b.ISO_Rateplan_id,c.description,d.rateplan,");
			}
			else
			{
				search.append(" b.RatePlanID, c.description, d.Name,");
			}
			search.append(" b.terminal_version,f.rebuildType,e.rebuildDate, r.businessname ");
			
			search.append(" , m.providerTerminalId, p.name, q.name , k.name, a.UseRatePlanModel , b.RatePlanID, w.website_log_date, b.ship_date ");
			
			search.append("from merchants a with(nolock) ");
			search.append("inner join terminals         b with(nolock) on a.merchant_id     = b.merchant_id ");
			search.append("inner join terminal_types    c with(nolock) on b.terminal_type   = c.terminal_type ");
			if (this.ratePlanId.equals(""))
			{
				search.append("left outer join ISO_RatePlan      d with(nolock) on b.ISO_Rateplan_id = d.ISO_Rateplan_id ");
			}
			else
			{
				search.append("left outer join RatePlan      d with(nolock) on b.RatePlanID = d.RatePlanID ");
			}
			search.append("inner join reps              r with(nolock) on r.rep_id = a.rep_id ");
                        search.append("left join website_log        w with(nolock) on w.applied_ref_id = a.merchant_id  AND w.message like '%Site Id:'+CONVERT(VARCHAR(7),b.millennium_no)+' added%'");
			search.append("left join rebuilds           e with(nolock) on b.millennium_no = e.millennium_no ");
			search.append("left join TerminaRebuildType f with(nolock) on e.rebuildType = f.idTerminalRebuildType ");

			search.append(" left join terminal_mapping m with(nolock) on m.debisysTerminalId=b.millennium_no and m.providerId in (99,110,122,146) ");
			search.append(" left join provider p with(nolock) on p.provider_id=m.providerId  ");
			search.append(" left join accountexecutives q with(nolock) on a.ExecutiveAccountId=q.id  ");
			search.append(" left outer join rateplan k with(nolock) on k.RatePlanID = b.RatePlanID  ");
			
			
			String strSQLWhere = "";

			if (this.merchant_ids.equals("ALL")) 
			{
				if (this.ratePlanId.equals(""))
				{
					searchFinal = this.defineSQLReport(strAccessLevel, strDistChainType, strRefId, strSQL, search, strSQLWhere, whereTerminalTypes);
				}
				else
				{
					if (sessionData.getProperty("ref_id").equals(context.getAttribute("iso_default").toString()) &&
						( (sessionData.getUser().isIntranetUser() && 
						sessionData.getUser().hasIntranetUserPermission(sessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
						))
					{
						searchFinal = this.defineSQLReport("", strDistChainType, strRefId, strSQL, search, strSQLWhere, "b.RatePlanId = " + this.ratePlanId);
					}
					else
					{
						searchFinal = this.defineSQLReport(strAccessLevel, strDistChainType, strRefId, strSQL, search, strSQLWhere, "b.RatePlanId = " + this.ratePlanId);
					}
				}
			} 
			else 
			{
				if (whereTerminalTypes.length() > 0) 
				{
					search.append(" where a.merchant_id in ("+ this.merchant_ids + ") and " + whereTerminalTypes	+ " order by a.merchant_id,e.rebuildDate desc");
				} 
				else 
				{
					search.append(" where a.merchant_id in ("+ this.merchant_ids + ") order by a.merchant_id,e.rebuildDate desc");
				}
				GeneralReports.cat.debug(search.toString());
				searchFinal = search.toString();
			}

			pstmt = dbConn.prepareStatement(searchFinal);
			rs = pstmt.executeQuery();
			String siteId = "";

			while (rs.next()) 
			{
				if (!siteId.equals(rs.getString(11))) 
				{
					MerchantBySiteId merchatSiteId = new MerchantBySiteId();
					merchatSiteId.setMerchant_id(rs.getString(2));

					if (rs.getString(3) == null || rs.getString(3).trim().length()==0) {
						merchatSiteId.setDba(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} else {
						merchatSiteId.setDba(rs.getString(3));
					}
					
					if (rs.getString(4) == null || rs.getString(4).trim().length()==0) 
					{
						merchatSiteId.setPhys_address(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					}
					else
					{
						merchatSiteId.setPhys_address(rs.getString(4));
					}
					
					
					if (rs.getString(5) == null || rs.getString(5).trim().length()==0) 
					{
						merchatSiteId.setPhys_city(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					}
					else
					{
						merchatSiteId.setPhys_city(rs.getString(5));
					}
					
					
					if (rs.getString(6) == null || rs.getString(6).trim().length()==0) 
					{
						merchatSiteId.setPhys_state(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					}
					else
					{
						merchatSiteId.setPhys_state(rs.getString(6));
					}
					
					
					if (rs.getString(7) == null || rs.getString(7).trim().length()==0) 
					{
						merchatSiteId.setPhys_zip(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					}
					else
					{
						merchatSiteId.setPhys_zip(rs.getString(7));
					}
					

					if (rs.getString(8) == null || rs.getString(8).trim().length()==0) 
					{
						merchatSiteId.setPhys_country(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					}
					else
					{
					   merchatSiteId.setPhys_country(rs.getString(8));
					}

					if (rs.getString(9) == null || rs.getString(9).trim().length()==0) 
					{
						merchatSiteId.setContact_phone(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setContact_phone(rs.getString(9));						
					}
					
					if ( (rs.getString(10) == null) || (rs.getString(10).trim().length()==0) ) 
					{
						merchatSiteId.setContact_fax(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setContact_fax(rs.getString(10));						
					}
					
					
					merchatSiteId.setMillennium_no(rs.getString(11));
					
					merchatSiteId.setDescription(rs.getString(13));
					
					//System.out.println( "k.name, a.UseRatePlanModel = "+ rs.getString(22)+" "+rs.getBoolean(23));
					if ( rs.getBoolean(23) )
					{
						merchatSiteId.setRatePlan(rs.getString(22));
						merchatSiteId.setISO_Rateplan_id(Integer.toString(rs.getInt(24)));
					}
					else
					{
						merchatSiteId.setRatePlan(rs.getString(14));
						merchatSiteId.setISO_Rateplan_id(rs.getString(12));
					}	
						
					if (rs.getString(15) == null || rs.getString(15).trim().length()==0) 
					{
						merchatSiteId.setTerminal_version(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setTerminal_version(rs.getString(15));						
					}
					
					
					if (rs.getString(16) == null || rs.getString(16).trim().length()==0 ) 
					{
						merchatSiteId.setRebuildType(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setRebuildType(rs.getString(16));						
					}
					
					if (rs.getString(17) == null || rs.getString(17).trim().length()==0 ) 
					{
						merchatSiteId.setRebuildDate(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setRebuildDate(rs.getString(17));					
					}
					
					merchatSiteId.setRep_Name(rs.getString(18));

					
					if (rs.getString(19) == null || rs.getString(19).trim().length()==0 ) 
					{
						merchatSiteId.setProviderTerminalId(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setProviderTerminalId(rs.getString(19));					
					}
					
					
					if (rs.getString(21) == null || rs.getString(21).trim().length()==0 ) 
					{
						merchatSiteId.setAccountExecutive(GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setAccountExecutive(rs.getString(21));					
					}
                                        
                                        if (rs.getString(25) == null || rs.getString(25).trim().length()==0 ) 
					{
						merchatSiteId.setTerminalCreationDate((rs.getString(26) != null)?rs.getString(26):GeneralReports.NOT_AVALAIBLE_DESCRIPTION);
					} 
					else 
					{
						merchatSiteId.setTerminalCreationDate(rs.getString(25));					
					}
					
					
					vecMerchantList.add(merchatSiteId);
					siteId = rs.getString(11);
				}
			}
			request.removeAttribute("merchantDetails");
			if (vecMerchantList.size() > 0){
				downloadRows = true;				
				request.setAttribute("merchantDetails", vecMerchantList);
			}
						
			rs.close();
			pstmt.close();
		} catch (Exception e) {
			GeneralReports.cat
					.error("Error during getSitesIdByMerchantList", e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (pstmt != null) {
					pstmt.close();
				}

				TorqueHelper.closeConnection(dbConn);
				dbConn = null;
				pstmt = null;
				rs = null;
			} catch (Exception e) {
				GeneralReports.cat.error("Error during closeConnection", e);
			}
		}

		return downloadRows;
	}

	/**
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param strRefId
	 * @param strSQL
	 * @param search
	 * @param strSQLWhere
	 * @return
	 */
	private String defineSQLReport(String strAccessLevel,
			String strDistChainType, String strRefId, String strSQL,
			StringBuffer search, String strSQLWhere, String whereTerminalTypes) {
		String searchFinal;

		if ( strAccessLevel.length() > 0 )
		{
			if (strAccessLevel.equals(DebisysConstants.ISO)) {
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					strSQLWhere = strSQLWhere + " and r.iso_id = " + strRefId;
				} else if (strDistChainType
						.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
					strSQLWhere = " and r.rep_id IN "
							+ "(select rep_id from reps (nolock) where type="
							+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
							+ "(select rep_id from reps (nolock) where type="
							+ DebisysConstants.REP_TYPE_SUBAGENT
							+ " and iso_id in "
							+ "(select rep_id from reps (nolock) where type="
							+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = "
							+ strRefId + "))) ";
				}
			} else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
				strSQLWhere = " and r.rep_id IN "
						+ "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
						+ "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = "
						+ strRefId + ")) ";
			} else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
				strSQLWhere = " and r.rep_id IN "
						+ "(select rep_id from reps (nolock) where type="
						+ DebisysConstants.REP_TYPE_REP + " and iso_id = "
						+ strRefId + ")";
			} else if (strAccessLevel.equals(DebisysConstants.REP)) {
				strSQLWhere = " and r.rep_id = " + strRefId;
			} else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
				strSQLWhere = " and m.merchant_id = " + strRefId;
			}
		}
		strSQL = strSQL + strSQLWhere;

		GeneralReports.cat.debug(search.toString());
		//
		search.append("inner join (");
		search.append(strSQL);

		if (whereTerminalTypes.length() > 0) 
		{
			search.append(") g on a.merchant_id=g.merchant_id where "
					+ whereTerminalTypes
					+ " order by a.merchant_id,e.rebuildDate desc");
		} else {
			search
					.append(") g on a.merchant_id=g.merchant_id order by a.merchant_id,e.rebuildDate desc");
		}

		GeneralReports.cat.debug(search.toString());
		//

		searchFinal = search.toString();
		return searchFinal;
	}

	/**
	 * @param context
	 * @param sessionData
	 * @param request
	 * @return
	 * @throws TransactionException
	 */
	/**
	 * @param context
	 * @param sessionData
	 * @param request
	 * @return
	 * @throws TransactionException
	 */
	/**
	 * @param context
	 * @param sessionData
	 * @param request
	 * @return downloadUrl
	 * @throws TransactionException
	 */
	public String downloadSitesIdByMerchantList(ServletContext context,
			SessionData sessionData, HttpServletRequest request)
			throws TransactionException {
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		ArrayList<MerchantBySiteId> merchantsSiteIds = new ArrayList<MerchantBySiteId>();

		if (this.getSitesIdByMerchantList(sessionData, context, request)) {
			try {
				merchantsSiteIds = (ArrayList<MerchantBySiteId>) request.getAttribute("merchantDetails");

				long startQuery = 0;
				long endQuery = 0;
				startQuery = System.currentTimeMillis();
				BufferedWriter outputFile = null;
				try {
					strFileName = (new GeneralReports()).generateFileName(context);
					outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
					cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
					outputFile.flush();

					String merchantId = Languages.getString("jsp.admin.customers.merchants.merchant_id", sessionData.getLanguage());
			        String merchantName = Languages.getString("jsp.admin.reports.merchant_name", sessionData.getLanguage());
			        String phys_address = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_address", sessionData.getLanguage());
			        String phys_city = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_city", sessionData.getLanguage());       
			        String phys_state = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_state", sessionData.getLanguage());
			        String phys_zip = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_zip", sessionData.getLanguage());
			        String phys_country = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.phys_country", sessionData.getLanguage());
			        String contact_phone = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.contact_phone", sessionData.getLanguage());
			        String contact_fax = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.contact_fax", sessionData.getLanguage());       
			        String millennium_no = Languages.getString("jsp.merchant.requestrebuil.text5", sessionData.getLanguage());
			        String ISO_Rateplan_id = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.ISO_Rateplan_id", sessionData.getLanguage());
			        String description = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.description", sessionData.getLanguage());
			        String rateplan = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.rateplan", sessionData.getLanguage());
			        String terminal_version = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.terminal_version", sessionData.getLanguage());
			        String rebuildType = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.rebuildType", sessionData.getLanguage());
			        String rebuildDate = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.rebuildDate", sessionData.getLanguage());
			        String repName = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.repName", sessionData.getLanguage());
			        //String providerTerminalId = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.providerTerminalId", sessionData.getLanguage());
                                String terminalCreationDate = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.terminalCreationDate", sessionData.getLanguage());
			        //String providerName = Languages.getString("jsp.admin.reports.analisys.merchant_detail_siteid.providerName", sessionData.getLanguage()); 
			        String accountExecutive = Languages.getString("jsp.admin.reports.tools.accountexecutives.label", sessionData.getLanguage());
			        
			        outputFile.write("\""+repName+"\",");
			        outputFile.write("\"" + merchantId + "\",");
					outputFile.write("\"" + merchantName + "\",");
					outputFile.write("\""+  phys_address + "\",");
					outputFile.write("\"" + phys_city + "\",");
					outputFile.write("\"" + phys_state+ "\",");
					outputFile.write("\"" + phys_zip+ "\",");
					outputFile.write("\"" + phys_country + "\",");
					outputFile.write("\"" + contact_phone + "\",");
					outputFile.write("\"" + contact_fax	+ "\",");
					outputFile.write("\"" + accountExecutive	+ "\",");
					outputFile.write("\"" + millennium_no + "\",");
					outputFile.write("\"" + ISO_Rateplan_id + "\",");
					outputFile.write("\"" + rateplan + "\",");
					outputFile.write("\"" + description + "\",");
					outputFile.write("\"" + terminal_version + "\",");
					outputFile.write("\"" + rebuildType	+ "\",");
					outputFile.write("\"" + rebuildDate	+ "\",");
					
					outputFile.write("\"" + terminalCreationDate	+ "\",");
					//outputFile.write("\"" + providerName	+ "\",");
					
					outputFile.write("\r\n");
					
				       
					for (MerchantBySiteId merchantSiteId : merchantsSiteIds) 
					{
						outputFile.write("\"" + merchantSiteId.getRep_Name()+ "\",");
						outputFile.write("\"" + merchantSiteId.getMerchant_id()+ "\",");
						outputFile.write("\"" + merchantSiteId.getDba() + "\",");
						outputFile.write("\""+ merchantSiteId.getPhys_address() + "\",");
						outputFile.write("\"" + merchantSiteId.getPhys_city()+ "\",");
						outputFile.write("\"" + merchantSiteId.getPhys_state()+ "\",");
						outputFile.write("\"" + merchantSiteId.getPhys_zip()+ "\",");
						outputFile.write("\"" + merchantSiteId.getPhys_country() + "\",");
						outputFile.write("\"" + merchantSiteId.getContact_phone() + "\",");
						outputFile.write("\"" + merchantSiteId.getContact_fax()	+ "\",");
						outputFile.write("\"" + merchantSiteId.getAccountExecutive() + "\",");
						outputFile.write("\"" + merchantSiteId.getMillennium_no() + "\",");
						outputFile.write("\"" + merchantSiteId.getISO_Rateplan_id() + "\",");
						outputFile.write("\"" + merchantSiteId.getRatePlan()+ "\",");
						outputFile.write("\"" + merchantSiteId.getDescription()+ "\",");
						outputFile.write("\"" + merchantSiteId.getTerminal_version() + "\",");
						outputFile.write("\"" + merchantSiteId.getRebuildType()	+ "\",");
						outputFile.write("\"" + merchantSiteId.getRebuildDate()	+ "\",");
						
						outputFile.write("\"" + merchantSiteId.getTerminalCreationDate()+ "\",");
						//outputFile.write("\"" + merchantSiteId.getProviderName()+ "\",");

						outputFile.write("\r\n");
					}
					
					outputFile.write("\r\n");
					outputFile.flush();
					outputFile.close();
					endQuery = System.currentTimeMillis();
					cat.debug("File Out: "+ (((endQuery - startQuery) / 1000.0))	+ " seconds");
					startQuery = System.currentTimeMillis();
					File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
					long size = sourceFile.length();
					byte[] buffer = new byte[(int) size];
					String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
					cat.debug("Zip File Name: " + zipFileName);
					downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
					cat.debug("Download URL: " + downloadUrl);

					try {

						ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

						// Set the compression ratio
						out.setLevel(Deflater.DEFAULT_COMPRESSION);
						FileInputStream in = new FileInputStream(workingDir	+ filePrefix + strFileName + ".csv");

						// Add ZIP entry to output stream.
						out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

						// Transfer bytes from the current file to the ZIP file
						// out.write(buffer, 0, in.read(buffer));

						int len;
						while ((len = in.read(buffer)) > 0) {
							out.write(buffer, 0, len);
						}

						// Close the current entry
						out.closeEntry();
						// Close the current file input stream
						in.close();
						out.close();
					} catch (IllegalArgumentException iae) {
						iae.printStackTrace();
					} catch (FileNotFoundException fnfe) {
						fnfe.printStackTrace();
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
					sourceFile.delete();
					endQuery = System.currentTimeMillis();
					GeneralReports.cat
							.debug("Zip Out: "
									+ (((endQuery - startQuery) / 1000.0))
									+ " seconds");

				} catch (IOException ioe) {
					try {
						if (outputFile != null) {
							outputFile.close();
						}
					} catch (IOException ioe2) {
					}
				}

			} catch (Exception e) {
				GeneralReports.cat.error(
						"Error during downloadSitesIdByMerchantList", e);
				throw new TransactionException();
			}
			long end = System.currentTimeMillis();

			// Display the elapsed time to the standard output
			GeneralReports.cat.debug("Total Elapsed Time: "
					+ (((end - start) / 1000.0)) + " seconds");
		}
		return downloadUrl;
	}// End of function downloadDailyLiability

	/**
	 * @param context
	 * @return strFileName
	 */
	public String generateFileName(ServletContext context) {
		boolean isUnique = false;
		String strFileName = "";
		while (!isUnique) {
			strFileName = this.generateRandomNumber();
			isUnique = this.checkFileName(strFileName, context);
		}
		return strFileName;
	}

	/**
	 * @return
	 */
	private String generateRandomNumber() {
		StringBuffer s = new StringBuffer();
		// number between 1-9 because first digit must not be 0
		int nextInt = (int) ((Math.random() * 9) + 1);
		s = s.append(nextInt);

		for (int i = 0; i <= 10; i++) {
			// number between 0-9
			nextInt = (int) (Math.random() * 10);
			s = s.append(nextInt);
		}

		return s.toString();
	}

	/**
	 * @param strFileName
	 * @param context
	 * @return
	 */
	private boolean checkFileName(String strFileName, ServletContext context) {
		boolean isUnique = true;
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);

		File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
		if (f.exists()) {
			// duplicate file found
			cat.error("Duplicate file found:" + workingDir + "/" + filePrefix
					+ strFileName + ".csv");
			return false;
		}

		f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
		if (f.exists()) {
			// duplicate file found
			cat.error("Duplicate file found:" + downloadPath + "/" + filePrefix
					+ strFileName + ".zip");
			return false;
		}

		return isUnique;
	}


	/**
	 * @param ratePlanId
	 *        the ratePlanId to set
	 */
	public void setRatePlanId(String ratePlanId)
	{
		this.ratePlanId = ratePlanId;
	}

	/**
	 * @return the ratePlanId
	 */
	public String getRatePlanId()
	{
		return this.ratePlanId;
	}
        
        
        /**
         * 
         * @param context
         * @param sessionData
         * @param request
         * @return
         * @throws TransactionException 
         */
        public String downloadMultiSourceTrxs(ServletContext context,SessionData sessionData, HttpServletRequest request)
        throws TransactionException 
        {
            long start = System.currentTimeMillis();
            String strFileName = "";
            String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
            String downloadPath = DebisysConfigListener.getDownloadPath(context);
            String workingDir = DebisysConfigListener.getWorkingDir(context);
            String filePrefix = DebisysConfigListener.getFilePrefix(context);
            //ArrayList<Object> multiSource = new ArrayList<Object>();
            
            try 
            {
                //multiSource = (ArrayList<MultiSurcePojo>) request.getAttribute("multiSourceTransactionsReport");
               // if (  request.getAttribute("multiSourceTransactionsReport") == null )
                //    multiSource = (ArrayList<MultiSurcMultiSurceByProviderPojoePojo>) request.getAttribute("multiSourceTransactionsReportProv");
                    
                long startQuery = 0;
                long endQuery = 0;
                startQuery = System.currentTimeMillis();
                BufferedWriter outputFile = null;
                try 
                {
                    strFileName = (new GeneralReports()).generateFileName(context);
                    outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
                    cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
                    outputFile.flush();

                    String labelDate        = Languages.getString("jsp.tools.dtu2536.report.titles.Date",sessionData.getLanguage());
                    String labeltrxNo       = Languages.getString("jsp.tools.dtu2536.report.titles.trxNo",sessionData.getLanguage());
                    String labelrelatedTrx  = Languages.getString("jsp.tools.dtu2536.report.titles.relatedTrx",sessionData.getLanguage());
                    String labelprovider    = Languages.getString("jsp.tools.dtu2536.report.titles.provider",sessionData.getLanguage());
                    String labelsku         = Languages.getString("jsp.tools.dtu2536.report.titles.sku",sessionData.getLanguage());
                    String labelamt         = Languages.getString("jsp.tools.dtu2536.report.titles.amt",sessionData.getLanguage());
                    String labeltype        = Languages.getString("jsp.tools.dtu2536.report.titles.type",sessionData.getLanguage());
                    String labelmultiSource = Languages.getString("jsp.tools.dtu2536.report.titles.multiSource",sessionData.getLanguage());                   

                    if (  request.getAttribute("multiSourceTransactionsReport") != null )
                    {
                        outputFile.write("\"#\",");
                        outputFile.write("\"" + labelDate + "\",");
                        outputFile.write("\"" + labeltrxNo + "\",");
                        outputFile.write("\""+  labelrelatedTrx + "\",");
                        outputFile.write("\"" + labelprovider + "\",");
                        outputFile.write("\"" + labelsku+ "\",");
                        outputFile.write("\"" + labelamt+ "\",");
                        outputFile.write("\"" + labeltype + "\",");
                        outputFile.write("\"" + labelmultiSource + "\",");
                        outputFile.write("\r\n");
                    
                        ArrayList<MultiSurcePojo> multiSource = (ArrayList<MultiSurcePojo>) request.getAttribute("multiSourceTransactionsReport");
                        for (MultiSurcePojo multiSourceRow : multiSource) 
                        {
                            outputFile.write("\"" + multiSourceRow.getRow()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getDateTime()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getTrxId()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getTrxIdRelated()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getProduct().getProvider().getDescripton()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getProduct().getDescripton()+ "\",");                        
                            outputFile.write("\"" + multiSourceRow.getAmount()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getType()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getMultiSource()+ "\",");
                            outputFile.write("\r\n");
                        }  
                    }
                    else
                    {
                        String labelQty         = Languages.getString("jsp.tools.dtu2536.report.titles.qty",sessionData.getLanguage());
    
                        outputFile.write("\"#\",");
                        outputFile.write("\"" + labelQty + "\",");
                        outputFile.write("\"" + labelamt+ "\",");
                        outputFile.write("\"" + labelprovider + "\",");
                        outputFile.write("\"" + labeltype + "\",");
                        outputFile.write("\"" + labelmultiSource + "\",");
                        outputFile.write("\r\n");
                        
                        ArrayList<MultiSurceByProviderPojo> multiSource = (ArrayList<MultiSurceByProviderPojo>) request.getAttribute("multiSourceTransactionsReportProv");
                        for (MultiSurceByProviderPojo multiSourceRow : multiSource) 
                        {
                            outputFile.write("\"" + multiSourceRow.getRow()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getCount()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getSumAmount()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getDescripton()+"\",");
                            outputFile.write("\"" + multiSourceRow.getTypeTrx()+ "\",");
                            outputFile.write("\"" + multiSourceRow.getMultiSource()+ "\",");
                            outputFile.write("\r\n");
                        }  
                    }
                    outputFile.write("\r\n");
                    outputFile.flush();
                    outputFile.close();
                    endQuery = System.currentTimeMillis();
                    cat.debug("File Out: "+ (((endQuery - startQuery) / 1000.0))	+ " seconds");
                    startQuery = System.currentTimeMillis();
                    File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
                    long size = sourceFile.length();
                    byte[] buffer = new byte[(int) size];
                    String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
                    cat.debug("Zip File Name: " + zipFileName);
                    downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
                    cat.debug("Download URL: " + downloadUrl);

                    try 
                    {

                        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

                        // Set the compression ratio
                        out.setLevel(Deflater.DEFAULT_COMPRESSION);
                        FileInputStream in = new FileInputStream(workingDir	+ filePrefix + strFileName + ".csv");

                        // Add ZIP entry to output stream.
                        out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

                        // Transfer bytes from the current file to the ZIP file
                        // out.write(buffer, 0, in.read(buffer));

                        int len;
                        while ((len = in.read(buffer)) > 0) {
                                out.write(buffer, 0, len);
                        }

                        // Close the current entry
                        out.closeEntry();
                        // Close the current file input stream
                        in.close();
                        out.close();
                    } catch (IllegalArgumentException iae) {
                            iae.printStackTrace();
                    } catch (FileNotFoundException fnfe) {
                            fnfe.printStackTrace();
                    } catch (IOException ioe) {
                            ioe.printStackTrace();
                    }
                    sourceFile.delete();
                    endQuery = System.currentTimeMillis();
                    GeneralReports.cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");

                } catch (IOException ioe) {
                    try {
                            if (outputFile != null) {
                                    outputFile.close();
                            }
                    } catch (IOException ioe2) {
                    }
                }

            } 
            catch (Exception e) 
            {
                GeneralReports.cat.error("Error during downloadSitesIdByMerchantList", e);
                throw new TransactionException();
            }
            long end = System.currentTimeMillis();
            // Display the elapsed time to the standard output
            GeneralReports.cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");		
            return downloadUrl;
	}// End of function downloadDailyLiability
                
}
