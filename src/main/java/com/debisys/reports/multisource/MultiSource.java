package com.debisys.reports.multisource;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.pojo.MultiSurceByProviderPojo;
import com.debisys.reports.pojo.MultiSurcePojo;
import com.debisys.reports.pojo.ProductPojo;
import com.debisys.reports.pojo.ProviderPojo;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.TimeZone;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class MultiSource {
    
    private static Logger logger = Logger.getLogger(MultiSource.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.multisource.sql");
    private String    start_date       = "";
    private String    end_date         = "";
     

    /**
     * @return the start_date
     */
    public String getStart_date() {
        return start_date;
    }

    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     */
    public String getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
    
    
    
    
    
    public static ArrayList<ProviderPojo> getProviders() throws ReportException
    {
        ArrayList<ProviderPojo> providers = new ArrayList<ProviderPojo>();
        Connection dbConn = null;
        String strSQL = "";
        
        try{
            dbConn = Torque.getConnection(MultiSource.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                logger.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = MultiSource.sql_bundle.getString("getProviders");
            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL);
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {
                ProviderPojo provider = new ProviderPojo();
                provider.setId( rs.getString("provider_id"));
                provider.setDescripton( rs.getString("name"));
                provider.setClassName( rs.getString("classname"));
                providers.add( provider );
            }
            rs.close();
            pstmt.close();
        }
        catch(Exception e)
        {
            logger.error("Error during getProviders ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                logger.error("Error during closeConnection", e);
            }
        }
        return providers;                
    }
     
    
    public static ArrayList<ProductPojo> getProductsByproviders(String[] providers) throws ReportException
    {
        ArrayList<ProductPojo> products = new ArrayList<ProductPojo>();
        Connection dbConn = null;
        String strSQL = "";
        
        try
        {
            dbConn = Torque.getConnection(MultiSource.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                logger.error("Can't get database connection");
                throw new ReportException();
            }
            StringBuilder providersIn = new StringBuilder();
            String tokensProv = "TOKEN_IN_PROVIDERS";
            strSQL = MultiSource.sql_bundle.getString("getProducts");
            //WHERE pr.provider_id in (TOKEN_IN_PROVIDERS)
            if ( providers != null && providers[0].equals("-1"))
            {
                 strSQL = strSQL.replaceFirst(tokensProv, "");
            }    
            else    
            {
                for(String providerId : providers)
                {                
                    providersIn.append(providerId+",");
                }
                int lastIndexComa = providersIn.lastIndexOf(",");
                String providersT = providersIn.substring(0, lastIndexComa);
                strSQL = strSQL.replaceFirst(tokensProv, "WHERE pr.provider_id in ("+providersT+")");
            }   
            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL);
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {
                ProductPojo product = new ProductPojo();
                product.setId(rs.getString("id"));
                product.setDescripton(rs.getString("description"));  
                ProviderPojo provider = new ProviderPojo();
                provider.setId(rs.getString("provider_id"));
                provider.setDescripton(rs.getString("name"));
                product.setProvider(provider);
                
                products.add( product );
            }
            rs.close();
            pstmt.close();
        }
        catch(Exception e)
        {
            logger.error("Error during getProductsByproviders ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                logger.error("Error during closeConnection", e);
            }
        }
        return products;  
    }
     
    
    /**
     * 
     * @param strDistChainType
     * @param strAccessLevel
     * @param strRefId
     * @param providers
     * @param products
     * @param startDate
     * @param endDate
     * @param trxId
     * @param typeSearch
     * @param language
     * @return
     * @throws ReportException 
     */
    public static ArrayList<MultiSurcePojo> findTrxMultiSource(String strDistChainType,
                                                               String strAccessLevel, 
                                                               String strRefId,
                                                               String[] providers, 
                                                               String[] products, 
                                                               String startDate, 
                                                               String endDate, 
                                                               String trxId, 
                                                               String typeSearch, 
                                                               String language) throws ReportException
    {
        ArrayList<MultiSurcePojo> multiSource = new ArrayList<MultiSurcePojo>();
        Connection dbConn = null;
        String strSQL = "";
        StringBuilder providersIn = new StringBuilder();
        StringBuilder productsIn  = new StringBuilder();
        String sqlAdditionalWhere = "";
        String strSQLWhere = "";
        //String strDistChainType = sessionData.getProperty("dist_chain_type");
        int rowCount = 1;        
        
        try
        {
            dbConn = Torque.getConnection(MultiSource.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                logger.error("Can't get database connection");
                throw new ReportException();
            }
            Vector vTimeZoneFilterDates;
            vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, startDate, endDate+ " 23:59:59.999", false);
            String sDate = vTimeZoneFilterDates.get(0).toString();
            String eDate = vTimeZoneFilterDates.get(1).toString();
            
            if (strAccessLevel.equals(DebisysConstants.ISO))
            {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
                {
                    strSQLWhere += " AND w.rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + strRefId + ")";
                }
                else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                    strSQLWhere += " AND w.rep_id IN " + "(select rep_id from reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
                    + "(select rep_id from reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
                    + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + ")))";
                }
            }
            else
            {
                return null;
            }
            
            if ( trxId != null && trxId.length()>0 )
            {
                 strSQLWhere += " AND w.rec_id = "+trxId+" "; 
            }
            if ( products != null && !products[0].equals("-1"))
            {
                //this products
                for(String prod : products)
                {
                    productsIn.append(prod+",");
                }
                int lastIndexComa = productsIn.lastIndexOf(",");
                sqlAdditionalWhere = " AND p.id in ("+productsIn.substring(0, lastIndexComa)+") ";            
            }             
            else if ( providers != null && !providers[0].equals("-1"))
            {
                if ( providers.length == 1 )
                {
                    sqlAdditionalWhere = " AND prov.provider_id = " +providers[0];
                }
                else
                {
                    for(String providerId : providers)
                    {
                        providersIn.append(providerId+",");
                    }
                    int lastIndexComa = providersIn.lastIndexOf(",");
                    sqlAdditionalWhere = " AND prov.provider_id in (" +providersIn.substring(0, lastIndexComa)+")";
                }
                                
            }             
            
            PreparedStatement pstmt = null;
                      
            if ( typeSearch.equals("0") )
            {
                //all
                String multiStrSQL  = "";
                String directStrSQL = "";
                multiStrSQL  = MultiSource.sql_bundle.getString("findMultiSourceTrxs") + strSQLWhere + sqlAdditionalWhere;
                directStrSQL = MultiSource.sql_bundle.getString("findDirectlyTrx") + strSQLWhere + sqlAdditionalWhere;         
                strSQL = multiStrSQL + " UNION " + directStrSQL;
                logger.debug(strSQL.replaceFirst("\\?", "'"+sDate+"'")
                                   .replaceFirst("\\?", "'"+eDate+"'")
                                   .replaceFirst("\\?", "'"+sDate+"'")
                                   .replaceFirst("\\?", "'"+eDate+"'") );
                
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setString(1, sDate);
                pstmt.setString(2, eDate);
                pstmt.setString(3, sDate);
                pstmt.setString(4, eDate);               
            }
            else if ( typeSearch.equals("1") )
            {
                //MultSource
                strSQL = MultiSource.sql_bundle.getString("findMultiSourceTrxs") + strSQLWhere + sqlAdditionalWhere;         
                pstmt = dbConn.prepareStatement(strSQL);
                logger.debug(strSQL.replaceFirst("\\?", "'"+sDate+"'").replaceFirst("\\?", "'"+eDate+"'"));
                pstmt.setString(1, sDate);
                pstmt.setString(2, eDate);
            }
            else
            {
                //Directly
                strSQL = MultiSource.sql_bundle.getString("findDirectlyTrx") + strSQLWhere + sqlAdditionalWhere;         
                logger.debug(strSQL.replaceFirst("\\?", "'"+sDate+"'").replaceFirst("\\?", "'"+eDate+"'"));
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setString(1, sDate);
                pstmt.setString(2, eDate);
            }
            
            String trueMultiSource = Languages.getString("jsp.tools.dtu2536.MultiSourceFlag", language);
            String noMultiSource   = Languages.getString("jsp.tools.dtu2536.MultiSourceFlagNo", language);
            
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {                
                MultiSurcePojo multisourcepojo = new MultiSurcePojo();
                multisourcepojo.setRow(rowCount);
                multisourcepojo.setDateTime(rs.getString(1));
                multisourcepojo.setTrxId(rs.getString(2));
                multisourcepojo.setTrxIdRelated(rs.getString(3));
                
                String amtFormat = NumberUtil.formatCurrency(rs.getString(9));
                multisourcepojo.setAmount(amtFormat);
                
                multisourcepojo.setType(rs.getString(10));
                if ( rs.getInt(12) == 1 )
                {
                    multisourcepojo.setMultiSource(noMultiSource);
                }  
                else
                {
                    multisourcepojo.setMultiSource(trueMultiSource);
                }  
                //set the product
                ProductPojo product = new ProductPojo();
                product.setId(rs.getString(7));
                product.setDescripton(rs.getString(8)); 
                //*******************************************//
                
                //set the provider
                ProviderPojo provider = new ProviderPojo();
                provider.setId(rs.getString(5));
                provider.setDescripton(rs.getString(6));
                //*******************************************//
                
                //set the provider product
                product.setProvider(provider);
                multisourcepojo.setProduct(product);
                
                multiSource.add( multisourcepojo );
                rowCount++;
            }
            rs.close();
            pstmt.close();
            logger.debug("End sql.");
        }
        catch(Exception e)
        {
            logger.error("Error during findTrxMultiSource ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                logger.error("Error during closeConnection", e);
            }
        }
        return multiSource;  
    }
    
    
    
    
   /**
    * 
    * @param strDistChainType
    * @param strAccessLevel
    * @param strRefId
    * @param providers
    * @param products
    * @param startDate
    * @param endDate
    * @param trxId
    * @param typeSearch
    * @param language
    * @return
    * @throws ReportException 
    */
    public static ArrayList<MultiSurceByProviderPojo> findTrxMultiSourceByProvider(String strDistChainType,
                                                                                   String strAccessLevel, 
                                                                                   String strRefId,
                                                                                   String[] providers, 
                                                                                   String[] products, 
                                                                                   String startDate, 
                                                                                   String endDate, 
                                                                                   String trxId, 
                                                                                   String typeSearch, 
                                                                                   String language) throws ReportException
    {
        ArrayList<MultiSurceByProviderPojo> multiSourceByProv = new ArrayList<MultiSurceByProviderPojo>();
        Connection dbConn = null;
        String strSQL = "";
        StringBuilder providersIn = new StringBuilder();
        StringBuilder productsIn  = new StringBuilder();
        String sqlAdditionalWhere = "";
        String groupBy = " GROUP BY prov.provider_id, prov.name, w.Transaction_type ";
        String strSQLWhere = "";
        int rowCount = 1;        
        try
        {
            dbConn = Torque.getConnection(MultiSource.sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                logger.error("Can't get database connection");
                throw new ReportException();
            }
            Vector vTimeZoneFilterDates;
            vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, startDate, endDate+ " 23:59:59.999", false);
            String sDate = vTimeZoneFilterDates.get(0).toString();
            String eDate = vTimeZoneFilterDates.get(1).toString();
            
            if (strAccessLevel.equals(DebisysConstants.ISO))
            {
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
                {
                    strSQLWhere += " AND w.rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + strRefId + ")";
                }
                else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                {
                    strSQLWhere += " AND w.rep_id IN " + "(select rep_id from reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
                    + "(select rep_id from reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
                    + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + ")))";
                }
            }
            else
            {
                return null;
            }
                
            
            if ( products != null && !products[0].equals("-1"))
            {
                //this products
                for(String prod : products)
                {
                    productsIn.append(prod+",");
                }
                int lastIndexComa = productsIn.lastIndexOf(",");
                sqlAdditionalWhere = " AND p.id in ("+productsIn.substring(0, lastIndexComa)+") ";            
            }             
            else if ( providers != null && !providers[0].equals("-1"))
            {
                if ( providers.length == 1 )
                {
                    sqlAdditionalWhere = " AND prov.provider_id = " +providers[0];
                }
                else
                {
                    for(String providerId : providers)
                    {
                        providersIn.append(providerId+",");
                    }
                    int lastIndexComa = providersIn.lastIndexOf(",");
                    sqlAdditionalWhere = " AND prov.provider_id in (" +providersIn.substring(0, lastIndexComa)+")";
                }
                                
            }             
            
            PreparedStatement pstmt = null;
                      
            if ( typeSearch.equals("0") )
            {
                //all
                String multiStrSQL  = "";
                String directStrSQL = "";                
                multiStrSQL  = MultiSource.sql_bundle.getString("findMultiSourceTrxsProv") + strSQLWhere + sqlAdditionalWhere + groupBy;
                directStrSQL = MultiSource.sql_bundle.getString("findDirectlyTrxProv") + strSQLWhere + sqlAdditionalWhere + groupBy;                         
                strSQL = multiStrSQL + " UNION " + directStrSQL;
                logger.debug(strSQL.replaceFirst("\\?", "'"+sDate+"'")
                                   .replaceFirst("\\?", "'"+eDate+"'")
                                   .replaceFirst("\\?", "'"+sDate+"'")
                                   .replaceFirst("\\?", "'"+eDate+"'") );
                
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setString(1, sDate);
                pstmt.setString(2, eDate);
                pstmt.setString(3, sDate);
                pstmt.setString(4, eDate);
            }
            else if ( typeSearch.equals("1") )
            {
                //MultSource
                strSQL = MultiSource.sql_bundle.getString("findMultiSourceTrxsProv") + strSQLWhere + sqlAdditionalWhere  + groupBy;         
                pstmt = dbConn.prepareStatement(strSQL);
                logger.debug(strSQL.replaceFirst("\\?", "'"+sDate+"'").replaceFirst("\\?", "'"+eDate+"'"));
                pstmt.setString(1, sDate);
                pstmt.setString(2, eDate);
            }
            else
            {
                //Directly
                strSQL = MultiSource.sql_bundle.getString("findDirectlyTrxProv") + strSQLWhere + sqlAdditionalWhere  + groupBy;
                logger.debug(strSQL.replaceFirst("\\?", "'"+sDate+"'").replaceFirst("\\?", "'"+eDate+"'"));
                pstmt = dbConn.prepareStatement(strSQL);
                pstmt.setString(1, sDate);
                pstmt.setString(2, eDate);
            }
            
            String trueMultiSource = Languages.getString("jsp.tools.dtu2536.MultiSourceFlag", language);
            String noMultiSource   = Languages.getString("jsp.tools.dtu2536.MultiSourceFlagNo", language);
            
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {                
                MultiSurceByProviderPojo multisourcepojoProv = new MultiSurceByProviderPojo();
                multisourcepojoProv.setRow(rowCount);                
                multisourcepojoProv.setCount(rs.getInt(1));
                multisourcepojoProv.setId(rs.getString(2));
                multisourcepojoProv.setDescripton(rs.getString(3));                
                String amtFormat = NumberUtil.formatCurrency(rs.getString(4));
                multisourcepojoProv.setSumAmount(amtFormat);   
                
                multisourcepojoProv.setTypeTrx( rs.getString(6));
                                        
                if ( rs.getInt(5) == 1 )
                {
                    multisourcepojoProv.setMultiSource(trueMultiSource);
                }  
                else
                {
                    multisourcepojoProv.setMultiSource(noMultiSource);
                }  
                multiSourceByProv.add( multisourcepojoProv );
                rowCount++;
            }
            rs.close();
            pstmt.close();
            logger.debug("End sql.");
        }
        catch(Exception e)
        {
            logger.error("Error during findTrxMultiSourceByProvider ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                logger.error("Error during closeConnection", e);
            }
        }
        return multiSourceByProv;  
    }
    
}
