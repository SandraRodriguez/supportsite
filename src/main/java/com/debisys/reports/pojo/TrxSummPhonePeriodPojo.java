/**
 * 
 */
package com.debisys.reports.pojo;

import java.math.BigDecimal;

/**
 * @author nmartinez
 *
 */
public class TrxSummPhonePeriodPojo {

	private BigDecimal saleAmount = BigDecimal.ZERO;
	private BigDecimal taxTypeCheck = BigDecimal.ZERO;
	private BigDecimal bonus = BigDecimal.ZERO;
	private BigDecimal locationCount = BigDecimal.ZERO;				
	private BigDecimal tax = BigDecimal.ZERO;				
	private BigDecimal netAmount = BigDecimal.ZERO;
	private BigDecimal totalTrx = BigDecimal.ZERO;
	private BigDecimal totalRecharge = BigDecimal.ZERO;
	private String startDate;
	private String endDate;
	
	
	
	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the saleAmount
	 */
	public BigDecimal getSaleAmount() {
		return saleAmount;
	}
	/**
	 * @param saleAmount the saleAmount to set
	 */
	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = (saleAmount==null?BigDecimal.ZERO:saleAmount);
	}
	/**
	 * @return the taxTypeCheck
	 */
	public BigDecimal getTaxTypeCheck() {
		return taxTypeCheck;
	}
	/**
	 * @param taxTypeCheck the taxTypeCheck to set
	 */
	public void setTaxTypeCheck(BigDecimal taxTypeCheck) {
		this.taxTypeCheck = (taxTypeCheck==null?BigDecimal.ZERO:taxTypeCheck);
	}
	/**
	 * @return the bonus
	 */
	public BigDecimal getBonus() {
		return bonus;
	}
	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(BigDecimal bonus) {
		this.bonus = (bonus==null?BigDecimal.ZERO:bonus);
	}
	/**
	 * @return the locationCount
	 */
	public BigDecimal getLocationCount() {
		return locationCount;
	}
	/**
	 * @param locationCount the locationCount to set
	 */
	public void setLocationCount(BigDecimal locationCount) {
		this.locationCount = (locationCount==null?BigDecimal.ZERO:locationCount);
	}
	/**
	 * @return the tax
	 */
	public BigDecimal getTax() {
		return tax;
	}
	/**
	 * @param tax the tax to set
	 */
	public void setTax(BigDecimal tax) {
		this.tax = (tax==null?BigDecimal.ZERO:tax);
	}
	/**
	 * @return the netAmount
	 */
	public BigDecimal getNetAmount() {
		return netAmount;
	}
	/**
	 * @param netAmount the netAmount to set
	 */
	public void setNetAmount(BigDecimal netAmount) {
		this.netAmount = (netAmount==null?BigDecimal.ZERO:netAmount);
	}
	/**
	 * @return the totalTrx
	 */
	public BigDecimal getTotalTrx() {
		return totalTrx;
	}
	/**
	 * @param totalTrx the totalTrx to set
	 */
	public void setTotalTrx(BigDecimal totalTrx) {
		this.totalTrx = (totalTrx==null?BigDecimal.ZERO:totalTrx);
	}
	/**
	 * @param totalRecharge the totalRecharge to set
	 */
	public void setTotalRecharge(BigDecimal totalRecharge) {
		this.totalRecharge = (totalRecharge==null?BigDecimal.ZERO:totalRecharge);
	}
	/**
	 * @return the totalRecharge
	 */
	public BigDecimal getTotalRecharge() {
		return totalRecharge;
	}
	
}
