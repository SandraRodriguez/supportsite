/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

import java.util.ArrayList;

/**
 *
 * @author nmartinez
 */
public class TerminalPermissionBasicPojo {
    
    private String idNew;
    private boolean isIsoSelected;
    private ArrayList<TerminalPermissionsByEntityPojo> lstTerminalPermissionsByEntityPojo;

    public TerminalPermissionBasicPojo(){        
    }
    
    public String getIdNew() {
        return idNew;
    }

    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }
    
    public boolean isIsIsoSelected() {
        return isIsoSelected;
    }

    public void setIsIsoSelected(boolean isIsoSelected) {
        this.isIsoSelected = isIsoSelected;
    }

    public ArrayList<TerminalPermissionsByEntityPojo> getLstTerminalPermissionsByEntityPojo() {
        return lstTerminalPermissionsByEntityPojo;
    }

    public void setLstTerminalPermissionsByEntityPojo(ArrayList<TerminalPermissionsByEntityPojo> lstTerminalPermissionsByEntityPojo) {
        this.lstTerminalPermissionsByEntityPojo = lstTerminalPermissionsByEntityPojo;
    }
    
}
