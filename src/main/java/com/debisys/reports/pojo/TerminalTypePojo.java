/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class TerminalTypePojo extends BasicPojo{
    private String idNewTerminalTypeId;
    private String IdNew;

    /**
     * @return the idNewTerminalTypeId
     */
    public String getIdNewTerminalTypeId() {
        return idNewTerminalTypeId;
    }

    /**
     * @param idNewTerminalTypeId the idNewTerminalTypeId to set
     */
    public void setIdNewTerminalTypeId(String idNewTerminalTypeId) {
        this.idNewTerminalTypeId = idNewTerminalTypeId;
    }

    /**
     * @return the IdNew
     */
    public String getIdNew() {
        return IdNew;
    }

    /**
     * @param IdNew the IdNew to set
     */
    public void setIdNew(String IdNew) {
        this.IdNew = IdNew;
    }

    

    
}
