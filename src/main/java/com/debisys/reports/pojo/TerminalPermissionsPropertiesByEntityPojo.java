package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class TerminalPermissionsPropertiesByEntityPojo extends BasicPojo{
        
    private String entityId;    
    private String propertyName;    
    private Integer enabled;
    private Integer controlType;
    private String propertyId;
    
    /**
     * 
     * @param id
     * @param entityId
     * @param propertyName
     * @param propertyValue
     * @param enabled
     * @param datetime
     * @param controlType
     * @param propertyId 
     */
    public TerminalPermissionsPropertiesByEntityPojo(String id, String entityId,  
                    String propertyName, String propertyValue, Integer enabled, String datetime,Integer controlType,
                    String propertyId){
        this.id = id;
        this.entityId = entityId;        
        this.propertyName = propertyName;
        this.descripton = propertyValue;
        this.enabled = enabled;
        this.datetime = datetime;        
        this.controlType = controlType;
        this.propertyId = propertyId;
    }
    
    /**
     * @return the propertyId
     */
    public String getPropertyId() {
        return propertyId;
    }

    /**
     * @param propertyId the propertyId to set
     */
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }
    
    /**
     * @return the controlType
     */
    public Integer getControlType() {
        return controlType;
    }

    /**
     * @param controlType the controlType to set
     */
    public void setControlType(Integer controlType) {
        this.controlType = controlType;
    }
    
    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }    

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the enabled
     */
    public Integer getEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }
    
    
}
