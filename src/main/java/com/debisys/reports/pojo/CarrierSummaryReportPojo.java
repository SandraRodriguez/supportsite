/**
 * 
 */
package com.debisys.reports.pojo;

/**
 * @author nmartinez
 *
 */
@Deprecated
public class CarrierSummaryReportPojo {

	private String description;
	private String currentPeriod;
	private String previuosPeriod;
	private String change;
	private String recordType;
		
	
	/**
	 * @param description
	 * @param current
	 * @param previous
	 * @param change
	 * @param recordType
	 */
	public CarrierSummaryReportPojo(String description, String current, String previous, String change, String recordType){
		this.description = description;
		this.currentPeriod = current;
		this.previuosPeriod = previous;	
		this.change = change;
		this.recordType = recordType;
	}
	

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the currentPeriod
	 */
	public String getCurrentPeriod() {
		return currentPeriod;
	}


	/**
	 * @param currentPeriod the currentPeriod to set
	 */
	public void setCurrentPeriod(String currentPeriod) {
		this.currentPeriod = currentPeriod;
	}


	/**
	 * @return the previuosPeriod
	 */
	public String getPreviuosPeriod() {
		return previuosPeriod;
	}


	/**
	 * @param previuosPeriod the previuosPeriod to set
	 */
	public void setPreviuosPeriod(String previuosPeriod) {
		this.previuosPeriod = previuosPeriod;
	}


	/**
	 * @return the change
	 */
	public String getChange() {
		return change;
	}

	/**
	 * @param change the change to set
	 */
	public void setChange(String change) {
		this.change = change;
	}


	/**
	 * @return the recordType
	 */
	public String getRecordType() {
		return recordType;
	}


	/**
	 * @param recordType the recordType to set
	 */
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	
}
