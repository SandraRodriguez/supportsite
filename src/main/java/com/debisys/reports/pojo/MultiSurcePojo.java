package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class MultiSurcePojo {
    
    private int row;
    private String dateTime;
    private String trxId;
    private String trxIdRelated;
    private ProductPojo product;
    private String amount;
    private String type;
    private String multiSource;

    /**
     * @return the dateTime
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the trxId
     */
    public String getTrxId() {
        return trxId;
    }

    /**
     * @param trxId the trxId to set
     */
    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    /**
     * @return the trxIdRelated
     */
    public String getTrxIdRelated() {
        if (trxIdRelated == null )
            return "N/A";
        else
            return trxIdRelated;
    }

    /**
     * @param trxIdRelated the trxIdRelated to set
     */
    public void setTrxIdRelated(String trxIdRelated) {
        this.trxIdRelated = trxIdRelated;
    }
    
    /**
     * @return the product
     */
    public ProductPojo getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(ProductPojo product) {
        this.product = product;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the multiSource
     */
    public String getMultiSource() {
        return multiSource;
    }

    /**
     * @param multiSource the multiSource to set
     */
    public void setMultiSource(String multiSource) {
        this.multiSource = multiSource;
    }

    /**
     * @return the row
     */
    public int getRow() {
        return row;
    }

    /**
     * @param row the row to set
     */
    public void setRow(int row) {
        this.row = row;
    }
    
    
}
