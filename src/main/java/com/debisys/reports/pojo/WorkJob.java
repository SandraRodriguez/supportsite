/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class WorkJob {
    
    private String userId;
    private String reportCode;
    private String title;
    private String countryId;
    private String providers;
    private String products;
    private String skuCategories;
    private String categoryJob;

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the reportCode
     */
    public String getReportCode() {
        return reportCode;
    }

    /**
     * @param reportCode the reportCode to set
     */
    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the countryId
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     * @param countryId the countryId to set
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     * @return the providers
     */
    public String getProviders() {
        return providers;
    }

    /**
     * @param providers the providers to set
     */
    public void setProviders(String providers) {
        this.providers = providers;
    }

    /**
     * @return the products
     */
    public String getProducts() {
        return products;
    }

    /**
     * @param products the products to set
     */
    public void setProducts(String products) {
        this.products = products;
    }

    /**
     * @return the skuCategories
     */
    public String getSkuCategories() {
        return skuCategories;
    }

    /**
     * @param skuCategories the skuCategories to set
     */
    public void setSkuCategories(String skuCategories) {
        this.skuCategories = skuCategories;
    }

    /**
     * @return the categoryJob
     */
    public String getCategoryJob() {
        return categoryJob;
    }

    /**
     * @param categoryJob the categoryJob to set
     */
    public void setCategoryJob(String categoryJob) {
        this.categoryJob = categoryJob;
    }
    
}
