package com.debisys.reports.pojo;

/**
 *
 * @author fernandob
 */
public class BankPojo {

    private long id;
    private String code;
    private String name;
    private boolean enabled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public BankPojo() {
        this.id = 0L;
        this.code = "";
        this.name = "";
        this.enabled = false;
    }

    public BankPojo(long id, String code, String name, boolean enabled) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "BankPojo{" + "id=" + id + ", code=" + code + ", name=" + name + ", enabled=" + enabled + '}';
    }
}
