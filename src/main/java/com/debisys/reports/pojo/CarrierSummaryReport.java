/**
 * 
 */
package com.debisys.reports.pojo;

import java.util.ArrayList;

/**
 * @author nmartinez
 *
 */
@Deprecated
public class CarrierSummaryReport {
	
	private ArrayList<CarrierSummaryReportPojo> records;
		
	
	/**
	 * 
	 */
	public CarrierSummaryReport(){
		setRecords(new ArrayList<CarrierSummaryReportPojo>());		
	}


	/**
	 * @return the records
	 */
	public ArrayList<CarrierSummaryReportPojo> getRecords() {
		return records;
	}


	/**
	 * @param records the records to set
	 */
	public void setRecords(ArrayList<CarrierSummaryReportPojo> records) {
		this.records = records;
	}
	
	
	
	
	
}
