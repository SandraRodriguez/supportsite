/**
 * 
 */
package com.debisys.reports.pojo;

import java.util.ArrayList;

/**
 * @author nmartinez
 *
 */
public class PinStockPojo {

	private ArrayList<String> data;

	public PinStockPojo(){
		data = new ArrayList<String>();
	}
	
	/**
	 * @return the data
	 */
	public ArrayList<String> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(ArrayList<String> data) {
		this.data = data;
	}
	
}
