package com.debisys.reports.pojo;

/**
 * @author nmartinez
 *
 */
public class MerchantBySiteId 
{		
	/**
	 * 
	 */
	public MerchantBySiteId(){}
		
	private String rep_id;
	private String merchant_id;                             
	private String dba;
	private String phys_address;
	private String phys_city;
	private String phys_state;
	private String phys_zip;
	private String phys_country;
	private String contact_phone;
	private String contact_fax;
	private String millennium_no;
	private String ISO_Rateplan_id;
	private String RatePlan;
	private String Description;
	private String terminal_version;
	private String rebuildType;
	private String rebuildDate;
	private String rep_Name;
	private String providerTerminalId;
	private String providerName;
	private String accountExecutive;
        private String terminalCreationDate;
	
	
	/**
	 * @return the rep_id
	 */
	public String getRep_id() {
		return rep_id;
	}
	/**
	 * @param rep_id the rep_id to set
	 */
	public void setRep_id(String rep_id) {
		this.rep_id = rep_id;
	}
	/**
	 * @return the merchant_id
	 */
	public String getMerchant_id() {
		return merchant_id;
	}
	/**
	 * @param merchant_id the merchant_id to set
	 */
	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}
	/**
	 * @return the dba
	 */
	public String getDba() {
		return dba;
	}
	/**
	 * @param dba the dba to set
	 */
	public void setDba(String dba) {
		this.dba = dba;
	}
	/**
	 * @return the phys_address
	 */
	public String getPhys_address() {
		return phys_address;
	}
	/**
	 * @param phys_address the phys_address to set
	 */
	public void setPhys_address(String phys_address) {
		this.phys_address = phys_address;
	}
	/**
	 * @return the phys_city
	 */
	public String getPhys_city() {
		return phys_city;
	}
	/**
	 * @param phys_city the phys_city to set
	 */
	public void setPhys_city(String phys_city) {
		this.phys_city = phys_city;
	}
	/**
	 * @return the phys_state
	 */
	public String getPhys_state() {
		return phys_state;
	}
	/**
	 * @param phys_state the phys_state to set
	 */
	public void setPhys_state(String phys_state) {
		this.phys_state = phys_state;
	}
	/**
	 * @return the phys_zip
	 */
	public String getPhys_zip() {
		return phys_zip;
	}
	/**
	 * @param phys_zip the phys_zip to set
	 */
	public void setPhys_zip(String phys_zip) {
		this.phys_zip = phys_zip;
	}
	/**
	 * @return the phys_country
	 */
	public String getPhys_country() {
		return phys_country;
	}
	/**
	 * @param phys_country the phys_country to set
	 */
	public void setPhys_country(String phys_country) {
		this.phys_country = phys_country;
	}
	/**
	 * @return the contact_phone
	 */
	public String getContact_phone() {
		return contact_phone;
	}
	/**
	 * @param contact_phone the contact_phone to set
	 */
	public void setContact_phone(String contact_phone) {
		this.contact_phone = contact_phone;
	}
	/**
	 * @return the contact_fax
	 */
	public String getContact_fax() {
		return contact_fax;
	}
	/**
	 * @param contact_fax the contact_fax to set
	 */
	public void setContact_fax(String contact_fax) {
		this.contact_fax = contact_fax;
	}
	/**
	 * @return the millennium_no
	 */
	public String getMillennium_no() {
		return millennium_no;
	}
	/**
	 * @param millennium_no the millennium_no to set
	 */
	public void setMillennium_no(String millennium_no) {
		this.millennium_no = millennium_no;
	}
	/**
	 * @return the iSO_Rateplan_id
	 */
	public String getISO_Rateplan_id() {
		return ISO_Rateplan_id;
	}
	/**
	 * @param rateplan_id the iSO_Rateplan_id to set
	 */
	public void setISO_Rateplan_id(String rateplan_id) {
		ISO_Rateplan_id = rateplan_id;
	}
	/**
	 * @return the ratePlan
	 */
	public String getRatePlan() {
		return RatePlan;
	}
	/**
	 * @param ratePlan the ratePlan to set
	 */
	public void setRatePlan(String ratePlan) {
		RatePlan = ratePlan;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		Description = description;
	}
	/**
	 * @return the terminal_version
	 */
	public String getTerminal_version() {
		return terminal_version;
	}
	/**
	 * @param terminal_version the terminal_version to set
	 */
	public void setTerminal_version(String terminal_version) {
		this.terminal_version = terminal_version;
	}
	/**
	 * @return the rebuildType
	 */
	public String getRebuildType() {
		return rebuildType;
	}
	/**
	 * @param rebuildType the rebuildType to set
	 */
	public void setRebuildType(String rebuildType) {
		this.rebuildType = rebuildType;
	}
	/**
	 * @return the rebuildDate
	 */
	public String getRebuildDate() {
		return rebuildDate;
	}
	/**
	 * @param rebuildDate the rebuildDate to set
	 */
	public void setRebuildDate(String rebuildDate) {
		this.rebuildDate = rebuildDate;
	}
	/**
	 * @return the rep_Name
	 */
	public String getRep_Name()
	{
		return rep_Name;
	}
	/**
	 * @param rep_Name the rep_Name to set
	 */
	public void setRep_Name(String rep_Name)
	{
		this.rep_Name = rep_Name;
	}
	/**
	 * @param providerName the providerName to set
	 */
	public void setProviderName(String providerName)
	{
		this.providerName = providerName;
	}
	/**
	 * @return the providerName
	 */
	public String getProviderName()
	{
		return providerName;
	}
	/**
	 * @param providerTerminalId the providerTerminalId to set
	 */
	public void setProviderTerminalId(String providerTerminalId)
	{
		this.providerTerminalId = providerTerminalId;
	}
	/**
	 * @return the providerTerminalId
	 */
	public String getProviderTerminalId()
	{
		return providerTerminalId;
	}
	/**
	 * @param accountExecutive the accountExecutive to set
	 */
	public void setAccountExecutive(String accountExecutive) {
		this.accountExecutive = accountExecutive;
	}
	/**
	 * @return the accountExecutive
	 */
	public String getAccountExecutive() {
		return accountExecutive;
	}

    public String getTerminalCreationDate() {
        return terminalCreationDate;
    }

    public void setTerminalCreationDate(String terminalCreationDate) {
        this.terminalCreationDate = terminalCreationDate;
    }

}
