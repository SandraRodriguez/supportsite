/**
 * 
 */
package com.debisys.reports.pojo;

import java.util.ArrayList;

/**
 * @author nmartinez
 *
 */
public class ReportPinStock {

	private ArrayList<String> warnings;	
	private ArrayList<String> titles;
	private ArrayList<PinStockPojo> data;
	
	/**
	 * 
	 */
	public ReportPinStock(){
		titles = new ArrayList<String>();
		setData(new ArrayList<PinStockPojo>());
		warnings = new ArrayList<String>();
	}
		
	
	/**
	 * @return the titles
	 */
	public ArrayList<String> getTitles() {
		return titles;
	}
	
	/**
	 * @param titles the titles to set
	 */
	public void setTitles(ArrayList<String> titles) {
		this.titles = titles;
	}

	/**
	 * @return the data
	 */
	public ArrayList<PinStockPojo> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(ArrayList<PinStockPojo> data) {
		this.data = data;
	}


	/**
	 * @return the warnings
	 */
	public ArrayList<String> getWarnings() {
		return warnings;
	}


	/**
	 * @param warnings the warnings to set
	 */
	public void setWarnings(ArrayList<String> warnings) {
		this.warnings = warnings;
	}
	
	
}
