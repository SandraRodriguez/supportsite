/**
 * 
 */
package com.debisys.reports.pojo;


import java.util.ArrayList;
import java.util.List;

/**
 * @author nmartinez
 *
 */
public class TrxSummPhonePojo extends TrxSummPhonePeriodPojo{
	
	
	private List<TrxSummPhonePeriodPojo> listPeriods;
	
	
	public TrxSummPhonePojo()
	{
		listPeriods = new ArrayList<TrxSummPhonePeriodPojo>();
	}

	/**
	 * @param listPeriods the listPeriods to set
	 */
	public void setListPeriods(List<TrxSummPhonePeriodPojo> listPeriods) {
		this.listPeriods = listPeriods;
	}

	/**
	 * @return the listPeriods
	 */
	public List<TrxSummPhonePeriodPojo> getListPeriods() {
		return listPeriods;
	}
		
	

}
