/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class ImtuPojo extends BasicPojo {
    
    //private ProviderPojo provider;
    private ProductPojo product;
    private String buyRate;
    private boolean alternate;
    private String skuCategory;
    private boolean conversion;
    private Integer countIsosProducts;
    private boolean disabled;
    
    public ImtuPojo()
    {
       // provider = new ProviderPojo();
        product = new ProductPojo();
    }       

    /**
     * @return the product
     */
    public ProductPojo getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(ProductPojo product) {
        this.product = product;
    }

    /**
     * @return the buyRate
     */
    public String getBuyRate() {
        return buyRate;
    }

    /**
     * @param buyRate the buyRate to set
     */
    public void setBuyRate(String buyRate) {
        this.buyRate = buyRate;
    }

    /**
     * @return the alternate
     */
    public boolean isAlternate() {
        return alternate;
    }

    /**
     * @param alternate the alternate to set
     */
    public void setAlternate(boolean alternate) {
        this.alternate = alternate;
    }

    /**
     * @return the skuCategory
     */
    public String getSkuCategory() {
        return skuCategory;
    }

    /**
     * @param skuCategory the skuCategory to set
     */
    public void setSkuCategory(String skuCategory) {
        this.skuCategory = skuCategory;
    }

    /**
     * @return the conversion
     */
    public boolean isConversion() {
        return conversion;
    }

    /**
     * @param conversion the conversion to set
     */
    public void setConversion(boolean conversion) {
        this.conversion = conversion;
    }

    /**
     * @return the countIsosProducts
     */
    public Integer getCountIsosProducts() {
        return countIsosProducts;
    }

    /**
     * @param countIsosProducts the countIsosProducts to set
     */
    public void setCountIsosProducts(Integer countIsosProducts) {
        this.countIsosProducts = countIsosProducts;
    }

    /**
     * @return the disabled
     */
    public boolean isDisabled() {
        return disabled;
    }

    /**
     * @param disabled the disabled to set
     */
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
    
    
    
}
