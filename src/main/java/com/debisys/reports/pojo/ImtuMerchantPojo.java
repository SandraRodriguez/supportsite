/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class ImtuMerchantPojo extends BasicPojo {
    
    private BasicPojo Iso;
    private Integer numberOfTerminals;

    
    public ImtuMerchantPojo()
    {
        Iso = new BasicPojo();
    }
    
    
    /**
     * @return the Iso
     */
    public BasicPojo getIso() {
        return Iso;
    }

    /**
     * @param Iso the Iso to set
     */
    public void setIso(BasicPojo Iso) {
        this.Iso = Iso;
    }

    /**
     * @return the numberOfTerminals
     */
    public Integer getNumberOfTerminals() {
        return numberOfTerminals;
    }

    /**
     * @param numberOfTerminals the numberOfTerminals to set
     */
    public void setNumberOfTerminals(Integer numberOfTerminals) {
        this.numberOfTerminals = numberOfTerminals;
    }
    
}
