/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;


/**
 *
 * @author nmartinez
 */
public class WorkMasterPojo extends BasicPojo {
       
    private String creationDate;
    private String workCode;
    private String workParameters;
    private String idUuser;
    private String endDate; 
    private String titleJob;  
    private String atEndDate;
    
    /**
     * 
     */
    public WorkMasterPojo( )
    {
        
    }  

    /**
     * @return the creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the WorkCode
     */
    public String getWorkCode() {
        return workCode;
    }

    /**
     * @param WorkCode the WorkCode to set
     */
    public void setWorkCode(String WorkCode) {
        this.workCode = WorkCode;
    }

    /**
     * @return the WorkParameters
     */
    public String getWorkParameters() {
        return workParameters;
    }

    /**
     * @param WorkParameters the WorkParameters to set
     */
    public void setWorkParameters(String WorkParameters) {
        this.workParameters = WorkParameters;
    }

    /**
     * @return the id_user
     */
    public String getId_user() {
        return idUuser;
    }

    /**
     * @param id_user the id_user to set
     */
    public void setId_user(String id_user) {
        this.idUuser = id_user;
    }
    
    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }    

    /**
     * @return the titleJob
     */
    public String getTitleJob() {
        return titleJob;
    }

    /**
     * @param titleJob the titleJob to set
     */
    public void setTitleJob(String titleJob) {
        this.titleJob = titleJob;
    }

    /**
     * @return the atEndDate
     */
    public String getAtEndDate() {
        return atEndDate;
    }

    /**
     * @param atEndDate the atEndDate to set
     */
    public void setAtEndDate(String atEndDate) {
        this.atEndDate = atEndDate;
    }
    
        
}
