/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class ProductPojo extends BasicPojo{
    
    private ProviderPojo provider;

    /**
     * @return the provider
     */
    public ProviderPojo getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(ProviderPojo provider) {
        this.provider = provider;
    }
    
}
