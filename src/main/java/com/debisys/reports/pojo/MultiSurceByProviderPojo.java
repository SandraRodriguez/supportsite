/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class MultiSurceByProviderPojo extends BasicPojo{
    
    private int row;
    private int count;
    private String sumAmount;
    private String typeTrx;
    private String multiSource;

    /**
     * @return the sumAmount
     */
    public String getSumAmount() {
        return sumAmount;
    }

    /**
     * @param sumAmount the sumAmount to set
     */
    public void setSumAmount(String sumAmount) {
        this.sumAmount = sumAmount;
    }

    /**
     * @return the typeTrx
     */
    public String getTypeTrx() {
        return typeTrx;
    }

    /**
     * @param typeTrx the typeTrx to set
     */
    public void setTypeTrx(String typeTrx) {
        this.typeTrx = typeTrx;
    }

    /**
     * @return the row
     */
    public int getRow() {
        return row;
    }

    /**
     * @param row the row to set
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the multiSource
     */
    public String getMultiSource() {
        return multiSource;
    }

    /**
     * @param multiSource the multiSource to set
     */
    public void setMultiSource(String multiSource) {
        this.multiSource = multiSource;
    }

     
}
