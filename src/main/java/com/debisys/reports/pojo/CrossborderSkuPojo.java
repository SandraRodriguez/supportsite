/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

import com.debisys.exceptions.ReportException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class CrossborderSkuPojo extends BasicPojo{

    static Category cat = Category.getInstance(ProviderPojo.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
    
   
    /**
     * 
     * @return
     * @throws ReportException 
     */
    public static ArrayList<CrossborderSkuPojo> getCrossborderSkuPojo() throws ReportException
    {
        ArrayList<CrossborderSkuPojo> crossborderSku = new ArrayList<CrossborderSkuPojo>();
        Connection dbConn = null;
        String strSQL = "";
        
        try{
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = "SELECT p.description, p.id FROM CrossborderCarriers cbc WITH(NOLOCK) " +
                     " INNER JOIN CrossborderSKU cbs WITH(NOLOCK) ON cbc.id = cbs.CarrierID " +
                     " INNER JOIN products p WITH(NOLOCK) ON p.id = cbs.ProductID ";
            
            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL);
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {
                CrossborderSkuPojo crossborderProduct = new CrossborderSkuPojo();
                crossborderProduct.setId( rs.getString("id"));
                crossborderProduct.setDescripton(rs.getString("description"));
                crossborderSku.add( crossborderProduct );
            }
            rs.close();
            pstmt.close();
        }
        catch(Exception e)
        {
            cat.error("Error during getCrossborderSkuPojo ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }
        }
        return crossborderSku;                
    }
    
}
