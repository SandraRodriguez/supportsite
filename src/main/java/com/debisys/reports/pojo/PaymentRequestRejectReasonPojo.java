package com.debisys.reports.pojo;

/**
 *
 * @author fernandob
 */
public class PaymentRequestRejectReasonPojo {

    private String reasonId;
    private String code;
    private String description;

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PaymentRequestRejectReasonPojo{" + "reasonId=" + reasonId + ", code=" + code + ", description=" + description + '}';
    }

    public PaymentRequestRejectReasonPojo() {
        this.reasonId = "";
        this.code = "";
        this.description = "";
    }

    public PaymentRequestRejectReasonPojo(String reasonId, String code, String description) {
        this.reasonId = reasonId;
        this.code = code;
        this.description = description;
    }

}
