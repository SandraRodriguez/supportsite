package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class BasicPojo {
    
    protected String id;
    protected String descripton;
    protected String datetime;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the descripton
     */
    public String getDescripton() {
        return descripton;
    }

    /**
     * @param descripton the descripton to set
     */
    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
    
}
