/**
 * 
 */
package com.debisys.reports.pojo;

import java.util.ArrayList;

/**
 * @author nmartinez
 *
 */
public class SummaryReport {
	
	private ArrayList<SummaryReportPojo> records;
		
	
	/**
	 * 
	 */
	public SummaryReport(){
		setRecords(new ArrayList<SummaryReportPojo>());		
	}


	/**
	 * @return the records
	 */
	public ArrayList<SummaryReportPojo> getRecords() {
		return records;
	}


	/**
	 * @param records the records to set
	 */
	public void setRecords(ArrayList<SummaryReportPojo> records) {
		this.records = records;
	}
	
	
	
	
	
}
