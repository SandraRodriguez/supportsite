/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class ImtuIsoPojo extends BasicPojo {
    
     private ProductPojo product;
     //private BasicPojo Iso;
     private Integer numRatePlans;
     private Integer numTerminals;
     private Integer numActiveTerminals;
     private String totalIsoBuyRate;
     
     public ImtuIsoPojo()
     {
         product = new ProductPojo();                  
     }
    /**
     * @return the product
     */
    public ProductPojo getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(ProductPojo product) {
        this.product = product;
    }   

    /**
     * @return the numRatePlans
     */
    public Integer getNumRatePlans() {
        return numRatePlans;
    }

    /**
     * @param numRatePlans the numRatePlans to set
     */
    public void setNumRatePlans(Integer numRatePlans) {
        this.numRatePlans = numRatePlans;
    }

    /**
     * @return the numTerminals
     */
    public Integer getNumTerminals() {
        return numTerminals;
    }

    /**
     * @param numTerminals the numTerminals to set
     */
    public void setNumTerminals(Integer numTerminals) {
        this.numTerminals = numTerminals;
    }

    /**
     * @return the numActiveTerminals
     */
    public Integer getNumActiveTerminals() {
        return numActiveTerminals;
    }

    /**
     * @param numActiveTerminals the numActiveTerminals to set
     */
    public void setNumActiveTerminals(Integer numActiveTerminals) {
        this.numActiveTerminals = numActiveTerminals;
    }

    /**
     * @return the totalIsoBuyRate
     */
    public String getTotalIsoBuyRate() {
        return totalIsoBuyRate;
    }

    /**
     * @param totalIsoBuyRate the totalIsoBuyRate to set
     */
    public void setTotalIsoBuyRate(String totalIsoBuyRate) {
        this.totalIsoBuyRate = totalIsoBuyRate;
    }
     
}
