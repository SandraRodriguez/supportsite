package com.debisys.reports.pojo;

public class MerchantDetailPojo {

	private String siteId;
	private String datetime;
	private String clerk;
	private String clerk_name;
	private String description;
	private String amount;
	private String comission;
	
	public MerchantDetailPojo(){}
	
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getClerk() {
		return clerk;
	}
	public void setClerk(String clerk) {
		this.clerk = clerk;
	}
	public String getClerk_name() {
		return clerk_name;
	}
	public void setClerk_name(String clerk_name) {
		this.clerk_name = clerk_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getComission() {
		return comission;
	}
	public void setComission(String comission) {
		this.comission = comission;
	}
	
	
	
}
