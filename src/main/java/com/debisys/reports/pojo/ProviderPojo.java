/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

import com.debisys.exceptions.ReportException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class ProviderPojo extends BasicPojo
{
    static Category cat = Category.getInstance(ProviderPojo.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.utils.sql");
    
    private String className;

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }
    
    
    
    
}
