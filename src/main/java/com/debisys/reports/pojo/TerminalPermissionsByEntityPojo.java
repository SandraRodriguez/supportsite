package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class TerminalPermissionsByEntityPojo {
    
    private String idNew;
    private String entityId;
    private String permissionId;    
    private String status;
    private String datetime;
    private String entityName;

    /**
     * 
     */
    public TerminalPermissionsByEntityPojo(){
        
    }
    
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
            
    public String getIdNew() {
        return idNew;
    }

    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }  

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
