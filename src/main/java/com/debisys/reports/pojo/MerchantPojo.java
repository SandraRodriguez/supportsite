/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.pojo;

/**
 *
 * @author nmartinez
 */
public class MerchantPojo {
        
    private String merchant_id;
    private String dba;
    private String legal_businessname;
    private String phys_address;
    private String phys_city;
    private String phys_county;
    private String phys_state;
    private String phys_zip;
    private String phys_country;
    private String repBuss;
    private String rep_id;

    public MerchantPojo(){
        
    }
     
    public String getRepBuss() {
        return repBuss;
    }

    public void setRepBuss(String repBuss) {
        this.repBuss = repBuss;
    }

    public String getRep_id() {
        return rep_id;
    }

    public void setRep_id(String rep_id) {
        this.rep_id = rep_id;
    }
            
    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getDba() {
        return dba;
    }

    public void setDba(String dba) {
        this.dba = dba;
    }

    public String getLegal_businessname() {
        return legal_businessname;
    }

    public void setLegal_businessname(String legal_businessname) {
        this.legal_businessname = legal_businessname;
    }

    public String getPhys_address() {
        return phys_address;
    }

    public void setPhys_address(String phys_address) {
        this.phys_address = phys_address;
    }

    public String getPhys_city() {
        return phys_city;
    }

    public void setPhys_city(String phys_city) {
        this.phys_city = phys_city;
    }

    public String getPhys_county() {
        return phys_county;
    }

    public void setPhys_county(String phys_county) {
        this.phys_county = phys_county;
    }

    public String getPhys_state() {
        return phys_state;
    }

    public void setPhys_state(String phys_state) {
        this.phys_state = phys_state;
    }

    public String getPhys_zip() {
        return phys_zip;
    }

    public void setPhys_zip(String phys_zip) {
        this.phys_zip = phys_zip;
    }

    public String getPhys_country() {
        return phys_country;
    }

    public void setPhys_country(String phys_country) {
        this.phys_country = phys_country;
    }
    
    
    
    
    
}
