/**
 * 
 */
package com.debisys.reports.pojo;

import java.util.ArrayList;

/**
 * @author nmartinez
 *
 */
public class ReportTrxSummaryByAmount {

	private String title;
	private ArrayList<String> warnnings;
	private ArrayList<TrxSummaryByAmountPojo> records;
	private TrxSummaryByAmountPojo columnsDescriptions;
	
	/**
	 * 
	 */
	public ReportTrxSummaryByAmount(){
		records = new ArrayList<TrxSummaryByAmountPojo>();
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the warnnings
	 */
	public ArrayList<String> getWarnnings() {
		return warnnings;
	}
	/**
	 * @param warnnings the warnnings to set
	 */
	public void setWarnnings(ArrayList<String> warnnings) {
		this.warnnings = warnnings;
	}
	/**
	 * @return the records
	 */
	public ArrayList<TrxSummaryByAmountPojo> getRecords() {
		return records;
	}
	/**
	 * @param records the records to set
	 */
	public void setRecords(ArrayList<TrxSummaryByAmountPojo> records) {
		this.records = records;
	}

	/**
	 * @return the columnsDescriptions
	 */
	public TrxSummaryByAmountPojo getColumnsDescriptions() {
		return columnsDescriptions;
	}

	/**
	 * @param columnsDescriptions the columnsDescriptions to set
	 */
	public void setColumnsDescriptions(TrxSummaryByAmountPojo columnsDescriptions) {
		this.columnsDescriptions = columnsDescriptions;
	}
	
		
	
}
