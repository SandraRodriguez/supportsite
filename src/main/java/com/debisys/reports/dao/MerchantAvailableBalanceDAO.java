package com.debisys.reports.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.debisys.exceptions.ReportException;

public class MerchantAvailableBalanceDAO {

	private final static Logger logger = Logger.getLogger(MerchantAvailableBalanceDAO.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	
	public Vector<Vector<Object>> queryAvailableBalanceReport(String query){
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
		try{
			dbConn = getConnection();
			logger.info("AvailableBalanceReport query: " + query);
			ps = dbConn.prepareStatement(query);
			rs = ps.executeQuery();
			while(rs.next()){
				Vector<Object> row = new Vector<Object>();
				row.add(rs.getString("merchant_id"));
				row.add(rs.getString("merchant_name"));
				row.add(rs.getString("rep_name"));
				row.add(rs.getDouble("initial_balance"));
				row.add(rs.getDouble("sale"));
				row.add(rs.getDouble("payment"));
				row.add(rs.getDouble("commission"));
				row.add(rs.getDouble("final_balance"));
				rows.add(row);
			}
			logger.info(String.format("Returned rows %d", rows.size()));
		} catch (Exception e){
			logger.error(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(dbConn);
		}
		return rows;
	}
	
	private Connection getConnection() throws ReportException {
		Connection dbConn = null;
		try{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if(dbConn == null){
				logger.error("Can't get database connection");
                throw new ReportException();
			}
		} catch (TorqueException e){
			logger.error(e);
		} catch(ReportException e){
			logger.error(e);
			throw e;
		}
		return dbConn;
	}
	
	private void closeConnection(Connection dbConn){
		if(dbConn != null){
			Torque.closeConnection(dbConn);
		}
	}
	
	private void closeResultSet(ResultSet rs){
		try{
			if (rs != null) {
				rs.close();
				rs = null;
			}
		} catch(SQLException e){
			logger.error(e);
		}
	}
	
	private void closePreparedStatement(PreparedStatement ps){
		try{
			if (ps != null) {
				ps.close();
				ps = null;
			}
		} catch(SQLException e){
			logger.error(e);
		}
	}

	public ArrayList<HashMap<String, String>> queryGetMerchantsByIso(String query) {
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();
		try{
			dbConn = getConnection();
			logger.info("GetMerchantsByIso query: " + query);
			ps = dbConn.prepareStatement(query);
			rs = ps.executeQuery();
			
			while(rs.next()){
				HashMap<String, String> row = new HashMap<String, String>();
				row.put("merchant_id", rs.getString("merchant_id"));
				row.put("dba", rs.getString("dba"));
				rows.add(row);
			}
		} catch (Exception e){
			logger.error(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(dbConn);
		}
		return rows;
	}
	
	public ArrayList<HashMap<String, String>> queryGetRepsByIso(String query) {
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> rows = new ArrayList<HashMap<String, String>>();
		try{
			dbConn = getConnection();
			logger.info("GetRepsByIso query: " + query);
			ps = dbConn.prepareStatement(query);
			rs = ps.executeQuery();
			
			while(rs.next()){
				HashMap<String, String> row = new HashMap<String, String>();
				row.put("rep_id", rs.getString("rep_id"));
				row.put("businessname", rs.getString("businessname"));
				rows.add(row);
			}
		} catch (Exception e){
			logger.error(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(dbConn);
		}
		return rows;
	}

	public boolean validateMerchantInRep(String query, String merchantId, boolean isSharedBalance, String isoId) {
		boolean result = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			dbConn = getConnection();
			logger.info("GetMerchantsByIso query: " + query);
			int i = 0;
			int sharedBal = (isSharedBalance ? 1 : 0);
			ps = dbConn.prepareStatement(query);
			ps.setLong(++i, Long.parseLong(merchantId));
			ps.setInt(++i, sharedBal);
			ps.setLong(++i, Long.parseLong(isoId));
			ps.setInt(++i, sharedBal);
			rs = ps.executeQuery();
			result = rs.next();
		} catch (Exception e){
			logger.error(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(dbConn);
		}
		return result;
	}

	public boolean validateRepInISO(String query, String repId, boolean isSharedBalance, String isoId) {
		boolean result = false;
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			dbConn = getConnection();
			logger.info("GetMerchantsByIso query: " + query);
			int i = 0;
			int sharedBal = (isSharedBalance ? 1 : 0);
			ps = dbConn.prepareStatement(query);
			ps.setLong(++i, Long.parseLong(repId));
			ps.setLong(++i, Long.parseLong(isoId));
			ps.setInt(++i, sharedBal);
			rs = ps.executeQuery();
			result = rs.next();
		} catch (Exception e){
			logger.error(e);
		} finally {
			closeResultSet(rs);
			closePreparedStatement(ps);
			closeConnection(dbConn);
		}
		return result;
	}
}
