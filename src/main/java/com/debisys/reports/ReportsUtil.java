/**
 * 
 */
package com.debisys.reports;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletContext;

import com.debisys.languages.Languages;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;

/**
 * @author nmartinez
 *
 */
public class ReportsUtil {
	
		
	
	/**
	 * Standar headers to Summary reports
	 * @param sessionData
	 * @param application
	 * @param entityTypeReport
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersSummaryTransactions(SessionData sessionData,ServletContext application , String entityTypeReport)
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		boolean taxPermissionInternationalDefault = sessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  
														DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
															DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

		
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.business_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		else
		{
			headers.add(new ColumnReport("businessname", Languages.getString("jsp.admin.reports.business_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
		
		if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
        {
			headers.add(new ColumnReport("phys_address", Languages.getString("jsp.admin.customers.merchants_info.address", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_city", Languages.getString("jsp.admin.customers.merchants_edit.city", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_state", Languages.getString("jsp.admin.customers.merchants_edit.state_domestic", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_zip", Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
		
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		else
		{
			headers.add(new ColumnReport("rep_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
               
		if(entityTypeReport.equals(DebisysConstants.MERCHANT) && strAccessLevel.equals(DebisysConstants.ISO)){
			headers.add(new ColumnReport("invoice_type", Languages.getString("jsp.admin.reports.transactions.commerce.invoice_type", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
		headers.add( new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Integer.class, true) );
		headers.add(  new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true) );

		if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
				  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{				
			headers.add(new ColumnReport("total_bonus", Languages.getString("jsp.admin.reports.bonus", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			headers.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			
			if(sessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))) 
			{
				headers.add(new ColumnReport("tax_total_sales", Languages.getString("jsp.admin.reports.netAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
				//if ( !entityTypeReport.equals(DebisysConstants.MERCHANT) )
				//{	
					headers.add(new ColumnReport("tax_amount", Languages.getString("jsp.admin.reports.taxAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
				//}
			}
		}
		
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			headers.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));			
			if ( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_iso_commission", Languages.getString("jsp.admin.net_iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}
		}
		if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) 
		        && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			
			headers.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			if( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_agent_commission", Languages.getString("jsp.admin.net_agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		    }
		}		
				
		if (!strAccessLevel.equals(DebisysConstants.REP) && 
				!strAccessLevel.equals(DebisysConstants.MERCHANT) && 
					strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		{
			headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			
			if(  taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_subagent_commission", Languages.getString("jsp.admin.net_subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}
		}
	
		if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
		{
			headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			if( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_rep_commission", Languages.getString("jsp.admin.net_rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}			
		}
		
		headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
		if( taxPermissionInternationalDefault ) 
		{		
			headers.add(new ColumnReport("tax_merchant_commission", Languages.getString("jsp.admin.net_merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			headers.add(new ColumnReport("adj_amount", Languages.getString("jsp.admin.reports.adjustment", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}		
				
		return headers;
	}
        
        
        public static ArrayList<ColumnReport> getHeadersSummaryMerchantTransactions(SessionData sessionData,ServletContext application , String entityTypeReport)
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		boolean taxPermissionInternationalDefault = sessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  
														DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
															DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

		
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.business_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		else
		{
			headers.add(new ColumnReport("businessname", Languages.getString("jsp.admin.reports.business_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
		
		if ( strAccessLevel.equals(DebisysConstants.ISO) && DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
        {
			headers.add(new ColumnReport("phys_address", Languages.getString("jsp.admin.customers.merchants_info.address", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_city", Languages.getString("jsp.admin.customers.merchants_edit.city", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_state", Languages.getString("jsp.admin.customers.merchants_edit.state_domestic", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_zip", Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
		
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		else
		{
			headers.add(new ColumnReport("rep_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
                 if(strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
                    headers.add(new ColumnReport("salesman_id",Languages.getString("jsp.admin.reports.SalesRepId", sessionData.getLanguage()).toUpperCase(), String.class, false));
                    headers.add(new ColumnReport("salesmanname",Languages.getString("jsp.admin.reports.SalesRepName", sessionData.getLanguage()).toUpperCase(), String.class, false));
                     headers.add(new ColumnReport("account_no",Languages.getString("jsp.admin.reports.AccountNo", sessionData.getLanguage()).toUpperCase(), String.class, false));
                }
                 
		if(entityTypeReport.equals(DebisysConstants.MERCHANT) && strAccessLevel.equals(DebisysConstants.ISO)){
			headers.add(new ColumnReport("invoice_type", Languages.getString("jsp.admin.reports.transactions.commerce.invoice_type", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
		headers.add( new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Integer.class, true) );
		headers.add(  new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true) );

		if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
				  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{				
			headers.add(new ColumnReport("total_bonus", Languages.getString("jsp.admin.reports.bonus", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			headers.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			
			if(sessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))) 
			{
				headers.add(new ColumnReport("tax_total_sales", Languages.getString("jsp.admin.reports.netAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
				//if ( !entityTypeReport.equals(DebisysConstants.MERCHANT) )
				//{	
					headers.add(new ColumnReport("tax_amount", Languages.getString("jsp.admin.reports.taxAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
				//}
			}
		}
		
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			headers.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));			
			if ( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_iso_commission", Languages.getString("jsp.admin.net_iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}
		}
		if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) 
		        && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			
			headers.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			if( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_agent_commission", Languages.getString("jsp.admin.net_agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		    }
		}		
				
		if (!strAccessLevel.equals(DebisysConstants.REP) && 
				!strAccessLevel.equals(DebisysConstants.MERCHANT) && 
					strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		{
			headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			
			if(  taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_subagent_commission", Languages.getString("jsp.admin.net_subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}
		}
	
		if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
		{
			headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			if( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_rep_commission", Languages.getString("jsp.admin.net_rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}			
		}
		
		headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
		if( taxPermissionInternationalDefault ) 
		{		
			headers.add(new ColumnReport("tax_merchant_commission", Languages.getString("jsp.admin.net_merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			headers.add(new ColumnReport("adj_amount", Languages.getString("jsp.admin.reports.adjustment", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}		
				
		return headers;
	}
	
	
	
	
	/**
	 * @param sessionData
	 * @param application
	 * @param entityTypeReport
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersSummaryTransactionsTerminals(SessionData sessionData,ServletContext application , String entityTypeReport)
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		boolean taxPermissionInternationalDefault = sessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) &&  
														DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
															DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

		
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.business_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		else
		{
			headers.add(new ColumnReport("businessname", Languages.getString("jsp.admin.reports.business_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
		
		if ( strAccessLevel.equals(DebisysConstants.ISO) && 
        		DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
        {
			headers.add(new ColumnReport("phys_address", Languages.getString("jsp.admin.customers.merchants_info.address", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_city", Languages.getString("jsp.admin.customers.merchants_edit.city", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_state", Languages.getString("jsp.admin.customers.merchants_edit.state_domestic", sessionData.getLanguage()).toUpperCase(), String.class, false));
			headers.add(new ColumnReport("phys_zip", Languages.getString("jsp.admin.customers.merchants_edit.zip_domestic", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
		
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		else
		{
			headers.add(new ColumnReport("rep_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		
		headers.add( new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Integer.class, true) );
		headers.add(  new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true) );

		if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) 
				  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{				
			headers.add(new ColumnReport("total_bonus", Languages.getString("jsp.admin.reports.bonus", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			headers.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			
			if(sessionData.checkPermission((DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))) 
			{
				headers.add(new ColumnReport("tax_total_sales", Languages.getString("jsp.admin.reports.netAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
				headers.add(new ColumnReport("tax_amount", Languages.getString("jsp.admin.reports.taxAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}
		}
		
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			headers.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));			
			if ( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_iso_commission", Languages.getString("jsp.admin.net_iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}
		}
		if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) 
		        && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
			
			headers.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			if( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_agent_commission", Languages.getString("jsp.admin.net_agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		    }
		}		
				
		if (!strAccessLevel.equals(DebisysConstants.REP) && 
				!strAccessLevel.equals(DebisysConstants.MERCHANT) && 
					strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		{
			headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			
			if(  taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_subagent_commission", Languages.getString("jsp.admin.net_subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}
		}
	
		if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
		{
			headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			if( taxPermissionInternationalDefault ) 
			{			
				headers.add(new ColumnReport("tax_rep_commission", Languages.getString("jsp.admin.net_rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			}			
		}
		
		headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
		if( taxPermissionInternationalDefault ) 
		{		
			headers.add(new ColumnReport("tax_merchant_commission", Languages.getString("jsp.admin.net_merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			headers.add(new ColumnReport("adj_amount", Languages.getString("jsp.admin.reports.adjustment", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}		
				
		return headers;
	}
	
	
	
	/**
	 * Use the getHeadersSummaryTransactions method to add headers and add runningliability, liabilitylimit and available_credit column
	 * @param sessionData
	 * @param application
	 * @param entityTypeReport
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersSummaryTransactionsMerchant(SessionData sessionData,ServletContext application , String entityTypeReport)
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		
		headers = getHeadersSummaryMerchantTransactions(sessionData, application, entityTypeReport );
		
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("runningliability", Languages.getString("jsp.admin.reports.balance", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		}
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("liabilitylimit", Languages.getString("jsp.admin.reports.limit", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		}
		if ( entityTypeReport.equals(DebisysConstants.MERCHANT) )
		{
			headers.add(new ColumnReport("available_credit", Languages.getString("jsp.admin.reports.available", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		}	
		
		return headers;
		
	}
	
	
	/**
	 * @param sessionData
	 * @param application
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersCreditOrPaymentSummaryByMerchant(SessionData sessionData, ServletContext application )
	{
            String strAccessLevel = sessionData.getProperty("access_level");
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		 
		headers.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.dba", sessionData.getLanguage()).toUpperCase(), String.class, false));
		headers.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
                if(strAccessLevel.equals(DebisysConstants.ISO) && sessionData.checkPermission(DebisysConstants.SYSTEM_2000_TOOLS)){
                    headers.add(new ColumnReport("salesman_id",Languages.getString("jsp.admin.reports.SalesRepId", sessionData.getLanguage()).toUpperCase(), String.class, false));
                    headers.add(new ColumnReport("salesmanname",Languages.getString("jsp.admin.reports.SalesRepName", sessionData.getLanguage()).toUpperCase(), String.class, false));
                     headers.add(new ColumnReport("account_no",Languages.getString("jsp.admin.reports.AccountNo", sessionData.getLanguage()).toUpperCase(), String.class, false));
                }
		
		if (!DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
    	{
			headers.add(new ColumnReport("RouteName", Languages.getString("jsp.admin.customers.merchants_edit.route", sessionData.getLanguage()).toUpperCase(), String.class, false));
    	}
		headers.add(new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Integer.class, true));
		headers.add(new ColumnReport("total_payments", Languages.getString("jsp.admin.reports.total", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		
		
		return headers;
		
	}
	
	
	/**
	 * @param strAccessLevel
	 * @param strDistChainType
	 * @param strRefId
	 * @param columNameToAppend
	 * @return
	 */
	public static String getChainToBuildWhereSQL( String strAccessLevel, String strDistChainType, String strRefId, String columNameToAppend)
	{
		String strSQLWhere = "";
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
			{
				strSQLWhere =strSQLWhere + " AND  "+ columNameToAppend +" IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + strRefId + ")";
			}
			else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			{

				strSQLWhere = strSQLWhere + " AND  "+ columNameToAppend +" IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
				+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
				+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + ")))";
			}
		}
		else if (strAccessLevel.equals(DebisysConstants.AGENT))
		{
			strSQLWhere = strSQLWhere + " AND  "+ columNameToAppend +" IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
			+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId + "))";
		}
		else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		{
			strSQLWhere = strSQLWhere + " AND  "+ columNameToAppend+" IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id = "
			+ strRefId + ")";
		}
		else if (strAccessLevel.equals(DebisysConstants.REP))
		{
			strSQLWhere = strSQLWhere + " AND  "+ columNameToAppend + " = " + strRefId;
		}
		else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
		{
			strSQLWhere = strSQLWhere + " AND  " + " wt.merchant_id = " + strRefId;
		}
		return " "+strSQLWhere+ " ";		
	}
	
        /**
         * 
         * @param strAccessLevel
         * @param strDistChainType
         * @param strRefId
         * @param columNameToAppend
         * @param isInternal
         * @return 
         */
        public static String getChainToBuildWhereSQLProviderError( String strAccessLevel, String strDistChainType, String strRefId, String columNameToAppend, boolean isInternal)
	{
		String strSQLWhere = "";
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
			if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
			{
				strSQLWhere =strSQLWhere + "  "+ columNameToAppend +" IN (SELECT rep_id FROM reps WITH (NOLOCK) " + (isInternal ? "" :  "WHERE iso_id = " + strRefId) + ")";
			}
			else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			{

				strSQLWhere = strSQLWhere + "  "+ columNameToAppend +" IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " "
                                        + (isInternal ? "" : " AND iso_id IN "
				+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
				+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId )+ ")))";
			}
		}
		else if (strAccessLevel.equals(DebisysConstants.AGENT))
		{
			strSQLWhere = strSQLWhere + "  "+ columNameToAppend +" IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " "
                                + (isInternal ? "" : " AND iso_id IN "
			+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId )+ "))";
		}
		else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		{
			strSQLWhere = strSQLWhere + "  "+ columNameToAppend+" IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " "
                                + (isInternal ? "" : "AND iso_id = "
			+ strRefId )+ ")";
		}
		else if (strAccessLevel.equals(DebisysConstants.REP))
		{
			strSQLWhere = strSQLWhere + " "+ columNameToAppend + " = " + strRefId;
		}
		else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
		{
			strSQLWhere = strSQLWhere + "  " + " wt.merchant_id = " + strRefId;
		}
		return " "+strSQLWhere+ " ";		
	}
	
	
	private static String getDatesRangesByMainTable(String tableName, String startDate, String endDate)
	{
		String sqlDatesRange = "  ( %s.datetime >= '%s' and  %s.datetime < '%s' )  ";
		if ( tableName.equals("ht"))
			return String.format( sqlDatesRange, tableName, startDate, tableName, endDate);
		else
			return String.format( sqlDatesRange, tableName, startDate, tableName, endDate);
	}
	
	
	
	/**
         * 
         * @param strAccessLevel
         * @param strDistChainType
         * @param strRefId
         * @param typeSQL
         * @param startDate
         * @param endDate
         * @param scheduleReport
         * @param productsIds
         * @param merchantsIds
         * @return 
         */
	public static String getSQLTrxErrorSummaryByMerchants(  String strAccessLevel, String strDistChainType, String strRefId, int typeSQL, 
															String startDate, String endDate, ScheduleReport scheduleReport, String productsIds , String merchantsIds)
	{
		StringBuilder sql = new StringBuilder();
		String sqlWhere = getChainToBuildWhereSQLErrorSummary(strAccessLevel, strDistChainType, strRefId, "m", false);
		
		String rangeDateTimesWhenRelativeHostTrx = "_RangeDateTimesWhenRelativeHostTrx_";
		String rangeDateTimesWhenRelativeWebTrx = "_RangeDateTimesWhenRelativeWebTrx_";
		
		HashMap<String,String> hashTokensToReplaceWhenRelativeDates = new HashMap<String,String>();
		hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "ht.datetime");
		hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeWebTrx, "wt.datetime");
		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT && scheduleReport!=null )
		{
			scheduleReport.setHastTokensRelativeDates(hashTokensToReplaceWhenRelativeDates);
		}	
		
		sql.append(" SELECT dba, merchant_id, qty, total_sales, error_count, ");
		sql.append("	   errorPercent = ");
		sql.append("	   CASE typeError ");
		sql.append("		WHEN 1 THEN ");
		sql.append("			(( CONVERT(float,error_count) / ( CONVERT(float,error_count+qty))) * 100 ) ");
		sql.append("		ELSE errorPercent ");
		sql.append("      END	");
		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
		}
		
		sql.append(" FROM ( ");
		
		
		sql.append(" SELECT m.dba, wt.merchant_id, count(wt.rec_id) as qty, sum(amount) as total_sales, ");  
		sql.append("	(SELECT count(millennium_no)  ");
		sql.append("	 FROM hosttransactions ht WITH (NOLOCK)  ");
		sql.append("	 WHERE ");
		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeHostTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("ht", startDate, endDate) );
		}
		
		if ( productsIds != null && !productsIds.equals("") )
		{
			sql.append("	  AND ht.productId IN (	").append(productsIds).append(" )    ");
		}
		sql.append("	  AND ht.millennium_no IN (   SELECT t.millennium_no    ");
		sql.append("								  FROM terminals t WITH (NOLOCK), merchants m WITH (NOLOCK)    ");
		sql.append("								  WHERE m.merchant_id=t.merchant_id and ht.result_code <> 0 and m.merchant_id=wt.merchant_id)) as error_count ");
		sql.append(",     1 typeError , 1 errorPercent  ");
		sql.append(" FROM web_transactions wt WITH (NOLOCK), merchants m WITH (NOLOCK) "); 
		sql.append(" WHERE wt.merchant_id=m.merchant_id AND  ");		
		sql.append( sqlWhere );
		sql.append(" AND ");
		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeWebTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("wt", startDate, endDate) );
		}
		if ( merchantsIds != null && !merchantsIds.equals("") )
		{
			sql.append(" AND m.merchant_id in (").append(merchantsIds).append(") ");			
		}	
		if ( productsIds != null && !productsIds.equals("") )
		{
			sql.append("	  AND wt.id IN (  ").append(productsIds).append(" ) ");
		}		
		sql.append(" GROUP BY wt.merchant_id, m.dba, m.runningliability, m.liabilitylimit  ");
		
		sql.append(" UNION ");
		
		sql.append(" SELECT  m.dba, m.merchant_id,0 qty, 0 total_sales, count(*) as error_count , 0 typeError, 100 errorPercent ");
		sql.append(" FROM hosttransactions ht WITH (NOLOCK), merchants m WITH (NOLOCK), terminals t WITH (NOLOCK) ");
		sql.append(" WHERE  ");
		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeHostTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("ht", startDate, endDate) );
		}
		sql.append("   AND m.merchant_id=t.merchant_id AND ht.result_code <> 0 AND ht.millennium_no=t.millennium_no AND ");		
		sql.append( sqlWhere );
		sql.append("	AND m.merchant_id not in ( SELECT DISTINCT wt.merchant_id  ");	
		sql.append("   							FROM web_transactions wt WITH (NOLOCK), merchants m WITH (NOLOCK)  ");
		sql.append("							WHERE wt.merchant_id=m.merchant_id AND  ");		
		sql.append(" 							").append(sqlWhere);
		sql.append("							AND ");		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeWebTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("wt", startDate, endDate) );
		}
		if ( merchantsIds != null && !merchantsIds.equals("") )
		{
			sql.append(" AND m.merchant_id in (").append(merchantsIds).append(") ");			
		}	
		if ( productsIds != null && !productsIds.equals("") )
		{
			sql.append("	  AND wt.id IN (  ").append(productsIds).append(" ) ");
		}
		sql.append("							GROUP BY wt.merchant_id ");
		sql.append("						 ) ");
		if ( productsIds != null && !productsIds.equals("") )
		{
			sql.append("	  AND ht.productId IN (	").append(productsIds).append(" )    ");
		}
		if ( merchantsIds != null && !merchantsIds.equals("") )
		{
			sql.append(" AND m.merchant_id in (").append(merchantsIds).append(") ");			
		}
		sql.append(" GROUP BY m.merchant_id, m.dba ");
		
		
		sql.append(" ) errorTable	");		
		sql.append(" ORDER BY typeError desc  ");
	
		return sql.toString();
	}
	
	
	//--init search --janez
	public static String getSQLTrxErrorSummaryByTypes(  String strAccessLevel, String strDistChainType, String strRefId, String columNameToAppend , int typeSQL, 
			String startDate, String endDate, ScheduleReport scheduleReport, String productsIds , String resultCodes, boolean isInternal)
	{
		StringBuilder sql = new StringBuilder();
                String sqlWhere = getChainToBuildWhereSQLErrorSummary(strAccessLevel, strDistChainType, strRefId, columNameToAppend, isInternal);               		          
		
		String rangeDateTimesWhenRelativeHostTrx = "_RangeDateTimesWhenRelativeHostTrx_";				
		HashMap<String,String> hashTokensToReplaceWhenRelativeDates = new HashMap<String,String>();
		hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "ht.datetime");
				
		sql.append("SELECT ht.result_code, htrc.description, count(ht.millennium_no) AS error_qty, 1 typegroup, ");
		sql.append("( ");
		sql.append("		SELECT count(ht.millennium_no) ");
		sql.append("		FROM hosttransactions AS ht WITH (NOLOCK) ");
		sql.append("		LEFT OUTER JOIN hosttransactionresultcodes htrc WITH (NOLOCK) ON ht.result_code = htrc.result_id ");
		sql.append("		WHERE ");
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
                    sql.append( rangeDateTimesWhenRelativeHostTrx );
		}
		else
		{
                    sql.append( getDatesRangesByMainTable("ht", startDate, endDate) );
		}	
		sql.append("		AND ht.result_code <> 0 ");
		sql.append(isInternal ? "" : " AND ht.millennium_no IN  ");
		sql.append(isInternal ? "" :"			( ");
		sql.append(isInternal ? "" :"				SELECT t.millennium_no ");
		sql.append(isInternal ? "" :"				FROM terminals t WITH (NOLOCK), merchants m WITH (NOLOCK) ");
		sql.append(isInternal ? "" :"				WHERE m.merchant_id = t.merchant_id AND ");
		sql.append(isInternal ? "" :"				 "+sqlWhere);
		sql.append(isInternal ? "" :"			) " );
                sql.append(") totalCount ");
			
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT && scheduleReport!=null )
		{
			sql.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
			scheduleReport.setHastTokensRelativeDates(hashTokensToReplaceWhenRelativeDates);
		}
		
		sql.append(" FROM hosttransactions AS ht WITH (NOLOCK)  ");
		sql.append(" LEFT OUTER JOIN hosttransactionresultcodes htrc WITH (NOLOCK) ON  ht.result_code = htrc.result_id  ");
		sql.append(" WHERE ");
		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeHostTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("ht", startDate, endDate) );
		}	
		
		sql.append("	AND ht.result_code <> 0 ");
		sql.append(isInternal ? "" :"	AND ht.millennium_no IN ( ");
		sql.append(isInternal ? "" :"								SELECT t.millennium_no ");
		sql.append(isInternal ? "" :"								FROM terminals t WITH (NOLOCK), merchants m WITH (NOLOCK) ");
		sql.append(isInternal ? "" :" 								WHERE m.merchant_id = t.merchant_id AND ");
		sql.append(isInternal ? "" :"								"+sqlWhere);
		sql.append(isInternal ? "" :")");
		if ( productsIds != null && !productsIds.equals("") )
		{
			sql.append("	  AND ht.productId IN (	").append(productsIds).append(" )    ");
		}
		if (resultCodes != null && !resultCodes.equals(""))
		{
			sql.append(" 	  AND ht.result_code IN (").append(resultCodes).append(") ");
		}
		sql.append(" GROUP BY ht.result_code, htrc.description");
		sql.append(" UNION");
		sql.append(" SELECT 0, 'VOID' description, count(millennium_no) AS error_qty, 0 typegroup, ");
		sql.append("  ( ");
		sql.append("		SELECT count(ht.millennium_no) ");
		sql.append("		FROM hosttransactions AS ht WITH (NOLOCK) ");
		sql.append("		LEFT OUTER JOIN hosttransactionresultcodes htrc WITH (NOLOCK) ON ht.result_code = htrc.result_id ");
		sql.append("		WHERE ");
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeHostTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("ht", startDate, endDate) );
		}	
		sql.append("		AND ht.result_code <> 0 ");
		sql.append(isInternal ? "" :"		AND ht.millennium_no IN  ");
		sql.append(isInternal ? "" :"			( ");
		sql.append(isInternal ? "" :"				SELECT t.millennium_no ");
		sql.append(isInternal ? "" :"				FROM terminals t WITH (NOLOCK), merchants m WITH (NOLOCK) ");
		sql.append(isInternal ? "" :"				WHERE m.merchant_id = t.merchant_id AND ");
		sql.append(isInternal ? "" :"				 "+sqlWhere);
		sql.append(isInternal ? "" :"			) ");
                sql.append("  ) totalCount ");
	    
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
		}
		
		sql.append(" FROM hosttransactions ht WITH (NOLOCK) ");
		sql.append(" WHERE millennium_no <> 0 AND ht.void_result_code NOT IN (-1,0) AND ht.type IN (32)  AND ");

		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeHostTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("ht", startDate, endDate) );
		}	
		
		sql.append("		AND ht.result_code <> 0  ");
		sql.append(isInternal ? "" :"		AND ht.millennium_no IN ( ");
		sql.append(isInternal ? "" :"									SELECT t.millennium_no ");
		sql.append(isInternal ? "" :"									FROM terminals t WITH (NOLOCK), merchants m WITH (NOLOCK) ");
		sql.append(isInternal ? "" :"									WHERE m.merchant_id = t.merchant_id AND ");
		sql.append(isInternal ? "" :"				 					"+sqlWhere);
		sql.append(isInternal ? "" :"							    ) ");
		if ( productsIds != null && !productsIds.equals("") )
		{
			sql.append("	  AND ht.productId IN (	").append(productsIds).append(" )    ");
		}
		if (resultCodes != null && !resultCodes.equals(""))
		{
			sql.append(" 	  AND ht.result_code IN (").append(resultCodes).append(") ");
		}
		sql.append("ORDER BY typegroup DESC, error_qty DESC ");
		sql.append("OPTION (MAXDOP 1) ");
                
                if (isInternal) {
                    return "SELECT ht.result_code, htrc.description, count(ht.millennium_no) as error_qty,1 typegroup  "
                            + "FROM hosttransactions AS ht WITH (NOLOCK) LEFT OUTER JOIN hosttransactionresultcodes htrc WITH (NOLOCK) ON ht.result_code=htrc.result_id   "
                            + "WHERE " + getDatesRangesByMainTable("ht", startDate, endDate)
                            + "AND ht.result_code <> 0 AND ht.millennium_no  IN (select t.millennium_no    "
                            + "FROM terminals t WITH (NOLOCK), merchants m WITH (NOLOCK)    "
                            + "WHERE m.merchant_id=t.merchant_id )"
                            + (productsIds != null && !productsIds.equals("") ? " AND ht.productId IN (" + productsIds + ") " : "")
                            + (resultCodes != null && !resultCodes.equals("") ? " AND ht.result_code IN (" + resultCodes + ") " : "")
                            + "GROUP BY ht.result_code, htrc.description  "
                            + "ORDER BY error_qty DESC OPTION (MAXDOP 1)";                            
                } else {
                    return sql.toString();
                }						
	}
	
	
	
	
	/**
         * 
         * @param strAccessLevel
         * @param strDistChainType
         * @param strRefId
         * @param columNameToAppend
         * @param typeSQL
         * @param startDate
         * @param endDate
         * @param scheduleReport
         * @param productsIds
         * @param resultCodes
         * @param providerId
         * @return 
         */
	public static String getSQLTrxErrorSummaryByProvider(  String strAccessLevel, 
                                                               String strDistChainType,
                                                               String strRefId, 
                                                               String columNameToAppend , 
                                                               int typeSQL, 
                                                               String startDate, 
                                                               String endDate, 
                                                               ScheduleReport scheduleReport,
                                                               String productsIds , 
                                                               String resultCodes, 
                                                               String providerId,
                                                               boolean isInternal)
	{
		StringBuilder sqlMainQuery = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();
		String sqlWhere = getChainToBuildWhereSQLProviderError( strAccessLevel, strDistChainType, strRefId, columNameToAppend, isInternal);
		
		String rangeDateTimesWhenRelativeHostTrx = "_RangeDateTimesWhenRelativeHostTrx_";				
		HashMap<String,String> hashTokensToReplaceWhenRelativeDates = new HashMap<String,String>();
		hashTokensToReplaceWhenRelativeDates.put( rangeDateTimesWhenRelativeHostTrx, "ht.datetime");
		
		sqlMainQuery.append(" SELECT ht.H2HProviderResultCode, pem.provider_error_message, COUNT(ht.millennium_no) AS error_qty ");
				
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT && scheduleReport!=null )
		{
			sqlMainQuery.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
			scheduleReport.setHastTokensRelativeDates(hashTokensToReplaceWhenRelativeDates);
		}
		
		 //sqlCount+") totalCount " )
		sqlCount.append(", ( SELECT count(ht.millennium_no) ");
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		sql.append(" FROM hosttransactions AS ht WITH (NOLOCK) ");
		sql.append(" INNER JOIN ProviderErrorMessages pem WITH (NOLOCK) ON ht.provider_id = pem.provider_id AND ht.H2HProviderResultCode = pem.provider_error_code ");
		sql.append(" INNER JOIN	( ");
		sql.append("		SELECT t.millennium_no ");
		sql.append("		FROM terminals t WITH (NOLOCK) INNER JOIN merchants m WITH (NOLOCK) ON m.merchant_id = t.merchant_id ");
		sql.append("		WHERE ");
		sql.append( sqlWhere );
		sql.append("			) tr ON tr.millennium_no = ht.millennium_no ");
		sql.append("WHERE  ");		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append( rangeDateTimesWhenRelativeHostTrx );
		}
		else
		{
			sql.append( getDatesRangesByMainTable("ht", startDate, endDate) );
		}		
		sql.append("AND ht.result_code <> 0 ");
		sql.append("AND ht.provider_id = ").append(providerId);
		if (resultCodes != null && !resultCodes.equals("") && !resultCodes.equals("-1") )
		{
			sql.append(" 	  AND ht.H2HProviderResultCode IN (").append(resultCodes).append(") ");
		}
		if ( productsIds != null && !productsIds.equals("") )
		{
			sql.append("	  AND ht.productId IN (	").append(productsIds).append(" )    ");
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////
		//ESTE HACE EL CONTEO
                sqlCount.append(sql).append(") totalCount ");		
		///////////////////////////////////////////////
		
		sqlMainQuery.append( sqlCount );
		sqlMainQuery.append( sql );
		sqlMainQuery.append("GROUP BY ht.H2HProviderResultCode, pem.provider_error_message ");
		sqlMainQuery.append("ORDER BY error_qty DESC ");
		sqlMainQuery.append("OPTION (MAXDOP 1) ");
		
		
		return sqlMainQuery.toString();
	}
	
	
	
	
	
	public static String getSQLTrxSummaryByPhoneNumber(String strAccessLevel, String strDistChainType, String strRefId, String columNameToAppend , int typeSQL, 
			   						   				   String startDate, String endDate, ScheduleReport scheduleReport, String phone )
	{
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();
		String sqlWhere = getChainToBuildWhereSQL( strAccessLevel, strDistChainType, strRefId, columNameToAppend );
		String strWhereDateTimeCurrent  = "";
		String strWhereDateTimePrevious = "";
		String getDateDB = " GETDATE() ";
		
		
		sql.append(" SELECT SUM(sale_amount) 			    AS sale_amount, ");
		sql.append("	    SUM(taxtypecheck) 			    AS taxtypecheck, ");
		sql.append("	    SUM(bonus) 					    AS bonus, ");
		//sql.append("	    COUNT(DISTINCT merchantId)-1    AS location_count, ");
		sql.append("        location_count = CASE COUNT(DISTINCT merchantId)-1 WHEN -1 THEN 0 ELSE COUNT(DISTINCT merchantId) - 1 END, ");
		sql.append("	    SUM(tax)					    AS tax, ");
		sql.append("	    SUM(sale_amount) - SUM(tax)	    AS net_amount, ");		
		sql.append("	    SUM(sale_amount) + SUM(bonus)   AS total_Recharge,");
		//sql.append("	    COUNT(distinct recId)-1         AS totalTrx, ");
		
		sql.append("        totalTrx = CASE COUNT(DISTINCT recId)-1 WHEN -1 THEN 0 ELSE COUNT(DISTINCT recId) - 1 END, ");
		
		sql.append("	    SUM(sale_amountP) 			    AS sale_amountP, ");
		sql.append(" 		SUM(taxtypecheckP) 			    AS taxtypecheckP, ");
		sql.append(" 		SUM(bonusP) 				    AS bonusP, ");
		//sql.append(" 		COUNT(DISTINCT merchantIdP)-1   AS location_countP,");
		sql.append("        location_countP = CASE COUNT(DISTINCT merchantIdP)-1 WHEN -1 THEN 0 ELSE COUNT(DISTINCT merchantIdP) - 1 END, ");
		sql.append(" 		SUM(taxP) 					    AS taxP,");
		sql.append(" 		SUM(sale_amountP) - SUM(taxP)   AS net_amountP, ");
		sql.append("	    SUM(sale_amountP) + SUM(bonusP) AS total_RechargeP,");
		//sql.append(" 		COUNT(distinct recIdP)-1 	    AS totalTrxP, ");
		sql.append("        totalTrxP = CASE COUNT(DISTINCT recIdP)-1 WHEN -1 THEN 0 ELSE COUNT(DISTINCT recIdP) - 1 END, ");
		
		sql.append( additionalFieldsTrxSummaryByPhone() );
		
		String dateCurrentStart = " CONVERT(VARCHAR(10), dateadd(m,0,'"+ startDate +"'), 121) ";
		String dateCurrentEnd   = " CONVERT(VARCHAR(10), dateadd(m,0,'"+  endDate  +"'), 121) ";
		
		if ( typeSQL == DebisysConstants.SCHEDULE_REPORT )
		{
			sql.append(" CONVERT(VARCHAR(10),dateadd(m,-1,").append(getDateDB).append("), 121) startDate, ");
			sql.append(" CONVERT(VARCHAR(10),dateadd(m,0,").append(getDateDB).append("), 121) endDate, ");
			sql.append(" CONVERT(VARCHAR(10),dateadd(m,-2,").append(getDateDB).append("), 121) startDateP, ");
			sql.append(" CONVERT(VARCHAR(10),dateadd(m,-1,").append(getDateDB).append("), 121) endDateP ");
			
			strWhereDateTimeCurrent  = " AND wt.DATETIME >= CONVERT(VARCHAR(10),dateadd(m,-1,"+ getDateDB +"), 121) AND wt.DATETIME < CONVERT(VARCHAR(10),dateadd(m,0,"+ getDateDB +"), 121) ";
			
			strWhereDateTimePrevious = " AND wt.DATETIME >= CONVERT(VARCHAR(10),dateadd(m,-2,"+ getDateDB +"), 121)  AND wt.DATETIME < CONVERT(VARCHAR(10),dateadd(m,-1,"+ getDateDB +"), 121) ";
			
		}
		else
		{
			String datePreviuosStart = " CONVERT(VARCHAR(10), dateadd(m,-1, '"+startDate+"'), 121) ";
			String datePreviuosEnd = " CONVERT(VARCHAR(10), dateadd(m,-1, '"+endDate+"'), 121) ";
			sql.append(" 		  ").append(dateCurrentStart).append(" startDate, ").append(dateCurrentEnd).append(" endDate, ").append(datePreviuosStart).append(" startDateP, ").append(datePreviuosEnd).append(" endDateP ");
			
			strWhereDateTimeCurrent  = "		AND wt.datetime >= '"+startDate+"' AND wt.datetime < '"+endDate+"' ";
			
			strWhereDateTimePrevious = "		AND wt.datetime >= dateadd(m,-1, '"+startDate+"') AND wt.datetime < dateadd(m,-1, '"+endDate+"') ";
		
		}	
		
		sql.append(" FROM ");
		sql.append(" (  ");
		sql.append(" 	SELECT wt.amount				AS sale_amount, ");
		sql.append("		   isnull(wt.taxtype,0)		AS taxtypecheck, ");
		sql.append("	   	   wt.bonus_amount			AS bonus,  ");
		sql.append("           wt.taxpercentage / 10000 AS taxpercentage,  ");
		sql.append("	       wt.merchant_id			AS merchantId,  ");
		sql.append("	       Tax = CASE wt.taxpercentage / 10000 WHEN 0 THEN 0 ELSE ( wt.amount - ( wt.amount / (1+(wt.taxpercentage / 10000)))) END, ");
		sql.append("		   wt.rec_id				AS recId, ");
		sql.append("	   		0						AS sale_amountP, ");
		sql.append("	   		0						AS taxtypecheckP, ");
		sql.append("	   		0						AS bonusP, ");
		sql.append("	   		0						AS taxpercentageP, ");
		sql.append("	   		0						AS merchantIdP,  ");
		sql.append("	   		0						AS taxP, ");
		sql.append("	   		0						AS recIdP ");
		sql.append(" 	FROM web_transactions wt WITH (NOLOCK), products p WITH (NOLOCK) where wt.id = p.id and p.trans_type = 3 ");
		sql.append( sqlWhere );		
		sql.append( strWhereDateTimeCurrent );
		sql.append("		AND wt.ani = '").append(phone).append("' ");
		sql.append(" UNION ");
		sql.append("	SELECT ");
		sql.append("	   		0						 AS sale_amount,  ");
		sql.append("	   		0						 AS taxtypecheck, ");
		sql.append("	   		0						 AS bonus, ");
		sql.append("	   		0						 AS taxpercentage, ");
		sql.append("	   		0						 AS merchantId, ");
		sql.append("	   		0						 AS Tax, ");
		sql.append("       		0						 AS recId,  ");
		sql.append("	   		wt.amount				 AS sale_amountP, ");
		sql.append("	   		isnull(wt.taxtype,0)	 AS taxtypecheckP, ");
		sql.append("	   		wt.bonus_amount			 AS bonusP, ");
		sql.append("	   		wt.taxpercentage / 10000 AS taxpercentageP, ");
		sql.append("      		wt.merchant_id			 AS merchantIdP, ");
		sql.append("	   		taxP = CASE wt.taxpercentage / 10000 WHEN 0 THEN 0 ELSE ( wt.amount - ( wt.amount / (1+(wt.taxpercentage / 10000))))	END, ");
		sql.append("	   		wt.rec_id				 AS recIdP 	");
		sql.append(" 	FROM web_transactions wt WITH (NOLOCK), products p WITH (NOLOCK) where wt.id = p.id and p.trans_type = 3 ");
		sql.append( sqlWhere );
		sql.append( strWhereDateTimePrevious );
		sql.append("		AND wt.ani = '").append(phone).append("' ");
		sql.append(") trx");
		
		return sql.toString();
	}
	
	
	/**
	 * This fields are put in the select at the final so summarize total rearchae, taxes, #TopUps, etc.
	 * @return
	 */
	private static String additionalFieldsTrxSummaryByPhone()
	{
		StringBuilder sql = new StringBuilder();
		sql.append(" percentTopUps = CASE (COUNT(DISTINCT recIdP) - 1) WHEN 0 THEN 100  ");
		sql.append("	ELSE ((((COUNT(DISTINCT recId) - 1) - (COUNT(DISTINCT recIdP) - 1))) / CONVERT(DECIMAL(9, 2), (COUNT(DISTINCT recIdP) - 1)))  END "); //* 100
		sql.append(" ,percentRecharge = CASE SUM(sale_amountP) WHEN 0 THEN 100 ");
		sql.append("	ELSE ((((SUM(sale_amount)) - (SUM(sale_amountP)))) / CONVERT(DECIMAL(9, 2), (SUM(sale_amountP)))) END ");
		sql.append(",percentBonus = CASE SUM(bonusP) WHEN 0 THEN 0");
		sql.append("	ELSE ((((SUM(bonus)) - (SUM(bonusP)))) / CONVERT(DECIMAL(9, 2), (SUM(bonusP)))) END");
		sql.append(",percentTotalRecharge = CASE (SUM(sale_amountP) + SUM(bonusP)) WHEN 0 THEN 0");
		sql.append("	ELSE (((( (SUM(sale_amount) + SUM(bonus))  ) - ( (SUM(sale_amountP) + SUM(bonusP))  ))) / CONVERT(DECIMAL(9, 2), ( (SUM(sale_amountP) + SUM(bonusP))   ))) END");
		sql.append(",percentNetAmt = CASE (SUM(sale_amountP) - SUM(TaxP)) WHEN 0 THEN 0");
		sql.append("	ELSE (((( (SUM(sale_amount) - SUM(Tax))  ) - ( (SUM(sale_amountP) - SUM(TaxP))  ))) / CONVERT(DECIMAL(9, 2), ( (SUM(sale_amountP) - SUM(TaxP))   ))) END");
		sql.append(",percentTaxAmt = CASE SUM(taxP) WHEN 0 THEN 0");
		sql.append("	ELSE (((( SUM(tax)  ) - ( SUM(taxP)  ))) / CONVERT(DECIMAL(9, 2), ( SUM(taxP)   ))) END");
		sql.append(",percentLocationCount = CASE (COUNT(DISTINCT merchantIdP) - 1) WHEN 0 THEN 100");
		sql.append("	ELSE ( (((COUNT(DISTINCT merchantId) - 1) - (COUNT(DISTINCT merchantIdP) - 1))) / CONVERT(DECIMAL(9, 2), (COUNT(DISTINCT merchantIdP) - 1))) END, ");
			
		return sql.toString();
	}
		
		
	private static String getChainToBuildWhereSQLErrorSummary( String strAccessLevel, String strDistChainType, String strRefId, String columNameToAppend, boolean isInternal)
	{
		String strSQLWhere = "";
                if(!isInternal) {
                    if (strAccessLevel.equals(DebisysConstants.ISO))
                    {
                            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
                            {
                                    strSQLWhere = strSQLWhere + columNameToAppend +".rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) " + (isInternal ? "" : "WHERE iso_id = "  + strRefId) + ")";
                            }
                            else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                            {

                                    strSQLWhere = strSQLWhere + columNameToAppend +".rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
                                    + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
                                    + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + (isInternal ? "" :  " AND iso_id = " + strRefId )+ ")))";
                            }
                    }
                    else if (strAccessLevel.equals(DebisysConstants.AGENT))
                    {
                            strSQLWhere = strSQLWhere + columNameToAppend +".rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " "
                                    + (isInternal ? "": " AND iso_id IN "
                            + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId )+ "))";
                    }
                    else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
                    {
                            strSQLWhere = strSQLWhere + columNameToAppend+".rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " "
                                    + (isInternal ? "" :  "AND iso_id = "
                            + strRefId )+ ")";
                    }
                    else if (strAccessLevel.equals(DebisysConstants.REP))
                    {
                            strSQLWhere = strSQLWhere + columNameToAppend + ".rep_id = " + strRefId;
                    }
                    else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
                    {
                            strSQLWhere =  columNameToAppend+".merchant_id = " + strRefId;
                    }                    
                }
		return " "+strSQLWhere+ " ";		
	}

	
        /**
	 * @param sessionData
	 * @param application
	 * @return
	 */
	public static ArrayList<ColumnReport> getHeadersSummaryTrxByClerk(SessionData sessionData, ServletContext application)
	{
            StringBuilder sortJavascript = new StringBuilder();
            boolean bUseTaxValue = false;
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
                
            ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
            headers.add(new ColumnReport("clerk_name", Languages.getString("jsp.admin.reports.clerk", sessionData.getLanguage()).toUpperCase(), String.class, false));
            headers.add(new ColumnReport("merchant_name", Languages.getString("jsp.admin.reports.merchant_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
            headers.add(new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Integer.class, true));
            headers.add(new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            
            if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
                    DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
            {
                headers.add(new ColumnReport("bonus_amount", Languages.getString("jsp.admin.reports.bonus", sessionData.getLanguage()).toUpperCase(), Double.class, true));
                headers.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            }
            if ( bUseTaxValue )            
            {
                headers.add(new ColumnReport("", Languages.getString("jsp.admin.reports.total", sessionData.getLanguage()).toUpperCase(), String.class, false));
                //<TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage())%></TD>                
            }
            if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                      DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                         DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
            {
                headers.add(new ColumnReport("tax_total_sales", Languages.getString("jsp.admin.reports.netAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
                headers.add(new ColumnReport("total_tax", Languages.getString("jsp.admin.reports.taxAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            }
            ColumnReport avg_per_trans = new ColumnReport("avg_per_trans", Languages.getString("jsp.admin.reports.avg_per_trans", sessionData.getLanguage()).toUpperCase(), Double.class, false);
            avg_per_trans.setValueExpressionWhenIsCalculate("$V{tottotal_sales}/$V{totqty}");
            headers.add(avg_per_trans);		            
            
            String sortString = "\"None\", \"CaseInsensitiveString\", \"CaseInsensitiveString\", \"Number\", \"Number\", \"Number\"";
            if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                    DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                       DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
            {
                sortString = sortString + ",\"Number\"";
                sortString = sortString + ",\"Number\"";
            }
            if ( bUseTaxValue )
            {
                sortString += ", \"Number\"";
            }
            if (strAccessLevel.equals(DebisysConstants.ISO))
            {
                sortString += ",\"Number\"";
                headers.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
            }
            if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                    strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
                sortString += ",\"Number\"";
                headers.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
            }
             if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                        strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
                sortString += ",\"Number\"";
                headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
                
            }
            if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
            {
                sortString += ",\"Number\"";
                headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
            }                
            sortString += ",\"Number\"";
            headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));		
            sortJavascript.append(sortString);
            sessionData.setProperty("sortJavascript", sortJavascript.toString());
            return headers;		
	}
        
        /**
         * 
         * @param sessionData
         * @param application
         * @param bUseTaxValue
         * @return 
         */
        public static ArrayList<ColumnReport> getHeadersSummaryTrxByProduct(SessionData sessionData, ServletContext application, boolean bUseTaxValue)
	{
            ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
            boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(sessionData, application);
            
            //this flag indicates if the user has:
            //The Tax Persmission in SS reports.
            //The deployment type be International
            //The custom config type be default
            boolean permissionInterCustomConfig = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                                                    DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                                                        DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);


                //this flag means if the user is in International Default
            boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                                                DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

            String minusvat = Languages.getString("jsp.admin.reports.minusvat",sessionData.getLanguage());
            
            headers.add(new ColumnReport("description", Languages.getString("jsp.admin.reports.transactions.products_summary.product_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
            headers.add(new ColumnReport("id", Languages.getString("jsp.admin.reports.id", sessionData.getLanguage()).toUpperCase(), String.class, false));
            headers.add(new ColumnReport("qty", Languages.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Integer.class, true));
            headers.add(new ColumnReport("total_sales", Languages.getString("jsp.admin.reports.recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
     
            if ( isInternationalDefault )
            {
                headers.add(new ColumnReport("total_bonus", Languages.getString("jsp.admin.reports.bonus", sessionData.getLanguage()).toUpperCase(), Double.class, true));
                headers.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            }
            if ( bUseTaxValue )
            {
                headers.add(new ColumnReport("total", Languages.getString("jsp.admin.reports.total", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            }
            if ( permissionInterCustomConfig) 
            {
                headers.add(new ColumnReport("tax_total_sales", Languages.getString("jsp.admin.reports.netAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
                headers.add(new ColumnReport("taxAmount", Languages.getString("jsp.admin.reports.taxAmount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            }
            if (strAccessLevel.equals(DebisysConstants.ISO))
            {
                headers.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
                if ( bUseTaxValue )
                {   
                    headers.add(new ColumnReport("iso_percentM", Languages.getString("jsp.admin.iso_percent", sessionData.getLanguage()).toUpperCase()+minusvat, Double.class, true));
                }
                if ( permissionInterCustomConfig) 
                {
                    headers.add(new ColumnReport("tax_iso_commission", Languages.getString("jsp.admin.net_iso_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
                }
	    }            
            if ((strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) && 
                    strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
                headers.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
                if ( bUseTaxValue )
                {
                    headers.add(new ColumnReport("agent_percentM", Languages.getString("jsp.admin.agent_percent", sessionData.getLanguage()).toUpperCase()+minusvat, Double.class, true));               
                }
                if(permissionInterCustomConfig)
                {
                    headers.add(new ColumnReport("tax_agent_commission", Languages.getString("jsp.admin.net_agent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));                                   
                }
            }            
            if (!strAccessLevel.equals(DebisysConstants.REP) && !strAccessLevel.equals(DebisysConstants.MERCHANT) && 
                       strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
                headers.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
                if ( bUseTaxValue )
                {
                    headers.add(new ColumnReport("subagent_percentM", Languages.getString("jsp.admin.subagent_percent", sessionData.getLanguage()).toUpperCase()+minusvat, Double.class, true));                                   
                }
                if ( permissionInterCustomConfig ) 
                {
                    headers.add(new ColumnReport("tax_subagent_commission", Languages.getString("jsp.admin.net_subagent_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));                                   
                }
            }
            if (!strAccessLevel.equals(DebisysConstants.MERCHANT))
            {
                headers.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
                if ( bUseTaxValue )
                {
                    headers.add(new ColumnReport("rep_percentM", Languages.getString("jsp.admin.rep_percent", sessionData.getLanguage()).toUpperCase()+minusvat, Double.class, true));               
                }
                if ( permissionInterCustomConfig )
                {
                    headers.add(new ColumnReport("tax_rep_commission", Languages.getString("jsp.admin.net_rep_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
                }
            }            
            headers.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
            if ( bUseTaxValue )
            {
                headers.add(new ColumnReport("merchant_percentM", Languages.getString("jsp.admin.merchant_percent", sessionData.getLanguage()).toUpperCase()+minusvat, Double.class, true));               
            }
            if ( permissionInterCustomConfig )
            {
                headers.add(new ColumnReport("tax_merchant_commission", Languages.getString("jsp.admin.net_merchant_percent", sessionData.getLanguage()).toUpperCase(), Double.class, true));               
            }
            if (sessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
            {
                //channel_percent
                String channel_percentExp = "(($V{totiso_commission}+$V{totagent_commission}+$V{totsubagent_commission}+$V{totrep_commission}+$V{totmerchant_commission})/$V{tottotal_sales})*100";
                ColumnReport channel_percent = new ColumnReport("channel_percent", Languages.getString("jsp.admin.channel_percent", sessionData.getLanguage()).toUpperCase(), Double.class, false);
                channel_percent.setValueExpressionWhenIsCalculate( channel_percentExp );
                
                headers.add( channel_percent );               
            }
            if (strAccessLevel.equals(DebisysConstants.ISO))
            {
                headers.add(new ColumnReport("adj_amount", Languages.getString("jsp.admin.reports.adjustment", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            }
            if (isIntlAndHasDataPromoPermission) headers.add(new ColumnReport("PromoData", Languages.getString("jsp.reports.reports.transactions.form.table.column.name.promoData", sessionData.getLanguage()).toUpperCase(), String.class, false));
            return headers;
        }
        
        /**
         * 
         * @param sessionData
         * @param application
         * @param bUseTaxValue
         * @return 
         */
        public static ArrayList<ColumnReport> getHeadersSummaryTrxByProductMerchant(SessionData sessionData, ServletContext application, boolean bUseTaxValue)
	{
            ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
            String strAccessLevel = sessionData.getProperty("access_level");
            String strDistChainType = sessionData.getProperty("dist_chain_type");
                
            boolean addColumnsDTU1281jsp = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_ADDITIONAL_COLUMNS_TRX_SUMM_PRODUCT_MERCHANT_REPORT);
           
            addColumnToReport(sessionData, headers, "Agent", "jsp.admin.report.transactions.products.merchants.agent", String.class, false );
            addColumnToReport(sessionData, headers, "Subagent", "jsp.admin.report.transactions.products.merchants.subagent", String.class, false );
            addColumnToReport(sessionData, headers, "Rep", "jsp.admin.report.transactions.products.merchants.rep", String.class, false );
            addColumnToReport(sessionData, headers, "MerchantName", "jsp.admin.report.transactions.products.merchants.merchant", String.class, false );
            addColumnToReport(sessionData, headers, "merchant_id", "jsp.admin.report.transactions.products.merchants.merchantID", String.class, false );
            addColumnToReport(sessionData, headers, "phys_city", "jsp.admin.report.transactions.products.merchants.city", String.class, false );
                        
            if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)  
                || 
                (    
                    DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  
                    && 
                    DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) 
                )  
            )
            { 
                addColumnToReport(sessionData, headers, "state", "jsp.admin.report.transactions.products.merchants.state", String.class, false );
                addColumnToReport(sessionData, headers, "RechargeValue", "jsp.admin.report.transactions.products.merchants.recharegevalue", Double.class, false );
            }
            else
            {
                addColumnToReport(sessionData, headers, "RechargeValue", "jsp.admin.report.transactions.products.merchants.recharegevalue", Double.class, false );
                addColumnToReport(sessionData, headers, "bonus", "jsp.admin.report.transactions.products.merchants.bonusAmount", Double.class, false );
                addColumnToReport(sessionData, headers, "TotalRecharge", "jsp.admin.report.transactions.products.merchants.totalRecharge", Double.class, false );
                if( sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
                { 
                    addColumnToReport(sessionData, headers, "netamount", "jsp.admin.reports.netAmount", Double.class, false );
                    addColumnToReport(sessionData, headers, "taxamount", "jsp.admin.reports.taxAmount", Double.class, false );                 
                }
            }                    
            addColumnToReport(sessionData, headers, "id", "jsp.admin.report.transactions.products.merchants.SKU", String.class, false );
            addColumnToReport(sessionData, headers, "description", "jsp.admin.report.transactions.products.merchants.Product", String.class, false );
            addColumnToReport(sessionData, headers, "lasttransaction", "jsp.admin.report.transactions.products.merchants.lasttransaction", String.class, false );           
                      
            if ( strAccessLevel.equals(DebisysConstants.ISO) )
            { 
                addColumnToReport(sessionData, headers, "iso_rate", "jsp.admin.report.transactions.products.merchants.iso_percent", Double.class, false );                 
            }
            if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {   
                if ( strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) )
                { 
                    addColumnToReport(sessionData, headers, "agent_rate", "jsp.admin.report.transactions.products.merchants.agent_percent", Double.class, false );
                }  
                if ( strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) )
                { 
                    addColumnToReport(sessionData, headers, "subagent_rate", "jsp.admin.report.transactions.products.merchants.subagent_percent", Double.class, false );
                }
            }
            if ( strAccessLevel.equals(DebisysConstants.ISO)  || strAccessLevel.equals(DebisysConstants.AGENT)   
                    || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP) )
            { 
                addColumnToReport(sessionData, headers, "rep_rate", "jsp.admin.report.transactions.products.merchants.rep_percent", Double.class, false );
            } 
            addColumnToReport(sessionData, headers, "merchant_rate", "jsp.admin.report.transactions.products.merchants.merchant_percent", Double.class, false );
            addColumnToReport(sessionData, headers, "count", "jsp.admin.report.transactions.products.merchants.trxnumber", Integer.class, false );
            if( addColumnsDTU1281jsp )
            { 
                addColumnToReport(sessionData, headers, "sales", "jsp.admin.report.transactions.products.merchants.salesTitle", Integer.class, false );
                addColumnToReport(sessionData, headers, "ret", "jsp.admin.report.transactions.products.merchants.returnsTitle", Integer.class, false );
                addColumnToReport(sessionData, headers, "netTrx", "jsp.admin.report.transactions.products.merchants.netTitle", Integer.class, false );  
            }            
            return headers;
        }
        
        /**
         * 
         * @param sessionData
         * @param headers
         * @param sqlColumnName
         * @param nameResourceLanguage
         * @param typeData
         * @param addTotalToReportTemplate 
         */
        private static void addColumnToReport(SessionData sessionData, ArrayList<ColumnReport> headers, String sqlColumnName, 
                                        String nameResourceLanguage, Class typeData, boolean addTotalToReportTemplate){
            headers.add(new ColumnReport(sqlColumnName, Languages.getString(nameResourceLanguage, sessionData.getLanguage()).toUpperCase(), 
                            typeData, addTotalToReportTemplate));
        }
        
        
        	public static ArrayList<ColumnReport> getRepTransferSummaryTransactions(SessionData sessionData,ServletContext application , String entityTypeReport)
	{
		ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();
		 headers.add(new ColumnReport("username", Languages.getString("jsp.admin.reports.rep.transfers.username", sessionData.getLanguage()).toUpperCase(), String.class, false));
                  headers.add(new ColumnReport("senderId", Languages.getString("jsp.admin.reports.rep.transfers.senderID", sessionData.getLanguage()).toUpperCase(), String.class, false));
                  headers.add(new ColumnReport("senderName", Languages.getString("jsp.admin.reports.rep.transfers.senderName", sessionData.getLanguage()).toUpperCase(), String.class, false));

                headers.add(new ColumnReport("receiverId", Languages.getString("jsp.admin.reports.rep.transfers.receieverID", sessionData.getLanguage()).toUpperCase(), String.class, false));
                headers.add(new ColumnReport("receiverName", Languages.getString("jsp.admin.reports.rep.transfers.receieverName",sessionData.getLanguage()).toUpperCase(), String.class, false));
                headers.add(new ColumnReport("senderPaymentID", Languages.getString("jsp.admin.reports.rep.transfers.sendPaymentID", sessionData.getLanguage()).toUpperCase(), String.class, false));
                 headers.add(new ColumnReport("receiverPaymentID", Languages.getString("jsp.admin.reports.rep.transfers.receiverPaymentID", sessionData.getLanguage()).toUpperCase(), String.class, false));
                headers.add(new ColumnReport("Amount", Languages.getString("jsp.admin.reports.rep.transfers.paymentAmount", sessionData.getLanguage()).toUpperCase(), String.class, true));
                headers.add(new ColumnReport("ip", Languages.getString("jsp.admin.reports.rep.transfers.ip", sessionData.getLanguage()).toUpperCase(), String.class, false));
               headers.add(new ColumnReport("date", Languages.getString("jsp.admin.reports.rep.transfers.date", sessionData.getLanguage()).toUpperCase(), String.class, false));
                return headers;
	}
    
    public static boolean isIntlAndHasDataPromoPermission(SessionData sessionData, ServletContext context) {
        try {
            return (sessionData.checkPermission(DebisysConstants.PERM_ALLOW_PROMO_DATA_COLUMN_IN_TRANSACTIONS_REPORT) && 
		    DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
		    DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT));
        } catch (Exception e) {
            return false;
        }
    }         
}
