package com.debisys.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.debisys.customers.Merchant;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.emida.utils.dbUtils.TorqueHelper;

public class NsfReport {

	private static final Logger log = Logger.getLogger(ChurnReport.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	
	private static boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
			if(!s.contains(".")){
				return false;
			}
		}catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	public static Vector<Vector<String>> getNsfReport(long [] merchants) throws ReportException{
		Vector<Vector<String>> vecNSF = new Vector<Vector<String>>();
		Vector<String> vecTemp = null;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null){
				log.error("Can't get database connection");
				throw new CustomerException();
			}
			String strSQL = "EXEC sp_getNSFSnapShot ?";//Merchant.sql_bundle.getString("getMerchantList");
			pstmt = dbConn.prepareStatement(strSQL);
			String merchs = "";
			for(int i=0; i< merchants.length; i++){
				if(merchs.length() > 0){
					merchs += ",";
				}
				merchs += merchants[i];
			}
			pstmt.setString(1, merchs);
			log.info("Getting infor for: " + merchs);
			rs = pstmt.executeQuery();
			//String []s = ;
			vecTemp = new Vector<String>(); //Arrays.asList(new String[]{"Merchant DBA","Merchant Id","Rep Name","Credit Limit","Credit Daily Limit","Running Liability","Average Daily Sales","Average Current Month Sales","Last Month Sales","AvgDaily Sales","Remaining Days Sales","NSF Total History","Sales SinceLast Adjustment","Days To Next ACH"}));
			
			
			int j = 0;
			String s = null;
			int rows =rs.getMetaData().getColumnCount();
			vecTemp.add("#");
			for(int i=1; i<= rows; i++){
				s =  rs.getMetaData().getColumnLabel(i);
				vecTemp.add(s.replace('_', ' '));
			}
			vecNSF.add(vecTemp);
			while (rs.next()){
				j++;
				vecTemp = new Vector<String>();
				vecTemp.add(("" + j));
				for(int i=1; i<= rows; i++){
					String val = "" + rs.getString(i);
					if(val.toLowerCase().equals("null")){
						val = "NA";
					}else{
						if(isDouble(val)){
							val = new DecimalFormat("#0.00").format(Double.valueOf(val));
							if(i == rows){
								if(Double.valueOf(val) == 0.0){
									val = "NA";
								}
							}
						}
					}
					vecTemp.add(val);
				}
				vecNSF.add(vecTemp);
			}
		}catch (Exception e){
			log.error("Error during getNsfReport", e);
			throw new ReportException();
		}finally{
			try{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}catch (Exception e){
				log.error("Error during closeConnection in getNsfReport", e);
			}
		}
		return vecNSF;
	}

	public static Vector<Vector<String>> getReturnsByMerchant(long merchant_id) throws ReportException{
		Vector<Vector<String>> vecreturns = new Vector<Vector<String>>();
		Vector<String> vecTemp = null;
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null){
				log.error("Can't get database connection");
				throw new CustomerException();
			}
			String strSQL = "SELECT DISTINCT b.entityid AS Merchant_ID,  CONVERT(nvarchar(10), r.return_date, 101) AS Return_Date, r.return_code AS Return_Code, ac.reason AS Reason, r.company_name AS Company_Name, r.amount AS Amount, pbe.description AS Description, r.ach_return_id " +
					"FROM ACHReturns AS r WITH (NOLOCK) INNER JOIN billing AS b WITH (NOLOCK) ON r.rec_id = b.achfiletracenumber LEFT JOIN ACHReturnCodes AS ac WITH (NOLOCK) ON (ac.code = r.return_code) " +
					"inner join paymentBatchEntity pbe WITH (NOLOCK) on b.paymentBatchEntityId = pbe.paymentBatchEntityId " +
					"WHERE b.entityTypeId=8 AND b.entityid = ?";
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setLong(1, merchant_id);
			log.info("Getting returns for: " + merchant_id);
			rs = pstmt.executeQuery();
			vecTemp = new Vector<String>(); 
			int j = 0;
			String s = null;
			int rows =rs.getMetaData().getColumnCount();
			vecTemp.add("#");
			for(int i=1; i<= rows; i++){
				s =  rs.getMetaData().getColumnLabel(i);
				vecTemp.add(s.replace('_', ' '));
			}
			vecreturns.add(vecTemp);
			while (rs.next()){
				j++;
				vecTemp = new Vector<String>();
				vecTemp.add(("" + j));
				for(int i=1; i<= rows; i++){
					String val = "" + rs.getString(i);
					if(val.equals("null")){
						val = "";
					}else{
						if(isDouble(val)){
							val = new DecimalFormat("#0.00").format(Double.valueOf(val));
						}
					}
					vecTemp.add(val);
				}
				vecreturns.add(vecTemp);
			}
		}catch (Exception e){
			log.error("Error during getReturnsByMerchant", e);
			throw new ReportException();
		}finally{
			try{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}catch (Exception e){
				log.error("Error during closeConnection in getReturnsByMerchant", e);
			}
		}
		return vecreturns;
	}
	
	public String downloadNFSReportCVS(ServletContext context, long [] merchants) throws ReportException{
		// TODO: check permissions
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		try{
			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
			try{
				strFileName = generateFileName(context);
				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
				log.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
				outputFile.flush();
				// TODO: internationalize
				/*outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle1")+"\n");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.churn.csvtitle2a")+": " + sessionData.getProperty("company_name") + " (" + sessionData.getProperty("ref_id") +")\"\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle2b")+": " + sessionData.getProperty("username") + "\n");
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z");
				String d = sdf.format(new Date());
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle3")+": " + d + "\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle4")+":\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csv"+(reportType==1?"weekly":"monthly"))+"\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.threshold2")+": " + (threshold*100) + "\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.note") + ": " + Languages.getString("jsp.admin.reports.churn.note4") + "\n\n");*/
				buildReportToDownload(outputFile, getNsfReport(merchants));
				outputFile.flush();
				outputFile.close();
				endQuery = System.currentTimeMillis();
				log.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				log.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				log.debug("Download URL: " + downloadUrl);
				try{
					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
					int len;
					while ((len = in.read(buffer)) > 0){
						out.write(buffer, 0, len);
					}
					out.closeEntry();
					in.close();
					out.close();
				}catch (IllegalArgumentException iae){
					iae.printStackTrace();
				}catch (FileNotFoundException fnfe){
					fnfe.printStackTrace();
				}catch (IOException ioe){
					ioe.printStackTrace();
				}
				sourceFile.delete();
				endQuery = System.currentTimeMillis();
				log.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
			}catch (IOException ioe){
				try{
					if (outputFile != null){
						outputFile.close();
					}
				}catch (IOException ioe2){
				}
			}
		}catch (Exception e){
			log.error("Error during downloadChurnReportCVS", e);
			throw new ReportException();
		}
		long end = System.currentTimeMillis();
		log.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
		return downloadUrl;
	}

	/**
	 * @param outputFile
	 * @throws IOException
	 */
	private static void buildReportToDownload(BufferedWriter outputFile, Vector<Vector<String>> vData) throws IOException  {
		for (int i = 0; i < vData.size(); i++){
			int items = vData.get(i).size();
			for (int k = 1; k < items; k++){
				if (vData.get(i).get(k)!=null && vData.get(i).get(k)!= "null" ){
					String val = vData.get(i).get(k).trim();
					if(val.matches("^[0-9]+$")){
						val = "\"'" + val + "\"";
					}else{
						if (!val.startsWith("\"") && !val.endsWith("\"") && val.contains(",")){
							val = "\"" + val + "\"";
						}
					}
					outputFile.write( val + ",");
				}else{
					outputFile.write("\"N/A\",");
				}
			}
			outputFile.write("\n");
		}
	}
	
	public String generateFileName(ServletContext context){
		boolean isUnique = false;
		String strFileName = "";
		while (!isUnique){
			strFileName = generateRandomNumber();
			isUnique = checkFileName(strFileName, context);
		}
		return strFileName;
	}
	
	private String generateRandomNumber(){
		StringBuffer s = new StringBuffer();
		// number between 1-9 because first digit must not be 0
		int nextInt = (int) ((Math.random() * 9) + 1);
		s = s.append(nextInt);
		for (int i = 0; i <= 10; i++) {
			// number between 0-9
			nextInt = (int) (Math.random() * 10);
			s = s.append(nextInt);
		}

		return s.toString();
	}

	private boolean checkFileName(String strFileName, ServletContext context){
		boolean isUnique = true;
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);

		File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
		if (f.exists()){
			// duplicate file found
			log.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
			return false;
		}

		f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
		if (f.exists()){
			// duplicate file found
			log.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
			return false;
		}
		return isUnique;
	}
}
