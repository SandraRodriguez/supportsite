/**
 *
 */
package com.debisys.reports.schedule;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.HTMLEncoder;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.TimeZone;

/**
 * @author fernandob
 *
 */
public class MerchantCreditReport extends ReportDownloader implements ISupportSiteReport {

    // attributes
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
    private boolean scheduling;
    private String startDate;
    private String formattedStartDate;
    private String endDate;
    private String formattedEndDate;
    private String merchantId;
    private String merchantDba;
    private String reportTitle;
    private String strCurrentDate;
    private boolean UsingDateLimitInterval;
    private int limitDays;
    protected double totalSales;
    protected double totalPayments;

    @Override
    protected boolean writeDownloadFileHeader() throws IOException {
        boolean retValue = false;
        try {
            this.writetofile(this.fc, "\"" + this.reportTitle + "\"\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
                    + this.sessionData.getUser().getUsername() + "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
                    + this.getStrCurrentDate() + "\n");
            this.writetofile(fc, "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\"\n");

            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + "\"," + this.startDate + "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + "\"," + this.endDate + "\n");

            if ((this.merchantId != null)) {
                StringBuffer line = new StringBuffer();
                line.append("\"" + Languages.getString("jsp.admin.reports.merchants", sessionData.getLanguage()) + "\",");
                line.append(this.merchantId);
                line.append("\n");
                this.writetofile(fc, line.toString());
            }

            retValue = true;
        } catch (Exception localException) {
            cat.error("MerchantCreditReport.writeDownloadFileHeader Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecordsHeaders() throws IOException {
        boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = this.getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            this.writetofile(fc, "\"#\",");
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecords() throws IOException {
        boolean retValue = false;
        Vector<Vector<Object>> reportResults = this.getResults();

        try {
            if ((reportResults != null) && (reportResults.size() > 0)) {
                this.totalPayments = 0.0;
                this.totalSales = 0.0;
                for (int i = 0; i < reportResults.size(); i++) {
                    Vector<Object> currentRecord = reportResults.get(i);
                    // Write the line number
                    this.writetofile(fc, String.valueOf(i + 1) + ",");

                    SimpleDateFormat fDate = new SimpleDateFormat("dd MMMMMM yyyy");
                    String strDate = fDate.format((Date) currentRecord.get(0));
                    double openningCredit = (Double) currentRecord.get(1);
                    double sales = (Double) currentRecord.get(2);
                    double payments = (Double) currentRecord.get(3);
                    double closingCredit = (Double) currentRecord.get(4);

                    this.writetofile(fc, "" + strDate + ", ");
                    this.writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(openningCredit)) + ", ");
                    this.writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(sales)) + ", ");
                    this.writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(payments)) + ", ");
                    this.writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(closingCredit)) + "\n");
                    this.totalPayments += payments;
                    this.totalSales += sales;
                }
                retValue = true;
            }
        } catch (NumberFormatException localNumberException) {
            cat.error("MerchantCreditReport.writeDownloadRecords Exception : " + localNumberException.toString());
        } catch (Exception localException) {
            cat.error("MerchantCreditReport.writeDownloadRecords Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadFileFooter() throws IOException {
        boolean retValue = false;
        try {
            this.writetofile(fc, Languages.getString("jsp.admin.reports.totals", this.sessionData.getLanguage()));
            this.writetofile(fc, ", ");
            this.writetofile(fc, ", ");
            this.writetofile(fc, ", ");
            this.writetofile(fc, NumberUtil.formatAmount(Double.toString(this.totalSales)));
            this.writetofile(fc, ", ");
            this.writetofile(fc, NumberUtil.formatAmount(Double.toString(this.totalPayments)));
            this.writetofile(fc, ", ");
            retValue = true;
        } catch (Exception localException) {
            cat.error("writeDownloadFileFooter Exception : " + localException.toString());
        }
        return retValue;
    }


    @Override
    public String generateSqlString(QUERY_TYPE qType) {
        StringBuilder strSql = new StringBuilder();
        String strAccessLevel = sessionData.getProperty("access_level");

        try {
            Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.startDate,
                    this.endDate + " 23:59:59.997", false);
            String tzStartDate = vTimeZoneFilterDates.get(0);
            String tzEndDate = vTimeZoneFilterDates.get(1);

            String queryStartDate = String.format("'%s'", tzStartDate);
            String queryEndDate = String.format("'%s'", tzEndDate);

            if (((this.startDate != null) && (this.startDate.length() > 0)) && ((this.endDate != null) && (this.endDate.length() > 0))) {
                if (this.isScheduling()) {
                    if (qType == QUERY_TYPE.RELATIVE) {
                        queryStartDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE;
                        queryEndDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE;
                    } else if (qType == QUERY_TYPE.FIXED) {
                        queryStartDate = String.format("'%s'", tzStartDate);
                        queryEndDate = String.format("'%s'", tzEndDate);
                    }
                } else {
                    queryStartDate = String.format("'%s'", tzStartDate);
                    queryEndDate = String.format("'%s'", tzEndDate);
                }
            } else {
                queryStartDate = String.format("'%s'", tzStartDate);
                queryEndDate = String.format("'%s'", tzEndDate);
            }

            strSql.append("CREATE TABLE #reportResult(");
            strSql.append(" date VARCHAR(50) NOT NULL DEFAULT(''),");
            strSql.append(" openingCredit MONEY NOT NULL DEFAULT(0),");
            strSql.append(" sales  MONEY NOT NULL DEFAULT(0),");
            strSql.append(" payments  MONEY NOT NULL DEFAULT(0),");
            strSql.append(" closingCredit  MONEY NOT NULL DEFAULT(0) ) ");

            strSql.append(" BEGIN TRY ");

            strSql.append(" DECLARE @lastTrxDate DATETIME, @trxBalance MONEY,");
            strSql.append("         @lastPaymentDate DATETIME, @paymentBalance MONEY,");
            strSql.append("         @reportStartDate DATETIME, @reportEndDate DATETIME");

            strSql.append(String.format(" SELECT @reportStartDate = %s ", queryStartDate));
            strSql.append(String.format(" SELECT @reportEndDate = %s ", queryEndDate));

            // Balance from web_transactions query
            strSql.append(" SELECT @lastTrxDate = WT.[datetime], @trxBalance = WT.[liabilitylimit] - WT.[runningliability] - WT.[amount]");
            strSql.append(" FROM   dbo.Web_Transactions WT with(nolock) ");
            strSql.append("        INNER JOIN ( ");
            strSql.append("               SELECT MAX([datetime]) [datetime], [merchant_id] ");
            strSql.append("               FROM   dbo.Web_Transactions WITH(NOLOCK) ");
            strSql.append(String.format(" WHERE  [merchant_id] = %s ", this.merchantId));
            strSql.append("                      AND [datetime] BETWEEN @reportStartDate AND @reportEndDate ");
            strSql.append("               GROUP BY [merchant_id] ");
            strSql.append("         ) LastTrx ");
            strSql.append("        ON WT.[datetime] = LastTrx.[datetime] ");
            strSql.append("          AND  WT.[merchant_id] = LastTrx.[merchant_id] ");
            strSql.append(String.format(" WHERE	WT.[merchant_id] = %s ", this.merchantId));
            strSql.append(" AND WT.[datetime] BETWEEN @reportStartDate AND @reportEndDate ");

            strSql.append(" IF ((@lastTrxDate IS NULL) AND (@trxBalance IS NULL)) ");
            strSql.append("  BEGIN ");
            strSql.append(" 	SELECT @lastTrxDate = WT.[datetime],  ");
            strSql.append(" 	       @trxBalance = WT.[liabilitylimit] - WT.[runningliability] - WT.[amount] ");
            strSql.append(" 	FROM   dbo.web_transactions WT WITH(nolock)  ");
            strSql.append(" 	       INNER JOIN (SELECT Max([datetime]) [datetime], [merchant_id] ");
            strSql.append(" 			   FROM   dbo.web_transactions WITH(nolock)  ");
            strSql.append(String.format(" 		  WHERE  [merchant_id] = %s ", this.merchantId));
            strSql.append("                                      AND [datetime] < @reportStartDate  ");
            strSql.append(" 			   GROUP  BY [merchant_id]) LastTrx  ");
            strSql.append(" 	       ON WT.[datetime] = LastTrx.[datetime]  ");
            strSql.append(" 		  AND WT.[merchant_id] = LastTrx.[merchant_id]  ");
            strSql.append(String.format(" 	WHERE  WT.[merchant_id] = %s  ", this.merchantId));
            strSql.append(" 	                       AND WT.[datetime] < @reportStartDate ");
            strSql.append("  END ");

            strSql.append(" IF (@lastTrxDate IS NULL) ");
            strSql.append("  BEGIN ");
            strSql.append("    SET @lastTrxDate = 0 ");
            strSql.append("  END ");
            strSql.append(" IF	(@trxBalance IS NULL) ");
            strSql.append("  BEGIN ");
            strSql.append("    SET @trxBalance = 0 ");
            strSql.append("  END ");
            // Balance from merchant_credits query
            strSql.append(" SELECT @lastPaymentDate = MC.[log_datetime], @paymentBalance = MC.[balance] ");
            strSql.append(" FROM   [dbo].[Merchant_credits] MC WITH(NOLOCK) ");
            strSql.append("        INNER JOIN( ");
            strSql.append("               SELECT  MAX([log_datetime]) log_datetime, [merchant_id] ");
            strSql.append("               FROM    [dbo].[Merchant_credits] WITH(NOLOCK) ");
            strSql.append(String.format(" WHERE   [merchant_id] = %s", this.merchantId));
            strSql.append("                       AND [log_datetime] BETWEEN @reportStartDate AND @reportEndDate");
            strSql.append("          GROUP BY [merchant_id] ");
            strSql.append("        ) LastPayment ");
            strSql.append("        ON MC.[log_datetime] = LastPayment.[log_datetime] ");
            strSql.append("           AND  MC.[merchant_id] = LastPayment.[merchant_id] ");
            strSql.append(String.format(" WHERE  MC.[merchant_id] = %s", this.merchantId));
            strSql.append("                      AND MC.[log_datetime] BETWEEN @reportStartDate AND @reportEndDate ");

            strSql.append(" IF((@lastPaymentDate IS NULL) AND (@paymentBalance IS NULL)) ");
            strSql.append("  BEGIN ");
            strSql.append(" 	SELECT @lastPaymentDate = MC.[log_datetime],  ");
            strSql.append(" 		   @paymentBalance = MC.[balance]  ");
            strSql.append(" 	FROM   [dbo].[merchant_credits] MC WITH(nolock)  ");
            strSql.append(" 	       INNER JOIN(SELECT Max([log_datetime]) log_datetime,  [merchant_id]");
            strSql.append("                           FROM   [dbo].[merchant_credits] WITH(nolock)  ");
            strSql.append(String.format("	      WHERE  [merchant_id] = %s ", this.merchantId));
            strSql.append(" 		                     AND [log_datetime] < @reportStartDate ");
            strSql.append(" 			      GROUP  BY [merchant_id]) LastPayment  ");
            strSql.append(" 	       ON MC.[log_datetime] = LastPayment.[log_datetime]  ");
            strSql.append(" 		  AND MC.[merchant_id] = LastPayment.[merchant_id]  ");
            strSql.append(String.format(" WHERE  MC.[merchant_id] = %s  ", this.merchantId));
            strSql.append(" 		         AND MC.[log_datetime] < @reportStartDate ");
            strSql.append("  END ");

            strSql.append(" IF (@lastPaymentDate IS NULL) ");
            strSql.append("  BEGIN ");
            strSql.append("    SET @lastPaymentDate = 0 ");
            strSql.append("  END ");
            strSql.append(" IF	(@paymentBalance IS NULL) ");
            strSql.append("  BEGIN ");
            strSql.append("    SET @paymentBalance = 0 ");
            strSql.append("  END ");
            //Get the most recent balance between transactions and payments
            strSql.append(" DECLARE @pivotDate DATETIME, @lastReportBalance MONEY ");
            strSql.append(" IF (@lastTrxDate >= @lastPaymentDate) ");
            strSql.append("  BEGIN ");
            strSql.append("    SET @pivotDate = ISNULL(@lastTrxDate, GETDATE()) ");
            strSql.append("    SET @lastReportBalance = ISNULL(@trxBalance, 0) ");
            strSql.append("  END ");
            strSql.append(" ELSE ");
            strSql.append("  BEGIN ");
            strSql.append("    SET @pivotDate = ISNULL(@lastPaymentDate, GETDATE()) ");
            strSql.append("    SET @lastReportBalance = ISNULL(@paymentBalance, 0) ");
            strSql.append("  END ");
            // Calculate records from most recent to oldest
            strSql.append(" DECLARE @iteratorStartDate DATETIME, @iteratorEndDate DATETIME, ");
            strSql.append("         @openingCredit MONEY, @closingCredit MONEY, @sales MONEY, @payments MONEY ");
            strSql.append(" SET @iteratorStartDate = Dateadd(dd, 0, Datediff(dd, 0, @reportEndDate)) ");
            strSql.append(" SET @closingCredit = @lastReportBalance ");
            strSql.append(" WHILE( @iteratorStartDate >= @reportStartDate ) ");
            strSql.append("  BEGIN ");
            strSql.append("   SET @iteratorEndDate =  DATEADD(ms,-2,DATEADD(day,1,DATEADD(dd, DATEDIFF(dd,0,@iteratorStartDate), 0))) ");
            strSql.append("   IF (@iteratorEndDate > @reportEndDate) ");
            strSql.append("    BEGIN ");
            strSql.append("      SET @iteratorEndDate = @reportEndDate");
            strSql.append("    END");
            // Calculate sales for the day
            strSql.append("   SET @sales = 0 ");
            strSql.append("   SELECT @sales = SUM(trueamount) ");
            strSql.append("   FROM   [dbo].[Web_Transactions] WITH(NOLOCK) ");
            strSql.append("   WHERE  [merchant_id] = ");
            strSql.append(this.merchantId);
            strSql.append("          AND [datetime] BETWEEN @iteratorStartDate AND @iteratorEndDate ");
            strSql.append("   GROUP BY [merchant_id] ");
            strSql.append("   SET @sales = ISNULL(@sales, 0) ");
            // Calculate payments for the day
            strSql.append("   SET @payments = 0");
            strSql.append("   SELECT @payments = SUM(payment)");
            strSql.append("   FROM   [dbo].[Merchant_credits] WITH(NOLOCK)");
            strSql.append("   WHERE  [merchant_id] = ");
            strSql.append(this.merchantId);
            strSql.append("   AND [log_datetime] BETWEEN @iteratorStartDate AND @iteratorEndDate ");
            strSql.append("   SET @payments = ISNULL(@payments, 0) ");
            strSql.append("   SET @openingCredit = @closingCredit - @payments + @sales ");
            strSql.append("   INSERT INTO #reportResult(date, openingCredit, sales, payments, closingCredit) ");
            strSql.append("   VALUES(@iteratorStartDate, @openingCredit, @sales, @payments, @closingCredit) ");
            strSql.append("   SET @closingCredit = @openingCredit ");
            strSql.append("   SET @iteratorStartDate = DATEADD(day, -1, @iteratorStartDate) ");
            strSql.append("  END ");
            strSql.append("  SELECT CONVERT(datetime, date, 121) date, openingcredit, sales, payments, closingcredit  ");

            if (this.isScheduling()) {
                if (qType == QUERY_TYPE.FIXED) {
                    strSql.append(", ");
                    strSql.append("        CONVERT(VARCHAR(20), @reportStartDate, 121)  AS startDate, ");
                    strSql.append("        CONVERT(VARCHAR(20), @reportEndDate, 121)  AS endDate ");
                } else {
                    strSql.append(" ");
                    strSql.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS);
                    strSql.append(" ");
                }
            }
            strSql.append("  FROM #reportResult ORDER BY date ASC ");
            strSql.append(" END TRY ");
            strSql.append(" BEGIN CATCH ");
            strSql.append("   PRINT 'ERROR NUMBER : ' ");
            strSql.append("   PRINT ERROR_NUMBER() ");
            strSql.append("   PRINT 'ERROR SEVERITY : ' ");
            strSql.append("   PRINT ERROR_SEVERITY() ");
            strSql.append("   PRINT 'ERROR STATE : '");
            strSql.append("   PRINT ERROR_STATE() ");
            strSql.append("   PRINT 'ERROR PROCEDURE : '");
            strSql.append("   PRINT ERROR_PROCEDURE() ");
            strSql.append("   PRINT 'ERROR LINE : '");
            strSql.append("   PRINT ERROR_LINE()");
            strSql.append("   PRINT 'ERROR MESSAGE : '");
            strSql.append("   PRINT ERROR_MESSAGE()");
            strSql.append(" END CATCH ");
            strSql.append(" truncate table #reportResult ");
            strSql.append(" drop table #reportResult ");

        } catch (ReportException localReportException) {
            cat.error("MerchantCreditReport.getSqlString. Exception while generationg sql string : " + localReportException.toString());
        } catch (Exception localException) {
            cat.error("MerchantCreditReport.getSqlString. Exception while generationg sql string : " + localException.toString());
        }
        return strSql.toString();
    }

    @Override
    public boolean scheduleReport() {
        boolean retValue = false;

        try {
            this.setScheduling(true);
            String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
            String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

            ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_MERCHANT_CREDIT_REPORT, 1);
            scheduleReport.setNameDateTimeColumn("PR.RequestDate");
            scheduleReport.setRelativeQuery(relativeQuery);
            scheduleReport.setFixedQuery(fixedQuery);
            TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
            this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
            retValue = true;
        } catch (Exception localException) {
            cat.error("MerchantCreditReport.scheduleReport Exception : " + localException.toString());
        }

        return retValue;
    }

    @Override
    public String downloadReport() {
        return this.exportToFile();
    }

    @Override
    public Vector<Vector<Object>> getResults() {
        Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            this.setScheduling(false); // If returning results, then scheduling
            // does not have sense
            String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
            cat.debug("Merchant Credit query : " + strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector<Object> vecTemp = new Vector<Object>();
                SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                vecTemp.add(dFormat.parse(rs.getString("date")));
                vecTemp.add(rs.getDouble("openingCredit"));
                vecTemp.add(rs.getDouble("sales"));
                vecTemp.add(rs.getDouble("payments"));
                vecTemp.add(rs.getDouble("closingCredit"));
                retValue.add(vecTemp);
            }
            if (retValue.size() == 0) {
                retValue = null;
            }
        } catch (TorqueException localTorqueException) {
            cat.error("MerchantCreditReport.getResults Exception : " + localTorqueException.toString());
        } catch (ReportException localReportException) {
            cat.error("MerchantCreditReport.getResults Exception : " + localReportException.toString());
        } catch (SQLException localSqlException) {
            cat.error("MerchantCreditReport.getResults Exception : " + localSqlException.toString());
        } catch (Exception localException) {
            cat.error("MerchantCreditReport.getResults Exception : " + localException.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                    dbConn = null;
                }
            } catch (SQLException cleaningSqlException) {
                cat.error("MerchantCreditReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
            }
        }

        return retValue;
    }

    @Override
    public ArrayList<ColumnReport> getReportRecordHeaders() {
        ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
        try {
            retValue.add(new ColumnReport("date", Languages.getString("jsp.admin.reports.date", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("openingCredit", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.opening_credit",
                    sessionData.getLanguage()).toUpperCase(), Double.class, false));
            retValue.add(new ColumnReport("sales", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.sales",
                    sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("payments", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.payments",
                    sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("closingCredit", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.closing_credit",
                    sessionData.getLanguage()).toUpperCase(), Double.class, false));
        } catch (Exception localException) {
            cat.error("MerchantCreditReport.getReportRecordHeaders Exception : " + localException);
        }
        return retValue;
    }

    @Override
    public ArrayList<String> getTitles() {
        ArrayList<String> retValue = new ArrayList<String>();

        try {
            retValue.add(this.merchantDba);

            retValue.add(this.reportTitle + " "
                    + Languages.getString("jsp.admin.from", this.sessionData.getLanguage()) + " "
                    + HTMLEncoder.encode(this.formattedStartDate) + " "
                    + Languages.getString("jsp.admin.to", this.sessionData.getLanguage()) + " "
                    + HTMLEncoder.encode(this.formattedEndDate));
            Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
            String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
                    + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
            retValue.add(noteTimeZone);
            retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
            retValue.add(Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.limited_to_90_days", this.sessionData.getLanguage()));
        } catch (NumberFormatException localNumberFormatException) {
            cat.error("MerchantCreditReport.getTitles Exception : " + localNumberFormatException.toString());
        } catch (Exception localException) {
            cat.error("MerchantCreditReport.getTitles Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    public boolean isUsingDateLimitInterval() {
        return this.UsingDateLimitInterval;
    }

    @Override
    public int getLimitDays() {
        return this.limitDays;
    }

    public static ResourceBundle getSql_bundle() {
        return sql_bundle;
    }

    public static void setSql_bundle(ResourceBundle sql_bundle) {
        MerchantCreditReport.sql_bundle = sql_bundle;
    }

    public boolean isScheduling() {
        return scheduling;
    }

    public void setScheduling(boolean scheduling) {
        this.scheduling = scheduling;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFormattedStartDate() {
        return formattedStartDate;
    }

    public void setFormattedStartDate(String formattedStartDate) {
        this.formattedStartDate = formattedStartDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFormattedEndDate() {
        return formattedEndDate;
    }

    public void setFormattedEndDate(String formattedEndDate) {
        this.formattedEndDate = formattedEndDate;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantDba() {
        return merchantDba;
    }

    public void setMerchantDba(String merchantDba) {
        this.merchantDba = merchantDba;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getStrCurrentDate() {
        return strCurrentDate;
    }

    public void setStrCurrentDate(String strCurrentDate) {
        this.strCurrentDate = strCurrentDate;
    }

    public MerchantCreditReport() {
        super();
        this.sessionData = null;
        this.context = null;
        this.scheduling = false;
        this.startDate = "";
        this.formattedStartDate = "";
        this.endDate = "";
        this.formattedEndDate = "";
        this.merchantId = "";
        this.merchantDba = "";
        this.reportTitle = "";
        this.strCurrentDate = "";
        this.UsingDateLimitInterval = false;
        this.limitDays = 90;
        this.totalPayments = 0.0;
        this.totalSales = 0.0;
    }

    public MerchantCreditReport(SessionData sessionData, ServletContext pContext, boolean scheduling, String startDate, String formattedStartDate,
            String endDate, String formattedEndDate, String merchantId, String merchantDba, String reportTitle) {
        this();
        this.sessionData = sessionData;
        this.setContext(pContext);
        this.strRefId =  this.sessionData.getProperty("ref_id");
        this.scheduling = scheduling;
        this.startDate = startDate;
        this.formattedStartDate = formattedStartDate;
        this.endDate = endDate;
        this.formattedEndDate = formattedEndDate;
        this.merchantId = merchantId;
        this.merchantDba = merchantDba;
        this.reportTitle = reportTitle;
        this.strCurrentDate = DateUtil.formatDateNoTime(new Date());
        this.limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(DebisysConstants.SC_MERCHANT_CREDIT_REPORT, context);
        if (this.limitDays != Integer.MIN_VALUE) {
            this.UsingDateLimitInterval = true;
        }
    }

}
