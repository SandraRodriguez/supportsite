/**
 * 
 */
package com.debisys.reports.schedule;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.schedule.ISupportSiteReport.QUERY_TYPE;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

/**
 * @author fernandob
 * 
 */
public class ExternalCreditOrPaymentReport extends ReportDownloader implements ISupportSiteReport
{
	// attributes
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private boolean scheduling;
	private String start_date;
	private String end_date;
	private String strMerchantIds;
	private String strDeploymentType;
	private String strCurrentDate;
	private String sort = "";
	private String col = "";
	private boolean allMerchantsSelected;
	private boolean UsingDateLimitInterval;
	private int limitDays;
	
	// Report Summary data
	private long quantityCount;
	private double paymentSum;
	
	// properties
	
	/**
	 * @return the limitDays
	 */
	public int getLimitDays()
	{
		return limitDays;
	}

	/**
	 * @param limitDays the limitDays to set
	 */
	public void setLimitDays(int limitDays)
	{
		this.limitDays = limitDays;
	}
	/**
	 * @return the usingDateLimitInterval
	 */
	public boolean isUsingDateLimitInterval()
	{
		return UsingDateLimitInterval;
	}

	/**
	 * @param usingDateLimitInterval the usingDateLimitInterval to set
	 */
	public void setUsingDateLimitInterval(boolean usingDateLimitInterval)
	{
		UsingDateLimitInterval = usingDateLimitInterval;
	}

	/**
	 * @return the scheduling
	 */
	public boolean isScheduling()
	{
		return scheduling;
	}

	/**
	 * @param shceduling
	 *            the scheduling to set
	 */
	public void setScheduling(boolean shceduling)
	{
		this.scheduling = shceduling;
	}

	/**
	 * @return the start_date
	 */
	public String getStart_date()
	{
		return start_date;
	}

	/**
	 * @param startDate
	 *            the start_date to set
	 */
	public void setStart_date(String startDate)
	{
		start_date = startDate.trim();
	}

	/**
	 * @return the end_date
	 */
	public String getEnd_date()
	{
		return end_date;
	}

	/**
	 * @param endDate
	 *            the end_date to set
	 */
	public void setEnd_date(String endDate)
	{
		end_date = endDate.trim();
	}

	/**
	 * @return the strMerchantIds
	 */
	public String getStrMerchantIds()
	{
		return strMerchantIds;
	}

	/**
	 * @param strMerchantIds
	 *            the strMerchantIds to set
	 */
	public void setStrMerchantIds(String strMerchantIds)
	{
		this.strMerchantIds = strMerchantIds;
	}

	/**
	 * @return the strDeploymentType
	 */
	public String getStrDeploymentType()
	{
		return strDeploymentType;
	}

	/**
	 * @param strDeploymentType
	 *            the strDeploymentType to set
	 */
	public void setStrDeploymentType(String strDeploymentType)
	{
		this.strDeploymentType = strDeploymentType;
	}

	/**
	 * @return the strCurrentDate
	 */
	public String getStrCurrentDate()
	{
		return strCurrentDate;
	}

	/**
	 * @param strCurrentDate
	 *            the strCurrentDate to set
	 */
	public void setStrCurrentDate(String strCurrentDate)
	{
		this.strCurrentDate = strCurrentDate;
	}

	public String getCol()
	{
		return StringUtil.toString(this.col);
	}

	public void setCol(String strCol)
	{
		this.col = strCol;
	}

	public String getSort()
	{
		return StringUtil.toString(this.sort);
	}

	public void setSort(String strSort)
	{
		this.sort = strSort;
	}

	/**
	 * @return the allMerchantsSelected
	 */
	public boolean isAllMerchantsSelected()
	{
		return allMerchantsSelected;
	}

	/**
	 * @param allMerchantsSelected
	 *            the allMerchantsSelected to set
	 */
	public void setAllMerchantsSelected(boolean allMerchantsSelected)
	{
		this.allMerchantsSelected = allMerchantsSelected;
	}
	
	// methods
	public ExternalCreditOrPaymentReport()
	{
		super();
		this.scheduling = false;
		this.start_date = null;
		this.end_date = null;
		this.strMerchantIds = null;
		this.strDeploymentType = null;
		this.strCurrentDate = null;
		this.allMerchantsSelected = false;
		this.UsingDateLimitInterval = false;
		this.limitDays = 0;
	}

	/**
	 * @param scheduling
	 * @param startDate
	 * @param endDate
	 * @param strMerchantIds
	 * @param strDeploymentType
	 * @param strCurrentDate
	 */
	public ExternalCreditOrPaymentReport(SessionData sessionData, ServletContext pContext, boolean scheduling,
			String startDate, String endDate, String strMerchantIds, String strDeploymentType, boolean allMerchSel)
	{
		this();
		this.sessionData = sessionData;
		this.setContext(pContext);
		this.scheduling = scheduling;
		this.start_date = startDate;
		this.end_date = endDate;
		this.strMerchantIds = strMerchantIds;
		this.strDeploymentType = strDeploymentType;
		this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
		this.allMerchantsSelected = allMerchSel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileFooter()
	 */
	@Override
	protected boolean writeDownloadFileFooter() throws IOException
	{
		boolean retValue = false;

		try
		{
			this.writetofile(fc, ","); // Line number
			this.writetofile(fc, ","); // dba
			this.writetofile(fc, ","); // merchant id
			if (!this.strDeploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
				this.writetofile(fc, ","); // route id
			}
			this.writetofile(fc, "\"" + this.quantityCount + "\",");
			this.writetofile(fc, "\"" + this.paymentSum + "\"\n");
			retValue = true;
		}
		catch (NumberFormatException localNumberException)
		{
			cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadRecords Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileHeader()
	 */
	@Override
	protected boolean writeDownloadFileHeader() throws IOException
	{
		boolean retValue = false;
		try
		{
			this.writetofile(this.fc, "\""
					+ Languages.getString("jsp.admin.reports.title41", sessionData.getLanguage()).toUpperCase()
					+ "\"\n");
			this.writetofile(fc, "\""
					+ Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
					+ sessionData.getUser().getUsername() + "\n");
			this.writetofile(fc, "\""
					+ Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
					+ this.getStrCurrentDate() + "\n");
			this.writetofile(fc, "\n");
			this.writetofile(fc, "\""
					+ Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\"\n");
			if (sessionData.getProperty("repName") != null)
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
						+ "\"," + sessionData.getProperty("repName") + "\n");
			}
			else
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
						+ "\"," + sessionData.getUser().getCompanyName() + "\n");
			}

			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + "\","
					+ this.start_date + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + "\","
					+ this.end_date + "\n");

			
			if ((this.strMerchantIds != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.merchants", sessionData.getLanguage()) + "\",");
				if (this.allMerchantsSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.strMerchantIds);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadFileHeader Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecords()
	 */
	@Override
	protected boolean writeDownloadRecords() throws IOException
	{
		boolean retValue = false;
		Vector<Vector<Object>> reportResults = this.getResults();

		try
		{
			if ((reportResults != null) && (reportResults.size() > 0))
			{
				this.quantityCount = 0;
				this.paymentSum = 0.0;
				for (int i = 0; i < reportResults.size(); i++)
				{
					Vector<Object> currentRecord = reportResults.get(i);
					// Write the line number
					this.writetofile(fc, "\"" + String.valueOf(i + 1) + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(0).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(1).toString().trim() + "\",");
					if (!this.strDeploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
						this.writetofile(fc, "\"" + currentRecord.get(2).toString().trim() + "\",");
					}
					this.quantityCount += Long.valueOf(currentRecord.get(3).toString().trim());
					this.writetofile(fc, "\"" + currentRecord.get(3).toString().trim() + "\",");
					this.paymentSum += Double.valueOf(currentRecord.get(4).toString().trim());
					this.writetofile(fc, "\"" + currentRecord.get(4).toString().trim() + "\"\n");
				}
				retValue = true;
			}
		}
		catch (NumberFormatException localNumberException)
		{
			cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadRecords Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecordsHeaders()
	 */
	@Override
	protected boolean writeDownloadRecordsHeaders() throws IOException
	{
		boolean retValue = false;

		ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

		if ((columnHeaders != null) && (columnHeaders.size() > 0))
		{
			this.writetofile(fc, "\"#\",");
			for (int i = 0; i < columnHeaders.size(); i++)
			{
				this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
				if (i < (columnHeaders.size() - 1))
				{
					this.writetofile(fc, ",");
				}
				else
				{
					this.writetofile(fc, "\n");
				}
			}
			retValue = true;
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#downloadReport()
	 */
	@Override
	public String downloadReport()
	{
		return this.exportToFile();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#generateSqlString(com.debisys.
	 * reports.ISupportSiteReport.QUERY_TYPE)
	 */
	@Override
	public String generateSqlString(QUERY_TYPE qType)
	{
		String retValue = null;

		StringBuffer strSQL = new StringBuffer();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		//String reportId = sessionData.getProperty("reportId");

		try
		{
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, this.end_date + " 23:59:59.999", false);
			strSQL
					.append("SELECT m.dba, m.merchant_id, rt.RouteName, count(mc.id) as qty, sum(payment) as total_payments ");
			if (this.isScheduling())
			{
				if (qType == QUERY_TYPE.FIXED)
				{
					strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
					strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
				}
				else
				{
					strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
				}
			}
			strSQL.append(" FROM merchants AS m WITH (NOLOCK) ");
			strSQL.append("		INNER JOIN merchant_credits AS mc WITH (NOLOCK) ON mc.merchant_id = m.merchant_id ");
			strSQL.append("		LEFT JOIN dbo.[SpecialUsers] SU WITH(NOLOCK) ON mc.[logon_id] = SU.[UserCode] ");
			strSQL.append("		LEFT JOIN Routes AS rt WITH (NOLOCK) ON m.RouteID = rt.RouteID ");
			strSQL.append(" WHERE SU.[UserCode] is null ");

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL
							.append(" AND m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id = "
									+ sessionData.getProperty("ref_id")
									+ ")) AND (mc.log_datetime >= '"
									+ this.start_date
									+ "' and  mc.log_datetime <= '"
									+ this.end_date
									+ " 23:59:59.999"
									+ "')");
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL
							.append(" AND m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in ");
					strSQL.append(" (select rep_id from reps WITH (NOLOCK) where type= "
							+ DebisysConstants.REP_TYPE_REP + " and iso_id in ");
					strSQL.append(" (select rep_id from reps WITH (NOLOCK) where type= "
							+ DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in ");
					strSQL.append(" (select rep_id from reps WITH (NOLOCK) where type= "
							+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId + "))))");
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL
						.append(" AND m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in ");
				strSQL.append("   (select rep_id from reps WITH (NOLOCK) where type = " + DebisysConstants.REP_TYPE_REP
						+ " and iso_id in ");
				strSQL.append("      (select rep_id from reps WITH (NOLOCK) where type = "
						+ DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = " + strRefId + ")))");
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				strSQL
						.append(" AND m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in ");
				strSQL.append("   (select rep_id from reps WITH (NOLOCK) where type = " + DebisysConstants.REP_TYPE_REP
						+ " and iso_id = " + strRefId + "))");
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSQL.append(" AND m.rep_id = " + strRefId + " ");
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				strSQL.append(" AND mc.merchant_id = " + strRefId + " ");
			}

			if (this.allMerchantsSelected)
			{
                            User currentUser = this.sessionData.getUser();
                            strSQL.append(ReportsUtil.getChainToBuildWhereSQL(currentUser.getAccessLevel(), currentUser.getDistChainType(), currentUser.getRefId(),
								"m.rep_id"));
			}
			else
			{
				if ((this.strMerchantIds != null) && (!this.strMerchantIds.equals("")))
				{
					if (this.strMerchantIds.indexOf(",") != -1)
					{
						strSQL.append(" AND m.merchant_id IN (" + this.strMerchantIds + ")");
					}
					else
					{
						strSQL.append(" AND m.merchant_id = " + this.strMerchantIds);
					}
				}
			}

			strSQL.append(" AND mc.ExternalDBA IS NOT NULL ");

			if (DateUtil.isValid(this.start_date) && DateUtil.isValid(this.end_date))
			{
				if (this.isScheduling())
				{
					if (qType == QUERY_TYPE.RELATIVE)
					{
						strSQL.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
					}
					else if (qType == QUERY_TYPE.FIXED)
					{
						strSQL.append("         AND (mc.log_datetime  >= '" + vTimeZoneFilterDates.get(0) + "'");
						strSQL.append("         AND mc.log_datetime  < '" + vTimeZoneFilterDates.get(1) + "')");
					}
				}
				else
				{
					// If not scheduling, the wild card is removed, and the
					// current report dates are used
					int i;
					while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
					{
						strSQL.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
					}
					strSQL.append("         AND (mc.log_datetime  >= '" + vTimeZoneFilterDates.get(0) + "'");
					strSQL.append("         AND mc.log_datetime  < '" + vTimeZoneFilterDates.get(1) + "')");
				}
				strSQL.append(" group by m.merchant_id, m.dba, rt.RouteName ");
				
				int intCol = 0;
				int intSort = 0;
				String strCol = "";
				String strSort = "";

				try
				{
					if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
					{
						intCol = Integer.parseInt(this.col);
						intSort = Integer.parseInt(this.sort);
					}
				}
				catch (NumberFormatException nfe)
				{
					intCol = 0;
					intSort = 0;
				}

				if (intSort == 2)
				{
					strSort = "DESC";
				}
				else
				{
					strSort = "ASC";
				}

				switch (intCol)
				{
				case 1:
					strCol = "m.dba";
					break;
				case 2:
					strCol = "m.merchant_id";
					break;
				case 3:
					strCol = "rt.RouteName";
					break;
				case 4:
					strCol = "qty";
					break;
				case 5:
					strCol = "total_payments";
					break;
				default:
					strCol = "m.dba";
					break;
				}
				strSQL.append(" ORDER BY " + strCol + " " + strSort);

				retValue = strSQL.toString();
			}
			else
			{
				retValue = null;
			}
		}
		catch (ReportException localReportException)
		{
			cat.error("ExternalCreditOrPaymentReport.getSqlString. Exception while generationg sql string : "
					+ localReportException.toString());
		}
		catch (Exception localException)
		{
			cat.error("ExternalCreditOrPaymentReport.getSqlString. Exception while generationg sql string : "
					+ localException.toString());
		}
		cat.debug(retValue);
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getReportRecordHeaders()
	 */
	@Override
	public ArrayList<ColumnReport> getReportRecordHeaders()
	{
		ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();

		try
		{
			retValue.add(new ColumnReport("dba", Languages
					.getString("jsp.admin.reports.dba", sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.id",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			if (!this.strDeploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
				retValue.add(new ColumnReport("RouteName", Languages.getString(
						"jsp.admin.customers.merchants_edit.route", sessionData.getLanguage()).toUpperCase(),
						String.class, false));
			}
			retValue.add(new ColumnReport("qty", Languages
					.getString("jsp.admin.reports.qty", sessionData.getLanguage()).toUpperCase(), Double.class, true));
			retValue.add(new ColumnReport("total_payments", Languages.getString("jsp.admin.reports.total",
					sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}
		catch (Exception localException)
		{
			cat.error("ExternalCreditOrPaymentReport.getReportRecordHeaders Exception : " + localException);
		}

		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getResults()
	 */
	@Override
	public Vector<Vector<Object>> getResults()
	{
		Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			this.setScheduling(false); // If returning results, then scheduling
			// does not have sense
			String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector<Object> vecTemp = new Vector<Object>();
				vecTemp.add(StringUtil.toString(rs.getString("dba")));
				vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
				vecTemp.add(StringUtil.toString(rs.getString("RouteName")));
				vecTemp.add(rs.getLong("qty"));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("total_payments")));
				retValue.add(vecTemp);
			}
			if (retValue.size() == 0)
			{
				retValue = null;
			}
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("ExternalCreditOrPaymentReport.getResults Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("ExternalCreditOrPaymentReport.getResults Exception : " + localReportException.toString());
		}
		catch (SQLException localSqlException)
		{
			cat.error("ExternalCreditOrPaymentReport.getResults Exception : " + localSqlException.toString());
		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
					rs = null;
				}
				if (pstmt != null)
				{
					pstmt.close();
					pstmt = null;
				}
				if (dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (SQLException cleaningSqlException)
			{
				cat.error("ExternalCreditOrPaymentReport.getResults Exception while cleaning DB objects: "
						+ cleaningSqlException.toString());
			}
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getTitles()
	 */
	@Override
	public ArrayList<String> getTitles()
	{
		ArrayList<String> retValue = new ArrayList<String>();

		try
		{
			retValue.add(Languages.getString("jsp.admin.reports.extern.title1", this.sessionData.getLanguage()));
			Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData
					.getProperty("ref_id")));
			String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage())
					+ ":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
			retValue.add(noteTimeZone);
			retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
		}
		catch (NumberFormatException localNumberFormatException)
		{
			cat.error("getTitles Exception : " + localNumberFormatException.toString());
		}
		catch (Exception localException)
		{
			cat.error("getTitles Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#scheduleReport()
	 */
	@Override
	public boolean scheduleReport()
	{
		boolean retValue = false;

		try
		{
			this.setScheduling(true);
			String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
			String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

			ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_EXTERNAL_CREDIT_OR_PAYMENT, 1);
			scheduleReport.setNameDateTimeColumn("mc.log_datetime");
			scheduleReport.setRelativeQuery(relativeQuery);
			cat.debug("Relative Query : " + relativeQuery);
			scheduleReport.setFixedQuery(fixedQuery);
			cat.debug("Fixed Query : " + fixedQuery);
			TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this
					.getTitles());
			this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("ExternalCreditOrPaymentReport.scheduleReport Exception : " + localException.toString());
		}

		return retValue;
	}

}
