/**
 * 
 */
package com.debisys.reports.schedule;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.schedule.ISupportSiteReport.QUERY_TYPE;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.TimeZone;

/**
 * @author fernandob
 * 
 */
public class VoidTopupRequestReport extends ReportDownloader implements ISupportSiteReport
{
	// attributes

	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	private String pinCase;
	private String merchant;
	private String status;
	private String start_date;
	private String end_date;
	private boolean scheduling;
	private String strCurrentDate;
	private boolean allStatusSelected;
	private boolean allMerchantsSelected;
	private boolean UsingDateLimitInterval;
	private int limitDays;
	
	// properties
	
	/**
	 * @return the limitDays
	 */
	public int getLimitDays()
	{
		return limitDays;
	}

	/**
	 * @param limitDays the limitDays to set
	 */
	public void setLimitDays(int limitDays)
	{
		this.limitDays = limitDays;
	}

	/**
	 * @return the usingDateLimitInterval
	 */
	public boolean isUsingDateLimitInterval()
	{
		return UsingDateLimitInterval;
	}

	/**
	 * @param usingDateLimitInterval the usingDateLimitInterval to set
	 */
	public void setUsingDateLimitInterval(boolean usingDateLimitInterval)
	{
		UsingDateLimitInterval = usingDateLimitInterval;
	}

	/**
	 * @return the start_date
	 */
	public String getStart_date()
	{
		return start_date;
	}

	/**
	 * @param startDate
	 *            the start_date to set
	 */
	public void setStart_date(String startDate)
	{
		start_date = startDate;
	}

	/**
	 * @return the end_date
	 */
	public String getEnd_date()
	{
		return end_date;
	}

	/**
	 * @param endDate
	 *            the end_date to set
	 */
	public void setEnd_date(String endDate)
	{
		end_date = endDate;
	}

	/**
	 * @return the sessionData
	 */
	public SessionData getSessionData()
	{
		return sessionData;
	}

	/**
	 * @param sessionData
	 *            the sessionData to set
	 */
	public void setSessionData(SessionData sessionData)
	{
		this.sessionData = sessionData;
	}

	/**
	 * @return the scheduling
	 */
	public boolean isScheduling()
	{
		return scheduling;
	}

	/**
	 * @param shceduling
	 *            the scheduling to set
	 */
	public void setScheduling(boolean shceduling)
	{
		this.scheduling = shceduling;
	}

	/**
	 * @return the strCurrentDate
	 */
	public String getStrCurrentDate()
	{
		return strCurrentDate;
	}

	/**
	 * @param strCurrentDate
	 *            the strCurrentDate to set
	 */
	public void setStrCurrentDate(String strCurrentDate)
	{
		this.strCurrentDate = strCurrentDate;
	}

	/**
	 * @return the pinCase
	 */
	public String getPinCase()
	{
		return pinCase;
	}

	/**
	 * @param pinCase
	 *            the pinCase to set
	 */
	public void setPinCase(String pinCase)
	{
		this.pinCase = pinCase;
	}

	/**
	 * @return the merchant
	 */
	public String getMerchant()
	{
		return merchant;
	}

	/**
	 * @param merchant
	 *            the merchant to set
	 */
	public void setMerchant(String merchant)
	{
		this.merchant = merchant;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}

	/**
	 * @return the allStatusSelected
	 */
	public boolean isAllStatusSelected()
	{
		return allStatusSelected;
	}

	/**
	 * @param allStatusSelected
	 *            the allStatusSelected to set
	 */
	public void setAllStatusSelected(boolean allStatusSelected)
	{
		this.allStatusSelected = allStatusSelected;
	}

	/**
	 * @return the allMerchantsSelected
	 */
	public boolean isAllMerchantsSelected()
	{
		return allMerchantsSelected;
	}

	/**
	 * @param allMerchantsSelected
	 *            the allMerchantsSelected to set
	 */
	public void setAllMerchantsSelected(boolean allMerchantsSelected)
	{
		this.allMerchantsSelected = allMerchantsSelected;
	}

	// methods
	public VoidTopupRequestReport()
	{
		super();
		this.pinCase = null;
		this.merchant = null;
		this.status = null;
		this.start_date = null;
		this.end_date = null;
		this.sessionData = null;
		this.scheduling = false;
		this.strCurrentDate = null;
		this.allStatusSelected = false;
		this.allMerchantsSelected = false;
		this.UsingDateLimitInterval = false;
		this.limitDays = 0;
	}

	/**
	 * @param pinCase
	 * @param iso
	 * @param merchant
	 * @param status
	 * @param startDate
	 * @param endDate
	 * @param sessionData
	 * @param shceduling
	 * @param strCurrentDate
	 */
	public VoidTopupRequestReport(SessionData sessionData, ServletContext pContext, String pinCase, String merchant, String status, String startDate,
			String endDate, boolean shceduling, boolean allStatus, boolean allMerchants)
	{
		this();
		this.setContext(pContext);
		this.pinCase = pinCase;
		this.merchant = merchant;
		this.status = status;
		start_date = startDate;
		end_date = endDate;
		this.sessionData = sessionData;
                this.strRefId = sessionData.getProperty("ref_id");
		this.scheduling = shceduling;
		this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
		this.allStatusSelected = allStatus;
		this.allMerchantsSelected = allMerchants;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileFooter()
	 */
	@Override
	protected boolean writeDownloadFileFooter() throws IOException
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileHeader()
	 */
	@Override
	protected boolean writeDownloadFileHeader() throws IOException
	{
		boolean retValue = false;
		try
		{
			this.writetofile(this.fc, "\"" + Languages.getString("jsp.admin.reports.voidtopup_request_report", sessionData.getLanguage()) + "\"\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
					+ sessionData.getUser().getUsername() + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
					+ this.getStrCurrentDate() + "\n");
			this.writetofile(fc, "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\"\n");
			if (sessionData.getProperty("repName") != null)
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage()) + "\","
						+ sessionData.getProperty("repName") + "\n");
			}
			else
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage()) + "\","
						+ sessionData.getUser().getCompanyName() + "\n");
			}

			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + "\"," + this.start_date + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + "\"," + this.end_date + "\n");

			if ((this.status != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.voidtopup_request_report.status", sessionData.getLanguage()) + "\",");
				if (this.allStatusSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.status);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}

			if ((this.merchant != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.voidtopup_request_report.merchants", sessionData.getLanguage()) + "\",");
				if (this.allMerchantsSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.merchant);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}

			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadFileHeader Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecords()
	 */
	@Override
	protected boolean writeDownloadRecords() throws IOException
	{
		boolean retValue = false;
		Vector<Vector<Object>> reportResults = this.getResults();

		try
		{
			if ((reportResults != null) && (reportResults.size() > 0))
			{
				for (int i = 0; i < reportResults.size(); i++)
				{
					Vector<Object> currentRecord = reportResults.get(i);
					// Write the line number
					this.writetofile(fc, "\"" + String.valueOf(i + 1) + "\",");
					for (int fieldIdx = 0; fieldIdx < currentRecord.size(); fieldIdx++)
					{
						this.writetofile(fc, "\"" + currentRecord.get(fieldIdx).toString().trim() + "\"");
						if (fieldIdx < (currentRecord.size() - 1))
						{
							this.writetofile(fc, ",");
						}
						else
						{
							this.writetofile(fc, "\n");
						}
					}
				}
				retValue = true;
			}
		}
		catch (NumberFormatException localNumberException)
		{
			cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadRecords Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecordsHeaders()
	 */
	@Override
	protected boolean writeDownloadRecordsHeaders() throws IOException
	{
		boolean retValue = false;

		ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

		if ((columnHeaders != null) && (columnHeaders.size() > 0))
		{
			this.writetofile(fc, "\"#\",");
			for (int i = 0; i < columnHeaders.size(); i++)
			{
				this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
				if (i < (columnHeaders.size() - 1))
				{
					this.writetofile(fc, ",");
				}
				else
				{
					this.writetofile(fc, "\n");
				}
			}
			retValue = true;
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#downloadReport(com.debisys.users
	 * .SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public String downloadReport()
	{
		return this.exportToFile();
	}

	
	private boolean isPinCaseSet()
	{
		return ((this.pinCase != null) && (!this.pinCase.equals("")));
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#generateSqlString(com.debisys.
	 * users.SessionData, javax.servlet.ServletContext, boolean)
	 */
	@Override
	public String generateSqlString(QUERY_TYPE qType)
	{
		String retValue = null;
		StringBuffer strSQL = new StringBuffer();

		try
		{
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");
			Vector<String> vTimeZoneFilterDates = null; 
				
			if (!this.isPinCaseSet())
			{
				vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date
					+ " 23:59:59.999", false);
			}

			strSQL
					.append("SELECT	PRR.ID, PRR.RequestDate, M.DBA, T.millennium_no, PRR.control_no, P.transaction_count, PRO.Description AS OriginDescription,  ");
			strSQL
					.append("		PRRS.Code AS StatusCode, PRRS.Description AS StatusDescription, PRR.RequestStatusDesc, M.rec_id, M.merchant_id, M.rep_id,  ");
			strSQL.append("		PD.id AS SKU, PD.description, P.ani, PRR.rec_id AS TransID, TR.client  ");
			if (this.isScheduling())
			{
				if ((qType == QUERY_TYPE.FIXED) && (!this.isPinCaseSet()))
				{
					strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
					strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
				}
				else
				{
					strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
				}
			}
			strSQL.append("FROM	TOPUPReturnRequests PRR WITH(NOLOCK) ");
			strSQL.append("		INNER JOIN Terminals T WITH(NOLOCK) ON PRR.millennium_no = T.millennium_no  ");
			strSQL.append("		INNER JOIN Merchants M WITH(NOLOCK) ON T.merchant_id = M.merchant_id  ");
			strSQL.append("		INNER JOIN Transactions TR WITH(NOLOCK) ON TR.rec_id = PRR.rec_id  ");
			strSQL.append("		INNER JOIN PINS P WITH(NOLOCK) ON P.control_no = TR.control_no  ");
			strSQL.append("		INNER JOIN Products PD WITH(NOLOCK) ON P.product_id = PD.id  ");
			strSQL.append("		INNER JOIN Return_Type RT WITH(NOLOCK) ON PD.Return_Type = RT.Return_Type  ");
			strSQL.append("		INNER JOIN PINReturnOrigin PRO WITH(NOLOCK) ON PRR.SentBy = PRO.ID  ");
			strSQL.append("		INNER JOIN TOPUPReturnRequestStatus PRRS WITH(NOLOCK) ON PRR.RequestStatus = PRRS.ID  ");
			strSQL.append("WHERE 1 = 1 ");

			if (this.allMerchantsSelected)
			{
				User currentUser = this.sessionData.getUser();
				strSQL.append(" AND  "
						+ ReportsUtil.getChainToBuildWhereSQL(currentUser.getAccessLevel(), currentUser.getDistChainType(), currentUser.getRefId(),
								"M.rep_id"));
			}
			else
			{
				if ((this.merchant != null) && (!this.merchant.equals("")))
				{
					if (this.merchant.indexOf(",") != -1)
					{
						strSQL.append(" AND M.merchant_id IN " + this.merchant);
					}
					else
					{
						strSQL.append(" AND M.merchant_id =" + this.merchant);
					}
				}
			}

			if ((this.pinCase != null) && (!this.pinCase.equals("")))
			{
				strSQL.append(" AND PRR.ID=" + this.pinCase);
			}
			else
			{
				if (((this.start_date != null) && (!this.start_date.trim().equals("")))
						&& ((this.end_date != null) && (!this.end_date.trim().equals(""))))
				{
					if (this.isScheduling())
					{
						if (qType == QUERY_TYPE.RELATIVE)
						{
							strSQL.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
						}
						else if (qType == QUERY_TYPE.FIXED)
						{
							strSQL.append("         AND (PRR.RequestDate  >= '" + vTimeZoneFilterDates.get(0) + "'");
							strSQL.append("         AND PRR.RequestDate  < '" + vTimeZoneFilterDates.get(1) + "')");
						}
					}
					else
					{
						// If not scheduling, the wild card is removed, and the
						// current report dates are used
						int i;
						while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
						{
							strSQL.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
						}
						strSQL.append("         AND (PRR.RequestDate  >= '" + vTimeZoneFilterDates.get(0) + "'");
						strSQL.append("         AND PRR.RequestDate  < '" + vTimeZoneFilterDates.get(1) + "')");
					}
				}
				if ((this.status != null) && (!this.status.equals("")))
				{
					if (this.status.indexOf(",") != -1)
					{
						strSQL.append(" AND PRRS.Code IN " + this.status);
					}
					else
					{
						strSQL.append(" AND PRRS.Code = '" + this.status + "'");
					}
				}
			}
			cat.debug(strSQL);
			retValue = strSQL.toString();

		}
		catch (ReportException localReportException)
		{
			cat.error("VoidTopupRequestReport.getSqlString. Exception while generationg sql string : " + localReportException.toString());
		}
		catch (Exception localException)
		{
			cat.error("VoidTopupRequestReport.getSqlString. Exception while generationg sql string : " + localException.toString());
		}

		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#getReportRecordHeaders(com.debisys
	 * .users.SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public ArrayList<ColumnReport> getReportRecordHeaders()
	{
		ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();

		try
		{
			retValue
					.add(new ColumnReport("ID", Languages
							.getString("jsp.admin.reports.voidtopup_request_report.grid.Case", sessionData.getLanguage()).toUpperCase(),
							String.class, false));
			retValue.add(new ColumnReport("RequestDate", Languages.getString("jsp.admin.reports.voidtopup_request_report.grid.DateRequest",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("DBA", Languages.getString("jsp.admin.reports.voidtopup_request_report.grid.BusinessName",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("millennium_no", Languages.getString("jsp.admin.reports.voidtopup_request_report.grid.Site",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("TransID", Languages.getString("jsp.admin.reports.voidtopup_request_report.grid.TransID",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("description", Languages.getString("jsp.admin.reports.voidtopup_request_report.grid.ProdDescription",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("SKU", Languages
					.getString("jsp.admin.reports.voidtopup_request_report.grid.SKU", sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("ani", Languages
					.getString("jsp.admin.reports.voidtopup_request_report.grid.ANI", sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("rep_id", Languages.getString("jsp.admin.reports.voidtopup_request_report.grid.rep_id",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("StatusDescription", Languages.getString(
					"jsp.admin.reports.voidtopup_request_report.grid.StatusDescription", sessionData.getLanguage()).toUpperCase(), String.class,
					false));
			retValue.add(new ColumnReport("RequestStatusDesc", Languages.getString(
					"jsp.admin.reports.voidtopup_request_report.grid.RequestStatusDesc", sessionData.getLanguage()).toUpperCase(), String.class,
					false));
			retValue.add(new ColumnReport("OriginDescription", Languages.getString(
					"jsp.admin.reports.voidtopup_request_report.grid.OriginDescription", sessionData.getLanguage()).toUpperCase(), String.class,
					false));
			retValue.add(new ColumnReport("client", Languages.getString("jsp.admin.reports.voidtopup_request_report.grid.client",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		catch (Exception localException)
		{
			cat.error("VoidTopupRequestReport.getReportRecordHeaders Exception : " + localException);
		}

		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * The following calls should be done before getting results
	 * this.setContext(); this.setConvenienceCardCarrier_ids();
	 * this.setDeployment(); this.setEnd_date(endDate);
	 * this.setPermissionInterCustomConfig(); this.setProduct_ids();
	 * this.setSelectedMerchantIDs(); this.setSessionData() ;
	 * this.setShceduling() ; this.setStart_date() ; this.setStrCurrentDate() ;
	 * this.setTaxCalculationEnabled(); this.setTaxEnabled();
	 * 
	 * @seecom.debisys.reports.ISupportSiteReport#getResults(com.debisys.users.
	 * SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public Vector<Vector<Object>> getResults()
	{
		Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			this.setScheduling(false); // If returning results, then scheduling
										// does not have sense
			String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector<Object> vecTemp = new Vector<Object>();
				vecTemp.add(0, rs.getString("ID"));
				vecTemp.add(1, DateUtil.formatDateTime(rs.getTimestamp("RequestDate")));
				vecTemp.add(2, rs.getString("dba"));
				vecTemp.add(3, rs.getString("millennium_no"));
				vecTemp.add(4, rs.getString("TransID"));
				vecTemp.add(5, rs.getString("description"));
				vecTemp.add(6, rs.getString("SKU"));
				vecTemp.add(7, rs.getString("ani"));
				vecTemp.add(8, rs.getString("rep_id"));
				vecTemp.add(9, rs.getString("StatusDescription"));
				vecTemp.add(10, rs.getString("RequestStatusDesc"));
				vecTemp.add(11, rs.getString("OriginDescription"));
				vecTemp.add(12, rs.getString("client"));
				retValue.add(vecTemp);
			}
			if (retValue.size() == 0)
			{
				retValue = null;
			}
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("VoidTopupRequestReport.getResults Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("VoidTopupRequestReport.getResults Exception : " + localReportException.toString());
		}
		catch (SQLException localSqlException)
		{
			cat.error("VoidTopupRequestReport.getResults Exception : " + localSqlException.toString());
		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
					rs = null;
				}
				if (pstmt != null)
				{
					pstmt.close();
					pstmt = null;
				}
				if (dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (SQLException cleaningSqlException)
			{
				cat.error("VoidTopupRequestReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}

		return retValue;
	}

	@Override
	public ArrayList<String> getTitles()
	{
		ArrayList<String> retValue = new ArrayList<String>();

		try
		{
			retValue.add(Languages.getString("jsp.admin.reports.voidtopup_request_report", this.sessionData.getLanguage()));
			Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
			String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
					+ vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
			retValue.add(noteTimeZone);
		}
		catch (NumberFormatException localNumberFormatException)
		{
			cat.error("getTitles Exception : " + localNumberFormatException.toString());
		}
		catch (Exception localException)
		{
			cat.error("getTitles Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#scheduleReport(com.debisys.users
	 * .SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public boolean scheduleReport()
	{
		boolean retValue = false;

		try
		{
			this.setScheduling(true);
			String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
			String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

			ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_VOID_TOPUP_REQUESTS, 1);
			scheduleReport.setNameDateTimeColumn("PRR.RequestDate");
			scheduleReport.setRelativeQuery(relativeQuery);
			scheduleReport.setFixedQuery(fixedQuery);
			TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
			this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("VoidTopupRequestReport.scheduleReport Exception : " + localException.toString());
		}

		return retValue;
	}

}
