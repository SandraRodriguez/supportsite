/**
 * 
 */
package com.debisys.reports.schedule;

import static com.emida.utils.crypto.TRProxyFactory.dataProtection;
import static com.emida.utils.dbUtils.CommonQueries.getSkuFromPins;
import static com.emida.utils.dbUtils.CommonQueries.getSkusWithEncryption;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.TransactionReport;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TaxTypes;
import com.debisys.utils.TimeZone;
import java.math.BigDecimal;

/**
 * @author fernandob
 *
 */
public class TransactionsReport extends ReportDownloader implements ISupportSiteReport
{
	// attributes
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private boolean scheduling;
	private String start_date;
	private String end_date;
	private String invoiceNumber;
	private String strCurrentDate;
	private boolean includeTax;
	private int intSectionPage;
	private String product_ids;
	private String rep_id;
	private String merchant_id; 
	private String merchant_ids;
	private String transactionID;
	private String PINNumber;
	private String millennium_no;
	private boolean viewReferenceCard;
	private int intRecordsPerPage;
	private int intPageNumber;
	private boolean showAdditionalData;
	private double dTax;
	private boolean allMerchantsSelected;
	private boolean UsingDateLimitInterval;
	private int limitDays;
	private String reportTitle;
        private String s2kRateplans;
	
	// properties
	
	/**
	 * @return the reportTitle
	 */
	public String getReportTitle()
	{
		return reportTitle;
	}

	/**
	 * @param reportTitle the reportTitle to set
	 */
	public void setReportTitle(String reportTitle)
	{
		this.reportTitle = reportTitle;
	}

	/**
	 * @return the limitDays
	 */
	public int getLimitDays()
	{
		return limitDays;
	}

	/**
	 * @param limitDays the limitDays to set
	 */
	public void setLimitDays(int limitDays)
	{
		this.limitDays = limitDays;
	}

	/**
	 * @return the usingDateLimitInterval
	 */
	public boolean isUsingDateLimitInterval()
	{
		return UsingDateLimitInterval;
	}

	/**
	 * @param usingDateLimitInterval the usingDateLimitInterval to set
	 */
	public void setUsingDateLimitInterval(boolean usingDateLimitInterval)
	{
		UsingDateLimitInterval = usingDateLimitInterval;
	}
	/**
	 * @return the scheduling
	 */
	public boolean isScheduling()
	{
		return scheduling;
	}

	/**
	 * @param scheduling the scheduling to set
	 */
	public void setScheduling(boolean scheduling)
	{
		this.scheduling = scheduling;
	}

	/**
	 * @return the start_date
	 */
	public String getStart_date()
	{
		return start_date;
	}

	/**
	 * @param startDate the start_date to set
	 */
	public void setStart_date(String startDate)
	{
		start_date = startDate;
	}

	/**
	 * @return the end_date
	 */
	public String getEnd_date()
	{
		return end_date;
	}

	/**
	 * @param endDate the end_date to set
	 */
	public void setEnd_date(String endDate)
	{
		end_date = endDate;
	}

	/**
	 * @return the invoiceNumber
	 */
	public String getInvoiceNumber()
	{
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber the invoiceNumber to set
	 */
	public void setInvoiceNumber(String invoiceNumber)
	{
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return the strCurrentDate
	 */
	public String getStrCurrentDate()
	{
		return strCurrentDate;
	}

	/**
	 * @return the includeTax
	 */
	public boolean isIncludeTax()
	{
		return includeTax;
	}

	/**
	 * @param includeTax the includeTax to set
	 */
	public void setIncludeTax(boolean includeTax)
	{
		this.includeTax = includeTax;
	}

	/**
	 * @param strCurrentDate the strCurrentDate to set
	 */
	public void setStrCurrentDate(String strCurrentDate)
	{
		this.strCurrentDate = strCurrentDate;
	}
	
	/**
	 * @return the intSectionPage
	 */
	public int getIntSectionPage()
	{
		return intSectionPage;
	}

	/**
	 * @param intSectionPage the intSectionPage to set
	 */
	public void setIntSectionPage(int intSectionPage)
	{
		this.intSectionPage = intSectionPage;
	}

	/**
	 * @return the product_ids
	 */
	public String getProduct_ids()
	{
		return product_ids;
	}

	/**
	 * @param productIds the product_ids to set
	 */
	public void setProduct_ids(String productIds)
	{
		product_ids = productIds;
	}

	/**
	 * @return the rep_id
	 */
	public String getRep_id()
	{
		return rep_id;
	}

	/**
	 * @param repId the rep_id to set
	 */
	public void setRep_id(String repId)
	{
		rep_id = repId;
	}

	/**
	 * @return the merchant_id
	 */
	public String getMerchant_id()
	{
		return merchant_id;
	}

	/**
	 * @param merchantId the merchant_id to set
	 */
	public void setMerchant_id(String merchantId)
	{
		merchant_id = merchantId;
	}

	/**
	 * @return the merchant_ids
	 */
	public String getMerchant_ids()
	{
		return merchant_ids;
	}

	/**
	 * @param merchantIds the merchant_ids to set
	 */
	public void setMerchant_ids(String merchantIds)
	{
		merchant_ids = merchantIds;
	}

	/**
	 * @return the transactionID
	 */
	public String getTransactionID()
	{
		return transactionID;
	}

	/**
	 * @param transactionID the transactionID to set
	 */
	public void setTransactionID(String transactionID)
	{
		this.transactionID = transactionID;
	}

	/**
	 * @return the pINNumber
	 */
	public String getPINNumber()
	{
		return PINNumber;
	}

	/**
	 * @param pINNumber the pINNumber to set
	 */
	public void setPINNumber(String pINNumber)
	{
		PINNumber = pINNumber;
	}

	/**
	 * @return the millennium_no
	 */
	public String getMillennium_no()
	{
		return millennium_no;
	}

	/**
	 * @param millenniumNo the millennium_no to set
	 */
	public void setMillennium_no(String millenniumNo)
	{
		millennium_no = millenniumNo;
	}

	/**
	 * @return the viewReferenceCard
	 */
	public boolean isViewReferenceCard()
	{
		return viewReferenceCard;
	}

	/**
	 * @param viewReferenceCard the viewReferenceCard to set
	 */
	public void setViewReferenceCard(boolean viewReferenceCard)
	{
		this.viewReferenceCard = viewReferenceCard;
	}

	/**
	 * @return the intRecordsPerPage
	 */
	public int getIntRecordsPerPage()
	{
		return intRecordsPerPage;
	}

	/**
	 * @param intRecordsPerPage the intRecordsPerPage to set
	 */
	public void setIntRecordsPerPage(int intRecordsPerPage)
	{
		this.intRecordsPerPage = intRecordsPerPage;
	}

	/**
	 * @return the intPageNumber
	 */
	public int getIntPageNumber()
	{
		return intPageNumber;
	}

	/**
	 * @param intPageNumber the intPageNumber to set
	 */
	public void setIntPageNumber(int intPageNumber)
	{
		this.intPageNumber = intPageNumber;
	}

	/**
	 * @return the showAdditionalData
	 */
	public boolean isShowAdditionalData()
	{
		return showAdditionalData;
	}

	/**
	 * @param showAdditionalData the showAdditionalData to set
	 */
	public void setShowAdditionalData(boolean showAdditionalData)
	{
		this.showAdditionalData = showAdditionalData;
	}

	/**
	 * @return the dTax
	 */
	public double getdTax()
	{
		return dTax;
	}

	/**
	 * @param dTax the dTax to set
	 */
	public void setdTax(double dTax)
	{
		this.dTax = dTax;
	}

	/**
	 * @return the allMerchantsSelected
	 */
	public boolean isAllMerchantsSelected()
	{
		return allMerchantsSelected;
	}

	/**
	 * @param allMerchantsSelected the allMerchantsSelected to set
	 */
	public void setAllMerchantsSelected(boolean allMerchantsSelected)
	{
		this.allMerchantsSelected = allMerchantsSelected;
	}

    public String getS2kRateplans() {
        return s2kRateplans;
    }

    public void setS2kRateplans(String s2kRateplans) {
        this.s2kRateplans = s2kRateplans;
    }
        
        

		// methods
	
	public TransactionsReport()
	{
		super();
		this.scheduling = false;
		this.start_date = null;
		this.end_date = null;
		this.invoiceNumber = null;
		this.strCurrentDate = null;
		this.includeTax = false;
		this.intSectionPage = Integer.MIN_VALUE;
		this.product_ids = null;
		this.rep_id = null;
		this.merchant_id = null;
		this.merchant_ids = null;
		this.transactionID = null;
		this.PINNumber = null;
		this.millennium_no = null;
		this.viewReferenceCard = false;
		this.intRecordsPerPage = Integer.MIN_VALUE;
		this.intPageNumber = Integer.MIN_VALUE;
		this.showAdditionalData = false;
		this.dTax = Double.MIN_VALUE;
		this.allMerchantsSelected = false;
		this.UsingDateLimitInterval = false;
		this.limitDays = 0;
	}

	/**
	 * @param scheduling
	 * @param startDate
	 * @param endDate
	 * @param transactionId
	 * @param invoiceNumber
	 * @param strCurrentDate
	 * @param includeTax
	 * @param intSectionPage
	 * @param productIds
	 * @param repId
	 * @param merchantId
	 * @param merchantIds
	 * @param transactionID2
	 * @param pINNumber
	 * @param millenniumNo
	 * @param viewReferenceCard
	 * @param intRecordsPerPage
	 * @param intPageNumber
	 * @param showAdditionalData
	 */
	public TransactionsReport(SessionData sessionData, ServletContext pContext, boolean scheduling, String startDate, String endDate, 
			String invoiceNumber, String pINNumber, boolean includeTax, int intSectionPage, String productIds,
			String repId, String merchantId, String merchantIds, String transactionId, String millenniumNo, boolean viewReferenceCard,
			int intRecordsPerPage, int intPageNumber, boolean showAdditionalData, boolean allMerchSel)
	{
		this();
		this.sessionData = sessionData;
		this.setContext(pContext);
                this.loadProperties();
                this.strRefId = sessionData.getProperty("ref_id");
		this.scheduling = scheduling;
		this.start_date = startDate;
		this.end_date = endDate;
		this.invoiceNumber = invoiceNumber;
		this.PINNumber = pINNumber;
		this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
		this.includeTax = includeTax;
		this.intSectionPage = intSectionPage;
		this.product_ids = productIds;
		this.rep_id = repId;
		this.merchant_id = merchantId;
		this.merchant_ids = merchantIds;
		this.transactionID = transactionId;
		this.millennium_no = millenniumNo;
		this.viewReferenceCard = viewReferenceCard;
		this.intRecordsPerPage = intRecordsPerPage;
		this.intPageNumber = intPageNumber;
		this.showAdditionalData = showAdditionalData;
		this.dTax = 0.0;
		this.allMerchantsSelected = allMerchSel;
	}

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ReportDownloader#writeDownloadFileFooter()
	 */
	@Override
	protected boolean writeDownloadFileFooter() throws IOException
	{
		return true;
	}

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ReportDownloader#writeDownloadFileHeader()
	 */
	@Override
	protected boolean writeDownloadFileHeader() throws IOException
	{
		return true;
	}

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ReportDownloader#writeDownloadRecords()
	 */
	@Override
	protected boolean writeDownloadRecords() throws IOException
	{
		return true;
	}

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ReportDownloader#writeDownloadRecordsHeaders()
	 */
	@Override
	protected boolean writeDownloadRecordsHeaders() throws IOException
	{
		return true;
	}

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ISupportSiteReport#downloadReport()
	 */
	@Override
	public String downloadReport()
	{
		return null;
	}
	
	private ArrayList<String> getSkusWithEncryptionWrapper()
	{
		ArrayList<String> retValue = null;
		Connection dbConn = null;
	
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			retValue = getSkusWithEncryption(dbConn);
			
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("TransactionsReport.getResults Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("TransactionsReport.getResults Exception : " + localReportException.toString());
		}
		finally
		{
			try
			{
				if(dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (Exception cleaningSqlException)
			{
				cat.error("TransactionsReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}		
		return retValue;		
	}

	private String getSkuFromPinsWrapper(String pin)
	{
		String retValue = null;
		Connection dbConn = null;
	
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			retValue = getSkuFromPins(pin, dbConn);
			
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("TransactionsReport.getResults Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("TransactionsReport.getResults Exception : " + localReportException.toString());
		}
		finally
		{
			try
			{
				if(dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (Exception cleaningSqlException)
			{
				cat.error("TransactionsReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}		
		return retValue;		
	}
	
	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ISupportSiteReport#generateSqlString(com.debisys.reports.schedule.ISupportSiteReport.QUERY_TYPE)
	 */
	@Override
	public String generateSqlString(QUERY_TYPE qType)
	{
		String retValue = null;

		StringBuilder strSQL = new StringBuilder();
		StringBuilder strSearchSQL = new StringBuilder();
		StringBuilder strSQLWhere = new StringBuilder();
		StringBuilder strAdditionalTablesCarrier = new StringBuilder();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		boolean showAdditionalData = sessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA);
		boolean isDeploymentInternational = DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
			DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
		boolean viewReferenceCard = false;
		ArrayList<String> skusWithEncryption = null;
		String strRepsSelect = "";
		String viewReferenceCardJOIN = "";

		try
		{
		    Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
			dTax = 1;
			if (isDeploymentInternational)
			{
				viewReferenceCard = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
			}
			
		    if ( includeTax || (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && isDeploymentInternational) )
		    {
		    	if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && isDeploymentInternational )
		    	{
		    		dTax = TransactionReport.getTaxValue(sessionData,this.end_date);
		    		strSQL.append("select distinct wt.rec_id,wt.invoice_no, wt.millennium_no, terminal_types.description as trm_description, wt.dba, wt.phys_city, wt.phys_county, ");
		    		strSQL.append(" wt.merchant_id,wt.amount as amount, ");
                                strSQL.append(" wt.amount/(1 + isnull(wt.taxpercentage / 10000, " + Double.toString(dTax) + " - 1)) as netamount, ");
                                strSQL.append(" wt.trueamount/(1 + isnull(wt.taxpercentage / 10000, " + Double.toString(dTax) + " - 1)) as netamountWhole, ");
		    		strSQL.append(" wt.amount - wt.amount/(1 + isnull(wt.taxpercentage / 10000, " + Double.toString(dTax) + " - 1)) as taxamount ");
                                //strSQL.append(" wt.trueamount - wt.trueamount/(1 + isnull(wt.taxpercentage / 10000, " + Double.toString(dTax) + " - 1)) as taxamountWhole, ");
                                strSQL.append(", wt.trueamount - wt.amount AS taxamountWhole ");
		    		strSQL.append(", dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", wt.datetime, 1) as datetime, wt.control_no, wt.clerk_name, wt.clerk, ");
		    		strSQL.append(" wt.transaction_type, wt.phys_state, wt.other_info, wt.ani, wt.isQRCode, wt.password, wt.balance, wt.id, wt.description, wt.product_trans_type, wt.bonus_amount, ");
		    		strSQL.append(" (wt.bonus_amount + wt.amount) as total_recharge, CAST(round(CAST(ISNULL(wt.taxpercentage,0) / 100  AS float),2) AS VARCHAR(10)) + '%' as taxpercentage, wt.taxtype, ");
		    		setSelectCommission(strSQL);
		    		strSQL.append(" rp.Name AS rateplanName");
                                strSQL.append(", wtp.tax AS wholeTax");
                                strSQL.append(", wt.trueamount ");
                                strSQL.append(", tx.sourceAmount as trnNewTax ");
                                this.addPromoData(strSQL);
		    	}
		    	else
		    	{
		    		dTax = 1 + (DebisysConfigListener.getMxValueAddedTax(context) / 100);
		    		strSQL.append("select distinct wt.rec_id,wt.invoice_no, wt.millennium_no, terminal_types.description as trm_description, wt.dba, wt.phys_city, wt.phys_county, ");
		    		strSQL.append(" wt.merchant_id, (wt.amount / " + Double.toString(dTax) + ") as amount, " + "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId);
		    		strSQL.append(", wt.datetime, 1) as datetime, wt.control_no, wt.clerk_name, wt.clerk, wt.transaction_type, wt.phys_state, wt.other_info, wt.ani, wt.isQRCode, wt.password, (wt.balance ) as balance, ");
		    		strSQL.append(" wt.id, wt.description, wt.product_trans_type, wt.bonus_amount, (wt.bonus_amount + wt.amount) as total_recharge, CAST(round(CAST(ISNULL(wt.taxpercentage,0) / 100  AS float),2) AS VARCHAR(10)) + '%' as taxpercentage, wt.taxtype, ");
		    		setSelectCommission(strSQL);
		    		strSQL.append(" rp.Name AS rateplanName");
                                strSQL.append(", wtp.tax AS wholeTax");
                                strSQL.append(", wt.trueamount ");
                                strSQL.append(", tx.sourceAmount as trnNewTax ");
                                this.addPromoData(strSQL);
		    	}
		    }
		    else
			{
		    	strSQL.append("select distinct wt.rec_id,wt.invoice_no, wt.millennium_no, terminal_types.description as trm_description, wt.dba, wt.phys_city, wt.phys_county, ");
		    	strSQL.append(" wt.merchant_id, wt.amount, " + "dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", wt.datetime, 1) as datetime, wt.control_no, wt.clerk_name, wt.clerk, ");
		    	strSQL.append(" wt.transaction_type, wt.phys_state, wt.other_info, wt.ani, wt.isQRCode, wt.password, (wt.balance) as balance, ");
		    	strSQL.append(" wt.id, wt.description, wt.product_trans_type, wt.bonus_amount, (wt.bonus_amount + wt.amount) as total_recharge, CAST(round(CAST(ISNULL(wt.taxpercentage,0) / 100  AS float),2) AS VARCHAR(10)) + '%' as taxpercentage, wt.taxtype, ");
                        setSelectCommission(strSQL);
                        strSQL.append(" rp.Name AS rateplanName");
                        strSQL.append(", wtp.tax AS wholeTax");
                        strSQL.append(", wt.trueamount ");
                        strSQL.append(", tx.sourceAmount as trnNewTax ");
                        this.addPromoData(strSQL);
			}
			
			
		    switch (intSectionPage)
		    {
		      //transactions.jsp
		      case 1:

		    	if (strAccessLevel.equals(DebisysConstants.CARRIER))
				{
		    		if (showAdditionalData)
		    		{
		    			strSQL.append(", wt.additionalData ");
		    		}

		    		if (this.rep_id.length() == 0)
					{
						strRepsSelect = " SELECT m.rep_id FROM merchants m WITH (NOLOCK) "
							+ "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.merchantid "
										+ "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
										+ "INNER JOIN products p WITH (NOLOCK) ON rc.CarrierID = p.carrier_id AND wt.id = p.id WHERE " + "rc.repid = " + strRefId;
					}
					else
					{
						strRepsSelect = " SELECT rep_id FROM reps WITH (nolock) WHERE type=1 and iso_id IN "
								+ "(SELECT rep_id FROM reps WITH (nolock) WHERE type=5 and iso_id IN "
								+ "(SELECT rep_id FROM reps WITH (nolock) WHERE type=4 and iso_id IN "
								+ "(SELECT rep_id FROM reps WITH (nolock) WHERE type IN (2,3) AND rep_id IN (" + this.rep_id + ")))) "
								+ "AND rep_id IN (SELECT DISTINCT rep_id FROM Merchants m WITH (NOLOCK) "
										+ "INNER JOIN MerchantCarriers mc WITH (NOLOCK) ON m.merchant_id = mc.MerchantID AND m.merchant_id = wt.merchant_id "
										+ "INNER JOIN RepCarriers rc WITH (NOLOCK) ON mc.CarrierID = rc.CarrierID "
										+ "INNER JOIN Products p WITH (NOLOCK) ON rc.CarrierID = p.carrier_id AND wt.id = p.id " + "WHERE rc.repid = " + strRefId
										+ ")";
					}

		    		 strSearchSQL.append(" wt.rep_id in (" +strRepsSelect + ") ");

				}
		    	else if (strAccessLevel.equals(DebisysConstants.ISO))
		        {
		        	if (showAdditionalData)
		        	{
		        			strSQL.append(", wt.additionalData ");
		        	}

		          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		          {
		        	strRepsSelect = " select rep_id from reps with (nolock) where iso_id = " + strRefId;
		            strSearchSQL.append(" wt.rep_id IN ("+strRepsSelect + ") ");
		          }
		          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		          {
		        	strRepsSelect = "select rep_id from reps with (nolock) where iso_id in "
										+ "(select rep_id from reps with (nolock) where iso_id in " + "(select rep_id from reps with (nolock) where iso_id = "
										+ strRefId + "))";
		            strSearchSQL.append(" wt.rep_id IN (" +strRepsSelect+ ") ");

		          }
		          if (sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER))
							{
								strSearchSQL.append(" AND wt.id IN (");
								strSearchSQL.append(" SELECT p.id FROM Products p WITH (NOLOCK) INNER JOIN RepCarriers rc WITH (NOLOCK) ON p.carrier_id = rc.CarrierID ");
								strSearchSQL.append(" WHERE rc.RepID = " + sessionData.getUser().getRefId() + ")");


							}
		        }

		        break;
		        //rep_transactions.jsp
		      case 2:

		    	 if (strAccessLevel.equals(DebisysConstants.CARRIER))
				 {
		    		 strRepsSelect= "SELECT DISTINCT r.rep_id FROM reps AS r WITH(nolock) "+
			        	"INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id "+
			        	"INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id "+
			        	"INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid "+
			        	"INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
			        	"WHERE ta.repid =" + strRefId +
			        	" AND  wt.rep_id="+ this.rep_id;

		    		 strSearchSQL.append(" wt.rep_id in (" + strRepsSelect + ")");

				 }
		    	 else if (strAccessLevel.equals(DebisysConstants.ISO))
		         {
			          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
			          {
			        	strRepsSelect = "select rep_id from reps with (nolock) where iso_id=" + strRefId ;
			            strSearchSQL.append(" wt.rep_id = " + this.rep_id + " AND wt.rep_id in (select rep_id from reps with (nolock) where iso_id=" + strRefId + ")");
			          }
			          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			          {
			        	strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
					                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
					                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + "))";
			            strSearchSQL.append("wt.rep_id = " + this.rep_id + " AND wt.rep_id in (" +strRepsSelect+") ");
			          }
		        }
		        else if (strAccessLevel.equals(DebisysConstants.AGENT))
		        {
		          strSearchSQL.append(" wt.rep_id = " + this.rep_id + " AND wt.rep_id in ");
		          strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in ");
		          strSearchSQL.append(" (select rep_id from reps with (nolock) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id=" + strRefId + ")) ");

		        }
		        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		        {
		          strSearchSQL.append(" wt.rep_id = " + this.rep_id + " AND wt.rep_id in ");
		          strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id=" + strRefId + ")");
		        }
		        else if (strAccessLevel.equals(DebisysConstants.REP))
		        {
		        	strSearchSQL.append(" wt.rep_id = " + strRefId);
		        }
		        break;
		        //merchants_transactions.jsp
		      //can be accessed via reports or transactions section
		      case 3:
		    	  if (strAccessLevel.equals(DebisysConstants.CARRIER))
					 {
		    		  strRepsSelect= "SELECT DISTINCT merchants.rep_id FROM merchants WITH (NOLOCK) "+
						"INNER JOIN reps WITH (NOLOCK) ON merchants.rep_id = reps.rep_id "+
						"LEFT JOIN Routes WITH (NOLOCK) ON merchants.RouteID = Routes.RouteID "+
						"INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid = merchants.merchant_id "+
						"INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid "+
						"INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
						"WHERE ta.repid = " + strRefId +
						" AND wt.merchant_id=" + this.merchant_id;
			    		 strSearchSQL.append(" wt.rep_id in (" +strRepsSelect+") ");

					 }
		      case 14:
		    	 if (strAccessLevel.equals(DebisysConstants.CARRIER))
				 {
		    		  strRepsSelect = " ";
				      strSearchSQL.append(" mc.carrierid in (SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid = "+strRefId+")");
				      strAdditionalTablesCarrier.append(" inner join merchantcarriers mc WITH (NOLOCK) ON mc.merchantid=wt.merchant_id and mc.carrierid = p.carrier_id AND wt.merchant_id =" + this.merchant_id);
				 }
		    	 else if (strAccessLevel.equals(DebisysConstants.ISO))
		         {
		          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		          {
		        	  strRepsSelect="select rep_id from reps with (nolock) where iso_id=" + strRefId;
		            strSearchSQL.append(" wt.merchant_id = " + this.merchant_id + " AND wt.rep_id in (select rep_id from reps with (nolock) where iso_id=" + strRefId + ")");
		          }
		          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		          {
		        	  strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
		                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
		                "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + "))";
		            strSearchSQL.append(" wt.merchant_id = " + this.merchant_id + " AND wt.rep_id in (" +strRepsSelect+ ") ");
		          }
		        }
		        else if (strAccessLevel.equals(DebisysConstants.AGENT))
		        {
		          strSearchSQL.append(" wt.merchant_id = " + this.merchant_id + " AND wt.rep_id in ");
		          strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in ");
		          strSearchSQL.append("  (select rep_id from reps with (nolock) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id=" + strRefId + ")) ");
		        }
		        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		        {
		          strSearchSQL.append(" wt.merchant_id = " + this.merchant_id + " AND rep_id in ");
		          strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id=" + strRefId + ")");
		        }
		        else if (strAccessLevel.equals(DebisysConstants.REP))
		        {
		          strSearchSQL.append(" wt.merchant_id = " + this.merchant_id + " AND wt.rep_id = " + strRefId);
		        }
		        else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
		        {
		          strSearchSQL.append(" wt.merchant_id = " + strRefId);
		        }

		        break;
		        //agents_transactions.jsp
		      case 5:
		    	 if (strAccessLevel.equals(DebisysConstants.CARRIER))
			     {
		    		 strRepsSelect="SELECT rep_id FROM reps WITH (nolock) WHERE type=1 AND iso_id IN "+
						"(SELECT rep_id FROM reps WITH (nolock) WHERE type=5 AND iso_id IN "+
						"(SELECT rep_id FROM reps WITH (nolock) WHERE type=4 " +
						"AND rep_id=" + this.rep_id +
						" AND rep_id IN "+
						"( "+
						"SELECT DISTINCT r2.rep_id FROM reps AS r WITH(nolock) "+
						"INNER JOIN dbo.reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id "+
						"INNER JOIN reps AS r2 WITH (nolock) ON r1.iso_id = r2.rep_id "+
						"INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id "+
						"INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id "+
						"INNER JOIN Repcarriers ta WITH (NOLOCK) on ta.carrierid = mc.carrierid "+
						"INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
						"WHERE ta.repid =" + strRefId +
						")))";
			        strSearchSQL.append(" wt.rep_id in (" +strRepsSelect+")");

			     }
		    	 else if (strAccessLevel.equals(DebisysConstants.ISO))
			     {
			        strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
			             "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in " +
			              "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and rep_id=" + this.rep_id + " and iso_id = " + strRefId + "))";
			         strSearchSQL.append(" wt.rep_id IN (" + strRepsSelect + ") ");
			     }else if (strAccessLevel.equals(DebisysConstants.AGENT))
			     {
			        strSearchSQL.append(" wt.rep_id IN ");
			        strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in ");
			        strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = " + strRefId + ")) ");
			     }

		        break;
		        //subagents_transactions.jsp
		      case 6:
		    	  if (strAccessLevel.equals(DebisysConstants.CARRIER))
				     {
		    		  strRepsSelect="select rep_id from reps with (nolock) where type=1 and iso_id in "+
			    			  "(select rep_id from reps with (nolock) where type=5 " +
			    			  "and rep_id=" + this.rep_id +
			    			  " and rep_id IN "+
			    			  "( "+
							  "SELECT DISTINCT R1.rep_id from reps AS r with(nolock) "+
							  "INNER JOIN dbo.reps AS r1 WITH (nolock) ON r.iso_id = r1.rep_id "+
							  "INNER JOIN merchants WITH (NOLOCK)  ON MERCHANTS.rep_id = r.rep_id "+
							  "INNER JOIN Merchantcarriers mc WITH (NOLOCK) ON mc.merchantid =merchants.merchant_id "+
							  "INNER JOIN Repcarriers ta WITH (NOLOCK)   on ta.carrierid = mc.carrierid "+
							  "INNER JOIN Products p WITH (NOLOCK) on wt.id = p.id AND mc.carrierid = p.carrier_id "+
							  "WHERE ta.repid =" + strRefId +
							  "))";
				        strSearchSQL.append(" wt.rep_id in (" +strRepsSelect+")");

				   }else if (strAccessLevel.equals(DebisysConstants.ISO))
			        {
			        	strRepsSelect="select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in " +
			              "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_SUBAGENT + " and rep_id=" + this.rep_id + " AND iso_id in " +
			              "(select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_AGENT + " and iso_id=" + strRefId + "))";
			          strSearchSQL.append(" wt.rep_id in (" +strRepsSelect+") ");
			        }
			        else if (strAccessLevel.equals(DebisysConstants.AGENT))
			        {
			          strSearchSQL.append(" wt.rep_id in ");
			          strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in ");
			          strSearchSQL.append(" (select rep_id from reps with (nolock) where type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND rep_id=" + this.rep_id + "and iso_id=" + strRefId + "))");

			        }
		        else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		        {
		          strSearchSQL.append(" wt.rep_id in ");
		          strSearchSQL.append(" (select rep_id from reps with (nolock) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id=" + strRefId + ")");

		        }
		        break;
		      case 7:
		        if (strAccessLevel.equals(DebisysConstants.CARRIER))
		        {
		        	strRepsSelect="select rep_id from reps with (nolock) where type in  (2,3)";
		        	strSearchSQL.append(" wt.rep_id in (" +strRepsSelect+") ");
		        }
		        break;
		      default:
		        cat.error("Error during search");
		        throw new TransactionException();
		    }

		    
			if (this.allMerchantsSelected)
			{
				User currentUser = this.sessionData.getUser();
				strSearchSQL.append(ReportsUtil.getChainToBuildWhereSQL(currentUser.getAccessLevel(), currentUser.getDistChainType(), currentUser.getRefId(),
								"wt.rep_id"));
			}
			else
			{
				if ((this.merchant_ids != null) && (!this.merchant_ids.equals("")))
				{
					if (this.merchant_ids.indexOf(",") != -1)
					{
						strSearchSQL.append(" AND wt.merchant_id IN ( " + this.merchant_ids+") ");
					}
					else
					{
						strSearchSQL.append(" AND wt.merchant_id =" + this.merchant_ids);
					}
				}
			}

			if ((this.transactionID != null) && (!this.transactionID.equals("")))
			{
				strSearchSQL.append(" AND wt.rec_id = " + this.transactionID + " ");
			}

			if ((this.invoiceNumber != null) && (!this.invoiceNumber.equals("")))
			{
				strSearchSQL.append(" AND wt.invoice_no = " + this.invoiceNumber + " ");
			}

			String strExtraFields = "";
	  	    String strExtraJoin = "";
		    if((sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
					&& strAccessLevel.equals(DebisysConstants.ISO)
					&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					|| this.PINNumber != null && !this.PINNumber.equals(""))
		    {

		    	if(sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
				&& strAccessLevel.equals(DebisysConstants.ISO)
				&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
		    		strExtraFields = ", t.pin ";
				}
		    	strExtraJoin = " inner join transactions t with (nolock) on (wt.rec_id = t.rec_id) ";

			    if (this.PINNumber != null && !this.PINNumber.equals("") )
				{
					skusWithEncryption = getSkusWithEncryptionWrapper();
					strSearchSQL.append(" AND wt.product_trans_type = 2 ");
					strSearchSQL.append(" AND wt.amount > 0 ");
					String pin = this.PINNumber;
					String sku = getSkuFromPinsWrapper(pin);
					if (skusWithEncryption.contains(sku)) {
						pin = dataProtection().t0(pin);
					}
					strSearchSQL.append(" AND t.pin = '" + pin + "' ");
				}
		    }


		    if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
	  			&& strAccessLevel.equals(DebisysConstants.ISO)
	  			&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
		    	strExtraFields += ", dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", ach_t.Process_Date, 1) as ach_datetime ";
		    	strExtraJoin += " left outer join ach_transaction_Audit ach_t with (nolock) on (wt.rec_id = ach_t.rec_id and wt.merchant_id = ach_t.entity_id)";
			}

		    
	  		strExtraJoin += " inner join terminals with (nolock) on wt.millennium_no = terminals.millennium_no";
			strExtraJoin += " inner join terminal_types with (nolock) on terminals.terminal_type = terminal_types.terminal_type " + strAdditionalTablesCarrier;
                        strExtraJoin += " LEFT JOIN rateplan rp WITH (NOLOCK) ON terminals.Rateplanid = rp.RatePlanID ";
                        strExtraJoin += " LEFT JOIN wholeTaxProduct wtp with(nolock) ON (wtp.product_id = wt.id) ";
			strExtraJoin += " LEFT JOIN TransactionFinancialCharges tx  with(nolock) ON (wt.rec_id = tx.transactionId) ";
                        
			if ( viewReferenceCard )
			{	
				/*
					The case statement below is equivalent to the following Java code
					if ( info1!="null" && info1.startsWith("s") && info1.indexOf("=")!=-1 && !info2.equals(ani) && ani.length()>0 )
					{
						info = info1.substring(1);
					}
					else
					{
						info ="";
					}
				*/
				strSQL.append(", CASE htr.info1 WHEN NULL THEN '' ELSE CASE WHEN (CHARINDEX('s', htr.info1) > 0) and (CHARINDEX('=', htr.info1) > 0) and (htr.info2 != wt.ani) and (LEN(WT.ani) > 0)");
				strSQL.append(" THEN RIGHT(htr.info1, (LEN(htr.info1) - 1)) ELSE '' END END info");
				viewReferenceCardJOIN=" LEFT OUTER JOIN hostTransactionsrecord htr with(nolock) on htr.transaction_id = wT.rec_id ";
				strExtraJoin = strExtraJoin + viewReferenceCardJOIN;
			}
			
			strSQL.append(strExtraFields);
			
			if (this.isScheduling())
			{
				if (qType == QUERY_TYPE.FIXED)
				{
					strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
					strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
				}
				else
				{
					strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
				}
			}

			// FROM : Here is the main FROM part of the clause
			strSQL.append(" from web_transactions wt with (nolock) inner join products p WITH (NOLOCK) ON p.id = wt.id " + strExtraJoin);
			
			// WHERE : Here starts the where part of the clause
		    strSQL.append(" where ");

		    if ((this.millennium_no != null) && ( !this.millennium_no.equals("") ))
		    {
		    	strSQL.append(" wt.millennium_no in (" + this.millennium_no + ") AND ");
		    }

		    // Filtering by productIds in the Where
		    if ((this.product_ids != null) && (!this.product_ids.equals("")))
		    {
		    	strSearchSQL.append(" AND wt.RecordedProductid IN (" + this.product_ids + ") ");
		    }

			
                    if(sessionData.checkPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS)){
                        if(s2kRateplans != null && !s2kRateplans.trim().isEmpty()){
                            strSQLWhere.append(" AND (rp.RatePlanID IN ("+s2kRateplans+")) ");
                        }
                    }
                    
                    if (DateUtil.isValid(this.start_date) && DateUtil.isValid(this.end_date))
			{
				if (this.isScheduling())
				{
					if (qType == QUERY_TYPE.RELATIVE)
					{
						strSQLWhere.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
					}
					else if (qType == QUERY_TYPE.FIXED)
					{
						strSQLWhere.append("         AND (wt.datetime  >= '" + vTimeZoneFilterDates.get(0) + "'");
						strSQLWhere.append("         AND wt.datetime  < '" + vTimeZoneFilterDates.get(1) + "')");
					}
				}
				else
				{
					// If not scheduling, the wild card is removed, and the
					// current report dates are used
					int i;
					while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
					{
						strSQLWhere.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
					}
					strSQLWhere.append("         AND (wt.datetime  >= '" + vTimeZoneFilterDates.get(0) + "' ");
					strSQLWhere.append("         AND wt.datetime  < '" + vTimeZoneFilterDates.get(1) + "') ");
				}
			} else{
                            throw new Exception("Missing selection start and end date control!!.");
                        }
		    
		    if ((sessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
	         		 && strAccessLevel.equals(DebisysConstants.ISO) && intSectionPage==1 )
	         		 || (strAccessLevel.equals(DebisysConstants.CARRIER) && intSectionPage==1 ) )
	        {
		    	strSQLWhere.append(" AND wt.billing_typeId <> 6 ");
	        }
		    strSQLWhere.append(" order by wt.rec_id");
			
			retValue = strSQL.toString() + strSearchSQL.toString() + strSQLWhere.toString();
		}
		catch (ReportException localReportException)
		{
			cat.error("TransactionsReport.getSqlString. Exception while generationg sql string : "
					+ localReportException.toString());
		}
		catch (Exception localException)
		{
			cat.error("TransactionsReport.getSqlString. Exception while generationg sql string : "
					+ localException.toString());
		}
		cat.debug(retValue);
		return retValue;
	}

	private void setSelectCommission(StringBuilder strSQL) {
		strSQL.append(" (wt.merchant_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) WHEN tx.transactionId IS NOT NULL THEN tx.sourceAmount WHEN wtp.tax IS NOT NULL THEN wt.amount ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.trueamount - p.Fixed_fee ELSE wt.trueamount END)) END) AS merchant_commission, ");
		strSQL.append(" (wt.rep_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) WHEN tx.transactionId IS NOT NULL THEN tx.sourceAmount WHEN wtp.tax IS NOT NULL THEN wt.amount ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.trueamount - p.Fixed_fee ELSE wt.trueamount END)) END) AS rep_commission, ");
		strSQL.append(" (wt.agent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) WHEN tx.transactionId IS NOT NULL THEN tx.sourceAmount WHEN wtp.tax IS NOT NULL THEN wt.amount ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.trueamount - p.Fixed_fee ELSE wt.trueamount END)) END) AS agent_commission, ");
		strSQL.append(" (wt.subagent_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) WHEN tx.transactionId IS NOT NULL THEN tx.sourceAmount WHEN wtp.tax IS NOT NULL THEN wt.amount ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.trueamount - p.Fixed_fee ELSE wt.trueamount END)) END) AS subagent_commission, ");
		strSQL.append(" (wt.iso_rate / 100) * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) WHEN tx.transactionId IS NOT NULL THEN tx.sourceAmount WHEN wtp.tax IS NOT NULL THEN wt.amount ELSE ((CASE p.isRegulatoryFee WHEN 1 THEN wt.trueamount - p.Fixed_fee ELSE wt.trueamount END)) END) AS iso_commission, ");
	}
        
        private void addPromoData(StringBuilder strSQL) {
            boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(sessionData, context);
            if (isIntlAndHasDataPromoPermission) strSQL.append(" , wt.additionalData ");
        }

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ISupportSiteReport#getReportRecordHeaders()
	 */
	@Override
	public ArrayList<ColumnReport> getReportRecordHeaders()
	{
		ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();

		try
		{
			String strAccessLevel = sessionData.getProperty("access_level");
			String strDistChainType = sessionData.getProperty("dist_chain_type");

			//[0]
			retValue.add(new ColumnReport("rec_id", Languages.getString("jsp.admin.reports.tran_no",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			//[1]
			retValue.add(new ColumnReport("millennium_no", Languages.getString("jsp.admin.reports.term_no",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			//[2]
			retValue.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.dba", sessionData.getLanguage())
					.toUpperCase(), String.class, false));
			//[3]
			retValue.add(new ColumnReport("merchant_id", Languages.getString("jsp.admin.reports.merchant_id",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			//[4]
			retValue.add(new ColumnReport("datetime", Languages.getString("jsp.admin.reports.date", sessionData.getLanguage())
					.toUpperCase(), String.class, false));
			//[5]
			retValue.add(new ColumnReport("phys_city", Languages.getString("jsp.admin.reports.city", sessionData.getLanguage())
					.toUpperCase(), String.class, false));
			//[6]
			retValue.add(new ColumnReport("phys_county", Languages
					.getString("jsp.admin.reports.county", sessionData.getLanguage()).toUpperCase(), String.class,
					false));
			//[7]
			retValue.add(new ColumnReport("clerk_name", Languages.getString("jsp.admin.reports.clerk", sessionData.getLanguage())
					.toUpperCase(), String.class, false));
			//[8]
			retValue.add(new ColumnReport("ani", Languages.getString("jsp.admin.reports.reference",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			//[9] Ref No
			retValue.add(new ColumnReport("password", Languages
					.getString("jsp.admin.reports.ref_no", sessionData.getLanguage()).toUpperCase(), String.class,
					false));
			//[10] Recharge Value
			retValue.add(new ColumnReport("amount", Languages.getString("jsp.admin.reports.recharge",
					sessionData.getLanguage()).toUpperCase(), String.class, false));

			if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT))
			{
				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
					{
						//[11]
						retValue.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent",
								sessionData.getLanguage()).toUpperCase(), String.class, false));
						//[12]
						retValue.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent",
								sessionData.getLanguage()).toUpperCase(), String.class, false));
						//[13]
						retValue.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent", sessionData
								.getLanguage()), String.class, false));
					}
					else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						//[11]
						retValue.add(new ColumnReport("iso_commission", Languages.getString("jsp.admin.iso_percent",
								sessionData.getLanguage()).toUpperCase(), String.class, false));
						//[12]
						retValue.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent",
								sessionData.getLanguage()).toUpperCase(), String.class, false));
						//[13]
						retValue.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent",
								sessionData.getLanguage()).toUpperCase(), String.class, false));
						//[14]
						retValue.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent",
								sessionData.getLanguage()).toUpperCase(), String.class, false));
						//[15]
						retValue.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent",
								sessionData.getLanguage()).toUpperCase(), String.class, false));
					}
				}
				else if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					//[11]
					retValue.add(new ColumnReport("agent_commission", Languages.getString("jsp.admin.agent_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					//[12]
					retValue.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					//[13]
					retValue.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					//[14]
					retValue.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
				}
				else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
				{
					//[11]
					retValue.add(new ColumnReport("subagent_commission", Languages.getString("jsp.admin.subagent_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					//[12]
					retValue.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					//[13]
					retValue.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
				}
				else if (strAccessLevel.equals(DebisysConstants.REP))
				{
					//[11]
					retValue.add(new ColumnReport("rep_commission", Languages.getString("jsp.admin.rep_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					//[12]
					retValue.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
				}
				else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
				{
					//[11]
					retValue.add(new ColumnReport("merchant_commission", Languages.getString("jsp.admin.merchant_percent",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
				}

			}	

			if (DebisysConfigListener.getDeploymentType(this.context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
					&& DebisysConfigListener.getCustomConfigType(this.context).equals(
							DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
			{

				retValue.add(new ColumnReport("bonus_amount", Languages.getString("jsp.admin.reports.bonus",
						sessionData.getLanguage()).toUpperCase(), String.class, false));
				retValue.add(new ColumnReport("total_recharge", Languages.getString("jsp.admin.reports.total_recharge",
						sessionData.getLanguage()).toUpperCase(), String.class, false));

				if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
				{
					retValue.add(new ColumnReport("netamount", Languages.getString("jsp.admin.reports.netAmount",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					retValue.add(new ColumnReport("taxamount", Languages.getString("jsp.admin.reports.taxAmount",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					retValue.add(new ColumnReport("taxpercentage", Languages.getString("jsp.admin.reports.taxpercentage",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
					retValue.add(new ColumnReport("taxtype", Languages.getString("jsp.admin.reports.taxtype",
							sessionData.getLanguage()).toUpperCase(), String.class, false));
				}
			}

			retValue.add(new ColumnReport("balance", Languages.getString("jsp.admin.reports.balance",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("id", Languages.getString("jsp.admin.product", sessionData.getLanguage())
					.toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("description", Languages.getString("jsp.admin.description", sessionData.getLanguage())
					.toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("control_no", Languages.getString("jsp.admin.reports.control_no",
					sessionData.getLanguage()).toUpperCase(), String.class, false));

			if (sessionData
					.checkPermission(DebisysConstants.PERM_VIEW_AUTHORIZATION_CARRIER_NUMBER_IN_TRANSACTIONS_REPORT))
			{ // Authorization Carrier Number

				retValue.add(new ColumnReport("other_info", Languages.getString("jsp.admin.reports.authorization_carrier_number",
						sessionData.getLanguage()).toUpperCase(), String.class, false));

			}
			if (sessionData.checkPermission(DebisysConstants.PERM_TRANSACTION_REPORT_SHOW_ADDITIONALDATA))
			{

				retValue.add(new ColumnReport("additionalData", Languages.getString("jsp.admin.reports.additionalData",
						sessionData.getLanguage()).toUpperCase(), String.class, false));

			}
			else
			{
				retValue.add(new ColumnReport("trm_description", Languages.getString(
						"jsp.admin.reports.transactions.terminals_summary.terminal_type", sessionData.getLanguage())
						.toUpperCase(), String.class, false));
			}

			if (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
					&& strAccessLevel.equals(DebisysConstants.ISO)
					&& DebisysConfigListener.getDeploymentType(this.context).equals(
							DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
				retValue.add(new ColumnReport("pin", Languages.getString("jsp.admin.reports.pin_number",
						sessionData.getLanguage()).toUpperCase(), String.class, false));
			}
			if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
					&& strAccessLevel.equals(DebisysConstants.ISO)
					&& DebisysConfigListener.getDeploymentType(this.context).equals(
							DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
				retValue.add(new ColumnReport("ach_datetime", Languages.getString("jsp.admin.reports.ach_date",
						sessionData.getLanguage()).toUpperCase(), String.class, false));
			}
			retValue.add(new ColumnReport("transaction_type", Languages.getString("jsp.admin.reports.transaction_type",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			if (com.debisys.users.User.isInvoiceNumberEnabled(sessionData, this.context)
					&& DebisysConfigListener.getDeploymentType(this.context).equals(
							DebisysConstants.DEPLOYMENT_INTERNATIONAL)
					&& DebisysConfigListener.getCustomConfigType(this.context).equals(
							DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
			{
				retValue.add(new ColumnReport("invoice_no", Languages.getString("jsp.admin.reports.invoiceno",
						sessionData.getLanguage()).toUpperCase(), String.class, false));
			}

			if (viewReferenceCard)
			{
				retValue.add(new ColumnReport("info", Languages.getString("jsp.admin.report.transactions.DTU1204",
						sessionData.getLanguage()).toUpperCase(), String.class, false));
			}
		}
		catch (Exception localException)
		{
			cat.error("TransactionsReport.getReportRecordHeaders Exception : " + localException);
		}

		return retValue;
	}

    
    /**
     * 
     * @return 
     */    
    public Vector<Vector<Object>> getTransactionResults() {

        Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String strAccessLevel = sessionData.getProperty("access_level");

            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            this.setScheduling(false); // If returning results, then scheduling
            // does not have sense
            String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
            pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = pstmt.executeQuery();
            int intRecordCount = DbUtil.getRecordCount(rs);
            rs.beforeFirst();
            Vector<Object> recordCountVec = new Vector<Object>();
            recordCountVec.add(intRecordCount);
            retValue.add(recordCountVec);

            boolean canFilterS2KRateplans = sessionData.checkPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS);

            if (intRecordCount > 0) {

                Hashtable<Integer, String> ht_taxTypes = TaxTypes.getTaxTypes();

                while (rs.next()) {
                    
                    Vector<Object> vecTemp = new Vector<Object>();
                    int fieldIndex = 0;
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("rec_id"))); 		//[0]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("millennium_no")));	//[1]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("dba")));			//[2]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("merchant_id")));	//[3]
                    vecTemp.add(fieldIndex++, DateUtil.formatDateTime(rs.getTimestamp("datetime")));    //[4]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("phys_city")));		//[5]
                    
                    if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals("")) {
                        vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("clerk_name")));	//[6]
                    } else if (rs.getString("clerk") != null && rs.getString("clerk").trim().equalsIgnoreCase("RCH")) {
                        vecTemp.add(fieldIndex++, "(Recharge)");									//[6]
                    } else {
                        vecTemp.add(fieldIndex++, "N/A");											//[6]
                    }
                    
                    String ani = StringUtil.toString(rs.getString("ani"));	// ani	 					//[7]

                    String isQRCode = rs.getString("isQRCode");
                    if (isQRCode == null || (isQRCode != null && isQRCode.equals("0"))) {
                        vecTemp.add(fieldIndex++, ani);
                    } else if (isQRCode != null && isQRCode.equals("1") && sessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS)) {
                        vecTemp.add(fieldIndex++, ani);
                    } else {
                        vecTemp.add(fieldIndex++, StringUtil.hideXNumberDigits(ani));
                    }
                    
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("password")));	// password	//[8]
                    vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("amount")));		//[9]
                    vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("balance")));	 // balance //[10]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("id")));	// product id/description    //[11] ** Previously this field had fields 11 and 12
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("description"))); 	//[12]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("control_no")));		//[13]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("product_trans_type")));  // trans_type for view pin functionality //[14]
                    // retrieve the type description 2, and check if it is null or not
                    
                    if ((rs.getString("transaction_type") != null) && (!rs.getString("transaction_type").equals(""))) {
                        vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("transaction_type")));//[15]
                    } else {
                        vecTemp.add(fieldIndex++, ""); //[15]
                    }
                    
                    String other_info = StringUtil.toString(rs.getString("other_info"));
                    if (other_info.trim() != "") {
                        if (other_info.length() > 6) {
                            other_info = other_info.substring((other_info.length() - 6), other_info.length());
                        }
                    }
                    
                    vecTemp.add(fieldIndex++, other_info); 											//[16]
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("phys_state")));
                    vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("bonus_amount")));//[17]
                    vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("total_recharge")));//[18]

                    if (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO)
                            && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                        vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("pin"))); //[19]
                    } else {
                        vecTemp.add(fieldIndex++, "");										//[19]
                    }

                    if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && strAccessLevel.equals(DebisysConstants.ISO)
                            && deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                        vecTemp.add(fieldIndex++, (rs.getTimestamp("ach_datetime") != null && !"".equals(rs.getTimestamp("ach_datetime"))) ? DateUtil
                                .formatDateTime(rs.getTimestamp("ach_datetime")) : "TBD");	//[20]
                    } else {
                        vecTemp.add(fieldIndex++, "");										//[20]
                    }

                    vecTemp.add(fieldIndex++, rs.getString("trm_description"));
                    
                    if (showAdditionalData && intSectionPage == 1) { // only for transactions.jsp
                        vecTemp.add(fieldIndex++, rs.getString("additionalData"));			//[21]
                    } else {
                        vecTemp.add(fieldIndex++, "");										//[21]
                    }
                    
                    vecTemp.add(fieldIndex++, rs.getString("invoice_no"));
                    
                    if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
                            && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                            && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                        if (rs.getString("taxpercentage") != null) {
                            String taxType = "";
                            if (rs.getString("taxtype") != null) {
                                taxType = ht_taxTypes.get(rs.getInt("taxtype"));
                            }
                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("netamount")));	//[22]
                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("taxamount")));	//[23]
                            vecTemp.add(fieldIndex++, NumberUtil.formatPercentage(rs.getString("taxpercentage")));//[24]
                            vecTemp.add(fieldIndex++, taxType);	//[25]
                        } else {
                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("netamount"))); 	//[22]
                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("taxamount")));	//[23]
                            vecTemp.add(fieldIndex++, NumberUtil.formatPercentage(String.valueOf((dTax - 1) * 100)));//[24]
                            vecTemp.add(fieldIndex++, "Local");	//[25]
                        }
                    } else {
                        vecTemp.add(fieldIndex++, "");	//[22]
                        vecTemp.add(fieldIndex++, "");	//[23]
                        vecTemp.add(fieldIndex++, "");	//[24]
                        vecTemp.add(fieldIndex++, "");	//[25]
                    }
                    
                    vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("phys_county")));	//[26]

                    if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT)) {

                        vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("agent_commission")));		//[27]
                        vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("subagent_commission")));	//[28]
                        vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("iso_commission")));			//[29]
                        vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("merchant_commission")));	//[30]
                        vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("rep_commission")));			//[31]
                    
                    } else {

                        vecTemp.add(fieldIndex++, "");	//[27]
                        vecTemp.add(fieldIndex++, "");	//[28]
                        vecTemp.add(fieldIndex++, "");	//[29]
                        vecTemp.add(fieldIndex++, "");	//[30]
                        vecTemp.add(fieldIndex++, "");	//[31]
                    }
                    
                    if (viewReferenceCard) {
                        vecTemp.add(fieldIndex++, rs.getString("info"));	//[32]
                    } else {
                        vecTemp.add(fieldIndex++, "");	//[32]
                    }

                    if (isQRCode != null && isQRCode.equals("1") && sessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS)) {
                        vecTemp.add(fieldIndex++, "(QRCode)");
                    } else {
                        vecTemp.add(fieldIndex++, "");
                    }

                    if (canFilterS2KRateplans) {
                        vecTemp.add(fieldIndex++, rs.getString("rateplanName"));
                    } else {
                        vecTemp.add(fieldIndex++, "");
                    }
                    retValue.add(vecTemp);
                } 
            } // end if record count > 0
        } catch (TorqueException localTorqueException) {
            cat.error("TransactionsReport.getResults Exception : " + localTorqueException.toString());
        } catch (ReportException localReportException) {
            cat.error("TransactionsReport.getResults Exception : " + localReportException.toString());
        } catch (SQLException localSqlException) {
            cat.error("TransactionsReport.getResults Exception : " + localSqlException.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                    dbConn = null;
                }
            } catch (SQLException cleaningSqlException) {
                cat.error("TransactionsReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
            }
        }
        return retValue;
    }
        
        
        
        
    /* (non-Javadoc)
     * @see com.debisys.reports.schedule.ISupportSiteReport#getResults()
     */
    @Override
    public Vector<Vector<Object>> getResults() {
		Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
                boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(sessionData, context);

		try
		{
			String strAccessLevel = sessionData.getProperty("access_level");

			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			this.setScheduling(false); // If returning results, then scheduling
										// does not have sense
			String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
			pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pstmt.executeQuery();
			int intRecordCount = DbUtil.getRecordCount(rs);
			Vector<Object> recordCountVec = new Vector<Object>();
			recordCountVec.add(intRecordCount);
			retValue.add(recordCountVec);
                        
            boolean canFilterS2KRateplans = sessionData.checkPermission(DebisysConstants.PERM_VIEW_FILTER_S2K_RATEPLANS);

			if (intRecordCount > 0)
			{
				rs.absolute(DbUtil.getRowNumber(rs, this.intRecordsPerPage, intRecordCount, this.intPageNumber));

				Hashtable<Integer, String> ht_taxTypes = TaxTypes.getTaxTypes();

				for (int i = 0; i < intRecordsPerPage; i++)
				{
					Vector<Object> vecTemp = new Vector<Object>();
					int fieldIndex = 0;
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("rec_id"))); 		//[0]
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("millennium_no")));	//[1]
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("dba")));			//[2]
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("merchant_id")));	//[3]
					vecTemp.add(fieldIndex++, DateUtil.formatDateTime(rs.getTimestamp("datetime")));//[4]
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("phys_city")));		//[5]
					if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals(""))
					{
						vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("clerk_name")));	//[6]
					}
					else if (rs.getString("clerk") != null && rs.getString("clerk").trim().equalsIgnoreCase("RCH"))
					{
						vecTemp.add(fieldIndex++, "(Recharge)");									//[6]
					}
					else
					{
						vecTemp.add(fieldIndex++, "N/A");											//[6]
					}
					// ani
					String ani = StringUtil.toString(rs.getString("ani"));							//[7]
					
                    String isQRCode = rs.getString("isQRCode");
                    if(isQRCode == null || (isQRCode != null && isQRCode.equals("0"))){
                        vecTemp.add(fieldIndex++, ani);
                    }
                    else if (isQRCode != null && isQRCode.equals("1") && sessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS)){
                        vecTemp.add(fieldIndex++, ani);
                    }
                    else{
                        vecTemp.add(fieldIndex++, StringUtil.hideXNumberDigits(ani));
                    }
                                        
					// password
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("password")));		//[8]tx.sourceAmount
					if(rs.getString("trnNewTax")!= null){
                                            vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("trnNewTax")));		//[9]
                                        }
                                        else{
                                            vecTemp.add(fieldIndex++, NumberUtil.formatAmount((rs.getString("wholeTax")!= null) ? rs.getString("trueamount") : rs.getString("amount")));		//[9]
                                        }
                                        // balance
					vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("balance")));	//[10]
					// product id/description
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("id")));				//[11] ** Previously this field had fields 11 and 12
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("description"))); 	//[12]
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("control_no")));		//[13]
					// trans_type for view pin functionality
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("product_trans_type")));//[14]
					// retrieve the type description 2, and check if it is null
					// or not
					if ((rs.getString("transaction_type") != null) && (!rs.getString("transaction_type").equals("")))
					{
						vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("transaction_type")));//[15]
					}
					else
					{
						vecTemp.add(fieldIndex++, ""); //[15]
					}
					String other_info = StringUtil.toString(rs.getString("other_info"));
					if (other_info.trim() != "")
					{
						if (other_info.length() > 6)
						{
							other_info = other_info.substring((other_info.length() - 6), other_info.length());
						}
					}
					vecTemp.add(fieldIndex++, other_info); 											//[16]
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("phys_state")));
					vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("bonus_amount")));//[17]
					vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("total_recharge")));//[18]

					if (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO)
							&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
						vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("pin"))); //[19]
					}
					else
					{
						vecTemp.add(fieldIndex++, "");										//[19]
					}

					if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && strAccessLevel.equals(DebisysConstants.ISO)
							&& deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
					{
						vecTemp.add(fieldIndex++, (rs.getTimestamp("ach_datetime") != null && !"".equals(rs.getTimestamp("ach_datetime"))) ? DateUtil
								.formatDateTime(rs.getTimestamp("ach_datetime")) : "TBD");	//[20]
					}
					else
					{
						vecTemp.add(fieldIndex++, "");										//[20]
					}

					vecTemp.add(fieldIndex++, rs.getString("trm_description"));
					if (showAdditionalData && intSectionPage == 1)
					{ // only for transactions.jsp
						vecTemp.add(fieldIndex++, rs.getString("additionalData"));			//[21]
					}
					else
					{
						vecTemp.add(fieldIndex++, "");										//[21]
					}
					vecTemp.add(fieldIndex++, rs.getString("invoice_no"));
					if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
							&& DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
							&& DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
					{
						if (rs.getString("taxpercentage") != null)
						{
							String taxType = "";
							if (rs.getString("taxtype") != null)
							{
								taxType = ht_taxTypes.get(rs.getInt("taxtype"));
							}
                                                        if(rs.getString("wholeTax")!= null){
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getBigDecimal("netamountWhole").setScale(1, BigDecimal.ROUND_DOWN).toString()));	//[22]
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getBigDecimal("taxamountWhole").toString()));	//[23]
                                                        }
                                                        else{
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("netamount")));	//[22]
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("taxamount")));	//[23]
                                                        }
							
							vecTemp.add(fieldIndex++, NumberUtil.formatPercentage(rs.getString("taxpercentage")));//[24]
							vecTemp.add(fieldIndex++, taxType);	//[25]
						}
						else
						{
                                                        if(rs.getString("wholeTax")!= null){
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("netamountWhole"))); 	//[22]
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("taxamountWhole")));	//[23]
                                                        }
                                                        else{
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("netamount"))); 	//[22]
                                                            vecTemp.add(fieldIndex++, NumberUtil.formatCurrency(rs.getString("taxamount")));	//[23]
                                                        }
							
							vecTemp.add(fieldIndex++, NumberUtil.formatPercentage(String.valueOf((dTax - 1) * 100)));//[24]
							vecTemp.add(fieldIndex++, "Local");	//[25]
						}
					}
					else
					{
						vecTemp.add(fieldIndex++, "");	//[22]
						vecTemp.add(fieldIndex++, "");	//[23]
						vecTemp.add(fieldIndex++, "");	//[24]
						vecTemp.add(fieldIndex++, "");	//[25]
					}
					vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("phys_county")));	//[26]

					if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_COMMISSION_DETAILS_TRANSACTIONS_REPORT))
					{

						vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("agent_commission")));		//[27]
						vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("subagent_commission")));	//[28]
						vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("iso_commission")));			//[29]
						vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("merchant_commission")));	//[30]
						vecTemp.add(fieldIndex++, NumberUtil.formatAmount(rs.getString("rep_commission")));			//[31]
					}
					else
					{

						vecTemp.add(fieldIndex++, "");	//[27]
						vecTemp.add(fieldIndex++, "");	//[28]
						vecTemp.add(fieldIndex++, "");	//[29]
						vecTemp.add(fieldIndex++, "");	//[30]
						vecTemp.add(fieldIndex++, "");	//[31]
					}
					if (viewReferenceCard)
					{
						vecTemp.add(fieldIndex++, rs.getString("info"));	//[32]
					}
					else
					{
						vecTemp.add(fieldIndex++, "");	//[32]
					}
                                        
                    if (isQRCode != null && isQRCode.equals("1") && sessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS)){
                        vecTemp.add(fieldIndex++, "(QRCode)" );
                    }
                    else{
                        vecTemp.add(fieldIndex++, "");
                    }
                    
                    if(canFilterS2KRateplans){
                        vecTemp.add(fieldIndex++, rs.getString("rateplanName"));
                    }
                    else{
                        vecTemp.add(fieldIndex++, "");
                    }
                    
                    if (isIntlAndHasDataPromoPermission) {
                        vecTemp.add(fieldIndex++, StringUtil.toString(rs.getString("additionalData")));
                    } // Promo Data

					retValue.add(vecTemp);

					if (!rs.next())
					{
						break;
					}

				} // end for
			} // end if record count > 0
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("TransactionsReport.getResults Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("TransactionsReport.getResults Exception : " + localReportException.toString());
		}
		catch (SQLException localSqlException)
		{
			cat.error("TransactionsReport.getResults Exception : " + localSqlException.toString());
		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
					rs = null;
				}
				if (pstmt != null)
				{
					pstmt.close();
					pstmt = null;
				}
				if (dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (SQLException cleaningSqlException)
			{
				cat.error("TransactionsReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}
		return retValue;
	}

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ISupportSiteReport#getTitles()
	 */
	@Override
	public ArrayList<String> getTitles()
	{
		ArrayList<String> retValue = new ArrayList<String>();

		try
		{
			if((this.reportTitle == null) || ((this.reportTitle != null) && (this.reportTitle.equals(""))))
			{
				this.reportTitle = Languages.getString("jsp.admin.reports.transactions.transactions.title", this.sessionData.getLanguage()); 
			}
			retValue.add(this.reportTitle);
			Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
			String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
			retValue.add(noteTimeZone);
		}
		catch (NumberFormatException localNumberFormatException)
		{
			cat.error("getTitles Exception : " + localNumberFormatException.toString());
		}
		catch (Exception localException)
		{
			cat.error("getTitles Exception : " + localException.toString());
		}
		return retValue;

	}

	/* (non-Javadoc)
	 * @see com.debisys.reports.schedule.ISupportSiteReport#scheduleReport()
	 */
	@Override
	public boolean scheduleReport()
	{
		boolean retValue = false;
		
		try
		{
			this.setScheduling(true);
			String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
			String relativeQuery =  this.generateSqlString(QUERY_TYPE.RELATIVE);
			StringBuilder sbAdditionalData = new StringBuilder();
			
			ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_TRANSACTIONS , 1);
			scheduleReport.setNameDateTimeColumn("wt.datetime");	
			scheduleReport.setRelativeQuery( relativeQuery );
			cat.debug("Relative Query : " + relativeQuery);
			scheduleReport.setFixedQuery( fixedQuery );		
			cat.debug("Fixed Query : " + fixedQuery);
			
			
			if ((this.invoiceNumber != null) && (!this.invoiceNumber.equals("")))
			{
				sbAdditionalData.append(" ");
				sbAdditionalData.append(Languages.getString("jsp.admin.reports.invoiceno",sessionData.getLanguage()));
				sbAdditionalData.append(" " + this.invoiceNumber + " ");
			}

			if (this.allMerchantsSelected)
			{
				sbAdditionalData.append(" ");
				sbAdditionalData.append(Languages.getString("jsp.admin.report.transactions.products.merchants.eleven",sessionData.getLanguage()));
			}
			
			scheduleReport.setAdditionalData(sbAdditionalData.toString());
			TransactionReportScheduleHelper.addMetaDataReport( this.getReportRecordHeaders(), scheduleReport, this.getTitles() );				
			this.sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("TransactionsReport.scheduleReport Exception : " + localException.toString());
		}								

		return retValue;
	}


}
