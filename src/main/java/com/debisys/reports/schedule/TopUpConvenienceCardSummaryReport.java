/**
 * 
 */
package com.debisys.reports.schedule;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.debisys.customers.UserPayments;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.schedule.ISupportSiteReport.QUERY_TYPE;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

/**
 * @author fernandob
 * 
 */
public class TopUpConvenienceCardSummaryReport extends ReportDownloader implements ISupportSiteReport
{
	
	
	// attributes
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private String deployment;
	private boolean taxEnabled;
	private String start_date;
	private String end_date;
	private String convenienceCardCarrier_ids;
	private String strCurrentDate; // Formatted date when the report was generated
	private String selectedMerchantIDs;
	private SessionData sessionData;
	private boolean scheduling;
	private boolean permissionInterCustomConfig;
	private boolean allMerchantsSelected;
	private boolean allCarriersSelected;
	private boolean UsingDateLimitInterval;
	private int limitDays;
	
	// attributes related with report totals
	private int intTotalAssocSum;
	private int intTotalSwipeSum;
	private double dblTotalRechargeSum;
	private double dblTotalRechargeMinusVatSum;
	private double dblTotalTaxSum;
	
	
	// properties
	
	/**
	 * @return the limitDays
	 */
	public int getLimitDays()
	{
		return limitDays;
	}

	/**
	 * @param limitDays the limitDays to set
	 */
	public void setLimitDays(int limitDays)
	{
		this.limitDays = limitDays;
	}

	/**
	 * @return the usingDateLimitInterval
	 */
	public boolean isUsingDateLimitInterval()
	{
		return UsingDateLimitInterval;
	}

	/**
	 * @param usingDateLimitInterval the usingDateLimitInterval to set
	 */
	public void setUsingDateLimitInterval(boolean usingDateLimitInterval)
	{
		UsingDateLimitInterval = usingDateLimitInterval;
	}


	/**
	 * @return the deployment
	 */
	public String getDeployment()
	{
		return deployment;
	}

	/**
	 * @param deployment
	 *            the deployment to set
	 */
	public void setDeployment(String deployment)
	{
		this.deployment = deployment;
	}

	/**
	 * @return the taxEnabled
	 */
	public boolean isTaxEnabled()
	{
		return taxEnabled;
	}

	/**
	 * @param taxEnabled
	 *            the taxEnabled to set
	 */
	public void setTaxEnabled(boolean taxEnabled)
	{
		this.taxEnabled = taxEnabled;
	}

	/**
	 * @return the start_date
	 */
	public String getStart_date()
	{
		return start_date;
	}

	/**
	 * @param startDate the start_date to set
	 */
	public void setStart_date(String startDate)
	{
		start_date = startDate;
	}

	/**
	 * @return the end_date
	 */
	public String getEnd_date()
	{
		return end_date;
	}

	/**
	 * @param endDate the end_date to set
	 */
	public void setEnd_date(String endDate)
	{
		end_date = endDate;
	}


	/**
	 * @return the convenienceCardCarrier_ids
	 */
	public String getConvenienceCardCarrier_ids()
	{
		return convenienceCardCarrier_ids;
	}

	/**
	 * 
	 * @param convenienceCardCarrierIds the convenienceCardCarrier_ids to set
	 */
	public void setConvenienceCardCarrier_ids(String convenienceCardCarrierIds)
	{
		convenienceCardCarrier_ids = convenienceCardCarrierIds;
	}

	/**
	 * @return the strCurrentDate
	 */
	public String getStrCurrentDate()
	{
		return strCurrentDate;
	}

	/**
	 * @param strCurrentDate the strCurrentDate to set
	 */
	public void setStrCurrentDate(String strCurrentDate)
	{
		this.strCurrentDate = strCurrentDate;
	}

	/**
	 * @return the selectedMerchantIDs
	 */
	public String getSelectedMerchantIDs()
	{
		return selectedMerchantIDs;
	}

	/**
	 * @param selectedMerchantIDs the selectedMerchantIDs to set
	 */
	public void setSelectedMerchantIDs(String selectedMerchantIDs)
	{
		this.selectedMerchantIDs = selectedMerchantIDs;
	}
	
	/**
	 * @return the sessionData
	 */
	public SessionData getSessionData()
	{
		return sessionData;
	}

	/**
	 * @param sessionData the sessionData to set
	 */
	public void setSessionData(SessionData sessionData)
	{
		this.sessionData = sessionData;
	}

	/**
	 * @return the scheduling
	 */
	public boolean isScheduling()
	{
		return scheduling;
	}

	/**
	 * @param shceduling the scheduling to set
	 */
	public void setScheduling(boolean shceduling)
	{
		this.scheduling = shceduling;
	}

	/**
	 * @return the permissionInterCustomConfig
	 */
	public boolean isPermissionInterCustomConfig()
	{
		return permissionInterCustomConfig;
	}

	/**
	 * @param permissionInterCustomConfig the permissionInterCustomConfig to set
	 */
	public void setPermissionInterCustomConfig(boolean permissionInterCustomConfig)
	{
		this.permissionInterCustomConfig = permissionInterCustomConfig;
	}
	
	/**
	 * @return the allMerchantsSelected
	 */
	public boolean isAllMerchantsSelected()
	{
		return allMerchantsSelected;
	}

	/**
	 * @param allMerchantsSelected the allMerchantsSelected to set
	 */
	public void setAllMerchantsSelected(boolean allMerchantsSelected)
	{
		this.allMerchantsSelected = allMerchantsSelected;
	}

	/**
	 * @return the allCarriersSelected
	 */
	public boolean isAllCarriersSelected()
	{
		return allCarriersSelected;
	}

	/**
	 * @param allCarriersSelected the allCarriersSelected to set
	 */
	public void setAllCarriersSelected(boolean allCarriersSelected)
	{
		this.allCarriersSelected = allCarriersSelected;
	}

	// methods
	public TopUpConvenienceCardSummaryReport()
	{
		super();
		this.deployment = null;
		this.taxEnabled = false;
		this.start_date = null;
		this.end_date = null;
		this.convenienceCardCarrier_ids = null;
		this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
		this.selectedMerchantIDs = null;
		this.sessionData = null;
		this.context = null;
		this.scheduling = false;
		this.permissionInterCustomConfig = false;
		// attributes related with report totals
		this.intTotalAssocSum = 0;
		this.intTotalSwipeSum = 0;
		this.dblTotalRechargeSum = 0.0;
		this.dblTotalRechargeMinusVatSum = 0.0;
		this.dblTotalTaxSum = 0.0;
		this.allCarriersSelected = false;
		this.allMerchantsSelected = false;
		this.UsingDateLimitInterval = false;
		this.limitDays = 0;
	}
	
	
	/**
	 * @param context 						ServletContext
	 * @param deployment 					Where the report is running
	 * @param taxEnabled					Permission
	 * @param startDate						From what date the report data should be returned
	 * @param endDate						To what date the report data should be returned
	 * @param productIds					
	 * @param convenienceCardCarrierIds		Carrier ID's from the selected list on the form
	 * @param taxCalculationEnabled			Permission
	 * @param strCurrentDate				In what date this report is running
	 * @param selectedMerchantIDs			Merchant ID's list from those selected in the form
	 * @param sessionData					JSP session data object
	 * @param context2
	 * @param shceduling
	 * @param permissionInterCustomConfig
	 */
	public TopUpConvenienceCardSummaryReport(ServletContext pContext, String deployment, boolean taxEnabled,
			String startDate, String endDate, String convenienceCardCarrierIds,
			String strCurrentDate, String selectedMerchantIDs, SessionData sessionData,
			boolean shceduling, boolean permissionInterCustomConfig, boolean allCarrSel, boolean allMercSel)
	{
		this();
		this.setContext(pContext);
		this.deployment = deployment;
		this.taxEnabled = taxEnabled;
		this.start_date = startDate;
		this.end_date = endDate;
		this.convenienceCardCarrier_ids = convenienceCardCarrierIds;
		this.strCurrentDate = strCurrentDate;
		this.selectedMerchantIDs = selectedMerchantIDs;
		this.sessionData = sessionData;
                this.strRefId = sessionData.getProperty("ref_id");
		this.scheduling = shceduling;
		this.permissionInterCustomConfig = permissionInterCustomConfig;
		this.allCarriersSelected = allCarrSel;
		this.allMerchantsSelected = allMercSel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileFooter()
	 */
	@Override
	protected boolean writeDownloadFileFooter() throws IOException
	{
		boolean retValue = false;
		try
		{
			this.writetofile(fc, Languages.getString("jsp.admin.reports.totals", this.sessionData.getLanguage()));
			this.writetofile(fc, ", ");
			this.writetofile(fc, String.valueOf(this.intTotalAssocSum));
			this.writetofile(fc, ", ");
			this.writetofile(fc, String.valueOf(this.intTotalSwipeSum));
			this.writetofile(fc, ", ");
			this.writetofile(fc, NumberUtil.formatCurrency(Double.toString(this.dblTotalRechargeSum)));
			if ( this.isTaxEnabled() || this.isPermissionInterCustomConfig()) 
			{
				this.writetofile(fc, ", ");
				this.writetofile(fc, NumberUtil.formatCurrency(Double.toString(this.dblTotalRechargeMinusVatSum)));
				this.writetofile(fc, ", ");
				this.writetofile(fc, NumberUtil.formatCurrency(Double.toString(dblTotalTaxSum)));
			}
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadFileFooter Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileHeader()
	 */
	@Override
	protected boolean writeDownloadFileHeader() throws IOException
	{
		boolean retValue = false;
		try
		{
			this.writetofile(this.fc, "\"" + Languages.getString("jsp.admin.reports.convenienceCard.carrier_summary.title", sessionData.getLanguage()) + "\"\n");
			this.writetofile(fc, "\""
					+ Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
					+ sessionData.getUser().getUsername() + "\n");
			this.writetofile(fc, "\""
					+ Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage())
					+ "\"," + this.getStrCurrentDate() + "\n");
			this.writetofile(fc, "\n");
			this.writetofile(fc, "\""
					+ Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage())
					+ "\"\n");
			if (sessionData.getProperty("repName") != null)
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
						+ "\"," + sessionData.getProperty("repName") + "\n");
			}
			else
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
						+ "\"," + sessionData.getUser().getCompanyName() + "\n");
			}

			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage())
					+ "\"," + this.start_date + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage())
					+ "\"," + this.end_date + "\n");
			
			
			if ((this.convenienceCardCarrier_ids != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.convenienceCard.SelectedCarriers", sessionData.getLanguage()) + "\",");
				if (this.allCarriersSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.convenienceCardCarrier_ids);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}


			if ((this.selectedMerchantIDs != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.convenienceCard.SelectedMerchants", sessionData.getLanguage()) + "\",");
				if (this.allCarriersSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.selectedMerchantIDs);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadFileHeader Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecords()
	 */
	@Override
	protected boolean writeDownloadRecords() throws IOException
	{
		boolean retValue = false;
		Vector<Vector<Object>> reportResults = this.getResults();
		double dblTotalTax=0;
		double dblTotalRechargeMinusVat=0;
		int intTotalAssoc = 0;
		int intTotalSwipe = 0;
		double dblTotalRecharge = 0.0;

		// Summary values that will be used by the footer writer 
		this.dblTotalRechargeSum = 0;
		this.dblTotalRechargeMinusVatSum = 0;
		this.dblTotalTaxSum = 0;
		this.intTotalAssocSum = 0;
		this.intTotalSwipeSum = 0;

		
		try
		{
			if((reportResults != null) && (reportResults.size() > 0))
			{
				for(int i = 0;i < reportResults.size();i++)
				{
					Vector<Object> currentRecord = reportResults.get(i);
					intTotalAssoc = Integer.parseInt(currentRecord.get(1).toString());
					intTotalSwipe = Integer.parseInt(currentRecord.get(2).toString());
					dblTotalRecharge = Double.parseDouble(currentRecord.get(3).toString());
			        if ( this.isTaxEnabled() || this.isPermissionInterCustomConfig()) 
			        {
			        	dblTotalRechargeMinusVat = Double.parseDouble(currentRecord.get(4).toString()); 
			        	this.dblTotalRechargeMinusVatSum += dblTotalRechargeMinusVat;
			        }
			        if(this.isPermissionInterCustomConfig())
			        {
			        	dblTotalTax = Double.parseDouble(currentRecord.get(5).toString()); 
			        	this.dblTotalTaxSum += dblTotalTax;
			        }
			        this.intTotalAssocSum += intTotalAssoc;
			        this.intTotalSwipeSum += intTotalSwipe;
			        this.dblTotalRechargeSum += dblTotalRecharge;
			        
			        for(int fieldIdx = 0; fieldIdx < currentRecord.size(); fieldIdx++)
			        {
			        	this.writetofile(fc, "\"" + currentRecord.get(fieldIdx).toString().trim() + "\"");
						if(fieldIdx < (currentRecord.size() - 1))
						{
							this.writetofile(fc, ",");
						}
						else
						{
							this.writetofile(fc, "\n");
						}
			        }
				}
				retValue = true;
			}
		}
		catch (NumberFormatException localNumberException)
		{
			cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
		}
		catch(Exception localException)
		{
			cat.error("writeDownloadRecords Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecordsHeaders()
	 */
	@Override
	protected boolean writeDownloadRecordsHeaders() throws IOException
	{
		boolean retValue = false;
	
		ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();
		
		if((columnHeaders != null) && (columnHeaders.size() > 0))
		{
			for(int i = 0; i < columnHeaders.size(); i++)
			{
				this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
				if(i < (columnHeaders.size() - 1))
				{
					this.writetofile(fc, ",");
				}
				else
				{
					this.writetofile(fc, "\n");
				}
			}			
			retValue = true;
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#downloadReport(com.debisys.users
	 * .SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public String downloadReport()
	{
		return this.exportToFile();
	}
	
	private void addSelectedMerchants(StringBuffer sqlString)
	{
		if (this.allMerchantsSelected)
		{
			User currentUser = this.sessionData.getUser();
			sqlString.append(" "
					+ ReportsUtil.getChainToBuildWhereSQL(currentUser.getAccessLevel(), currentUser.getDistChainType(), currentUser.getRefId(),
							"wt.rep_id"));
		}
		else
		{
			if ((this.selectedMerchantIDs != null) && (!this.selectedMerchantIDs.equals("")))
			{
				if (this.selectedMerchantIDs.indexOf(",") != -1)
				{
					sqlString.append(" AND wt.merchant_id IN (" + this.selectedMerchantIDs + ")");
				}
				else
				{
					sqlString.append(" AND wt.merchant_id = " + this.selectedMerchantIDs);
				}
			}
		}
	}

	private void addSelectedCarriers(StringBuffer strSQL) throws ReportException
	{
		if (this.allCarriersSelected)
		{
			TransactionReport.getConvenienceCardCarrierList(this.sessionData, null, true);
			strSQL.append("  AND c.[id] IN (" + TransactionReport.getSqlConvenienceCardCarrierList() + ") ");
		}
		else
		{
			if ((this.convenienceCardCarrier_ids != null) && (!this.convenienceCardCarrier_ids.equals("")))
			{
				if (this.convenienceCardCarrier_ids.indexOf(",") != -1)
				{
					strSQL.append(" AND c.[id] IN (" + this.convenienceCardCarrier_ids + ")");
				}
				else
				{
					strSQL.append(" AND c.[id] = " + this.convenienceCardCarrier_ids);
				}
			}
		}
	}

	
	
	private String getSqlString(QUERY_TYPE qType)
	{
		String retValue = null;
		double dTax = 0;
		StringBuffer strSQL = new StringBuffer();

		try
		{
			String strAccessLevel = sessionData.getProperty("access_level");
			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strRefId = sessionData.getProperty("ref_id");
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date + " 23:59:59.999", false);

			if (this.isTaxEnabled())
			{
				dTax = 1 + (DebisysConfigListener.getMxValueAddedTax(this.context) / 100);
			}
			else if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
					&& DebisysConfigListener.getDeploymentType(this.context).equals(
							DebisysConstants.DEPLOYMENT_INTERNATIONAL)
					&& DebisysConfigListener.getCustomConfigType(this.context).equals(
							DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
			{
				dTax = TransactionReport.getTaxValue(sessionData, this.end_date);
			}
			if (this.isTaxEnabled()
					|| sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
					&& DebisysConfigListener.getDeploymentType(this.context).equals(
							DebisysConstants.DEPLOYMENT_INTERNATIONAL)
					&& DebisysConfigListener.getCustomConfigType(this.context).equals(
							DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
			{
				strSQL.append("SELECT name , sum(assocCount) as assocCount , sum(swipeCount) as swipeCount,sum(amount) as recharge,sum(netamount) as net, sum(taxamount) as tax");
				if(this.isScheduling())
				{
					if (qType == QUERY_TYPE.FIXED)
					{
						strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
						strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
					}
					else
					{
						strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
					}
				}				
				strSQL.append(" FROM (");
				strSQL.append("  SELECT c.name,");
				strSQL.append("         CASE WHEN wt.id = 1710 THEN count(wt.rec_id) end as assocCount,");
				strSQL.append("         CASE WHEN wt.id != 1710 THEN count(wt.rec_id) end as swipeCount,");
				strSQL.append("         SUM(wt.amount) as amount,");
				strSQL.append("         SUM(wt.amount /" + Double.toString(dTax) + ") as netamount,");
				strSQL.append("			SUM(wt.amount - (wt.amount / " + Double.toString(dTax) + ")) as taxamount ");
				strSQL.append("  FROM	[dbo].[web_transactions] wt WITH(NOLOCK)");
				strSQL.append("			INNER JOIN [dbo].[PamperCardAssociation] pa WITH(NOLOCK) ON wt.[PamperCardAssociationID] = pa.[id]");
				strSQL.append("			INNER JOIN [dbo].[Carriers] c WITH(NOLOCK) ON c.[ID] = pa.[CarrierID]");				
			}
			else
			{
				strSQL.append("SELECT name , sum(assocCount) as assocCount , sum(swipeCount) as swipeCount,sum(amount) as recharge");
				if(this.isScheduling())
				{
					if (qType == QUERY_TYPE.FIXED)
					{
						strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
						strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
					}
					else
					{
						strSQL.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
					}
				}
				strSQL.append(" FROM (");
				strSQL.append("  SELECT c.name,");
				strSQL.append("         CASE WHEN wt.id = 1710 THEN count(wt.rec_id) end as assocCount,");
				strSQL.append("         CASE WHEN wt.id != 1710 THEN count(wt.rec_id) end as swipeCount,");
				strSQL.append("         SUM(wt.amount) as amount ");
				strSQL.append("  FROM	[dbo].[web_transactions] wt WITH(NOLOCK)");
				strSQL.append("			INNER JOIN [dbo].[PamperCardAssociation] pa WITH(NOLOCK) ON wt.[PamperCardAssociationID] = pa.[id]");
				strSQL.append("			INNER JOIN [dbo].[Carriers] c WITH(NOLOCK) ON c.[ID] = pa.[CarrierID]");								  				
			}
			
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id]");
					strSQL.append(" WHERE	r.[type] = " + DebisysConstants.REP_TYPE_REP);
					strSQL.append("			AND r.[iso_id] = " + strRefId);					
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id] AND r.[type] = " + DebisysConstants.REP_TYPE_REP);
					strSQL.append("			INNER JOIN [dbo].[reps] r1 WITH(NOLOCK) ON r.[iso_id] = r1.[rep_id] AND r1.[type] = " + DebisysConstants.REP_TYPE_SUBAGENT);
					strSQL.append("			INNER JOIN [dbo].[reps] r2 WITH(NOLOCK) ON r1.[iso_id] = r2.[rep_id] AND r2.[type] = " + DebisysConstants.REP_TYPE_AGENT);
					strSQL.append(" WHERE	r2.[iso_id] = " + strRefId);					
				}
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id] AND r.[type] = " + DebisysConstants.REP_TYPE_REP);
				strSQL.append("			INNER JOIN [dbo].[reps] r1 WITH(NOLOCK) ON r.[iso_id] = r1.[rep_id] AND r1.[type] = " + DebisysConstants.REP_TYPE_SUBAGENT);
				strSQL.append(" WHERE	r1.[iso_id] = " + strRefId);				
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id] AND r.[type] = " + DebisysConstants.REP_TYPE_REP);
				strSQL.append(" WHERE	r.[iso_id] = " + strRefId);				
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSQL.append("         AND wt.[rep_id] = " + strRefId);
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				strSQL.append("         AND wt.[merchant_id] = " + strRefId);
			}

			this.addSelectedCarriers(strSQL);

			if(this.isScheduling())
			{
				if(qType == QUERY_TYPE.RELATIVE)
				{
					strSQL.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
				}
				else if(qType == QUERY_TYPE.FIXED)
				{
					strSQL.append("         AND (wt.[datetime] >= '" + vTimeZoneFilterDates.get(0) + "'");
					strSQL.append("         AND wt.[datetime] < '" + vTimeZoneFilterDates.get(1) + "')");					
				}
			}
			else
			{
				// If not scheduling, the wild card is removed, and the current report dates are used
				int i;
				while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
				{
					strSQL.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
				}
				strSQL.append("         AND (wt.[datetime] >= '" + vTimeZoneFilterDates.get(0) + "'");
				strSQL.append("         AND wt.[datetime] < '" + vTimeZoneFilterDates.get(1) + "')");
			}

			strSQL.append("         AND wt.[PamperCardAssociationId] > 0 ");
			strSQL.append("    GROUP BY c.[name], wt.[id] ) myquery "); // Closes the inner SQL
			strSQL.append(" GROUP BY [name]");

			cat.debug(strSQL);
			retValue = strSQL.toString();

		}
		catch (ReportException localReportException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getSqlString. Exception while generationg sql string : "
					+ localReportException.toString());
		}
		catch (Exception localException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getSqlString. Exception while generationg sql string : "
					+ localException.toString());
		}

		return retValue;
	}

	private String getSqlStringMx(QUERY_TYPE qType)
	{
		double dTax = 1 + (DebisysConfigListener.getMxValueAddedTax(this.context) / 100);
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		StringBuffer strSQL = new StringBuffer();
		
		try
		{
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date + " 23:59:59.999", false);
			strSQL.append("SELECT name , sum(assocCount) as assocCount , sum(swipeCount) as swipeCount,sum(amount) as recharge,sum(netamount) as net");
			strSQL.append(" FROM (");
			strSQL.append("  SELECT c.name,");
			strSQL.append("         CASE WHEN wt.id = 1710 THEN count(wt.rec_id) end as assocCount,");
			strSQL.append("         CASE WHEN wt.id != 1710 THEN count(wt.rec_id) end as swipeCount,");
			strSQL.append("         SUM(wt.amount) as amount,");
			strSQL.append("         SUM(wt.amount /" + Double.toString(dTax) + ") as netamount ");
			
			if(this.isScheduling())
			{
				if (qType == QUERY_TYPE.FIXED)
				{
					strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
					strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
				}
				else
				{
					strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
				}
			}
			
			strSQL.append("  FROM	[dbo].[web_transactions] wt WITH(NOLOCK)");
			strSQL.append("			INNER JOIN [dbo].[PamperCardAssociation] pa WITH(NOLOCK) ON wt.[PamperCardAssociationID] = pa.[id]");
			strSQL.append("			INNER JOIN [dbo].[Carriers] c WITH(NOLOCK) ON c.[ID] = pa.[CarrierID]");				

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id]");
					strSQL.append(" WHERE	r.[type] = " + DebisysConstants.REP_TYPE_REP);
					strSQL.append("			AND r.[iso_id] = " + strRefId);					
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id] AND r.[type] = " + DebisysConstants.REP_TYPE_REP);
					strSQL.append("			INNER JOIN [dbo].[reps] r1 WITH(NOLOCK) ON r.[iso_id] = r1.[rep_id] AND r1.[type] = " + DebisysConstants.REP_TYPE_SUBAGENT);
					strSQL.append("			INNER JOIN [dbo].[reps] r2 WITH(NOLOCK) ON r1.[iso_id] = r2.[rep_id] AND r2.[type] = " + DebisysConstants.REP_TYPE_AGENT);
					strSQL.append(" WHERE	r2.[iso_id] = " + strRefId);					
				}
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id] AND r.[type] = " + DebisysConstants.REP_TYPE_REP);
				strSQL.append("			INNER JOIN [dbo].[reps] r1 WITH(NOLOCK) ON r.[iso_id] = r1.[rep_id] AND r1.[type] = " + DebisysConstants.REP_TYPE_SUBAGENT);
				strSQL.append(" WHERE	r1.[iso_id] = " + strRefId);				
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				strSQL.append("			INNER JOIN [dbo].[reps] r WITH(NOLOCK) ON wt.[rep_id] = r.[rep_id] AND r.[type] = " + DebisysConstants.REP_TYPE_REP);
				strSQL.append(" WHERE	r.[iso_id] = " + strRefId);				
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSQL.append("         AND wt.[rep_id] = " + strRefId);
				this.addSelectedMerchants(strSQL);
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				strSQL.append("         AND wt.[merchant_id] = " + strRefId);
			}

			this.addSelectedCarriers(strSQL);
			
			

			if(this.isScheduling())
			{
				if(qType == QUERY_TYPE.RELATIVE)
				{
					strSQL.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
				}
				else if(qType == QUERY_TYPE.FIXED)
				{
					strSQL.append("         AND (wt.[datetime] >= '" + vTimeZoneFilterDates.get(0) + "'");
					strSQL.append("         AND wt.[datetime] < '" + vTimeZoneFilterDates.get(1) + "')");					
				}
			}
			else
			{
				// If not scheduling, the wild card is removed, and the current report dates are used
				int i;
				while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
				{
					strSQL.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
				}
				strSQL.append("         AND (wt.[datetime] >= '" + vTimeZoneFilterDates.get(0) + "'");
				strSQL.append("         AND wt.[datetime] < '" + vTimeZoneFilterDates.get(1) + "')");					
			}

			strSQL.append("         AND wt.[PamperCardAssociationId] > 0 ");
			strSQL.append("    GROUP BY c.[name], wt.[id] ) myquery"); // Closes the inner SQL
			strSQL.append(" GROUP BY [name]");

			cat.debug(strSQL);
		}
		catch (Exception localException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getSqlStringMx. Exception while generationg sql string : "
					+ localException.toString());
		}

		return strSQL.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#generateSqlString(com.debisys.
	 * users.SessionData, javax.servlet.ServletContext, boolean)
	 */
	@Override
	public String generateSqlString(QUERY_TYPE qType)
	{
		String retValue = null;

		if (deployment.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
			retValue = this.getSqlStringMx(qType);
		}
		else
		{
			retValue = this.getSqlString(qType);
		}
		return retValue;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#getReportRecordHeaders(com.debisys
	 * .users.SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public ArrayList<ColumnReport> getReportRecordHeaders()
	{
		ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
		
		retValue.add(new ColumnReport("name", Languages.getString("jsp.admin.reports.convenienceCard.carrier_name", sessionData.getLanguage()).toUpperCase(), String.class, false));
		retValue.add(new ColumnReport("assocCount", Languages.getString("jsp.admin.reports.convenienceCard.activation", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		retValue.add(new ColumnReport("swipeCount", Languages.getString("jsp.admin.reports.convenienceCard.swipe", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		retValue.add(new ColumnReport("recharge", Languages.getString("jsp.admin.reports.analysis.merchant_activity_report.amount", sessionData.getLanguage()).toUpperCase(), Double.class, true));
		if(this.isTaxEnabled())
		{
			String totalVat = Languages.getString("jsp.admin.reports.total",sessionData.getLanguage()).toUpperCase() + Languages.getString("jsp.admin.reports.minusvat",sessionData.getLanguage()).toUpperCase();
			retValue.add(new ColumnReport("net", totalVat, Double.class, true));
		}
		
		if(this.isPermissionInterCustomConfig())
		{
			retValue.add(new ColumnReport("net", Languages.getString("jsp.admin.reports.netAmount",sessionData.getLanguage()).toUpperCase(), Double.class, true));
			retValue.add(new ColumnReport("tax", Languages.getString("jsp.admin.reports.taxAmount",sessionData.getLanguage()).toUpperCase(), Double.class, true));
		}
		return retValue;
	}

	private Vector<Vector<Object>> getResultsAll()
	{
		Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			this.setScheduling(false); // If returning results, then scheduling does not have sense
			String strSQL = this.getSqlString(QUERY_TYPE.NORMAL); 
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next())
			{

				Vector<Object> vecTemp = new Vector<Object>();
				vecTemp.add(rs.getString("name"));
				vecTemp.add(Integer.toString((rs.getInt("assocCount"))));
				vecTemp.add(Integer.toString((rs.getInt("swipeCount"))));
				vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("recharge"))));
				if(this.isTaxEnabled() || sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(this.context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(this.context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
					vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("net"))));
					vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("tax"))));
				}
				retValue.add(vecTemp);

			}
			if(retValue.size() == 0)
			{
				retValue = null;
			}
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getResultsAll Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getResultsAll Exception : " + localReportException.toString());
		}
		catch (SQLException localSqlException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getResultsAll Exception : " + localSqlException.toString());
		}
		finally
		{
			try
			{
				if(rs != null)
				{
					rs.close();
					rs = null;
				}
				if(pstmt != null)
				{
					pstmt.close();
					pstmt = null;
				}
				if(dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (SQLException cleaningSqlException)
			{
				cat.error("TopUpConvenienceCardSummaryReport.getResultsAll Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}
		
		return retValue;
	}

	private Vector<Vector<Object>> getResultsMx()
	{
		Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			this.setScheduling(false); // If returning results, then scheduling does not have sense
			String strSQL = this.getSqlStringMx(QUERY_TYPE.NORMAL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				Vector<Object> vecTemp = new Vector<Object>();
				vecTemp.add(rs.getString("name"));
				vecTemp.add(Integer.toString((rs.getInt("assocCount"))));
				vecTemp.add(Integer.toString((rs.getInt("swipeCount"))));
				vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("recharge"))));
				vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("net"))));
				retValue.add(vecTemp);
			}
			if(retValue.size() == 0)
			{
				retValue = null;
			}
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getResultsMx Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getResultsMx Exception : " + localReportException.toString());
		}
		catch (SQLException localSqlException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.getResultsMx Exception : " + localSqlException.toString());
		}
		finally
		{
			try
			{
				if(rs != null)
				{
					rs.close();
					rs = null;
				}
				if(pstmt != null)
				{
					pstmt.close();
					pstmt = null;
				}
				if(dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (SQLException cleaningSqlException)
			{
				cat.error("TopUpConvenienceCardSummaryReport.getResultsMx Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}
		
		return retValue;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 *  The following calls should be done before getting results	
	    this.setContext();
		this.setConvenienceCardCarrier_ids();
		this.setDeployment();
		this.setEnd_date(endDate);
		this.setPermissionInterCustomConfig();
		this.setProduct_ids();
		this.setSelectedMerchantIDs();
		this.setSessionData() ;
		this.setShceduling() ;
		this.setStart_date() ;
		this.setStrCurrentDate() ;
		this.setTaxCalculationEnabled();
		this.setTaxEnabled();

	 * @seecom.debisys.reports.ISupportSiteReport#getResults(com.debisys.users.
	 * SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public Vector<Vector<Object>> getResults()
	{
		Vector<Vector<Object>> retValue = null;
		
		if (deployment.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
			retValue = this.getResultsMx();
		}
		else
		{
			retValue = this.getResultsAll();
		}
		return retValue;
	}

	@Override
	public ArrayList<String> getTitles()
	{
		ArrayList<String> retValue = new ArrayList<String>();
		
		try
		{
			retValue.add(Languages.getString("jsp.admin.reports.convenienceCard.carrier_summary.title", this.sessionData.getLanguage()));
			
			Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
			String  noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote",this.sessionData.getLanguage())+":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
			retValue.add(noteTimeZone);
		}
		catch (NumberFormatException localNumberFormatException)
		{
			cat.error("getTitles Exception : " + localNumberFormatException.toString());
		}
		catch (Exception localException)
		{
			cat.error("getTitles Exception : " + localException.toString());
		}
		return retValue;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#scheduleReport(com.debisys.users
	 * .SessionData, javax.servlet.ServletContext)
	 */
	@Override
	public boolean scheduleReport()
	{
		boolean retValue = false;
		
		try
		{
			this.setScheduling(true);
			String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
			String relativeQuery =  this.generateSqlString(QUERY_TYPE.RELATIVE);
			
			ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_TOPUP_CONVENIENCE_CARD_SUM , 1);
			scheduleReport.setNameDateTimeColumn("wt.[datetime]");	
			scheduleReport.setRelativeQuery( relativeQuery );
			cat.debug("TopUpConvenienceCardSummaryReport.scheduleReport.Relative Query : " + relativeQuery);
			scheduleReport.setFixedQuery( fixedQuery );
			cat.debug("TopUpConvenienceCardSummaryReport.scheduleReport.Fixed Query : " + fixedQuery);
			TransactionReportScheduleHelper.addMetaDataReport( this.getReportRecordHeaders(), scheduleReport, this.getTitles() );				
			this.sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("TopUpConvenienceCardSummaryReport.scheduleReport Exception : " + localException.toString());
		}								

		return retValue;
	}

}
