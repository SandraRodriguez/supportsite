/**
 * 
 */
package com.debisys.reports.schedule;

import java.util.ArrayList;
import java.util.Vector;
import com.debisys.utils.ColumnReport;

/**
 * @author fernandob
 *
 */
public interface ISupportSiteReport
{
	// constants
	public enum QUERY_TYPE{NORMAL, FIXED, RELATIVE};
	
	public String generateSqlString(QUERY_TYPE qType);
	
	public boolean scheduleReport();
	
	public String downloadReport();
	
	public Vector<Vector<Object>> getResults();
	
	public ArrayList<ColumnReport> getReportRecordHeaders();
	
	public ArrayList<String> getTitles();
	
	public boolean isUsingDateLimitInterval();
	
	public int getLimitDays();

}
