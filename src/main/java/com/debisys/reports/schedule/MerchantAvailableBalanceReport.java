package com.debisys.reports.schedule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.debisys.languages.Languages;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.dao.MerchantAvailableBalanceDAO;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.HTMLEncoder;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.TimeZone;

public class MerchantAvailableBalanceReport extends ReportDownloader implements ISupportSiteReport {
	
	private static final String COMMA = ", ";

	private static final Logger logger = Logger.getLogger(MerchantAvailableBalanceReport.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	private int limitDays;
	private String startDate;
	private String endDate;
	private String queryStartDate;
	private String queryEndDate;
	private QUERY_TYPE queryType;
	private boolean isScheduling;
	private String[] merchantIdList;
	private boolean isSharedBalance;
	private String reportTitle;
	private double totalSales = 0;
	private double totalPayments = 0;
	private double totalFinalBalance = 0;
	private double totalInitialBalance = 0;
	private double totalCommissions = 0;
	private boolean isUsingDateLimitInterval;
	
	public MerchantAvailableBalanceReport(){
		super();
	}
	
	public MerchantAvailableBalanceReport(ServletContext context){
		super();
		setContext(context);
	}

	@Override
	public String generateSqlString(QUERY_TYPE qType) {
		queryType = qType;
		String query = isSharedBalance() ? sql_bundle.getString("getRepsAvailableBalance") : sql_bundle.getString("getMerchantAvailableBalance");
		String strAccessLevel = sessionData.getProperty("access_level");
		Vector<String> timeZoneFilterDates = null;
		timeZoneFilterDates = getBoundaryDates(strAccessLevel, timeZoneFilterDates);
        defineStartAndEndQueryDates(timeZoneFilterDates.get(0), timeZoneFilterDates.get(1));
        query = replaceTag(query, "@startDate", queryStartDate);
        query = replaceTag(query, "@endDate", queryEndDate);
        String merchantIds = getMerchantIdsAsString();
        query = replaceTag(query, "@merchantId", merchantIds);
		return query;
	}
	
	private String getMerchantIdsAsString() {
		StringBuilder sb = new StringBuilder();
		if(merchantIdList != null && Arrays.binarySearch(merchantIdList, "ALL") < 0){
			for(int i = 0; i < merchantIdList.length; i++){
				sb.append(merchantIdList[i]);
				if(i < merchantIdList.length - 1)
					sb.append(COMMA);
			}
		} else {
			sb.append("(");
			sb.append(isSharedBalance() ? getRepIdsByISOQuery() : getMerchantIdsByISOQuery());
			sb.append(")");
		}
		return sb.toString();
	}

	private String replaceTag(String str, String tag, String value){
		return str.replace(tag, value);
	}

	private Vector<String> getBoundaryDates(String strAccessLevel, Vector<String> timeZoneFilterDates) {
		try {
			timeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.startDate,
			        this.endDate + " 23:59:59.997", false);
		} catch (Exception e) {
			logger.error("MerchantCreditReport.getSqlString. Exception while generationg sql string : " + e.getMessage());
		}
		return timeZoneFilterDates;
	}
	
	private void defineStartAndEndQueryDates(String tzStartDate, String tzEndDate){
		if (((this.startDate != null) && (this.startDate.length() > 0)) && ((this.endDate != null) && (this.endDate.length() > 0))) {
            if (this.isScheduling()) {
                if (queryType == QUERY_TYPE.RELATIVE) {
                    queryStartDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE;
                    queryEndDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE;
                } else if (queryType == QUERY_TYPE.FIXED) {
                    queryStartDate = String.format("'%s'", tzStartDate);
                    queryEndDate = String.format("'%s'", tzEndDate);
                }
            } else {
                queryStartDate = String.format("'%s'", tzStartDate);
                queryEndDate = String.format("'%s'", tzEndDate);
            }
        } else {
            queryStartDate = String.format("'%s'", tzStartDate);
            queryEndDate = String.format("'%s'", tzEndDate);
        }
	}
	
	public ArrayList<HashMap<String, String>> getMerchantsByIso(){
		String query = getMerchantsByISOQuery();
		MerchantAvailableBalanceDAO dao = new MerchantAvailableBalanceDAO();
		ArrayList<HashMap<String, String>> rows = dao.queryGetMerchantsByIso(query);
		Collections.sort(rows, new MerchantHashMapComparator());
		return rows;
	}
	
	public ArrayList<HashMap<String, String>> getRepsByIso(){
		String query = getRepsByISOQuery();
		MerchantAvailableBalanceDAO dao = new MerchantAvailableBalanceDAO();
		return dao.queryGetRepsByIso(query);
	}

	private String getMerchantsByISOQuery() {
		String query = sql_bundle.getString("getMerchantByISO");
		query = replaceTag(query, "@isoId", getStrRefId());
		String sharedBalance = isSharedBalance() ? "1" : "0";
		query = replaceTag(query, "@isSharedBalance", sharedBalance);
		return query;
	}
	
	private String getMerchantIdsByISOQuery() {
		String query = sql_bundle.getString("getMerchantIdsByISO");
		query = replaceTag(query, "@isoId", getStrRefId());
		String sharedBalance = isSharedBalance() ? "1" : "0";
		query = replaceTag(query, "@isSharedBalance", sharedBalance);
		return query;
	}
	
	private String getRepsByISOQuery() {
		String query = sql_bundle.getString("getRepsByISO");
		query = replaceTag(query, "@isoId", getStrRefId());
		String sharedBalance = isSharedBalance() ? "1" : "0";
		query = replaceTag(query, "@isSharedBalance", sharedBalance);
		return query;
	}
	
	private String getRepIdsByISOQuery(){
		String query = sql_bundle.getString("getRepIdsByISO");
		query = replaceTag(query, "@isoId", getStrRefId());
		String sharedBalance = isSharedBalance() ? "1" : "0";
		query = replaceTag(query, "@isSharedBalance", sharedBalance);
		return query;
	}

	@Override
	public boolean scheduleReport() {
		boolean retValue = false;

        try {
            this.setScheduling(true);
            String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
            String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

            ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_MERCHANT_CREDIT_REPORT, 1);
            scheduleReport.setNameDateTimeColumn("PR.RequestDate");
            scheduleReport.setRelativeQuery(relativeQuery);
            scheduleReport.setFixedQuery(fixedQuery);
            TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, getTitles());
            this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
            retValue = true;
        } catch (Exception localException) {
            logger.error("MerchantCreditReport.scheduleReport Exception : " + localException.getMessage());
        }

        return retValue;
	}

	@Override
	public String downloadReport() {
		return exportToFile();
	}

	@Override
	public Vector<Vector<Object>> getResults() {
		MerchantAvailableBalanceDAO dao = new MerchantAvailableBalanceDAO();
		return dao.queryAvailableBalanceReport(generateSqlString(QUERY_TYPE.NORMAL));
	}

	@Override
	public ArrayList<ColumnReport> getReportRecordHeaders() {
		ArrayList<ColumnReport> columns = new ArrayList<ColumnReport>();
		try{
			String merchantId = isSharedBalance() ? Languages.getString("jsp.admin.customers.reps.rep_id", sessionData.getLanguage()) : Languages.getString("jsp.admin.customers.merchants.merchant_id", sessionData.getLanguage());
			columns.add(new ColumnReport("merchantId", merchantId.toUpperCase(), String.class, false));
			String merchantName = isSharedBalance() ? Languages.getString("jsp.admin.customers.merchants.rep_name", sessionData.getLanguage()) : Languages.getString("jsp.admin.reports.merchant_name", sessionData.getLanguage());
			columns.add(new ColumnReport("merchantName", merchantName.toUpperCase(), String.class, false));
			String repName = isSharedBalance() ? Languages.getString("jsp.admin.customers.reps_info.legal_businessname", sessionData.getLanguage()) : Languages.getString("jsp.admin.customers.merchants.rep_name", sessionData.getLanguage());
			columns.add(new ColumnReport("repName", repName.toUpperCase(), String.class, false));
			columns.add(new ColumnReport("initialBalance", Languages.getString("jsp.admin.reports.abr.initialBalance", sessionData.getLanguage()).toUpperCase(), Double.class, false));
			columns.add(new ColumnReport("aplications", Languages.getString("jsp.admin.reports.abr.sales", sessionData.getLanguage()).toUpperCase(), Double.class, false));
			columns.add(new ColumnReport("deposits", Languages.getString("jsp.admin.reports.abr.deposits", sessionData.getLanguage()).toUpperCase(), Double.class, false));
			columns.add(new ColumnReport("commissions", Languages.getString("jsp.admin.reports.abr.commissions", sessionData.getLanguage()).toUpperCase(), Double.class, false));
			columns.add(new ColumnReport("finalBalance", Languages.getString("jsp.admin.reports.abr.finalBalance", sessionData.getLanguage()).toUpperCase(), Double.class, false));
		} catch (Exception e){
			logger.error(e);
		}
		return columns;
	}

	@Override
	public ArrayList<String> getTitles() {
		ArrayList<String> retValue = new ArrayList<String>();

        try {
            retValue.add(this.getStrRefId());

            retValue.add(reportTitle + " "
                    + Languages.getString("jsp.admin.from", this.sessionData.getLanguage()) + " "
                    + HTMLEncoder.encode(this.startDate) + " "
                    + Languages.getString("jsp.admin.to", this.sessionData.getLanguage()) + " "
                    + HTMLEncoder.encode(this.endDate));
            Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
            String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
                    + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
            retValue.add(noteTimeZone);
            retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
            retValue.add(Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.limited_to_90_days", this.sessionData.getLanguage()));
        } catch (NumberFormatException localNumberFormatException) {
            logger.error("MerchantCreditReport.getTitles Exception : " + localNumberFormatException.getMessage());
        } catch (Exception localException) {
            logger.error("MerchantCreditReport.getTitles Exception : " + localException.getMessage());
        }
        return retValue;
	}
	
	public boolean validateMerchant(String merchantId){
		boolean valid = true;
		MerchantAvailableBalanceDAO dao = new MerchantAvailableBalanceDAO();
		String query = sql_bundle.getString("validateMerchantInRep");
		if(!dao.validateMerchantInRep(query, merchantId, isSharedBalance, getStrRefId())){
			valid = false;
		}
		return valid;
	}

	public boolean validateRep(String repId){
		boolean valid = true;
		MerchantAvailableBalanceDAO dao = new MerchantAvailableBalanceDAO();
		String query = sql_bundle.getString("validateRepInISO");
		if(!dao.validateRepInISO(query, repId, isSharedBalance, getStrRefId())){
			valid = false;
		}
		return valid;
	}

	@Override
	public boolean isUsingDateLimitInterval() {
		return isUsingDateLimitInterval;
	}
	
	public void setUsingDateLimitInterval(boolean isUsingDateLimitInterval) {
		this.isUsingDateLimitInterval = isUsingDateLimitInterval;
	}

	@Override
	public int getLimitDays() {
		return limitDays;
	}

	@Override
	protected boolean writeDownloadFileHeader() throws IOException {
		boolean retValue = false;
        try {
        	TransactionReport transReport = new TransactionReport();
            this.writetofile(this.fc, "\"" + reportTitle + "\"\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
                    + this.sessionData.getUser().getUsername() + "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
                    + transReport.getStartDateFormatted() + "\n");
            this.writetofile(fc, "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\"\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + "\"," + this.startDate + "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + "\"," + this.endDate + "\n");

            retValue = true;
        } catch (Exception localException) {
            logger.error("MerchantCreditReport.writeDownloadFileHeader Exception : " + localException.getMessage());
        }
        return retValue;
	}

	@Override
	protected boolean writeDownloadRecordsHeaders() throws IOException {
		boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            this.writetofile(fc, "\"#\",");
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
	}

	@Override
	protected boolean writeDownloadRecords() throws IOException {
		boolean retValue = false;
        Vector<Vector<Object>> reportResults = this.getResults();
        try {
        	if ((reportResults != null) && (reportResults.size() > 0)) {
        		for (int i = 0; i < reportResults.size(); i++) {
                    Vector<Object> row = reportResults.get(i);
                    double iniBalance = (Double)row.get(3);
		        	double sales = (Double)row.get(4);
		        	double payments = (Double)row.get(5);
		        	double commissions = (Double)row.get(6);
		        	double finalBalance = (Double)row.get(7);
		        	
		        	totalInitialBalance += iniBalance;
		        	totalSales += sales;
		        	totalPayments += payments;
		        	totalCommissions += commissions;
		        	totalFinalBalance += finalBalance;
		        	
		        	writetofile(fc, String.valueOf(i + 1) + COMMA);
		        	writetofile(fc, "\"" + row.get(0) + "\"" + COMMA);
		        	writetofile(fc, "\"" + row.get(1) + "\"" + COMMA);
		        	writetofile(fc, "\"" + row.get(2) + "\"" + COMMA);
		        	writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(iniBalance)) + COMMA);
		        	writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(sales)) + COMMA);
		        	writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(payments)) + COMMA);
		        	writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(commissions)) + COMMA);
		        	writetofile(fc, "" + NumberUtil.formatAmount(Double.toString(finalBalance)) + "\n");
        		}
        	}
        } catch(Exception e){
        	logger.error("writeDownloadRecords Exception : " + e.getMessage());
        }
		return retValue;
	}

	@Override
	protected boolean writeDownloadFileFooter() throws IOException {
		boolean retValue = false;
        try {
            this.writetofile(fc, Languages.getString("jsp.admin.reports.totals", this.sessionData.getLanguage()));
            this.writetofile(fc, COMMA);
            this.writetofile(fc, COMMA);
            this.writetofile(fc, COMMA);
            this.writetofile(fc, COMMA);
            this.writetofile(fc, NumberUtil.formatAmount(Double.toString(this.totalInitialBalance)) + COMMA);
            this.writetofile(fc, NumberUtil.formatAmount(Double.toString(this.totalSales)) + COMMA);
            this.writetofile(fc, NumberUtil.formatAmount(Double.toString(this.totalPayments)) + COMMA);
            this.writetofile(fc, NumberUtil.formatAmount(Double.toString(this.totalCommissions)) + COMMA);
            this.writetofile(fc, NumberUtil.formatAmount(Double.toString(this.totalFinalBalance)));
            retValue = true;
        } catch (Exception localException) {
            logger.error("writeDownloadFileFooter Exception : " + localException.getMessage());
        }
        return retValue;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public void setLimitDays(int limitDays) {
		this.limitDays = limitDays;
	}

	public boolean isScheduling() {
		return isScheduling;
	}

	public void setScheduling(boolean isScheduling) {
		this.isScheduling = isScheduling;
	}

	public String[] getMerchantIdList() {
		return merchantIdList;
	}

	public void setMerchantIdList(String[] merchantIdList) {
		this.merchantIdList = merchantIdList;
	}

	public boolean isSharedBalance() {
		return isSharedBalance;
	}

	public void setSharedBalance(boolean isSharedBalance) {
		this.isSharedBalance = isSharedBalance;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}
}
