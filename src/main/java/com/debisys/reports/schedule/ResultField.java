package com.debisys.reports.schedule;

/**
 * Because in may reports the columns are variable, is difficult to apply
 * formats or ordering if the field names are not known. This class encapsulates
 * the field name and value, so presentation knows when to apply the correct
 * format
 *
 * @author fernandob
 */
public class ResultField {

    private String name;
    private Object value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ResultField{" + "name=" + name + ", value=" + value + '}';
    }

    public ResultField() {
        this.name = "";
        this.value = null;
    }

    public ResultField(String name, Object value) {
        this.name = name;
        this.value = value;
    }

}
