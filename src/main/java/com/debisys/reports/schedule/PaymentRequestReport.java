/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.schedule;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.ResourceReport;
import static com.debisys.reports.schedule.ReportDownloader.cat;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletContext;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import static com.debisys.utils.DbUtil.hasColumn;

/**
 *
 * @author fernandob
 */
public class PaymentRequestReport extends ReportDownloader implements ISupportSiteReport1 {

    public static final SimpleDateFormat documentDateFormatter = new SimpleDateFormat("MM/dd/yyyy");
    public static final SimpleDateFormat requestDateFormatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss a");
    public static final String REQUEST_ID = "id";
    public static final String STATUS_CODE_ID = "StatusCode";
    public static final String REJECT_REASON_ID = "rejectReazon";
    public static final String PROCESS_USER_ID = "processUser";
    public static final String DBA_ID = "dba";
    public static final String MILLENIUM_NO_ID = "millennium_no";
    public static final String CLERK_ID = "clerkid";
    public static final String CLERK_NAME_ID = "ClerkName";
    public static final String CLERK_ID_NAME_ID = "ClerkIdName";
    public static final String DOCUMENT_NUMBER_ID = "DocumentNumber";
    public static final String DOCUMENT_DATE_ID = "DocumentDate";
    public static final String BANK_NAME_ID = "BankName";
    public static final String AMOUNT_ID = "Amount";
    public static final String REQUEST_DATE_ID = "RequestDate";
    public static final String AGENT_ID = "Agent";
    public static final String SUB_AGENT_ID = "SubAgent";
    public static final String REP_ID = "Rep";
    public static final String MERCHANT_ID = "merchant_id";
    public static final String BANK_ID = "BankID";
    public static final String TRANSAFER_ACCOUNT = "TransferAccount";
    public static final String INVOICE_TYPE = "InvoiceType";
    public static final String DEPOSIT_TYPE_DESCRIPTION_ENG = "descriptionEnglish";
    public static final String DEPOSIT_TYPE_DESCRIPTION_SPA = "descriptionSpanish";

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

    // instance fields
    private String startDate;
    private String endDate;
    private ArrayList<Long> agentId;
    private boolean allAgentsSelected;
    private ArrayList<Long> subAgentId;
    private boolean allSubAgentsSelected;
    private ArrayList<Long> repId;
    private boolean allRepsSelected;
    private ArrayList<Long> merchantId;
    private boolean allMerchantsSelected;
    private ArrayList<Long> paymentStatusId;
    private boolean allPaymentStatusSelected;
    private ArrayList<Long> bankId;
    private boolean allBanksSelected;
    private long paymentRequestId;
    private String documentId;
    private BigDecimal paymentAmount;
    private long siteId;

    private boolean scheduling;
    private String strCurrentDate;
    private int limitDays;
    private boolean usingDateLimitInterval;
    private Vector<String> vTimeZoneData;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ArrayList<Long> getAgentId() {
        return agentId;
    }

    public void setAgentId(ArrayList<Long> agentId) {
        this.agentId = agentId;
    }

    public boolean isAllAgentsSelect() {
        return allAgentsSelected;
    }

    public void setAllAgentsSelect(boolean allAgentsSelect) {
        this.allAgentsSelected = allAgentsSelect;
    }

    public ArrayList<Long> getSubAgentId() {
        return subAgentId;
    }

    public void setSubAgentId(ArrayList<Long> subAgentId) {
        this.subAgentId = subAgentId;
    }

    public boolean isAllSubAgentsSelected() {
        return allSubAgentsSelected;
    }

    public void setAllSubAgentsSelected(boolean allSubAgentsSelected) {
        this.allSubAgentsSelected = allSubAgentsSelected;
    }

    public ArrayList<Long> getRepId() {
        return repId;
    }

    public void setRepId(ArrayList<Long> repId) {
        this.repId = repId;
    }

    public boolean isAllRepsSelected() {
        return allRepsSelected;
    }

    public void setAllRepsSelected(boolean allRepsSelected) {
        this.allRepsSelected = allRepsSelected;
    }

    public ArrayList<Long> getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(ArrayList<Long> merchantId) {
        this.merchantId = merchantId;
    }

    public boolean isAllMerchantsSelected() {
        return allMerchantsSelected;
    }

    public void setAllMerchantsSelected(boolean allMerchantsSelected) {
        this.allMerchantsSelected = allMerchantsSelected;
    }

    public ArrayList<Long> getPaymentStatusId() {
        return paymentStatusId;
    }

    public void setPaymentStatusId(ArrayList<Long> paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    public boolean isAllPaymentStatusSelected() {
        return allPaymentStatusSelected;
    }

    public void setAllPaymentStatusSelected(boolean allPaymentStatusSelected) {
        this.allPaymentStatusSelected = allPaymentStatusSelected;
    }

    public ArrayList<Long> getBankId() {
        return bankId;
    }

    public void setBankId(ArrayList<Long> bankId) {
        this.bankId = bankId;
    }

    public boolean isAllBanksSelected() {
        return allBanksSelected;
    }

    public void setAllBanksSelected(boolean allBanksSelected) {
        this.allBanksSelected = allBanksSelected;
    }

    public long getPaymentRequestId() {
        return paymentRequestId;
    }

    public void setPaymentRequestId(long paymentRequestId) {
        this.paymentRequestId = paymentRequestId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public boolean isAllAgentsSelected() {
        return allAgentsSelected;
    }

    public void setAllAgentsSelected(boolean allAgentsSelected) {
        this.allAgentsSelected = allAgentsSelected;
    }

    public boolean isScheduling() {
        return scheduling;
    }

    public void setScheduling(boolean scheduling) {
        this.scheduling = scheduling;
    }

    /**
     * @return the strCurrentDate
     */
    public String getStrCurrentDate() {
        return strCurrentDate;
    }

    /**
     * @param strCurrentDate the strCurrentDate to set
     */
    public void setStrCurrentDate(String strCurrentDate) {
        this.strCurrentDate = strCurrentDate;
    }

    private PaymentRequestReport() {
        this.startDate = null;
        this.endDate = null;
        this.agentId = null;
        this.allAgentsSelected = false;
        this.subAgentId = null;
        this.allSubAgentsSelected = false;
        this.repId = null;
        this.allRepsSelected = false;
        this.merchantId = null;
        this.allMerchantsSelected = false;
        this.paymentStatusId = null;
        this.allPaymentStatusSelected = false;
        this.bankId = null;
        this.allBanksSelected = false;
        this.paymentRequestId = Long.MAX_VALUE;
        this.documentId = null;
        this.paymentAmount = BigDecimal.ZERO;;
        this.siteId = Long.MIN_VALUE;
        this.scheduling = false;
        this.strCurrentDate = null;
        this.limitDays = 0;
        this.usingDateLimitInterval = false;
        this.vTimeZoneData = null;

    }

    public PaymentRequestReport(SessionData sessionData, ServletContext pContext,
            String startDate, String endDate, ArrayList<Long> agentId,
            boolean allAgentsSelected, ArrayList<Long> subAgentId,
            boolean allSubAgentsSelected, ArrayList<Long> repId,
            boolean allRepsSelected, ArrayList<Long> merchantId,
            boolean allMerchantsSelected, ArrayList<Long> paymentStatusId,
            boolean allPaymentStatusSelected, ArrayList<Long> bankId,
            boolean allBanksSelected, long paymentRequestId, String documentId,
            BigDecimal paymentAmount, long siteId, boolean scheduling) {
        this();
        this.sessionData = sessionData;
        this.strRefId = sessionData.getProperty("ref_id");
        this.setContext(pContext);
        this.startDate = startDate;
        this.endDate = endDate;
        this.agentId = agentId;
        this.allAgentsSelected = allAgentsSelected;
        this.subAgentId = subAgentId;
        this.allSubAgentsSelected = allSubAgentsSelected;
        this.repId = repId;
        this.allRepsSelected = allRepsSelected;
        this.merchantId = merchantId;
        this.allMerchantsSelected = allMerchantsSelected;
        this.paymentStatusId = paymentStatusId;
        this.allPaymentStatusSelected = allPaymentStatusSelected;
        this.bankId = bankId;
        this.allBanksSelected = allBanksSelected;
        this.paymentRequestId = paymentRequestId;
        this.documentId = documentId;
        this.paymentAmount = paymentAmount;
        this.siteId = siteId;
        this.scheduling = scheduling;
        this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
        this.setDatesWithLimit();
    }


    @Override
    protected boolean writeDownloadFileHeader() throws IOException {
        boolean retValue = false;
        try {
            this.writetofile(this.fc, String.format("%s\n",
                    Languages.getString("jsp.admin.reports.paymentrequest_search.reportTitle", sessionData.getLanguage()).toUpperCase()));
            this.writetofile(fc, String.format("%s, %s\n",
                    Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()),
                    sessionData.getUser().getUsername()));
            this.writetofile(fc, String.format("%s, %s",
                    Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()),
                    this.getStrCurrentDate()));
            this.writetofile(fc, "\n");
            this.writetofile(fc, Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()));
            this.writetofile(fc, "\n");
            if ((this.startDate != null) && (!this.startDate.isEmpty())) {
                this.writetofile(fc, Languages.getString("jsp.admin.start_date", sessionData.getLanguage())
                        + " " + this.startDate + "\n");
            }
            if ((this.endDate != null) && (!this.endDate.isEmpty())) {
                this.writetofile(fc, Languages.getString("jsp.admin.end_date", sessionData.getLanguage())
                        + " " + this.endDate + "\n");
            }
            if ((!this.allAgentsSelected) && (this.agentId != null)
                    && (!this.agentId.isEmpty())) {
                this.writetofile(fc, "\n");
                this.writetofile(fc, String.format(",,%s", Languages.getString("jsp.admin.reports.paymentrequest_search.agent", sessionData.getLanguage()).toUpperCase()));
                this.writeFilterList2File(this.agentId);
            }

            if ((!this.allSubAgentsSelected) && (this.subAgentId != null)
                    && (!this.subAgentId.isEmpty())) {
                this.writetofile(fc, "\n");
                this.writetofile(fc, String.format(",,%s", Languages.getString("jsp.admin.reports.paymentrequest_search.subAgent", sessionData.getLanguage()).toUpperCase()));
                this.writeFilterList2File(this.subAgentId);
            }

            if ((!this.allRepsSelected) && (this.repId != null)
                    && (!this.repId.isEmpty())) {
                this.writetofile(fc, "\n");
                this.writetofile(fc, String.format(",,%s", Languages.getString("jsp.admin.reports.paymentrequest_search.rep", sessionData.getLanguage()).toUpperCase()));
                this.writeFilterList2File(this.repId);
            }

            if ((!this.allMerchantsSelected) && (this.merchantId != null)
                    && (!this.merchantId.isEmpty())) {
                this.writetofile(fc, "\n");
                this.writetofile(fc, String.format(",,%s", Languages.getString("jsp.admin.reports.paymentrequest_search.merchant", sessionData.getLanguage()).toUpperCase()));
                this.writeFilterList2File(this.merchantId);
            }

            if ((!this.allPaymentStatusSelected) && (this.paymentStatusId != null)
                    && (!this.paymentStatusId.isEmpty())) {
                this.writetofile(fc, "\n");
                this.writetofile(fc, String.format(",,%s", Languages.getString("jsp.admin.reports.paymentrequest_search.status", sessionData.getLanguage()).toUpperCase()));
                this.writeFilterList2File(this.paymentStatusId);
            }

            if ((!this.allBanksSelected) && (this.bankId != null)
                    && (!this.bankId.isEmpty())) {
                this.writetofile(fc, String.format(",%s", Languages.getString("jsp.admin.reports.paymentrequest_search.banks", sessionData.getLanguage()).toUpperCase()));
                this.writeFilterList2File(this.bankId);
            }

            if (this.paymentRequestId != Long.MIN_VALUE) {
                this.writetofile(this.fc, String.format(",%s, %d\n",
                        Languages.getString("jsp.admin.reports.paymentrequest_search.requestId", sessionData.getLanguage()).toUpperCase(), this.paymentRequestId));
            }

            if ((this.documentId != null) && (!this.documentId.trim().isEmpty())) {
                this.writetofile(this.fc, String.format(",%s, %s\n",
                        Languages.getString("jsp.admin.reports.paymentrequest_search.documentId", sessionData.getLanguage()).toUpperCase(), documentId));
            }

            if (this.paymentAmount != BigDecimal.ZERO) {
                this.writetofile(this.fc, String.format(",%s, %f\n",
                        Languages.getString("jsp.admin.reports.paymentrequest_search.amount", sessionData.getLanguage()).toUpperCase(), this.paymentRequestId));
            }

            if (this.siteId != Long.MIN_VALUE) {
                this.writetofile(this.fc, String.format(",%s,%d\n",
                        Languages.getString("jsp.admin.reports.paymentrequest_search.siteId", sessionData.getLanguage()).toUpperCase(), this.siteId));
            }

            retValue = true;
        } catch (Exception localException) {
            cat.error("writeDownloadFileHeader Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecordsHeaders() throws IOException {
        boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            this.writetofile(fc, "#,");
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, columnHeaders.get(i).getLanguageDescription());
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecords() throws IOException {
        boolean retValue = false;
        ArrayList<ArrayList<ResultField>> reportResults = this.getResults();

        try {
            if ((reportResults != null) && (reportResults.size() > 0)) {
                int recordCount = 1;
                for (ArrayList<ResultField> currentRecord : reportResults) {
                    // Write the line number
                    this.writetofile(fc, String.valueOf(recordCount++) + ", ");
                    for (ResultField currentField : currentRecord) {
                        String currentValue = "";
//
//                        if (currentField.getName().equals(PaymentRequestReport.REJECT_REASON_ID)) {
//                            String resourceKey = "jsp.admin.reports.paymentrequest_search.rejectReason_" + currentField.getValue();
//                            String rejectReazon = Languages.getString(resourceKey, this.sessionData.getLanguage());
//                            if ((rejectReazon != null) && (!rejectReazon.isEmpty())) {
//                                currentValue = rejectReazon;
//                            } else {
//                                currentValue = String.valueOf(currentField.getValue());
//                            }
//                        } else 
                        if (currentField.getName().equals(PaymentRequestReport.STATUS_CODE_ID)) {
                            String resourceKey = "jsp.admin.reports.paymentrequest_search.status_" + currentField.getValue();
                            String statusString = Languages.getString(resourceKey, this.sessionData.getLanguage());
                            if ((statusString != null) && (!statusString.isEmpty())) {
                                currentValue = statusString;
                            } else {
                                currentValue = String.valueOf(currentField.getValue());
                            }
                        } else if (currentField.getName().equals(PaymentRequestReport.DOCUMENT_DATE_ID)) {
                            currentValue = PaymentRequestReport.documentDateFormatter.format(currentField.getValue());
                        } else if (currentField.getName().equals(PaymentRequestReport.REQUEST_DATE_ID)) {
                            currentValue = PaymentRequestReport.requestDateFormatter.format(currentField.getValue());
                        } else if (currentField.getName().equals(PaymentRequestReport.BANK_ID)
                                || currentField.getName().equals(PaymentRequestReport.MERCHANT_ID)
                                || currentField.getName().equals(PaymentRequestReport.CLERK_ID)
                                || currentField.getName().equals(PaymentRequestReport.CLERK_NAME_ID)) {
                            continue;
                        } else if (currentField.getName().equals(PaymentRequestReport.TRANSAFER_ACCOUNT)) {
                            currentValue = String.valueOf(currentField.getValue());
                        } else if (currentField.getName().equals(PaymentRequestReport.INVOICE_TYPE)) {
                            currentValue = String.valueOf(currentField.getValue());
                        } else {
                            currentValue = String.valueOf(currentField.getValue());
                        }
                        this.writetofile(fc, currentValue);
                        if (!currentField.getName().equals(INVOICE_TYPE)) {
                            this.writetofile(fc, ", ");
                        } else {
                            this.writetofile(fc, "\n");
                        }
                    }
                }
                retValue = true;
            }
        } catch (NumberFormatException localNumberException) {
            cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
        } catch (Exception localException) {
            cat.error("writeDownloadRecords Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadFileFooter() throws IOException {
        return true;
    }

    private void setEntityChain(StringBuilder strSql) {
        if (strSql != null) {
            String strAccessLevel = this.sessionData.getProperty("access_level");

            if (this.sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    strSql.append(String.format(" AND R.iso_id = %s", this.sessionData.getProperty("ref_id")));
                } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                    strSql.append(String.format(" AND R.rep_id = %s", this.sessionData.getProperty("ref_id")));
                } else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                    strSql.append(String.format(" AND M.merchant_id = %s", this.sessionData.getProperty("ref_id")));
                }

            } else if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    strSql.append(String.format(" AND R.iso_id IN (SELECT rep_id FROM dbo.reps SA WITH (NOLOCK) WHERE type = %s AND iso_id IN ", DebisysConstants.REP_TYPE_SUBAGENT));
                    strSql.append(String.format(" (SELECT rep_id FROM dbo.reps AG WITH (NOLOCK) WHERE type = %s AND iso_id = %s ))", DebisysConstants.REP_TYPE_AGENT, sessionData.getProperty("ref_id")));
                } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                    strSql.append(String.format(" AND R.iso_id IN (SELECT rep_id FROM dbo.reps SA WITH (NOLOCK) WHERE type = %s AND iso_id = %s )", DebisysConstants.REP_TYPE_SUBAGENT, sessionData.getProperty("ref_id")));
                } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                    strSql.append(String.format(" AND R.iso_id = %s", this.sessionData.getProperty("ref_id")));
                } else if (strAccessLevel.equals(DebisysConstants.REP)) {
                    strSql.append(String.format(" AND R.rep_id = %s", this.sessionData.getProperty("ref_id")));
                } else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                    strSql.append(String.format(" AND M.merchant_id = %s", this.sessionData.getProperty("ref_id")));
                }
            }
        }
    }

    private void setSqlInList(StringBuilder strSql, ArrayList<Long> list) {
        if ((strSql != null) && (list != null)) {
            for (int i = 0; i < list.size(); i++) {
                strSql.append(list.get(i));
                if (i < (list.size() - 1)) {
                    strSql.append(',');
                }
            }
        }
    }

    private synchronized void setDatesWithLimit() {
        String tmpStartDate = this.startDate;
        String tmpEndDate = this.endDate;

        try {
            if ((!this.usingDateLimitInterval)
                    && ((this.startDate != null) && (!this.startDate.isEmpty()))
                    && ((this.endDate != null) && (!this.endDate.isEmpty()))) {
                //String strAccessLevel = this.sessionData.getProperty("access_level");
                this.getLimitDays();
                int numDays = DateUtil.getDaysBetween(this.startDate, this.endDate);
                if (numDays > this.limitDays) {
                    this.startDate = DateUtil.addSubtractDays(this.endDate, -this.limitDays);
                }
                //this.vTimeZoneData = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.startDate, this.endDate + " 23:59:59.900", false);                
                this.vTimeZoneData = new Vector<String>();
                this.vTimeZoneData.add(this.startDate + " 00:00:00.000");
                this.vTimeZoneData.add(this.endDate + " 23:59:59.900");

                this.usingDateLimitInterval = true;
            }
        } catch (Exception localException) {
            cat.warn("Error while setting report limits", localException);
            this.startDate = tmpStartDate;
            this.endDate = tmpEndDate;
            this.usingDateLimitInterval = false;
        }
    }

    @Override
    public String generateSqlString(QUERY_TYPE qType) {
        String retValue = null;
        StringBuilder strSql = new StringBuilder();

        try {
            this.setDatesWithLimit();
            String strAccessLevel = this.sessionData.getProperty("access_level");
            String language = this.sessionData.getUser().getLanguageCode();
            strSql.append(" SELECT PR.id, PRS.code AS StatusCode, ISNULL(rrbl.Message, '') As rejectReazon,  ISNULL(PRSC.logonId, '') AS processUser, ");
            strSql.append("  M.dba, T.millennium_no, TP.NAME  AS ClerkName, PR.DocumentNumber, ");
            strSql.append("  PR.DocumentDate, B.NAME AS BankName, PR.Amount, PR.RequestDate, dt.descriptionEnglish, dt.descriptionSpanish, ");
            if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    strSql.append(" A.businessname As Agent, SA.businessname As SubAgent, ");
                } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                    strSql.append(" SA.businessname As SubAgent, ");
                }
            }
            strSql.append("   R.businessname As Rep,  ");
            strSql.append("  PR.clerkid, M.merchant_id, B.id AS BankID, dt.id AS depositTypeId, PR.transferAccount AS TransferAccount, it.description AS InvoiceType ");
            if (this.isScheduling()) {
                if ((qType == QUERY_TYPE.FIXED) && (vTimeZoneData != null)) {
                    strSql.append(String.format(", '%s' AS startDate ", vTimeZoneData.get(0)));
                    strSql.append(String.format(", '%s' AS endDate ", vTimeZoneData.get(1)));
                } else {
                    strSql.append(String.format(" %s ", ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS));
                }
            }
            strSql.append(" FROM  dbo.PaymentRequests PR WITH (nolock) ");
            strSql.append("     INNER JOIN dbo.PaymentRequestStatus PRS with(nolock) ");
            strSql.append("         ON PR.statusid = PRS.id ");
            strSql.append("     INNER JOIN terminals T with(nolock) ");
            strSql.append("         ON PR.millennium_no = T.millennium_no ");
            strSql.append("     INNER JOIN dbo.merchants M with(nolock) ");
            strSql.append("         ON T.merchant_id = M.merchant_id ");
            strSql.append("     INNER JOIN dbo.reps R with(nolock) ");
            strSql.append("         ON M.rep_id = R.rep_id ");
            if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    strSql.append("     INNER JOIN dbo.reps SA with(nolock) ");
                    strSql.append("         ON R.iso_id = SA.rep_id ");
                    strSql.append("     INNER JOIN dbo.reps A with(nolock) ");
                    strSql.append("         ON SA.iso_id = A.rep_id ");
                } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                    strSql.append("     INNER JOIN dbo.reps SA with(nolock) ");
                    strSql.append("         ON R.iso_id = SA.rep_id ");
                }
            }
            strSql.append("     LEFT JOIN dbo.terminal_passwords TP with(nolock) ");
            strSql.append("         ON PR.clerkid = TP.rec_id ");
            strSql.append("     LEFT JOIN dbo.Banks B WITH (nolock)");
            strSql.append("         ON PR.bankid = B.id ");
            strSql.append("     LEFT JOIN dbo.depositTypes dt WITH (nolock) ON pr.depositTypeId = dt.id ");
            strSql.append("     LEFT JOIN dbo.PaymentRequestStatusChange PRSC WITH (nolock)");
            strSql.append("         ON PR.ID = PRSC.paymentRequestId ");
            strSql.append("     LEFT JOIN [dbo].[PaymentRequestsRejectReasons] PRRR WITH (NOLOCK)");
            strSql.append("         ON PRSC.rejectReason = PRRR.reasonId ");
            
            strSql.append("     LEFT JOIN reportConfiguration rp  WITH (NOLOCK) ON rp.reportCode='"+ResourceReport.RESOURCE_PAYMENT_REQUEST_MANAGEMENT+"' ");
            strSql.append("     LEFT JOIN ReportResourceByLanguage rrbl  WITH (NOLOCK) ON ");
            strSql.append("              rrbl.reportId = rp. reportId and PRRR.code = rrbl.KeyMessage and rrbl.Language=").append(language);
            strSql.append(" LEFT OUTER JOIN invoice_types it WITH (NOLOCK) ON it.Code =  PR.invoiceTypeCode  ");
            
            strSql.append(" WHERE  1 = 1 ");

            this.setEntityChain(strSql);

            // If payment id is given we are looking for specific payment, then no other parameters are taken into account
            if (this.paymentRequestId != Long.MIN_VALUE) {
                strSql.append(String.format(" AND PR.ID = %d", this.paymentRequestId));
            } else {
                if (this.agentId != null) {
                    if (!this.allAgentsSelected) {
                        if (this.agentId.size() == 1) {
                            strSql.append(String.format(" AND AG.rep_id = %d", (long) this.agentId.get(0)));
                        } else {
                            strSql.append(" AND AG.rep_id IN (");
                            this.setSqlInList(strSql, this.agentId);
                            strSql.append(')');
                        }
                    }
                } else if (this.subAgentId != null) {
                    if (!this.allSubAgentsSelected) {
                        if (this.subAgentId.size() == 1) {
                            strSql.append(String.format(" AND SA.rep_id = %d", (long) this.subAgentId.get(0)));
                        } else {
                            strSql.append(" AND SA.rep_id IN (");
                            this.setSqlInList(strSql, this.subAgentId);
                            strSql.append(')');
                        }
                    }
                } else if (this.repId != null) {
                    if (!this.allRepsSelected) {
                        if (this.repId.size() == 1) {
                            strSql.append(String.format(" AND R.rep_id = %d", (long) this.repId.get(0)));
                        } else {
                            strSql.append(" AND R.rep_id IN (");
                            this.setSqlInList(strSql, this.repId);
                            strSql.append(')');
                        }
                    }
                } else if (this.merchantId != null) {
                    if (!this.allMerchantsSelected) {
                        if (this.merchantId.size() == 1) {
                            strSql.append(String.format(" AND M.merchant_id = %d", (long) this.merchantId.get(0)));
                        } else {
                            strSql.append(" AND M.merchant_id IN (");
                            this.setSqlInList(strSql, this.merchantId);
                            strSql.append(')');
                        }
                    }
                }

                if ((!this.allPaymentStatusSelected) && (this.paymentStatusId != null)
                        && (!this.paymentStatusId.isEmpty())) {
                    if (this.paymentStatusId.size() == 1) {
                        strSql.append(String.format(" AND PR.StatusID = %d", (long) this.paymentStatusId.get(0)));
                    } else {
                        strSql.append(" AND M.PR.StatusID IN (");
                        this.setSqlInList(strSql, this.paymentStatusId);
                        strSql.append(')');
                    }
                }
                if ((!this.allBanksSelected) && (this.bankId != null)
                        && (!this.bankId.isEmpty())) {
                    if (this.bankId.size() == 1) {
                        strSql.append(String.format(" AND PR.BankID = %d", (long) this.bankId.get(0)));
                    } else {
                        strSql.append(" AND M.PR.BankID IN (");
                        this.setSqlInList(strSql, this.bankId);
                        strSql.append(')');
                    }
                }
                if ((this.documentId != null) && (!this.documentId.trim().equals(""))) {
                    strSql.append(String.format(" AND PR.DocumentNumber = '%s'", this.documentId.trim()));
                }
                if ((this.paymentAmount != null) && (this.paymentAmount != BigDecimal.ZERO)) {
                    strSql.append(String.format(" AND PR.Amount = %f", this.paymentAmount.doubleValue()));
                }
                if ((this.siteId != Long.MIN_VALUE) && (this.siteId != 0)) {
                    strSql.append(String.format(" AND PR.millennium_no = %d", this.siteId));
                }
                if (((this.startDate != null) && (!this.startDate.trim().equals("")))
                        && ((this.endDate != null) && (!this.endDate.trim().equals("")))) {
                    if (this.isScheduling()) {
                        if (qType == QUERY_TYPE.RELATIVE) {
                            strSql.append(String.format(" AND %s ", ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT));
                        } else if ((qType == QUERY_TYPE.FIXED) && (this.vTimeZoneData != null)) {
                            strSql.append(String.format(" AND (PR.RequestDate  >= '%s' ", vTimeZoneData.get(0)));
                            strSql.append(String.format(" AND PR.RequestDate  < '%s')", vTimeZoneData.get(1)));
                        }
                    } else {
                        // If not scheduling, the wild card is removed, and the
                        // current report dates are used
                        int i;
                        while ((i = strSql.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1) {
                            strSql.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
                        }
                        if (this.vTimeZoneData != null) {
                            strSql.append(String.format(" AND (PR.RequestDate  >= '%s' ", vTimeZoneData.get(0)));
                            strSql.append(String.format(" AND PR.RequestDate  < '%s')", vTimeZoneData.get(1)));
                        }
                    }
                }
            }
            retValue = strSql.toString();

        } catch (Exception localException) {
            cat.error("PaymentRequestReport.getSqlString. Exception while generationg sql string : ", localException);
        }
        return retValue;
    }

    @Override
    public String downloadReport() {
        return this.exportToFile();
    }

    @Override
    public ArrayList<ArrayList<ResultField>> getResults() {
        ArrayList<ArrayList<ResultField>> retValue = new ArrayList<ArrayList<ResultField>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            this.setScheduling(false); // If returning results, then scheduling
            String strAccessLevel = this.sessionData.getProperty("access_level");
            // does not have sense
            String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
            cat.debug("Payment request report script : " + strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();
            String tmpValue = "";

            while (rs.next()) {
                ArrayList<ResultField> vecTemp = new ArrayList<ResultField>();
                if (hasColumn(rs, REQUEST_ID)) {
                    ResultField id = new ResultField(REQUEST_ID, (rs.getString(REQUEST_ID) == null) ? "" : rs.getString(REQUEST_ID));
                    vecTemp.add(id);   // [0]
                }
                if (hasColumn(rs, STATUS_CODE_ID)) {
                    ResultField statusCode = new ResultField(STATUS_CODE_ID, (rs.getString(STATUS_CODE_ID) == null) ? "" : rs.getString(STATUS_CODE_ID));
                    vecTemp.add(statusCode);   // [1]
                }
                if (hasColumn(rs, REJECT_REASON_ID)) {
                    ResultField rejectReason = new ResultField(REJECT_REASON_ID, (rs.getString(REJECT_REASON_ID) == null) ? "" : rs.getString(REJECT_REASON_ID));
                    vecTemp.add(rejectReason);   // [2]
                }
                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    if (hasColumn(rs, PROCESS_USER_ID)) {
                        ResultField processUser = new ResultField(PROCESS_USER_ID, (rs.getString(PROCESS_USER_ID) == null) ? "" : rs.getString(PROCESS_USER_ID));
                        vecTemp.add(processUser);
                    }
                }
                if (hasColumn(rs, DBA_ID)) {
                    ResultField dba = new ResultField(DBA_ID, (rs.getString(DBA_ID) == null) ? "" : rs.getString(DBA_ID));
                    vecTemp.add(dba);   // [3 - 4]
                }
                if (hasColumn(rs, MILLENIUM_NO_ID)) {
                    ResultField millenniumNo = new ResultField(MILLENIUM_NO_ID, (rs.getString(MILLENIUM_NO_ID) == null) ? "" : rs.getString(MILLENIUM_NO_ID));
                    vecTemp.add(millenniumNo);   // [4 - 5]
                }
                if (hasColumn(rs, CLERK_ID)) {
                    ResultField clerkid = new ResultField(CLERK_ID, (rs.getString(CLERK_ID) == null) ? "" : rs.getString(CLERK_ID));
                    vecTemp.add(clerkid);
                }
                if (hasColumn(rs, CLERK_NAME_ID)) {
                    ResultField clerkName = new ResultField(CLERK_NAME_ID, (rs.getString(CLERK_NAME_ID) == null) ? "" : rs.getString(CLERK_NAME_ID));
                    vecTemp.add(clerkName);   // [5 - 6]
                }
                if ((hasColumn(rs, CLERK_NAME_ID)) && (hasColumn(rs, CLERK_ID))) {
                    ResultField clerkIdPlusName = new ResultField(CLERK_ID_NAME_ID,
                            String.format("(%s)%s", rs.getString(CLERK_ID), rs.getString(CLERK_NAME_ID)));
                    vecTemp.add(clerkIdPlusName);   // [5 - 6]
                }
                if (hasColumn(rs, DOCUMENT_NUMBER_ID)) {
                    ResultField documentNumber = new ResultField(DOCUMENT_NUMBER_ID, (rs.getString(DOCUMENT_NUMBER_ID) == null) ? "" : rs.getString(DOCUMENT_NUMBER_ID));
                    vecTemp.add(documentNumber);   // [6 - 7]
                }
                if (hasColumn(rs, DOCUMENT_DATE_ID)) {
                    ResultField documentDate = new ResultField(DOCUMENT_DATE_ID, (rs.getTimestamp(DOCUMENT_DATE_ID) == null) ? "" : rs.getTimestamp(DOCUMENT_DATE_ID));
                    vecTemp.add(documentDate);   // [7 - 8]
                }
                if (hasColumn(rs, BANK_NAME_ID)) {
                    ResultField bankName = new ResultField(BANK_NAME_ID, (rs.getString(BANK_NAME_ID) == null) ? "" : rs.getString(BANK_NAME_ID));
                    vecTemp.add(bankName);   // [8 - 9]
                }
                
                if (this.sessionData.getLanguage().equalsIgnoreCase("english")) {
                    if (hasColumn(rs, DEPOSIT_TYPE_DESCRIPTION_ENG)) {
                        ResultField requestDepositTypeDes = new ResultField(DEPOSIT_TYPE_DESCRIPTION_ENG, (rs.getString(DEPOSIT_TYPE_DESCRIPTION_ENG) == null) ? "" : rs.getString(DEPOSIT_TYPE_DESCRIPTION_ENG));
                        vecTemp.add(requestDepositTypeDes);
                    }
                }
                else {
                    if (hasColumn(rs, DEPOSIT_TYPE_DESCRIPTION_SPA)) {
                        ResultField requestDepositTypeDes = new ResultField(DEPOSIT_TYPE_DESCRIPTION_SPA, (rs.getString(DEPOSIT_TYPE_DESCRIPTION_SPA) == null) ? "" : rs.getString(DEPOSIT_TYPE_DESCRIPTION_SPA));
                        vecTemp.add(requestDepositTypeDes);
                    }
                }
                
                if (hasColumn(rs, AMOUNT_ID)) {
                    ResultField amount = new ResultField(AMOUNT_ID, (rs.getString(AMOUNT_ID) == null) ? "" : rs.getString(AMOUNT_ID));
                    vecTemp.add(amount);   // [9 - 10]
                }
                if (hasColumn(rs, REQUEST_DATE_ID)) {
                    ResultField requestDate = new ResultField(REQUEST_DATE_ID, (rs.getTimestamp(REQUEST_DATE_ID) == null) ? "" : rs.getTimestamp(REQUEST_DATE_ID));
                    vecTemp.add(requestDate);   // [10 - 11]
                }
                if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    if (strAccessLevel.equals(DebisysConstants.ISO)) {
                        if (hasColumn(rs, AGENT_ID)) {
                            ResultField agent = new ResultField(AGENT_ID, (rs.getString(AGENT_ID) == null) ? "" : rs.getString(AGENT_ID));
                            vecTemp.add(agent);   // [11 - 12]
                        }
                        if (hasColumn(rs, SUB_AGENT_ID)) {
                            ResultField subAgent = new ResultField(SUB_AGENT_ID, (rs.getString(SUB_AGENT_ID) == null) ? "" : rs.getString(SUB_AGENT_ID));
                            vecTemp.add(subAgent);
                        }
                    } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                        if (hasColumn(rs, SUB_AGENT_ID)) {
                            ResultField subAgent = new ResultField(SUB_AGENT_ID, (rs.getString(SUB_AGENT_ID) == null) ? "" : rs.getString(SUB_AGENT_ID));
                            vecTemp.add(subAgent);
                        }
                    }
                }
                if (hasColumn(rs, REP_ID)) {
                    ResultField rep = new ResultField(REP_ID, (rs.getString(REP_ID) == null) ? "" : rs.getString(REP_ID));
                    vecTemp.add(rep);
                }
                if (hasColumn(rs, MERCHANT_ID)) {
                    ResultField requestMerchantId = new ResultField(MERCHANT_ID, (rs.getString(MERCHANT_ID) == null) ? "" : rs.getString(MERCHANT_ID));
                    vecTemp.add(requestMerchantId);
                }
                if (hasColumn(rs, BANK_ID)) {
                    ResultField requestBankId = new ResultField(BANK_ID, (rs.getString(BANK_ID) == null) ? "" : rs.getString(BANK_ID));
                    vecTemp.add(requestBankId);
                }
                if (hasColumn(rs, TRANSAFER_ACCOUNT)) {
                    ResultField requestBankId = new ResultField(TRANSAFER_ACCOUNT, (rs.getString(TRANSAFER_ACCOUNT) == null) ? "" : rs.getString(TRANSAFER_ACCOUNT));
                    vecTemp.add(requestBankId);
                }
                if (hasColumn(rs, INVOICE_TYPE)) {
                    ResultField requestBankId = new ResultField(INVOICE_TYPE, (rs.getString(INVOICE_TYPE) == null) ? "" : rs.getString(INVOICE_TYPE));
                    vecTemp.add(requestBankId);
                }
                
                retValue.add(vecTemp);
            }
            if (retValue.isEmpty()) {
                retValue = null;
            }
        } catch (TorqueException localTorqueException) {
            cat.error("PaymentRequestReport.getResults Exception : " + localTorqueException.toString());
        } catch (ReportException localReportException) {
            cat.error("PaymentRequestReport.getResults Exception : " + localReportException.toString());
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                    dbConn = null;
                }
            } catch (SQLException cleaningSqlException) {
                cat.error("PaymentRequestReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
            }
        }
        return retValue;
    }

    @Override
    public ArrayList<ColumnReport> getReportRecordHeaders() {
        ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
        String strAccessLevel = this.sessionData.getProperty("access_level");
        try {
            retValue.add(new ColumnReport(REQUEST_ID, Languages.getString("jsp.admin.reports.paymentrequest_search.requestnumber", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(STATUS_CODE_ID, Languages
                    .getString("jsp.admin.reports.paymentrequest_search.status", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(REJECT_REASON_ID, Languages
                    .getString("jsp.admin.reports.paymentrequest_search.rejectReason", sessionData.getLanguage()).toUpperCase(), String.class, false));
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                retValue.add(new ColumnReport(PROCESS_USER_ID, Languages
                        .getString("jsp.admin.reports.paymentRequestReport.processUser", sessionData.getLanguage()).toUpperCase(), String.class, false));
            }
            retValue.add(new ColumnReport(DBA_ID, Languages.getString("jsp.admin.reports.paymentrequest_search.dba",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(MILLENIUM_NO_ID, Languages.getString("jsp.admin.reports.paymentrequest_search.siteid",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(CLERK_NAME_ID, Languages.getString("jsp.admin.reports.paymentrequest_search.clerk",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(DOCUMENT_NUMBER_ID, Languages.getString("jsp.admin.reports.paymentrequest_search.documentnumber",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(DOCUMENT_DATE_ID, Languages.getString("jsp.admin.reports.paymentrequest_search.documentdate",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(BANK_NAME_ID, Languages.getString("jsp.admin.reports.paymentrequest_search.bankname",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            if(this.sessionData.getLanguage().equalsIgnoreCase("english")){
                retValue.add(new ColumnReport(DEPOSIT_TYPE_DESCRIPTION_ENG, Languages.getString("jsp.admin.tools.depositTypes.depositType",sessionData.getLanguage()).toUpperCase(), String.class, false));
            }
            else{
                retValue.add(new ColumnReport(DEPOSIT_TYPE_DESCRIPTION_SPA, Languages.getString("jsp.admin.tools.depositTypes.depositType",sessionData.getLanguage()).toUpperCase(), String.class, false));
            }
            retValue.add(new ColumnReport(AMOUNT_ID, Languages
                    .getString("jsp.admin.reports.paymentrequest_search.amount", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(REQUEST_DATE_ID, Languages
                    .getString("jsp.admin.reports.paymentrequest_search.datetime", sessionData.getLanguage()).toUpperCase(), String.class, false));
            if (sessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    retValue.add(new ColumnReport(AGENT_ID, Languages
                            .getString("jsp.admin.reports.paymentrequest_search.agent", sessionData.getLanguage()).toUpperCase(), String.class, false));
                    retValue.add(new ColumnReport(SUB_AGENT_ID, Languages
                            .getString("jsp.admin.reports.paymentrequest_search.subAgent", sessionData.getLanguage()).toUpperCase(), String.class, false));
                } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
                    retValue.add(new ColumnReport(SUB_AGENT_ID, Languages
                            .getString("jsp.admin.reports.paymentrequest_search.subAgent", sessionData.getLanguage()).toUpperCase(), String.class, false));
                }
            }
            retValue.add(new ColumnReport(REP_ID, Languages
                    .getString("jsp.admin.reports.paymentrequest_search.rep", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(TRANSAFER_ACCOUNT, Languages
                    .getString("jsp.admin.reports.paymentrequest_search.transferAccount", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport(INVOICE_TYPE, Languages
                    .getString("jsp.admin.reports.paymentrequest_search.invoiceType", sessionData.getLanguage()).toUpperCase(), String.class, false));            
        } catch (Exception localException) {
            cat.error("PaymentRequestReport.getReportRecordHeaders Exception : " + localException);
        }

        return retValue;
    }

    @Override
    public ArrayList<String> getTitles() {
        ArrayList<String> retValue = new ArrayList<String>();

        try {
            retValue.add(Languages.getString("jsp.admin.reports.paymentrequest_search.reportTitle", this.sessionData.getLanguage()));
            //Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
            if (this.vTimeZoneData != null) {
                String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
                        + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
                retValue.add(noteTimeZone);
            }
            retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
        } catch (NumberFormatException localNumberFormatException) {
            cat.error("getTitles Exception : " + localNumberFormatException.toString());
        } catch (Exception localException) {
            cat.error("getTitles Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    public boolean isUsingDateLimitInterval() {
        return true;
    }

    @Override
    public int getLimitDays() {
        String reportId = sessionData.getProperty("reportId");
        this.limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
        return this.limitDays;
    }

    @Override
    public boolean scheduleReport() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
