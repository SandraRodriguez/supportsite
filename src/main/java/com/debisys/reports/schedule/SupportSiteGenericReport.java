package com.debisys.reports.schedule;

import static com.debisys.reports.schedule.ReportDownloader.cat;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author fernandob
 * @param <T> Type of the record object pojo
 */
public abstract class SupportSiteGenericReport<T> extends ReportDownloader implements ISupportSiteGenericReport<T> {

    protected boolean scheduling;
    protected String strCurrentDate;
    protected int limitDays;
    protected boolean usingDateLimitInterval;
    protected Vector<String> vTimeZoneData;
    protected String startDate;
    protected String endDate;

    public boolean isScheduling() {
        return scheduling;
    }

    public void setScheduling(boolean scheduling) {
        this.scheduling = scheduling;
    }

    public String getStrCurrentDate() {
        return strCurrentDate;
    }

    public void setStrCurrentDate(String strCurrentDate) {
        this.strCurrentDate = strCurrentDate;
    }

    public Vector<String> getvTimeZoneData() {
        return vTimeZoneData;
    }

    public void setvTimeZoneData(Vector<String> vTimeZoneData) {
        this.vTimeZoneData = vTimeZoneData;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public SupportSiteGenericReport() {
        this.scheduling = false;
        this.strCurrentDate = "";
        this.limitDays = 90;
        this.usingDateLimitInterval = true;
        this.vTimeZoneData = null;
        this.startDate = "";
        this.endDate = "";
    }

    public SupportSiteGenericReport(boolean scheduling, String strCurrentDate, int limitDays,
            boolean usingDateLimitInterval, Vector<String> vTimeZoneData, String startDate, String endDate) {
        this.scheduling = scheduling;
        this.strCurrentDate = strCurrentDate;
        this.limitDays = limitDays;
        this.usingDateLimitInterval = usingDateLimitInterval;
        this.vTimeZoneData = vTimeZoneData;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    protected synchronized void setDatesWithLimit() {
        String tmpStartDate = this.startDate;
        String tmpEndDate = this.endDate;

        try {
            if ((!this.usingDateLimitInterval)
                    && ((this.startDate != null) && (!this.startDate.isEmpty()))
                    && ((this.endDate != null) && (!this.endDate.isEmpty()))) {
                //String strAccessLevel = this.sessionData.getProperty("access_level");
                this.getLimitDays();
                int numDays = DateUtil.getDaysBetween(this.startDate, this.endDate);
                if (numDays > this.limitDays) {
                    this.startDate = DateUtil.addSubtractDays(this.endDate, -this.limitDays);
                }
                this.vTimeZoneData = new Vector<String>();
                this.vTimeZoneData.add(this.startDate + " 00:00:00.000");
                this.vTimeZoneData.add(this.endDate + " 23:59:59.900");

                this.usingDateLimitInterval = true;
            }
        } catch (Exception localException) {
            cat.warn("Error while setting report limits", localException);
            this.startDate = tmpStartDate;
            this.endDate = tmpEndDate;
            this.usingDateLimitInterval = false;
        }
    }

    @Override
    protected boolean writeDownloadRecordsHeaders() throws IOException {
        boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            this.writetofile(fc, "#,");
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, columnHeaders.get(i).getLanguageDescription());
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
    }

    protected void setSqlInList(StringBuilder strSql, ArrayList<Long> list) {
        if ((strSql != null) && (list != null)) {
            for (int i = 0; i < list.size(); i++) {
                strSql.append(list.get(i));
                if (i < (list.size() - 1)) {
                    strSql.append(',');
                }
            }
        }
    }

    @Override
    public String downloadReport() {
        return this.exportToFile();
    }

    @Override
    public boolean isUsingDateLimitInterval() {
        return true;
    }

    @Override
    public int getLimitDays() {
        String reportId = sessionData.getProperty("reportId");
        this.limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
        return this.limitDays;
    }

}
