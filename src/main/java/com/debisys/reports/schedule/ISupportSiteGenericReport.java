/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.schedule;

import java.util.ArrayList;
import com.debisys.utils.ColumnReport;


/**
 *
 * @author fernandob
 */
public interface ISupportSiteGenericReport<T> {
	// constants
	public enum QUERY_TYPE{NORMAL, FIXED, RELATIVE};
	
	public String generateSqlString(QUERY_TYPE qType);
	
	public boolean scheduleReport();
	
	public String downloadReport();
	
	public ArrayList<T> getResults();
	
	public ArrayList<ColumnReport> getReportRecordHeaders();
	
	public ArrayList<String> getTitles();
	
	public boolean isUsingDateLimitInterval();
	
	public int getLimitDays();
    
}
