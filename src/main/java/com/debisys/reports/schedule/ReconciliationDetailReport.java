/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.schedule;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import static com.debisys.reports.schedule.ReportDownloader.cat;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.TimeZone;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletContext;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author fernandob
 */
public class ReconciliationDetailReport extends ReportDownloader implements ISupportSiteReport {

    private enum resultTotalsIndexes {

        openAvailableTotal, inwardsIITotal,
        inwardsIETotal, inwardsEETotal,
        outwardsAllTotal, commissionTotal,
        changedIEBalanceTotal, salesTotal,
        closingAvailableTotal
    };

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

    // instance fields
    private String startDate;
    private String endDate;

    private boolean scheduling;
    private String strCurrentDate;
    private int limitDays;
    private boolean usingDateLimitInterval;
    private Vector<String> vTimeZoneData;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isScheduling() {
        return scheduling;
    }

    public void setScheduling(boolean scheduling) {
        this.scheduling = scheduling;
    }

    public String getStrCurrentDate() {
        return strCurrentDate;
    }

    public void setStrCurrentDate(String strCurrentDate) {
        this.strCurrentDate = strCurrentDate;
    }

    private ReconciliationDetailReport() {
        this.startDate = null;
        this.endDate = null;
        this.scheduling = false;
        this.strCurrentDate = null;
    }

    public ReconciliationDetailReport(SessionData sessionData, ServletContext pContext,
            String startDate, String endDate, boolean scheduling) {
        this();
        this.sessionData = sessionData;
        this.setContext(pContext);
        this.strRefId = sessionData.getProperty("ref_id");
        this.startDate = startDate;
        this.endDate = endDate;
        this.scheduling = scheduling;
        this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
    }

    @Override
    protected boolean writeDownloadFileHeader() throws IOException {
        boolean retValue = false;
        try {
            this.writetofile(this.fc, String.format("%s\n",
                    Languages.getString("jsp.admin.reports.transactions.reconcile_detail.title", sessionData.getLanguage()).toUpperCase()));
            this.writetofile(fc, String.format("%s,%s\n",
                    Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()),
                    sessionData.getUser().getUsername()));
            this.writetofile(fc, String.format("%s,%s",
                    Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()),
                    this.getStrCurrentDate()));
            this.writetofile(fc, "\n");
            this.writetofile(fc, Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\n");

            if (sessionData.getProperty("repName") != null) {
                this.writetofile(fc, "" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
                        + "," + sessionData.getProperty("repName") + "\n");
            } else {
                this.writetofile(fc, Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
                        + "," + sessionData.getUser().getCompanyName() + "\n");
            }

            this.writetofile(fc, Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + ","
                    + this.startDate + "\n");
            this.writetofile(fc, Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + ","
                    + this.endDate + "\n");

            retValue = true;
        } catch (Exception localException) {
            cat.error("writeDownloadFileHeader Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecordsHeaders() throws IOException {
        boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            this.writetofile(fc, "#,");
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, "" + columnHeaders.get(i).getLanguageDescription() + "");
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecords() throws IOException {
        boolean retValue = false;
        Vector<Vector<Object>> reportResults = this.getResults();

        try {
            if ((reportResults != null) && (reportResults.size() > 0)) {
                for (int i = 0; i < reportResults.size(); i++) {
                    Vector<Object> currentRecord = reportResults.get(i);
                    // Write the line number
                    this.writetofile(fc, "" + String.valueOf(i + 1) + ",");
                    this.writetofile(fc, "" + currentRecord.get(0).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(1).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(2).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(3).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(4).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(5).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(6).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(7).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(8).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(9).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(10).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(11).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(12).toString().trim() + "\n");
                }
                retValue = true;
            }
        } catch (NumberFormatException localNumberException) {
            cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
        } catch (Exception localException) {
            cat.error("writeDownloadRecords Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadFileFooter() throws IOException {
        // Already containded on the last line of the result set
        return true;
    }

    private synchronized void setDatesWithLimit() {
        String tmpStartDate = this.startDate;
        String tmpEndDate = this.endDate;

        try {
            if (!this.usingDateLimitInterval) {
                String strAccessLevel = this.sessionData.getProperty("access_level");
                this.getLimitDays();
                int numDays = DateUtil.getDaysBetween(this.startDate, this.endDate);
                if (numDays > this.limitDays) {
                    this.endDate = DateUtil.addSubtractDays(startDate, this.limitDays);
                }
                this.vTimeZoneData = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.startDate, this.endDate, false);
                this.usingDateLimitInterval = true;
            }
        } catch (Exception localException) {
            cat.warn("Error while setting report limits", localException);
            this.startDate = tmpStartDate;
            this.endDate = tmpEndDate;
            this.usingDateLimitInterval = false;
        }
    }

    @Override
    public String generateSqlString(QUERY_TYPE qType) {
        String retValue = null;
        StringBuilder strSql = new StringBuilder();
        String s_merchantsSQL = "";
        String s_entitiesSQL = "";

        try {
            this.setDatesWithLimit();
            String sStartDate = this.vTimeZoneData.get(0).substring(0, this.vTimeZoneData.get(0).indexOf(" "));
            String sEndDate = this.vTimeZoneData.get(1).substring(0, this.vTimeZoneData.get(1).indexOf(" "));

            String queryStartDate = String.format("%s", sStartDate);
            String queryEndDate = String.format("%s", sEndDate);

            if (((this.startDate != null) && (this.startDate.length() > 0)) && ((this.endDate != null) && (this.endDate.length() > 0))) {
                if (this.isScheduling()) {
                    if (qType == QUERY_TYPE.RELATIVE) {
                        queryStartDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE;
                        queryEndDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE;
                    } else if (qType == QUERY_TYPE.FIXED) {
                        queryStartDate = String.format("%s", sStartDate);
                        queryEndDate = String.format("%s", sEndDate);
                    }
                } else {
                    queryStartDate = String.format("%s", sStartDate);
                    queryEndDate = String.format("%s", sEndDate);
                }
            } else {
                queryStartDate = String.format("%s", sStartDate);
                queryEndDate = String.format("%s", sEndDate);
            }

            String strRefId = this.sessionData.getProperty("ref_id");
            String strAccessLevel = this.sessionData.getProperty("access_level");
            String strDistChainType = this.sessionData.getProperty("dist_chain_type");

            // Main query
            strSql.append(" IF OBJECT_ID('tempdb..#entityTypes', 'U') IS NOT NULL ");
            strSql.append("  BEGIN ");
            strSql.append(" 	TRUNCATE TABLE #entityTypes ");
            strSql.append(" 	DROP TABLE #entityTypes ");
            strSql.append("  END ");

            strSql.append(" CREATE TABLE #entityTypes( ");
            strSql.append(" 	entityCode CHAR NOT NULL PRIMARY KEY, ");
            strSql.append(" 	entityName VARCHAR(50) ");
            strSql.append(" ) ");

            strSql.append(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('A', 'Agent')   ");
            strSql.append(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('S', 'SubAgent') ");
            strSql.append(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('R', 'Rep') ");
            strSql.append(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('M', 'Merchant') ");

            strSql.append(" IF OBJECT_ID('tempdb..#recDetailResults', 'U') IS NOT NULL ");
            strSql.append("  BEGIN ");
            strSql.append(" 	TRUNCATE TABLE #recDetailResults ");
            strSql.append(" 	DROP TABLE #recDetailResults ");
            strSql.append("  END ");

            strSql.append(" CREATE TABLE #recDetailResults( ");
            strSql.append(" 	resultId INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ");
            strSql.append(" 	entityId NUMERIC(15,0), ");
            strSql.append(" 	entityAccountType CHAR(1), ");
            strSql.append(" 	businessName VARCHAR(50), ");
            strSql.append(" 	entityType CHAR(1), ");
            strSql.append(" 	openingAvailable MONEY, ");
            strSql.append(" 	inwardsAll MONEY, ");
            strSql.append(" 	inwardsII MONEY, ");
            strSql.append(" 	inwardsIE MONEY, ");
            strSql.append(" 	inwardsEE MONEY, ");
            strSql.append(" 	outwardsAll MONEY, ");
            strSql.append(" 	commission MONEY, ");
            strSql.append(" 	changedIEBalance MONEY, ");
            strSql.append(" 	sales MONEY, ");
            strSql.append(" 	closingAvailable MONEY ");
            strSql.append(" ) ");

            strSql.append(" BEGIN TRY ");

            if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                s_merchantsSQL = DbUtil.sqlSelISO5Merch(strRefId);
                s_entitiesSQL = DbUtil.sqlSelAllISO5Entities(strRefId);
            } else {
                s_merchantsSQL = DbUtil.sqlSelISO3Merch(strRefId);
                s_entitiesSQL = DbUtil.sqlSelAllISO3Entities(strRefId);
            }

            // ======================== RETRIEVE AGENT DATA ====================================================
            String sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "r.BusinessName").replaceAll("_ENTITYNAME2_", "r.BusinessName")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Reps r WITH (NOLOCK) ON heb.EntityID = r.Rep_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_entitiesSQL + " AND Type = 4").replaceAll("_ENTITYTYPE_", "A");
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");

            strSql.append(sSql);

            // ======================== RETRIEVE SUBAGENT DATA==================================================
            sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "r.BusinessName").replaceAll("_ENTITYNAME2_", "r.BusinessName")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Reps r WITH (NOLOCK) ON heb.EntityID = r.Rep_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_entitiesSQL + " AND Type = 5").replaceAll("_ENTITYTYPE_", "S");
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");
            strSql.append(sSql);

            // ======================== RETRIEVE REP DATA ======================================================
            sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "r.BusinessName").replaceAll("_ENTITYNAME2_", "r.BusinessName")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Reps r WITH (NOLOCK) ON heb.EntityID = r.Rep_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_entitiesSQL + " AND Type = 1").replaceAll("_ENTITYTYPE_", "R");
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");
            strSql.append(sSql);

            // ======================== RETRIEVE MERCHANT DATA =================================================
            sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "m.DBA AS BusinessName").replaceAll("_ENTITYNAME2_", "m.DBA")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Merchants m WITH (NOLOCK) ON heb.EntityID = m.Merchant_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_merchantsSQL).replaceAll("_ENTITYTYPE_", "M");
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");
            strSql.append(sSql);

            // *********************** SELECT THE FINAL RESULT FROM TEMPORAL TABLE *****************************
            strSql.append(String.format(" SELECT CASE RDR.entityAccountType WHEN 0 THEN '%s' ELSE '%s' END AS EntityAccountType, ",
                    Languages.getString("jsp.admin.reports.transactions.reconcile_summary.internal", sessionData.getLanguage()),
                    Languages.getString("jsp.admin.reports.transactions.reconcile_summary.external", sessionData.getLanguage())));
            strSql.append("  RDR.businessName, ET.entityName, RDR.entityId, RDR.openingAvailable, ");
            strSql.append("  RDR.inwardsII, RDR.inwardsIE, RDR.inwardsEE, ");
            strSql.append("  RDR.outwardsAll, RDR.commission, RDR.changedIEBalance, ");
            strSql.append("  RDR.sales, RDR.closingAvailable , RDR.inwardsAll, RDR.entityType ");
            strSql.append(" FROM #recDetailResults RDR ");
            strSql.append("  INNER JOIN #entityTypes ET ON RDR.entityType = ET.entityCode ");
            strSql.append(" ORDER BY  RDR.resultId");

            strSql.append(" END TRY ");
            strSql.append(" BEGIN CATCH ");
            strSql.append("   PRINT 'ERROR NUMBER : ' ");
            strSql.append("   PRINT ERROR_NUMBER() ");
            strSql.append("   PRINT 'ERROR SEVERITY : ' ");
            strSql.append("   PRINT ERROR_SEVERITY() ");
            strSql.append("   PRINT 'ERROR STATE : '");
            strSql.append("   PRINT ERROR_STATE() ");
            strSql.append("   PRINT 'ERROR PROCEDURE : '");
            strSql.append("   PRINT ERROR_PROCEDURE() ");
            strSql.append("   PRINT 'ERROR LINE : '");
            strSql.append("   PRINT ERROR_LINE()");
            strSql.append("   PRINT 'ERROR MESSAGE : '");
            strSql.append("   PRINT ERROR_MESSAGE()");
            strSql.append(" END CATCH ");
            strSql.append(" TRUNCATE TABLE #entityTypes ");
            strSql.append(" DROP TABLE #entityTypes ");
            strSql.append(" TRUNCATE TABLE #recDetailResults ");
            strSql.append(" DROP TABLE #recDetailResults ");

            retValue = strSql.toString();
        } catch (Exception localException) {
            cat.error("ReconciliationDetailReport.getSqlString. Exception while generationg sql string : ", localException);
        }
        return retValue;
    }

    @Override
    public boolean scheduleReport() {
        boolean retValue = false;

        try {
            this.setScheduling(true);
            String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
            String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

            ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_RECONCILIATION_DETAIL_REPORT, 1);
            scheduleReport.setNameDateTimeColumn("entrydate");
            scheduleReport.setRelativeQuery(relativeQuery);
            scheduleReport.setFixedQuery(fixedQuery);
            TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
            this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
            retValue = true;
        } catch (Exception localException) {
            cat.error("ReconciliationDetailReport.scheduleReport Exception : " + localException.toString());
        }

        return retValue;
    }

    @Override
    public String downloadReport() {
        return this.exportToFile();
    }

    @Override
    public Vector<Vector<Object>> getResults() {
        Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        double totalSums[] = new double[9];
        String entityAccountType, businessName, entityName, entityId,
               openAvailable, inwardsII, inwardsIE, inwardsEE,
                outwardsAll, commission, changedIEBalance, sales,
                closingAvailable;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            this.setScheduling(false); // If returning results, then scheduling
            // does not have sense
            String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();

                entityAccountType = ((rs.getString("EntityAccountType") == null) || (rs.wasNull())) ? "" : rs.getString("EntityAccountType").trim();
                vecTemp.add(entityAccountType);
                businessName = ((rs.getString("businessName") == null) || (rs.wasNull())) ? "" : rs.getString("businessName").trim();
                vecTemp.add(businessName);
                entityName = ((rs.getString("entityName") == null) || (rs.wasNull())) ? "" : rs.getString("entityName").trim();
                vecTemp.add(entityName);
                entityId = ((rs.getString("entityId") == null) || (rs.wasNull())) ? "" : rs.getString("entityId").trim();
                vecTemp.add(entityId);

                openAvailable = ((rs.getString("openingAvailable") == null) || (rs.wasNull())) ? "0" : rs.getString("openingAvailable").trim();
                totalSums[resultTotalsIndexes.openAvailableTotal.ordinal()] += Double.valueOf(openAvailable);
                vecTemp.add(openAvailable);
                inwardsII = ((rs.getString("inwardsII") == null) || (rs.wasNull())) ? "0" : rs.getString("inwardsII").trim();
                totalSums[resultTotalsIndexes.inwardsIITotal.ordinal()] += Double.valueOf(inwardsII);
                vecTemp.add(inwardsII);
                inwardsIE = ((rs.getString("inwardsIE") == null) || (rs.wasNull())) ? "0" : rs.getString("inwardsIE").trim();
                totalSums[resultTotalsIndexes.inwardsIETotal.ordinal()] += Double.valueOf(inwardsIE);
                vecTemp.add(inwardsIE);
                inwardsEE = ((rs.getString("inwardsEE") == null) || (rs.wasNull())) ? "0" : rs.getString("inwardsEE").trim();
                totalSums[resultTotalsIndexes.inwardsEETotal.ordinal()] += Double.valueOf(inwardsEE);
                vecTemp.add(inwardsEE);
                outwardsAll = ((rs.getString("outwardsAll") == null) || (rs.wasNull())) ? "0" : rs.getString("outwardsAll").trim();
                totalSums[resultTotalsIndexes.outwardsAllTotal.ordinal()] += Double.valueOf(outwardsAll);
                vecTemp.add(outwardsAll);
                commission = ((rs.getString("commission") == null) || (rs.wasNull())) ? "0" : rs.getString("commission").trim();
                totalSums[resultTotalsIndexes.commissionTotal.ordinal()] += Double.valueOf(commission);
                vecTemp.add(commission);
                changedIEBalance = ((rs.getString("changedIEBalance") == null) || (rs.wasNull())) ? "0" : rs.getString("changedIEBalance").trim();
                totalSums[resultTotalsIndexes.changedIEBalanceTotal.ordinal()] += Double.valueOf(changedIEBalance);
                vecTemp.add(changedIEBalance);
                sales = ((rs.getString("sales") == null) || (rs.wasNull())) ? "0" : rs.getString("sales").trim();
                totalSums[resultTotalsIndexes.salesTotal.ordinal()] += Double.valueOf(sales);
                vecTemp.add(sales);
                closingAvailable = ((rs.getString("closingAvailable") == null) || (rs.wasNull())) ? "0" : rs.getString("closingAvailable").trim();
                totalSums[resultTotalsIndexes.closingAvailableTotal.ordinal()] += Double.valueOf(closingAvailable);
                vecTemp.add(closingAvailable);
                retValue.add(vecTemp);
            }
            if (retValue.isEmpty()) {
                retValue = null;
            } else {

                Vector totalsLine = new Vector();
                totalsLine.add(Languages.getString("jsp.admin.reports.transactions.reconcile_detail.grandTotal", sessionData.getLanguage()));
                for (int i = 0; i < 3; i++) {
                    totalsLine.add("");
                }
                for (int totalsIdx = 0; totalsIdx < totalSums.length; totalsIdx++) {
                    totalsLine.add(String.valueOf(totalSums[totalsIdx]));
                }

                retValue.add(totalsLine);
            }
        } catch (TorqueException localTorqueException) {
            cat.error("ReconciliationDetailReport.getResults Exception : " + localTorqueException.toString());
        } catch (ReportException localReportException) {
            cat.error("ReconciliationDetailReport.getResults Exception : " + localReportException.toString());
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                    dbConn = null;
                }
            } catch (SQLException cleaningSqlException) {
                cat.error("ReconciliationDetailReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
            }
        }
        return retValue;
    }

    @Override
    public ArrayList<ColumnReport> getReportRecordHeaders() {
        ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
        try {
            retValue.add(new ColumnReport("EntityAccountType", Languages.getString("jsp.admin.reports.transactions.reconcile_detail.type", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("businessName", Languages.getString("jsp.admin.reports.transactions.reconcile_detail.business_name",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("entityName", Languages.getString("jsp.admin.reports.transactions.reconcile_detail.account",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("entityId", Languages.getString("jsp.admin.reports.transactions.reconcile_detail.emida_id",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("openingAvailable", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.opening_bal",
                    sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("inwardsII", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.inwards_i_to_i",
                    sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("inwardsIE", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.inwards_i_to_e",
                    sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("inwardsEE", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.inwards_e_to_e", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("outwardsAll", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.outwards_all", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("commission", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.commissions_applied", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("changedIEBalance", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.changed_ie_bal", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("sales", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.retail_sales", sessionData.getLanguage()).toUpperCase(), Double.class, true));
            retValue.add(new ColumnReport("closingAvailable", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.closing_balance", sessionData.getLanguage()).toUpperCase(), Double.class, true));
        } catch (Exception localException) {
            cat.error("ReconciliationDetailReport.getReportRecordHeaders Exception : " + localException);
        }
        return retValue;
    }

    @Override
    public ArrayList<String> getTitles() {
        ArrayList<String> retValue = new ArrayList<String>();

        try {
            retValue.add(Languages.getString("jsp.admin.reports.transactions.reconcile_detail.title", this.sessionData.getLanguage()));
            Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData
                    .getProperty("ref_id")));
            String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage())
                    + ":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
            retValue.add(noteTimeZone);
            retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
            retValue.add(Languages.getString("jsp.admin.reports.transactions.reconcile_summary.report_generated", this.sessionData.getLanguage())
                    + " " + this.strCurrentDate);
        } catch (NumberFormatException localNumberFormatException) {
            cat.error("getTitles Exception : " + localNumberFormatException.toString());
        } catch (Exception localException) {
            cat.error("getTitles Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    public boolean isUsingDateLimitInterval() {
        return true;
    }

    @Override
    public int getLimitDays() {
        String reportId = sessionData.getProperty("reportId");
        this.limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
        return this.limitDays;
    }

}
