/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.schedule;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import static com.debisys.reports.schedule.ReportDownloader.cat;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.TimeZone;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletContext;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author fernandob
 */
public class PinRemainingInventoryReport extends ReportDownloader implements ISupportSiteReport {

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

    private boolean scheduling;
    private String reportTitle;
    private String strCurrentDate;
    private String isoName;
    private long selectedProductId;
    private String selectedProductName;
    private boolean paging;
    private int pageToGet;
    private int pageLength;
    private int recordCount;

    // Properties
    public static ResourceBundle getSql_bundle() {
        return sql_bundle;
    }

    public static void setSql_bundle(ResourceBundle sql_bundle) {
        PinRemainingInventoryReport.sql_bundle = sql_bundle;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getStrCurrentDate() {
        return strCurrentDate;
    }

    public void setStrCurrentDate(String strCurrentDate) {
        this.strCurrentDate = strCurrentDate;
    }

    public String getIsoName() {
        return isoName;
    }

    public void setIsoName(String isoName) {
        this.isoName = isoName;
    }

    public long getSelectedProductId() {
        return selectedProductId;
    }

    public void setSelectedProductId(long selectedProductId) {
        this.selectedProductId = selectedProductId;
    }

    public String getSelectedProductName() {
        return selectedProductName;
    }

    public void setSelectedProductName(String selectedProductName) {
        this.selectedProductName = selectedProductName;
    }

    public boolean isPaging() {
        return paging;
    }

    public void setPaging(boolean paging) {
        this.paging = paging;
        if (!this.paging) {
            this.setPageLength(0);
            this.setPageToGet(0);
        }
    }

    public int getPageToGet() {
        return pageToGet;
    }

    public void setPageToGet(int pageToGet) {
        this.pageToGet = pageToGet;
    }

    public int getPageLength() {
        return pageLength;
    }

    public void setPageLength(int pageLength) {
        this.pageLength = pageLength;
    }

    public boolean isScheduling() {
        return scheduling;
    }

    public void setScheduling(boolean scheduling) {
        this.scheduling = scheduling;
        if (this.isScheduling()) {
            this.setPaging(false);
        }
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    @Override
    protected boolean writeDownloadFileHeader() throws IOException {
        boolean retValue = false;
        try {
            this.writetofile(this.fc, "\"" + this.reportTitle + "\"\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
                    + this.sessionData.getUser().getUsername() + "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
                    + this.strCurrentDate + "\n");
            this.writetofile(fc, "\n");
            this.writetofile(fc, String.format("\"%s\" : %s\n",
                    Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()),
                    this.selectedProductName));
            retValue = true;
        } catch (Exception localException) {
            cat.error("PinRemainingInventoryReport.writeDownloadFileHeader Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecordsHeaders() throws IOException {
        boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = this.getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            this.writetofile(fc, "\"#\",");
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecords() throws IOException {
        boolean retValue = false;
        this.setPaging(false);
        Vector<Vector<Object>> reportResults = this.getResults();

        try {
            if ((reportResults != null) && (reportResults.size() > 0)) {
                for (int i = 0; i < reportResults.size(); i++) {
                    Vector<Object> currentRecord = reportResults.get(i);
                    // Write the line number
                    this.writetofile(fc, String.valueOf(i + 1) + ",");

                    String controlNo = String.valueOf(currentRecord.get(0));
                    String importDate = String.valueOf(currentRecord.get(1));
                    this.writetofile(fc, String.format("%s,%s\n", controlNo, importDate));
                }
                retValue = true;
            }
        } catch (NumberFormatException localNumberException) {
            cat.error("PinRemainingInventoryReport.writeDownloadRecords Exception : " + localNumberException.toString());
        } catch (Exception localException) {
            cat.error("PinRemainingInventoryReport.writeDownloadRecords Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadFileFooter() throws IOException {
        return true;
    }

    @Override
    public String generateSqlString(QUERY_TYPE qType) {
        String retValue = null;
        StringBuffer strSQL = new StringBuffer();
        try {
            strSQL.append("SELECT [control_no], CONVERT(datetime, [insert_date], 121) AS [insert_date] ");
            strSQL.append(" FROM [dbo].[Pins] WITH(NOLOCK) ");
            strSQL.append(" WHERE "); // This allows us to treat anything
            strSQL.append(String.format(" [product_id] = %s", this.selectedProductId));
            strSQL.append(" AND [activation_date] IS NULL ");
            strSQL.append(" AND [pinStatusId] = 1 ");
            strSQL.append(" ORDER BY [control_no] ");
            cat.debug(strSQL);
            retValue = strSQL.toString();

        } catch (Exception localException) {
            cat.error("PinReturnRequestReport.getSqlString. Exception while generating sql string : " + localException.toString());
        }

        return retValue;
    }

    @Override
    public boolean scheduleReport() {
        boolean retValue = false;

        try {
            this.setScheduling(true);
            String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
            String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

            ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_PIN_REMAINING_INVENTORY_REPORT, 1);
            scheduleReport.setNameDateTimeColumn("[insert_date]");
            scheduleReport.setRelativeQuery(relativeQuery);
            scheduleReport.setFixedQuery(fixedQuery);
            TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
            this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
            retValue = true;
        } catch (Exception localException) {
            cat.error("PinRemainingInventoryReport.scheduleReport Exception : " + localException.toString());
        }

        return retValue;
    }

    @Override
    public String downloadReport() {
        return this.exportToFile();
    }

    @Override
    public Vector<Vector<Object>> getResults() {
        Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            this.setScheduling(false); // If returning results, then scheduling
            // does not have sense
            String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
            cat.debug("Pin remaining inventory query : " + strSQL);

            if (this.isPaging()) {
                pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = pstmt.executeQuery();
                this.recordCount = DbUtil.getRecordCount(rs);
                if (!(this.pageLength > 0)) {
                    this.pageLength = this.recordCount;
                }
                int i = 0;
                if (this.recordCount > 0) {
                    rs.absolute(DbUtil.getRowNumber(rs, this.pageLength, this.recordCount, this.pageToGet));
                    do {
                        Vector vecTemp = new Vector();
                        vecTemp.add(0, rs.getString("control_no"));
                        vecTemp.add(1, DateUtil.formatDateTime(rs.getTimestamp("insert_date")));
                        retValue.add(vecTemp);
                        i++;
                    } while (rs.next() && i < this.pageLength);
                }

            } else {
                pstmt = dbConn.prepareStatement(strSQL);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    Vector vecTemp = new Vector();
                    vecTemp.add(0, rs.getString("control_no"));
                    vecTemp.add(1, DateUtil.formatDateTime(rs.getTimestamp("insert_date")));
                    retValue.add(vecTemp);
                }
            }

            if (retValue.isEmpty()) {
                retValue = null;
            }
        } catch (TorqueException localTorqueException) {
            cat.error("PinRemainingInventoryReport.getResults Exception : " + localTorqueException.toString());
        } catch (ReportException localReportException) {
            cat.error("PinRemainingInventoryReport.getResults Exception : " + localReportException.toString());
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            cat.error("PinRemainingInventoryReport.getResults Exception : " + localException.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                    dbConn = null;
                }
            } catch (SQLException cleaningSqlException) {
                DbUtil.processSqlException(cleaningSqlException);
            }
        }

        return retValue;
    }

    @Override
    public ArrayList<ColumnReport> getReportRecordHeaders() {
        ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
        try {
            retValue.add(new ColumnReport("control_no", Languages.getString("jsp.admin.reports.pin_remaining_inventory.control_number", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("insert_date", Languages.getString("jsp.admin.reports.pin_remaining_inventory.import_date",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
        } catch (Exception localException) {
            cat.error("PinRemainingInventoryReport.getReportRecordHeaders Exception : " + localException);
        }
        return retValue;
    }

    @Override
    public ArrayList<String> getTitles() {
        ArrayList<String> retValue = new ArrayList<String>();

        try {
            retValue.add(Languages.getString("jsp.admin.reports.pin_remaining_inventory.report_type_title_date", this.sessionData.getLanguage()));
            retValue.add(this.reportTitle);
            Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
            String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
                    + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
            retValue.add(noteTimeZone);
            retValue.add(Languages.getString("jsp.admin.iso_name", this.sessionData.getLanguage())
                    + " : " + this.isoName);
            retValue.add(String.format("%s : %s",
                    Languages.getString("jsp.admin.product", this.sessionData.getLanguage()),
                    this.selectedProductName));
        } catch (NumberFormatException localNumberFormatException) {
            cat.error("PinRemainingInventoryReport.getTitles Exception : " + localNumberFormatException.toString());
        } catch (Exception localException) {
            cat.error("PinRemainingInventoryReport.getTitles Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    public boolean isUsingDateLimitInterval() {
        return false;
    }

    @Override
    public int getLimitDays() {
        return 0;
    }

    public PinRemainingInventoryReport() {
        this.scheduling = false;
        this.reportTitle = "";
        this.strCurrentDate = "";
        this.isoName = "";
        this.selectedProductId = Long.MIN_VALUE;
        this.selectedProductName = "";
        this.paging = false;
        this.pageToGet = 0;
        this.pageLength = 0;
        this.recordCount = 0;
    }

    public PinRemainingInventoryReport(SessionData sessionData, ServletContext pContext, boolean scheduling,
            String reportTitle, String strCurrentDate, String isoName, long selectedProductId, String selectedProductName,
            boolean paging, int pageToGet, int pageLength) {
        this();
        this.sessionData = sessionData;
        this.setContext(pContext);
        this.strRefId = sessionData.getProperty("ref_id");
        this.scheduling = scheduling;
        this.reportTitle = reportTitle;
        this.strCurrentDate = strCurrentDate;
        this.isoName = isoName;
        this.selectedProductId = selectedProductId;
        this.selectedProductName = selectedProductName;
        this.paging = paging;
        this.pageToGet = pageToGet;
        this.pageLength = pageLength;
    }

}
