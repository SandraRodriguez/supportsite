/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.schedule;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import static com.debisys.reports.schedule.ReportDownloader.cat;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import static com.debisys.utils.DbUtil.hasColumn;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.HTMLEncoder;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.TimeZone;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletContext;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author fernandob
 */
public class MerchantCreditAccountStatusDetail extends ReportDownloader implements ISupportSiteReport {

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

    private boolean scheduling;
    private String deploymentType;
    private String customConfigType;
    private String startDate;
    private String formattedStartDate;
    private String endDate;
    private String formattedEndDate;
    private String merchantId;
    private String merchantDba;
    private String reportTitle;
    private String strCurrentDate;
    private boolean usingDateLimitInterval;
    private int limitDays;
    private boolean paging;
    private int pageToGet;
    private int pageLength;
    private int recordCount;

    public boolean isScheduling() {
        return scheduling;
    }

    public void setScheduling(boolean scheduling) {
        this.scheduling = scheduling;
        if (this.isScheduling()) {
            this.setPaging(false);
        }

    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFormattedStartDate() {
        return formattedStartDate;
    }

    public void setFormattedStartDate(String formattedStartDate) {
        this.formattedStartDate = formattedStartDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFormattedEndDate() {
        return formattedEndDate;
    }

    public void setFormattedEndDate(String formattedEndDate) {
        this.formattedEndDate = formattedEndDate;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantDba() {
        return merchantDba;
    }

    public void setMerchantDba(String merchantDba) {
        this.merchantDba = merchantDba;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public String getStrCurrentDate() {
        return strCurrentDate;
    }

    public void setStrCurrentDate(String strCurrentDate) {
        this.strCurrentDate = strCurrentDate;
    }

    public boolean isPaging() {
        return paging;
    }

    public void setPaging(boolean paging) {
        this.paging = paging;
        if (!this.paging) {
            this.setPageLength(0);
            this.setPageToGet(0);
        }

    }

    public int getPageToGet() {
        return pageToGet;
    }

    public void setPageToGet(int pageToGet) {
        this.pageToGet = pageToGet;
    }

    public int getPageLength() {
        return pageLength;
    }

    public void setPageLength(int pageLength) {
        this.pageLength = pageLength;
    }

    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }

    @Override
    protected boolean writeDownloadFileHeader() throws IOException {
        boolean retValue = false;
        try {
            this.writetofile(this.fc, "\"" + this.reportTitle + "\"\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
                    + this.sessionData.getUser().getUsername() + "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
                    + this.getStrCurrentDate() + "\n");
            this.writetofile(fc, "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\"\n");

            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + "\"," + this.startDate + "\n");
            this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + "\"," + this.endDate + "\n");

            if ((this.merchantId != null)) {
                StringBuffer line = new StringBuffer();
                line.append("\"" + Languages.getString("jsp.admin.reports.merchants", sessionData.getLanguage()) + "\",");
                line.append(this.merchantId);
                line.append("\n");
                this.writetofile(fc, line.toString());
            }

            retValue = true;
        } catch (Exception localException) {
            cat.error("MerchantCreditAccountStatusDetail.writeDownloadFileHeader Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecordsHeaders() throws IOException {
        boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = this.getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            this.writetofile(fc, "\"#\",");
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecords() throws IOException {
        boolean retValue = false;
        Vector<Vector<Object>> reportResults = this.getResults();

        try {
            if ((reportResults != null) && (reportResults.size() > 0)) {
                for (int i = 0; i < reportResults.size(); i++) {
                    Vector<Object> currentRecord = reportResults.get(i);
                    // Write the line number
                    this.writetofile(fc, String.valueOf(i + 1) + ",");
                    for (int j = 0; j < currentRecord.size(); j++) {
                        this.writetofile(fc, String.format("%s", currentRecord.get(j)));
                        if (j < (currentRecord.size() - 1)) {
                            this.writetofile(fc, ", ");
                        } else if (j == (currentRecord.size() - 1)) {
                            this.writetofile(fc, "\n");
                        }
                    }
                }
                retValue = true;
            }
        } catch (NumberFormatException localNumberException) {
            cat.error("MerchantCreditAccountStatusDetail.writeDownloadRecords Exception : " + localNumberException.toString());
        } catch (Exception localException) {
            cat.error("MerchantCreditAccountStatusDetail.writeDownloadRecords Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadFileFooter() throws IOException {
        return true;
    }

    private String getSqlWhereClause() {
        StringBuilder strSQLWhere = new StringBuilder();
        String strAccessLevel = this.sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");

        if (strAccessLevel.equals(DebisysConstants.ISO)) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                strSQLWhere.append(String.format("rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = %s) ", this.strRefId));
            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                strSQLWhere.append(String.format("rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = %s  AND iso_id IN ", DebisysConstants.REP_TYPE_REP));
                strSQLWhere.append(String.format(" (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = %s AND iso_id IN ", DebisysConstants.REP_TYPE_SUBAGENT));
                strSQLWhere.append(String.format(" (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = %s  AND iso_id = %s ))) ", DebisysConstants.REP_TYPE_AGENT, this.strRefId));
            }
        } else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
            strSQLWhere.append(String.format("rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = %s AND iso_id IN ", DebisysConstants.REP_TYPE_REP));
            strSQLWhere.append(String.format(" (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = %s AND iso_id = %s)) ", DebisysConstants.REP_TYPE_SUBAGENT, this.strRefId));
        } else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
            strSQLWhere.append(String.format("rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = %s AND iso_id = %s) ", DebisysConstants.REP_TYPE_REP, this.strRefId));
        } else if (strAccessLevel.equals(DebisysConstants.REP)) {
            strSQLWhere.append(String.format("rep_id = %s ", this.strRefId));
        } else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
            strSQLWhere.append(String.format("merchant_id = %s", this.strRefId));
        }
        return strSQLWhere.toString();
    }

    @Override
    public String generateSqlString(QUERY_TYPE qType) {
        String retValue = null;
        StringBuilder strSQL = new StringBuilder();
        String strAccessLevel = sessionData.getProperty("access_level");
        Vector<String> vTimeZoneFilterDates = null;

        try {
            // Calculate start date limit interval
            this.getLimitDays();
            int numDays = DateUtil.getDaysBetween(this.startDate, this.endDate);
            if (numDays > this.limitDays) {
                this.usingDateLimitInterval = true;
                this.endDate = DateUtil.addSubtractDays(startDate, this.limitDays);
            }
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            String sTodayDate = formatter.format(new Date(System.currentTimeMillis()));

            try {

                // Merchant credit data is limited to the last 90 days
                Date earliestStartDate = formatter.parse(DateUtil.addSubtractDays(sTodayDate, -this.limitDays));
                if (earliestStartDate.after(formatter.parse(this.startDate))) {
                    this.startDate = formatter.format(earliestStartDate);
                }
                if ((new Date(System.currentTimeMillis())).before(formatter.parse(this.endDate))) {
                    this.endDate = sTodayDate;
                }
                vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.startDate,
                        this.endDate + " 23:59:59.997", false);

            } catch (Exception e) {
                cat.error("Error during parse date", e);
                throw new ReportException();
            }

            String tzStartDate = vTimeZoneFilterDates.get(0);
            String tzEndDate = vTimeZoneFilterDates.get(1);

            String queryStartDate = String.format("'%s'", tzStartDate);
            String queryEndDate = String.format("'%s'", tzEndDate);

            if (((this.startDate != null) && (this.startDate.length() > 0)) && ((this.endDate != null) && (this.endDate.length() > 0))) {
                if (this.isScheduling()) {
                    if (qType == QUERY_TYPE.RELATIVE) {
                        queryStartDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE;
                        queryEndDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE;
                    } else if (qType == QUERY_TYPE.FIXED) {
                        queryStartDate = String.format("'%s'", tzStartDate);
                        queryEndDate = String.format("'%s'", tzEndDate);
                    }
                } else {
                    queryStartDate = String.format("'%s'", tzStartDate);
                    queryEndDate = String.format("'%s'", tzEndDate);
                }
            } else {
                queryStartDate = String.format("'%s'", tzStartDate);
                queryEndDate = String.format("'%s'", tzEndDate);
            }

            // Generate payments subquery
            //SELECT
            strSQL.append(String.format(" SELECT dbo.GetLocalTimeByAccessLevel(%s, %s, mc.log_datetime, 1)  AS [dateTime], ", strAccessLevel, this.strRefId));
            strSQL.append(" mc.id, (mc.[balance] - mc.[payment]) AS openingCredit, ISNULL(mc.description, '') AS description, '");
            strSQL.append(Languages.getString("jsp.admin.reports.transactions.merchant_credit_accountstatus_detail.typePayment", sessionData.getLanguage()));
            strSQL.append("' AS [type], mc.payment AS amount, mc.[balance] AS closingCredit,");
            strSQL.append(" '-' AS commissionPaymentId, '-' AS taxPaymentId ");
            if (this.isScheduling()) {
                if (qType == ISupportSiteReport.QUERY_TYPE.FIXED) {
                    strSQL.append(String.format(", %s AS startDate ", queryStartDate));
                    strSQL.append(String.format(", %s AS endDate ", queryEndDate));
                } else {
                    strSQL.append(String.format(" %s ", ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS));
                }
            }
            // FROM
            strSQL.append(" FROM [dbo].[merchant_credits] mc WITH (NOLOCK) ");
            // WHERE
            strSQL.append(String.format(" WHERE mc.log_datetime >= %s AND mc.log_datetime < %s ", queryStartDate, queryEndDate));
            strSQL.append(" AND mc.logon_id <> 'Events Job' ");
            strSQL.append(" AND mc.merchant_id IN (SELECT merchant_id FROM merchants m WITH (NOLOCK) WHERE m.");
            strSQL.append(this.getSqlWhereClause());
            strSQL.append(")");
            if (!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                strSQL.append(String.format(" AND mc.merchant_id = %s", this.merchantId));
            }

            strSQL.append(" UNION ALL ");

            // Generate sales subquery
            //SELECT
            strSQL.append(String.format(" SELECT dbo.GetLocalTimeByAccessLevel(%s, %s, wt.datetime, 1)  AS [dateTime], ", strAccessLevel, this.strRefId));
            strSQL.append(" wt.rec_id as id, (wt.[liabilitylimit] - wt.[runningliability]) AS openingCredit, ISNULL(wt.description, '') as [description] , '");
            strSQL.append(Languages.getString("jsp.admin.reports.transactions.merchant_credit_accountstatus_detail.typeSale", sessionData.getLanguage()));
            strSQL.append("' AS [type], wt.amount, (wt.[liabilitylimit] - wt.[runningliability] - wt.[amount]) as closingCredit, ");
            strSQL.append(" ISNULL(CONVERT(VARCHAR(255), cpt.paymentid), '') commissionPaymentId,  ");
            strSQL.append(" ISNULL(CONVERT(VARCHAR(255), tt.paymentid), '') taxPaymentId  ");
            if (this.isScheduling()) {
                if (qType == ISupportSiteReport.QUERY_TYPE.FIXED) {
                    strSQL.append(String.format(", %s AS startDate ", queryStartDate));
                    strSQL.append(String.format(", %s AS endDate ", queryEndDate));
                } else {
                    strSQL.append(String.format(" %s ", ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS));
                }
            }
            // FROM
            strSQL.append(" FROM Web_Transactions wt WITH (NOLOCK) ");
            strSQL.append("      INNER JOIN terminals tm WITH (NOLOCK) ON wt.millennium_no = tm.millennium_no ");
            strSQL.append("      LEFT JOIN CommissionProcessedTransactions CPT ON CPT.TransactionId = wt.rec_id and CPT.merchantCredit = 1 and CPT.entityType='m' ");
            strSQL.append("      LEFT JOIN TaxProcessedTransactions tt on  wt.rec_id = tt.TransactionId ");
            // WHERE
            strSQL.append(String.format(" WHERE wt.datetime >= %s AND wt.datetime < %s ", queryStartDate, queryEndDate));
            strSQL.append(" AND tm.merchant_id IN (SELECT merchant_id FROM merchants m WITH (NOLOCK) WHERE m.");
            strSQL.append(this.getSqlWhereClause());
            strSQL.append(")");
            if (!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
                strSQL.append(String.format(" AND tm.merchant_id = %s", this.merchantId));
            }

            strSQL.append(" ORDER BY [datetime] ");

            retValue = strSQL.toString();

        } catch (Exception localException) {
            cat.error("MerchantCreditAccountStatusDetail.getSqlString. Exception while generating sql string : " + localException.toString());
        }
        return retValue;
    }

    @Override
    public boolean scheduleReport() {
        boolean retValue = false;

        try {
            this.setScheduling(true);
            String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
            String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

            ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_MERCHANT_CREDIT_ACCOUNT_STATUS_REPORT, 1);
            scheduleReport.setNameDateTimeColumn("mc.log_datetime"); // Not used
            scheduleReport.setRelativeQuery(relativeQuery);
            scheduleReport.setFixedQuery(fixedQuery);
            TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
            this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
            retValue = true;
        } catch (Exception localException) {
            cat.error("MerchantCreditAccountStatusDetail.scheduleReport Exception : " + localException.toString());
        }

        return retValue;
    }

    @Override
    public String downloadReport() {
        return this.exportToFile();
    }

    private Vector getRecordFromRs(ResultSet rs) throws SQLException {
        Vector retValue = null;
        if (rs != null) {
            retValue = new Vector();
            if (hasColumn(rs, "dateTime")) {
                retValue.add(DateUtil.formatDateTime(rs.getTimestamp("dateTime")));
            }
            if (hasColumn(rs, "id")) {
                retValue.add(String.valueOf(rs.getLong("id")));
            }
            if (hasColumn(rs, "openingCredit")) {
                retValue.add(NumberUtil.formatAmount(String.valueOf(rs.getBigDecimal("openingCredit"))));
            }
            if (hasColumn(rs, "description")) {
                String cleanDesc = (rs.getString("description") == null)?"":rs.getString("description").trim();
                retValue.add(cleanDesc);
            }
            if (hasColumn(rs, "type")) {
                retValue.add(rs.getString("type"));
            }
            if (hasColumn(rs, "amount")) {
                retValue.add(NumberUtil.formatAmount(String.valueOf(rs.getBigDecimal("amount"))));
            }
            if (hasColumn(rs, "closingCredit")) {
                retValue.add(NumberUtil.formatAmount(String.valueOf(rs.getBigDecimal("closingCredit"))));
            }
            if (hasColumn(rs, "commissionPaymentId")) {
                retValue.add(rs.getString("commissionPaymentId"));
            }

            if ((this.deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                    && (this.customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {

                if (hasColumn(rs, "taxPaymentId")) {
                    retValue.add(rs.getString("taxPaymentId"));
                }
            }
        }
        return retValue;
    }

    @Override
    public Vector<Vector<Object>> getResults() {
        Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            this.setScheduling(false); // If returning results, then scheduling
            // does not have sense
            String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
            cat.debug("Pin remaining inventory query : " + strSQL);

            if (this.isPaging()) {
                pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = pstmt.executeQuery();
                this.recordCount = DbUtil.getRecordCount(rs);
                if (!(this.pageLength > 0)) {
                    this.pageLength = this.recordCount;
                }
                int i = 0;
                if (this.recordCount > 0) {
                    rs.absolute(DbUtil.getRowNumber(rs, this.pageLength, this.recordCount, this.pageToGet));
                    do {
                        Vector vecTemp = this.getRecordFromRs(rs);
                        if (vecTemp != null) {
                            retValue.add(vecTemp);
                        }
                        i++;
                    } while (rs.next() && i < this.pageLength);
                }

            } else {
                pstmt = dbConn.prepareStatement(strSQL);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    Vector vecTemp = this.getRecordFromRs(rs);
                    if (vecTemp != null) {
                        retValue.add(vecTemp);
                    }
                }
            }

            if (retValue.isEmpty()) {
                retValue = null;
            }
        } catch (TorqueException localTorqueException) {
            cat.error("MerchantCreditAccountStatusDetailgetResults Exception : " + localTorqueException.toString());
        } catch (ReportException localReportException) {
            cat.error("MerchantCreditAccountStatusDetailgetResults Exception : " + localReportException.toString());
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } catch (Exception localException) {
            cat.error("MerchantCreditAccountStatusDetailgetResults Exception : " + localException.toString());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                    dbConn = null;
                }
            } catch (SQLException cleaningSqlException) {
                DbUtil.processSqlException(cleaningSqlException);
            }
        }

        return retValue;
    }

    @Override
    public ArrayList<ColumnReport> getReportRecordHeaders() {
        ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
        try {
            retValue.add(new ColumnReport("dateTime", Languages.getString("jsp.admin.reports.date", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("id", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.id", sessionData.getLanguage())
                    .toUpperCase(), Long.class, false));
            retValue.add(new ColumnReport("openingCredit", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.opening_credit", sessionData.getLanguage())
                    .toUpperCase(), Double.class, false));
            retValue.add(new ColumnReport("description", Languages.getString("jsp.admin.reports.transactions.merchant_credit_accountstatus_detail.description", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("type", Languages.getString("jsp.admin.reports.transactions.merchant_credit_accountstatus_detail.type", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("amount", Languages.getString("jsp.admin.reports.transactions.merchant_credit_accountstatus_detail.amount", sessionData.getLanguage())
                    .toUpperCase(), Double.class, false));
            retValue.add(new ColumnReport("closingCredit", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.closing_credit", sessionData.getLanguage())
                    .toUpperCase(), Double.class, false));
            retValue.add(new ColumnReport("commissionPaymentId", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.payment_id", sessionData.getLanguage())
                    .toUpperCase(), Long.class, false));
            if ((this.deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL))
                    && (this.customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))) {
                retValue.add(new ColumnReport("taxPaymentId", Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.tax.payment_id", sessionData.getLanguage())
                        .toUpperCase(), String.class, false));
            }
        } catch (Exception localException) {
            cat.error("MerchantCreditAccountStatusDetail.getReportRecordHeaders Exception : " + localException);
        }
        return retValue;
    }

    @Override
    public ArrayList<String> getTitles() {
        ArrayList<String> retValue = new ArrayList<String>();

        try {
            retValue.add(this.merchantDba);

            retValue.add(this.reportTitle + " "
                    + Languages.getString("jsp.admin.from", this.sessionData.getLanguage()) + " "
                    + HTMLEncoder.encode(this.formattedStartDate) + " "
                    + Languages.getString("jsp.admin.to", this.sessionData.getLanguage()) + " "
                    + HTMLEncoder.encode(this.formattedEndDate));
            Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
            String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
                    + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
            retValue.add(noteTimeZone);
            retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
            retValue.add(Languages.getString("jsp.admin.reports.transactions.merchant_credit_detail.limited_to_90_days", this.sessionData.getLanguage()));
        } catch (NumberFormatException localNumberFormatException) {
            cat.error("MerchantCreditAccountStatusDetail.getTitles Exception : " + localNumberFormatException.toString());
        } catch (Exception localException) {
            cat.error("MerchantCreditAccountStatusDetail.getTitles Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    public boolean isUsingDateLimitInterval() {
        return this.usingDateLimitInterval;
    }

    @Override
    public int getLimitDays() {
        String reportId = sessionData.getProperty("reportId");
        this.limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
        return this.limitDays;
    }

    public MerchantCreditAccountStatusDetail() {
        this.scheduling = false;
        this.deploymentType = "";
        this.customConfigType = "";
        this.startDate = "";
        this.formattedStartDate = "";
        this.endDate = "";
        this.formattedEndDate = "";
        this.merchantId = "";
        this.merchantDba = "";
        this.reportTitle = "";
        this.strCurrentDate = "";
        this.usingDateLimitInterval = false;
        this.limitDays = 0;
        this.paging = false;
        this.pageToGet = 0;
        this.pageLength = 0;
        this.recordCount = 0;
    }

    public MerchantCreditAccountStatusDetail(SessionData sessionData, ServletContext pContext, boolean scheduling,
            String deploymentType, String customConfigType, String startDate, String formattedStartDate,
            String endDate, String formattedEndDate, String merchantId, String merchantDba, String reportTitle,
            String strCurrentDate, boolean paging, int pageToGet, int pageLength) {
        this();
        this.sessionData = sessionData;
        this.strRefId =  this.sessionData.getProperty("ref_id");
        this.setContext(pContext);
        this.scheduling = scheduling;
        this.deploymentType = deploymentType;
        this.customConfigType = customConfigType;
        this.startDate = startDate;
        this.formattedStartDate = formattedStartDate;
        this.endDate = endDate;
        this.formattedEndDate = formattedEndDate;
        this.merchantId = merchantId;
        this.merchantDba = merchantDba;
        this.reportTitle = reportTitle;
        this.strCurrentDate = strCurrentDate;
        this.paging = paging;
        this.pageToGet = pageToGet;
        this.pageLength = pageLength;
    }

}
