/**
 * 
 */
package com.debisys.reports.schedule;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.debisys.customers.UserPayments;
import com.debisys.dateformat.DateFormat;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

/**
 * @author fernandob
 * 
 */
public class IsoActionsOnRepAccountsReport extends ReportDownloader implements ISupportSiteReport
{
	// attributes
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private boolean scheduling;
	private String strUserIds;
	private String strRepIds;
	private String start_date;
	private String end_date;
	private String strCurrentDate;
	private boolean allUsersSelected;
	private boolean allRepsSelected;
	private boolean UsingDateLimitInterval;
	private int limitDays;
	
	// properties
	
	/**
	 * @return the limitDays
	 */
	public int getLimitDays()
	{
		return limitDays;
	}

	/**
	 * @param limitDays the limitDays to set
	 */
	public void setLimitDays(int limitDays)
	{
		this.limitDays = limitDays;
	}

	/**
	 * @return the usingDateLimitInterval
	 */
	public boolean isUsingDateLimitInterval()
	{
		return UsingDateLimitInterval;
	}

	/**
	 * @param usingDateLimitInterval the usingDateLimitInterval to set
	 */
	public void setUsingDateLimitInterval(boolean usingDateLimitInterval)
	{
		UsingDateLimitInterval = usingDateLimitInterval;
	}

	/**
	 * @return the scheduling
	 */
	public boolean isScheduling()
	{
		return scheduling;
	}

	/**
	 * @param shceduling
	 *            the scheduling to set
	 */
	public void setScheduling(boolean shceduling)
	{
		this.scheduling = shceduling;
	}

	/**
	 * @return the strUserIds
	 */
	public String getStrUserIds()
	{
		return strUserIds;
	}

	/**
	 * @param strUserIds
	 *            the strUserIds to set
	 */
	public void setStrUserIds(String strUserIds)
	{
		this.strUserIds = strUserIds;
	}

	public void setStrUserIds(String[] strUserIds)
	{
		this.strUserIds = com.debisys.utils.StringUtil.arrayToString(strUserIds, ",");
	}

	/**
	 * @return the strRepIds
	 */
	public String getStrRepIds()
	{
		return strRepIds;
	}

	/**
	 * @param strRepIds
	 *            the strRepIds to set
	 */
	public void setStrRepIds(String strRepIds)
	{
		this.strRepIds = strRepIds;
	}

	/**
	 * @param strRepIds
	 *            the strRepIds to set
	 */
	public void setStrRepIds(String[] strRepIds)
	{
		this.strRepIds = com.debisys.utils.StringUtil.arrayToString(strRepIds, ",");
	}

	/**
	 * @return the start_date
	 */
	public String getStart_date()
	{
		return start_date;
	}

	/**
	 * @param startDate
	 *            the start_date to set
	 */
	public void setStart_date(String startDate)
	{
		start_date = startDate.trim();
	}

	/**
	 * @return the end_date
	 */
	public String getEnd_date()
	{
		return end_date;
	}

	/**
	 * @param endDate
	 *            the end_date to set
	 */
	public void setEnd_date(String endDate)
	{
		end_date = endDate.trim();
	}

	/**
	 * @return the strCurrentDate
	 */
	public String getStrCurrentDate()
	{
		return strCurrentDate;
	}

	/**
	 * @param strCurrentDate
	 *            the strCurrentDate to set
	 */
	public void setStrCurrentDate(String strCurrentDate)
	{
		this.strCurrentDate = strCurrentDate;
	}

	public String getStartDateFormatted()
	{
		return getDateFormatted(this.start_date);
	}

	public String getEndDateFormatted()
	{
		return getDateFormatted(this.end_date);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileFooter()
	 */
	@Override
	protected boolean writeDownloadFileFooter() throws IOException
	{
		return true;
	}

	/**
	 * @return the allUsersSelected
	 */
	public boolean isAllUsersSelected()
	{
		return allUsersSelected;
	}

	/**
	 * @param allUsersSelected
	 *            the allUsersSelected to set
	 */
	public void setAllUsersSelected(boolean allUsersSelected)
	{
		this.allUsersSelected = allUsersSelected;
	}

	/**
	 * @return the allRepsSelected
	 */
	public boolean isAllRepsSelected()
	{
		return allRepsSelected;
	}

	/**
	 * @param allRepsSelected
	 *            the allRepsSelected to set
	 */
	public void setAllRepsSelected(boolean allRepsSelected)
	{
		this.allRepsSelected = allRepsSelected;
	}

	// methods
	public IsoActionsOnRepAccountsReport()
	{
		super();
		this.scheduling = false;
		this.strUserIds = null;
		this.strRepIds = null;
		this.start_date = null;
		this.end_date = null;
		this.strCurrentDate = null;
		this.allRepsSelected = false;
		this.allUsersSelected = false;
		this.UsingDateLimitInterval = false;
		this.limitDays = 0;
	}

	/**
	 * @param scheduling
	 * @param strUserIds
	 * @param strRepIds
	 * @param startDate
	 * @param endDate
	 */
	public IsoActionsOnRepAccountsReport(SessionData sessionData, ServletContext pContext, boolean scheduling, String strUserIds, String strRepIds,
			String startDate, String endDate, boolean allRepsSel, boolean allUsrSel)
	{
		this();
		this.sessionData = sessionData;
		this.setContext(pContext);
                this.strRefId = sessionData.getProperty("ref_id");
		this.scheduling = scheduling;
		this.setStrUserIds(strUserIds);
		this.setStrRepIds(strRepIds);
		this.start_date = startDate.trim();
		this.end_date = endDate.trim();
		this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
		this.allRepsSelected = allRepsSel;
		this.allUsersSelected = allUsrSel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileHeader()
	 */
	@Override
	protected boolean writeDownloadFileHeader() throws IOException
	{
		boolean retValue = false;
		try
		{
			this.writetofile(this.fc, "\"" + Languages.getString("jsp.admin.reports.title41", sessionData.getLanguage()).toUpperCase() + "\"\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
					+ sessionData.getUser().getUsername() + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
					+ this.getStrCurrentDate() + "\n");
			this.writetofile(fc, "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\"\n");
			if (sessionData.getProperty("repName") != null)
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage()) + "\","
						+ sessionData.getProperty("repName") + "\n");
			}
			else
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage()) + "\","
						+ sessionData.getUser().getCompanyName() + "\n");
			}

			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + "\"," + this.start_date + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + "\"," + this.end_date + "\n");

			if ((this.strRepIds != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.reps", sessionData.getLanguage()) + "\",");
				if (this.allUsersSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.strRepIds);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}

			if ((this.strUserIds != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.user.user", sessionData.getLanguage()) + "\",");
				if (this.allUsersSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.strUserIds);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadFileHeader Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecords()
	 */
	@Override
	protected boolean writeDownloadRecords() throws IOException
	{
		boolean retValue = false;
		Vector<Vector<Object>> reportResults = this.getResults();

		try
		{
			if ((reportResults != null) && (reportResults.size() > 0))
			{
				for (int i = 0; i < reportResults.size(); i++)
				{
					Vector<Object> currentRecord = reportResults.get(i);
					// Write the line number
					this.writetofile(fc, "\"" + String.valueOf(i + 1) + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(0).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(1).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(2).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(3).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(4).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(5).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(6).toString().trim() + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(7).toString().trim() + "\"\n");
				}
				retValue = true;
			}
		}
		catch (NumberFormatException localNumberException)
		{
			cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadRecords Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecordsHeaders()
	 */
	@Override
	protected boolean writeDownloadRecordsHeaders() throws IOException
	{
		boolean retValue = false;

		ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

		if ((columnHeaders != null) && (columnHeaders.size() > 0))
		{
			this.writetofile(fc, "\"#\",");
			this.writetofile(fc, "\"" + columnHeaders.get(0).getLanguageDescription() + "\",");
			this.writetofile(fc, "\"" + columnHeaders.get(1).getLanguageDescription() + "\",");
			this.writetofile(fc, "\"" + columnHeaders.get(2).getLanguageDescription() + "\",");
			this.writetofile(fc, "\"" + columnHeaders.get(3).getLanguageDescription() + "\",");
			this.writetofile(fc, "\"" + columnHeaders.get(4).getLanguageDescription() + "\",");
			this.writetofile(fc, "\"" + columnHeaders.get(5).getLanguageDescription() + "\",");
			this.writetofile(fc, "\"" + columnHeaders.get(6).getLanguageDescription() + "\",");
			this.writetofile(fc, "\"" + columnHeaders.get(7).getLanguageDescription() + "\"\n");

			retValue = true;
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#downloadReport()
	 */
	@Override
	public String downloadReport()
	{
		return this.exportToFile();
	}
	
	private String getDateFormatted(String dateToFormat)
	{
		if (DateUtil.isValid(dateToFormat))
		{
			String date = null;
			try
			{
				// Current support site date is in MM/dd/YYYY.
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				java.util.Date dd = sdf.parse(dateToFormat);

				// We will specify a new date format through the database
				SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
				date = sdf1.format(dd);

				// Since Java SimpleDateFormat does not know whether a datetime
				// contains
				// a time or not, remove the time (usually 12:00:00 AM) from the
				// end of the date
				// ranges
				date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
			}
			catch (ParseException e)
			{
				cat.error("Error during TransactionReport date localization ", e);
			}
			return date;
		}
		else
		{
			// Something isn't valid in the date string - don't try to localize
			// it
			return StringUtil.toString(dateToFormat);
		}
	}	
	
	private void insertSelectedUsers(StringBuffer sqlString) throws ReportException
	{
		if(sqlString != null)
		{
			if (this.allUsersSelected)
			{
				UserPayments user = new UserPayments();
				user.getUserList(this.sessionData, 1);
				sqlString.append("  AND mc.logon_id IN (" + user.getSqlUsersList() + ") ");
			}
			else
			{
				if ((this.strUserIds != null) && (!this.strUserIds.equals("")))
				{
					String[] logonIds = this.strUserIds.split(",");
					String quotedLogonIds = "";
					for(int i = 0; i < logonIds.length;i++)
					{
						quotedLogonIds += "'" + logonIds[i] + "'";
						if(i < (logonIds.length - 1))
						{
							quotedLogonIds += ",";
						}
					}
					sqlString.append("  AND mc.logon_id IN (" + quotedLogonIds + ") ");
				}
			}
		}
	}
	
	private String generateSql_1(QUERY_TYPE qType)
	{
		String retValue = null;
		StringBuffer strSQL = new StringBuffer();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date
					+ " 23:59:59.999", false);
			
			strSQL.append("SELECT mc.datetime AS datetime, mc.logon_id AS logon_id, reps.businessname AS businessname,  ");
			strSQL
					.append("		  mc.description AS description, mc.amount AS payment, mc.available_credit AS balance, 0 AS commission, mc.amount AS net_payment ");
			if (this.isScheduling())
			{
				if (qType == QUERY_TYPE.FIXED)
				{
					strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
					strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
				}
				else
				{
					strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
				}
			}
			strSQL.append(" FROM rep_merchant_credits AS mc WITH(NOLOCK) ");
			strSQL.append("		INNER JOIN merchants AS m WITH(NOLOCK) ON mc.merchant_id = m.merchant_id   ");
			strSQL.append("		INNER JOIN reps WITH(NOLOCK) ON m.rep_id = reps.rep_id ");
			strSQL.append(" WHERE 1 = 1 ");

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL
							.append(" AND m.merchant_id IN (SELECT merchant_id FROM merchants (nolock) WHERE rep_id IN (SELECT rep_id FROM reps WHERE iso_id = "
									+ strRefId + ")  ) ");

				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					String strTheChain = "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " and iso_id IN "
							+ "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id IN "
							+ "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId
							+ "))) ";

					strSQL.append(" AND m.merchant_id IN (SELECT merchant_id FROM merchants (nolock) WHERE rep_id IN " + strTheChain + ")");
				}
			}

			this.insertSelectedUsers(strSQL);

			if (this.allRepsSelected)
			{
				User currentUser = this.sessionData.getUser();
				strSQL.append(" AND mc.merchant_id IN ( ");
				strSQL.append(" SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN ( " + " SELECT rep_id ");
				strSQL.append(" FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP);
				strSQL.append( ReportsUtil.getChainToBuildWhereSQL(currentUser.getAccessLevel(), currentUser.getDistChainType(), currentUser.getRefId(),
								"m.rep_id"));
				strSQL.append(" ) " + " )	");
			}
			else
			{
				if ((this.strRepIds != null) && (!this.strRepIds.equals("")))
				{
					strSQL.append(" AND mc.merchant_id IN ( ");
					strSQL.append(" SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN ( " + " SELECT rep_id ");
					strSQL.append(" FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " and rep_id IN (");
					strSQL.append(strRepIds + ") " + " ) " + " )	");
				}
			}

			int numDays = DateUtil.getDaysBetween(this.start_date, this.end_date);
			if (numDays > this.limitDays)
			{
				this.UsingDateLimitInterval = true;
				this.start_date = DateUtil.addSubtractDays(this.end_date, -this.limitDays);
			}

			if (DateUtil.isValid(this.start_date) && DateUtil.isValid(this.end_date))
			{
				if (this.isScheduling())
				{
					if (qType == QUERY_TYPE.RELATIVE)
					{
						strSQL.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
					}
					else if (qType == QUERY_TYPE.FIXED)
					{
						strSQL.append("         AND (mc.datetime  >= '" + vTimeZoneFilterDates.get(0) + "'");
						strSQL.append("         AND mc.datetime  < '" + vTimeZoneFilterDates.get(1) + "')");
					}
				}
				else
				{
					// If not scheduling, the wild card is removed, and the
					// current report dates are used
					int i;
					while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
					{
						strSQL.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
					}
					strSQL.append("         AND (mc.datetime  >= '" + vTimeZoneFilterDates.get(0) + "'");
					strSQL.append("         AND mc.datetime  < '" + vTimeZoneFilterDates.get(1) + "')");
				}
			}

			retValue = strSQL.toString();
		}
		catch (ReportException localReportException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getSqlString. Exception while generationg sql string : " + localReportException.toString());
		}
		catch (Exception localException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getSqlString. Exception while generationg sql string : " + localException.toString());
		}
		cat.debug(retValue);
		return retValue;
	}

	private String generateSql_2(QUERY_TYPE qType)
	{
		String retValue = null;
		StringBuffer strSQL = new StringBuffer();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date
					+ " 23:59:59.999", false);
			strSQL.append("SELECT mc.datetime AS datetime, mc.logon_id AS logon_id, r.businessname AS businessname, mc.description AS description, ");
			strSQL.append("		  mc.amount AS payment, mc.available_credit AS balance, mc.commission AS commission, ");
			strSQL.append("(mc.amount - (mc.amount * (isnull(mc.commission,0) * .01))) AS net_payment ");
			if (this.isScheduling())
			{
				if (qType == QUERY_TYPE.FIXED)
				{
					strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
					strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
				}
				else
				{
					strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
				}
			}
			strSQL.append(" FROM rep_credits mc WITH(NOLOCK) ");
			strSQL.append("		INNER JOIN reps r WITH(NOLOCK) on (mc.rep_id = r.rep_id)  ");
			strSQL.append(" WHERE 1 = 1 ");

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL.append(" AND mc.rep_id IN (SELECT rep_id FROM reps AS reps_2 WITH(NOLOCK) WHERE (iso_id =" + strRefId + ")  ) ");

				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					String strTheChain = "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " and iso_id IN "
							+ "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id IN "
							+ "(SELECT rep_id FROM reps WITH(NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId
							+ "))) ";

					strSQL.append(" AND mc.rep_id IN " + strTheChain);
				}
			}

			this.insertSelectedUsers(strSQL);

			
			if (this.allRepsSelected)
			{
				User currentUser = this.sessionData.getUser();
				strSQL.append(
				 ReportsUtil.getChainToBuildWhereSQL(currentUser.getAccessLevel(), currentUser.getDistChainType(), currentUser.getRefId(),
								"mc.rep_id"));
			}
			else
			{
				if ((this.strRepIds != null) && (!this.strRepIds.equals("")))
				{
					strSQL.append(" AND mc.rep_id IN(" + strRepIds + ") ");
				}
			}

			int numDays = DateUtil.getDaysBetween(this.start_date, this.end_date);
			if (numDays > limitDays)
			{
				this.UsingDateLimitInterval = true;
				this.end_date = DateUtil.addSubtractDays(start_date, limitDays);
			}

			if (DateUtil.isValid(this.start_date) && DateUtil.isValid(this.end_date))
			{
				if (this.isScheduling())
				{
					if (qType == QUERY_TYPE.RELATIVE)
					{
						strSQL.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
					}
					else if (qType == QUERY_TYPE.FIXED)
					{
						strSQL.append("         AND (mc.datetime  >= '" + vTimeZoneFilterDates.get(0) + "'");
						strSQL.append("         AND mc.datetime  < '" + vTimeZoneFilterDates.get(1) + "')");
					}
				}
				else
				{
					// If not scheduling, the wild card is removed, and the
					// current report dates are used
					int i;
					while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
					{
						strSQL.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
					}
					strSQL.append("         AND (mc.datetime  >= '" + vTimeZoneFilterDates.get(0) + "'");
					strSQL.append("         AND mc.datetime  < '" + vTimeZoneFilterDates.get(1) + "')");
				}
			}
			retValue = strSQL.toString();
		}
		catch (ReportException localReportException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getSqlString. Exception while generationg sql string : " + localReportException.toString());
		}
		catch (Exception localException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getSqlString. Exception while generationg sql string : " + localException.toString());
		}
		cat.debug(retValue);
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#generateSqlString(com.debisys.
	 * reports.ISupportSiteReport.QUERY_TYPE)
	 */
	@Override
	public String generateSqlString(QUERY_TYPE qType)
	{
		String strSelect = null;
		String reportId = sessionData.getProperty("reportId");
		this.limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
		if(this.isScheduling())
		{
			strSelect = "select datetime, logon_id,  businessname, description, payment,  balance, commission, net_payment, startDate, endDate From (";
		}
		else
		{
			strSelect = "select datetime, logon_id,  businessname, description, payment,  balance, commission, net_payment From (";
		}
		return strSelect + "(" + this.generateSql_1(qType) + ") UNION ALL (" + this.generateSql_2(qType) + ")) isoActions " + " ORDER BY datetime DESC";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getReportRecordHeaders()
	 */
	@Override
	public ArrayList<ColumnReport> getReportRecordHeaders()
	{
		ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();

		try
		{
			retValue.add(new ColumnReport("datetime", Languages.getString("jsp.admin.customers.merchants_credit_history.date",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("logon_id", Languages.getString("jsp.admin.customers.merchants_credit_history.user",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("businessname", Languages.getString("jsp.admin.reports.notification_list.rep_name",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("description", Languages.getString("jsp.admin.customers.merchants_credit_history.desc",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("payment", Languages.getString("jsp.admin.customers.merchants_credit_history.payment",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("balance", Languages.getString("jsp.admin.customers.merchants_credit_history.new_avail_credit",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("commission", Languages.getString("jsp.admin.customers.merchants_credit_history.commission",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("net_payment", Languages.getString("jsp.admin.customers.merchants_credit_history.net_payment",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		catch (Exception localException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getReportRecordHeaders Exception : " + localException);
		}

		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getResults()
	 */
	@Override
	public Vector<Vector<Object>> getResults()
	{
		Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			this.setScheduling(false); // If returning results, then scheduling
										// does not have sense
			String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector<Object> vecTemp = new Vector<Object>();
				vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
				vecTemp.add(StringUtil.toString(rs.getString("logon_id")));
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add(StringUtil.toString(rs.getString("description")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("payment")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("balance")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("net_payment")));

				retValue.add(vecTemp);
			}
			if (retValue.size() == 0)
			{
				retValue = null;
			}
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getResults Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getResults Exception : " + localReportException.toString());
		}
		catch (SQLException localSqlException)
		{
			cat.error("IsoActionsOnRepAccountsReport.getResults Exception : " + localSqlException.toString());
		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
					rs = null;
				}
				if (pstmt != null)
				{
					pstmt.close();
					pstmt = null;
				}
				if (dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (SQLException cleaningSqlException)
			{
				cat.error("IsoActionsOnRepAccountsReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getTitles()
	 */
	@Override
	public ArrayList<String> getTitles()
	{
		ArrayList<String> retValue = new ArrayList<String>();

		try
		{
			retValue.add(Languages.getString("jsp.admin.reports.title41", this.sessionData.getLanguage()));
			Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
			String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
					+ vTimeZoneData.get(0) + " [" + vTimeZoneData.get(1) + "]";
			retValue.add(noteTimeZone);
			retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
		}
		catch (NumberFormatException localNumberFormatException)
		{
			cat.error("getTitles Exception : " + localNumberFormatException.toString());
		}
		catch (Exception localException)
		{
			cat.error("getTitles Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#scheduleReport()
	 */
	@Override
	public boolean scheduleReport()
	{
		boolean retValue = false;

		try
		{
			this.setScheduling(true);
			String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
			String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

			ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_ISO_ACTIONS_ON_REP_ACCOUNTS, 1);
			scheduleReport.setNameDateTimeColumn("mc.datetime");
			scheduleReport.setRelativeQuery(relativeQuery);
			cat.debug("Relative Query : " + relativeQuery);
			scheduleReport.setFixedQuery(fixedQuery);
			cat.debug("Fixed Query : " + fixedQuery);
			TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
			this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("IsoActionsOnRepAccountsReport.scheduleReport Exception : " + localException.toString());
		}

		return retValue;
	}

}
