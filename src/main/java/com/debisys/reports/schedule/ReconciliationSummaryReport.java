/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports.schedule;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import static com.debisys.reports.schedule.ReportDownloader.cat;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.TimeZone;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletContext;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

/**
 *
 * @author fernandob
 */
public class ReconciliationSummaryReport extends ReportDownloader implements ISupportSiteReport {

    private enum resultGroups {

        INTERNAL, EXTERNAL, BOTH
    }
    
    private final String AGENT_CODE = "A";
    private final String SUBAGENT_CODE = "S";
    private final String REP_CODE = "R";
    private final String MERCHANT_CODE = "M";

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

    // instance fields
    private String startDate;
    private String endDate;
    private String headerUserInfo;

    private boolean scheduling;
    private String strCurrentDate;
    private int limitDays;
    private boolean usingDateLimitInterval;
    private Vector<String> vTimeZoneData;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getHeaderUserInfo() {
        return headerUserInfo;
    }

    public void setHeaderUserInfo(String headerUserInfo) {
        this.headerUserInfo = headerUserInfo;
    }

    public boolean isScheduling() {
        return scheduling;
    }

    public void setScheduling(boolean scheduling) {
        this.scheduling = scheduling;
    }

    public String getStrCurrentDate() {
        return strCurrentDate;
    }

    public void setStrCurrentDate(String strCurrentDate) {
        this.strCurrentDate = strCurrentDate;
    }

    private ReconciliationSummaryReport() {
        this.startDate = null;
        this.endDate = null;
        this.headerUserInfo = null;
        this.scheduling = false;
        this.strCurrentDate = null;
    }

    public ReconciliationSummaryReport(SessionData sessionData, ServletContext pContext,
            String startDate, String endDate, String headerUserInfo, boolean scheduling) {
        this();
        this.sessionData = sessionData;
        this.setContext(pContext);
        this.strRefId = sessionData.getProperty("ref_id");
        this.startDate = startDate;
        this.endDate = endDate;
        this.headerUserInfo = headerUserInfo;
        this.scheduling = scheduling;
        this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());;
    }

    @Override
    protected boolean writeDownloadFileHeader() throws IOException {
        boolean retValue = false;
        try {
            this.writetofile(this.fc, String.format("%s\n",
                    Languages.getString("jsp.admin.reports.transactions.reconcile_summary.title", sessionData.getLanguage()).toUpperCase()));
            this.writetofile(fc, String.format("%s,%s\n",
                    Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()),
                    this.headerUserInfo));
            this.writetofile(fc, String.format("%s,%s",
                    Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()),
                    this.getStrCurrentDate()));
            this.writetofile(fc, "\n");
            this.writetofile(fc, Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\n");

            if (sessionData.getProperty("repName") != null) {
                this.writetofile(fc, "" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
                        + "," + sessionData.getProperty("repName") + "\n");
            } else {
                this.writetofile(fc, Languages.getString("jsp.admin.iso_name", sessionData.getLanguage())
                        + "," + sessionData.getUser().getCompanyName() + "\n");
            }

            this.writetofile(fc, Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + ","
                    + this.startDate + "\n");
            this.writetofile(fc, Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + ","
                    + this.endDate + "\n");

            retValue = true;
        } catch (Exception localException) {
            cat.error("writeDownloadFileHeader Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecordsHeaders() throws IOException {
        boolean retValue = false;

        ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

        if ((columnHeaders != null) && (columnHeaders.size() > 0)) {
            for (int i = 0; i < columnHeaders.size(); i++) {
                this.writetofile(fc, "" + columnHeaders.get(i).getLanguageDescription() + "");
                if (i < (columnHeaders.size() - 1)) {
                    this.writetofile(fc, ",");
                } else {
                    this.writetofile(fc, "\n");
                }
            }
            retValue = true;
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadRecords() throws IOException {
        boolean retValue = false;
        Vector<Vector<Object>> reportResults = this.getResults();

        try {
            if ((reportResults != null) && (reportResults.size() > 0)) {
                for (int i = 0; i < reportResults.size(); i++) {
                    Vector<Object> currentRecord = reportResults.get(i);
                    // Write the line number
                    this.writetofile(fc, "" + currentRecord.get(0).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(1).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(2).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(3).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(4).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(5).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(6).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(7).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(8).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(9).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(10).toString().trim() + ", ");
                    this.writetofile(fc, "" + currentRecord.get(11).toString().trim() + "\n");
                }
                retValue = true;
            }
        } catch (NumberFormatException localNumberException) {
            cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
        } catch (Exception localException) {
            cat.error("writeDownloadRecords Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    protected boolean writeDownloadFileFooter() throws IOException {
        // Already containded on the last line of the result set
        return true;
    }

    private synchronized void setDatesWithLimit() {
        String tmpStartDate = this.startDate;
        String tmpEndDate = this.endDate;

        try {
            if (!this.usingDateLimitInterval) {
                String strAccessLevel = this.sessionData.getProperty("access_level");
                this.getLimitDays();
                int numDays = DateUtil.getDaysBetween(this.startDate, this.endDate);
                if (numDays > this.limitDays) {
                    this.endDate = DateUtil.addSubtractDays(startDate, this.limitDays);
                }
                this.vTimeZoneData = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.startDate, this.endDate, false);
                this.usingDateLimitInterval = true;
            }
        } catch (Exception localException) {
            cat.warn("Error while setting report limits", localException);
            this.startDate = tmpStartDate;
            this.endDate = tmpEndDate;
            this.usingDateLimitInterval = false;
        }
    }
    
    /***
     * Because the report is a sumarized one. the result must contain at least 
     * a line of zeros for an entiy group with no results.
     * This method checks if nos results were found for an specific entity, if so
     * it will add a default line of zeros
     * @param strSql The main query where the additional sentences will be added 
     * @param entityType To what entity type the script will be added
     */
    private void addDummyRecords(StringBuilder strSql, String entityType)
    {
        if(strSql != null)
        {
            StringBuilder pattern = new StringBuilder();
            pattern.append(" IF(NOT EXISTS (Select resultId From #recdetailresults Where entitytype = '%s' and entityaccounttype = %d)) ");
            pattern.append("  BEGIN ");
            pattern.append("	INSERT INTO #recdetailresults (entityid, entityaccounttype, businessname, entitytype, ");
            pattern.append("      openingavailable, inwardsall, inwardsii, inwardsie, inwardsee, outwardsall, commission, ");
            pattern.append("      changediebalance, sales, closingavailable) ");
            pattern.append("    VALUES(0, %d, '', '%s', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0) ");
            pattern.append("  END ");
            
            strSql.append(String.format(pattern.toString(), entityType, 0, 0 , entityType));
            strSql.append(String.format(pattern.toString(), entityType, 1, 1 , entityType));
        }
    }

    private void addGroupedResults(StringBuilder strSql, resultGroups group) {
        strSql.append(" DELETE #tmpResults ");
        strSql.append(" INSERT INTO #tmpResults (accountType, entityName, openningBalance, inwardsII, inwardsIE,  ");
        strSql.append(" 	inwardsEE, outwardsAll, commission, changedIEBalance,  ");
        strSql.append(" 	sales, closingAvailable, grandTotal) ");

        if ((group == resultGroups.EXTERNAL) || (group == resultGroups.INTERNAL)) {
            strSql.append(String.format(" SELECT CASE RDR.entityAccountType WHEN 0 THEN '%s' ELSE '%s' END AS EntityAccountType, ",
                    Languages.getString("jsp.admin.reports.transactions.reconcile_summary.internal", sessionData.getLanguage()),
                    Languages.getString("jsp.admin.reports.transactions.reconcile_summary.external", sessionData.getLanguage())));
        } else {
            strSql.append(String.format(" SELECT '%s', ",
                    Languages.getString("jsp.admin.reports.transactions.reconcile_summary.both", sessionData.getLanguage())));
        }

        strSql.append(" 	ET.entityName,  ");
        strSql.append(" 	SUM(RDR.openingavailable) openingavailableSum,  ");
        strSql.append(" 	SUM(RDR.inwardsii) inwardsiiSum,  ");
        strSql.append(" 	SUM(RDR.inwardsie) inwardsieSum,  ");
        strSql.append(" 	SUM(RDR.inwardsee) inwardseeSum,  ");
        strSql.append(" 	SUM(RDR.outwardsall) outwardsallSum, ");
        strSql.append(" 	SUM(RDR.commission) commissionSum, ");
        strSql.append(" 	SUM(RDR.changediebalance) changediebalanceSum, ");
        strSql.append(" 	SUM(RDR.sales) salesSum, ");
        strSql.append(" 	SUM(RDR.closingavailable) closingavailableSum, ");
        strSql.append(" 	0 ");
        strSql.append(" FROM   #recdetailresults RDR  ");
        strSql.append(" 		INNER JOIN #entitytypes ET  ");
        strSql.append(" 			   ON RDR.entitytype = ET.entitycode  ");
        if (group == resultGroups.INTERNAL) {
            strSql.append(" WHERE RDR.entityaccounttype = 0 ");
            strSql.append(" Group By RDR.entityaccounttype, RDR.entitytype, ET.entityName ");
            strSql.append(" Order by RDR.entityaccounttype, ");
        } else if (group == resultGroups.EXTERNAL) {
            strSql.append(" WHERE RDR.entityaccounttype <> 0 ");
            strSql.append(" Group By RDR.entityaccounttype, RDR.entitytype, ET.entityName ");
            strSql.append(" Order by RDR.entityaccounttype, ");

        } else if (group == resultGroups.BOTH) {
            strSql.append(" Group By RDR.entitytype, ET.entityName ");
            strSql.append(" Order by ");
        }

        strSql.append(" 		CASE WHEN RDR.entitytype = 'A'  THEN 0  ");
        strSql.append(" 		  WHEN RDR.entitytype = 'S' THEN 2 ");
        strSql.append(" 		  WHEN RDR.entitytype = 'R' THEN 3 ");
        strSql.append(" 		  WHEN RDR.entitytype = 'M' THEN 4 ");
        strSql.append(" 		END ASC ");

        strSql.append(" INSERT INTO #recSummaryResults (accountType, entityName, openningBalance, inwardsII, inwardsIE, ");
        strSql.append(" 	inwardsEE, outwardsAll, commission, changedIEBalance, ");
        strSql.append(" 	sales, closingAvailable, grandTotal) ");
        strSql.append(" SELECT accountType, entityName, ");
        if (this.isScheduling()) {
            strSql.append(" 	'$' + convert(varchar(25), openningBalance, 1), ");
            strSql.append(" 	'$' + convert(varchar(25), inwardsII, 1), '$' + convert(varchar(25), inwardsIE, 1), ");
            strSql.append(" 	'$' + convert(varchar(25), inwardsEE, 1), '$' + convert(varchar(25), outwardsAll, 1), ");
            strSql.append(" 	'$' + convert(varchar(25), commission, 1), '$' + convert(varchar(25), changedIEBalance, 1), ");
            strSql.append(" 	'$' + convert(varchar(25), sales, 1), '$' + convert(varchar(25), closingAvailable, 1), ");
            strSql.append(" 	''");
        } else {
            strSql.append(" 	openningBalance, ");
            strSql.append(" 	inwardsII, inwardsIE, ");
            strSql.append(" 	inwardsEE, outwardsAll, ");
            strSql.append(" 	commission, changedIEBalance, ");
            strSql.append(" 	sales, closingAvailable, ");
            strSql.append(" 	''");
        }
        strSql.append(" FROM #tmpResults ");

        // C�lculo del gran total
        strSql.append(" SELECT @subTotal = (SUM(openningBalance) + SUM(inwardsII) + SUM(inwardsIE) + ");
        strSql.append(" 	SUM(inwardsEE) + SUM(outwardsAll) + SUM(commission) + SUM(changedIEBalance) + ");
        strSql.append(" 	SUM(sales)) - SUM(closingAvailable) ");
        strSql.append(" FROM #tmpResults ");

        // Subtotals row
        strSql.append(" IF(EXISTS(SELECT resultId FROM #tmpResults)) ");
        strSql.append("  BEGIN ");
        strSql.append("    INSERT INTO #recSummaryResults (accountType, entityName, openningBalance, inwardsII, inwardsIE, ");
        strSql.append(" 	inwardsEE, outwardsAll, commission, changedIEBalance, ");
        strSql.append(" 	sales, closingAvailable, grandTotal) ");

        
        String subtotalTitle = "Subtotal";
        switch(group)
        {
            case INTERNAL:
                subtotalTitle = Languages.getString("jsp.admin.reports.transactions.reconcile_summary.total_internal", sessionData.getLanguage());
                break;
            case EXTERNAL:
                subtotalTitle = Languages.getString("jsp.admin.reports.transactions.reconcile_summary.total_external", sessionData.getLanguage());
                break;
            case BOTH:
                subtotalTitle = Languages.getString("jsp.admin.reports.transactions.reconcile_summary.grand_total", sessionData.getLanguage());
                break;                
        }
        strSql.append(String.format(" SELECT '', '%s', ", subtotalTitle));
        if (this.isScheduling()) {
            strSql.append(" '$' + convert(varchar(25), SUM(openningBalance), 1), ");
            strSql.append(" '$' + convert(varchar(25), SUM(inwardsII), 1), '$' + convert(varchar(25), SUM(inwardsIE), 1), ");
            strSql.append(" '$' + convert(varchar(25), SUM(inwardsEE), 1), '$' + convert(varchar(25), SUM(outwardsAll), 1), ");
            strSql.append(" '$' + convert(varchar(25), SUM(commission), 1), '$' + convert(varchar(25), SUM(changedIEBalance), 1), ");
            strSql.append(" '$' + convert(varchar(25), SUM(sales), 1), '$' + convert(varchar(25), SUM(closingAvailable), 1), ");
            strSql.append(" '$' + convert(varchar(25), @subTotal, 1) ");
        } else {
            strSql.append(" SUM(openningBalance), ");
            strSql.append(" SUM(inwardsII), SUM(inwardsIE), ");
            strSql.append(" SUM(inwardsEE), SUM(outwardsAll), ");
            strSql.append(" SUM(commission), SUM(changedIEBalance), ");
            strSql.append(" SUM(sales), SUM(closingAvailable), ");
            strSql.append(" @subTotal");
        }
        strSql.append("  FROM #tmpResults ");

        if (group != resultGroups.BOTH) {
            strSql.append(" INSERT INTO #recSummaryResults (accountType, entityName, openningBalance, inwardsII, inwardsIE, ");
            strSql.append(" 	inwardsEE, outwardsAll, commission, changedIEBalance, ");
            strSql.append(" 	sales, closingAvailable, grandTotal) ");
            strSql.append(" VALUES ('', '', '', '', '', '', '', '', '', '', '', '') ");
        }

        // Clean duplicate transaction types
        strSql.append(" UPDATE RSR ");
        strSql.append(" SET	accountType = '' ");
        strSql.append(" FROM	#recSummaryResults RSR ");
        strSql.append(" 	INNER JOIN #tmpResults TR ON RSR.accountType = TR.accountType ");
        strSql.append(" WHERE   RSR.resultId not in( ");
        strSql.append("             SELECT  MIN(RSR2.resultId) ");
        strSql.append("             FROM    #recSummaryResults RSR2 ");
        strSql.append("                     INNER JOIN #tmpResults TR2 ON RSR2.accountType = TR2.accountType ");
        strSql.append(" 	) ");

        strSql.append(" END  ");

    }

    @Override
    public String generateSqlString(QUERY_TYPE qType) {
        String retValue = null;
        StringBuilder strSql = new StringBuilder();
        String s_merchantsSQL = "";
        String s_entitiesSQL = "";

        try {
            this.setDatesWithLimit();
            String sStartDate = this.vTimeZoneData.get(0).substring(0, this.vTimeZoneData.get(0).indexOf(" "));
            String sEndDate = this.vTimeZoneData.get(1).substring(0, this.vTimeZoneData.get(1).indexOf(" "));

            String queryStartDate = String.format("%s", sStartDate);
            String queryEndDate = String.format("%s", sEndDate);

            if (((this.startDate != null) && (this.startDate.length() > 0)) && ((this.endDate != null) && (this.endDate.length() > 0))) {
                if (this.isScheduling()) {
                    if (qType == QUERY_TYPE.RELATIVE) {
                        queryStartDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE;
                        queryEndDate = ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE;
                    } else if (qType == QUERY_TYPE.FIXED) {
                        queryStartDate = String.format("%s", sStartDate);
                        queryEndDate = String.format("%s", sEndDate);
                    }
                } else {
                    queryStartDate = String.format("%s", sStartDate);
                    queryEndDate = String.format("%s", sEndDate);
                }
            } else {
                queryStartDate = String.format("%s", sStartDate);
                queryEndDate = String.format("%s", sEndDate);
            }

            String strRefId = this.sessionData.getProperty("ref_id");
            String strAccessLevel = this.sessionData.getProperty("access_level");
            String strDistChainType = this.sessionData.getProperty("dist_chain_type");

            // Main query
            strSql.append(" IF OBJECT_ID('tempdb..#entityTypes', 'U') IS NOT NULL ");
            strSql.append("  BEGIN ");
            strSql.append(" 	TRUNCATE TABLE #entityTypes ");
            strSql.append(" 	DROP TABLE #entityTypes ");
            strSql.append("  END ");

            strSql.append(" CREATE TABLE #entityTypes( ");
            strSql.append(" 	entityCode CHAR NOT NULL PRIMARY KEY, ");
            strSql.append(" 	entityName VARCHAR(50) ");
            strSql.append(" ) ");

            strSql.append(String.format(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('%s', '%s')   ", 
                    AGENT_CODE, Languages.getString("jsp.admin.reports.transactions.reconcile_summary.agents", this.sessionData.getLanguage())));
            strSql.append(String.format(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('%s', '%s') ", 
                    SUBAGENT_CODE, Languages.getString("jsp.admin.reports.transactions.reconcile_summary.subagents", this.sessionData.getLanguage())));
            strSql.append(String.format(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('%s', '%s') ", 
                    REP_CODE, Languages.getString("jsp.admin.reports.transactions.reconcile_summary.reps", this.sessionData.getLanguage())));
            strSql.append(String.format(" INSERT INTO #entityTypes(entityCode, entityName) VALUES('%s', '%s') ", 
                    MERCHANT_CODE, Languages.getString("jsp.admin.reports.transactions.reconcile_summary.merchants", this.sessionData.getLanguage())));

            strSql.append(" IF OBJECT_ID('tempdb..#recDetailResults', 'U') IS NOT NULL ");
            strSql.append("  BEGIN ");
            strSql.append(" 	TRUNCATE TABLE #recDetailResults ");
            strSql.append(" 	DROP TABLE #recDetailResults ");
            strSql.append("  END ");

            strSql.append(" CREATE TABLE #recDetailResults( ");
            strSql.append(" 	resultId INT NOT NULL IDENTITY(1,1) PRIMARY KEY, ");
            strSql.append(" 	entityId NUMERIC(15,0), ");
            strSql.append(" 	entityAccountType CHAR(1), ");
            strSql.append(" 	businessName VARCHAR(50), ");
            strSql.append(" 	entityType CHAR(1), ");
            strSql.append(" 	openingAvailable MONEY, ");
            strSql.append(" 	inwardsAll MONEY, ");
            strSql.append(" 	inwardsII MONEY, ");
            strSql.append(" 	inwardsIE MONEY, ");
            strSql.append(" 	inwardsEE MONEY, ");
            strSql.append(" 	outwardsAll MONEY, ");
            strSql.append(" 	commission MONEY, ");
            strSql.append(" 	changedIEBalance MONEY, ");
            strSql.append(" 	sales MONEY, ");
            strSql.append(" 	closingAvailable MONEY ");
            strSql.append(" ) ");

            strSql.append(" IF OBJECT_ID('tempdb..#tmpResults', 'U') IS NOT NULL   ");
            strSql.append(" BEGIN  	");
            strSql.append(" 	TRUNCATE TABLE #tmpResults  	");
            strSql.append(" 	DROP TABLE #tmpResults   ");
            strSql.append(" END  ");

            strSql.append(" CREATE TABLE #tmpResults(  	");
            strSql.append(" 	resultId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,  	");
            strSql.append(" 	accountType VARCHAR(50),");
            strSql.append(" 	entityName VARCHAR(50),");
            strSql.append(" 	openningBalance MONEY,");
            strSql.append(" 	inwardsII MONEY,");
            strSql.append(" 	inwardsIE MONEY,");
            strSql.append(" 	inwardsEE MONEY,");
            strSql.append(" 	outwardsAll MONEY,");
            strSql.append(" 	commission MONEY,");
            strSql.append(" 	changedIEBalance MONEY,");
            strSql.append(" 	sales MONEY,");
            strSql.append(" 	closingAvailable MONEY,");
            strSql.append(" 	grandTotal MONEY");
            strSql.append(" )  ");

            strSql.append(" IF OBJECT_ID('tempdb..#recSummaryResults', 'U') IS NOT NULL   ");
            strSql.append(" BEGIN  	");
            strSql.append(" 	TRUNCATE TABLE #recSummaryResults  	");
            strSql.append(" 	DROP TABLE #recSummaryResults   ");
            strSql.append(" END  ");

            strSql.append(" CREATE TABLE #recSummaryResults(  	");
            strSql.append(" 	resultId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,  	");
            strSql.append(" 	accountType VARCHAR(50),");
            strSql.append(" 	entityName VARCHAR(50),");
            strSql.append(" 	openningBalance VARCHAR(50),");
            strSql.append(" 	inwardsII VARCHAR(50),");
            strSql.append(" 	inwardsIE VARCHAR(50),");
            strSql.append(" 	inwardsEE VARCHAR(50),");
            strSql.append(" 	outwardsAll VARCHAR(50),");
            strSql.append(" 	commission VARCHAR(50),");
            strSql.append(" 	changedIEBalance VARCHAR(50),");
            strSql.append(" 	sales VARCHAR(50),");
            strSql.append(" 	closingAvailable VARCHAR(50),");
            strSql.append(" 	grandTotal VARCHAR(50)");
            strSql.append(" )  ");

            strSql.append(" BEGIN TRY ");

            if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                s_merchantsSQL = DbUtil.sqlSelISO5Merch(strRefId);
                s_entitiesSQL = DbUtil.sqlSelAllISO5Entities(strRefId);
            } else {
                s_merchantsSQL = DbUtil.sqlSelISO3Merch(strRefId);
                s_entitiesSQL = DbUtil.sqlSelAllISO3Entities(strRefId);
            }

            // ======================== RETRIEVE AGENT DATA ====================================================
            String sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "r.BusinessName").replaceAll("_ENTITYNAME2_", "r.BusinessName")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Reps r WITH (NOLOCK) ON heb.EntityID = r.Rep_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_entitiesSQL + " AND Type = 4").replaceAll("_ENTITYTYPE_", AGENT_CODE);
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");

            strSql.append(sSql);
            
            this.addDummyRecords(strSql, AGENT_CODE);

            // ======================== RETRIEVE SUBAGENT DATA==================================================
            sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "r.BusinessName").replaceAll("_ENTITYNAME2_", "r.BusinessName")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Reps r WITH (NOLOCK) ON heb.EntityID = r.Rep_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_entitiesSQL + " AND Type = 5").replaceAll("_ENTITYTYPE_", SUBAGENT_CODE);
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");
            strSql.append(sSql);
            this.addDummyRecords(strSql, SUBAGENT_CODE);
            // ======================== RETRIEVE REP DATA ======================================================
            sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "r.BusinessName").replaceAll("_ENTITYNAME2_", "r.BusinessName")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Reps r WITH (NOLOCK) ON heb.EntityID = r.Rep_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_entitiesSQL + " AND Type = 1").replaceAll("_ENTITYTYPE_", REP_CODE);
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");
            strSql.append(sSql);
            this.addDummyRecords(strSql, REP_CODE);
            // ======================== RETRIEVE MERCHANT DATA =================================================
            sSql = sql_bundle.getString("getReconciliationDetailEx");
            sSql = sSql.replaceAll("_ENTITYNAME_", "m.DBA AS BusinessName").replaceAll("_ENTITYNAME2_", "m.DBA")
                    .replaceAll("_ENTITYJOIN_", "INNER JOIN Merchants m WITH (NOLOCK) ON heb.EntityID = m.Merchant_ID");
            sSql = sSql.replaceAll("_ENTITIES_", s_merchantsSQL).replaceAll("_ENTITYTYPE_", MERCHANT_CODE);
            sSql = sSql.replaceAll("_STARTDATE_", queryStartDate).replaceAll("_ENDDATE_", queryEndDate).replaceAll("_ENDDATE2_", queryEndDate);

            strSql.append(" INSERT INTO #recDetailResults(entityId, entityAccountType, ");
            strSql.append("   businessName, entityType, openingAvailable, inwardsAll, ");
            strSql.append("   inwardsII, inwardsIE, inwardsEE, outwardsAll, ");
            strSql.append("   commission, changedIEBalance, sales, closingAvailable) ");
            strSql.append(sSql);
            this.addDummyRecords(strSql, MERCHANT_CODE);

            strSql.append(" DECLARE @subTotal MONEY ");

            // ======================== SUMMARY CALCULATION =================================================
            this.addGroupedResults(strSql, resultGroups.INTERNAL);
            this.addGroupedResults(strSql, resultGroups.EXTERNAL);
            this.addGroupedResults(strSql, resultGroups.BOTH);

            // *********************** SELECT THE FINAL RESULT FROM TEMPORAL TABLE *****************************
            strSql.append(" SELECT accountType, entityName, openningBalance, inwardsII, inwardsIE, ");
            strSql.append(" 	inwardsEE, outwardsAll, commission, changedIEBalance, ");
            strSql.append(" 	sales, closingAvailable, grandTotal ");
            strSql.append(" FROM	#recSummaryResults ");
            strSql.append(" ORDER BY resultId ");

            strSql.append(" END TRY ");
            strSql.append(" BEGIN CATCH ");
            strSql.append("   PRINT 'ERROR NUMBER : ' ");
            strSql.append("   PRINT ERROR_NUMBER() ");
            strSql.append("   PRINT 'ERROR SEVERITY : ' ");
            strSql.append("   PRINT ERROR_SEVERITY() ");
            strSql.append("   PRINT 'ERROR STATE : '");
            strSql.append("   PRINT ERROR_STATE() ");
            strSql.append("   PRINT 'ERROR PROCEDURE : '");
            strSql.append("   PRINT ERROR_PROCEDURE() ");
            strSql.append("   PRINT 'ERROR LINE : '");
            strSql.append("   PRINT ERROR_LINE()");
            strSql.append("   PRINT 'ERROR MESSAGE : '");
            strSql.append("   PRINT ERROR_MESSAGE()");
            strSql.append(" END CATCH ");
            strSql.append(" TRUNCATE TABLE #entityTypes ");
            strSql.append(" DROP TABLE #entityTypes ");
            strSql.append(" TRUNCATE TABLE #recDetailResults ");
            strSql.append(" DROP TABLE #recDetailResults ");
            strSql.append(" TRUNCATE TABLE #tmpResults ");
            strSql.append(" DROP TABLE #tmpResults ");
            strSql.append(" TRUNCATE TABLE #recSummaryResults ");
            strSql.append(" DROP TABLE #recSummaryResults ");

            retValue = strSql.toString();
        } catch (Exception localException) {
            cat.error("ReconciliationSummaryReport.getSqlString. Exception while generationg sql string : ", localException);
        }
        return retValue;
    }

    @Override
    public boolean scheduleReport() {
        boolean retValue = false;

        try {
            this.setScheduling(true);
            String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
            String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

            ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_RECONCILIATION_SUMARY_REPORT, 1);
            scheduleReport.setNameDateTimeColumn("entrydate");
            scheduleReport.setRelativeQuery(relativeQuery);
            scheduleReport.setFixedQuery(fixedQuery);
            TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
            this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
            retValue = true;
        } catch (Exception localException) {
            cat.error("ReconciliationSummaryReport.scheduleReport Exception : " + localException.toString());
        }

        return retValue;
    }

    @Override
    public String downloadReport() {
        return this.exportToFile();
    }

    @Override
    public Vector<Vector<Object>> getResults() {
        Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String entityAccountType, entityName, 
                openAvailable, inwardsII, inwardsIE, inwardsEE,
                outwardsAll, commission, changedIEBalance, sales,
                closingAvailable, grandTotal;

        try {
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null) {
                cat.error("Can't get database connection");
                throw new ReportException();
            }

            this.setScheduling(false); // If returning results, then scheduling
            // does not have sense
            String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
            cat.debug("Reconciliation Summary Report script : " + strSQL);
            pstmt = dbConn.prepareStatement(strSQL);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                Vector vecTemp = new Vector();
                
                
                entityAccountType = ((rs.getString("accountType") == null) || (rs.wasNull())) ? "" : rs.getString("accountType").trim();
                vecTemp.add(entityAccountType);
                entityName = ((rs.getString("entityName") == null) || (rs.wasNull())) ? "" : rs.getString("entityName").trim();
                vecTemp.add(entityName);

                openAvailable = ((rs.getString("openningBalance") == null) || (rs.wasNull())) ? "0" : rs.getString("openningBalance").trim();
                vecTemp.add(openAvailable);
                inwardsII = ((rs.getString("inwardsII") == null) || (rs.wasNull())) ? "0" : rs.getString("inwardsII").trim();
                vecTemp.add(inwardsII);
                inwardsIE = ((rs.getString("inwardsIE") == null) || (rs.wasNull())) ? "0" : rs.getString("inwardsIE").trim();
                vecTemp.add(inwardsIE);
                inwardsEE = ((rs.getString("inwardsEE") == null) || (rs.wasNull())) ? "0" : rs.getString("inwardsEE").trim();
                vecTemp.add(inwardsEE);
                outwardsAll = ((rs.getString("outwardsAll") == null) || (rs.wasNull())) ? "0" : rs.getString("outwardsAll").trim();
                vecTemp.add(outwardsAll);
                commission = ((rs.getString("commission") == null) || (rs.wasNull())) ? "0" : rs.getString("commission").trim();
                vecTemp.add(commission);
                changedIEBalance = ((rs.getString("changedIEBalance") == null) || (rs.wasNull())) ? "0" : rs.getString("changedIEBalance").trim();
                vecTemp.add(changedIEBalance);
                sales = ((rs.getString("sales") == null) || (rs.wasNull())) ? "0" : rs.getString("sales").trim();
                vecTemp.add(sales);
                closingAvailable = ((rs.getString("closingAvailable") == null) || (rs.wasNull())) ? "0" : rs.getString("closingAvailable").trim();
                vecTemp.add(closingAvailable);
                grandTotal = ((rs.getString("grandTotal") == null) || (rs.wasNull())) ? "0" : rs.getString("grandTotal").trim();
                vecTemp.add(grandTotal);
                
                retValue.add(vecTemp);
            }
            if (retValue.isEmpty()) {
                retValue = null;
            }

        } catch (TorqueException localTorqueException) {
            cat.error("ReconciliationSummaryReport.getResults Exception : " + localTorqueException.toString());
        } catch (ReportException localReportException) {
            cat.error("ReconciliationSummaryReport.getResults Exception : " + localReportException.toString());
        } catch (SQLException localSqlException) {
            DbUtil.processSqlException(localSqlException);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (pstmt != null) {
                    pstmt.close();
                    pstmt = null;
                }
                if (dbConn != null) {
                    Torque.closeConnection(dbConn);
                    dbConn = null;
                }
            } catch (SQLException cleaningSqlException) {
                cat.error("ReconciliationSummaryReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
            }
        }
        return retValue;
    }

    @Override
    public ArrayList<ColumnReport> getReportRecordHeaders() {
        ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
        try {
            retValue.add(new ColumnReport("accountType", Languages.getString("jsp.admin.customers.entity_account_type", sessionData.getLanguage())
                    .toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("entityName", Languages.getString("jsp.admin.customers.entity_type",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("openningBalance", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.opening_bal",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("inwardsII", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.inwards_i_to_i",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("inwardsIE", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.inwards_i_to_e",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("inwardsEE", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.inwards_e_to_e",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("outwardsAll", Languages.getString("jsp.admin.reports.transactions.reconcile_summary.outwards_all",
                    sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("commission", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.commissions_applied", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("changedIEBalance", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.changed_ie_bal", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("sales", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.retail_sales", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("closingAvailable", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.closing_balance", sessionData.getLanguage()).toUpperCase(), String.class, false));
            retValue.add(new ColumnReport("grandTotal", Languages
                    .getString("jsp.admin.reports.transactions.reconcile_summary.grand_total", sessionData.getLanguage()).toUpperCase(), String.class, false));
        } catch (Exception localException) {
            cat.error("ReconciliationSummaryReport.getReportRecordHeaders Exception : " + localException);
        }
        return retValue;
    }

    @Override
    public ArrayList<String> getTitles() {
        ArrayList<String> retValue = new ArrayList<String>();

        try {
            retValue.add(Languages.getString("jsp.admin.reports.transactions.reconcile_summary.title", this.sessionData.getLanguage()));
            Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData
                    .getProperty("ref_id")));
            String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage())
                    + ":&nbsp;" + vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
            retValue.add(noteTimeZone);
            retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
            retValue.add(Languages.getString("jsp.admin.reports.transactions.reconcile_summary.report_generated", this.sessionData.getLanguage())
                    + " " + this.strCurrentDate);
        } catch (NumberFormatException localNumberFormatException) {
            cat.error("getTitles Exception : " + localNumberFormatException.toString());
        } catch (Exception localException) {
            cat.error("getTitles Exception : " + localException.toString());
        }
        return retValue;
    }

    @Override
    public boolean isUsingDateLimitInterval() {
        return true;
    }

    @Override
    public int getLimitDays() {
        String reportId = sessionData.getProperty("reportId");
        this.limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays(reportId, context);
        return this.limitDays;
    }

}
