/**
 *
 */
package com.debisys.reports.schedule;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.ZipEntry;
import java.util.zip.Deflater;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.io.FileOutputStream;
import org.apache.log4j.Category;
import javax.servlet.ServletContext;
import java.nio.channels.FileChannel;
import java.util.zip.ZipOutputStream;
import java.io.FileNotFoundException;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import java.util.ArrayList;

/**
 * @author fernandob This class was created in order to avoid copying the
 * down-load code included in most download options for reports
 */
public abstract class ReportDownloader {

    // attributes
    static Category cat = Category.getInstance(ReportDownloader.class);
    protected ServletContext context = null;
    protected SessionData sessionData = null;
    protected String downloadUrl = null;
    protected String downloadPath = null;
    protected String workingDir = null;
    protected String filePrefix = null;
    protected String deploymentType = null;
    protected String customConfigType = null;
    protected String strRefId = null;
    protected String strFileName = null;
    protected FileChannel fc = null;

    // properties
    /**
     * @return the context
     */
    public ServletContext getContext() {
        return context;
    }

    /**
     * @param pContext
     */
    public void setContext(ServletContext pContext) {
        context = pContext;
    }

    /**
     * @return the sessionData
     */
    public SessionData getSessionData() {
        return sessionData;
    }

    /**
     * @param sessionData the sessionData to set
     */
    public void setSessionData(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    /**
     * @return the downloadUrl
     */
    public String getDownloadUrl() {
        return downloadUrl;
    }

    /**
     * @return the downloadPath
     */
    public String getDownloadPath() {
        return downloadPath;
    }

    /**
     * @return the workingDir
     */
    public String getWorkingDir() {
        return workingDir;
    }

    /**
     * @return the filePrefix
     */
    public String getFilePrefix() {
        return filePrefix;
    }

    /**
     * @return the deploymentType
     */
    public String getDeploymentType() {
        return deploymentType;
    }

    /**
     * @return the customConfigType
     */
    public String getCustomConfigType() {
        return customConfigType;
    }

    /**
     * @return the strRefId
     */
    public String getStrRefId() {
        return strRefId;
    }

    public void setStrRefId(String strRefId) {
        this.strRefId = strRefId;
    }

    /**
     * @return the strFileName
     */
    public String getStrFileName() {
        return strFileName;
    }

    /**
     * @return the fc
     */
    public FileChannel getFc() {
        return fc;
    }

    // methods
    public ReportDownloader() {
        this.context = null;
        this.downloadUrl = null;
        this.downloadPath = null;
        this.workingDir = null;
        this.filePrefix = null;
        this.deploymentType = null;
        this.customConfigType = null;
        this.strRefId = null;
    }

    protected boolean loadProperties() {
        boolean retValue = false;

        try {
            this.downloadUrl = DebisysConfigListener.getDownloadUrl(this.context);
            this.downloadPath = DebisysConfigListener.getDownloadPath(this.context);
            this.workingDir = DebisysConfigListener.getWorkingDir(this.context);
            this.filePrefix = DebisysConfigListener.getFilePrefix(this.context);
            this.deploymentType = DebisysConfigListener.getDeploymentType(this.context);
            this.customConfigType = DebisysConfigListener.getCustomConfigType(this.context);
        } catch (Exception localException) {
            cat.error("ReportDownloader.loadProperties Exception  : " + localException.toString());
        }

        return retValue;
    }

    private boolean checkFileName(String strFileName, ServletContext context) {
        boolean isUnique = true;

        File f = new File(this.workingDir + "/" + this.filePrefix + strFileName + ".csv");
        if (f.exists()) {
            // duplicate file found
            cat.error("Duplicate file found:" + this.workingDir + "/" + this.filePrefix + strFileName + ".csv");
            return false;
        }

        f = new File(this.downloadPath + "/" + this.filePrefix + strFileName + ".zip");
        if (f.exists()) {
            // duplicate file found
            cat.error("Duplicate file found:" + this.downloadPath + "/" + this.filePrefix + strFileName + ".zip");
            return false;
        }

        return isUnique;
    }

    private String generateRandomNumber() {
        StringBuffer s = new StringBuffer();
        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);

        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private String generateFileName(ServletContext context) {
        boolean isUnique = false;
        String generatedFileName = "";
        while (!isUnique) {
            generatedFileName = this.generateRandomNumber();
            isUnique = this.checkFileName(generatedFileName, context);
        }
        return generatedFileName;
    }

    protected void writetofile(FileChannel fc, String c) throws IOException {
        fc.write(ByteBuffer.wrap(c.getBytes()));
    }

    /**
     * This method is intended to write general report information as filters
     * and other header information not related with field headers
     *
     * @return True if all needed information was written to the file or if
     * there is no need to write that info. False if any error
     * @throws java.io.IOException
     */
    protected abstract boolean writeDownloadFileHeader() throws IOException;

    /**
     * Use this method to write column headers
     *
     * @return True if all needed information was written to the file or if
     * there is no need to write that info. False if any error
     * @throws java.io.IOException
     */
    protected abstract boolean writeDownloadRecordsHeaders() throws IOException;

    /**
     * Use this method to write the report results to file
     *
     * @return True if all needed information was written to the file or if
     * there is no need to write that info. False if any error
     * @throws java.io.IOException
     */
    protected abstract boolean writeDownloadRecords() throws IOException;

    /**
     * Use this method to write any footer information, return true if no footer
     * information is needed.
     *
     * @return True if all needed information was written to the file or if
     * there is no need to write that info. False if any error
     * @throws java.io.IOException
     */
    protected abstract boolean writeDownloadFileFooter() throws IOException;

    private boolean createFile() {
        boolean retValue = false;

        try {
            long startQuery = System.currentTimeMillis();
            // Create the working folder if not exists
            File file = new File(this.workingDir);
            if (!file.exists()) {
                file.mkdir();
            }
            String tmpFileName = workingDir + filePrefix + this.strFileName + ".csv";
            this.fc = new RandomAccessFile(tmpFileName, "rw").getChannel();
            try {
                cat.debug("Temp File Name: " + tmpFileName);

                if (!this.writeDownloadFileHeader()) {
                    cat.error("Errors were found when writing download information");
                }
                if (!this.writeDownloadRecordsHeaders()) {
                    cat.error("Errors were found while writing filed headers");
                }
                if (!this.writeDownloadRecords()) {
                    cat.error("Errors were found while writing download details");
                }
                if (!this.writeDownloadFileFooter()) {
                    cat.error("Errors were found while writing download footer");
                }
                long endQuery = System.currentTimeMillis();
                cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
                retValue = true;
            } catch (Exception localException) {
                cat.error("Report Downloader.createFile : General error while writing data to file : " + localException.toString());
            } finally {
                if (fc != null) {
                    fc.close();
                }
            }
        } catch (FileNotFoundException e) {
            cat.error("File not found : " + e.toString());
        } catch (IOException localIoException) {
            cat.error("Input / Output Exception : " + localIoException.toString());
        }
        return retValue;
    }

    private String compressFile() {
        String retValue = null;

        File sourceFile = new File(workingDir + filePrefix + this.strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        long startQuery = System.currentTimeMillis();

        try {

            String zipFileName = downloadPath + filePrefix + this.strFileName + ".zip";
            cat.debug("Zip File Name: " + zipFileName);
            downloadUrl = downloadUrl + filePrefix + this.strFileName + ".zip";
            cat.debug("Download URL: " + downloadUrl);

            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

            // Set the compression ratio
            out.setLevel(Deflater.DEFAULT_COMPRESSION);
            FileInputStream in = new FileInputStream(workingDir + filePrefix + this.strFileName + ".csv");

            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(filePrefix + this.strFileName + ".csv"));

            // Transfer bytes from the current file to the ZIP file
            // out.write(buffer, 0, in.read(buffer));
            int len;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            // Close the current entry
            out.closeEntry();
            // Close the current file input stream
            in.close();
            out.close();
            retValue = downloadUrl;
        } catch (IllegalArgumentException iae) {
            cat.error("compressFile.Illegal argument : " + iae.toString());
        } catch (FileNotFoundException fnfe) {
            cat.error("compressFile.File not found : " + fnfe.toString());
        } catch (IOException ioe) {
            cat.error("compressFile.IO Exception : " + ioe.toString());
        }
        sourceFile.delete();
        long endQuery = System.currentTimeMillis();
        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
        return retValue;
    }

    /**
     *
     * @return The compressed file path where the report result
     */
    public String exportToFile() {
        String retValue = null;
        this.loadProperties();
        this.strFileName = this.generateFileName(this.context);
        if (createFile()) {
            retValue = this.compressFile();
        }

        return retValue;
    }
    
    
    
    protected void writeFilterList2File(ArrayList<Long> list) throws IOException {
        if (list != null) {
            for (Long list1 : list) {
                this.writetofile(fc, "," + String.valueOf(list1));
            }
            this.writetofile(fc, "\n");
        }
    }

}
