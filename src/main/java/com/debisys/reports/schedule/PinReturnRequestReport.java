/**
 * 
 */
package com.debisys.reports.schedule;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.schedule.ISupportSiteReport.QUERY_TYPE;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.users.User;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.TimeZone;

/**
 * @author fernandob
 * 
 */
public class PinReturnRequestReport extends ReportDownloader implements ISupportSiteReport
{
	// attributes
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private boolean scheduling;
	private String pinCase;
	private String start_date;
	private String end_date;
	private String status;
	private String merchant;
	private String strCurrentDate;
	private boolean allStatusSelected;
	private boolean allMerchantsSelected;
	private boolean UsingDateLimitInterval;
	private int limitDays;
	
	// properties
	
	/**
	 * @return the limitDays
	 */
	public int getLimitDays()
	{
		return limitDays;
	}

	/**
	 * @param limitDays the limitDays to set
	 */
	public void setLimitDays(int limitDays)
	{
		this.limitDays = limitDays;
	}

	/**
	 * @return the usingDateLimitInterval
	 */
	public boolean isUsingDateLimitInterval()
	{
		return UsingDateLimitInterval;
	}

	/**
	 * @param usingDateLimitInterval the usingDateLimitInterval to set
	 */
	public void setUsingDateLimitInterval(boolean usingDateLimitInterval)
	{
		UsingDateLimitInterval = usingDateLimitInterval;
	}

	/**
	 * @return the scheduling
	 */
	public boolean isScheduling()
	{
		return scheduling;
	}

	/**
	 * @param shceduling
	 *            the scheduling to set
	 */
	public void setScheduling(boolean shceduling)
	{
		this.scheduling = shceduling;
	}

	/**
	 * @return the pinCase
	 */
	public String getPinCase()
	{
		return pinCase;
	}

	/**
	 * @param pinCase
	 *            the pinCase to set
	 */
	public void setPinCase(String pinCase)
	{
		this.pinCase = pinCase;
	}

	/**
	 * @return the start_date
	 */
	public String getStart_date()
	{
		return start_date;
	}

	/**
	 * @param startDate
	 *            the start_date to set
	 */
	public void setStart_date(String startDate)
	{
		start_date = startDate;
	}

	/**
	 * @return the end_date
	 */
	public String getEnd_date()
	{
		return end_date;
	}

	/**
	 * @param endDate
	 *            the end_date to set
	 */
	public void setEnd_date(String endDate)
	{
		end_date = endDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}

	/**
	 * @return the merchant
	 */
	public String getMerchant()
	{
		return merchant;
	}

	/**
	 * @param merchant
	 *            the merchant to set
	 */
	public void setMerchant(String merchant)
	{
		this.merchant = merchant;
	}

	/**
	 * @return the strCurrentDate
	 */
	public String getStrCurrentDate()
	{
		return strCurrentDate;
	}

	/**
	 * @param strCurrentDate
	 *            the strCurrentDate to set
	 */
	public void setStrCurrentDate(String strCurrentDate)
	{
		this.strCurrentDate = strCurrentDate;
	}

	/**
	 * @return the allStatusSelected
	 */
	public boolean isAllStatusSelected()
	{
		return allStatusSelected;
	}

	/**
	 * @param allStatusSelected
	 *            the allStatusSelected to set
	 */
	public void setAllStatusSelected(boolean allStatusSelected)
	{
		this.allStatusSelected = allStatusSelected;
	}

	/**
	 * @return the allMerchantsSelected
	 */
	public boolean isAllMerchantsSelected()
	{
		return allMerchantsSelected;
	}

	/**
	 * @param allMerchantsSelected
	 *            the allMerchantsSelected to set
	 */
	public void setAllMerchantsSelected(boolean allMerchantsSelected)
	{
		this.allMerchantsSelected = allMerchantsSelected;
	}

	// methods
	public PinReturnRequestReport()
	{
		super();
		this.sessionData = null;
		this.context = null;
		this.scheduling = false;
		this.pinCase = null;
		start_date = null;
		end_date = null;
		this.status = null;
		this.merchant = null;
		this.strCurrentDate = null;
		this.allMerchantsSelected = false;
		this.allStatusSelected = false;
		this.UsingDateLimitInterval = false;
		this.limitDays = 0;
	}

	/**
	 * @param shceduling
	 * @param pinCase
	 * @param startDate
	 * @param endDate
	 * @param status
	 * @param merchant
	 * @param iso
	 * @param strCurrentDate
	 */
	public PinReturnRequestReport(SessionData sessionData, ServletContext pContext, boolean shceduling, String pinCase, String startDate,
			String endDate, String status, String merchant, boolean allMerchSel, boolean allStatusSel)
	{
		this();
		this.sessionData = sessionData;
		this.setContext(pContext);
                this.strRefId = sessionData.getProperty("ref_id");
		this.scheduling = shceduling;
		this.pinCase = pinCase;
		start_date = startDate;
		end_date = endDate;
		this.status = status;
		this.merchant = merchant;
		this.allMerchantsSelected = allMerchSel;
		this.allStatusSelected = allStatusSel;
		this.strCurrentDate = DateUtil.formatDateTime(Calendar.getInstance().getTime());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileFooter()
	 */
	@Override
	protected boolean writeDownloadFileFooter() throws IOException
	{
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadFileHeader()
	 */
	@Override
	protected boolean writeDownloadFileHeader() throws IOException
	{
		boolean retValue = false;
		try
		{
			this.writetofile(this.fc, "\""
					+ Languages.getString("jsp.admin.reports.pin_request_report.return_request_report", sessionData.getLanguage()) + "\"\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generatedby", sessionData.getLanguage()) + "\","
					+ sessionData.getUser().getUsername() + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.generationdate", sessionData.getLanguage()) + "\","
					+ this.getStrCurrentDate() + "\n");
			this.writetofile(fc, "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.transactions.filtersapplied", sessionData.getLanguage()) + "\"\n");
			if (sessionData.getProperty("repName") != null)
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage()) + "\","
						+ sessionData.getProperty("repName") + "\n");
			}
			else
			{
				this.writetofile(fc, "\"" + Languages.getString("jsp.admin.iso_name", sessionData.getLanguage()) + "\","
						+ sessionData.getUser().getCompanyName() + "\n");
			}

			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.start_date", sessionData.getLanguage()) + "\"," + this.start_date + "\n");
			this.writetofile(fc, "\"" + Languages.getString("jsp.admin.end_date", sessionData.getLanguage()) + "\"," + this.end_date + "\n");

			if ((this.status != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.pin_request_report.status", sessionData.getLanguage()) + "\",");
				if (this.allStatusSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.status);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}

			if ((this.merchant != null))
			{
				StringBuffer line = new StringBuffer();
				line.append("\"" + Languages.getString("jsp.admin.reports.merchants", sessionData.getLanguage()) + "\",");
				if (this.allMerchantsSelected)
				{
					line.append(Languages.getString("jsp.admin.reports.all", this.sessionData.getLanguage()));
				}
				else
				{
					line.append(this.merchant);
				}
				line.append("\n");
				this.writetofile(fc, line.toString());
			}

			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadFileHeader Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecords()
	 */
	@Override
	protected boolean writeDownloadRecords() throws IOException
	{
		boolean retValue = false;
		Vector<Vector<Object>> reportResults = this.getResults();

		try
		{
			if ((reportResults != null) && (reportResults.size() > 0))
			{
				for (int i = 0; i < reportResults.size(); i++)
				{
					Vector<Object> currentRecord = reportResults.get(i);
					// Write the line number
					this.writetofile(fc, "\"" + String.valueOf(i + 1) + "\",");
					this.writetofile(fc, "\"" + currentRecord.get(0).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(1).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(2).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(3).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(4).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(5).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(6).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(7).toString().trim() + "\", ");
					this.writetofile(fc, "\"" + currentRecord.get(8).toString().trim() + "\"\n");
				}
				retValue = true;
			}
		}
		catch (NumberFormatException localNumberException)
		{
			cat.error("writeDownloadRecords Exception : " + localNumberException.toString());
		}
		catch (Exception localException)
		{
			cat.error("writeDownloadRecords Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ReportDownloader#writeDownloadRecordsHeaders()
	 */
	@Override
	protected boolean writeDownloadRecordsHeaders() throws IOException
	{
		boolean retValue = false;

		ArrayList<ColumnReport> columnHeaders = getReportRecordHeaders();

		if ((columnHeaders != null) && (columnHeaders.size() > 0))
		{
			this.writetofile(fc, "\"#\",");
			for (int i = 0; i < columnHeaders.size(); i++)
			{
				this.writetofile(fc, "\"" + columnHeaders.get(i).getLanguageDescription() + "\"");
				if (i < (columnHeaders.size() - 1))
				{
					this.writetofile(fc, ",");
				}
				else
				{
					this.writetofile(fc, "\n");
				}
			}
			retValue = true;
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#downloadReport()
	 */
	@Override
	public String downloadReport()
	{
		return this.exportToFile();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.debisys.reports.ISupportSiteReport#generateSqlString(com.debisys.
	 * reports.ISupportSiteReport.QUERY_TYPE)
	 */
	@Override
	public String generateSqlString(QUERY_TYPE qType)
	{
		String retValue = null;
		StringBuffer strSQL = new StringBuffer();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date
					+ " 23:59:59.999", false);
			strSQL.append("SELECT	PR.ID, PR.RequestDate, ME.dba, PR.millennium_no, PS.product_id, PR.control_no, ");
			strSQL.append("		PD.description as Product, PS.amount, S.Description as Status, PRR.Description as Reason, ");
			strSQL.append("		S.Code as StatusCode, PRR.Code as ReasonCode ");
			if (this.isScheduling())
			{
				if (qType == QUERY_TYPE.FIXED)
				{
					strSQL.append(", '" + vTimeZoneFilterDates.get(0) + "' AS startDate, ");
					strSQL.append("'" + vTimeZoneFilterDates.get(1) + "' AS endDate ");
				}
				else
				{
					strSQL.append(" " + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " ");
				}
			}
			strSQL.append(" FROM	PINReturnRequests PR WITH (NOLOCK) ");
			strSQL.append("		INNER JOIN terminals TER WITH (NOLOCK) ON PR.millennium_no = TER.millennium_no ");
			strSQL.append("		INNER JOIN merchants ME  WITH (NOLOCK) ON TER.merchant_id = ME.merchant_id ");
			strSQL.append("		INNER JOIN pins PS WITH (NOLOCK) ON PR.control_no = PS.control_no ");
			strSQL.append("		INNER JOIN products PD WITH (NOLOCK) ON PS.product_id = PD.id ");
			strSQL.append("		INNER JOIN PINReturnRequestStatus S WITH (NOLOCK) ON PR.RequestStatus = S.ID ");
			strSQL.append("		INNER JOIN PINReturnRequestReasons PRR WITH (NOLOCK) on PR.RequestReason = PRR.ID ");
			strSQL.append("WHERE 1 = 1 "); // This allows us to treat anything
											// else as an AND condition

			if ((this.pinCase != null) && (!this.pinCase.equals("")))
			{
				strSQL.append(" AND PR.ID = " + this.pinCase);
			}

			if (((this.start_date != null) && (this.start_date.length() > 0)) && ((this.end_date != null) && (this.end_date.length() > 0)))
			{
				if (this.isScheduling())
				{
					if (qType == QUERY_TYPE.RELATIVE)
					{
						strSQL.append(" AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT + " ");
					}
					else if (qType == QUERY_TYPE.FIXED)
					{
						strSQL.append("         AND (PR.RequestDate  >= '" + vTimeZoneFilterDates.get(0) + "'");
						strSQL.append("         AND PR.RequestDate  < '" + vTimeZoneFilterDates.get(1) + "')");
					}
				}
				else
				{
					// If not scheduling, the wild card is removed, and the
					// current report dates are used
					int i;
					while ((i = strSQL.indexOf(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS)) != -1)
					{
						strSQL.delete(i, i + ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS.length());
					}
					strSQL.append("         AND (PR.RequestDate  >= '" + vTimeZoneFilterDates.get(0) + "'");
					strSQL.append("         AND PR.RequestDate  < '" + vTimeZoneFilterDates.get(1) + "')");
				}
			}

			if ((this.status != null) && (!this.status.equals("")))
			{
				if (this.status.indexOf(",") != -1)
				{
					strSQL.append(" AND PR.RequestStatus IN " + this.status);
				}
				else
				{
					strSQL.append(" AND PR.RequestStatus = " + this.status);
				}
			}

			if (this.allMerchantsSelected)
			{
				User currentUser = this.sessionData.getUser();
				strSQL.append( ReportsUtil.getChainToBuildWhereSQL(currentUser.getAccessLevel(), currentUser.getDistChainType(), currentUser.getRefId(),	"ME.rep_id") );
			}
			else
			{
				if ((this.merchant != null) && (!this.merchant.equals("")))
				{
					if (this.merchant.indexOf(",") != -1)
					{
						strSQL.append(" AND TER.merchant_id IN " + this.merchant);
					}
					else
					{
						strSQL.append(" AND TER.merchant_id =" + this.merchant);
					}
				}
			}
			cat.debug(strSQL);
			retValue = strSQL.toString();

		}
		catch (ReportException localReportException)
		{
			cat.error("PinReturnRequestReport.getSqlString. Exception while generationg sql string : " + localReportException.toString());
		}
		catch (Exception localException)
		{
			cat.error("PinReturnRequestReport.getSqlString. Exception while generationg sql string : " + localException.toString());
		}

		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getReportRecordHeaders()
	 */
	@Override
	public ArrayList<ColumnReport> getReportRecordHeaders()
	{
		ArrayList<ColumnReport> retValue = new ArrayList<ColumnReport>();
		try
		{
			retValue.add(new ColumnReport("ID", Languages.getString("jsp.admin.reports.pin_request_report.grid.Case", sessionData.getLanguage())
					.toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("RequestDate", Languages.getString("jsp.admin.reports.pin_request_report.grid.DateRequest",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("dba", Languages.getString("jsp.admin.reports.pin_request_report.grid.BusinessName",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("millennium_no", Languages.getString("jsp.admin.reports.pin_request_report.grid.Site",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("product_id", Languages.getString("jsp.admin.reports.pin_request_report.grid.SKU",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("control_no", Languages.getString("jsp.admin.reports.pin_request_report.grid.Control",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("Product", Languages.getString("jsp.admin.reports.pin_request_report.grid.Product",
					sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("amount", Languages
					.getString("jsp.admin.reports.pin_request_report.grid.Amount", sessionData.getLanguage()).toUpperCase(), String.class, false));
			retValue.add(new ColumnReport("Status", Languages
					.getString("jsp.admin.reports.pin_request_report.grid.Status", sessionData.getLanguage()).toUpperCase(), String.class, false));
		}
		catch (Exception localException)
		{
			cat.error("PinReturnRequestReport.getReportRecordHeaders Exception : " + localException);
		}

		return retValue;
	}

	@Override
	public ArrayList<String> getTitles()
	{
		ArrayList<String> retValue = new ArrayList<String>();

		try
		{
			retValue.add(Languages.getString("jsp.admin.reports.pin_request_report", this.sessionData.getLanguage()));
			Vector<String> vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(this.sessionData.getProperty("ref_id")));
			String noteTimeZone = Languages.getString("jsp.admin.timezone.reportNote", this.sessionData.getLanguage()) + ":&nbsp;"
					+ vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]";
			retValue.add(noteTimeZone);
			retValue.add(Languages.getString("jsp.admin.reports.test_trans", this.sessionData.getLanguage()));
		}
		catch (NumberFormatException localNumberFormatException)
		{
			cat.error("getTitles Exception : " + localNumberFormatException.toString());
		}
		catch (Exception localException)
		{
			cat.error("getTitles Exception : " + localException.toString());
		}
		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#getResults()
	 */
	@Override
	public Vector<Vector<Object>> getResults()
	{
		Vector<Vector<Object>> retValue = new Vector<Vector<Object>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}

			this.setScheduling(false); // If returning results, then scheduling
										// does not have sense
			String strSQL = this.generateSqlString(QUERY_TYPE.NORMAL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();

			while (rs.next())
			{
				Vector<Object> vecTemp = new Vector<Object>();
				vecTemp.add(rs.getString("ID"));
				vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("RequestDate")));
				vecTemp.add(rs.getString("dba"));
				vecTemp.add(rs.getString("millennium_no"));
				vecTemp.add(rs.getString("product_id"));
				vecTemp.add(rs.getString("control_no"));
				vecTemp.add(rs.getString("Product"));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("amount")));
				vecTemp.add(rs.getString("Status"));
				vecTemp.add(rs.getString("Reason"));
				vecTemp.add(TransactionReport.getPINReturnRequestCommentsById(rs.getLong("ID")));
				vecTemp.add(rs.getString("StatusCode"));
				vecTemp.add(rs.getString("ReasonCode"));
				retValue.add(vecTemp);
			}
			if (retValue.size() == 0)
			{
				retValue = null;
			}
		}
		catch (TorqueException localTorqueException)
		{
			cat.error("PinReturnRequestReport.getResults Exception : " + localTorqueException.toString());
		}
		catch (ReportException localReportException)
		{
			cat.error("PinReturnRequestReport.getResults Exception : " + localReportException.toString());
		}
		catch (SQLException localSqlException)
		{
			cat.error("PinReturnRequestReport.getResults Exception : " + localSqlException.toString());
		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
					rs = null;
				}
				if (pstmt != null)
				{
					pstmt.close();
					pstmt = null;
				}
				if (dbConn != null)
				{
					Torque.closeConnection(dbConn);
					dbConn = null;
				}
			}
			catch (SQLException cleaningSqlException)
			{
				cat.error("PinReturnRequestReport.getResults Exception while cleaning DB objects: " + cleaningSqlException.toString());
			}
		}

		return retValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.debisys.reports.ISupportSiteReport#scheduleReport()
	 */
	@Override
	public boolean scheduleReport()
	{
		boolean retValue = false;

		try
		{
			this.setScheduling(true);
			String fixedQuery = this.generateSqlString(QUERY_TYPE.FIXED);
			String relativeQuery = this.generateSqlString(QUERY_TYPE.RELATIVE);

			ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_PIN_RETURN_REQUESTS, 1);
			scheduleReport.setNameDateTimeColumn("PR.RequestDate");
			scheduleReport.setRelativeQuery(relativeQuery);
			scheduleReport.setFixedQuery(fixedQuery);
			TransactionReportScheduleHelper.addMetaDataReport(this.getReportRecordHeaders(), scheduleReport, this.getTitles());
			this.sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
			retValue = true;
		}
		catch (Exception localException)
		{
			cat.error("PinReturnRequestReport.scheduleReport Exception : " + localException.toString());
		}

		return retValue;
	}

}
