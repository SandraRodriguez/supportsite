package com.debisys.reports.schedule;

import java.util.Comparator;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

public class MerchantHashMapComparator implements Comparator<HashMap<String, String>> {

	@Override
	public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
		int result = 0;
		if(o1 != null && o2 != null){
			String name1 = o1.get("dba");
			String name2 = o2.get("dba");
			name1 = StringUtils.isNotEmpty(name1) ? name1 : "";
			name2 = StringUtils.isNotEmpty(name2) ? name2 : "";
			result = name1.compareTo(name2);
		} else if (o1 != null && o2 == null){
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

}
