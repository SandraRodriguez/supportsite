/**
 * 
 */
package com.debisys.reports.schedule;

import java.util.ArrayList;
import com.debisys.utils.ColumnReport;

/**
 * @author fernandob
 *
 */
public interface ISupportSiteReport1
{
	// constants
	public enum QUERY_TYPE{NORMAL, FIXED, RELATIVE};
	
	public String generateSqlString(QUERY_TYPE qType);
	
	public boolean scheduleReport();
	
	public String downloadReport();
	
	public ArrayList<ArrayList<ResultField>> getResults();
	
	public ArrayList<ColumnReport> getReportRecordHeaders();
	
	public ArrayList<String> getTitles();
	
	public boolean isUsingDateLimitInterval();
	
	public int getLimitDays();

}
