package com.debisys.reports;

public class TerminalsTransaction {

	private int terminalId;
	private String terminalDescription;
	private double countTransactions;

	public TerminalsTransaction(int terminalId, String terminalDescription, double countTransactions) {
		super();
		this.terminalId = terminalId;
		this.terminalDescription = terminalDescription;
		this.countTransactions = countTransactions;
	}
	
	public int getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(int terminalId) {
		this.terminalId = terminalId;
	}
	public String getTerminalDescription() {
		return terminalDescription;
	}
	public void setTerminalDescription(String terminalDescription) {
		this.terminalDescription = terminalDescription;
	}
	public double getCountTransactions() {
		return countTransactions;
	}
	public void setCountTransactions(double countTransactions) {
		this.countTransactions = countTransactions;
	}
	
	
	
}
