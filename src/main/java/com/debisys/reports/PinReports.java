/**
 * 
 */
package com.debisys.reports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.customers.RepReports;
import com.debisys.exceptions.ReportException;
import com.debisys.reports.pojo.PinStockPojo;
import com.debisys.reports.pojo.ReportPinStock;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;

/**
 * @author nmartinez
 *
 */
public class PinReports {
	
	private static Logger       cat        = Logger.getLogger(PinReports.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	
	
	
	
	
	/**
	 * @param sessionData
	 * @param application
	 * @param isLoaded
	 * @param scheduleReportType
	 * @return
	 * @throws ReportException
	 */
	public static ReportPinStock getPinStockReport(SessionData sessionData, ServletContext application, boolean isLoaded,int scheduleReportType) throws ReportException
	{
		ReportPinStock report = new ReportPinStock();
		Connection dbConn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
			
		boolean result = false;		
		//String strAccessLevel   = sessionData.getProperty("access_level");
		//String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId 		= sessionData.getProperty("ref_id");
		String orderNo          = sessionData.getProperty("order_no");
		
		String start_date 		= sessionData.getProperty("start_date");
		String end_date 		= sessionData.getProperty("end_date");
		String userName 		= sessionData.getUser().getUsername();
		
		String product_ids      = "0";
		if (  sessionData.getProperty("pids")!=null &&  sessionData.getProperty("pids").length()>0 )
			product_ids	= sessionData.getProperty("pids");
		
		String languageFlag = sessionData.getUser().getLanguageCode();
		String instance = DebisysConfigListener.getInstance(application);
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");	
		   	SimpleDateFormat ssFormat = new SimpleDateFormat("MM/dd/yyyy");    	 
		   	String finalStartDate = formatter.format(ssFormat.parse(start_date));
		   	String finalEndDate=formatter.format(ssFormat.parse(end_date));
		   	
			String strSQL = sql_bundle.getString("getPinStockReport");	
			
			String fixedQuery = strSQL.replaceFirst("\\?", "'" + finalStartDate + "'")
				      .replaceFirst("\\?", "'" + finalEndDate + "'")
				      .replaceFirst("\\?", (isLoaded?"1":"0") )
				      .replaceFirst("\\?", (orderNo.length()>0?orderNo:"0") )
				      .replaceFirst("\\?", strRefId )
				      .replaceFirst("\\?", "'"+product_ids+"'" )
				      .replaceFirst("\\?",  "'"+userName+"'" )
				      .replaceFirst("\\?", "'"+instance+"'" )
					  .replaceFirst("\\?",  "'"+languageFlag+"'" );
			
			if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
			{
				StringBuilder execStore = new StringBuilder();
				execStore.append("DECLARE @startDateRelative VARCHAR(10) ");
				execStore.append("DECLARE @endDateRelative VARCHAR(10) ");
				execStore.append("SELECT @startDateRelative = CONVERT(VARCHAR(10), "+ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE+",101) , @endDateRelative = CONVERT(VARCHAR(10), "+ ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE+",101) ");
				
				String relativeQuery =  strSQL.replaceFirst("\\?", "@startDateRelative" )
									   .replaceFirst("\\?", "@endDateRelative" )
									   .replaceFirst("\\?", (isLoaded?"1":"0") )
									   .replaceFirst("\\?", (orderNo.length()>0?orderNo:"0") )
									   .replaceFirst("\\?", strRefId )
									   .replaceFirst("\\?", "'"+product_ids+"'" )
									   .replaceFirst("\\?",  "'"+userName+"'" )
									   .replaceFirst("\\?",  "'"+instance+"'" )	
									   .replaceFirst("\\?",  "'"+languageFlag+"'" );
				
				execStore.append(relativeQuery);
				
				ScheduleReport scheduleReport = null;
				if ( isLoaded )
				{
					scheduleReport = new ScheduleReport( DebisysConstants.SC_PIN_STOCK_AVAILABLE , 1);
				}					
				else
				{
					scheduleReport = new ScheduleReport( DebisysConstants.SC_PIN_STOCK_LOADED , 1);
				}					
				
				scheduleReport.setRelativeQuery( execStore.toString() );
				
				scheduleReport.setFixedQuery( fixedQuery );	
				ArrayList<ColumnReport> headers = RepReports.getHeadersPinStock();
				TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, null );				
				sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);
				
			}
			
	  	   	ps = dbConn.prepareStatement( strSQL );  	
		   	
	  	    ps.setString(1, finalStartDate);
		    ps.setString(2, finalEndDate);
		    ps.setBoolean(3,isLoaded);
			
		    if(orderNo!=null && !orderNo.equals(""))
				ps.setString(4,orderNo);
			else
				ps.setString(4,"0");
			ps.setString(5,strRefId);
			ps.setString(6,product_ids);			  
			
			ps.setString(7, userName);
			ps.setString(8, instance);
			ps.setString(9,languageFlag);
	        
			cat.debug("Executing getPinStockReport SP: [" + fixedQuery + "] ");
		  	rs = ps.executeQuery();       
		  	cat.debug("End executing getPinStockReport SP: [" + fixedQuery + "] ");		
			while (rs.next())
			{
				String desc = rs.getString(1);
				String typeRecord = rs.getString(3);
				String[] dataValues = desc.split(";");
				
				if ( typeRecord.equals("T") )
				{
					for( String value : dataValues )
					{
						report.getTitles().add(value);
					}
				}
				else if ( typeRecord.equals("R") )
				{
					PinStockPojo pinPojo = new PinStockPojo();
					for( String value : dataValues )
					{
						pinPojo.getData().add(value);
					}
					report.getData().add(pinPojo);
					result = true;
				}
				else if ( typeRecord.equals("W") )
				{
					for( String value : dataValues )
					{
						report.getWarnings().add(value);
					}
				}
			}			
			rs.close();
			ps.close();			
		}
		catch (Exception e)
		{
			cat.error("Error during getPinStockReport", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
			if ( !result )
			{
				report = null;
			}
		}
		return report;
	}
	
}
