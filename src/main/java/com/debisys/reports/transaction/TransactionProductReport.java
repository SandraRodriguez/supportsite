package com.debisys.reports.transaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.TransactionReport;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

public class TransactionProductReport {

	private static final Logger logger = Logger.getLogger(TransactionRepReport.class);
	
	private ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private TransactionReport transReport;
	
	public TransactionProductReport(TransactionReport transReport) {
		this.transReport = transReport;
	}
	
	public Vector getProductSummary(SessionData sessionData, ServletContext context,
			boolean permissionInterCustomConfig, int scheduleReportType, ArrayList<ColumnReport> headers,
			ArrayList<String> titles) throws ReportException {
		transReport.setWarningmessage(new Vector<String>());
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector vecProductSummary = new Vector();
		String strSQL = "";
		String strWhereSQL = "";

		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
                boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(sessionData, context);
                String wTadditionalData = ((isIntlAndHasDataPromoPermission) ? " wt.additionalData, " : "");

                if (permissionInterCustomConfig) {
                    strSQL = sql_bundle.getString("getProductSummaryMxData");
		} else {
                    strSQL = sql_bundle.getString("getProductSummaryData");
		}

		try {
                    strSQL = strSQL.replace("additionalData_field", wTadditionalData);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				logger.error("Can't get database connection");
				throw new ReportException();
			}

			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, transReport.getStartDate(), transReport.getEndDate() + " 23:59:59.999", false);
			if (permissionInterCustomConfig) {
				double dTax = TransactionReport.getTaxValue(sessionData, transReport.getEndDate());
				transReport.setWarningmessage(transReport.getWarningMessage(sessionData, transReport.getEndDate(), transReport.getStartDate()));
				if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
					strSQL = strSQL.replaceAll("dTax", "@tax");
				} else {
					strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
				}
			}

			if (strAccessLevel.equals(DebisysConstants.ISO)) {
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
					strSQL = strSQL + " wt.rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = "
							+ sessionData.getProperty("ref_id") + ") ";
				} else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
					strSQL = strSQL + " wt.rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= "
							+ DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
							+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT
							+ " AND iso_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= "
							+ DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + ")))";
				}
				if (StringUtils.isNotEmpty(transReport.getMerchantIds())) {
					strSQL += " AND wt.merchant_id in (" + transReport.getMerchantIds() + ")";
				}
			} else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
				strSQL = strSQL + " wt.rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= "
						+ DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
						+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT
						+ " AND iso_id = " + strRefId + "))";
			} else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
				strSQL = strSQL + " wt.rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= "
						+ DebisysConstants.REP_TYPE_REP + " AND iso_id = " + strRefId + ")";
			}

			else if (strAccessLevel.equals(DebisysConstants.REP)) {
				strSQL = strSQL + " wt.rep_id = " + strRefId;
			} else if (strAccessLevel.equals(DebisysConstants.MERCHANT)) {
				strSQL = strSQL + " wt.merchant_id = " + strRefId;
			}

			if (StringUtils.isNotEmpty(transReport.getProductIds())) {
				strSQL = strSQL + " AND wt.id in (" + transReport.getProductIds() + ") ";
			}

			String strSQLGroupBy = " GROUP BY wt.id," + wTadditionalData + "wt.description";

			if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
				String taxAmountSQL = " , sum(wt.amount) - sum(wt.amount / (1 + isnull(wt.taxpercentage / 10000, @tax - 1))) AS taxAmount ";

				strSQL = strSQL.replaceFirst("TOKENS_FROM_SQL_JASPER_REPORTS", taxAmountSQL + "  , @channel as channel_percent "
								+ ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS + " into #trxSummByProduct");
				strWhereSQL = " AND (wt.datetime >= @startDate and  wt.datetime <= @endDate ) ";

				StringBuilder strSQLSummaryScriptJasper = new StringBuilder();
				strSQLSummaryScriptJasper.append("DECLARE @startDate DATETIME ");
				strSQLSummaryScriptJasper.append("DECLARE @endDate DATETIME ");
				strSQLSummaryScriptJasper.append("DECLARE @entity NUMERIC(15, 0) ");
				strSQLSummaryScriptJasper.append("DECLARE @tax DECIMAL(10, 2) ");
				strSQLSummaryScriptJasper.append("DECLARE @accessLevel INT ");
				strSQLSummaryScriptJasper.append("DECLARE @distChainType INT ");
				strSQLSummaryScriptJasper.append("DECLARE @channel MONEY ");
				strSQLSummaryScriptJasper.append("SELECT @channel = 0 ");
				strSQLSummaryScriptJasper.append("SELECT @startDate=?, @endDate=?, @entity=?, @accessLevel=?, @distChainType=? ");
				strSQLSummaryScriptJasper.append("EXEC GetTaxByEntityAndDates @accessLevel, @distChainType, @entity, @endDate, @tax OUTPUT ");

				String headerFixedToJasperSQL = strSQLSummaryScriptJasper.toString()
						.replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(0).toString() + "'")
						.replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(1).toString() + "'")
						.replaceFirst("\\?", strRefId).replaceFirst("\\?", strAccessLevel)
						.replaceFirst("\\?", strDistChainType);

				String tryControl = "BEGIN TRY ";

				StringBuilder endTryControl = new StringBuilder();

				endTryControl.append("    UPDATE #trxSummByProduct SET channel_percent =( (iso_commission+agent_commission+subagent_commission+rep_commission+merchant_commission)/total_sales)*100 WHERE total_sales>0");
				endTryControl.append("    SELECT * FROM #trxSummByProduct ");
				endTryControl.append("    DROP TABLE #trxSummByProduct ");
				endTryControl.append("END TRY ");
				endTryControl.append("BEGIN CATCH ");
				endTryControl.append("  PRINT 'ERROR MESSAGE:'");
				endTryControl.append("  PRINT ERROR_MESSAGE()");
				endTryControl.append("  PRINT 'EROR LINE:'");
				endTryControl.append("  PRINT ERROR_LINE()");
				endTryControl.append(" IF OBJECT_ID('tempdb..#trxSummByProduct') IS NOT NULL ");
				endTryControl.append(" BEGIN ");
				endTryControl.append("      PRINT 'DELETING temp TABLE #trxSummByProduct' ");
				endTryControl.append("      DROP TABLE #trxSummByProduct ");
				endTryControl.append("      PRINT 'DELETING OK temp TABLE #trxSummByProduct' ");
				endTryControl.append(" END");
				endTryControl.append(" ");
				endTryControl.append("END CATCH ");

				String fixedQuery = headerFixedToJasperSQL + " " + tryControl + " " + strSQL + strWhereSQL + strSQLGroupBy + " " + endTryControl;

				String headerToJasperSQL = strSQLSummaryScriptJasper.toString()
						.replaceFirst("\\?", ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE)
						.replaceFirst("\\?", ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE)
						.replaceFirst("\\?", strRefId).replaceFirst("\\?", strAccessLevel)
						.replaceFirst("\\?", strDistChainType);

				String relativeQuery = headerToJasperSQL + " " + tryControl + " " + strSQL + " " + strWhereSQL + " " + strSQLGroupBy + " " + endTryControl;

				ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_TRX_SUMM_BY_PRODUCT, 1);
				scheduleReport.setNameDateTimeColumn("wt.datetime");

				logger.debug(fixedQuery);

				scheduleReport.setRelativeQuery(relativeQuery);
				scheduleReport.setFixedQuery(fixedQuery);

				TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
				sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
				return null;

			} else {
				strSQL = strSQL.replaceFirst("TOKENS_FROM_SQL_JASPER_REPORTS", "");
			}

			strWhereSQL = " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";

			strSQL = strSQL + strWhereSQL + strSQLGroupBy;
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";

			try {
				if (StringUtils.isNotEmpty(transReport.getCol()) && StringUtils.isNotEmpty(transReport.getSort())) {
					intCol = Integer.parseInt(transReport.getCol());
					intSort = Integer.parseInt(transReport.getSort());
				}
			} catch (NumberFormatException nfe) {
				intCol = 0;
				intSort = 0;
			}

			if (intSort == 2) {
				strSort = "DESC";
			} else {
				strSort = "ASC";
			}

			if (permissionInterCustomConfig) {
				switch (intCol) {
				case 1:
					strCol = "wt.description";
					break;
				case 2:
					strCol = "wt.id";
					break;
				case 3:
					strCol = "qty";
					break;
				case 4:
					strCol = "total_sales";
					break;
				case 5:
					strCol = "merchant_commission";
					break;
				case 6:
					strCol = "rep_commission";
					break;
				case 7:
					strCol = "subagent_commission";
					break;
				case 8:
					strCol = "agent_commission";
					break;
				case 9:
					strCol = "iso_commission";
					break;
				case 10:
					strCol = "adj_amount";
					break;
				case 11:
					strCol = "tax_total_sales";
					break;
				case 12:
					strCol = "tax_merchant_commission";
					break;
				case 13:
					strCol = "tax_rep_commission";
					break;
				case 14:
					strCol = "tax_subagent_commission";
					break;
				case 15:
					strCol = "tax_agent_commission";
					break;
				case 16:
					strCol = "tax_iso_commission";
					break;
				case 17:
					strCol = "total_bonus";
					break;
				case 18:
					strCol = "total_recharge";
					break;
				default:
					strCol = "wt.description";
					break;
				}

				strSQL = strSQL + " order by " + strCol + " " + strSort;
				logger.debug(strSQL);

				pstmt = dbConn.prepareStatement(strSQL);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Vector vecTemp = new Vector();
					if (permissionInterCustomConfig) {
						if (rs.getInt("taxtypecheck") > 0 && !transReport.checkfortaxtype(sessionData)) {
							transReport.getWarningMessage().add(Languages.getString("jsp.admin.reports.taxtypewarning", sessionData.getLanguage()));
						}
					}
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("id")));
					vecTemp.add(rs.getString("qty"));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_sales")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("adj_amount")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_total_sales")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_merchant_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_rep_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_subagent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_agent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_iso_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_bonus")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_recharge")));
                                        if (isIntlAndHasDataPromoPermission) vecTemp.add(rs.getString("additionalData"));
					vecProductSummary.add(vecTemp);
				}
				rs.close();
				pstmt.close();
			} else {
				switch (intCol) {
				case 1:
					strCol = "wt.description";
					break;
				case 2:
					strCol = "wt.id";
					break;
				case 3:
					strCol = "qty";
					break;
				case 4:
					strCol = "total_sales";
					break;
				case 5:
					strCol = "merchant_commission";
					break;
				case 6:
					strCol = "rep_commission";
					break;
				case 7:
					strCol = "subagent_commission";
					break;
				case 8:
					strCol = "agent_commission";
					break;
				case 9:
					strCol = "iso_commission";
					break;
				case 10:
					strCol = "adj_amount";
					break;
				case 17:
					strCol = "total_bonus";
					break;
				case 18:
					strCol = "total_recharge";
					break;
				default:
					strCol = "wt.description";
					break;
				}

				strSQL = strSQL + " order by " + strCol + " " + strSort;
				logger.debug(strSQL);

				pstmt = dbConn.prepareStatement(strSQL);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Vector<String> vecTemp = new Vector();
					vecTemp.add(StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("id")));
					vecTemp.add(rs.getString("qty"));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_sales")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("adj_amount")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_bonus")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_recharge")));
                                        if (isIntlAndHasDataPromoPermission) vecTemp.add(rs.getString("additionalData"));
					vecProductSummary.add(vecTemp);
				}
				rs.close();
				pstmt.close();
			}
		} catch (Exception e) {
			logger.error("Error during getProductSummary", e);
			throw new ReportException();
		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				logger.error("Error during closeConnection", e);
			}
		}
		return vecProductSummary;
	}
	
	public Vector getProductSummaryMx(SessionData sessionData, ServletContext context) throws ReportException
	{
		Connection dbConn = null;
		Vector vecProductSummary = new Vector();
		String strSQL = sql_bundle.getString("getProductSummaryMx");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");

		try
		{
            strSQL = strSQL.replaceFirst("TOKENS_FROM_SQL_JASPER_REPORTS", "");
			double dTax = 1 + (DebisysConfigListener.getMxValueAddedTax(context) / 100);
			strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				logger.error("Can't get database connection");
				throw new ReportException();
			}

			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, transReport.getStartDate(), transReport.getEndDate() + " 23:59:59.999", false);
			// get a count of total matches
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL = strSQL + " wt.rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " + sessionData.getProperty("ref_id")
					+ ") AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "')";
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL = strSQL + " wt.rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
					+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
					+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + ")))"
					+ " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "')";
				}
				if (StringUtils.isNotEmpty(transReport.getMerchantIds()))
				{
					strSQL += " AND wt.merchant_id in (" + transReport.getMerchantIds() + ")";
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSQL = strSQL + " wt.rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
				+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId + "))"
				+ " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "')";
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				strSQL = strSQL + " wt.rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_REP + " AND iso_id = " + strRefId
				+ ")" + " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "')";
			}

			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSQL = strSQL + " wt.rep_id = " + strRefId + " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "')";
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				strSQL = strSQL + " wt.merchant_id = " + strRefId + " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "')";
			}

			if (StringUtils.isNotEmpty(transReport.getProductIds()))
			{
				strSQL = strSQL + " AND wt.id in (" + transReport.getProductIds() + ") ";
			}
			strSQL = strSQL + " group by wt.id, wt.description ";
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";

			try
			{
				if (StringUtils.isNotBlank(transReport.getCol()) && StringUtils.isNotBlank(transReport.getSort()))
				{
					intCol = Integer.parseInt(transReport.getCol());
					intSort = Integer.parseInt(transReport.getSort());
				}
			}
			catch (NumberFormatException nfe)
			{
				intCol = 0;
				intSort = 0;
			}

			if (intSort == 2)
			{
				strSort = "DESC";
			}
			else
			{
				strSort = "ASC";
			}

			switch (intCol)
			{
				case 1:
					strCol = "wt.description";
					break;
				case 2:
					strCol = "wt.id";
					break;
				case 3:
					strCol = "qty";
					break;
				case 4:
					strCol = "total_sales";
					break;
				case 5:
					strCol = "merchant_commission";
					break;
				case 6:
					strCol = "rep_commission";
					break;
				case 7:
					strCol = "subagent_commission";
					break;
				case 8:
					strCol = "agent_commission";
					break;
				case 9:
					strCol = "iso_commission";
					break;
				case 10:
					strCol = "adj_amount";
					break;
				case 11:
					strCol = "tax_total_sales";
					break;
				case 12:
					strCol = "tax_merchant_commission";
					break;
				case 13:
					strCol = "tax_rep_commission";
					break;
				case 14:
					strCol = "tax_subagent_commission";
					break;
				case 15:
					strCol = "tax_agent_commission";
					break;
				case 16:
					strCol = "tax_iso_commission";
					break;
				case 17:
					strCol = "total_bonus";
					break;
				case 18:
					strCol = "total_recharge";
					break;
				default:
					strCol = "wt.description";
					break;
			}

			strSQL = strSQL + " order by " + strCol + " " + strSort;
			logger.debug(strSQL);
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("description")));
				vecTemp.add(StringUtil.toString(rs.getString("id")));
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("adj_amount")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_total_sales")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_merchant_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_rep_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_subagent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_agent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_iso_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("total_recharge")));
				vecProductSummary.add(vecTemp);
			}
			rs.close();
			pstmt.close();

		}
		catch (Exception e)
		{
			logger.error("Error during getProductSummaryMx", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				logger.error("Error during closeConnection", e);
			}
		}
		return vecProductSummary;
	}// End of function getProductSummaryMx
	
	public Vector<Object> getProductTransactions(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context, boolean frtCurrency) throws ReportException {
		Connection dbConn = null;
		Vector<Object> vecTransactions = new Vector<Object>();
		String strSearchSQL = "";
		String deploymentType = DebisysConfigListener.getDeploymentType(context);
		String strSQL="";
		boolean viewReferenceCard = false;
		
		String viewReferenceCardJOIN="";
		boolean isDeploymentInternational = DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
											DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
		
		if ( isDeploymentInternational ){
			//DTU-1204
			viewReferenceCard = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
		}
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				logger.error("Can't get database connection");
				throw new TransactionException();
			}
			// get a count of total matches
			PreparedStatement pstmt = null;
			String strRefId = sessionData.getProperty("ref_id");
			String strAccessLevel = sessionData.getProperty("access_level");
			String strDistChainType = sessionData.getProperty("dist_chain_type");

			/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			String strSQLadd ="";
			strSQLadd= ",wt.merchant_rate * (CASE p.ach_type WHEN 6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt ELSE CASE  WHEN pcp.PCTerminalClass like '%RegulatoryFee' THEN wt.amount-ISNULL(wt.transFixedFeeAmt,0) ELSE wt.amount END END) /100 as merchant_commission, "
				+ "wt.rep_rate * (CASE p.ach_type WHEN 6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt ELSE CASE  WHEN pcp.PCTerminalClass like '%RegulatoryFee' THEN wt.amount-ISNULL(wt.transFixedFeeAmt,0) ELSE wt.amount END END) /100 as rep_commission,  "
				+ "wt.agent_rate * (CASE p.ach_type WHEN 6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt ELSE CASE  WHEN pcp.PCTerminalClass like '%RegulatoryFee' THEN wt.amount-ISNULL(wt.transFixedFeeAmt,0) ELSE wt.amount END END) /100 as agent_commission, "
				+ "wt.subagent_rate * (CASE p.ach_type WHEN 6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt ELSE CASE  WHEN pcp.PCTerminalClass like '%RegulatoryFee' THEN wt.amount-ISNULL(wt.transFixedFeeAmt,0) ELSE wt.amount END END) /100 as subagent_commission, "
				+ "wt.iso_rate * (CASE p.ach_type WHEN 6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt ELSE CASE  WHEN pcp.PCTerminalClass like '%RegulatoryFee' THEN wt.amount-ISNULL(wt.transFixedFeeAmt,0) ELSE wt.amount END END) /100 as iso_commission, "
				+ "isnull(convert(varchar, mt.backupSku), ' ') as SKU, isnull(prov.display_name, ' ') as provider_name, rc.Receipt ";
			/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
			
			if ( viewReferenceCard ){
				//DTU-1204
				strSQLadd += " ,htr.info2, htr.info1 ";
				viewReferenceCardJOIN=" LEFT OUTER JOIN hostTransactionsrecord htr with(nolock) on htr.transaction_id = wT.rec_id ";
			}
			
			Vector<String> vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, transReport.getStartDate(), DateUtil.addSubtractDays(transReport.getEndDate(), 1), false);
			double dTax = 0;
			if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && isDeploymentInternational )
			{
				dTax=TransactionReport.getTaxValue(sessionData, transReport.getEndDate());
				strSQL = "SELECT DISTINCT wt.rec_id, wt.millennium_no, wt.dba, wt.phys_city, wt.phys_county, wt.merchant_id,  " + "wt.amount as amount, wt.amount / (1 + isnull(wt.taxpercentage / 10000, " + Double.toString(dTax) +" - 1) ) as netamount,wt.trueamount,wt.amount - (wt.amount / (1 + isnull(wt.taxpercentage / 10000, " + Double.toString(dTax) +" - 1) ) ) as taxamount, "
				+ "wt.clerk, dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", wt.datetime, 1) as datetime, wt.control_no, wt.clerk_name, wt.ani, wt.isQRCode, wt.password, wt.balance, wt.id, " + "wt.description, wt.transaction_type, wt.phys_state, wt.other_info, ISNULL(ib.vendorName, wt.additionalData) AS additionalData,wt.invoice_no, p.program_id "
				+ ", wt.bonus_amount, (wt.bonus_amount + wt.amount) as total_recharge, (wt.taxpercentage / 100) as taxpercentage, wt.taxtype "
				/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				+ strSQLadd
				/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				+ " FROM web_transactions wt WITH (NOLOCK) left join PCTerminalProducts pcp WITH (NOLOCK) ON pcp.ProductId = wt.id and pcp.language = 1 " +
						"left join products p WITH (NOLOCK) ON p.id = wt.id " +
				" LEFT JOIN ( SELECT ib1.vendorName, ib1.vendorId AS vn1, ib2.backVendorId AS vn2 FROM IPPBiller AS ib1 WITH (NOLOCK) LEFT JOIN IPPBackBiller AS ib2 WITH (NOLOCK) ON (ib1.vendorId = ib2.vendorId)) As ib ON (ib.vn1 = wt.additionalData OR ib.vn2 = wt.additionalData) "
				+ " LEFT JOIN multisourceTransactions mt with(nolock) ON wt.rec_id = mt.rec_id LEFT JOIN products sku with(nolock) ON mt.backupSku = sku.id " 
                                + " LEFT JOIN provider prov with(nolock) ON sku.program_id = prov.provider_id "
                                + " LEFT JOIN receiptcache rc with(nolock) ON rc.transactionId =  wt.rec_id "        
				+ viewReferenceCardJOIN 
				+ " WHERE ";
			}
			else
			{
				strSQL = "SELECT DISTINCT wt.rec_id, wt.millennium_no, wt.dba, wt.phys_city, wt.phys_county, wt.merchant_id, wt.amount, "
					+ "wt.clerk, dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", wt.datetime, 1) as datetime, wt.control_no, wt.clerk_name, wt.ani, wt.isQRCode, wt.password, wt.balance, wt.id, " + "wt.description, wt.transaction_type, wt.phys_state, wt.other_info, ISNULL(ib.vendorName, wt.additionalData) AS additionalData,wt.invoice_no, p.program_id "
					+ ", wt.bonus_amount, (wt.bonus_amount + wt.amount) as total_recharge "
					/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					+ strSQLadd
					/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					+	" FROM web_transactions wt WITH (NOLOCK) left join PCTerminalProducts pcp WITH (NOLOCK) ON pcp.ProductId = wt.id and pcp.language = 1 " +
							"left join products p WITH (NOLOCK) ON p.id = wt.id " +
					" LEFT JOIN ( SELECT ib1.vendorName, ib1.vendorId AS vn1, ib2.backVendorId AS vn2 FROM IPPBiller AS ib1 WITH (NOLOCK) LEFT JOIN IPPBackBiller AS ib2 WITH (NOLOCK) ON (ib1.vendorId = ib2.vendorId)) As ib ON (ib.vn1 = wt.additionalData OR ib.vn2 = wt.additionalData) "
					+ " LEFT JOIN multisourceTransactions mt with(nolock) ON wt.rec_id = mt.rec_id LEFT JOIN products sku with(nolock) ON mt.backupSku = sku.id "
                                        +" LEFT JOIN provider prov with(nolock) ON sku.program_id = prov.provider_id "
                                        +" LEFT JOIN receiptcache rc with(nolock) ON rc.transactionId =  wt.rec_id "
					+ viewReferenceCardJOIN
					+ " WHERE ";
			}

			String strSQLWhere = " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND wt.datetime < '" + vTimeZoneFilterDates.get(1) + "') ";
			// Added by Jacuna. RS25 DBSY-525
			boolean blnUSEAdditionalFields = ((deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
					&& sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO)) || 
						(deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && 
							strAccessLevel.equals(DebisysConstants.ISO)));
			if (blnUSEAdditionalFields)
			{
				String strAdditionalFields = "";
				String strAdditionalJoins = "";
				strSQL = "SELECT DISTINCT wt.rec_id, wt.millennium_no, wt.dba, wt.phys_city, wt.phys_county, wt.merchant_id, wt.amount, "
					+ "wt.clerk, dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", wt.datetime, 1) as datetime, wt.control_no, wt.clerk_name, wt.ani, wt.isQRCode, wt.password, wt.balance, wt.id, "
					+ "wt.description, wt.product_trans_type, wt.transaction_type, wt.phys_state, wt.other_info, ISNULL(ib.vendorName, wt.additionalData) AS additionalData,wt.invoice_no, p.program_id ";
				if (sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN))
				{
					strAdditionalFields = ", t.pin";
					strAdditionalJoins = " INNER JOIN transactions AS t WITH (nolock) ON wt.rec_id = t.rec_id ";
				}
				if (sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE))
				{
					strAdditionalFields += ", dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + strRefId + ", CASE isnull(ach_transaction_audit.Process_Date,-1) WHEN -1 THEN BillingBatch.endDate ELSE ach_transaction_audit.Process_Date END, 1) as ach_datetime";
					strAdditionalJoins += " LEFT OUTER JOIN Billing WITH (nolock) ON (wt.rec_id = Billing.recid AND wt.merchant_id = Billing.entityid) AND Billing.billingStatusId <> 4 " +
					"LEFT OUTER JOIN ach_transaction_audit WITH (nolock) ON ach_transaction_audit.entity_id = wt.merchant_id and ach_transaction_audit.rec_id = wt.rec_id "+
					"LEFT OUTER JOIN BillingBatch WITH (nolock) ON (billing.billingBatchId = BillingBatch.billingBatchId and BillingBatch.billingBatchStatusId = 2) ";
				}

				strSQL += strSQLadd /*  DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
				+strAdditionalFields + " FROM web_transactions wt WITH (NOLOCK) left join PCTerminalProducts pcp WITH (NOLOCK) ON pcp.ProductId = wt.id and pcp.language = 1 " +
						"left join products p WITH (NOLOCK) ON p.id = wt.id " + strAdditionalJoins;
				strSQL += " LEFT JOIN ( SELECT ib1.vendorName, ib1.vendorId AS vn1, ib2.backVendorId AS vn2 FROM IPPBiller AS ib1 WITH (NOLOCK) LEFT JOIN IPPBackBiller AS ib2 WITH (NOLOCK) ON (ib1.vendorId = ib2.vendorId)) As ib ON(ib.vn1 = wt.additionalData OR ib.vn2 = wt.additionalData) "; 
				strSQL += " LEFT JOIN multisourceTransactions mt with(nolock) ON wt.rec_id = mt.rec_id LEFT JOIN products sku with(nolock) ON mt.backupSku = sku.id "  
                                       + " LEFT JOIN provider prov with(nolock) ON sku.program_id = prov.provider_id "
                                       + " LEFT JOIN receiptcache rc with(nolock) ON rc.transactionId =  wt.rec_id ";        
				strSQL += " WHERE ";

				strSQLWhere = " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND wt.datetime < '" + vTimeZoneFilterDates.get(1) + "') ";

			}
			// end. Added by Jacuna. RS25 DBSY-525

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				String sUseWTid = " wt.id= ";
				if (blnUSEAdditionalFields)
				{
					sUseWTid = " wt.id= ";
				}
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSearchSQL = sUseWTid + transReport.getProductId() + " AND rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id=" + strRefId + ")";
				}
				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSearchSQL = sUseWTid + transReport.getProductId() + " AND rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP
					+ " AND iso_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
					+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_AGENT + " AND iso_id=" + strRefId + "))) ";
				}
				
				if (StringUtils.isNotEmpty(transReport.getMerchantIds()))
				{
					if (blnUSEAdditionalFields) {
						strSQLWhere += " AND wt.merchant_id in (" + transReport.getMerchantIds() + ")";
					} else {
						strSQLWhere += " AND wt.merchant_id in (" + transReport.getMerchantIds() + ")";
					}
				}
			}
			else if (strAccessLevel.equals(DebisysConstants.AGENT))
			{
				strSearchSQL = " wt.id = " + transReport.getProductId() + " AND rep_id IN " + " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP
				+ " AND iso_id IN " + "  (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id=" + strRefId + ")) ";
			}
			else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
			{
				strSearchSQL = " wt.id = " + transReport.getProductId() + " AND rep_id IN " + " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP
				+ " and iso_id=" + strRefId + ")";
			}
			else if (strAccessLevel.equals(DebisysConstants.REP))
			{
				strSearchSQL = " wt.id = " + transReport.getProductId() + " AND rep_id = " + strRefId;
			}
			else if (strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				strSearchSQL = " wt.id = " + transReport.getProductId() + " and merchant_id=" + strRefId;
			}
			strSQLWhere += " order by wt.rec_id";
			String fullSql = strSQL + strSearchSQL + strSQLWhere;
			pstmt = dbConn.prepareStatement(fullSql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			logger.debug(fullSql);
			ResultSet rs = pstmt.executeQuery();
			int intRecordCount = DbUtil.getRecordCount(rs);
			// first row is always the count
			vecTransactions.add(Integer.valueOf(intRecordCount));

			if (intRecordCount > 0)
			{
				rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
				for (int i = 0; i < intRecordsPerPage; i++)
				{
					Vector<String> vecTemp = new Vector<String>();
					vecTemp.add(StringUtil.toString(rs.getString("rec_id")));
					vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
					vecTemp.add(StringUtil.toString(rs.getString("dba")));
					vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
					vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
					vecTemp.add(StringUtil.toString(rs.getString("phys_city")));
					if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals(""))
					{
						vecTemp.add(StringUtil.toString(rs.getString("clerk_name")));
					}
					else
					{
						vecTemp.add("N/A");
					}
					String ani = StringUtil.toString(rs.getString("ani"));
                                        String isQRCode = rs.getString("isQRCode");
                                        if(isQRCode == null || (isQRCode != null && isQRCode.equals("0"))){
                                            vecTemp.add( ani );
                                        }
                                        else if (isQRCode != null && isQRCode.equals("1") && sessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS)){
                                            vecTemp.add( ani );
                                        }
                                        else{
                                            vecTemp.add(StringUtil.hideXNumberDigits(ani));
                                        }
                                        
					vecTemp.add(StringUtil.toString(rs.getString("password")));// password			// 9
					vecTemp.add(NumberUtil.formatAmount(rs.getString("amount")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("balance")));				// 11
					// product id/description
					vecTemp.add(StringUtil.toString(rs.getString("id")) + "/" + StringUtil.toString(rs.getString("description")));
					vecTemp.add(StringUtil.toString(rs.getString("control_no")));
					vecTemp.add(StringUtil.toString(rs.getString("transaction_type")));				// 14
					String other_info = StringUtil.toString(rs.getString("other_info"));
					if (other_info.trim() != "")
					{
                                            if (other_info.length() > 6)
                                            {
                                                other_info = other_info.substring((other_info.length() - 6), other_info.length());
                                            }
					}
					vecTemp.add(other_info);
					vecTemp.add(StringUtil.toString(rs.getString("phys_state")));					// 16

					if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && sessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) && strAccessLevel.equals(DebisysConstants.ISO))
					{
                                            if (rs.getString("product_trans_type").equals("2") && Double.parseDouble(rs.getString("amount").replaceAll(",", "")) > 0)
                                            {
                                                vecTemp.add(StringUtil.toString(rs.getString("pin")));
                                            }
                                            else
                                            {
                                                vecTemp.add("N/A");
                                            }
					}
					else
					{
                                            vecTemp.add("");
					}

					// 17
					if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC) && sessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE) && strAccessLevel.equals(DebisysConstants.ISO))
					{
                                            vecTemp.add(StringUtil.toString((rs.getTimestamp("ach_datetime") != null) ? DateUtil.formatDateTime(rs.getTimestamp("ach_datetime")) : "TBD"));
					}else{
                                            vecTemp.add("");
					}

					// 18
					vecTemp.add(StringUtil.toString(rs.getString("additionalData")));
					vecTemp.add(StringUtil.toString(rs.getString("program_id")));
					vecTemp.add(StringUtil.toString(rs.getString("invoice_no")));					 // 21
					if(DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
							&& DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
					{
						vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("bonus_amount"))));				// 22
						vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("total_recharge"))));			// 23
						if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)){
							vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("netamount"))));			// 24
							vecTemp.add(StringUtil.toString(NumberUtil.formatAmount(rs.getString("taxamount"))));			// 25
							if(rs.getString("taxpercentage") != null)
							{
								vecTemp.add(NumberUtil.formatAmount(rs.getString("taxpercentage")));
								String taxType = "";
								if(rs.getString("taxtype") != null) {
									taxType = rs.getString("taxtype");
									if(taxType.equals("0"))
									{
										taxType = "Local";
									}
									else if (taxType.equals("1"))
									{
										taxType = "Inbound CrossBorder";
									}
									else if (taxType.equals("2"))
									{
										taxType = "Outbound CrossBorder";
									}
								}
								vecTemp.add(taxType);
							} else {
								vecTemp.add(NumberUtil.formatAmount(String.valueOf((dTax - 1) * 100)));
								vecTemp.add("Local");
							}
						} else {
							vecTemp.add("");
							vecTemp.add("");
							vecTemp.add("");
							vecTemp.add("");
						}
					}
					else{
						vecTemp.add("");				// 22
						vecTemp.add("");				// 23
						vecTemp.add("");
						vecTemp.add("");
						vecTemp.add("");
						vecTemp.add("");
						vecTemp.add("");
						vecTemp.add("");
					}
					vecTemp.add(StringUtil.toString(rs.getString("phys_county")));
					/* DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_commission")));
					/* END DBSY-944 SS Enhancement for Transactions Report - include Commission breakouts - EM R33*/
					
					if ( viewReferenceCard ){
						String info = "";
						String info1 = StringUtil.toString(rs.getString("info1"));
						String info2 = StringUtil.toString(rs.getString("info2"));
						if ( info1!="null" && info1.startsWith("s") && info1.indexOf("=")!=-1 && !info2.equals(ani) && ani.length()>0 ){
							info = info1.substring(1);
						}						
						vecTemp.add(info);
					} else {
			        	vecTemp.add("");
			        }
					vecTemp.add(rs.getString("sku"));
					vecTemp.add(rs.getString("provider_name"));
                                        
                                        if (isQRCode != null && isQRCode.equals("1") && sessionData.checkPermission(DebisysConstants.PERM_SHOW_ACCOUNTID_QRCODE_TRANSACTIONS)){
                                            vecTemp.add( "(QRCode)" );
                                        }
                                        else{
                                            vecTemp.add("");
                                        }
                                        vecTemp.add("DTURECEIPT_"+rs.getString("Receipt"));
					vecTransactions.add(vecTemp);
					if (!rs.next())
					{
						break;
					}
				}
			}
			pstmt.close();
			rs.close();
			}
		catch (Exception e) {
			logger.error("Error during getProductTransactions", e);
			throw new ReportException();
		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				logger.error("Error during closeConnection", e);
			}
		}
		return vecTransactions;
	}
}
