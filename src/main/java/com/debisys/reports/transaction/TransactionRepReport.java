package com.debisys.reports.transaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.ReportsUtil;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.summarys.DownloadsSummary;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

/**
 * 
 * @author cmercado
 *
 */
public class TransactionRepReport {
	
	private static final Logger logger = Logger.getLogger(TransactionRepReport.class);
	
	private ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private TransactionReport transReport;
	
	public TransactionRepReport(TransactionReport transReport) {
		this.transReport = transReport;
	}

	@SuppressWarnings("rawtypes")
	public Vector<?> getRepSummary(SessionData sessionData,ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles) throws ReportException{
		transReport.setWarningmessage(new Vector<String>());
		Vector vecRepSummary = new Vector();
		int whiteSpacesToTotals = 2;
		boolean isIntlAndHasDataPromoPermission = ReportsUtil.isIntlAndHasDataPromoPermission(sessionData, context);
		String strSQL = "";
		String groupBy = "";
		String wTadditionalData = isIntlAndHasDataPromoPermission ? " wt.additionalData, " : "";
		
		String rate_div = " 100 * (CASE p.program_id WHEN 154 THEN wt.amount ELSE CASE p.ach_type WHEN 6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt ELSE CASE ISNULL(wt.transfixedfeeamt,0) WHEN 0 THEN wt.amount ELSE wt.amount - wt.transfixedfeeamt END END END) ";
		String strTotals = "";
		if(isInternationalConfiguration(sessionData, context) ) {
			strTotals = "COUNT(wt.rec_id) AS qty, sum(isnull(wt.taxtype,0)) as taxtypecheck, SUM(wt.amount) AS total_sales, SUM(wt.merchant_rate / "+rate_div+") AS merchant_commission, SUM(wt.rep_rate / "+rate_div+") AS rep_commission, SUM(wt.agent_rate / "+rate_div+") AS agent_commission, SUM(wt.subagent_rate / "+rate_div+") AS subagent_commission, SUM(wt.iso_rate / "+rate_div+") AS iso_commission, sum(wt.amount/(1 + isnull(wt.taxpercentage / 10000, dTax - 1))) AS tax_total_sales, SUM((wt.merchant_rate / "+rate_div+") / (1 + isnull(wt.taxpercentage / 10000, dTax - 1))) AS tax_merchant_commission, SUM((wt.rep_rate / "+rate_div+") / (1 + isnull(wt.taxpercentage / 10000, dTax - 1))) AS tax_rep_commission, SUM((wt.agent_rate / "+rate_div+") / (1 + isnull(wt.taxpercentage / 10000, dTax - 1))) AS tax_agent_commission, SUM((wt.subagent_rate / "+rate_div+") / (1 + isnull(wt.taxpercentage / 10000, dTax - 1))) AS tax_subagent_commission, SUM((wt.iso_rate / "+rate_div+") / (1 + isnull(wt.taxpercentage / 10000, dTax - 1))) AS tax_iso_commission, SUM((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate AS bit)) AS adj_amount, SUM(wt.bonus_amount) AS total_bonus, (SUM(wt.bonus_amount + wt.amount)) AS total_recharge, sum(wt.amount - wt.amount/(1 + isnull(wt.taxpercentage / 10000, dTax - 1))) as tax_amount";
		} else {			
			strTotals = "count(wt.rec_id) as qty, sum(wt.amount) as total_sales, sum(wt.merchant_rate / "+rate_div+") as merchant_commission, sum(wt.rep_rate / "+rate_div+") as rep_commission, sum(wt.agent_rate / "+rate_div+") as  agent_commission, sum(wt.subagent_rate / "+rate_div+") as subagent_commission, sum(wt.iso_rate / "+rate_div+") as iso_commission, sum((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate as bit)) as adj_amount, sum(wt.bonus_amount) as total_bonus, (sum(wt.bonus_amount) + sum(wt.amount)) as total_recharge";
		}
		strSQL = "SELECT wt.businessname, wt.rep_id, "+ strTotals +"," + wTadditionalData + "r.address phys_address, r.city phys_city, r.state phys_state, r.zip phys_zip "+ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS;
		strSQL += " FROM web_transactions wt with (nolock) inner join products p with (nolock) ON p.id = wt.id ";
		strSQL += " inner join reps r with (nolock) ON wt.rep_id = r.rep_id ";
		
		String strSQLcount = "SELECT "+ strTotals +" FROM web_transactions wt with (nolock) inner join products p WITH (NOLOCK) ON p.id = wt.id inner join reps r WITH (NOLOCK) ON wt.rep_id = r.rep_id";
		String strSQLcond = " WHERE ";

		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		try{
			boolean chekForTaxType = transReport.checkfortaxtype(sessionData);
			
			if(isInternationalConfiguration(sessionData, context) )
			{
				double	dTax = TransactionReport.getTaxValue(sessionData, transReport.getEndDate());
				transReport.setWarningmessage(transReport.getWarningMessage(sessionData, transReport.getEndDate(), transReport.getStartDate()));
				strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
				strSQLcount = strSQLcount.replaceAll("dTax", Double.toString(dTax));
			}
			
			if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.MERCHANT)){
				strSQLcond += " 0 = 0 ";
			} else{
				strSQLcond += " wt.rep_id in ";
			}
			if (strAccessLevel.equals(DebisysConstants.ISO)){
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
					strSQLcond += " (SELECT rep_id FROM reps WITH(NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id = " + strRefId + ") ";
				} else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
					strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
					+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
					+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + "))) ";
				}
			} else if (strAccessLevel.equals(DebisysConstants.AGENT)){
				strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
				+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId + ")) ";
			} else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id =" + strRefId + ") ";
			}
                        
			transReport.setRepIds(transReport.getRepIds().replace("all,", "").replace("all", ""));
			if (StringUtils.isNotEmpty(transReport.getRepIds())){
				strSQLcond += " AND wt.rep_id in (" + transReport.getRepIds() + ") ";
			}

			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, transReport.getStartDate(), transReport.getEndDate() + " 23:59:59.999", false);

			groupBy = " group by wt.rep_id, wt.businessname," + wTadditionalData + "r.address, r.city, r.state, r.zip ";
			if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
			{	
				ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_TRX_SUMM_BY_REP , 1);
				scheduleReport.setNameDateTimeColumn("wt.datetime");	
				
				String fixedQuery = strSQL + strSQLcond;
				fixedQuery += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";
				fixedQuery += groupBy;
				scheduleReport.setFixedQuery( fixedQuery );
				
				String relativeQuery =  strSQL + strSQLcond;
				relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT;
				relativeQuery += groupBy;
				scheduleReport.setRelativeQuery( relativeQuery );
				
				TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, titles );				
				sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);								
				return vecRepSummary;
			} else {
				strSQL = strSQL.replaceFirst(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS, "");
			}
			strSQLcond += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";

			strSQLcount += strSQLcond;// counter no needs grouping  and order

			strSQLcond += groupBy;
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";
			try{
				if (StringUtils.isNotBlank(transReport.getCol()) && StringUtils.isNotBlank(transReport.getSort())){
					intCol = Integer.parseInt(transReport.getCol());
					intSort = Integer.parseInt(transReport.getSort());
				}
			} catch (NumberFormatException nfe){
				intCol = 0;
				intSort = 0;
			}
			if (intSort == 2){
				strSort = "DESC";
			} else{
				strSort = "ASC";
			}
			
			if(isInternationalConfiguration(sessionData, context) )
			{
				switch (intCol){
					case 1:strCol = "wt.businessname";break;
					case 2:strCol = "wt.rep_id";break;
					case 3:strCol = "qty";break;
					case 4:strCol = "total_sales";break;
					case 5:strCol = "merchant_commission";break;
					case 6:strCol = "rep_commission";break;
					case 7:strCol = "subagent_commission";break;
					case 8:strCol = "agent_commission";break;
					case 9:strCol = "iso_commission";break;
					case 10:strCol = "adj_amount";break;
					case 11:strCol = "tax_total_sales";break;
					case 12:strCol = "tax_merchant_commission";break;
					case 13:strCol = "tax_rep_commission";break;
					case 14:strCol = "tax_subagent_commission";break;
					case 15:strCol = "tax_agent_commission";break;
					case 16:strCol = "tax_iso_commission";break;
					case 17:strCol = "total_bonus";break;
					case 18:strCol = "total_recharge";break;
					case 19:strCol = "tax_amount";break;
					default:strCol = "wt.businessname";break;
				}
				strSQLcond += " order by " + strCol + " " + strSort;
				strSQL += strSQLcond;
				
				if ( scheduleReportType != DebisysConstants.DOWNLOAD_REPORT ) {
					executeTransactionRepInternational(strSQL, vecRepSummary, isIntlAndHasDataPromoPermission);
				} else {
					downloadSummary(sessionData, context, headers, titles, whiteSpacesToTotals, strSQL);
					return null;
				}					
									
				// now the counter
				if ( scheduleReportType != DebisysConstants.DOWNLOAD_REPORT ) {
					executeCountTransactionRepInternational(strSQLcount, vecRepSummary, sessionData, chekForTaxType);
				}
			} else {
				switch (intCol){
					case 1:strCol = "wt.businessname";break;
					case 2:strCol = "wt.rep_id";break;
					case 3:strCol = "qty";	  break;
					case 4:strCol = "total_sales";break;
					case 5:strCol = "merchant_commission";break;
					case 6:strCol = "rep_commission";break;
					case 7:strCol = "subagent_commission";break;
					case 8:strCol = "agent_commission";break;
					case 9:strCol = "iso_commission";break;
					case 10:strCol = "adj_amount";break;
					case 11:strCol = "r.address";break;
					case 12:strCol = "r.city";break;
					case 13:strCol = "r.state";break;
					case 14:strCol = "r.zip";break;
					case 17:strCol = "total_bonus";break;
					case 18:strCol = "total_recharge";break;
					default:strCol = "wt.businessname";break;
				}
				strSQLcond += " order by " + strCol + " " + strSort;
				strSQL += strSQLcond;

				if ( scheduleReportType != DebisysConstants.DOWNLOAD_REPORT ) {
					executeTransactionRepDomestic(strSQL, vecRepSummary, isIntlAndHasDataPromoPermission);
				}
				else
				{
					downloadSummaryDomestic(sessionData, context, headers, titles, strSQL);
					return null;
				}
				
				// now the counter
				if ( scheduleReportType == DebisysConstants.EXECUTE_REPORT )
				{
					executeCountTransactionRepDomestic(strSQLcount, vecRepSummary);
				}
			}
		} catch (Exception e){
			logger.error("Error during getRepSummary", e);
			throw new ReportException();
		} 
		return vecRepSummary;
	}

	private boolean isInternationalConfiguration(SessionData sessionData, ServletContext context) {
		return sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
				DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
					DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void executeTransactionRepInternational(String strSQL, Vector vecRepSummary, boolean isIntlAndHasDataPromoPermission) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection dbConn = null;
		try {
			logger.debug(strSQL);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null){
				logger.error("Can't get database connection");
				throw new ReportException();
			}
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()){
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_amount")));
		        if (isIntlAndHasDataPromoPermission) 
		        	vecTemp.add(StringUtil.toString(rs.getString("additionalData")));
				vecRepSummary.add(vecTemp);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void executeCountTransactionRepInternational(String strSQLcount, Vector vecRepSummary, SessionData sessionData, boolean chekForTaxType) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection dbConn = null;
		
		try {
			logger.debug(strSQLcount);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				logger.error("Can't get database connection");
				throw new ReportException();
			}
			pstmt = dbConn.prepareStatement(strSQLcount);
			rs = pstmt.executeQuery();
	
			if (rs.next()) {
				Vector vecTemp = new Vector();
				vecTemp.add("TOTALS");
				vecTemp.add(rs.getString("qty"));
	
				if(rs.getInt("taxtypecheck") > 0 && !chekForTaxType) {
					transReport.getWarningMessage().add(Languages.getString("jsp.admin.reports.taxtypewarning", sessionData.getLanguage()));
				}
	
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_amount")));
				vecRepSummary.add(vecTemp);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void executeTransactionRepDomestic(String strSQL, Vector vecRepSummary, boolean isIntlAndHasDataPromoPermission) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection dbConn = null;
		
		try {
			logger.debug(strSQL);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				logger.error("Can't get database connection");
				throw new ReportException();
			}
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
		
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
				vecTemp.add(StringUtil.toString(rs.getString("phys_address")));
				vecTemp.add(StringUtil.toString(rs.getString("phys_city")));
				vecTemp.add(StringUtil.toString(rs.getString("phys_state")));
				vecTemp.add(StringUtil.toString(rs.getString("phys_zip")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				if (isIntlAndHasDataPromoPermission) 
					vecTemp.add(StringUtil.toString(rs.getString("additionalData")));
				vecRepSummary.add(vecTemp);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void executeCountTransactionRepDomestic(String strSQLcount, Vector vecRepSummary) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection dbConn = null;
		
		try {
			logger.debug(strSQLcount);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				logger.error("Can't get database connection");
				throw new ReportException();
			}
			logger.debug(strSQLcount);
			pstmt = dbConn.prepareStatement(strSQLcount);
			rs = pstmt.executeQuery();
		
			if (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add("TOTALS");
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				vecRepSummary.add(vecTemp);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
		}
	}
	
	private void downloadSummary(SessionData sessionData, ServletContext context, ArrayList<ColumnReport> headers,
			ArrayList<String> titles, int whiteSpacesToTotals, String strSQL) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection dbConn = null;
		DownloadsSummary trxSummary;
		String strFileName;
		logger.info("Creating CSV");
		try {
			logger.debug(strSQL);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null){
				logger.error("Can't get database connection");
				throw new ReportException();
			}
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			strFileName = transReport.generateFileName( context );
			trxSummary = new DownloadsSummary();
			String totalLabel = Languages.getString("jsp.admin.reports.totals", sessionData.getLanguage() );
			transReport.setStrUrlLocation(trxSummary.download(  rs , sessionData, context, headers, strFileName, titles, totalLabel, whiteSpacesToTotals));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
		}
	}
	
	private void downloadSummaryDomestic(SessionData sessionData, ServletContext context,
			ArrayList<ColumnReport> headers, ArrayList<String> titles, String strSQL) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection dbConn = null;
		DownloadsSummary trxSummary;
		int whiteSpacesToTotals;
		String strFileName;
		whiteSpacesToTotals = 6;
		logger.info("Creating CSV");
		try {
			logger.debug(strSQL);
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null){
				logger.error("Can't get database connection");
				throw new ReportException();
			}
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			strFileName = transReport.generateFileName( context );
			trxSummary = new DownloadsSummary();
			String totalLabel = Languages.getString("jsp.admin.reports.totals", sessionData.getLanguage() );
			transReport.setStrUrlLocation(trxSummary.download( rs, sessionData, context, headers, strFileName, titles, totalLabel, whiteSpacesToTotals));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Vector getRepSummaryMx(SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers) throws ReportException{
		Connection dbConn = null;
		Vector vecRepSummary = new Vector();
		//String strSQL = sql_bundle.getString("getRepSummaryMx");
		//String rate_div = " 100 * (CASE ISNULL(transfixedfeeamt,0) WHEN 0 THEN amount ELSE transfixedfeeamt END) ";
		String rate_div = " 100 * (CASE p.program_id WHEN 154 THEN wt.amount ELSE CASE p.ach_type WHEN 6 THEN wt.Fixed_trans_fee + wt.transFixedFeeAmt  ELSE CASE ISNULL(wt.transfixedfeeamt,0) WHEN 0 THEN wt.amount ELSE wt.transfixedfeeamt END END END) ";
		String strTotals = "COUNT(wt.rec_id) AS qty, SUM(wt.amount) AS total_sales, SUM(wt.merchant_rate / "+rate_div+") AS merchant_commission, SUM(wt.rep_rate / "+rate_div+") AS rep_commission, SUM(wt.agent_rate / "+rate_div+") AS agent_commission, SUM(wt.subagent_rate / "+rate_div+") AS subagent_commission, SUM(wt.iso_rate / "+rate_div+") AS iso_commission, SUM(wt.amount) / dTax AS tax_total_sales, SUM(wt.merchant_rate / "+rate_div+") / dTax AS tax_merchant_commission, SUM(wt.rep_rate / "+rate_div+") / dTax AS tax_rep_commission, SUM(wt.agent_rate / "+rate_div+") / dTax AS tax_agent_commission, SUM(wt.subagent_rate / "+rate_div+") / dTax AS tax_subagent_commission, SUM(wt.iso_rate / "+rate_div+") / dTax AS tax_iso_commission, SUM((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate AS bit)) AS adj_amount, SUM(wt.bonus_amount) AS total_bonus, (SUM(wt.bonus_amount + wt.amount)) AS total_recharge";
		String strSQL = "SELECT wt.businessname, wt.rep_id, "+ strTotals +"  FROM web_transactions wt (nolock), products p with (nolock) ";
		String strSQLcount = "SELECT "+ strTotals +" FROM web_transactions wt with (nolock) inner join products p with (nolock) ON p.id = wt.id";
		String strSQLcond = " WHERE p.id = wt.id AND ";

		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		try{
			double dTax = 1 + (DebisysConfigListener.getMxValueAddedTax(context) / 100);
			strSQL = strSQL.replaceAll("dTax", Double.toString(dTax));
			strSQLcount = strSQLcount.replaceAll("dTax", Double.toString(dTax));
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null){
				logger.error("Can't get database connection");
				throw new ReportException();
			}
			if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.MERCHANT)){
				strSQLcond += " 0 =  0 ";
			}else{
				strSQLcond += " wt.rep_id in ";
			}
			if (strAccessLevel.equals(DebisysConstants.ISO)){
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
					strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id = " + strRefId + ") ";
				}else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
					strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
					+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
					+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + "))) ";
				}
			}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
				strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
				+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId + ")) ";
			}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
				strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP + " AND iso_id =" + strRefId + ") ";
			}
			if (StringUtils.isNotEmpty(transReport.getRepIds()) && !transReport.getRepIds().contains("all")){
				strSQLcond += " AND wt.rep_id in (" + transReport.getRepIds() + ") ";
			}
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, transReport.getStartDate(), transReport.getEndDate() + " 23:59:59.999", false);
			strSQLcond += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";

			strSQL += strSQLcond;
			strSQLcount += strSQLcond;

			strSQL += " group by wt.rep_id, wt.businessname ";
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";
			try{
				if (StringUtils.isNotEmpty(transReport.getCol()) && StringUtils.isNotBlank(transReport.getSort())){
					intCol = Integer.parseInt(transReport.getCol());
					intSort = Integer.parseInt(transReport.getSort());
				}
			}catch (NumberFormatException nfe){
				intCol = 0;
				intSort = 0;
			}
			if (intSort == 2){
				strSort = "DESC";
			}else{
				strSort = "ASC";
			}
			switch (intCol){
				case 1:strCol = "wt.businessname";break;
				case 2:strCol = "wt.rep_id";break;
				case 3:strCol = "qty";break;
				case 4:strCol = "total_sales";break;
				case 5:strCol = "merchant_commission";break;
				case 6:strCol = "rep_commission";break;
				case 7:strCol = "subagent_commission";break;
				case 8:strCol = "agent_commission";break;
				case 9:strCol = "iso_commission";break;
				case 10:strCol = "adj_amount";break;
				case 11:strCol = "tax_total_sales";break;
				case 12:strCol = "tax_merchant_commission";break;
				case 13:strCol = "tax_rep_commission";break;
				case 14:strCol = "tax_subagent_commission";break;
				case 15:strCol = "tax_agent_commission";break;
				case 16:strCol = "tax_iso_commission";break;
				case 17:strCol = "total_bonus";break;
				case 18:strCol = "total_recharge";break;
				default:strCol = "wt.businessname";break;
			}
			strSQL = strSQL + " order by " + strCol + " " + strSort;
			logger.debug(strSQL);
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()){
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				vecRepSummary.add(vecTemp);
			}
			rs.close();
			pstmt.close();
			pstmt = null;
			rs = null;
			if ( vecRepSummary.size() > 0 )
			{
				// now the counter
				logger.debug(strSQLcount);
				pstmt = dbConn.prepareStatement(strSQLcount);
				rs = pstmt.executeQuery();
				if (rs.next()){
					Vector vecTemp = new Vector();
					vecTemp.add("TOTALS");
					vecTemp.add(rs.getString("qty"));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_total_sales")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_merchant_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_rep_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_subagent_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_agent_commission")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("tax_iso_commission")));
					vecRepSummary.add(vecTemp);
				}
				pstmt.close();
				rs.close();
				pstmt = null;
				rs = null;
			}
		}catch (Exception e){
			logger.error("Error during getRepSummaryMx", e);
			throw new ReportException();
		}finally{
			try{
				Torque.closeConnection(dbConn);
			}catch (Exception e){
				logger.error("Error during closeConnection", e);
			}
		}
		return vecRepSummary;
	}
}
