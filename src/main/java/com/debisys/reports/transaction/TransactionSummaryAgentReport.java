package com.debisys.reports.transaction;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.TransactionReport;
import com.debisys.reports.summarys.DownloadsSummary;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.*;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import javax.servlet.ServletContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;

public class TransactionSummaryAgentReport {

	private static final Logger logger = Logger.getLogger(TransactionSummaryAgentReport.class);

	private ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	private TransactionReport transReport;

	public TransactionSummaryAgentReport(TransactionReport transReport) {
		this.transReport = transReport;
	}
	
	public Vector getAgentSummary(SessionData sessionData,ServletContext application,int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles ) throws ReportException {
		Connection dbConn = null;
		Vector vecAgentSummary = new Vector();

		String strSQL = "";

		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");

		String rate_div = rateStatement(application);

		StringBuilder strBuffSQL = new StringBuilder();
		transReport.setWarningmessage(new Vector<String>());
		if( sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) &&
				DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
				DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
		{
			double	dTax = TransactionReport.getTaxValue(sessionData, transReport.getEndDate());

			transReport.setWarningmessage(transReport.getWarningMessage(sessionData, transReport.getEndDate(), transReport.getStartDate()));

			strBuffSQL.append("SELECT r3.businessname, r3.rep_id, ");
			strBuffSQL.append("COUNT(wt.rec_id) as qty, ");
			strBuffSQL.append("SUM(ISNULL(wt.taxtype,0)) as taxtypecheck, ");
			strBuffSQL.append("SUM(wt.amount) as total_sales,  ");
			strBuffSQL.append("SUM((wt.merchant_rate / 100) * rate_div ) as merchant_commission, ");
			strBuffSQL.append("SUM((wt.rep_rate / 100) * rate_div ) as rep_commission,  ");
			strBuffSQL.append("SUM((wt.agent_rate / 100) * rate_div) as agent_commission, ");
			strBuffSQL.append("SUM((wt.subagent_rate / 100) * rate_div ) as subagent_commission, ");
			strBuffSQL.append("SUM((wt.iso_rate / 100) * rate_div ) as iso_commission,  ");
			strBuffSQL.append("SUM(wt.amount/(1 + ISNULL(wt.taxpercentage / 10000, dTax - 1))) AS tax_total_sales, ");
			strBuffSQL.append("SUM((wt.merchant_rate / 100 * rate_div) / (1 + ISNULL(wt.taxpercentage / 10000, dTax - 1))) AS tax_merchant_commission, ");
			strBuffSQL.append("SUM((wt.rep_rate / 100 * rate_div) / (1 + ISNULL(wt.taxpercentage / 10000, dTax - 1))) AS tax_rep_commission,  ");
			strBuffSQL.append("SUM((wt.agent_rate / 100 * rate_div) / (1 + ISNULL(wt.taxpercentage / 10000, dTax - 1))) AS tax_agent_commission, ");
			strBuffSQL.append("SUM((wt.subagent_rate / 100 * rate_div) / (1 + ISNULL(wt.taxpercentage / 10000, dTax - 1))) AS tax_subagent_commission, ");
			strBuffSQL.append("SUM((wt.iso_rate / 100 * rate_div) / (1 + ISNULL(wt.taxpercentage / 10000, dTax - 1))) AS tax_iso_commission, ");
			strBuffSQL.append("SUM((wt.amount * (100-wt.iso_disCOUNT_rate)/100)*cast(wt.iso_disCOUNT_rate AS bit)) AS adj_amount, ");
			strBuffSQL.append("SUM(wt.bonus_amount) AS total_bonus, (SUM(wt.bonus_amount + wt.amount)) AS total_recharge, ");
			strBuffSQL.append("SUM(wt.amount - wt.amount/(1 + ISNULL(wt.taxpercentage / 10000, dTax - 1))) as tax_amount, ");
			strBuffSQL.append("r3.address phys_address, r3.city phys_city, r3.state phys_state, r3.zip phys_zip ");

			if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
			{
				strBuffSQL.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
			}

			strBuffSQL.append("FROM web_transactions WT WITH (NOLOCK) INNER JOIN products p with (nolock) on p.id = wt.id ");
			strBuffSQL.append("INNER JOIN reps r1 WITH (NOLOCK) ON wt.rep_id=r1.rep_id ");
			strBuffSQL.append("INNER JOIN reps r2 WITH (NOLOCK) ON r1.iso_id=r2.rep_id ");
			strBuffSQL.append("INNER JOIN reps r3 WITH (NOLOCK) ON r2.iso_id=r3.rep_id WHERE ");

			strSQL = strBuffSQL.toString().replaceAll( "dTax", Double.toString(dTax) ).replaceAll( "rate_div", rate_div );

		}
		else
		{
			strBuffSQL.append("SELECT r3.businessname, r3.rep_id, ");
			strBuffSQL.append("COUNT(wt.rec_id) as qty, ");
			strBuffSQL.append("SUM(wt.amount) as total_sales, ");
			strBuffSQL.append("SUM((wt.merchant_rate /  100) * rate_div) as merchant_commission, ");
			strBuffSQL.append("SUM((wt.rep_rate /  100) * rate_div) as rep_commission, ");
			strBuffSQL.append("SUM((wt.agent_rate /  100) * rate_div) as agent_commission, ");
			strBuffSQL.append("SUM((wt.subagent_rate /  100) * rate_div) as subagent_commission, ");
			strBuffSQL.append("SUM((wt.iso_rate /  100) * rate_div) as iso_commission, ");
			strBuffSQL.append("SUM((wt.amount * (100 - wt.iso_disCOUNT_rate) / 100) * cast(wt.iso_disCOUNT_rate as bit)) as adj_amount, SUM(wt.bonus_amount) as total_bonus, ");
			strBuffSQL.append("(SUM(wt.bonus_amount) + SUM(wt.amount)) as total_recharge, ");
			strBuffSQL.append("r3.address phys_address, r3.city phys_city, r3.state phys_state, r3.zip phys_zip ");

			if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
			{
				strBuffSQL.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
			}

			strBuffSQL.append("FROM web_transactions wt WITH (NOLOCK) ");
			strBuffSQL.append("INNER JOIN products p WITH (NOLOCK) ON p.id = wt.id ");
			strBuffSQL.append("INNER JOIN reps r1 WITH (NOLOCK) ON wt.rep_id=r1.rep_id ");
			strBuffSQL.append("INNER JOIN reps r2 WITH (NOLOCK) ON r1.iso_id=r2.rep_id ");
			strBuffSQL.append("INNER JOIN reps r3 WITH (NOLOCK) ON r2.iso_id=r3.rep_id WHERE ");

			strSQL = strBuffSQL.toString().replaceAll("rate_div", rate_div);
		}
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));

			if (dbConn == null)
			{
				logger.error("Can't get database connection");
				throw new ReportException();
			}

			strSQL = strSQL + " wt.rep_id in ";

			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL = strSQL + " (SELECT r4.rep_id FROM reps AS r4 WITH (NOLOCK) WHERE r4.type = " + DebisysConstants.REP_TYPE_REP + " AND r4.iso_id IN "
							+ "(SELECT r5.rep_id FROM reps AS r5 WITH (NOLOCK) WHERE r5.type = " + DebisysConstants.REP_TYPE_SUBAGENT + " AND r5.iso_id IN "
							+ "(SELECT r6.rep_id FROM reps AS r6 WITH (NOLOCK) WHERE r6.type = " + DebisysConstants.REP_TYPE_AGENT + " AND r6.iso_id = " + strRefId + "))) ";
				}

			}

			// ATE 9/13/07 Doing the join explicitly in the query now
			// not anymore bc of performance issues
			//strSQL = strSQL + " AND wt.rep_id=r1.rep_id AND r1.iso_id=r2.rep_id AND r2.iso_id=r3.rep_id ";
			strSQL = strSQL + " ";

			if (transReport.getRep_ids() != null && !transReport.getRep_ids().equals(""))
			{
				strSQL += " AND r3.rep_id IN (" + transReport.getRep_ids() + ") ";
			}


			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";

			try
			{
				if (transReport.getCol() != null && transReport.getSort() != null && !transReport.getCol().equals("") && !transReport.getSort().equals(""))
				{
					intCol = Integer.parseInt(transReport.getCol());
					intSort = Integer.parseInt(transReport.getSort());
				}
			}
			catch (NumberFormatException nfe)
			{
				intCol = 0;
				intSort = 0;
			}

			if (intSort == 2)
			{
				strSort = "DESC";
			}
			else
			{
				strSort = "ASC";
			}

			switch (intCol)
			{
				case 1:
					strCol = "r3.businessname";
					break;
				case 2:
					strCol = "r3.address";
					break;
				case 3:
					strCol = "r3.city";
					break;
				case 4:
					//strCol = "total_sales";
					strCol = "r3.state";
					break;
				case 5:
					strCol = "r3.zip";
					break;
				case 6:
					strCol = "r3.rep_id";
					break;
				case 7:
					strCol = "qty";
					break;
				case 8:
					strCol = "total_recharge";
					break;
				case 9:
					strCol = "iso_commission";
					break;
				case 10:
					strCol = "agent_commission";
					break;
				case 11:
					strCol = "subagent_commission";
					break;
				case 12:
					strCol = "rep_commission";
					break;
				case 13:
					strCol = "merchant_commission";
					break;
				case 14:
					strCol = "adj_amount";
					break;
				case 15:
					strCol = "total_bonus";
					break;
				case 16:

					break;

				case 17:
					strCol = "tax_total_sales";
					break;

				case 18:
					strCol = "tax_amount";
					break;

				case 19:
					strCol = "tax_merchant_commission";
					break;
				case 20:
					strCol = "tax_rep_commission";
					break;
				case 21:
					strCol = "tax_subagent_commission";
					break;
				case 22:
					strCol = "tax_agent_commission";
					break;
				case 23:
					strCol = "tax_iso_commission";
					break;

				default:
					strCol = "r3.businessname";
					break;
			}

			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, transReport.getStartDate(), transReport.getEndDate() + " 23:59:59.999", false);

			if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
			{
				String fixedQuery = strSQL;
				String relativeQuery =  strSQL;

				relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT;

				relativeQuery += " GROUP BY r3.rep_id, r3.businessname, r3.address, r3.city, r3.state, r3.zip ";
				relativeQuery += " ORDER BY " + strCol + " " + strSort;

				fixedQuery += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";
				fixedQuery += " GROUP BY r3.rep_id, r3.businessname, r3.address, r3.city, r3.state, r3.zip ";
				fixedQuery += " ORDER BY " + strCol + " " + strSort;

				ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_TRX_SUMM_BY_AGENT , 1);
				scheduleReport.setNameDateTimeColumn("wt.datetime");

				scheduleReport.setRelativeQuery( relativeQuery );

				scheduleReport.setFixedQuery( fixedQuery );
				TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, titles );
				sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);
				return null;
			}
			else
			{
				strSQL = strSQL.replaceAll( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS, "" );

				strSQL += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";
				strSQL += " GROUP BY r3.rep_id, r3.businessname, r3.address, r3.city, r3.state, r3.zip ";

				strSQL = strSQL + " ORDER BY " + strCol + " " + strSort;
				logger.debug(strSQL);

			}

			boolean chekForTaxType = transReport.checkfortaxtype(sessionData);
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();

			if ( scheduleReportType != DebisysConstants.DOWNLOAD_REPORT )
			{
				while (rs.next())
				{
					if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
							&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)
							&& sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
					{
						if( rs.getInt("taxtypecheck")>0 && !chekForTaxType )
						{
							transReport.getWarningMessage().add(Languages.getString("jsp.admin.reports.taxtypewarning", sessionData.getLanguage()));
						}
					}
					Vector vecTemp = new Vector();
					vecTemp.add(StringUtil.toString(rs.getString("businessname")));
					vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
					vecTemp.add(rs.getString("qty"));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_sales")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_commission")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("adj_amount")));
					vecTemp.add(StringUtil.toString(rs.getString("phys_address")));
					vecTemp.add(StringUtil.toString(rs.getString("phys_city")));
					vecTemp.add(StringUtil.toString(rs.getString("phys_state")));
					vecTemp.add(StringUtil.toString(rs.getString("phys_zip")));

					if( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) &&
							DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT) )
					{
						vecTemp.add(NumberUtil.formatAmount(rs.getString("total_bonus")));
						vecTemp.add(NumberUtil.formatAmount(rs.getString("total_recharge")));
						if(sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS))
						{
							vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_total_sales")));
							vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_amount")));
							vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_merchant_commission")));
							vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_rep_commission")));
							vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_agent_commission")));
							vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_subagent_commission")));
							vecTemp.add(NumberUtil.formatAmount(rs.getString("tax_iso_commission")));
						}
					}
					vecAgentSummary.add(vecTemp);
				}
			}
			else
			{
				DownloadsSummary trxSummary = new DownloadsSummary();
				String strFileName = transReport.generateFileName(application);
				String totalLabel = Languages.getString("jsp.admin.reports.totals", sessionData.getLanguage() );
				transReport.setStrUrlLocation(trxSummary.download(  rs, sessionData, application, headers, strFileName, titles, totalLabel, 2  ));
			}
			rs.close();
			pstmt.close();

		}
		catch (Exception e)
		{
			logger.error("Error during getAgentSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				logger.error("Error during closeConnection", e);
			}
		}
		return vecAgentSummary;
	}


	private String rateStatement(ServletContext application){
		boolean domestic = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
		if(domestic){
			return " (CASE p.ach_type WHEN 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE CASE ISNULL(wt.transFixedFeeAmt, 0) WHEN 0 THEN wt.amount ELSE (wt.amount - wt.transFixedFeeAmt) END END) ";
		}else{
			return " (CASE p.ach_type WHEN 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END)";
		}
	}

}
