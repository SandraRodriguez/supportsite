package com.debisys.reports.transaction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.TransactionReport;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

/**
 *
 * @author cmercado
 *
 */
public class TransactionProductMerchantReport {

    private static final Logger logger = Logger.getLogger(TransactionProductMerchantReport.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

    private TransactionReport transReport;
    private SimpleDateFormat dbformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public TransactionProductMerchantReport(TransactionReport transReport) {
        this.transReport = transReport;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public Vector getMerchantProductSummary(int intPageNumber, int intRecordsPerPage, SessionData sessionData, ServletContext context,
            boolean hasMerchantList, boolean download,
            int scheduleReportType,
            ArrayList<ColumnReport> headers, ArrayList<String> titles) throws ReportException {
        logger.debug("==>intPageNumber" + intPageNumber);
        logger.debug("==>intRecordsPerPage" + intRecordsPerPage);

        transReport.setWarningMessage(new Vector<String>());

        int agentname = 2;
        int subagentname = 3;
        int repname = 4;
        int merchantname = 5;
        int merchantid = 6;
        int city = 7;
        int country = 8;
        int rechargevalue = 9;
        int bonus = 10;
        int totalrecharge = 11;
        int productid = 13;
        int state = 14;
        int lasttrx = 15;
        int isorate = 16;
        int agentrate = 17;
        int subagentrate = 18;
        int reprate = 19;
        int merchantrate = 20;
        int productname = 21;
        int trxcount = 22;
        int netamount = 23;
        int taxamount = 24;
        // int ASC = 1;
        int DESC = 2;
        int intCol = 0;

        int intSort = 0;
        String strCol = "";
        String strSort = "";

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            if (StringUtils.isNotEmpty(transReport.getCol()) && StringUtils.isNotEmpty(transReport.getSort())) {
                intCol = Integer.parseInt(transReport.getCol());
                intSort = Integer.parseInt(transReport.getSort());
            }
        } catch (NumberFormatException nfe) {
            intCol = 0;
            intSort = 0;
        }

        if (intSort == DESC) {
            strSort = "DESC";
        } else {
            strSort = "ASC";
        }

        if (intCol == agentname) {
            strCol = "Rep";
        } else if (intCol == subagentname) {
            strCol = "SubAgent";
        } else if (intCol == repname) {
            strCol = "Agent";
        } else if (intCol == merchantname) {
            strCol = "MerchantName";
        } else if (intCol == merchantid) {
            strCol = "m.merchant_id";
        } else if (intCol == city) {
            strCol = "m.phys_city";
        } else if (intCol == country) {
            strCol = "phys_country";
        } else if (intCol == rechargevalue) {
            strCol = "RechargeValue";
        } else if (intCol == bonus) {
            strCol = "bonus";
        } else if (intCol == totalrecharge) {
            strCol = "TotalRecharge";
        } else if (intCol == productid) {
            strCol = "wt.id";
        } else if (intCol == lasttrx) {
            strCol = "lasttransaction";
        } else if (intCol == isorate) {
            strCol = "iso_rate";
        } else if (intCol == agentrate) {
            strCol = "agent_rate";
        } else if (intCol == subagentrate) {
            strCol = "subagent_rate";
        } else if (intCol == reprate) {
            strCol = "rep_rate";
        } else if (intCol == merchantrate) {
            strCol = "merchant_rate";
        } else if (intCol == productname) {
            strCol = "wt.description";
        } else if (intCol == state) {
            strCol = "m.phys_state";
        } else if (intCol == trxcount) {
            strCol = "count";
        } else if (intCol == netamount) {
            strCol = "netamount";
        } else if (intCol == taxamount) {
            strCol = "taxamount";
        } else {
            strCol = "MerchantName";
        }
        String strSQLWhere = "";
        String strSQLDatesTimeRanges = "";
        String group = "";
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strRefId = sessionData.getProperty("ref_id");
        Connection dbConn = null;
        Vector vecProductSummary = new Vector();
        double dTax = 1;
        String strSQL = "";
        boolean addColumnsDTU1281 = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_ADDITIONAL_COLUMNS_TRX_SUMM_PRODUCT_MERCHANT_REPORT);
        boolean taxCalculation = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS);
        boolean deployInter = DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
        boolean deployDefault = DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);

        if (taxCalculation && deployInter && deployDefault) {
            dTax = TransactionReport.getTaxValue(sessionData, transReport.getEndDate());
            transReport.setWarningMessage(transReport.getWarningMessage(sessionData, transReport.getEndDate(), transReport.getStartDate()));
            strSQL = sql_bundle.getString("getSummarybyTaxProductbyMerchant");
            if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                strSQL = strSQL.replaceAll("dTax", "@tax");
            } else {
                strSQL = strSQL.replaceAll("dTax", String.valueOf(dTax));
            }

        } else {
            strSQL = sql_bundle.getString("getSummarybyProductbyMerchant");
        }

        if (transReport.getSelectionType() == 0) {
            return null;
        } else {
            try {
                dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
                if (dbConn == null) {
                    logger.error("Can't get database connection");
                    throw new ReportException();
                }

                if (transReport.getSelectionType() == 8 || hasMerchantList) {
                    if (StringUtils.isNotEmpty(transReport.getSelectedIds()) && !transReport.getSelectedIds().contains("all")) {
                        strSQLWhere = " where m.merchant_id in (" + transReport.getSelectedIds() + ") ";
                    } else {
                        return null;
                    }
                } else if (transReport.getSelectionType() == 12) {
                    if (strRefId != null && !strRefId.equals("") && !strRefId.contains("all")) {
                        strSQLWhere = " where m.merchant_id in (" + strRefId + ") ";
                    } else {
                        return null;
                    }
                } else {
                    String merchantlist = Merchant.getMerchantList(sessionData, transReport.getSelectionType(), transReport.getSelectedIds());
                    if (merchantlist != null && !merchantlist.equals("") && !merchantlist.contains("all")) {
                        strSQLWhere = "where m.merchant_id in (" + merchantlist + ")";
                    } else {
                        return null;
                    }
                }

                if (StringUtils.isNotEmpty(transReport.getProductIds()) && !transReport.getProductIds().contains("all")) {
                    strSQLWhere += " and wt.id in (" + transReport.getProductIds() + ") ";
                }
                Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, transReport.getStartDate(), transReport.getEndDate() + " 23:59:59.999", false);

                strSQLDatesTimeRanges = " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0) + "' AND  wt.datetime < '" + vTimeZoneFilterDates.get(1) + "') ";

                group = " group by wt.id,r.businessname,r2.businessname,r3.businessname, m.legal_businessname,m.phys_city,m.merchant_id,m.phys_country,wt.description,m.phys_state ";

                StringBuilder additionalSelect = new StringBuilder();
                String tokenToChange = "ADDITIONALCOLUMNSINSELECT";

                if (addColumnsDTU1281) {
                    if (scheduleReportType != DebisysConstants.SCHEDULE_REPORT) {
                        additionalSelect.append(
                                ",(select count(*) from web_transactions wtt with (NOLOCK) where wtt.datetime >= '"
                                + vTimeZoneFilterDates.get(0) + "' AND  wtt.datetime < '"
                                + vTimeZoneFilterDates.get(1)
                                + "' and wtt.merchant_id = m.merchant_id and wtt.amount>0 and wtt.id=wt.id ) sales");
                        additionalSelect.append(
                                ",(select count(*) from web_transactions wtt with (NOLOCK) where wtt.datetime >= '"
                                + vTimeZoneFilterDates.get(0) + "' AND  wtt.datetime < '"
                                + vTimeZoneFilterDates.get(1)
                                + "' and wtt.merchant_id = m.merchant_id and wtt.amount<=0 and wtt.id=wt.id ) returns ");
                        strSQL = strSQL.replaceFirst(tokenToChange, additionalSelect.toString());
                    }
                } else {
                    strSQL = strSQL.replaceFirst(tokenToChange, "");
                }

                if (scheduleReportType == DebisysConstants.SCHEDULE_REPORT) {
                    if (addColumnsDTU1281) {
                        String sales = " select count(*) from web_transactions wtt with (NOLOCK) where wtt.datetime >= @startDate AND  wtt.datetime < @endDate and wtt.merchant_id = m.merchant_id and wtt.amount>0 and wtt.id=wt.id ";
                        String ret = " select count(*) from web_transactions wtt with (NOLOCK) where wtt.datetime >= @startDate AND  wtt.datetime < @endDate and wtt.merchant_id = m.merchant_id and wtt.amount<=0 and wtt.id=wt.id ";
                        additionalSelect.append(",( " + sales + " ) sales");
                        additionalSelect.append(",( " + ret + ") ret ");
                        additionalSelect.append(",( (" + sales + ") - (" + ret + ") ) netTrx ");
                    }
                    additionalSelect.append(ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS);
                    strSQL = strSQL.replaceFirst(tokenToChange, additionalSelect.toString());

                    strSQLDatesTimeRanges = " AND (wt.datetime >= @startDate and  wt.datetime <= @endDate ) ";

                    StringBuilder strSQLSummaryScriptJasper = new StringBuilder();
                    strSQLSummaryScriptJasper.append("DECLARE @startDate DATETIME ");
                    strSQLSummaryScriptJasper.append("DECLARE @endDate DATETIME ");
                    strSQLSummaryScriptJasper.append("DECLARE @entity NUMERIC(15, 0) ");
                    strSQLSummaryScriptJasper.append("DECLARE @tax DECIMAL(10, 2) ");
                    strSQLSummaryScriptJasper.append("DECLARE @accessLevel INT ");
                    strSQLSummaryScriptJasper.append("DECLARE @distChainType INT ");
                    strSQLSummaryScriptJasper.append("SELECT @startDate=?, @endDate=?, @entity=?, @accessLevel=?, @distChainType=? ");
                    strSQLSummaryScriptJasper.append("EXEC GetTaxByEntityAndDates @accessLevel, @distChainType, @entity, @endDate, @tax OUTPUT ");

                    String headerFixedToJasperSQL = strSQLSummaryScriptJasper.toString()
                            .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(0).toString() + "'")
                            .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(1).toString() + "'")
                            .replaceFirst("\\?", strRefId)
                            .replaceFirst("\\?", strAccessLevel)
                            .replaceFirst("\\?", strDistChainType);

                    String fixedQuery = headerFixedToJasperSQL + " " + strSQL + strSQLWhere + strSQLDatesTimeRanges + group;

                    String headerToJasperSQL = strSQLSummaryScriptJasper.toString()
                            .replaceFirst("\\?", ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE)
                            .replaceFirst("\\?", ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE)
                            .replaceFirst("\\?", strRefId)
                            .replaceFirst("\\?", strAccessLevel)
                            .replaceFirst("\\?", strDistChainType);

                    String relativeQuery = headerToJasperSQL + " " + strSQL + " " + strSQLWhere + strSQLDatesTimeRanges + group;

                    ScheduleReport scheduleReport = new ScheduleReport(DebisysConstants.SC_TRX_SUMM_BY_PRODUCT_MERCHANT, 1);
                    scheduleReport.setNameDateTimeColumn("wt.datetime");

                    logger.debug(fixedQuery);

                    scheduleReport.setRelativeQuery(relativeQuery);
                    scheduleReport.setFixedQuery(fixedQuery);
                    String headerUserInfo = transReport.getHeaderUserInfo(sessionData, context);
                    String filteredBy = transReport.getfilteredby(sessionData);
                    filteredBy = filteredBy.replaceAll("\n<br/>", " ");
                    titles.add(headerUserInfo);
                    titles.add(filteredBy);

                    TransactionReportScheduleHelper.addMetaDataReport(headers, scheduleReport, titles);
                    sessionData.setPropertyObj(DebisysConstants.SC_SESS_VAR_NAME, scheduleReport);
                    return null;

                }

                if (intSort == 0 && intCol == 0) {
                    strSQL = strSQL + strSQLWhere + strSQLDatesTimeRanges + group;
                } else {
                    strSQL = strSQL + strSQLWhere + strSQLDatesTimeRanges + group + "  order by " + strCol + " " + strSort;
                }

                logger.debug(strSQL);
                pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);

                long startQuery = System.currentTimeMillis();
                rs = pstmt.executeQuery();
                long endQuery = System.currentTimeMillis();

                int intRecordCount = DbUtil.getRecordCount(rs);
                vecProductSummary.add(intRecordCount);
                logger.debug("intRecordCount" + intRecordCount);
                logger.debug("intRecordsPerPage" + intRecordsPerPage);
                logger.debug("download = " + download);
                if (intRecordCount > 0) {
                    if (download) {
                        //generateExportFile(context, rs, sessionData);
                        String downloadUrl = generateExportFile(context, rs, sessionData);
                        vecProductSummary.add(downloadUrl);
                        endQuery = System.currentTimeMillis();
                        logger.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
                    } else {
                        rs.absolute(DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber));
                        logger.debug("intRecordsPerPage" + intRecordsPerPage);
                        int taxtypecounter = 0;
                        for (int i = 0; i < intRecordsPerPage; i++) {
                            taxtypecounter += rs.getInt("taxtypecheck");

                            Vector vecTemp = new Vector();
                            vecTemp.add(StringUtil.toString(rs.getString("Agent")));
                            vecTemp.add(StringUtil.toString(rs.getString("SubAgent")));
                            vecTemp.add(StringUtil.toString(rs.getString("Rep")));
                            vecTemp.add(StringUtil.toString(rs.getString("MerchantName")));
                            vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
                            vecTemp.add(StringUtil.toString(rs.getString("phys_city")));

                            vecTemp.add(StringUtil.toString(rs.getString("phys_country")));
                            vecTemp.add(NumberUtil.formatCurrency(String.valueOf(rs.getDouble("RechargeValue"))));
                            vecTemp.add(NumberUtil.formatCurrency(String.valueOf(rs.getDouble("bonus"))));
                            vecTemp.add(NumberUtil.formatCurrency(String.valueOf(rs.getDouble("TotalRecharge"))));
                            vecTemp.add(StringUtil.toString(rs.getString("id")));
                            vecTemp.add(StringUtil.toString(rs.getString("description")));
                            Vector TimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, rs.getString("lasttransaction"), rs.getString("lasttransaction"), true);
                            vecTemp.add(StringUtil.toString(DateUtil.formatDateTime(this.dbformatter.parse(TimeZoneFilterDates.get(0).toString()))));
                            vecTemp.add(NumberUtil.formatAmount(String.valueOf(rs.getFloat("iso_rate"))));
                            vecTemp.add(NumberUtil.formatAmount(String.valueOf(rs.getFloat("agent_rate"))));
                            vecTemp.add(NumberUtil.formatAmount(String.valueOf(rs.getFloat("subagent_rate"))));
                            vecTemp.add(NumberUtil.formatAmount(String.valueOf(rs.getFloat("rep_rate"))));
                            vecTemp.add(NumberUtil.formatAmount(String.valueOf(rs.getFloat("merchant_rate"))));
                            vecTemp.add(String.valueOf(rs.getInt("count")));
                            vecTemp.add(StringUtil.toString(rs.getString("phys_state")));
                            if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
                                    && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                    && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                                vecTemp.add(NumberUtil.formatCurrency(String.valueOf(rs.getDouble("netamount"))));
                                vecTemp.add(NumberUtil.formatCurrency(String.valueOf(rs.getDouble("taxamount"))));
                            } else {
                                vecTemp.add("");
                                vecTemp.add("");
                            }
                            if (addColumnsDTU1281) {
                                vecTemp.add((String.valueOf(rs.getInt("sales"))));
                                vecTemp.add((String.valueOf(rs.getInt("returns"))));
                                vecTemp.add((String.valueOf(rs.getInt("sales") - rs.getInt("returns"))));
                            }
                            vecProductSummary.add(vecTemp);
                            if (!rs.next()) {
                                break;
                            }
                        }
                        if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)
                                && DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) {
                            if (taxtypecounter > 0 && !transReport.checkfortaxtype(sessionData)) {
                                transReport.getWarningMessage().add(Languages.getString("jsp.admin.reports.taxtypewarning", sessionData.getLanguage()));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error("Error during getMerchantProductSummary", e);
                Torque.closeConnection(dbConn);
                throw new ReportException();
            } finally {
                DbUtil.closeDatabaseObjects(dbConn, pstmt, rs);
            }
        }
        return vecProductSummary;
    }

    @SuppressWarnings("rawtypes")
    private String generateExportFile(ServletContext context, ResultSet rs, SessionData sessionData) {
        String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
        String downloadPath = DebisysConfigListener.getDownloadPath(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        boolean addColumnsDTU1281 = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_ADDITIONAL_COLUMNS_TRX_SUMM_PRODUCT_MERCHANT_REPORT);
        String strRefId = sessionData.getProperty("ref_id");
        String fileToDownload = "";
        //download all data to file.
        String strFileName = transReport.generateFileName(context);
        try (FileChannel fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel()) {
            transReport.writetofile(fc, transReport.getattachedheader().replace("-", "").replace("<br/>", ""));
            transReport.writetofile(fc, "\r\n");
            rs.first();
            if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC)
                    || (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                    && DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))) {
                //Mexico and USA

                //table header
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.agent", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.subagent", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.rep", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.merchant", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.merchantID", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.city", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.state", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.recharegevalue", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.SKU", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.Product", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.lasttransaction", sessionData.getLanguage()) + "\",");

                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.iso_percent", sessionData.getLanguage()) + "\",");
                }
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                        transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.agent_percent", sessionData.getLanguage()) + "\",");
                    }
                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                        transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.subagent_percent", sessionData.getLanguage()) + "\",");
                    }
                }
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP)) {
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.rep_percent", sessionData.getLanguage()) + "\",");
                }
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.merchant_percent", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.trxnumber", sessionData.getLanguage()) + "\",");

                if (addColumnsDTU1281) {
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.salesTitle", sessionData.getLanguage()) + "\",");
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.returnsTitle", sessionData.getLanguage()) + "\",");
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.netTitle", sessionData.getLanguage()) + "\",");
                }
                transReport.writetofile(fc, "\r\n");
                //table content
                while (rs.next()) {
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("Rep")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("SubAgent")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("Agent")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("MerchantName")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("merchant_id")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("phys_city")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("phys_state")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getDouble("RechargeValue"))) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("id")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("description")) + "\",");
                    Vector TimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, rs.getString("lasttransaction"), rs.getString("lasttransaction"), true);
                    transReport.writetofile(fc, "\"" + DateUtil.formatDateTime(this.dbformatter.parse(TimeZoneFilterDates.get(0).toString())) + "\",");
                    if (strAccessLevel.equals(DebisysConstants.ISO)) {
                        transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("iso_rate"))) + "\",");
                    }
                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                            transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("agent_rate"))) + "\",");
                        }
                        if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                            transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("subagent_rate"))) + "\",");
                        }
                    }
                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP)) {
                        transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("rep_rate"))) + "\",");
                    }
                    transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("merchant_rate"))) + "\",");
                    transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("count")) + "\",");

                    if (addColumnsDTU1281) {
                        transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("sales")) + "\",");
                        transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("returns")) + "\",");
                        transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("sales") - rs.getInt("returns")) + "\",");
                    }

                    transReport.writetofile(fc, "\r\n");
                }
            } else {
                //Internation Not-Mexico
                //table header
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.agent", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.subagent", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.rep", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.merchant", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.merchantID", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.city", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.recharegevalue", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.bonusAmount", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.totalRecharge", sessionData.getLanguage()) + "\",");

                if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.reports.netAmount", sessionData.getLanguage()) + "\",");
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.reports.taxAmount", sessionData.getLanguage()) + "\",");
                }
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.SKU", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.Product", sessionData.getLanguage()) + "\",");
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.lasttransaction", sessionData.getLanguage()) + "\",");

                if (strAccessLevel.equals(DebisysConstants.ISO)) {
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.iso_percent", sessionData.getLanguage()) + "\",");
                }
                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                        transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.agent_percent", sessionData.getLanguage()) + "\",");
                    }
                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                        transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.subagent_percent", sessionData.getLanguage()) + "\",");
                    }
                }
                if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP)) {
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.rep_percent", sessionData.getLanguage()) + "\",");
                }
                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.merchant_percent", sessionData.getLanguage()) + "\",");

                transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.trxnumber", sessionData.getLanguage()) + "\",");

                if (addColumnsDTU1281) {
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.salesTitle", sessionData.getLanguage()) + "\",");
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.returnsTitle", sessionData.getLanguage()) + "\",");
                    transReport.writetofile(fc, "\"" + Languages.getString("jsp.admin.report.transactions.products.merchants.netTitle", sessionData.getLanguage()) + "\",");
                }
                transReport.writetofile(fc, "\r\n");

                //table content
                while (true) {
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("Rep")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("SubAgent")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("Agent")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("MerchantName")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("merchant_id")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("phys_city")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getDouble("RechargeValue"))) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(NumberUtil.formatAmount(String.valueOf(rs.getDouble("bonus")))) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(NumberUtil.formatAmount(String.valueOf(rs.getDouble("TotalRecharge")))) + "\",");
                    if (sessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)) {
                        transReport.writetofile(fc, "\"" + StringUtil.toString(NumberUtil.formatAmount(String.valueOf(rs.getDouble("netamount")))) + "\",");
                        transReport.writetofile(fc, "\"" + StringUtil.toString(NumberUtil.formatAmount(String.valueOf(rs.getDouble("taxamount")))) + "\",");
                    }
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("id")) + "\",");
                    transReport.writetofile(fc, "\"" + StringUtil.toString(rs.getString("description")) + "\",");
                    Vector TimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, rs.getString("lasttransaction"), rs.getString("lasttransaction"), true);
                    transReport.writetofile(fc, "\"" + DateUtil.formatDateTime(this.dbformatter.parse(TimeZoneFilterDates.get(0).toString())) + "\",");

                    if (strAccessLevel.equals(DebisysConstants.ISO)) {
                        transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("iso_rate"))) + "\",");
                    }
                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                        if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT)) {
                            transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("agent_rate"))) + "\",");
                        }
                        if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
                            transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("subagent_rate"))) + "\",");
                        }
                    }
                    if (strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.REP)) {
                        transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("rep_rate"))) + "\",");
                    }

                    transReport.writetofile(fc, "\"" + StringUtil.toString(String.valueOf(rs.getFloat("merchant_rate"))) + "\",");
                    transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("count")) + "\",");

                    if (addColumnsDTU1281) {
                        transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("sales")) + "\",");
                        transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("returns")) + "\",");
                        transReport.writetofile(fc, "\"" + String.valueOf(rs.getInt("sales") - rs.getInt("returns")) + "\",");
                    }

                    transReport.writetofile(fc, "\r\n");
                    if (!rs.next()) {
                        break;
                    }
                }
            }
            fileToDownload = compressFile(downloadUrl, downloadPath, workingDir, filePrefix, strFileName);
        } catch (IOException ioe) {
            // break radio silence about transaction report downloads!
            logger.error("Download transactions failed! " + ioe.getClass().getName() + " (" + ioe.getMessage() + ")", ioe);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return fileToDownload;
    }

    private String compressFile(String downloadUrl, String downloadPath, String workingDir, String filePrefix, String strFileName) {
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        logger.debug("Zip File Name: " + zipFileName);
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
        logger.debug("Download URL: " + downloadUrl);
        try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
                FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv")) {
            // Set the compression ratio
            out.setLevel(Deflater.DEFAULT_COMPRESSION);
            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
            // Transfer bytes from the current file to the ZIP file
            // out.write(buffer, 0, in.read(buffer));
            int len;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
            // Close the current entry
            out.closeEntry();
            // Close the current file input stream
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        sourceFile.delete();
        return downloadUrl;
    }
}
