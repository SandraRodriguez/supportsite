package com.debisys.reports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.emida.utils.crypto.TRProxyFactory;
import com.emida.utils.dbUtils.CommonQueries;

/**
 * Holds the information for transactions.
 * <P>
 * 
 * @author Juan Cruz
 */

public class NFinanseReport implements HttpSessionBindingListener {

	private Hashtable validationErrors = new Hashtable();

	// Report Params
	private String start_date = "";
	private String end_date = "";
	private String merchant_ids = "";
	private String category_ids = "";
	private String sort = "";
	private String col = "";

	// log4j logging
	public static Category cat = Category.getInstance(NFinanseReport.class);
	public static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	public void valueBound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
	}

	public void valueUnbound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	private String toJoinedString(String[] strValue){
		String strTemp = "";
		if (strValue != null && strValue.length > 0) {
			boolean isFirst = true;
			for (int i = 0; i < strValue.length; i++) {
				if (strValue[i] != null && !strValue[i].equals("")) {
					if (!isFirst) {
						strTemp = strTemp + "," + strValue[i];
					} else {
						strTemp = strValue[i];
						isFirst = false;
					}
				}
			}
		}
		return strTemp;
	}

	public String getStartDate() {
		return StringUtil.toString(this.start_date);
	}

	public void setStartDate(String strValue) {
		this.start_date = strValue;
	}

	public String getEndDate() {
		return StringUtil.toString(this.end_date);
	}

	public void setEndDate(String strValue) {
		this.end_date = strValue;
	}

	public String getCategoryIds() {
		return StringUtil.toString(this.category_ids);
	}

	public void setCategoryIds(String strValue) {
		this.category_ids = strValue;
	}

	public void setCategoryIds(String[] strValue) {
		this.category_ids = toJoinedString(strValue);
	}

	public String getMerchantIds() {
		return StringUtil.toString(this.merchant_ids);
	}

	public void setMerchantIds(String strValue) {
		this.merchant_ids = strValue;
	}

	public void setMerchantIds(String[] strValue) {
		this.merchant_ids = toJoinedString(strValue);
	}

	public void setCol(String strValue) {
		this.col = strValue;
	}

	public void setSort(String strValue) {
		this.sort = strValue;
	}

	
	/**
	 * Sets the validation error against a specific field. Used by
	 * {@link #validateDateRange validateDateRange}.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @param error
	 *            The error message for the field or null if none is present.
	 */

	public void addFieldError(String fieldname, String error) {
		this.validationErrors.put(fieldname, error);
	}

	/**
	 * Returns a hash of errors.
	 * 
	 * @return Hash of errors.
	 */

	public Hashtable getErrors() {
		return this.validationErrors;
	}

	/**
	 * Validates for missing or invalid fields.
	 * 
	 * @return <code>true</code> if the entity passes the validation checks,
	 *         otherwise <code>false</code>
	 */

	public boolean validateDateRange(SessionData sessionData) {
		this.validationErrors.clear();
		boolean valid = true;
		boolean AreThereStartDate;
		boolean AreThereEndDate;

		AreThereStartDate = !((this.start_date == null) || (this.start_date.length() == 0)) ? true : false;
		AreThereEndDate = !((this.end_date == null) || (this.end_date.length() == 0)) ? true : false;

		// Do all dates validations if there is any of the dates
		if ((AreThereStartDate || AreThereEndDate)) {
			if ((this.start_date == null) || (this.start_date.length() == 0)) {
				addFieldError("startdate", Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()));
				valid = false;
			} else if (!DateUtil.isValid(this.start_date)) {
				addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
				valid = false;
			}
			if ((this.end_date == null) || (this.end_date.length() == 0)) {
				addFieldError("enddate", Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()));
				valid = false;
			} else if (!DateUtil.isValid(this.end_date)) {
				addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
				valid = false;
			}
		}
		if ((valid) && AreThereStartDate && AreThereEndDate) {

			Date dtStartDate = new Date();
			Date dtEndDate = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			formatter.setLenient(false);
			try {
				dtStartDate = formatter.parse(this.start_date);
			} catch (ParseException ex) {
				addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
				valid = false;
			}
			try {
				dtEndDate = formatter.parse(this.end_date);
			} catch (ParseException ex) {
				addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
				valid = false;
			}
			if (dtStartDate.after(dtEndDate)) {
				addFieldError("date", Languages.getString("com.debisys.reports.error5", sessionData.getLanguage()));
				valid = false;
			}

		}
		return valid;
	}
	
	private String getReportFilters(SessionData sessionData){
		String filter = "WHERE ";
		// date
		filter += "(w.datetime >= '" + start_date + " 00:00:00' and  w.datetime <= '" + end_date + " 23:59:59.999" + "')" + "	";
		// categories
		if(category_ids != null && category_ids.length() > 0){
			filter += "	AND p.Trans_Type IN (" + category_ids + ") ";
		}else{
			filter += "	AND p.Trans_Type IN (5, 6) ";
		}
		// by merchant & current user
		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		if (strAccessLevel.equals(DebisysConstants.ISO)){
	        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
	            filter += " AND w.rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK)) WHERE iso_id=" + strRefId + ")";
	        }else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){
	            filter += " AND w.rep_id IN " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_REP
	            	+ " AND iso_id in " + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id in "
	            	+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type=" + DebisysConstants.REP_TYPE_AGENT + " AND iso_id=" + strRefId + "))) ";
	        }
			if(merchant_ids != null && merchant_ids.length() > 0){
				filter += "	AND w.merchant_id IN (" + merchant_ids + ") ";
			}
	    }else if (strAccessLevel.equals(DebisysConstants.MERCHANT)){
	        filter += "	AND w.merchant_id IN (" + strRefId + ") ";
	    }
		return filter;
	}

	private String getReportJoins(boolean forceMerchants){
		String joins = "";
		if(category_ids != null && category_ids.length() > 0){
			joins += " INNER JOIN products AS p WITH (NOLOCK) ON(w.id = p.id) ";
		}else{
			joins += " INNER JOIN products AS p WITH (NOLOCK) ON(w.id = p.id) ";
		}
		if((merchant_ids != null && merchant_ids.length() > 0) || forceMerchants){
			joins += " INNER JOIN merchants AS m WITH (NOLOCK) ON(w.merchant_id = m.merchant_id) ";
		}
		return joins;
	}
	
	private String getSortField(){
		String sort = "w.rec_id";
		if(col.equals("0")){sort = "w.id";}
		if(col.equals("1")){sort = "w.transferAmt";}
		if(col.equals("2")){sort = "w.transFixedFeeAmt";}
		if(col.equals("3")){sort = "category";}
		if(col.equals("4")){sort = "w.datetime";}
		if(col.equals("5")){sort = "w.rec_id";}
		if(col.equals("6")){sort = "m.contact";}
		if(col.equals("7")){sort = "m.phys_address";}
		if(col.equals("8")){sort = "m.phys_city";}
		if(col.equals("9")){sort = "m.phys_state";}
		if(col.equals("10")){sort = "w.ani";}
		if(col.equals("11")){sort = "w.millennium_no";}

		return sort;
	}
	
	public Vector getTransactions(SessionData sessionData, int intPageNumber, int intRecordsPerPage) throws ReportException {
		String sortDir = "";
		Connection dbConn = null;
		Vector vecResultsTemp = new Vector();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String strSQL = "", strSQLcount = "";
		int intRecordCount = 0;
		try {
			dbConn = Torque.getConnection(NFinanseReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				NFinanseReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			sortDir = "ASC";
			if(sort.equals("1")){sortDir = "DESC";}
			String sortField = getSortField();
			String filters = getReportFilters(sessionData);
			String joins = getReportJoins(false);			

			// try to get the totals
			strSQL = "SELECT COUNT(w.transferAmt) AS totalrecords, ISNULL(SUM(w.transferAmt),0) AS totalamount FROM Web_Transactions AS w WITH (NOLOCK) " + joins + filters;
			NFinanseReport.cat.debug(strSQL);
			pst = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pst.executeQuery();
			Vector vecTotal = new Vector();
			if(rs!=null){
				if(rs.next()){
					intRecordCount = rs.getInt("totalrecords");
					vecTotal.add("TOTAL");
					vecTotal.add(NumberUtil.formatCurrency(rs.getString("totalamount")));
				}
			}
			rs.close();
			rs = null;
			vecResultsTemp.add(intRecordCount);
			if (intRecordCount > 0) {
				// now get the data
				int from = DbUtil.getRowNumber(rs, intRecordsPerPage, intRecordCount, intPageNumber);
				int to = from + intRecordsPerPage - 1;
				joins = getReportJoins(true);
				strSQL = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY " + sortField + " " + sortDir+ ") as nfila, w.ani, "  
						+ "	ISNULL(w.transferAmt, 0) AS transferAmt, ISNULL(w.transFixedFeeAmt, 0) AS transFixedFeeAmt, p.Trans_Type, w.datetime, w.rec_id, m.dba, m.phys_address, m.phys_city, m.phys_state, w.millennium_no, w.id, "
						+ "	CASE WHEN p.Trans_Type = 6 THEN 'GiftCard' ELSE (CASE WHEN p.Trans_Type = 5 THEN 'StoredValue' ELSE '' END) END AS category "  
						+ "FROM Web_Transactions AS w WITH (NOLOCK) " + joins + filters
						+ ") AS t WHERE t.nfila BETWEEN " + from + " AND " + to;

				NFinanseReport.cat.debug(strSQL);
				pst = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				rs = pst.executeQuery();
				if (rs != null) {
					if (!(intRecordsPerPage > 0)) {
						intRecordsPerPage = intRecordCount;
					}
					int i = 0;
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					while (rs.next() && i <= intRecordsPerPage) {
						Vector vecTemp = new Vector();
						vecTemp.add((from + i));
						vecTemp.add(StringUtil.toString(rs.getString("id")));
						vecTemp.add(NumberUtil.formatCurrency(rs.getString("transferAmt")));
						vecTemp.add(NumberUtil.formatCurrency(rs.getString("transFixedFeeAmt")));
						vecTemp.add(Languages.getString("jsp.admin.reports.nfinanse."+StringUtil.toString(rs.getString("category")),sessionData.getLanguage()) );
						vecTemp.add(format.format(rs.getTimestamp("datetime")));
						vecTemp.add(StringUtil.toString(rs.getString("rec_id")));
						vecTemp.add(StringUtil.toString(rs.getString("dba")));
						vecTemp.add(StringUtil.toString(rs.getString("phys_address")));
						vecTemp.add(StringUtil.toString(rs.getString("phys_city")));
						vecTemp.add(StringUtil.toString(rs.getString("phys_state")));
						vecTemp.add(StringUtil.toString(rs.getString("ani")));
						vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));						
						vecResultsTemp.add(vecTemp);
						i++;
					}
				}
				rs.close();
				rs = null;
			}
			pst.close();
			pst = null;
			vecResultsTemp.add(vecTotal);
		} catch (Exception e) {
			NFinanseReport.cat.error("Error during getTransactions", e);
			throw new ReportException();
		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				NFinanseReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecResultsTemp;
	}
	
	public StringBuilder getTransactionsCSV(SessionData sessionData) throws ReportException {
		String sortDir = "";
		Connection dbConn = null;
		StringBuilder results = new StringBuilder();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String strSQL = "", strSQLcount = "";
		String QU = "\"";
		String CO = ",";
		try {
			dbConn = Torque.getConnection(NFinanseReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				NFinanseReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			String sortField = getSortField();
			sortDir = "ASC";
			if(sort.equals("1")){sortDir = "DESC";}
			
			String filters = getReportFilters(sessionData);
			String joins = getReportJoins(false);			

			results.append(
					Languages.getString("jsp.admin.reports.nfinanse.summarytitle", sessionData.getLanguage()) + " " + start_date + " " + 
					Languages.getString("jsp.admin.reports.nfinanse.summaryto", sessionData.getLanguage()) + " " + end_date + "\n\n");
			// try to get the totals
			strSQL = "SELECT COUNT(w.transferAmt) AS totalrecords, ISNULL(SUM(w.transferAmt),0) AS totalamount FROM Web_Transactions AS w WITH (NOLOCK) " + joins + filters;
			NFinanseReport.cat.debug(strSQL);
			pst = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pst.executeQuery();
			Vector vecTotal = new Vector();
			if(rs!=null){
				if(rs.next()){
					results.append("\n");
					results.append("Records:"+ CO+ CO );
					results.append(rs.getInt("totalrecords") + CO + CO + CO + CO + CO + CO);
					results.append("\n");
					results.append("Total:"+ CO + CO );
					results.append(QU + NumberUtil.formatCurrency(rs.getString("totalamount")) + QU + CO + CO + CO + CO + CO + CO);
				}
			}
			rs.close();
			rs = null;
			pst.close();
			
			results.append("\n\n");
			// now get the data
			joins = getReportJoins(true);
			strSQL = "SELECT w.ani, "  
					+ "	ISNULL(w.transferAmt, 0) AS transferAmt, ISNULL(w.transFixedFeeAmt, 0) AS transFixedFeeAmt, p.Trans_Type, w.datetime, w.rec_id, m.dba, m.phys_address, m.phys_city, m.phys_state, w.millennium_no, w.id, "
					+ "	CASE WHEN p.Trans_Type = 6 THEN 'GiftCard' ELSE (CASE WHEN p.Trans_Type = 5 THEN 'StoredValue' ELSE '' END) END AS category "  
					+ "FROM Web_Transactions AS w WITH (NOLOCK) " + joins + filters + " ORDER BY " + sortField + " " + sortDir;
			NFinanseReport.cat.debug(strSQL);
			pst = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = pst.executeQuery();
			
			results.append(Languages.getString("jsp.admin.reports.nfinanse.sku", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.loadamount", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.loadfee", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.transtype", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.datetime", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.transid", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.mername", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.meraddress", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.mercity", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.merstate", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.serialnumber", sessionData.getLanguage()) + CO);
			results.append(Languages.getString("jsp.admin.reports.nfinanse.terminalid", sessionData.getLanguage()) + "\n");
			if (rs != null) {
				int i = 0;
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				while (rs.next()) {
					results.append("=" + QU + StringUtil.toString(rs.getString("id")) + QU + CO);
					results.append(QU + NumberUtil.formatCurrency(rs.getString("transferAmt")) + " " + QU + CO);
					results.append(QU + NumberUtil.formatCurrency(rs.getString("transFixedFeeAmt")) + " " + QU + CO);
					results.append( Languages.getString("jsp.admin.reports.nfinanse."+StringUtil.toString(rs.getString("category")),sessionData.getLanguage()) + CO);
					results.append(QU + format.format(rs.getTimestamp("datetime")) + QU + CO);
					results.append("=" + QU + StringUtil.toString(rs.getString("rec_id")) + QU + CO);
					results.append(QU + StringUtil.toString(rs.getString("dba")) + QU + CO);
					results.append(QU + StringUtil.toString(rs.getString("phys_address")) + QU + CO);
					results.append(QU + StringUtil.toString(rs.getString("phys_city")) + QU + CO);
					results.append(QU + StringUtil.toString(rs.getString("phys_state")) + QU + CO);
					results.append("=" + QU + StringUtil.toString(rs.getString("ani")) + QU + CO);
					results.append("=" + QU + StringUtil.toString(rs.getString("millennium_no")) + QU);				
					results.append("\n");
					i++;
				}
			}
			rs.close();
			pst.close();

			pst = null;
		} catch (Exception e) {
			NFinanseReport.cat.error("Error during getTransactions", e);
			throw new ReportException();
		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				NFinanseReport.cat.error("Error during closeConnection", e);
			}
		}
		return results;		
	}
}
