/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports;

import com.debisys.exceptions.CustomerException;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author nmartinez
 */
public class ResourceReport {
    
    public static final String RESOURCE_IMTU_FEATURE = "IMTU_Feature";
    public static final String RESOURCE_MERCHANT_MAP_LOCATOR = "MerchantMapLocator";
    public static final String RESOURCE_PAYMENT_REQUEST_MANAGEMENT = "PaymentRequestMng";
    public static final String RESOURCE_PROMOTION_ALLOW_TERMINAL_TYPE = "PromotionAllowByTerminalType";
    
    private static Logger cat = Logger.getLogger(ResourceReport.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
    
    private String reportId;
    private String language;
    private String keyMessage;
    private String message;
    
    public ResourceReport(){}
    
        
    /**
     * 
     * @param reportCode
     * @param language
     * @return
     * @throws CustomerException 
     */
    public static ArrayList<ResourceReport> findResourcesByReport(String reportCode, String language, String keyMessage) throws CustomerException
    {
        ArrayList<ResourceReport> propertiesReport = new  ArrayList<ResourceReport>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "SELECT rr.KeyMessage, rr.Message FROM ReportResourceByLanguage rr WITH(NOLOCK) "
                       + " INNER JOIN reportConfiguration rc WITH(NOLOCK) ON rr.reportId = rc.reportId"
                       + " WHERE rc.reportCode='"+reportCode+"' AND rr.Language="+language;
            String filter = "";
            if ( keyMessage != null)
            {
                filter = " AND rr.KeyMessage='"+keyMessage+"'"; 
                sql = sql + filter;
            }
            cat.debug("findResourcesByReport =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                ResourceReport resource = new ResourceReport();
                resource.setKeyMessage(rs.getString("KeyMessage"));
                resource.setMessage(rs.getString("Message"));
                propertiesReport.add(resource);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findResourcesByReport ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findResourcesByReport Error during closeConnection", e);
            }
        }
        return propertiesReport;
    }

    /**
     * 
     * @param reportCode
     * @param language
     * @return
     * @throws CustomerException 
     */
    public static HashMap<String, ResourceReport> findResourcesByReportHash(String reportCode, String language) throws CustomerException
    {
        HashMap<String, ResourceReport> propertiesReport = new HashMap<String, ResourceReport>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "SELECT rr.KeyMessage, rr.Message FROM ReportResourceByLanguage rr WITH(NOLOCK) "
                       + " INNER JOIN reportConfiguration rc WITH(NOLOCK) ON rr.reportId = rc.reportId"
                       + " WHERE rc.reportCode='"+reportCode+"' AND rr.Language="+language;
            
            cat.debug("findResourcesByReportHash =[" + sql +"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                ResourceReport resource = new ResourceReport();
                resource.setKeyMessage(rs.getString("KeyMessage"));
                resource.setMessage(rs.getString("Message"));
                propertiesReport.put(resource.getKeyMessage() ,resource);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findResourcesByReportHash ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findResourcesByReport Error during closeConnection", e);
            }
        }
        return propertiesReport;
    }
    
    /**
     * @return the reportId
     */
    public String getReportId() {
        return reportId;
    }

    /**
     * @param reportId the reportId to set
     */
    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    /**
     * @return the language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * @return the keyMessage
     */
    public String getKeyMessage() {
        return keyMessage;
    }

    /**
     * @param keyMessage the keyMessage to set
     */
    public void setKeyMessage(String keyMessage) {
        this.keyMessage = keyMessage;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
