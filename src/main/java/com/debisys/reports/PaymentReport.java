package com.debisys.reports;

import com.debisys.dateformat.DateFormat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.reports.summarys.DownloadsSummary;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

/**
 * Holds the information for transactions.<P>
 *
 * @author Jay Chi
 */

public class PaymentReport implements HttpSessionBindingListener
{

  private String start_date = "";
  private String end_date = "";
  private Hashtable validationErrors = new Hashtable();
  private String sort = "";
  private String col = "";
  private String merchant_ids = "";
  private String strUrlLocation= "";
  private String sales_ids="";
  
  

//log4j logging
  static Category cat = Category.getInstance(PaymentReport.class);
  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
 
  /***
   * ADDED FOR LOCALIZATION - SW
   * @return
   */
  public String getStartDateFormatted()
  {
      return getDateFormatted(this.start_date);
  }
  
  /***
   * ADDED FOR LOCALIZATION - SW
   * @return
   */
  public String getEndDateFormatted()
  {
      return getDateFormatted(this.end_date);
  }
  
  /***
   * ADDED FOR LOCALIZATION - SW
   * @param dateToFormat
   * @return
   */
  private String getDateFormatted(String dateToFormat)
  {
      if(DateUtil.isValid(dateToFormat))
      {
            String date = null;
            try
            {
                // Current support site date is in MM/dd/YYYY.
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                java.util.Date dd = sdf.parse(dateToFormat);
                
                // We will specify a new date format through the database
                SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
                date = sdf1.format(dd); 
                
                // Since Java SimpleDateFormat does not know whether a datetime contains
                // a time or not, remove the time (usually 12:00:00 AM) from the end of the date ranges
                date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
            } 
            catch (ParseException e)
            {
                cat.error("Error during TransactionReport date localization ", e);
            }      
            return date;
      }
      else
      {
          //Something isn't valid in the date string - don't try to localize it
          return StringUtil.toString(this.start_date);
      }
  }  
  
  public String getStartDate()
  {
    return StringUtil.toString(this.start_date);
  }

  public void setStartDate(String strValue)
  {
	  this.start_date = strValue;
  }

  public String getEndDate()
  {
    return StringUtil.toString(this.end_date);
  }

  public void setEndDate(String strValue)
  {
	  this.end_date = strValue;
  }

  public String getMerchantIds()
  {
    return StringUtil.toString(this.merchant_ids);
  }

  public void setMerchantIds(String strValue)
  {
	  this.merchant_ids = strValue;
  }

  public void setMerchantIds(String[] strValue)
  {
    String strTemp = "";
    if (strValue != null && strValue.length > 0)
    {
      boolean isFirst = true;
      for (int i = 0; i < strValue.length; i++)
      {
        if (strValue[i] != null && !strValue[i].equals(""))
        {
          if (!isFirst)
          {
            strTemp = strTemp + "," + strValue[i];
          }
          else
          {
            strTemp = strValue[i];
            isFirst = false;
          }
        }
      }
    }

    this.merchant_ids = strTemp;
  }
  public String getCol()
  {
    return StringUtil.toString(this.col);
  }

  public void setCol(String strCol)
  {
	  this.col = strCol;
  }

  public String getSort()
  {
    return StringUtil.toString(this.sort);
  }

  public void setSort(String strSort)
  {
	  this.sort = strSort;
  }
  /**
   * Returns a hash of errors.
   *
   * @return Hash of errors.
   */

  public Hashtable getErrors()
  {
    return this.validationErrors;
  }

  /**
   * Returns a validation error against a specific field.  If a field
   * was found to have an error during
   * {@link #validateDateRange validateDateRange},  the error message
   * can be accessed via this method.
   *
   * @param fieldname The bean property name of the field
   * @return The error message for the field or null if none is
   *         present.
   */

  public String getFieldError(String fieldname)
  {
    return ((String) this.validationErrors.get(fieldname));
  }


  /**
   * Sets the validation error against a specific field.  Used by
   * {@link #validateDateRange validateDateRange}.
   *
   * @param fieldname The bean property name of the field
   * @param error     The error message for the field or null if none is
   *                  present.
   */

  public void addFieldError(String fieldname, String error)
  {
	  this.validationErrors.put(fieldname, error);
  }


  /**
   * Validates for missing or invalid fields.
   *
   * @return <code>true</code> if the entity passes the validation
   *         checks,  otherwise  <code>false</code>
   */

  public boolean validateDateRange(SessionData sessionData)
  {
	  this.validationErrors.clear();
    boolean valid = true;

    if ((this.start_date == null) ||
        (this.start_date.length() == 0))
    {
      addFieldError("startdate", Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.start_date))
    {
      addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
      valid = false;
    }

    if ((this.end_date == null) ||
        (this.end_date.length() == 0))
    {
      addFieldError("enddate", Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.end_date))
    {
      addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
      valid = false;
    }

    if (valid)
    {

      Date dtStartDate = new Date();
      Date dtEndDate = new Date();
      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
      formatter.setLenient(false);
      try
      {
        dtStartDate = formatter.parse(this.start_date);
      }
      catch (ParseException ex)
      {
        addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
        valid = false;
      }
      try
      {
        dtEndDate = formatter.parse(this.end_date);
      }
      catch (ParseException ex)
      {
        addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
        valid = false;
      }
      if (dtStartDate.after(dtEndDate))
      {
        addFieldError("date", "Start date must be before end date.");
        valid = false;
      }

    }

    return valid;
  }


  public Vector getPaymentSummary(SessionData sessionData, ServletContext context, int scheduleReportType, ArrayList<ColumnReport> headers, ArrayList<String> titles )
      throws ReportException
  {
    Connection dbConn = null;
    Vector vecPaymentSummary = new Vector();
    String strSQLSummary = ""; //sql_bundle.getString("getPaymentSummary");
    String strSQLTotal = sql_bundle.getString("getPaymentTotal");
    String strSQLWhere = "";
    String strAccessLevel = sessionData.getProperty("access_level");
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    String strRefId = sessionData.getProperty("ref_id");
    String strWhereDateTime = "";
    String groupBy =" group by m.merchant_id, m.dba, rt.RouteName,m.salesman_id,s.salesmanname,m.account_no ";
    
    
    StringBuilder strSQL = new StringBuilder();
   
    strSQL.append("SELECT m.dba, m.merchant_id,  ISNULL(m.salesman_id,'') salesman_id, ISNULL(s.salesmanname,'') salesmanname, ISNULL(m.account_no,'') account_no , ISNULL(rt.RouteName,'') RouteName , count(mc.id) as qty, sum(payment) as total_payments ");
    
    if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
    {
    	strSQL.append( ScheduleReport.WILDCARD_FROM_CLAUSE_TO_SCHEDULE_REPORTS );
    }
    strSQL.append("FROM merchants AS m WITH (NOLOCK) INNER JOIN merchant_credits AS mc WITH (NOLOCK) ON mc.merchant_id=m.merchant_id ");
    strSQL.append("left join s2ksalesman s with (nolock) on LTRIM(RTRIM(s.salesmanid))=LTRIM(RTRIM(m.salesman_id))  and s.isoid ="+ strRefId); 
    strSQL.append(" LEFT JOIN Routes AS rt WITH (NOLOCK) ON m.RouteID = rt.RouteID ");
    strSQL.append("WHERE ");
    
    strSQLSummary = strSQL.toString();
    
    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new ReportException();
      }
      Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"),this.start_date, this.end_date + " 23:59:59.999", false);
      strWhereDateTime = " AND (mc.log_datetime >= '" + vTimeZoneFilterDates.get(0) + "' and  mc.log_datetime <= '" + vTimeZoneFilterDates.get(1) + "')";
      
      //get a count of total matches
      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
          if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
          {
            strSQLWhere = " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id = " + 
            sessionData.getProperty("ref_id") + ")) ";            
          }
          else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
          {
            strSQLWhere = " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in " +
                "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_REP+ " and iso_id in " +
                  "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_SUBAGENT+ " and iso_id in " +
                    "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_AGENT+ " and iso_id = " + strRefId + "))))";                
          }
      } 
      else if (strAccessLevel.equals(DebisysConstants.AGENT))
      {
    	  strSQLWhere = " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in "
           	+ "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_REP+ " and iso_id in "
           	+ "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_SUBAGENT+ " and iso_id = " + strRefId + ")))";       
      } 
      else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
      {
    	  strSQLWhere = " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in " 
    	  	+ "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_REP+ " and iso_id = " + strRefId + "))";    	  	
      } 
      else if(strAccessLevel.equals(DebisysConstants.REP)) {    	  strSQLWhere = " m.rep_id = "+strRefId + " ";
    	  	
      } 
      else if(strAccessLevel.equals(DebisysConstants.MERCHANT)) {
    	  strSQLWhere = " mc.merchant_id = "+strRefId + " ";    	 
      }
      strSQLWhere = strSQLWhere + " AND mc.logon_id not in('Events Job') ";
      
      if (this.merchant_ids != null && !this.merchant_ids.equals(""))
      {
        strSQLWhere = strSQLWhere + " AND m.merchant_id in (" + this.merchant_ids + ") ";
      }
            
       
          if (this.sales_ids != null && !this.sales_ids.equals(""))
            {
              strSQLWhere = strSQLWhere + " AND m.salesman_id in ("+this.sales_ids +") ";
            }
      
      int intCol = 0;
      int intSort = 0;
      String strCol = "";
      String strSort = "";

      try
      {
        if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
        {
          intCol = Integer.parseInt(this.col);
          intSort = Integer.parseInt(this.sort);
        }
      }
      catch (NumberFormatException nfe)
      {
        intCol = 0;
        intSort = 0;
      }

      if (intSort == 2)
      {
        strSort = "DESC";
      }
      else
      {
        strSort = "ASC";
      }

      switch (intCol)
      {
        case 1:
          strCol = "m.dba";
          break;
        case 2:
          strCol = "m.merchant_id";
          break;
        case 3:
          strCol = "m.salesman_id";
          break;
        case 4:
          strCol = "s.salesmanname";
          break;
        case 5:
          strCol = "m.account_no";
          break;
        case 6:
            strCol = "rt.RouteName";
            break;          
        case 7:
          strCol = "qty";
          break;
        case 8:
          strCol = "total_payments";
          break;
        default:
          strCol = "m.dba";
          break;
      }

      strSQLSummary = strSQLSummary + strSQLWhere ;
      
      if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
      {
    	 String fixedQuery = strSQLSummary + strWhereDateTime + groupBy;
    	 
		 String relativeQuery =  strSQLSummary ;			
		 relativeQuery += " AND " + ScheduleReport.RANGE_CLAUSE_CALCULCATE_BY_SCHEDULER_COMPONENT;			
		 relativeQuery += groupBy;
							
		 ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_CREDIT_PAYMENT_SUMM_BY_MERCHANT , 1);
		 scheduleReport.setNameDateTimeColumn("mc.log_datetime");	
										
		 scheduleReport.setRelativeQuery( relativeQuery );				
		 scheduleReport.setFixedQuery( fixedQuery );				
		 TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, titles );				
		 sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);	
			
    	return new Vector();
      }
      
      strSQLSummary = strSQLSummary + strWhereDateTime + groupBy + " ORDER BY " + strCol + " " + strSort;;
      
      PreparedStatement pstmt = null;
      if ( scheduleReportType == DebisysConstants.DOWNLOAD_REPORT )
      {
    	  pstmt = dbConn.prepareStatement(strSQLSummary);
          ResultSet rs = pstmt.executeQuery();
          TransactionReport trxReport = new TransactionReport();
          DownloadsSummary downloadsSummary = new DownloadsSummary();
		  String strFileName = trxReport.generateFileName( context );
			
          String totalLabel = Languages.getString("jsp.admin.reports.totals", sessionData.getLanguage() );
          this.strUrlLocation = downloadsSummary.download( rs, sessionData, context, headers, strFileName, titles, totalLabel,3 );
    	  rs.close();
    	  pstmt.close();
    	  return new Vector();
      } 
      
      strSQLTotal = strSQLTotal + strSQLWhere + strWhereDateTime;
      
      //strSQLSummary = strSQLSummary + " order by " + strCol + " " + strSort;
      //strSQLTotal = strSQLTotal + strSQLWhere;

      cat.debug(strSQLTotal);
      
      pstmt = dbConn.prepareStatement(strSQLTotal);
      ResultSet rs = pstmt.executeQuery();
      if (rs.next())
      {
        if (rs.getInt("qty") > 0)
        {
        Vector vecTemp = new Vector();
        vecTemp.add(rs.getString("qty"));
        vecTemp.add(NumberUtil.formatAmount(rs.getString("total_payments")));
        vecPaymentSummary.add(vecTemp);
        }
      }

      cat.debug(strSQLSummary);
      pstmt.close();
      rs.close();

      pstmt = dbConn.prepareStatement(strSQLSummary);
      rs = pstmt.executeQuery();
      while (rs.next())
      {
        Vector vecTemp = new Vector();
        vecTemp.add(StringUtil.toString(rs.getString("dba")));
        vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
        vecTemp.add(rs.getString("qty"));
        vecTemp.add(NumberUtil.formatAmount(rs.getString("total_payments")));
        vecTemp.add(StringUtil.toString(rs.getString("RouteName")));
        vecTemp.add(StringUtil.toString(rs.getString("salesman_id")));
        vecTemp.add(StringUtil.toString(rs.getString("salesmanname")));
        vecTemp.add(StringUtil.toString(rs.getString("account_no")));
        vecPaymentSummary.add(vecTemp);
      }
      pstmt.close();
      rs.close();


    }
    catch (Exception e)
    {
      cat.error("Error during getMerchantPayments", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    return vecPaymentSummary;
  }

  private String download(ResultSet rs, ServletContext context,	ArrayList<ColumnReport> headers, ArrayList<String> titles,	String totalLabel) 
  {
	//this.strUrlLocation;
	return "";
  }

public Vector getExternalPaymentSummary(SessionData sessionData)
throws ReportException
	{
		Connection dbConn = null;
		Vector vecPaymentSummary = new Vector();
		String strSQLSummary = sql_bundle.getString("getExternalPaymentSummary");
		String strSQLTotal = sql_bundle.getString("getPaymentTotal");
		String strSQLWhere = "";
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		
		try
		{
		dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		if (dbConn == null)
		{
		  cat.error("Can't get database connection");
		  throw new ReportException();
		}
		//get a count of total matches
		if (strAccessLevel.equals(DebisysConstants.ISO))
		{
		    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		    {
		      strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in (select rep_id from reps WITH (NOLOCK) where iso_id = " + sessionData.getProperty("ref_id") + ")) AND (mc.log_datetime >= '" + this.start_date + "' and  mc.log_datetime <= '" + this.end_date + " 23:59:59.999" + "')";
		    }
		    else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
		    {
		      strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in " +
		          "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_REP+ " and iso_id in " +
		            "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_SUBAGENT+ " and iso_id in " +
		              "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_AGENT+ " and iso_id = " + strRefId + "))))" +
		          " AND (mc.log_datetime >= '" + this.start_date + "' and  mc.log_datetime <= '" + this.end_date + " 23:59:59.999" + "')";
		    }
		} 
		else if (strAccessLevel.equals(DebisysConstants.AGENT))
		{
			  strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in "
		     	+ "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_REP+ " and iso_id in "
		     	+ "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_SUBAGENT+ " and iso_id = " + strRefId + ")))" 
		     	+ " AND (mc.log_datetime >= '" + this.start_date + "' and  mc.log_datetime <= '" + this.end_date + " 23:59:59.999" + "')";
		} 
		else if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
		{
			  strSQLWhere = strSQLWhere + " m.merchant_id in (select merchant_id from merchants WITH (NOLOCK) where rep_id in " 
			  	+ "(select rep_id from reps WITH (NOLOCK) where type= " +DebisysConstants.REP_TYPE_REP+ " and iso_id = " + strRefId + "))"
			  	+ " AND (mc.log_datetime >= '" + this.start_date + "' and  mc.log_datetime <= '" + this.end_date + " 23:59:59.999" + "')";
		} 
		else if(strAccessLevel.equals(DebisysConstants.REP)) {
			  strSQLWhere += " m.rep_id = "+strRefId + " " +
			  		" AND (mc.log_datetime >= '" + this.start_date + "' and  mc.log_datetime <= '" + this.end_date + " 23:59:59.999'" + ") ";
		} 
		else if(strAccessLevel.equals(DebisysConstants.MERCHANT)) {
			  strSQLWhere += " mc.merchant_id = "+strRefId + " " +
			  " AND (mc.log_datetime >= '" + this.start_date + "' and  mc.log_datetime <= '" + this.end_date + " 23:59:59.999'" + ") ";;
		}
		
		if (this.merchant_ids != null && !this.merchant_ids.equals(""))
		{
		  strSQLWhere = strSQLWhere + " AND m.merchant_id in (" + this.merchant_ids + ") ";
		}
		
		strSQLWhere = strSQLWhere + " AND mc.ExternalDBA IS NOT NULL ";
		
		strSQLSummary = strSQLSummary + strSQLWhere + " group by m.merchant_id, m.dba, rt.RouteName ";
		int intCol = 0;
		int intSort = 0;
		String strCol = "";
		String strSort = "";
		
		try
		{
		  if (this.col != null && this.sort != null && !this.col.equals("") && !this.sort.equals(""))
		  {
		    intCol = Integer.parseInt(this.col);
		    intSort = Integer.parseInt(this.sort);
		  }
		}
		catch (NumberFormatException nfe)
		{
		  intCol = 0;
		  intSort = 0;
		}
		
		if (intSort == 2)
		{
		  strSort = "DESC";
		}
		else
		{
		  strSort = "ASC";
		}
		
		switch (intCol)
		{
		  case 1:
		    strCol = "m.dba";
		    break;
		  case 2:
		    strCol = "m.merchant_id";
		    break;
		  case 3:
		      strCol = "rt.RouteName";
		      break;          
		  case 4:
		    strCol = "qty";
		    break;
		  case 5:
		    strCol = "total_payments";
		    break;
		  default:
		    strCol = "m.dba";
		    break;
		}
		
		strSQLSummary = strSQLSummary + " order by " + strCol + " " + strSort;
		strSQLTotal = strSQLTotal + strSQLWhere;
		
		cat.debug(strSQLTotal);
		PreparedStatement pstmt = null;
		pstmt = dbConn.prepareStatement(strSQLTotal);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next())
		{
		  if (rs.getInt("qty") > 0)
		  {
		  Vector vecTemp = new Vector();
		  vecTemp.add(rs.getString("qty"));
		  vecTemp.add(NumberUtil.formatAmount(rs.getString("total_payments")));
		  vecPaymentSummary.add(vecTemp);
		  }
		}
		
		cat.debug(strSQLSummary);
		pstmt.close();
		rs.close();
		
		pstmt = dbConn.prepareStatement(strSQLSummary);
		rs = pstmt.executeQuery();
		while (rs.next())
		{
		  Vector vecTemp = new Vector();
		  vecTemp.add(StringUtil.toString(rs.getString("dba")));
		  vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
		  vecTemp.add(rs.getString("qty"));
		  vecTemp.add(NumberUtil.formatAmount(rs.getString("total_payments")));
		  vecTemp.add(StringUtil.toString(rs.getString("RouteName")));
		  vecPaymentSummary.add(vecTemp);
		}
		pstmt.close();
		rs.close();
		
		
		}
		catch (Exception e)
		{
		cat.error("Error during getMerchantPayments", e);
		throw new ReportException();
		}
		finally
		{
		try
		{
		  Torque.closeConnection(dbConn);
		}
		catch (Exception e)
		{
		  cat.error("Error during closeConnection", e);
		}
	}
	return vecPaymentSummary;
}

  public void valueBound(HttpSessionBindingEvent event)
   {
   }

   public void valueUnbound(HttpSessionBindingEvent event)
   {
   }

   public static void main(String[] args)
   {
   }
   
   /**
    * @return the strUrlLocation
    */
    public String getStrUrlLocation() 
    {
    	return strUrlLocation;
    }

    
    public String getSalesIds()
  {
    return StringUtil.toString(this.sales_ids);
  }

  public void setSalesIds(String strValue)
  {
	  this.sales_ids = strValue;
  }

  public void setSalesIds(String[] strValue)
  {
    String strTemp = "";
    if (strValue != null && strValue.length > 0)
    {
      boolean isFirst = true;
      for (int i = 0; i < strValue.length; i++)
      {
        if (strValue[i] != null && !strValue[i].equals(""))
        {
          if (!isFirst)
          {
            strTemp = strTemp+ "," + "'"+strValue[i]+"'";
          }
          else
          {
            strTemp = "'"+strValue[i]+"'";
            isFirst = false;
          }
        }
      }
    }
   
    this.sales_ids = strTemp;
  }

}