package com.debisys.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.customers.Rep;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.transactions.TransactionSearch;
import com.debisys.users.SessionData;
import com.debisys.util.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.StringUtil;

public class FeeReports implements HttpSessionBindingListener {

	// private Date today = new Date();
	private final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

	private final SimpleDateFormat dbformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	private String start_date = "";
	private String end_date = "";

	private static Logger cat = Logger.getLogger(FeeReports.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	public static Vector<Vector<String>> getEntityListReports(SessionData sessionData) throws CustomerException {
		Vector<Vector<String>> vResult = new Vector<Vector<String>>();
		try {
			// This method uses a recursive CTE to optimize the search of
			// children below any actor which as a start enhances the reading of
			// this code
			vResult = TransactionReport.getDescendantActors(Long.valueOf(sessionData.getProperty("ref_id")), DebisysConstants.MERCHANT);
		} catch (Exception e) {
			FeeReports.cat.error("Error during getEntityListReports", e);
			throw new CustomerException();
		}

		return vResult;
	}
	
	public static Vector<Vector> getFeeManagementReport(String sStartDate, String sEndDate, int nStatusId, String productIdSelected , int selectEntityType ,String[] entitiesId, SessionData sessionData) throws CustomerException {
		
		FeeReports.cat.info("sStartDate "+sStartDate);
		FeeReports.cat.info("sEndDate   "+sEndDate);
		FeeReports.cat.info("nStatusId  "+nStatusId);
		FeeReports.cat.info("productIdSelected "+productIdSelected);
		FeeReports.cat.info("selectEntityType "+selectEntityType);
		FeeReports.cat.info("entitiesIds "+entitiesId.length);
		
		for (int i = 0; i < entitiesId.length; i++) {
			String string = entitiesId[i];
			FeeReports.cat.info("entitiesId: ["+string+"]");
		}
		
		Vector<Vector> vecResultList = new Vector<Vector>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new TransactionException();
			}

			String strSQL = "SELECT feetransac1_.fee_transaction_id, feetransac1_.fee_batch_id, feetransac1_.charge, feetransac1_.assessed_date, feetransac1_.fee_assessment_note, feetransac1_.assessed_entity_id, feetransac1_.billed_entity_id, feetransac1_.fee_approval_status_id, " +
					"				feeagreeme3_.approval_required, feeagreeme3_.fee_Status_id, feeagreeme3_.creator_user_id, feeagreeme3_.notes, feeagreeme3_.paymentprocesingtypeid, " +
					"				feeproduct4_.fee_product_id, feeproduct4_.fee_name, feeproduct4_.fee_assessment_note AS fee_note_format, feeproduct4_.fee_criteria_type_id, feeproduct4_.fee_recurrence_type_id," +
					"				m.dba, r.businessname " +
					"FROM processed_fee_agreements processedf0_ WITH(NOLOCK) " +
					"LEFT JOIN fee_transactions feetransac1_ WITH(NOLOCK) ON processedf0_.fee_transaction_id = feetransac1_.fee_transaction_id " +
					"LEFT JOIN fee_batches feebatch2_ WITH(NOLOCK) ON feetransac1_.fee_batch_id = feebatch2_.fee_batch_id " +
					"LEFT JOIN dbo.fee_agreements feeagreeme3_ WITH(NOLOCK) ON feetransac1_.fee_agreement_id = feeagreeme3_.fee_agreement_id " +
					"LEFT JOIN fee_products feeproduct4_ WITH(NOLOCK) ON feeagreeme3_.fee_product_id = feeproduct4_.fee_product_id " +
					"LEFT JOIN merchants m WITH(NOLOCK) ON m.merchant_id = feetransac1_.billed_entity_id " +
					"LEFT JOIN reps r WITH(NOLOCK) ON r.rep_id = feetransac1_.billed_entity_id " +
					"WHERE feetransac1_.fee_approval_status_id = ? ";
			
			
			if(entitiesId.length == 1 && entitiesId[0].equals("")){
				// SELECT BY CURRENT ISO
				String strDistChainType = sessionData.getProperty("dist_chain_type");
				String strRefId = sessionData.getProperty("ref_id");
				cat.debug("Taking all entities type: "+selectEntityType+" for ISO: "+strRefId+" strDistChainType: "+strDistChainType);
				Vector vecTemp = Rep.getEntityListByIso(""+selectEntityType, strRefId, strDistChainType);
				String[] arrTemp = new String[vecTemp.size()];
				int count = 0;
				for (Object objVec : vecTemp) {
					Vector obj = (Vector)objVec;
					arrTemp[count] = (String) obj.get(0);
					count++;
				}
				entitiesId = arrTemp;
			}
			
			if(productIdSelected != null && !productIdSelected.trim().equals("")){
				strSQL += " AND feeagreeme3_.fee_product_id = ? ";
			}
			
			if(sStartDate != null && !sStartDate.trim().equals("") && sEndDate != null && !sEndDate.trim().equals("") && nStatusId != 0 ){
				strSQL += " AND feetransac1_.assessed_date >= '"+sStartDate+" 00:00:01' AND feetransac1_.assessed_date <= '"+sEndDate+" 23:59:59'";
			}
			
			if(selectEntityType == Integer.parseInt(DebisysConstants.AGENT)){
				strSQL += " AND r.type = 4 ";
			}
			else if(selectEntityType == Integer.parseInt(DebisysConstants.SUBAGENT)){
				strSQL += " AND r.type = 5 ";
			}
			else if(selectEntityType == Integer.parseInt(DebisysConstants.REP)){
				strSQL += " AND r.type = 1 ";
			}
			
			boolean haveEntities = false;
			if((selectEntityType == Integer.parseInt(DebisysConstants.AGENT) || selectEntityType == Integer.parseInt(DebisysConstants.SUBAGENT) || selectEntityType == Integer.parseInt(DebisysConstants.REP)) &&
					(entitiesId.length >0 && !entitiesId[0].trim().equals(""))){
				strSQL += " AND r.rep_id IN ( ";
				haveEntities = true;
			}
			else if(selectEntityType == Integer.parseInt(DebisysConstants.MERCHANT) && entitiesId.length >0 && !entitiesId[0].trim().equals("")){
				strSQL += " AND m.merchant_id IN ( ";
				haveEntities = true;
			}
			if(haveEntities){
				for (int i = 0; i < entitiesId.length; i++) {
					strSQL += entitiesId[i]+",";
				}
				strSQL = strSQL.substring(0,strSQL.length()-1)+")";
			}
			
			cat.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setInt(1, nStatusId);
			if(productIdSelected != null && !productIdSelected.trim().equals("")){
				pstmt.setString(2, productIdSelected);
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector vecTemp = new Vector();
				String fee_transaction_id = rs.getString("fee_transaction_id");
				String charge = rs.getString("charge");
				Timestamp assessed_date = rs.getTimestamp("assessed_date");
				String fee_assessment_note = rs.getString("fee_assessment_note");
				String assessed_entity_id = rs.getString("assessed_entity_id");
				String billed_entity_id = rs.getString("billed_entity_id");
				String fee_approval_status_id = rs.getString("fee_approval_status_id");
				String notes = rs.getString("notes");
				String fee_product_id = rs.getString("fee_product_id");
				String fee_name = rs.getString("fee_name"); // fee product name
				String fee_note_format = rs.getString("fee_note_format");
				String fee_criteria_type_id = rs.getString("fee_criteria_type_id");
				String fee_recurrence_type_id = rs.getString("fee_recurrence_type_id");
				String dba = rs.getString("dba");
				String businessname = rs.getString("businessname");
				
				if(fee_criteria_type_id.trim().equals("0")){
					fee_criteria_type_id = "Gross Sales";
				}
				else if(fee_criteria_type_id.trim().equals("1")){
					fee_criteria_type_id = "Transaction Volume";
				}
				else if(fee_criteria_type_id.trim().equals("2")){
					fee_criteria_type_id = "Flat";
				}
				
				if(fee_recurrence_type_id.trim().equals("0")){
					fee_recurrence_type_id = "Weekly";
				}
				else if(fee_recurrence_type_id.trim().equals("1")){
					fee_recurrence_type_id = "Monthly";
				}
				else if(fee_recurrence_type_id.trim().equals("2")){
					fee_recurrence_type_id = "Quarterly";
				}
				
				vecTemp.add(StringUtil.toString(fee_transaction_id));
				vecTemp.add(assessed_date);
				
				vecTemp.add(StringUtil.toString(fee_product_id));
				vecTemp.add(StringUtil.toString(fee_name));//3
				
				vecTemp.add(StringUtil.toString(fee_criteria_type_id));
				vecTemp.add(StringUtil.toString(fee_recurrence_type_id));
				
				vecTemp.add(StringUtil.toString(assessed_entity_id));
				if(dba != null && !dba.trim().equals("")){
					vecTemp.add(StringUtil.toString(dba));
				}
				else{
					vecTemp.add(StringUtil.toString(businessname));
				}
				vecTemp.add(StringUtil.toString(billed_entity_id));
				if(dba != null && !dba.trim().equals("")){
					vecTemp.add(StringUtil.toString(dba));
				}
				else{
					vecTemp.add(StringUtil.toString(businessname));
				}
				vecTemp.add(StringUtil.toString(charge));
				
				vecTemp.add(StringUtil.toString(notes));
				vecTemp.add(StringUtil.toString(fee_assessment_note));
				vecTemp.add(StringUtil.toString(fee_approval_status_id));
				
				vecResultList.add(vecTemp);
			}

		} catch (Exception e) {
			cat.error("Error during getFeeManagementReport", e);
		} finally {
			try {
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				if(rs != null){
					rs.close();
					rs = null;
				}
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection (getFeeManagementReport)", e);
			}
		}
		return vecResultList;
	}
	
	
	public static String downloadFeeManagementReport(String sStartDate, String sEndDate, int nStatusId, String productIdSelected , 
			int selectEntityType ,String[] entitiesId, SessionData sessionData, ServletContext context) throws TransactionException {
		
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strAccessLevel = sessionData.getProperty("access_level");

		//downloadUrl = "D:\\topReport\\";
		//downloadPath = "D:\\topReport\\";
		//workingDir = "D:\\topReport\\";

		try
		{
			Vector vData = null;
			vData = getFeeManagementReport(sStartDate, sEndDate, nStatusId, productIdSelected, selectEntityType, entitiesId, sessionData);

			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
			try
			{
				strFileName = (new TransactionSearch()).generateFileName(context);

				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));

				FeeReports.cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
				outputFile.flush();

				//Print the header for the report
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.status", sessionData.getLanguage())+ "\",");
				
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.assessedDate", sessionData.getLanguage())+ "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeProductName", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeCriteria", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeRecurrence", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.assessedId", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.busName", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.billedId", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.busName2", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.chargeInS", sessionData.getLanguage()) + "\",");
				
				outputFile.write("\r\n");

				for (int i = 0; i < vData.size(); i++){
					if(String.valueOf(((Vector) vData.get(i)).get(13)).equals("0") ){
						outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.pendingApproval", sessionData.getLanguage())+ "\",");
					}
					else if(String.valueOf(((Vector) vData.get(i)).get(13)).equals("1") ){
						outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.approved", sessionData.getLanguage())+ "\",");
					}
					else if(String.valueOf(((Vector) vData.get(i)).get(13)).equals("2") ){
						outputFile.write("\"" +  Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.denied", sessionData.getLanguage())+ "\",");
					}
					
					outputFile.write("\"" + DateUtil.formatDateTime(((java.sql.Timestamp)((Vector) vData.get(i)).get(1))) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(3) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(4) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(5) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(6) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(7) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(8) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(9) + "\",");
					outputFile.write("\"" + ((Vector) vData.get(i)).get(10) + "\",");// charge
					outputFile.write("\r\n");
				}
				outputFile.write("\r\n");
				outputFile.flush();
				outputFile.close();
				endQuery = System.currentTimeMillis();
				FeeReports.cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				FeeReports.cat.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				FeeReports.cat.debug("Download URL: " + downloadUrl);

				try
				{

					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

					// Set the compression ratio
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

					// Add ZIP entry to output stream.
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

					// Transfer bytes from the current file to the ZIP file
					// out.write(buffer, 0, in.read(buffer));

					int len;
					while ((len = in.read(buffer)) > 0)
					{
						out.write(buffer, 0, len);
					}

					// Close the current entry
					out.closeEntry();
					// Close the current file input stream
					in.close();
					out.close();
				}
				catch (IllegalArgumentException iae){
					iae.printStackTrace();
				}
				catch (FileNotFoundException fnfe){
					fnfe.printStackTrace();
				}
				catch (IOException ioe){
					ioe.printStackTrace();
				}
				sourceFile.delete();
				endQuery = System.currentTimeMillis();
				FeeReports.cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");

			}
			catch (IOException ioe){
				try{
					if (outputFile != null){
						outputFile.close();
					}
				}
				catch (IOException ioe2){
				}
			}

		}
		catch (Exception e)
		{
			FeeReports.cat.error("Error during downloadFeeManagementReport", e);
			throw new TransactionException();
		}
		long end = System.currentTimeMillis();

		// Display the elapsed time to the standard output
		FeeReports.cat.debug("downloadFeeManagementReport Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");

		return downloadUrl;
		
	}
	
	public static int updateFeeTransactionStatus(int status, int fee_transaction_id){
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		int errors = 0;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new TransactionException();
			}

			String strSQL = "UPDATE fee_transactions SET fee_approval_status_id = ? WHERE fee_transaction_id = ? ";
			cat.debug(strSQL+" /*"+status+","+fee_transaction_id+"*/");
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setInt(1, status);
			pstmt.setInt(2, fee_transaction_id);
			pstmt.executeUpdate();
			errors = 0;/**/
		} catch (Exception e) {
			errors = 1;
			cat.error("Error during updateFeeTransactionStatus", e);
		} finally {
			try {
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return errors;
	}

	public static Vector<Vector<String>> getFeeProductsList(SessionData sessionData) throws CustomerException {
		Vector<Vector<String>> vecResultList = new Vector<Vector<String>>();
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new TransactionException();
			}

			String strSQL = "SELECT fee_product_id, fee_name  FROM fee_products WITH (NOLOCK) ORDER BY fee_name ";
			cat.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(StringUtil.toString(rs.getString("fee_product_id")));
				vecTemp.add(StringUtil.toString(rs.getString("fee_name")));
				vecResultList.add(vecTemp);
			}

		} catch (Exception e) {
			cat.error("Error during getFeeProductsList", e);
		} finally {
			try {
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				if(rs != null){
					rs.close();
					rs = null;
				}
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecResultList;
	}

	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
	}

}
