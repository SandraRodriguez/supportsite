package com.debisys.reports;

import com.debisys.dateformat.DateFormat;
import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.*;
import com.emida.utils.dbUtils.TorqueHelper;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.log4j.Logger;


import javax.servlet.ServletContext;
import java.io.*;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Franky Villadiego
 */
public class SpiffReport {

    private static final Logger log = Logger.getLogger(SimReport.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.spiff.sql");

    private String start_date = "";
    private String end_date = "";
    private Hashtable validationErrors = new Hashtable();
    private String sort = "";
    private String col = "";
    private String merchant_ids = "";
    private String sim="";
    private String pnumber="";
    private Long recordCount = 0L;
    private long pageIndex = 1;
    private long pageSize = 5;


    public SpiffReport() {

    }

    public Long getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Long recordCount) {
        this.recordCount = recordCount;
    }

    public long getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(long pageIndex) {
        this.pageIndex = pageIndex;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartDate() {
        return start_date;
    }

    public void setStartDate(String start_date) {
        this.start_date = start_date;
    }

    public String getEndDate() {
        return end_date;
    }

    public void setEndDate(String end_date) {
        this.end_date = end_date;
    }

    public Hashtable getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(Hashtable validationErrors) {
        this.validationErrors = validationErrors;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public String getMerchantIds() {
        return merchant_ids;
    }

    public void setMerchantIds(String merchant_ids) {
        this.merchant_ids = merchant_ids;
    }

    public void setMerchantIds(String[] merchant_ids) {
        StringBuilder sb1 = new StringBuilder();
        for (String val : merchant_ids) {
            sb1.append(val).append(",");
        }
        sb1.deleteCharAt(sb1.length() - 1);
        this.merchant_ids = sb1.toString();
    }

    public String getSim() {
        return sim;
    }

    public void setSim(String sim) {
        this.sim = sim;
    }

    public String getPnumber() {
        return pnumber;
    }

    public void setPnumber(String pnumber) {
        this.pnumber = pnumber;
    }

    public boolean validateDateRange(SessionData sessionData) {
        this.validationErrors.clear();
        boolean valid = true;

        if ((this.start_date == null) || (this.start_date.length() == 0)) {
            addFieldError("startdate", Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()));
            valid = false;
        }
        else if (!DateUtil.isValid(this.start_date)) {
            addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
            valid = false;
        }

        if ((this.end_date == null) || (this.end_date.length() == 0)) {
            addFieldError("enddate", Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()));
            valid = false;
        }
        else if (!DateUtil.isValid(this.end_date))
        {
            addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
            valid = false;
        }

        if(this.sim != null && !this.sim.trim().isEmpty() && !NumberUtil.isNumeric(this.sim)){
            addFieldError("sim", Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport.simNoValid", sessionData.getLanguage()));
            valid = false;
        }
        if(this.pnumber != null && !this.pnumber.trim().isEmpty() && !NumberUtil.isNumeric(this.pnumber)){
            addFieldError("reference", Languages.getString("jsp.admin.reports.spiffReports.spiffDetailReport.referenceNoValid", sessionData.getLanguage()));
            valid = false;
        }

        if (valid) {

            Date dtStartDate = new Date();
            Date dtEndDate = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            formatter.setLenient(false);
            try
            {
                dtStartDate = formatter.parse(this.start_date);
            }
            catch (ParseException ex)
            {
                addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
                valid = false;
            }
            try
            {
                dtEndDate = formatter.parse(this.end_date);
            }
            catch (ParseException ex)
            {
                addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
                valid = false;
            }
            if (dtStartDate.after(dtEndDate))
            {
                addFieldError("date", "Start date must be before end date.");
                valid = false;
            }

        }

        return valid;
    }

    public String getStartDateFormatted()
    {
        return getDateFormatted(this.start_date);
    }


    public Hashtable getErrors() {
        return this.validationErrors;
    }

    public String getEndDateFormatted()
    {
        return getDateFormatted(this.end_date);
    }

    private String getDateFormatted(String dateToFormat)
    {
        if(DateUtil.isValid(dateToFormat))
        {
            String date = null;
            try
            {
                // Current support site date is in MM/dd/YYYY.
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                java.util.Date dd = sdf.parse(dateToFormat);

                // We will specify a new date format through the database
                SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
                date = sdf1.format(dd);

                // Since Java SimpleDateFormat does not know whether a datetime contains
                // a time or not, remove the time (usually 12:00:00 AM) from the end of the date ranges
                date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
            }
            catch (ParseException e)
            {
                log.error("Error during TransactionReport date localization ", e);
            }
            return date;
        }
        else
        {
            //Something isn't valid in the date string - don't try to localize it
            return StringUtil.toString(this.start_date);
        }
    }

    public void addFieldError(String fieldname, String error) {
        this.validationErrors.put(fieldname, error);
    }



    public String generateCsvReport(ServletContext context, SessionData sessionData) throws ReportException{

        String strFileName = "";
        String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);
        String downloadPath = DebisysConfigListener.getDownloadPath(context);

        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");

        log.debug("**********************************************");
        log.debug("downloadUrl=" + downloadUrl);
        log.debug("workingDir=" + workingDir);
        log.debug("filePrefix=" + filePrefix);
        log.debug("downloadPath=" + downloadPath);
        log.debug("strAccessLevel=" + strAccessLevel);
        log.debug("downloadUrl=" + strDistChainType);
        log.debug("**********************************************");

        log.debug("STARDATE=" + this.start_date);
        log.debug("ENDDATE=" + this.end_date);
        log.debug("SORT=" + this.sort);
        log.debug("COLUNM=" + this.col);
        log.debug("SIM=" + this.sim);
        log.debug("REF=" + this.pnumber);
        log.debug("MIDs=" + this.merchant_ids);


        try{

            BufferedWriter outputFile = null;
            strFileName = generateFileName(context);
            log.debug("generatedFileName=" + strFileName);
            outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
            outputFile.flush();
            log.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");

            String headers = getHeaders(sessionData);
            outputFile.write(headers);

            recordCount = getSpiffQueryCount(sessionData);
            List<SpiffInfo> reportInfo = getSpiffQuery(sessionData, false);
            List<String> values = buildValues(reportInfo, sessionData);
            //List<String> strings = dummyReport(sessionData);
            for (String row : values) {
                outputFile.write(row);
            }

            outputFile.flush();
            outputFile.close();

            File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
            long size = sourceFile.length();
            byte[] buffer = new byte[(int) size];
            String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
            log.debug("Zip File Name: " + zipFileName);


            downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
            log.debug("Download URL: " + downloadUrl);

            ZipOutputStream out =null;
            FileInputStream in = null;
            try {
                out = new ZipOutputStream(new FileOutputStream(zipFileName));

                // Set the compression ratio
                out.setLevel(Deflater.DEFAULT_COMPRESSION);
                in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

                // Transfer bytes from the current file to the ZIP file
                // out.write(buffer, 0, in.read(buffer));

                int len;
                while ((len = in.read(buffer)) > 0)
                {
                    out.write(buffer, 0, len);
                }

            }catch (Exception e){

            }finally {
                if(out != null){
                    out.closeEntry();
                }
                if(in != null){
                    in.close();
                }
                if(out != null){
                    out.close();
                }
            }

            sourceFile.delete();



        }catch (Exception e){
            log.error("Error during generateCsvInventoryReport", e);
            throw new ReportException();
        }finally{

        }

        return downloadUrl;
    }

    public String generateFileName(ServletContext context){
        boolean isUnique = false;
        String strFileName = "";
        while (!isUnique){
            strFileName = generateRandomNumber();
            isUnique = checkFileName(strFileName, context);
        }
        return strFileName;
    }

    private String generateRandomNumber(){
        StringBuffer s = new StringBuffer();
        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);
        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private boolean checkFileName(String strFileName, ServletContext context){
        boolean isUnique = true;
        String downloadPath = DebisysConfigListener.getDownloadPath(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);

        File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
        if (f.exists()){
            // duplicate file found
            log.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
            return false;
        }

        f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
        if (f.exists()){
            // duplicate file found
            log.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
            return false;
        }
        return isUnique;
    }

    private String getHeaders(SessionData sessionData) {
        StringBuilder sb = new StringBuilder();

        ArrayList<ColumnReport> reportHeaders = getReportHeaders(sessionData);

        for (ColumnReport header : reportHeaders) {
            sb.append("\"").append(header.getLanguageDescription()).append("\"").append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("\n");

        return sb.toString();

    }

    private List<String> buildValues(List<SpiffInfo> reportInfo, SessionData sessionData) {
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");

        List<String> result = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        for (SpiffInfo row : reportInfo) {

            sb.append("\"").append(row.getRecId()).append("\"").append(",");
            sb.append("\"").append(row.getActivationTrxIdStr()).append("\"").append(",");
            sb.append("\"").append(row.getMerchantId()).append("\"").append(",");
            sb.append("\"").append(row.getDba().trim()).append("\"").append(",");
            sb.append("\"").append(row.getProductName().trim()).append("\"").append(",");
            sb.append("\"").append(row.getSpiffType().trim()).append("\"").append(",");
            sb.append("\"").append(row.getCommissionPaymentDateStr()).append("\"").append(",");
            sb.append("\"").append(row.getActivationDateStr()).append("\"").append(",");
            sb.append("\"").append(row.getActivationProduct().trim()).append("\"").append(",");
            sb.append("\"").append(row.getPin().trim()).append("\"").append(",");
            sb.append("\"").append(row.getEsn().trim()).append("\"").append(",");
            String sim = row.getSim().trim().isEmpty() ? "\"\"" : "=\"" + row.getSim().trim() + "\"";
            sb.append(sim).append(",");
            sb.append("\"").append(row.getCommissionType()).append("\"").append(",");
            sb.append("\"").append(NumberUtil.formatCurrency(row.getMerchantCommissionAmount().toString())).append("\"").append(",");

            if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) {
                sb.append("\"").append(NumberUtil.formatCurrency(row.getRepCommissionAmount().toString())).append("\"").append(",");
            }

            if (strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT)
                    || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                sb.append("\"").append(NumberUtil.formatCurrency(row.getSubAgentCommissionAmount().toString())).append("\"").append(",");
            }

            if (strAccessLevel.equals(DebisysConstants.AGENT)
                    || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                sb.append("\"").append(NumberUtil.formatCurrency(row.getAgentCommissionAmount().toString())).append("\"").append(",");
            }
            if (strAccessLevel.equals(DebisysConstants.ISO)) {
                sb.append("\"").append(NumberUtil.formatCurrency(row.getIsoCommissionAmount().toString())).append("\"").append(",");
            }

            sb.append("\"").append(row.getPaymentType().trim()).append("\"").append(",");
            sb.append("\"").append(row.getPaymentReferenceNumber().trim()).append("\"").append(",");

            if (strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) {
                sb.append("\"").append(row.getRepName().trim()).append("\"").append(",");
            }

            if (strAccessLevel.equals(DebisysConstants.AGENT)
                    || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
                sb.append("\"").append(row.getSubAgentName().trim()).append("\"").append(",");
            }

            if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                sb.append("\"").append(row.getAgentName().trim()).append("\"").append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("\n");
            result.add(sb.toString());
            sb.delete(0, sb.toString().length());
        }

        return result;
    }

    public List<SpiffInfo> getSpiffReportResult (SessionData sessionData){

        List<SpiffInfo> list = new ArrayList<SpiffInfo>();
        log.debug("SORT=" + this.sort);
        log.debug("COLUNM=" + this.col);
        log.debug("SIM=" + this.sim);
        log.debug("REF=" + this.pnumber);
        log.debug("MIDs=" + this.merchant_ids);

        try {
            recordCount = getSpiffQueryCount(sessionData);
            list.addAll(getSpiffQuery(sessionData, true));
        } catch (ReportException e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<ColumnReport> getReportHeaders(SessionData sessionData){
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");


        ArrayList<ColumnReport> headers = new ArrayList<ColumnReport>();

        headers.add(new ColumnReport("recId", Languages.getString("jsp.admin.reports.spiffReports.recId", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("activationId", Languages.getString("jsp.admin.reports.spiffReports.activationId", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("merchantId", Languages.getString("jsp.admin.reports.spiffReports.merchantId", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("merchantDba", Languages.getString("jsp.admin.reports.spiffReports.merchantDba", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("productName", Languages.getString("jsp.admin.reports.spiffReports.productName", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("spiffType", Languages.getString("jsp.admin.reports.spiffReports.spiffType", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("comPaymentDate", Languages.getString("jsp.admin.reports.spiffReports.commissionPaymentDate", sessionData.getLanguage()).toUpperCase(), String.class, false));

        headers.add(new ColumnReport("activationDate", Languages.getString("jsp.admin.reports.spiffReports.activationDate", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("activationProduct", Languages.getString("jsp.admin.reports.spiffReports.activationProduct", sessionData.getLanguage()).toUpperCase(), String.class, false));

        headers.add(new ColumnReport("pin", Languages.getString("jsp.admin.reports.spiffReports.pin", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("esn", Languages.getString("jsp.admin.reports.spiffReports.esn", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("sim", Languages.getString("jsp.admin.reports.spiffReports.sim", sessionData.getLanguage()).toUpperCase(), String.class, false));

        headers.add(new ColumnReport("commissionType", Languages.getString("jsp.admin.reports.spiffReports.commissionType", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("mCommissionAmount", Languages.getString("jsp.admin.reports.spiffReports.mCommissionAmount", sessionData.getLanguage()).toUpperCase(), String.class, false));

        if (strAccessLevel.equals(DebisysConstants.REP) || strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) {
            headers.add(new ColumnReport("rCommissionAmount", Languages.getString("jsp.admin.reports.spiffReports.rCommissionAmount", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }

        if (strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT)
                || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
            headers.add(new ColumnReport("saCommissionAmount", Languages.getString("jsp.admin.reports.spiffReports.saCommissionAmount", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }

        if (strAccessLevel.equals(DebisysConstants.AGENT)
                || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
            headers.add(new ColumnReport("aCommissionAmount", Languages.getString("jsp.admin.reports.spiffReports.aCommissionAmount", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }
        if (strAccessLevel.equals(DebisysConstants.ISO)) {
            headers.add(new ColumnReport("iCommissionAmount", Languages.getString("jsp.admin.reports.spiffReports.iCommissionAmount", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }


        headers.add(new ColumnReport("paymentType", Languages.getString("jsp.admin.reports.spiffReports.paymentType", sessionData.getLanguage()).toUpperCase(), String.class, false));
        headers.add(new ColumnReport("paymentReference", Languages.getString("jsp.admin.reports.spiffReports.paymentReference", sessionData.getLanguage()).toUpperCase(), String.class, false));

        if (strAccessLevel.equals(DebisysConstants.SUBAGENT) || strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) {
            headers.add(new ColumnReport("repName", Languages.getString("jsp.admin.reports.spiffReports.repName", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }

        if (strAccessLevel.equals(DebisysConstants.AGENT)
                || (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))) {
            headers.add(new ColumnReport("subAgentName", Languages.getString("jsp.admin.reports.spiffReports.subAgentName", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }

        if (strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
            headers.add(new ColumnReport("agentName", Languages.getString("jsp.admin.reports.spiffReports.agentName", sessionData.getLanguage()).toUpperCase(), String.class, false));
        }


        return headers;
    }

    private String buildMerchantIDsFilterAnd(SessionData sessionData){
        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strRefId = sessionData.getProperty("ref_id");

        String format = MessageFormat.format("SD={0}. ED={1}. Sort={2}. Col={3}. IDs={4}. Sim={5}. PN={6}. AL={7}. CTYP={8}. REF={9}.", start_date, end_date, sort, col, merchant_ids, sim, pnumber, strAccessLevel, strDistChainType, strRefId);
        log.debug(format);


        StringBuilder sb1 = new StringBuilder();
        if (strAccessLevel.equals(DebisysConstants.ISO)){
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
                sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE iso_id =").append(strRefId).append(")");

            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
                sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(");
                sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id IN(");
                sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_AGENT).append(" AND iso_id =").append(strRefId).append(")))");
            }
        }else if (strAccessLevel.equals(DebisysConstants.AGENT)) {
            sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
            sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id IN(");
            sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_SUBAGENT).append(" AND iso_id =").append(strRefId).append("))");

        }else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
            sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id IN (");
            sb1.append("SELECT rep_id FROM reps WITH(NOLOCK) WHERE type =").append(DebisysConstants.REP_TYPE_REP).append(" AND iso_id =").append(strRefId).append(")");
        }else if(strAccessLevel.equals(DebisysConstants.REP)) {
            sb1.append("SELECT merchant_id FROM merchants WITH(NOLOCK) WHERE rep_id =").append(strRefId);

        }else if(strAccessLevel.equals(DebisysConstants.MERCHANT)) {
            sb1.append(strRefId);
        }
        return sb1.toString();

    }

    private void printColumnTypes(PreparedStatement pstmt) throws Exception{
        ResultSetMetaData metaData = pstmt.getMetaData();
        int columnCount = metaData.getColumnCount();
        log.debug("Columnas=" + columnCount);
        for(int x = 1; x<=columnCount; x++){
            int columnType = metaData.getColumnType(x);
            log.debug("Col" + x + "=" + columnType);
        }
    }



    public Long getSpiffQueryCount(SessionData sessionData) throws ReportException {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        Long recordCount = 0L;
        try{
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null){
                log.error("Can't get database connection");
                throw new CustomerException();
            }

            StringBuilder sbRootQuery = new StringBuilder();
            sbRootQuery.append("SELECT COUNT(*) FROM (");

            sbRootQuery.append(mainInnerQuery(sessionData));

            sbRootQuery.append(") AS tbl");

            log.debug("QUERY NUEWVO:" + sbRootQuery.toString());

            pstmt = dbConn.prepareStatement(sbRootQuery.toString());

            rs = pstmt.executeQuery();

            while (rs.next()){
                recordCount = rs.getLong(1);
            }

        }catch (Exception e){
            log.error("Error during getNsfReport", e);
            throw new ReportException();
        }finally{
            try{
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            }catch (Exception e){
                log.error("Error during closeConnection in getNsfReport", e);
            }
        }
        return recordCount;
    }


    public List<SpiffInfo> getSpiffQuery(SessionData sessionData, boolean paginate) throws ReportException {

        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        List<SpiffInfo> result = new ArrayList<SpiffInfo>();
        try{
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null){
                log.error("Can't get database connection");
                throw new CustomerException();
            }


            StringBuilder sbRootQuery = new StringBuilder();
            sbRootQuery.append("SELECT * FROM (");
            sbRootQuery.append("SELECT ROW_NUMBER() OVER(ORDER BY ").append(buildOrder()).append(") AS RWN, * FROM (");

            sbRootQuery.append(mainInnerQuery(sessionData));


            sbRootQuery.append(") AS tbl ");
            sbRootQuery.append(") AS tbl2 ");

            long l1 = 1;
            long l2 = this.recordCount;
            if(paginate) {
                l1 = (((pageIndex - 1) * pageSize) + 1);
                l2 = pageIndex * pageSize;
            }
            sbRootQuery.append(" WHERE RWN BETWEEN ").append(l1).append(" AND ").append(l2);

            log.debug("QUERY NUEWVO:" + sbRootQuery.toString());

            pstmt = dbConn.prepareStatement(sbRootQuery.toString());

            rs = pstmt.executeQuery();

            while (rs.next()){
                SpiffInfo si = new SpiffInfo();
                si.setRecId(rs.getLong("RecID"));
                si.setActivationTrxId(rs.getLong("ActivationTrxId"));
                si.setMerchantId(rs.getLong("MerchantID"));
                si.setDba(rs.getString("MerchantDBA"));
                si.setProductName(rs.getString("ProductName"));
                si.setSpiffType(rs.getString("SpiffType"));
                si.setRepName(rs.getString("RepName"));
                si.setCommissionType(rs.getString("CommissionType"));
                si.setIsoRate(rs.getBigDecimal("IsoRate"));
                si.setAgentRate(rs.getBigDecimal("AgentRate"));
                si.setSubAgentRate(rs.getBigDecimal("SubAgentRate"));
                si.setRepRate(rs.getBigDecimal("RepRate"));
                si.setMerchantRate(rs.getBigDecimal("MerchantRate"));

                //si.setCommissionPaymentDate(rs.getDate("PaymentDate"));
                //si.setPaymentType(rs.getString("PaymentType"));
                //si.setPaymentReferenceNumber(rs.getString("PaymentReference"));

                Long blRecId = rs.getLong("BlRecId");
                Long cptRecId = rs.getLong("CptRecId");
                if(blRecId != null){
                    si.setPaymentType("ACH");
                    si.setCommissionPaymentDate(rs.getDate("BlTransmitDate"));
                    si.setPaymentReferenceNumber(rs.getString("BlTraceNumber"));
                }else if (cptRecId != null){
                    si.setPaymentType("CREDIT");
                    si.setCommissionPaymentDate(rs.getDate("McLogDatetime"));
                    si.setPaymentReferenceNumber(rs.getString("McId"));
                }else{
                    si.setPaymentType("");
                }

                si.setPin(rs.getString("PIN"));
                si.setEsn(rs.getString("ESN"));
                si.setSim(rs.getString("SIM"));
                si.setActivationDate(rs.getDate("ActivationDate"));
                si.setActivationProduct(rs.getString("ActivationProduct"));

                si.setMerchantCommissionAmount(rs.getBigDecimal("CommissionAmount"));
                si.setRepCommissionAmount(rs.getBigDecimal("RepCommissionAmount"));
                si.setSubAgentCommissionAmount(rs.getBigDecimal("SubAgentCommissionAmount"));
                si.setAgentCommissionAmount(rs.getBigDecimal("AgentCommissionAmount"));
                si.setIsoCommissionAmount(rs.getBigDecimal("IsoCommissionAmount"));

                si.setRepName(rs.getString("RepName"));
                si.setSubAgentName(rs.getString("SubAgentName"));
                si.setAgentName(rs.getString("AgentName"));

                result.add(si);
            }

        }catch (Exception e){
            log.error("Error during getNsfReport", e);
            throw new ReportException();
        }finally{
            try{
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            }catch (Exception e){
                log.error("Error during closeConnection in getNsfReport", e);
            }
        }
        return result;
    }

    private String mainInnerQuery(SessionData sessionData) throws Exception {

        String merchantIDsFilterAnd = buildMerchantIDsFilterAnd(sessionData);
        String whereDates = buildBetweenDates(sessionData, "spt.datetime");


        StringBuilder innerQuery = new StringBuilder();
        innerQuery.append("SELECT ");
        innerQuery.append(fieldQuery());
        innerQuery.append(fromQuery());
        innerQuery.append(whereBasicQuery());

        if(this.merchant_ids != null  && !this.merchant_ids.isEmpty()){
            innerQuery.append(" AND spt.merchant_id IN(").append(this.merchant_ids).append(") ");
        }else {
            innerQuery.append(" AND spt.merchant_id IN(").append(merchantIDsFilterAnd).append(") ");
        }

        innerQuery.append(whereDates);

        if(this.pnumber != null && !this.pnumber.isEmpty()){
            innerQuery.append(" AND (bl.ACHFileTraceNumber=").append(this.pnumber).append(" OR mc.id=").append(this.pnumber).append(") ");
        }
        if(this.sim != null && !this.sim.isEmpty()){
            innerQuery.append(" AND sat.sim='").append(this.sim).append("' ");
        }

        return innerQuery.toString();
    }


    private String buildBetweenDates(SessionData sessionData, String fieldName) throws Exception{
        Vector vTimeZoneFilterDates = com.debisys.utils.TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"), this.start_date, this.end_date + " 23:59:59.999", false);
        StringBuilder sb = new StringBuilder();
        sb.append(" AND (").append(fieldName).append(">='").append(vTimeZoneFilterDates.get(0)).append("' AND ").append(fieldName).append("<='").append(vTimeZoneFilterDates.get(1)).append("') ");
        return sb.toString();
    }

    private String buildOrder() {
        StrBuilder sb = new StrBuilder();
        String columnToOrder = "";
        String order = (sort != null && sort.equals("1")) ? " DESC " : " ASC ";
        Integer valueCol = (col != null && !col.isEmpty()) ? Integer.valueOf(col) : -1;
        switch (valueCol){
            case 0: columnToOrder = "RecID" + order; break;
            case 1: columnToOrder = "ActivationTrxId" + order; break;
            case 2: columnToOrder = "MerchantId" + order; break;
            case 3: columnToOrder = "MerchantDBA" + order; break;
            case 4: columnToOrder = "ProductName" + order; break;
            case 5: columnToOrder = "SpiffType" + order; break;
            case 6: columnToOrder = "PaymentDate" + order; break;
            case 7: columnToOrder = "ActivationDate" + order; break;
            case 8: columnToOrder = "ActivationProduct" + order; break;
            case 9: columnToOrder = "PIN" + order; break;
            case 10: columnToOrder = "ESN" + order; break;
            case 11: columnToOrder = "SIM" + order; break;
            case 12: columnToOrder = "CommissionType" + order; break;
            case 13: columnToOrder = "CommissionAmount" + order; break;
            case 14: columnToOrder = "RepCommissionAmount" + order; break;
            case 15: columnToOrder = "SubAgentCommissionAmount" + order; break;
            case 16: columnToOrder = "AgentCommissionAmount" + order; break;
            case 17: columnToOrder = "IsoCommissionAmount" + order; break;
            case 18: columnToOrder = "PaymentType" + order; break;
            case 19: columnToOrder = "PaymentReference" + order; break;
            case 20: columnToOrder = "RepName" + order; break;
            case 21: columnToOrder = "SubAgentName" + order; break;
            case 22: columnToOrder = "AgentName" + order; break;
            default: columnToOrder = "RecID" + order;
        }

        sb.append(columnToOrder);
        return sb.toString();


    }

    private String fieldQuery(){
        StringBuilder sb = new StringBuilder()
        .append("spt.rec_id AS RecId,")
        .append("spt.merchant_id AS MerchantID,")
        .append("spt.dba AS MerchantDBA,")
        .append("spt.description AS ProductName,")
        .append("sty.spifftypedescription AS SpiffType,")
        //.append("bl.transmitDate AS PaymentDate,")
        .append("'ACTIVATION SPIFF' AS CommissionType,")
        //.append("'ACH' As PaymentType,")
        //.append("bl.ACHFileTraceNumber AS PaymentReference,")
        .append("(spt.amount * spt.merchant_rate / 100) AS CommissionAmount,")
        .append("(spt.amount * spt.rep_rate / 100) AS RepCommissionAmount,")
        .append("(spt.amount * spt.subagent_rate / 100) AS SubAgentCommissionAmount,")
        .append("(spt.amount * spt.agent_rate / 100) AS AgentCommissionAmount,")
        .append("(spt.amount * spt.iso_rate / 100) AS IsoCommissionAmount,")
        .append("spt.iso_rate AS IsoRate,")
        .append("spt.agent_rate AS AgentRate,")
        .append("spt.subagent_rate AS SubAgentRate,")
        .append("spt.rep_rate AS RepRate,")
        .append("spt.merchant_rate AS MerchantRate,")
        .append("spt.datetime AS TrxDate,")
        .append("sat.sim AS SIM,")
        .append("sat.esn AS ESN,")
        .append("t2.pin AS PIN,")
        .append("t4.datetime AS ActivationDate,")
        .append("prd.description AS ActivationProduct,")
        .append("rep.businessname AS RepName,")
        .append("suba.businessname AS SubAgentName,")
        .append("age.businessname AS AgentName,")
        .append("sat.transactionIdMaster AS ActivationTrxId,")
        .append("bl.recid AS BlRecId,")
        .append("bl.transmitDate AS BlTransmitDate,")
        .append("bl.ACHFileTraceNumber AS BlTraceNumber,")
        .append("cpt.transactionId AS CptRecId,")
        .append("mc.log_datetime AS McLogDatetime,")
        .append("mc.id AS McId ");

        return sb.toString();

    }

    private String fromQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("FROM SpiffTypes sty WITH(NOLOCK) ");
        sb.append("INNER JOIN spiffsproducts spp WITH(NOLOCK) ON spp.spifftype = sty.id ");
        sb.append("INNER JOIN spiff_transactions spt WITH(NOLOCK) ON spt.id = spp.productspiff ");
        sb.append("INNER JOIN reps rep WITH(NOLOCK) ON rep.rep_id = spt.rep_id ");
        sb.append("INNER JOIN reps suba WITH(NOLOCK) ON suba.rep_id = rep.iso_id ");
        sb.append("INNER JOIN reps age WITH(NOLOCK) ON age.rep_id = suba.iso_id ");
        sb.append("LEFT OUTER JOIN billing bl WITH(NOLOCK) ON bl.recid = spt.rec_id AND bl.paymentTypeId = 2 AND bl.entityTypeId=8 ");
        sb.append("LEFT OUTER JOIN CommissionProcessedTransactions cpt WITH ( NOLOCK ) ON cpt.transactionid = spt.rec_id AND cpt.entitytype = 'm' ");
        sb.append("LEFT OUTER JOIN merchant_credits mc WITH ( NOLOCK ) ON mc.id = cpt.paymentid ");
        sb.append("LEFT OUTER JOIN SimActivationTransactionsDetail satd WITH(NOLOCK) ON satd.transactionId = spt.rec_id ");
        sb.append("LEFT OUTER JOIN HelperTransactions ht WITH(NOLOCK) ON ht.EmidaTransactionId = spt.rec_id ");
        sb.append("INNER JOIN transactions t2 on t2.rec_id = spt.rec_id AND t2.ach_type IN(7,8) ");
        sb.append("LEFT OUTER JOIN transactions t3 on t2.related_rec_id = t3.rec_id and t3.type=1 ");
        sb.append("LEFT OUTER JOIN SimActivationTransactions sat WITH(NOLOCK) ON (sat.idnew = satd.idsimactivationtransactions OR sat.idnew = ht.IdNewSimActivations) ");
        //sb.append("LEFT OUTER JOIN sims si WITH(NOLOCK) ON si.transactionId = sat.idnew ");
        sb.append("LEFT OUTER JOIN products prd WITH(NOLOCK) ON prd.id = satd.productid ");
        sb.append("LEFT OUTER JOIN transactions t4 on t4.rec_id = sat.transactionIdMaster ");

        return sb.toString();
    }

    private String whereBasicQuery(){
        StrBuilder sb = new StrBuilder();
        sb.append(" WHERE 1 = 1 ");
        return sb.toString();
    }

    private String queryOneFields(){
        StringBuilder sb = new StringBuilder();
        sb.append("spt.rec_id AS RecId,").append("spt.merchant_id AS MerchantID,").append("spt.dba AS MerchantDBA,").append("spt.description AS ProductName,");
        sb.append("sty.spifftypedescription AS SpiffType,").append("bl.transmitDate AS PaymentDate,").append("'ACTIVATION SPIFF' AS CommissionType,");
        sb.append("'ACH' As PaymentType,").append("bl.ACHFileTraceNumber AS PaymentReference,").append("((spt.amount * spt.merchant_rate / 100) * -1) AS CommissionAmount,");
        sb.append("((spt.amount * spt.rep_rate / 100) * -1) AS RepCommissionAmount,").append("((spt.amount * spt.subagent_rate / 100) * -1) AS SubAgentCommissionAmount,");
        sb.append("((spt.amount * spt.agent_rate / 100) * -1) AS AgentCommissionAmount,").append("((spt.amount * spt.iso_rate / 100) * -1) AS IsoCommissionAmount,");
        sb.append("spt.iso_rate AS IsoRate,").append("spt.agent_rate AS AgentRate,").append("spt.subagent_rate AS SubAgentRate,");
        sb.append("spt.rep_rate AS RepRate,").append("spt.merchant_rate AS MerchantRate,").append("spt.datetime AS TrxDate,");
        sb.append("sat.sim AS SIM,").append("sat.esn AS ESN,").append("sat.pinAccount AS PIN,");
        sb.append("si.activationDate AS ActivationDate,").append("prd.description AS ActivationProduct,");
        sb.append("rep.businessname AS RepName,").append("suba.businessname AS SubAgentName,").append("age.businessname AS AgentName,");
        sb.append("sat.transactionIdMaster AS ActivationTrxId ");
        return sb.toString();
    }

    private String queryOneFrom(){
        StringBuilder sb = new StringBuilder();
        sb.append("FROM SpiffTypes sty WITH(NOLOCK) ");
        sb.append("INNER JOIN spiffsproducts spp WITH(NOLOCK) ON spp.spifftype = sty.id ");
        sb.append("INNER JOIN spiff_transactions spt WITH(NOLOCK) ON spt.id = spp.productspiff ");
        sb.append("INNER JOIN reps rep WITH(NOLOCK) ON rep.rep_id = spt.rep_id ");
        sb.append("INNER JOIN reps suba WITH(NOLOCK) ON suba.rep_id = rep.iso_id ");
        sb.append("INNER JOIN reps age WITH(NOLOCK) ON age.rep_id = suba.iso_id ");
        sb.append("LEFT OUTER JOIN billing bl WITH(NOLOCK) ON bl.recid = spt.rec_id AND bl.paymentTypeId = 2 AND bl.entityTypeId=8 ");
        sb.append("LEFT OUTER JOIN SimActivationTransactionsDetail satd WITH(NOLOCK) ON satd.transactionId = spt.rec_id ");
        sb.append("LEFT OUTER JOIN HelperTransactions ht WITH(NOLOCK) ON ht.EmidaTransactionId = spt.rec_id ");
        sb.append("INNER JOIN SimActivationTransactions sat WITH(NOLOCK) ON (sat.idnew = satd.idsimactivationtransactions OR sat.idnew = ht.IdNewSimActivations) ");
        sb.append("LEFT OUTER JOIN sims si WITH(NOLOCK) ON si.transactionId = sat.idnew ");
        sb.append("LEFT OUTER JOIN  products prd WITH(NOLOCK) ON prd.id = satd.productid ");

        return sb.toString();
    }

    private String queryOneWhere(){
        StrBuilder sb = new StrBuilder();
        sb.append(" WHERE 1 = 1 AND bl.recid IS NOT NULL ");
        return sb.toString();
    }



    private String queryTwoFields(){
        StringBuilder sb = new StringBuilder();
        sb.append("spt.rec_id AS RecId,").append("spt.merchant_id AS MerchantID,").append("spt.dba AS MerchantDBA,").append("spt.description AS ProductName,");
        sb.append("sty.spifftypedescription AS SpiffType,").append("mc.log_datetime AS PaymentDate,").append("'ACTIVATION SPIFF' AS CommissionType,");
        sb.append("'CREDIT' As PaymentType,").append("mc.id AS PaymentReference,").append("((spt.amount * spt.merchant_rate / 100)) AS CommissionAmount,");
        sb.append("((spt.amount * spt.rep_rate / 100)) AS RepCommissionAmount,").append("((spt.amount * spt.subagent_rate / 100)) AS SubAgentCommissionAmount,");
        sb.append("((spt.amount * spt.agent_rate / 100)) AS AgentCommissionAmount,").append("((spt.amount * spt.iso_rate / 100)) AS IsoCommissionAmount,");
        sb.append("spt.iso_rate AS IsoRate,").append("spt.agent_rate AS AgentRate,").append("spt.subagent_rate AS SubAgentRate,");
        sb.append("spt.rep_rate AS RepRate,").append("spt.merchant_rate AS MerchantRate,").append("spt.datetime AS TrxDate,");
        sb.append("sat.sim AS SIM,").append("sat.esn AS ESN,").append("sat.pinAccount AS PIN,");
        sb.append("si.activationDate AS ActivationDate,").append("prd.description AS ActivationProduct,");
        sb.append("rep.businessname AS RepName,").append("suba.businessname AS SubAgentName,").append("age.businessname AS AgentName,");
        sb.append("sat.transactionIdMaster AS ActivationTrxId ");
        return sb.toString();
    }

    private String queryTwoFrom(){
        StringBuilder sb = new StringBuilder();
        sb.append("FROM SpiffTypes sty WITH(NOLOCK) ");
        sb.append("INNER JOIN spiffsproducts spp WITH(NOLOCK) ON spp.spifftype = sty.id ");
        sb.append("INNER JOIN spiff_transactions spt WITH(NOLOCK) ON spt.id = spp.productspiff ");
        sb.append("INNER JOIN reps rep WITH(NOLOCK) ON rep.rep_id = spt.rep_id ");
        sb.append("INNER JOIN reps suba WITH(NOLOCK) ON suba.rep_id = rep.iso_id ");
        sb.append("INNER JOIN reps age WITH(NOLOCK) ON age.rep_id = suba.iso_id ");
        sb.append("LEFT OUTER JOIN CommissionProcessedTransactions cpt WITH(NOLOCK) ON cpt.transactionid = spt.rec_id AND cpt.entitytype = 'm' ");
        sb.append("LEFT OUTER JOIN merchant_credits mc WITH(NOLOCK) ON mc.id = cpt.paymentid ");
        sb.append("LEFT OUTER JOIN SimActivationTransactionsDetail satd WITH(NOLOCK) ON satd.transactionId = spt.rec_id ");
        sb.append("LEFT OUTER JOIN HelperTransactions ht WITH(NOLOCK) ON ht.EmidaTransactionId = spt.rec_id ");
        sb.append("INNER JOIN SimActivationTransactions sat WITH(NOLOCK) ON (sat.idnew = satd.idsimactivationtransactions OR sat.idnew = ht.IdNewSimActivations) ");
        sb.append("LEFT OUTER JOIN sims si WITH(NOLOCK) ON si.transactionId = sat.idnew ");
        sb.append("LEFT OUTER JOIN products prd WITH(NOLOCK) ON prd.id = satd.productid ");
        return sb.toString();
    }

    private String queryTwoWhere(){
        StringBuilder sb = new StringBuilder();
        sb.append(" WHERE 1 = 1 AND cpt.transactionid IS NOT NULL ");
        return sb.toString();
    }


    public class SpiffInfo{
        private Long recId; //Spiff trasaction ID
        private Long activationTrxId;
        private Long merchantId;
        private String dba = "";
        private String productName = "";
        private String spiffType = "";
        private Date commissionPaymentDate;
        private Date activationDate;
        private String activationProduct = "";
        private String pin = "";
        private String esn = "";
        private String sim = "";
        private String commissionType = "";
        private BigDecimal isoRate = new BigDecimal(0.0);
        private BigDecimal agentRate = new BigDecimal(0.0);
        private BigDecimal subAgentRate = new BigDecimal(0.0);
        private BigDecimal repRate = new BigDecimal(0.0);
        private BigDecimal merchantRate = new BigDecimal(0.0);
        private BigDecimal merchantCommissionAmount = new BigDecimal(0.0);
        private BigDecimal repCommissionAmount = new BigDecimal(0.0);
        private BigDecimal subAgentCommissionAmount = new BigDecimal(0.0);
        private BigDecimal agentCommissionAmount = new BigDecimal(0.0);
        private BigDecimal isoCommissionAmount = new BigDecimal(0.0);
        private String paymentType = "";
        private String paymentReferenceNumber = "";
        private String repName = "";
        private String subAgentName = "";
        private String agentName = "";



        public Long getRecId() {
            return recId;
        }

        public void setRecId(Long recId) {
            this.recId = recId;
        }

        public String getActivationTrxIdStr() {
            return (activationTrxId != null && activationTrxId > 0 )  ? activationTrxId.toString() : "";
        }

        public void setActivationTrxId(Long activationTrxId) {
            this.activationTrxId = activationTrxId;
        }

        public Long getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(Long merchantId) {
            this.merchantId = merchantId;
        }

        public String getDba() {
            return dba;
        }

        public void setDba(String dba) {
            this.dba = dba;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getSpiffType() {
            return spiffType;
        }

        public void setSpiffType(String spiffType) {
            this.spiffType = spiffType;
        }

        public String getCommissionPaymentDateStr() {
            return commissionPaymentDate == null ? "" : commissionPaymentDate.toString();
        }

        public void setCommissionPaymentDate(Date commissionPaymentDate) {
            this.commissionPaymentDate = commissionPaymentDate;
        }

        public String getActivationDateStr() {
            return activationDate == null ? "" : activationDate.toString();
        }

        public void setActivationDate(Date activationDate) {
            this.activationDate = activationDate;
        }

        public String getActivationProduct() {
            return activationProduct != null ? activationProduct : "";
        }

        public void setActivationProduct(String activationProduct) {
            this.activationProduct = activationProduct;
        }

        public String getPin() {
            return pin != null ? pin : "";
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getEsn() {
            return esn != null ? esn : "";
        }

        public void setEsn(String esn) {
            this.esn = esn;
        }

        public String getSim() {
            return sim != null ? sim : "";
        }

        public void setSim(String sim) {
            this.sim = sim;
        }

        public String getCommissionType() {
            return commissionType;
        }

        public void setCommissionType(String commissionType) {
            this.commissionType = commissionType;
        }

        public BigDecimal getMerchantCommissionAmount() {
            //return merchantCommissionAmount.doubleValue() < 0 ? merchantCommissionAmount.multiply(new BigDecimal(-1)) : merchantCommissionAmount;
            return merchantCommissionAmount;
        }

        public void setMerchantCommissionAmount(BigDecimal merchantCommissionAmount) {
            this.merchantCommissionAmount = merchantCommissionAmount;
        }

        public BigDecimal getRepCommissionAmount() {
            //return repCommissionAmount.doubleValue() < 0 ? repCommissionAmount.multiply(new BigDecimal(-1)) : repCommissionAmount;
            return repCommissionAmount;
        }

        public void setRepCommissionAmount(BigDecimal repCommissionAmount) {
            this.repCommissionAmount = repCommissionAmount;
        }

        public BigDecimal getSubAgentCommissionAmount() {
            //return subAgentCommissionAmount.doubleValue() < 0 ? subAgentCommissionAmount.multiply(new BigDecimal(-1)) : subAgentCommissionAmount;
            return subAgentCommissionAmount;
        }

        public void setSubAgentCommissionAmount(BigDecimal subAgentCommissionAmount) {
            this.subAgentCommissionAmount = subAgentCommissionAmount;
        }

        public BigDecimal getAgentCommissionAmount() {
            //return agentCommissionAmount.doubleValue() < 0 ? agentCommissionAmount.multiply(new BigDecimal(-1)) : agentCommissionAmount;
            return agentCommissionAmount;
        }

        public void setAgentCommissionAmount(BigDecimal agentCommissionAmount) {
            this.agentCommissionAmount = agentCommissionAmount;
        }

        public BigDecimal getIsoCommissionAmount() {
            //return isoCommissionAmount.doubleValue() < 0 ? isoCommissionAmount.multiply(new BigDecimal(-1)) : isoCommissionAmount;
            return isoCommissionAmount;
        }

        public void setIsoCommissionAmount(BigDecimal isoCommissionAmount) {
            this.isoCommissionAmount = isoCommissionAmount;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getPaymentReferenceNumber() {
            return paymentReferenceNumber  != null ? paymentReferenceNumber : "";
        }

        public void setPaymentReferenceNumber(String paymentReferenceNumber) {
            this.paymentReferenceNumber = paymentReferenceNumber;
        }

        public String getRepName() {
            return repName != null ? repName : "";
        }

        public void setRepName(String repName) {
            this.repName = repName;
        }

        public String getSubAgentName() {
            return subAgentName != null ? subAgentName : "";
        }

        public void setSubAgentName(String subAgentName) {
            this.subAgentName = subAgentName;
        }

        public String getAgentName() {
            return agentName != null ? agentName : "";
        }

        public void setAgentName(String agentName) {
            this.agentName = agentName;
        }

        public BigDecimal getIsoRate() {
            return isoRate;
        }

        public void setIsoRate(BigDecimal isoRate) {
            this.isoRate = isoRate;
        }

        public BigDecimal getAgentRate() {
            return agentRate;
        }

        public void setAgentRate(BigDecimal agentRate) {
            this.agentRate = agentRate;
        }

        public BigDecimal getSubAgentRate() {
            return subAgentRate;
        }

        public void setSubAgentRate(BigDecimal subAgentRate) {
            this.subAgentRate = subAgentRate;
        }

        public BigDecimal getRepRate() {
            return repRate;
        }

        public void setRepRate(BigDecimal repRate) {
            this.repRate = repRate;
        }

        public BigDecimal getMerchantRate() {
            return merchantRate;
        }

        public void setMerchantRate(BigDecimal merchantRate) {
            this.merchantRate = merchantRate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SpiffInfo spiffInfo = (SpiffInfo) o;

            return !(recId != null ? !recId.equals(spiffInfo.recId) : spiffInfo.recId != null);

        }

        @Override
        public int hashCode() {
            return recId != null ? recId.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "SpiffInfo[" +
                    "recId=" + recId +
                    ", merchantId=" + merchantId +
                    ", dba='" + dba + '\'' +
                    ", productName='" + productName + '\'' +
                    ", spiffType='" + spiffType + '\'' +
                    ", commissionPaymentDate=" + commissionPaymentDate +
                    ", activationDate=" + activationDate +
                    ", activationProduct='" + activationProduct + '\'' +
                    ", pin='" + pin + '\'' +
                    ", esn='" + esn + '\'' +
                    ", sim='" + sim + '\'' +
                    ", commissionType='" + commissionType + '\'' +
                    ", isoRate=" + isoRate +
                    ", agentRate=" + agentRate +
                    ", subAgentRate=" + subAgentRate +
                    ", repRate=" + repRate +
                    ", merchantRate=" + merchantRate +
                    ", merchantCommissionAmount=" + merchantCommissionAmount +
                    ", repCommissionAmount=" + repCommissionAmount +
                    ", subAgentCommissionAmount=" + subAgentCommissionAmount +
                    ", agentCommissionAmount=" + agentCommissionAmount +
                    ", isoCommissionAmount=" + isoCommissionAmount +
                    ", paymentType='" + paymentType + '\'' +
                    ", paymentReferenceNumber='" + paymentReferenceNumber + '\'' +
                    ", repName='" + repName + '\'' +
                    ", subAgentName='" + subAgentName + '\'' +
                    ", agentName='" + agentName + '\'' +
                    ']';
        }
    }
}
