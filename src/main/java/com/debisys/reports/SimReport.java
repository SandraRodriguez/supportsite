package com.debisys.reports;

import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.transactions.TransactionSearch;
import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.emida.utils.dbUtils.TorqueHelper;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.*;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Franky Villadiego
 */
public class SimReport {


    private static final Logger log = Logger.getLogger(SimReport.class);

    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sim.sql");

    public String generateCsvInventoryReport(ServletContext context, SessionData sessionData) throws ReportException{

        String strFileName = "";
        String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);
        String downloadPath = DebisysConfigListener.getDownloadPath(context);

        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");

        log.debug("**********************************************");
        log.debug("downloadUrl=" + downloadUrl);
        log.debug("workingDir=" + workingDir);
        log.debug("filePrefix=" + filePrefix);
        log.debug("downloadPath=" + downloadPath);
        log.debug("strAccessLevel=" + strAccessLevel);
        log.debug("downloadUrl=" + strDistChainType);
        log.debug("**********************************************");


        try{

            BufferedWriter outputFile = null;
            strFileName = generateFileName(context);
            log.debug("generatedFileName=" + strFileName);
            outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
            outputFile.flush();
            log.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");

            String headers = getHeaders(sessionData);
            outputFile.write(headers);

            List<SimInventoryReportBean> inventoryReport = getInventoryReport(sessionData);
            
            this.writeValues(inventoryReport, outputFile);
            
            outputFile.flush();
            outputFile.close();

            File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
            long size = sourceFile.length();
            byte[] buffer = new byte[(int) size];
            String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
            log.debug("Zip File Name: " + zipFileName);


            downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
            log.debug("Download URL: " + downloadUrl);

            ZipOutputStream out =null;
            FileInputStream in = null;
            try {
                out = new ZipOutputStream(new FileOutputStream(zipFileName));

                // Set the compression ratio
                out.setLevel(Deflater.DEFAULT_COMPRESSION);
                in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

                // Transfer bytes from the current file to the ZIP file
                // out.write(buffer, 0, in.read(buffer));

                int len;
                while ((len = in.read(buffer)) > 0)
                {
                    out.write(buffer, 0, len);
                }

            }catch (Exception e){

            }finally {
                if(out != null){
                    out.closeEntry();
                }
                if(in != null){
                    in.close();
                }
                if(out != null){
                    out.close();
                }
            }

            sourceFile.delete();



        }catch (Exception e){
            log.error("Error during generateCsvInventoryReport", e);
            throw new ReportException();
        }finally{

        }

        return downloadUrl;
    }

    private List<String> buildValues(List<SimInventoryReportBean> inventoryReport, SessionData sessionData) {

        List<String> result = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        for (SimInventoryReportBean row : inventoryReport) {

            sb.append("\"").append(row.getBatchId()).append("\"").append(",");
            sb.append("\"").append(row.getSimCarrier()).append("\"").append(",");
            sb.append("\"").append(row.getSimType()).append("\"").append(",");
            sb.append("\"").append(row.getBatchFileLoadDate()).append("\"").append(",");
            sb.append("\"").append(row.getStatus()).append("\"").append(",");
            sb.append("\"").append(row.getAssignedToIdStr()).append("\"").append(",");
            sb.append("\"").append(row.getAssignedToDba()).append("\"").append(",");
            sb.append("\"").append(row.getShippedDateStr()).append("\"").append(",");
            sb.append("=\"").append(row.getSimNumber()).append("\"").append(",");
            sb.append("\"").append(row.getBatchSimStatusId()).append("\"").append("\n");

            result.add(sb.toString());
            sb.delete(0, sb.toString().length());
        }

        return result;
    }
    

    private void writeValues(List<SimInventoryReportBean> inventoryReport, BufferedWriter outputFile) throws IOException {
        if (outputFile != null) {
            for (SimInventoryReportBean row : inventoryReport) {
                StringBuilder sb = new StringBuilder();
                sb.append("\"").append(row.getBatchId()).append("\"").append(",");
                sb.append("\"").append(row.getSimCarrier()).append("\"").append(",");
                sb.append("\"").append(row.getSimType()).append("\"").append(",");
                sb.append("\"").append(row.getBatchFileLoadDate()).append("\"").append(",");
                sb.append("\"").append(row.getStatus()).append("\"").append(",");
                sb.append("\"").append(row.getAssignedToIdStr()).append("\"").append(",");
                sb.append("\"").append(row.getAssignedToDba()).append("\"").append(",");
                sb.append("\"").append(row.getShippedDateStr()).append("\"").append(",");
                sb.append("=\"").append(row.getSimNumber()).append("\"").append(",");
                sb.append("\"").append(row.getBatchSimStatusId()).append("\"").append(",");
                sb.append("\"").append(row.getDefaultProductId()).append("\"").append(",");
                sb.append("\"").append(row.getDefaultSiteId()).append("\"").append("\n");
                outputFile.write(sb.toString());
            }
        }
    }

    
    
    
    


    public List<SimInventoryReportBean> getInventoryReport(SessionData sessionData) throws ReportException{

        List<SimInventoryReportBean> result = new ArrayList<SimInventoryReportBean>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try{
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null){
                log.error("Can't get database connection");
                throw new CustomerException();
            }

            StringBuilder sb = new StringBuilder();
            sb.append(sql_bundle.getString("getSimInventory" + sessionData.getLanguage()));
            sb.append(whereClause(sessionData));

            log.debug("QUERY_0:" + sb.toString());

            pstmt = dbConn.prepareStatement(sb.toString());

/*            ResultSetMetaData metaData = pstmt.getMetaData();
            for(int x = 1; x <=10; x++){
                int columnType = metaData.getColumnType(x);
                log.debug("Col" + x + "=" + columnType);
            }*/

            rs = pstmt.executeQuery();
            while (rs.next()){
                SimInventoryReportBean bean = new SimInventoryReportBean();
                bean.setSimId(rs.getString("SimID"));
                bean.setBatchId(rs.getString("BatchID"));
                bean.setSimCarrier(rs.getString("SimCarrier"));
                bean.setSimType(rs.getString("SimType"));
                bean.setBatchFileLoadDate(rs.getDate("BatchFileLoadDate"));
                bean.setStatus(rs.getString("Status"));
                bean.setAssignedToId(rs.getLong("AssignedToId"));
                bean.setAssignedToDba(rs.getString("AssignedToDba"));
                bean.setShippedDate(rs.getDate("ShippedDate"));
                bean.setSimNumber(rs.getString("SimNumber"));
                bean.setBatchSimStatusId(rs.getString("SimStatus"));
                bean.setDefaultProductId((rs.getString("productId") == null || rs.getString("productId").trim().equals("0")) ? "" : rs.getString("productId"));
                bean.setDefaultSiteId((rs.getString("defaultSiteId") == null || rs.getString("defaultSiteId").trim().equals("0")) ? "" : rs.getString("defaultSiteId"));
                result.add(bean);
            }

        }catch (Exception e){
            log.error("Error during getNsfReport", e);
            throw new ReportException();
        }finally{
            try{
                TorqueHelper.closeConnection(dbConn, pstmt, rs);
            }catch (Exception e){
                log.error("Error during closeConnection in getNsfReport", e);
            }
        }

        return result;
    }

    private String whereClause(SessionData sessionData) {

        String strAccessLevel = sessionData.getProperty("access_level");
        String strDistChainType = sessionData.getProperty("dist_chain_type");
        String strRefId = sessionData.getProperty("ref_id");

        StringBuilder sb = new StringBuilder();
        sb.append(" WHERE si.isoId =");
        if(strAccessLevel.equals(DebisysConstants.ISO)){
            sb.append(strRefId);
        }else {
            sb.append("0");
        }

        return sb.toString();
    }

    private String getHeaders(SessionData sessionData){
        StringBuilder sb = new StringBuilder();

        //Headers
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.batchid", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.simCarrier", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.simtype", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.batchFileLoadDate", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.status", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.assignedToID", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.assignedToDBA", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.shippedDate", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.simNumber", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.reports.sim.simStatus", sessionData.getLanguage())).append("\"").append(",");
        
        sb.append("\"").append(Languages.getString("jsp.admin.tools.smsInventory.simProduct", sessionData.getLanguage())).append("\"").append(",");
        sb.append("\"").append(Languages.getString("jsp.admin.tools.smsInventory.defaultSiteId", sessionData.getLanguage())).append("\"").append("\n");

        return sb.toString();

    }


    public String generateFileName(ServletContext context){
        boolean isUnique = false;
        String strFileName = "";
        while (!isUnique){
            strFileName = generateRandomNumber();
            isUnique = checkFileName(strFileName, context);
        }
        return strFileName;
    }

    private String generateRandomNumber(){
        StringBuffer s = new StringBuffer();
        // number between 1-9 because first digit must not be 0
        int nextInt = (int) ((Math.random() * 9) + 1);
        s = s.append(nextInt);
        for (int i = 0; i <= 10; i++) {
            // number between 0-9
            nextInt = (int) (Math.random() * 10);
            s = s.append(nextInt);
        }

        return s.toString();
    }

    private boolean checkFileName(String strFileName, ServletContext context){
        boolean isUnique = true;
        String downloadPath = DebisysConfigListener.getDownloadPath(context);
        String workingDir = DebisysConfigListener.getWorkingDir(context);
        String filePrefix = DebisysConfigListener.getFilePrefix(context);

        File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
        if (f.exists()){
            // duplicate file found
            log.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
            return false;
        }

        f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
        if (f.exists()){
            // duplicate file found
            log.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
            return false;
        }
        return isUnique;
    }


    public class SimInventoryReportBean {

        private String simId;
        private String batchId;
        private String simCarrier;
        private String simType;
        private Date batchFileLoadDate;
        private String status;
        private Long assignedToId;
        private String assignedToDba;
        private Date shippedDate;
        private String simNumber;
        private String batchSimStatusId;
        private String defaultProductId;
        private String defaultSiteId;


        public String getSimId() {
            return simId;
        }

        public void setSimId(String simId) {
            this.simId = simId;
        }

        public String getBatchId() {
            return batchId;
        }

        public void setBatchId(String batchId) {
            this.batchId = batchId;
        }

        public String getSimCarrier() {
            return simCarrier;
        }

        public void setSimCarrier(String simCarrier) {
            this.simCarrier = simCarrier;
        }

        public String getSimType() {
            return simType;
        }

        public void setSimType(String simType) {
            this.simType = simType;
        }

        public Date getBatchFileLoadDate() {
            return batchFileLoadDate;
        }

        public void setBatchFileLoadDate(Date batchFileLoadDate) {
            this.batchFileLoadDate = batchFileLoadDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAssignedToIdStr() {
            return assignedToId != null && assignedToId != 0 ? assignedToId.toString() : "N/A";
        }

        public void setAssignedToId(Long assignedToId) {
            this.assignedToId = assignedToId;
        }

        public String getAssignedToDba() {
            return assignedToDba;
        }

        public void setAssignedToDba(String assignedToDba) {
            this.assignedToDba = assignedToDba;
        }

        public String getShippedDateStr() {
            return shippedDate != null ? shippedDate.toString() : "N/A";
        }

        public void setShippedDate(Date shippedDate) {
            this.shippedDate = shippedDate;
        }

        public String getSimNumber() {
            return simNumber;
        }

        public void setSimNumber(String simNumber) {
            this.simNumber = simNumber;
        }

        public String getBatchSimStatusId() {
            return batchSimStatusId;
        }

        public void setBatchSimStatusId(String batchSimStatusId) {
            this.batchSimStatusId = batchSimStatusId;
        }

        public String getDefaultProductId() {
            return defaultProductId;
        }

        public void setDefaultProductId(String defaultProductId) {
            this.defaultProductId = defaultProductId;
        }

        public String getDefaultSiteId() {
            return defaultSiteId;
        }

        public void setDefaultSiteId(String defaultSiteId) {
            this.defaultSiteId = defaultSiteId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SimInventoryReportBean that = (SimInventoryReportBean) o;

            return !(simId != null ? !simId.equals(that.simId) : that.simId != null);

        }

        @Override
        public int hashCode() {
            return simId != null ? simId.hashCode() : 0;
        }
    }

}
