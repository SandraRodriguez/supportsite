/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports;

import com.debisys.exceptions.CustomerException;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;

/**
 *
 * @author Emida
 */
public class PropertiesByFeature {

    public static final String FEATURE_MERCHANT_MAP_LOCATOR = "MerchantMapLocator";
    public static final String FEATURE_VERIZONE_ACTIVATIONS = "VerizoneActivations";
    public static final String FEATURE_TERMINAL_PERMISSIONS_TOOL = "TerminalPermissionsTool";
    public static final String FEATURE_DOCUMENT_FLOW = "DocumentManagementFlow";
    
    
    private String idNew;
    private String reportId;
    private String property;
    private String value;
    private boolean enabled;
    
    
    private static Logger cat = Logger.getLogger(PropertiesByFeature.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
    

    /**
     * 
     * @param reportCode
     * @return
     * @throws CustomerException 
     */
    public static ArrayList<PropertiesByFeature> findPropertiesByFeature(String reportCode) throws CustomerException
    {
        ArrayList<PropertiesByFeature> propertiesReport = new  ArrayList<PropertiesByFeature>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "SELECT pr.* FROM propertiesbyfeature pr WITH(NOLOCK) " 
                        + "INNER JOIN reportConfiguration rc WITH(NOLOCK) ON pr.ReportId = rc.reportId " 
                        + "WHERE rc.reportCode='"+reportCode+"' AND pr.Enabled=1";
            
            cat.debug("findPropertiesByReport =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                PropertiesByFeature property = new PropertiesByFeature();
                property.setProperty(rs.getString("Property"));
                property.setValue(rs.getString("Value"));
                propertiesReport.add(property);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findPropertiesByFeature ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findPropertiesByFeature Error during closeConnection", e);
            }
        }
        return propertiesReport;
    }
    
    /**
     * 
     * @param reportCode
     * @return
     * @throws CustomerException 
     */
    public static HashMap<String, PropertiesByFeature> findPropertiesByFeatureHash(String reportCode) throws CustomerException
    {
        HashMap<String, PropertiesByFeature> propertiesReport = new  HashMap<String, PropertiesByFeature>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "SELECT pr.* FROM propertiesbyfeature pr WITH(NOLOCK) " 
                        + "INNER JOIN reportConfiguration rc WITH(NOLOCK) ON pr.ReportId = rc.reportId " 
                        + "WHERE rc.reportCode='"+reportCode+"' AND pr.Enabled=1";
            
            cat.debug("findPropertiesByReport =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                PropertiesByFeature property = new PropertiesByFeature();
                property.setProperty(rs.getString("Property"));
                property.setValue(rs.getString("Value"));
                propertiesReport.put(property.getProperty(), property);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findPropertiesByFeatureHash ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findPropertiesByFeatureHash Error during closeConnection", e);
            }
        }
        return propertiesReport;
    }
        
    /**
     * 
     * @param reportCode
     * @param propertyName
     * @return
     * @throws CustomerException 
     */
    public static HashMap<String, PropertiesByFeature> findPropertyByCode(String reportCode, String propertyName) throws CustomerException
    {
        HashMap<String, PropertiesByFeature> propertiesReport = new  HashMap<String, PropertiesByFeature>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "SELECT pr.* FROM propertiesbyfeature pr WITH(NOLOCK) " 
                        + "INNER JOIN reportConfiguration rc WITH(NOLOCK) ON pr.ReportId = rc.reportId " 
                        + "WHERE rc.reportCode='"+reportCode+"' AND pr.Enabled=1 And pr.Property = '"+propertyName+"'" ;
            
            cat.debug("findPropertyByCode =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                PropertiesByFeature property = new PropertiesByFeature();
                property.setProperty(rs.getString("Property"));
                property.setValue(rs.getString("Value"));
                propertiesReport.put(property.getProperty(), property);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findPropertyByCode ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findPropertyByCode Error during closeConnection", e);
            }
        }
        return propertiesReport;
    }
    
    /**
     * @return the idNew
     */
    public String getIdNew() {
        return idNew;
    }

    /**
     * @param idNew the idNew to set
     */
    public void setIdNew(String idNew) {
        this.idNew = idNew;
    }

    /**
     * @return the reportId
     */
    public String getReportId() {
        return reportId;
    }

    /**
     * @param reportId the reportId to set
     */
    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    /**
     * @return the property
     */
    public String getProperty() {
        return property;
    }

    /**
     * @param property the property to set
     */
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
}