package com.debisys.reports;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.customers.Merchant;
import com.debisys.dateformat.DateFormat;
import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.debisys.terminals.Terminal;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * Holds the information for transactions.<P>
 *
 * @author Jay Chi
 */

public class ClerkReport implements HttpSessionBindingListener
{

  private String startDate = "";
  private String startDateTime = "";
  private String startHour = "";
  private String startMin = "";
  private String startAmPm = "";

  private String endDate = "";
  private String endDateTime = "";
  private String endHour = "";
  private String endMin = "";
  private String endAmPm = "";


  private String siteId = "";
  private String clerkId = "";
  private String userId = "";
  private String password = "";
  private String bulkId = "";

  private String groupBy = "";
  private String paymentType = "";


  private Hashtable validationErrors = new Hashtable();
  private String sort = "";
  private String col = "";

  //log4j logging
  static Category cat = Category.getInstance(ClerkReport.class);
  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

  /***
   * ADDED FOR LOCALIZATION
   * @return
   */
  public String getStartDateFormatted()
  {
      return getDateFormatted(startDateTime);
  }
  
  /***
   * ADDED FOR LOCALIZATION
   * @return
   */
  public String getEndDateFormatted()
  {
      return getDateFormatted(endDateTime);
  }
  
  /***
   * ADDED FOR LOCALIZATION
   * @param dateToFormat
   * @return
   */
    private String getDateFormatted(String dateToFormat)
    {
        String date = null;
        try
        {
            // Current date format from the clerk_report page is in MM/dd/yyyy hh:mm a.
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
            java.util.Date dd = sdf.parse(dateToFormat);

            // We will specify a new date format through the database
            SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
            date = sdf1.format(dd); 
        } 
        catch (ParseException e)
        {
            cat.error("Error during ClerkReport date localization ", e);
        }      
        return date;
    }  
  
  public String getUserId()
  {
    return userId;
  }

  public void setUserId(String userId)
  {
    this.userId = userId;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public String getStartDateTime()
  {
    return startDateTime;
  }

  public void setStartDateTime(String startDateTime)
  {
    this.startDateTime = startDateTime;
  }

  public String getStartHour()
  {
    return startHour;
  }

  public void setStartHour(String startHour)
  {
    this.startHour = startHour;
  }

  public String getStartMin()
  {
    return startMin;
  }

  public void setStartMin(String startMin)
  {
    this.startMin = startMin;
  }

  public String getStartAmPm()
  {
    return startAmPm;
  }

  public void setStartAmPm(String startAmPm)
  {
    this.startAmPm = startAmPm;
  }

  public String getEndDateTime()
  {
    return endDateTime;
  }

  public void setEndDateTime(String endDateTime)
  {
    this.endDateTime = endDateTime;
  }

  public String getEndHour()
  {
    return endHour;
  }

  public void setEndHour(String endHour)
  {
    this.endHour = endHour;
  }

  public String getEndMin()
  {
    return endMin;
  }

  public void setEndMin(String endMin)
  {
    this.endMin = endMin;
  }

  public String getEndAmPm()
  {
    return endAmPm;
  }

  public void setEndAmPm(String endAmPm)
  {
    this.endAmPm = endAmPm;
  }

  public String getSiteId()
  {
    return siteId;
  }

  public void setSiteId(String siteId)
  {
    this.siteId = siteId;
  }

  public String getClerkId()
  {
    return clerkId;
  }

  public void setClerkId(String clerkId)
  {
    this.clerkId = clerkId;
  }

  public String getStartDate()
  {
    return StringUtil.toString(startDate);
  }

  public void setStartDate(String strValue)
  {
    startDate = strValue;
  }

  public String getEndDate()
  {
    return StringUtil.toString(endDate);
  }

  public void setEndDate(String strValue)
  {
    endDate = strValue;
  }

  public String getCol()
  {
    return StringUtil.toString(col);
  }

  public void setCol(String strCol)
  {
    col = strCol;
  }

  public String getSort()
  {
    return StringUtil.toString(sort);
  }

  public void setSort(String strSort)
  {
    sort = strSort;
  }

  public String getGroupBy()
  {
    return StringUtil.toString(groupBy);
  }

  public void setGroupBy(String groupBy)
  {
    this.groupBy = groupBy;
  }
  public String getPaymentType()
  {
    return StringUtil.toString(paymentType);
  }

  public void setPaymentType(String paymentType)
  {
    this.paymentType = paymentType;
  }

  public String getBulkId() {
	return bulkId;
}

public void setBulkId(String bulkId) {
	this.bulkId = bulkId;
}

public Vector getClerkSummary(SessionData sessionData)
      throws ReportException
  {
    String strAccessLevel = sessionData.getProperty("access_level");
    Connection dbConn = null;
    Vector vecClerkSummary = new Vector();
    String strSQL = "";

    if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK)
        || strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK_MANAGER))
    {
      this.siteId = sessionData.getProperty("siteId");
      if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK))
      {
        this.clerkId = sessionData.getProperty("clerkId");
      }
    }
    else
    {
      //validate permissions
      Terminal terminal = new Terminal();
      String merchantId = terminal.getMerchantId(siteId);

      //check to make sure this user has
      // permission to access this resource.
      Merchant merchant = new Merchant();
      merchant.setMerchantId(merchantId);
      if (merchantId.equals(""))
      {
        return null;
      }
      else if (!merchant.checkPermission(sessionData))
      {
        //this means merchant/terminal does
        // not belong to logged in user
        return null;
      }
    }


    if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK))
    {
      if (this.groupBy != null && this.groupBy.equalsIgnoreCase("payment"))
      {
        strSQL = sql_bundle.getString("getClerkSummaryByPaymentType");
      }
      else
      {
        strSQL = sql_bundle.getString("getClerkSummaryByClerk");
      }
    }
    else if (strAccessLevel != null && !strAccessLevel.equals(""))
    {
      if (this.groupBy != null && this.groupBy.equalsIgnoreCase("payment"))
      {
        strSQL = sql_bundle.getString("getClerkManagerSummaryByPaymentType");
      }
      else
      {
        strSQL = sql_bundle.getString("getClerkManagerSummaryByClerk");
      }
    }

    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new ReportException();
      }

      cat.debug(strSQL);
      PreparedStatement pstmt = null;
      pstmt = dbConn.prepareStatement(strSQL);
      if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK))
      {
        pstmt.setString(1, this.clerkId);
        pstmt.setDouble(2, Double.parseDouble(this.siteId));
        pstmt.setString(3, startDateTime);
        pstmt.setString(4, endDateTime);
      }
      else if (strAccessLevel != null && !strAccessLevel.equals(""))
      {
        pstmt.setDouble(1, Double.parseDouble(this.siteId));
        pstmt.setString(2, startDateTime);
        pstmt.setString(3, endDateTime);
      }
      ResultSet rs = pstmt.executeQuery();

      while (rs.next())
      {
        Vector vecTemp = new Vector();
        if (this.groupBy != null && this.groupBy.equalsIgnoreCase("payment"))
        {
          /*
            0 Not specified
            1 Cash
            2 Check
            3 Credit Card
            4 ATM Card
          */
         if (StringUtil.toString(rs.getString("payment_type")).equals(""))
         {
          vecTemp.add("null");
         }
          else
         {
          vecTemp.add(StringUtil.toString(rs.getString("payment_type")));
         }
         vecTemp.add(this.convertPaymentType(StringUtil.toString(rs.getString("payment_type"))));

        }
        else
        {
          vecTemp.add(StringUtil.toString(rs.getString("clerk")));
          vecTemp.add(StringUtil.toString(rs.getString("name")));

        }
        vecTemp.add(rs.getString("qty"));
        vecTemp.add(NumberUtil.formatAmount(rs.getString("total")));
        vecClerkSummary.add(vecTemp);
      }
      pstmt.close();
      rs.close();


    }
    catch (Exception e)
    {
      cat.error("Error during getClerkSummary", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    return vecClerkSummary;
  }
  public Vector getMerchantInfo(SessionData sessionData)
  throws ReportException
  {
	  Connection dbConn = null;
	  Vector vecTransactionReport = new Vector();

	  //validate permissions
	  Terminal terminal = new Terminal();
	  String merchantId = terminal.getMerchantId(siteId);
	  try
	  {
		  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		  if (dbConn == null)
		  {
			  cat.error("Can't get database connection");
			  throw new ReportException();
		  }
		  String strSQL = "SELECT m.dba, m.phys_address, m.phys_city, m.phys_state, m.phys_zip FROM merchants AS m WITH (NOLOCK) WHERE m.merchant_id = ?";
		    
		  cat.debug(strSQL + "/* " + merchantId + " */");
		  PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
		  pstmt.setLong(1, Long.parseLong(merchantId));
	      ResultSet rs = pstmt.executeQuery();
	      if (rs.next())
	      {
			  Vector vecTemp = new Vector();
			  vecTemp.add(StringUtil.toString(rs.getString("dba")));
			  vecTemp.add(StringUtil.toString(rs.getString("phys_address")));
	    	  vecTemp.add(StringUtil.toString(rs.getString("phys_city")));	
	    	  vecTemp.add(StringUtil.toString(rs.getString("phys_state")));	    	  
	    	  vecTemp.add(StringUtil.toString(rs.getString("phys_zip")));
			  vecTransactionReport.add(vecTemp);
		  }
		  pstmt.close();
		  rs.close();

	  }
	  catch (Exception e)
	  {
		  cat.error("Error during getClerkSummary", e);
		  throw new ReportException();
	  }
	  finally
	  {
		  try
		  {
			  Torque.closeConnection(dbConn);
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during closeConnection", e);
		  }
	  }
	  return vecTransactionReport;	  
  }
  public Vector getClerkDetailTransactionReport(SessionData sessionData)
  throws ReportException
  {
	  String strAccessLevel = sessionData.getProperty("access_level");
	  Connection dbConn = null;
	  Vector vecTransactionReport = new Vector();

	  if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK)
			  || strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK_MANAGER))
	  {
		  this.siteId = sessionData.getProperty("siteId");
		  if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK))
		  {
			  this.clerkId = sessionData.getProperty("clerkId");
		  }
	  }
	  else
	  {
		  //validate permissions
		  Terminal terminal = new Terminal();
		  String merchantId = terminal.getMerchantId(siteId);

		  //check to make sure this user has
		  // permission to access this resource.
		  Merchant merchant = new Merchant();
		  merchant.setMerchantId(merchantId);
		  if (merchantId.equals(""))
		  {
			  return null;
		  }
		  else if (!merchant.checkPermission(sessionData))
		  {
			  //this means merchant/terminal does
			  // not belong to logged in user
			  return null;
		  }
	  }

	  try
	  {
		  dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
		  if (dbConn == null)
		  {
			  cat.error("Can't get database connection");
			  throw new ReportException();
		  }
		  String strSQL = "SELECT wt.datetime, "
			  			+ " wt.clerk, "
		    			+ " wt.clerk_name, "
		    			+ " wt.description, "
		    			+ " wt.amount, "
		    			+ " (wt.merchant_rate / 100 * (CASE p.ach_type WHEN 6 THEN ISNULL(wt.Fixed_trans_fee,0) + ISNULL(wt.transFixedFeeAmt,0) ELSE wt.amount END)) as merchant_commission "
		    			+ " FROM web_transactions AS wt WITH (NOLOCK) INNER JOIN products p ON p.id = wt.id ";
		  String strSQLWhere = " WHERE wt.clerk = '" + this.clerkId + "'"  
		    				 + " AND wt.millennium_no = " + this.siteId
		    				 + " AND wt.datetime >= '" + this.startDateTime + "'"
		    				 + " AND wt.datetime <= '" + this.endDateTime + "'"
		    				 + " ORDER BY wt.datetime";
		    
		  String strSQLTotal = strSQL + strSQLWhere;
		  cat.debug(strSQLTotal);
		  PreparedStatement pstmt = dbConn.prepareStatement(strSQLTotal);
	      ResultSet rs = pstmt.executeQuery();
		  while (rs.next())
		  {
			  Vector vecTemp = new Vector();
			  vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
	    	  if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals(""))
	    	  {	
	    		  vecTemp.add(StringUtil.toString(rs.getString("clerk_name")));
	    	  }
	    	  else
	    	  {
	    		  vecTemp.add(StringUtil.toString(rs.getString("clerk")));
	    	  }		
	    	  vecTemp.add(StringUtil.toString(rs.getString("description")));	    	  
			  vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
			  vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
			  vecTemp.add(NumberUtil.formatAmount(rs.getString("amount")));
			  vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
			  vecTransactionReport.add(vecTemp);
		  }
		  pstmt.close();
		  rs.close();

	  }
	  catch (Exception e)
	  {
		  cat.error("Error during getClerkSummary", e);
		  throw new ReportException();
	  }
	  finally
	  {
		  try
		  {
			  Torque.closeConnection(dbConn);
		  }
		  catch (Exception e)
		  {
			  cat.error("Error during closeConnection", e);
		  }
	  }
	  return vecTransactionReport;
  }

  
  public Vector<Vector<String>> getClerkDetail(SessionData sessionData)
      throws ReportException
  {
    String strAccessLevel = sessionData.getProperty("access_level");
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    Vector<Vector<String>> vecClerkDetail = new Vector<Vector<String>>();
    String strSQL = "";
    String paymentTypeInd = sessionData.getProperty("paymentTypeInd");


    if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK)
        || strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK_MANAGER))
    {
      this.siteId = sessionData.getProperty("siteId");
      if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK))
      {
        this.clerkId = sessionData.getProperty("clerkId");
      }

    }
    else
    {
      //validate permissions
      Terminal terminal = new Terminal();
      String merchantId = terminal.getMerchantId(siteId);

      //check to make sure this user has
      // permission to access this resource.
      Merchant merchant = new Merchant();
      merchant.setMerchantId(merchantId);
      if (merchantId.equals(""))
      {
        return null;
      }
      else if (!merchant.checkPermission(sessionData))
      {
        //this means merchant/terminal does
        // not belong to logged in user
        return null;
      }
    }


    try
    {
      if (this.getGroupBy().equals("payment"))
      {
            strSQL = sql_bundle.getString("getClerkDetailByPaymentType");
            if (this.getPaymentType().equals("null"))
            {
              strSQL = strSQL + " (wt.payment_type IS NULL) ";
            }
             else
            {
              strSQL = strSQL + " (wt.payment_type=?) ";
            }
            if (strAccessLevel.equalsIgnoreCase(DebisysConstants.CLERK))
            {
              strSQL = strSQL + " AND wt.clerk='" + this.clerkId + "' ";
            }
      }
      else
      {
        strSQL = sql_bundle.getString("getClerkDetailByClerk");
      }

      dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new ReportException();
      }

      strSQL = strSQL + " ORDER BY wt.datetime ";
      cat.debug(strSQL);
      
      pstmt = dbConn.prepareStatement(strSQL);
      pstmt.setDouble(1, Double.parseDouble(this.siteId));
      pstmt.setString(2, startDateTime);
      pstmt.setString(3, endDateTime);
      if (this.getGroupBy().equals("payment"))
      {
        if (!this.getPaymentType().equals("null"))
        {
          pstmt.setString(4, this.getPaymentType());
        }
      }
      else
      {
        pstmt.setString(4, this.clerkId);
      }

      rs = pstmt.executeQuery();
      while (rs.next())
      {
        Vector<String> vecTemp = new Vector<String>();
        vecTemp.add(0, StringUtil.toString(rs.getString("rec_id")));
        vecTemp.add(1, StringUtil.toString(rs.getString("millennium_no")));
        vecTemp.add(2, DateUtil.formatDateTime(rs.getTimestamp("datetime")));
        vecTemp.add(3, StringUtil.toString(rs.getString("clerk")));
        vecTemp.add(4, StringUtil.toString(rs.getString("clerk_name")));
        vecTemp.add(5, StringUtil.toString(rs.getString("ani")));
        vecTemp.add(6, NumberUtil.formatCurrency(rs.getString("amount")));
        vecTemp.add(7, StringUtil.toString(rs.getString("description")));
        if (paymentTypeInd.equalsIgnoreCase("true"))
        {
          vecTemp.add(8, this.convertPaymentType(StringUtil.toString(rs.getString("payment_type"))));
        }
        vecClerkDetail.add(vecTemp);
      }
    }
    catch (Exception e)
    {
      cat.error("Error during getClerkDetail", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    return vecClerkDetail;
  }


  /**
   * Validates the clerk login for missing or invalid fields.
   *
   * @return <code>true</code> if the entity passes the validation
   *         checks,  otherwise  <code>false</code>
   */

  public boolean validateClerk() throws ReportException
  {
    validationErrors.clear();
    boolean valid = true;
    if ((siteId == null) ||
        (siteId.length() == 0))
    {
      addFieldError("siteId", "Please enter a site id.");
      valid = false;
    }

    if ((clerkId == null) ||
        (clerkId.length() == 0))
    {
      addFieldError("clerkId", "Please enter a clerk id.");
      valid = false;
    }

    if (valid)
    {
      Connection dbConn = null;

      try
      {
        dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
        if (dbConn == null)
        {
          cat.error("Can't get database connection");
          throw new ReportException();
        }
        PreparedStatement pstmt =
            dbConn.prepareStatement(sql_bundle.getString("validateClerkId"));
        pstmt.setString(1, this.siteId);
        pstmt.setString(2, this.clerkId);
        ResultSet rs = pstmt.executeQuery();
        cat.debug(pstmt.toString());
        if (rs.next())
        {
          valid = true;
        }
        else
        {
          addFieldError("clerkId", "Invalid clerk id, please try again.");
          valid = false;
        }
        rs.close();
        pstmt.close();
      }
      catch (Exception e)
      {
        cat.error("Error during validateClerk", e);
        throw new ReportException();
      }
      finally
      {
        try
        {
          Torque.closeConnection(dbConn);
        }
        catch (Exception e)
        {
          cat.error("Error during closeConnection", e);
        }
      }
    }

    return valid;
  }

  public boolean validateClerkManager() throws ReportException
  {
    validationErrors.clear();
    boolean valid = true;
    if ((userId == null) ||
        (userId.length() == 0))
    {
      addFieldError("userId", "Please enter a user id.");
      valid = false;
    }

    if ((password == null) ||
        (password.length() == 0))
    {
      addFieldError("password", "Please enter a password.");
      valid = false;
    }

    if (valid)
    {
      Connection dbConn = null;

      try
      {
        dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
        if (dbConn == null)
        {
          cat.error("Can't get database connection");
          throw new ReportException();
        }
        PreparedStatement pstmt =
            dbConn.prepareStatement(sql_bundle.getString("validateClerkManager"));
        pstmt.setString(1, this.userId);
        pstmt.setString(2, this.password);
        ResultSet rs = pstmt.executeQuery();
        cat.debug(pstmt.toString());
        if (rs.next())
        {
          this.siteId = rs.getString("millennium_no");
          valid = true;
        }
        else
        {
          addFieldError("login", "Invalid user id or password, please try again.");
          valid = false;
        }
        rs.close();
        pstmt.close();
      }
      catch (Exception e)
      {
        cat.error("Error during validateClerkManager", e);
        throw new ReportException();
      }
      finally
      {
        try
        {
          Torque.closeConnection(dbConn);
        }
        catch (Exception e)
        {
          cat.error("Error during closeConnection", e);
        }
      }
    }

    return valid;
  }

  /**
   * Validates for missing or invalid fields.
   *
   * @return <code>true</code> if the entity passes the validation
   *         checks,  otherwise  <code>false</code>
   */

  public boolean validate(SessionData sessionData)
  {
    validationErrors.clear();
    boolean valid = true;

    if ((startDate == null) ||
        (startDate.length() == 0))
    {
      addFieldError("startdate", Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()));
      valid = false;
    }

    if ((endDate == null) ||
        (endDate.length() == 0))
    {
      addFieldError("enddate", Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()));
      valid = false;
    }

    if (valid)
    {
      startDateTime = startDate + " " + startHour + ":" + startMin + " " + startAmPm;
      endDateTime = endDate + " " + endHour + ":" + endMin + " " + endAmPm;
      Date dtStartDate = new Date();
      Date dtEndDate = new Date();
      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy h:mm a");
      formatter.setLenient(false);
      try
      {
        dtStartDate = /*(Date) */formatter.parse(startDateTime);
      }
      catch (ParseException ex)
      {
        addFieldError("startDate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
        valid = false;
      }
      try
      {
        dtEndDate = /*(Date) */formatter.parse(endDateTime);
      }
      catch (ParseException ex)
      {
        addFieldError("endDate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
        valid = false;
      }

      if (dtStartDate.after(dtEndDate))
      {
        addFieldError("date", "Start date must be before end date.");
        valid = false;
      }

    }

    return valid;
  }

  /**
   * Sets the validation error against a specific field.  Used by
   * {@link #validateClerk validateClerk}.
   *
   * @param fieldname The bean property name of the field
   * @param error     The error message for the field or null if none is
   *                  present.
   */

  public void addFieldError(String fieldname, String error)
  {
    validationErrors.put(fieldname, error);
  }

  /**
   * Returns a hash of errors.
   *
   * @return Hash of errors.
   */

  public Hashtable getErrors()
  {
    return validationErrors;
  }

  private String convertPaymentType(String tmpPaymentType)
  {

    if (tmpPaymentType.equals(""))
    {
      return "Not Defined";
    }
    else if (tmpPaymentType.equals("0"))
    {
      return "Not Specified";
    }
    else if(tmpPaymentType.equals("1"))
    {
      return "Cash";
    }
    else if(tmpPaymentType.equals("2"))
    {
      return "Check";
    }
    else if(tmpPaymentType.equals("3"))
    {
      return "Credit Card";
    }
    else if(tmpPaymentType.equals("4"))
    {
      return "ATM Card";
    }
    else
    {
      return"Unknown";
    }

  }
  
	public Vector getBulkTotal(SessionData sessionData) throws ReportException {
		Connection dbConn = null;
		Vector vecBulk = new Vector();

		try {
			dbConn = Torque.getConnection(sql_bundle
					.getString("pkgAlternateDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			String strSQL = "select bl.BulkMasterId, st.BulkStatudName, bl.BulkMasterDate "
					+ "from bulkMaster as bl "
					+ "inner join BulkStatus as [st] on bl.BulkMasterStatusId = st.BulkStatusId ";

			String sqlWhere = "where bl.BulkMasterClerkId = '"
					+ this.getClerkId() + "'  ";

			if (!this.getBulkId().equals("")) {
				sqlWhere += "and bl.BulkMasterId  =  " + this.getBulkId() + " ";
			}

			if (!this.getStartDate().equals("") && !this.getEndDate().equals("")) {
				sqlWhere += "and bl.BulkMasterDate >= '" + this.getStartDate() + "' ";
				sqlWhere += "and bl.BulkMasterDate <= '" + this.getEndDate() + " 11:59:59 PM' ";
			}

			String strSQLTotal = strSQL + sqlWhere;
			cat.debug(strSQLTotal);
			PreparedStatement pstmt = dbConn.prepareStatement(strSQLTotal);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString(1)));
				vecTemp.add(StringUtil.toString(rs.getString(2)));
                                
                                // Localization - SW
                                // Send date as formatted
				//vecTemp.add(StringUtil.toString(rs.getString(3)));
				vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp(3)));
                                // End Localization
                                
				vecBulk.add(vecTemp);
			}

			pstmt.close();
			rs.close();

		} catch (Exception e) {
			cat.error("Error during getBulkTotal", e);
			throw new ReportException();
		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecBulk;
	}
	
	public Vector getBulkDetail(SessionData sessionData) throws ReportException {
		Connection dbConn = null;
		Vector vecBulk = new Vector();

		try {
			dbConn = Torque.getConnection(sql_bundle
					.getString("pkgAlternateDb"));
			if (dbConn == null) {
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			String strSQL = "select det.BulkDetailAccount, det.BulkDetailAmount, det.BulkDetailTransactionId, det.BulkDetailDate, det.BulkDetailReference, st.BulkDetailStatusName  "
					+ "from BulkDetail det " 
					+	"inner join BulkDetailStatus st on st.BulkDetailStatusId = det.BulkDetailStatusId "
					+ "where det.BulkDetailBulkMasterId = " + this.getBulkId() + " ";

			cat.debug(strSQL);
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString(1)));
				vecTemp.add(StringUtil.toString(rs.getString(2)));
				vecTemp.add(StringUtil.toString(rs.getString(3)));
				vecTemp.add(StringUtil.toString(rs.getString(4)));
				vecTemp.add(StringUtil.toString(rs.getString(5)));
				vecTemp.add(StringUtil.toString(rs.getString(6)));
				vecBulk.add(vecTemp);
			}

			pstmt.close();
			rs.close();

		} catch (Exception e) {
			cat.error("Error during getBulkDetail", e);
			throw new ReportException();
		} finally {
			try {
				Torque.closeConnection(dbConn);
			} catch (Exception e) {
				cat.error("Error during closeConnection", e);
			}
		}
		return vecBulk;
	}

  public void valueBound(HttpSessionBindingEvent event)
  {
  }

  public void valueUnbound(HttpSessionBindingEvent event)
  {
  }

  public static void main(String[] args)
  {
  }


}

