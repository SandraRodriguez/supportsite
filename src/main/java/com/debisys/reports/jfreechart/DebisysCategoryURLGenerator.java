package com.debisys.reports.jfreechart;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.ObjectUtils;
import org.jfree.chart.urls.CategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;

/**
 * NOT YET IMPLEMENTED, FOR FUTURE USE.
 * Overridden class for generating urls for charts
 *
 * @author Jay Chi
 */

public class DebisysCategoryURLGenerator implements CategoryURLGenerator,
                                                     Cloneable, Serializable {

    /** Prefix to the URL */
    private String prefix = "index.html";

    /** Series parameter name to go in each URL */
    private String seriesParameterName = "series";

    /** Category parameter name to go in each URL */
    private String categoryParameterName = "category";

    /**
     * Creates a new generator with default settings.
     */
    public DebisysCategoryURLGenerator() {
        super();
    }

    /**
     * Constructor that overrides default prefix to the URL.
     *
     * @param prefix  the prefix to the URL (<code>null</code> not permitted).
     */
    public DebisysCategoryURLGenerator(String prefix) {
        if (prefix == null) {
            throw new IllegalArgumentException("Null 'prefix' argument.");
        }
        this.prefix = prefix;
    }

    /**
     * Constructor that overrides all the defaults.
     *
     * @param prefix  the prefix to the URL (<code>null</code> not permitted).
     * @param seriesParameterName  the name of the series parameter to go in
     *                             each URL (<code>null</code> not permitted).
     * @param categoryParameterName  the name of the category parameter to go in
     *                               each URL (<code>null</code> not permitted).
     */
    public DebisysCategoryURLGenerator(String prefix,
                                        String seriesParameterName,
                                        String categoryParameterName) {

        if (prefix == null) {
            throw new IllegalArgumentException("Null 'prefix' argument.");
        }
        if (seriesParameterName == null) {
            throw new IllegalArgumentException("Null 'seriesParameterName' argument.");
        }
        if (categoryParameterName == null) {
            throw new IllegalArgumentException("Null 'categoryParameterName' argument.");
        }
        this.prefix = prefix;
        this.seriesParameterName = seriesParameterName;
        this.categoryParameterName = categoryParameterName;

    }

    /**
     * Generates a URL for a particular item within a series.
     *
     * @param dataset  the dataset.
     * @param series  the series index (zero-based).
     * @param category  the category index (zero-based).
     *
     * @return The generated URL.
     */
    public String generateURL(CategoryDataset dataset, int series, int category) {
        String url = this.prefix;
        Comparable seriesKey = dataset.getRowKey(series);
        Comparable categoryKey = dataset.getColumnKey(category);
        boolean firstParameter = url.indexOf("?") == -1;
        url += firstParameter ? "?" : "&";
       try {
            url += this.seriesParameterName + "="
                + URLEncoder.encode(seriesKey.toString(), "UTF-8");
        }
        catch (UnsupportedEncodingException uee) {
            url += this.seriesParameterName + "=" + seriesKey.toString();
        }
        try {
            url += "&" + this.categoryParameterName + "="
                + URLEncoder.encode(categoryKey.toString(), "UTF-8");
        }
        catch (UnsupportedEncodingException uee) {
            url += "&" + this.categoryParameterName + "=" + categoryKey.toString();
        }
        return url;
    }

    /**
     * Returns an independent copy of the URL generator.
     *
     * @return A clone.
     *
     * @throws CloneNotSupportedException not thrown by this class, but subclasses (if any) might.
     */
    public Object clone() throws CloneNotSupportedException {

        // all attributes are immutable, so we can just return the super.clone()
        return super.clone();

    }

    /**
     * Tests the generator for equality with an arbitrary object.
     *
     * @param obj  the object (<code>null</code> permitted).
     *
     * @return A boolean.
     */
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof DebisysCategoryURLGenerator)) {
            return false;
        }
        DebisysCategoryURLGenerator generator = (DebisysCategoryURLGenerator) obj;
        if (!ObjectUtils.equals(this.prefix, generator.prefix)) {
            return false;
        }

        if (!ObjectUtils.equals(this.seriesParameterName, generator.seriesParameterName)) {
            return false;
        }
        if (!ObjectUtils.equals(this.categoryParameterName, generator.categoryParameterName)) {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code.
     *
     * @return A hash code.
     */
    public int hashCode() {
        int result;
        result = (this.prefix != null ? this.prefix.hashCode() : 0);
        result = 29 * result
            + (this.seriesParameterName != null ? this.seriesParameterName.hashCode() : 0);
        result = 29 * result
            + (this.categoryParameterName != null ? this.categoryParameterName.hashCode() : 0);
        return result;
    }

}
