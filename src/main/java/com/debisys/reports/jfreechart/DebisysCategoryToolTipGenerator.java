package com.debisys.reports.jfreechart;

import java.text.DateFormat;
import java.text.NumberFormat;

import org.jfree.chart.labels.AbstractCategoryItemLabelGenerator;
import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.data.category.CategoryDataset;

/**
 * A standard tool tip generator that can be used with a
 * {@link org.jfree.chart.renderer.CategoryItemRenderer}.
 */
public class DebisysCategoryToolTipGenerator extends AbstractCategoryItemLabelGenerator
                                              implements CategoryToolTipGenerator {

    /** The default format string. */
    public static final String DEFAULT_TOOL_TIP_FORMAT_STRING = "{1} = {2}";

    /**
     * Creates a new generator with a default number formatter.
     */
    public DebisysCategoryToolTipGenerator() {
        super(DEFAULT_TOOL_TIP_FORMAT_STRING, NumberFormat.getInstance());
    }

    /**
     * Creates a new generator with the specified number formatter.
     *
     * @param labelFormat  the label format string (<code>null</code> not permitted).
     * @param formatter  the number formatter (<code>null</code> not permitted).
     */
    public DebisysCategoryToolTipGenerator(String labelFormat, NumberFormat formatter) {
        super(labelFormat, formatter);
    }

    /**
     * Creates a new generator with the specified date formatter.
     *
     * @param labelFormat  the label format string (<code>null</code> not permitted).
     * @param formatter  the date formatter (<code>null</code> not permitted).
     */
    public DebisysCategoryToolTipGenerator(String labelFormat, DateFormat formatter) {
        super(labelFormat, formatter);
    }

    /**
     * Generates the tool tip text for an item in a dataset.  Note: in the current dataset
     * implementation, each row is a series, and each column contains values for a
     * particular category.
     *
     * @param dataset  the dataset (<code>null</code> not permitted).
     * @param row  the row index (zero-based).
     * @param column  the column index (zero-based).
     *
     * @return The tooltip text (possibly <code>null</code>).
     */
    public String generateToolTip(CategoryDataset dataset, int row, int column) {
        return generateLabelString(dataset, row, column);
    }

}
