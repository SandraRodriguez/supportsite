package com.debisys.reports.jfreechart;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Paint;
import java.awt.geom.Rectangle2D;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemCollection;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis3D;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.labels.CategorySeriesLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.GroupedStackedBarRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.data.KeyToGroupMap;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

public class Chart
{
  /**
   * ******************************************************
   * hashSettings
   * key              value
   * data             Vector of data shere item 0 is desc, 1 is number value
   * title            Title of chart
   * category_label   Label name for the description
   * value_label      Label name for the values
   * width            Integer width
   * height           Integer height
   * horizontal       If exists then horizontal bar graph
   * showlabels       If exists then labels are shown on the chart
   * rgb              Color string i.e. "0,0,0"
   * ******************************************************
   */
  public static String generateBarChart(Hashtable hashSettings, HttpSession session, PrintWriter pw)
  {
    String filename = null;
    try
    {
      Vector vecData = new Vector();
      boolean isHorizontal = false;
      if (hashSettings.containsKey("data"))
      {
        vecData = (Vector) hashSettings.get("data");
      }
      if (hashSettings.containsKey("horizontal"))
      {
        isHorizontal = true;
      }


      //  Throw a custom NoDataException if there is no data
      if (vecData == null || vecData.size() == 0)
      {
        System.out.println("No data has been found");
        throw new NoDataException();
      }

      //  Create and populate a CategoryDataset
      Iterator it = vecData.iterator();
      DefaultCategoryDataset dataset = new DefaultCategoryDataset();
      int counter = 0;
      while (it.hasNext())
      {
        counter++;
        Vector vecTemp = null;
        vecTemp = (Vector) it.next();
        if (isHorizontal)
        {
          dataset.addValue(new Double(Double.parseDouble(vecTemp.get(1).toString())), "Transactions", counter + ") " + vecTemp.get(0).toString());
        }
        else
        {
          dataset.addValue(new Double(Double.parseDouble(vecTemp.get(1).toString())), "Transactions", vecTemp.get(0).toString());
        }
      }

      //  Create the chart object
      DecimalFormat labelFormatter = new DecimalFormat("#,##0.00");
      CategoryAxis3D categoryAxis = new CategoryAxis3D(hashSettings.get("category_label").toString());
      ValueAxis valueAxis = new NumberAxis(hashSettings.get("value_label").toString());
      BarRenderer3D renderer = new BarRenderer3D();
      //renderer.setItemURLGenerator(new StandardCategoryURLGenerator("xy_chart.jsp","series","section"));
      renderer.setToolTipGenerator(new DebisysCategoryToolTipGenerator("{1} = {2}",labelFormatter));
      renderer.setItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", labelFormatter));

      if (isHorizontal)
      {
        renderer.setNegativeItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE3, TextAnchor.BASELINE_CENTER));
        //set no wrap
        categoryAxis.setMaximumCategoryLabelWidthRatio(5.0f);
      }
      else
      {
        renderer.setPositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.TOP_CENTER));
      }

      boolean blnShowLabels = false;
      if (hashSettings.containsKey("showlabels"))
      {
        blnShowLabels = true;
      }
      renderer.setItemLabelsVisible(blnShowLabels);

      String[] colorsRGB = null;
      if (hashSettings.containsKey("rgb"))
      {
        colorsRGB = hashSettings.get("rgb").toString().split(",");
      }
      if (colorsRGB != null && colorsRGB.length == 3)
      {
        renderer.setPaint(new Color(Integer.parseInt(colorsRGB[0]), Integer.parseInt(colorsRGB[1]), Integer.parseInt(colorsRGB[2])));
      }
      else
      {
        renderer.setPaint(new Color(254, 166, 74));
      }

      CategoryPlot categoryPlot = new CategoryPlot(dataset, categoryAxis, valueAxis, renderer);
      if (isHorizontal)
      {
        categoryPlot.setOrientation(PlotOrientation.HORIZONTAL);
      }
      else
      {
        categoryPlot.setOrientation(PlotOrientation.VERTICAL);
      }
      categoryPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);


      JFreeChart chart = new JFreeChart(hashSettings.get("title").toString()  , new Font("SansSerif", Font.BOLD, 12), categoryPlot, false);

      chart.setBackgroundPaint(java.awt.Color.white);

      //  Write the chart image to the temporary directory
      int intWidth  = 300;
      int intHeight = 300;
      if (hashSettings.containsKey("width") && hashSettings.containsKey("height"))
      {

      try
      {
        intWidth = Integer.parseInt(hashSettings.get("width").toString());
        intHeight = Integer.parseInt(hashSettings.get("height").toString());
      }
      catch (NumberFormatException nfe)
      {
        intWidth  = 300;
        intHeight  = 300;
      }
      }


      ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
      filename = ServletUtilities.saveChartAsJPEG(chart, intWidth, intHeight, info, session);

      //  Write the image map to the PrintWriter
     ChartUtilities.writeImageMap(pw, filename, info, false);
      pw.flush();

    }
    catch (NoDataException e)
    {
      System.out.println(e.toString());
      filename = "public_nodata_500x300.png";
    }
    catch (Exception e)
    {
      System.out.println("Exception - " + e.toString());
      e.printStackTrace(System.out);
      filename = "public_error_500x300.png";
    }
    return filename;
  }


  public static String generateHeartbeatBarChart(Hashtable hashSettings, HttpSession session, PrintWriter pw)
  {
    String filename = null;
    try
    {
      Vector vecData = new Vector();
      boolean isHorizontal = false;
      if (hashSettings.containsKey("data"))
      {
        vecData = (Vector) hashSettings.get("data");
      }
      if (hashSettings.containsKey("horizontal"))
      {
        isHorizontal = true;
      }


      //  Throw a custom NoDataException if there is no data
      if (vecData == null || vecData.size() == 0)
      {
        System.out.println("No data has been found");
        throw new NoDataException();
      }

      //  Create and populate a CategoryDataset
      Iterator it = vecData.iterator();
      DefaultCategoryDataset dataset = new DefaultCategoryDataset();
      GroupedStackedBarRenderer renderer = new GroupedStackedBarRenderer();
      int counter = 0;
      KeyToGroupMap map = new KeyToGroupMap("G1");
      while (it.hasNext())
      {
        counter++;
        Vector vecTemp = null;
        vecTemp = (Vector) it.next();
      
          dataset.addValue(new Integer(Integer.parseInt(vecTemp.get(1).toString())), vecTemp.get(2).toString(), vecTemp.get(0).toString());
          map.mapKeyToGroup(vecTemp.get(2).toString(),"G1");
          renderer.setSeriesToGroupMap(map); 
     }
      
     
      //  Create the chart object
       CategoryAxis3D categoryAxis = new CategoryAxis3D(hashSettings.get("category_label").toString());
      ValueAxis valueAxis = new NumberAxis(hashSettings.get("value_label").toString());
      Paint p2 = new GradientPaint(
              0.0f, 0.0f, new Color(0x22, 0xFF, 0x22), 0.0f, 0.0f, new Color(0x88, 0xFF, 0x88)
          );
          renderer.setSeriesPaint(1, p2); 
          renderer.setSeriesPaint(5, p2); 
          renderer.setSeriesPaint(9, p2); 
          
          Paint p3 = new GradientPaint(
              0.0f, 0.0f, new Color(0xFF, 0x22, 0x22), 0.0f, 0.0f, new Color(0xFF, 0x88, 0x88)
          );
          renderer.setSeriesPaint(2, p3);
          renderer.setSeriesPaint(6, p3);
          renderer.setSeriesPaint(10, p3);

      DecimalFormat labelFormatter = new DecimalFormat("#,##0");
      renderer.setItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", labelFormatter));

      if (isHorizontal)
      {
        renderer.setNegativeItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE3, TextAnchor.BASELINE_CENTER));
        //set no wrap
        categoryAxis.setMaximumCategoryLabelWidthRatio(5.0f);
      }
      else
      {
        renderer.setPositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.TOP_CENTER));
      }

      boolean blnShowLabels = false;
      if (hashSettings.containsKey("showlabels"))
      {
        blnShowLabels = true;
      }
      renderer.setItemLabelsVisible(blnShowLabels);
      CategoryPlot categoryPlot = new CategoryPlot(dataset, categoryAxis, valueAxis, renderer);
      categoryPlot.setOrientation(PlotOrientation.VERTICAL);
      categoryPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_LEFT);
      categoryPlot.setFixedLegendItems(createLegendItems());
 
      JFreeChart chart = new JFreeChart(hashSettings.get("title").toString()  , new Font("SansSerif", Font.BOLD, 12), categoryPlot, true);

      chart.setBackgroundPaint(java.awt.Color.white);

      //  Write the chart image to the temporary directory
      int intWidth  = 300;
      int intHeight = 300;
      if (hashSettings.containsKey("width") && hashSettings.containsKey("height"))
      {

      try
      {
        intWidth = Integer.parseInt(hashSettings.get("width").toString());
        intHeight = Integer.parseInt(hashSettings.get("height").toString());
      }
      catch (NumberFormatException nfe)
      {
        intWidth  = 300;
        intHeight  = 300;
      }
      }


      ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
      filename = ServletUtilities.saveChartAsJPEG(chart, intWidth, intHeight, info, session);

      //  Write the image map to the PrintWriter
     ChartUtilities.writeImageMap(pw, filename, info, false);
      pw.flush();

    }
    catch (NoDataException e)
    {
      System.out.println(e.toString());
      filename = "public_nodata_500x300.png";
    }
    catch (Exception e)
    {
      System.out.println("Exception - " + e.toString());
      e.printStackTrace(System.out);
      filename = "public_error_500x300.png";
    }
    return filename;
  }

  
  public static LegendItemCollection createLegendItems() {
      LegendItemCollection result = new LegendItemCollection();
      Paint p2 = new GradientPaint(
              0.0f, 0.0f, new Color(0x22, 0xFF, 0x22), 0.0f, 0.0f, new Color(0x88, 0xFF, 0x88)
          );
      Paint p3 = new GradientPaint(
              0.0f, 0.0f, new Color(0xFF, 0x22, 0x22), 0.0f, 0.0f, new Color(0xFF, 0x88, 0x88)
          );
      LegendItem item = new LegendItem("Successful", 
              "description", 
              "tooltip", 
              "url", 
              new Rectangle2D.Float(0, 5, 10, 5), p2);
      LegendItem item2 = new LegendItem("Failure", 
              "description", 
              "tooltip", 
              "url", 
              new Rectangle2D.Float(0, 5, 10, 5), p3);
    result.add(item);
     result.add(item2);
      return result;
  }
}