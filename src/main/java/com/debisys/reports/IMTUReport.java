/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.debisys.reports;

import com.debisys.exceptions.CustomerException;
import com.debisys.exceptions.ReportException;
import com.debisys.reports.pojo.BasicPojo;
import com.debisys.reports.pojo.ImtuIsoPojo;
import com.debisys.reports.pojo.ImtuMerchantPojo;
import com.debisys.reports.pojo.ImtuPojo;
import com.debisys.reports.pojo.ProviderPojo;
import com.debisys.reports.pojo.WorkJob;
import com.debisys.reports.pojo.WorkMasterPojo;
import com.debisys.utils.Countries;
import com.debisys.utils.NumberUtil;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.apache.torque.Torque;

/**
 *
 * @author nmartinez
 */
public class IMTUReport {
    
    private static Logger cat = Logger.getLogger(IMTUReport.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
    
    
    /**
     * 
     * @param reportId
     * @return
     * @throws CustomerException 
     */
    public static ArrayList<ImtuPojo> findIMTUReportByID(String reportId) throws CustomerException
    {
        ArrayList<ImtuPojo> ImtuPojos = new  ArrayList<ImtuPojo>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "SELECT COUNT(distinct i.IsoId) AS isos, pr.provider_id, pr.name, p.description, i.sku, p.Buy_Rate, p.alternate_id, p.disabled, " +
                        " sk.Code AS skuCateCode, sk.Description AS skuCateDesc, sk.Conversion "+
                        "FROM IMTUReport i WITH(NOLOCK) " +
                        "INNER JOIN products p WITH(NOLOCK) ON p.id = i.sku " +
                        "INNER JOIN provider pr WITH(NOLOCK) ON pr.provider_id = p.program_id " +
                        "INNER JOIN SkuCategories sk WITH(NOLOCK) ON p.SkuCategory = sk.Code "+
                        "WHERE i.WorkMasterId = '"+ reportId +"' " +
                        "GROUP BY i.sku , pr.provider_id, pr.name, p.description, p.Buy_Rate, p.alternate_id, p.disabled, " +
                        " sk.Code, sk.Description, sk.Conversion " +
                        "ORDER BY pr.provider_id, pr.name, p.description, i.sku, p.Buy_Rate, p.alternate_id ";
                       
            
            cat.debug("findIMTUReportByID =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                ImtuPojo imtuPojo = new ImtuPojo();
                imtuPojo.setId( rs.getString("provider_id") );
                imtuPojo.setDescripton( rs.getString("name") );
                imtuPojo.getProduct().setId( rs.getString("sku") );
                imtuPojo.getProduct().setDescripton( rs.getString("description") );
                imtuPojo.setCountIsosProducts( rs.getInt("isos") );
                imtuPojo.setDisabled( rs.getBoolean("disabled") );
                imtuPojo.setBuyRate( Double.toString(rs.getDouble("Buy_Rate")) );
                
                if ( rs.getBoolean("Conversion") )
                    imtuPojo.setConversion(true);
                else
                    imtuPojo.setConversion(false);
                imtuPojo.setSkuCategory( rs.getString("skuCateDesc") + "("+rs.getString("skuCateCode")+")" );
                ImtuPojos.add(imtuPojo);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findIMTUReportByID ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findIMTUReportByID Error during closeConnection", e);
            }
        }
        return ImtuPojos;
    }
    
    /**
     * 
     * @param reportId
     * @param productId
     * @return
     * @throws CustomerException 
     */
    public static ArrayList<ImtuIsoPojo> findIMTUReportByIDAndProduct(String reportId, String productId) throws CustomerException
    {
        ArrayList<ImtuIsoPojo> ImtuIsoPojos = new  ArrayList<ImtuIsoPojo>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "SELECT i.IsoId, r.businessname, p.id as productId, p.description, p.cost, " +
                         "COUNT( distinct i.Millennium_no) AS numberOfTerminals, " +
                         "COUNT( distinct i.RateplanId) AS numberRatePlans, " +
                         "ISNULL(imtuat.NumTerminals, 0) AS numActTerminals " +
                         "FROM IMTUReport i WITH(NOLOCK) " +
                         "INNER JOIN products p WITH(NOLOCK)  ON p.id = i.sku " +
                         "INNER JOIN reps r WITH(NOLOCK) ON r.rep_id= i.IsoId " +
                         "LEFT OUTER JOIN IMTUReportAT imtuat WITH(NOLOCK) ON imtuat.WorkMasterId = i.WorkMasterId AND imtuat.IsoId = i.IsoId  " +
                         " AND  i.sku =   imtuat.ProductId " +
                         "WHERE i.WorkMasterId = '"+reportId+"' " +
                         "AND i.sku = " + productId +" "+
                         "GROUP BY i.IsoId, r.businessname, p.id, p.description, p.cost, imtuat.NumTerminals  " +
                         "ORDER BY r.businessname ";
                       
            
            cat.debug("findIMTUReportByID =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                ImtuIsoPojo imtuIsoPojo = new ImtuIsoPojo();
                imtuIsoPojo.getProduct().setId( rs.getString("productId") );
                imtuIsoPojo.getProduct().setDescripton( rs.getString("description").trim() );
                imtuIsoPojo.setId( rs.getString("IsoId") );
                imtuIsoPojo.setDescripton( rs.getString("businessname").trim() );
                imtuIsoPojo.setNumActiveTerminals(33);
                imtuIsoPojo.setNumRatePlans( rs.getInt("numberRatePlans") );
                imtuIsoPojo.setNumTerminals( rs.getInt("numberOfTerminals") );                
                imtuIsoPojo.setTotalIsoBuyRate( rs.getString("cost") );
                imtuIsoPojo.setNumActiveTerminals( rs.getInt("numActTerminals") );
                ImtuIsoPojos.add(imtuIsoPojo);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findIMTUReportByIDAndProduct ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findIMTUReportByIDAndProduct Error during closeConnection", e);
            }
        }
        return ImtuIsoPojos;
    }
    
    /**
     * 
     * @param reportId
     * @param productId
     * @param isoId
     * @return
     * @throws CustomerException 
     */
    public static ArrayList<ImtuMerchantPojo> findIMTUReportByIDAndProductAndIsoId(String reportId, String productId, String isoId) throws CustomerException
    {
        ArrayList<ImtuMerchantPojo> ImtuMerchantsPojos = new  ArrayList<ImtuMerchantPojo>();
        Connection dbConn = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            
            String sql = "\n SELECT r.businessname, m.merchant_id, m.dba, COUNT( DISTINCT i.Millennium_no ) as numOfTerminals \n" +
                        "FROM IMTUReport i WITH(NOLOCK) \n" +
                        "INNER JOIN reps r WITH(NOLOCK)      ON r.rep_id      = i.IsoId \n" +
                        "INNER JOIN merchants m WITH(NOLOCK) ON m.merchant_id = i.MerchantId \n" +
                        "WHERE i.WorkMasterId = '"+reportId+"' \n" +
                        "AND i.sku   = "+productId+" \n" +
                        "AND i.IsoId = "+isoId+" \n" +
                        "GROUP BY r.businessname, m.merchant_id, m.dba \n" +
                        "ORDER BY r.businessname, m.dba ";
            
            cat.debug("findIMTUReportByIDAndProductAndIsoId =[" + sql + "]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                ImtuMerchantPojo imtuMerchantPojo = new ImtuMerchantPojo();
                imtuMerchantPojo.setId( rs.getString("merchant_id") );
                imtuMerchantPojo.setDescripton( rs.getString("dba") );
                imtuMerchantPojo.getIso().setDescripton( rs.getString("businessname") );
                imtuMerchantPojo.getIso().setId(isoId);
                //imtuMerchantPojo.s
                imtuMerchantPojo.setNumberOfTerminals( rs.getInt("numOfTerminals") );
                ImtuMerchantsPojos.add(imtuMerchantPojo);
            }
            rs.close();
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during findIMTUReportByIDAndProductAndIsoId ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findIMTUReportByIDAndProductAndIsoId Error during closeConnection", e);
            }
        }
        return ImtuMerchantsPojos;
    }
    
    
    /**
     * 
     * @return
     * @throws ReportException 
     */
    public static ArrayList<ProviderPojo> getProviders() throws ReportException
    {
        ArrayList<ProviderPojo> providers = new ArrayList<ProviderPojo>();
        Connection dbConn = null;
        String strSQL = "";
        
        try{
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = "\n SELECT distinct p.program_id, pr.name FROM CrossborderCarriers cbc WITH(NOLOCK) \n" +
                     " INNER JOIN CrossborderSKU cbs WITH(NOLOCK) ON cbc.id = cbs.CarrierID \n" +
                     " INNER JOIN products p WITH(NOLOCK) ON p.id = cbs.ProductID \n" +
                     " INNER JOIN provider pr WITH(NOLOCK) ON pr.provider_id = p.program_id ";
            
            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL);
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {
                ProviderPojo provider = new ProviderPojo();
                provider.setId( rs.getString("program_id"));
                provider.setDescripton( rs.getString("name"));                
                providers.add( provider );
            }
            rs.close();
            pstmt.close();
        }
        catch(Exception e)
        {
            cat.error("Error during getProviders ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }
        }
        return providers;                
    }
    
    /**
     * 
     * @return 
     */
    public static ArrayList<Countries> getCountries()
    {
      ArrayList<Countries> countries = new ArrayList<Countries>();
      Connection dbConn = null;
      try
      {
          String sql = "\n SELECT distinct c.CountryName, cbc.CountryID, c.internationalCode  \n" +
                        " FROM CrossborderCarriers cbc WITH(NOLOCK) \n" +
                        " INNER JOIN CrossborderSKU cbs WITH(NOLOCK) ON cbc.id = cbs.CarrierID \n" +
                        " INNER JOIN products p WITH(NOLOCK) ON p.id = cbs.ProductID \n" +
                        " INNER JOIN countries c WITH(NOLOCK) ON c.CountryId = cbc.CountryID ";
          
        dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
        if (dbConn == null)
        {
          cat.error("Can't get database connection");
          throw new Exception();
        }
        PreparedStatement pstmt = dbConn.prepareStatement(sql);
        ResultSet rs = pstmt.executeQuery();
        while(rs.next())
        {
          Countries country = new Countries();          
          //country.setCountryCode(rs.getString("CountryCode"));
          country.setCountryId(rs.getInt("CountryId"));          
          country.setCountryName(rs.getString("CountryName"));
          //country.setExternal_id(rs.getString("external_id"));
          //country.setIdNew(rs.getString("IdNew"));
         // country.setImageId(rs.getInt("ImageId"));
          country.setInternationalCode(rs.getString("internationalCode"));
          //country.setInternationalDialCode(rs.getString("internationalDialCode"));
          //country.setIsoAlpha3Code(rs.getString("IsoAlpha3Code"));          
          countries.add(country);
        }

        rs.close();
        pstmt.close();
      }
      catch (Exception e)
      {
        cat.error("Error during getCountries", e);
      }
      finally
      {
        try
        {
          Torque.closeConnection(dbConn);
        }
        catch (Exception e)
        {
          cat.error("Error during release connection", e);
        }
      }
      return countries;
    }
    
    /**
     * 
     * @return
     * @throws ReportException 
     */
    public static ArrayList<WorkMasterPojo> getWorksByUserId(String userIdUnique, String workCode) throws ReportException
    {
        ArrayList<WorkMasterPojo> workPojo = new ArrayList<WorkMasterPojo>();
        Connection dbConn = null;
        String strSQL = "";
        
        try{
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = "SELECT [Id],[CreationDate],[WorkCode],[IdUser], [EndDate], [TitleJob], [ATEndaDate] FROM [WorkMaster] "+
                    " WHERE [IdUser]='"+userIdUnique+"' AND [WorkCode]='"+workCode+"'"+
                    " ORDER BY [CreationDate] DESC ";                    
            
            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL);
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {
                WorkMasterPojo work = new WorkMasterPojo();
                work.setId( rs.getString("id") );
                work.setCreationDate( rs.getString("CreationDate") );
                work.setEndDate( rs.getString("EndDate") );
                work.setTitleJob( rs.getString("TitleJob") );
                work.setAtEndDate( rs.getString("ATEndaDate") );
                workPojo.add( work );
            }
            rs.close();
            pstmt.close();
        }
        catch(Exception e)
        {
            cat.error("Error during getWorksByUserId ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }
        }
        return workPojo;                
    }
    
    /**
     * 
     * @param workId
     * @return
     * @throws CustomerException 
     */
    public static int deleteByWorkId(String workId) throws CustomerException
    {
        Connection dbConn = null;
        int result = 0;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String sql = "DELETE FROM [WorkMaster] WHERE Id='"+workId+"' ";
            cat.debug("deleteByWorkId =[" + sql+"]");
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            result = pstmt.executeUpdate();            
            pstmt.close();
        }
        catch (Exception e)
        {
            cat.error("Error during deleteByWorkId ", e);
            throw new CustomerException();
        }
        finally
        {
            try
            {
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("findIMTUReportByID Error during closeConnection", e);
            }
        }
        return result;
    }
    
    /**
     * 
     * @return
     * @throws ReportException 
     */
    public static ArrayList<BasicPojo> getSkuCategories() throws ReportException
    {
        ArrayList<BasicPojo> skusPojos = new ArrayList<BasicPojo>();
        Connection dbConn = null;
        String strSQL = "";
        
        try{
            dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new ReportException();
            }
            strSQL = "SELECT Code, Description FROM [SkuCategories] ORDER BY Description ";
                    
            PreparedStatement pstmt = null;
            pstmt = dbConn.prepareStatement(strSQL);
            ResultSet rs = pstmt.executeQuery();
            while( rs.next())
            {
                BasicPojo category = new BasicPojo();
                category.setId( rs.getString("Code"));
                category.setDescripton(rs.getString("Description"));
                skusPojos.add( category );
            }
            rs.close();
            pstmt.close();
        }
        catch(Exception e)
        {
            cat.error("Error during getSkuCategories ", e);
            throw new ReportException();
        }
        finally
        {
            try
            {
                Torque.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("Error during closeConnection", e);
            }
        }
        return skusPojos;                
    }
    
    /**
     * 
     * @param workJob
     * @return
     * @throws CustomerException 
     */
    public static int createNewWork(WorkJob workJob) throws CustomerException
    {
        Connection dbConn = null;
        int result = 0;
        ResultSet rs = null;
        Integer uniqueId = null;
        try
        {
            dbConn = TorqueHelper.getConnection(sql_bundle.getString("pkgDefaultDb"));
            if (dbConn == null)
            {
                cat.error("Can't get database connection");
                throw new CustomerException();
            }
            String categoryName = workJob.getCategoryJob();
            
            String sql = "INSERT INTO [WorkMaster] ([WorkCode],[IdUser],[TitleJob],[CreationDate]) \n"+
                         "VALUES ('"+workJob.getReportCode()+"','"+workJob.getUserId()+"','"+workJob.getTitle()+"',getdate()); \n"+
                        " SELECT @@identity as ID ";

            cat.debug("createNewWork =[" + sql+"]");
            dbConn.setAutoCommit(false);
            PreparedStatement pstmt = dbConn.prepareStatement( sql );
            rs = pstmt.executeQuery();
            if (rs.next())
            {
                uniqueId = rs.getInt("ID");
            }
            rs.close();
            
            if ( uniqueId != null )
            {
                pstmt.close();                
                StringBuilder statement = new StringBuilder("sp_IMTU_Report "+workJob.getCountryId()+", '"+uniqueId+"'");
                
                statement.append(",'").append(categoryName).append("'");
                statement.append(",'").append(uniqueId).append("'");
                
                if ( workJob.getProviders() != null )
                {
                    statement.append(",'").append(workJob.getProviders()).append("'");
                }
                else
                {
                    statement.append(",").append("null").append("");
                }
                if ( workJob.getProducts() != null )
                {
                    statement.append(",'").append(workJob.getProducts()).append("'");
                }
                else
                {
                    statement.append(",").append("null").append("");
                }
                if ( workJob.getSkuCategories() != null )
                {
                    statement.append(",'").append(workJob.getSkuCategories()).append("'");
                }
                else
                {
                    statement.append(",").append("null").append("");
                }
                
                sql = "exec sp_CreateJobWork ?,?,?";
                cat.info("exec sp_CreateJobWork '"+statement.toString()+"', '"+ workJob.getCategoryJob()+"', 'IMTU_" +uniqueId+"'");
                pstmt = dbConn.prepareStatement( sql );
                pstmt.setString(1, statement.toString() );
                pstmt.setString(2, categoryName );
                pstmt.setString(3, uniqueId.toString() );
                pstmt.execute();                
                dbConn.commit();
            }
            pstmt.close();
        }        
        catch (SQLException ex) 
        {
            try 
            {
                if ( dbConn != null)
                    dbConn.rollback();                                
            } catch (Exception ex1) 
            {
                cat.error("SQLException doing dbConn.rollback(); ", ex1);
            }
        }
        catch (Exception e)
        {
            cat.error("Error during createNewWork ", e);            
        }
        finally
        {
            try
            {
                if ( dbConn != null)
                    dbConn.setAutoCommit(true); 
                TorqueHelper.closeConnection(dbConn);
            }
            catch (Exception e)
            {
                cat.error("createNewWork Error during closeConnection", e);
            }            
        }
        return result;
    }
}
