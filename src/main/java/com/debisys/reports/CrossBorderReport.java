package com.debisys.reports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Category;
import org.apache.torque.Torque;
import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.transactions.TransactionSearch;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;

/**
 * @author Jay Chi
 */

public class CrossBorderReport implements HttpSessionBindingListener
{

  private String start_date = "";
  private String end_date = "";
  private Hashtable validationErrors = new Hashtable();
  /*BEGIN Release 13.0 - Added by LF*/
  private String sourceCountry = "";
  private String targetCountry = "";
  /*END Release 13.0 - Added by LF*/

  //log4j logging
  static Category cat = Category.getInstance(CrossBorderReport.class);
  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

  /*BEGIN Release 13.0 - Added by LF*/
  /**
 * @param targetCountry The targetCountry to set.
 */
public void setTargetCountry(String targetCountry)
{
	this.targetCountry = targetCountry;
}

/**
 * @return Returns the targetCountry.
 */
public String getTargetCountry()
{
	return this.targetCountry;
}

/**
 * @param sourceCountry The sourceCountry to set.
 */
public void setSourceCountry(String sourceCountry)
{
	this.sourceCountry = sourceCountry;
}

/**
 * @return Returns the sourceCountry.
 */
public String getSourceCountry()
{
	return this.sourceCountry;
}
  /*END Release 13.0 - Added by LF*/

public String getStartDate()
  {
    return StringUtil.toString(this.start_date);
  }

  public void setStartDate(String strValue)
  {
	  this.start_date = strValue;
  }

  public String getEndDate()
  {
    return StringUtil.toString(this.end_date);
  }

  public void setEndDate(String strValue)
  {
	  this.end_date = strValue;
  }


  /**
   * Returns a hash of errors.
   *
   * @return Hash of errors.
   */

  public Hashtable getErrors()
  {
    return this.validationErrors;
  }

  /**
   * Returns a validation error against a specific field.  If a field
   * was found to have an error during
   * {@link #validateDateRange validateDateRange},  the error message
   * can be accessed via this method.
   *
   * @param fieldname The bean property name of the field
   * @return The error message for the field or null if none is
   *         present.
   */

  public String getFieldError(String fieldname)
  {
    return ((String) this.validationErrors.get(fieldname));
  }


  /**
   * Sets the validation error against a specific field.  Used by
   * {@link #validateDateRange validateDateRange}.
   *
   * @param fieldname The bean property name of the field
   * @param error     The error message for the field or null if none is
   *                  present.
   */

  public void addFieldError(String fieldname, String error)
  {
	  this.validationErrors.put(fieldname, error);
  }


  /**
   * Validates for missing or invalid fields.
   *
   * @return <code>true</code> if the entity passes the validation
   *         checks,  otherwise  <code>false</code>
   */

  public boolean validateDateRange(SessionData sessionData)
  {
	  this.validationErrors.clear();
    boolean valid = true;

    if ((this.start_date == null) ||
        (this.start_date.length() == 0))
    {
      this.addFieldError("startdate", Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.start_date))
    {
      this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
      valid = false;
    }

    if ((this.end_date == null) ||
        (this.end_date.length() == 0))
    {
      this.addFieldError("enddate", Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.end_date))
    {
      this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
      valid = false;
    }

    if (valid)
    {

      Date dtStartDate = new Date();
      Date dtEndDate = new Date();
      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
      formatter.setLenient(false);
      try
      {
        dtStartDate = formatter.parse(this.start_date);
      }
      catch (ParseException ex)
      {
        this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
        valid = false;
      }
      try
      {
        dtEndDate = formatter.parse(this.end_date);
      }
      catch (ParseException ex)
      {
        this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
        valid = false;
      }
      if (dtStartDate.after(dtEndDate))
      {
        this.addFieldError("date", "Start date must be before end date.");
        valid = false;
      }

    }

    return valid;
  }


  public Vector getCrossBorderSummary(SessionData sessionData)
      throws ReportException
  {
    Connection dbConn = null;
    Vector vecCrossBorderSummary = new Vector();
    String strAccessLevel = sessionData.getProperty("access_level");
    String strDistChainType = sessionData.getProperty("dist_chain_type");
    String strRefId = sessionData.getProperty("ref_id");
    String strSQLSummary = "";
    PreparedStatement pstmt = null;

    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new ReportException();
      }

      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
        if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
        {
          strSQLSummary = sql_bundle.getString("getCrossBorderSummary3");
          pstmt = dbConn.prepareStatement(strSQLSummary);
          pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
          pstmt.setDouble(2, Double.parseDouble(strRefId));
          pstmt.setString(3, this.start_date);
          pstmt.setString(4, DateUtil.addSubtractDays(this.end_date, 1));

        }
        else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
        {
          strSQLSummary = sql_bundle.getString("getCrossBorderSummary5");
          pstmt = dbConn.prepareStatement(strSQLSummary);
          pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
          pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
          pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
          pstmt.setDouble(4, Double.parseDouble(strRefId));
          pstmt.setString(5, this.start_date);
          pstmt.setString(6, DateUtil.addSubtractDays(this.end_date, 1));

        }

      } 
      else if(strAccessLevel.equals(DebisysConstants.AGENT))
      {
          strSQLSummary = sql_bundle.getString("getCrossBorderSummary8");
          pstmt = dbConn.prepareStatement(strSQLSummary);
          pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
          pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
          pstmt.setDouble(3, Double.parseDouble(strRefId));
          pstmt.setString(4, this.start_date);
          pstmt.setString(5, DateUtil.addSubtractDays(this.end_date, 1));
      }
      else if(strAccessLevel.equals(DebisysConstants.SUBAGENT))
      {
          strSQLSummary = sql_bundle.getString("getCrossBorderSummary9");
          pstmt = dbConn.prepareStatement(strSQLSummary);
          pstmt.setString(1, this.start_date);
          pstmt.setString(2, DateUtil.addSubtractDays(this.end_date, 1));
          pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
          pstmt.setDouble(4, Double.parseDouble(strRefId));
      }
      else if(strAccessLevel.equals(DebisysConstants.REP)) {
    	  strSQLSummary = sql_bundle.getString("getCrossBorderSummary6");
          pstmt = dbConn.prepareStatement(strSQLSummary);
          pstmt.setDouble(1, Double.parseDouble(strRefId));
          pstmt.setString(2, this.start_date);
          pstmt.setString(3, DateUtil.addSubtractDays(this.end_date, 1));
      }
      else if(strAccessLevel.equals(DebisysConstants.MERCHANT)) {
    	  strSQLSummary = sql_bundle.getString("getCrossBorderSummary7");
          pstmt = dbConn.prepareStatement(strSQLSummary);
          pstmt.setDouble(1, Double.parseDouble(strRefId));
          pstmt.setString(2, this.start_date);
          pstmt.setString(3, DateUtil.addSubtractDays(this.end_date, 1));
      }

      cat.debug(strSQLSummary);
      ResultSet rs = pstmt.executeQuery();
      while (rs.next())
      {
        Vector vecTemp = new Vector();
        vecTemp.add(rs.getString("source_country"));
        vecTemp.add(rs.getString("target_country"));
        vecTemp.add(rs.getString("qty"));
        vecTemp.add(rs.getString("source_total"));
        vecTemp.add(rs.getString("accepted_currency"));
        vecTemp.add(rs.getString("target_total"));
        vecTemp.add(rs.getString("exchange_currency"));
        vecTemp.add(rs.getString("unique_customers"));
        vecTemp.add(rs.getString("source_country_code"));
        vecTemp.add(rs.getString("target_country_code"));
        vecCrossBorderSummary.add(vecTemp);
      }
      rs.close();
      pstmt.close();

    }
    catch (Exception e)
    {
      cat.error("Error during getCrossBorderSummary", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    return vecCrossBorderSummary;
  }

  	/***
  	 * DBSY-890 SW
  	 * @param sessionData
  	 * @return
  	 * @throws ReportException
  	 */
  	public Vector<Vector<String>> getCarrierUserCrossBorderSummary(SessionData sessionData) throws ReportException
  	{
  		Connection dbConn = null;
  		Vector<Vector<String>> vecCrossBorderSummary = new Vector<Vector<String>>();
  		String strAccessLevel = sessionData.getProperty("access_level");
  		String strDistChainType = sessionData.getProperty("dist_chain_type");
  		String strRefId = sessionData.getProperty("ref_id");
  		String strCarrierUserProds = sessionData.getProperty("carrierUserProds");
  		String strSQLSummary = "";
  		PreparedStatement pstmt = null;

  		try
  		{
  			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
  			if (dbConn == null)
  			{	
  				cat.error("Can't get database connection");
  				throw new ReportException();
  			}	

  			// If no products are setup, return an empty list and handle in JSP
  			if(strCarrierUserProds == null)
  				return vecCrossBorderSummary;
  			
  			if(strCarrierUserProds.equals(""))
  				return vecCrossBorderSummary;
  			
  			if (strAccessLevel.equals(DebisysConstants.ISO))
  			{
  				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
  				{
  					strSQLSummary = sql_bundle.getString("getCarrierUserCrossBorderSummary3");
  					strSQLSummary = strSQLSummary.replace("PROD_LIST", strCarrierUserProds);
  					pstmt = dbConn.prepareStatement(strSQLSummary);
  					pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
  					pstmt.setDouble(2, Double.parseDouble(strRefId));
  					pstmt.setString(3, this.start_date);
  					pstmt.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
  				}
  				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  				{
  					strSQLSummary = sql_bundle.getString("getCarrierUserCrossBorderSummary5");
  					strSQLSummary = strSQLSummary.replace("PROD_LIST", strCarrierUserProds);
  					pstmt = dbConn.prepareStatement(strSQLSummary);
  					pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
  					pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
  					pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
  					pstmt.setDouble(4, Double.parseDouble(strRefId));
  					pstmt.setString(5, this.start_date);
  					pstmt.setString(6, DateUtil.addSubtractDays(this.end_date, 1));
  				}
  			}
  			else 
  			{
  			
  			}

  			cat.debug(strSQLSummary);
  			ResultSet rs = pstmt.executeQuery();
  			
  			while (rs.next())
  			{
  				Vector<String> vecTemp = new Vector<String>();
  				vecTemp.add(rs.getString("source_country"));
  				vecTemp.add(rs.getString("target_country"));
  				vecTemp.add(rs.getString("qty"));
  				vecTemp.add(rs.getString("source_total"));
  				vecTemp.add(rs.getString("accepted_currency"));
  				vecTemp.add(rs.getString("target_total"));
  				vecTemp.add(rs.getString("exchange_currency"));
  				vecTemp.add(rs.getString("unique_customers"));
  				vecTemp.add(rs.getString("source_country_code"));
  				vecTemp.add(rs.getString("target_country_code"));
  				vecCrossBorderSummary.add(vecTemp);
  			}
  			rs.close();
  			pstmt.close();  			
  		}
  		catch (Exception e)
  		{
  			cat.error("Error during getCarrierUserCrossBorderSummary", e);
  			throw new ReportException();
  		}
  		finally
  		{
  			try
  			{
  				Torque.closeConnection(dbConn);
  			}
  			catch (Exception e)
  			{
  				cat.error("Error during closeConnection", e);
  			}	
  		}
  		return vecCrossBorderSummary;
  	}
  	
  	/***
  	 * DBSY-890 SW
  	 * @param intPageNumber
  	 * @param intRecordsPerPage
  	 * @param sessionData
  	 * @return
  	 * @throws ReportException
  	 */
  	public Vector<Vector<String>> getCarrierUserCrossBorderDetails(int intPageNumber, int intRecordsPerPage, SessionData sessionData) throws ReportException
  	{
  		Connection dbConn = null;
  		Vector<Vector<String>> vecDetail = new Vector<Vector<String>>();
  		
  		try
  		{
  			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
  			if (dbConn == null)
  			{
  				cat.error("Can't get database connection");
  				throw new TransactionException();
  			}
  			//	get a count of total matches
  			PreparedStatement pstmt = null;
  			String strRefId = sessionData.getProperty("ref_id");
  			String strAccessLevel = sessionData.getProperty("access_level");
  			String strDistChainType = sessionData.getProperty("dist_chain_type");
  			String strCarrierUserProds = sessionData.getProperty("carrierUserProds");
  			
  			String strSQL = "SELECT wt.rec_id, wt.millennium_no, wt.dba, wt.merchant_id, "
  					+ "wt.datetime, wt.phys_city, wt.phys_county, wt.clerk, "
            			+ "wt.ani, wt.password, wt.amount, wt.balance, wt.id, wt.description, "
            			+ "wt.control_no, wt.phys_state, wt.other_info " +
  	                    "FROM web_transactions AS wt WITHY(NOLOCK) WHERE ";
  			strSQL = strSQL + " wt.rep_id in ";

  			if (strAccessLevel.equals(DebisysConstants.ISO))
  			{
  				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
  				{
  					strSQL = strSQL + " (select rep_id from reps (nolock) where type="
  	              	+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId
  	              	+ ") ";
  				}
  				else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  				{
  					strSQL = strSQL + " (select rep_id from reps (nolock) where type="
  	              	+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
  	              	+ "(select rep_id from reps (nolock) where type = "
  	              	+ DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in "
  	              	+ "(select rep_id from reps (nolock) where type="
  	              	+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId
  	              	+ "))) ";
  				}
  			}
  			else 
  			{
  				
  			}

  	      	strSQL = strSQL + " AND (wt.datetime >= '" + this.start_date + "' and  wt.datetime < '" + DateUtil.addSubtractDays(this.end_date, 1) + "') ";

  	      	strSQL += " and wt.accepted_currency is not null";
  	      	strSQL += " and wt.source_country_code = '" + this.sourceCountry + "'";
  	      	strSQL += " and wt.target_country_code = '" + this.targetCountry + "'";
  	      	strSQL += " and wt.id in (" + strCarrierUserProds + ")";
  	      	     
  	      	pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
  	      	cat.debug(strSQL);
  	      	ResultSet rs = pstmt.executeQuery();
  	      	int intRecordCount = DbUtil.getRecordCount(rs);
  	      	
  	      	//first row is always the count
  	      	Vector<String> vecCount = new Vector<String>();
  	      	vecCount.add(Integer.toString(intRecordCount));
  	      	vecDetail.add(vecCount);

  	      	if (intRecordCount > 0)
  	      	{
  	      		rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
  	      		
  	      		for (int i=0;i<intRecordsPerPage;i++)
  	      		{ 
  	      			Vector<String> vecTemp = new Vector<String>();
  	      			vecTemp.add(StringUtil.toString(rs.getString("rec_id")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("dba")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
  	      			vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("phys_city") + "," + StringUtil.toString(rs.getString("phys_county"))));
  	      			vecTemp.add(StringUtil.toString(rs.getString("clerk")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("ani")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("password")));
  	      			vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
  	      			vecTemp.add(NumberUtil.formatCurrency(rs.getString("balance")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("id") + "/" + rs.getString("description")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("control_no")));
  	      			String other_info = StringUtil.toString(rs.getString("other_info"));
  	      			if(other_info.trim() != "")
  	      			{
  	      				if(other_info.length() > 6)
  	      				{			    	  
  	      					other_info = other_info.substring((other_info.length()-6), other_info.length());
  	      				}
  	      			}
  	      			vecTemp.add(other_info); 
  	      			vecTemp.add(StringUtil.toString(rs.getString("phys_state")));           
  	      			vecDetail.add(vecTemp);
  	      			if ( !rs.next() )
  	      			{
  	      				break;
  	      			}	
  	      		}
  	      	}
  	      	pstmt.close();
  	      	rs.close();
  		}
  		catch (Exception e)
  		{
  			cat.error("Error during getCarrierUserCrossBorderDetails", e);
  			throw new ReportException();
  		}
  		finally
  		{
  			try
  			{
  				Torque.closeConnection(dbConn);
  			}
  			catch (Exception e)
  			{
  				cat.error("Error during closeConnection", e);
  			}	
  		}
  		return vecDetail;
  	}//End of function getCarrierUserCrossBorderDetails
  	
  	/***
  	 * DBSY-890 SW
  	 * @param context
  	 * @param sessionData
  	 * @return
  	 * @throws TransactionException
  	 */
  	public String downloadCarrierUserCrossBorderDetails(ServletContext context, SessionData sessionData) throws TransactionException
    {
  		long start = System.currentTimeMillis();
  		String strFileName = "";
  		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
  		String downloadPath = DebisysConfigListener.getDownloadPath(context);
  		String workingDir = DebisysConfigListener.getWorkingDir(context);
  		String filePrefix = DebisysConfigListener.getFilePrefix(context);

  		try
  		{
  			Vector<Vector<String>> vData = null;
  			vData = this.getCarrierUserCrossBorderDetails(1, Integer.MAX_VALUE, sessionData);
  			
  			// First element just has the record count. Get rid of it. 
  			vData.removeElementAt(0);
  			
  			long startQuery = 0;
  			long endQuery = 0;
  			startQuery = System.currentTimeMillis();
  			BufferedWriter outputFile = null;
  			
  			try
  			{
  				strFileName = (new TransactionSearch()).generateFileName(context);
  				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
  				cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
  				outputFile.flush();

  				Iterator<Vector<String>> it = vData.iterator();
  		             
  				outputFile.write("\"" + Languages.getString("jsp.admin.reports.tran_no", sessionData.getLanguage()) + "\",");
  				outputFile.write("\"" + Languages.getString("jsp.admin.reports.term_no", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.dba", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.merchant_id", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.date", sessionData.getLanguage()) + "\",");
					
				if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
				{
					outputFile.write("\"" + Languages.getString("jsp.admin.reports.phys_state", sessionData.getLanguage()) + "\",");
				}
				
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.city_county", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.clerk", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.reference", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.ref_no", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.amount", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.balance", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.product", sessionData.getLanguage()) + "\",");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.control_no", sessionData.getLanguage()) + "\",");
				
				outputFile.write("\r\n");
				
				if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
				{
					outputFile.write("\"" + Languages.getString("jsp.admin.reports.authorization_number", sessionData.getLanguage()) + "\",");
				}
  				
  				while (it.hasNext())
  				{
  					Vector<String> vecTemp = null;
  					vecTemp = it.next();
  					
  					outputFile.write("\"" + vecTemp.get(0) + "\",");
  					outputFile.write("\"" + vecTemp.get(1) + "\",");
  					outputFile.write("\"" + vecTemp.get(2) + "\",");
  					outputFile.write("\"" + vecTemp.get(3) + "\",");
  					outputFile.write("\"" + vecTemp.get(4) + "\",");
  					
  					if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
  					{
  						outputFile.write("\"" + vecTemp.get(14) + "\",");
  					}
  					
  					outputFile.write("\"" + vecTemp.get(5) + "\",");
  					outputFile.write("\"" + vecTemp.get(6) + "\",");
  					outputFile.write("\"" + vecTemp.get(7) + "\",");
  					outputFile.write("\"" + vecTemp.get(8) + "\",");
  					outputFile.write("\"" + vecTemp.get(9) + "\",");
  					outputFile.write("\"" + vecTemp.get(10) + "\",");
  					outputFile.write("\"" + vecTemp.get(11) + "\",");
  					outputFile.write("\"" + vecTemp.get(12) + "\",");
  					
  					if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  					{
  						outputFile.write("\"" + vecTemp.get(13) + "\",");
  					}
  					
  					outputFile.write("\r\n");
  				}
  				outputFile.write("\r\n");
  				outputFile.flush();
  				outputFile.close();
  				endQuery = System.currentTimeMillis();
  				cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
  				startQuery = System.currentTimeMillis();
  				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
  				long size = sourceFile.length();
  				byte[] buffer = new byte[(int) size];
  				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
  				cat.debug("Zip File Name: " + zipFileName);
  				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
  				cat.debug("Download URL: " + downloadUrl);

  				try
  				{
  					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

  					// 	Set the compression ratio
  					out.setLevel(Deflater.DEFAULT_COMPRESSION);
  					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

  					// Add ZIP entry to output stream.
  					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

  					// Transfer bytes from the current file to the ZIP file
  					//	out.write(buffer, 0, in.read(buffer));

  					int len;
  					while ((len = in.read(buffer)) > 0)
  					{
  						out.write(buffer, 0, len);
  					}

  					// 	Close the current entry
  					out.closeEntry();
  					// Close the current file input stream
  					in.close();
  					out.close();
  				}
  				catch (IllegalArgumentException iae)
  				{
  					iae.printStackTrace();
  				}
  				catch (FileNotFoundException fnfe)
  				{
  					fnfe.printStackTrace();
  				}
  				catch (IOException ioe)
  				{
  					ioe.printStackTrace();
  				}
  				sourceFile.delete();
  				endQuery = System.currentTimeMillis();
  				cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
  			}
  			catch (IOException ioe)
  			{
  				try
  				{
  					if (outputFile != null)
  					{
  						outputFile.close();
  					}
  				}
  				catch (IOException ioe2)
  				{
  				}
  			}
  		}
  		catch (Exception e)
  		{
  			cat.error("Error during downloadCarrierUserCrossBorderDetails", e);
  			throw new TransactionException();
  		}
  		long end = System.currentTimeMillis();
  		
  		// Display the elapsed time to the standard output
  		cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");

  		return downloadUrl;
    }//End of function downloadCarrierUserCrossBorderDetails

  public void valueBound(HttpSessionBindingEvent event)
  {
  }

  public void valueUnbound(HttpSessionBindingEvent event)
  {
  }

  public static void main(String[] args)
  {
  }

  	/*BEGIN Release 13.0 - Added by LF*/
  	public Vector getCrossBorderDetails(int intPageNumber, int intRecordsPerPage, SessionData sessionData) throws ReportException
  	{
  	  Connection dbConn = null;
  	  Vector vecDetail = new Vector();
  	  try
  	  {
  	    dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
  	    if (dbConn == null)
  	    {
  	      cat.error("Can't get database connection");
  	      throw new TransactionException();
  	    }
  	    //get a count of total matches
  	    PreparedStatement pstmt = null;
  	    String strRefId = sessionData.getProperty("ref_id");
  	    String strAccessLevel = sessionData.getProperty("access_level");
  	    String strDistChainType = sessionData.getProperty("dist_chain_type");

  	    String strSQL = "SELECT wt.rec_id, wt.millennium_no, wt.dba, wt.merchant_id, "
            			+ "wt.datetime, wt.phys_city, wt.phys_county,wt.clerk_name, "
            			+ "wt.ani, wt.password, wt.amount, wt.balance, wt.id, wt.description, "
            			+ "wt.control_no, wt.phys_state, wt.other_info " 
            		  + "FROM web_transactions AS wt WITH (NOLOCK) WHERE ";      		  
  	      
  	      if (strAccessLevel.equals(DebisysConstants.ISO))
  	      {
  	    	  if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
  	    	  {
  	    		  strSQL = strSQL + " wt.rep_id in ";
  	    		  strSQL = strSQL + " (select rep_id from reps WITH (NOLOCK) where type="
  	              	+ DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId
  	              	+ ") ";
  	    	  }
  	    	  else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
  	    	  {
  	    		  strSQL = strSQL + " wt.rep_id in ";
  	    		  strSQL = strSQL + " (select rep_id from reps WITH (NOLOCK) where type="
  	              	+ DebisysConstants.REP_TYPE_REP + " and iso_id in "
  	              	+ "(select rep_id from reps WITH (NOLOCK) where type = "
  	              	+ DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id in "
  	              	+ "(select rep_id from reps WITH (NOLOCK) where type="
  	              	+ DebisysConstants.REP_TYPE_AGENT + " and iso_id = " + strRefId
  	              	+ "))) ";
  	    	  }
  	      	}
  	      // DBSY-569 SW
  	      else if(strAccessLevel.equals(DebisysConstants.AGENT))
  	      {
  	    	strSQL = strSQL + " wt.rep_id in ";
  	    	strSQL = strSQL + " (select rep_id from reps WITH (NOLOCK) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id in "
            	+ "(select rep_id from reps (nolock) where type = "	+ DebisysConstants.REP_TYPE_SUBAGENT + " and iso_id = " + strRefId + ")) ";
  	      }
  	      else if(strAccessLevel.equals(DebisysConstants.SUBAGENT))
  	      {
  	    	strSQL = strSQL + " wt.rep_id in ";
  	    	strSQL = strSQL + " (select rep_id from reps WITH (NOLOCK) where type=" + DebisysConstants.REP_TYPE_REP + " and iso_id = " + strRefId + ") ";
  	      }
  	      // END DBSY-569 SW
  	      else if (strAccessLevel.equals(DebisysConstants.REP)){
  	    	  strSQL += " wt.rep_id = "+strRefId;
  	      }
  	      else if (strAccessLevel.equals(DebisysConstants.MERCHANT)){
	    	  strSQL += " wt.merchant_id = "+strRefId;
	      }

  	      
  	      	strSQL = strSQL + " AND (wt.datetime >= '" + this.start_date
  	          + "' and  wt.datetime < '" + DateUtil.addSubtractDays(this.end_date, 1)
  	          + "') ";

  	      	strSQL += " and wt.accepted_currency is not null";
  	      	strSQL += " and wt.source_country_code = '" + this.sourceCountry + "'";
  	      	strSQL += " and wt.target_country_code = '" + this.targetCountry + "'";
     
  	      	pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
  	      	cat.debug(strSQL);
  	      	ResultSet rs = pstmt.executeQuery();
  	      	int intRecordCount = DbUtil.getRecordCount(rs);
  	      	//first row is always the count
  	      	vecDetail.add(Integer.valueOf(intRecordCount));

  	      	if (intRecordCount > 0)
  	      	{
  	      		rs.absolute(DbUtil.getRowNumber(rs,intRecordsPerPage,intRecordCount,intPageNumber));
  	      		for (int i=0;i<intRecordsPerPage;i++)
  	      		{ 
  	      			Vector vecTemp = new Vector();
  	      			vecTemp.add(StringUtil.toString(rs.getString("rec_id")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("millennium_no")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("dba")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
  	      			vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("datetime")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("phys_city") + ","
  	    			  + StringUtil.toString(rs.getString("phys_county"))));
	  	      		if (rs.getString("clerk_name") != null && !rs.getString("clerk_name").equals(""))
	  	      		{
	  	      				vecTemp.add(StringUtil.toString(rs.getString("clerk_name")));
	  	      		}
	  	      		else
	  	      		{
	  	      			vecTemp.add("N/A");
	  	      		}
  	      			
  	      			// ani
  	      			vecTemp.add(StringUtil.toString(rs.getString("ani")));
  	      			// password
  	      			vecTemp.add(StringUtil.toString(rs.getString("password")));
  	      			vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
  	      			// balance
  	      			vecTemp.add(NumberUtil.formatCurrency(rs.getString("balance")));
  	      			// product id/description
  	      			vecTemp.add(StringUtil.toString(rs.getString("id") + "/"
  	    			  + rs.getString("description")));
  	      			vecTemp.add(StringUtil.toString(rs.getString("control_no")));
  	      			String other_info = StringUtil.toString(rs.getString("other_info"));
  	      			if(other_info.trim() != "")
  	      			{
  	      				if(other_info.length() > 6)
  	      				{			    	  
  	      					other_info = other_info.substring((other_info.length()-6), other_info.length());
  	      				}
  	      			}
  	      			vecTemp.add(other_info); 
  	      			vecTemp.add(StringUtil.toString(rs.getString("phys_state")));           
  	      			vecDetail.add(vecTemp);
  	      			if ( !rs.next() )
  	      			{
  	      				break;
  	      			}	
  	      		}
  	      	}
      pstmt.close();
      rs.close();
    }
    catch (Exception e)
    {
      cat.error("Error during getCrossBorderDetails", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        Torque.closeConnection(dbConn);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    return vecDetail;
  }//End of function getCrossBorderDetails

  public String downloadCrossBorderDetails(ServletContext context, SessionData sessionData) throws TransactionException
  {
    long start = System.currentTimeMillis();
    String strFileName = "";
    String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
    String downloadPath = DebisysConfigListener.getDownloadPath(context);
    String workingDir = DebisysConfigListener.getWorkingDir(context);
    String filePrefix = DebisysConfigListener.getFilePrefix(context);

    try
    {
  	  Vector vData = null;
  	  vData = this.getCrossBorderDetails(1, Integer.MAX_VALUE, sessionData);
  	  
      long startQuery = 0;
      long endQuery = 0;
      startQuery = System.currentTimeMillis();
      BufferedWriter outputFile = null;
      try
      {
        strFileName = (new TransactionSearch()).generateFileName(context);
        outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
        cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
        outputFile.flush();

        for ( int i = 1; i < vData.size(); i++ )
        {
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(0) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(1) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(2) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(3) + "\",");
  			if (DebisysConfigListener.getDeploymentType(context).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
  			{
  		    	  outputFile.write("\"" + ((Vector)vData.get(i)).get(14) + "\",");
  			}
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(4) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(5) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(6) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(7) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(8) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(9) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(10) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(11) + "\",");
      	  outputFile.write("\"" + ((Vector)vData.get(i)).get(12) + "\",");
  			if (DebisysConfigListener.getCustomConfigType(context).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
  			{
  		    	  outputFile.write("\"" + ((Vector)vData.get(i)).get(13) + "\",");
  			}
      	  outputFile.write("\r\n");
        }
  	  outputFile.write("\r\n");
        outputFile.flush();
        outputFile.close();
        endQuery = System.currentTimeMillis();
        cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
        startQuery = System.currentTimeMillis();
        File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
        long size = sourceFile.length();
        byte[] buffer = new byte[(int) size];
        String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
        cat.debug("Zip File Name: " + zipFileName);
        downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
        cat.debug("Download URL: " + downloadUrl);

        try
        {

          ZipOutputStream out =
              new ZipOutputStream(new FileOutputStream(zipFileName));

          // Set the compression ratio
          out.setLevel(Deflater.DEFAULT_COMPRESSION);
          FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

          // Add ZIP entry to output stream.
          out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

          // Transfer bytes from the current file to the ZIP file
          //out.write(buffer, 0, in.read(buffer));

          int len;
          while ((len = in.read(buffer)) > 0)
          {
            out.write(buffer, 0, len);
          }

          // Close the current entry
          out.closeEntry();
          // Close the current file input stream
          in.close();
          out.close();
        }
        catch (IllegalArgumentException iae)
        {
          iae.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
          fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
          ioe.printStackTrace();
        }
        sourceFile.delete();
        endQuery = System.currentTimeMillis();
        cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");


      }
      catch (IOException ioe)
      {
        try
        {
          if (outputFile != null)
          {
            outputFile.close();
          }
        }
        catch (IOException ioe2)
        {
        }
      }


    }
    catch (Exception e)
    {
      cat.error("Error during downloadCrossBorderDetails", e);
      throw new TransactionException();
    }
    long end = System.currentTimeMillis();

    // Display the elapsed time to the standard output
    cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");

    return downloadUrl;
  }//End of function downloadCrossBorderDetails
  /*END Release 13.0 - Added by LF*/
	
	/***
	 * DBSY-890 SW
	 * @param context
	 * @param sessionData
	 * @return
	 * @throws TransactionException
	 */
	public String downloadCarrierUserRoamingETopUps(ServletContext context, SessionData sessionData) throws TransactionException
	{
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		String deploymentType = DebisysConfigListener.getDeploymentType(context);
		String customConfigType = DebisysConfigListener.getCustomConfigType(context);

		try
		{
			Vector<Vector<String>> vData = new Vector<Vector<String>>();
			
			vData = this.getCarrierUserCrossBorderSummary(sessionData);

			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
		
			try
			{
	        	strFileName = (new TransactionSearch()).generateFileName(context);
	        	outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
	        	cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
	        	outputFile.flush();

	        	// Column Headers
	        	outputFile.write("\"" + "Cross Border Summary Report from" + sessionData.getProperty("startDate") + " to " + sessionData.getProperty("endDate") + "\",");
          		outputFile.write("\r\n");
          		outputFile.write("\"" + Languages.getString("jsp.admin.reports.description", sessionData.getLanguage()) + "\",");
          		outputFile.write("\"" + Languages.getString("jsp.admin.reports.crossborder.summary.qty", sessionData.getLanguage()) + "\",");
          		outputFile.write("\"" + Languages.getString("jsp.admin.reports.crossborder.summary.total_source", sessionData.getLanguage()) + "\",");
          		outputFile.write("\"" + Languages.getString("jsp.admin.reports.crossborder.summary.avg_trans", sessionData.getLanguage()) + "\",");
          		outputFile.write("\"" + Languages.getString("jsp.admin.reports.crossborder.summary.unique_customers", sessionData.getLanguage()) + "\",");
          		outputFile.write("\"" + Languages.getString("jsp.admin.reports.crossborder.summary.avg_user", sessionData.getLanguage()) + "\",");
          		outputFile.write("\"" + Languages.getString("jsp.admin.reports.crossborder.summary.total_target", sessionData.getLanguage()) + "\",");
          		outputFile.write("\r\n");
          		// End Column Headers

        		String prevCountry = "";
        		//store sums
        		double currTransCountSum = 0;
        		double currSourceTotalSum = 0;
        		double currTargetTotalSum = 0;
        		double currUniqueCustomersSum = 0;
          		
	        	for (Vector<String> row : vData)
	        	{
	        		if (prevCountry == "" || !prevCountry.equals(row.get(0)))
	                {
	        			outputFile.write("\"" + row.get(0) + "\",");
	    	            outputFile.write("\r\n");
	    				prevCountry = row.get(0);
	    			}
	    			else
	                {
	                  	prevCountry = row.get(0);
	                }

	                double currTransCount = Double.parseDouble(row.get(2));
	                double currSourceTotal = Double.parseDouble(row.get(3));
	                double currTargetTotal = Double.parseDouble(row.get(5));
	                double currUniqueCustomers = Double.parseDouble(row.get(7));

	                double avgPerTrans = currSourceTotal/currTransCount;
	                double avgPerUser  = currSourceTotal/currUniqueCustomers;
	          
	                //add to running total
	                currTransCountSum = currTransCountSum + currTransCount;
	                currSourceTotalSum = currSourceTotalSum + currSourceTotal;
	                currTargetTotalSum = currTargetTotalSum + currTargetTotal;
	                currUniqueCustomersSum = currUniqueCustomersSum + currUniqueCustomers;
	                
	                outputFile.write("\"   " + row.get(1) + "\",");
	                outputFile.write("\"" + row.get(2) + "\",");
	                outputFile.write("\"" + NumberUtil.formatCurrency(row.get(3))+ " " +row.get(4) + "\",");
	                outputFile.write("\"" + NumberUtil.formatCurrency(Double.toString(avgPerTrans))+ " " + row.get(4) + "\",");
	                outputFile.write("\"" + row.get(7).toString() + "\",");
	                outputFile.write("\"" + NumberUtil.formatCurrency(Double.toString(avgPerUser))+ " " + row.get(4) + "\",");
	                outputFile.write("\"" + NumberUtil.formatCurrency(row.get(5))+ " " +row.get(6) + "\",");
    	            outputFile.write("\r\n");
	        	}
            
	        	outputFile.write("\"Totals\",");
	        	outputFile.write("\"" + (int)currTransCountSum + "\",");
	        	outputFile.write("\"" + NumberUtil.formatCurrency(Double.toString(currSourceTotalSum)) + "\",");
	        	outputFile.write("\" \",");
	        	outputFile.write("\"" + (int)currUniqueCustomersSum + "\",");
	        	outputFile.write("\" \",");
	        	outputFile.write("\" \",");
	            outputFile.write("\r\n");
          		
          		
	        	outputFile.flush();
	        	outputFile.close();
	        	endQuery = System.currentTimeMillis();
	        	
	        	cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
	        	startQuery = System.currentTimeMillis();
	        	File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
	        	long size = sourceFile.length();
	        	byte[] buffer = new byte[(int) size];
	        	String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
	        	cat.debug("Zip File Name: " + zipFileName);
	        	downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
	        	cat.debug("Download URL: " + downloadUrl);

	        	try
	        	{

	        		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));

	        		// Set the compression ratio
	        		out.setLevel(Deflater.DEFAULT_COMPRESSION);
	        		FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");

	        		// Add ZIP entry to output stream.
	        		out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));

	        		// Transfer bytes from the current file to the ZIP file
	        		// out.write(buffer, 0, in.read(buffer));
	        		int len;
	        		while ((len = in.read(buffer)) > 0)
	        		{
	        			out.write(buffer, 0, len);
	        		}

	        		// Close the current entry
	        		out.closeEntry();
	        		// Close the current file input stream
	        		in.close();
	        		out.close();
	        	}
	        	catch (IllegalArgumentException iae)
	        	{
	        		iae.printStackTrace();
	        	}
	        	catch (FileNotFoundException fnfe)
	        	{
	        		fnfe.printStackTrace();
	        	}
	        	catch (IOException ioe)
	        	{
	        		ioe.printStackTrace();
	        	}
	        	sourceFile.delete();
	        	endQuery = System.currentTimeMillis();
	        	cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
			}
			catch (IOException ioe)
			{
				cat.error("Error on I/O operation: " + ioe.getClass().getName() + " (" + ioe.getMessage() + ") ", ioe);
				try
				{
					if (outputFile != null)
					{
	  						outputFile.close();
					}
				}
				catch (IOException ioe2)
				{
				}
			}
		}
		catch (Exception e)
		{
			cat.error("Error during downloadCarrierUserRoamingETopUps", e);
			throw new TransactionException();
		}
		long end = System.currentTimeMillis();

		// Display the elapsed time to the standard output
		cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");

		return downloadUrl;
  }// End of function downloadCarrierUserRoamingETopUps
}
