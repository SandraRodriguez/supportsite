/**
 * 
 */
package com.debisys.reports.reconciliation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DbUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;

/**
 * @author nmartinez
 *
 */
public class ReconciliationWithBonus {
	
	static Category cat = Category.getInstance(ReconciliationWithBonus.class);
    private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.reconciliation.sql");

	  
	private String date;
	private String trxType;
	private String amnTrx;
	private String trxPerDay;
	private String totalRecharge;
	private String totalBonusXday;
	private String totalAmt;
	private String transactionId;
	private String merchantId;
	private int id;
	private String title;
	private String note;
	private String warnnigs;
	
	private ArrayList<ReconciliationWithBonus> arrReport;
	private String startDate="";
	private String endDate="";
	private boolean summaryOrDetailed;
	private boolean download;
	private String downloadUrl;
	private ServletContext context;
	private SessionData sessionData;
	private int nPageNumber;
	private int recordCount;
	private int nRecordsPerPage;
	
	/**
	 * @return the totalRecharge
	 */
	public String getTotalRecharge() {
		return totalRecharge;
	}

	/**
	 * @param totalRecharge the totalRecharge to set
	 */
	public void setTotalRecharge(String totalRecharge) {
		this.totalRecharge = totalRecharge;
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the warnnigs
	 */
	public String getWarnnigs() {
		return warnnigs;
	}

	/**
	 * @param warnnigs the warnnigs to set
	 */
	public void setWarnnigs(String warnnigs) {
		this.warnnigs = warnnigs;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}	
	
	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the trxType
	 */
	public String getTrxType() {
		return trxType;
	}

	/**
	 * @param trxType the trxType to set
	 */
	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}

	/**
	 * @return the amnTrx
	 */
	public String getAmnTrx() {
		return amnTrx;
	}

	/**
	 * @param amnTrx the amnTrx to set
	 */
	public void setAmnTrx(String amnTrx) {
		this.amnTrx = amnTrx;
	}

	/**
	 * @return the trxPerDay
	 */
	public String getTrxPerDay() {
		return trxPerDay;
	}

	/**
	 * @param trxPerDay the trxPerDay to set
	 */
	public void setTrxPerDay(String trxPerDay) {
		this.trxPerDay = trxPerDay;
	}
	

	/**
	 * @return the totalBonusXday
	 */
	public String getTotalBonusXday() {
		return totalBonusXday;
	}

	/**
	 * @param totalBonusXday the totalBonusXday to set
	 */
	public void setTotalBonusXday(String totalBonusXday) {
		this.totalBonusXday = totalBonusXday;
	}

	/**
	 * @return the totalAmt
	 */
	public String getTotalAmt() {
		return totalAmt;
	}

	/**
	 * @param totalAmt the totalAmt to set
	 */
	public void setTotalAmt(String totalAmt) {
		this.totalAmt = totalAmt;
	}

	/**
	 * @return the arrReport
	 */
	public ArrayList<ReconciliationWithBonus> getArrReport() {
		return arrReport;
	}

	/**
	 * @param arrReport the arrReport to set
	 */
	public void setArrReport(ArrayList<ReconciliationWithBonus> arrReport) {
		this.arrReport = arrReport;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the summaryOrDetailed
	 */
	public boolean isSummaryOrDetailed() {
		return summaryOrDetailed;
	}

	/**
	 * 1 Summary - 0 Detailed
	 * @param summaryOrDetailed the summaryOrDetailed to set
	 */
	public void setSummaryOrDetailed(boolean summaryOrDetailed) {
		this.summaryOrDetailed = summaryOrDetailed;
	}
	
	/**
	 * @return the downloadUrl
	 */
	public String getDownloadUrl() {
		return downloadUrl;
	}

	/**
	 * @param downloadUrl the downloadUrl to set
	 */
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	/**
	 * @return the download
	 */
	public boolean isDownload() {
		return download;
	}

	/**
	 * @param download the download to set
	 */
	public void setDownload(boolean download) {
		this.download = download;
	}
	
	/**
	 * @return the nPageNumber
	 */
	public int getnPageNumber() {
		return nPageNumber;
	}

	/**
	 * @param nPageNumber the nPageNumber to set
	 */
	public void setnPageNumber(int nPageNumber) {
		this.nPageNumber = nPageNumber;
	}
	
	/**
	 * @return the nRecordsPerPage
	 */
	public int getnRecordsPerPage() {
		return nRecordsPerPage;
	}

	/**
	 * @param nRecordsPerPage the nRecordsPerPage to set
	 */
	public void setnRecordsPerPage(int nRecordsPerPage) {
		this.nRecordsPerPage = nRecordsPerPage;
	}
	
	
	public int recordCountLastQuery(){
		return recordCount;
	}
	
	/**
	 * @param context the context to set
	 */
	public void setContext(ServletContext context) {
		this.context = context;
	}

	/**
	 * @return the context
	 */
	public ServletContext getContext() {
		return context;
	}

	/**
	 * @param sessionData the sessionData to set
	 */
	public void setSessionData(SessionData sessionData) {
		this.sessionData = sessionData;
	}

	/**
	 * @return the sessionData
	 */
	public SessionData getSessionData() {
		return sessionData;
	}
	
	public void getReconciliationWithBonusReport( )
	{		
		executeReport();
	}
	
	public void getReconciliationWithBonusReportDownload()
	{
		executeReport();
	}
	
	public void executeReport()    
	{	
	  long start = System.currentTimeMillis();
	  Connection dbConn = null;
	  String strSQL = "";
	  //String strFileName = "";
	
	  downloadUrl = DebisysConfigListener.getDownloadUrl(context);
	  
	  //String deploymentType = DebisysConfigListener.getDeploymentType(context);
	  //String customConfigType = DebisysConfigListener.getCustomConfigType(context);
	  String strRefId = sessionData.getProperty("ref_id");
	  String strAccessLevel = sessionData.getProperty("access_level");
	  String strDistChainType = sessionData.getProperty("dist_chain_type");
	  PreparedStatement pstmt = null;
	  ResultSet rs = null;
	  
	  if ( !strAccessLevel.equals(DebisysConstants.ISO))
	  {
		  return ;
	  }
	  try
	  {
	    dbConn = Torque.getConnection(sql_bundle.getString("pkgDefaultDb"));
	    if (dbConn == null)
	    {
	      cat.error("[ReconciliationWithBonus] Can't get database connection");
	      throw new TransactionException();
	    }
	    String instance = DebisysConfigListener.getInstance(context);
	    pstmt = dbConn.prepareStatement("SELECT [value] FROM properties WITH (NOLOCK) WHERE project = 'support' AND module = 'global' AND property = 'RecordsPerPageReconciliationWithBonusReport' AND instance = '"+instance+"'");
	    rs = pstmt.executeQuery();
	    if ( rs.next() )
	    {
	    	try
	  	    {
	    		nRecordsPerPage = rs.getInt(1);	
	  	    }
	    	catch(Exception e)
	  	    {
	    		nRecordsPerPage = 10;
	    		cat.debug("Property RecordsPerPageReconciliationWithBonusReport is badly setup, by default is 10");
	  	    }	    	
	    }
	    else
	    {
	    	nRecordsPerPage = 10;
	    	cat.debug("Property RecordsPerPageReconciliationWithBonusReport has not been setup, by default is 10");
	    }
	    rs.close();
	    pstmt.close();
	    
	    int DaysBackReconciliationWithBonusReport=10;
	    pstmt = dbConn.prepareStatement("SELECT [value] FROM properties WITH (NOLOCK) WHERE project = 'support' AND module = 'global' AND property = 'DaysBackReconciliationWithBonusReport' AND instance = '"+instance+"'");
	    rs = pstmt.executeQuery();
	    if ( rs.next() )
	    {
	    	try
	  	    {
	    		DaysBackReconciliationWithBonusReport = rs.getInt(1);	
	  	    }
	    	catch(Exception e)
	  	    {
	    		DaysBackReconciliationWithBonusReport = 10;
	    		cat.debug("Property DaysBackReconciliationWithBonusReport is badly setup, by default is 10");
	  	    }	    	
	    }
	    else
	    {
	    	DaysBackReconciliationWithBonusReport = 10;
	    	cat.debug("Property DaysBackReconciliationWithBonusReport has not been setup, by default is 10");
	    }
	    rs.close();
	    pstmt.close();
	    
	    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
    	int daysBetween = DateUtil.getDaysBetween(this.endDate, this.startDate);
		if ( daysBetween > DaysBackReconciliationWithBonusReport)
		{					   
			this.startDate = DateUtil.addSubtractDays(this.endDate,-DaysBackReconciliationWithBonusReport);
			this.warnnigs = Languages.getString("jsp.admin.customers.noteHistory1",sessionData.getLanguage())+ " "+ DaysBackReconciliationWithBonusReport +" "+ Languages.getString("jsp.admin.customers.noteHistory2",sessionData.getLanguage());
		}
		String tmpEndDate = DateUtil.addSubtractDays(this.endDate,1);
		//this.endDate = DateUtil.addSubtractDays(this.endDate,1);
		Date date1 = format.parse(this.startDate);
		Date date2 = format.parse(tmpEndDate);
		format.applyPattern("MM/dd/yyyy"); //yyyy-MM-dd
		this.startDate = format.format(date1);
		//this.endDate = format.format(date2);
		tmpEndDate = format.format(date2);
		
	    if ( this.summaryOrDetailed )  
	    {
	    	cat.debug("Summary Reconciliation With Bonus Report selected");
	    	if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
			{
				strSQL = sql_bundle.getString("summaryReconciliationWithBonusISO3Levels");
			}
			else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			{
				strSQL = sql_bundle.getString("summaryReconciliationWithBonusISO5Levels");
			}	    
	    }		   
		else
		{	
			cat.debug("Detailed Reconciliation With Bonus Report selected");
			if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
			{
				strSQL = sql_bundle.getString("detailedReconciliationWithBonusISO3Levels");
			}
			else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			{
				strSQL = sql_bundle.getString("detailedReconciliationWithBonusISO5Levels");
			}						
		}
		
		if ( this.download )    
		   pstmt = dbConn.prepareStatement(strSQL);
		else
		   pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				
		cat.debug("******************************************");
	    cat.debug(strSQL);
	    cat.debug("Parameters");
	    cat.debug("strRefId "+strRefId);
	    cat.debug("startDate "+startDate);
	    cat.debug("endDate "+tmpEndDate);
	    cat.debug("******************************************");
	    
	    long startQuery = System.currentTimeMillis();
	    pstmt.setString(1, strRefId);
	    pstmt.setString(2, startDate);
		pstmt.setString(3, tmpEndDate);
			    
	    long endQuery = System.currentTimeMillis();
	    rs = pstmt.executeQuery();
	    cat.debug("Query: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
	    startQuery = System.currentTimeMillis();
		 
	    if ( this.download )
	    {
	    	generateFileToDownload(startQuery, rs, endQuery);	
	    }
	    else
	    {
	    	generateReport(rs);	
	    }   		    
	    
	  }
	  catch (Exception e)
	  {
	    cat.error("Error during download: ", e);	    
	  }
	  finally
	  {		  
	    try
	    {
	      if ( pstmt!= null)
	    	  pstmt.close();
	      
	      if ( rs != null )
	    	  rs.close();
	      
	      Torque.closeConnection(dbConn);
	    }
	    catch (Exception e)
	    {
	      cat.error("Error during closeConnection", e);
	    }
	  }
	  long end = System.currentTimeMillis();
	  // Display the elapsed time to the standard output
	  cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
	  
	}

	private void generateReport(ResultSet rs) throws SQLException
	{
		ArrayList<ReconciliationWithBonus> arrReportBonus = new ArrayList<ReconciliationWithBonus>();
		recordCount = DbUtil.getRecordCount(rs);
		if (recordCount > 0)
		{	
			rs.absolute(DbUtil.getRowNumber(rs, nRecordsPerPage, recordCount, nPageNumber) - 1);
			int recordIndex = ((nPageNumber * nRecordsPerPage)-20)+1;
			for (int i = 0; i < nRecordsPerPage && rs.next(); i++)
			{
				ReconciliationWithBonus reconWithBonus = new ReconciliationWithBonus();
				reconWithBonus.setId(recordIndex);
				
				if ( this.summaryOrDetailed )  
			    {
					//1 CONVERT(VARCHAR(10), wt.datetime, 103) date, 
					//2 c.short_name, 
					//3 wt.amount, 
					//4 COUNT(*) trx, 
					//5 SUM(wt.amount) totalAmount ,
					//6 SUM(wt.bonus_amount) totalBonus,  
					//7 SUM((wt.bonus_amount + wt.amount)) as total_recharge
					reconWithBonus.setDate( rs.getString(1) );
					reconWithBonus.setTrxType( rs.getString(2) );
					reconWithBonus.setAmnTrx( NumberUtil.formatCurrency(rs.getString(3),true) );
					reconWithBonus.setTrxPerDay( rs.getString(4) );
					reconWithBonus.setTotalAmt( NumberUtil.formatCurrency(rs.getString(5),true) );
					reconWithBonus.setTotalBonusXday( NumberUtil.formatCurrency(rs.getString(6),true) );
					reconWithBonus.setTotalRecharge( NumberUtil.formatCurrency(rs.getString(7),true) );
					arrReportBonus.add(reconWithBonus);
			    }
				else
				{	
					reconWithBonus.setDate( rs.getString(1) );
					reconWithBonus.setTrxType( rs.getString(2) );
					reconWithBonus.setAmnTrx( NumberUtil.formatCurrency(rs.getString(3),true) );
					reconWithBonus.setTransactionId( rs.getString(4) );
					reconWithBonus.setMerchantId( rs.getString(5) );
					arrReportBonus.add(reconWithBonus);
				}					
				recordIndex++;
			}
		}		
		if ( arrReportBonus.size() > 0 )
			this.arrReport = arrReportBonus;
		rs.close();
	}

	private void generateFileToDownload(long startQuery, ResultSet rs, long endQuery)
			throws SQLException 
	{
		String strFileName;
		FileChannel fc = null;
		
		try
	    {
		  String downloadPath = DebisysConfigListener.getDownloadPath(context);	
		  String workingDir = DebisysConfigListener.getWorkingDir(context);
		  String filePrefix = DebisysConfigListener.getFilePrefix(context);
		  
	      strFileName = this.generateFileName(context);
		  fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel();		
	      //outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
	      cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
	      
    	  this.defineTitlesForDownload(fc);
	      int icount=1;	      
	      while (rs.next())
	      {
	    	  if ( this.summaryOrDetailed )  
	  	      {	    		 
	    		 this.writetofile(fc,"\"" + icount + "\",");
		  	     this.writetofile(fc,"\"" + rs.getString(1) + "\",");
		  	     this.writetofile(fc,"\"" + rs.getString(2) + "\",");
		  	     this.writetofile(fc,"\"" + rs.getString(3) + "\",");
		  	     this.writetofile(fc,"\"" + rs.getString(4) + "\",");
		  	     this.writetofile(fc,"\"" + NumberUtil.formatCurrency(rs.getString(5),true) + "\",");
		  	     this.writetofile(fc,"\"" + NumberUtil.formatCurrency(rs.getString(6),true) + "\",");	        
		  	     this.writetofile(fc,"\"" + NumberUtil.formatCurrency(rs.getString(7),true) + "\",");
		  	     this.writetofile(fc,"\r\n");
	  	      }
	    	  else
	    	  {
	    		this.writetofile(fc,"\"" + icount + "\",");
	  	        this.writetofile(fc,"\"" + rs.getString(1) + "\",");
	  	        this.writetofile(fc,"\"" + rs.getString(2) + "\",");
	  	        this.writetofile(fc,"\"" + NumberUtil.formatCurrency(rs.getString(3),true) + "\",");
	  	        this.writetofile(fc,"\"" + rs.getString(4) + "\",");
	  	        this.writetofile(fc,"\"" + rs.getString(5) + "\",");	        
	  	        this.writetofile(fc,"\r\n");
	    	  }
	          icount++;
	      }
	      fc.close();	            
	      rs.close();
	      endQuery = System.currentTimeMillis();
	      cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
	      
	      startQuery = System.currentTimeMillis();
	      File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
	      long size = sourceFile.length();
	      byte[] buffer = new byte[(int) size];
	      String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
	      cat.debug("Zip File Name: " + zipFileName);
	      downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
	      cat.debug("Download URL: " + downloadUrl);
	
	      try
	      {
	        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
	
	        // Set the compression ratio
	        out.setLevel(Deflater.DEFAULT_COMPRESSION);
	        FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");
	
	        // Add ZIP entry to output stream.
	        out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
	
	        // Transfer bytes from the current file to the ZIP file
	        //out.write(buffer, 0, in.read(buffer));
	
	        int len;
	        while ((len = in.read(buffer)) > 0)
	        {
	          out.write(buffer, 0, len);
	        }
	
	        // Close the current entry
	        out.closeEntry();
	        // Close the current file input stream
	        in.close();
	        out.close();
	      }
	      catch (IllegalArgumentException iae)
	      {
	        iae.printStackTrace();
	      }
	      catch (FileNotFoundException fnfe)
	      {
	        fnfe.printStackTrace();
	      }
	      catch (IOException ioe)
	      {
	        ioe.printStackTrace();
	      }
	      sourceFile.delete();
	      endQuery = System.currentTimeMillis();
	      cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
		 
	    }
	    catch (IOException ioe)
	    {
		    cat.error("Method [generateFileToDownload] IOException 1: ", ioe);
	      try
	      {
	        if (fc != null)
	        {
	          fc.close();
	        }
	      }
	      catch (IOException ioe2)
	      {
	      	cat.error("Method [generateFileToDownload] IOException 2: ", ioe2);
	      }
	    }
		//downloadUrl;
	}
	
	private void writetofile(FileChannel fc,String c) throws IOException
	{
	      fc.write(ByteBuffer.wrap(c.getBytes()));
	}
		
	private void defineTitlesForDownload(FileChannel fc) throws IOException
	{
		if ( this.title != null)
		{
			this.writetofile(fc,this.title);
			this.writetofile(fc,"\r\n");
		}			
			
		if ( this.note != null)
		{
			this.writetofile(fc,this.note);
			this.writetofile(fc,"\r\n");
		}
		
		if ( this.warnnigs != null)
		{
			this.writetofile(fc,this.warnnigs);
			this.writetofile(fc,"\r\n");
		}
		
		this.writetofile(fc,Languages.getString("jsp.admin.start_date",sessionData.getLanguage())+":"+this.startDate);
		this.writetofile(fc,"\r\n");
		this.writetofile(fc,Languages.getString("jsp.admin.end_date",sessionData.getLanguage())+":"+this.endDate);
		this.writetofile(fc,"\r\n");
		
		if ( this.summaryOrDetailed )  
	    {
			this.writetofile(fc,"\"#\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.date", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.type", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.value", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.TrxDay", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.TotAmntExcluding", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.TotBnMinDay", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.TotSAmntIncluding", sessionData.getLanguage()) + "\",");
	    }
		else
		{
			this.writetofile(fc,"\"#\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.date", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.type", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.value", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.TrxNumber", sessionData.getLanguage()) + "\",");
			this.writetofile(fc,"\"" + Languages.getString("jsp.admin.reports.reconciliation.withbonus.MerchantId", sessionData.getLanguage()) + "\",");
		}
		this.writetofile(fc,"\r\n");
	}
		
	public String generateFileName(ServletContext context)
	{
	    boolean isUnique = false;
	    String strFileName = "";
	    while (!isUnique)
	    {
	      strFileName = this.generateRandomNumber();
	      isUnique = this.checkFileName(strFileName, context);
	    }
	    return strFileName;
	}
	
	private String generateRandomNumber()
	{
	    StringBuffer s = new StringBuffer();
	    // number between 1-9 because first digit must not be 0
	    int nextInt = (int) ((Math.random() * 9) + 1);
	    s = s.append(nextInt);

	    for (int i = 0; i <= 10; i++)
	    {
	      //number between 0-9
	      nextInt = (int) (Math.random() * 10);
	      s = s.append(nextInt);
	    }

	    return s.toString();
	}
	
	private boolean checkFileName(String strFileName, ServletContext context)
	{
	    boolean isUnique = true;
	    String downloadPath = DebisysConfigListener.getDownloadPath(context);
	    String workingDir = DebisysConfigListener.getWorkingDir(context);
	    String filePrefix = DebisysConfigListener.getFilePrefix(context);
	
	    File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
	    if (f.exists())
	    {
	      //duplicate file found
	      cat.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
	      return false;
	    }
	
	    f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
	    if (f.exists())
	    {
	      //duplicate file found
	      cat.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
	      return false;
	    }
	
	    return isUnique;
	}
	
}
