/**
 * 
 */
package com.debisys.reports.summarys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;

import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.reports.pojo.ReportTrxSummaryByAmount;
import com.debisys.reports.pojo.TrxSummaryByAmountPojo;
import com.debisys.schedulereports.ScheduleConfigurationJob;
import com.debisys.users.SessionData;
import com.debisys.util.NumberUtil;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TimeZone;

/**
 * @author nmartinez
 *
 */
public class DownloadsSummary {
	
	private static Logger cat = Logger.getLogger(ScheduleConfigurationJob.class);
	
	//private String strFileName=null;
	private FileChannel fc=null;
	
	
	/**
	 * @param fc
	 * @param c
	 * @throws IOException
	 */
	private void writetofile(FileChannel fc,String c) throws IOException{
		fc.write(ByteBuffer.wrap(c.getBytes()));
	}
	
	
	 /**
	 * @param strAmount
	 * @return
	 */
	private static double formatAmountDown(String strAmount)
	 {
		 Number Currency = 0;
	    if (strAmount != null && !strAmount.equals(""))
	    {
	      try
	      {
	        DecimalFormat df = new DecimalFormat("###0.00");
	        Currency = df.parse( strAmount );
	      } 
	      catch (Exception nfe)
	      {
	        Currency = 0.00;
	      }
	    } 
	    else
	    {
	    	Currency = 0.00;
	    }
	    return Currency.doubleValue();
	 }
	  
	
	/**
	 * @param rs
	 * @param sessionData
	 * @param application
	 * @param headers
	 * @param strFileName
	 * @param titles
	 * @param totalLabel
	 * @param blankSpacesTotal
	 * @return
	 * @throws TransactionException
	 * @throws SQLException
	 * @throws IOException
	 */	
	public String download( ResultSet rs, SessionData sessionData, ServletContext application, ArrayList<ColumnReport> headers,  
							String strFileName,  ArrayList<String> titles, String totalLabel, int blankSpacesTotal) 
	throws TransactionException, SQLException, IOException
	{
		  String downloadUrl = DebisysConfigListener.getDownloadUrl(application);
		  String downloadPath = DebisysConfigListener.getDownloadPath(application);
		  String workingDir = DebisysConfigListener.getWorkingDir(application);
		  String filePrefix = DebisysConfigListener.getFilePrefix(application);
		  
		  if ( fc == null)
		  {			  
			  fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel();
		  }		   
		  HashMap<String, Double> totalsFields = new HashMap<String, Double>();
		  boolean addWarningMessage = false;
		  for( String titlesInfo : titles)
		  {
			  writetofile( fc, titlesInfo + ",");
			  writetofile(fc,"\r\n");	
		  }	
		  writetofile(fc,"\r\n");
		  int whiteSpaceBeforeTotals = 0;
		  int countHeaders=0;
		  for( ColumnReport colum : headers)
		  {			  
			  writetofile( fc, colum.getLanguageDescription()+ ",");
			  if ( whiteSpaceBeforeTotals == 0 && colum.isSumValue() )
				  whiteSpaceBeforeTotals = countHeaders;
			  countHeaders++;
		  }		  
		  writetofile(fc,"\r\n");		  
		  while (rs.next())
		  {			
			for( ColumnReport colum : headers)
			{
				String columnName = colum.getSqlName();
				String columnValue = rs.getString( columnName );
				
				if ( columnValue == null)
					columnValue ="";
				if ( colum.getClassNameData().equals(java.lang.Double.class) ) 
					writetofile( fc, "\"" + NumberUtil.formatCurrency( NumberUtil.formatAmount(columnValue) ) + "\"," );
				else
					writetofile( fc, "\"" + columnValue + "\"," );
				if ( colum.isSumValue() )
				{
					double val = Double.valueOf(columnValue);
					
					if ( totalsFields.containsKey( columnName ) )
					{
						Double partialValue = totalsFields.get(columnName);
						partialValue += formatAmountDown( columnValue );
						totalsFields.put( columnName , partialValue );
					}
					else
					{
						totalsFields.put( columnName , val );
					}
				}
			}
			writetofile(fc,"\r\n");
		  }
		  //blankSpacesTotal
		  whiteSpaceBeforeTotals--;//the total label
		  for(int j=0; j<whiteSpaceBeforeTotals; j++)
			  writetofile( fc, "\"\"," );

		  if ( totalLabel != null)
			  writetofile( fc, totalLabel+":," );
		  
		  for( ColumnReport colum : headers)
		  {
				String columnName = colum.getSqlName();
				if ( totalsFields.containsKey( columnName ) )
				{
					Double partialValue = totalsFields.get(columnName);
					if ( colum.getClassNameData().equals(java.lang.Double.class) )
						writetofile( fc, "\"" + NumberUtil.formatCurrency( NumberUtil.formatAmount( partialValue.toString() ) ) + "\"," );
					else
						writetofile( fc, "\"" + NumberUtil.formatAmount( partialValue.toString() ) + "\"," );
				}
		  }	
		  if ( addWarningMessage )
		  {
			  writetofile(fc, Languages.getString("jsp.admin.reports.taxtypewarning", sessionData.getLanguage()) );			  
		  }			  
		  writetofile(fc,"\r\n");	  
		  
		  fc.close();
		  
		  downloadUrl = createZipFile(strFileName, downloadUrl, downloadPath, workingDir, filePrefix);
		  
		  return downloadUrl;          
	  }


	
	/**
	 * @param rs
	 * @param sessionData
	 * @param application
	 * @param headers
	 * @param strFileName
	 * @param titles
	 * @param blankSpacesTotal
	 * @return
	 * @throws TransactionException
	 * @throws SQLException
	 * @throws IOException
	 */
	public String downloadRepCredits( ResultSet rs, SessionData sessionData, ServletContext application, ArrayList<ColumnReport> headers,  
			String strFileName,  ArrayList<String> titles, int blankSpacesTotal) 
					throws TransactionException, SQLException, IOException
	{
		String downloadUrl = DebisysConfigListener.getDownloadUrl(application);
		String downloadPath = DebisysConfigListener.getDownloadPath(application);
		String workingDir = DebisysConfigListener.getWorkingDir(application);
		String filePrefix = DebisysConfigListener.getFilePrefix(application);
		
		if ( fc == null)
		{			  
			fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel();
		}		   
		HashMap<String, Double> totalsFields = new HashMap<String, Double>();
		boolean addWarningMessage = false;
		for( String titlesInfo : titles)
		{
			writetofile( fc, titlesInfo + ",");
			writetofile(fc,"\r\n");	
		}	
		writetofile(fc,"\r\n");
		int whiteSpaceBeforeTotals = 0;
		int countHeaders=0;
		for( ColumnReport colum : headers)
		{			  
			writetofile( fc, colum.getLanguageDescription()+ ",");
			if ( whiteSpaceBeforeTotals == 0 && colum.isSumValue() )
			  whiteSpaceBeforeTotals = countHeaders;
			countHeaders++;
		}		  
		writetofile(fc,"\r\n");	
		
		Double previousAvailableCredit = -1D;
		Double AvailableCredit = 0d;
		while (rs.next())
		{		
			Vector<String> vTimeZoneDate=null;
			try
			{
				vTimeZoneDate = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"),rs.getString("datetime"),rs.getString("datetime"), true);
				String logon_id = rs.getString("logon_id");				
				writetofile( fc, "\""+vTimeZoneDate.get(2)+"\"," );
				writetofile( fc, "\""+StringUtil.toString(logon_id)+"\"," );
				writetofile( fc, "\""+StringUtil.toString(rs.getString("description"))+"\"," );
				
				if ( logon_id.equals("Events Job") )
					writetofile( fc, "\""+StringUtil.toString("")+"\"," );
				else
					writetofile( fc, "\""+NumberUtil.formatCurrency(rs.getString("amount"))+"\"," );
								
				AvailableCredit = rs.getDouble("available_credit");
				if ((AvailableCredit!=null && !AvailableCredit.equals(previousAvailableCredit)) 
					|| 
					( rs.getString("logon_id") != null  && !rs.getString("logon_id").equals("Commission") && !rs.getString("logon_id").equals("Billing") )
				   )
				{
					writetofile( fc, "\""+NumberUtil.formatCurrency(rs.getString("available_credit"))+"\"," );				
				}
				else
				{
					writetofile( fc, "\""+StringUtil.toString("")+"\"," );
				}
				writetofile( fc, "\""+NumberUtil.formatAmount(rs.getString("commission"))+"\"," );
							
				if ( logon_id.equals("Events Job") )
					writetofile( fc, "\""+StringUtil.toString("")+"\"," );
				else
					writetofile( fc, "\""+NumberUtil.formatCurrency(rs.getString("net_payment"))+"\"," );
				
				previousAvailableCredit = AvailableCredit;
						
			}
			catch (Exception e) 
			{
				cat.error("downloadRepCredits Exception "+e);
			}
			writetofile(fc,"\r\n");
		}
		//blankSpacesTotal
		whiteSpaceBeforeTotals--;//the total label
		for(int j=0; j<whiteSpaceBeforeTotals; j++)
			writetofile( fc, "\"\"," );
		
					  
		//writetofile(fc,"\r\n");
		fc.close();
		downloadUrl = createZipFile(strFileName, downloadUrl, downloadPath, workingDir, filePrefix);
		return downloadUrl;          
	}
	
	
	/**
	 * @param rs
	 * @param sessionData
	 * @param application
	 * @param headers
	 * @param strFileName
	 * @param titles
	 * @param blankSpacesTotal
	 * @return
	 * @throws TransactionException
	 * @throws SQLException
	 * @throws IOException
	 */
	public String downloadMerchantCredits( ResultSet rs, SessionData sessionData, ServletContext application, List<ColumnReport> headers,  
										   String strFileName,  List<String> titles, int blankSpacesTotal, boolean needsDBAName) 
										   throws TransactionException, SQLException, IOException
	{
		String downloadUrl = DebisysConfigListener.getDownloadUrl(application);
		String downloadPath = DebisysConfigListener.getDownloadPath(application);
		String workingDir = DebisysConfigListener.getWorkingDir(application);
		String filePrefix = DebisysConfigListener.getFilePrefix(application);
		boolean bIsExternalRep=false;
		bIsExternalRep = com.debisys.users.User.isExternalRepAllowedEnabled(sessionData);
		if ( fc == null)
		{			  
			fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel();
		}		   
		HashMap<String, Double> totalsFields = new HashMap<String, Double>();
		boolean addWarningMessage = false;
		for( String titlesInfo : titles)
		{
			writetofile( fc, titlesInfo + ",");
			writetofile(fc,"\r\n");	
		}	
		writetofile(fc,"\r\n");
		int whiteSpaceBeforeTotals = 0;
		int countHeaders=0;
		for( ColumnReport colum : headers)
		{			  
			writetofile( fc, colum.getLanguageDescription()+ ",");
			if ( whiteSpaceBeforeTotals == 0 && colum.isSumValue() )
			  whiteSpaceBeforeTotals = countHeaders;
			countHeaders++;
		}		  
		writetofile(fc,"\r\n");	
		
		Double previousBalance = -1D;
		Double balance;
		String exDBA;
		boolean externalPayment=false;
		while (rs.next())
		{		
			Vector<String> vTimeZoneDate=null;
			try 
			{
				vTimeZoneDate = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"),rs.getString("log_datetime"),rs.getString("log_datetime"), true);
				String logon_id = rs.getString("logon_id");
				writetofile( fc, "\""+vTimeZoneDate.get(2)+"\"," );
				writetofile( fc, "\""+logon_id+"\"," );
				writetofile( fc, "\""+StringUtil.toString(rs.getString("dba"))+"\"," );
				writetofile( fc, "\""+StringUtil.toString(rs.getString("description"))+"\"," );
								
				if (logon_id.equals("Events Job"))
					writetofile( fc, "\""+StringUtil.toString("")+"\"," );
				else
					writetofile( fc, "\""+NumberUtil.formatCurrency(rs.getString("payment"))+"\"," );
				
				balance = rs.getDouble("balance");
				if ( (balance != null && !balance.equals(previousBalance))
					  || 
					 (rs.getString("logon_id") != null && !rs.getString("logon_id").equals("Commission") && !rs.getString("logon_id").equals("Billing"))
				   )
				{				
					writetofile( fc, "\""+NumberUtil.formatCurrency(rs.getString("balance"))+"\"," );	
				}
				else
				{
					writetofile( fc, "\""+StringUtil.toString("")+"\"," );	
				}
	
				if (rs.getString("commission") == null)
				{
					writetofile( fc, "\""+StringUtil.toString("0.00")+"\"," );				
				}
				else
				{
					writetofile( fc, "\""+rs.getString("commission")+"\"," );				
				}
	
				if (logon_id.equals("Events Job"))
					writetofile( fc, "\""+StringUtil.toString("")+"\"," );
				else
					writetofile( fc, "\""+NumberUtil.formatCurrency(rs.getString("net_payment"))+"\"," );
						
				
				// DBSY-1139 ExternalDBA
				exDBA = rs.getString("ExternalDBA");
	
				if ( bIsExternalRep )
				{
					if (exDBA == null)
					{
						externalPayment = false;				
						writetofile( fc, "\""+StringUtil.toString("Assigned")+"\"," );
					}
					else
					{
						externalPayment = true;
						if (needsDBAName)
						{
							writetofile( fc, "\""+StringUtil.toString(exDBA)+"\"," );					
						}
						else
						{
							writetofile( fc, "\""+StringUtil.toString("External")+"\"," );					
						}		
					}
				}
				
				if (needsDBAName && externalPayment == false)
				{
					// We don't want to add it to the returned table
				}
				else
				{
					previousBalance = balance;
				}				
			} 
			catch (Exception e) {
				cat.error("downloadMerchantCredits Exception "+e);
			}
			writetofile(fc,"\r\n");
		}
		//blankSpacesTotal
		whiteSpaceBeforeTotals--;//the total label
		for(int j=0; j<whiteSpaceBeforeTotals; j++)
			writetofile( fc, "\"\"," );
		
					  
		//writetofile(fc,"\r\n");
		fc.close();
		downloadUrl = createZipFile(strFileName, downloadUrl, downloadPath, workingDir, filePrefix);
		return downloadUrl;          
	}
	
	
	
	/**
	 * @param report
	 * @param sessionData
	 * @param application
	 * @param strFileName
	 * @return
	 * @throws TransactionException
	 * @throws SQLException
	 * @throws IOException
	 */
	public String downloadTrxSummaryByAmount( ReportTrxSummaryByAmount report, SessionData sessionData, ServletContext application, String strFileName ) 
			   																						throws TransactionException, SQLException, IOException
	{
		String downloadUrl = DebisysConfigListener.getDownloadUrl(application);
		String downloadPath = DebisysConfigListener.getDownloadPath(application);
		String workingDir = DebisysConfigListener.getWorkingDir(application);
		String filePrefix = DebisysConfigListener.getFilePrefix(application);
		
		if ( fc == null)
		{			  
			fc = new RandomAccessFile(workingDir + filePrefix + strFileName + ".csv", "rw").getChannel();
		}		   
		HashMap<String, Double> totalsFields = new HashMap<String, Double>();
		boolean addWarningMessage = false;
		
		String title = report.getTitle().toUpperCase();
		writetofile( fc, "\""+StringUtil.toString( title )+"\"," );	
		
		
		for( String warn : report.getWarnnings())
		{
			writetofile( fc, warn + ",");
			writetofile(fc,"\r\n");	
		}
			
		writetofile(fc,"\r\n");
		
		String colDesc   = report.getColumnsDescriptions().getDescription();
		String colCur    = report.getColumnsDescriptions().getCurrentPeriod();
		String colPrev   = report.getColumnsDescriptions().getPreviuosPeriod();
		String colChange = report.getColumnsDescriptions().getChange();
		
		writetofile( fc, StringUtil.toString( colDesc ) + ",");
		writetofile( fc, StringUtil.toString( colCur ) + ",");
		writetofile( fc, StringUtil.toString( colPrev ) + ",");
		writetofile( fc, StringUtil.toString( colChange ) + ",");
		
		writetofile(fc,"\r\n");
		
		for( TrxSummaryByAmountPojo record : report.getRecords())
		{		
			writetofile( fc, StringUtil.toString( record.getDescription() ) + ",");
			writetofile( fc, StringUtil.toString( record.getCurrentPeriod() ) + ",");
			writetofile( fc, StringUtil.toString( record.getPreviuosPeriod() ) + ",");
			writetofile( fc, StringUtil.toString( record.getChange() ) + ",");
			writetofile(fc,"\r\n");	
		}
		writetofile(fc,"\r\n");	
		fc.close();
		downloadUrl = createZipFile(strFileName, downloadUrl, downloadPath, workingDir, filePrefix);
		return downloadUrl;          
	}
	
	/**
	 * @param strFileName
	 * @param downloadUrl
	 * @param downloadPath
	 * @param workingDir
	 * @param filePrefix
	 * @return
	 */
	private String createZipFile(String strFileName, String downloadUrl,String downloadPath, String workingDir, String filePrefix) 
	{
		File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
          long size = sourceFile.length();
          byte[] buffer = new byte[(int) size];
          String zipFileName = downloadPath + filePrefix + strFileName + ".zip";          
          downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
		  		
	  	  try
	      {
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
            // Set the compression ratio
            out.setLevel(Deflater.DEFAULT_COMPRESSION);
            FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");
            // Add ZIP entry to output stream.
            out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
            int len;
            while ((len = in.read(buffer)) > 0)
            {
              out.write(buffer, 0, len);
            }
            // Close the current entry
            out.closeEntry();
            // Close the current file input stream
            in.close();
            out.close();
	      }
	      catch (IllegalArgumentException iae)
	      {
	         iae.printStackTrace();
	      }
	      catch (FileNotFoundException fnfe)
	      {
	         fnfe.printStackTrace();
	      }
	      catch (IOException ioe)
	      {
	        ioe.printStackTrace();
	      }
	      finally
	      {
	    	  sourceFile.delete();
		  }
		return downloadUrl;
	}


	
	
	
	
}
