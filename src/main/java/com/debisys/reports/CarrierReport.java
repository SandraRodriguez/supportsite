package com.debisys.reports;

import com.debisys.customers.RepReports;
import com.debisys.dateformat.DateFormat;
import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.reports.pojo.SummaryReport;
import com.debisys.reports.pojo.SummaryReportPojo;
import com.debisys.schedulereports.ScheduleReport;
import com.debisys.schedulereports.TransactionReportScheduleHelper;
import com.debisys.transactions.TransactionSearch;
import com.debisys.users.SessionData;
import com.debisys.utils.ColumnReport;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConfigListener;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.debisys.utils.TaxTypes;
import com.debisys.utils.TimeZone;
import com.emida.utils.dbUtils.TorqueHelper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.apache.log4j.Category;
import org.apache.torque.Torque;

/**
 * @author Jay Chi
 */

public class CarrierReport implements HttpSessionBindingListener
{
	
	private String start_date = "";
	private String end_date = "";
	private Hashtable validationErrors = new Hashtable();
	private String merchant_ids = "";
	private String product_ids = "";
	private String rep_ids = "";
	private String sort = "";
	private String col = "";
	private String phone_number = "";
	
	private String    strUrlLocation = "";
	
	public String getPhoneNumber()
	{
		return StringUtil.toString(this.phone_number);
	}
	
	public void setPhoneNumber(String strValue)
	{
		this.phone_number = strValue;
	}
	
	public String getCol()
	{
		return StringUtil.toString(this.col);
	}
	
	public void setCol(String strCol)
	{
		this.col = strCol;
	}
	
	public String getSort()
	{
		return StringUtil.toString(this.sort);
	}
	
	public void setSort(String strSort)
	{
		this.sort = strSort;
	}
	
	public String getRepIds()
	{
		return StringUtil.toString(this.rep_ids);
	}
	
	public void setRepIds(String[] strValue)
	{
		String strTemp = "";
		if (strValue != null && strValue.length > 0)
		{
			boolean isFirst = true;
			for (int i = 0; i < strValue.length; i++)
			{
				if (strValue[i] != null && !strValue[i].equals(""))
				{
					if (!isFirst)
					{
						strTemp = strTemp + "," + strValue[i];
					}
					else
					{
						strTemp = strValue[i];
						isFirst = false;
					}
				}
			}
		}
		
		this.rep_ids = strTemp;
	}
	
	private void logdebug(String logthis)
	{
		if (CarrierReport.cat.isDebugEnabled())
			CarrierReport.cat.debug(logthis);
	}
	
	public void setRepIds(String strValue)
	{
		this.rep_ids = strValue;
	}
	
	public void setMerchantIds(String strValue)
	{
		this.merchant_ids = strValue;
	}
	
	public String getMerchantIds()
	{
		return StringUtil.toString(this.merchant_ids);
	}
	
	public String getProductIds()
	{
		return StringUtil.toString(this.product_ids);
	}
	
	public void setProductIds(String strValue)
	{
		this.product_ids = strValue;
	}
	
	public void setProductIds(String[] strValue)
	{
		String strTemp = "";
		if (strValue != null && strValue.length > 0)
		{
			boolean isFirst = true;
			for (int i = 0; i < strValue.length; i++)
			{
				if (strValue[i] != null && !strValue[i].equals(""))
				{
					if (strValue[i] == "ALL")
					{
						this.product_ids = "ALL";
						i = strValue.length;
					}
					else
						if (!isFirst)
						{
							strTemp = strTemp + "," + strValue[i];
						}
						else
						{
							strTemp = strValue[i];
							isFirst = false;
						}
				}
			}
		}
		
		this.product_ids = strTemp;
	}
	
	public void setMerchantIds(String[] strValue)
	{
		String strTemp = "";
		if (strValue != null && strValue.length > 0)
		{
			boolean isFirst = true;
			for (int i = 0; i < strValue.length; i++)
			{
				if (strValue[i] != null && !strValue[i].equals(""))
				{
					if (strValue[i] == "ALL")
					{
						this.merchant_ids = "ALL";
						i = strValue.length;
					}
					else
						if (!isFirst)
						{
							strTemp = strTemp + "," + strValue[i];
						}
						else
						{
							strTemp = strValue[i];
							isFirst = false;
						}
				}
			}
		}
		
		this.merchant_ids = strTemp;
	}
	
	// log4j logging
	static Category cat = Category.getInstance(CarrierReport.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
	
	public String getStartDate()
	{
		return StringUtil.toString(this.start_date);
	}
	
	public void setStartDate(String strValue)
	{
		this.start_date = strValue;
	}
	
	public String getEndDate()
	{
		return StringUtil.toString(this.end_date);
	}
	
	public void setEndDate(String strValue)
	{
		this.end_date = strValue;
	}
	
	public String getStartDateFormatted()
	{
		return this.getDateFormatted(this.start_date);
	}
	
	public String getEndDateFormatted()
	{
		return this.getDateFormatted(this.end_date);
	}
	
	private String getDateFormatted(String dateToFormat)
	{
		if (DateUtil.isValid(dateToFormat))
		{
			String date = null;
			try
			{
				// Current support site date is in MM/dd/YYYY.
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				java.util.Date dd = sdf.parse(dateToFormat);
				
				// We will specify a new date format through the database
				SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
				date = sdf1.format(dd);
				
				// Since Java SimpleDateFormat does not know whether a datetime contains
				// a time or not, remove the time (usually 12:00:00 AM) from the end of the date
				// ranges
				date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
			}
			catch (ParseException e)
			{
				CarrierReport.cat.error("Error during CarrierReport date localization ", e);
			}
			return date;
		}
		else
		{
			// Something isn't valid in the date string - don't try to localize it
			return StringUtil.toString(dateToFormat);
		}
	}
	
	/**
	 * Returns a hash of errors.
	 * 
	 * @return Hash of errors.
	 */
	
	public Hashtable getErrors()
	{
		return this.validationErrors;
	}
	
	/**
	 * Returns a validation error against a specific field. If a field was found to have an error
	 * during {@link #validateDateRange validateDateRange}, the error message can be accessed via
	 * this method.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @return The error message for the field or null if none is present.
	 */
	
	public String getFieldError(String fieldname)
	{
		return ((String) this.validationErrors.get(fieldname));
	}
	
	/**
	 * Sets the validation error against a specific field. Used by {@link #validateDateRange
	 * validateDateRange}.
	 * 
	 * @param fieldname
	 *            The bean property name of the field
	 * @param error
	 *            The error message for the field or null if none is present.
	 */
	
	public void addFieldError(String fieldname, String error)
	{
		this.validationErrors.put(fieldname, error);
	}
	
	/**
	 * Validates for missing or invalid fields.
	 * 
	 * @return <code>true</code> if the entity passes the validation checks, otherwise
	 *         <code>false</code>
	 */
	
	public boolean validateDateRange(SessionData sessionData)
	{
		this.validationErrors.clear();
		boolean valid = true;
		
		if ((this.start_date == null) || (this.start_date.length() == 0))
		{
			this.addFieldError("startdate", Languages.getString("com.debisys.reports.error1",
					sessionData.getLanguage()));
			valid = false;
		}
		else
			if (!DateUtil.isValid(this.start_date))
			{
				this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2",
						sessionData.getLanguage()));
				valid = false;
			}
		
		if ((this.end_date == null) || (this.end_date.length() == 0))
		{
			this.addFieldError("enddate", Languages.getString("com.debisys.reports.error3",
					sessionData.getLanguage()));
			valid = false;
		}
		else
			if (!DateUtil.isValid(this.end_date))
			{
				this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4",
						sessionData.getLanguage()));
				valid = false;
			}
		
		if (valid)
		{
			
			Date dtStartDate = new Date();
			Date dtEndDate = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			formatter.setLenient(false);
			try
			{
				dtStartDate = formatter.parse(this.start_date);
			}
			catch (ParseException ex)
			{
				this.addFieldError("startdate", Languages.getString("com.debisys.reports.error2",
						sessionData.getLanguage()));
				valid = false;
			}
			try
			{
				dtEndDate = formatter.parse(this.end_date);
			}
			catch (ParseException ex)
			{
				this.addFieldError("enddate", Languages.getString("com.debisys.reports.error4",
						sessionData.getLanguage()));
				valid = false;
			}
			if (dtStartDate.after(dtEndDate))
			{
				this.addFieldError("date", "Start date must be before end date.");
				valid = false;
			}
			
		}
		
		return valid;
	}
	
	/***
	 * DBSY-890 SW
	 * 
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public Vector<String> getCarrierUserSummary(SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
		Vector<String> vecCarrierSummary = new Vector<String>();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		String strCarrierUserProds = sessionData.getProperty("carrierUserProds");
		String strSQLSummary = "";
		String strSQLSummaryErrorCount = "";
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			// If no products are setup, return an empty list and handle in JSP
			if (strCarrierUserProds == null)
				return vecCarrierSummary;
			
			if (strCarrierUserProds.equals(""))
				return vecCarrierSummary;
			
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQLSummary = sql_bundle.getString("getCarrierUserSummary3");
					strSQLSummary = strSQLSummary.replace("PROD_LIST", strCarrierUserProds);
					pstmt1 = dbConn.prepareStatement(strSQLSummary);
					pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt1.setDouble(2, Double.parseDouble(strRefId));
					pstmt1.setString(3, this.start_date);
					pstmt1.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
					cat.debug(strSQLSummary + " " + DebisysConstants.REP_TYPE_REP + " " + strRefId
							+ " " + this.start_date + " "
							+ DateUtil.addSubtractDays(this.end_date, 1));
					
					strSQLSummaryErrorCount = sql_bundle
							.getString("getCarrierUserSummary3ErrorCount");
					strSQLSummaryErrorCount = strSQLSummaryErrorCount.replace("PROD_LIST",
							strCarrierUserProds);
					pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
					pstmt2.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt2.setDouble(2, Double.parseDouble(strRefId));
					pstmt2.setString(3, this.start_date);
					pstmt2.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
					cat.debug(strSQLSummaryErrorCount + " " + DebisysConstants.REP_TYPE_REP + " "
							+ strRefId + " " + this.start_date + " "
							+ DateUtil.addSubtractDays(this.end_date, 1));
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLSummary = sql_bundle.getString("getCarrierUserSummary5");
						strSQLSummary = strSQLSummary.replace("PROD_LIST", strCarrierUserProds);
						pstmt1 = dbConn.prepareStatement(strSQLSummary);
						pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
						pstmt1.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
						pstmt1.setString(3, this.start_date);
						pstmt1.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
						pstmt1.setInt(5, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt1.setDouble(6, Double.parseDouble(strRefId));
						cat.debug(strSQLSummary + " " + DebisysConstants.REP_TYPE_SUBAGENT + " "
								+ DebisysConstants.REP_TYPE_AGENT + " " + this.start_date + " "
								+ DateUtil.addSubtractDays(this.end_date, 1) + " "
								+ DebisysConstants.REP_TYPE_REP + " " + strRefId);
						
						strSQLSummaryErrorCount = sql_bundle
								.getString("getCarrierUserSummary5ErrorCount");
						strSQLSummaryErrorCount = strSQLSummaryErrorCount.replace("PROD_LIST",
								strCarrierUserProds);
						pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
						pstmt2.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt2.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
						pstmt2.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
						pstmt2.setDouble(4, Double.parseDouble(strRefId));
						pstmt2.setString(5, this.start_date);
						pstmt2.setString(6, DateUtil.addSubtractDays(this.end_date, 1));
						cat.debug(strSQLSummaryErrorCount + " " + DebisysConstants.REP_TYPE_REP
								+ " " + DebisysConstants.REP_TYPE_SUBAGENT + " "
								+ DebisysConstants.REP_TYPE_AGENT + " " + strRefId + " "
								+ this.start_date + " "
								+ DateUtil.addSubtractDays(this.end_date, 1));
						
					}
			}
			else
			{
				
			}
			
			rs = pstmt1.executeQuery();
			double transCount = 0;
			double transTotal = 0;
			double termCount = 0;
			double mercCount = 0;
			
			double daysInMonth = 0;
			double numOfDays = 0;
			double avgTransAmount = 0;
			double avgTransPerTermPerDay = 0;
			double avgAmountPerDay = 0;
			double avgAmountPerDayPerTerm = 0;
			double avgAmountPerMonthPerTerm = 0;
			double projectedAmountPerMonth = 0;
			
			double totalSubscribers = 0;
			double avgRechargeAmountPerSubscriber = 0;
			double avgRechargeCountPerSubscriber = 0;
			
			double errorCount = 0;
			double errorRate = 0;
			
			if (rs.next())
			{
				transCount = rs.getDouble("trans_count");
				transTotal = rs.getDouble("trans_total");
				termCount = rs.getDouble("term_count");
				mercCount = rs.getDouble("merc_count");
				totalSubscribers = rs.getDouble("total_subscribers");
			}
			
			rs = null;
			rs = pstmt2.executeQuery();
			if (rs.next())
			{
				errorCount = rs.getDouble("error_count");
			}
			pstmt1.close();
			pstmt2.close();
			rs.close();
			
			// calculate totals
			daysInMonth = DateUtil.getDaysBetween(this.start_date, DateUtil.addSubtractMonths(
					this.start_date, 1));
			numOfDays = DateUtil.getDaysBetween(this.start_date, DateUtil.addSubtractDays(
					this.end_date, 1));
			if (transCount > 0)
			{
				avgTransAmount = transTotal / transCount;
				if (transCount > 0 || errorCount > 0)
				{
					errorRate = (errorCount / (transCount + errorCount)) * 100;
				}
			}
			
			if (termCount > 0 && numOfDays > 0)
			{
				avgTransPerTermPerDay = (transCount / termCount) / numOfDays;
			}
			if (numOfDays > 0)
			{
				avgAmountPerDay = transTotal / numOfDays;
			}
			if (termCount > 0 && numOfDays > 0)
			{
				avgAmountPerDayPerTerm = (transTotal / numOfDays) / termCount;
				avgAmountPerMonthPerTerm = avgAmountPerDayPerTerm * daysInMonth;
			}
			
			projectedAmountPerMonth = avgAmountPerDay * daysInMonth;
			
			// recharge stats
			if (totalSubscribers > 0)
			{
				avgRechargeAmountPerSubscriber = transTotal / totalSubscribers;
				avgRechargeCountPerSubscriber = transCount / totalSubscribers;
			}
			
			vecCarrierSummary.add(Integer.toString((int) daysInMonth));
			vecCarrierSummary.add(DateUtil.formatDateTime(this.start_date) + " - "
					+ DateUtil.formatDateTime(this.end_date));
			vecCarrierSummary.add(Double.toString(transTotal));
			vecCarrierSummary.add(Integer.toString((int) transCount));
			vecCarrierSummary.add(Integer.toString((int) numOfDays));
			vecCarrierSummary.add(Integer.toString((int) termCount));
			vecCarrierSummary.add(Integer.toString((int) mercCount));
			vecCarrierSummary.add(Double.toString(avgTransAmount));
			vecCarrierSummary.add(NumberUtil.formatAmount(Double.toString(avgTransPerTermPerDay)));
			vecCarrierSummary.add(Double.toString(avgAmountPerDay));
			vecCarrierSummary.add(Double.toString(avgAmountPerDayPerTerm));
			vecCarrierSummary.add(Double.toString(avgAmountPerMonthPerTerm));
			vecCarrierSummary.add(Double.toString(projectedAmountPerMonth));
			vecCarrierSummary.add(Double.toString(avgRechargeAmountPerSubscriber));
			vecCarrierSummary.add(NumberUtil.formatAmount(Double
					.toString(avgRechargeCountPerSubscriber)));
			vecCarrierSummary.add(Integer.toString((int) totalSubscribers));
			vecCarrierSummary.add(Integer.toString((int) errorCount));
			vecCarrierSummary.add(NumberUtil.formatAmount(Double.toString(errorRate)));
			
		}
		catch (Exception e)
		{
			cat.error("Error during getCarrierSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt1, rs);
				TorqueHelper.closeConnection(null, pstmt2, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecCarrierSummary;
	}
	
	public Vector getCarrierSummary(SessionData sessionData) throws ReportException
	{
		Connection dbConn = null;
		Vector vecCarrierSummary = new Vector();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		String strSQLSummary = "";
		String strSQLSummaryErrorCount = "";
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs = null;
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQLSummary = sql_bundle.getString("getCarrierSummary3");
					pstmt1 = dbConn.prepareStatement(strSQLSummary);
					pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt1.setDouble(2, Double.parseDouble(strRefId));
					pstmt1.setString(3, this.start_date);
					pstmt1.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
					
					strSQLSummaryErrorCount = sql_bundle.getString("getCarrierSummary3ErrorCount");
					pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
					pstmt2.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt2.setDouble(2, Double.parseDouble(strRefId));
					pstmt2.setString(3, this.start_date);
					pstmt2.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
					// cat.debug("PARAMETERS: " + DebisysConstants.REP_TYPE_REP + "," + strRefId +
					// "," + start_date + "," + DateUtil.addSubtractDays(end_date,1));
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLSummary = sql_bundle.getString("getCarrierSummary5");
						pstmt1 = dbConn.prepareStatement(strSQLSummary);
						pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt1.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
						pstmt1.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
						pstmt1.setDouble(4, Double.parseDouble(strRefId));
						pstmt1.setString(5, this.start_date);
						pstmt1.setString(6, DateUtil.addSubtractDays(this.end_date, 1));
						
						System.out.println("start_date "+this.start_date);
						System.out.println("end_date "+DateUtil.addSubtractDays(this.end_date, 1));
						
						strSQLSummaryErrorCount = sql_bundle
								.getString("getCarrierSummary5ErrorCount");
						pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
						pstmt2.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt2.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
						pstmt2.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
						pstmt2.setDouble(4, Double.parseDouble(strRefId));
						pstmt2.setString(5, this.start_date);
						pstmt2.setString(6, DateUtil.addSubtractDays(this.end_date, 1));
						// cat.debug("PARAMETERS: " + DebisysConstants.REP_TYPE_REP + "," +
						// DebisysConstants.REP_TYPE_SUBAGENT + "," +
						// DebisysConstants.REP_TYPE_AGENT + "," + strRefId + "," + start_date + ","
						// + DateUtil.addSubtractDays(end_date,1));
					}
				
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQLSummary = sql_bundle.getString("getCarrierSummary5AGENT");
					pstmt1 = dbConn.prepareStatement(strSQLSummary);
					pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt1.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
					pstmt1.setDouble(3, Double.parseDouble(strRefId));
					pstmt1.setString(4, this.start_date);
					pstmt1.setString(5, DateUtil.addSubtractDays(this.end_date, 1));
					
					strSQLSummaryErrorCount = sql_bundle
							.getString("getCarrierSummary5ErrorCountAGENT");
					pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
					pstmt2.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt2.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
					pstmt2.setDouble(3, Double.parseDouble(strRefId));
					pstmt2.setString(4, this.start_date);
					pstmt2.setString(5, DateUtil.addSubtractDays(this.end_date, 1));
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						strSQLSummary = sql_bundle.getString("getCarrierSummary5SUBAGENT");
						pstmt1 = dbConn.prepareStatement(strSQLSummary);
						pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt1.setDouble(2, Double.parseDouble(strRefId));
						pstmt1.setString(3, this.start_date);
						pstmt1.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
						
						strSQLSummaryErrorCount = sql_bundle
								.getString("getCarrierSummary5ErrorCountSUBAGENT");
						pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
						pstmt2.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt2.setDouble(2, Double.parseDouble(strRefId));
						pstmt2.setString(3, this.start_date);
						pstmt2.setString(4, DateUtil.addSubtractDays(this.end_date, 1));
						
					}
					else
						if (strAccessLevel.equals(DebisysConstants.REP))
						{
							strSQLSummary = sql_bundle.getString("getCarrierSummary6");
							pstmt1 = dbConn.prepareStatement(strSQLSummary);
							pstmt1.setDouble(1, Double.parseDouble(strRefId));
							pstmt1.setString(2, this.start_date);
							pstmt1.setString(3, DateUtil.addSubtractDays(this.end_date, 1));
							
							strSQLSummaryErrorCount = sql_bundle
									.getString("getCarrierSummary6ErrorCount");
							pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
							pstmt2.setDouble(1, Double.parseDouble(strRefId));
							pstmt2.setString(2, this.start_date);
							pstmt2.setString(3, DateUtil.addSubtractDays(this.end_date, 1));
						}
						else
							if (strAccessLevel.equals(DebisysConstants.MERCHANT))
							{
								strSQLSummary = sql_bundle.getString("getCarrierSummary7");
								pstmt1 = dbConn.prepareStatement(strSQLSummary);
								pstmt1.setDouble(1, Double.parseDouble(strRefId));
								pstmt1.setString(2, this.start_date);
								pstmt1.setString(3, DateUtil.addSubtractDays(this.end_date, 1));
								
								strSQLSummaryErrorCount = sql_bundle
										.getString("getCarrierSummary7ErrorCount");
								pstmt2 = dbConn.prepareStatement(strSQLSummaryErrorCount);
								pstmt2.setDouble(1, Double.parseDouble(strRefId));
								pstmt2.setString(2, this.start_date);
								pstmt2.setString(3, DateUtil.addSubtractDays(this.end_date, 1));
							}
			
			// cat.debug(strSQLSummary);
			rs = pstmt1.executeQuery();
			double transCount = 0;
			double transTotal = 0;
			double termCount = 0;
			double mercCount = 0;
			
			double daysInMonth = 0;
			double numOfDays = 0;
			double avgTransAmount = 0;
			double avgTransPerTermPerDay = 0;
			double avgAmountPerDay = 0;
			double avgAmountPerDayPerTerm = 0;
			double avgAmountPerMonthPerTerm = 0;
			double projectedAmountPerMonth = 0;
			
			double totalSubscribers = 0;
			double avgRechargeAmountPerSubscriber = 0;
			double avgRechargeCountPerSubscriber = 0;
			
			double errorCount = 0;
			double errorRate = 0;
			
			if (rs.next())
			{
				transCount = rs.getDouble("trans_count");
				transTotal = rs.getDouble("trans_total");
				termCount = rs.getDouble("term_count");
				mercCount = rs.getDouble("merc_count");
				totalSubscribers = rs.getDouble("total_subscribers");
			}
			
			rs = null;
			rs = pstmt2.executeQuery();
			if (rs.next())
			{
				errorCount = rs.getDouble("error_count");
			}
			pstmt1.close();
			pstmt2.close();
			rs.close();
			
			// calculate totals
			daysInMonth = DateUtil.getDaysBetween(this.start_date, DateUtil.addSubtractMonths(this.start_date, 1));
			numOfDays   = DateUtil.getDaysBetween(this.start_date, DateUtil.addSubtractDays(this.end_date, 1));
			if (transCount > 0)
			{
				avgTransAmount = transTotal / transCount;
				if (transCount > 0 || errorCount > 0)
				{
					errorRate = (errorCount / (transCount + errorCount)) * 100;
				}
			}
			
			if (termCount > 0 && numOfDays > 0)
			{
				avgTransPerTermPerDay = (transCount / termCount) / numOfDays;
			}
			if (numOfDays > 0)
			{
				avgAmountPerDay = transTotal / numOfDays;
			}
			if (termCount > 0 && numOfDays > 0)
			{
				avgAmountPerDayPerTerm = (transTotal / numOfDays) / termCount;
				avgAmountPerMonthPerTerm = avgAmountPerDayPerTerm * daysInMonth;
			}
			
			projectedAmountPerMonth = avgAmountPerDay * daysInMonth;
			
			// recharge stats
			
			if (totalSubscribers > 0)
			{
				avgRechargeAmountPerSubscriber = transTotal / totalSubscribers;
				avgRechargeCountPerSubscriber = transCount / totalSubscribers;
			}
			
			vecCarrierSummary.add(Integer.toString((int) daysInMonth));
			vecCarrierSummary.add(DateUtil.formatDateTime(this.start_date) + " - " + DateUtil.formatDateTime(this.end_date));
			vecCarrierSummary.add(Double.toString(transTotal));
			vecCarrierSummary.add(Integer.toString((int) transCount));
			vecCarrierSummary.add(Integer.toString((int) numOfDays));
			vecCarrierSummary.add(Integer.toString((int) termCount));
			vecCarrierSummary.add(Integer.toString((int) mercCount));
			vecCarrierSummary.add(Double.toString(avgTransAmount));
			vecCarrierSummary.add(NumberUtil.formatAmount(Double.toString(avgTransPerTermPerDay)));
			vecCarrierSummary.add(Double.toString(avgAmountPerDay));
			vecCarrierSummary.add(Double.toString(avgAmountPerDayPerTerm));
			vecCarrierSummary.add(Double.toString(avgAmountPerMonthPerTerm));
			vecCarrierSummary.add(Double.toString(projectedAmountPerMonth));
			vecCarrierSummary.add(Double.toString(avgRechargeAmountPerSubscriber));
			vecCarrierSummary.add(NumberUtil.formatAmount(Double.toString(avgRechargeCountPerSubscriber)));
			vecCarrierSummary.add(Integer.toString((int) totalSubscribers));
			vecCarrierSummary.add(Integer.toString((int) errorCount));
			vecCarrierSummary.add(NumberUtil.formatAmount(Double.toString(errorRate)));
			
		}
		catch (Exception e)
		{
			cat.error("Error during getCarrierSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt1, rs);
				TorqueHelper.closeConnection(null, pstmt2, null);
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return vecCarrierSummary;
	}
	
	public void valueBound(HttpSessionBindingEvent event)
	{
	}
	
	public void valueUnbound(HttpSessionBindingEvent event)
	{
	}
	
	public static void main(String[] args)
	{
	}
	
	/***
	 * DBSY-890 SW
	 * 
	 * @param context
	 * @param sessionData
	 * @return
	 * @throws TransactionException
	 */
	public String downloadCarrierUserSummary(ServletContext context, SessionData sessionData)
			throws TransactionException
	{
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		String deploymentType = DebisysConfigListener.getDeploymentType(context);
		String customConfigType = DebisysConfigListener.getCustomConfigType(context);
		
		try
		{
			Vector<String> vPast = new Vector<String>();
			Vector<String> vCurrent = new Vector<String>();
			
			String tempStartDate = this.start_date;
			String tempEndDate = this.end_date;
			
			vCurrent = this.getCarrierUserSummary(sessionData);
			
			this.setStartDate(DateUtil.addSubtractMonths(this.start_date, -1));
			this.setEndDate(DateUtil.addSubtractMonths(this.end_date, -1));
			vPast = this.getCarrierUserSummary(sessionData);
			
			// Reset the dates to what they were
			this.setStartDate(tempStartDate);
			this.setEndDate(tempEndDate);
			
			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
			
			try
			{
				strFileName = (new TransactionSearch()).generateFileName(context);
				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix
						+ strFileName + ".csv"));
				CarrierReport.cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName
						+ ".csv");
				outputFile.flush();
				
				// Column Headers
				
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.title26", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.description", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.current_period", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.previous_period", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.percentage_change", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\r\n");
				
				double totalTranAmountCurr = Double.parseDouble(vCurrent.get(2));
				double numOfTransCurr = Double.parseDouble(vCurrent.get(3));
				double liveTerminalCountCurr = Double.parseDouble(vCurrent.get(5));
				double activeLocationCountCurr = Double.parseDouble(vCurrent.get(6));
				double avgPerTranCurr = Double.parseDouble(vCurrent.get(7));
				double transPerTerminalDayCurr = Double.parseDouble(vCurrent.get(8));
				double avgPerDayCurr = Double.parseDouble(vCurrent.get(9));
				double avgPerDayTerminalCurr = Double.parseDouble(vCurrent.get(10));
				double avgPerMonthTerminalCurr = Double.parseDouble(vCurrent.get(11));
				double avgRevenuePerUserCurr = Double.parseDouble(vCurrent.get(13));
				double avgRechargesPerSubscriberCurr = Double.parseDouble(vCurrent.get(14));
				double totalSubscribersCurr = Double.parseDouble(vCurrent.get(15));
				double errorCountCurr = Double.parseDouble(vCurrent.get(16));
				double errorRateCurr = Double.parseDouble(vCurrent.get(17));
				
				double totalTranAmountPrev = Double.parseDouble(vPast.get(2));
				double numOfTransPrev = Double.parseDouble(vPast.get(3));
				double liveTerminalCountPrev = Double.parseDouble(vPast.get(5));
				double activeLocationCountPrev = Double.parseDouble(vPast.get(6));
				double avgPerTranPrev = Double.parseDouble(vPast.get(7));
				double transPerTerminalDayPrev = Double.parseDouble(vPast.get(8));
				double avgPerDayPrev = Double.parseDouble(vPast.get(9));
				double avgPerDayTerminalPrev = Double.parseDouble(vPast.get(10));
				double avgPerMonthTerminalPrev = Double.parseDouble(vPast.get(11));
				double avgRevenuePerUserPrev = Double.parseDouble(vPast.get(13));
				double avgRechargesPerSubscriberPrev = Double.parseDouble(vPast.get(14));
				double totalSubscribersPrev = Double.parseDouble(vPast.get(15));
				double errorCountPrev = Double.parseDouble(vPast.get(16));
				double errorRatePrev = Double.parseDouble(vPast.get(17));
				
				String totalTranAmountChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String numOfTransChange = Languages.getString("jsp.admin.reports.not_available",
						sessionData.getLanguage());
				String liveTerminalCountChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String activeLocationCountChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String avgPerTranChange = Languages.getString("jsp.admin.reports.not_available",
						sessionData.getLanguage());
				String transPerTerminalDayChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String avgPerDayChange = Languages.getString("jsp.admin.reports.not_available",
						sessionData.getLanguage());
				String avgPerDayTerminalChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String avgPerMonthTerminalChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String avgRevenuePerUserChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String avgRechargesPerSubscriberChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String totalSubscribersChange = Languages.getString(
						"jsp.admin.reports.not_available", sessionData.getLanguage());
				String errorCountChange = Languages.getString("jsp.admin.reports.not_available",
						sessionData.getLanguage());
				String errorRateChange = Languages.getString("jsp.admin.reports.not_available",
						sessionData.getLanguage());
				
				if (totalTranAmountPrev != 0)
				{
					totalTranAmountChange = NumberUtil.formatAmount(Double
							.toString((totalTranAmountCurr - totalTranAmountPrev)
									/ totalTranAmountPrev * 100))
							+ "%";
				}
				if (numOfTransPrev != 0)
				{
					numOfTransChange = NumberUtil.formatAmount(Double
							.toString((numOfTransCurr - numOfTransPrev) / numOfTransPrev * 100))
							+ "%";
				}
				if (liveTerminalCountPrev != 0)
				{
					liveTerminalCountChange = NumberUtil.formatAmount(Double
							.toString((liveTerminalCountCurr - liveTerminalCountPrev)
									/ liveTerminalCountPrev * 100))
							+ "%";
				}
				if (activeLocationCountPrev != 0)
				{
					activeLocationCountChange = NumberUtil.formatAmount(Double
							.toString((activeLocationCountCurr - activeLocationCountPrev)
									/ activeLocationCountPrev * 100))
							+ "%";
				}
				if (avgPerTranPrev != 0)
				{
					avgPerTranChange = NumberUtil.formatAmount(Double
							.toString((avgPerTranCurr - avgPerTranPrev) / avgPerTranPrev * 100))
							+ "%";
				}
				if (transPerTerminalDayPrev != 0)
				{
					transPerTerminalDayChange = NumberUtil.formatAmount(Double
							.toString((transPerTerminalDayCurr - transPerTerminalDayPrev)
									/ transPerTerminalDayPrev * 100))
							+ "%";
				}
				if (avgPerDayPrev != 0)
				{
					avgPerDayChange = NumberUtil.formatAmount(Double
							.toString((avgPerDayCurr - avgPerDayPrev) / avgPerDayPrev * 100))
							+ "%";
				}
				if (avgPerDayTerminalPrev != 0)
				{
					avgPerDayTerminalChange = NumberUtil.formatAmount(Double
							.toString((avgPerDayTerminalCurr - avgPerDayTerminalPrev)
									/ avgPerDayTerminalPrev * 100))
							+ "%";
				}
				if (avgPerMonthTerminalPrev != 0)
				{
					avgPerMonthTerminalChange = NumberUtil.formatAmount(Double
							.toString((avgPerMonthTerminalCurr - avgPerMonthTerminalPrev)
									/ avgPerMonthTerminalPrev * 100))
							+ "%";
				}
				if (avgRevenuePerUserPrev != 0)
				{
					avgRevenuePerUserChange = NumberUtil.formatAmount(Double
							.toString((avgRevenuePerUserCurr - avgRevenuePerUserPrev)
									/ avgRevenuePerUserPrev * 100))
							+ "%";
				}
				if (avgRechargesPerSubscriberPrev != 0)
				{
					avgRechargesPerSubscriberChange = NumberUtil
							.formatAmount(Double
									.toString((avgRechargesPerSubscriberCurr - avgRechargesPerSubscriberPrev)
											/ avgRechargesPerSubscriberPrev * 100))
							+ "%";
				}
				if (totalSubscribersPrev != 0)
				{
					totalSubscribersChange = NumberUtil.formatAmount(Double
							.toString((totalSubscribersCurr - totalSubscribersPrev)
									/ totalSubscribersPrev * 100))
							+ "%";
				}
				if (errorCountPrev != 0)
				{
					errorCountChange = NumberUtil.formatAmount(Double
							.toString((errorCountCurr - errorCountPrev) / errorCountPrev * 100))
							+ "%";
				}
				if (errorRatePrev != 0)
				{
					errorRateChange = NumberUtil.formatAmount(Double
							.toString((errorRateCurr - errorRatePrev) / errorRatePrev * 100))
							+ "%";
				}
				
				outputFile.write("\""
						+ Languages.getString(
								"jsp.admin.reports.carrier.summary.number_of_days_month",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(0) + "\",\""
						+ vPast.get(0) + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.date_range",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(1) + "\",\""
						+ vPast.get(1) + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.number_of_days",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(4) + "\",\""
						+ vPast.get(4) + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString(
								"jsp.admin.reports.carrier.summary.total_trans_amount", sessionData
										.getLanguage()) + "\",\""
						+ NumberUtil.formatCurrency(vCurrent.get(2)).replace(",", "") + "\",\""
						+ NumberUtil.formatCurrency(vPast.get(2)).replace(",", "") + "\",\""
						+ totalTranAmountChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.number_of_trans",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(3) + "\",\""
						+ vPast.get(3) + "\",\"" + numOfTransChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.live_term_count",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(5) + "\",\""
						+ vPast.get(5) + "\",\"" + liveTerminalCountChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString(
								"jsp.admin.reports.carrier.summary.active_location_count",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(6) + "\",\""
						+ vPast.get(6) + "\",\"" + activeLocationCountChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.number_of_errors",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(16) + "\",\""
						+ vPast.get(16) + "\",\"" + errorCountChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.error_rate",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(17) + "%"
						+ "\",\"" + vPast.get(17) + "%" + "\",\"" + errorRateChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.avg_per_trans",
								sessionData.getLanguage()) + "\",\""
						+ NumberUtil.formatCurrency(vCurrent.get(7)).replace(",", "") + "\",\""
						+ NumberUtil.formatCurrency(vPast.get(7)).replace(",", "") + "\",\""
						+ avgPerTranChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString(
								"jsp.admin.reports.carrier.summary.trans_per_term_day", sessionData
										.getLanguage()) + "\",\"" + vCurrent.get(8) + "\",\""
						+ vPast.get(8) + "\",\"" + transPerTerminalDayChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.avg_per_day",
								sessionData.getLanguage()) + "\",\""
						+ NumberUtil.formatCurrency(vCurrent.get(9)).replace(",", "") + "\",\""
						+ NumberUtil.formatCurrency(vPast.get(9)).replace(",", "") + "\",\""
						+ avgPerDayChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString(
								"jsp.admin.reports.carrier.summary.avg_amount_per_day_term",
								sessionData.getLanguage()) + "\",\""
						+ NumberUtil.formatCurrency(vCurrent.get(10)).replace(",", "") + "\",\""
						+ NumberUtil.formatCurrency(vPast.get(10)).replace(",", "") + "\",\""
						+ avgPerDayTerminalChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString(
								"jsp.admin.reports.carrier.summary.avg_amount_per_month_term",
								sessionData.getLanguage()) + "\",\""
						+ NumberUtil.formatCurrency(vCurrent.get(11)).replace(",", "") + "\",\""
						+ NumberUtil.formatCurrency(vPast.get(11)).replace(",", "") + "\",\""
						+ avgPerMonthTerminalChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.projected_amt",
								sessionData.getLanguage()) + "\",\""
						+ NumberUtil.formatCurrency(vCurrent.get(12)).replace(",", "") + "\",");
				outputFile.write("\r\n");
				
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.recharge_report",
								sessionData.getLanguage()) + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.description", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.current_period", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.previous_period", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.percentage_change", sessionData
								.getLanguage()) + "\",");
				outputFile.write("\r\n");
				
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.arpu", sessionData
								.getLanguage()) + "\",\""
						+ NumberUtil.formatCurrency(vCurrent.get(13)).replace(",", "") + "\",\""
						+ NumberUtil.formatCurrency(vPast.get(13)).replace(",", "") + "\",\""
						+ avgRevenuePerUserChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.arps", sessionData
								.getLanguage()) + "\",\"" + vCurrent.get(14) + "\",\""
						+ vPast.get(14) + "\",\"" + avgRechargesPerSubscriberChange + "\",");
				outputFile.write("\r\n");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.carrier.summary.total",
								sessionData.getLanguage()) + "\",\"" + vCurrent.get(15) + "\",\""
						+ vPast.get(15) + "\",\"" + totalSubscribersChange + "\",");
				outputFile.write("\r\n");
				// End Column Headers
				
				outputFile.flush();
				outputFile.close();
				endQuery = System.currentTimeMillis();
				
				CarrierReport.cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Download URL: " + downloadUrl);
				
				try
				{
					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
					
					// Set the compression ratio
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName
							+ ".csv");
					
					// Add ZIP entry to output stream.
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
					
					// Transfer bytes from the current file to the ZIP file
					// out.write(buffer, 0, in.read(buffer));
					int len;
					while ((len = in.read(buffer)) > 0)
					{
						out.write(buffer, 0, len);
					}
					
					// Close the current entry
					out.closeEntry();
					// Close the current file input stream
					in.close();
					out.close();
				}
				catch (IllegalArgumentException iae)
				{
					iae.printStackTrace();
				}
				catch (FileNotFoundException fnfe)
				{
					fnfe.printStackTrace();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
				}
				
				sourceFile.delete();
				endQuery = System.currentTimeMillis();
				CarrierReport.cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
			}
			catch (IOException ioe)
			{
				CarrierReport.cat.error("Error on I/O operation: " + ioe.getClass().getName()
						+ " (" + ioe.getMessage() + ") ", ioe);
				try
				{
					if (outputFile != null)
					{
						outputFile.close();
					}
				}
				catch (IOException ioe2)
				{
				}
			}
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during downloadCarrierUserSummary", e);
			throw new TransactionException();
		}
		long end = System.currentTimeMillis();
		
		// Display the elapsed time to the standard output
		CarrierReport.cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
		
		return downloadUrl;
	}// End of function downloadCarrierUserSummary
	
	/* DBSY-1124 Carrier User Reports */
	/**
	 * @return Hourly summary of transactions
	 */
	public String[][] getCarrierUserHourlySummary(SessionData sessionData, ServletContext context)
			throws ReportException
	{
		
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		
		String strCarrierUserProds = sessionData.getProperty("carrierUserProds");
		String[][] HourlySummary =
		{
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" },
		{ "0", "0" }, };
		
		String strSQLDetail = "";
		ResultSet rs = null;
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		
		strSQLDetail = "SELECT DATEPART(hh,wt.datetime) AS [hour], "
				+ " SUM(wt.amount) as [amount], count(*) as [num] ";
		
		strSQLDetail = strSQLDetail + " FROM web_transactions wt WITH (NOLOCK) " + " WHERE ";
		
		String strSQLWhere = "";
		
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat
						.error("Can't get database connection: getCarrierUserHourlySummary");
				throw new ReportException();
			}
			
			if (this.merchant_ids != null && (this.merchant_ids.length() > 0))
				if (this.merchant_ids.contains("ALL"))
				{
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
					{
						strSQLWhere = strSQLWhere
								+ " wt.rep_id IN (SELECT rep_id FROM reps with (nolock) WHERE iso_id = "
								+ sessionData.getProperty("ref_id") + ")";
					}
					else
						if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
						{
							strSQLWhere = strSQLWhere + " wt.rep_id IN "
									+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
									+ DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
									+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
									+ DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
									+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
									+ DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId
									+ ")))";
						}
					
				}
				else
					strSQLWhere = strSQLWhere + " wt.merchant_id IN ( " + this.merchant_ids + " )";
			else
				// no merchant found
				return null;
			
			if (this.product_ids != null && (this.product_ids.length() > 0))
				if (this.product_ids.contains("ALL"))
				{
					// User did not select anything and "ALL" was selected
					// Add in all of the products from session data.
					strSQLWhere += " AND wt.id in (" + strCarrierUserProds + ") ";
				}
				else
					strSQLWhere += " AND wt.id in (" + this.product_ids + ") ";
			else
				// no products found
				return null;
			
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
			strSQLWhere += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0)
					+ "' and  wt.datetime < '" + vTimeZoneFilterDates.get(1) + "') ";
			strSQLDetail = strSQLDetail + strSQLWhere + " GROUP BY DATEPART(hh,wt.datetime) ";
			
			if (CarrierReport.cat.isDebugEnabled())
				CarrierReport.cat.debug(strSQLDetail);
			
			pstmt = dbConn.prepareStatement(strSQLDetail);
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				HourlySummary[rs.getInt("hour")][0] = String.valueOf((rs.getInt("num")));
				HourlySummary[rs.getInt("hour")][1] = NumberUtil.formatAmount(String.valueOf((rs
						.getDouble("amount"))));
			}
			
			rs.close();
			pstmt.close();
			
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getCarrierUserHourlySummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error(
						"Error during closeConnection: getCarrierUserHourlySummary", e);
			}
		}
		return HourlySummary;
	}
	
	/***
	 * DBSY-890 SW Gets a list of all of the products allowed for the specific carrier user
	 * 
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public static Vector<Vector<String>> getCarrierUserProductList(SessionData sessionData)
			throws ReportException
	{
		Vector<Vector<String>> vecProductList = new Vector<Vector<String>>();
		Connection dbConn = null;
		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strPinsOnly = sessionData.getProperty("pins_only");
		String carrierUserProds = sessionData.getProperty("carrierUserProds");
		
		try
		{
			// If no products are setup, return an empty list and handle in JSP
			if (carrierUserProds == null)
				return vecProductList;
			
			if (carrierUserProds.equals(""))
				return vecProductList;
			
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			String strSQL = CarrierReport.sql_bundle.getString("getProductList");
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQL = strSQL
							+ " merchants m WITH (NOLOCK), terminals t WITH (NOLOCK), terminal_rates tr WITH (NOLOCK) WHERE m.merchant_id IN (SELECT merchant_id FROM merchants WITH (NOLOCK) WHERE "
							+ "rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
							+ DebisysConstants.REP_TYPE_REP
							+ "  AND iso_id="
							+ strRefId
							+ ")) and t.merchant_id=m.merchant_id and tr.millennium_no=t.millennium_no and p.id=tr.product_id ";
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQL = strSQL
								+ " merchants m WITH (NOLOCK), terminals t WITH (NOLOCK), terminal_rates tr WITH (NOLOCK) WHERE m.merchant_id IN (SELECT merchant_id FROM merchants WITH (NOLOCK) WHERE "
								+ "rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
								+ DebisysConstants.REP_TYPE_REP
								+ "  AND "
								+ "iso_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
								+ DebisysConstants.REP_TYPE_SUBAGENT
								+ "  AND "
								+ "iso_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
								+ DebisysConstants.REP_TYPE_AGENT
								+ " AND iso_id="
								+ strRefId
								+ ")))) and t.merchant_id=m.merchant_id and tr.millennium_no=t.millennium_no and p.id=tr.product_id ";
					}
			}
			else
			{
				
			}
			
			if (strPinsOnly != null && new Boolean(strPinsOnly))
			{
				strSQL += " AND p.Trans_Type=2";
			}
			
			if (carrierUserProds != null)
			{
				strSQL += " AND p.id in (" + carrierUserProds + ")";
			}
			
			strSQL = strSQL + " order by p.description";
			if (strAccessLevel.equals(DebisysConstants.ISO)
					&& (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) || strDistChainType
							.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)))
			{
				strSQL = strSQL + " option (merge join ) ";
			}
			CarrierReport.cat.debug(strSQL);
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				Vector<String> vecTemp = new Vector<String>();
				vecTemp.add(rs.getString("id"));
				vecTemp.add(rs.getString("description"));
				vecProductList.add(vecTemp);
			}
			
			CarrierReport.cat.debug("Products fetched: " + vecProductList.size());
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getCarrierUserProductList", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecProductList;
	} // End getCarrierUserProductList
	
	/**
	 * Gets a list of agents for an iso
	 * 
	 * @throws ReportException
	 *             Thrown on database errors
	 */
	
	@SuppressWarnings(
	{ "unchecked", "rawtypes" })
	public static Vector getAgentList(SessionData sessionData) throws ReportException
	{
		Vector vecRepList = new Vector();
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			String strSQL = CarrierReport.sql_bundle.getString("getRepList");
			String strSQLWhere = "";
			String strAccessLevel = sessionData.getProperty("access_level");
			String strRefId = sessionData.getProperty("ref_id");
			
			if (strAccessLevel.equals(DebisysConstants.ISO))
				strSQLWhere = " r.rep_id IN (SELECT r1.rep_id FROM reps as r1 WITH (NOLOCK) WHERE r1.type="
						+ DebisysConstants.REP_TYPE_AGENT + " AND r1.iso_id = " + strRefId + ") ";
			
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL + strSQLWhere
					+ "order by r.businessname");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("rep_id"));
				vecTemp.add(rs.getString("businessname"));
				vecRepList.add(vecTemp);
			}
			
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getAgentList", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		
		return vecRepList;
	}
	
	public Vector<Vector<String>> getMerchantSummary(SessionData sessionData, ServletContext context)
			throws ReportException
	{
		Connection dbConn = null;
		
		PreparedStatement pstmt = null;
		// String deploymentType = DebisysConfigListener.getDeploymentType(context);
		Vector<Vector<String>> vecMerchantSummary = new Vector<Vector<String>>();
		Hashtable<String, Vector<String>> trxPerMerchantPerBillingType = new Hashtable<String, Vector<String>>();
		Integer[] positiveBillingTrxTypes = new Integer[]
		{ 1, 3, 6 };
		java.util.Arrays.sort(positiveBillingTrxTypes);
		Integer[] negativeBillingTrxTypes = new Integer[]
		{ 2, 5 };
		java.util.Arrays.sort(negativeBillingTrxTypes);
		String strSQLTotal = "";
		String strSQLDetail = "";
		ResultSet rs = null;
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		
		String rate_div = " 100 *  (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END)  ";
		
		strSQLTotal = "SELECT count(wt.rec_id) as qty, "
				+ "sum(wt.amount) as total_sales, "
				+ "sum(wt.merchant_rate / "
				+ rate_div
				+ ") as merchant_commission, "
				+ "sum(wt.rep_rate / "
				+ rate_div
				+ ") as rep_commission, "
				+ "sum(wt.agent_rate / "
				+ rate_div
				+ ") as agent_commission, "
				+ "sum(wt.subagent_rate / "
				+ rate_div
				+ ") as subagent_commission, "
				+ "sum(wt.iso_rate / "
				+ rate_div
				+ ") as iso_commission, "
				+ "sum((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate as bit)) as adj_amount, "
				+ "sum(wt.bonus_amount) as total_bonus, "
				+ "(sum(wt.bonus_amount) + sum(wt.amount)) as total_recharge,  "
				+ " billing_typeID = CASE WHEN wt.billing_typeID IS NULL THEN 1 ELSE wt.billing_typeID END ";
		
		strSQLDetail = "SELECT m.dba, m.RouteID, wt.merchant_id, count(wt.rec_id) as qty, "
				+ "sum(wt.amount) as total_sales, "
				+ "sum(wt.merchant_rate / "
				+ rate_div
				+ ") as merchant_commission, "
				+ "sum(wt.rep_rate / "
				+ rate_div
				+ ") as rep_commission, "
				+ "sum(wt.agent_rate / "
				+ rate_div
				+ ") as agent_commission, "
				+ "sum(wt.subagent_rate / "
				+ rate_div
				+ ") as subagent_commission, "
				+ "sum(wt.iso_rate / "
				+ rate_div
				+ ") as iso_commission, m.runningliability, m.liabilitylimit, m.liabilitylimit-m.runningliability as available_credit, "
				+ "sum((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate as bit)) as adj_amount, "
				+ "m.phys_address, m.phys_city, m.phys_state, m.phys_zip, "
				+ "sum(wt.bonus_amount) as total_bonus, "
				+ "(sum(wt.bonus_amount) + sum(wt.amount)) as total_recharge, "
				+ " billing_typeID = CASE WHEN wt.billing_typeID IS NULL THEN 1 ELSE wt.billing_typeID END ";
		
		if (strAccessLevel.equals(DebisysConstants.CARRIER))
		{
			strSQLDetail = strSQLDetail
					+ " FROM web_transactions wt WITH (NOLOCK) "
					+ " inner join merchants m WITH (NOLOCK) ON wt.merchant_id=m.merchant_id "
					+ " inner join products p WITH (NOLOCK) ON p.id = wt.id "
					+ " inner join merchantcarriers mc WITH (NOLOCK) ON mc.merchantid=wt.merchant_id and mc.carrierid = p.carrier_id "
					+ " WHERE ";
		}
		else
		{
			strSQLDetail = strSQLDetail + " FROM web_transactions wt WITH (NOLOCK) "
					+ " inner join merchants m WITH (NOLOCK) ON wt.merchant_id=m.merchant_id "
					+ " inner join products p WITH (NOLOCK) ON p.id = wt.id " + " WHERE ";
			
		}
		
		String strSQLWhere = "";
		
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			// get a count of total matches
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQLWhere = strSQLWhere
							+ " wt.rep_id IN (SELECT rep_id FROM reps with (nolock) WHERE iso_id = "
							+ sessionData.getProperty("ref_id") + ")";
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLWhere = strSQLWhere + " wt.rep_id IN "
								+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
								+ DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
								+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
								+ DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
								+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
								+ DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId
								+ ")))";
					}
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQLWhere = strSQLWhere + " wt.rep_id IN "
							+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
							+ DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
							+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
							+ DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId
							+ "))";
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						strSQLWhere = strSQLWhere + " wt.rep_id IN "
								+ "(SELECT rep_id FROM reps with (nolock) WHERE type= "
								+ DebisysConstants.REP_TYPE_REP + " AND iso_id = " + strRefId + ")";
					}
					else
						if (strAccessLevel.equals(DebisysConstants.REP))
						{
							strSQLWhere = strSQLWhere + " wt.rep_id = " + strRefId;
						}
						else
							if (strAccessLevel.equals(DebisysConstants.MERCHANT))
							{
								strSQLWhere = strSQLWhere + " wt.merchant_id = " + strRefId;
							}
							else
								if (strAccessLevel.equals(DebisysConstants.CARRIER))
								{
									strSQLWhere = strSQLWhere
											+ " mc.carrierid in (SELECT carrierid FROM Repcarriers WITH (NOLOCK) WHERE repid = "
											+ strRefId + ") ";
								}
			
			if (this.merchant_ids != null && !this.merchant_ids.equals(""))
			{
				strSQLWhere = strSQLWhere + " AND wt.merchant_id in (" + this.merchant_ids + ") ";
			}
			if (this.product_ids != null && !this.product_ids.equals(""))
			{
				strSQLWhere = strSQLWhere + " AND wt.id in (" + this.product_ids + ") ";
			}
			// strSQLTotal = strSQLTotal +
			// " FROM web_transactions wt (nolock), merchants m (nolock) WHERE wt.merchant_id=m.merchant_id AND ";
			
			if (strAccessLevel.equals(DebisysConstants.CARRIER))
			{
				strSQLTotal = strSQLTotal
						+ " FROM web_transactions wt WITH (NOLOCK) "
						+ " inner join merchants m WITH (NOLOCK) ON wt.merchant_id=m.merchant_id "
						+ " inner join products p WITH (NOLOCK) ON p.id = wt.id "
						+ " inner join merchantcarriers mc WITH (NOLOCK) ON mc.merchantid=wt.merchant_id and mc.carrierid = p.carrier_id"
						+ " WHERE ";
			}
			else
			{
				strSQLTotal = strSQLTotal + " FROM web_transactions wt WITH (NOLOCK) "
						+ " inner join merchants m WITH (NOLOCK) ON wt.merchant_id=m.merchant_id "
						+ " inner join products p WITH (NOLOCK) ON p.id = wt.id " + " WHERE ";
				String carrierUserProds = sessionData.getProperty("carrierUserProds");
				if (carrierUserProds != null)
				{
					strSQLWhere += " AND wt.id in (" + carrierUserProds + ") ";
				}
				else
					// no products attached to carrier user
					return null;
				
			}
			
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
			strSQLWhere += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0)
					+ "' and  wt.datetime < '" + vTimeZoneFilterDates.get(1) + "') ";
			strSQLDetail = strSQLDetail
					+ strSQLWhere
					+ " group by wt.merchant_id, m.dba, m.RouteID, m.runningliability, m.liabilitylimit, m.phys_address, m.phys_city, m.phys_state, m.phys_zip, billing_typeId";
			strSQLDetail = strSQLDetail + " order by m.dba asc";
			// String s_positiveBillingTrxTypes =
			// java.util.Arrays.toString(positiveBillingTrxTypes).replaceAll("\\[",
			// "\\(").replaceAll("\\]", "\\)");
			strSQLTotal = strSQLTotal + strSQLWhere + " GROUP BY billing_typeId ";
			
			CarrierReport.cat.debug(strSQLTotal);
			pstmt = dbConn.prepareStatement(strSQLTotal);
			rs = pstmt.executeQuery();
			
			double qty = 0d;
			double total_sales = 0d;
			double merchant_commission = 0d;
			double rep_commission = 0d;
			double subagent_commission = 0d;
			double agent_commission = 0d;
			double iso_commission = 0d;
			double adj_amount = 0d;
			double total_bonus = 0d;
			double total_recharge = 0d;
			while (rs.next())
			{
				if (rs.getDouble("qty") > 0)
				{
					int billingType = rs.getInt("billing_typeId");
					if (java.util.Arrays.binarySearch(positiveBillingTrxTypes, billingType) >= 0)
					{
						qty += Double.parseDouble(rs.getString("qty"));
					}
					total_sales += Double.parseDouble(rs.getString("total_sales"));
					merchant_commission += Double.parseDouble(rs.getString("merchant_commission"));
					rep_commission += Double.parseDouble(rs.getString("rep_commission"));
					subagent_commission += Double.parseDouble(rs.getString("subagent_commission"));
					agent_commission += Double.parseDouble(rs.getString("agent_commission"));
					iso_commission += Double.parseDouble(rs.getString("iso_commission"));
					adj_amount += Double.parseDouble(rs.getString("adj_amount"));
					total_bonus += Double.parseDouble(rs.getString("total_bonus"));
					total_recharge += Double.parseDouble(rs.getString("total_recharge"));
				}
			}
			Vector<String> vecTemp = new Vector<String>();
			vecTemp.add(0, Double.toString(qty)); // Integer.toString
			vecTemp.add(1, Double.toString(total_sales)); // NumberUtil.formatAmount
			vecTemp.add(2, Double.toString(merchant_commission)); // NumberUtil.formatAmount
			vecTemp.add(3, Double.toString(rep_commission)); // NumberUtil.formatAmount
			vecTemp.add(4, Double.toString(subagent_commission)); // NumberUtil.formatAmount
			vecTemp.add(5, Double.toString(agent_commission)); // NumberUtil.formatAmount
			vecTemp.add(6, Double.toString(iso_commission)); // NumberUtil.formatAmount
			vecTemp.add(7, Double.toString(adj_amount)); // NumberUtil.formatAmount
			vecTemp.add(8, Double.toString(total_bonus)); // NumberUtil.formatAmount
			vecTemp.add(9, Double.toString(total_recharge)); // NumberUtil.formatAmount
			vecMerchantSummary.add(0, vecTemp);
			rs.close();
			pstmt.close();
			
			if (CarrierReport.cat.isDebugEnabled())
				CarrierReport.cat.debug(strSQLDetail);
			
			pstmt = dbConn.prepareStatement(strSQLDetail);
			rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				double quantity = rs.getDouble("qty");
				int billingType = rs.getInt("billing_typeId");
				double current_total_sales = Double.parseDouble(rs.getString("total_sales"));
				double current_total_recharge = Double.parseDouble(rs.getString("total_recharge"));
				double current_merchant_commission = Double.parseDouble(rs
						.getString("merchant_commission"));
				double current_rep_commission = Double.parseDouble(rs.getString("rep_commission"));
				double current_subagent_commission = Double.parseDouble(rs
						.getString("subagent_commission"));
				double current_agent_commission = Double.parseDouble(rs
						.getString("agent_commission"));
				double current_iso_commission = Double.parseDouble(rs.getString("iso_commission"));
				String merchant_id = rs.getString("merchant_id");
				
				vecTemp = new Vector<String>();
				vecTemp.add(0, rs.getString("dba")); // StringUtil.toString
				vecTemp.add(1, rs.getString("merchant_id")); // StringUtil.toString
				vecTemp.add(2, rs.getString("qty")); // Integer.toString
				vecTemp.add(3, rs.getString("total_sales")); // NumberUtil.formatAmount
				vecTemp.add(4, rs.getString("merchant_commission")); // NumberUtil.formatAmount
				vecTemp.add(5, rs.getString("rep_commission")); // NumberUtil.formatAmount
				vecTemp.add(6, rs.getString("subagent_commission")); // NumberUtil.formatAmount
				vecTemp.add(7, rs.getString("agent_commission")); // NumberUtil.formatAmount
				vecTemp.add(8, rs.getString("iso_commission")); // NumberUtil.formatAmount
				vecTemp.add(9, rs.getString("runningliability")); // NumberUtil.formatAmount
				vecTemp.add(10, rs.getString("liabilitylimit")); // NumberUtil.formatAmount
				vecTemp.add(11, rs.getString("available_credit")); // NumberUtil.formatAmount
				vecTemp.add(12, rs.getString("adj_amount")); // NumberUtil.formatAmount
				vecTemp.add(13, rs.getString("phys_address")); // StringUtil.toString
				vecTemp.add(14, rs.getString("phys_city")); // StringUtil.toString
				vecTemp.add(15, rs.getString("phys_state")); // StringUtil.toString
				vecTemp.add(16, rs.getString("phys_zip")); // StringUtil.toString
				vecTemp.add(17, rs.getString("total_bonus")); // NumberUtil.formatAmount
				vecTemp.add(18, rs.getString("total_recharge")); // NumberUtil.formatAmount
				
				/* BEGIN Release 21 - Added by YH */
				PreparedStatement pstmtTemp = dbConn.prepareStatement(CarrierReport.sql_bundle
						.getString("getRouteName"));
				pstmtTemp.setInt(1, rs.getInt("RouteID"));
				ResultSet rsTemp = pstmtTemp.executeQuery();
				String strRouteName = "";
				if (rsTemp.next())
				{
					strRouteName = StringUtil.toString(rsTemp.getString("RouteName"));
				}
				vecTemp.add(19, strRouteName);
				rsTemp.close();
				pstmtTemp.close();
				/* END Release 21 - Added by YH */
				if (trxPerMerchantPerBillingType.containsKey(merchant_id))
				{
					Vector<String> cached = trxPerMerchantPerBillingType.get(merchant_id);
					double accumulating_total_sales = Double.parseDouble(cached.get(3));
					double accumulating_total_recharge = Double.parseDouble(cached.get(18));
					double accumulating_merchant_commission = Double.parseDouble(cached.get(4));
					double accumulating_rep_commission = Double.parseDouble(cached.get(5));
					double accumulating_subagent_commission = Double.parseDouble(cached.get(6));
					double accumulating_agent_commission = Double.parseDouble(cached.get(7));
					double accumulating_iso_commission = Double.parseDouble(cached.get(8));
					accumulating_total_sales += current_total_sales;
					accumulating_total_recharge += current_total_recharge;
					accumulating_merchant_commission += current_merchant_commission;
					accumulating_rep_commission += current_rep_commission;
					accumulating_subagent_commission += current_subagent_commission;
					accumulating_agent_commission += current_agent_commission;
					accumulating_iso_commission += current_iso_commission;
					// updating total_sales and total_recharge
					vecTemp.remove(3);
					vecTemp.add(3, new Double(accumulating_total_sales).toString());
					vecTemp.remove(18);
					vecTemp.add(18, new Double(accumulating_total_recharge).toString());
					vecTemp.remove(4);
					vecTemp.add(4, new Double(accumulating_merchant_commission).toString());
					vecTemp.remove(5);
					vecTemp.add(5, new Double(accumulating_rep_commission).toString());
					vecTemp.remove(6);
					vecTemp.add(6, new Double(accumulating_subagent_commission).toString());
					vecTemp.remove(7);
					vecTemp.add(7, new Double(accumulating_agent_commission).toString());
					vecTemp.remove(8);
					vecTemp.add(8, new Double(accumulating_iso_commission).toString());
					double accumulating_qty = Double.parseDouble(cached.get(2));
					// check if current transaction summary is of either negative or positive
					// transactions
					if (java.util.Arrays.binarySearch(positiveBillingTrxTypes, billingType) >= 0)
					{
						accumulating_qty += quantity;
					}
					else
						if (java.util.Arrays.binarySearch(negativeBillingTrxTypes, billingType) >= 0)
						{
							// nothing to do at the time. Negative transactions should not add to
							// trx count.
						}
					// updating qty
					vecTemp.remove(2);
					vecTemp.add(2, new Double(accumulating_qty).toString());
				}
				trxPerMerchantPerBillingType.put(merchant_id, vecTemp);
			}
			// feeding the vector object ..
			vecMerchantSummary.addAll(trxPerMerchantPerBillingType.values());
			rs.close();
			pstmt.close();
			
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getMerchantSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecMerchantSummary;
	}
	
	/**
	 * Gets a list of reps given a list subagent
	 * 
	 * @param list
	 *            of subagent
	 * @return list of reps
	 * @throws ReportException
	 *             Thrown on database errors
	 */
	public static String getRepList(String list) throws ReportException
	{
		String RepList = "";
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			String strSQL = CarrierReport.sql_bundle.getString("getRepList");
			String strSQLWhere = "";
			strSQLWhere = " r.rep_id IN "
					+ "(SELECT r1.rep_id FROM reps AS r1 WITH (NOLOCK) WHERE r1.type = "
					+ DebisysConstants.REP_TYPE_REP + " AND r1.iso_id IN "
					+ "(SELECT r2.rep_id FROM reps AS r2 WITH (NOLOCK) WHERE r2.type = "
					+ DebisysConstants.REP_TYPE_SUBAGENT + " AND r2.rep_id IN (" + list + ") ) ) ";
			
			strSQLWhere = strSQLWhere + " ORDER BY r.businessname";
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				RepList += rs.getString("rep_id").trim() + ";";
				RepList += rs.getString("businessname").replace(";", "").replace(":", "") + ":";
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getRepList", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		
		return RepList;
	}
	
	/**
	 * Gets a list of reps for an iso, agent, or subagent
	 * 
	 * @throws ReportException
	 *             Thrown on database errors
	 */
	
	public static Vector getRepList(SessionData sessionData) throws ReportException
	{
		Vector vecRepList = new Vector();
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			String strSQL = CarrierReport.sql_bundle.getString("getRepList");
			String strSQLWhere = "";
			String strAccessLevel = sessionData.getProperty("access_level");
			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strRefId = sessionData.getProperty("ref_id");
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQLWhere = " r.iso_id=" + strRefId + " AND r.type="
							+ DebisysConstants.REP_TYPE_REP;
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLWhere = " r.rep_id IN "
								+ "(SELECT r1.rep_id FROM reps AS r1 WITH (NOLOCK) WHERE r1.type="
								+ DebisysConstants.REP_TYPE_REP + " AND r1.iso_id IN "
								+ "(SELECT r2.rep_id FROM reps AS r2 WITH (NOLOCK) WHERE r2.type="
								+ DebisysConstants.REP_TYPE_SUBAGENT + " AND r2.iso_id IN "
								+ "(SELECT r3.rep_id FROM reps AS r3 WITH (NOLOCK) WHERE r3.type="
								+ DebisysConstants.REP_TYPE_AGENT + " AND r3.iso_id = " + strRefId
								+ "))) ";
					}
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQLWhere = " r.rep_id IN "
							+ "(SELECT r1.rep_id FROM reps AS r1 WITH (NOLOCK) WHERE r1.type="
							+ DebisysConstants.REP_TYPE_REP + " AND r1.iso_id IN "
							+ "(SELECT r2.rep_id FROM reps AS r2 WITH (NOLOCK) WHERE r2.type="
							+ DebisysConstants.REP_TYPE_SUBAGENT + " AND r2.iso_id = " + strRefId
							+ "))";
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						strSQLWhere = " r.iso_id = " + strRefId + " AND r.type = "
								+ DebisysConstants.REP_TYPE_REP;
					}
			
			strSQLWhere = strSQLWhere + " ORDER BY r.businessname";
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("rep_id"));
				vecTemp.add(rs.getString("businessname"));
				vecRepList.add(vecTemp);
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getRepList", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		
		return vecRepList;
	}
	
	@SuppressWarnings("unchecked")
	public Vector getRepSummary(SessionData sessionData, ServletContext context)
			throws ReportException
	{
		Connection dbConn = null;
		
		Vector vecRepSummary = new Vector();
		
		// String strSQL = CarrierReport.sql_bundle.getString("getRepSummary");
		// String rate_div =
		// " 100 * (CASE ISNULL(transfixedfeeamt,0) WHEN 0 THEN amount ELSE transfixedfeeamt END) ";
		String rate_div = " 100 * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) ";
		String strTotals = "";
		
		strTotals = "count(wt.rec_id) as qty, sum(wt.amount) as total_sales, sum(wt.merchant_rate / "
				+ rate_div
				+ ") as merchant_commission, sum(wt.rep_rate / "
				+ rate_div
				+ ") as rep_commission, sum(wt.agent_rate / "
				+ rate_div
				+ ") as  agent_commission, sum(wt.subagent_rate / "
				+ rate_div
				+ ") as subagent_commission, sum(wt.iso_rate / "
				+ rate_div
				+ ") as iso_commission, sum((wt.amount * (100-wt.iso_discount_rate)/100)*cast(wt.iso_discount_rate as bit)) as adj_amount, sum(wt.bonus_amount) as total_bonus, (sum(wt.bonus_amount) + sum(wt.amount)) as total_recharge";
		
		// String strSQL = "SELECT wt.businessname, wt.rep_id, "+ strTotals
		// +", r.address, r.city, r.state, r.zip FROM web_transactions wt (nolock), reps r (nolock)";
		String strSQL = "SELECT wt.businessname, wt.rep_id, "
				+ strTotals
				+ ", r.address, r.city, r.state, r.zip FROM web_transactions wt with (nolock) inner join products p with (nolock) ON p.id = wt.id inner join reps r with (nolock) ON wt.rep_id = r.rep_id ";
		// String strSQLcount = "SELECT "+ strTotals
		// +" FROM web_transactions wt (nolock), reps r (nolock) ";
		String strSQLcount = "SELECT "
				+ strTotals
				+ " FROM web_transactions wt with (nolock) inner join products p WITH (NOLOCK) ON p.id = wt.id inner join reps r WITH (NOLOCK) ON wt.rep_id = r.rep_id";
		// String strSQLcond = "WHERE wt.rep_id = r.rep_id AND ";
		String strSQLcond = " WHERE ";
		
		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		try
		{
			
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			if (strAccessLevel.equals(DebisysConstants.REP)
					|| strAccessLevel.equals(DebisysConstants.MERCHANT))
			{
				strSQLcond += " 0 =  0 ";
			}
			else
			{
				strSQLcond += " wt.rep_id in ";
			}
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQLcond += " (SELECT rep_id FROM reps WITH(NOLOCK) WHERE type="
							+ DebisysConstants.REP_TYPE_REP + " AND iso_id = " + strRefId + ") ";
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
								+ DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
								+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = "
								+ DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
								+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
								+ DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId
								+ "))) ";
					}
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
							+ DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
							+ "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type = "
							+ DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId
							+ ")) ";
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						strSQLcond += " (SELECT rep_id FROM reps WITH (NOLOCK) WHERE type="
								+ DebisysConstants.REP_TYPE_REP + " AND iso_id =" + strRefId + ") ";
					}
			if (this.rep_ids != null && !this.rep_ids.equals(""))
			{
				strSQLcond += " AND wt.rep_id in (" + this.rep_ids + ") ";
			}
			
			String carrierUserProds = sessionData.getProperty("carrierUserProds");
			if (carrierUserProds != null)
			{
				strSQL += " AND wt.id in (" + carrierUserProds + ") ";
				strSQLcount += " AND wt.id in (" + carrierUserProds + ") ";
			}
			else
				// no products attached to carrier user
				return null;
			
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, this.end_date + " 23:59:59.999", false);
			strSQLcond += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0)
					+ "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";
			
			strSQLcount += strSQLcond;// counter no needs grouping and order
			
			strSQLcond += " group by wt.rep_id, wt.businessname, r.address, r.city, r.state, r.zip ";
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";
			try
			{
				if (this.col != null && this.sort != null && !this.col.equals("")
						&& !this.sort.equals(""))
				{
					intCol = Integer.parseInt(this.col);
					intSort = Integer.parseInt(this.sort);
				}
			}
			catch (NumberFormatException nfe)
			{
				intCol = 0;
				intSort = 0;
			}
			if (intSort == 2)
			{
				strSort = "DESC";
			}
			else
			{
				strSort = "ASC";
			}
			
			switch (intCol)
			{
				case 1:
					strCol = "wt.businessname";
					break;
				case 2:
					strCol = "wt.rep_id";
					break;
				case 3:
					strCol = "qty";
					break;
				case 4:
					strCol = "total_sales";
					break;
				case 5:
					strCol = "merchant_commission";
					break;
				case 6:
					strCol = "rep_commission";
					break;
				case 7:
					strCol = "subagent_commission";
					break;
				case 8:
					strCol = "agent_commission";
					break;
				case 9:
					strCol = "iso_commission";
					break;
				case 10:
					strCol = "adj_amount";
					break;
				case 11:
					strCol = "r.address";
					break;
				case 12:
					strCol = "r.city";
					break;
				case 13:
					strCol = "r.state";
					break;
				case 14:
					strCol = "r.zip";
					break;
				case 17:
					strCol = "total_bonus";
					break;
				case 18:
					strCol = "total_recharge";
					break;
				default:
					strCol = "wt.businessname";
					break;
			}
			strSQLcond += " order by " + strCol + " " + strSort;
			strSQL += strSQLcond;
			
			CarrierReport.cat.debug(strSQL);
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
				vecTemp.add(StringUtil.toString(rs.getString("address")));
				vecTemp.add(StringUtil.toString(rs.getString("city")));
				vecTemp.add(StringUtil.toString(rs.getString("state")));
				vecTemp.add(StringUtil.toString(rs.getString("zip")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				vecRepSummary.add(vecTemp);
			}
			pstmt.close();
			rs.close();
			pstmt = null;
			rs = null;
			// now the counter
			CarrierReport.cat.debug(strSQLcount);
			pstmt = dbConn.prepareStatement(strSQLcount);
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add("TOTALS");
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("adj_amount")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_bonus")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				vecRepSummary.add(vecTemp);
			}
			pstmt.close();
			rs.close();
			pstmt = null;
			rs = null;
			
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getRepSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecRepSummary;
	}
	
	public static Vector getSubAgentList(SessionData sessionData) throws ReportException
	{
		Vector vecRepList = new Vector();
		Connection dbConn = null;
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			String strSQL = CarrierReport.sql_bundle.getString("getRepList");
			String strSQLWhere = "";
			String strAccessLevel = sessionData.getProperty("access_level");
			String strDistChainType = sessionData.getProperty("dist_chain_type");
			String strRefId = sessionData.getProperty("ref_id");
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				// if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				// {
				strSQLWhere = " r.rep_id IN (SELECT r1.rep_id FROM reps AS r1 WITH (NOLOCK) WHERE r1.type="
						+ DebisysConstants.REP_TYPE_SUBAGENT
						+ " AND r1.iso_id IN (SELECT r2.rep_id FROM reps AS r2 WITH (NOLOCK) WHERE r2.type="
						+ DebisysConstants.REP_TYPE_AGENT + " AND r2.iso_id = " + strRefId + ")) ";
				// }
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQLWhere = " r.iso_id=" + strRefId + " AND r.type="
							+ DebisysConstants.REP_TYPE_SUBAGENT;
				}
			
			PreparedStatement pstmt = dbConn.prepareStatement(strSQL + strSQLWhere);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(rs.getString("rep_id"));
				vecTemp.add(rs.getString("businessname"));
				vecRepList.add(vecTemp);
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getSubAgentList", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		
		return vecRepList;
	}
	
	public Vector getSubAgentSummary(SessionData sessionData, ServletContext application)
			throws ReportException
	{
		
		Connection dbConn = null;
		Vector vecSubAgentSummary = new Vector();
		String strSQL = "";
		// String rate_div =
		// " 100 * (CASE ISNULL(transfixedfeeamt,0) WHEN 0 THEN amount ELSE transfixedfeeamt END) ";
		String rate_div = " 100 * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) ";
		
		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		strSQL = CarrierReport.sql_bundle.getString("getSubAgentSummary");
		
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			strSQL = strSQL + " wt.rep_id in ";
			
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL = strSQL
							+ " (SELECT r4.rep_id FROM reps AS r4 WITH(NOLOCK) WHERE r4.type="
							+ DebisysConstants.REP_TYPE_REP + " AND r4.iso_id IN "
							+ "(SELECT r5.rep_id FROM reps AS r5 WITH(NOLOCK) WHERE r5.type = "
							+ DebisysConstants.REP_TYPE_SUBAGENT + " AND r5.iso_id IN "
							+ "(SELECT r6.rep_id FROM reps AS r6 WITH(NOLOCK) WHERE r6.type="
							+ DebisysConstants.REP_TYPE_AGENT + " AND r6.iso_id = " + strRefId
							+ "))) ";
				}
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQL = strSQL
							+ " (SELECT r3.rep_id FROM reps AS r3 WITH(NOLOCK) WHERE r3.type="
							+ DebisysConstants.REP_TYPE_REP + " AND r3.iso_id IN "
							+ "(SELECT r4.rep_id FROM reps AS r4 WITH(NOLOCK) WHERE r4.type = "
							+ DebisysConstants.REP_TYPE_SUBAGENT + " AND r4.iso_id = " + strRefId
							+ ")) ";
				}
			// ATE 9/13/07 Doing the join explicitly in the query now
			// not anymore...bc of performance
			// strSQL = strSQL + " AND wt.rep_id=r1.rep_id AND r1.iso_id=r2.rep_id ";
			strSQL = strSQL + " ";
			
			if (this.rep_ids != null && !this.rep_ids.equals(""))
			{
				strSQL = strSQL + " AND r2.rep_id in (" + this.rep_ids + ") ";
			}
			
			String carrierUserProds = sessionData.getProperty("carrierUserProds");
			if (carrierUserProds != null)
			{
				strSQL += " AND wt.id in (" + carrierUserProds + ") ";
			}
			else
				// no products attached to carrier user
				return null;
			
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, this.end_date + " 23:59:59.999", false);
			strSQL = strSQL + " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0)
					+ "' and  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";
			strSQL = strSQL
					+ " group by r2.rep_id, r2.businessname, r2.address, r2.city, r2.state, r2.zip ";
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";
			
			try
			{
				if (this.col != null && this.sort != null && !this.col.equals("")
						&& !this.sort.equals(""))
				{
					intCol = Integer.parseInt(this.col);
					intSort = Integer.parseInt(this.sort);
				}
			}
			catch (NumberFormatException nfe)
			{
				intCol = 0;
				intSort = 0;
			}
			
			if (intSort == 2)
			{
				strSort = "DESC";
			}
			else
			{
				strSort = "ASC";
			}
			
			switch (intCol)
			{
				case 1:
					strCol = "r2.businessname";
					break;
				case 2:
					strCol = "r2.rep_id";
					break;
				case 3:
					strCol = "qty";
					break;
				case 4:
					strCol = "total_sales";
					break;
				case 5:
					strCol = "merchant_commission";
					break;
				case 6:
					strCol = "rep_commission";
					break;
				case 7:
					strCol = "subagent_commission";
					break;
				case 8:
					strCol = "agent_commission";
					break;
				case 9:
					strCol = "iso_commission";
					break;
				case 10:
					strCol = "adj_amount";
					break;
				case 11:
					strCol = "r2.address";
					break;
				case 12:
					strCol = "r2.city";
					break;
				case 13:
					strCol = "r2.state";
					break;
				case 14:
					strCol = "r2.zip";
					break;
				
				case 15:
					strCol = "total_bonus";
					break;
				
				case 16:
					strCol = "total_recharge";
					break;
				
				case 17:
					strCol = "tax_total_sales";
					break;
				
				case 18:
					strCol = "tax_amount";
					break;
				
				case 19:
					strCol = "tax_merchant_commission";
					break;
				case 20:
					strCol = "tax_rep_commission";
					break;
				case 21:
					strCol = "tax_subagent_commission";
					break;
				case 22:
					strCol = "tax_agent_commission";
					break;
				case 23:
					strCol = "tax_iso_commission";
					break;
				
				default:
					strCol = "r2.businessname";
					break;
			}
			
			strSQL = strSQL + " order by " + strCol + " " + strSort;
			CarrierReport.cat.debug(strSQL);
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("adj_amount")));
				vecTemp.add(StringUtil.toString(rs.getString("address")));
				vecTemp.add(StringUtil.toString(rs.getString("city")));
				vecTemp.add(StringUtil.toString(rs.getString("state")));
				vecTemp.add(StringUtil.toString(rs.getString("zip")));
				if (DebisysConfigListener.getDeploymentType(application).equals(
						DebisysConstants.DEPLOYMENT_INTERNATIONAL)
						&& DebisysConfigListener.getCustomConfigType(application).equals(
								DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
					vecTemp.add(StringUtil.toString(rs.getString("total_bonus")));// Total bonus
					vecTemp.add(StringUtil.toString(rs.getString("total_recharge")));// Total
					// Recharge
				}
				vecSubAgentSummary.add(vecTemp);
			}
			rs.close();
			pstmt.close();
			
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getSubAgentSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecSubAgentSummary;
	}
	
	public Vector getAgentSummary(SessionData sessionData, ServletContext application)
			throws ReportException
	{
		Connection dbConn = null;
		Vector vecAgentSummary = new Vector();
		
		String strSQL = "";
		
		String strRefId = sessionData.getProperty("ref_id");
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		// String rate_div =
		// " 100 * (CASE ISNULL(transfixedfeeamt,0) WHEN 0 THEN amount ELSE transfixedfeeamt END) ";
		String rate_div = " 100 * (CASE WHEN p.ach_type = 6 THEN (wt.Fixed_trans_fee + wt.transFixedFeeAmt) ELSE wt.amount END) ";
		
		strSQL = CarrierReport.sql_bundle.getString("getAgentSummary");
		
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			strSQL = strSQL + " wt.rep_id in ";
			
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				{
					strSQL = strSQL
							+ " (SELECT r4.rep_id FROM reps AS r4 WITH (NOLOCK) WHERE r4.type = "
							+ DebisysConstants.REP_TYPE_REP + " AND r4.iso_id IN "
							+ "(SELECT r5.rep_id FROM reps AS r5 WITH (NOLOCK) WHERE r5.type = "
							+ DebisysConstants.REP_TYPE_SUBAGENT + " AND r5.iso_id IN "
							+ "(SELECT r6.rep_id FROM reps AS r6 WITH (NOLOCK) WHERE r6.type = "
							+ DebisysConstants.REP_TYPE_AGENT + " AND r6.iso_id = " + strRefId
							+ "))) ";
				}
				
			}
			
			// ATE 9/13/07 Doing the join explicitly in the query now
			// not anymore bc of performance issues
			// strSQL = strSQL +
			// " AND wt.rep_id=r1.rep_id AND r1.iso_id=r2.rep_id AND r2.iso_id=r3.rep_id ";
			strSQL = strSQL + " ";
			
			if (this.rep_ids != null && !this.rep_ids.equals(""))
			{
				strSQL += " AND r3.rep_id IN (" + this.rep_ids + ") ";
			}
			
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, this.end_date + " 23:59:59.999", false);
			strSQL += " AND (wt.datetime >= '" + vTimeZoneFilterDates.get(0)
					+ "' AND  wt.datetime <= '" + vTimeZoneFilterDates.get(1) + "') ";
			
			String carrierUserProds = sessionData.getProperty("carrierUserProds");
			if (carrierUserProds != null)
			{
				if (carrierUserProds.equals(""))
					return null;
				strSQL += " AND wt.id in (" + carrierUserProds + ") ";
			}
			else
				// no products attached to carrier user
				return null;
			
			strSQL += " GROUP BY r3.rep_id, r3.businessname, r3.address, r3.city, r3.state, r3.zip ";
			int intCol = 0;
			int intSort = 0;
			String strCol = "";
			String strSort = "";
			
			try
			{
				if (this.col != null && this.sort != null && !this.col.equals("")
						&& !this.sort.equals(""))
				{
					intCol = Integer.parseInt(this.col);
					intSort = Integer.parseInt(this.sort);
				}
			}
			catch (NumberFormatException nfe)
			{
				intCol = 0;
				intSort = 0;
			}
			
			if (intSort == 2)
			{
				strSort = "DESC";
			}
			else
			{
				strSort = "ASC";
			}
			
			switch (intCol)
			{
				case 1:
					strCol = "r3.businessname";
					break;
				case 2:
					strCol = "r3.rep_id";
					break;
				case 3:
					strCol = "qty";
					break;
				case 4:
					strCol = "total_sales";
					break;
				case 5:
					strCol = "merchant_commission";
					break;
				case 6:
					strCol = "rep_commission";
					break;
				case 7:
					strCol = "subagent_commission";
					break;
				case 8:
					strCol = "agent_commission";
					break;
				case 9:
					strCol = "iso_commission";
					break;
				case 10:
					strCol = "adj_amount";
					break;
				case 11:
					strCol = "r3.address";
					break;
				case 12:
					strCol = "r3.city";
					break;
				case 13:
					strCol = "r3.state";
					break;
				case 14:
					strCol = "r3.zip";
					break;
				case 15:
					strCol = "total_bonus";
					break;
				case 16:
					strCol = "total_recharge";
					break;
				case 17:
					strCol = "tax_total_sales";
					break;
				case 18:
					strCol = "tax_amount";
					break;
				case 19:
					strCol = "tax_merchant_commission";
					break;
				case 20:
					strCol = "tax_rep_commission";
					break;
				case 21:
					strCol = "tax_subagent_commission";
					break;
				case 22:
					strCol = "tax_agent_commission";
					break;
				case 23:
					strCol = "tax_iso_commission";
					break;
				
				default:
					strCol = "r3.businessname";
					break;
			}
			
			strSQL = strSQL + " order by " + strCol + " " + strSort;
			CarrierReport.cat.debug(strSQL);
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement(strSQL);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("businessname")));
				vecTemp.add(StringUtil.toString(rs.getString("rep_id")));
				vecTemp.add(rs.getString("qty"));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("total_sales")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("merchant_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("rep_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("subagent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("agent_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("iso_commission")));
				vecTemp.add(NumberUtil.formatAmount(rs.getString("adj_amount")));
				vecTemp.add(StringUtil.toString(rs.getString("address")));
				vecTemp.add(StringUtil.toString(rs.getString("city")));
				vecTemp.add(StringUtil.toString(rs.getString("state")));
				vecTemp.add(StringUtil.toString(rs.getString("zip")));
				if (DebisysConfigListener.getDeploymentType(application).equals(
						DebisysConstants.DEPLOYMENT_INTERNATIONAL)
						&& DebisysConfigListener.getCustomConfigType(application).equals(
								DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_bonus")));
					vecTemp.add(NumberUtil.formatAmount(rs.getString("total_recharge")));
				}
				vecAgentSummary.add(vecTemp);
			}
			rs.close();
			pstmt.close();
			
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getAgentSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecAgentSummary;
	}
	
	public Vector getPhoneSummary(SessionData sessionData, ServletContext application)
			throws ReportException
	{
		Connection dbConn = null;
		Vector vecPhoneSummary = new Vector();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		String strSQLSummary = "";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strCarrierUserProds = sessionData.getProperty("carrierUserProds");
		
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					
					strSQLSummary = CarrierReport.sql_bundle.getString("getPhoneSummary3");
					
					strSQLSummary = strSQLSummary.replaceAll("_ACCESSLEVEL_", strAccessLevel);
					strSQLSummary = strSQLSummary.replaceAll("_REFID_", strRefId);
					if (strCarrierUserProds != null)
					{
						strSQLSummary += " AND wt.id in (" + strCarrierUserProds + ") ";
					}
					else
						// no products attached to carrier user
						return null;
					
					pstmt = dbConn.prepareStatement(strSQLSummary);
					pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt.setDouble(2, Double.parseDouble(strRefId));
					pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
					pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
					pstmt.setString(5, this.phone_number);
					this.logdebug("[getPhoneSummary:ISO3L] SQL pstmt: " + strSQLSummary);
					this.logdebug("[getPhoneSummary:ISO3L] params: 1="
							+ Integer.parseInt(DebisysConstants.REP_TYPE_REP) + "; 2="
							+ Double.parseDouble(strRefId) + "; 3=" + vTimeZoneFilterDates.get(0)
							+ "; 4=" + vTimeZoneFilterDates.get(1) + "; 5=" + this.phone_number);
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						
						strSQLSummary = CarrierReport.sql_bundle.getString("getPhoneSummary5");
						
						strSQLSummary = strSQLSummary.replaceAll("_ACCESSLEVEL_", strAccessLevel);
						strSQLSummary = strSQLSummary.replaceAll("_REFID_", strRefId);
						if (strCarrierUserProds != null)
						{
							strSQLSummary += " AND wt.id in (" + strCarrierUserProds + ") ";
						}
						else
							// no products attached to carrier user
							return null;
						pstmt = dbConn.prepareStatement(strSQLSummary);
						pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
						pstmt.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
						pstmt.setDouble(4, Double.parseDouble(strRefId));
						pstmt.setString(5, vTimeZoneFilterDates.get(0).toString());
						pstmt.setString(6, vTimeZoneFilterDates.get(1).toString());
						pstmt.setString(7, this.phone_number);
						this.logdebug("[getPhoneSummary:ISO5L] SQL pstmt: " + strSQLSummary);
						this.logdebug("[getPhoneSummary:ISO5L] params: 1="
								+ Integer.parseInt(DebisysConstants.REP_TYPE_REP) + "; 2="
								+ Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT) + "; 3="
								+ Integer.parseInt(DebisysConstants.REP_TYPE_AGENT) + "; 4="
								+ Double.parseDouble(strRefId) + "; 5="
								+ vTimeZoneFilterDates.get(0) + "; 6="
								+ vTimeZoneFilterDates.get(1) + "; 7=" + this.phone_number);
					}
			}
			// DBSY-569 SW
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					
					strSQLSummary = CarrierReport.sql_bundle.getString("getPhoneSummary8");
					
					strSQLSummary = strSQLSummary.replaceAll("_ACCESSLEVEL_", strAccessLevel);
					strSQLSummary = strSQLSummary.replaceAll("_REFID_", strRefId);
					if (strCarrierUserProds != null)
					{
						strSQLSummary += " AND wt.id in (" + strCarrierUserProds + ") ";
					}
					else
						// no products attached to carrier user
						return null;
					pstmt = dbConn.prepareStatement(strSQLSummary);
					pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
					pstmt.setDouble(3, Double.parseDouble(strRefId));
					CarrierReport.cat.debug(strRefId);
					pstmt.setString(4, vTimeZoneFilterDates.get(0).toString());
					pstmt.setString(5, vTimeZoneFilterDates.get(1).toString());
					pstmt.setString(6, this.phone_number);
					this.logdebug("[getPhoneSummary:Agent] SQL pstmt: " + strSQLSummary);
					this.logdebug("[getPhoneSummary:Agent] params: 1="
							+ Integer.parseInt(DebisysConstants.REP_TYPE_REP) + "; 2="
							+ Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT) + "; 3="
							+ Double.parseDouble(strRefId) + "; 4=" + vTimeZoneFilterDates.get(0)
							+ "; 5=" + vTimeZoneFilterDates.get(1) + "; 6=" + this.phone_number);
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						
						strSQLSummary = CarrierReport.sql_bundle.getString("getPhoneSummary9");
						
						strSQLSummary = strSQLSummary.replaceAll("_ACCESSLEVEL_", strAccessLevel);
						strSQLSummary = strSQLSummary.replaceAll("_REFID_", strRefId);
						if (strCarrierUserProds != null)
						{
							strSQLSummary += " AND wt.id in (" + strCarrierUserProds + ") ";
						}
						else
							// no products attached to carrier user
							return null;
						pstmt = dbConn.prepareStatement(strSQLSummary);
						pstmt.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt.setDouble(2, Double.parseDouble(strRefId));
						pstmt.setString(3, vTimeZoneFilterDates.get(0).toString());
						pstmt.setString(4, vTimeZoneFilterDates.get(1).toString());
						pstmt.setString(5, this.phone_number);
						this.logdebug("[getPhoneSummary:SubAgent] SQL pstmt: " + strSQLSummary);
						this.logdebug("[getPhoneSummary:SubAgent] params: 1="
								+ Integer.parseInt(DebisysConstants.REP_TYPE_REP) + "; 2="
								+ Double.parseDouble(strRefId) + "; 3="
								+ vTimeZoneFilterDates.get(0) + "; 4="
								+ vTimeZoneFilterDates.get(1) + "; 5=" + this.phone_number);
					}
					// END DBSY-569 SW
					else
						if (strAccessLevel.equals(DebisysConstants.REP))
						{
							
							strSQLSummary = CarrierReport.sql_bundle.getString("getPhoneSummary6");
							
							strSQLSummary = strSQLSummary.replaceAll("_ACCESSLEVEL_",
									strAccessLevel);
							strSQLSummary = strSQLSummary.replaceAll("_REFID_", strRefId);
							if (strCarrierUserProds != null)
							{
								strSQLSummary += " AND wt.id in (" + strCarrierUserProds + ") ";
							}
							else
								// no products attached to carrier user
								return null;
							pstmt = dbConn.prepareStatement(strSQLSummary);
							pstmt.setDouble(1, Double.parseDouble(strRefId));
							pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
							pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
							pstmt.setString(4, this.phone_number);
							this.logdebug("[getPhoneSummary:Rep] SQL pstmt: " + strSQLSummary);
							this.logdebug("[getPhoneSummary:Rep] params: 1="
									+ Double.parseDouble(strRefId) + "; 2="
									+ vTimeZoneFilterDates.get(0) + "; 3="
									+ vTimeZoneFilterDates.get(1) + "; 4=" + this.phone_number);
						}
						else
							if (strAccessLevel.equals(DebisysConstants.MERCHANT))
							{
								
								strSQLSummary = CarrierReport.sql_bundle
										.getString("getPhoneSummary7");
								
								strSQLSummary = strSQLSummary.replaceAll("_ACCESSLEVEL_",
										strAccessLevel);
								strSQLSummary = strSQLSummary.replaceAll("_REFID_", strRefId);
								if (strCarrierUserProds != null)
								{
									strSQLSummary += " AND wt.id in (" + strCarrierUserProds + ") ";
								}
								else
									// no products attached to carrier user
									return null;
								pstmt = dbConn.prepareStatement(strSQLSummary);
								pstmt.setDouble(1, Double.parseDouble(strRefId));
								pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
								pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
								pstmt.setString(4, this.phone_number);
								this.logdebug("[getPhoneSummary:Merchant] SQL pstmt: "
										+ strSQLSummary);
								this.logdebug("[getPhoneSummary:Merchant] params: 1="
										+ Double.parseDouble(strRefId) + "; 2="
										+ vTimeZoneFilterDates.get(0) + "; 3="
										+ vTimeZoneFilterDates.get(1) + "; 4=" + this.phone_number);
							}
			
			rs = pstmt.executeQuery();
			
			if (rs.next())
			{
				// Added in date formatting for Localization - SW
				vecPhoneSummary.add(0, "1");
				vecPhoneSummary.add(1, DateUtil.formatDateTime(this.start_date) + " - "
						+ DateUtil.formatDateTime(this.end_date));
				vecPhoneSummary.add(2, Integer.toString(rs.getInt("top_up_count")));
				vecPhoneSummary.add(3, NumberUtil.formatAmount(Double.toString(rs
						.getDouble("total_amount"))));
				vecPhoneSummary.add(4, Integer.toString(rs.getInt("location_count")));
				if (DebisysConfigListener.getDeploymentType(application).equals(
						DebisysConstants.DEPLOYMENT_INTERNATIONAL)
						&& DebisysConfigListener.getCustomConfigType(application).equals(
								DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
					vecPhoneSummary.add(5, NumberUtil.formatAmount(Double.toString(rs
							.getDouble("total_bonus"))));
					vecPhoneSummary.add(6, NumberUtil.formatAmount(Double.toString(rs
							.getDouble("total_recharge"))));
					
				}
			}
			
			this.logdebug("[getPhoneSummary:vecResult] contents: (0)=" + vecPhoneSummary.get(0)
					+ "; (1)=" + vecPhoneSummary.get(1) + "; (2)=" + vecPhoneSummary.get(2)
					+ "; (3)=" + vecPhoneSummary.get(3));
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getPhoneSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecPhoneSummary;
	}
	
	// this method is added by Ken on 04-19-2006
	public Vector getDailyLiability(SessionData sessionData, boolean frtCurrency)
			throws ReportException
	{
		Connection dbConn = null;
		Vector vecDailyLiability = new Vector();
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		formatter.setLenient(false);
		String currentDate = formatter.format(new Date(System.currentTimeMillis()));
		
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strSQLDailyLiability = CarrierReport.sql_bundle.getString("getCarrierDailyLiability");
		
		// Alfred A./DBSY-1082 - Modified the select statement to acquire business names.
		if (strAccessLevel.equals(DebisysConstants.ISO)
				&& strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		{
			strSQLDailyLiability = CarrierReport.sql_bundle
					.getString("getCarrierDailyLiabilitySubAgentLevel");
		}
		else
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				strSQLDailyLiability = CarrierReport.sql_bundle
						.getString("getCarrierDailyLiabilityISOLevel");
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					strSQLDailyLiability = CarrierReport.sql_bundle
							.getString("getCarrierDailyLiabilityAgentLevel");
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						strSQLDailyLiability = CarrierReport.sql_bundle
								.getString("getCarrierDailyLiabilitySubAgentLevel");
					}
		
		
		
		try
		{
			strSQLDailyLiability=strSQLDailyLiability.replace("LASTPURCHASEPARAM", "MAX(dbo.GetLocalTimeByAccessLevel(" + strAccessLevel + ", " + sessionData.getProperty("ref_id") + ", wt.datetime, 1) )as lastpurchase");
			 Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(sessionData.getProperty("access_level"), sessionData.getProperty("ref_id"),DateUtil.addSubtractDays(currentDate, -30), DateUtil.addSubtractDays(currentDate, 1) , false);
			 String strSQLWhere = " WHERE (wt.datetime >= '"
					+ vTimeZoneFilterDates.get(0) + "' and  wt.datetime < '"
					+ vTimeZoneFilterDates.get(1) + "') AND";
			String strRefId = sessionData.getProperty("ref_id");
			String strJOIN = "";
			String strSQLLEFTJOIN = "";
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			// get a count of total matches
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					// strSQLWhere = strSQLWhere +
					// " wt.rep_id IN (SELECT rep_id FROM reps WITH (NOLOCK) WHERE iso_id = " +
					// sessionData.getProperty("ref_id") + ")";
					System.out.println("DailyLiability ISO 3");
					strJOIN += " INNER JOIN merchants m WITH(NOLOCK) ON (m.rep_id = r.rep_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) "
							+ " INNER JOIN Web_Transactions wt WITH(NOLOCK) ON (wt.merchant_id = m.merchant_id ) ";
					strSQLWhere += " r.iso_id = " + sessionData.getProperty("ref_id") + " ";
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						
						// strSQLWhere = strSQLWhere + " wt.rep_id IN " +
						// "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " +
						// DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
						// + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " +
						// DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id IN "
						// + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " +
						// DebisysConstants.REP_TYPE_AGENT + " AND iso_id = " + strRefId + ")))";
						System.out.println("DailyLiability ISO 5");
						strSQLWhere += " r.type= " + DebisysConstants.REP_TYPE_REP
								+ " AND a.iso_id = " + strRefId + " ";
						strJOIN += "INNER JOIN reps sa WITH(NOLOCK) ON  sa.rep_id=r.iso_id and sa.type= "
								+ DebisysConstants.REP_TYPE_SUBAGENT
								+ " INNER JOIN reps a WITH(NOLOCK) ON a.rep_id = sa.iso_id and a.type= "
								+ DebisysConstants.REP_TYPE_AGENT
								+ " INNER JOIN merchants m WITH(NOLOCK) ON (m.rep_id = r.rep_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) "
								+ " INNER JOIN Web_Transactions wt (NOLOCK) ON   (wt.merchant_id = m.merchant_id ) ";
						
					}
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					// strSQLWhere = strSQLWhere + " wt.rep_id IN " +
					// "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " +
					// DebisysConstants.REP_TYPE_REP + " AND iso_id IN "
					// + "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " +
					// DebisysConstants.REP_TYPE_SUBAGENT + " AND iso_id = " + strRefId + "))";
					
					System.out.println("DailyLiability Agent");
					strSQLWhere += " r.type= " + DebisysConstants.REP_TYPE_REP
							+ " AND sa.iso_id = " + strRefId + " ";
					strJOIN += "INNER JOIN reps sa WITH(NOLOCK) ON  sa.rep_id=r.iso_id and sa.type= "
							+ DebisysConstants.REP_TYPE_SUBAGENT
							+ " INNER JOIN merchants m WITH(NOLOCK) ON (m.rep_id = r.rep_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) "
							+ " INNER JOIN Web_Transactions wt (NOLOCK) ON   (wt.merchant_id = m.merchant_id ) ";
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						// strSQLWhere = strSQLWhere + " wt.rep_id IN " +
						// "(SELECT rep_id FROM reps WITH (NOLOCK) WHERE type= " +
						// DebisysConstants.REP_TYPE_REP + " AND iso_id = "
						// + strRefId + ")";
						System.out.println("DailyLiability Sub-Agent");
						strSQLWhere += " r.type= " + DebisysConstants.REP_TYPE_REP
								+ " AND r.iso_id = " + strRefId + " ";
						strJOIN += " INNER JOIN merchants m WITH(NOLOCK) ON (m.rep_id = r.rep_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) "
								+ " INNER JOIN Web_Transactions wt (NOLOCK) ON   (wt.merchant_id = m.merchant_id ) ";
					}
					
					else
						if (strAccessLevel.equals(DebisysConstants.REP))
						{
							System.out.println("DailyLiability Rep");
							strSQLWhere += " wt.rep_id =  " + strRefId + " ";
							strJOIN += " INNER JOIN merchants m WITH(NOLOCK) ON (m.rep_id = r.rep_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) "
									+ " INNER JOIN Web_Transactions wt (NOLOCK) ON   (wt.merchant_id = m.merchant_id )  ";
							
						}
						else
							if (strAccessLevel.equals(DebisysConstants.MERCHANT))
							{
								System.out.println("DailyLiability Merchant");
								strSQLWhere = strSQLWhere + " wt.merchant_id = " + strRefId;
								strJOIN += " INNER JOIN merchants m WITH(NOLOCK) ON (m.rep_id = r.rep_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) "
										+ " INNER JOIN Web_Transactions wt (NOLOCK) ON  (wt.merchant_id = m.merchant_id )  ";
							}
			
			if (this.merchant_ids != null && !this.merchant_ids.equals(""))
			{
				strSQLWhere = strSQLWhere + " AND wt.merchant_id in (" + this.merchant_ids + ")";
			}
			else
				if (this.merchant_ids.equals(""))
				{
					strSQLWhere = strSQLWhere + " AND wt.merchant_id in ("
							+ "666666666666666666666666666666666" + ")";
				}
			
			String carrierUserProds = sessionData.getProperty("carrierUserProds");
			if (carrierUserProds != null)
			{
				strSQLWhere += " AND wt.id in (" + carrierUserProds + ") ";
			}
			else
				// no products attached to carrier user
				return null;
			
			// Alfred A./DBSY-1082 - Modified the Group By section to account for the difference in
			// the select statements above.
			if (strAccessLevel.equals(DebisysConstants.ISO)
					&& strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
			{
				strSQLDailyLiability = strSQLDailyLiability
						+ strJOIN
						+ strSQLWhere
						+ " GROUP BY r.businessname, wt.merchant_id, m.dba, m.contact_phone,m.runningliability, m.liabilitylimit, m.phys_country HAVING sum(amount) > 0 ORDER BY remaining_days";
			}
			else
				if (strAccessLevel.equals(DebisysConstants.ISO))
				{
					strSQLDailyLiability = strSQLDailyLiability
							+ strJOIN
							+ strSQLWhere
							+ " GROUP BY a.businessname, sa.businessname, r.businessname, wt.merchant_id, m.dba, m.contact_phone,m.runningliability, m.liabilitylimit, m.phys_country HAVING sum(amount) > 0 ORDER BY remaining_days";
				}
				else
					if (strAccessLevel.equals(DebisysConstants.AGENT))
					{
						strSQLDailyLiability = strSQLDailyLiability
								+ strJOIN
								+ strSQLWhere
								+ " GROUP BY sa.businessname, r.businessname, wt.merchant_id, m.dba, m.contact_phone,m.runningliability, m.liabilitylimit, m.phys_country HAVING sum(amount) > 0 ORDER BY remaining_days";
					}
					else
						if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
						{
							strSQLDailyLiability = strSQLDailyLiability
									+ strJOIN
									+ strSQLWhere
									+ " GROUP BY r.businessname, wt.merchant_id, m.dba, m.contact_phone,m.runningliability, m.liabilitylimit, m.phys_country HAVING sum(amount) > 0 ORDER BY remaining_days";
						}
						else
						{
							strSQLDailyLiability = strSQLDailyLiability
									+ strJOIN
									+ strSQLWhere
									+ " GROUP BY wt.merchant_id, m.dba, m.contact_phone,m.runningliability, m.liabilitylimit, m.phys_country HAVING sum(amount) > 0 ORDER BY remaining_days";
						}
			
			CarrierReport.cat.debug(strSQLDailyLiability);
			PreparedStatement pstmt = null;
			pstmt = dbConn.prepareStatement(strSQLDailyLiability);
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				String remainingDays = rs.getString("remaining_days");
				if (remainingDays != null && !remainingDays.equals(""))
				{
					try
					{
						DecimalFormat df = new DecimalFormat("###0.0");
						remainingDays = df.format(Double.parseDouble(remainingDays));
					}
					catch (NumberFormatException nfe)
					{
						remainingDays = "0.0";
					}
				}
				else
				{
					remainingDays = "0.0";
				}
				Vector vecTemp = new Vector();
				
				// Alfred A./DBSY-1082 - Depending on the level of the user, different amounts of
				// values will be returned.
				if (strAccessLevel.equals(DebisysConstants.ISO)
						&& strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					vecTemp.add(StringUtil.toString(rs.getString(1)));
				}
				else
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						vecTemp.add(StringUtil.toString(rs.getString(1)));
						vecTemp.add(StringUtil.toString(rs.getString(2)));
						vecTemp.add(StringUtil.toString(rs.getString(3)));
					}
					else
						if (strAccessLevel.equals(DebisysConstants.AGENT))
						{
							vecTemp.add(StringUtil.toString(rs.getString(1)));
							vecTemp.add(StringUtil.toString(rs.getString(2)));
						}
						else
							if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
							{
								vecTemp.add(StringUtil.toString(rs.getString(1)));
							}
				
				vecTemp.add(StringUtil.toString(rs.getString("merchant_id")));
				vecTemp.add(StringUtil.toString(rs.getString("dba")));
				vecTemp.add(StringUtil.toString(rs.getString("contact_phone")));
				vecTemp.add(remainingDays);
				if (frtCurrency)
				{
					vecTemp.add(NumberUtil.formatCurrency(StringUtil.toString(rs
							.getString("available_credit"))));
					vecTemp.add(NumberUtil.formatCurrency(StringUtil.toString(rs
							.getString("credit_limit"))));
					vecTemp.add(NumberUtil.formatCurrency(StringUtil.toString(rs
							.getString("terminal_sales"))));
					vecTemp.add(NumberUtil.formatCurrency(StringUtil.toString(rs
							.getString("avg_amount"))));
				}
				else
				{
					vecTemp.add(NumberUtil.formatAmount(StringUtil.toString(rs
							.getString("available_credit"))));
					vecTemp.add(NumberUtil.formatAmount(StringUtil.toString(rs
							.getString("credit_limit"))));
					vecTemp.add(NumberUtil.formatAmount(StringUtil.toString(rs
							.getString("terminal_sales"))));
					vecTemp.add(NumberUtil.formatAmount(StringUtil.toString(rs
							.getString("avg_amount"))));
				}
				vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("lastpurchase")));
				vecDailyLiability.add(vecTemp);
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getDailyLiability", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecDailyLiability;
	}
	
	public String downloadDailyLiability(ServletContext context, SessionData sessionData)
			throws TransactionException
	{
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		
		// Alfred A./DBSY-1082 - Adjusting the downloaded file based on user properties
		String strAccessLevel = sessionData.getProperty("access_level");
		
		try
		{
			Vector vData = null;
			vData = this.getDailyLiability(sessionData, false);
			
			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
			try
			{
				strFileName = (new TransactionSearch()).generateFileName(context);
				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix
						+ strFileName + ".csv"));
				CarrierReport.cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName
						+ ".csv");
				outputFile.flush();
				this.dailyLiabilityReportHeader(outputFile, strAccessLevel, strDistChainType,
						sessionData);
				// Totals
				double dblRemainingDaysTotal = 0;
				double dblAvailableCredits = 0;
				double dblCreditLimit = 0;
				double dblSalesSinceLastAdj = 0;
				double dblAverageSalesPerDay = 0;
				
				for (int i = 0; i < vData.size(); i++)
				{
					outputFile.write("\"" + (i + 1) + "\",");
					int loopCounter = 7;
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)
							&& strAccessLevel.equals(DebisysConstants.ISO))
					{
						loopCounter = 8;
					}
					else
						if (strAccessLevel.equals(DebisysConstants.ISO))
						{
							loopCounter = 10;
						}
						else
							if (strAccessLevel.equals(DebisysConstants.AGENT))
							{
								loopCounter = 9;
							}
							else
								if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
								{
									loopCounter = 8;
								}
					for (int j = 0; j <= loopCounter; j++)
					{
						outputFile.write("\"" + ((Vector) vData.get(i)).get(j) + "\",");
					}
					
					outputFile.write("\r\n");
					
					int startCounter = 3;
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)
							&& strAccessLevel.equals(DebisysConstants.ISO))
					{
						startCounter = 4;
					}
					else
						if (strAccessLevel.equals(DebisysConstants.ISO))
						{
							startCounter = 6;
						}
						else
							if (strAccessLevel.equals(DebisysConstants.AGENT))
							{
								startCounter = 5;
							}
							else
								if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
								{
									startCounter = 4;
								}
					dblRemainingDaysTotal += Double.parseDouble(((Vector) vData.get(i)).get(
							startCounter).toString());
					dblAvailableCredits += Double.parseDouble(((Vector) vData.get(i)).get(
							startCounter + 1).toString());
					dblCreditLimit += Double.parseDouble(((Vector) vData.get(i)).get(
							startCounter + 2).toString());
					dblSalesSinceLastAdj += Double.parseDouble(((Vector) vData.get(i)).get(
							startCounter + 3).toString());
					dblAverageSalesPerDay += Double.parseDouble(((Vector) vData.get(i)).get(
							startCounter + 4).toString());
					
				}
				// Totals
				if (strAccessLevel.equals(DebisysConstants.ISO)
						&& strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					outputFile.write("\"\",");
				}
				else
					if (strAccessLevel.equals(DebisysConstants.ISO))
					{
						outputFile.write("\"\",");
						outputFile.write("\"\",");
						outputFile.write("\"\",");
					}
					else
						if (strAccessLevel.equals(DebisysConstants.AGENT))
						{
							outputFile.write("\"\",");
							outputFile.write("\"\",");
						}
						else
							if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
							{
								outputFile.write("\"\",");
							}
				outputFile.write("\"\",");
				outputFile.write("\"\",");
				outputFile.write("\"\",");
				outputFile.write("\""
						+ Languages
								.getString("jsp.admin.reports.totals", sessionData.getLanguage())
						+ ":" + "\",");
				outputFile.write("\"" + dblRemainingDaysTotal + "\",");
				outputFile.write("\"" + dblAvailableCredits + "\",");
				outputFile.write("\"" + dblCreditLimit + "\",");
				outputFile.write("\"" + dblSalesSinceLastAdj + "\",");
				outputFile.write("\"" + dblAverageSalesPerDay + "\",");
				
				outputFile.write("\r\n");
				outputFile.flush();
				outputFile.close();
				endQuery = System.currentTimeMillis();
				CarrierReport.cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Download URL: " + downloadUrl);
				
				try
				{
					
					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
					
					// Set the compression ratio
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName
							+ ".csv");
					
					// Add ZIP entry to output stream.
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
					
					// Transfer bytes from the current file to the ZIP file
					// out.write(buffer, 0, in.read(buffer));
					
					int len;
					while ((len = in.read(buffer)) > 0)
					{
						out.write(buffer, 0, len);
					}
					
					// Close the current entry
					out.closeEntry();
					// Close the current file input stream
					in.close();
					out.close();
				}
				catch (IllegalArgumentException iae)
				{
					iae.printStackTrace();
				}
				catch (FileNotFoundException fnfe)
				{
					fnfe.printStackTrace();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
				}
				sourceFile.delete();
				endQuery = System.currentTimeMillis();
				CarrierReport.cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
				
			}
			catch (IOException ioe)
			{
				try
				{
					if (outputFile != null)
					{
						outputFile.close();
					}
				}
				catch (IOException ioe2)
				{
				}
			}
			
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during downloadDailyLiability", e);
			throw new TransactionException();
		}
		long end = System.currentTimeMillis();
		
		// Display the elapsed time to the standard output
		CarrierReport.cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
		
		return downloadUrl;
	}// End of function downloadDailyLiability
	
	/**
	 * @param outputFile
	 * @throws IOException
	 */
	private void dailyLiabilityReportHeader(BufferedWriter outputFile, String strAccessLevel,
			String strDistChainType, SessionData sessionData) throws IOException
	{
		outputFile.write("\"#\",");
		if (strAccessLevel.equals(DebisysConstants.ISO)
				&& strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
		{
			outputFile.write("\"" + Languages.getString("jsp.admin.rep", sessionData.getLanguage())
					+ "\",");
		}
		else
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				outputFile
						.write("\""
								+ Languages.getString("jsp.admin.agent", sessionData.getLanguage())
								+ "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.subagent", sessionData.getLanguage())
						+ "\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.rep", sessionData.getLanguage()) + "\",");
			}
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					outputFile.write("\""
							+ Languages.getString("jsp.admin.subagent", sessionData.getLanguage())
							+ "\",");
					outputFile.write("\""
							+ Languages.getString("jsp.admin.rep", sessionData.getLanguage())
							+ "\",");
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						outputFile.write("\""
								+ Languages.getString("jsp.admin.rep", sessionData.getLanguage())
								+ "\",");
					}
		outputFile.write("\""
				+ Languages.getString("jsp.admin.reports.merchant_id", sessionData.getLanguage())
				+ "\",");
		outputFile.write("\""
				+ Languages.getString("jsp.admin.reports.merchant_name", sessionData.getLanguage())
				+ "\",");
		outputFile
				.write("\""
						+ Languages
								.getString(
										"jsp.admin.reports.transactions.daily_liability_report.contact_phone_number",
										sessionData.getLanguage()) + "\",");
		outputFile.write("\""
				+ Languages.getString(
						"jsp.admin.reports.transactions.daily_liability_report.remaining_days",
						sessionData.getLanguage()) + "\",");
		outputFile.write("\""
				+ Languages.getString(
						"jsp.admin.reports.transactions.daily_liability_report.available_credit",
						sessionData.getLanguage()) + "\",");
		outputFile.write("\""
				+ Languages.getString(
						"jsp.admin.reports.transactions.daily_liability_report.credit_limit",
						sessionData.getLanguage()) + "\",");
		outputFile
				.write("\""
						+ Languages
								.getString(
										"jsp.admin.reports.transactions.daily_liability_report.sales_since_last_adj",
										sessionData.getLanguage()).replaceAll("<BR>", "") + "\",");
		outputFile.write("\""
				+ Languages.getString(
						"jsp.admin.reports.transactions.daily_liability_report.avg_sales_per_day",
						sessionData.getLanguage()).replaceAll("<BR>", "") + "\",");
		outputFile.write("\r\n");
	}
	
	public String downloadPhoneDetails(ServletContext context, SessionData sessionData)
			throws TransactionException, ReportException
	{
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		
		boolean bool_deployTypeIntl = DebisysConfigListener.getDeploymentType(context).equals(
				DebisysConstants.DEPLOYMENT_INTERNATIONAL);
		
		boolean bool_customerConfigDefault = DebisysConfigListener.getCustomConfigType(context)
				.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
		
		boolean bool_permInvoiceNumEnabled = com.debisys.users.User.isInvoiceNumberEnabled(
				sessionData, context);
		
		Vector vData = this.getPhoneDetail(sessionData, context);
		
		if (vData != null)
		{
			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
			
			try
			{
				strFileName = (new TransactionSearch()).generateFileName(context);
				File file = new File(workingDir);
				if (!file.exists())
				{
					file.mkdir();
				}
				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix
						+ strFileName + ".csv"));
				CarrierReport.cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName
						+ ".csv");
				String sPhysState = "";
				String sAuthorizationNo = "";
				
				// column headers
				outputFile.write("\"#\",");
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.dba", sessionData.getLanguage())
						+ "\","); // dba
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.date", sessionData.getLanguage())
						+ "\","); // date
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.recharge", sessionData
								.getLanguage()) + "\","); // recharge
				
				if (bool_deployTypeIntl && bool_customerConfigDefault)
				{
					outputFile.write("\""
							+ Languages.getString("jsp.admin.reports.bonus", sessionData
									.getLanguage()) + "\","); // bonus
					outputFile.write("\""
							+ Languages.getString("jsp.admin.reports.total_recharge", sessionData
									.getLanguage()) + "\","); // total recharge
				}
				outputFile.write("\""
						+ Languages.getString("jsp.admin.reports.clerk_id", sessionData
								.getLanguage()) + "\","); // clerk id
				
				if (bool_permInvoiceNumEnabled && bool_deployTypeIntl && bool_customerConfigDefault)
				{
					outputFile.write("\""
							+ Languages.getString("jsp.admin.reports.invoiceno", sessionData
									.getLanguage()) + "\","); // invoice num
				}
				outputFile.write("\r\n");
				
				Iterator it = vData.iterator();
				int intCounter = 1;
				
				while (it.hasNext())
				{
					Vector vecTemp = null;
					vecTemp = (Vector) it.next();
					
					outputFile.write("\"" + intCounter++ + "\",");
					outputFile.write("\"" + vecTemp.get(0) + "\","); // dba
					outputFile.write("\"" + vecTemp.get(1) + "\","); // date
					outputFile.write("\"" + vecTemp.get(2) + "\","); // recharge
					
					if (bool_deployTypeIntl && bool_customerConfigDefault)
					{
						outputFile.write("\"" + vecTemp.get(5) + "\","); // bonus
						outputFile.write("\"" + vecTemp.get(6) + "\","); // total recharge
					}
					outputFile.write("\"" + vecTemp.get(3) + "\","); // clerk id
					
					if (bool_permInvoiceNumEnabled && bool_deployTypeIntl
							&& bool_customerConfigDefault)
					{
						outputFile.write("\"" + vecTemp.get(4) + "\","); // invoice num
					}
					outputFile.write("\r\n");
				}
				
				outputFile.write("\r\n");
				outputFile.flush();
				outputFile.close();
				endQuery = System.currentTimeMillis();
				CarrierReport.cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Download URL: " + downloadUrl);
				
				try
				{
					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
					
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName
							+ ".csv");
					
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
					
					int len;
					while ((len = in.read(buffer)) > 0)
					{
						out.write(buffer, 0, len);
					}
					
					out.closeEntry();
					in.close();
					out.close();
				}
				catch (IllegalArgumentException iae)
				{
					iae.printStackTrace();
				}
				catch (FileNotFoundException fnfe)
				{
					fnfe.printStackTrace();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
				}
				sourceFile.delete();
				endQuery = System.currentTimeMillis();
				CarrierReport.cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
			}
			catch (IOException ioe)
			{
				CarrierReport.cat.error("Error during downloadPhoneDetails");
			}
		}
		
		long end = System.currentTimeMillis();
		
		CarrierReport.cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
		
		return downloadUrl;
	}// End of function downloadPhoneDetails
	
	public Vector getPhoneDetail(SessionData sessionData, ServletContext application)
			throws ReportException
	{
		Connection dbConn = null;
		Vector vecPhoneDetail = new Vector();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		String strSQLDetail = "";
                String viewReferenceCardJOIN = "";
		String viewReferenceCardFIELDS = "";
		//this flag means if the user is in International Default
		boolean isInternationalDefault = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
		   									DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
		   
		boolean viewReferenceCard = false;
		if ( isInternationalDefault ){
		   	viewReferenceCard = sessionData.checkPermission(DebisysConstants.PERM_ENABLE_VIEW_REFERENCES_CARD_IN_REPORTS);
		   	viewReferenceCardJOIN = " LEFT OUTER JOIN hostTransactionsrecord htr with(nolock) on htr.transaction_id = wT.rec_id ";
		   	viewReferenceCardFIELDS = " ,htr.info2, htr.info1 ";
		}
		PreparedStatement pstmt1 = null;
		
		try
		{
			dbConn = Torque.getConnection(CarrierReport.sql_bundle.getString("pkgAlternateDb"));
			
			if (dbConn == null)
			{
				CarrierReport.cat.error("Can't get database connection");
				throw new ReportException();
			}
			
			Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId,
					this.start_date, DateUtil.addSubtractDays(this.end_date, 1), false);
			
			if (strAccessLevel.equals(DebisysConstants.ISO))
			{
				if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
				{
					strSQLDetail = CarrierReport.sql_bundle.getString("getPhoneDetail3");
					String carrierUserProds = sessionData.getProperty("carrierUserProds");
					if (carrierUserProds != null)
					{
						strSQLDetail += " AND wt.id in (" + carrierUserProds + ") ";
					}
					else
						// no products attached to carrier user
						return null;
					strSQLDetail = strSQLDetail.replaceAll("_ACCESSLEVEL_", strAccessLevel);
					strSQLDetail = strSQLDetail.replaceAll("_REFID_", strRefId);
                                        if ( viewReferenceCard )
					{
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_FIELDS", viewReferenceCardFIELDS );
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_JOIN", viewReferenceCardJOIN );
					}	
					else
					{
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_FIELDS", "");
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_JOIN", "");				
					}
					pstmt1 = dbConn.prepareStatement(strSQLDetail);
					pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt1.setDouble(2, Double.parseDouble(strRefId));
					pstmt1.setString(3, vTimeZoneFilterDates.get(0).toString());
					pstmt1.setString(4, vTimeZoneFilterDates.get(1).toString());
					pstmt1.setString(5, this.phone_number);
				}
				else
					if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
					{
						strSQLDetail = CarrierReport.sql_bundle.getString("getPhoneDetail5");
						String carrierUserProds = sessionData.getProperty("carrierUserProds");
						if (carrierUserProds != null)
						{
							strSQLDetail += " AND wt.id in (" + carrierUserProds + ") ";
						}
						else
							// no products attached to carrier user
							return null;
						strSQLDetail = strSQLDetail.replaceAll("_ACCESSLEVEL_", strAccessLevel);
						strSQLDetail = strSQLDetail.replaceAll("_REFID_", strRefId);
                                                if ( viewReferenceCard )
					{
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_FIELDS", viewReferenceCardFIELDS );
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_JOIN", viewReferenceCardJOIN );
					}	
					else
					{
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_FIELDS", "");
						strSQLDetail = strSQLDetail.replaceFirst("VIEW_REFERENCE_CARD_JOIN", "");				
					}
						pstmt1 = dbConn.prepareStatement(strSQLDetail);
						pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt1.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
						pstmt1.setInt(3, Integer.parseInt(DebisysConstants.REP_TYPE_AGENT));
						pstmt1.setDouble(4, Double.parseDouble(strRefId));
						pstmt1.setString(5, vTimeZoneFilterDates.get(0).toString());
						pstmt1.setString(6, vTimeZoneFilterDates.get(1).toString());
						pstmt1.setString(7, this.phone_number);
					}
			}
			// DBSY-569 SW
			else
				if (strAccessLevel.equals(DebisysConstants.AGENT))
				{
					
					strSQLDetail = CarrierReport.sql_bundle.getString("getPhoneDetail8");
					String carrierUserProds = sessionData.getProperty("carrierUserProds");
					if (carrierUserProds != null)
					{
						strSQLDetail += " AND wt.id in (" + carrierUserProds + ") ";
					}
					else
						// no products attached to carrier user
						return null;
					strSQLDetail = strSQLDetail.replaceAll("_ACCESSLEVEL_", strAccessLevel);
					strSQLDetail = strSQLDetail.replaceAll("_REFID_", strRefId);
					pstmt1 = dbConn.prepareStatement(strSQLDetail);
					pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
					pstmt1.setInt(2, Integer.parseInt(DebisysConstants.REP_TYPE_SUBAGENT));
					pstmt1.setDouble(3, Double.parseDouble(strRefId));
					pstmt1.setString(4, vTimeZoneFilterDates.get(0).toString());
					pstmt1.setString(5, vTimeZoneFilterDates.get(1).toString());
					pstmt1.setString(6, this.phone_number);
				}
				else
					if (strAccessLevel.equals(DebisysConstants.SUBAGENT))
					{
						
						strSQLDetail = CarrierReport.sql_bundle.getString("getPhoneDetail9");
						String carrierUserProds = sessionData.getProperty("carrierUserProds");
						if (carrierUserProds != null)
						{
							strSQLDetail += " AND wt.id in (" + carrierUserProds + ") ";
						}
						else
							// no products attached to carrier user
							return null;
						strSQLDetail = strSQLDetail.replaceAll("_ACCESSLEVEL_", strAccessLevel);
						strSQLDetail = strSQLDetail.replaceAll("_REFID_", strRefId);
						pstmt1 = dbConn.prepareStatement(strSQLDetail);
						pstmt1.setInt(1, Integer.parseInt(DebisysConstants.REP_TYPE_REP));
						pstmt1.setDouble(2, Double.parseDouble(strRefId));
						pstmt1.setString(3, vTimeZoneFilterDates.get(0).toString());
						pstmt1.setString(4, vTimeZoneFilterDates.get(1).toString());
						pstmt1.setString(5, this.phone_number);
					}
					// END DBSY-569
					else
						if (strAccessLevel.equals(DebisysConstants.REP))
						{
							
							strSQLDetail = CarrierReport.sql_bundle.getString("getPhoneDetail6");
							String carrierUserProds = sessionData.getProperty("carrierUserProds");
							if (carrierUserProds != null)
							{
								strSQLDetail += " AND wt.id in (" + carrierUserProds + ") ";
							}
							else
								// no products attached to carrier user
								return null;
							strSQLDetail = strSQLDetail.replaceAll("_ACCESSLEVEL_", strAccessLevel);
							strSQLDetail = strSQLDetail.replaceAll("_REFID_", strRefId);
							pstmt1 = dbConn.prepareStatement(strSQLDetail);
							pstmt1.setDouble(1, Double.parseDouble(strRefId));
							pstmt1.setString(2, vTimeZoneFilterDates.get(0).toString());
							pstmt1.setString(3, vTimeZoneFilterDates.get(1).toString());
							pstmt1.setString(4, this.phone_number);
						}
						else
							if (strAccessLevel.equals(DebisysConstants.MERCHANT))
							{
								strSQLDetail = CarrierReport.sql_bundle
										.getString("getPhoneDetail7");
								String carrierUserProds = sessionData
										.getProperty("carrierUserProds");
								if (carrierUserProds != null)
								{
									strSQLDetail += " AND wt.id in (" + carrierUserProds + ") ";
								}
								else
									// no products attached to carrier user
									return null;
								strSQLDetail = strSQLDetail.replaceAll("_ACCESSLEVEL_",
										strAccessLevel);
								strSQLDetail = strSQLDetail.replaceAll("_REFID_", strRefId);
								pstmt1 = dbConn.prepareStatement(strSQLDetail);
								pstmt1.setDouble(1, Double.parseDouble(strRefId));
								pstmt1.setString(2, vTimeZoneFilterDates.get(0).toString());
								pstmt1.setString(3, vTimeZoneFilterDates.get(1).toString());
								pstmt1.setString(4, this.phone_number);
							}
			
			cat.debug(strSQLDetail);
			ResultSet rs = pstmt1.executeQuery();
			boolean bool_dtaxUsed = false;
			Hashtable<Integer, String> ht_taxTypes = TaxTypes.getTaxTypes();
			
			while (rs.next())
			{
				Vector vecTemp = new Vector();
				vecTemp.add(StringUtil.toString(rs.getString("dba")));
				vecTemp.add(DateUtil.formatDateTime(rs.getTimestamp("date")));
				vecTemp.add(NumberUtil.formatCurrency(rs.getString("amount")));
				vecTemp.add(StringUtil.toString(rs.getString("clerk")));
				vecTemp.add(StringUtil.toString(rs.getString("invoice_no")));
				
				if (DebisysConfigListener.getDeploymentType(application).equals(
						DebisysConstants.DEPLOYMENT_INTERNATIONAL)
						&& DebisysConfigListener.getCustomConfigType(application).equals(
								DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
				{
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("bonus_amount")));
					vecTemp.add(NumberUtil.formatCurrency(rs.getString("total_recharge")));
				}
				vecPhoneDetail.add(vecTemp);
			}
			
			rs.close();
			pstmt1.close();
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during getPhoneDetail", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				CarrierReport.cat.error("Error during closeConnection", e);
			}
		}
		return vecPhoneDetail;
	}
	
	
	
	/**
	 * @param sessionData
	 * @return
	 * @throws ReportException
	 */
	public SummaryReport getCarrierSummaryReport(SessionData sessionData, ServletContext application, int scheduleReportType, ArrayList<String> titles) throws ReportException
	{
		Connection dbConn = null;
		SummaryReport summaryReport = new SummaryReport();
		String strAccessLevel = sessionData.getProperty("access_level");
		String strDistChainType = sessionData.getProperty("dist_chain_type");
		String strRefId = sessionData.getProperty("ref_id");
		String instance = DebisysConfigListener.getInstance(application);
		String languageFlag = sessionData.getUser().getLanguageCode();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String strSQL = "";
		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
				throw new ReportException();
			}
			strSQL = " EXEC CarrierSummaryReport ?,?,?,?,?,?,? ";
                        Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates(strAccessLevel, strRefId, this.start_date, this.end_date + " 23:59:59.998", false);
			String fixedQuery = strSQL.replaceFirst("\\?", strRefId)
									   .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(0).toString() + "'")
									   .replaceFirst("\\?", "'" + vTimeZoneFilterDates.get(1).toString() + "'")
									   .replaceFirst("\\?", strAccessLevel )
									   .replaceFirst("\\?", strDistChainType )
									   .replaceFirst("\\?", "'"+instance+"'" )
									   .replaceFirst("\\?", languageFlag );
                        
			if ( scheduleReportType == DebisysConstants.SCHEDULE_REPORT )
			{
				StringBuilder execStoreRelat = new StringBuilder();
				StringBuilder execStoreFixed = new StringBuilder();
				StringBuilder execStore = new StringBuilder();
				
				execStore.append("DECLARE @startDateRelative DATETIME ");
				execStore.append("DECLARE @endDateRelative DATETIME ");
				
				execStoreRelat.append(execStore);
				execStoreRelat.append("SELECT @startDateRelative ="+ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_START_DATE+" , @endDateRelative = "+ ScheduleReport.WILDCARD_STORE_PROCEDURE_TO_END_DATE+" ");
				
				execStoreFixed.append(execStore);
				execStoreFixed.append("SELECT @startDateRelative = '"+vTimeZoneFilterDates.get(0).toString()+"' , @endDateRelative = '"+ vTimeZoneFilterDates.get(1).toString() +"' ");
				
				
				
				
				String relativeQuery = strSQL.replaceFirst("\\?", strRefId)
										.replaceFirst("\\?", "@startDateRelative" )
										.replaceFirst("\\?", "@endDateRelative" )
									    .replaceFirst("\\?", strAccessLevel )
									    .replaceFirst("\\?", strDistChainType )
									    .replaceFirst("\\?", "'"+instance+"'" )
									    .replaceFirst("\\?", languageFlag );
	
								
				execStoreRelat.append(relativeQuery);
				execStoreFixed.append(fixedQuery);
												
				ScheduleReport scheduleReport = new ScheduleReport( DebisysConstants.SC_CARRIER_SUMMARY_REPORT , 1);
															
				scheduleReport.setRelativeQuery( execStoreRelat.toString() );
				
				scheduleReport.setFixedQuery( execStoreFixed.toString() );		
				ArrayList<ColumnReport> headers = RepReports.getHeadersSummaryReportByPeriods();
				TransactionReportScheduleHelper.addMetaDataReport( headers, scheduleReport, titles );				
				sessionData.setPropertyObj( DebisysConstants.SC_SESS_VAR_NAME , scheduleReport);								
				return null;
				
			}
			cat.info("SQL "+fixedQuery);
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setDouble(1, Double.parseDouble(strRefId));
			pstmt.setString(2, vTimeZoneFilterDates.get(0).toString());
			pstmt.setString(3, vTimeZoneFilterDates.get(1).toString());
			pstmt.setString(4, strAccessLevel);
			pstmt.setString(5, strDistChainType);
			pstmt.setString(6, instance);
			pstmt.setString(7, languageFlag);		
			
			rs = pstmt.executeQuery();
			
			while( rs.next() )
			{				
				String description = rs.getString(1);
				String current     = rs.getString(2);
				String previous    = rs.getString(3);
				String change      = rs.getString(4);
				String recordType  = rs.getString(5);
				
				SummaryReportPojo summReportPojo = new SummaryReportPojo(description, current, previous, change, recordType );
				summaryReport.getRecords().add( summReportPojo );
			}
			rs.close();
			pstmt.close();
			
			if ( scheduleReportType == DebisysConstants.DOWNLOAD_REPORT )
			{
				this.setStrUrlLocation( downloadCarrierSummaryReport(application, sessionData, summaryReport, titles ) );
			}
			
		}
		catch (Exception e)
		{
			cat.error("Error during getCarrierSummary", e);
			throw new ReportException();
		}
		finally
		{
			try
			{
				TorqueHelper.closeConnection(dbConn, pstmt, rs);				
			}
			catch (Exception e)
			{
				cat.error("Error during closeConnection", e);
			}
		}
		return summaryReport;
	}
	
	
	
	public String downloadCarrierSummaryReport(ServletContext context, SessionData sessionData, SummaryReport summaryReport, ArrayList<String> titles)
			throws TransactionException
	{
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		String deploymentType = DebisysConfigListener.getDeploymentType(context);
		String customConfigType = DebisysConfigListener.getCustomConfigType(context);
		
		try
		{			
			
			String tempStartDate = this.start_date;
			String tempEndDate = this.end_date;
			
			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
			
			try
			{
				strFileName = (new TransactionSearch()).generateFileName(context);
				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix	+ strFileName + ".csv"));
				CarrierReport.cat.debug("Temp File Name: " + workingDir + filePrefix + strFileName+ ".csv");
				outputFile.flush();
								
				/*
				for(String additionalTitle : titles)
				{
					outputFile.write( StringUtil.toString( additionalTitle ) + "\",");
				}
				outputFile.write( "\r\n" );
				*/					
				for( SummaryReportPojo summPojo : summaryReport.getRecords())
				{
					String description = summPojo.getDescription();
					String current 	   = summPojo.getCurrentPeriod();
					String previous    = summPojo.getPreviuosPeriod();
					String change 	   = summPojo.getChange();
					String typeRecord  = summPojo.getRecordType();
					
					if ( typeRecord.equals("R") || typeRecord.equals("C") )
					{
						outputFile.write( StringUtil.toString( description ) + ",");
						outputFile.write( "\""+StringUtil.toString( current ) + "\"" + ",");
						outputFile.write( "\""+StringUtil.toString( previous ) + "\"" + ",");
						outputFile.write(  StringUtil.toString( change ) + ",");						
					}
					if ( typeRecord.equals("T"))
					{
						outputFile.write( StringUtil.toString( description ) + ",");												
					}					
					outputFile.write( "\r\n" );					
				}
				
				
				outputFile.flush();
				outputFile.close();
				endQuery = System.currentTimeMillis();
				
				CarrierReport.cat.debug("File Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				CarrierReport.cat.debug("Download URL: " + downloadUrl);
				
				try
				{
					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
					
					// Set the compression ratio
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName
							+ ".csv");
					
					// Add ZIP entry to output stream.
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
					
					// Transfer bytes from the current file to the ZIP file
					// out.write(buffer, 0, in.read(buffer));
					int len;
					while ((len = in.read(buffer)) > 0)
					{
						out.write(buffer, 0, len);
					}
					
					// Close the current entry
					out.closeEntry();
					// Close the current file input stream
					in.close();
					out.close();
				}
				catch (IllegalArgumentException iae)
				{
					iae.printStackTrace();
				}
				catch (FileNotFoundException fnfe)
				{
					fnfe.printStackTrace();
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
				}
				
				sourceFile.delete();
				endQuery = System.currentTimeMillis();
				CarrierReport.cat.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0))
						+ " seconds");
			}
			catch (IOException ioe)
			{
				CarrierReport.cat.error("Error on I/O operation: " + ioe.getClass().getName()
						+ " (" + ioe.getMessage() + ") ", ioe);
				try
				{
					if (outputFile != null)
					{
						outputFile.close();
					}
				}
				catch (IOException ioe2)
				{
				}
			}
		}
		catch (Exception e)
		{
			CarrierReport.cat.error("Error during downloadCarrierUserSummary", e);
			throw new TransactionException();
		}
		long end = System.currentTimeMillis();
		
		// Display the elapsed time to the standard output
		CarrierReport.cat.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
		
		return downloadUrl;
	}

	/**
	 * @return the strUrlLocation
	 */
	public String getStrUrlLocation() {
		return strUrlLocation;
	}

	/**
	 * @param strUrlLocation the strUrlLocation to set
	 */
	public void setStrUrlLocation(String strUrlLocation) {
		this.strUrlLocation = strUrlLocation;
	}
	
	
}
