package com.debisys.reports;

import com.debisys.dateformat.DateFormat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Category;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.exceptions.TransactionException;
import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.DateUtil;
import com.debisys.utils.DebisysConstants;
import com.debisys.utils.NumberUtil;
import com.debisys.utils.StringUtil;
import com.emida.utils.dbUtils.TorqueHelper;

/**
 * Holds the information for transactions.<P>
 *
 * @author Jay Chi
 */

public class BillingReport implements HttpSessionBindingListener
{

  private String start_date = "";
  private String end_date = "";
  private String currencyid = "";
  private String conversionr = "";
  private Hashtable validationErrors = new Hashtable();
  private String sort = "";
  private String col = "";
  private String merchant_ids = "";

  //log4j logging
  static Category cat = Category.getInstance(BillingReport.class);
  private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");
 
  /***
   * ADDED FOR LOCALIZATION - SW
   * @return
   */
  public String getStartDateFormatted()
  {
      return getDateFormatted(this.start_date);
  }
  
  /***
   * ADDED FOR LOCALIZATION - SW
   * @return
   */
  public String getEndDateFormatted()
  {
      return getDateFormatted(this.end_date);
  }
  
  /***
   * ADDED FOR LOCALIZATION - SW
   * @param dateToFormat
   * @return
   */
  private String getDateFormatted(String dateToFormat)
  {
      if(DateUtil.isValid(dateToFormat))
      {
            String date = null;
            try
            {
                // Current support site date is in MM/dd/YYYY.
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                java.util.Date dd = sdf.parse(dateToFormat);
                
                // We will specify a new date format through the database
                SimpleDateFormat sdf1 = new SimpleDateFormat(DateFormat.getDateFormat());
                date = sdf1.format(dd); 
                
                // Since Java SimpleDateFormat does not know whether a datetime contains
                // a time or not, remove the time (usually 12:00:00 AM) from the end of the date ranges
                date = date.replaceFirst("\\s\\d{1,2}:\\d{2}(:\\d{2})*(\\s[AaPp][Mm])*", "");
            } 
            catch (ParseException e)
            {
                cat.error("Error during TransactionReport date localization ", e);
            }      
            return date;
      }
      else
      {
          //Something isn't valid in the date string - don't try to localize it
          return StringUtil.toString(this.start_date);
      }
  }  
  
  public String getStartDate()
  {
    return StringUtil.toString(this.start_date);
  }

  public void setStartDate(String strValue)
  {
	  this.start_date = strValue;
  }

  public String getEndDate()
  {
    return StringUtil.toString(this.end_date);
  }

  public void setEndDate(String strValue)
  {
	  this.end_date = strValue;
  }

  public String getMerchantIds()
  {
    return StringUtil.toString(this.merchant_ids);
  }

  public void setMerchantIds(String strValue)
  {
	  this.merchant_ids = strValue;
  }

  public String getConversionR()
  {
    return StringUtil.toString(this.conversionr);
  }

  public void setConversionR(String strValue)
  {
	  this.conversionr = strValue;
  }
  
  public String getCurrencyID()
  {
    return StringUtil.toString(this.currencyid);
  }

  public void setCurrencyID(String strValue)
  {
	  this.currencyid = strValue;
  }
  
  public void setMerchantIds(String[] strValue)
  {
    String strTemp = "";
    if (strValue != null && strValue.length > 0)
    {
      boolean isFirst = true;
      for (int i = 0; i < strValue.length; i++)
      {
        if (strValue[i] != null && !strValue[i].equals(""))
        {
          if (!isFirst)
          {
            strTemp = strTemp + "," + strValue[i];
          }
          else
          {
            strTemp = strValue[i];
            isFirst = false;
          }
        }
      }
    }

    this.merchant_ids = strTemp;
  }
  public String getCol()
  {
    return StringUtil.toString(this.col);
  }

  public void setCol(String strCol)
  {
	  this.col = strCol;
  }

  public String getSort()
  {
    return StringUtil.toString(this.sort);
  }

  public void setSort(String strSort)
  {
	  this.sort = strSort;
  }
  /**
   * Returns a hash of errors.
   *
   * @return Hash of errors.
   */

  public Hashtable getErrors()
  {
    return this.validationErrors;
  }

  /**
   * Returns a validation error against a specific field.  If a field
   * was found to have an error during
   * {@link #validateDateRange validateDateRange},  the error message
   * can be accessed via this method.
   *
   * @param fieldname The bean property name of the field
   * @return The error message for the field or null if none is
   *         present.
   */

  public String getFieldError(String fieldname)
  {
    return ((String) this.validationErrors.get(fieldname));
  }


  /**
   * Sets the validation error against a specific field.  Used by
   * {@link #validateDateRange validateDateRange}.
   *
   * @param fieldname The bean property name of the field
   * @param error     The error message for the field or null if none is
   *                  present.
   */

  public void addFieldError(String fieldname, String error)
  {
	  this.validationErrors.put(fieldname, error);
  }


  /**
   * Validates for missing or invalid fields.
   *
   * @return <code>true</code> if the entity passes the validation
   *         checks,  otherwise  <code>false</code>
   */

  public boolean validateDateRange(SessionData sessionData)
  {
	  this.validationErrors.clear();
    boolean valid = true;

    if ((this.start_date == null) ||
        (this.start_date.length() == 0))
    {
      addFieldError("startdate", Languages.getString("com.debisys.reports.error1", sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.start_date))
    {
      addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
      valid = false;
    }

    if ((this.end_date == null) ||
        (this.end_date.length() == 0))
    {
      addFieldError("enddate", Languages.getString("com.debisys.reports.error3", sessionData.getLanguage()));
      valid = false;
    }
    else if (!DateUtil.isValid(this.end_date))
    {
      addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
      valid = false;
    }

    if (valid)
    {

      Date dtStartDate = new Date();
      Date dtEndDate = new Date();
      SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
      formatter.setLenient(false);
      try
      {
        dtStartDate = formatter.parse(this.start_date);
      }
      catch (ParseException ex)
      {
        addFieldError("startdate", Languages.getString("com.debisys.reports.error2", sessionData.getLanguage()));
        valid = false;
      }
      try
      {
        dtEndDate = formatter.parse(this.end_date);
      }
      catch (ParseException ex)
      {
        addFieldError("enddate", Languages.getString("com.debisys.reports.error4", sessionData.getLanguage()));
        valid = false;
      }
      if (dtStartDate.after(dtEndDate))
      {
        addFieldError("date", "Start date must be before end date.");
        valid = false;
      }

    }

    return valid;
  }

public Vector getBillingSummary(SessionData sessionData) 
throws ReportException
{
	Vector vecBillingSummary = new Vector();
    Connection dbConn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    String strSQLSummary = sql_bundle.getString("getBillingSummary");
    String strAccessLevel = sessionData.getProperty("access_level");
    String strRefId = sessionData.getProperty("ref_id");
    this.currencyid = sessionData.getProperty("currencyid");
    this.conversionr = sessionData.getProperty("conversionr");

    try
    {
      dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
      if (dbConn == null)
      {
        cat.error("Can't get database connection");
        throw new ReportException();
      }
      //Report available just for ISOs
      if (strAccessLevel.equals(DebisysConstants.ISO))
      {
          pstmt = dbConn.prepareCall(strSQLSummary);
          pstmt.setString(1, start_date);
          pstmt.setString(2, end_date);
          pstmt.setLong(3, Long.parseLong(strRefId));
          if (currencyid != "")
        	  pstmt.setInt(4, Integer.parseInt(currencyid));
          else
        	  pstmt.setInt(4, 0);
          if (conversionr != "")
        	  pstmt.setFloat(5, Float.parseFloat(conversionr));
          else
        	  pstmt.setFloat(5, 1);

          rs = pstmt.executeQuery();
          while (rs.next())
          {
            Vector vecTemp = new Vector();
            vecTemp.add(rs.getString("Trans_Month_Year"));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Trans_Amount")));
            vecTemp.add(Integer.parseInt(rs.getString("Trans_Quantity")));
            vecTemp.add(Integer.parseInt(rs.getString("Number_Days")));
            vecTemp.add(Integer.parseInt(rs.getString("Live_POS")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Emida_Rev")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Avg_Amount_Trans")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Avg_Trans_Term_Day")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Total_Avg_day")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Avg_Amount_Day_Term")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Avg_Amount_Mo_Term")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("Num_Recharge_Mo_Sub")));
            vecTemp.add(NumberUtil.formatAmount(rs.getString("ARPU")));
            vecBillingSummary.add(vecTemp);
          }

          cat.debug(strSQLSummary);
          pstmt.close();
          rs.close();
      }
   
    }
    catch (Exception e)
    {
      cat.error("Error during getBillingSummary", e);
      throw new ReportException();
    }
    finally
    {
      try
      {
        TorqueHelper.closeConnection(dbConn, pstmt, rs);
      }
      catch (Exception e)
      {
        cat.error("Error during closeConnection", e);
      }
    }
    
	return vecBillingSummary;
	
}

  public void valueBound(HttpSessionBindingEvent event)
   {
   }

   public void valueUnbound(HttpSessionBindingEvent event)
   {
   }

   public static void main(String[] args)
   {
   }


}
