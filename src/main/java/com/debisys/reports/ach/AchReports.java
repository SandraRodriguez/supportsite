package com.debisys.reports.ach;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.apache.torque.Torque;

import com.debisys.customers.Rep;
import com.debisys.users.SessionData;
import com.debisys.utils.DbUtil;

/**
 * @author nmartinez
 *
 */
public class AchReports {
	
	private static Logger cat = Logger.getLogger(Rep.class);
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.ach.sql");
		
	private int recordCount;
	
	public int recordCountLastQuery(){
		return recordCount;
	}
	
	public ArrayList<FmrCommissionPaymentVo> FRMCommisionPayoutSummary(SessionData sessionData, String startDate, String endDate,
																			  int nRecordsPerPage, int nPageNumber)
	{
		ArrayList<FmrCommissionPaymentVo> fmrCommissionPaymentVoReport = new ArrayList<FmrCommissionPaymentVo>();
		String userInSession = sessionData.getUser().getRefId();
		String strSQL = sql_bundle.getString("FRMCommisionPayoutSummary");
		Connection dbConn = null;
		PreparedStatement pstmt = null;		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
			}
			
			cat.info("SQL=["+strSQL.replaceFirst("\\?", "'"+startDate+"'").replaceFirst("\\?", "'"+endDate+"'").replaceFirst("\\?", userInSession )+"]");
			
			pstmt = dbConn.prepareStatement(strSQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			pstmt.setString(1, startDate);
			pstmt.setString(2, endDate);
			pstmt.setString(3, userInSession);
			ResultSet rs = pstmt.executeQuery();
			recordCount = DbUtil.getRecordCount(rs);	
			int countVo = 1;
			if (recordCount > 0)
			{
				rs.absolute(DbUtil.getRowNumber(rs, nRecordsPerPage, recordCount, nPageNumber) - 1);
				for (int i = 0; i < nRecordsPerPage && rs.next(); i++)
				{
					String dateTime = rs.getString(1);				
					String dateStart = rs.getString(3);
					String dateEnd = rs.getString(4);
					String description = rs.getString(2)+" ("+dateStart+" to "+dateEnd+")";
					String noTrxs = rs.getString(5);
					String commisionAmount = rs.getString(6);
					
					String billBatchId = rs.getString(7);
					String entityId = rs.getString(8);
					String statementId = rs.getString(9);
					
					FmrCommissionPaymentVo fmrCommissionPaymentVo = new FmrCommissionPaymentVo(dateTime,description,noTrxs,commisionAmount,billBatchId,entityId,statementId,"");
					fmrCommissionPaymentVo.setId(String.valueOf(countVo));
					fmrCommissionPaymentVo.setPaymentTypeId(rs.getString(10));
					fmrCommissionPaymentVoReport.add(fmrCommissionPaymentVo);
					countVo++;
				}
			}			
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("FRMCommisionPayoutSummary  => ", e);

		}
		finally
		{
			try
			{
				if (pstmt != null)
				{
					pstmt.close();
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("FRMCommisionPayoutSummary closeConnection ", e);
			}
		}			
	    return fmrCommissionPaymentVoReport;
	}	

	
	public ArrayList<FmrCommissionPaymentVo> FRMCommisionPayoutDetail(String entityId, String billingBatchId, String statementTypeID, String paymentTypeId)
	{
		ArrayList<FmrCommissionPaymentVo> fmrCommissionPaymentVoReport = new ArrayList<FmrCommissionPaymentVo>();
		String strSQL = sql_bundle.getString("FRMCommisionPayoutDetail");
		Connection dbConn = null;
		PreparedStatement pstmt = null;		
		try
		{
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null)
			{
				cat.error("Can't get database connection");
			}
			cat.info("SQL=["+strSQL.replaceFirst("\\?", entityId).replaceFirst("\\?", billingBatchId).replaceFirst("\\?", statementTypeID )+"]");
			
			pstmt = dbConn.prepareStatement(strSQL);
			pstmt.setString(1, entityId);
			pstmt.setString(2, billingBatchId);
			pstmt.setString(3, statementTypeID);
			pstmt.setString(4, paymentTypeId);
			
			ResultSet rs = pstmt.executeQuery();

			int countVo = 1;
			while (rs.next())
			{
				String transactionType = rs.getString(1);				
				String noTrxs = rs.getString(2);
				String commisionAmount = rs.getString(3);
				
				FmrCommissionPaymentVo fmrCommissionPaymentVo = new FmrCommissionPaymentVo("","",noTrxs,commisionAmount,"","","",transactionType);
				fmrCommissionPaymentVo.setId(String.valueOf(countVo));				
				fmrCommissionPaymentVoReport.add(fmrCommissionPaymentVo);
				countVo++;
			}
			rs.close();
			pstmt.close();
		}
		catch (Exception e)
		{
			cat.error("FRMCommisionPayoutDetail  => ", e);
		}
		finally
		{
			try
			{
				if (pstmt != null)
				{
					pstmt.close();
				}
				Torque.closeConnection(dbConn);
			}
			catch (Exception e)
			{
				cat.error("FRMCommisionPayoutDetail closeConnection ", e);
			}
		}			
	    return fmrCommissionPaymentVoReport;
	}
	
}
