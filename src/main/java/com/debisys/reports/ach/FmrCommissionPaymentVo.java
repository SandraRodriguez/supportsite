/**
 * 
 */
package com.debisys.reports.ach;

/**
 * @author nmartinez
 *
 */
public class FmrCommissionPaymentVo {

	private String id;
	private String dateTime;
	private String description;
	private String NoTransactions;
	private String commissionAmount;
	private String billingBatchId;
	private String entityId;
	private String statementTypeID;
	private String transactionType;
	private String paymentTypeId;
	
	/*
	 * 
	 */
	public FmrCommissionPaymentVo(String dateTime, String description, String NoTransactions, String commissionAmount, 
								  String billingBatchId, String entityId, String statementTypeID, String transactionType)
	{
		this.dateTime = dateTime;
		this.commissionAmount = commissionAmount;
		this.description = description;
		this.NoTransactions = NoTransactions;		
		this.billingBatchId = billingBatchId;
		this.entityId = entityId;
		this.statementTypeID = statementTypeID;
		this.transactionType = transactionType;
	}
	
	
	
	/**
	 * @return the billingBatchId
	 */
	public String getBillingBatchId() {
		return billingBatchId;
	}



	/**
	 * @param billingBatchId the billingBatchId to set
	 */
	public void setBillingBatchId(String billingBatchId) {
		this.billingBatchId = billingBatchId;
	}



	/**
	 * @return the entityId
	 */
	public String getEntityId() {
		return entityId;
	}



	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}



	/**
	 * @return the statementTypeID
	 */
	public String getStatementTypeID() {
		return statementTypeID;
	}



	/**
	 * @param statementTypeID the statementTypeID to set
	 */
	public void setStatementTypeID(String statementTypeID) {
		this.statementTypeID = statementTypeID;
	}
	
	
	/**
	 * @return the dateTime
	 */
	public String getDateTime() {
		return dateTime;
	}
	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the noTransactions
	 */
	public String getNoTransactions() {
		return NoTransactions;
	}
	/**
	 * @param noTransactions the noTransactions to set
	 */
	public void setNoTransactions(String noTransactions) {
		NoTransactions = noTransactions;
	}
	/**
	 * @return the commissionAmount
	 */
	public String getCommissionAmount() {
		return commissionAmount;
	}
	/**
	 * @param commissionAmount the commissionAmount to set
	 */
	public void setCommissionAmount(String commissionAmount) {
		this.commissionAmount = commissionAmount;
	}



	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}



	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param paymentTypeId the paymentTypeId to set
	 */
	public void setPaymentTypeId(String paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}



	/**
	 * @return the paymentTypeId
	 */
	public String getPaymentTypeId() {
		return paymentTypeId;
	}
	
	
	
	

}
