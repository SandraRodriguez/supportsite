package com.debisys.reports;

import com.debisys.users.SessionData;
import com.debisys.utils.DebisysConfigListener;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import org.apache.torque.Torque;

import com.debisys.exceptions.ReportException;
import com.debisys.languages.Languages;
import com.emida.utils.dbUtils.TorqueHelper;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletContext;

public class ChurnReport {

	public static final String AT_RISK_FILTER = "!!!";

	private static final Logger logger = Logger.getLogger(ChurnReport.class);
	
	private static ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.reports.sql");

	private static boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
			if(!s.contains(".")){
				return false;
			}
		}catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param sessionData
	 * @param merchStatus
	 * @param reportType
	 * @param threshold
	 * @param atRisk
	 * @param isoID
	 * @return
	 * @throws ReportException
	 */
	public Vector<Vector<String>> getChurnReport(SessionData sessionData, int merchStatus, int reportType, double threshold, boolean atRisk, String isoID) throws ReportException {
		// TODO: check permissions
		Connection dbConn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<String>> merchants = new Vector<Vector<String>>();
		Vector<String> vecTemp = null;
		try {
			dbConn = Torque.getConnection(sql_bundle.getString("pkgAlternateDb"));
			if (dbConn == null) {
				logger.error("Can't get database connection");
				throw new ReportException();
			}

			String strRefId ="";
			if ( isoID!=null)
			{
				strRefId=isoID;
			}
			else
			{
				strRefId=sessionData.getProperty("ref_id");
			}				
			
			String strSQL = "EXEC getriskreportsalesbymerchant ?, ?, ?, ?, ?, ?";
			logger.debug(strSQL);
			pstmt = dbConn.prepareStatement(strSQL);
			logger.debug("getriskreportsalesbymerchant(" + strRefId + ", " + merchStatus + ", " + reportType + ", " + threshold + ", "+ (atRisk?ChurnReport.AT_RISK_FILTER:"") + ", 0)");
			pstmt.setString(1, strRefId);
			pstmt.setInt(2, merchStatus);
			pstmt.setInt(3, reportType);
			pstmt.setDouble(4, threshold);
			pstmt.setString(5, atRisk?ChurnReport.AT_RISK_FILTER:"");
			pstmt.setInt(6, 1);
			
			rs = pstmt.executeQuery();
			if(rs.next()){
				vecTemp = new Vector<String>();
				vecTemp.add(("#"));
				String rows = rs.getString(1);
				for(String s: rows.split(",")){
					vecTemp.add("" + s);
				}
				merchants.add(vecTemp);
			}
			rs.close();
			rs = null;
			pstmt.clearParameters();
			pstmt.setString(1, strRefId);
			pstmt.setInt(2, merchStatus);
			pstmt.setInt(3, reportType);
			pstmt.setDouble(4, threshold);
			pstmt.setString(5, atRisk?ChurnReport.AT_RISK_FILTER:"");
			pstmt.setInt(6, 0);
			
			rs = pstmt.executeQuery();
			int j = 0;
			int rows =rs.getMetaData().getColumnCount();
			while (rs.next()){
				j++;
				vecTemp = new Vector<String>();
				vecTemp.add(("" + j));
				for(int i=1; i<= rows; i++){
					String val = "" + rs.getString(i);
					if(val.equals("null")){
						val = "";
					}else{
						if(isDouble(val)){
							val = new DecimalFormat("#0.00").format(Double.valueOf(val));
						}
					}
					vecTemp.add(val);
				}
				merchants.add(vecTemp);
			}
		} catch (Exception e) {
			logger.error("Error during getChurnReport", e);
			throw new ReportException();
		} finally {
			try {
				TorqueHelper.closeConnection(dbConn, pstmt, rs);
			} catch (Exception e) {
				logger.error("Error during closeConnection", e);
			}
		}
		return merchants;
	}

	/**
	 * 
	 * @param sessionData
	 * @param context
	 * @param merchStatus
	 * @param reportType
	 * @param threshold
	 * @param atRisk
	 * @param isoID
	 * @return
	 * @throws ReportException
	 */
	public String downloadChurnReportCVS(SessionData sessionData, ServletContext context, int merchStatus, int reportType, double threshold, boolean atRisk, String isoID) throws ReportException{
		// TODO: check permissions
		long start = System.currentTimeMillis();
		String strFileName = "";
		String downloadUrl = DebisysConfigListener.getDownloadUrl(context);
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);
		try{
			long startQuery = 0;
			long endQuery = 0;
			startQuery = System.currentTimeMillis();
			BufferedWriter outputFile = null;
			try{
				strFileName = generateFileName(context);
				outputFile = new BufferedWriter(new FileWriter(workingDir + filePrefix + strFileName + ".csv"));
				logger.debug("Temp File Name: " + workingDir + filePrefix + strFileName + ".csv");
				outputFile.flush();
				// TODO: internationalize
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle1", sessionData.getLanguage())+"\n");
				outputFile.write("\"" + Languages.getString("jsp.admin.reports.churn.csvtitle2a", sessionData.getLanguage())+": " + sessionData.getProperty("company_name") + " (" + sessionData.getProperty("ref_id") +")\"\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle2b", sessionData.getLanguage())+": " + sessionData.getProperty("username") + "\n");
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z");
				String d = sdf.format(new Date());
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle3", sessionData.getLanguage())+": " + d + "\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csvtitle4", sessionData.getLanguage())+":\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.csv"+(reportType==1?"weekly":"monthly"),sessionData.getLanguage())+"\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.threshold2", sessionData.getLanguage())+": " + (threshold*100) + "\n");
				outputFile.write(Languages.getString("jsp.admin.reports.churn.note", sessionData.getLanguage()) + ": " + Languages.getString("jsp.admin.reports.churn.note4", sessionData.getLanguage()) + "\n\n");
				buildReportToDownload(outputFile, getChurnReport(sessionData, merchStatus, reportType, threshold, atRisk,isoID));
				outputFile.flush();
				outputFile.close();
				endQuery = System.currentTimeMillis();
				logger.debug("File Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
				startQuery = System.currentTimeMillis();
				File sourceFile = new File(workingDir + filePrefix + strFileName + ".csv");
				long size = sourceFile.length();
				byte[] buffer = new byte[(int) size];
				String zipFileName = downloadPath + filePrefix + strFileName + ".zip";
				logger.debug("Zip File Name: " + zipFileName);
				downloadUrl = downloadUrl + filePrefix + strFileName + ".zip";
				logger.debug("Download URL: " + downloadUrl);
				try{
					ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
					out.setLevel(Deflater.DEFAULT_COMPRESSION);
					FileInputStream in = new FileInputStream(workingDir + filePrefix + strFileName + ".csv");
					out.putNextEntry(new ZipEntry(filePrefix + strFileName + ".csv"));
					int len;
					while ((len = in.read(buffer)) > 0){
						out.write(buffer, 0, len);
					}
					out.closeEntry();
					in.close();
					out.close();
				}catch (IllegalArgumentException iae){
					iae.printStackTrace();
				}catch (FileNotFoundException fnfe){
					fnfe.printStackTrace();
				}catch (IOException ioe){
					ioe.printStackTrace();
				}
				sourceFile.delete();
				endQuery = System.currentTimeMillis();
				logger.debug("Zip Out: " + (((endQuery - startQuery) / 1000.0)) + " seconds");
			}catch (IOException ioe){
				try{
					if (outputFile != null){
						outputFile.close();
					}
				}catch (IOException ioe2){
				}
			}
		}catch (Exception e){
			logger.error("Error during downloadChurnReportCVS", e);
			throw new ReportException();
		}
		long end = System.currentTimeMillis();
		logger.debug("Total Elapsed Time: " + (((end - start) / 1000.0)) + " seconds");
		return downloadUrl;
	}

	/**
	 * @param outputFile
	 * @throws IOException
	 */
	private static void buildReportToDownload(BufferedWriter outputFile, Vector<Vector<String>> vData) throws IOException  {
		for (int i = 0; i < vData.size(); i++){
			int items = vData.get(i).size();
			for (int k = 1; k < items; k++){
				if (vData.get(i).get(k)!=null && vData.get(i).get(k)!= "null" ){
					String val = vData.get(i).get(k).trim();
					if(val.matches("^[0-9]+$")){
						val = "\"'" + val + "\"";
					}else{
						if (!val.startsWith("\"") && !val.endsWith("\"") && val.contains(",")){
							val = "\"" + val + "\"";
						}
					}
					outputFile.write( val + ",");
				}else{
					outputFile.write("\"N/A\",");
				}
			}
			outputFile.write("\n");
		}
	}
	
	public String generateFileName(ServletContext context){
		boolean isUnique = false;
		String strFileName = "";
		while (!isUnique){
			strFileName = generateRandomNumber();
			isUnique = checkFileName(strFileName, context);
		}
		return strFileName;
	}
	
	private String generateRandomNumber(){
		StringBuffer s = new StringBuffer();
		// number between 1-9 because first digit must not be 0
		int nextInt = (int) ((Math.random() * 9) + 1);
		s = s.append(nextInt);
		for (int i = 0; i <= 10; i++) {
			// number between 0-9
			nextInt = (int) (Math.random() * 10);
			s = s.append(nextInt);
		}

		return s.toString();
	}

	private boolean checkFileName(String strFileName, ServletContext context){
		boolean isUnique = true;
		String downloadPath = DebisysConfigListener.getDownloadPath(context);
		String workingDir = DebisysConfigListener.getWorkingDir(context);
		String filePrefix = DebisysConfigListener.getFilePrefix(context);

		File f = new File(workingDir + "/" + filePrefix + strFileName + ".csv");
		if (f.exists()){
			// duplicate file found
			logger.error("Duplicate file found:" + workingDir + "/" + filePrefix + strFileName + ".csv");
			return false;
		}

		f = new File(downloadPath + "/" + filePrefix + strFileName + ".zip");
		if (f.exists()){
			// duplicate file found
			logger.error("Duplicate file found:" + downloadPath + "/" + filePrefix + strFileName + ".zip");
			return false;
		}
		return isUnique;
	}
}
