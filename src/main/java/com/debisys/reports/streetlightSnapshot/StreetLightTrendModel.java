
package com.debisys.reports.streetlightSnapshot;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class StreetLightTrendModel {
    
    
    private String light;
    private boolean active;
    private String definition;
    private int totalOccurences;
    private int countMerchants;
    private BigDecimal merchantsPercent;
    private BigDecimal occurencesPercent;

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public int getTotalOccurences() {
        return totalOccurences;
    }

    public void setTotalOccurences(int totalOccurences) {
        this.totalOccurences = totalOccurences;
    }

    public int getCountMerchants() {
        return countMerchants;
    }

    public void setCountMerchants(int countMerchants) {
        this.countMerchants = countMerchants;
    }

    public BigDecimal getMerchantsPercent() {
        return merchantsPercent;
    }

    public void setMerchantsPercent(BigDecimal merchantsPercent) {
        this.merchantsPercent = merchantsPercent;
    }

    public BigDecimal getOccurencesPercent() {
        return occurencesPercent;
    }

    public void setOccurencesPercent(BigDecimal occurencesPercent) {
        this.occurencesPercent = occurencesPercent;
    }
    
    
}
