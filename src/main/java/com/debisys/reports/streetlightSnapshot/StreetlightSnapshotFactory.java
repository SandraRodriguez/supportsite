package com.debisys.reports.streetlightSnapshot;

import com.debisys.languages.Languages;
import com.debisys.users.SessionData;
import com.debisys.utils.TimeZone;
import com.emida.utils.dbUtils.TorqueHelper;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import org.apache.log4j.Category;

/**
 *
 * @author dgarzon
 */
public class StreetlightSnapshotFactory {
    
    static Category cat = Category.getInstance(StreetlightSnapshotFactory.class);
    private static final ResourceBundle sql_bundle = ResourceBundle.getBundle("com.debisys.tools.simInventory.sql");

    private static String getSQLQuery(String iso) {

        //###################  TEMP VALUES ########################
        //iso = "380654193158";
        
        String sql = " select SUM(a) TotalA, SUM(b) TotalB, SUM(c) TotalC "
                + " FROM( "
                + " select  "
                + " 	SUM(CASE WHEN remaining_days < 1 THEN 1 ELSE 0 END) as a, "
                + " 	SUM(CASE WHEN remaining_days >= 1 AND remaining_days < 3 THEN 1 ELSE 0 END) AS b,  "
                + " 	SUM (CASE WHEN remaining_days >= 3 THEN 1 ELSE 0 END) AS c "
                + " from( "
                + "  "
                + " SELECT  "
                + " 	ROUND( (m.liabilitylimit - m.runningliability) / (sum(dailytotalamount) / 30),1) AS remaining_days, m.merchant_id "
                + " 	FROM aggdailytotalsalessummary wt with(nolock)  "
                + " 	INNER JOIN merchants m WITH(NOLOCK) ON (m.merchant_id = wt.merchant_id AND (m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = ''))  "
                + "     INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1) " 
                + "     INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5) " 
                + "     INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4)" 
                + "     INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3))"
                + " 	WHERE i.rep_id = "+iso
                + "     AND wt.input_system >= DATEADD(d, -30, dbo.GetLocalTimeByAccessLevel(1, "+iso+", GETDATE(), 0)) "
                + "	AND wt.input_system < DATEADD(d, 1, dbo.GetLocalTimeByAccessLevel(1, "+iso+", GETDATE(), 0)) "
                //+ "     AND datetime >= '2015-03-21 00:00:00' and datetime <= '2017-04-21 23:59:59' "
                + " 	GROUP BY m.merchant_id, m.liabilitylimit, m.runningliability "
                + " 	HAVING sum(dailytotalamount) > 0  "
                + " ) as b "
                + " GROUP BY  remaining_days "
                + " ) rta ";
        
        return sql;
    }
    
    
    public static List<StreetlightSnapshotModel> getReport(SessionData sessionData) {

        List<StreetlightSnapshotModel> list = new ArrayList<StreetlightSnapshotModel>();
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(StreetlightSnapshotFactory.sql_bundle.getString("pkgAlternateDb"));//CHANGE = pkgDefaultDb, pkgAlternateDb
            String iso = sessionData.getProperty("ref_id");
            String sql = getSQLQuery(iso);
            cat.info("getReport ["+sql+"]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {

                StreetlightSnapshotModel model1 = new StreetlightSnapshotModel();
                model1.setLight("RED");
                model1.setActive(true);
                model1.setDefinition(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.lessDayOfBalance",sessionData.getLanguage()));
                model1.setTotalMerchants(rs.getInt("TotalA"));
                
                StreetlightSnapshotModel model2 = new StreetlightSnapshotModel();
                model2.setLight("YELLOW");
                model2.setActive(true);
                model2.setDefinition(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.1And3DayOfBalance",sessionData.getLanguage()));
                model2.setTotalMerchants(rs.getInt("TotalB"));
                
                StreetlightSnapshotModel model3 = new StreetlightSnapshotModel();
                model3.setLight("GREEN");
                model3.setActive(true);
                model3.setDefinition(Languages.getString("jsp.admin.reports.flow.streetlightSnapshot.more3DayOfBalance",sessionData.getLanguage()));
                model3.setTotalMerchants(rs.getInt("TotalC"));

                list.add(model1);
                list.add(model2);
                list.add(model3);
                
            }
        } catch (Exception e) {
            cat.error("Error during getReport", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }
    
    private static String getSqlInactiveMerchants(String iso){
        
        //###################  TEMP VALUES ########################
        //iso = "380654193158";
        
        String sql = 
            //"SELECT DISTINCT(m.merchant_id),m.dba, ROUND( (m.liabilitylimit - m.runningliability) / (sum(dailytotalamount) / 30),1) AS remaining_days " +
            "SELECT DISTINCT(m.merchant_id), m.dba, ROUND(CASE (sum(dailytotalamount) / 30) WHEN 0 THEN 0 ELSE (m.liabilitylimit - m.runningliability) / (sum(dailytotalamount) / 30) END,1) AS remaining_days " +
            "FROM dbo.merchants M WITH(NOLOCK) " +
            "LEFT JOIN aggdailytotalsalessummary wt WITH(NOLOCK) ON (m.merchant_id = wt.merchant_id)  " +
            "INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1)  " +
            "INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5)  " +
            "INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4) " +
            "INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3)) " +
            "WHERE i.rep_id = "+iso+" AND " +
            "	((m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) AND " +
            "    NOT EXISTS(SELECT rec_id FROM dbo.aggdailytotalsalessummary WT2 WITH(NOLOCK) " +
            "	WHERE "
            //+ " WT2.datetime >= '2017-05-01 00:00:00' and WT2.datetime <= '2017-05-05 23:59:59' and WT2.merchant_id = m.merchant_id)" 
            + " WT2.input_system >= DATEADD(d, -7, dbo.GetLocalTimeByAccessLevel(1, "+iso+", GETDATE(), 0)) AND "
            + " WT2.input_system < DATEADD(d, 1, dbo.GetLocalTimeByAccessLevel(1, "+iso+", GETDATE(), 0))) "
            + " GROUP BY m.merchant_id, m.dba, m.liabilitylimit, m.runningliability ";
        
        return sql;
    }
    
    public static List<StreetlightSnapshotModelGray> getInactiveMerchants(String filterBy, String filterValues, String iso, boolean isTrend) {

        List<StreetlightSnapshotModelGray> list = new ArrayList<StreetlightSnapshotModelGray>(); 
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(StreetlightSnapshotFactory.sql_bundle.getString("pkgAlternateDb"));//CHANGE = pkgDefaultDb, pkgAlternateDb
            
            String sql = "";
            if(isTrend){
                sql = getSqlInactiveMerchantsTrend(filterBy, filterValues, iso);
            }
            else{
                sql = getSqlInactiveMerchants(iso);
            }
            
            cat.info("getInactiveMerchants ["+sql+"]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                StreetlightSnapshotModelGray model0 = new StreetlightSnapshotModelGray();
                model0.setMerchantDBA(rs.getString("dba"));
                model0.setMerchantId(rs.getBigDecimal("merchant_id"));
                model0.setRemainingDays(rs.getDouble("remaining_days"));               
                list.add(model0);
            }
        } catch (Exception e) {
            cat.error("Error during getInactiveMerchants", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }
    
    
    
    //##############################################################################################################################################
    
    private static String getSQLQueryMainTrend(String filterBy, String filterValues, String iso, String startDate, String endDate) {
        StringBuilder sql = new StringBuilder();
        try {
            
            //long numDays = getNumDays(startDate, endDate);
            String newStartDate = formatSQLDate(startDate);
            String newEndDate = formatSQLDate(endDate);
            
            String daysBefore30 = getDate30DaysBefore(endDate);
            
            Vector vTimeZoneFilterDates = TimeZone.getTimeZoneFilterDates("1", iso, newStartDate, newEndDate + " 23:59:59.999", false);
            Vector vTZFilter30DaysBefore = TimeZone.getTimeZoneFilterDates("1", iso, daysBefore30, newEndDate + " 23:59:59.999", false);
            
            //###################  TEMP VALUES ########################
            /*filterBy = "iso";
            filterValues = "380654193158";
            iso = "380654193158";
            vTimeZoneFilterDates.set(0, "2017-03-21 00:00:00");
            vTimeZoneFilterDates.set(1, "2017-04-21 23:59:59");
            
            vTZFilter30DaysBefore.set(0, "2017-03-21 00:00:00");
            vTZFilter30DaysBefore.set(1, "2017-04-21 23:59:59"); //*/
            //###################  END TEMP VALUES  ###################

            
            sql.append(" SELECT rta.TotalA, rta.TotalB, rta.TotalC, rta.countMerchantsRed,rta.countMerchantsYellow, rta.countMerchantsGreen, ");
            sql.append(" ((CAST(rta.TotalA AS DECIMAL(12,2)) * 100)/rta.totalOccurrences) AS percentOccRed, ((CAST(rta.TotalB AS DECIMAL(12,2)) * 100)/rta.totalOccurrences) AS percentOccYellow, ((CAST(rta.TotalC AS DECIMAL(12,2)) * 100)/rta.totalOccurrences) AS percentOccGreen ");
            sql.append(" FROM( ");
            sql.append(" 	SELECT SUM(a) TotalA, SUM(b) TotalB, SUM(c) TotalC,  ");
            sql.append(" 	COUNT(DISTINCT merchant1) AS countMerchantsRed , COUNT(DISTINCT merchant2) AS countMerchantsYellow, COUNT(DISTINCT merchant3) AS countMerchantsGreen,  ");
            sql.append(" 	SUM(totalOccurrences) AS totalOccurrences ");
            sql.append(" 	FROM( ");
            sql.append(" 		SELECT  ");
            sql.append(" 			SUM(CASE WHEN rta0.remaining_days < 1 THEN 1 ELSE 0 END) as a,  ");
            sql.append(" 			SUM(CASE WHEN rta0.remaining_days >= 1 AND rta0.remaining_days < 3 THEN 1 ELSE 0 END) AS b,   ");
            sql.append(" 			SUM (CASE WHEN rta0.remaining_days >= 3 THEN 1 ELSE 0 END) AS c, ");
            sql.append(" 			CASE WHEN rta0.remaining_days < 1 THEN rta0.cMerchant ELSE null END AS merchant1,  ");
            sql.append(" 			CASE WHEN rta0.remaining_days >= 1 AND rta0.remaining_days  < 3 THEN rta0.cMerchant ELSE null END AS merchant2, ");
            sql.append(" 			CASE WHEN rta0.remaining_days >= 3 THEN rta0.cMerchant ELSE null END AS merchant3, ");
            sql.append(" 			COUNT(rta0.remaining_days) AS totalOccurrences ");
            sql.append(" 		FROM(  ");
            sql.append(" 			SELECT    ");
            sql.append(" 				ROUND(((cw.merchant_CreditLimit - cw.merchant_runningliability)/sumAmount),1) as remaining_days, cw.datetime, b.merchant_id AS cMerchant ");
            sql.append(" 			FROM(  ");
            sql.append(" 			SELECT  ");
            sql.append("                                (SELECT (sum(wt1.dailytotalamount) / 30) FROM aggdailytotalsalessummary wt1 WITH(NOLOCK) WHERE m.merchant_id = wt1.merchant_id ");
            sql.append("                                AND wt1.input_system >= '").append(vTZFilter30DaysBefore.get(0)).append("' and wt1.input_system <= '").append(vTZFilter30DaysBefore.get(1)).append("') AS sumAmount, ");
            sql.append("                                m.merchant_id, m.rep_id "); // ").append(numDays).append("
            sql.append(" 				FROM  merchants m  with(nolock)  ");
            sql.append(" 				LEFT JOIN  aggdailytotalsalessummary wt WITH(NOLOCK) ON (wt.merchant_id = m.merchant_id)  ");
            sql.append(" 				INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1) ");
            sql.append(" 				INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5) ");
            sql.append(" 				INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4) ");
            sql.append(" 				INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3))  ");
            sql.append(" 				WHERE ");
            sql.append(getWhereFilter(filterBy, filterValues, iso));
            sql.append(" 				((m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = ''))  ");
            sql.append(" 				AND (input_system >= '").append(vTimeZoneFilterDates.get(0)).append("' and input_system <= '").append(vTimeZoneFilterDates.get(1)).append("') ");
            sql.append(" 				GROUP BY m.merchant_id, m.liabilitylimit, m.runningliability, m.rep_id ");
            sql.append(" 				HAVING SUM(dailytotalamount) > 0   ");
            sql.append(" 			) as b  ");
            sql.append(" 			LEFT JOIN BalanceAll_CWC cw WITH(NOLOCK) ON b.merchant_id = cw.merchant ");
            sql.append(" 			WHERE cw.datetime >= '").append(vTimeZoneFilterDates.get(0)).append("' and cw.datetime <= '").append(vTimeZoneFilterDates.get(1)).append("' ");
            sql.append(" 		) rta0   ");
            sql.append(" 		group by remaining_days, cMerchant ");
            sql.append(" 	) grouping_values ");
            sql.append(" )rta ");
            
            
        } catch (Exception ex) {
            cat.error("Error during getSQLQueryMainTrend", ex);
        }
        return sql.toString();
    }
    
    private static String getWhereFilter(String filterBy, String filterValues, String iso) {
        String whereFilter = "";
        if (filterBy.equalsIgnoreCase("merchants")) {
            whereFilter = " m.merchant_id IN (" + filterValues + ") AND ";
        }
        else if (filterBy.equalsIgnoreCase("reps")) {
            whereFilter = " r.rep_id IN (" + filterValues + ") AND ";
        }
        else if (filterBy.equalsIgnoreCase("subAgents")) {
            whereFilter = " s.rep_id IN (" + filterValues + ") AND ";
        }
        else if (filterBy.equalsIgnoreCase("agents")) {
            whereFilter = " a.rep_id IN (" + filterValues + ") AND ";
        }
        else {
            whereFilter = " i.rep_id = " + iso + " AND ";
        }
        return whereFilter;
    }
    
    private static String getSqlInactiveMerchantsTrend(String filterBy, String filterValues, String iso) {

        //###################  TEMP VALUES ########################
        //filterBy = "iso";
        //filterValues = "380654193158";
        //iso = "380654193158";
        //###################  END TEMP VALUES ########################

        StringBuilder sql = new StringBuilder();
//        sql.append(" SELECT DISTINCT(m.merchant_id), m.dba, ROUND( (m.liabilitylimit - m.runningliability) / (sum(dailytotalamount) / 7),1) AS remaining_days ");
        sql.append(" SELECT DISTINCT(m.merchant_id), m.dba, ROUND(CASE (sum(dailytotalamount) / 7) WHEN 0 THEN 0 ELSE (m.liabilitylimit - m.runningliability) / (sum(dailytotalamount) / 7) END,1) AS remaining_days ");
        sql.append(" FROM dbo.merchants M WITH(NOLOCK) ");
        sql.append(" LEFT JOIN aggdailytotalsalessummary wt WITH(NOLOCK) ON (m.merchant_id = wt.merchant_id) ");
        sql.append(" INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1) ");
        sql.append(" INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5) ");
        sql.append(" INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4) ");
        sql.append(" INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3)) ");
        sql.append(" WHERE ");
        sql.append(getWhereFilter(filterBy, filterValues, iso));
        sql.append(" 	((m.merchant_type = 1 OR m.merchant_type = 2) AND (m.DateCancelled is null OR m.DateCancelled = '')) AND ");
        sql.append(" 	NOT EXISTS(SELECT rec_id FROM dbo.aggdailytotalsalessummary WT2 WITH(NOLOCK) ");
        sql.append(" 	WHERE ");
        
        //sql.append("    WT2.datetime >= '2017-05-01 00:00:00' and WT2.datetime <= '2017-05-05 23:59:59'  ");
        
        sql.append("    WT2.input_system >= DATEADD(d, -7, dbo.GetLocalTimeByAccessLevel(1, ").append(iso).append(", GETDATE(), 0)) AND ");
        sql.append("    WT2.input_system < DATEADD(d, 1, dbo.GetLocalTimeByAccessLevel(1, ").append(iso).append(", GETDATE(), 0)) ");
        sql.append("    AND WT2.merchant_id = m.merchant_id) ");
        sql.append(" GROUP BY m.merchant_id, m.dba, m.liabilitylimit, m.runningliability ");

        return sql.toString();
    }
    
    private static String formatSQLDate(String currentDate){
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = myFormat.parse(currentDate);
            myFormat.applyPattern("yyyy-MM-dd");
            return myFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentDate;
    }
    
    public static String getDate30DaysBefore(String currentDate){
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            Date date1 = myFormat.parse(currentDate);
            
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date1);
            calendar.set(Calendar.DATE, (calendar.get(Calendar.DATE) - 30));
            
            myFormat.applyPattern("yyyy-MM-dd");
            return myFormat.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return currentDate;
    }
    
    
    public static List<StreetLightTrendModel> getReportMainTrend(String filterBy, String filterValues, String iso, String startDate, String endDate) {

        List<StreetLightTrendModel> list = new ArrayList<StreetLightTrendModel>(); 
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            int totalMerchants = getCountMerchants(iso);
            if(totalMerchants == 0){
                totalMerchants = 1;
            }
            dbConn = TorqueHelper.getConnection(StreetlightSnapshotFactory.sql_bundle.getString("pkgAlternateDb"));   //pkgDefaultDb          
            String sql = getSQLQueryMainTrend(filterBy, filterValues, iso, startDate, endDate);
            cat.info("getReportTrend ["+sql+"]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                
                StreetLightTrendModel model1 = new StreetLightTrendModel();
                model1.setLight("RED");
                model1.setActive(true);
                model1.setDefinition("< 1 day of balance");
                model1.setTotalOccurences(rs.getInt("TotalA"));
                model1.setCountMerchants(rs.getInt("countMerchantsRed"));
                model1.setMerchantsPercent(new BigDecimal((rs.getInt("countMerchantsRed")*100)).divide(BigDecimal.valueOf(totalMerchants), 2, RoundingMode.HALF_UP));
                
                BigDecimal percentOccRed = rs.getBigDecimal("percentOccRed");
                if(percentOccRed != null){
                    percentOccRed = rs.getBigDecimal("percentOccRed").divide(BigDecimal.ONE, 2 ,RoundingMode.HALF_UP);
                }
                else{
                    percentOccRed = BigDecimal.ZERO;
                }
                
                model1.setOccurencesPercent(percentOccRed);
                
                StreetLightTrendModel model2 = new StreetLightTrendModel();
                model2.setLight("YELLOW");
                model2.setActive(true);
                model2.setDefinition(">= 1,  and <3 days of balance");
                model2.setTotalOccurences(rs.getInt("TotalB"));
                model2.setCountMerchants(rs.getInt("countMerchantsYellow"));
                model2.setMerchantsPercent(new BigDecimal((rs.getInt("countMerchantsYellow")*100)).divide(BigDecimal.valueOf(totalMerchants), 2, RoundingMode.HALF_UP));
                
                BigDecimal percentOccYellow = rs.getBigDecimal("percentOccYellow");
                if(percentOccYellow != null){
                    percentOccYellow = rs.getBigDecimal("percentOccYellow").divide(BigDecimal.ONE, 2 ,RoundingMode.HALF_UP);
                }
                else{
                    percentOccYellow = BigDecimal.ZERO;
                }
                
                model2.setOccurencesPercent(percentOccYellow);
                
                StreetLightTrendModel model3 = new StreetLightTrendModel();
                model3.setLight("GREEN");
                model3.setActive(true);
                model3.setDefinition(">= 3 days of balance");
                model3.setTotalOccurences(rs.getInt("TotalC"));
                model3.setCountMerchants(rs.getInt("countMerchantsGreen"));
                model3.setMerchantsPercent(new BigDecimal((rs.getInt("countMerchantsGreen")*100)).divide(BigDecimal.valueOf(totalMerchants), 2, RoundingMode.HALF_UP));
                
                BigDecimal percentOccGreen = rs.getBigDecimal("percentOccGreen");
                if(percentOccGreen != null){
                    percentOccGreen = rs.getBigDecimal("percentOccGreen").divide(BigDecimal.ONE, 2 ,RoundingMode.HALF_UP);
                }
                else{
                    percentOccGreen = BigDecimal.ZERO;
                }
                
                model3.setOccurencesPercent(percentOccGreen);

                list.add(model1);
                list.add(model2);
                list.add(model3);
                
            }
        } catch (Exception e) {
            cat.error("Error during getReportMainTrend", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return list;
    }
    
    
    public static int getCountMerchants(String iso) {

        //###################  TEMP VALUES ########################
        //iso = "380654193158";
        //###################  END TEMP VALUES  ###################
        
        Connection dbConn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            dbConn = TorqueHelper.getConnection(StreetlightSnapshotFactory.sql_bundle.getString("pkgAlternateDb")); //CHANGE = pkgDefaultDb, pkgAlternateDb
            String sql = "SELECT COUNT(m.merchant_id) countMerchants "
                    + " FROM merchants m WITH(NOLOCK) "
                    + " INNER JOIN reps r with(nolock) ON (r.rep_id = m.rep_id AND r.type = 1) "
                    + " INNER JOIN reps s with(nolock) ON (s.rep_id = r.iso_id AND s.type = 5) "
                    + " INNER JOIN reps a with(nolock) ON (a.rep_id = s.iso_id AND a.type = 4) "
                    + " INNER JOIN reps i with(nolock) ON (i.rep_id = a.iso_id AND (i.type = 2 OR i.type = 3)) "
                    + " WHERE i.rep_id = " + iso + " AND (merchant_type = 1 OR merchant_type = 2) AND (DateCancelled is null OR DateCancelled = '') ";
            cat.info("getCountMerchants by Iso [" + iso + "] [" + sql + "]");
            pstmt = dbConn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getInt("countMerchants");
            }
        } catch (Exception e) {
            cat.error("Error during getCountMerchants", e);
        } finally {
            TorqueHelper.closeConnection(dbConn, pstmt, rs);
        }
        return 0;
    }

}
