
package com.debisys.reports.streetlightSnapshot;

/**
 *
 * @author dgarzon
 */
public class StreetlightSnapshotModel {
    
    private String light;
    private boolean active;
    private String definition;
    private int totalMerchants;

    public String getLight() {
        return light;
    }

    public void setLight(String light) {
        this.light = light;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public int getTotalMerchants() {
        return totalMerchants;
    }

    public void setTotalMerchants(int totalMerchants) {
        this.totalMerchants = totalMerchants;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    
}
