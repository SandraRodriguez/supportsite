
package com.debisys.reports.streetlightSnapshot;

import java.math.BigDecimal;

/**
 *
 * @author dgarzon
 */
public class StreetlightSnapshotModelGray {
    
    private String merchantDBA;
    private BigDecimal merchantId;
    private double remainingDays;

    public String getMerchantDBA() {
        return merchantDBA;
    }

    public void setMerchantDBA(String merchantDBA) {
        this.merchantDBA = merchantDBA;
    }

    public BigDecimal getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(BigDecimal merchantId) {
        this.merchantId = merchantId;
    }

    public double getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(double remainingDays) {
        this.remainingDays = remainingDays;
    }
    
    
    
}
