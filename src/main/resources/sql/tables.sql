if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[website_log]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[website_log]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[website_log_categories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[website_log_categories]
GO

CREATE TABLE [dbo].[website_log] (
	[website_log_id] [int] IDENTITY (1, 1) NOT NULL ,
	[website_log_category_id] [int] NULL ,
	[website_log_date] [datetime] NULL ,
	[ref_id] [numeric](15, 0) NULL ,
	[ref_type] [int] NULL ,
	[applied_ref_id] [numeric](18, 0) NULL ,
	[applied_ref_type] [int] NULL ,
	[logon_id] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[message] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[website_log_categories] (
	[website_log_category_id] [int] NOT NULL ,
	[description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[website_log] WITH NOCHECK ADD 
	CONSTRAINT [PK_website_log] PRIMARY KEY  CLUSTERED 
	(
		[website_log_id]
	)  ON [PRIMARY] 
GO

insert website_log_categories (website_log_category_id, description) values (1, 'Login/Logout')
insert website_log_categories (website_log_category_id, description) values (2, 'Customer')
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[merchant_credits]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[merchant_credits]
GO

CREATE TABLE [dbo].[merchant_credits] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[merchant_id] [numeric](15, 0) NULL ,
	[username] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[payment] [money] NULL ,
	[balance] [money] NULL ,
	[prior_terminal_sales] [money] NULL ,
	[prior_available_credit] [money] NULL ,
	[datetime] [datetime] NOT NULL 
) ON [PRIMARY]
GO
