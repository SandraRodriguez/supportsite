if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_paging_cursor]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)਍搀爀漀瀀 瀀爀漀挀攀搀甀爀攀 嬀搀戀漀崀⸀嬀猀瀀开瀀愀最椀渀最开挀甀爀猀漀爀崀ഀഀ
GO਍ഀഀ
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_paging_rowcount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)਍搀爀漀瀀 瀀爀漀挀攀搀甀爀攀 嬀搀戀漀崀⸀嬀猀瀀开瀀愀最椀渀最开爀漀眀挀漀甀渀琀崀ഀഀ
GO਍ഀഀ
SET QUOTED_IDENTIFIER ON ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍䌀刀䔀䄀吀䔀 倀刀伀䌀䔀䐀唀刀䔀 猀瀀开瀀愀最椀渀最开挀甀爀猀漀爀 ⠀ഀഀ
@Tables varchar(1000),਍䀀倀䬀 瘀愀爀挀栀愀爀⠀㄀　　⤀Ⰰഀഀ
@Sort varchar(200) = NULL,਍䀀倀愀最攀一甀洀戀攀爀 椀渀琀 㴀 ㄀Ⰰഀഀ
@PageSize int = 10,਍䀀䘀椀攀氀搀猀 瘀愀爀挀栀愀爀⠀㄀　　　⤀ 㴀 ✀⨀✀Ⰰഀഀ
@Filter varchar(1000) = NULL,਍䀀䜀爀漀甀瀀 瘀愀爀挀栀愀爀⠀㄀　　　⤀ 㴀 一唀䰀䰀⤀ഀഀ
AS਍ഀഀ
/*Find the @PK type*/਍䐀䔀䌀䰀䄀刀䔀 䀀倀䬀吀愀戀氀攀 瘀愀爀挀栀愀爀⠀㄀　　⤀ഀഀ
DECLARE @PKName varchar(100)਍䐀䔀䌀䰀䄀刀䔀 䀀琀礀瀀攀 瘀愀爀挀栀愀爀⠀㄀　　⤀ഀഀ
DECLARE @prec int਍ഀഀ
IF CHARINDEX('.', @PK) > 0਍ऀ䈀䔀䜀䤀一ഀഀ
		SET @PKTable = SUBSTRING(@PK, 0, CHARINDEX('.',@PK))਍ऀऀ匀䔀吀 䀀倀䬀一愀洀攀 㴀 匀唀䈀匀吀刀䤀一䜀⠀䀀倀䬀Ⰰ 䌀䠀䄀刀䤀一䐀䔀堀⠀✀⸀✀Ⰰ䀀倀䬀⤀ ⬀ ㄀Ⰰ 䰀䔀一⠀䀀倀䬀⤀⤀ഀഀ
	END਍䔀䰀匀䔀ഀഀ
	BEGIN਍ऀऀ匀䔀吀 䀀倀䬀吀愀戀氀攀 㴀 䀀吀愀戀氀攀猀ഀഀ
		SET @PKName = @PK਍ऀ䔀一䐀ഀഀ
਍匀䔀䰀䔀䌀吀 䀀琀礀瀀攀㴀琀⸀渀愀洀攀Ⰰ 䀀瀀爀攀挀㴀挀⸀瀀爀攀挀ഀഀ
FROM sysobjects o ਍䨀伀䤀一 猀礀猀挀漀氀甀洀渀猀 挀 漀渀 漀⸀椀搀㴀挀⸀椀搀ഀഀ
JOIN systypes t on c.xusertype=t.xusertype਍圀䠀䔀刀䔀 漀⸀渀愀洀攀 㴀 䀀倀䬀吀愀戀氀攀 䄀一䐀 挀⸀渀愀洀攀 㴀 䀀倀䬀一愀洀攀ഀഀ
਍䤀䘀 䌀䠀䄀刀䤀一䐀䔀堀⠀✀挀栀愀爀✀Ⰰ 䀀琀礀瀀攀⤀ 㸀 　ഀഀ
   SET @type = @type + '(' + CAST(@prec AS varchar) + ')'਍ഀഀ
DECLARE @strPageSize varchar(50)਍䐀䔀䌀䰀䄀刀䔀 䀀猀琀爀匀琀愀爀琀刀漀眀 瘀愀爀挀栀愀爀⠀㔀　⤀ഀഀ
DECLARE @strFilter varchar(1000)਍䐀䔀䌀䰀䄀刀䔀 䀀猀琀爀䜀爀漀甀瀀 瘀愀爀挀栀愀爀⠀㄀　　　⤀ഀഀ
਍⼀⨀䐀攀昀愀甀氀琀 匀漀爀琀椀渀最⨀⼀ഀഀ
IF @Sort IS NULL OR @Sort = ''਍ऀ匀䔀吀 䀀匀漀爀琀 㴀 䀀倀䬀ഀഀ
਍⼀⨀䐀攀昀愀甀氀琀 倀愀最攀 一甀洀戀攀爀⨀⼀ഀഀ
IF @PageNumber < 1਍ऀ匀䔀吀 䀀倀愀最攀一甀洀戀攀爀 㴀 ㄀ഀഀ
਍⼀⨀匀攀琀 瀀愀最椀渀最 瘀愀爀椀愀戀氀攀猀⸀⨀⼀ഀഀ
SET @strPageSize = CAST(@PageSize AS varchar(50))਍匀䔀吀 䀀猀琀爀匀琀愀爀琀刀漀眀 㴀 䌀䄀匀吀⠀⠀⠀䀀倀愀最攀一甀洀戀攀爀 ⴀ ㄀⤀⨀䀀倀愀最攀匀椀稀攀 ⬀ ㄀⤀ 䄀匀 瘀愀爀挀栀愀爀⠀㔀　⤀⤀ഀഀ
਍⼀⨀匀攀琀 昀椀氀琀攀爀 ☀ 最爀漀甀瀀 瘀愀爀椀愀戀氀攀猀⸀⨀⼀ഀഀ
IF @Filter IS NOT NULL AND @Filter != ''਍ऀ匀䔀吀 䀀猀琀爀䘀椀氀琀攀爀 㴀 ✀ 圀䠀䔀刀䔀 ✀ ⬀ 䀀䘀椀氀琀攀爀 ⬀ ✀ ✀ഀഀ
ELSE਍ऀ匀䔀吀 䀀猀琀爀䘀椀氀琀攀爀 㴀 ✀✀ഀഀ
IF @Group IS NOT NULL AND @Group != ''਍ऀ匀䔀吀 䀀猀琀爀䜀爀漀甀瀀 㴀 ✀ 䜀刀伀唀倀 䈀夀 ✀ ⬀ 䀀䜀爀漀甀瀀 ⬀ ✀ ✀ഀഀ
ELSE਍ऀ匀䔀吀 䀀猀琀爀䜀爀漀甀瀀 㴀 ✀✀ഀഀ
	਍⼀⨀䔀砀攀挀甀琀攀 搀礀渀愀洀椀挀 焀甀攀爀礀⨀⼀ऀഀഀ
EXEC(਍✀䐀䔀䌀䰀䄀刀䔀 䀀倀愀最攀匀椀稀攀 椀渀琀ഀഀ
SET @PageSize = ' + @strPageSize + '਍ഀഀ
DECLARE @PK ' + @type + '਍䐀䔀䌀䰀䄀刀䔀 䀀琀戀氀倀䬀 吀䄀䈀䰀䔀 ⠀ഀഀ
            PK  ' + @type + ' NOT NULL PRIMARY KEY਍            ⤀ഀഀ
਍䐀䔀䌀䰀䄀刀䔀 倀愀最椀渀最䌀甀爀猀漀爀 䌀唀刀匀伀刀 䐀夀一䄀䴀䤀䌀 刀䔀䄀䐀开伀一䰀夀 䘀伀刀ഀഀ
SELECT '  + @PK + ' FROM ' + @Tables + @strFilter + ' ' + @strGroup + ' ORDER BY ' + @Sort + '਍ഀഀ
OPEN PagingCursor਍䘀䔀吀䌀䠀 刀䔀䰀䄀吀䤀嘀䔀 ✀ ⬀ 䀀猀琀爀匀琀愀爀琀刀漀眀 ⬀ ✀ 䘀刀伀䴀 倀愀最椀渀最䌀甀爀猀漀爀 䤀一吀伀 䀀倀䬀ഀഀ
਍匀䔀吀 一伀䌀伀唀一吀 伀一ഀഀ
਍圀䠀䤀䰀䔀 䀀倀愀最攀匀椀稀攀 㸀 　 䄀一䐀 䀀䀀䘀䔀吀䌀䠀开匀吀䄀吀唀匀 㴀 　ഀഀ
BEGIN਍            䤀一匀䔀刀吀 䀀琀戀氀倀䬀 ⠀倀䬀⤀  嘀䄀䰀唀䔀匀 ⠀䀀倀䬀⤀ഀഀ
            FETCH NEXT FROM PagingCursor INTO @PK਍            匀䔀吀 䀀倀愀最攀匀椀稀攀 㴀 䀀倀愀最攀匀椀稀攀 ⴀ ㄀ഀഀ
END਍ഀഀ
CLOSE       PagingCursor਍䐀䔀䄀䰀䰀伀䌀䄀吀䔀  倀愀最椀渀最䌀甀爀猀漀爀ഀഀ
਍匀䔀䰀䔀䌀吀 ✀ ⬀ 䀀䘀椀攀氀搀猀 ⬀ ✀ 䘀刀伀䴀 ✀ ⬀ 䀀吀愀戀氀攀猀 ⬀ ✀ 䨀伀䤀一 䀀琀戀氀倀䬀 琀戀氀倀䬀 伀一 ✀ ⬀ 䀀倀䬀 ⬀ ✀ 㴀 琀戀氀倀䬀⸀倀䬀 ✀ ⬀ 䀀猀琀爀䘀椀氀琀攀爀 ⬀ ✀ ✀ ⬀ 䀀猀琀爀䜀爀漀甀瀀 ⬀ ✀ 伀刀䐀䔀刀 䈀夀 ✀ ⬀ 䀀匀漀爀琀ഀഀ
)਍䜀伀ഀഀ
SET QUOTED_IDENTIFIER OFF ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍匀䔀吀 儀唀伀吀䔀䐀开䤀䐀䔀一吀䤀䘀䤀䔀刀 伀一 ഀഀ
GO਍匀䔀吀 䄀一匀䤀开一唀䰀䰀匀 伀一 ഀഀ
GO਍ഀഀ
CREATE PROCEDURE sp_paging_rowcount਍⠀䀀吀愀戀氀攀猀 瘀愀爀挀栀愀爀⠀㄀　　　⤀Ⰰഀഀ
@PK varchar(100),਍䀀匀漀爀琀 瘀愀爀挀栀愀爀⠀㈀　　⤀ 㴀 一唀䰀䰀Ⰰഀഀ
@PageNumber int = 1,਍䀀倀愀最攀匀椀稀攀 椀渀琀 㴀 ㄀　Ⰰഀഀ
@Fields varchar(1000) = '*',਍䀀䘀椀氀琀攀爀 瘀愀爀挀栀愀爀⠀㄀　　　⤀ 㴀 一唀䰀䰀Ⰰഀഀ
@Group varchar(1000) = NULL)਍䄀匀ഀഀ
਍⼀⨀䐀攀昀愀甀氀琀 匀漀爀琀椀渀最⨀⼀ഀഀ
IF @Sort IS NULL OR @Sort = ''਍ऀ匀䔀吀 䀀匀漀爀琀 㴀 䀀倀䬀ഀഀ
਍⼀⨀䘀椀渀搀 琀栀攀 䀀倀䬀 琀礀瀀攀⨀⼀ഀഀ
DECLARE @SortTable varchar(100)਍䐀䔀䌀䰀䄀刀䔀 䀀匀漀爀琀一愀洀攀 瘀愀爀挀栀愀爀⠀㄀　　⤀ഀഀ
DECLARE @strSortColumn varchar(200)਍䐀䔀䌀䰀䄀刀䔀 䀀漀瀀攀爀愀琀漀爀 挀栀愀爀⠀㈀⤀ഀഀ
DECLARE @type varchar(100)਍䐀䔀䌀䰀䄀刀䔀 䀀瀀爀攀挀 椀渀琀ഀഀ
DECLARE @strTemp varchar(3000)਍ഀഀ
/*Set sorting variables.*/	਍䤀䘀 䌀䠀䄀刀䤀一䐀䔀堀⠀✀䐀䔀匀䌀✀Ⰰ䀀匀漀爀琀⤀㸀　ഀഀ
	BEGIN਍ऀऀ匀䔀吀 䀀猀琀爀匀漀爀琀䌀漀氀甀洀渀 㴀 刀䔀倀䰀䄀䌀䔀⠀䀀匀漀爀琀Ⰰ ✀䐀䔀匀䌀✀Ⰰ ✀✀⤀ഀഀ
		SET @operator = '<='਍ऀ䔀一䐀ഀഀ
ELSE਍ऀ䈀䔀䜀䤀一ഀഀ
		IF CHARINDEX('ASC', @Sort) = 0਍ऀऀऀ匀䔀吀 䀀猀琀爀匀漀爀琀䌀漀氀甀洀渀 㴀 刀䔀倀䰀䄀䌀䔀⠀䀀匀漀爀琀Ⰰ ✀䄀匀䌀✀Ⰰ ✀✀⤀ഀഀ
		SET @operator = '>='਍ऀ䔀一䐀ഀഀ
਍ഀഀ
IF CHARINDEX('.', @strSortColumn) > 0਍ऀ䈀䔀䜀䤀一ഀഀ
		SET @SortTable = SUBSTRING(@strSortColumn, 0, CHARINDEX('.',@strSortColumn))਍ऀऀ匀䔀吀 䀀匀漀爀琀一愀洀攀 㴀 匀唀䈀匀吀刀䤀一䜀⠀䀀猀琀爀匀漀爀琀䌀漀氀甀洀渀Ⰰ 䌀䠀䄀刀䤀一䐀䔀堀⠀✀⸀✀Ⰰ䀀猀琀爀匀漀爀琀䌀漀氀甀洀渀⤀ ⬀ ㄀Ⰰ 䰀䔀一⠀䀀猀琀爀匀漀爀琀䌀漀氀甀洀渀⤀⤀ഀഀ
	END਍䔀䰀匀䔀ഀഀ
	BEGIN਍ऀऀ匀䔀吀 䀀匀漀爀琀吀愀戀氀攀 㴀 䀀吀愀戀氀攀猀ഀഀ
		SET @SortName = @strSortColumn਍ऀ䔀一䐀ഀഀ
਍匀䔀䰀䔀䌀吀 䀀琀礀瀀攀㴀琀⸀渀愀洀攀Ⰰ 䀀瀀爀攀挀㴀挀⸀瀀爀攀挀ഀഀ
FROM sysobjects o ਍䨀伀䤀一 猀礀猀挀漀氀甀洀渀猀 挀 漀渀 漀⸀椀搀㴀挀⸀椀搀ഀഀ
JOIN systypes t on c.xusertype=t.xusertype਍圀䠀䔀刀䔀 漀⸀渀愀洀攀 㴀 䀀匀漀爀琀吀愀戀氀攀 䄀一䐀 挀⸀渀愀洀攀 㴀 䀀匀漀爀琀一愀洀攀ഀഀ
਍䤀䘀 䌀䠀䄀刀䤀一䐀䔀堀⠀✀挀栀愀爀✀Ⰰ 䀀琀礀瀀攀⤀ 㸀 　ഀഀ
   SET @type = @type + '(' + CAST(@prec AS varchar) + ')'਍ഀഀ
DECLARE @strPageSize varchar(50)਍䐀䔀䌀䰀䄀刀䔀 䀀猀琀爀匀琀愀爀琀刀漀眀 瘀愀爀挀栀愀爀⠀㔀　⤀ഀഀ
DECLARE @strFilter varchar(1000)਍䐀䔀䌀䰀䄀刀䔀 䀀猀琀爀匀椀洀瀀氀攀䘀椀氀琀攀爀 瘀愀爀挀栀愀爀⠀㄀　　　⤀ഀഀ
DECLARE @strGroup varchar(1000)਍ഀഀ
/*Default Page Number*/਍䤀䘀 䀀倀愀最攀一甀洀戀攀爀 㰀 ㄀ഀഀ
	SET @PageNumber = 1਍ഀഀ
/*Set paging variables.*/਍匀䔀吀 䀀猀琀爀倀愀最攀匀椀稀攀 㴀 䌀䄀匀吀⠀䀀倀愀最攀匀椀稀攀 䄀匀 瘀愀爀挀栀愀爀⠀㔀　⤀⤀ഀഀ
SET @strStartRow = CAST(((@PageNumber - 1)*@PageSize + 1) AS varchar(50))਍ഀഀ
/*Set filter & group variables.*/਍䤀䘀 䀀䘀椀氀琀攀爀 䤀匀 一伀吀 一唀䰀䰀 䄀一䐀 䀀䘀椀氀琀攀爀 ℀㴀 ✀✀ഀഀ
	BEGIN਍ऀऀ匀䔀吀 䀀猀琀爀䘀椀氀琀攀爀 㴀 ✀ 圀䠀䔀刀䔀 ✀ ⬀ 䀀䘀椀氀琀攀爀 ⬀ ✀ ✀ഀഀ
		SET @strSimpleFilter = ' AND ' + @Filter + ' '਍ऀ䔀一䐀ഀഀ
ELSE਍ऀ䈀䔀䜀䤀一ഀഀ
		SET @strSimpleFilter = ''਍ऀऀ匀䔀吀 䀀猀琀爀䘀椀氀琀攀爀 㴀 ✀✀ഀഀ
	END਍䤀䘀 䀀䜀爀漀甀瀀 䤀匀 一伀吀 一唀䰀䰀 䄀一䐀 䀀䜀爀漀甀瀀 ℀㴀 ✀✀ഀഀ
	SET @strGroup = ' GROUP BY ' + @Group + ' '਍䔀䰀匀䔀ഀഀ
	SET @strGroup = ''਍ഀഀ
/*Execute dynamic query*/	਍䔀堀䔀䌀⠀ഀഀ
'਍䐀䔀䌀䰀䄀刀䔀 䀀匀漀爀琀䌀漀氀甀洀渀 ✀ ⬀ 䀀琀礀瀀攀 ⬀ ✀ഀഀ
SET ROWCOUNT ' + @strStartRow + '਍匀䔀䰀䔀䌀吀 䀀匀漀爀琀䌀漀氀甀洀渀㴀✀ ⬀ 䀀猀琀爀匀漀爀琀䌀漀氀甀洀渀 ⬀ ✀ 䘀刀伀䴀 ✀ ⬀ 䀀吀愀戀氀攀猀 ⬀ 䀀猀琀爀䘀椀氀琀攀爀 ⬀ ✀ ✀ ⬀ 䀀猀琀爀䜀爀漀甀瀀 ⬀ ✀ 伀刀䐀䔀刀 䈀夀 ✀ ⬀ 䀀匀漀爀琀 ⬀ ✀ഀഀ
SET ROWCOUNT ' + @strPageSize + '਍匀䔀䰀䔀䌀吀 ✀ ⬀ 䀀䘀椀攀氀搀猀 ⬀ ✀ 䘀刀伀䴀 ✀ ⬀ 䀀吀愀戀氀攀猀 ⬀ ✀ 圀䠀䔀刀䔀 ✀ ⬀ 䀀猀琀爀匀漀爀琀䌀漀氀甀洀渀 ⬀ 䀀漀瀀攀爀愀琀漀爀 ⬀ ✀ 䀀匀漀爀琀䌀漀氀甀洀渀 ✀ ⬀ 䀀猀琀爀匀椀洀瀀氀攀䘀椀氀琀攀爀 ⬀ ✀ ✀ ⬀ 䀀猀琀爀䜀爀漀甀瀀 ⬀ ✀ 伀刀䐀䔀刀 䈀夀 ✀ ⬀ 䀀匀漀爀琀 ⬀ ✀ഀഀ
')਍䜀伀ഀഀ
SET QUOTED_IDENTIFIER OFF ਍䜀伀ഀഀ
SET ANSI_NULLS ON ਍䜀伀ഀഀ
਍�