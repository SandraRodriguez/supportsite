<%@ page language="java" import="java.util.*,com.debisys.mb.*,com.debisys.utils.*" pageEncoding="ISO-8859-1"%>
<%	
		String path = request.getContextPath();
		
		String message = "";
		String stopUpdateAnimation = "";
		String image = "";
		String messageUpdateBiz = "";
		String deleteRowWhenOk = "";
		
		String siteId = request.getParameter("siteId");
				
		try
	    {
	    	 String appGroupId = request.getParameter("appGroupId");
	    	 String last = request.getParameter("last");	
	    	 String lang = request.getParameter("lang");
	    	 
			 Base result = MicroBrowser.updateTerminalMBApplicationGroup(siteId, appGroupId, lang);
			 messageUpdateBiz = result.getDescription();
			 
			 if ( result.getId().equals("1") )
			 {
				 message="OK";
				 image = path + "/images/green-check.png";
				 deleteRowWhenOk = "1";				 
			 }
			 else
			 {
				 message="Fail";
				 image = path + "/images/icn-redx1.gif";
				 deleteRowWhenOk = "sitesBadArray.push( '"+siteId+"--"+messageUpdateBiz+"--"+image+"' );";
				 //System.out.println("******** "+deleteRowWhenOk);
			 }			 
			 if ( last != null && last.equals("true") )
			 {
				 stopUpdateAnimation = " $('#messages').html(\"\"); showReport(1); ";				 
			 }
		}		    
		catch (Exception e)
		{
			 message="Bad";
			 image = path + "/images/icn-redx1.gif";
		}  		    	
		finally
		{
			//out.write(message);
		}
%>
<script type="text/javascript">
	<%=deleteRowWhenOk%>
	<%=stopUpdateAnimation%>	
</script>
