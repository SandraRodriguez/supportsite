<%@ page import="java.util.*,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.mb.*" 
%>
<%
	int section=9;
	int section_page=26;
%>
	<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
	<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
	<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ 
	include file="/includes/security.jsp" 
%>
<%
	String path = request.getContextPath();
	String strIsoID = SessionData.getProperty("ref_id");
	String strAgentID = "-2";
	String strSubAgentID = "-2";
	String strRepID = "-2";
	String strMerchantID = "-2";
	String strStartDate = "";
	String strEndDate = "";	
	//int intACHRange = DebisysConfigListener.getACHRange(application) - 1;
	ArrayList<ApplicationsGroups> arrApplications = MicroBrowser.findApplicationsGroups();
	
	if (request.getParameter("submitted") != null)
	{
		try
	    {
		    if (request.getParameter("submitted").equals("y"))
    		{		    	
		    	if (request.getParameter("AgentID") != null)
		    	{
		    		strAgentID = request.getParameter("AgentID");
		    	}
		    	if (request.getParameter("SubAgentID") != null)
		    	{
		    		strSubAgentID = request.getParameter("SubAgentID");
		    	}
		    	if (request.getParameter("RepID") != null)
		    	{
		    		strRepID = request.getParameter("RepID");
		    	}
		    	if (request.getParameter("MerchantID") != null)
		    	{
		    		strMerchantID = request.getParameter("MerchantID");
		    	}
		    	if (request.getParameter("StartDate") != null)
		    	{
		    		strStartDate = request.getParameter("StartDate");
		    	}
		    	if (request.getParameter("EndDate") != null)
		    	{
		    		strEndDate = request.getParameter("EndDate");
		    	}		    	
		    }
		    
  		}
		catch (Exception e){}  
	}	
	
	String labelProcessing = Languages.getString("jsp.tools.datatable.processing",SessionData.getLanguage());
	String labelShowRecords = Languages.getString("jsp.tools.datatable.showRecords",SessionData.getLanguage());
	String labelNoRecords = Languages.getString("jsp.tools.datatable.norecords",SessionData.getLanguage());
	String labelNoDataAvailable = Languages.getString("jsp.tools.datatable.noDataAvailable",SessionData.getLanguage());
	String labelInfo = Languages.getString("jsp.tools.datatable.info",SessionData.getLanguage());
	String labelInfoEmpty = Languages.getString("jsp.tools.datatable.infoEmpty",SessionData.getLanguage());
	String labelFilter = Languages.getString("jsp.tools.datatable.filter",SessionData.getLanguage());
	String labelSearch = Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage());
	String labelLoading = Languages.getString("jsp.tools.datatable.loading",SessionData.getLanguage());
	String labelFirst = Languages.getString("jsp.tools.datatable.first",SessionData.getLanguage());
	String labelLast = Languages.getString("jsp.tools.datatable.last",SessionData.getLanguage());
	String labelNext = Languages.getString("jsp.tools.datatable.next",SessionData.getLanguage());
	String labelPrevious = Languages.getString("jsp.tools.datatable.previous",SessionData.getLanguage());
	String labelShow = Languages.getString("jsp.tools.datatable.show",SessionData.getLanguage());
	String labelRecords = Languages.getString("jsp.tools.datatable.records",SessionData.getLanguage());
		    		
%>
<%@include file="/includes/header.jsp" 
%>

		<style type="text/css" title="currentStyle">
			@import "<%=path%>/includes/media/demo_page.css";
			@import "<%=path%>/includes/media/demo_table.css";
		</style>
		
		<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			var sitesBadArray = [];
			$(document).ready(function() 
				{				
					reloadTableTerminals();					
				}
			);
			
			function reloadTableTerminals()
			{
				$('#mbTerminalsUpdate').dataTable( {
					"iDisplayLength": 10,
					"bLengthChange": true,
					"bFilter": true,
					"sPaginationType": "full_numbers",
					
					"oLanguage": {
				           "sProcessing":      "<%=labelProcessing%>",
						    "sLengthMenu":     "<%=labelShowRecords%>",
						    "sZeroRecords":    "<%=labelNoRecords%>",
						    "sEmptyTable":     "<%=labelNoDataAvailable%>",
						    "sInfo":           "<%=labelInfo%>",
						    "sInfoEmpty":      "<%=labelInfoEmpty%>",
						    "sInfoFiltered":   "<%=labelFilter%>",
						    "sInfoPostFix":    "",
						    "sSearch":         "<%=labelSearch%>",
						    "sUrl":            "",
						    "sInfoThousands":  ",",
						    "sLoadingRecords": "<%=labelLoading%>",
						    "oPaginate": {
						        "sFirst":    "<%=labelFirst%>",
						        "sLast":     "<%=labelLast%>",
						        "sNext":     "<%=labelNext%>",
						        "sPrevious": "<%=labelPrevious%>"
						    },
						    "oAria": {
						        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
						    },
						    "sLengthMenu": ' <%=labelShow%> <select id="pagesNumbers" >'+
									        '<option value="10">10</option>'+
									        '<option value="20">20</option>'+
									        '<option value="30">30</option>'+
									        '<option value="40">40</option>'+
									        '<option value="50">50</option>'+
									        '<option value="-1">All</option>'+
									        '</select> <%=labelRecords%>'
				         },
				     "aaSorting": [[ 0, "asc" ]]     
				}				
				);
			}
		</script>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.frmMain.showRep.disabled = true;
  document.frmMain.showRep.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}			
</SCRIPT>

<table id="tb1" border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    	<td width="18" height="20" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	    <td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.tools.dtu143.masterTitle",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20" align="right"><img src="images/top_right_blue.gif"></td>
		</tr>
	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table id="tb2" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
			  <tr>
			    <td>
	    			<form name="frmMain" id="frmMain" method="post" action="/support/admin/mb/updatemb.jsp" onSubmit="scroll();">
	    			<input type="hidden" id="submitted" name="submitted" value="y">	
					<input type="hidden" id="IsoID" name="IsoID" value="<%=strIsoID%>">
					<input type="hidden" id="AgentID" name="AgentID" value="<%=strAgentID%>">							
					<input type="hidden" id="SubAgentID" name="SubAgentID" value="<%=strSubAgentID%>">							
					<input type="hidden" id="RepID" name="RepID" value="<%=strRepID%>">
					<input type="hidden" id="MerchantID" name="MerchantID" value="<%=strMerchantID%>">	
											
      					<table id="tb3" border="0" width="100%" cellpadding="0" cellspacing="0">
     						<tr>
	        					<td class="formArea2">
	          						<table  id="tb4" width="300">
               							<tr>
               								<td valign="top" nowrap="nowrap">
												<table id="tb5" width="400">	                            		    			 
	   	    	                	        			<% 
	                            		    		    if( strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) )
	                            		    		   	{
	                            		    		   	%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.agent",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="agentList" name="agentList" onchange="getSubAgents();">
				            	    	                    	    <%
            					    	                    	    	Vector vecAgentList = TransactionSearch.getAgentList(SessionData, strIsoID);
    	    	        	    				                        Iterator itAL = vecAgentList.iterator();
            	    	                    					    	String strSelectedAL = "";
        	    	    	                    	    				//out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
			                	                	                	if(strAgentID != "")
            					    	                    	    	{
           	        	    				    	                		if(strAgentID.equals("-1"))
               		    	                				    	    	{
           	        	    				    	                			strSelectedAL = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedAL = "";
			                	                	                		}
			                	    	                    	    	} 
			    	    	                	                		else
        					                	                		{
			    	    	                	                			strSelectedAL = "";
        	                	            				    		} 
        	    	    	                    	    				out.println("<option " + strSelectedAL + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
				            	    	                    	    	while (itAL.hasNext())
	        					                	                    {
                	            				    	                	Vector vecTempAL = null;
                	            				    	                	vecTempAL = (Vector) itAL.next();
				                	                	                	if(strAgentID != "")
                					    	                    	    	{
               	        	    				    	                		if(strAgentID.equals(vecTempAL.get(0)))
                   		    	                				    	    	{
               	        	    				    	                			strSelectedAL = "selected";
				               	    	    	                    	    	}
           	    					                	                		else
				       	        	                	                		{
           	    					                	                			strSelectedAL = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedAL = "";
            	                	            				    		}                	                	                	
				                        	                		    	out.println("<option " + strSelectedAL + " value=" + vecTempAL.get(0) +">" + vecTempAL.get(1) + "</option>");
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
    	                    	    	    				<% 
	                            		    		   			}
	                            		    		   		%>	                            		    		   			    	                	        	        			
            	    		    	            				<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if( strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) )
            		                            		    	    {
            	    		    	            					%>
	            	    		        	        					function getSubAgents()
	                                        	            			{
		                                        	            			var agentOption = document.getElementById('agentList').value ;
		                                        	            			document.getElementById('AgentID').value = agentOption;
		                                        	            			//if ( agentOption =="-1"){
		                                        	            			  document.getElementById('SubAgentID').value = "-1"
		                                        	            			  document.getElementById('RepID').value = "-1"
		                                        	            			  document.getElementById('MerchantID').value = "-1"
		                                        	            			//}
		                                        	            			document.frmMain.submit();
	                                                     				}
            	    		    	            					<%
            		                            		    	    }
            	    		    	            					%>                                                   				
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		 
	   	    	                	        		<% 
	                            		    		   
	                            		    		   	if ( strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) )
	                            		    		   	{
	                            		    		   	%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.subagent",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="subAgentList" name="subAgentList" onchange="getReps();">
				            	    	                    	    <%
            					    	                    	    	Vector vecSubAgentList = null;
            					    	                    	    	if ( !strAgentID.equals("-2") && !strAgentID.equals("-1") )
            					    	                    	    		vecSubAgentList = TransactionSearch.getSubAgentList(SessionData, strAgentID);
            					    	                    	    		
            					    	                    	    	if ( vecSubAgentList!=null )	
            					    	                    	    	{
	    	    	        	    				                        Iterator itSAL = vecSubAgentList.iterator();
	            	    	                    					    	String strSelectedSAL = "";
	        	    	    	                    	    				if(strSubAgentID != "")
	            					    	                    	    	{
	           	        	    				    	                		if(strSubAgentID.equals("-1"))
	               		    	                				    	    	{
	           	        	    				    	                			strSelectedSAL = "selected";
				               	    	    	                    	    	}
	       	    					                	                		else
				       	        	                	                		{
	       	    					                	                			strSelectedSAL = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedSAL = "";
	        	                	            				    		} 
	        	    	    	                    	    				out.println("<option " + strSelectedSAL + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
					            	    	                    	    	while (itSAL.hasNext())
		        					                	                    {
	                	            				    	                	Vector vecTempSAL = null;
	                	            				    	                	vecTempSAL = (Vector) itSAL.next();
					                	                	                	if(strSubAgentID != "")
	                					    	                    	    	{
	               	        	    				    	                		if(strSubAgentID.equals(vecTempSAL.get(0)))
	                   		    	                				    	    	{
	               	        	    				    	                			strSelectedSAL = "selected";
					               	    	    	                    	    	}
	           	    					                	                		else
					       	        	                	                		{
	           	    					                	                			strSelectedSAL = "";
					                	                	                		}
					                	    	                    	    	} 
					    	    	                	                		else
		        					                	                		{
					    	    	                	                			strSelectedSAL = "";
	            	                	            				    		}                	                	                	
					                        	                		    	out.println("<option " + strSelectedSAL + " value=" + vecTempSAL.get(0) +">" + vecTempSAL.get(1) + "</option>");
	                				            	        	    		}
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
    	                    	    	    				<% 
	                            		    		   				}
	                            		    		   			
	                            		    		   		%>	                            		    		   			    	                	        	        			
            	    		    	            				<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if( strAccessLevel.equals(DebisysConstants.ISO) && strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) )
            		                            		    	    {
	            	    		    	            				%>            	    		    	            				
		            	    		        	        					function getReps()
		                                        	            			{
		                                        	            				var subagentOption = document.getElementById('subAgentList').value
			                                        	            			document.getElementById('SubAgentID').value = subagentOption ;
			                                        	            			if ( subagentOption =="-1"){
			                                        	            			  document.getElementById('RepID').value = "-1"
			                                        	            			  document.getElementById('MerchantID').value = "-1"
			                                        	            			}
			                                        	            			document.frmMain.submit();
		                                                     				}
	            	    		    	            				<%
            		                            		    	    }
            	    		    	            					%>
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		       	                	        			        	                	        			      	                	        			
	   	    	                	        		<% 
	   	    	                	        		if ( strAccessLevel.equals(DebisysConstants.ISO) )
                          		    		   		{
	                            		    		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.rep",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="repList" name="repList" onchange="getMerchants();">
				            	    	                    	    <%
            					    	                    	    	Vector vecRepList = null;
				            	    	                    	    	if(strAccessLevel.equals(DebisysConstants.ISO))
				            	    	                    	    	{
				            	    	                    	    		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				            	    	                    	    		{
				            	    	                    	    			if ( !strSubAgentID.equals("-2") && !strSubAgentID.equals("-1") )
				            	    	                    	    				vecRepList = TransactionSearch.getRepList(SessionData, strSubAgentID);
				            	    	                    	    		}
				            	    	                    	    		else
				            	    	                    	    		{
				            	    	                    	    			vecRepList = TransactionSearch.getRepList(SessionData, strIsoID);
				            	    	                    	    		}
				            	    	                    	    	}
				            	    	                    	    	else
			            	    	                    	    		{
				            	    	                    	    		vecRepList = TransactionSearch.getRepList(SessionData, strSubAgentID);
			            	    	                    	    		}
			            	    	                    	    		
			            	    	                    	    		if ( vecRepList != null)
			            	    	                    	    		{
	    	    	        	    				                        Iterator itRL = vecRepList.iterator();
	            	    	                    					    	String strSelectedRL = "";
	        	    	    	                    	    				if(strRepID != "")
	            					    	                    	    	{
	           	        	    				    	                		if(strRepID.equals("-1"))
	               		    	                				    	    	{
	           	        	    				    	                			strSelectedRL = "selected";
				               	    	    	                    	    	}
	       	    					                	                		else
				       	        	                	                		{
	       	    					                	                			strSelectedRL = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedRL = "";
	        	                	            				    		} 
	        	    	    	                    	    				out.println("<option " + strSelectedRL + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
					            	    	                    	    	while (itRL.hasNext())
		        					                	                    {
	                	            				    	                	Vector vecTempRL = null;
	                	            				    	                	vecTempRL = (Vector) itRL.next();
					                	                	                	if(strRepID != "")
	                					    	                    	    	{
	               	        	    				    	                		if(strRepID.equals(vecTempRL.get(0)))
	                   		    	                				    	    	{
	               	        	    				    	                			strSelectedRL = "selected";
					               	    	    	                    	    	}
	           	    					                	                		else
					       	        	                	                		{
	           	    					                	                			strSelectedRL = "";
					                	                	                		}
					                	    	                    	    	} 
					    	    	                	                		else
		        					                	                		{
					    	    	                	                			strSelectedRL = "";
	            	                	            				    		}                	                	                	
					                        	                		    	out.println("<option " + strSelectedRL + " value=" + vecTempRL.get(0) +">" + vecTempRL.get(1) + "</option>");
	                				            	        	    		}
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
    	                    	    	    				<% 
	                            		    		   		}	                            		    		   			
	                            		    		   		%>	                            		    		   			    	                	        	        			
            	    		    	            				<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
            		                            		    	    {
            	                            		    		   		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            	                            		    		   		{
            	    		    	            							%>            	    		    	            				
		            	    		        	        					function getMerchants()
		                                        	            			{
			                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
			                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
			                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
			                                        	            			document.frmMain.submit();
		                                                     				}
            	    		    	            							<%
            	                            		    		   		}
            	                            		    		   		else
            	                            		    		   		{
		            	                            		    		   	%>
		            	    		        	        					function getMerchants()
		                                        	            			{
			                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
			                                        	            			document.frmMain.submit();
		                                                     				}            	                            		    		   	
		            	                            		    		   	<%
            	                            		    		   		}
            		                            		    	    }            	                            		    		   	
            	    		    	            					%>
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		  
	   	    	                	        		<% 
	   	    	                	        		if ( strAccessLevel.equals(DebisysConstants.ISO) )                         		    		   						
                         		    		   		{
	                            		    		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.merchant",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="merchantList" name="merchantList" onchange="showReport(0);">
				            	    	                    	    <%
            					    	                    	    	Vector vecMerchantList = null;
            					    	                    	    	
            					    	                    	    	if ( !strRepID.equals("-2") && !strRepID.equals("-1") )
            					    	                    	    		vecMerchantList = TransactionSearch.getMerchantList(SessionData, strRepID);
    	    	        	    				                        
    	    	        	    				                        if ( vecMerchantList != null)
    	    	        	    				                        {
	    	    	        	    				                        Iterator itML = vecMerchantList.iterator();
	            	    	                    					    	String strSelectedML = "";
	        	    	    	                    	    				if(strMerchantID != "")
	            					    	                    	    	{
	           	        	    				    	                		if(strMerchantID.equals("-1"))
	               		    	                				    	    	{
	           	        	    				    	                			strSelectedML = "selected";
				               	    	    	                    	    	}
	       	    					                	                		else
				       	        	                	                		{
	       	    					                	                			strSelectedML = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedML = "";
	        	                	            				    		} 
	        	    	    	                    	    				out.println("<option " + strSelectedML + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
					            	    	                    	    	while (itML.hasNext())
		        					                	                    {
	                	            				    	                	Vector vecTempML = null;
	                	            				    	                	vecTempML = (Vector) itML.next();
					                	                	                	if(strMerchantID != "")
	                					    	                    	    	{
	               	        	    				    	                		if(strMerchantID.equals(vecTempML.get(0)))
	                   		    	                				    	    	{
	               	        	    				    	                			strSelectedML = "selected";
					               	    	    	                    	    	}
	           	    					                	                		else
					       	        	                	                		{
	           	    					                	                			strSelectedML = "";
					                	                	                		}
					                	    	                    	    	} 
					    	    	                	                		else
		        					                	                		{
					    	    	                	                			strSelectedML = "";
	            	                	            				    		}                	                	                	
					                        	                		    	out.println("<option " + strSelectedML + " value=" + vecTempML.get(0) +">" + vecTempML.get(1) + "</option>");
	                				            	        	    		}
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
  	                    	    	    				<% 
                           		    		   			}
                           		    		   			%>	                            		    		   			    	                	        	        			
          	    		    	            				<SCRIPT>
          	    		    	            					<%
          	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
          		                            		    	    {
          	                            		    		   		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
          	                            		    		   		{
            	    		    	            					%>            	    		    	            				
            	    		        	        					function saveInfo()
                                        	            			{
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.frmMain.submit();
                                                     				}
                                                     				
            	    		    	            					<%
          		                            		    	    	}
          	                            		    		   		else
          	                            		    		   		{
            	                            		    		   	%>
            	    		        	        					function saveInfo()
                                        	            			{			                                        	            			
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.frmMain.submit();
                                                     				}  
                                                     				          	                            		    		   	
            	                            		    		   	<%
          	                            		    		   		}
          		                            		    	    }
          	    		    	            					%>
                                              				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		<tr class="main">
	   	    	                	        			<td>
	   	    	                	        				<%=Languages.getString("jsp.tools.dtu143.currentVersion",SessionData.getLanguage())%>
	   	    	                	        			</td>
	   	    	                	        			<td>
	   	    	                	        				<select id="appGroups">
	   	    	                	        					<% 
   	    	                	        						for (ApplicationsGroups appGroup : arrApplications)  
   	    	                	        						{
   	    	                	        							%>
   	    	                	        							<option value="<%=appGroup.getAplicationGroupId()%>">(<%=appGroup.getAplicationGroupId()%>)/<%=appGroup.getDescription()%></option>
   	    	                	        							<% 
   	    	                	        						}
   	    	                	        						%> 
	   	    	                	        				</select>	 
	   	    	                	        			</td>
	   	    	                	        		</tr>   
	   	    	                	        		     	                	        			        	                	        			      	                	        			        	                	        			        	                	        			      	                	        			        	                	        			      	                	        			        	                	        			
			              							  
													<tr class="main">
    													<td colspan="4">
      														<input type="hidden" name="search" id="search" value="y">
									         				<input type="button" name="showRep" id="showRep" 
									         					   value="<%=Languages.getString("jsp.tools.dtu143.searchTerminals.submit",SessionData.getLanguage())%>" 
									         					   onclick="showReport(0);">
			            	    		                	<SCRIPT>
            	    		    	            					<%
            	    		    	            					String errorLoading = Languages.getString("jsp.tools.dtu143.errorLoading",SessionData.getLanguage());
            	    		    	            					String waitPlease = Languages.getString("jsp.tools.dtu143.waitPlease",SessionData.getLanguage());
            	    		    	            					
            	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
            		                            		    	    {
            	                            		    		   		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            	                            		    		   		{
		            	    		    	            					%>            	    		    	            				
		            	    		        	        					function showReport(validating)
		                                        	            			{			                                        	            			
			                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
			                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
			                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
		                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
		                                                    	    			
		                                                    	    			var agent = document.getElementById('agentList').value ;
			                                        	            			var sub = document.getElementById('subAgentList').value ;
			                                        	            			var rep = document.getElementById('repList').value ;
		                                                    	    			var merchant = document.getElementById('merchantList').value;
		                                                    	    			var current = document.getElementById('appGroups').value;
		                                                    	    			
		                                                    	    			
		                                                    	    			var sParams = "&level="+<%=DebisysConstants.DIST_CHAIN_5_LEVEL%>+"&agent="+agent+"&";
		                                                    	    			sParams += "sub="+sub+"&";
		                                                    	    			sParams += "rep="+rep+"&";
		                                                    	    			sParams += "merchant="+merchant+"&";
		                                                    	    			sParams += "current="+current+"&";
		                                                    	    			sParams += "validate="+validating;
		                                                    	    					//alert(sParams);	                                        	            			
		                                                    	    			var requestTable = 'admin/mb/searchdata.jsp?Random=' + Math.random() + sParams;
		                                                    	    			if ( validating=="0")
		                                                    	    			{
		                                                    	    				sitesBadArray = [];
		                                                    	    				$('#messages').html("");
		                                                    	    			}
		                                                    	    			$('#divData').show();
		                                                    	    			$('#divData').html("<%=waitPlease%>");
		                                                    	    			
		                                                    	    			$("#divData").load(requestTable,  function( response, status, xhr ) {
																				  if ( status == "error" ) {
																				    var msg = "<%=errorLoading%>";
																				    $( "#divData" ).html( msg + xhr.status + " " + xhr.statusText );
																				  }
																				});
		                                                     				}
		            	    		    	            					<%
            		                            		    	    	}
            	                            		    		   		else
            	                            		    		   		{
		            	                            		    		   	%>
		            	    		        	        					function showReport(validating)
		                                        	            			{
			                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
		                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
			                                        	            						                                        	            			
		                                                    	    			var current = document.getElementById('appGroups').value;
		                                                    	    			
		                                                    	    			var rep = document.getElementById('repList').value ;
		                                                    	    			var merchant = document.getElementById('merchantList').value ;
		                                                    	    			var sParams = "&level="+<%=DebisysConstants.DIST_CHAIN_3_LEVEL%>+"&rep="+rep+"&";
		                                                    	    			sParams += "merchant="+merchant+"&";
		                                                    	    			sParams += "current="+current+"&";
		                                                    	    			sParams += "validate="+validating;
		                                                    	    			
		                                                    	    			var requestTable = 'admin/mb/searchdata.jsp?Random=' + Math.random() + sParams;
		                                                    	    			if ( validating=="0")
		                                                    	    			{
		                                                    	    				sitesBadArray = [];
		                                                    	    				$('#messages').html("");
		                                                    	    			}		                                                    	    			
		                                                    	    					
		                                                    	    			$('#divData').show();
		                                                    	    			$('#divData').html("<%=waitPlease%>");
		                                                    	    			
		                                                    	    			$("#divData").load(requestTable,  function( response, status, xhr ) {
																				  if ( status == "error" ) {
																				    var msg = "<%=errorLoading%>";
																				    $( "#divData" ).html( msg + xhr.status + " " + xhr.statusText );
																				  }
																				});
		                                                     				}
		            	                            		    		   	<%
            	                            		    		   		}
            		                            		    	    }
            	    		    	            					%>
                                                			</SCRIPT>										         				
    													</td>														
													</tr>
													<tr>
												 		<td colspan="4" align="center" style="height: 30px">
															<span id="messages" class="messagesNormal"></span>
														</td>
												 	</tr> 
													<tr>
														<td colspan="4">
															<div id="divData" >
															</div>																	
														</td>
													</tr>
												</table>
		               						</td>
              							</tr>
            						</table>
            					</td>
            				</tr>	
            			</table>
            		 </form>
            	  </td>
               </tr>	
           </table>
		</td>
	</tr>
</table>
	<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
	</iframe>
<%@ 
	include file="/includes/footer.jsp" 
%>