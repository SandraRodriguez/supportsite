<%@ page language="java" import="java.util.*,com.debisys.mb.*,
							     com.debisys.languages.Languages" pageEncoding="ISO-8859-1"%>
							     
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
 String path = request.getContextPath();
 String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
 
 String language = SessionData.getLanguage();
 String agent = request.getParameter("agent");
 String sub = request.getParameter("sub");
 String rep = request.getParameter("rep");
 String merchant = request.getParameter("merchant");
 String level = request.getParameter("level");    
 String currenAppGroup = request.getParameter("current");
 String validate = request.getParameter("validate");
 
 String labelChangeToVersion = Languages.getString("jsp.tools.dtu143.changeToVersion", language);
 String labelMerchantId = Languages.getString("jsp.tools.dtu143.merchantId", language);
 String labelMerchantDBA = Languages.getString("jsp.tools.dtu143.merchantDBA", language);
 String labelSiteId = Languages.getString("jsp.tools.dtu143.siteId", language);
 String labelResult = Languages.getString("jsp.tools.dtu143.result", language);
 String labelSelectAll = Languages.getString("jsp.tools.dtu143.selectAll", language);
 String labelUnselectAll = Languages.getString("jsp.tools.dtu143.UnselectAll", language);
 String doNotClose = Languages.getString("jsp.tools.dtu143.doNotClose", language);
 String taskFinished = Languages.getString("jsp.tools.dtu143.taskFinished", language);
 String errorLoading = Languages.getString("jsp.tools.dtu143.errorLoading", language);  
 String selectAtLeastOne = Languages.getString("jsp.tools.dtu143.warnningAtLeastOneSelected", language);
 
 String message = "";
 String callFunctionJavaScript = "";
   
 ArrayList<ApplicationsGroups> arrApplications = MicroBrowser.findApplicationsGroups();
 ArrayList<TerminalsMB> arrTerminals = MicroBrowser.findTerminals(level,agent,sub,rep,merchant,currenAppGroup);
   
 int sizearr = arrTerminals.size();
   
 if ( sizearr > 0 && validate.equals("1") )
 {
 	message = Languages.getString("jsp.tools.dtu143.errorAfterUpdate", language);
 	callFunctionJavaScript = "searchBadSites(); "; 
 }
 else if ( sizearr == 0 && validate.equals("1") )
 {
 	message = Languages.getString("jsp.tools.dtu143.sucessfullAfterUpdate", language);
 }				 
 				 
%>

<table cellpadding="0" cellspacing="0" border="0" >
	<tr class="main">
		<td>
			<%=labelChangeToVersion%>
		</td>
		<td>
			<select id="newAppGroups">
				<% 
				for (ApplicationsGroups appGroup : arrApplications)  
				{
					if ( !appGroup.getAplicationGroupId().equals(currenAppGroup) )
					{
					%>
					<option value="<%=appGroup.getAplicationGroupId()%>">(<%=appGroup.getAplicationGroupId()%>)/<%=appGroup.getDescription()%></option>
					<%
					} 
				}
				%> 
			</select>	 
		</td>
 	</tr>  	
 	<tr>
 		<td colspan="2" align="right">
			<input type="button" value="Update " onclick="loopTerminalsList()"/>
		</td>
 	</tr>
 	<tr>
 		<td colspan="2">
 			<table cellpadding="0" cellspacing="0" border="0" class="display" id="mbTerminalsUpdate">
				<thead>
					<tr class="rowhead2">
						<th><%=labelMerchantId%></th>
						<th><%=labelMerchantDBA%></th>
						<th><%=labelSiteId%></th>
						<th><%=labelResult%></th>
						<th><span id="spanSelect"><%=labelSelectAll%></span>
						 	<input type="checkbox" id="unselect" onclick="unselectAll(this);">
						</th>
					</tr>
				</thead>
			 	<tbody>
				 <%			 
				 for(TerminalsMB terminal : arrTerminals )
			     {    
			     	String terminalId = terminal.getSiteId();
			     %>
				 <tr id="tr_<%=terminalId%>">
					<td><%=terminal.getMerchantId()%></td>
					<td><%=terminal.getMerchantDba()%></td>
					<td><%=terminalId%></td>
					<td>
						<div id="div_Site_<%=terminalId%>" >
														
						</div>						
					</td>
					<td><input type="checkbox"   id="chk_Site_<%=terminalId%>" /></td>
				 </tr>
				 <%				 				 		
				 }
			   	 %>
			    </tbody>
			   	<!--<tfoot><tr><th>Merchant Id</th><th>Merchant DBA</th><th>Site Id</th><th>Result</th><th>Action</th></tr></tfoot>-->			   	
			</table>
 		</td>
 	</tr>
</table>

 
<script type="text/javascript">
	reloadTableTerminals();
	$('#messages').html("<%=message%>");
	<%=callFunctionJavaScript%>					
</script>
		
<script type="text/javascript">		
	function searchBadSites()
	{	
		if ( sitesBadArray != null)
        {
			$('#pagesNumbers').val("-1");
			$('#pagesNumbers').change();
			var oTable = $('#mbTerminalsUpdate').dataTable();
			var nNodes = oTable.fnGetNodes( );
			var pages = nNodes.length;
			
			$('#mbTerminalsUpdate tbody tr ').each(function()
	  		{
            	var nameControl = $(this).attr( 'id' );
            	var strs = nameControl.split("_");		                
                var siteId = strs[1];
            	
            	for ( var i = 0; i < sitesBadArray.length; i = i + 1 ) 
            	{
            		var strs = sitesBadArray[ i ].split("--")
            		if ( strs[ 0 ] == siteId )
            		{
            			var imgControl = "<img title='"+strs[ 1 ]+"' src='"+strs[ 2 ]+"' />";
            			$("#div_Site_"+siteId).html(imgControl);
            		}
				}
	        });
	        sitesBadArray = [];
	        $('#pagesNumbers').val("10");
			$('#pagesNumbers').change();
        }
						
	}		
</script>
 
<script type="text/javascript">	
	function unselectAll(control)
	{
	    var actionSelect = false;
		if ($('#unselect').is(':checked'))
		{
			actionSelect = true
			$('#spanSelect').html("<%=labelUnselectAll%>");
		}
		else
		{
			actionSelect = false;
			$('#spanSelect').html("<%=labelSelectAll%>");
		}	
		$('#pagesNumbers').val("-1");
		$('#pagesNumbers').change();
		$('#mbTerminalsUpdate tbody input[type=checkbox]').attr('checked',actionSelect);
		$('#pagesNumbers').val("10");
		$('#pagesNumbers').change();	
		//$('#example thead input[type=checkbox]').attr('checked',false);	
		
	}
</script>
	
<script type="text/javascript">	 				
	function loopTerminalsList()
	{
	    var example_length = $('#pagesNumbers').val();
		var newApplicationGroup = $('#newAppGroups').val();
		var oTable = $('#mbTerminalsUpdate').dataTable();
		
		oTable.fnPageChange( 0 );
				
  		var nNodes = oTable.fnGetNodes( );
  		var pages = nNodes.length / example_length;
  		var pageAfterFilter = oTable.fnSettings().fnRecordsDisplay();
  		var chCount = 0;
  		var lastSiteIdTmp="";		
  		$('#messages').html("<%=doNotClose%>");
  			
  		var someOneSelected = false;	
 		for ( var j = 0; j < pages; j++ ) 
  		{
	  		$('#mbTerminalsUpdate tbody tr td input[type="checkbox"]').each(function()
	  		{
	            if ($(this).is(':checked'))
	            {
	            	var nameControl1 = $(this).attr( 'id' );
	            	var strs1 = nameControl1.split("_");		                
	                var siteId1 = strs1[2];
	            	lastSiteIdTmp = siteId1;
	            	someOneSelected = true;
	            }
	        });
	        oTable.fnPageChange( 'next' ); 
	    }    
		oTable.fnPageChange( 0 );
		if ( someOneSelected )
		{	     
	  		for ( var i = 0; i < pages; i++ ) 
		  	{
		  		$('#mbTerminalsUpdate tbody tr td input[type="checkbox"]').each(function()
		  		{
		            if ($(this).is(':checked'))
		            {
		                var nameControl = $(this).attr( 'id' );
		                var strs = nameControl.split("_");		                
		                var siteId = strs[2];
		                var additionalFlagLastSiteId="";
		                if ( lastSiteIdTmp == siteId )
		                {		                	
		                	additionalFlagLastSiteId="&last=true";
		                }
		                else
		                {		                	
		                	additionalFlagLastSiteId="&last=false";
		                }
		                var requestURL = "admin/mb/mbbulk.jsp?Random=" + Math.random()+"&siteId="+siteId+"&appGroupId="+newApplicationGroup+additionalFlagLastSiteId+"&lang=<%=language%>";
		                $( "#div_Site_"+siteId ).load( requestURL, function( response, status, xhr ) 
		                	{
							  if ( status == "error" )
							  {
							    var msg = "<%=errorLoading%>";
							    $( "#div_Site_"+siteId ).html( msg + xhr.status + " " + xhr.statusText );
							  }								  
						     }
						  );
		            }
		        });		        
		        oTable.fnPageChange( 'next' ); 
			 }		    
	     }
	     else
	     {
	     	$('#messages').html("<%=selectAtLeastOne%>");
	     }
	}
 </script>