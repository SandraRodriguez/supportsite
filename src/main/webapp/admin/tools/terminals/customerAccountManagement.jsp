<%--
    Document   : Chat
    Created on : Dec 21, 2017, 11:38:45 AM
    Author     : andres diaz
--%>
<%@page import="com.debisys.tools.terminals.TerminalPermissions"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.reports.TransactionReport" %>

<%
	boolean isDomestic = Boolean.parseBoolean(request.getParameter("domestic"));
    int section = 1;
    int section_page = 1;
    String permissionCode = TerminalPermissions.CUSTOMER_ACCOUNT_MANAGEMENT;
    String resourceByPerm = isDomestic ? "jsp.tools.dtu5399." + DebisysConstants.PERM_ALLOW_USE_DOMESTIC_CUSTOMER_ACCOUNT_MANAGEMENT 
    		: "jsp.tools.dtu5228."+DebisysConstants.PERM_ALLOW_USE_CUSTOMER_ACCOUNT_MANAGEMENT;
    String pageReturn = "customerAccountManagement.jsp";

%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%@ include file="/includes/security.jsp"%>
<%@include file="/includes/header.jsp"%>

<jsp:include page="permissionconfiguration.jsp" flush="true" >
    <jsp:param name="permissionCode" value="<%=permissionCode%>" />
    <jsp:param name="resourceByPerm" value="<%=resourceByPerm%>" />
    <jsp:param name="pageReturn" value="<%=pageReturn%>" />
    <jsp:param name="section" value="<%=section%>" />
    <jsp:param name="section_page" value="<%=section_page%>" />
</jsp:include>

<%@ include file="/includes/footer.jsp"%>