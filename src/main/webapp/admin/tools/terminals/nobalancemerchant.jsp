<%-- 
    Document   : nobalancemerchant
    Created on : Apr 8, 2016, 4:39:47 PM
    Author     : nmartinez
--%>
<%@page import="com.debisys.tools.terminals.TerminalPermissions"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.reports.TransactionReport" %>

<%
    int section = 9;
    int section_page = 33;
    String permissionCode = TerminalPermissions.NO_BALANCE_MERCHANT;
    String resourceByPerm = "jsp.tools.title."+DebisysConstants.PERM_ALLOW_NO_BALANCE_MERCHANT;
    String pageReturn = "nobalancemerchant.jsp";
    
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%@ include file="/includes/security.jsp"%>
<%@include file="/includes/header.jsp"%>

<jsp:include page="permissionconfiguration.jsp" flush="true" >
    <jsp:param name="permissionCode" value="<%=permissionCode%>" />
    <jsp:param name="resourceByPerm" value="<%=resourceByPerm%>" />
    <jsp:param name="pageReturn" value="<%=pageReturn%>" />  
    <jsp:param name="section" value="<%=section%>" /> 
    <jsp:param name="section_page" value="<%=section_page%>" />
</jsp:include>

<%@ include file="/includes/footer.jsp"%>

