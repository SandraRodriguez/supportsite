<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*" %>


<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%        
    String idNew = request.getParameter("idNew"); 
    String userName = SessionData.getProperty("username");
    String entityId = request.getParameter("entityId");
    String parentId = request.getParameter("parentId");
    
    String applicationName = DebisysConfigListener.getInstance( this.getServletContext() );
       
    boolean result = com.debisys.tools.terminals.TerminalPermissions.deleteEntityIdPermission(idNew, userName, applicationName, entityId, parentId);
    if ( result )
        out.write("OK");
    else
        out.write(" ");
%>

