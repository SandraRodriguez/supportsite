<%@page import="com.debisys.tools.terminals.TerminalPermissions"%>
<%@page import="com.debisys.utils.LogChanges"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*" %>


<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
    String permissionCode = request.getParameter("code");
    String strRefId = SessionData.getProperty("ref_id");
    String userName = SessionData.getProperty("username");
    String applicationName = DebisysConfigListener.getInstance(this.getServletContext());
    
    String numEntities = request.getParameter("numEntities");
    ArrayList<String> idsEntities = new ArrayList<String>();
    String[] arr=null;
    if ( numEntities != null){
        int num = Integer.parseInt(numEntities);
        for(int k = 0; k<num; k++){
            String merchantIdReq = request.getParameter("m_"+k);
            idsEntities.add(merchantIdReq);
        }        
        arr = idsEntities.toArray(new String[idsEntities.size()]);
    }    
    if ( arr != null){      
        String idNew = com.debisys.tools.terminals.TerminalPermissions.getTerminalPermissionsIdByCode(permissionCode);
        if (idNew != null) {
            LogChanges logChangeControl = new LogChanges();
            int nResult = logChangeControl.getLogChangeTypeIdByNameAndTable("Insert","TerminalPermissionsByEntity");
            for (int i = 0; i < arr.length; i++) {
                TerminalPermissions.addEntityIdToPermission(idNew, arr[i], strRefId, permissionCode, userName, applicationName, nResult);
            }
            out.write("OK");            
        } else{
            out.write("FAIL1");
        }
    } else{
        out.write("FAIL2");
    }
%>
