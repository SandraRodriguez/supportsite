<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.pojo.TerminalPermissionsPropertiesByEntityPojo"%>
<%@page import="com.debisys.tools.terminals.TerminalPermissionsPropertiesByEntity"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.debisys.reports.pojo.MerchantPojo"%>
<%@page import="com.debisys.reports.pojo.TerminalPermissionBasicPojo"%>
<%@page import="com.debisys.reports.pojo.TerminalPermissionsByEntityPojo"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.tools.terminals.TerminalPermissions " %>

<%
    int section = 0;
    int section_page = 0; 
    String sectionParam       = request.getParameter("section");
    String section_pageParam  = request.getParameter("section_page");
    String entityId           = request.getParameter("entityId");                
    String pagereturn         = request.getParameter("pagereturn");
    String code               = request.getParameter("code");
    String resourceByPerm     = request.getParameter("resourceByPerm");
    
    try
    {	                                
        section = Integer.parseInt(sectionParam);
        section_page = Integer.parseInt(section_pageParam);
    }
    catch(Exception e){
     out.close();
    }
        
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@include file="/includes/header.jsp"%>

<%
String path = request.getContextPath(); 
%>
<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_page.css";
    @import "<%=path%>/includes/media/demo_table.css";
</style>

<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>

<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>

<link href="js/themes/bootstrap.min.css" type="text/css" rel="stylesheet">
<script src="js/themes/vue.min.js" type="text/javascript" charset="utf-8"></script>
<script  src="js/themes/vue-resource.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/themes/vue-color.min.js" type="text/javascript" charset="utf-8"></script>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet">

<%
    String resourceByPermCome   = Languages.getString( resourceByPerm, SessionData.getLanguage()).toUpperCase();
    String pageTitle            = "Admin properties for: "; 
    String language = "1";
    if( SessionData.getLanguage().equals("spanish") ){
        language = "2";
    }  
    String feature = PropertiesByFeature.FEATURE_TERMINAL_PERMISSIONS_TOOL;    
    HashMap<String, PropertiesByFeature> propertiesByCode = PropertiesByFeature.findPropertiesByFeatureHash(feature);        
    if (request.getMethod().equals("POST")){        
        ArrayList<String> propertiesToCatch = TerminalPermissionsPropertiesByEntity.getPermissionsByCodeAndId(code);
        for (String prop : propertiesToCatch){
            String propertyValue = request.getParameter(prop);
            String propertyIdValue = request.getParameter("idProperty"+prop);
            String propertyStatus = request.getParameter("chk"+prop);
            if (propertyStatus!=null && propertyStatus.equals("on")){
                propertyStatus="1";
            } else{
                propertyStatus="0";
            }
            if ( propertyIdValue.equals("null") ){
                propertyIdValue = request.getParameter("idPropertyInsert"+prop);
                TerminalPermissionsPropertiesByEntity.insertIntoPropertiesEntity(propertyStatus, propertyValue, propertyIdValue, entityId);
            } else{
                TerminalPermissionsPropertiesByEntity.updatePropertyById(propertyIdValue, propertyStatus, propertyValue, prop);
            }
        }
    } 
    String idNewPermissionCode = com.debisys.tools.terminals.TerminalPermissions.getTerminalPermissionsIdByCode(code);
    ArrayList<TerminalPermissionsPropertiesByEntityPojo> lst = TerminalPermissionsPropertiesByEntity.getPropertiesByPermissionCodeAndId(entityId, idNewPermissionCode);
    
    String nameHeader = "Name";
    String descHeader = "Description";
    String enableHeader = "Status";
    if ( propertiesByCode.get("TABLE_NAME_"+language) != null){
        nameHeader = propertiesByCode.get("TABLE_NAME_"+language).getValue();
    }
    if ( propertiesByCode.get("TABLE_DESCRIPTION_"+language) != null){
        descHeader = propertiesByCode.get("TABLE_DESCRIPTION_"+language).getValue();
    }
    if ( propertiesByCode.get("TABLE_STATUS_"+language) != null){
        enableHeader = propertiesByCode.get("TABLE_STATUS_"+language).getValue();
    }
    
    String backButton = "Back";
    String saveButton = "Save";
    if ( propertiesByCode.get("BACKBUTTON_"+language) != null){
        backButton = propertiesByCode.get("BACKBUTTON_"+language).getValue();
    }
    if ( propertiesByCode.get("SAVEBUTTON_"+language) != null){
        saveButton = propertiesByCode.get("SAVEBUTTON_"+language).getValue();
    }
    
%>
       
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() 
        {	                                
                                           
        }
    );    
    
</script>

<div id="multiRecords" style="padding-left: 150px"></div>
                
<table border="0" cellpadding="0" cellspacing="0" width="120%" id="entitiesTable">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" align="left">
            <b><%=pageTitle+" "+resourceByPermCome%></b>
        </td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
   
    <tr>
        <td colspan="3">
            <table border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0" style="width: 100%;" >
                <tr>
                    <td width="1" bgcolor="#FFFFFF"><img src="images/trans.gif" width="1"></td>
                    <td align="center" >
                        <form name="mainform" method="post" action="<%=path%>/admin/tools/terminals/marketPlace.jsp"  >
                            <input type="submit" value="<%=backButton%>" />
                        </form>                        
                    </td>
                    <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                </tr> 
                <tr>
                    <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                    <td align="center" valign="top" bgcolor="#FFFFFF">
                        <form name="mainform" method="post" action="<%=path%>/admin/tools/terminals/adminProperties.jsp"  >
        
                            <input type="hidden" id="section" name="section" value="<%=request.getParameter("section")%>" />
                            <input type="hidden" id="section_page" name="section_page" value="<%=request.getParameter("section_page")%>" />
                            <input type="hidden" id="pagereturn" name="pagereturn" value="<%=request.getParameter("pagereturn")%>" />
                            <input type="hidden" id="code" name="code" value="<%=request.getParameter("code")%>" />
                            <input type="hidden" id="resourceByPerm" name="resourceByPerm" value="<%=request.getParameter("resourceByPerm")%>" />
                            <input type="hidden" id="entityId" name="entityId" value="<%=request.getParameter("entityId")%>" />
                                
                            <table border="0" cellpadding="0" cellspacing="0" align="center" class="sort-table">
                                <thead>
                                    <tr class="SectionTopBorder">
                                        <td class="rowhead2" align="left"><%=nameHeader%></td>
                                        <td class="rowhead2" align="left"><%=descHeader%></td>
                                        <td class="rowhead2" align="left"><%=enableHeader%></td>
                                    </tr>
                                </thead>
                                <%    
                                int rowId= 1;
                                for(TerminalPermissionsPropertiesByEntityPojo terminalPermissionPropertyPojo : lst )
                                {
                                    String permissionId = terminalPermissionPropertyPojo.getId();
                                    String permissionName = terminalPermissionPropertyPojo.getPropertyName();
                                    String permissionValue = terminalPermissionPropertyPojo.getDescripton();
                                    Integer showControl = terminalPermissionPropertyPojo.getEnabled();
                                    String propertyId = terminalPermissionPropertyPojo.getPropertyId();
                                    Integer type = terminalPermissionPropertyPojo.getControlType();
                                    
                                    if ( showControl == 1 || showControl == 2 ){
                                        String indexId = code + "_PROPERTYNAME_"+permissionName.toUpperCase()+"_"+language;
                                        String propertyNValue = permissionName;
                                        if ( propertiesByCode.get(indexId) != null){
                                            propertyNValue = propertiesByCode.get(indexId).getValue() ;
                                        }
                                    %>
                                    <tr class="row<%=rowId%>">                                        
                                        <td class="main" align="left">
                                            <%if (type!=3){%>
                                                <%=propertyNValue%>
                                                <input type="hidden" id="idProperty<%=permissionName%>" name="idProperty<%=permissionName%>" value="<%=permissionId%>" />
                                                <input type="hidden" id="idPropertyInsert<%=permissionName%>" name="idPropertyInsert<%=permissionName%>" value="<%=propertyId%>" />                                            
                                            <%}%>                                            
                                        </td>
                                        <td class="main" align="center">
                                            <%if ( showControl == 1 || permissionValue == null){
                                                if (permissionValue == null)
                                                    permissionValue="";
                                            %>        
                                                <%if (type==1){%>
                                                    <input type="text" id="<%=permissionName%>" name="<%=permissionName%>" value="<%=permissionValue%>" size="80"/>
                                                <%}else if (type==2){
                                                    String indexIdOpt = code + "_PROPERTYNAME_"+permissionName.toUpperCase()+"_OPT_"+language;
                                                    if ( propertiesByCode.get(indexIdOpt) != null){
                                                        propertyNValue = propertiesByCode.get(indexIdOpt).getValue();
                                                    }
                                                    String controlToPaint = parseOptions(propertyNValue, permissionName, permissionValue);
                                                    out.print(controlToPaint);
                                                %>                                                    
                                                
                                                <%
                                                }%>
                                            <%}else{%>
                                            <span><%=permissionValue%></span> 
                                            <%}%>
                                        </td>
                                        
                                            <%if (type!=3){
                                                %>
                                                <td class="main" align="left">
                                                    <%                                                
                                                    if ( showControl == 1 ){%>
                                                    <input type="checkbox" id="chk<%=permissionName%>" name="chk<%=permissionName%>" checked  />
                                                    <%}else{%>
                                                    <input type="checkbox" id="chk<%=permissionName%>" name="chk<%=permissionName%>"  />                                                
                                                </td>
                                            <%}
                                            } else{%>
                                                <td class="main" align="left" style="padding-bottom: 30px;"></td>
                                            <%}%>                                            
                                        
                                    </tr>
                                        
                                <%  
                                    }
                                    if ( rowId == 1){
                                        rowId = 2;
                                    } else{
                                        rowId = 1;
                                    }
                                }
                                %>                                
                               

                                <!-- tr>
                                    <td colspan="1" class="main" align="center">Color</td>
                                    <td colspan="3" class="main" align="center">
                                        <div id="controlColors" name="controlColors" >

                                            <input type="hidden" id="MarketPlaceColorHidden" value="" />

                                            <div class="page animsition" >
                                                <div class="page-content container-fluid" id="colors-page">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <button class="btn btn-success" @click="saveColors()"></button>                
                                                        </div>
                                                    </div>
                                                <div class="row">            
                                                    <div class="col-sm-12">
                                                        <div class="row" style="color:black;" >
                                                            <div class="col-sm-12" >

                                                                <material-picker v-model="baseColors" @change-color="onChangeBase"  
                                                                                 head="" style="width: 50%;"></material-picker>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                            <script  src="js/themes/marketPlaceColor.js" type="text/javascript" charset="utf-8"></script>

                                        </div>
                                    </td>
                                </tr -->
                                
                                <tr>
                                    <td colspan="3" align="center" >
                                        <input type="submit" value="<%=saveButton%>"  />
                                    </td>
                                </tr> 
                            </table>
                        </form>
                    </td>
                    <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                </tr>
                <tr><td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td></tr>
            </table>
        </td>
    </tr>
</table>
    <%!
    String parseOptions(String op1, String controlId, String controlValue)
    {
        String controlHeader = "<select id='"+controlId+"' name='"+controlId+"'>";
        StringBuilder options = new StringBuilder();        
        for(String optionValue : op1.split("\\|")){
            String[] val =  optionValue.split("=");
            if (controlValue.equals(val[0])){
                options.append("<option selected value='"+val[0]+"'>"+val[1]+"</option>");
            } else{
                options.append("<option value='"+val[0]+"'>"+val[1]+"</option>");
            }
        }        
        return controlHeader + options + "</select>";      
    }
    %>
<%@ include file="/includes/footer.jsp"%>




