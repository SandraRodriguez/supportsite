<%-- 
    Document   : tokensecurity
    Created on : Nov 7, 2017, 1:15:39 PM
    Author     : nmartinez
--%>
<%@page import="com.debisys.tools.terminals.TerminalPermissions"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.reports.TransactionReport" %>

<%
    int section = 9;
    int section_page = 39;
    String permissionCode = TerminalPermissions.TOKEN_SECURITY;
    String resourceByPerm = "jsp.tools.title."+DebisysConstants.PERM_TOKEN_SECURITY;
    String pageReturn = "tokensecurity.jsp";
    
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%@ include file="/includes/security.jsp"%>
<%@include file="/includes/header.jsp"%>

<jsp:include page="permissionconfiguration.jsp" flush="true" >
    <jsp:param name="permissionCode" value="<%=permissionCode%>" />
    <jsp:param name="resourceByPerm" value="<%=resourceByPerm%>" />
    <jsp:param name="pageReturn" value="<%=pageReturn%>" />  
    <jsp:param name="section" value="<%=section%>" /> 
    <jsp:param name="section_page" value="<%=section_page%>" />
</jsp:include>

<%@ include file="/includes/footer.jsp"%>
