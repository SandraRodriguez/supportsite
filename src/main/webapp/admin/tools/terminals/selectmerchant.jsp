<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.debisys.reports.pojo.MerchantPojo"%>
<%@page import="com.debisys.reports.pojo.TerminalPermissionBasicPojo"%>
<%@page import="com.debisys.reports.pojo.TerminalPermissionsByEntityPojo"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.tools.terminals.TerminalPermissions " %>

<%
    int section = 0;
    int section_page = 0; 
    String sectionParam            = request.getParameter("section");
    String section_pageParam       = request.getParameter("section_page");
                    
    String pagereturn         = request.getParameter("pagereturn");
    String code               = request.getParameter("code");
    String resourceByPerm     = request.getParameter("resourceByPerm");
    
    try
    {	                                
        section = Integer.parseInt(sectionParam);
        section_page = Integer.parseInt(section_pageParam);
    }
    catch(Exception e){
     out.close();
    }
        
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@include file="/includes/header.jsp"%>

<%
String path = request.getContextPath(); 
%>
<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_page.css";
    @import "<%=path%>/includes/media/demo_table.css";
</style>

<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
<%--
<script src="/support/js/dataTables.tableTools.js" type="text/javascript"></script>
--%>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>

<%
    String resourceByPermCome   = Languages.getString( resourceByPerm, SessionData.getLanguage()).toUpperCase();
    String pageTitle            = Languages.getString("jsp.tools.searchmerchant.title",SessionData.getLanguage()).toUpperCase();                  
    String labelProcessing      = Languages.getString("jsp.tools.datatable.processing",SessionData.getLanguage());
    String labelShowRecords     = Languages.getString("jsp.tools.datatable.showRecords",SessionData.getLanguage());
    String labelNoRecords       = Languages.getString("jsp.tools.datatable.norecords",SessionData.getLanguage());
    String labelNoDataAvailable = Languages.getString("jsp.tools.datatable.noDataAvailable",SessionData.getLanguage());
    String labelInfo            = Languages.getString("jsp.tools.datatable.info",SessionData.getLanguage());
    String labelInfoEmpty       = Languages.getString("jsp.tools.datatable.infoEmpty",SessionData.getLanguage());
    String labelFilter          = Languages.getString("jsp.tools.datatable.filter",SessionData.getLanguage());
    String labelSearch          = Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage());
    String labelLoading         = Languages.getString("jsp.tools.datatable.loading",SessionData.getLanguage());
    String labelFirst           = Languages.getString("jsp.tools.datatable.first",SessionData.getLanguage());
    String labelLast            = Languages.getString("jsp.tools.datatable.last",SessionData.getLanguage());
    String labelNext            = Languages.getString("jsp.tools.datatable.next",SessionData.getLanguage());
    String labelPrevious        = Languages.getString("jsp.tools.datatable.previous",SessionData.getLanguage());
    String labelShow            = Languages.getString("jsp.tools.datatable.show",SessionData.getLanguage());
    String labelRecords         = Languages.getString("jsp.tools.datatable.records",SessionData.getLanguage());
    String labelMerchantId      = Languages.getString("jsp.tools.searchmerchant.merchantId", SessionData.getLanguage());
    String labelMerchantDBA     = Languages.getString("jsp.tools.searchmerchant.merchantDBA", SessionData.getLanguage());
    String labelLegalBName      = Languages.getString("jsp.tools.searchmerchant.merchantLegalBName", SessionData.getLanguage());
    String labelPhys_address    = Languages.getString("jsp.tools.searchmerchant.merchantPhys_address", SessionData.getLanguage());
    String labelPhys_city       = Languages.getString("jsp.tools.searchmerchant.merchantPhys_city", SessionData.getLanguage());
    String labelPhys_county     = Languages.getString("jsp.tools.searchmerchant.merchantPhys_county", SessionData.getLanguage());
    String labelPhys_state      = Languages.getString("jsp.tools.searchmerchant.merchantPhys_state", SessionData.getLanguage());
    String labelPhys_zip        = Languages.getString("jsp.tools.searchmerchant.merchantPhys_zip", SessionData.getLanguage());
    String labelPhys_country    = Languages.getString("jsp.tools.searchmerchant.merchantPhys_country", SessionData.getLanguage());
    String labelRepBuss         = Languages.getString("jsp.tools.searchmerchant.repBuss", SessionData.getLanguage());
    String labelRepId           = Languages.getString("jsp.tools.searchmerchant.repId", SessionData.getLanguage());
    String labelSelectAll       = Languages.getString("jsp.tools.searchmerchant.selectAll", SessionData.getLanguage());
    String labelUnselectAll     = Languages.getString("jsp.tools.searchmerchant.UnselectAll", SessionData.getLanguage());    
    
    String labelselectMerchants = Languages.getString("jsp.tools.searchmerchant.selectMerchants", SessionData.getLanguage());    
    String labelcancel          = Languages.getString("jsp.tools.searchmerchant.cancel", SessionData.getLanguage());    
 
 
%>

    
                
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() 
        {	                                
            setTableMerchants();                                
        }
    );
</script>
    
<script type="text/javascript" charset="utf-8">
    function setTableMerchants()
    {
            $('#merchantsTable').dataTable( {
                    "iDisplayLength": 10,
                    "bLengthChange": true,
                    "bFilter": true,
                    "sPaginationType": "full_numbers",

                    "oLanguage": {
                       "sProcessing":      "<%=labelProcessing%>",
                                "sLengthMenu":     "<%=labelShowRecords%>",
                                "sZeroRecords":    "<%=labelNoRecords%>",
                                "sEmptyTable":     "<%=labelNoDataAvailable%>",
                                "sInfo":           "<%=labelInfo%>",
                                "sInfoEmpty":      "<%=labelInfoEmpty%>",
                                "sInfoFiltered":   "<%=labelFilter%>",
                                "sInfoPostFix":    "",
                                "sSearch":         "<%=labelSearch%>",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "<%=labelLoading%>",
                                "oPaginate": {
                                    "sFirst":    "<%=labelFirst%>",
                                    "sLast":     "<%=labelLast%>",
                                    "sNext":     "<%=labelNext%>",
                                    "sPrevious": "<%=labelPrevious%>"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                },
                                "sLengthMenu": ' <%=labelShow%> <select id="pagesNumbers" >'+
                                                            '<option value="10">10</option>'+
                                                            '<option value="20">20</option>'+
                                                            '<option value="30">30</option>'+
                                                            '<option value="40">40</option>'+
                                                            '<option value="50">50</option>'+
                                                            '<option value="-1">All</option>'+
                                                            '</select> <%=labelRecords%>'
                     },
                 "aaSorting": [[ 0, "asc" ]]     
            }				
            );
    }
</script>    
    
<script type="text/javascript" charset="utf-8">
    
    var countMerchantDone = 0;
    var numRequest=1;
    function loopTableMerchants()
    {
        //$('#tr_merchantId_739660434690').remove();
        var flaf = true;
        if (flaf){
            var oTable = $('#merchantsTable').dataTable();
            var data_length = $('#pagesNumbers').val();
            var nNodes = oTable.fnGetNodes( );
            var pages = nNodes.length / data_length;
            var ids=[];
            var merchantSelected = "";
            oTable.fnPageChange( 0 );
            var mid= 0;
            var countMerchantToPost = 799;
            var countMerchant = 0;
            $("#multiRecords").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
            
            for ( var j = 0; j < pages; j++ )
            {
                $('#merchantsTable tbody tr td input[type="checkbox"]').each(function()
                {
                    if ($(this).is(':checked'))
                    {
                        var nameControl = $(this).attr( 'id' );                        
                        var strs = nameControl.split("_");
                        var merchantId = strs[2];                        
                        if ( ids.indexOf(merchantId) === -1 )
                        {
                            ids.push(merchantId);
                            merchantSelected += "&m_"+mid+"="+merchantId;
                            mid++;
                            if ( countMerchant === countMerchantToPost){                                
                                var merchantsToInsert = merchantSelected;
                                merchantSelected = "";
                                var midToInsert = mid;
                                mid = 0;
                                countMerchant = 0;
                                $.ajax({
                                    type: 'POST',
                                    url: 'admin/tools/terminals/addrecords.jsp?nr'+numRequest,
                                    data: 'ee=0&numEntities='+midToInsert+'&Random=' + Math.random()+ '&code=<%=code%>'+merchantsToInsert,
                                    success: function ProcessSave(data) {
                                        countMerchantDone++;
                                        
                                    },                                    
                                    async:true
                                  });
                                  numRequest++;                                
                            } else{
                                //console.log("index j"+j);
                                countMerchant++;
                            }
                        }
                    }
                });
                oTable.fnPageChange( 'next' );
                
                              
            }
            oTable.fnPageChange( 0 );
            if ( merchantSelected.length>0){
                $.ajax({
                    type: 'POST',
                    url: 'admin/tools/terminals/addrecords.jsp',
                    data: 'ee=0&numEntities='+mid+'&Random=' + Math.random()+ '&code=<%=code%>'+merchantSelected,
                    success: function ProcessSave(data) {
                        countMerchantDone++;                        
                    },                                    
                    async:true
                });
                numRequest++; 
            } else{
                //setTimeout('window.location.href = "<%=path%>/admin/tools/terminals/<%=pagereturn%>";', 950);            
            }   
            setInterval('endWork();', 5000);            
            
        }                             
    }
</script>
                
<script type="text/javascript" charset="utf-8">	
    function endWork(){
        if (countMerchantDone===numRequest-1){
            //$("#multiRecords").html("Finished work!! "+new Date());     
            setTimeout('window.location.href = "<%=path%>/admin/tools/terminals/<%=pagereturn%>";', 950);
        } else{
            //$("#multiRecords").html("Wainting for work!! "+new Date() + " ["+countMerchantDone+ " -"+numRequest+"]");       
        }
        
    }
    function unselectAll()
    {
        var actionSelect = false;
        if ($('#unselect').is(':checked'))
        {
            actionSelect = true
            $('#spanSelect').html("<%=labelUnselectAll%>");
        }
        else
        {
            actionSelect = false;
            $('#spanSelect').html("<%=labelSelectAll%>");
        }
        $('#pagesNumbers').val("-1");
        $('#pagesNumbers').change();
        $('#merchantsTable tbody input[type=checkbox]').attr('checked',actionSelect);
        $('#pagesNumbers').val("10");
        $('#pagesNumbers').change();					
    }
</script>
        
<script type="text/javascript" charset="utf-8">	
    function cancel()
    {
        window.location.href = "<%=path%>/admin/tools/terminals/<%=pagereturn%>";					
    }
</script>
<div id="multiRecords" style="padding-left: 150px"></div>
                
<table border="0" cellpadding="0" cellspacing="0" width="120%" id="entitiesTable">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" class="formAreaTitle" align="left">
            <b><%=pageTitle+" "+resourceByPermCome %></b>
        </td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <%    
    String strRefId = SessionData.getProperty("ref_id");
    ArrayList<MerchantPojo> lstMerchantPojo;
    String cacheIdMerchants = "merchantsCache"+strRefId;
    lstMerchantPojo = TerminalPermissions.getMerchantsByParent(strRefId, code, strDistChainType);
    /*
    if ( application.getAttribute(cacheIdMerchants) != null ){
        lstMerchantPojo = (ArrayList)application.getAttribute(cacheIdMerchants);
    } else{
        lstMerchantPojo = TerminalPermissions.getMerchantsByParent(strRefId, code, strDistChainType);
        application.setAttribute(cacheIdMerchants, lstMerchantPojo);
    }
    */  
    
    %>
    <tr>
        <td colspan="3">
            <table border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0" >
                <tr>
                    <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                    <td align="center" valign="top" bgcolor="#FFFFFF">
                        <table border="0" cellpadding="2" cellspacing="0"  align="center" >  
                            <tr>
                                <td align="center" >
                                    <input type="button" value="<%=labelselectMerchants%>" onclick="loopTableMerchants()" />
                                    <input type="button" value="<%=labelcancel%>" onclick="cancel()" />
                                </td>
                            </tr> 
                            <tr>
                            <td colspan="2" class="main" align="center" >
                                    <table cellpadding="0"  cellspacing="0" border="0" class="display" id="merchantsTable" name="merchantsTable">
                                        <thead>
                                            <tr class="rowhead2">
                                                <th><%=labelMerchantId%></th>
                                                <th><%=labelMerchantDBA%></th>
                                                <th><%=labelLegalBName%></th>
                                                <th><%=labelPhys_address%></th>                                               
                                                <th><%=labelRepBuss%></th>
                                                <th><%=labelRepId%></th>
                                                <th><span id="spanSelect"><%=labelSelectAll%></span>
						 	<input type="checkbox" id="unselect" name="unselect" onclick="unselectAll();">
						</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         <%			 
                                         for(MerchantPojo mchPojo : lstMerchantPojo )
                                         {   
                                         %>
                                         <tr id="tr_merchantId_<%=mchPojo.getMerchant_id()%>">                                             
                                             <td><%=mchPojo.getMerchant_id()%></td>
                                            <td><%=mchPojo.getDba()%></td>
                                            <td><%=mchPojo.getLegal_businessname()%></td>                                               
                                            <td><%=mchPojo.getPhys_address() %></td>
                                            <!--
                                            <td><%=mchPojo.getPhys_city()%></td>
                                            <td><%=mchPojo.getPhys_county()%></td>
                                            <td><%=mchPojo.getPhys_state()%></td>
                                            <td><%=mchPojo.getPhys_zip()%></td> 
                                            -->
                                            <td><%=mchPojo.getRepBuss()%></td>
                                            <td><%=mchPojo.getRep_id() %></td> 
                                            <td><input type="checkbox" id="chk_merchantId_<%=mchPojo.getMerchant_id()%>" />
                                                <div id="div_entityId_<%=mchPojo.getMerchant_id()%>" ></div>						
                                            </td>
                                            </td>
                                         </tr>
                                         <%				 				 		
                                         }
                                         %>
                                        </tbody>                                        
                                    </table>
                                </td>
                            </tr>
                           
                        </table>
                    </td>
                    <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                </tr>
                <tr><td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td></tr>
            </table>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp"%>
