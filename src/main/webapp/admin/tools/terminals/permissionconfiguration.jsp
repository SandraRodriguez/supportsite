<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="com.debisys.reports.pojo.TerminalPermissionBasicPojo"%>
<%@page import="com.debisys.reports.pojo.TerminalPermissionsByEntityPojo"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.tools.terminals.TerminalPermissions,com.debisys.languages.*" %>

<%
   String permissionCode = request.getParameter("permissionCode");;
   String resourceByPerm = request.getParameter("resourceByPerm");
   String pageReturn     = request.getParameter("pageReturn");
   String section        = request.getParameter("section");
   String section_page   = request.getParameter("section_page");
   String feature = PropertiesByFeature.FEATURE_TERMINAL_PERMISSIONS_TOOL;
   String propertyToFind = permissionCode+"_PAGE";
   
   Boolean showAdminLink = false;
   HashMap<String, PropertiesByFeature> propertiesByCode = PropertiesByFeature.findPropertyByCode(feature, propertyToFind);   
   if (propertiesByCode.get(propertyToFind) != null){
        if ( propertiesByCode.get(propertyToFind).getValue() != null){
            String adminProperties = propertiesByCode.get(propertyToFind).getValue();
            if (adminProperties.toLowerCase().equals("true") || adminProperties.toLowerCase().equals("1")){
                showAdminLink = true;
            }
        }
   }
    
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
String path = request.getContextPath(); 
%>
<style type="text/css" title="currentStyle">
    @import "<%=path%>/includes/media/demo_table.css";
</style>
  
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=path%>/includes/media/jquery.dataTables.js"></script>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>

<%
    String labelProcessing      = Languages.getString("jsp.tools.datatable.processing",SessionData.getLanguage());
    String labelShowRecords     = Languages.getString("jsp.tools.datatable.showRecords",SessionData.getLanguage());
    String labelNoRecords       = Languages.getString("jsp.tools.datatable.norecords",SessionData.getLanguage());
    String labelNoDataAvailable = Languages.getString("jsp.tools.datatable.noDataAvailable",SessionData.getLanguage());
    String labelInfo            = Languages.getString("jsp.tools.datatable.info",SessionData.getLanguage());
    String labelInfoEmpty       = Languages.getString("jsp.tools.datatable.infoEmpty",SessionData.getLanguage());
    String labelFilter          = Languages.getString("jsp.tools.datatable.filter",SessionData.getLanguage());
    String labelSearch          = Languages.getString("jsp.tools.datatable.search",SessionData.getLanguage());
    String labelLoading         = Languages.getString("jsp.tools.datatable.loading",SessionData.getLanguage());
    String labelFirst           = Languages.getString("jsp.tools.datatable.first",SessionData.getLanguage());
    String labelLast            = Languages.getString("jsp.tools.datatable.last",SessionData.getLanguage());
    String labelNext            = Languages.getString("jsp.tools.datatable.next",SessionData.getLanguage());
    String labelPrevious        = Languages.getString("jsp.tools.datatable.previous",SessionData.getLanguage());
    String labelShow            = Languages.getString("jsp.tools.datatable.show",SessionData.getLanguage());
    String labelRecords         = Languages.getString("jsp.tools.datatable.records",SessionData.getLanguage());
    String labelMerchantId      = Languages.getString("jsp.tools.dtu143.merchantId", SessionData.getLanguage());
    String labelMerchantDBA     = Languages.getString("jsp.tools.dtu143.merchantDBA", SessionData.getLanguage());
    String labeladdNewMerchants = Languages.getString("jsp.tools.searchmerchant.addNewMerchants", SessionData.getLanguage());    
    String labelDeactivate      = Languages.getString("jsp.tools.searchmerchant.deactivate", SessionData.getLanguage());    
    String labelAdminProperties = Languages.getString("jsp.tools.properties.adminProperties", SessionData.getLanguage());    
%>
                
<script type="text/javascript" charset="utf-8"> 
    //$("#permissionConfigurationRecords").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
    $(document).ready(function() 
        {	              
            reloadTableTerminals();                                
            $("#permissionConfigurationRecords").html("");            
        }
    );
</script>

<script type="text/javascript" charset="utf-8">	
    function reloadTableTerminals()
    {
        $('#eternalTable').dataTable( {
                "iDisplayLength": 10,
                "bLengthChange": true,
                "bFilter": true,
                "sPaginationType": "full_numbers",
                                 
                "oLanguage": {
                    "sProcessing":      "<%=labelProcessing%>",
                    "sLengthMenu":     "<%=labelShowRecords%>",
                    "sZeroRecords":    "<%=labelNoRecords%>",
                    "sEmptyTable":     "<%=labelNoDataAvailable%>",
                    "sInfo":           "<%=labelInfo%>",
                    "sInfoEmpty":      "<%=labelInfoEmpty%>",
                    "sInfoFiltered":   "<%=labelFilter%>",
                    "sInfoPostFix":    "",
                    "sSearch":         "<%=labelSearch%>",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "<%=labelLoading%>",
                    "oPaginate": {
                        "sFirst":    "<%=labelFirst%>",
                        "sLast":     "<%=labelLast%>",
                        "sNext":     "<%=labelNext%>",
                        "sPrevious": "<%=labelPrevious%>"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "sLengthMenu": ' <%=labelShow%> <select id="pagesNumbers" >'+
                                                '<option value="10">10</option>'+
                                                '<option value="20">20</option>'+
                                                '<option value="30">30</option>'+
                                                '<option value="40">40</option>'+
                                                '<option value="50">50</option>'+
                                                '<option value="-1">All</option>'+
                                                '</select> <%=labelRecords%>'
                 },
             "aaSorting": []     
        }				
        );
    }
</script>
   
<script type="text/javascript" charset="utf-8">
    function deleteEntityRecord(IdNew, entityId, parentId) 
    {   
        var requestURL = "admin/tools/terminals/deleterecord.jsp?Random=" + Math.random()+"&idNew="+IdNew+"&entityId="+entityId+"&parentId="+parentId;
        //alert(requestURL);
        $( "#div_entityId_"+IdNew ).load( requestURL, function( response, status, xhr ) 
            {
                window.location.href = "<%=path%>/admin/tools/terminals/<%=pageReturn%>";
            }
        );
        
    }
</script>

<script type="text/javascript" charset="utf-8">
    function addForAllMerchantsByParenEntityId(entityId) 
    {
        var requestURL = "admin/tools/terminals/addrecord.jsp?Random=" + Math.random()+"&entityId="+entityId+"&code=<%=permissionCode%>";        
        //alert(requestURL);
        $( "#div_entityId_"+entityId ).load( requestURL, function( response, status, xhr ) 
            {
                window.location.href = "<%=path%>/admin/tools/terminals/<%=pageReturn%>";
            }
        );
        
    }
</script>

<div style="text-align:center">
    <table border="0" cellpadding="0" cellspacing="0" width="450">
        <tr>
            <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
            <td background="images/top_blue.gif" width="2000" class="formAreaTitle">
                <b><%=Languages.getString(resourceByPerm,SessionData.getLanguage()).toUpperCase()%></b>
            </td>
            <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
        </tr>
        <%
        String strRefId = SessionData.getProperty("ref_id");
        TerminalPermissionBasicPojo terminalPermissionBasicPojo = TerminalPermissions.getEntitiesByParent(strRefId, permissionCode);
        ArrayList<TerminalPermissionsByEntityPojo> lstTerminalPermissions = terminalPermissionBasicPojo.getLstTerminalPermissionsByEntityPojo();
        boolean isIsIsoSelected = terminalPermissionBasicPojo.isIsIsoSelected();
        %>
        <tr>
            <td colspan="3">
                <table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
                    <tr>                    
                        <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                        <td align="center" valign="top" bgcolor="#FFFFFF">
                            <table border="0" cellpadding="2" cellspacing="10" width="100%" align="center">
                                <tr>
                                    <td class="main" align="center" nowrap><br/>
                                        <a href="<%=path%>/admin/tools/terminals/selectmerchant.jsp?pagereturn=<%=pageReturn%>&code=<%=permissionCode%>&resourceByPerm=<%=resourceByPerm%>&section_page=<%=section_page%>&section=<%=section%>">
                                            <%=labeladdNewMerchants%>
                                        </a>                                        
                                    </td>
                                    <%
                                    if (showAdminLink){
                                    %>                                
                                    <td class="main" align="center" nowrap><br/>
                                        <a href="<%=path%>/admin/tools/terminals/adminProperties.jsp?entityId=<%=strRefId%>&pagereturn=<%=pageReturn%>&code=<%=permissionCode%>&resourceByPerm=<%=resourceByPerm%>&section_page=<%=section_page%>&section=<%=section%>">
                                            <%=labelAdminProperties%>
                                        </a>                                        
                                    </td>                                
                                    <%}%>
                                                              

                                <%                                
                                String styleWhenAllMerchantSelected = "";
                                if ( isIsIsoSelected )
                                {              
                                    styleWhenAllMerchantSelected = "background-color: #C0C0C0";
                                    String isoSelected  = Languages.getString("jsp.tools.terminals.permissions.isoSelected1",SessionData.getLanguage());				
                                    String isoSelected1 = Languages.getString("jsp.tools.terminals.permissions.isoSelected2",SessionData.getLanguage());
                                %>
                                    <tr>
                                    <td colspan="2" class="main" align="center" nowrap><br/><%=isoSelected%></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="main" align="center" nowrap><%=isoSelected1%><br/><br/></td>
                                    </tr>  
                                    <tr>
                                        <td colspan="2" class="main" align="center" nowrap>
                                            <input type="button" value="<%=labelDeactivate%>" 
                                                   onclick="deleteEntityRecord('<%=terminalPermissionBasicPojo.getIdNew()%>','<%=strRefId%>','<%=strRefId%>')"/><br/><br/>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="2"><div id="div_entityId_<%=terminalPermissionBasicPojo.getIdNew()%>" ></div></td>
                                    </tr> 

                                <%
                                }
                                else
                                {
                                    String isoSelected4  = Languages.getString("jsp.tools.terminals.permissions.isoSelected3",SessionData.getLanguage());				                                                                
                                    %>                                                               
                                    <tr>
                                        <td colspan="2" class="main" align="center" nowrap>
                                            <input type="button" value="<%=isoSelected4%>" 
                                                   onclick="addForAllMerchantsByParenEntityId('<%=strRefId%>')" /><br/><br/>
                                        </td>
                                    </tr>                                  
                                    <tr>
                                        <td colspan="2"><div id="div_entityId_<%=strRefId%>" ></div></td>
                                    </tr> 
                                <%
                                }
                                %>

                                <tr>
                                    <td colspan="2" class="main" align="center" >
                                        <div id="permissionConfigurationRecords">
                                            <span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>
                                        </div>
                                        <div id="diveternalTable">

                                        <table cellpadding="0" cellspacing="0" border="0" class="display" id="eternalTable">
                                            <thead>
                                                <tr class="rowhead2">
                                                    <th><%=labelMerchantId%></th>
                                                    <th><%=labelMerchantDBA%></th>
                                                    <%
                                                    if ( !isIsIsoSelected )
                                                    {
                                                    %>
                                                    <th></th> 
                                                    <%
                                                    }
                                                    %>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             <%			 
                                             for(TerminalPermissionsByEntityPojo terminalPermission : lstTerminalPermissions )
                                             {
                                             %>
                                             <tr  style="<%=styleWhenAllMerchantSelected%>">

                                                <td><%=terminalPermission.getEntityId()%></td>
                                                <td><%=terminalPermission.getEntityName()%></td>                                               

                                                    <%
                                                    if ( !isIsIsoSelected )
                                                    {
                                                    %>
                                                     <td>
                                                        <input type="button" value="Delete" onclick="deleteEntityRecord('<%=terminalPermission.getIdNew()%>','<%=terminalPermission.getEntityId()%>','<%=strRefId%>')" />
                                                        <div id="div_entityId_<%=terminalPermission.getIdNew()%>" ></div>
                                                     </td>   
                                                    <%
                                                    }
                                                    %>


                                             </tr>
                                             <%				 				 		
                                             }
                                             %>
                                            </tbody>                                        
                                        </table>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </td>
                        <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                    </tr>
                    <tr><td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td></tr>
                </table>
            </td>
        </tr>
    </table>
</div>

