<%@page import="com.debisys.utils.LogChanges"%>
<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*" %>


<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%        
    String permissionCode = request.getParameter("code"); 
    String entityId = request.getParameter("entityId"); 
    String strRefId = SessionData.getProperty("ref_id");
    String userName = SessionData.getProperty("username");
    String applicationName = DebisysConfigListener.getInstance( this.getServletContext() );
    String idNew = com.debisys.tools.terminals.TerminalPermissions.getTerminalPermissionsIdByCode(permissionCode);
    LogChanges logChangeControl = new LogChanges();
    int nResult = logChangeControl.getLogChangeTypeIdByNameAndTable("Insert","TerminalPermissionsByEntity");
    if (idNew != null) {
        boolean result = com.debisys.tools.terminals.TerminalPermissions.addEntityIdToPermission(idNew, entityId, strRefId, 
                permissionCode, userName, applicationName, nResult );
        if ( result)
            out.write("OK");
        else
            out.write(" ");
    }
%>
