<%@page import="com.debisys.tools.terminals.TerminalPermissions"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*" %>

<%
    int section = 9;
    int section_page = 27;
    String permissionCode = TerminalPermissions.CODE_ETERNAL_LOGIN;
    String resourceByPerm = "jsp.tools.dtu2639."+DebisysConstants.PERM_ONE_LOGIN_FOR_MERCHANTS_TERMINALS;                  
    String pageReturn = "oncelogin.jsp";
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@include file="/includes/header.jsp"%>

<jsp:include page="permissionconfiguration.jsp" flush="true" >
    <jsp:param name="permissionCode" value="<%=permissionCode%>" />
    <jsp:param name="resourceByPerm" value="<%=resourceByPerm%>" />
    <jsp:param name="pageReturn" value="<%=pageReturn%>" /> 
    <jsp:param name="section" value="<%=section%>" /> 
    <jsp:param name="section_page" value="<%=section_page%>" />
</jsp:include>
             
<%@ include file="/includes/footer.jsp"%>
