<%--
    Document   : Chat
    Created on : Dec 21, 2017, 11:38:45 AM
    Author     : andres diaz
--%>
<%@page import="com.debisys.tools.terminals.TerminalPermissions"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.services.*,
                 com.debisys.reports.TransactionReport" %>

<%
    int section = 1;
    int section_page = 1;
    String permissionCode = TerminalPermissions.CHAT;
    String resourceByPerm = "jsp.tools.dtu4003."+DebisysConstants.PERM_ALLOW_USE_CHAT;
    String pageReturn = "chat.jsp";

%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%@ include file="/includes/security.jsp"%>
<%@include file="/includes/header.jsp"%>

<jsp:include page="permissionconfiguration.jsp" flush="true" >
    <jsp:param name="permissionCode" value="<%=permissionCode%>" />
    <jsp:param name="resourceByPerm" value="<%=resourceByPerm%>" />
    <jsp:param name="pageReturn" value="<%=pageReturn%>" />
    <jsp:param name="section" value="<%=section%>" />
    <jsp:param name="section_page" value="<%=section_page%>" />
</jsp:include>

<%@ include file="/includes/footer.jsp"%>