<%@ page language="java" import="java.util.*, com.debisys.languages.*,com.debisys.customers.Rep" pageEncoding="ISO-8859-1"%>
<%
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="s2k" class="com.debisys.tools.s2k" scope="request"/>
<%
	
  	int section=9;
  	int section_page=12;
  	int result = -1;
%>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    	<td width="10" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    	<td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.admin.reports.transactions.s2ktitle",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
 	</tr>
 	
 	<tr>
 		<td colspan="3">
   			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
 	    		<tr>
 		      		<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
  		    		<td align="center" valign="top" bgcolor="#FFFFFF">
   			    		<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    		<tr>
    				    		<td width="18">&nbsp;</td>
     				    		<td class="main">
									<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
          								<tr>
           									<td VALIGN="middle" WIDTH="376" height="28">
             									<img border="0" src="images/analysis_cube.gif" width="22" height="22">
             									<font face="Tahoma" style="font-size: 9pt; font-weight: 700" color="#000080">
             									<%=Languages.getString("jsp.admin.reports.transactions.s2ktitle",SessionData.getLanguage())%>
             									</font>
          									</td>
      									</tr>
      									
				<tr>
				  <td VALIGN="top" WIDTH="376" height="28">
				 <font face="Tahoma" color="#000080" style="font-size: 9pt"><a href="admin/tools/s2ktaskscheduler.jsp" style="text-decoration: none">
				    - <%=Languages.getString("jsp.admin.tools.s2k.tools.title3",SessionData.getLanguage())%>
				     </a></font>
				     </td>
				</tr>
				<tr>
				  <td VALIGN="top" WIDTH="376" height="28">
				 <font face="Arial, Helvetica, sans-serif" color="#2C0973" style="font-size: 13px"><a href="admin/tools/s2kstockcode.jsp" style="text-decoration: none">
				    - <%=Languages.getString("jsp.admin.tools.s2k.tools.title2",SessionData.getLanguage())%>
				     </a></font>
				     </td>
				</tr>
				<tr>
				  <td VALIGN="top" WIDTH="376" height="28">
				 <font face="Arial, Helvetica, sans-serif" color="#2C0973" style="font-size: 13px"><a href="admin/tools/s2ksalesmanid.jsp" style="text-decoration: none">
				    - <%=Languages.getString("jsp.admin.tools.s2k.tools.title1",SessionData.getLanguage())%>
				     </a></font>
				     </td>
				</tr>
			</table>
     				    		</td>
     		        			<td width="18">&nbsp;</td>
        					</tr>
       					</table>
    				</td>
    				<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	  			</tr>
	  			<tr>
		  			<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	  			</tr>
			</table>
 		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
