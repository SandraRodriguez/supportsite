<%@ page
	import="java.util.HashMap,
			java.io.IOException,
			org.apache.commons.httpclient.Header,
			org.apache.commons.httpclient.HttpClient,
			org.apache.commons.httpclient.HttpException,
			org.apache.commons.httpclient.methods.PostMethod,
			org.apache.commons.httpclient.methods.StringRequestEntity" %>
<%!
	// ----------------------------------------------------------------------------------------------------------------------
	public String postMethod(String url, HashMap<String, String> headers, String content) throws HttpException, IOException
	{
		long startTime = System.currentTimeMillis();
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(url);
		if (headers != null && !headers.isEmpty())
		{
			for (String entry : headers.keySet())
			{
				post.addRequestHeader(new Header(entry, headers.get(entry)));
			}
		}
		post.setRequestEntity(new StringRequestEntity(content));
		client.executeMethod(post);
		String responseFromPost = post.getResponseBodyAsString();
		post.releaseConnection();
		long elapsed = System.currentTimeMillis() - startTime;
		return responseFromPost;
	}
	public String VerifyAddressAdvanced(String strRecipient, String strAddress1, String strAddress2, String strUrbanization, String strZipcode, String strCity, String strState){
		try
		{
			final String strLicenseKey = "ac717d22-49c9-42c3-b910-a7fd77bbe7e9";
			String url = "http://pav3.cdyne.com/PavService.svc/VerifyAddressAdvanced";
			String body = "<PavRequest xmlns=\"pav3.cdyne.com\">"
				+ "<CityName>" + strCity + "</CityName>"
				+ "<FirmOrRecipient>" + strRecipient + "</FirmOrRecipient>"
				+ "<LicenseKey>" + strLicenseKey + "</LicenseKey>"
				+ "<PrimaryAddressLine>" + strAddress1 + "</PrimaryAddressLine>"
				+ "<ReturnCaseSensitive>false</ReturnCaseSensitive>"
				+ "<ReturnCensusInfo>false</ReturnCensusInfo>"
				+ "<ReturnCityAbbreviation>true</ReturnCityAbbreviation>"
				+ "<ReturnGeoLocation>true</ReturnGeoLocation>"
				+ "<ReturnLegislativeInfo>false</ReturnLegislativeInfo>"
				+ "<ReturnMailingIndustryInfo>false</ReturnMailingIndustryInfo>"
				+ "<ReturnResidentialIndicator>true</ReturnResidentialIndicator>"
				+ "<ReturnStreetAbbreviated>true</ReturnStreetAbbreviated>"
				+ "<SecondaryAddressLine>" + strAddress2 + "</SecondaryAddressLine>"
				+ "<State>" + strState + "</State>"
				+ "<Urbanization>" + strUrbanization + "</Urbanization>"
				+ "<ZipCode>" + strZipcode + "</ZipCode>"
				+ "</PavRequest>";
	
			HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "text/xml; charset=utf-8");
			headers.put("Content-Length", "" + body.length());
	
			return postMethod(url, headers, body);
		}catch(Exception ex){
			return "";
		}
	}
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
int section=2;
int section_page=4;
%>
<%@ include file="/includes/security.jsp" %>
<%
String strAction, strRecipient, strAddress1, strAddress2, strUrbanization, strZipcode, strCity, strState;

strAction = request.getParameter("action");
strRecipient = request.getParameter("FirmOrRecipient");
strAddress1 = request.getParameter("PrimaryAddressLine");
strAddress2 = request.getParameter("SecondaryAddressLine");
strUrbanization = request.getParameter("Urbanization");
strZipcode = request.getParameter("ZipCode");
strCity = request.getParameter("CityName");
strState = request.getParameter("State");

if(strAction.equals("VerifyAddressAdvanced")){
	response.resetBuffer();
	response.setContentType("text/xml");
	String xml = VerifyAddressAdvanced(strRecipient, strAddress1, strAddress2, strUrbanization, strZipcode, strCity, strState);
	%><%=xml%><%
}
%>