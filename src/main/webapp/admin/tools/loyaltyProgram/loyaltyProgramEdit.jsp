<%-- 
    Document   : stockLevelsThreshold
    Created on : Mar 7, 2016, 4:05:23 PM
    Author     : dgarzon
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsRedeemValuesPojo"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.AccountSource"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsProgramPojo"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsProgram"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 17;
    int section_page = 1;
    //coment
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="js/referralAgent.js"></script>
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>


<%    String programId = request.getParameter("programId");
    String iso_id = SessionData.getProperty("iso_id");
    String actionValue = "insertLoyaltyProgram";
    String textWarning = "";

    LoyaltyPointsProgram program = new LoyaltyPointsProgram();
    List<AccountSource> accountList = program.getAccountSourceList();
    int xValue = 1;
    int countValue = 0;

    String name = "";
    String startDate = "";
    String endDate = "";
    String accountSource = "";
    int points = 0;
    double valuePoint = 0;
    String valueType = "";
    boolean smsCheck = false;
    String smsMessage = "";
    String loyaltyProgramId = "";
    List<LoyaltyPointsRedeemValuesPojo> redeemValuesList = new ArrayList<LoyaltyPointsRedeemValuesPojo>();

    if (programId != null && !programId.trim().equals("null")) {
        LoyaltyPointsProgramPojo mainPojo = LoyaltyPointsProgram.getLoyaltyPointsProgramByCarrier(programId, iso_id).get(0);
        redeemValuesList = LoyaltyPointsProgram.getLoyaltyPointsRedeemValues(mainPojo.getId());
        name = mainPojo.getName();
        accountSource = mainPojo.getAccountSourceId();
        points = mainPojo.getPoints();
        valuePoint = mainPojo.getValue();
        valueType = mainPojo.getValueType();
        smsCheck = mainPojo.isSmsNotifications();
        smsMessage = mainPojo.getSmsMessage();
        loyaltyProgramId = mainPojo.getId();
        actionValue = "editLoyaltyProgram";

        countValue = redeemValuesList.size();
        // 03/15/2016
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        if (mainPojo.getStartDate() != null) {
            startDate = format.format(new Date(mainPojo.getStartDate().getTime()));
        }
        if (mainPojo.getEndDate() != null) {
            endDate = format.format(new Date(mainPojo.getEndDate().getTime()));
        }
        //SessionData.setProperty("start_date", startDate);
        //SessionData.setProperty("end_date", endDate);
    }

%>

<script LANGUAGE="JavaScript">
    $(function () {
        $("#spinnerP").spinner({min: 0});
        $("#spinnerV").spinner({min: 0});
        $(".spinner").spinner({min: 0});
        spinnerNumeric();

    });


    $(document).ready(function () {
        var maxField = 15; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('#tableRedeem'); //Input field wrapper
        var x = <%=xValue%>; //Initial field counter is 1
        var count = <%=countValue%>;
        $(addButton).click(function () { //Once add button is clicked            

            var fieldHTML = '';
            fieldHTML += '<tr id="trId_' + count + '">';
            fieldHTML += '<td class="main" align="left"><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.points", SessionData.getLanguage())%> </td>';
            fieldHTML += '<td><input id="spinnerPoints_' + count + '" type="text" class="spinner" name="spinnerPoints"/></td>';
            fieldHTML += '<td class="main" align="left"> = </td>';
            fieldHTML += '<td><input id="spinnerValue_' + count + '" type="text" class="spinner" name="spinnerValues"/></td>';
            fieldHTML += '<td><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.bonusAdded", SessionData.getLanguage())%></td>';
            fieldHTML += '<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="images/details_close.png"/></a></td>';
            fieldHTML += '</tr>';

            if (x < maxField) { //Check maximum number of input fields
                $("#countRow").val(count);
                x++; //Increment field counter                
                count++;
                $('#tableRedeem').append(fieldHTML); // Add field html
                $(".spinner").spinner({min: 0});
                spinnerNumeric();

            }

        });
        $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('td').parent('tr').remove();
            x--; //Decrement field counter
        });

        

    });
    
    function spinnerNumeric (){
        $("input:text.spinner").keydown(function (event) { // fields with class numeric
            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) ||
                    (event.keyCode >= 96 && event.keyCode <= 105) ||
                    event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                    event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

            } else {
                event.preventDefault();
            }

            if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190) {
                event.preventDefault();
            }
            //if a decimal has been added, disable the "."-button

        });
    }

    function validateForm() {
        removeRedBorder();
        var name = $("#name").val();
        var spinnerP = $.isNumeric($("#spinnerP").val());
        var spinnerV = $.isNumeric($("#spinnerV").val());
        if (!spinnerP || !spinnerV || name == '') {
            markErrorFields();
            return false;
        }

        var allOkPoints = false;
        var spinnerPoints = document.getElementsByName("spinnerPoints");
        var spinnerValues = document.getElementsByName("spinnerValues");
        for (i = 0; i < spinnerPoints.length; i++) {
            if ($.trim(spinnerPoints[i].value) !== '' && $.trim(spinnerValues[i].value) !== '') {
                allOkPoints = true;
                i = spinnerPoints.length;
            }
        }


        if (allOkPoints) {
            return true;
        }
        else {
            $("#spanWarningRows").text("<%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.errorInsertRow", SessionData.getLanguage())%> ").show().fadeOut(10000);
            return false;
        }

        return false;
    }

    function markErrorFields() {

        var name = $("#name").val();
        var spinnerPoints = $.isNumeric($("#spinnerP").val());
        var spinnerValue = $.isNumeric($("#spinnerV").val());


        if (name == '') {
            $("#name").css("border", "1px solid red");
        }
        if (!spinnerPoints) {
            $("#spinnerP").css("border", "1px solid red");
        }
        if (!spinnerValue) {
            $("#spinnerV").css("border", "1px solid red");
        }

    }

    function removeRedBorder() {
        $("#name").css('border', '');
        $("#spinnerP").css('border', '');
        $("#spinnerV").css('border', '');
    }

    function goBackLoyalty() {
        if (isInternetExplorer()) {
            location.assign('/support/loyalty/loyaltyProgram/historical');
        }
        else {
            
            window.location.href = "loyalty/loyaltyProgram/historical";
        }
    }


</script>

<style type="text/css">
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }
    #spanInfo{
        font-size: 20px;
        color: #088A08;
    }

    #spanWarningRows{
        font-size: 17px;
        color: red;
    }

</style>

<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.title", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width=100% align=center>

                <tr>
                    <td class="formArea2" width="100%">

                        <table>
                            <tr>
                                <td colspan="4">	
                                    <form id="mainform" name="mainform" class="reportForm" method="post" action="admin/tools/loyaltyProgram/loyaltyProgramVerifyDB.jsp" onsubmit="return  validateForm();">
                                        <table border="0" cellpadding="2" cellspacing="6" width=100% align="center">
                                            <tr>
                                                <td class="main"><br>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="main" align="left" ><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.startDate", SessionData.getLanguage())%></td>
                                                <td class="main" align="left" >
                                                    <input class="plain" id="startDate" name="startDate" value="<%=(!startDate.equals("")) ? startDate : SessionData.getProperty("start_date")%>" size="12">
                                                    <a href="javascript:void(0)" onclick="if (self.gfPop)
                                                                gfPop.fStartPop(document.mainform.startDate, document.mainform.endDate);
                                                            return false;"HIDEFOCUS>
                                                        <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22"border="0" alt=""> </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="main" align="left" ><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.endDate", SessionData.getLanguage())%></td>
                                                <td class="main" align="left" >
                                                    <input class="plain" id="endDate" name="endDate" value="<%=(!endDate.equals("")) ? endDate : SessionData.getProperty("end_date")%>" size="12">
                                                    <a href="javascript:void(0)" onclick="if (self.gfPop)
                                                                gfPop.fEndPop(document.mainform.startDate, document.mainform.endDate);
                                                            return false;" HIDEFOCUS>
                                                        <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""> </a>
                                                </td>

                                            </tr>
                                            <tr>

                                                <td class="main" align="left" ><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.transactionType", SessionData.getLanguage())%></td>
                                                <td>
                                                    <select name="accountSourceList" id = "accountSourceList">
                                                        <%
                                                            for (AccountSource accountSource1 : accountList) {%>  
                                                        <option value="<%=accountSource1.getId()%>" <%=(accountSource1.getId().equalsIgnoreCase(accountSource)) ? "selected" : ""%>><%=accountSource1.getDescription()%></option>
                                                        <%}%>
                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td class="main" align="left"><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.name", SessionData.getLanguage())%></td>
                                                <td class="main" align="left"><input id="name" name="name" type="text" value="<%=name%>"/></td>
                                            </tr>

                                            <tr >
                                                <td class="main" align="left"><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.points", SessionData.getLanguage())%> </td>
                                                <td><input id="spinnerP" name="spinnerP" type="text" value="<%=points%>" class="spinner"/></td>
                                                <td> = <input id="spinnerV" name="spinnerV" type="text" value="<%=valuePoint%>" class="spinner"/></td>
                                                <td>
                                                    <select name="valueType" id = "valueType">
                                                        <option value="PURCHASE" <%=("PURCHASE".equalsIgnoreCase(valueType)) ? "selected" : ""%>>Dollars purchased</option>
                                                        <option value="TRANSACTIONS" <%=("TRANSACTIONS".equalsIgnoreCase(valueType)) ? "selected" : ""%>>Transactions</option>
                                                    </select>
                                                </td>
                                            </tr>


                                            <tr>
                                                <!-- redeem points-->
                                                <td colspan="6">
                                                    <fieldset>
                                                        <legend id="legendIndividualRecords"><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.redeemPoints", SessionData.getLanguage())%></legend>
                                                        <a href="javascript:void(0);" class="add_button" title="Add field"><img src="images/details_open.png"/></a>
                                                        <span id="spanWarningRows"><%=textWarning%></span>
                                                        <table id="tableRedeem" class= "tableRedeem" border="0" cellpadding="2" cellspacing="6" width=100% align="center">

                                                            <%
                                                                for (LoyaltyPointsRedeemValuesPojo redeem : redeemValuesList) {
                                                                    countValue++;
                                                            %>

                                                            <tr class="roww<%=(countValue % 2 == 0) ? "1" : "2"%>" id="trId_<%=countValue%>">


                                                                <td class="main" align="left"><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.points", SessionData.getLanguage())%> </td>
                                                                <td><input id="spinnerPoints_<%=countValue%>" class="spinner" type="text" name="spinnerPoints" value = "<%=redeem.getPoints()%>"/></td>
                                                                <td class="main" align="left"> = </td>
                                                                <td><input id="spinnerValue_<%=countValue%>" type="text" class="spinner" name="spinnerValues" value = "<%=redeem.getFreeValue()%>"/></td>
                                                                <td><%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.bonusAdded", SessionData.getLanguage())%></td>
                                                                <td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="images/details_close.png"/></a></td>
                                                            </tr>
                                                            <%}%>


                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="1">
                                                    <input type="checkbox" id="checkSMS" name="checkSMS" <%=(smsCheck) ? "checked" : ""%>/>    
                                                </td>
                                                <td colspan="5">
                                                    <%=Languages.getString("jsp.includes.menu.tools.loyaltyProgram.smsConfirmMessage", SessionData.getLanguage())%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <textarea rows="6" cols="70" id="smsMessage" name="smsMessage"><%if (smsMessage.equals("")) {%> You just to earning a free top-up. - Check at Carrier Page. www.carrier.com <%} else {%><%=smsMessage%> <%}%>
                                                    </textarea>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <input type="hidden" name="loyaltyProgramId" id="loyaltyProgramId" value="<%=loyaltyProgramId%>" />
                                                    <input type="hidden" name="action" id="action" value="<%=actionValue%>" />
                                                    <input type="hidden" name="countRow" id="countRow" value="<%=countValue%>" />
                                                </td>

                                                <td colspan="1" align="right"> <input type="button" name="button" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.back", SessionData.getLanguage())%>" onclick="goBackLoyalty();"></td>
                                                <td align="center">
                                                    <input type="submit" name="submit" id="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonVerifyAndProcess", SessionData.getLanguage())%>" align="center" />
                                                </td>

                                            </tr>


                                        </table>
                                    </form>
                                </td>
                            </tr>

                            <tr>
                                <td>


                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>


<%@ include file="/includes/footer.jsp" %>
