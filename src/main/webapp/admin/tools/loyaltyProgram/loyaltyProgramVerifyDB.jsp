<%-- 
    Document   : loyaltyProgramVerifyDB
    Created on : Mar 7, 2016, 4:11:16 PM
    Author     : dgarzon
--%>

<%@page import="java.util.Locale"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsProgram"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsProgramPojo"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsRedeemValuesPojo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String action = request.getParameter("action");

    if (action.equals("insertLoyaltyProgram") || action.equals("editLoyaltyProgram")) {

        String[] spinnerPoints = request.getParameterValues("spinnerPoints");
        String[] spinnerValues = request.getParameterValues("spinnerValues");
        List<LoyaltyPointsRedeemValuesPojo> redeemValuesList = new ArrayList<LoyaltyPointsRedeemValuesPojo>();
        for (int i = 0; i < spinnerPoints.length; i++) {
            if (spinnerPoints[i].trim() != "" && spinnerValues[i].trim() != "") {
                LoyaltyPointsRedeemValuesPojo pojo = new LoyaltyPointsRedeemValuesPojo();
                pojo.setPoints(Integer.parseInt(spinnerPoints[i].trim()));
                pojo.setFreeValue(Integer.parseInt(spinnerValues[i].trim()));
                redeemValuesList.add(pojo);
            }
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String name = request.getParameter("name");
        String accountSourceList = request.getParameter("accountSourceList");
        String spinnerP = request.getParameter("spinnerP");
        String spinnerV = request.getParameter("spinnerV");
        String valueType = request.getParameter("valueType");
        String checkSMS = request.getParameter("checkSMS");
        String smsMessage = request.getParameter("smsMessage");

        LoyaltyPointsProgramPojo programPojo = new LoyaltyPointsProgramPojo();

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss",Locale.US); // 03/20/2016        
        if (startDate != null && !startDate.trim().equals("")) {
            startDate += " 00:00:00";
            Date sDate = format.parse(startDate);
            programPojo.setStartDate(new Timestamp(sDate.getTime()));
        } else {
            programPojo.setStartDate(null);
        }

        if (endDate != null && !endDate.trim().equals("")) {
            endDate += " 00:00:00";
            Date eDate = format.parse(endDate);
            programPojo.setEndDate(new Timestamp(eDate.getTime()));
        } else {
            programPojo.setEndDate(null);
        }

        programPojo.setName(name);
        programPojo.setAccountSourceId(accountSourceList);
        programPojo.setCarrierId(new BigDecimal(SessionData.getProperty("iso_id")));
        programPojo.setPoints(Integer.parseInt(spinnerP));
        programPojo.setValue(Integer.parseInt(spinnerV));
        programPojo.setValueType(valueType);
        programPojo.setSmsNotifications((checkSMS == null) ? false : true);
        programPojo.setSmsMessage(smsMessage.trim());
        programPojo.setActive(true);

        LoyaltyPointsProgram loyaltyPointsProgram = new LoyaltyPointsProgram();
        if (action.equals("insertLoyaltyProgram")) {
            loyaltyPointsProgram.insertLoyaltyPointsProgram(SessionData, application, programPojo, redeemValuesList);
        } 
        else if (action.equals("editLoyaltyProgram")) {
            String loyaltyProgramId = request.getParameter("loyaltyProgramId");
            programPojo.setId(loyaltyProgramId);
            loyaltyPointsProgram.updateLoyaltyPointsProgram(programPojo, redeemValuesList);
        }
        response.sendRedirect(request.getContextPath()+"/loyalty/loyaltyProgram/historical");

    }    
    else if (action.equals("deleteLoyaltyProgram")) {
        String id = request.getParameter("programIdRemove");
        LoyaltyPointsProgram loyaltyPointsProgram = new LoyaltyPointsProgram();
        loyaltyPointsProgram.deleteLoyaltyPointsProgram(SessionData, application, id, SessionData.getProperty("iso_id"));
        response.sendRedirect("loyaltyProgramConf.jsp##");

    } 
    else if (action.equals("enableDisableProgram")) {
        String id = request.getParameter("programId");
        LoyaltyPointsProgram loyaltyPointsProgram = new LoyaltyPointsProgram();
        loyaltyPointsProgram.enableDisableProgram(id,SessionData.getProperty("iso_id"));
        response.sendRedirect("loyaltyProgramConf.jsp##");
    }

%>

