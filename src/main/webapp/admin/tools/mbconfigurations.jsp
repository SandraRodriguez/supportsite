<%@page import="java.util.Vector"%>
<%@page import="com.debisys.terminals.Terminal"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.utils.StringUtil"%>
<%
int section = 9;
int section_page = 19;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
if ( request.getParameter("SiteID") == null )
{
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
}
else
{
%>
<html>
<head>
<title><%=request.getParameter("SiteID")%></title>
</head>
<body>
<link href="default.css" type="text/css" rel="stylesheet">
<%
}
%>
<link href="/support/includes/sortROC.css" type="text/css" rel="StyleSheet" />
<%
if ( request.getParameter("SiteID") == null && (StringUtil.toString(request.getParameter("option")).equals("") || StringUtil.toString(request.getParameter("option")).equals("remove") || StringUtil.toString(request.getParameter("option")).equals("add")) )
{
	if ( StringUtil.toString(request.getParameter("option")).equals("remove") )
	{
		try
		{
			(new Terminal()).deleteMBConfiguration(Integer.parseInt(request.getParameter("id")));
		}
		catch (Exception e)
		{
		}
	}
	else if ( StringUtil.toString(request.getParameter("option")).equals("add") )
	{
		try
		{
			(new Terminal()).addMBConfiguration(request.getParameter("AddName"), null, null);
		}
		catch (Exception e)
		{
		}
	}
%>
<table border="0" cellpadding="0" cellspacing="0" width="500px">
	<tr>
		<td background="/support/images/top_blue.gif" width="1%" align="left"><img src="/support/images/top_left_blue.gif" width="18" height="20"></td>
		<td background="/support/images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.customer.terminal.EditMBConfiguration",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="/support/images/top_blue.gif" width="1%" align="right"><img src="/support/images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2" align="center">
			<table width="95%" cellspacing="1" cellpadding="1" border="0">
				<tr class="SectionTopBorder">
					<td class="rowhead2" align="center" valign="top"><%=Languages.getString("jsp.admin.customer.terminal.ConfID",SessionData.getLanguage()).toUpperCase()%></td>
					<td class="rowhead2" align="center" valign="top"><%=Languages.getString("jsp.admin.customer.terminal.ConfName",SessionData.getLanguage()).toUpperCase()%></td>
					<td align="center" valign="top"></td>
					<td align="center" valign="top"></td>
				</tr>
<%
	int nRow = 1;
	Iterator<Vector<String>> it = (new Terminal()).getMBConfigurations().iterator();
	while ( it.hasNext() )
	{
		Vector<String> vTmp = it.next();
%>
				<tr class="row<%=nRow%>">
					<td align="center"><%=vTmp.get(0)%></td>
					<td align="center"><%=vTmp.get(1)%></td>
					<td align="center"><a href="javascript:" onclick="DoEdit(<%=vTmp.get(0)%>);"><%=Languages.getString("jsp.admin.customer.terminal.ConfEdit",SessionData.getLanguage())%></a></td>
<%
		if ( vTmp.get(2).equals("0") && !vTmp.get(1).equals("Default") )
		{
%>
					<td align="center"><a href="javascript:" onclick="DoRemove(<%=vTmp.get(0)%>);"><%=Languages.getString("jsp.admin.customer.terminal.ConfRemove",SessionData.getLanguage())%></a></td>
<%
		}
		else
		{
			out.println("<td></td>");
		}
%>
				</tr>
<%
		vTmp = null;
		nRow = (nRow == 1)?2:1;
	}
	it = null;
%>
				<tr><td colspan="4" align="center"><br/><br/><hr/></td></tr>
				<tr><td class="rowhead2" colspan="4" align="center"><%=Languages.getString("jsp.admin.customer.terminal.ConfAddConfig",SessionData.getLanguage()).toUpperCase()%></td></tr>
				<tr class="SectionTopBorder">
					<td class="rowhead2" align="center" valign="top" colspan="4"><%=Languages.getString("jsp.admin.customer.terminal.ConfName",SessionData.getLanguage()).toUpperCase()%></td>
				</tr>
				<tr class="main">
					<td align="center" colspan="4"><input type="text" id="name_0" value="" size="20" maxlength="50" onblur="this.value=this.value.replace(/\s/g, '');"></td>
				</tr>
				<tr>
					<td colspan="4" align="center">
						<input type="button" value="<%=Languages.getString("jsp.admin.customer.terminal.ConfAddConfig",SessionData.getLanguage())%>" onclick="DoAdd();">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<form id="mbForm" method="post">
	<input type="hidden" id="id" name="id">
	<input type="hidden" id="option" name="option">
	<input type="hidden" id="AddName" name="AddName">
</form>
<script>
function DoEdit(nVal)
{
	document.getElementById("id").value = nVal;
	document.getElementById("option").value = "edit";
	document.getElementById("mbForm").submit();
}
function DoRemove(nVal)
{
	if ( confirm("<%=Languages.getString("jsp.admin.customer.terminal.ConfRemoveItem",SessionData.getLanguage())%>") )
	{
		document.getElementById("id").value = nVal;
		document.getElementById("option").value = "remove";
		document.getElementById("mbForm").submit();
	}
}
function DoAdd()
{
	var sName = document.getElementById("name_0").value;
	if ( sName.length == 0 )
	{
		alert("<%=Languages.getString("jsp.admin.customer.terminal.ConfPleaseEnterName",SessionData.getLanguage())%>");
		return;
	}
	document.getElementById("AddName").value = sName;
	document.getElementById("option").value = "add";
	document.getElementById("mbForm").submit();
}
</script>
<%
}
else if ( request.getParameter("option").equals("edit") )
{
	String sRequestId = request.getParameter("id");
	if ( request.getParameter("add") != null )
	{
		try
		{
			(new Terminal()).addMBConfigurationData(Integer.parseInt(sRequestId), request.getParameter("addName"), request.getParameter("addValue"));
		}
		catch (Exception e)
		{
		}
	}
	else if ( request.getParameter("remove") != null )
	{
		try
		{
			(new Terminal()).deleteMBConfigurationData(Integer.parseInt(request.getParameter("itemId")));
		}
		catch (Exception e)
		{
		}
	}
	else if ( request.getParameter("save") != null )
	{
		Vector<Vector<String>> vParams = new Vector<Vector<String>>();
		Enumeration<String> eParams = request.getParameterNames();
		while (eParams.hasMoreElements())
		{
			String sName = eParams.nextElement();
			if ( sName.startsWith("name_") )
			{
				Vector<String> vItem = new Vector<String>();
				int nId = Integer.parseInt(sName.split("_")[1]);
				vItem.add(request.getParameter(sName));
				vItem.add(request.getParameter("value_" + Integer.toString(nId)));
				vItem.add(Integer.toString(nId));
				vParams.add(vItem);
			}
		}
		try
		{
			(new Terminal()).saveMBConfigurationData(vParams);
			(new Terminal()).saveMBConfiguration(Integer.parseInt(sRequestId), request.getParameter("Description"));
		}
		catch (Exception e)
		{
		}
	}

	Vector<Vector<String>> vData = null;
	if ( request.getParameter("SiteID") == null )
	{
		vData = (new Terminal()).getMBConfigurationData(Integer.parseInt(sRequestId));
	}
	else
	{
		vData = (new Terminal()).getMBConfigurationDataByTerminal(request.getParameter("SiteID"));
		if ( vData.size() == 0 )
		{
			if (request.getParameter("mconfID") != null)
				sRequestId = Integer.toString((new Terminal()).addMBConfigurationExt(request.getParameter("SiteID"), request.getParameter("SiteID"), request.getParameter("mid"),request.getParameter("mconfID"), true));
			else
				sRequestId = Integer.toString((new Terminal()).addMBConfiguration(request.getParameter("SiteID"), request.getParameter("SiteID"), request.getParameter("mid")));
			vData = (new Terminal()).getMBConfigurationDataByTerminal(request.getParameter("SiteID"));
		}
		else
		{
			sRequestId = Integer.toString((new Terminal()).getMBConfigurationByTerminal(request.getParameter("SiteID")));
		}
		Vector<String> vTmp = new Vector<String>();
		vTmp.add("Terminal: " + request.getParameter("SiteID"));
		vData.add(0, vTmp);
	}
	
%>
<form method="post">
	<input type="hidden" id="option" name="option" value="edit">
	<input type="hidden" id="save" name="save" value="">
	<input type="hidden" name="id" value="<%=sRequestId%>">
	<table border="0" cellpadding="0" cellspacing="0" width="500px">
		<tr>
			<td background="/support/images/top_blue.gif" width="1%" align="left"><img src="/support/images/top_left_blue.gif" width="18" height="20"></td>
			<td background="/support/images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=vData.get(0).get(0)%></b></td>
			<td background="/support/images/top_blue.gif" width="1%" align="right"><img src="/support/images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2" align="center">
<%
	if ( request.getParameter("SiteID") == null )
	{
%>
				<br/><span class="main"><%=Languages.getString("jsp.admin.customer.terminal.ConfDescription",SessionData.getLanguage())%></span><br/>
				<textarea name="Description" rows="3" cols="30" onblur="ValidateDesc(this);"><%=StringUtil.toString(vData.get(0).get(1))%></textarea><br/><br/>
<%
	}
	else
	{
		out.println("<input type=\"hidden\" name=\"Description\" value=\"\">");
	}
%>
<script>
function ValidateField(e, c)
{
	var exp = new RegExp(e);
	//exp.compile(e);

	if ( !exp.test(c.value) )
	{
		alert('<%=Languages.getString("jsp.admin.errorExpressionInvalid",SessionData.getLanguage())%>');
		window.setTimeout("document.getElementById('" + c.id + "').focus();document.getElementById('" + c.id + "').select();", 50);
		return false;
	}
	return true;
}
</script>
				<table width="95%" cellspacing="1" cellpadding="1" border="0">
<%
	if ( vData.size() > 1 )
	{
%>
					<tr class="SectionTopBorder">
						<td class="rowhead2" align="center" valign="top"><%=Languages.getString("jsp.admin.customer.terminal.ConfName",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center" valign="top"><%=Languages.getString("jsp.admin.customer.terminal.ConfValue",SessionData.getLanguage()).toUpperCase()%></td>
					</tr>
<%
		int nRow = 1;
		Iterator<Vector<String>> it = vData.iterator();
		it.next();
		while ( it.hasNext() )
		{
			Vector<String> vTmp = it.next();
			String sReadOnly = "";
			String sValidateExp = "";
			if ( vTmp.get(1).equals("$NIG_CALL_ID") || vTmp.get(1).equals("NIG_CALL") )
			{
				sReadOnly = "readonly";
			}
			if ( vTmp.get(1).equals("ADMPWD") )
			{
				sValidateExp = "ValidateField('^[0-9a-zA-Z]+$', this);";
			}
			else if ( vTmp.get(1).equals("AUTO_JOUR") || vTmp.get(1).equals("DAY_DB") || vTmp.get(1).equals("DISCONN") || vTmp.get(1).equals("FLAG_AB")
							|| vTmp.get(1).equals("FLAG_AGP") || vTmp.get(1).equals("FLAG_AS") || vTmp.get(1).equals("FLAG_SC") || vTmp.get(1).equals("LOGO_APP")
							|| vTmp.get(1).equals("PREDIAL") || vTmp.get(1).equals("PRINT_REC") || vTmp.get(1).equals("RCP_COPY") || vTmp.get(1).equals("REPORT_LOG")
							|| vTmp.get(1).equals("FLAG_MD") || vTmp.get(1).equals("MBAUTHR") )
			{
				sValidateExp = "ValidateField('^[01]$', this);";
			}
			else if ( vTmp.get(1).equals("NIG_CALL_START") )
			{//This validates a HHmmss format
				sValidateExp = "ValidateField('^(([01][0-9])|20|21|22|23)[012345][0-9][012345][0-9]$', this);";
			}
			else if ( vTmp.get(1).equals("RAND_HRS") )
			{
				sValidateExp = "ValidateField('^[0-9]$', this);";
			}
			else if ( vTmp.get(1).equals("LANG") )
			{
				sValidateExp = "ValidateField('^(ENG|SPA)$', this);";
			}
%>
					<tr class="row<%=nRow%>">
						<td align="center"><%=Languages.getString("jsp.admin.customer.terminal.ConfTxt_" + vTmp.get(1),SessionData.getLanguage())%><input type="hidden" name="name_<%=vTmp.get(0)%>" value="<%=vTmp.get(1)%>"></td>
						<td align="left"><input type="text" id="value_<%=vTmp.get(0)%>" name="value_<%=vTmp.get(0)%>" value="<%=vTmp.get(2)%>" size="10" maxlength="50" onblur="this.value=this.value.replace(/\s/g, '');<%=sValidateExp%>" <%=sReadOnly%>>
<%
			String sHint = Languages.getString("jsp.admin.customer.terminal.Conf_" + vTmp.get(1),SessionData.getLanguage());
			if ( !sHint.equals("default") )
			{
%>
							<img src="/support/images/help.gif" title="<%=sHint%>">
<%
			}
%>
						</td>
					</tr>
<%
			vTmp = null;
			nRow = (nRow == 1)?2:1;
		}
		it = null;
	}
	else
	{
%>
					<tr class="main">
						<td colspan="2" align="center">
							<%=Languages.getString("jsp.admin.customer.terminal.ConfNoItems",SessionData.getLanguage())%>
						</td>
					</tr>
<%
	}

	if ( request.getParameter("SiteID") == null || (request.getParameter("SiteID") != null && vData.size() > 1) )
	{
%>
					<tr>
						<td colspan="2" align="center">
							<br/><input type="submit" value="<%=Languages.getString("jsp.admin.customer.terminal.ConfSave",SessionData.getLanguage())%>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value="<%=Languages.getString("jsp.admin.customer.terminal.ConfReset",SessionData.getLanguage())%>">
						</td>
					</tr>
<%
	}
%>
<!-- <%/*Commented out because we don't want users to add configuration keys*/%>
					<tr><td colspan="4" align="center"><br/><br/><br/><br/><hr/></td></tr>
					<tr><td class="rowhead2" colspan="4" align="center"><%=Languages.getString("jsp.admin.customer.terminal.ConfAddParam",SessionData.getLanguage())%></td></tr>
					<tr class="SectionTopBorder">
						<td class="rowhead2" align="center" valign="top" colspan="2"><%=Languages.getString("jsp.admin.customer.terminal.ConfName",SessionData.getLanguage())%></td>
						<td class="rowhead2" align="center" valign="top" colspan="2"><%=Languages.getString("jsp.admin.customer.terminal.ConfValue",SessionData.getLanguage())%></td>
					</tr>
					<tr class="main">
						<td align="center"></td>
						<td align="center"><input type="text" id="name_0" value="" size="20" maxlength="50" onblur="this.value=this.value.replace(/\s/g, '');"></td>
						<td align="center"><input type="text" id="value_0" value="" size="10" maxlength="50" onblur="this.value=this.value.replace(/\s/g, '');"></td>
						<td align="center"></td>
					</tr>
					<tr>
						<td colspan="4" align="center">
							<input type="button" value="<%=Languages.getString("jsp.admin.customer.terminal.ConfAddParam",SessionData.getLanguage())%>" onclick="DoAdd();">
						</td>
					</tr>
 -->
<%
	if ( request.getParameter("SiteID") == null )
	{
%>
					<tr><td colspan="2" align="center">
						<hr/>
						<input type="button" value="<%=Languages.getString("jsp.admin.customer.terminal.ConfBackToList",SessionData.getLanguage())%>" onclick="document.getElementById('ReturnForm').submit();">
					</td></tr>
<%
	}
	else
	{
%>
					<tr><td colspan="2" align="center">
						<hr/>
						<input type="button" value="<%=Languages.getString("jsp.admin.customer.terminal.ConfClose",SessionData.getLanguage())%>" onclick="self.close();">
					</td></tr>
<%
	}
%>
				</table>
			</td>
		</tr>
	</table>
</form>
<form id="AddForm" method="post">
	<input type="hidden" name="option" value="edit">
	<input type="hidden" name="add" value="">
	<input type="hidden" name="id" value="<%=sRequestId%>">
	<input type="hidden" id="addName" name="addName">
	<input type="hidden" id="addValue" name="addValue">
</form>
<form id="RemoveForm" method="post">
	<input type="hidden" name="option" value="edit">
	<input type="hidden" name="remove" value="">
	<input type="hidden" name="id" value="<%=sRequestId%>">
	<input type="hidden" id="itemId" name="itemId">
</form>
<form id="ReturnForm" method="post">
</form>
<script>
function ValidateDesc(ctl)
{
	if ( ctl.value.length > 255 )
	{
		ctl.value = ctl.value.substring(0, 254);
	}
};
function DoAdd()
{
	var sName = document.getElementById("name_0").value;
	var sValue = document.getElementById("value_0").value;
	if ( sName.length == 0 || sValue.length == 0 )
	{
		alert("<%=Languages.getString("jsp.admin.customer.terminal.ConfPleaseEnter",SessionData.getLanguage())%>");
		return;
	}
	document.getElementById("addName").value = sName;
	document.getElementById("addValue").value = sValue;
	document.getElementById("AddForm").submit();
}
function DoRemove(nId)
{
	if ( confirm("<%=Languages.getString("jsp.admin.customer.terminal.ConfRemoveItem",SessionData.getLanguage())%>") )
	{
		document.getElementById("itemId").value = nId;
		document.getElementById("RemoveForm").submit();
	}
}
</script>
<%
}

if ( request.getParameter("SiteID") == null )
{
%>
<%@ include file="/includes/footer.jsp" %>
<%
}
else
{
%>
</body>
</html>
<%
}
%>
