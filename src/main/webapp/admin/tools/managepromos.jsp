<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.*,
                 java.util.Hashtable,
                 com.debisys.promotions.*" %>
         

<%
int section=9;
int section_page=12;
String sISOID = "";
String sProductID = "";
String strInstance = "";
String strRepID = "";
String strPromoID = "";
String sCarrierID = "";
String strSProductList[]=null;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>

<%
  	Vector vecISOs = new Vector();
  	Vector vecProducts = new Vector();
  	Vector vecSearchResults = new Vector();
  	
	strInstance = SessionData.getProperty("instance");  	

	if (((request.getParameter("repId") == null)&&(request.getParameter("carrierId") == null))||SessionData.getUser().isIntranetUser()) {
		vecISOs = ISO.GetISOList();
	} else if ((strAccessLevel.equals(DebisysConstants.CARRIER))&&(request.getParameter("carrierId") != null)) {
		vecISOs = ISO.GetISOListByCarrier(Long.parseLong(request.getParameter("carrierId")));
	} else {
		sISOID = request.getParameter("repId");
		strRepID = request.getParameter("repId"); 
	}
	
  	if ((request.getParameter("isosID") != null) && NumberUtil.isNumeric(request.getParameter("isosID"))) {
		sISOID = request.getParameter("isosID");
	}
	if ((request.getParameter("carrierId") != null) && NumberUtil.isNumeric(request.getParameter("carrierId"))) {
		sCarrierID = request.getParameter("carrierId");
	}

	if (NumberUtil.isNumeric(sISOID)&&!NumberUtil.isNumeric(sCarrierID)) {
		vecProducts = Product.GetProductList(Long.parseLong(sISOID));
	}
	else if (NumberUtil.isNumeric(sCarrierID)) {
		if (NumberUtil.isNumeric(sISOID))
			vecProducts = Product.GetProductsByCarrier(Long.parseLong(sISOID),Long.parseLong(sCarrierID));
		else
			vecProducts = Product.GetProductsByCarrier(-1,Long.parseLong(sCarrierID));
	}
	if (request.getParameter("prsID") != null ) {
		sProductID = request.getParameter("prsID");
		
	}
	if (request.getParameter("op") != null) {
		if (request.getParameter("op").equals("inc")) {
			if (request.getParameter("promoID") != null && NumberUtil.isNumeric(request.getParameter("promoID"))) {
				Promotion.UpdatePriority(SessionData, strInstance, Integer.parseInt(request.getParameter("promoID")),-1,sProductID);
			}
		} else if (request.getParameter("op").equals("dec")) {
			if (request.getParameter("promoID") != null && NumberUtil.isNumeric(request.getParameter("promoID"))) {
				Promotion.UpdatePriority(SessionData, strInstance, Integer.parseInt(request.getParameter("promoID")),1,sProductID);
			}
		} else if (request.getParameter("op").equals("change")) {
			if (request.getParameter("promoID") != null && NumberUtil.isNumeric(request.getParameter("promoID"))) {
				Promotion.ChangeStatus(Integer.parseInt(request.getParameter("promoID")), SessionData, strInstance);
			}
		} else if (request.getParameter("op").equals("delete")) {
			if (request.getParameter("promoID") != null && NumberUtil.isNumeric(request.getParameter("promoID"))) {
				Promotion.RemovePromotion(Integer.parseInt(request.getParameter("promoID")), SessionData, strInstance);
			}
		} else if (request.getParameter("op").equals("dectop")) {
			if (request.getParameter("promoID") != null && NumberUtil.isNumeric(request.getParameter("promoID"))) {
				Promotion.UpdatePriority(SessionData, strInstance, Integer.parseInt(request.getParameter("promoID")),2,sProductID);
			}
		} else if (request.getParameter("op").equals("inctop")) {
			if (request.getParameter("promoID") != null && NumberUtil.isNumeric(request.getParameter("promoID"))) {
				Promotion.UpdatePriority(SessionData, strInstance, Integer.parseInt(request.getParameter("promoID")),-2,sProductID);
			}
		} else if (request.getParameter("op").equals("addnew")) {
	  		Promotion promotion = new Promotion();
	  		promotion.setActive(0);
	  		ISO iso = new ISO();
	  		if (NumberUtil.isNumeric(sISOID)) {
	  			iso.setIsoID(Long.parseLong(sISOID));
	  		}
	  		promotion.setISO(iso);
	  			Product pr = new Product();
			  	pr.setProductID(-1);
			  	promotion.setProduct(pr);
	
	  		/*if (NumberUtil.isNumeric(sProductID)) {
	  			pr.setProductID(Integer.parseInt(sProductID));
	  		}*/
	  	//	int priority=Promotion.GetHighestPriority(sProductID)+1;
	  	
	  		promotion.setPriority(0);
	  		
	  		promotion.setDescription("New Promo");
			promotion = Promotion.GetPromotionInfo(Promotion.InsertPromo(promotion, SessionData, strInstance));
			strPromoID = Integer.toString(promotion.getPromoId());
			/*Promotion.AddMerchant(promotion.getPromoId(), -1, 0, 1, SessionData, strInstance);
			Promotion.AddSite(promotion.getPromoId(), -1, 0, 1, SessionData, strInstance);
			Promotion.AddSite(promotion.getPromoId(), -1, 1, 1, SessionData, strInstance);
			Promotion.AddMerchant(promotion.getPromoId(), -1, 1, 1, SessionData, strInstance);*/
			response.sendRedirect("editpromotion.jsp?lblSource=edit&promoID=" + String.valueOf(promotion.getPromoId())+ "&repId=" + strRepID + "&carrierId=" + sCarrierID);
		}
	}

	if (request.getParameter("lblShowReport")!= null ) {
		if (request.getParameter("lblShowReport").equals("y")) {
			long ISOtempID = -1;
			long CarriertempID = -1;
			int ProdtempID = -1;
			if (NumberUtil.isNumeric(sISOID)) {
				ISOtempID = Long.parseLong(sISOID);
			}
			if (NumberUtil.isNumeric(sProductID)) {
				ProdtempID = Integer.parseInt(sProductID);
			}
			if (NumberUtil.isNumeric(sCarrierID)) {
				CarriertempID = Long.parseLong(sCarrierID);
			}
			vecSearchResults = Promotion.GetPromoList(ISOtempID,ProdtempID,CarriertempID);
		}
	}

%>
<%@ include file="/includes/header.jsp" %>


<script language="javascript">

function show_confirm(message) {
	var r = confirm(message);
	if (r == true) {
		return true;
	} else {
		return false;
	}
}


function showHide1()
{
 if(document.getElementById('prsID').value == "")
 {
 	document.getElementById('addNewButton').style.visibility='visible';
    document.getElementById('addNewButton').style.display='block';
 }
 else
 {
	document.getElementById('addNewButton').style.visibility='hidden';
 	document.getElementById('addNewButton').style.display='none';
 }
}

</script>

<body onload="showHide1();">

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.managepromotions.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td class="main">
	<form name="mainform" method="post" action="admin/tools/managepromos.jsp" >
		<table border="0" width="100%" cellpadding="0" cellspacing="0" class="formArea2">
			<tr >
			    <td align="right" colspan=2 >
	          	 	<input type="hidden" name="op" id="op"  value="">
	   		        <input id="repId" type="hidden" name="repId" value="<%=strRepID%>">
	   		        <input id="carrierId" type="hidden" name="carrierId" value="<%=sCarrierID%>">
	   		        <input type="submit" id="addNewButton" value="<%=Languages.getString("jsp.admin.tools.managepromotions.addpromotion",SessionData.getLanguage())%>" ONCLICK="document.getElementById('op').value='addnew';document.getElementById('lblShowReport').value='y';">						    
			    </td>    	
			</tr>
	    	<tr >
		    	<td >
	          		<br>
						<table width="300">
						<%	Iterator it = null;
							if (vecISOs != null && vecISOs.size() > 0)
							{
							%>						
							<tr class="main">
								<td nowrap valign="center">
									<%=Languages.getString("jsp.admin.tools.managepromotions.select_iso",SessionData.getLanguage())%>:
								</td>
								<td>
									<select id="isosID" name="isosID" onchange="javascript:document.forms[1].submit();">
										<option value=""><%=Languages.getString("jsp.admin.tools.managepromotions.view_all_isos",SessionData.getLanguage())%></option>
									<%
										it = vecISOs.iterator();
										while (it.hasNext()) {
											ISO isoTemp = null;
											isoTemp = (ISO) it.next();
											if (!sISOID.equals(String.valueOf(isoTemp.getIsoID()))) {
												out.println("<option value=\""+isoTemp.getIsoID()+"\">" +
													HTMLEncoder.encode(isoTemp.getIsoName()) + "</option>");
											}
											else {
												out.println("<option value=\""+isoTemp.getIsoID()+"\" selected>" +
													HTMLEncoder.encode(isoTemp.getIsoName()) + "</option>");
											}
										}
										vecISOs.clear();
									%>
									</select>
								</td>
							</tr>
							<%
							} // End If (vecISOs != null && vecISOs.size() >0)
							%>						
							<tr class="main">
								<td nowrap valign="center">
									<%=Languages.getString("jsp.admin.tools.managepromotions.select_product",SessionData.getLanguage())%>:
								</td>
								<td>
									<select id="prsID" name="prsID" onchange="showHide1();">
									<%
										if (sProductID.equals("0")||sProductID.equals("-1")||sProductID.equals(""))
										{
											//out.println("<option value=\"\">" + Languages.getString("jsp.admin.tools.managepromotions.view_all_products",SessionData.getLanguage()) + "</option>");
											out.println("<option value=\"\" selected >" + Languages.getString("jsp.admin.tools.managepromotions.all_products",SessionData.getLanguage()) + "</option>");
										}
										else
										{
											//out.println("<option value=\"\">" + Languages.getString("jsp.admin.tools.managepromotions.view_all_products",SessionData.getLanguage()) + "</option>");
											out.println("<option value=\"\">" + Languages.getString("jsp.admin.tools.managepromotions.all_products",SessionData.getLanguage()) + "</option>");
										} 
										it = vecProducts.iterator();
										while (it.hasNext()) {
											Product prodTemp = null;
											prodTemp = (Product) it.next();
											if (!sProductID.equals(String.valueOf(prodTemp.getProductID()))) {
												out.println("<option value=\""+prodTemp.getProductID()+"\">" +
													HTMLEncoder.encode(prodTemp.getProductDes()+"  (" + prodTemp.getProductID() + ")") + "</option>");
											}
											else {
												out.println("<option value=\""+prodTemp.getProductID()+"\" selected>" +
													HTMLEncoder.encode(prodTemp.getProductDes()+"  (" + prodTemp.getProductID() + ")") + "</option>");
											}
										}
										vecProducts.clear();
									%>
									</select>
								</td>
							</tr>
							<tr>
							    <td class=main colspan=2 align=center>
							      <input type="hidden" name="search" value="y">
							      <input id="repId" type="hidden" name="repId" value="<%=strRepID%>">
	   		        			  <input id="carrierId" type="hidden" name="carrierId" value="<%=sCarrierID%>">	
							      <input id="lblShowReport" type="hidden" name="lblShowReport" value="">
							      <input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.tools.managepromotions.show_promotions",SessionData.getLanguage())%>" ONCLICK="document.getElementById('btnSubmit').enable='false';document.getElementById('lblShowReport').value='y';">
							    </td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;</td>
								<td class=main>
									<%=sProductID!=""&&vecSearchResults.size() > 0?Languages.getString("jsp.admin.tools.managepromotions.priorityinfo",SessionData.getLanguage()):"" %>
								</td>	
							</tr>
						</table>
						
	<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
	%>
	 
	<table width="100%" cellspacing="1" cellpadding="1" border="0">     
	     <tr>
	        <td class=rowhead2><%=Languages.getString("jsp.admin.tools.managepromotions.promotion_id",SessionData.getLanguage()).toUpperCase()%></td>
	     	<td class=rowhead2><%=Languages.getString("jsp.admin.tools.managepromotions.name",SessionData.getLanguage()).toUpperCase()%></td>	
			<% if (sISOID == "") { %>
	     		<td class=rowhead2 width=85><%=Languages.getString("jsp.admin.tools.managepromotions.iso",SessionData.getLanguage()).toUpperCase()%></td>
	     	<% } // end if (sISOID == "") %>
	     	<td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.managepromotions.starting_date",SessionData.getLanguage()).toUpperCase()%></td>
	     	<td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.managepromotions.ending_date",SessionData.getLanguage()).toUpperCase()%></td>
	       	<td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.managepromotions.status",SessionData.getLanguage()).toUpperCase()%></td>
	       		<% if (sProductID == "") { %>
	     		<td class=rowhead2><%=Languages.getString("jsp.admin.tools.managepromotions.product",SessionData.getLanguage()).toUpperCase()+"&nbsp;&nbsp;["+Languages.getString("jsp.admin.tools.managepromotions.product_id",SessionData.getLanguage()).toUpperCase()+"]"%></td>
	     	<% } else {  %>
	     		<td class=rowhead2 width=85><%=Languages.getString("jsp.admin.tools.managepromotions.priority",SessionData.getLanguage()).toUpperCase()%></td>
			<% } // end if (sProductID == "") %>   
	       	<td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.managepromotions.actions",SessionData.getLanguage()).toUpperCase()%></td>
	       	  
	     </tr> 
	<%
	                  it = vecSearchResults.iterator();
	                  int intEvenOdd = 1;
	                  int icount=0;
	                  while (it.hasNext())
	                  {
	                    icount++;
	                    Promotion promTemp = (Promotion) it.next();
	                    out.println("<tr class=row" + intEvenOdd +"><td>");
	                    out.println("<table><tr class=row" + intEvenOdd + ">");
	                    if (strPromoID.equals(Integer.toString(promTemp.getPromoId()))) {
	                    	out.println("<td align=left> " + HTMLEncoder.encode(Integer.toString(promTemp.getPromoId())) + "</td><td class=\"errorText\">" + Languages.getString("jsp.admin.tools.managepromotions.new",SessionData.getLanguage()) + "</td>");
	                    } else {
	                    	out.println("<td align=left> " + HTMLEncoder.encode(Integer.toString(promTemp.getPromoId())) + "</td>");
	                    }
	                    out.println("</tr></table></td>");	
	                    out.println("<td nowrap><a href=\"admin/tools/editpromotion.jsp?lblSource=edit&promoID=" + String.valueOf(promTemp.getPromoId())+ "&repId=" + strRepID + "&carrierId=" + sCarrierID + "\">" + HTMLEncoder.encode(promTemp.getDescription())  + "</a></td>");                    
	                    //out.println("<td nowrap> " + HTMLEncoder.encode(promTemp.getDescription()) + "</td>");
	                   
	                    if (sISOID == "") {
	                    	out.println("<td width=150> " + HTMLEncoder.encode(promTemp.getISO().getIsoName()) + "</td>");                                        
	                    }
	                    out.println("<td align=left nowrap> " + HTMLEncoder.encode(promTemp.getStartDate()) + "</td>");
	                    out.println("<td align=left nowrap> " + HTMLEncoder.encode(promTemp.getEndDate()) + "</td>");
	                  //  out.println("<td align=left> " + HTMLEncoder.encode((promTemp.getActive() == 0)?Languages.getString("jsp.admin.tools.managepromotions.inactive",SessionData.getLanguage()):Languages.getString("jsp.admin.tools.managepromotions.active",SessionData.getLanguage())) + "&nbsp;&nbsp;"+ "(<a href=\"admin/tools/managepromos.jsp?isosID=" + sISOID + "&prsID=" + sProductID + "&op=change" + "&repId=" + strRepID + "&carrierId=" + sCarrierID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y" + "\">" + ((promTemp.getActive()==0)?Languages.getString("jsp.admin.tools.managepromotions.enable",SessionData.getLanguage()):Languages.getString("jsp.admin.tools.managepromotions.disable",SessionData.getLanguage())) + ")</a></td>");
	                     out.println("<td align=left> " + HTMLEncoder.encode((promTemp.getActive() == 0)?Languages.getString("jsp.admin.tools.managepromotions.inactive",SessionData.getLanguage()):Languages.getString("jsp.admin.tools.managepromotions.active",SessionData.getLanguage())) + "&nbsp;&nbsp;"+ "(<a href=\"admin/tools/promoConfirm.jsp?isosID=" + sISOID + "&prsID=" + sProductID +  "&repId=" + strRepID + "&carrierId=" + sCarrierID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y" + "\">" + ((promTemp.getActive()==0)?Languages.getString("jsp.admin.tools.managepromotions.enable",SessionData.getLanguage()):Languages.getString("jsp.admin.tools.managepromotions.disable",SessionData.getLanguage())) + ")</a></td>");
	                   
	                     // if (promTemp.getProduct().getProductID() == 0) {
	                    if (promTemp.getProduct().getProductID() == 0) {
							out.println("<td align=left width=150> " + Languages.getString("jsp.admin.tools.managepromotions.all_products",SessionData.getLanguage()) + "&nbsp;&nbsp;[" + HTMLEncoder.encode(Integer.toString(promTemp.getProduct().getProductID())) + "]" + "</td>");	                    
	                    } else 
	                    if (sProductID.equals("")) {
	                    	out.println("<td align=left width=150> " + HTMLEncoder.encode(promTemp.getProduct().getProductDes()) + "&nbsp;&nbsp;[" + HTMLEncoder.encode(Integer.toString(promTemp.getProduct().getProductID())) + "]" + "</td>");
	                    } else {
	                     out.println("<td><table class=main><tr>");
	                    	//out.println("<td align=left> " + HTMLEncoder.encode(Integer.toString(promTemp.getPriority())) + "</td>");    
		                    out.println("<td><table><tr><td><a href=\"admin/tools/managepromos.jsp?isosID=" + sISOID + "&prsID=" + sProductID + "&op=inc" + "&repId=" + strRepID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y" + "&carrierId=" + sCarrierID + "\"><img border=0 src=\"support/../images/uparrow.png\" with=\"15\" height=\"15\" />" + "</a></td>");
		                    out.println("<td><a href=\"admin/tools/managepromos.jsp?isosID=" + sISOID + "&prsID=" + sProductID + "&op=inctop" + "&repId=" + strRepID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y"  + "&carrierId=" + sCarrierID + "\"><img border=0 src=\"support/../images/topuparrow.png\" width=\"15\" height=\"15\" />" + "</a></td></tr>");
		                    out.println("<tr><td><a href=\"admin/tools/managepromos.jsp?isosID=" + sISOID + "&prsID=" + sProductID + "&op=dec" + "&repId=" + strRepID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y" + "&carrierId=" + sCarrierID + "\"><img border=0 src=\"support/../images/downarrow.png\" width=\"15\" height=\"15\"/>"  + "</a></td>");
		                    out.println("<td><a href=\"admin/tools/managepromos.jsp?isosID=" + sISOID + "&prsID=" + sProductID + "&op=dectop" + "&repId=" + strRepID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y" + "&carrierId=" + sCarrierID + "\"><img border=0 src=\"support/../images/topdownarrow.png\" width=\"15\" height=\"15\"/>"  + "</a></td></tr></table></td></tr></table>");
	                    }
	                    // Pending actions                                        
	                    out.println("<td align=left nowrap> ");
	                    out.println("<table>");
	                     %>
	                     <tr><td><a href="javascript:void(0);" onclick="return DeletePromo(<%=String.valueOf(promTemp.getPromoId())%>)";> <img src="images/icon_delete.gif" border=0 alt="Delete this route." title="Delete this Promo." /></a></td></tr>
	                   
	                    <%
	                  //  out.println("<tr><td><a href=\"admin/tools/managepromos.jsp?isosID=" + sISOID + "&prsID=" + sProductID + "&op=delete" + "&repId=" + strRepID + "&carrierId=" + sCarrierID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y" + "\" onclick=return confirm('Are you sure you want to delete?') >" + Languages.getString("jsp.admin.tools.managepromotions.delete",SessionData.getLanguage()) + "</a></td></tr>");
	                   out.println("<tr><td><a href=\"admin/tools/promolog.jsp?isosID=" + sISOID + "&prsID=" + sProductID + "&repId=" + strRepID + "&promoID=" + String.valueOf(promTemp.getPromoId()) + "&lblShowReport=y" + "&carrierId=" + sCarrierID + "\"><img src='images/report-icon.png' border=0 alt='History.' title='History' /></a></td></tr>");
	                    out.println("</table>");
	                    out.println("</td>");                     
	                    out.println("</tr>");
	                    %>
	                  <tr>
								<td>		  
						<FORM ID="frmDelete" action="admin/tools/managepromos.jsp" METHOD="POST">
									<INPUT TYPE="hidden" NAME="op" VALUE="delete">
									<INPUT TYPE="hidden" ID="isosID" NAME="isosID" VALUE="<%=sISOID%>">
									<INPUT TYPE="hidden" ID="prsID" NAME="prsID" VALUE="<%=sProductID%>">
									<INPUT TYPE="hidden" ID="repId" NAME="repId" VALUE="<%=strRepID%>">
									<INPUT TYPE="hidden" ID="carrierId" NAME="carrierId" VALUE="<%=sCarrierID%>">
									<INPUT TYPE="hidden" ID="promoID" NAME="promoID">
									<INPUT TYPE="hidden" ID="lblShowReport" NAME="lblShowReport" VALUE="y">
						</FORM>
								<SCRIPT>
								function DeletePromo(nId)
								{
									var agree=confirm("<%=Languages.getString("jsp.admin.tools.managepromotions.deleteconf1",SessionData.getLanguage())%>");
									if (agree)
									{
									   document.forms[1].promoID.value=nId;
									   document.forms[1].submit();
									}
									else
									{
										return false ;
									}
																		
									
								}
								</SCRIPT>
								</td>
								</tr>
						<%			
	                    if (intEvenOdd == 1)
	                    {
	                      intEvenOdd = 2;
	                    }
	                    else
	                    {
	                      intEvenOdd = 1;
	                    }
	
	                  }
	                  vecSearchResults.clear();
	            %>                
	     
	</table>
	
									
									
	<%
	} else if (vecSearchResults != null && vecSearchResults.size()==0) {/*(vecSearchResults != null && vecSearchResults.size() > 0)*/ 
		if (request.getParameter("lblShowReport")!= null ) {
			if (request.getParameter("lblShowReport").equals("y")) { %>
	 			<table width="100%" cellspacing="1" cellpadding="1" border="0">     
				     <tr>
				        <td class=main><%=Languages.getString("jsp.admin.tools.promolog.noresults",SessionData.getLanguage())%></td>
				     </tr> 
				    </table>           
	            
	<%
			}
		}
	} /*(vecSearchResults != null && vecSearchResults.size() > 0)*/ %>   
	            
	            
	            					
	          </td>
	      </tr>
	    </table>
    </form>

</td>
</tr>
</table>
</td>
</tr>
</table>
</body>

<%@ include file="/includes/footer.jsp" %>
