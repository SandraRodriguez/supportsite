<%@ page import="com.debisys.tools.ErrorManagement,java.util.HashMap,java.util.Vector,java.util.Iterator,java.net.URLEncoder,com.debisys.utils.HTMLEncoder" %>
<%
	int section=9;
	int section_page=22;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ErrorManagement" class="com.debisys.tools.ErrorManagement" scope="request"/>
<jsp:setProperty name="ErrorManagement" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector vecSearchResults = new Vector();
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;
	if (request.getParameter("search") != null)
	{
		if (request.getParameter("page") != null)
	  	{
	    	try
	    	{
	      		intPage=Integer.parseInt(request.getParameter("page"));
	    	}
	    	catch(NumberFormatException ex)
		    {
		      	intPage = 1;
	    	}
		}
	  	if (intPage < 1)
	  	{
	    	intPage=1;
	  	}
		vecSearchResults = ErrorManagement.searchErrorsByIso(intPage, intPageSize, SessionData);
	  	intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
	  	vecSearchResults.removeElementAt(0);
	  	if (intRecordCount>0)
	  	{
	    	intPageCount = (intRecordCount / intPageSize) + 1;
	    	if ((intPageCount * intPageSize) + 1 >= intRecordCount)
	    	{
	      		intPageCount++;
	    	}
	  	}
	}
	else
	{
		if (request.getParameter("delete") != null)
		{
			String providerId = request.getParameter("providerId");
			String errorCode = request.getParameter("errorCode");
			ErrorManagement.deleteErrorCode(providerId, errorCode, SessionData);
			vecSearchResults = ErrorManagement.searchErrorsByIso(intPage, intPageSize, SessionData);
		  	intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
		  	vecSearchResults.removeElementAt(0);
		  	if (intRecordCount>0)
		  	{
		    	intPageCount = (intRecordCount / intPageSize) + 1;
		    	if ((intPageCount * intPageSize) + 1 >= intRecordCount)
		    	{
		      		intPageCount++;
		    	}
		  	}
		}
		else
		{
			vecSearchResults = ErrorManagement.searchErrorsByIso(intPage, intPageSize, SessionData);
		  	intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
		  	vecSearchResults.removeElementAt(0);
		  	if (intRecordCount>0)
		  	{
		    	intPageCount = (intRecordCount / intPageSize) + 1;
		    	if ((intPageCount * intPageSize) + 1 >= intRecordCount)
		    	{
		      		intPageCount++;
		    	}
		  	}
		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<script>

	function DoDeleteError(sProviderId, sErrorCode)
	{
		if (confirm("<%=Languages.getString("jsp.admin.tools.deleteWarning",SessionData.getLanguage())%>") )
		{
			window.location="/support/admin/tools/errorManagement.jsp?delete=y&providerId=" + sProviderId + "&errorCode=" + sErrorCode;
		}
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    	<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.errors.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
    				<td>
     					<table border="0" width="100%" cellpadding="0" cellspacing="0">
     						<tr>
	        					<td class="formArea2">
          							<br>
	          						<table border=0>
              							<tr class="main">
               								<td nowrap valign="top" width="100" colspan=2><%=Languages.getString("jsp.admin.narrow_results",SessionData.getLanguage())%>:</td>
               							</tr>
               							<tr class=main>
               								<td valign="top" nowrap colspan=2>
               									<input name="criteria" type="text" size="40" maxlength="64">
               									<br>
               									(<%=Languages.getString("jsp.admin.tools.search_instructions",SessionData.getLanguage())%>)
               								</td>
               								<td valign="top" align="left">
                  								<input type="hidden" name="search" value="y">
                  								<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
               								</td>
              							</tr>
								        <tr>
								        	<td colspan=2 align=left>
							                	<form method="get" action="admin/tools/errorManagement_add.jsp">
							                		<input type="submit" name="addError" id="addError" value="<%=Languages.getString("jsp.admin.tools.add_error",SessionData.getLanguage())%>">
							                	</form>
							              	</td>
							            </tr>
              						</table>
              					</td>
              				</tr>
           				</table>
            		</td>
            	</tr>
            </table>
<%
			if (vecSearchResults != null && vecSearchResults.size() > 0)
			{
%>
            <table width="100%" cellspacing="1" cellpadding="1" border="0">
            	<tr>
            		<td class="main" colspan=7><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
			            <%
			            	if(!ErrorManagement.getCriteria().equals(""))
			            	{
								out.println(Languages.getString("jsp.admin.for",SessionData.getLanguage()) + " \"" + HTMLEncoder.encode(ErrorManagement.getCriteria()) + "\"");
							}
			            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),Integer.toString(intPageCount-1)},SessionData.getLanguage())%>
            		</td>
            	</tr>
            	<tr>
              		<td align=right class="main" nowrap colspan=7>
						<%
							if (intPage > 1)
							{
  								out.println("<a href=\"admin/tools/errorManagement.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8") + "&page=1&col=" + URLEncoder.encode(ErrorManagement.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(ErrorManagement.getSort(), "UTF-8") + "\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
  								out.println("<a href=\"admin/tools/errorManagement.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8") + "&page=" + (intPage-1) + "&col=" + URLEncoder.encode(ErrorManagement.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(ErrorManagement.getSort(), "UTF-8") + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
							}
							int intLowerLimit = intPage - 12;
							int intUpperLimit = intPage + 12;
							if (intLowerLimit<1)
							{
  								intLowerLimit=1;
  								intUpperLimit = 25;
							}
							for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
							{
  								if (i==intPage)
  								{
    								out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
  								}
  								else
  								{
    								out.println("<a href=\"admin/tools/errorManagement.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8") + "&page=" + i + "&col=" + URLEncoder.encode(ErrorManagement.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(ErrorManagement.getSort(), "UTF-8") + "\">" + i + "</a>&nbsp;");
  								}
							}
							if (intPage < (intPageCount-1))
							{
  								out.println("<a href=\"admin/tools/errorManagement.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8") + "&page=" + (intPage+1) + "&col=" + URLEncoder.encode(ErrorManagement.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(ErrorManagement.getSort(), "UTF-8") + "\">"+Languages.getString("jsp.admin.next",SessionData.getLanguage())+"&gt;&gt;</a>&nbsp;");
  								out.println("<a href=\"admin/tools/errorManagement.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&criteria=" + URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8") + "&page=" + (intPageCount-1) + "&col=" + URLEncoder.encode(ErrorManagement.getCol(), "UTF-8") + "&sort=" + URLEncoder.encode(ErrorManagement.getSort(), "UTF-8") + "\">"+Languages.getString("jsp.admin.last",SessionData.getLanguage())+"</a>");
							}
              			%>
					</td>
				</tr>
            	<tr>
            		<td class=rowhead2><%=Languages.getString("jsp.admin.tools.provider_id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=1&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
            		<td class=rowhead2><%=Languages.getString("jsp.admin.tools.provider_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=2&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=2&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
            		<td class=rowhead2><%=Languages.getString("jsp.admin.tools.provider_error_code",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=3&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=3&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
            		<td class=rowhead2><%=Languages.getString("jsp.admin.tools.provider_error_message",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=4&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="admin/tools/errorManagement.jsp?search=y&criteria=<%=URLEncoder.encode(ErrorManagement.getCriteria(), "UTF-8")%>&page=<%=intPage%>&col=4&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
              		<td class=rowhead2>&nbsp;</td>
              		<td class=rowhead2>&nbsp;</td>
            	</tr>
            	<%
	            	Vector vecStates = com.debisys.customers.Merchant.getStates(customConfigType);
					Iterator it = vecSearchResults.iterator();
                  	int intEvenOdd = 1;
                  	while (it.hasNext())
                  	{
                    	Vector vecTemp = null;
                    	vecTemp = (Vector) it.next();
                    	%>
                    		<tr class=row<%= intEvenOdd%> >
                    	<%
                    	out.println("<td align=\"center\">" + vecTemp.get(0));
                    	out.println("<td nowrap>" + vecTemp.get(1) + "</td>" +
                                	"<td>" + vecTemp.get(2) + "</td>" +
                                	"<td nowrap>" + vecTemp.get(3) + "</td>");
                    	out.print("<td align=\"center\"><a href=\"javascript:\" onclick=\"DoDeleteError(" + vecTemp.get(0) + ",'" + vecTemp.get(2) + "');\"><img src=images/icon_delete.gif border=0  alt=\""+Languages.getString("jsp.admin.tools.delete",SessionData.getLanguage())+"\"></a></td>" );
                    	out.print("<td align=\"center\"><a href=\"admin/tools/errorManagement_edit.jsp?providerId=" + vecTemp.get(0) + "&providerName="+ vecTemp.get(1) + "&errorCode="+ vecTemp.get(2) + "\"><img src=images/icon_edit.gif border=0 alt=\""+Languages.getString("jsp.admin.tools.edit",SessionData.getLanguage())+"\"></a></td>" +
                              	  "</tr>");
                    	if (intEvenOdd == 1)
                    	{
                      		intEvenOdd = 2;
                    	}
                    	else
                    	{
                      		intEvenOdd = 1;
                    	}
                  	}
                  	vecSearchResults.clear();
            	%>
			</table>
			<%
				}
				else
				{
					out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.tools.not_found",SessionData.getLanguage())+"</font>");
				}
			%>
    	</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>