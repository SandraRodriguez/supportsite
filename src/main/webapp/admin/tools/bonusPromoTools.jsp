<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to set the bonuses and topup thresholds
// 
// After deciding on this design, I thought it would be a good way to easily update the thresholds and their bonus awards.
// Now I realize this design is not very good. Need to change it.
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="BonusThreshold" class="com.debisys.tools.BonusThreshold" scope="request"/>
<%
  	int section=11;
  	int section_page=1;
  	int result = -1;
  	String divDisplayStr = null;  	
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
<%
	if(request.getParameter("type") != null)
	{
		if(request.getParameter("type").equals("DELETE"))
		{
			BonusThreshold.deleteBonusThreshold(request, SessionData);
		}
		else if(request.getParameter("type").equals("INSERT"))
		{
			// Do all this to guarantee that we dont update negative thresholds or decimal amounts.
			String threshold = request.getParameter("newThreshold");
			String bonus = request.getParameter("newBonus");
			
			// They didn't put in anything.
	    	if(threshold == null || bonus == null || threshold.length() == 0 || bonus.length() == 0)
    		{
    			divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantAdd",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.noEmptyValue",SessionData.getLanguage());
    		}
    		else
    		{
		    	double dbl_threshold = Double.parseDouble(threshold);
		   		double dbl_bonus = Double.parseDouble(bonus);
				if(dbl_bonus < 0 || dbl_threshold < 0)
				{
					divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantAdd",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.amtsNonNeg",SessionData.getLanguage());
				}
				else if((dbl_threshold % 1 > 0) || (dbl_bonus % 1 > 0))
				{
					divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantAdd",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.amtsWholeDollar",SessionData.getLanguage());
				}
	    		else if((dbl_threshold == 0) || (dbl_bonus == 0))
	    		{
	    			divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantAdd",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.nonZero",SessionData.getLanguage());
	    		}
    		}
			
	    	if(divDisplayStr == null)
	    	{
				result = BonusThreshold.insertBonusThreshold(request, SessionData);
			}
		}
		else if(request.getParameter("type").equals("UPDATE"))
		{
			// Do all this to guarantee that we dont update negative thresholds or decimal amounts. 	
			int iterator = 1;
			String threshold = request.getParameter("thresholds" + iterator);
			String bonus = request.getParameter("extraBonuses" + iterator);
	    	do
	    	{
				// They didn't put in anything.
	    		if(threshold == null || bonus == null || threshold.length() == 0 || bonus.length() == 0)
	    		{
	    			divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantAdd",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.noEmptyValue",SessionData.getLanguage());
	    			break;
	    		}
	    		double dbl_threshold = Double.parseDouble(threshold);
	    		double dbl_bonus = Double.parseDouble(bonus);
	    		if(dbl_bonus < 0 || dbl_threshold < 0)
	    		{
	    			divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantUpdate",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.amtsNonNeg",SessionData.getLanguage());
	    		}
	    		else if((dbl_threshold % 1 > 0) || (dbl_bonus % 1 > 0))
	    		{
	    			divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantUpdate",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.amtsWholeDollar",SessionData.getLanguage());
	    		}
	    		else if((dbl_threshold == 0) || (dbl_bonus == 0))
	    		{
	    			divDisplayStr = Languages.getString("jsp.admin.tools.bonusPromoTools.cantUpdate",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.tools.bonusPromoTools.nonZero",SessionData.getLanguage());
	    		}
		    	iterator++;
	    		threshold = request.getParameter("thresholds" + iterator);
	    		bonus = request.getParameter("extraBonuses" + iterator);
	    	}
	    	while(threshold != null && bonus != null);
	    	
	    	if(divDisplayStr == null)
				BonusThreshold.updateBonusThresholds(request, SessionData);
		}
	}
	Vector vecSearchResults = BonusThreshold.getBonusThresholds(SessionData);
%>
<script>  
	function submitform()
	{
	  	document.bonusPromoForm.submit();
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.bonusPromoTools.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
				<tr>
					<td class=formArea2>
						<table>
<%
	if(divDisplayStr != null)
	{						
%>
							<tr>
								<td>
									<table>
										<tr>
											<td class=main><%=divDisplayStr%></td>
										</tr>
									</table>
								</td>
							</tr>
<%
	}
%>
							<tr>
								<td>
									<table>
										<tr>
											<td>
												<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
													<tr>
														<td class="main"><br>
															<table width="100%" cellspacing="1" cellpadding="2">
																<tr>
																	<th class=rowhead2>#</th>
																	<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoTools.title.bonusThreshold",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
																	<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoTools.title.bonusAward",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	      
																	<th class=rowhead2><%=Languages.getString("jsp.admin.tools.bonusPromoTools.delete",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	          				
																</tr>
<%
																// Had this set to <form name=bonusPromoForm action="admin/tools/bonusPromoTools.jsp"> before and 
																// it was causing a problem in IE6 and IE7, but in firefox and IE8 it worked fine. Added method="post" to it
																// and now it works fine.
%>																
																<form name=bonusPromoForm action="admin/tools/bonusPromoTools.jsp" method="POST">
																<input type=hidden name=type value=UPDATE />
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		// Will fix this and make each row go to the addBonusThreshold page to edit. But not now, too busy.
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		out.println(
					  											"<tr class=row" + intEvenOdd + ">" +
			              											"<td>" + intCounter + "</td>" +
														            "<td><input type=text name=thresholds" + intCounter + " value=" + vecTemp.get(0) + " /></td>" +
														            "<td><input type=text name=extraBonuses" + intCounter + " value=" + vecTemp.get(1) + " /></td>" +
//														            "<td align=center><a href=\"admin/tools/addBonusThreshold.jsp?prevThreshold=" + vecTemp.get(0) + "&prevBonus=" + vecTemp.get(1) + "&type=UPDATE\"><img src=\"images/icon_edit.gif\" border=0 alt=\"Edit this bonus threshold.\" title=\"Edit this bonus threshold.\" /></a></td>" +
																	"<td align=center><a href=\"admin/tools/bonusPromoTools.jsp?prevThreshold=" + vecTemp.get(0) + "&prevBonus=" + vecTemp.get(1) + "&type=DELETE\"><img src=\"images/icon_delete.gif\" border=0 alt=\"Delete this bonus threshold.\" title=\"Delete this bonus threshold.\" /></a></td>" +
														            "<td><input type=hidden name=prevThresholds" + intCounter + " value=" + vecTemp.get(0) + " /></td>" +
														            "<td><input type=hidden name=prevExtraBonuses" + intCounter++ + " value=" + vecTemp.get(1) + " /></td>" +
			          											"</tr>"
		);
          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	//vecSearchResults.clear();
%>            							
																</form>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
<%
	if(vecSearchResults.size() > 0)
	{
%>							
										<td>
											<input type="button" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoTools.update_bonusThresholds",SessionData.getLanguage())%>" onClick=submitform();>
										</td>
<%
	}
%>								
										<td>
											<form name=insertThresholdBonus action="admin/tools/addBonusThreshold.jsp">
												<input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoTools.add_bonusThresholds",SessionData.getLanguage())%>">
											</form>
										</td>
									</tr>
								</table>
							</tr>							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>