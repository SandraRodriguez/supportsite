<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page import="com.debisys.tools.AccountExecutive,com.debisys.utils.DebisysConstants"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	String strRefId = SessionData.getUser().getRefId();
	String strAccessLevel = SessionData.getProperty("access_level");

	boolean permissionEnableEditMerchantAccountExecutives= SessionData.checkPermission(DebisysConstants.PERM_Enable_Edit_Merchant_Account_Executives);
	boolean isDomestic = com.debisys.utils.DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
						
	String actionTask = request.getParameter("actionTask");
	if ( strAccessLevel.equals(DebisysConstants.ISO) && permissionEnableEditMerchantAccountExecutives )
	{
		if ( request.getMethod().equals("GET") )
		{
			ArrayList<String> statusAll = new ArrayList<String>();
			
			if ( actionTask.equals("1") )
			{
				statusAll = AccountExecutive.updateAllByStatus(1,strRefId);
			}
			else if ( actionTask.equals("0") )
			{
				statusAll = AccountExecutive.updateAllByStatus(0,strRefId);
				session.setAttribute("toolAccountExecutives",statusAll);
			}
			else if ( actionTask.equals("updaterow") )
			{
				String status = request.getParameter("status");
				String id = request.getParameter("id");
				statusAll = AccountExecutive.updateStatusById(id,status);
			}
			if (statusAll.size()>0)
			{
				session.setAttribute("toolAccountExecutives",statusAll);
			}
			%>
				<script type="text/javascript">
					window.location="/support/admin/tools/accountexecutives.jsp?actionTask=<%=actionTask%>";
				</script>
			<% 
		}
	}

%>


