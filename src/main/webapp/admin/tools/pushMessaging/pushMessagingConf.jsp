<%-- 
    Document   : pushMessagingConf
    Created on : Mar 14, 2016, 2:19:48 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.pushMessaging.PushMessagingPojo"%>
<%@page import="com.debisys.tools.pushMessaging.PushMessagingSetup"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 32;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script type="text/javascript" src="js/referralAgent.js"></script>

<style type="text/css">
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }
    #spanInfo{
        font-size: 20px;
        color: #088A08;
    }

</style>

<script type="text/javascript" src="/support/includes/jquery.js"></script>

<%    String strRefId = SessionData.getProperty("ref_id");
    String iso_id = SessionData.getProperty("iso_id");

    PushMessagingSetup pushMessagingSetup = new PushMessagingSetup();
    List<PushMessagingPojo> pushMessagingPojoList = pushMessagingSetup.getPushMessaging(null);


%>

<script LANGUAGE="JavaScript">
    function editPushMessaging(pushMessagingId) {
        var url = goPath()+"pushMessagingEdit.jsp?pushMessagingId=" + pushMessagingId;
        $(location).attr("href", url);
    }
    
    function goPath() {
        if (isInternetExplorer()) {
            return "";
        }
        else {
            return "admin/tools/pushMessaging/";
        }
    }

    function removePushMessaging(pushMessagingId) {
        if (confirm("<%=Languages.getString("jsp.admin.tools.pushMessaging.deleteConfirm", SessionData.getLanguage())%>")) {
            $.ajax({
                type: "POST",
                async: false,
                data: {pushMessagingId: pushMessagingId, action: 'deletePushMessaging'},
                url: goPath()+"pushMessagingVerifyDB.jsp",
                success: function (data) {
                }
            });
        }
        location.reload();
    }

    function enableDisable(pushMessagingId) {

        $.ajax({
            type: "POST",
            async: false,
            data: {pushMessagingId: pushMessagingId, action: 'enableDisablePushMessaging'},
            url: goPath()+"pushMessagingVerifyDB.jsp",
            success: function (data) {
            }
        });

        location.reload();
    }

</script>

<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.pushMessaging.reportTitle", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">


                <tr>
                    <td class="formArea2" width="100%">

                        <table width="100%">
                            <tr>
                                <td>

                                    <form name=insertPushMessaging" action="admin/tools/pushMessaging/pushMessagingEdit.jsp" method="post">
                                        <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.tools.pushMessaging.add", SessionData.getLanguage())%>">
                                        <input type="hidden" name="type" value="INSERT" />
                                    </form>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="4">					
                                    <table border="0" cellpadding="2" cellspacing="0" width=100% align="center">
                                        <tr>
                                            <td class="main"><br>
                                                <table cellspacing="1" cellpadding="2" width=100%>
                                                    <tr>
                                                        <th class="rowhead2">#</th>
                                                        <th class="rowhead2"><%=Languages.getString("jsp.admin.tools.pushMessaging.title", SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
                                                        <th class="rowhead2"><%=Languages.getString("jsp.admin.tools.pushMessaging.notificationDate", SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
                                                        
                                                        <th class="rowhead2"><%=Languages.getString("jsp.admin.tools.pushMessaging.active", SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
                                                        <th class="rowhead2"><%=Languages.getString("jsp.admin.tools.pushMessaging.edit", SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
                                                        <th class="rowhead2"><%=Languages.getString("jsp.admin.tools.pushMessaging.delete", SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
                                                    </tr>

                                                    <%
                                                        int intCounter = 1;
                                                        int intEvenOdd = 1;

                                                        for (PushMessagingPojo obj : pushMessagingPojoList) {
                                                    %>		
                                                    <tr class=rowwrap<%=intEvenOdd%>>
                                                        <td valign=top><%=intCounter%></td>
                                                        <td valign=top id="name<%=intCounter%>" name="name<%=intCounter%>"><%=obj.getTitle()%></td>
                                                        <td valign=top id="startDate<%=intCounter%>" name=startDate<%=intCounter%>><%=(obj.getNotificationDate()== null) ? "" : obj.getNotificationDate()%></td>
                                                        
                                                        
                                                        
                                                        <td valign=top id="active<%=intCounter%>" name=active<%=intCounter%>>
                                                            <input type="checkbox" id="checkActive" name="checkActive" <%=(obj.isActive()) ? "checked" : ""%> onclick="enableDisable('<%=obj.getId()%>')"/>
                                                        </td>

                                                        <td valign=top><input type="button" name="buttonEdit" value="<%=Languages.getString("jsp.admin.tools.pushMessaging.edit", SessionData.getLanguage())%>" onclick="editPushMessaging('<%=obj.getId()%>');">
                                                        </td>

                                                        <td valign=top><input type="button" name="buttonDelete" value="<%=Languages.getString("jsp.admin.tools.pushMessaging.delete", SessionData.getLanguage())%>" onclick="removePushMessaging('<%=obj.getId()%>');">
                                                        </td>

                                                    </tr>
                                                    <%
                                                            // Used to make the rows alternation in background color
                                                            if (intEvenOdd == 1) {
                                                                intEvenOdd = 2;
                                                            } else {
                                                                intEvenOdd = 1;
                                                            }
                                                            intCounter++;
                                                        }
                                                        pushMessagingPojoList.clear();
                                                    %>   

                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<%@ include file="/includes/footer.jsp" %>
