<%-- 
    Document   : pushMessagingEdit
    Created on : Mar 14, 2016, 2:20:06 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.utils.CalendarBussines"%>
<%@page import="com.debisys.utils.BaseDaysMonths"%>
<%@page import="com.debisys.tools.pushMessaging.PushMessagingProcessGroup"%>
<%@page import="com.debisys.tools.pushMessaging.PushMessagingGroups"%>
<%@page import="com.debisys.tools.pushMessaging.PushNotificationComTypes"%>
<%@page import="com.debisys.tools.pushMessaging.PushMessagingSetup"%>
<%@page import="com.debisys.tools.pushMessaging.PushMessagingPojo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 32;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="js/referralAgent.js"></script>
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>

<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>


<%    String pushMessagingId = request.getParameter("pushMessagingId");
    String iso_id = SessionData.getProperty("iso_id");
    String actionValue = "insertPushMessaging";

    PushMessagingSetup program = new PushMessagingSetup();
    List<PushNotificationComTypes> typeList = program.getPushNotificationComTypesList(SessionData, application);
    List<PushMessagingGroups> msgGroupsList = program.getPushMessagingGroups();

    String title = "";
    String startDate = "";
    String englishMessage = "";
    String spanishMessage = "";
    String pushNotificationComTypesId = "";
    boolean checkAndroidMethod= false;
    boolean checkIosMethod = false;
    boolean checkIsSmartPhone = false;
    List<PushMessagingProcessGroup> processGrouplist = new ArrayList<PushMessagingProcessGroup>();

    if (pushMessagingId != null && !pushMessagingId.trim().equals("null")) {
        PushMessagingPojo mainPojo = PushMessagingSetup.getPushMessaging(pushMessagingId).get(0);
        
        title = mainPojo.getTitle();
        pushNotificationComTypesId = mainPojo.getPushNotificationComTypesId();
        checkAndroidMethod = mainPojo.isAndroidMethod();
        checkIosMethod = mainPojo.isIosMethod();
        checkIsSmartPhone = mainPojo.isSmartPhone();
        pushMessagingId = mainPojo.getId();
        englishMessage = mainPojo.getEnglishMessage();
        spanishMessage = mainPojo.getSpanishMessage();
        actionValue = "editPushMessaging";
        processGrouplist = mainPojo.getGroupList();

        // 03/15/2016
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        if (mainPojo.getNotificationDate()!= null) {
            startDate = format.format(new Date(mainPojo.getNotificationDate().getTime()));
        }
        
    }
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, 1); // add 1 day  
    int yearUIControl = cal.get( Calendar.YEAR );
    int monthUIControl = cal.get( Calendar.MONTH );
    int dayUIControl = cal.get( Calendar.DATE );
    int hourUIControl = cal.get( Calendar.HOUR_OF_DAY );
    
    String languageFlag = SessionData.getUser().getLanguageCode();
        ArrayList<BaseDaysMonths> months = CalendarBussines.getCalendarObjectsByType(CalendarBussines.CALENDAR_OBJECTS_TYPE.MONTHS, languageFlag);

        String monthsUIControl = "";
        String monthsMinUIControl = "";
        String daysUIControl = "";
        String daysMinUIControl = "";
        for (BaseDaysMonths month : months) {
            monthsUIControl = monthsUIControl + "'" + month.getShortName() + "',";
            monthsMinUIControl = monthsMinUIControl + "'" + month.getShortName() + "',";
        }

        ArrayList<BaseDaysMonths> days = CalendarBussines.getCalendarObjectsByType(CalendarBussines.CALENDAR_OBJECTS_TYPE.DAYS, languageFlag);
        for (BaseDaysMonths day : days) {
            daysUIControl = daysUIControl + "\"" + day.getName() + "\",";
            daysMinUIControl = daysMinUIControl + "\"" + day.getShortName() + "\",";
        }

        String timeLabel = Languages.getString("jsp.admin.reports.scheduleReport.time", SessionData.getLanguage());
        String hourLabel = Languages.getString("jsp.admin.reports.managescheduleteports.unitHour", SessionData.getLanguage());
        String minuteLabel = Languages.getString("jsp.admin.reports.managescheduleteports.unitMinute", SessionData.getLanguage());
        String doneLabel = Languages.getString("jsp.admin.reports.scheduleReport.done", SessionData.getLanguage());
        String nextLabel = Languages.getString("jsp.admin.reports.scheduleReport.next", SessionData.getLanguage());
        String prevLabel = Languages.getString("jsp.admin.reports.scheduleReport.prev", SessionData.getLanguage());
        String nowLabel = Languages.getString("jsp.admin.reports.scheduleReport.now", SessionData.getLanguage());

%>

<script LANGUAGE="JavaScript">
    
    $(document).ready(function() {			
		
	
	$("#startDate").datetimepicker({
				hourMin:  0,
				hourMax:  23,
				monthNames: [ <%= monthsUIControl%> ],
				dayNames: [ <%= daysUIControl%> ],
				dayNamesMin: [ <%= daysMinUIControl%> ],
				timeText: '<%= timeLabel%>',
				hourText: '<%= hourLabel%>',
				minuteText: '<%= minuteLabel%>',
				closeText: '<%= doneLabel%>',
				prevText: '<%= prevLabel%>',
				nextText: '<%= nextLabel%>',
				currentText: '<%= nowLabel%>'				
	});
		
		
		
 	
 	    $.timepicker.regional['current'] = {
			timeOnlyTitle: '<%= hourLabel%>:<%= minuteLabel%>',
			timeText: '<%= timeLabel%>',
			hourText: '<%= hourLabel%>',
			minuteText: '<%= minuteLabel%>',
			isRTL: false
		};
		$.timepicker.setDefaults($.timepicker.regional['current']);
		
		document.getElementById("divSimple").style.display="none";		
		document.getElementById("divRecurring").style.display="none";
		document.getElementById("tdendScheduleDateControls").style.display="none";		
	}
	);
    
    function validateForm() {
        removeRedBorder();
        var name = $("#title").val();
        var startDate = $("#startDate").val();
        var englishMessage = $("#englishMessage").val();
        var spanishMessage = $("#spanishMessage").val();
        //var checkAndroidMethod = $("#checkAndroidMethod").prop('checked');
        //var checkIosMethod = $("#checkIosMethod").prop('checked');
        
        var groupLength = $("#groupIds :selected").length;
        
        if (name === '' || englishMessage === '' || spanishMessage === '' || startDate ==='' || groupLength === 0) {
            markErrorFields();
            return false;
        }
        
        return true;
    }

    function markErrorFields() {

        var title = $("#title").val();
        var startDate = $("#startDate").val();
        var englishMessage = $("#englishMessage").val();
        var spanishMessage = $("#spanishMessage").val();
        //var checkAndroidMethod = $("#checkAndroidMethod").prop('checked');
        //var checkIosMethod = $("#checkIosMethod").prop('checked');
        
        var groupLength = $("#groupIds :selected").length;
        
        if (title === '') {
            $("#title").css("border", "1px solid red");
        }
        
        if (startDate === '') {
            $("#startDate").css("border", "1px solid red");
        }
        
        if (englishMessage === '') {
            $("#englishMessage").css("border", "1px solid red");
        }
        if (spanishMessage === '') {
            $("#spanishMessage").css("border", "1px solid red");
        }
        
        if(groupLength === 0){
            $("#groupIds").css("border", "1px solid red");
        }
        
        /*if (checkAndroidMethod ===false && checkIosMethod ===false) {
            $("#checkAndroidMethod").css("outline", "1px solid red");
            $("#checkIosMethod").css("outline", "1px solid red");
        }*/

    }

    function removeRedBorder() {
        $("#title").css('border', '');
        $("#startDate").css('border', '');
        $("#englishMessage").css('border', '');
        $("#spanishMessage").css('border', '');
        //$("#checkAndroidMethod").css('outline', '');
        //$("#checkIosMethod").css('outline', '');
    }

    function goBack() {
                
        if (isInternetExplorer()) {
            location.assign('/support/reports/pushmessaging/historical');
        }
        else {
            window.location.href = "reports/pushmessaging/historical";
        }
    }
    function showControlStartDate(){
	$("#startDate").datepicker("show");
    }
    


</script>

<style type="text/css">
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }
    #spanInfo{
        font-size: 20px;
        color: #088A08;
    }

    #spanWarningRows{
        font-size: 17px;
        color: red;
    }

</style>

<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.pushMessaging.reportTitle", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width=100% align=center>

                <tr>
                    <td class="formArea2" width="100%">

                        <table>
                            <tr>
                                <td colspan="4">	
                                    <form id="mainform" name="mainform" class="reportForm" method="post" action="admin/tools/pushMessaging/pushMessagingVerifyDB.jsp" onsubmit="return  validateForm();">
                                        <table border="0" cellpadding="2" cellspacing="6" width=100% align="center">
                                            <tr>
                                                <td class="main"><br>

                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="main" align="left"><%=Languages.getString("jsp.admin.tools.pushMessaging.title", SessionData.getLanguage())%></td>
                                                <td class="main" align="left"><input id="title" name="title" type="text" value="<%=title%>"/></td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="main" align="left" ><%=Languages.getString("jsp.admin.tools.pushMessaging.notificationDate", SessionData.getLanguage())%></td>
                                                                                                
                                                <td class="main" align="left" >
                                                    <div id="sd_divStartDate">
                                                        <input  name="startDate" id="startDate"readonly= "readonly"   
                                                                value="<%=(!startDate.equals("")) ? startDate : SessionData.getProperty("start_date")%>"/>
                                                        <img idc="startDate" class="datetimeButton" src="/support/images/calendar.png" />
                                                    </div>	
                                                </td>
                                            </tr>
                                            
                                            <tr>

                                                <td class="main" align="left" ><%=Languages.getString("jsp.admin.tools.pushMessaging.communicationType", SessionData.getLanguage())%></td>
                                                <td>
                                                    <select name="communicationTypeList" id = "communicationTypeList">
                                                        <%
                                                            for (PushNotificationComTypes accountSource1 : typeList) {%>  
                                                        <option value="<%=accountSource1.getId()%>" <%=(accountSource1.getId().equalsIgnoreCase(pushNotificationComTypesId)) ? "selected" : ""%>><%=accountSource1.getName()%></option>
                                                        <%}%>
                                                    </select>
                                                </td>

                                            </tr>


                                            <tr >
                                                <td class="main" align="left"><%=Languages.getString("jsp.admin.tools.pushMessaging.englishMessage", SessionData.getLanguage())%></td>
                                                <td class="main" align="left"><textarea rows="6" cols="50" id="englishMessage" name="englishMessage"><%=englishMessage%></textarea>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="main" align="left"><%=Languages.getString("jsp.admin.tools.pushMessaging.spanishMessage", SessionData.getLanguage())%></td>
                                                <td class="main" align="left">
                                                    <textarea rows="6" cols="50" id="spanishMessage" name="spanishMessage"><%=spanishMessage%></textarea>
                                                </td>
                                                
                                            </tr>

                                            <!-- <tr >
                                                <td class="main" align="left"><%=Languages.getString("jsp.admin.tools.pushMessaging.androidMethod", SessionData.getLanguage())%></td>
                                                <td class="main" align="left"><input type="checkbox" id="checkAndroidMethod" name="checkAndroidMethod" <%=(checkAndroidMethod) ? "checked" : ""%> /></td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="main" align="left"><%=Languages.getString("jsp.admin.tools.pushMessaging.iosMethod", SessionData.getLanguage())%></td>
                                                <td class="main" align="left"><input type="checkbox" id="checkIosMethod" name="checkIosMethod" <%=(checkIosMethod) ? "checked" : ""%> /></td>
                                            </tr> -->
                                            
                                            <tr>
                                                <td class="main" align="left"><%=Languages.getString("jsp.admin.tools.pushMessaging.isSmartPhone", SessionData.getLanguage())%></td>
                                                <td class="main" align="left"><input type="checkbox" id="checkIsSmartPhone" name="checkIsSmartPhone" checked disabled></td>
                                            </tr>
                                                                                        
                                            <tr>
                                                <td class="main" align="left" ><%=Languages.getString("jsp.admin.tools.pushMessaging.messagingGroup", SessionData.getLanguage())%></td>
                                                <td>
                                                    <select name="groupIds" id = "groupIds" multiple>
                                                        <%
                                                            String select = "";
                                                            for (PushMessagingGroups group : msgGroupsList) {
                                                                select = "";
                                                                for(PushMessagingProcessGroup processGroup : processGrouplist){
                                                                    if(processGroup.getPushMessagingGroupsId().equalsIgnoreCase(group.getId())){
                                                                        select = "selected";
                                                                    }
                                                                }
                                                        %>  
                                                            <option value="<%=group.getId()%>" <%=select%>><%=group.getDescription()%></option>
                                                        <%}%>
                                                    </select>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                
                                                <td colspan="1" align="right"> 
                                                    <input type="hidden" name="pushMessagingId" id="pushMessagingId" value="<%=pushMessagingId%>" />
                                                    <input type="hidden" name="action" id="action" value="<%=actionValue%>" />
                                                </td>
                                                <td align="center">
                                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.back", SessionData.getLanguage())%>" onclick="goBack();">
                                                    <input type="submit" name="submit" id="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonVerifyAndProcess", SessionData.getLanguage())%>" align="center" />
                                                </td>

                                            </tr>


                                        </table>
                                    </form>
                                </td>
                            </tr>

                            <tr>
                                <td>


                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0"
        style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>


<%@ include file="/includes/footer.jsp" %>
