<%-- 
    Document   : pushMessagingVerifyDB
    Created on : Mar 14, 2016, 2:20:18 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.pushMessaging.PushMessagingPojo"%>
<%@page import="com.debisys.tools.pushMessaging.PushMessagingSetup"%>
<%@page import="java.util.Locale"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsProgram"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsProgramPojo"%>
<%@page import="com.debisys.tools.loyaltyPointsProgram.LoyaltyPointsRedeemValuesPojo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String action = request.getParameter("action");

    if (action.equals("insertPushMessaging") || action.equals("editPushMessaging")) {

       
        String startDate = request.getParameter("startDate");
        String title = request.getParameter("title");
        String communicationTypeList = request.getParameter("communicationTypeList");
        String englishMessage = request.getParameter("englishMessage");
        String spanishMessage = request.getParameter("spanishMessage");
        String methodAndroid = request.getParameter("checkAndroidMethod");
        String methodIos = request.getParameter("checkIosMethod");
        
        String[] groupIds = request.getParameterValues("groupIds");

        PushMessagingPojo obj = new PushMessagingPojo();

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm",Locale.US); // 03/20/2016        
        if (startDate != null && !startDate.trim().equals("")) {
            //startDate += " 00:00:00";
            Date sDate = format.parse(startDate);
            obj.setNotificationDate(new Timestamp(sDate.getTime()));
        } else {
            obj.setNotificationDate(null);
        }

        obj.setTitle(title);
        obj.setPushNotificationComTypesId(communicationTypeList);
        obj.setEnglishMessage(englishMessage);
        obj.setSpanishMessage(spanishMessage);
        obj.setAndroidMethod((methodAndroid != null)? true:false);
        obj.setIosMethod((methodIos != null)? true:false);
        obj.setActive(true);
        obj.setIsSmartPhone(true);

        PushMessagingSetup pushMessagingSetup = new PushMessagingSetup();
        if (action.equals("insertPushMessaging")) {
            pushMessagingSetup.insertPushMessaging(SessionData, application, obj, groupIds);
        } 
        else if (action.equals("editPushMessaging")) {
            String loyaltyProgramId = request.getParameter("pushMessagingId");
            obj.setId(loyaltyProgramId);
            pushMessagingSetup.updatePushMessaging(obj, groupIds);
        }
        response.sendRedirect(request.getContextPath()+"/reports/pushmessaging/historical");

    } 
    else if (action.equals("deletePushMessaging")) {
        String id = request.getParameter("pushMessagingId");
        PushMessagingSetup pushMessagingSetup = new PushMessagingSetup();
        pushMessagingSetup.deletePushMessaging(SessionData, application, id);
        response.sendRedirect("loyaltyProgramConf.jsp##");

    } 
    else if (action.equals("enableDisablePushMessaging")) {
        String id = request.getParameter("pushMessagingId");
        PushMessagingSetup pushMessagingSetup = new PushMessagingSetup();
        pushMessagingSetup.enableDisablePushMessaging(id);
        response.sendRedirect("pushMessagingConf.jsp##");
    }

%>

