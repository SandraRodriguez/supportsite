<%@ page language="java" import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"></jsp:useBean>
<jsp:useBean id="Taskhandler" class="com.debisys.tools.s2ktaskhandler" scope="request"></jsp:useBean>
<jsp:setProperty name="Taskhandler" property="*"/>
<%
	int section=9;
  	int section_page=16;
  	int result = -1;
  	String agentlist="",subagentlist="",replist="";
	Hashtable searchErrors = null;
  	String jobid ="";
	String action = "";
  	boolean redirect =false;
  	boolean load = false;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT SRC="includes/s2k/addedittasks.js" type="text/javascript"></SCRIPT>
<%

String strselectagentslist[] = null, strsubagentslist[] = null, strrepslist[] = null, strmerchantslist[] = null;
String strRatePlansList[] = null;
String strRefId = SessionData.getProperty("ref_id");
String jobstartDate = "", startDate = "", endDate = "", companycode = "", location = "", shipto = "";

if (request.getParameter("action") != null) {
    action = request.getParameter("action");
    jobid = request.getParameter("jobid");
    if (action.equals("add")) {
        if (request.getParameter("jobstartDate") != null) {
            jobstartDate = request.getParameter("jobstartDate");
        }

        if (request.getParameter("startDate") != null) {
            startDate = request.getParameter("startDate");
        }

        if (request.getParameter("endDate") != null) {
            endDate = request.getParameter("endDate");
        }

        Taskhandler.setjobstartDate(jobstartDate);
        Taskhandler.setStartDate(startDate);
        Taskhandler.setEndDate(endDate);

        if (Taskhandler.validateDateRange(SessionData)) {

            if (request.getParameter("companycode") != null) {
                companycode = request.getParameter("companycode");
            }

            if (request.getParameter("location") != null) {
                location = request.getParameter("location");
            }

            if (request.getParameter("shipto") != null) {
                shipto = request.getParameter("shipto");
            }

            if (request.getParameter("selectagentslist") != null) {
                strselectagentslist = request.getParameterValues("selectagentslist");
            }

            if (request.getParameter("subagentslist") != null) {
                strsubagentslist = request.getParameterValues("subagentslist");
            }

            if (request.getParameter("repslist") != null) {
                strrepslist = request.getParameterValues("repslist");
            }

            if (request.getParameter("merchantslist") != null) {
                strmerchantslist = request.getParameterValues("merchantslist");
            }

            Taskhandler.setSelectedList(strAccessLevel, strDistChainType, strRefId, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);

            if (Taskhandler.getSelectionType() != 0) {

                String insertresult = Taskhandler.inserttask(strRefId, jobstartDate, startDate, endDate, 0, companycode, location, shipto, 0, false, strAccessLevel);
                if (insertresult == "-4") {
                    load = true;
                    Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                    Taskhandler.clearvalidation();
                    Taskhandler.addFieldError("insert task", Languages.getString("jsp.admin.tools.s2k.error12", SessionData.getLanguage()));
                    searchErrors = Taskhandler.getErrors();
                }
                else if (insertresult == "-1") {
                    load = true;
                    Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                    Taskhandler.clearvalidation();
                    Taskhandler.addFieldError("insert task", Languages.getString("jsp.admin.tools.s2k.error13", SessionData.getLanguage()));
                    searchErrors = Taskhandler.getErrors();
                }
                else {
                    int addm = Taskhandler.addmerchants(strRefId, insertresult);
                    Taskhandler.addRateplans(request.getParameterValues("ratePlanList"), insertresult);
                    redirect = true;

                }
            }
            else {

                load = true;
                Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                Taskhandler.clearvalidation();
                Taskhandler.addFieldError("insert task", Languages.getString("jsp.admin.no_merchant", SessionData.getLanguage()));
                searchErrors = Taskhandler.getErrors();
            }

        }
        else {
            if (request.getParameter("selectagentslist") != null) {
                strselectagentslist = request.getParameterValues("selectagentslist");
            }

            if (request.getParameter("subagentslist") != null) {
                strsubagentslist = request.getParameterValues("subagentslist");
            }

            if (request.getParameter("repslist") != null) {
                strrepslist = request.getParameterValues("repslist");
            }

            if (request.getParameter("merchantslist") != null) {
                strmerchantslist = request.getParameterValues("merchantslist");
            }
            load = true;
            Taskhandler.setSelectedList(strAccessLevel, strDistChainType, strRefId, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
            Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
            searchErrors = Taskhandler.getErrors();
        }
    }
    else if (action.equals("edit")) {
        //load data
        if (request.getParameter("jobid") != null) {
            jobid = request.getParameter("jobid");
            Taskhandler.gets2kjob(jobid, strAccessLevel, strRefId);
            Taskhandler.getbillinglevel(jobid, strDistChainType);
        }
    }
    else if (action.equals("save")) {

        if (request.getParameter("jobstartDate") != null) {
            jobstartDate = request.getParameter("jobstartDate");
        }

        if (request.getParameter("startDate") != null) {
            startDate = request.getParameter("startDate");
        }

        if (request.getParameter("endDate") != null) {
            endDate = request.getParameter("endDate");
        }

        Taskhandler.setjobstartDate(jobstartDate);
        Taskhandler.setStartDate(startDate);
        Taskhandler.setEndDate(endDate);

        if (Taskhandler.validateDateRange(SessionData)) {

            if (request.getParameter("companycode") != null) {
                companycode = request.getParameter("companycode");
            }

            if (request.getParameter("location") != null) {
                location = request.getParameter("location");
            }

            if (request.getParameter("shipto") != null) {
                shipto = request.getParameter("shipto");
            }

            if (request.getParameter("selectagentslist") != null) {
                strselectagentslist = request.getParameterValues("selectagentslist");
            }

            if (request.getParameter("subagentslist") != null) {
                strsubagentslist = request.getParameterValues("subagentslist");
            }

            if (request.getParameter("repslist") != null) {
                strrepslist = request.getParameterValues("repslist");
            }

            if (request.getParameter("merchantslist") != null) {
                strmerchantslist = request.getParameterValues("merchantslist");
            }

            Taskhandler.setSelectedList(strAccessLevel, strDistChainType, strRefId, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
            if (Taskhandler.getSelectionType() != 0) {
                if (request.getParameter("jobid") != null) {

                    jobid = request.getParameter("jobid");
                    int resp = Taskhandler.edittask(strRefId, jobid, jobstartDate, startDate, endDate, companycode, location, shipto, 0, false, strAccessLevel);

                    if (resp == 0) {
                        redirect = true;
                        Taskhandler.deletemerchants(jobid);
                        int addm = Taskhandler.addmerchants(strRefId, jobid);
                        Taskhandler.deleteS2kRatePlans(jobid);
                        Taskhandler.addRateplans(request.getParameterValues("ratePlanList"), jobid);
                    }
                    else if (resp == 2) {
                        Taskhandler.clearvalidation();
                        Taskhandler.addFieldError("edit task", Languages.getString("jsp.admin.tools.s2k.error9", SessionData.getLanguage()));
                        searchErrors = Taskhandler.getErrors();
                        load = true;
                        Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                    }
                    else if (resp == -4) {
                        Taskhandler.clearvalidation();
                        Taskhandler.addFieldError("edit task", Languages.getString("jsp.admin.tools.s2k.error11", SessionData.getLanguage()));
                        searchErrors = Taskhandler.getErrors();
                        load = true;
                        Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                    }
                    else {
                        Taskhandler.clearvalidation();
                        Taskhandler.addFieldError("edit task", Languages.getString("jsp.admin.tools.s2k.error13", SessionData.getLanguage()));
                        searchErrors = Taskhandler.getErrors();
                        load = true;
                        Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                    }
                }
            }
            else {
                load = true;
                Taskhandler.setSelectedList(strAccessLevel, strDistChainType, strRefId, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
                Taskhandler.clearvalidation();
                Taskhandler.addFieldError("insert task", Languages.getString("jsp.admin.no_merchant", SessionData.getLanguage()));
                searchErrors = Taskhandler.getErrors();
            }
        }
        else {
            if (request.getParameter("selectagentslist") != null) {
                strselectagentslist = request.getParameterValues("selectagentslist");
            }

            if (request.getParameter("subagentslist") != null) {
                strsubagentslist = request.getParameterValues("subagentslist");
            }

            if (request.getParameter("repslist") != null) {
                strrepslist = request.getParameterValues("repslist");
            }

            if (request.getParameter("merchantslist") != null) {
                strmerchantslist = request.getParameterValues("merchantslist");
            }

            load = true;
            Taskhandler.setSelectedList(strAccessLevel, strDistChainType, strRefId, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);
            Taskhandler.load(strDistChainType, strselectagentslist, strsubagentslist, strrepslist, strmerchantslist);

            searchErrors = Taskhandler.getErrors();
        }

    }

}
 %>

<script language="javascript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function isInternetExplorer() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {// If Internet Explorer, return version number
        return true;
    }
    else {
        return false;
    }
}

function goback(){

    if (isInternetExplorer()) {
        window.location.href = "s2ktaskscheduler.jsp";
    }
    else {
        window.location.href = "admin/tools/s2ktaskscheduler.jsp";
    }
}
function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}

function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}
</script>
<% if(redirect){ %>
<body onload = "goback()">
</body>
<% }else{ %>
<table border="0" cellpadding="0" cellspacing="0" width="900">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.s2k.tools.title3",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>

  <tr>
    <td>
    <% if(action.equals("edit") || action.equals("save")  )   { %>
    <form name="mainform" method="post" action="admin/tools/addedittasks.jsp?action=save&jobid=<%=jobid%>" onSubmit="return check();">
<%} else {%>
    <form name="mainform" method="post" action="admin/tools/addedittasks.jsp?action=add" onSubmit="return check();">
<%}%>

	    <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">



	          <table width="300">
<tr class="main">
               <td nowrap valign="top">
<font face="Arial Rounded MT Bold"><%
		if (searchErrors != null)
		{
		  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br><ul>");
		Enumeration enum1=searchErrors.keys();
		while(enum1.hasMoreElements())
		{
		  String strKey = enum1.nextElement().toString();
		  String strError = (String) searchErrors.get(strKey);
		  out.println("<li>" + strError+"</li>");
		}

		  out.println("</ul></font></td></tr></table>");
		}


%></font>
               <%=Languages.getString("jsp.admin.tools.s2k.select_date_range",SessionData.getLanguage())%>:</td>
               </tr>
<tr class="main" >
    <td nowrap><div width="15" ><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</div><input class="plain" name="startDate" value="<%=Taskhandler.getStartDate()%>" size="18"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
 </tr>
<tr class="main">
    <td nowrap ><div width="15" ><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:</div><input class="plain" name="endDate" value="<%=Taskhandler.getEndDate()%>" size="18"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
</tr>
<tr class="main" >
    <td nowrap><div width="15" ><%=Languages.getString("jsp.admin.tools.s2k.tools.task1",SessionData.getLanguage())%>:</div><input class="plain" name="jobstartDate" value="<%=Taskhandler.getjobstartDate()%>" size="18"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fJobPop(document.mainform.jobstartDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
   </td>

   </tr>
               <tr>
               <td valign="top" nowrap>

<table width="1300" border="0" >
<%
 if ( strAccessLevel.equals(DebisysConstants.ISO) )
 {
%>
    <%if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){ %>
    <div class="float" valign="top" align="left"  nowrap>
    <%=Languages.getString("jsp.admin.reports.transactions.agents.option",SessionData.getLanguage())%><br/>
        <select name="selectagentslist" id='selectagentslist' size="10" multiple='multiple' onchange="showSub(<%=strRefId%>,'agent');">
    	       <% if(Taskhandler.getSelectionType()==6){%>
    	          <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}
				else{ %>
				<option value="all" ><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}
							  Vector vecAgentList = TransactionReport.getAgentList(SessionData);
							  Iterator ite = vecAgentList.iterator();
							  while (ite.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();

							    if( (action.equals("edit") || load) && Taskhandler.getSelectionType()!=6 ){
							    	if(Taskhandler.checkagentlist(vecTemp.get(0).toString())){
							    		out.println("<option value=" + vecTemp.get(0) +" selected >" + vecTemp.get(1) + "</option>");
									    		if(agentlist!="")
									    			agentlist += "," + vecTemp.get(0).toString() ;
									    		else
									    			agentlist = vecTemp.get(0).toString() ;

							    		}
							    	else
							    		out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							    }
							    else
							    	out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
							  %>
        </select></div>



        <% if( (Taskhandler.getSelectionType()==6 || !action.equals("edit")) && ( !load || agentlist.length()==0 ) ){%>
    <div class="float" valign=top   align="left"  name="subagentslistbox" id='subagentslistbox' style="display:none;" nowrap>
        <%=Languages.getString("jsp.admin.reports.transactions.subagents.option",SessionData.getLanguage())%><br>
        <select name="subagentslist" id='subagentslist' size="10" multiple='multiple' style="display:none;" onchange="showSub(<%=strRefId%>,'subagent');">
        </select></div>
        <%}else{ %>

    <div class="float" valign=top   align="left"  name="subagentslistbox" id='subagentslistbox' style="display:block;" nowrap>
        <%=Languages.getString("jsp.admin.reports.transactions.subagents.option",SessionData.getLanguage())%><br>
        <select name="subagentslist" id='subagentslist' size="10" multiple='multiple' style="display:block;" onchange="showSub(<%=strRefId%>,'subagent');">
		 <% if(Taskhandler.getSelectionType()==4){%>
    	          <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}
				else{ %>
				<option value="all" ><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}

							  Vector vecSubAgentList = TransactionReport.S2KgetSubAgentList(strRefId,agentlist);
							  Iterator ites = vecSubAgentList.iterator();
							  while (ites.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ites.next();
							    if( (action.equals("edit") || load) && Taskhandler.getSelectionType()!=4){
							    	if(Taskhandler.checksubagentlist(vecTemp.get(0).toString())){
							    		out.println("<option value=" + vecTemp.get(0) +" selected >" + vecTemp.get(1) + "</option>");
							    		if(subagentlist!="")
									    			subagentlist += "," + vecTemp.get(0).toString() ;
									    		else
									    			subagentlist = vecTemp.get(0).toString() ;
							    	}else
							    		out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							    }
							    else
							    	out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
							  %>

				  </select></div>
				<%} %>

<%}%>


    <%
    Vector vecRepList = null;
    if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){ %>
    <div class="float" valign=top  align="left" name="repslistbox" id='repslistbox' nowrap>
    <%=Languages.getString("jsp.admin.reports.Select_Reps",SessionData.getLanguage())%><br>

       <select name="repslist" id="repslist" size="10" multiple style="display:block;" onchange="showSub(<%=strRefId%>,'reps');">

       <% if(Taskhandler.getSelectionType()==7){%>
    	          <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}
				else{ %>
				<option value="all" ><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}
							  vecRepList = TransactionReport.getRepList(SessionData);
							  Iterator ite = vecRepList.iterator();


							  while (ite.hasNext())
							  {

							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();
							    if( (action.equals("edit") || load)  && Taskhandler.getSelectionType()!=7 ){
							    	if(Taskhandler.checkreplist(vecTemp.get(0).toString()) ){

							    		out.println("<option value=" + vecTemp.get(0) +" selected >" + vecRepList.get(1) + "</option>");

							    		if(replist!="")
									    			replist += "," + vecTemp.get(0).toString() ;
									    		else
									    			replist = vecTemp.get(0).toString() ;
							    	}else
							    		out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							    }
							    else
							    	out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");

							  }
							  %>
				  </select></div>
     <% }else{ %>



        <% 
if((Taskhandler.getSelectionType()==6 || Taskhandler.getSelectionType()==4  || !action.equals("edit")) && ( !load || subagentlist.length()==0 ) ){%>
      <div class="float" valign=top  align="left" name="repslistbox" id='repslistbox' style="display:none;" nowrap>
    <%=Languages.getString("jsp.admin.reports.Select_Reps",SessionData.getLanguage())%><br>
         <select name="repslist" id="repslist" size="10" multiple style="display:none;" onchange="showSub(<%=strRefId%>,'reps');">
         <%}else{%>
         <div class="float" valign=top  align="left" name="repslistbox" style="display:block;"  id='repslistbox' nowrap>
    <%=Languages.getString("jsp.admin.reports.Select_Reps",SessionData.getLanguage())%><br>

       <select name="repslist" id="repslist" size="10" multiple style="display:block;"  onchange="showSub(<%=strRefId%>,'reps');">

       <% if(Taskhandler.getSelectionType()==5  ){%>
    	          <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}
				else{ %>
				<option value="all" ><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}

							  vecRepList = TransactionReport.getS2KRepList(subagentlist);
							  Iterator ite = vecRepList.iterator();
							  while (ite.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();
							    if(  (action.equals("edit") || load)   && Taskhandler.getSelectionType()!=5 ){
							    	if(Taskhandler.checkreplist(vecTemp.get(0).toString()) ){

							    		out.println("<option value=" + vecTemp.get(0) +" selected >" + vecTemp.get(1) + "</option>");

							    		if(replist!="")
									    			replist += "," + vecTemp.get(0).toString() ;
									    		else
									    			replist = vecTemp.get(0).toString() ;
							    	}else
							    		out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							    }
							    else
							    	out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
				}
							  %>
       </select></div>
    <% }%>
  <% if(((Taskhandler.getSelectionType()!=1 && Taskhandler.getSelectionType()!=8)  || !action.equals("edit"))  && ( !load  || replist.length()==0)  ){%>
  <div class="float" VALIGN="top" align="left"  name="merchantslistbox" id='merchantslistbox' style="display:none;" nowrap>
  <%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%><br>
    <SELECT NAME="merchantslist" id="merchantslist" SIZE=10 multiple="multiple"  style="display:none;" >
    </SELECT></div>
    	<%}
				else{ %>
				 <div class="float" VALIGN="top" align="left"  name="merchantslistbox" id='merchantslistbox' style="display:block;" nowrap>
  <%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%><br>
    <SELECT NAME="merchantslist" id="merchantslist" style="display:block;"  SIZE=10 multiple="multiple"   >
    <%

     if(Taskhandler.getSelectionType()==1){%>
    	          <option value="all" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}
				else{ %>
				<option value="all" ><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
				<%}

							  Vector vecMerchantList = TransactionReport.getS2KMerchantsList(replist);
							  Iterator ite = vecMerchantList.iterator();
							  while (ite.hasNext())
							  {
							    Vector vecTemp = null;
							    vecTemp = (Vector) ite.next();
							    if( (action.equals("edit") || load) && Taskhandler.getSelectionType()!=1 ){

							    	if(Taskhandler.checkmerchantlist(vecTemp.get(0).toString()))
							    		out.println("<option value=" + vecTemp.get(0) +" selected >" + vecTemp.get(1) + "</option>");
							    	else
							    		out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							    }
							    else
							    	out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
							  }
							  %>

    </SELECT>
                                 </div>
                                <% }%>
                                <tr><td>
                 
                                <% if ((Taskhandler.getSelectionType() == 6 || Taskhandler.getSelectionType() == 4 || !action.equals("edit")) && (!load || subagentlist.length() == 0)) {%>
                                <div class="float" valign="top"  align="left" name="ratePlanListbox" id="ratePlanListbox" style="display:none;" nowrap>
                                    <%=Languages.getString("jsp.admin.tools.s2k.rateplan", SessionData.getLanguage())%><br>
                                    <select name="ratePlanList" id="ratePlanList" size="10" multiple="multiple" style="display:none;">
                                        <option value="all" selected><%=Languages.getString("jsp.admin.reports.all", SessionData.getLanguage())%></option>
                                    </SELECT>
                                </div>
                                
                                <%}
                                else {%>
                                <div class="float" valign=top  align="left" name="ratePlanListbox" style="display:block;"  id="ratePlanListbox" nowrap>
                                    <%=Languages.getString("jsp.admin.tools.s2k.rateplan", SessionData.getLanguage())%><br>

                                    <select name="ratePlanList" id="ratePlanList" size="12" multiple style="display:block;">
                                        <%ArrayList<String> plans = new ArrayList<String>();
                                        if(!replist.isEmpty()){
                                            plans = Merchant.getRatePlansByRepList(replist);
                                        }
                                        
                                        else if(vecRepList != null && !vecRepList.isEmpty()){
                                            plans = Merchant.getRatePlansByRepsVector(vecRepList);
                                        }

                                        %>
                                        <option value="all" <%=(Taskhandler.getRateplansIdList().size() == 0)?"selected":""%>><%=Languages.getString("jsp.admin.reports.all", SessionData.getLanguage())%></option>
                                        <% 
                                                
                                                    for(String plan : plans){
                                                        String[] planInfo = plan.split("\\#");
                                                        if ((action.equals("edit") || load) ) {
                                                            if (Taskhandler.checkRatePlanlist(planInfo[0])) {
                                                                out.println("<option value=" + planInfo[0] + " selected >" + planInfo[1] + "</option>");
                                                            }
                                                            else {
                                                                out.println("<option value=" + planInfo[0] + ">" + planInfo[1] + "</option>");
                                                            }
                                                        }
                                                        else {
                                                            out.println("<option value=" + planInfo[0] + ">" + planInfo[1] + "</option>");
                                                        }
                                                    }
                                                
                                            }
                                        %>

                                    </select>
                                </div>
                                        </td>
                                        </tr>
                 
<%

 }

%>

</table>
               </td>
              </tr>
     <tr class="main" >
     <td nowrap valign="top"><%=Languages.getString("jsp.admin.tools.s2k.addedit.companycode",SessionData.getLanguage())%>:  <input class="plain" name="companycode" value="<%=Taskhandler.getcompanycode()%>"  maxlength="4"  size="4">
        <%=Languages.getString("jsp.admin.tools.s2k.addedit.location",SessionData.getLanguage())%>:  <input class="plain" name="location"  value="<%=Taskhandler.getLocation()%>"  maxlength="6"  size="6">
        <%=Languages.getString("jsp.admin.tools.s2k.addedit.shipto",SessionData.getLanguage())%>:  <input class="plain" name="shipto" value="<%=Taskhandler.getshipto()%>"  maxlength="4"  size="4">
     </td>
     </tr>
    <tr>
    <td class="main" colspan="4" align="center">
<% if(action.equals("edit") || action.equals("save") )   { %>
 	     <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.tools.s2k.save",SessionData.getLanguage())%>">
<%} else {%>
 	     <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.tools.s2k.add",SessionData.getLanguage())%>">
<%}%>
<input type="button" name="cancel" onclick="goback();" value="<%=Languages.getString("jsp.admin.tools.s2k.cancel",SessionData.getLanguage())%>" />
    </td>
</tr>
<tr>
<td class="main" colspan="4">
* <%=Languages.getString("jsp.admin.tools.s2k.instructions",SessionData.getLanguage())%>
</td>
</tr>
<tr>
<td class="main" colspan="4">
* <%=Languages.getString("jsp.admin.tools.s2k.instructions2",SessionData.getLanguage())%>
</td>
</tr>
            </table>

          </td>
      </tr>
    </table>

</form>
</td>
</tr>

</table>
</td>
</tr>
</table>

<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_time.js" id="gToday:datetime:agenda.js:gfPop:plugins_time.js" src="admin/CalenderXP/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">
</iframe>
<%}%>

<%@ include file="/includes/footer.jsp" %>