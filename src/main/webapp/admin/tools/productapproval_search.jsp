<%@ page import="com.debisys.languages.Languages,
				 com.debisys.customers.Merchant,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.StringUtil,
				 com.debisys.utils.DateUtil,
				 java.util.Arrays,
				 java.util.Vector,
				 java.util.Iterator,
				 java.util.Enumeration" %>
<%@page import="com.debisys.customers.CustomerSearch"%>
<%@page import="com.debisys.customers.SkuGlue"%>
<%
int section = 9;
int section_page = 20;
response.setCharacterEncoding("iso-8859-1");
%> 
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
Vector vResults = new Vector();
if ( request.getParameter("frmApply") != null )
{//If a change in credit/recycle/reject was made
	try
	{
		boolean bResultsProcessed = false;		
		Enumeration enParams = request.getParameterNames();
		int nCounter = 0;
%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<table border="0" cellpadding="0" cellspacing="0" width="550">
	<tr>
		<td background="images/top_blue.gif" width="18" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.productapproval.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="18" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
<%
		int nRow = 1;

		//First loop to count how many items has this enum
		while ( enParams.hasMoreElements() )
		{
			if ( enParams.nextElement().toString().startsWith("chk_") )
			{
				nCounter++;
			}
		}
		String sParams[] = new String[nCounter];
		enParams = request.getParameterNames();
		nCounter = 0;

		//Second loop to store tokens
		while ( enParams.hasMoreElements() )
		{
			String sToken = enParams.nextElement().toString();
			if ( sToken.startsWith("chk_") )
			{
				sParams[nCounter++] = sToken;
			}
		}

		Arrays.sort(sParams);//Now sort the tokens

		for ( nCounter = 0; nCounter < sParams.length; nCounter++ )
		{//Thru request params
			String sNextId = sParams[nCounter];
			if ( sNextId.startsWith("chk_") && !sNextId.equals("chk_0") )
			{//If user made a change in results
				if ( !bResultsProcessed )
				{
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.productapproval.ApprovalID",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.productapproval.MerchantID",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.productapproval.MerchantName",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.productapproval.ProductID",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.productapproval.description",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.customers.products.denied",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.customers.products.approve",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" align="center"><%=Languages.getString("jsp.admin.customers.products.pending",SessionData.getLanguage()).toUpperCase()%></td>
					</tr>
				</thead>
<%
					bResultsProcessed = true;
				}
				
				long lApprovalId = Long.parseLong(sNextId.replaceAll("chk_", ""));
				long lMerchantId = Long.parseLong(request.getParameter("mid_" + Long.toString(lApprovalId)));
				String sMerchantName = request.getParameter("mnm_" + Long.toString(lApprovalId));
				long lProductId = Long.parseLong(request.getParameter("pid_" + Long.toString(lApprovalId)));
				String sProductName = request.getParameter("pnm_" + Long.toString(lApprovalId));
				int nOldStatus = Integer.parseInt(request.getParameter("old_" + Long.toString(lApprovalId)));
				int nNewStatus = Integer.parseInt(request.getParameter(sNextId));
				String sNotes = "Customer Note: Merchant Updated: Product: " + lProductId + " " + sProductName + " ";
				int nDisabled = 0;
%>
				<tr class="row<%=nRow%>">
					<td align="right"><%=lApprovalId%></td>
					<td align="right"><%=lMerchantId%></td>
					<td align="right"><%=sMerchantName%></td>
					<td align="right"><%=lProductId%></td>
					<td align="right"><%=sProductName%></td>
					<td align="center"><%=(nNewStatus == 0)?SessionData.getString("jsp.admin.reports.pinreturn_search.yes"):" - "%></td>
					<td align="center"><%=(nNewStatus == 1)?SessionData.getString("jsp.admin.reports.pinreturn_search.yes"):" - "%></td>
					<td align="center"><%=(nNewStatus == 2)?SessionData.getString("jsp.admin.reports.pinreturn_search.yes"):" - "%></td>
<%
				if ( nOldStatus != nNewStatus )
				{
				  if (nNewStatus == 0)
				  {
				  	  sNotes += "approval denied.";
				  	  nDisabled = 1;
				  }
				  else if (nNewStatus == 1)
				  {
				  	  sNotes += "approval to sell.";
				  	  nDisabled = 0;
				  }
				  else if (nNewStatus == 2)
				  {
				   	  sNotes = "pending approval.";
				   	  nDisabled = 1;
				  }

					(new SkuGlue()).updateMerchantProductStatus(SessionData, Long.toString(lMerchantId), Long.toString(lProductId), Integer.toString(nNewStatus), sNotes);
					(new SkuGlue()).updateTerminalRatesSkuGlue(SessionData, Long.toString(lMerchantId), Long.toString(lProductId), nDisabled, sNotes);
				}
%>
				</tr>
<%
				nRow = (nRow == 1)?2:1;
			}//End of if user made a change in results
		}//End of thru request params
		if ( bResultsProcessed )
		{
%>
			</table>
		</td>
<%
		}
		else
		{
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class="main"><%=SessionData.getString("jsp.admin.reports.pinreturn_search.noitemschanged")%></td></tr>
			</table>
		</td>
<%
		}
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan="3" align="center">
			<form method="post" action="admin/tools/productapproval_search.jsp">
				<input type="submit" class="plain" value="<%=SessionData.getString("jsp.admin.reports.pinreturn_search.backtosearch")%>">
			</form>
		</td>
	</tr>
</table>
<%
	}
	catch (Exception e)
	{
		out.print(e.toString());
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b>ERROR IN PAGE</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class="main"><%=SessionData.getString("jsp.admin.reports.pinreturn_search.checklog")%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}
}//End of if a change in credit/recycle/reject was made
else if ( request.getParameter("frmSearch") != null )
{//If a search was made
	int nRecordCount = 0;
	int nPage = 1;
	int nPageSize = 50;
	int nPageCount = 1;
	response.setCharacterEncoding("iso-8859-1");

	try
	{
		int nStatusId = -1;
		String[] sISOs = new String[]{""};
		String[] sProducts = new String[]{""};
		String sMerchantDBA = null;
		long lCarrierId = 0;

		if ( request.getParameter("statusId").length() > 0 )
		{
			nStatusId = Integer.parseInt(request.getParameter("statusId"));
		}
		if ( request.getParameterValues("ISOIds") != null )
		{
			sISOs = request.getParameterValues("ISOIds");
		}
		if ( request.getParameterValues("productIds") != null )
		{
			sProducts = request.getParameterValues("productIds");
		}
		if ( request.getParameter("merchantDBA").length() > 0 )
		{
			sMerchantDBA = request.getParameter("merchantDBA");
		}
		if ( request.getParameter("carrierId").length() > 0 )
		{
			lCarrierId = Long.parseLong(request.getParameter("carrierId"));
		}
		if (request.getParameter("page") != null)
		{
			try
			{
				nPage = Integer.parseInt(request.getParameter("page"));
			}
			catch(NumberFormatException ex)
			{
				nPage = 1;
			}
		}

		if (nPage < 1)
		{
			nPage = 1;
		}
		vResults = (new TransactionReport()).getProductsApprovalByCarrier(Long.valueOf(SessionData.getProperty("ref_id")), nStatusId, sISOs, sProducts, sMerchantDBA, lCarrierId, nPage, nPageSize);
		nRecordCount = Integer.parseInt(vResults.get(0).toString());
		vResults.removeElementAt(0);
		if ( nRecordCount > 0 )
		{
			nPageCount = (nRecordCount / nPageSize);
			if ((nPageCount * nPageSize) < nRecordCount)
			{
				nPageCount++;
			}
		}
	}
	catch (Exception e)
	{
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b>ERROR IN PAGE</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class="main"><%=SessionData.getString("jsp.admin.reports.pinreturn_search.checklog")%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}
%>
<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROC.js" type="text/javascript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td background="images/top_blue.gif" width="18" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.productapproval.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="18" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
<%
	if ( vResults.size() > 0 )
	{//If there are results
%>
		<form id="frmPaging" method="post" action="admin/tools/productapproval_search.jsp">
			<input type="hidden" name="statusId" value="<%=StringUtil.toString(request.getParameter("statusId"))%>">
			<input type="hidden" name="ISOIds" value="<%=StringUtil.toString(request.getParameter("ISOIds"))%>">
			<input type="hidden" name="productIds" value="<%=StringUtil.toString(request.getParameter("productIds"))%>">
			<input type="hidden" name="merchantDBA" value="<%=StringUtil.toString(request.getParameter("merchantDBA"))%>">
			<input type="hidden" name="carrierId" value="<%=StringUtil.toString(request.getParameter("carrierId"))%>">
			<input type="hidden" id="nPage" name="page" value="<%=StringUtil.toString(request.getParameter("page"))%>">
			<input type="hidden" name="frmSearch">
		</form>
		<form method="post" action="admin/tools/productapproval_search.jsp" onsubmit="document.getElementById('btnApplyChanges').disabled=true;">
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td class="main"><%=SessionData.getString("jsp.admin.reports.click_to_sort")%></td>
					<td class="main" align="right">
<%
		if ( nPage > 1 )
		{
			out.println("<a href=\"javascript:\" onclick=\"$('#nPage').val(1);$('#frmPaging').submit();\">" + Languages.getString("jsp.admin.first",SessionData.getLanguage()) + "</a>&nbsp;");
			out.println("<a href=\"javascript:\" onclick=\"$('#nPage').val(" + (nPage - 1) + ");$('#frmPaging').submit();\">&nbsp;&lt;&lt;" + Languages.getString("jsp.admin.previous",SessionData.getLanguage()) + "</a>&nbsp;");
		}
		int nLowerLimit = nPage - 12;
		int nUpperLimit = nPage + 12;

		if ( nLowerLimit < 1 )
		{
			nLowerLimit = 1;
			nUpperLimit = 25;
		}

		for (int i = nLowerLimit; i <= nUpperLimit && i <= nPageCount; i++)
		{
			if ( i == nPage )
			{
				out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
			}
			else
			{
				out.println("<a href=\"javascript:\" onclick=\"$('#nPage').val(" + i + ");$('#frmPaging').submit();\">" + i + "</a>&nbsp;");
			}
		}
		
		if ( nPage <= (nPageCount - 1) )
		{
			out.println("<a href=\"javascript:\" onclick=\"$('#nPage').val(" + (nPage + 1) + ");$('#frmPaging').submit();\">" + Languages.getString("jsp.admin.next",SessionData.getLanguage()) + "&nbsp;&gt;&gt;</a>&nbsp;");
			out.println("<a href=\"javascript:\" onclick=\"$('#nPage').val(" + nPageCount + ");$('#frmPaging').submit();\">" + Languages.getString("jsp.admin.last",SessionData.getLanguage()) + "</a>");
		}
%>
					</td>
				</tr>
			</table>
			<script>
			function ToggleRadio(sOption, bValue)
			{
				$('input[id|="chk' + sOption + '"]').each(function(nIndex, oItem)
				{
					oItem.checked = bValue;
				});
			}
			</script>
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
						<td class="rowhead2" style="background-image: url(images/top_blue.png);">#</td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.admin.customers.products.denied").toUpperCase())%><input type="radio" name="chk_0" onclick="ToggleRadio('D', this.checked);"></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.admin.customers.products.approve").toUpperCase())%><input type="radio" name="chk_0" onclick="ToggleRadio('A', this.checked);"></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.admin.customers.products.pending").toUpperCase())%><input type="radio" name="chk_0" onclick="ToggleRadio('P', this.checked);"></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.ISOName").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.RepName").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.MerchantName").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.MerchantDBA").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.MerchantID").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.address").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.city").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.state").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.zip").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.phone").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.ProductID").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.description").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.carrier_id").toUpperCase())%></td>
						<td class="rowhead2" style="background-image: url(images/top_blue.png);" align="center"><%=StringUtil.formatTableCellTitle(SessionData.getString("jsp.productapproval.approvalstatusbit").toUpperCase())%></td>
					</tr>
				</thead>
<%
		int nRow = 1;
		for ( int i = 0; i < vResults.size(); i++ )
		{
			Vector vTemp = (Vector)vResults.get(i);
%>
				<tr class="row<%=nRow%>">
					<td><%=i + 1%>
						<input type="hidden" name="mid_<%=vTemp.get(14)%>" value="<%=vTemp.get(4)%>">
						<input type="hidden" name="mnm_<%=vTemp.get(14)%>" value="<%=vTemp.get(2)%>">
						<input type="hidden" name="pid_<%=vTemp.get(14)%>" value="<%=vTemp.get(10)%>">
						<input type="hidden" name="pnm_<%=vTemp.get(14)%>" value="<%=vTemp.get(11)%>">
						<input type="hidden" name="old_<%=vTemp.get(14)%>" value="<%=vTemp.get(13)%>">
					</td>
					<td align="center"><input type="radio" id="chkD-<%=vTemp.get(14)%>" name="chk_<%=vTemp.get(14)%>" value="0"></td>
					<td align="center"><input type="radio" id="chkA-<%=vTemp.get(14)%>" name="chk_<%=vTemp.get(14)%>" value="1"></td>
					<td align="center"><input type="radio" id="chkP-<%=vTemp.get(14)%>" name="chk_<%=vTemp.get(14)%>" value="2"></td>
					<td><%=vTemp.get(0).toString()%></td>
					<td><%=vTemp.get(1).toString()%></td>
					<td><%=vTemp.get(2).toString()%></td>
					<td><%=vTemp.get(3).toString()%></td>
					<td><%=vTemp.get(4).toString()%></td>
					<td><%=vTemp.get(5).toString()%></td>
					<td><%=vTemp.get(6).toString()%></td>
					<td><%=vTemp.get(7).toString()%></td>
					<td><%=vTemp.get(8).toString()%></td>
					<td><%=vTemp.get(9).toString()%></td>
					<td><%=vTemp.get(10).toString()%></td>
					<td><%=vTemp.get(11).toString()%></td>
					<td><%=vTemp.get(15).toString() + " (" + vTemp.get(12).toString() + ")"%></td>
					<td>
<%
			switch ( Integer.parseInt(vTemp.get(13).toString()) )
			{
				case 0:
					out.print(Languages.getString("jsp.admin.customers.products.denied",SessionData.getLanguage()));
					break;
				case 1:
					out.print(Languages.getString("jsp.admin.customers.products.approve",SessionData.getLanguage()));
					break;
				case 2:
					out.print(Languages.getString("jsp.admin.customers.products.pending",SessionData.getLanguage()));
					break;
			}
%>
					</td>
				</tr>
<%
			nRow = (nRow == 1)?2:1;
		}//End of for
%>
			</table>
			<script type="text/javascript">
				var stT1 = new SortROC(document.getElementById("t1"), ["None", "None", "None", "None", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", 
				"Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", 
				"Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"], 0, false, false);
			</script>
		</td>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan="3" align="center">
			<table>
				<tr><td><input id="btnApplyChanges" type="submit" value="<%=SessionData.getString("jsp.admin.reports.pinreturn_search.applychanges")%>"></td><td>&nbsp;&nbsp;&nbsp;</td><td><input type=reset value="<%=SessionData.getString("jsp.admin.reports.pinreturn_search.resetchanges")%>"></td></tr>
			</table>
		</td>
		<input type="hidden" name="frmApply" value="y">
		</form>
<%
	}//End of if there are results
	else
	{//Else show the message of no results
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class="main"><%=SessionData.getString("jsp.admin.reports.pinreturn_search.noresults")%></td></tr>
			</table>
		</td>

<%
	}//End of else show the message of no results
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan="3" align="center">
			<form method="post" action="admin/tools/productapproval_search.jsp">
				<input type="submit" class="plain" value="<%=SessionData.getString("jsp.admin.reports.pinreturn_search.backtosearch")%>">
			</form>
		</td>
	</tr>
</table>
<%
}//End of if a search was made
else
{//Else we need to show the search page
%>
<script>
function FilterCarrierID(ctl)
{
	ctl.value = ctl.value.replace(/[^0-9]/g, "");
}
</script>
<form method="post" action="admin/tools/productapproval_search.jsp">
<input type="hidden" name="frmSearch">
<table border="0" cellpadding="0" cellspacing="0" width="550">
	<tr>
		<td background="images/top_blue.gif" width="18" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.productapproval.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="18" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="center" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="left" style="text-align:left;">
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td></td>
								<td class="main" nowrap="nowrap"><%=SessionData.getString("jsp.productapproval.status")%>:</td>
								<td></td>
								<td>
									<select name="statusId">
										<option value=""><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
										<option value="0"><%=Languages.getString("jsp.admin.customers.products.denied",SessionData.getLanguage())%></option>
										<option value="1"><%=Languages.getString("jsp.admin.customers.products.approve",SessionData.getLanguage())%></option>
										<option value="2"><%=Languages.getString("jsp.admin.customers.products.pending",SessionData.getLanguage())%></option>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td class="main" nowrap="nowrap"><%=SessionData.getString("jsp.productapproval.selectISO")%>:</td>
								<td></td>
								<td>
									<select name="ISOIds" size="10" multiple="multiple">
										<option value="" selected="selected"><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
	Vector vISOs = (new CustomerSearch()).searchRep(1, Integer.MAX_VALUE, SessionData, DebisysConstants.REP_TYPE_ISO_5_LEVEL);
	if ( vISOs.size() > 1 )
	{
		vISOs.remove(0);
		Iterator it = vISOs.iterator();
		while (it.hasNext())
		{
			Vector vTemp = null;
			vTemp = (Vector) it.next();
			out.println("<option value=" + vTemp.get(1) +">" + vTemp.get(0) + "</option>");
		}
	}
	else
	{
		out.println("<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>");
	}
%>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=SessionData.getString("jsp.admin.reports.pinreturn_search.selectproducts")%>:</td>
								<td></td>
							    <td>
							    	<select name="productIds" size="10" multiple="multiple">
										<option value="" selected="selected"><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
<%
    Vector vecProductList = TransactionReport.getProductsByCarrier(Long.valueOf(SessionData.getProperty("ref_id")));
    if ( vecProductList.size() > 0 )
    {
	    Iterator itProduct = vecProductList.iterator();
	    while (itProduct.hasNext())
	    {
	      Vector vecTemp = null;
	      vecTemp = (Vector) itProduct.next();
	      out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + " (" + vecTemp.get(0) + ")</option>");
	    }
    }
    else
    {
			out.println("<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>");
    }
%>
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=SessionData.getString("jsp.productapproval.merchantDBA")%>:</td>
								<td></td>
							    <td><input type="text" class="plain" name="merchantDBA" maxlength="50"></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap="nowrap"><%=SessionData.getString("jsp.productapproval.carrierId")%>:</td>
								<td></td>
							    <td><input type="text" class="plain" name="carrierId" maxlength="12" onblur="FilterCarrierID(this);"></td>
							</tr>
							<tr><td><br></td></tr>
							<tr>
								<td><br></td><td colspan="3" align="left"><input type="submit" id="btnSearch" class="plain" value="<%=SessionData.getString("jsp.admin.reports.pinreturn_search.search")%>"></td>
							</tr>
							<tr><td><br></td></tr>
						</table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	            </tr>
			</table>
		</td>
	</tr>
</table>
</form>
<%
}//End of else we need to show the search page
%>
<%@ include file="/includes/footer.jsp" %>