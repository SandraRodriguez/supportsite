<%-- 
    Document   : isoLoginList
    Created on : Jun 29, 2017, 8:49:13 PM
    Author     : dgarzon
--%>


<%@page import="java.math.BigDecimal"%>
<%@page import="com.debisys.ach.TransactionSearch"%>
<%@page import="com.debisys.tools.isoLogin.IsoLoginVo"%>
<%@page import="com.debisys.tools.isoLogin.IsoLoginConfiguration"%>
<%@page import="com.debisys.tools.depositConfiguration.DepositTypesConf"%>
<%@page import="com.debisys.reports.banks.Banks"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesVo"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesConf"%>
<%@page import="com.debisys.tools.depositConfiguration.DepositTypesVo"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 38;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script language="JavaScript" src="/support/includes/primeui/primeui-2.0-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/isoLoginUsersJS.js"></script>


<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/primeui/primeui-2.0-min.js" language="JavaScript" charset="utf-8"></script>

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
    #spanWarningMessages, #spanWarningMessagesProducts{
        font-size: 17px;
        color: red;
    }

    .windowsFloatCharging {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }


</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#div_form').css("display", "none");
    });

</script>


<%
    String actionEvent = "insertIsoLogin";
    String msg = request.getParameter("msg");
    msg = (msg == null) ? "" : msg;

    String action = request.getParameter("action");
    String isoUserId = request.getParameter("isoUserId");
    
    BigDecimal defaultRepsList = null;
    if(request.getParameter("defaultRepsList") != null){
        defaultRepsList = new BigDecimal(request.getParameter("defaultRepsList"));
    }

    IsoLoginVo dtEdit = null;
    if (action != null && action.trim().equals("edit")) {
        dtEdit = IsoLoginConfiguration.getIsoLoginByPK(isoUserId);
        actionEvent = "editIsoLogin";
    }
    else if (action != null && action.trim().equals("insertIsoLogin")) {
        IsoLoginVo isoLoginVo = new IsoLoginVo();
        isoLoginVo.setIsoId(new BigDecimal(request.getParameter("isoList")));
        isoLoginVo.setUserId(request.getParameter("userId"));
        isoLoginVo.setPassword(request.getParameter("password"));
        isoLoginVo.setDefaultRepId(defaultRepsList);
        boolean couldInsert = IsoLoginConfiguration.insertIsoLogin(SessionData, isoLoginVo);
        if(!couldInsert){
            msg = Languages.getString("jsp.admin.customers.terminal.uniqueuserid", SessionData.getLanguage());
        }
        
    }
    else if (action != null && action.trim().equals("editIsoLogin")) {
        IsoLoginVo isoLoginVo = new IsoLoginVo();
        isoLoginVo.setId(request.getParameter("idISOLogin"));        
        isoLoginVo.setIsoId(new BigDecimal(request.getParameter("isoList")));
        isoLoginVo.setUserId(request.getParameter("userId"));
        isoLoginVo.setPassword(request.getParameter("password"));
        isoLoginVo.setDefaultRepId(defaultRepsList);
        boolean couldEdit = IsoLoginConfiguration.updateIsoLogin(isoLoginVo);
        if(!couldEdit){
            msg = Languages.getString("jsp.admin.customers.terminal.uniqueuserid", SessionData.getLanguage());
        }
    }
    else if (action != null && action.trim().equals("removeIsoLogin")) {
        IsoLoginConfiguration.deleteIsoLoginById(isoUserId);
    }
    
    String basicUrlToken = DebisysConfigListener.getUrlBasicIsoToken(application); 
    List<IsoLoginVo> isoLoginUsersList = IsoLoginConfiguration.getIsoLoginUsersList();
    
    Vector repsList = null;
    if(dtEdit != null){
        repsList = IsoLoginConfiguration.getRepListByIso(dtEdit.getIsoId().toString());
    }


%>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="windowsFloatCharging">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>    

<table border="0" cellpadding="0" cellspacing="0" width="1200" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <%=Languages.getString("jsp.admin.tools.isoLogin.title", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">

            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                <tr align="center">
                    <td>
                        <span id="spanWarningMessages"><%=msg%></span>
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        <div id="div_form_isoLogin" style="<%=(action != null && action.equals("edit") ? "" : "display:none;")%>width:100%;height:100%;">
                            <fieldset class="field_set">
                                <legend></legend>
                                <form action="admin/tools/isoLogin/isoLoginList.jsp" method="POST" onsubmit="return validateForm('<%=Languages.getString("jsp.admin.customers.terminal.uniqueuserid", SessionData.getLanguage())%>', '<%=action%>', '<%=isoUserId%>');">
                                    <table width="100%" border="0" cellspacing="1" align="center" height="40">
                                        <tr>
                                            <td><%=Languages.getString("jsp.admin.iso_name", SessionData.getLanguage())%></td>
                                            <td><%=Languages.getString("jsp.admin.tools.isoLogin.userId", SessionData.getLanguage())%></td>
                                            <td><%=Languages.getString("jsp.admin.tools.isoLogin.password", SessionData.getLanguage())%></td>
                                            <td>Rep</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="isoList" name="isoList" onchange="changeRepsByIso()">
                                                    <option value="-1"></option>
                                                    <%
                                                        Vector vecIsoList = IsoLoginConfiguration.getIsoList((dtEdit != null)?dtEdit.getIsoId():null);
                                                        Iterator itIL = vecIsoList.iterator();
                                                        while (itIL.hasNext()) {
                                                            Vector vecTempIL = null;
                                                            vecTempIL = (Vector) itIL.next();
                                                            out.println("<option value=\"" + vecTempIL.get(0) + "\"" + (dtEdit != null && dtEdit.getIsoId().toString().equalsIgnoreCase(vecTempIL.get(0).toString()) ? "selected" : "") + ">" + vecTempIL.get(1) + "</option>");
                                                        }
                                                    %>                                 	
                                                </select>	

                                            </td>
                                            
                                            <td><input type="text" name="userId" id="userId" value="<%=(dtEdit != null) ? dtEdit.getUserId(): ""%>" maxlength="50"></td>
                                            <td><input type="text" name="password" id="password" value="<%=(dtEdit != null) ? dtEdit.getPassword(): ""%>" maxlength="50"></td>
                                            
                                            
                                            <td>                                        
                                                <select name="defaultRepsList" id = "defaultRepsList" style="width: 260px;">
                                                    <%
                                                        if(repsList != null){
                                                        Iterator itIL2 = repsList.iterator();
                                                        while (itIL2.hasNext()) {
                                                            Vector vecValues = (Vector) itIL2.next();

                                                        %>
                                                        <option value="<%=vecValues.get(0)%>" <%=(dtEdit != null && dtEdit.getDefaultRepId() != null && String.valueOf(vecValues.get(0)).equalsIgnoreCase(dtEdit.getDefaultRepId().toString())) ? "selected" : ""%>><%="("+vecValues.get(0)+") "+vecValues.get(1)%></option>

                                                        <%}
                                                    }%>
                                                </select>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="hidden" name="action" id="action" value="<%=actionEvent%>" />
                                                <input type="hidden" name="idISOLogin" id="idISOLogin" value="<%=isoUserId%>" />
                                                <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%>" onclick="hideForm();">
                                                <input type="submit" value="<%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%>" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </fieldset>
                        </div>
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.isoLogin.newIsoLogin", SessionData.getLanguage())%>" onclick="newIsoLogin();">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="80%" border="0" cellspacing="1" align="center" height="40">
                            <tr class="rowhead2">
                                <td><%=Languages.getString("jsp.admin.iso", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.iso_name", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.isoLogin.userId", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.isoLogin.password", SessionData.getLanguage())%></td>
                                <td>URL</td>
                                <td>Rep</td>
                                <td><%=Languages.getString("jsp.admin.genericLabel.options", SessionData.getLanguage())%></td>
                            </tr>
                            <%
                                int index = 0;
                                for (IsoLoginVo isoLogin : isoLoginUsersList) {
                                    index++;
                            %>
                            <tr class="row<%=(index % 2 == 0) ? "1" : "2"%>">
                                <td><%=isoLogin.getIsoId()%></td>
                                <td><%=isoLogin.getIsoBusinessName()%></td>
                                <td><%=isoLogin.getUserId()%></td>
                                <td><%=isoLogin.getPassword()%></td>
                                <td><%=basicUrlToken+isoLogin.getToken()%></td>
                                <td><%=((isoLogin.getDefaultRepId() != null)?isoLogin.getDefaultRepId()+" - ":"")+" "+isoLogin.getDefaultRepBusinessName()%></td>
                                <td align="center"><input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.edit", SessionData.getLanguage())%>" onclick="editIsoLoginUsers('<%=isoLogin.getId()%>');">
                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%>" onclick="deleteIsoLogin('<%=isoLogin.getId()%>', '<%=Languages.getString("jsp.admin.genericLabel.removeItemQuestion", SessionData.getLanguage())%> (<%=isoLogin.getUserId()%>)', '<%=Languages.getString("jsp.admin.genericLabel.canNotDelete", SessionData.getLanguage())%>');"></td>
                            </tr>
                            <%}%>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

<script LANGUAGE="JavaScript">


</script>

<%@ include file="/includes/footer.jsp" %>