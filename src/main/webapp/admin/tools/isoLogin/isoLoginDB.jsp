<%-- 
    Document   : isoLoginDB
    Created on : Jul 5, 2017, 19:34:14 AM
    Author     : dgarzon
--%>


<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="com.debisys.tools.isoLogin.IsoLoginConfiguration"%>
<%@page import="com.debisys.tools.isoLogin.IsoLoginVo"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String action = request.getParameter("action");

    if (action.equals("insertIsoLogin")) {
        String userId = request.getParameter("user");
        IsoLoginVo isoLoginDB = IsoLoginConfiguration.getIsoLoginByUserId(userId);
        if (isoLoginDB != null) {
            out.println(false);
        }
        else {
            out.println(true);
        }
    }

    if (action.equals("edit")) {
        String id = request.getParameter("id");
        String userId = request.getParameter("user");
        IsoLoginVo isoLoginDB = IsoLoginConfiguration.getIsoLoginByUserId(userId);
        if (isoLoginDB != null && !isoLoginDB.getId().equalsIgnoreCase(id)) {
            out.println(false);
        }
        else {
            out.println(true);
        }
    }

    if (action.equals("getRepsByIso")) {
        String isoId = request.getParameter("isoId");
        Vector list = IsoLoginConfiguration.getRepListByIso(isoId);
        Iterator itIL = list.iterator();
        while (itIL.hasNext()) {
            Vector vecTempIL = (Vector) itIL.next();
            out.println(vecTempIL.get(0) + "|(" + vecTempIL.get(0)+") "+vecTempIL.get(1));
        }
    }

%>