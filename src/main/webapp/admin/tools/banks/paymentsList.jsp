<%@ page language="java" import="java.util.*,
							     com.debisys.utils.StringUtil,
							     com.debisys.languages.Languages,
							     com.debisys.reports.banks.BankPaymentRecords" pageEncoding="ISO-8859-1"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";		
	String showingPageOf = Languages.getString("jsp.admin.rateplans.ShowingPageOf", SessionData.getLanguage());
%>
<table width="100%">
	<tr>
		<% 
		String styleImages="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 1px; PADDING-TOP: 0px";
		%>
		<TABLE cellSpacing="0" cellPadding="0" >
			<TBODY>
				<TR class="main">
					<td id="OkDownloadTd" align="left">
						<input type="button" value="Download" onclick="downloadReport();"/>
					</td>					
					<TD align="right" class="main">
						<IMG id="ImgFirst" onclick="goToPage('ImgFirst');" title="Ir a la Primera P�gina" style="<%=styleImages%>" src="images/pagefirstdisabled.png" border="0" />
					</TD>
					<TD align="right" class="main">
						<IMG id="ImgPrevious" onclick="goToPage('ImgPrevious');" title="Ir a la Anterior P�gina" style="<%=styleImages%>" src="images/pageprevdisabled.png" border="0" />
					</TD>
					<TD align="right" class="main" noWrap id="showingPageOf">
						<%=showingPageOf%> 
					</TD>
					<TD align="right" class="main">
						<IMG id="ImgNext" onclick="goToPage('ImgNext');" title="Ir a la Siguiente P�gina" style="<%=styleImages%>" src="images/pagenextdisabled.png" border="0" />
					</TD>
					<TD align="right" class="main">
						<IMG id="ImgLast" onclick="goToPage('ImgLast');" title="Ir a la Ultima P�gina" style="<%=styleImages%>" src="images/pagelastdisabled.png" border="0" />
					</TD>					
				</TR>
			</TBODY>		
		</TABLE>
	</tr>
	<tr>
		<td>
			<div id="paymentsListPagination">			
			</div>
		</td>
	</tr>
</table>
<script type="text/javascript">
	
	var currentSlot=0;
	var sizePage=10;
	
	var topIndex=0;
	var bottomIndex=0;
	var showingPageLabel = "<%=showingPageOf%>";
		
	var showing = showingPageLabel.replace("{0}", "0");
	showing = showing.replace("{1}", "0");
	$("#showingPageOf").html(showing);
			
	function goToPage(imgId)
	{
		if ( imgId=="ImgFirst" )
		{
			currentSlot=0;
			showRecordsByRanges(MyArrayRanges[currentSlot].top,MyArrayRanges[currentSlot].bottom);		
		}
		else if ( imgId=="ImgPrevious" )
		{
			if ( currentSlot>0)
			{
				currentSlot--;
				showRecordsByRanges(MyArrayRanges[currentSlot].top,MyArrayRanges[currentSlot].bottom);
			}
					
		}
		else if ( imgId=="ImgNext" )
		{
			if ( (currentSlot+1) <= (MyArrayRanges.length-1) )
			{
				currentSlot++;
				showRecordsByRanges(MyArrayRanges[currentSlot].top,MyArrayRanges[currentSlot].bottom);
			}							
		}
		else if ( imgId=="ImgLast" )
		{
			//alert(MyArrayRanges.length);
			currentSlot = MyArrayRanges.length-1;
			//alert(currentSlot);
			showRecordsByRanges(MyArrayRanges[currentSlot].top,MyArrayRanges[currentSlot].bottom)
		}
		
	}
	
	
	function showRecordsByRanges(bottom,top)
	{
		var showing = showingPageLabel.replace("{0}", currentSlot);
		showing = showing.replace("{1}", MyArrayRanges.length-1);
		//alert(showing);
		$("#showingPageOf").html(showing);
		$("#waitTD").html("<span class='main'><%=Languages.getString("jsp.admin.reports.bankpayments.pleasewait",SessionData.getLanguage())%></span>");
		
		//alert("bottom:["+bottom+"] top:["+top+"]   totalRecords:["+totalRecords+"]");
		$("#paymentsTableRecords tr").each(
			  function()
			  {
					var valueMerchantSelect = $(this).attr("id");
					if ( valueMerchantSelect!="countersPaymentsTableRecords" && valueMerchantSelect!="titlesPaymentsTableRecords" )
					{						
						if ( valueMerchantSelect>=bottom && valueMerchantSelect<=top )
						{							
							$(this).show();
						}
						else
						{
							$(this).hide();							
						}
					}
			  }
			);
		setTimeout("$(\"#waitTD\").html(\"\");",500);		
	}
	
	
	
	function drawRecords()
	{
		var initDate = $("#txtStartDate").val();
		var endDate = $("#txtEndDate").val();
		var idRequest = $("#requestNumber").val();
		var idDocument = $("#IdDocumento").val();
		var amount = $("#amount").val();
		var siteId = $("#siteId").val();
				
		var bankCodesValues="";
		var merchantsValues="";
		var statusValues="";
		var urlDownload ="";
		
		var pageJSP = "admin/tools/banks/paymentsPagination.jsp";
		var itemsByPage = $("#itemsByPage option:selected").val();	
		
		var dateIni = $("#txtStartDate").val();
		var dateEnd = $("#txtEndDate").val();
				
		$("#merchantIds option:selected").each(
			  function()
			  {
					merchantsValues = merchantsValues + $(this).val()+",";
			  }
		 );
		 $("#bankCodes option:selected").each(
			  function()
			  {
					bankCodesValues = bankCodesValues + $(this).val()+",";
			  }
		 );
		 $("#status option:selected").each(
			  function()
			  {
					statusValues = statusValues + $(this).attr("id")+",";
			  }
		 );
		 
		
		if ( initDate.length > 0 && endDate.length > 0 )
		{
			$("#waitTD").html("<span class='main'><%=Languages.getString("jsp.admin.reports.bankpayments.pleasewait",SessionData.getLanguage())%></span>");
			
			$.post(pageJSP, 
				{ 
				   bankCodesValues: bankCodesValues,		
				   merchantsValues: merchantsValues,
				   idDocument: idDocument,
				   amount: amount,
				   idRequest: idRequest,
				   siteId: siteId,
				   statusValues: statusValues, 	
				   items: itemsByPage,
				   dateIni: dateIni,
				   dateEnd: dateEnd   	   
			  	},
				function okListPayments(data)
				{						
					$("#paymentsListPagination").html(data);
					$("#waitTD").html("");			
				}
			  );
		 }
		 
	  }
	
	  			
	  drawRecords();
	  	  
	  function objRanges(top,bottom)
	  {
		this.top = top;
		this.bottom=bottom;	
	  }
		  
</script>

