<%@ page language="java" import="java.util.*,
							     com.debisys.utils.StringUtil,
							     com.debisys.languages.Languages,
							     com.debisys.reports.banks.BankPaymentRecords" %>
							    
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>


<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
	ArrayList<BankPaymentRecords> payments = new ArrayList<BankPaymentRecords>();//BankPaymentRecords.findBankPayments();
	String urlDownload="";
	
	if ( payments!=null && payments.size()>0){
		urlDownload = basePath + BankPaymentRecords.downloadBankPayments(this.getServletContext(),SessionData,payments);		
	}		
%>
<table>
	<tr>
		<td>
			<a href="<%=urlDownload%>">View Here</a>
		</td>
	</tr>
</table>
<script type="text/javascript">
	window.open("<%=urlDownload%>","dwnloFileBanks");
</script>


