<%@ page language="java" import="java.util.*,
							     com.debisys.utils.StringUtil,
							     com.debisys.languages.Languages,
							     com.debisys.reports.banks.BankPaymentRecords" pageEncoding="ISO-8859-1"%>
							    
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();

	int items = Integer.parseInt(request.getParameter("items"));
	String dateIni = request.getParameter("dateIni");
	String dateEnd = request.getParameter("dateEnd");
	
	//*******************************************************************************************//
	//*******************************************************************************************//
	//*******************************************************************************************//
	int maxDifference = BankPaymentRecords.findPropertyMaxDays(com.debisys.utils.DebisysConfigListener.getInstance(this.getServletContext()));
	//*******************************************************************************************//
	//*******************************************************************************************//
	//*******************************************************************************************//
	
	String bankCodesValues = request.getParameter("bankCodesValues");
	String merchantsValues = request.getParameter("merchantsValues");
	String statusValues = request.getParameter("statusValues");
	String idDocument = request.getParameter("idDocument");
	String amount = request.getParameter("amount");
	String idRequest = request.getParameter("idRequest");
	String siteId = request.getParameter("siteId");
	
	String month= dateIni.substring(0,2);
	String day= dateIni.substring(3,5);
	String year= dateIni.substring(6,10);
	Date startDate = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month)-1, Integer.parseInt(day)).getTime();
	
	month= dateEnd.substring(0,2);
	day= dateEnd.substring(3,5);
	year= dateEnd.substring(6,10);
	Date endDate = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month)-1, Integer.parseInt(day)).getTime();				   
			
	long diff = endDate.getTime() - startDate.getTime();
	diff = ((diff / (1000L*60L*60L*24L)))+1;
    
    String changeEndDate="";    
    if (diff>maxDifference)
    {
    	//System.out.println("Difference between " + startDate);
    	//System.out.println(" and " + endDate + " is " + diff + " days.");
    
    	Calendar now = Calendar.getInstance();
    	now.setTimeInMillis(startDate.getTime());    	
        now.add(java.util.Calendar.DAY_OF_MONTH,maxDifference);
        
        year = String.valueOf(now.get(Calendar.YEAR));         	
    	int n_month = (now.get(Calendar.MONTH)) + 1;
    	month = String.valueOf(n_month);
    	day = String.valueOf(now.get(Calendar.DAY_OF_MONTH));
    	
    	//System.out.println("  YEAR                 : " + year);
        //System.out.println("  MONTH                : " + month);
        //System.out.println("  DAY_OF_MONTH         : " + day);
        
        java.text.DateFormat dateFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        String documentDate = dateFormatter.format(now.getTime());
        
        //System.out.println("documentDate : " + documentDate);
        changeEndDate="var dateEnd = $(\"#txtEndDate\").val('"+ documentDate +"');";
    }    
    
    dateEnd = dateEnd+" 23:59:59";      
        
	ArrayList<BankPaymentRecords> payments = BankPaymentRecords.findBankPayments(dateIni,dateEnd,idDocument,amount,bankCodesValues,statusValues,merchantsValues,siteId,idRequest,SessionData);
	String urlDownload="";
	
	if ( payments!=null && payments.size()>0)
	{
		urlDownload = basePath + BankPaymentRecords.downloadBankPayments(this.getServletContext(),SessionData,payments);		
	}	
%>

<table id="paymentsTableRecords" width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">	
	
	<thead>
		<tr id="titlesPaymentsTableRecords" class="SectionTopBorder">
			<td class=rowhead2 align=center valign="top">#</td>
			<td class=rowhead2 align=center valign="top">&nbsp;&nbsp;<%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.dba",SessionData.getLanguage()).toUpperCase())%>&nbsp;&nbsp;</td>
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.bankpayments.amount",SessionData.getLanguage()).toUpperCase())%></td>
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.bankpayments.bankCode",SessionData.getLanguage()).toUpperCase())%></td>
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.siteid",SessionData.getLanguage()).toUpperCase())%></td>
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.bankpayments.IdDocumento",SessionData.getLanguage()).toUpperCase())%></td>
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.requestnumber",SessionData.getLanguage()).toUpperCase())%></td>			
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.bankpayments.PaymentDate",SessionData.getLanguage()).toUpperCase())%></td>
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.payments.detailedrep_payments.report_date",SessionData.getLanguage()).toUpperCase())%></td>			
			<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.paymentrequest_search.status",SessionData.getLanguage()).toUpperCase())%></td>
		</tr>
	</thead>
	<tr>
	<% 
	int totalRecord=0;
	int rowClass=1;
	//
	for(BankPaymentRecords paymentRecord : payments)
	{
		if ( paymentRecord.getRecordId() <= items )
		{
		%>		
		<tr id="<%=paymentRecord.getRecordId()%>" class="row<%=rowClass%>" >
			<td align="center"><%=paymentRecord.getRecordId()%></td>
			<td align="center"><%=paymentRecord.getDba()%></td>
			<td align="center"><%=paymentRecord.getAmount()%></td>
			<td align="center"><%=paymentRecord.getBank()%></td>
			<td align="center"><%=paymentRecord.getMillennium_no()%></td>			
			<td align="center"><%=paymentRecord.getReferenceNumber()%></td>
			<td align="center"><%=paymentRecord.getDocumentId()%></td>
			<td align="center"><%=paymentRecord.getPaymentDate()%></td>			
			<td align="center"><%=paymentRecord.getRequestPaymentDate()%></td>
			<td align="center"><%=paymentRecord.getStatusDescription()%></td>
		</tr>
		<%
		}
		else
		{
		%>
		<tr id="<%=paymentRecord.getRecordId()%>" class="row<%=rowClass%>"  style="display: none; ">
			<td align="center"><%=paymentRecord.getRecordId()%></td>
			<td align="center"><%=paymentRecord.getDba()%></td>
			<td align="center"><%=paymentRecord.getAmount()%></td>
			<td align="center"><%=paymentRecord.getBank()%></td>
			<td align="center"><%=paymentRecord.getMillennium_no()%></td>			
			<td align="center"><%=paymentRecord.getReferenceNumber()%></td>
			<td align="center"><%=paymentRecord.getDocumentId()%></td>
			<td align="center"><%=paymentRecord.getPaymentDate()%></td>
			<td align="center"><%=paymentRecord.getRequestPaymentDate()%></td>
			<td align="center"><%=paymentRecord.getStatusDescription()%></td>			
		</tr>
		<%
		}
		
		if (rowClass==1)
		{
			rowClass=0;
		}
		else
		{
			rowClass=1;
		}
		totalRecord++;
	}
		
	int slots = totalRecord / items;
		
	%>
	</tr>
	
	<% 
	StringBuilder arrayRanges = new StringBuilder();
	int tmpRange=0;
	int j=0;
	for(int i=0;i<=slots;i++)
	{
		if ( tmpRange!=0 )
			tmpRange++;
		
		int bottom=tmpRange;
		
		tmpRange += items;
		
		if ( bottom<=totalRecord )
		{
			if (tmpRange>totalRecord )
			{
				tmpRange=totalRecord;
			}
			j++;
			arrayRanges.append("MyArrayRanges["+i+"] = new objRanges("+bottom+","+tmpRange+");"+'\n');			 
		}
	}
	System.out.println(arrayRanges.toString());
	%>
</table>

<script type="text/javascript">
	MyArrayRanges = null;
	MyArrayRanges = new Array(<%=j%>);
	<%=arrayRanges.toString()%>
	var totalRecords = <%=totalRecord%>;
	<%=changeEndDate%>	
	urlDownload = "<%=urlDownload%>";
	
	var showing = showingPageLabel.replace("{0}", "0");
	showing = showing.replace("{1}", MyArrayRanges.length-1);
	//alert(showing);
	$("#showingPageOf").html(showing);
</script>