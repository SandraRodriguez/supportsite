<%@ page import="com.debisys.languages.Languages,
				 com.debisys.customers.Merchant,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.StringUtil,
				 com.debisys.utils.DateUtil,
				 com.debisys.utils.NumberUtil,
				 java.util.Vector,
				 java.util.Iterator,
				 java.util.Enumeration,
				 java.util.*" contentType="text/html; charset=ISO-8859-1"%>
<%@page import="com.debisys.reports.banks.Banks"%>
<%
	int section = 9;
	int section_page = 21;
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
	
	//<option value="BNM">BNM</option>
	StringBuilder banksOptions = new StringBuilder();
	ArrayList<Banks> banks = Banks.findBanks();
	if ( banks!=null && banks.size()>0)
	{
		for(Banks bank : banks)
		{
			banksOptions.append("<option value=\""+bank.getId()+"\">"+bank.getName()+"</option>");
		}	
		
	}		
	
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript">	
	function selectionStatus()
	{
		var otherStatusSelected=false;
		var showMerchant=false;
		var allSelected=false;
		var countSelectedStatus=0;
		var size = $('#status option').size()-1;
		
		$("#status option:selected").each(
		  function()
		  {
	  		countSelectedStatus++;
			valueSelect = $(this).attr("id");
			if ( valueSelect=="APLY" )
			{					
				showMerchant=true;
			}
			else
			{
				otherStatusSelected=true;
			}
			if (valueSelect=="0")
			{
				allSelected=true;
				showMerchant=true
			}
		  }
		);
		
		if ( size==countSelectedStatus)
		{
			allSelected=true;
			$("#status option[id=0]").attr("selected",true);
			showMerchant=true;
		}	
		
		if (allSelected)
		{
			countSelectedStatus=0;
			$("#status option:selected").each(
			  function()
			  {
					valueSelect = $(this).attr("id");
					//alert("1 "+valueSelect);
					if (valueSelect != "0" )
					{
						$(this).attr("selected",false);
						
					}					
			  }
			);
		}
		
		
		//alert(countSelectedStatus+" "+otherStatusSelected+" "+showMerchant);
		if ( showMerchant )
		{				
			$('#requestNumber').attr('disabled', false);
			$('#siteId').attr('disabled', false);
			$('#merchantIds').attr('disabled', false);				
		}
		else
		{
			if (!allSelected)
			{
			 $('#requestNumber').attr('disabled', 'disabled');
			 $('#siteId').attr('disabled', 'disabled');
			 $('#merchantIds').attr('disabled', 'disabled');
			}
			$('#requestNumber').val('');
			$('#siteId').val('');			
			//unselect the currents selected merchant
			$("#merchantIds option:selected").each(
			  function()
			  {
					$(this).attr("selected",false);					
			  }
			);
		}
	} 
	
		
	function submitReport()
	{
		var pageJSP = "admin/tools/banks/paymentsList.jsp";
		
		$.post(pageJSP, 
				{ 
				   	time:"333"		  	    	   	   
			  	},
				function okListPayments(data)
				{	
					$("#paymentsList").html(data);
					$("#plusIMG").show();
					$("#minusIMG").hide();					 
					$("#paymentsList").fadeIn(900);
				}
		);	
			
		return false;
	}
	
	
	function downloadReport()
	{
		if ( urlDownload.length > 0)
		{
			window.open(urlDownload,"downloadBankPayments");
		}
	}
	
	
	function showFilters()
	{
		$("#filtersTR").show();
			
		$("#plusIMG").hide();
		$("#minusIMG").show();
	}
	function hideFilters()
	{
		$("#filtersTR").hide();
		$("#plusIMG").show();
		$("#minusIMG").hide();	
	}
	
	function resetFieldsWhenApplied(isSite,isMerchant,isIDRequest)
	{
		if (isSite)
		{			
			$("#merchantIds option:selected").each(
			  function()
			  {					
				$(this).attr("selected",false);									
			  }
			);
						
			if ( $('#requestNumber').val().length>0 ) 
		    { 
		   		$('#requestNumber').val(''); 
		    }	
		}
		else if(isMerchant)
		{
		   var someMerchantSelected=false;
		   $("#merchantIds option:selected").each(
			  function()
			  {					
				someMerchantSelected=true;							
			  }
			);
			
		   if (someMerchantSelected)
		   {	
			    if ( $('#requestNumber').val().length>0 ) 
			    { 
			   		$('#requestNumber').val(''); 
			    }
			    if ($('#siteId').val().length>0 )
				{ 
					$('#siteId').val(''); 
				}
		   }	
		}
		else if(isIDRequest)
		{
			$("#merchantIds option:selected").each(
			  function()
			  {
				$(this).attr("selected",false);										
			  }
			);
			if ($('#siteId').val().length>0 )
			{ 
				$('#siteId').val(''); 
			}
		
		}
	
	}
</script>

<%
Vector vResults = new Vector();
%>
<form name="mainform" method="post" action="admin/tools/bankPayments.jsp" >
<table border="0" cellpadding="0" cellspacing="0" width="60%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000" >
			<img id="plusIMG" src="images/plusbox.png" height="15px" onclick="showFilters()"/>
			<img id="minusIMG" src="images/minusbox.png" height="15px" onclick="hideFilters()"/>
			<b><%=Languages.getString("jsp.admin.reports.bankpayments.title",SessionData.getLanguage()).toUpperCase()%></b>
		</td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr id="filtersTR">
		<td colspan=3 align="center" bgcolor="#FFFFFF">
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="left" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="80%" align="center">
							<tr>
								<td>&nbsp;</td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
								<td>&nbsp;</td>
							    <td>
							    	<input type="text" maxlength=10 class="plain" name="startDate" id="txtStartDate" value="" size="12"/>
							    		<a id="linkStartDate" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS>
							    			<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
							    		</a>
							    </td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<input type="text" maxlength=10 class="plain" name="endDate" id="txtEndDate" value="" size="12"/>
							    	<a id="linkEndDate" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS>
							    		<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
							    	</a>
							    </td>
								<td></td>
							</tr>							
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.paymentrequest_search.status",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<select id="status" name="status" size=3 multiple onchange="selectionStatus();" >
							    		<option id="0"><%=Languages.getString("jsp.admin.reports.bankpayments.All",SessionData.getLanguage())%></option>
							    		<option id="APLY" selected="selected"><%=Languages.getString("jsp.admin.reports.bankpayments.Applied",SessionData.getLanguage())%></option>
							    		<option id="NAPL"><%=Languages.getString("jsp.admin.reports.bankpayments.Pend",SessionData.getLanguage())%></option>
							    		<!-- 
							    		<option id="DISC"><%=Languages.getString("jsp.admin.reports.bankpayments.Expired",SessionData.getLanguage())%></option>
							    		 -->						    		
							    	</select>							    	
								</td>
								<td></td>
							</tr>							
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.bankpayments.IdDocumento",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td><input type=text class="plain" id="IdDocumento" maxlength="9" ></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.bankpayments.amount",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td><input type=text class="plain" id="amount" name="amount" maxlength="9" ></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.bankpayments.bankCode",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<select id="bankCodes" id="bankCodes" size=5 multiple >
							    		<%=banksOptions.toString()%>			    		
							    	</select>
							    </td>
								<td></td>
							</tr>							
							<tr>
								<td colspan="5" align=center>
									<input type=submit id="btnSearch" class="plain" 
										value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.search",SessionData.getLanguage())%>"
										onclick="return submitReport()">										
									<%=Languages.getString("jsp.admin.reports.bankpayments.PageItems",SessionData.getLanguage())%>
									<select id="itemsByPage" >
										<option selected="selected">10</option>
										<option>20</option>
										<option>30</option>
										<option>40</option>						
									</select>	
								</td>
								<TD id="waitTD" align="right">
								</TD>								
							</tr>
						</table>
					</td>
					<td id="merchantsTD" valign="top" bgcolor="#FFFFFF" >
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
							<tr>
								<td align="right" class="main" nowrap>
									<%=Languages.getString("jsp.admin.reports.paymentrequest_search.requestnumber",SessionData.getLanguage())%>:
								 </td>
								<td align="left">
									<input type=text class="plain" id="requestNumber" maxlength="9" onblur="resetFieldsWhenApplied(false,false,true);">
								</td>
							</tr>
							<tr>
								<td align="right" valign="top" class="main" nowrap><%=Languages.getString("jsp.admin.reports.paymentrequest_search.selectmerchants",SessionData.getLanguage())%>:</td>
								<td align="left">
							    	<select id="merchantIds" size=10 multiple onblur="resetFieldsWhenApplied(false,true,false);">
										<%										
										    Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
										    Iterator itMerchant = vecMerchantList.iterator();
										    while (itMerchant.hasNext())
										    {
										      Vector vecTemp = null;
										      vecTemp = (Vector) itMerchant.next();
										      out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
										    }
										%>		
									</select>
								</td>								
							</tr>
							<tr>
								<td align="right" class="main" nowrap><%=Languages.getString("jsp.admin.reports.paymentrequest_search.siteid",SessionData.getLanguage())%>:</td>
								<td align="left">
							    	<input type="text" id="siteId" class="plain" maxlength="9"
							    		   onblur="resetFieldsWhenApplied(true,false,false);">
							    </td>											  									 	
							</tr>
						</table>	
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	            </tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td colspan=3>
			<div id="paymentsList">
			
			</div>
		</td>		
	</tr>
</table>
</form>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<script type="text/javascript">
	//$("#merchantsTD").hide();
	$("#plusIMG").hide();
	$("#minusIMG").hide();	
	selectionStatus();
</script>

<%@ include file="/includes/footer.jsp" %>