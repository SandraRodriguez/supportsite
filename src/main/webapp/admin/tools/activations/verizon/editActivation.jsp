<%-- 
    Document   : editActivation
    Created on : Oct 21, 2016, 3:57:04 PM
    Author     : nmartinez
--%>

<%@page import="com.debisys.reports.pojo.BasicPojo"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.tools.VerizonActivations"%>
<%@page import="java.util.ArrayList"%>

<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener,
         com.debisys.properties.RepProperty, java.text.DecimalFormat" %>
<%
    int section = 9;
    int section_page = 34;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="VerizonActivations" class="com.debisys.tools.VerizonActivations" scope="request" />
<jsp:setProperty name="VerizonActivations" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>



<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}

    input[type="text"]{height:20px; vertical-align:top;}
    .field_wrapper div{ margin-bottom:10px;}
    .add_button{ margin-top:10px; margin-left:10px;vertical-align: text-bottom;}
    .remove_button{ margin-top:10px; margin-left:10px;vertical-align: text-bottom;}



    #field_wrapper{
        width: 100%;
    }
    #left{
        float:left;
        width:250px;
        margin: 5px 2px;
        text-align:center;
        color:#fff;
    }
    .clear{
        clear:both;
    }

    #pSimNumber{
        color:green;
        font-size: 20px;
    }

    #divPleaseNote{
        font-size: 15px;
        background-color: yellow;
        size: 200px;
    }
    #legendIndividualRecords{
        font-size: 15px;
    }
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }

    .ventana_flotante {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }


</style>


<%
    
    String trxId = request.getParameter("trxId");
    if (trxId==null){
        trxId = request.getParameter("transactionId");
    }    
    boolean isTrxOfChainBussinnes = VerizonActivations.validatrxIdByIsoId(SessionData.getUser().getIsoId(), trxId);    
    if (!isTrxOfChainBussinnes){        
        %> 
            <table width="100%">                
                <tr class="main">
                    <td noWrap style="padding-left:18px;">
                        <font color=ff0000><%=Languages.getString("jsp.message.msg1",SessionData.getLanguage())%>       
                    </td>
                </tr>                
            </table>
        <%
    } 
    else
    {
            
    String Cons_Request_Date = "REQUEST_DATE";
    String Cons_FirstName = "FIRST_NAME";
    String Cons_LastName = "LAST_NAME";
    String Cons_StreetNumber = "STREET_NUMBER";
    String Cons_StreetName = "STREET_NAME";
    String Cons_City = "CITY";
    String Cons_State = "STATE";
    String Cons_ZipCode = "ZIP";
    String Cons_Language = "LANG";
    String Cons_AreaCode = "AREA";
    String Cons_PhoneNumber = "PHONENUMBER";
    String Cons_SubscriberPIN = "SUBSCR_PIN";
    String Cons_SIM = "SIM";
    String Cons_ESN = "ESNIMEI";
    String Cons_IMEI = "IMEI";
    String Cons_ActivationPhone = "ACTIVATION_PHONE";
    String Cons_LinesRequest = "LINES";
    String Cons_Request_Status = "STATUS";
    String Cons_PLAN_PIN = "PIN";
    
    HashMap<String, String> formValues = new HashMap<String, String>();
    formValues.put(Cons_Request_Date, "");    
    formValues.put(Cons_FirstName, "");
    formValues.put(Cons_LastName, "");
    formValues.put(Cons_StreetNumber, "");
    formValues.put(Cons_StreetName, "");
    formValues.put(Cons_City, "");
    formValues.put(Cons_State, "");
    formValues.put(Cons_ZipCode, "");
    formValues.put(Cons_Language, "");
    formValues.put(Cons_AreaCode, "");
    formValues.put(Cons_PhoneNumber, "");
    formValues.put(Cons_SubscriberPIN, "");
    formValues.put(Cons_SIM, "");
    formValues.put(Cons_ESN, "");
    formValues.put(Cons_IMEI, "");
    formValues.put(Cons_ActivationPhone, "");
    
    String sessionLanguage = SessionData.getUser().getLanguageCode();
    String[] languagesControl=null;
    
    HashMap<String, PropertiesByFeature> propertiesVerizonAct = PropertiesByFeature.findPropertiesByFeatureHash(PropertiesByFeature.FEATURE_VERIZONE_ACTIVATIONS);
   
    SimControl linesAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_LINES_REQUESTED, sessionLanguage);
    SimControl firstAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_FIRST_NAME, sessionLanguage);
    SimControl lastAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_LAST_NAME, sessionLanguage);
    SimControl phoneConAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_PHONE_NUMBER, sessionLanguage);
    SimControl streetNuAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_STREET_NUMBER, sessionLanguage);
    SimControl streetNAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_STREET_NAME, sessionLanguage);
    SimControl cityAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_CITY, sessionLanguage);
    SimControl stateAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_STATE, sessionLanguage);
    SimControl zipCodeAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_ZIP_CODE, sessionLanguage);
    SimControl areaCodeAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_AREA_CODE, sessionLanguage);
    SimControl subsPinAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_ACCOUNT_PIN_NUMBER, sessionLanguage);
    SimControl simAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_SIM, sessionLanguage);
    SimControl esnImeiAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_ESN_IMEI, sessionLanguage);
    //SimControl imeiAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_IMEI, sessionLanguage);
    SimControl actPhoneAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_ACTIVATION_PHONE, sessionLanguage);
    SimControl languagesAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_PRE_LANGUAGE, sessionLanguage);
    if (languagesAct!=null){        
        languagesControl = languagesAct.getValue().split(",");        
    }
    
    SimControl reqStatusAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_REQUEST_STATUS, sessionLanguage);
    SimControl warnn1 = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_WARNNING_1, sessionLanguage);  
    SimControl actDateAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_ACTIVATION_DATE, sessionLanguage);
    SimControl planPinAct = SimControl.setUpControlbyTokenHash(propertiesVerizonAct, com.debisys.tools.VerizonActivations.TOKEN_PLAN, sessionLanguage);
    
    String styleErrorField = "";
    if (propertiesVerizonAct.get("STYLE_ERROR")!=null){
        styleErrorField = propertiesVerizonAct.get("STYLE_ERROR").getValue();
    }
    
    String styleErrorMessage="";
    if (propertiesVerizonAct.get("REQUIERED_MESSAGE_LABEL_"+sessionLanguage)!=null){
        styleErrorMessage = propertiesVerizonAct.get("REQUIERED_MESSAGE_LABEL_"+sessionLanguage).getValue();
    }
    
    String message = "";
    if ( "POST".equalsIgnoreCase(request.getMethod()) && request.getParameter("submitBtn")!=null ) {        
        message = VerizonActivations.validateScreenControls(linesAct, firstAct, lastAct, streetNuAct, streetNAct, 
                zipCodeAct, cityAct, areaCodeAct, phoneConAct, simAct, esnImeiAct, subsPinAct, actPhoneAct, styleErrorField);
        if (message==null){
            message="";
            String instance = DebisysConfigListener.getInstance(application);
            VerizonActivations.changeActivatiobByStatusCode("support-site",SessionData.getUser().getUsername(), instance);        
        } else{
            message = styleErrorMessage + "<br/>"+ message;
        }
    }
    
    if (trxId!=null ){        
        VerizonActivations activation = VerizonActivations.getActivationRequestByRequestId(trxId);
        if (activation!=null){               
            formValues.put(Cons_PLAN_PIN, String.valueOf(activation.getPin()));
            formValues.put(Cons_LinesRequest, String.valueOf(activation.getLinesRequested()));    
            formValues.put(Cons_Request_Date, activation.getRequestDateStr());    
            formValues.put(Cons_FirstName, activation.getSubscriberFirstName());
            formValues.put(Cons_LastName, activation.getSubscriberLastName());
            formValues.put(Cons_StreetNumber, activation.getStreetNumber());
            formValues.put(Cons_StreetName, activation.getStreetName());
            if ( activation.getCity() != null)
                formValues.put(Cons_City, activation.getCity());
            formValues.put(Cons_State, activation.getStateIdNew());
            formValues.put(Cons_ZipCode, activation.getZipCode());
            formValues.put(Cons_Language, activation.getPreferredLanguage());
            formValues.put(Cons_AreaCode, activation.getDesiredAreaCode());
            formValues.put(Cons_PhoneNumber, activation.getContactPhone());
            formValues.put(Cons_SubscriberPIN, String.valueOf(activation.getAccountPinNumber()));
            formValues.put(Cons_SIM, activation.getSim());
            if ( activation.getEsnImei() !=null)
                formValues.put(Cons_ESN, activation.getEsnImei());
            //formValues.put(Cons_IMEI, activation.getImei());
            formValues.put(Cons_ActivationPhone, activation.getPhoneNumber());
            formValues.put(Cons_Request_Status, activation.getRequestStatus());
        }
    } else {
        response.sendRedirect("/support/admin/tools/tools.jsp");
        return;
    }
    
%>


<script type="text/javascript">
    $(document).ready(function () {

    });

    function validateForm() {
    }

    function validateKeypress(validChars)
    {
        var keyChar = String.fromCharCode(event.which || event.keyCode);        
        var pattern = new RegExp(validChars, "i");
        return pattern.test(keyChar) ? keyChar : false;
    }

</script>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="ventana_flotante">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>

<table border="0" cellpadding="0" cellspacing="0" width="1350">
    <tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;
        <%=Languages.getString("jsp.admin.tools.verizonActivations.ByTransactionId", SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
</tr>
<tr>
<td colspan="3" class="formArea2">

    <form id="formActivations" name="formActivations" class="reportForm" method="post" 
          action="admin/tools/activations/verizon/editActivation.jsp" onsubmit="return validateForm();">

        <fieldset>
            <legend id="legendIndividualRecords"><%=actDateAct.getLabel()%>: <%=formValues.get(Cons_Request_Date)%></legend>
            <table align="center" cellspacing="15">
                <tr align="center">
                    <td align="left" style="<%=linesAct.getStyleError()%>"><%=linesAct.getLabel()%></td>
                    <td><input id="linesRequested" name="linesRequested" type="text" 
                               onkeypress="return validateKeypress('<%=linesAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_LinesRequest)%>"/></td>
                    
                    <td align="left" style="<%=firstAct.getStyleError()%>"><%=firstAct.getLabel()%></td>
                    <td><input id="subscriberFirstName" name="subscriberFirstName" type="text" 
                               onkeypress="return validateKeypress('<%=firstAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_FirstName)%>" /></td>
                    
                    <td align="left" style="<%=lastAct.getStyleError()%>"><%=lastAct.getLabel()%></td>
                    <td><input id="subscriberLastName" name="subscriberLastName" type="text" 
                               onkeypress="return validateKeypress('<%=lastAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_LastName)%>"/></td>
                    
                    <td align="left" style="<%=phoneConAct.getStyleError()%>"><%=phoneConAct.getLabel()%></td>
                    <td><input id="contactPhone" name="contactPhone" type="text" 
                               onkeypress="return validateKeypress('<%=phoneConAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_PhoneNumber)%>"/></td>
                    
                </tr>
                <tr align="center">
                    <td align="left" style="<%=streetNuAct.getStyleError()%>"><%=streetNuAct.getLabel()%></td>
                    <td><input id="streetNumber" name="streetNumber" type="text" 
                               onkeypress="return validateKeypress('<%=streetNuAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_StreetNumber)%>"/></td>
                    
                    <td align="left" style="<%=streetNAct.getStyleError()%>"><%=streetNAct.getLabel()%></td>
                    <td><input id="streetName" name="streetName"  type="text" 
                               onkeypress="return validateKeypress('<%=streetNAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_StreetName)%>" /></td>
                    
                    <td align="left" style="<%=cityAct.getStyleError()%>"><%=cityAct.getLabel()%></td>
                    <td><input id="city" name="city" type="text" 
                               onkeypress="return validateKeypress('<%=cityAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_City)%>"/></td>
                    
                    <td align="left" style="<%=stateAct.getStyleError()%>"><%=stateAct.getLabel()%></td>
                    <td>
                        <select id="stateIdNew" name="stateIdNew" style="width: 160px;">
                        <%
                        
                            String selectedState = formValues.get(Cons_State);
                            ArrayList<BasicPojo> countries = VerizonActivations.findStates("2");
                            for(BasicPojo country : countries){                              
                                String selected="";
                                if (selectedState.equals(country.getId())){
                                    selected="selected";
                                }
                                out.println("<option "+selected+" value='"+country.getId()+"'>"+country.getDescripton()+"</option>");                              
                            }                                                
                        %>
                        </select>
                    </td>
                </tr>
                <tr align="center">
                    <td align="left" style="<%=zipCodeAct.getStyleError()%>"><%=zipCodeAct.getLabel()%></td>
                    <td><input id="zipCode" name="zipCode" type="text" 
                               onkeypress="return validateKeypress('<%=zipCodeAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_ZipCode)%>"/></td>
                    
                    <td align="left" style="<%=languagesAct.getStyleError()%>"><%=languagesAct.getLabel()%></td>
                    <td>
                        <select id="preferredLanguage" name="preferredLanguage" style="width: 160px;">
                        <%
                        if ( languagesControl !=null){
                            String selectedLanguage = formValues.get(Cons_Language);
                            for(String languageToControl : languagesControl){
                                String[] lng = languageToControl.split("=");
                                String selected="";
                                if (selectedLanguage.equals(lng[0])){
                                    selected="selected";
                                }
                                out.println("<option "+selected+" value='"+lng[0]+"'>"+lng[1]+"</option>");                              
                            }
                        }                        
                        %>
                        </select>
                    </td>
                    
                    <td align="left" style="<%=areaCodeAct.getStyleError()%>"><%=areaCodeAct.getLabel()%></td>
                    <td><input id="desiredAreaCode" name="desiredAreaCode" type="text" 
                               onkeypress="return validateKeypress('<%=areaCodeAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_AreaCode)%>"/></td>
                    
                    <td align="left" style="<%=subsPinAct.getStyleError()%>"><%=subsPinAct.getLabel()%></td>
                    <td><input id="accountPinNumber" name="accountPinNumber" type="text" 
                               onkeypress="return validateKeypress('<%=subsPinAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_SubscriberPIN)%>"/></td> 
                    
                </tr>    
                <tr align="center">                           
                    <td align="left" style="<%=simAct.getStyleError()%>"><%=simAct.getLabel()%></td>
                    <td><input id="sim" name="sim" type="text" 
                               onkeypress="return validateKeypress('<%=simAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_SIM)%>"/></td>
                    
                    <td align="left" style="<%=esnImeiAct.getStyleError()%>"><%=esnImeiAct.getLabel()%></td>
                    <td><input id="esn" name="esn" type="text" 
                               onkeypress="return validateKeypress('<%=esnImeiAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_ESN)%>"/></td>
                    
                    <td align="left" style="<%=actPhoneAct.getStyleError()%>"><%=actPhoneAct.getLabel()%></td>
                    <td><input id="phoneNumber" name="phoneNumber" type="text" 
                               onkeypress="return validateKeypress('<%=actPhoneAct.getPattern()%>');"
                               value="<%=formValues.get(Cons_ActivationPhone)%>"/></td>
                    
                    
                    <td align="center" colspan="2">
                        <fieldset>
                            <legend id="legendPLAN_PIN"><%=planPinAct.getLabel()%></legend>                                                            
                            <%=formValues.get(Cons_PLAN_PIN)%>
                        </fieldset>    
                    </td>
                    
                </tr>    
                <tr align="center" >
                    <td colspan="4" style="height: 110px;vertical-align: middle;">
                        <%      
                            HashMap<String,String> reqStatusValues = new HashMap<String, String>();
                            if (reqStatusAct!=null){

                                String[] reqValues = reqStatusAct.getValue().split(","); 
                                for(String reqInfo : reqValues){
                                    String[] reqData = reqInfo.split("=");
                                    reqStatusValues.put(reqData[0], reqData[1]);
                                }
                            }
                            String selectedStatus = formValues.get(Cons_Request_Status);
                            ArrayList<BasicPojo> reqStatus = VerizonActivations.findReqStatus();
                            StringBuilder camboStatus = new StringBuilder();
                            String selectedStatusCode = "";
                            camboStatus.append(String.format(reqStatusAct.getLabel(),trxId));
                            camboStatus.append("<select id='requestStatus' name='requestStatus'>");
                            for(BasicPojo req : reqStatus){
                                String selected="";
                                String statusIdNew = req.getId();
                                String statusCode = req.getDescripton();                                    
                                if(selectedStatus.equals(statusIdNew)){
                                    selected = "selected";
                                    selectedStatusCode = statusCode;
                                }
                                statusCode = reqStatusValues.get(statusCode);
                                camboStatus.append("<option "+selected+" value='"+statusIdNew+"'>"+statusCode+"</option>");
                            }
                            camboStatus.append("</select>");
                            boolean stopCycleActivation = selectedStatusCode.equals(com.debisys.tools.VerizonActivations.STATUS_COMPLETED) ||
                                    selectedStatusCode.equals(com.debisys.tools.VerizonActivations.STATUS_CANCELLED);
                            
                            if (stopCycleActivation){
                                if ( warnn1 != null)
                                    out.println(String.format(warnn1.getLabel(),trxId));
                                else
                                    out.println("Already processed!!!");
                            } else{
                                out.println(camboStatus.toString());
                                %>
                                    <input type="submit" id="submitBtn" name="submitBtn" value="Change status">
                                    <input type="hidden" id="transactionId" name="transactionId" value="<%=trxId%>">                        
                                <%
                            }
                        %>                        
                        
                    </td>                    
                </tr>                               

            </table>
            <span id="spanWarningMessages"><%=message%></span>

        </fieldset>

    </form>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>
<%}%>