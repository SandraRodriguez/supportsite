<%
int section=11;
int section_page=3;
int promotionstatusvalue = 0;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="PhoneRewards" class="com.debisys.tools.PhoneRewards" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
		
	if(request.getParameter("type") != null)
	{
		if (request.getParameter("type").equals("UPDATETOPROMO"))
		{
			PhoneRewards.updatePromotionStatus(request,SessionData,1);
		}
		else if (request.getParameter("type").equals("UPDATETOTHRESHOLD"))
		{
			PhoneRewards.updatePromotionStatus(request,SessionData,2);
		}
		else if (request.getParameter("type").equals("UPDATETODISBALE"))
		{
			PhoneRewards.updatePromotionStatus(request,SessionData,3);
		}
	}
%>
<SCRIPT LANGUAGE="JavaScript">

            function updateStatus()
            {
            var message="<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsUpdateWarning",SessionData.getLanguage())%>";
			if(confirm(message))
			{
				if(document.PhoneRewardsStatusForm.phonerewardscheck.checked){
					var textBox = document.getElementById('type');
	 						textBox.value = "UPDATETOPROMO";
	 				}
	 		 	else if(document.PhoneRewardsStatusForm.bonusthresholdcheck.checked){
					var textBox = document.getElementById('type');
	 						textBox.value = "UPDATETOTHRESHOLD";
	 				}
	 		 	else{
					var textBox = document.getElementById('type');
	 						textBox.value = "UPDATETODISBALE";
	 				}
					document.PhoneRewardsStatusForm.submit();
			}	
            }  
        </SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="800" background="images/top_blue.gif">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    	<td class="formAreaTitle" align="left" width="3000"><b><%=Languages.getString("jsp.includes.menu.tools",SessionData.getLanguage()).toUpperCase()%></b></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
 	</tr>
 	<tr>
 		<td colspan="3">
   			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
 	    		<tr>
 		      		<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
  		    		<td align="left" valign="top" bgcolor="#FFFFFF">
   			    		<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    		<tr>
    				    		<td width="18">&nbsp;</td>
     				    		<td class="main">
									<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
          								
          								<tr><td VALIGN="left" WIDTH="376" height="28">
          								<img border="0" src="images/analysis_cube.png" width="22" height="22">
          								<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973">
             									<%=Languages.getString("jsp.admin.tools.bonusphonerewards.status",SessionData.getLanguage())%>
             									</font>
          								</td>
          								</tr>
          								<tr>
          								<td VALIGN="middle" WIDTH="500" height="28">
          								<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973">
											<%=PhoneRewards.getPromotionStatus(SessionData)%>
             							</font>
          								</td>
          								</tr>
          								
          								<tr><td VALIGN="middle" WIDTH="500" height="28">
          								<img border="0" src="images/analysis_cube.gif" width="22" height="22">
          								<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973">
             									<%=Languages.getString("jsp.admin.tools.bonusphonerewards.control",SessionData.getLanguage())%>
             									</font>
          								</td>
          								</tr>
          								<tr>
          								<td VALIGN="middle" WIDTH="500" height="28">
										<form  name="PhoneRewardsStatusForm" action="admin/tools/index.jsp">
										<input type="hidden" name="type" value="UPDATE" />
						                
						                 <table class="main" >
						                 <tr>
						                  <td><input name="promotionstatus" id="phonerewardscheck" type="radio"  value ="<%=Languages.getString("jsp.admin.tools.bonusphonerewards.status.enable1",SessionData.getLanguage())%>      "    /><%=Languages.getString("jsp.admin.tools.bonusphonerewards.status.enable1",SessionData.getLanguage())%> </td>
						                  <td><input name="promotionstatus" id="bonusthresholdcheck" type="radio"  value ="<%=Languages.getString("jsp.admin.tools.bonusphonerewards.status.enable2",SessionData.getLanguage())%>    "  /><%=Languages.getString("jsp.admin.tools.bonusphonerewards.status.enable2",SessionData.getLanguage())%></td>
						                  <td><input name="promotionstatus" id="disbalebothcheck" type="radio"  value ="<%=Languages.getString("jsp.admin.tools.bonusphonerewards.status.disableboth",SessionData.getLanguage())%>   " /><%=Languages.getString("jsp.admin.tools.bonusphonerewards.status.disableboth",SessionData.getLanguage())%></td>
						                 </tr>
						                 <tr>
						                 <td><INPUT TYPE="button" id="percentage" VALUE="<%=Languages.getString("jsp.admin.tools.bonusphonerewards.applybutton",SessionData.getLanguage())%>" ONCLICK="updateStatus()">
						                 </td>
						                 </tr>
										 
						                 </table>
						                 </form>
          								</td>
          								</tr>
          								
          								
          								<tr>
           									<td VALIGN="middle" WIDTH="500" height="28">
             									<img border="0" src="images/analysis_cube.gif" width="22" height="22">
             									<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973">
             									<%=Languages.getString("jsp.includes.menu.bonusPromoAdministration.title",SessionData.getLanguage())%>
             									</font>
          									</td>
      									</tr>
<%
	if(SessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLDS))
	{
%>      									
       									<tr>
          									<td VALIGN="top" WIDTH="500" height="28">
            									<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973"><a href="admin/tools/bonusPromoTools.jsp" style="text-decoration: none">
             									- <%=Languages.getString("jsp.includes.menu.bonusPromoTools.title",SessionData.getLanguage())%>
             									</font>
           									</td>
        								</tr>
<%
	}
	if(SessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_BONUS_THRESHOLD_SMS))
	{
%>        								
        								<tr>
           									<td VALIGN="top" WIDTH="500" height="28">
            		 							<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973"><a href="admin/tools/bonusPromoToolsSMS.jsp" style="text-decoration: none">
              									- <%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title",SessionData.getLanguage())%>
              									</font>
           									</td>
										</tr>	
										
										
<%
	}
%>     
<%
	if(SessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_PHONE_REWARDS))
	{
%>        								
        								<tr>
           									<td VALIGN="top" WIDTH="500" height="28">
            		 							<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973"><a href="admin/tools/bonusPhoneRewardsPromo.jsp" style="text-decoration: none">
              									- <%=Languages.getString("jsp.includes.menu.bonusPhoneRewardsPromo.title",SessionData.getLanguage())%>
              									</font>
           									</td>
										</tr>	
<%
	}
	if(SessionData.checkPermission(DebisysConstants.PERM_CONFIGURE_PHONE_REWARDS_SMS))
	{
%>      
  					
  										<tr>
           									<td VALIGN="top" WIDTH="500" height="28">
            		 							<font face="Arial, Helvetica, sans-serif" style="font-size:13pxpt;font-weight:700" color="#2C0973"><a href="admin/tools/bonusPhoneRewardsPromoSMS.jsp" style="text-decoration: none">
              									- <%=Languages.getString("jsp.includes.menu.bonusPhoneRewardsPromoSms.title",SessionData.getLanguage())%>
              									</font>
           									</td>
										</tr>	
<%
	}
%>								</table>
     				    		</td>
     		        			<td width="18">&nbsp;</td>
        					</tr>
       					</table>
    				</td>
    				<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	  			</tr>
	  			<tr>
		  			<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	  			</tr>
			</table>
 		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>