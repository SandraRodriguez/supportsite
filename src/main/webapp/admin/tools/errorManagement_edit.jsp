<%@ page import="com.debisys.tools.ErrorManagement,java.util.HashMap,java.util.Vector,java.util.Iterator,java.util.Hashtable,java.net.URLEncoder,com.debisys.utils.HTMLEncoder,com.debisys.utils.StringUtil" %>
<%
	int section=9;
	int section_page=22;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ErrorManagement" class="com.debisys.tools.ErrorManagement" scope="request"/>
<jsp:setProperty name="ErrorManagement" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	String providerId = "";
	String providerName = "";
	String errorCode = "";
	String message = "";
	Hashtable<String, String>  editErrors = null;
	if (request.getParameter("submitted") != null)
	{
		try
		{
			if (request.getParameter("submitted").equals("y"))
			{
				String msg = request.getParameter("errorMessage");
				providerId = request.getParameter("providerId");
				errorCode = request.getParameter("errorCode");
				ErrorManagement.updateErrorMessage(providerId, errorCode, msg, SessionData);
				if (ErrorManagement.isError())
				{
					editErrors = ErrorManagement.getErrors();
					message = ErrorManagement.getErrorMessage();
				}
				else
				{
					response.sendRedirect("/support/admin/tools/errorManagement.jsp");
					return;
				}
			}
		}catch (Exception e)
		{
			System.out.println("ERROR "+e);
		}
	}
	else
	{
		providerId = request.getParameter("providerId");
		providerName = request.getParameter("providerName");
		errorCode = request.getParameter("errorCode");
		message = ErrorManagement.getErrorMessage(providerId, errorCode, SessionData);
	}
%>
<%@ include file="/includes/header.jsp" %>
<script  type="text/javascript">

	function insertAtCaret(areaId)
	{
		var txtarea = document.getElementById(areaId);
		var wList = document.getElementById("wildcards");
		var text = wList.options[wList.selectedIndex].value;
		var scrollPos = txtarea.scrollTop;
		var strPos = 0;
		var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? "ff" : (document.selection ? "ie" : false ) );
		if (br == "ie")
		{
			txtarea.focus();
			var range = document.selection.createRange();
			range.moveStart ('character', -txtarea.value.length);
			strPos = range.text.length;
		}
		else if (br == "ff")
		{
			strPos = txtarea.selectionStart;
		}
		var front = (txtarea.value).substring(0,strPos);
		var back = (txtarea.value).substring(strPos,txtarea.value.length);
		txtarea.value=front+text+back;
		strPos = strPos + text.length;
		if (br == "ie")
		{
			txtarea.focus();
			var range = document.selection.createRange();
			range.moveStart ('character', -txtarea.value.length);
			range.moveStart ('character', strPos);
			range.moveEnd ('character', 0);
			range.select();
		}
		else if (br == "ff")
		{
			txtarea.selectionStart = strPos;
			txtarea.selectionEnd = strPos;
			txtarea.focus();
		}
		txtarea.scrollTop = scrollPos;
	}

	$(function ()
	{
		$('#Buttonsubmit').click(function()
		{
			$('#formErrorCode').submit();
		});
	});
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    	<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.errors.edit.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
  			<form id="formErrorCode" name="formErrorCode" method="post" action="admin/tools/errorManagement_edit.jsp">
				<%
					if (editErrors != null)
					{
						out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
						Enumeration enum1=editErrors.keys();
						while(enum1.hasMoreElements())
						{
							String strKey = enum1.nextElement().toString();
							String strError = (String) editErrors.get(strKey);
							out.println("<li>" + strError + "</li>");
						}
						out.println("</font></td></tr>");
					}
				%>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
	  				<tr>
	    				<td>
	   						<table border="0" width="100%" cellpadding="0" cellspacing="0">
	   							<tr>
	       							<td class="formArea2">
	        							<br>
	         							<table border=0>
	            							<tr class="main">
	             								<td nowrap valign="top" width="100"><%=Languages.getString("jsp.admin.tools.provider_id",SessionData.getLanguage()) + ": " + providerId%></td>
	             							</tr>
	            							<tr class="main">
	             								<td nowrap valign="top" width="100"><%=Languages.getString("jsp.admin.tools.provider_name",SessionData.getLanguage()) + ": " + providerName%></td>
	             							</tr>
	            							<tr class="main">
	             								<td nowrap valign="top" width="100"><%=Languages.getString("jsp.admin.tools.provider_error_code",SessionData.getLanguage()) + ": " + errorCode%></td>
	             							</tr>
	            						</table>
	            						<br>
	            					</td>
	            				</tr>
	         				</table>
	            		</td>
	            	</tr>
	            </table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
	  				<tr>
	    				<td>
	   						<table border="0" width="100%" cellpadding="0" cellspacing="0">
	   							<tr>
	       							<td class="formArea2">
	        							<br>
	         							<table border=0>
						            		<tr class="main">
							            		<td valign=middle nowrap width="30%"><%=Languages.getString("jsp.admin.tools.wildcards",SessionData.getLanguage())%></td>
							            		<td valign=middle nowrap width="3%"></td>
							            		<td valign=middle nowrap width="67%"><%=Languages.getString("jsp.admin.tools.provider_error_message",SessionData.getLanguage())%></td>
											</tr>
											<tr class="main">
												<td valign=top nowrap width="30%">
													<%
														Vector vecWildCards = ErrorManagement.getWildcards();
														if (vecWildCards.isEmpty())
														{
													%>
															<br>
															<font color=ff0000><b><%=Languages.getString("jsp.admin.tools.errors.errors.wildcard_error",SessionData.getLanguage())%></b></font>
															<br><br>
													<%
														}
														else
														{
													%>
															<select name="wildcards" id="wildcards">
													<%
															Iterator itWc = vecWildCards.iterator();
														  	while (itWc.hasNext())
														  	{
																Vector vecTempWc = null;
																vecTempWc = (Vector) itWc.next();
																out.println("<option value=" + vecTempWc.get(0) +">" + vecTempWc.get(0) + " - (" + vecTempWc.get(1) + ")" + "</option>");
														  	}
													%>
												    		</select>
													<%
														}
													%>
													<br>
													<br>
													<input type="button" id="btnInsert" name="btnInsert" onclick="insertAtCaret('errorMessage');" value="<%=Languages.getString("jsp.admin.tools.add_wildcards",SessionData.getLanguage())%>">
												</td>
												<td valign=middle nowrap width="3%"></td>
												<td valign=middle nowrap width="67%">
													<textarea name="errorMessage" id="errorMessage" rows="10" cols="50" maxlength="140"><%=HTMLEncoder.encode(StringUtil.toString(message))%></textarea>
													<%if (editErrors != null && editErrors.containsKey("errorMessage")) out.print("<font color=ff0000>*</font>");%>
												</td>
						            		</tr>
						            		<tr class="main">
							            		<td valign=middle nowrap width="30%"></td>
							            		<td valign=middle nowrap width="3%"></td>
							            		<td valign=middle nowrap width="67%"><br>*<%=Languages.getString("jsp.admin.tools.edit.update_error.error_message_instructions",SessionData.getLanguage())%></td>
											</tr>
	            						</table>
	            						<br>
	            					</td>
	            				</tr>
	         				</table>
	            		</td>
	            	</tr>
	            </table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
	  				<tr>
	    				<td>
	   						<table border="0" width="100%" cellpadding="0" cellspacing="0" align=center >
	   							<tr>
	       							<td class="formArea2">
	        							<br>
	         							<table border=0 align=center>
	            							<tr class="formAreaTitle2" align=center>
	            								<td>
	            									<input type="hidden" name="submitted" value="y">
	            									<input type="hidden" name="providerId" id="providerId" value="<%=providerId%>">
	            									<input type="hidden" name="errorCode" id="errorCode" value="<%=errorCode%>">
	            									<input type="button" name="Buttonsubmit" id="Buttonsubmit" value="<%=Languages.getString("jsp.admin.tools.edit.update_error",SessionData.getLanguage())%>">
	            								</td>
	             								<td nowrap valign="top" width="100">
													<form target="blank" method="get" action="admin/tools/errorManagement.jsp">
														<input type=submit value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>">
													</form>
	             								</td>
	             							</tr>
	            						</table>
	            						<br>
	            					</td>
	            				</tr>
	         				</table>
	            		</td>
	            	</tr>
	            </table>
            </form>
    	</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>