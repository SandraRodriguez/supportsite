<%-- 
    Document   : stockLevelsThreshold
    Created on : Oct 6, 2015, 11:30:23 AM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.stockLevels.MerchantsStockLevelsPojo"%>
<%@page import="com.debisys.tools.stockLevels.MerchantStockLevels"%>
<%@page import="com.debisys.utils.DbUtil"%>
<%@page import="com.debisys.reports.PropertiesByFeature"%>
<%@page import="com.debisys.reports.ResourceReport"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<style type="text/css">
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }
    #spanInfo{
        font-size: 20px;
        color: #088A08;
    }
    
</style>

<script type="text/javascript" src="/support/includes/jquery.js"></script>

<%    
    String strRefId = SessionData.getProperty("ref_id");
    String iso_id = SessionData.getProperty("iso_id");
    String textWarning = "";
    String idStockLevel = "";
    String actionValue = "insertMerchantsStockLevels";
   
    
    MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
    List<MerchantsStockLevelsPojo> stockLevelsList = merchantStockLevels.getMerchantStockLevelsByIso(null,iso_id);
    
        

%>

<script LANGUAGE="JavaScript">

    function addToThresholdList() {
        
        var typeEntity = $("#currentEntityType").val();

        if (typeEntity != 5) {
            return;
        }
        
        var currentSeletedValues = $.map($('#merchantList option'), function (e) {return e.value;}); 
        var arrayMerchants = String($.trim($("#currentSelectedIds").val())).split(","); // principal list
                
        for(var i =0; i < arrayMerchants.length; i++ ){
            for(var j =0; j < currentSeletedValues.length; j++ ){
                if(arrayMerchants[i] === currentSeletedValues[j]){
                    arrayMerchants.splice(i, 1); // remove element
                    i--;
                }
            }
        }

        var arrayMerchantsOk = [];
        $.ajax({
                type: "POST",
                async: false,
                data: {arrMerchants: arrayMerchants, action: 'verifyMerchantList'},
                url: "admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp",
                success: function (data) {
                    var array_data = String($.trim(data)).split("\n");
                    var merchantsDescription = "";
                    var it = 0;
                    for (var i = 0; i < arrayMerchants.length; i++) {
                        var exist = false;
                        for (var j = 0; j < array_data.length; j++) {
                            var merchantData = String($.trim(array_data[j])).split(',');
                            //alert('['+merchantData+']');
                            if(array_data[0].indexOf('NOTHING') !== 0 ){
                                if(merchantsDescription.indexOf(merchantData[1]) === -1){
                                    merchantsDescription = merchantsDescription +" ["+ merchantData[1]+"] ";
                                    exist = true;
                                }
                            }
                            if(arrayMerchants[i] === merchantData[0]){
                                exist = true;
                            }
                        }
                        if(!exist){
                            arrayMerchantsOk[it] = arrayMerchants[i];
                            it++;
                        }
                        exist = false;
                    }
                
                    if(merchantsDescription !== ''){
                        $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.stockLevels.warnNomerchantsWithStockLevels", SessionData.getLanguage())%> : "+merchantsDescription).show().fadeOut(15000);
                    }
                }
            });
        
        if(arrayMerchantsOk.length > 0){
            $.ajax({
                type: "POST",
                async: false,
                data: {arrMerchants: arrayMerchantsOk, action: 'merchantList'},
                url: "admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp",
                success: function (data) {
                    var array_data = String($.trim(data)).split("\n");
                    for (var i = 0; i < array_data.length; i++) {
                        var merchantData = String($.trim(array_data[i])).split(",");
                        $('#merchantList').append('<option value="' + merchantData[0] + '">'+merchantData[1]+'</option>');//New input field html 
                    }
                }
            });
        }
    }
    
    function validateForm() {
        removeRedBorder();
        var lowMinValue = $.isNumeric($("#lowMinValue").val());
        var lowMaxValue = $.isNumeric($("#lowMaxValue").val());
        var mediumMinValue = $.isNumeric($("#mediumMinValue").val());
        var mediumMaxValue = $.isNumeric($("#mediumMaxValue").val());
        var highMinValue = $.isNumeric($("#highMinValue").val());
        var highMaxValue = $.isNumeric($("#highMaxValue").val());
        
        if(!lowMinValue || !lowMaxValue || !mediumMinValue || !mediumMaxValue || !highMinValue || !highMaxValue){
             $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.stockLevels.warnInvalidValues", SessionData.getLanguage())%> ").show().fadeOut(15000);
             markErrorFields();
             return false;
        }
        
        var description = $("#description").val();
        if(description == ''){
            $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.stockLevels.warnInvalidValues", SessionData.getLanguage())%> ").show().fadeOut(15000);
            $("#description").css("border", "1px solid red");
            return false;
        }
        
        var hasErrors = valiteRanges();
        if(hasErrors){
            return false;
        }
        
        var currentSeletedValues = $.map($('#merchantList option'), function (e) {return e.value;});
        if(currentSeletedValues.length == 0){
            $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.stockLevels.warnInsertMerchants", SessionData.getLanguage())%> ").show().fadeOut(15000);
            $("#merchantList").css("border", "1px solid red");
            return false;
        }
        $('#merchantList option').prop('selected', true); // Select All elements to send
        return true;
    }
    
    function removeRedBorder(){
        $("#description").css('border', '');
        $("#lowMinValue").css('border', '');
        $("#lowMaxValue").css('border', '');
        $("#mediumMinValue").css('border', '');
        $("#mediumMaxValue").css('border', '');
        $("#highMinValue").css('border', '');
        $("#highMaxValue").css('border', '');
        $("#merchantList").css('border', '');
    }
    
    function markErrorFields(){
    
        var description = $("#description").val();
        var lowMinValue = $.isNumeric($("#lowMinValue").val());
        var lowMaxValue = $.isNumeric($("#lowMaxValue").val());
        var mediumMinValue = $.isNumeric($("#mediumMinValue").val());
        var mediumMaxValue = $.isNumeric($("#mediumMaxValue").val());
        var highMinValue = $.isNumeric($("#highMinValue").val());
        var highMaxValue = $.isNumeric($("#highMaxValue").val());
        
        if(description == ''){$("#description").css("border", "1px solid red");}
        if(!lowMinValue){ $("#lowMinValue").css("border", "1px solid red"); }
        if(!lowMaxValue){ $("#lowMaxValue").css("border", "1px solid red"); }
        if(!mediumMinValue){ $("#mediumMinValue").css("border", "1px solid red"); }
        if(!mediumMaxValue){ $("#mediumMaxValue").css("border", "1px solid red"); }
        if(!highMinValue){ $("#highMinValue").css("border", "1px solid red"); }
        if(!highMaxValue){ $("#highMaxValue").css("border", "1px solid red"); }
    }
    
    function valiteRanges(){
        var lowMinValue = parseFloat($("#lowMinValue").val());
        var lowMaxValue = parseFloat($("#lowMaxValue").val());
        var mediumMinValue = parseFloat($("#mediumMinValue").val());
        var mediumMaxValue = parseFloat($("#mediumMaxValue").val());
        var highMinValue = parseFloat($("#highMinValue").val());
        var highMaxValue = parseFloat($("#highMaxValue").val());
        
        var hasErrors = false;
        
        if((lowMinValue > lowMaxValue) || (lowMaxValue >= mediumMinValue) || (mediumMinValue > mediumMaxValue) || (mediumMaxValue >= highMinValue)
                || (highMinValue > highMaxValue)){
            $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.stockLevels.errorInvalidRanges", SessionData.getLanguage())%> ").show().fadeOut(15000);
            hasErrors = true;
        }
        if(lowMinValue > lowMaxValue){
            $("#lowMinValue").css("border", "1px solid red");
            $("#lowMaxValue").css("border", "1px solid red");
        }
        if(lowMaxValue >= mediumMinValue){
            $("#lowMaxValue").css("border", "1px solid red");
            $("#mediumMinValue").css("border", "1px solid red");
        }
        if(mediumMinValue > mediumMaxValue){
            $("#mediumMinValue").css("border", "1px solid red");
            $("#mediumMaxValue").css("border", "1px solid red");
        }
        if(mediumMaxValue >= highMinValue){
            $("#mediumMaxValue").css("border", "1px solid red");
            $("#highMinValue").css("border", "1px solid red");
        }
        if(highMinValue > highMaxValue){
            $("#highMinValue").css("border", "1px solid red");
            $("#highMaxValue").css("border", "1px solid red");
        }
        return hasErrors;
    }
    
    
    function deleteStockLevel(id, description){
        if (confirm("<%=Languages.getString("jsp.admin.tools.stockLevels.messageDelete", SessionData.getLanguage())%>")) {
            $.ajax({
                type: "POST",
                async: false,
                data: {IdMerchantsStockLevels: id, action: 'deleteStockLevels'},
                url: "admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp",
                success: function (data) {
                }
            });
            newConfiguration();
        }
        location.reload();
    }

    function editStockLevel(id, description){
    
        $('#merchantList').empty();
        $('#idStockLevel').val(id);
        $("#spanInfo").text("<%=Languages.getString("jsp.admin.tools.stockLevels.editingMessage", SessionData.getLanguage())%> "+description);
        $("#action").val('updateMerchantsStockLevels');
        $.ajax({
                type: "POST",
                async: false,
                data: {IdMerchantsStockLevels: id, action: 'getMerchantsStockLevels'},
                url: "admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp",
                success: function (data) {
                    var array_data = String($.trim(data)).split(",");
                    $("#description").val(array_data[0]);
                    $("#lowMinValue").val(array_data[1]);
                    $("#lowMaxValue").val(array_data[2]);
                    $("#mediumMinValue").val(array_data[3]);
                    $("#mediumMaxValue").val(array_data[4]);
                    $("#highMinValue").val(array_data[5]);
                    $("#highMaxValue").val(array_data[6]);
                }
            });
            
        var arrayMerchants = [];
        $.ajax({
                type: "POST",
                async: false,
                data: {IdMerchantsStockLevels: id, action: 'getMerchantsStockLevelsDetail'},
                url: "admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp",
                success: function (data) {
                    arrayMerchants = String($.trim(data)).split("\n");
                }
            });
        $.ajax({
                type: "POST",
                async: false,
                data: {arrMerchants: arrayMerchants, action: 'merchantList'},
                url: "admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp",
                success: function (data) {
                    var array_data = String($.trim(data)).split("\n");
                    for (var i = 0; i < array_data.length; i++) {
                        var merchantData = String($.trim(array_data[i])).split(",");
                        $('#merchantList').append('<option value="' + merchantData[0] + '">'+merchantData[1]+'</option>');//New input field html 
                    }
                }
            });
    }
    
    function removeSelectItems(){
        $('#merchantList option:selected').remove();
    }
    
    function newConfiguration(){
        $("#spanInfo").text("<%=Languages.getString("jsp.admin.tools.stockLevels.buttonNewConfiguration", SessionData.getLanguage())%>");
        $('#merchantList').empty();
        $('#idStockLevel').val('');
        $("#description").val('');
        $("#lowMinValue").val('');
        $("#lowMaxValue").val('');
        $("#mediumMinValue").val('');
        $("#mediumMaxValue").val('');
        $("#highMinValue").val('');
        $("#highMaxValue").val('');
        $("#action").val('insertMerchantsStockLevels');
    }
    
    $(document).ready(function(){
       $("input:text.numeric").keydown(function(event){ // fields with class numeric
            if (event.shiftKey == true) {
                event.preventDefault();
            }

            if ((event.keyCode >= 48 && event.keyCode <= 57) || 
                (event.keyCode >= 96 && event.keyCode <= 105) || 
                event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
                event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

            } else {
                event.preventDefault();
            }

            if($(this).val().indexOf('.') !== -1 && event.keyCode == 190){
                event.preventDefault(); 
            }
            //if a decimal has been added, disable the "."-button

            }); 
    });

</script>




<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp; <%=Languages.getString("jsp.includes.menu.stockLevelsConfig", SessionData.getLanguage())%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF">
            <form id="formMerchantsStockLevels" name="formMerchantsStockLevels" class="reportForm" name="mainform" method="post" action="admin/tools/stockLevelsThreshold/stockLevelsThresholdVerifyDB.jsp" onsubmit="return validateForm();">
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="formArea2" colspan="3">                                        
                        <table width="1500px" border="0">
                            <tr>
                                
                                <table width="80%" border="1" cellspacing="2" align="center" height="70">
                                    <tr>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.description", SessionData.getLanguage())%></td>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.lowMinValue", SessionData.getLanguage())%></td>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.lowMaxValue", SessionData.getLanguage())%></td>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.mediumMinValue", SessionData.getLanguage())%></td>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.mediumMaxValue", SessionData.getLanguage())%></td>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.highMinValue", SessionData.getLanguage())%></td>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.highMaxValue", SessionData.getLanguage())%></td>
                                        <td><%=Languages.getString("jsp.admin.tools.stockLevels.options", SessionData.getLanguage())%></td>
                                    </tr>
                                    <%
                                        for(MerchantsStockLevelsPojo stockLevels : stockLevelsList){%>
                                        <tr>
                                            <td><%=stockLevels.getDescription()%></td>
                                            <td><%=stockLevels.getLowMinValue()%></td>
                                            <td><%=stockLevels.getLowMaxValue()%></td>
                                            <td><%=stockLevels.getMediumMinValue()%></td>
                                            <td><%=stockLevels.getMediumMaxValue()%></td>
                                            <td><%=stockLevels.getHighMinValue()%></td>
                                            <td><%=stockLevels.getHighMaxValue()%></td>
                                            <td><input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.stockLevels.buttonEdit", SessionData.getLanguage())%>" onclick="editStockLevel('<%=stockLevels.getId()%>','<%=stockLevels.getDescription()%>');">
                                            <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.stockLevels.buttonDelete", SessionData.getLanguage())%>" onclick="deleteStockLevel('<%=stockLevels.getId()%>','<%=stockLevels.getDescription()%>');"></td>
                                        </tr>
                                        <%}%>
                                </table>
                                <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.stockLevels.buttonNewConfiguration", SessionData.getLanguage())%>" onclick="newConfiguration();">
                                
                                </tr>
                            <tr>
                                <td colspan="3">
                                    <jsp:include page="/admin/customers/merchants/entity_combo_selector.jsp" >
                                        <jsp:param value="<%=strRefId%>" name="strRefId" />
                                        <jsp:param value="<%=strAccessLevel%>" name="strAccessLevel" />
                                        <jsp:param value="<%=strDistChainType%>" name="strDistChainType" />                                            
                                    </jsp:include>                                                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table style="border: 0px solid #ccc; border-collapse: separate; border-spacing: 20px;">
                                        <tr style="vertical-align: top">                                                                                                                     

                                            <td align="right">
                                                  <span id="spanWarningMessages"><%=textWarning%></span>
                                                  <br>
                                                  <span id="spanInfo"><%=textWarning%></span>
                                            </td>
                                        </tr>

                                    </table>                                  
                                </td> 
                            </tr>
                            <tr>
                                <td class=main align="center" colspan="3">
                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.stockLevels.buttonAdd", SessionData.getLanguage())%>" onclick="addToThresholdList();">
                                </td>
                            </tr>
                            
                            <tr>
                                <td align="center" width="60%">
                                    <fieldset>
                                        <legend><%=Languages.getString("jsp.admin.tools.stockLevels.ranges", SessionData.getLanguage())%></legend>
                                        <table border="0" cellspacing="6">
                                            <tr>
                                                <td class="main" align="center" colspan="2"><%=Languages.getString("jsp.admin.tools.stockLevels.description", SessionData.getLanguage())%></td>
                                                <td class="main" align="center" colspan="3"><input type="text" name="description" id="description" value="" size="50" maxlength="50"/></td>
                                            </tr>
                                            <tr>
                                                <td class="main" align="center" colspan="2">.</td>
                                                <td class="main" align="center" colspan="3">.</td>
                                            </tr>
                                            <tr>
                                                <td class="main" align="center" ><%=Languages.getString("jsp.admin.tools.stockLevels.lowMinValue", SessionData.getLanguage())%></td>
                                                <td class="main" align="center" ><input type="text" class="numeric" name="lowMinValue" id="lowMinValue" value="" size="20" maxlength="10"/></td>
                                                <td class="main" align="center" ><%=Languages.getString("jsp.admin.tools.stockLevels.lowMaxValue", SessionData.getLanguage())%></td>
                                                <td class="main" align="center" ><input type="text" class="numeric" name="lowMaxValue" id="lowMaxValue" value="" size="20" maxlength="10"/></td>
                                                <td class="main" align="center" bgcolor="#FF0000" width="10%"></td>
                                            </tr>
                                            <tr>
                                                <td class="main" align="center" ><%=Languages.getString("jsp.admin.tools.stockLevels.mediumMinValue", SessionData.getLanguage())%></td>
                                                <td class="main" align="center" ><input type="text" class="numeric" name="mediumMinValue" id="mediumMinValue" value="" size="20" maxlength="10"/></td>
                                                <td class="main" align="center" ><%=Languages.getString("jsp.admin.tools.stockLevels.mediumMaxValue", SessionData.getLanguage())%></td>
                                                <td class="main" align="center" ><input type="text" class="numeric" name="mediumMaxValue" id="mediumMaxValue" value="" size="20" maxlength="10"/></td>
                                                <td class="main" align="center" bgcolor="#FFA500" width="10%"></td>
                                            </tr>
                                            <tr>
                                                <td class="main" align="center" ><%=Languages.getString("jsp.admin.tools.stockLevels.highMinValue", SessionData.getLanguage())%></td>
                                                <td class="main" align="center" ><input type="text" class="numeric" name="highMinValue" id="highMinValue" value="" size="20" maxlength="10"/></td>
                                                <td class="main" align="center" ><%=Languages.getString("jsp.admin.tools.stockLevels.highMaxValue", SessionData.getLanguage())%></td>
                                                <td class="main" align="center" ><input type="text" class="numeric" name="highMaxValue" id="highMaxValue" value="" size="20" maxlength="10"/></td>
                                                <td class="main" align="center" bgcolor="#00FF00" width="10%"></td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                                
                                <td align="center" width="10%">
                                    <%=Languages.getString("jsp.admin.tools.stockLevels.merchantsStockLevelList", SessionData.getLanguage())%>
                                </td>
                                <td align="left" width="30%">
                                    <select id="merchantList" name="merchantList" size="15" multiple=""></select>
                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.stockLevels.buttonRemoveSelectItems", SessionData.getLanguage())%>" onclick="removeSelectItems();">
                                </td>
                                
                            </tr>
                            <tr>
                                <input type="hidden" name="idStockLevel" id="idStockLevel" value="<%=idStockLevel%>" />
                                <input type="hidden" name="action" id="action" value="<%=actionValue%>" />
                                <td align="center"><input type="submit" name="submit" id="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonVerifyAndProcess", SessionData.getLanguage())%>" align="center" /></td>
                            </tr>
                            
                            
                        </table>
                       </form>
                    </td>
                </tr>

            </table>                                                
        </td>
    </tr>
</table>


<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>
