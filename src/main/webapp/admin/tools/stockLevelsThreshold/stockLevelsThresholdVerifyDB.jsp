<%-- 
    Document   : stockLevelsThresholdVerifyDB
    Created on : Oct 7, 2015, 9:55:51 AM
    Author     : dgarzon
--%>

<%@page import="com.debisys.utils.JSONObject"%>
<%@page import="java.util.Vector"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.debisys.customers.CustomerSearch"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.debisys.tools.stockLevels.MerchantsStockLevelsPojo"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.stockLevels.MerchantStockLevels"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String strDistChainType = SessionData.getProperty("dist_chain_type");
    String strRefId = SessionData.getProperty("ref_id");
    String strAccessLevel = SessionData.getProperty("access_level");
    String action = request.getParameter("action");
    if(action.equals("verifyMerchantList")){
        String[] mArray = request.getParameterValues("arrMerchants[]");
        MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
        List<String> duplicateList = merchantStockLevels.verifyDuplicateMerchants(mArray);
        for (String merchant : duplicateList) {
            out.println(merchant);
        }
    }
    if(action.equals("merchantList")){
        String[] mArray = request.getParameterValues("arrMerchants[]");
        MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
        List<String> merchants = merchantStockLevels.getMerchantBussinessName(mArray);
        for (String merchant : merchants) {
            out.println(merchant);
        }
    }
    if(action.equals("insertMerchantsStockLevels") || action.equals("updateMerchantsStockLevels")){
        
        String[] mArray = request.getParameterValues("merchantList");
        
        String description = request.getParameter("description");
        String lowMinValue = request.getParameter("lowMinValue");
        String lowMaxValue = request.getParameter("lowMaxValue");
        String mediumMinValue = request.getParameter("mediumMinValue");
        String mediumMaxValue = request.getParameter("mediumMaxValue");
        String highMinValue = request.getParameter("highMinValue");
        String highMaxValue = request.getParameter("highMaxValue");
        
        MerchantsStockLevelsPojo pojo = new MerchantsStockLevelsPojo();
        pojo.setDescription(description);
        pojo.setIsoId(new BigDecimal(SessionData.getProperty("iso_id")));
        pojo.setLowMinValue(Double.parseDouble(lowMinValue));
        pojo.setLowMaxValue(Double.parseDouble(lowMaxValue));
        pojo.setMediumMinValue(Double.parseDouble(mediumMinValue));
        pojo.setMediumMaxValue(Double.parseDouble(mediumMaxValue));
        pojo.setHighMinValue(Double.parseDouble(highMinValue));
        pojo.setHighMaxValue(Double.parseDouble(highMaxValue));
        
        pojo.setLowColor("#FF0000");
        pojo.setMediumColor("#FFA500");
        pojo.setHighColor("#00FF00");
        
        MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
        
        if(action.equals("insertMerchantsStockLevels")){
            merchantStockLevels.insertMerchantsStockLevels(pojo,mArray);
        }
        else if(action.equals("updateMerchantsStockLevels")){
            String idStockLevel = request.getParameter("idStockLevel");
            pojo.setId(idStockLevel);
            merchantStockLevels.updateMerchantsStockLevels(pojo, mArray);
        }
        response.sendRedirect("stockLevelsThreshold.jsp##");
    }
    
    if(action.equals("getMerchantsStockLevels")){
        String id = request.getParameter("IdMerchantsStockLevels");
        MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
        MerchantsStockLevelsPojo obj = merchantStockLevels.getMerchantStockLevelsByIso(id,SessionData.getProperty("iso_id")).get(0);
        out.print(obj.getDescription()+","+obj.getLowMinValue()+","+obj.getLowMaxValue()+","+obj.getMediumMinValue()+","+obj.getMediumMaxValue()+","+obj.getHighMinValue()+","+obj.getHighMaxValue()+
        ","+obj.getLowColor()+","+obj.getMediumColor()+","+obj.getHighColor());
    }
    if(action.equals("getMerchantsStockLevelsDetail")){
        String id = request.getParameter("IdMerchantsStockLevels");
        MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
        List<String> objList = merchantStockLevels.getMerchantStockLevelsDetail(id);
        for (String merchant : objList) {
            out.println(merchant);
        }
    }
    if(action.equals("deleteStockLevels")){
        String id = request.getParameter("IdMerchantsStockLevels");
        MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
        merchantStockLevels.deleteMerchantsStockLevels(id);
        response.sendRedirect("stockLevelsThreshold.jsp##");
    }
    if(action.equals("stockLevelsReport")){
        String stockLevelsId = request.getParameter("merchantStockLevelId");
        String responseType = request.getParameter("responseType");
        
        ArrayList<String> fieldToRead = new ArrayList<String>();
        fieldToRead.add("m.merchant_id");
        fieldToRead.add("m.dba");
        fieldToRead.add("m.latitude");
        fieldToRead.add("m.longitude");
        fieldToRead.add("m.datecancelled");
        fieldToRead.add("m.cancelled");
        fieldToRead.add("m.LiabilityLimit");
        fieldToRead.add("m.RunningLiability");
        
        fieldToRead.add("msl.description");
        fieldToRead.add("msl.isoId");
        fieldToRead.add("msl.lowColor");
        fieldToRead.add("msl.mediumColor");
        fieldToRead.add("msl.highColor");
        fieldToRead.add("msl.lowMinValue");
        fieldToRead.add("msl.lowMaxValue");
        fieldToRead.add("msl.mediumMinValue");
        fieldToRead.add("msl.mediumMaxValue");
        fieldToRead.add("msl.highMinValue");
        fieldToRead.add("msl.highMaxValue");
        
        MerchantStockLevels merchantStockLevels = new MerchantStockLevels();
        StringBuilder entities = new StringBuilder();
        String sqlQuery = merchantStockLevels.getReportQuery(stockLevelsId, fieldToRead, strAccessLevel, strRefId, strDistChainType, null, null);
        if ( sqlQuery != null && sqlQuery.length() > 0 ){
            if ( responseType != null && responseType.equals("html"))
            {
                Vector vecEntitiesList = CustomerSearch.executeReport(sqlQuery, fieldToRead);
                Iterator it = vecEntitiesList.iterator();
                String all = Languages.getString("jsp.admin.reports.all",SessionData.getLanguage());
                entities.append("<option value=-1 >" + all + "</option>");
                while (it.hasNext())
                {
                  Vector vecTemp = null;
                  vecTemp = (Vector) it.next();
                  entities.append("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                } 
            }
            else
            {
                JSONObject json = CustomerSearch.searchMerchantsWithLatLong(sqlQuery,fieldToRead);
                entities.append(json.toString());
                System.out.println(entities.toString());
            }       
        }    
        out.println( entities.toString() );
    }
    
    
%>
