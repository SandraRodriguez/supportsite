<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to add a bonus and topup threshold
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="BonusThreshold" class="com.debisys.tools.BonusThreshold" scope="request"/>
<%
  	int section=11;
  	int section_page=2;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
<%

%>

<script>  
function submitform()
{
  document.bonusPromoForm.submit();
}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.bonusPromoTools.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
				<tr>
					<td class=formArea2>
						<table>
							<tr>
								<td colspan="2">					
									<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
										<tr>
											<td class="main"><br>
												<table width="100%" cellspacing="1" cellpadding="2">
													<form name=bonusPromoForm action="admin/tools/bonusPromoTools.jsp">
													<tr>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoTools.title.newBonusThreshold",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
											        	<td><input type=text name=newThreshold /></td>              				
													</tr>
													<tr>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoTools.title.newBonusAward",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
											            <td><input type=text name=newBonus /></td>
											            <td><input type=hidden name=type value="INSERT" /></td>
													</tr>
													</form>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table>
							<tr>
								<td>
									<input type="button" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoTools.add_bonusThresholds",SessionData.getLanguage())%>" onClick=submitform();>
								</td>
								<td>
									<form action="admin/tools/bonusPromoTools.jsp">
										<input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoTools.cancelInsert",SessionData.getLanguage())%>">
									</form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>