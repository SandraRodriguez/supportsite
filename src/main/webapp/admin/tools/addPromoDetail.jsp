<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.*,
                 java.util.Hashtable,
				 com.debisys.reports.jfreechart.Chart,                 
                 com.debisys.promotions.*" %>
<%
int section=9;
int section_page=17;
int ipromoThresholdId = -1;
String strPromoId= "";
String strISOID = "";
String strRepID = "";
String strCarrierID = "";
String strInstance="";
int  iReturn=-1;
String strError="";
String strSource="";
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%
	strInstance = SessionData.getProperty("instance");
	strPromoId=request.getParameter("promoID");
	strRepID=request.getParameter("repId");
	strCarrierID=request.getParameter("carrierId");
	strSource=request.getParameter("lblSource");
  	Vector vecPromoDetails = new Vector();
	if ((request.getParameter("promoThresholdId") != null)&&(NumberUtil.isNumeric(request.getParameter("promoThresholdId"))) ){
		ipromoThresholdId = Integer.parseInt(request.getParameter("promoThresholdId"));
		vecPromoDetails = Promotion.GetPromoDetails(ipromoThresholdId);
	}
	int sizeDetails = vecPromoDetails.size();
	
	if (request.getParameter("submitted") != null){
	try{
		if (request.getParameter("submitted").equals("y")){
			String deletePromoDetails[]=request.getParameterValues("deletePromoDetail");
			ipromoThresholdId = Integer.parseInt(request.getParameter("promoThresholdId"));
			String newPropertyName = request.getParameter("newPropertyName");
			String newPropertyValue = request.getParameter("newPropertyValue");
			if (deletePromoDetails != null && deletePromoDetails.length > 0){
					for(int i=0; i<deletePromoDetails.length;i++){
						
							Promotion.RemovePromoDetail(SessionData, deletePromoDetails[i],strInstance);
					}
			}else
				{
					if (newPropertyName != null && !newPropertyName.equals(""))
					{
	           			if(newPropertyValue != null && !newPropertyValue.equals(""))
						{
							iReturn = Promotion.InsertPromoDetail(ipromoThresholdId,newPropertyName,newPropertyValue,SessionData, strInstance);
				 				if (iReturn != 0) {
				  					strError = Languages.getString("jsp.admin.tools.editpromotions.pterror",SessionData.getLanguage());
				  				}	 	
	             		}
	           	    }
	             }	         
      }
	      
      
	}catch (Exception e){
		System.out.println(""+e);
	}
}

if ((request.getParameter("promoThresholdId") != null)&&(NumberUtil.isNumeric(request.getParameter("promoThresholdId"))) ){
		ipromoThresholdId = Integer.parseInt(request.getParameter("promoThresholdId"));
		vecPromoDetails = Promotion.GetPromoDetails(ipromoThresholdId);
	}
%>
<%@ include file="/includes/header.jsp" %>

<script language="javascript">
</script>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.editpromotions.promoDetailHeader",SessionData.getLanguage())%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
  <tr>
  	<td colspan="3" bgcolor="#ffffff">
 <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff">
  <tr>
   <td class="formArea2">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="main" align="center">
          <br><br>
       <form method="post" action="admin/tools/addPromoDetail.jsp">
        <input type="hidden" name="submitted" value="y">
        <input type="hidden" name="promoThresholdId" value='<%=request.getParameter("promoThresholdId")%>'>
          <table border="0" width="280">
          <tr>
          <td class="rowhead2" nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.details.name",SessionData.getLanguage())%></td>
          <td class="rowhead2" nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.details.value",SessionData.getLanguage())%></td>
          <td class="rowhead2"><%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.delete",SessionData.getLanguage())%></td>
          </tr>
          <%
          if ( vecPromoDetails.size() == 0)
          {
            out.print("<TR><TD COLSPAN=5 ALIGN=center CLASS=main>" + Languages.getString("jsp.admin.tools.editpromotions.details.noDetails",SessionData.getLanguage()) + "</TD></TR>");
          }
          else
          {            
            sizeDetails = vecPromoDetails.size();
              
             Iterator it = vecPromoDetails.iterator();
				int intEvenOdd = 1;
			 	int icount=0;
			 while (it.hasNext()) {
            	icount++;
					PromoDetails promTemp = (PromoDetails) it.next();	
	      		 out.println("<tr class=row2>" +
                          "<td>" +promTemp.getPropertyName()+ "</td>" +
                          "<td>" + promTemp.getPropertyValue() + "</td>" +
                           "<td align=center><input type=checkbox name=deletePromoDetail value=\"" + promTemp.getDetailsId() + "\"></td>" +
                          
                          "</tr>");
             }
             icount++;
          }
          %>
          <tr class="errorText"><td><%=strError%></td></tr>
		   </td>
           </tr>
</table>

		   <% out.print(Languages.getString("jsp.admin.tools.editpromotions.details.instructions",SessionData.getLanguage()));
               %>
         	 <table border="0" cellpadding="3" cellspacing="0" class="main">
        
         <tr>
         	<td colspan="2" align="center"><b><%=Languages.getString("jsp.admin.tools.editpromotions.details.title",SessionData.getLanguage())%></b></td>
         </tr>
          <% out.println("<tr>"  +
                "<td>"+ Languages.getString("jsp.admin.tools.editpromotions.details.name",SessionData.getLanguage())+"</td>" +
                "<td><input type=text id=\"newPropertyName\" name=newPropertyName value=\"\" size=20 maxlength=50></td>" +
                "</tr>" +
                "<tr>"  +
                "<td>"+ Languages.getString("jsp.admin.tools.editpromotions.details.value",SessionData.getLanguage())+"</td>" +
                "<td><input type=text id=\"newPropertyValue\" name=newPropertyValue value=\"\" size=20 maxlength=50></td><td><div id=\"div_cleck_name\" name=\"div_cleck_name\"></div></td>" +
                "</tr>");                
		%>		
                          <tr>
                               <td colspan="3" align="center"><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.submit",SessionData.getLanguage())%>"></td>
                             <td>
                             <input id="repId" type="hidden" name="repId" value="<%=strRepID%>">
								<input id="carrierId" type="hidden" name="carrierId" value="<%=strCarrierID%>">	
									<input id="promoID" type="hidden" name="promoID" value="<%=strPromoId%>">		
										<input id="lblSource" type="hidden" name="lblSource" value="<%=strSource%>">							
                             <input id="btnSubmit1" type="button" value="<%=Languages.getString("jsp.admin.customers.merchants_clerk_codes.back",SessionData.getLanguage())%>" ONCLICK="<%="javascript:location.href='/support/admin/tools/editpromotion.jsp?lblSource=" + strSource+"&promoID="+strPromoId+ "&repId=" + strRepID +"&carrierId=" + strCarrierID + "';"%>">
                            
       				 			</td>
       				 		</tr>
    					
		</table>
     </form>
   	</td>
							</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
<%@ include file="/includes/footer.jsp" %>
