<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to set the bonuses and topup thresholds
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="BonusThreshold" class="com.debisys.tools.BonusThreshold" scope="request"/>
<%
  	int section=11;
  	int section_page=4;
  	int result = -1;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 

<table width="99%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td>
<%
	if(request.getParameter("type") != null)
	{
		if(request.getParameter("type").equals("INSERT"))
		{
			BonusThreshold.insertBonusThresholdSMS(request, SessionData);
		}
		else if(request.getParameter("type").equals("UPDATE"))
		{
			BonusThreshold.updateBonusThresholdSMS(request, SessionData);
		}
	}
	Vector vecSearchResults = BonusThreshold.getBonusThresholdSMS(request, SessionData);
%>
<script>  
	function submitform()
	{
	  document.bonusPromoSMSForm.submit();
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width=100% align=center>
				<tr>
					<td class=formArea2>
						<table>
							<tr>
								<td colspan="4">					
									<table border="0" cellpadding="2" cellspacing="0" width=100% align="center">
										<tr>
											<td class="main"><br>
												<table cellspacing="1" cellpadding="2" width=100%>
													<tr>
														<th class=rowhead2>#</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.provider",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.name",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.message",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>   	   
														<th class=rowhead2><%=Languages.getString("jsp.admin.tools.bonusPromoTools.edit",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
													</tr>
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
%>		
														<tr class=rowwrap<%=intEvenOdd%>>
															<td valign=top><%=intCounter%></td>
															<td valign=top id=provider<%=intCounter%> name=provider<%=intCounter%>><%=vecTemp.get(0)%></td>
															<td valign=top id=msgName<%=intCounter%> name=msgName<%=intCounter%>><%=vecTemp.get(1)%></td>
															<td class=noclass style='width:500;text-wrap:hard-wrap' id=msg<%=intCounter%> name=msg<%=intCounter++%>><%=vecTemp.get(2)%></td>
															<td valign=top><a href="admin/tools/updateBonusPromoSMS.jsp?oldProvider=<%=vecTemp.get(0)%>&msgName=<%=vecTemp.get(1)%>&type=UPDATE"><img src="images/icon_edit.gif" border=0 alt="Edit this SMS msg." title="Edit this SMS msg." /></a></td>
														</tr>
<%          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>									
									<form name=insertThresholdBonus action="admin/tools/updateBonusPromoSMS.jsp">
										<input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.add_bonusThresholdSMS",SessionData.getLanguage())%>">
										<input type=hidden name=type value=INSERT />
									</form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>