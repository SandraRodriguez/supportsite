<%@ page language="java" import="java.util.*, com.debisys.languages.*,com.debisys.customers.Rep" pageEncoding="ISO-8859-1"%>
<%
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="s2k" class="com.debisys.tools.s2k" scope="request"/>
<%
	
  	int section=9;
  	int section_page=15;
  	int result = -1;
%>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
<% 
    
	String strRefId = SessionData.getProperty("ref_id");
	String isoId = Rep.getIsoID(strRefId, strAccessLevel);
	if(isoId == null){
	 isoId = strRefId;
	}
%>
<style>
.tablebutton {
align:center;
text-align: center;
}
input.btnhov {
  color:#050;
  font: bold 84% 'trebuchet ms',helvetica,sans-serif;
  background-color:#ffaaaaff;
  border: 2px solid;
  border-color: #7b9ebd;
  filter:progid:DXImageTransform.Microsoft.Gradient
  (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffaaaaff');
}
input.btn {
  color:#050;
  font: bold 84% 'trebuchet ms',helvetica,sans-serif;
  background-color:#ffaaaaff;
  border: 2px solid;
  border-color: #005eb2;
  filter:progid:DXImageTransform.Microsoft.Gradient
  (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffaaaaff');
}
</style>

<script type="text/javascript" src="includes/s2k/s2ksalesfunctions.js"></script>
<table   width="99%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td>
<%
	Vector vecSearchResults = s2k.gets2ksalesman(isoId);
%>
<table border="0" cellpadding="0" cellspacing="0" width="800"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.s2k.task.title122",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
	
		<td colspan="3"  bgcolor="#FFFFFF">
			<noscript>
								   <%
								   out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>"); 
								   out.println("<li>" + Languages.getString("jsp.s2k.errornoscript",SessionData.getLanguage())); 
									  out.println("</font></td></tr></table>"); 
								   %>
			</noscript>
			<table border="0" cellpadding="0" cellspacing="0" width=100% align=center>
				<tr>
					<td class=formArea>
						<table>
							<tr>
								<td colspan="4">					
									<table border="0" cellpadding="2" cellspacing="0" width=100% align="center">
										<tr>
											<td class="main"><br>
													<div style="height: 372px; overflow-y: auto;">
												<table  id="dataTable"  cellspacing="1" cellpadding="2" width=100% style = "table-layout: fixed;">
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  >
														<th class=rowhead2 width="40%"><%=Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
														<th class=rowhead2 width="40%"><%=Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>    	   
														<th class=rowhead2 width="10%" style="text-align:center;"><%=Languages.getString("jsp.admin.tools.s2k.edit",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2 width="10%" style="text-align:center;"><%=Languages.getString("jsp.admin.tools.s2k.delete",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
													</tr>
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
%>		
														<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"   class=rowwrap<%=intEvenOdd%>>
															<td valign=top><%=vecTemp.get(0)%></td>
															<td valign=top><%=vecTemp.get(1)%></td>
															<td valign=top align=center><img src="images/icon_edit.gif"    onclick="edit('<%=isoId%>','<%=vecTemp.get(0).toString()%>','<%=vecTemp.get(1).toString()%>')"  border=0 alt="Edit" title="Edit" /></td>
															<td valign=top align=center><img src="images/icon_delete.gif"  onclick="deletesalesman('<%=isoId%>','<%=vecTemp.get(0).toString()%>','<%=vecTemp.get(1).toString()%>')"  border=0 alt="Delete" title="Delete" /></td>
														</tr>
<%          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            				
									</table></div>
								</td>
							</tr></table>
							<table cellspacing="1" cellpadding="2" width=100% height ="30px" style = "table-layout: fixed;">							<tr>
								<td class=rowhead2> 				
									<%=Languages.getString("jsp.admin.customers.salesmanid",SessionData.getLanguage())%>:				
									<input type="text" name ="sid" id="sid" maxlength="4" onkeypress="doClick('addbutton',event);"/> 
									<%=Languages.getString("jsp.admin.customers.salesmanname",SessionData.getLanguage())%>:				
									<input type="text" name ="sn"  maxlength="55" id="sn" value="" onkeypress="doClick('addbutton',event);"/> 
									<input type="hidden" name="ISOID" id= "ISOID" value="<%=isoId%>" />
									<input id="addbutton" name="addbutton" onmouseover="hov(this,'btnhov');" onmouseout="hov(this,'btn');"  type="button" style="" onclick="addRow(<%=isoId%>)" value="<%=Languages.getString("jsp.admin.tools.s2k.add",SessionData.getLanguage())%>"/>
									<input id="editbutton" name="editbutton" type="hidden" value="<%=Languages.getString("jsp.admin.tools.s2k.save",SessionData.getLanguage())%>"/>
								    <input id="add" name="add" type="hidden" value=" <%=Languages.getString("jsp.admin.tools.s2k.add",SessionData.getLanguage())%> "/>
									<input id="cancelbutton" name="cancelbutton" onmouseover="hov(this,'btnhov')" onmouseout="hov(this,'btn')"  type="button" onclick="cancel();" value="<%=Languages.getString("jsp.admin.tools.s2k.cancel",SessionData.getLanguage())%>"/>
<script type="text/javascript">
//hide button onload								
var cancel_button = document.getElementById('cancelbutton');
  cancel_button.style.visibility = 'hidden'; 
</script>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>