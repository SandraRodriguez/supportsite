<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.debisys.reports.pojo.TerminalTypePojo"%>
<%@page import="com.debisys.reports.pojo.BasicPojo"%>
<%@ page import="com.debisys.rateplans.RatePlan,
         java.util.*,
         java.net.URLEncoder,
         com.debisys.utils.*,
         java.util.Hashtable,
         com.debisys.reports.jfreechart.Chart,                 
         com.debisys.promotions.*" %>

<%@page import="com.debisys.utils.TimeZone"%>                 

<%
    int section = 9;
    int section_page = 12;
    String sPromoID = "";
    Promotion promotion = null;
    String sSource = "";
    String sISOID = "";
    String sProductID = "";
    String strEntity = "";
    String strUncheckedEntity = "";
    String strMerchId = "";
    String strSiteId = "";
    String sRepID = "";
    String sAgentID = "";
    String sSubagentID = "";
    String sMerchantID = "";
    String sAction = "";
    String sSiteID = "";
    String sAccountID = "";
    String sCarrierID = "";
    String strAccountID = "";
    String strPrThresholdID = "";
    String strLine = "";
    String strError = "";
    String strPromoName = "";
    int iIsoType = 0;
    float thr_min_amount = 0;
    int thr_thresholdOutOf = 0;
    int thr_thresholdChances = 0;
    float thr_bonus = 0;
    String thr_add_receipt = "";
    int thr_award = 0;
    int thr_consolation = 0;
    int thr_type = 0, iReturn = -1;
    ISO iso;
    String strInstance = "";
    String strRepID = "";
    String strPromost = "";
    String strData = "";
    String strAdText = "";
    String strChances = "";
    String strOutof = "";
    String strStartTime = "";
    String strEndTime = "";
    String strStartDate = "";
    String strEndDate = "";
    String strShowReport = "";
    String strRadProduction = "0";
    String selectedProdList = "";
    String pageSubmittedVal = "";
    String treeVal = "";
    String isFirstTime = "true";
    String treeResult = "";
    String entityList = "";
    String sCheckedList = "";
    String sPartailCheckedList = "";
    String sCheckedNodes = "";
    String sCreateCheckList = "";
    String entityExists = "";
    String strMinBalance = "";
    String strLastDayMonth = "";
    if (request.getParameter("isTreeFirstTime") != null) {
        isFirstTime = request.getParameter("isTreeFirstTime");
    }
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>

<%    Vector vecISOs = new Vector();
    Vector vecProducts = new Vector();
    Vector vecSearchResults = new Vector();
    Vector vecAuthMerchants = new Vector();
    Vector vecAuthTestMerchants = new Vector();
    Vector vecReps = new Vector();
    Vector vecSubagents = new Vector();
    Vector vecAgents = new Vector();
    Vector vecMerchants = new Vector();
    Vector vecSites = new Vector();

    Vector vecAuthAccounts = new Vector();
    Vector vecPromoThresholds = new Vector();
    Vector vecPromoProducts = new Vector();
    strInstance = SessionData.getProperty("instance");
    boolean bManagePromoDetails = com.debisys.users.User.isManagePromoDetailsEnabled(SessionData, application);
    Vector countryResults = Promotion.getISO(request, SessionData);
    Iterator iterator = countryResults.iterator();
    Vector vecTemp1 = (Vector) iterator.next();
    String rootIso = vecTemp1.get(0).toString();
    String rootId = String.valueOf(vecTemp1.get(1));
    boolean qrCodeOnly = false;
    boolean isConcatenatedPromo = false;
    int concatenatedPromoInterval = 0;

    if (request.getParameter("lblShowReport") != null) {
        strShowReport = request.getParameter("lblShowReport");
    }

    if (request.getParameter("radProduction") != null) {
        strRadProduction = request.getParameter("radProduction");
    }
    if (request.getParameter("PageSubmitted") != null) {
        pageSubmittedVal = request.getParameter("PageSubmitted");
    }

    if (request.getParameter("selectedProdList") != null) {
        selectedProdList = request.getParameter("selectedProdList");
    }

    if (request.getParameter("lblSource") != null) {
        sSource = request.getParameter("lblSource");
    }
    if (request.getParameter("carrierId") != null) {
        sCarrierID = request.getParameter("carrierId");
    }

    if (request.getParameter("treeVal") != null) {
        treeVal = request.getParameter("treeVal");
    }

    if ((request.getParameter("repId") == null) && ((sCarrierID == null) || (!NumberUtil.isNumeric(sCarrierID)))) {
        vecISOs = ISO.GetISOList();
    } else {
        sISOID = request.getParameter("repId");
        strRepID = request.getParameter("repId");
    }
    if ((sCarrierID != null) && (NumberUtil.isNumeric(sCarrierID))) {
        vecISOs = ISO.GetISOListByCarrier(Long.parseLong(sCarrierID));
    }
    
    if (request.getParameter("qrCodeOnly") != null) {
        qrCodeOnly = Boolean.valueOf(request.getParameter("qrCodeOnly"));
    }

    if (request.getParameter("chkIsConcatenatedPromo") != null) {
        isConcatenatedPromo = true;
    }
    if (request.getParameter("txtConcatenatedPromoInterval") != null) {
        concatenatedPromoInterval = Integer.valueOf(request.getParameter("txtConcatenatedPromoInterval"))  * 60 * 1000;
    }
    

    if (sSource.equals("addnew")) {
        promotion = new Promotion();
        promotion.setActive(0);
        iso = new ISO();
        if (NumberUtil.isNumeric(sISOID)) {
            iso.setIsoID(Long.parseLong(sISOID));
        }
        promotion.setISO(iso);
        promotion.setPriority(0);
        Product pr = new Product();
        promotion.setProduct(pr);
        promotion.setDescription("New Promo");
        promotion.setQrCodeOnly(qrCodeOnly);
        promotion = Promotion.GetPromotionInfo(Promotion.InsertPromo(promotion, SessionData, strInstance));
        sPromoID = Integer.toString(promotion.getPromoId());
    }

    if (request.getParameter("lblLine") != null) {
        strLine = request.getParameter("lblLine");
    }

    if (request.getParameter("sitesID") != null) {
        sSiteID = request.getParameter("sitesID");
    }

    if (request.getParameter("prtrID") != null) {
        strPrThresholdID = request.getParameter("prtrID");
    }

    if (request.getParameter("checkedVal") != null) {
        strEntity = request.getParameter("checkedVal");
    }

    if (request.getParameter("checkedList") != null) {
        sCheckedList = request.getParameter("checkedList");
    }

    if (request.getParameter("uncheckedVal") != null) {
        strUncheckedEntity = request.getParameter("uncheckedVal");
    }

    if (request.getParameter("partialCheckedVal") != null) {
        sPartailCheckedList = request.getParameter("partialCheckedVal");
    }

    if (request.getParameter("createCheckList") != null) {
        sCreateCheckList = request.getParameter("createCheckList");
    }

    if (request.getParameter("merchantsID") != null) {
        sMerchantID = request.getParameter("merchantsID");
        if (NumberUtil.isNumeric(sMerchantID)) {
            vecSites = Promotion.GetSitesByMerchant(Long.parseLong(sMerchantID));
        }
    }

    if (request.getParameter("accountid") != null) {
        sAccountID = request.getParameter("accountid");
    }

    if (request.getParameter("lblAction") != null) {
        sAction = request.getParameter("lblAction");
    }

    if (request.getParameter("mids") != null) {
        strMerchId = request.getParameter("mids");
    }

    if (request.getParameter("sids") != null) {
        strSiteId = request.getParameter("sids");
    }

    if (request.getParameter("accids") != null) {
        strAccountID = request.getParameter("accids");
    }

    if (request.getParameter("promoname") != null) {
        strPromoName = request.getParameter("promoname");
    }

    if (request.getParameter("promostatus") != null) {
        strPromost = request.getParameter("promostatus");
    }

    if (request.getParameter("promodata") != null) {
        strData = request.getParameter("promodata");
    }

    if (request.getParameter("promoadtext") != null) {
        strAdText = request.getParameter("promoadtext");
    }

    if (request.getParameter("chances") != null) {
        strChances = request.getParameter("chances");
    }

    if (request.getParameter("outof") != null) {
        strOutof = request.getParameter("outof");
    }

    if (request.getParameter("minBal") != null) {
        strMinBalance = request.getParameter("minBal");
    }

    if (request.getParameter("lastDayMonth") != null) {
        strLastDayMonth = request.getParameter("lastDayMonth");
    }

    if (request.getParameter("starttime") != null) {
        strStartTime = request.getParameter("starttime");
    }

    if (request.getParameter("endtime") != null) {
        strEndTime = request.getParameter("endtime");
    }

    if (request.getParameter("startDate") != null) {
        strStartDate = request.getParameter("startDate");
    }

    if (request.getParameter("endDate") != null) {
        strEndDate = request.getParameter("endDate");
    }


    if (request.getParameter("promoID") != null && NumberUtil.isNumeric(request.getParameter("promoID"))) {
        sPromoID = request.getParameter("promoID");

        if (sAction.equals("save")) { // Save Promo basic information
            promotion = new Promotion();
            iso = new ISO();
            if ((request.getParameter("isosID") != null) && (NumberUtil.isNumeric(request.getParameter("isosID")))) {
                iso.setIsoID(Long.parseLong(request.getParameter("isosID")));
            }
            if (NumberUtil.isNumeric(sISOID)) {
                iso.setIsoID(Long.parseLong(sISOID));
            }
            promotion.setISO(iso);
            if (request.getParameter("promoname") != null) {
                promotion.setDescription(request.getParameter("promoname"));
            }
            if ((request.getParameter("promostatus") != null) && (NumberUtil.isNumeric(request.getParameter("promostatus")))) {
                promotion.setActive(Integer.parseInt(request.getParameter("promostatus")));
            }

            String strProductIds[] = request.getParameterValues("pids1");
            if (strProductIds != null) {
                Promotion.SaveProduct(Integer.valueOf(sPromoID), strProductIds, SessionData, strInstance);
            }

            if (request.getParameter("promodata") != null) {
                promotion.setData(request.getParameter("promodata"));
            }
            if (request.getParameter("promoadtext") != null) {
                promotion.setAdText(request.getParameter("promoadtext"));
            }
            if (request.getParameter("startDate") != null) {
                promotion.setStartDate(request.getParameter("startDate"));
            }            
            //if ((request.getParameter("starttime") != null) && (NumberUtil.isNumeric(request.getParameter("starttime")))) {
            //    promotion.setHourOffset(Integer.parseInt(request.getParameter("starttime")));
            //}
            if (request.getParameter("endDate") != null) {
                promotion.setEndDate(request.getParameter("endDate"));
            }
            //if ((request.getParameter("endtime") != null) && (NumberUtil.isNumeric(request.getParameter("endtime")))) {
            //    promotion.setHourOffsetEnd(Integer.parseInt(request.getParameter("endtime")));
            //}
            if (request.getParameter("recurrenceType") != null){                
                if ( request.getParameter("recurrenceType").equals(Promotion.RECURRENCE_PATTERN.WEEKLY.toString()) ){
                    promotion.setRecurrenceType(Promotion.RECURRENCE_PATTERN.WEEKLY);
                    if (request.getParameter("dayW") != null ){
                        String[] SelectedValues = request.getParameterValues("dayW");
                        String dayValues = StringUtils.join(SelectedValues,",");                        
                        promotion.setRecurrencePattern(dayValues);                                        
                    }
                } else if( request.getParameter("recurrenceType").equals(Promotion.RECURRENCE_PATTERN.MONTHLY.toString()) ){
                    promotion.setRecurrenceType(Promotion.RECURRENCE_PATTERN.MONTHLY);
                    if (request.getParameter("dayM") != null ){
                        String[] SelectedValues = request.getParameterValues("dayM");
                        String dayValues = StringUtils.join(SelectedValues,",");                        
                        promotion.setRecurrencePattern(dayValues);                                        
                    }
                } else{
                    promotion.setRecurrenceType(Promotion.RECURRENCE_PATTERN.NONE);
                    promotion.setRecurrencePattern(null);
                }                
            }
           
            if ((request.getParameter("chances") != null) && (NumberUtil.isNumeric(request.getParameter("chances")))) {
                promotion.setChances(Integer.parseInt(request.getParameter("chances")));
            }
            if ((request.getParameter("minBal") != null) && (NumberUtil.isNumeric(request.getParameter("minBal")))) {
                promotion.setMinBalRequired(Double.parseDouble(request.getParameter("minBal")));
            }

            if (request.getParameter("lastDayMonth") != null && (NumberUtil.isNumeric(request.getParameter("lastDayMonth")))) {
                promotion.setLastDayMonth(Integer.parseInt(request.getParameter("lastDayMonth")));
            }
            if ((request.getParameter("outof") != null) && (NumberUtil.isNumeric(request.getParameter("outof")))) {
                promotion.setOutOf(Integer.parseInt(request.getParameter("outof")));
            }

            if (request.getParameter("qrCodeOnly") != null) {
                promotion.setQrCodeOnly(qrCodeOnly);
            }
                       
            if (isConcatenatedPromo && (concatenatedPromoInterval == 0)){
                strError = Languages.getString("jsp.admin.tools.editpromotions.concatenatedIntervalInvalid", SessionData.getLanguage());
            }else{
                promotion.setConcatenatedPromo(isConcatenatedPromo);
                promotion.setConcatenatedPromoInterval(concatenatedPromoInterval);                
            }

            if (promotion.getLastDayMonth() < 0 || promotion.getLastDayMonth() > 31) {
                strError = Languages.getString("jsp.admin.tools.editpromotions.lastMonthDayerr", SessionData.getLanguage());
            } else if (promotion.getMinBalRequired() < 0) {
                strError = Languages.getString("jsp.admin.tools.editpromotions.minBalanceerr", SessionData.getLanguage());
            } else if (promotion.getChances() > promotion.getOutOf()) {
                strError = Languages.getString("jsp.admin.tools.editpromotions.chanceserr", SessionData.getLanguage());
            } else {
                promotion.setPromoId(Integer.parseInt(sPromoID));
                iReturn = Promotion.UpdatePromo(promotion, SessionData, strInstance);
                if (iReturn != 0) {
                    strError = Languages.getString("jsp.admin.tools.editpromotions.promnotexists", SessionData.getLanguage());
                    if ( iReturn == 2){
                        strError = Languages.getString("jsp.admin.tools.editpromotions.validation.dates", SessionData.getLanguage());
                    }
                }
            }
        }

        if (request.getParameter("repsID") != null) {
            sRepID = request.getParameter("repsID");
            if (NumberUtil.isNumeric(sRepID)) {
                vecMerchants = Promotion.GetMerchantsByRep(Long.parseLong(sRepID));
            }
        }

        promotion = Promotion.GetPromotionInfo(Integer.parseInt(sPromoID));
        if (sAction.equals("addacc")) {
            if (NumberUtil.isNumeric(sAccountID)) {
                //Add Account
                Promotion.AddAccount(promotion.getPromoId(), sAccountID, 1, SessionData, strInstance);
            }
        } else if (sAction.equals("remacc")) {
            if (NumberUtil.isNumeric(strAccountID)) {
                //Removes account
                Promotion.RemoveAccountN(promotion.getPromoId(), strAccountID, 1, SessionData, strInstance);
            }
        } else if (sAction.equals("deleteth")) {
            if (NumberUtil.isNumeric(strPrThresholdID)) {
                //Removes threshold
                Promotion.RemoveThreshold(promotion.getPromoId(), Integer.parseInt(strPrThresholdID), SessionData, strInstance);
            }
        } else if (sAction.equals("entitySave")) {
            if (strEntity != "" && strUncheckedEntity != "" && sPartailCheckedList != "") {
                entityList = strEntity + "," + strUncheckedEntity + "," + sPartailCheckedList;
            }
            if (strEntity == "") {
                entityList = strUncheckedEntity;
            }
            if (strUncheckedEntity == "") {
                entityList = strEntity;
            }
            Promotion.saveTree(promotion.getPromoId(), entityList, SessionData, strInstance);

        } else if (sAction.equals("updateth")) {
            if (NumberUtil.isNumeric(strPrThresholdID) && NumberUtil.isNumeric(strLine)) {
                //Update threshold
                String strVariable = "thr_min_amount_" + strLine;
                if (request.getParameter(strVariable) != null) {
                    thr_min_amount = Float.parseFloat(NumberUtil.formatAmount(request.getParameter(strVariable)));
                }
                strVariable = "thr_bonus_" + strLine;
                if (request.getParameter(strVariable) != null) {
                    thr_bonus = Float.parseFloat(NumberUtil.formatAmount(request.getParameter(strVariable)));
                }
                strVariable = "thr_add_receipt_" + strLine;
                if (request.getParameter(strVariable) != null) {
                    thr_add_receipt = request.getParameter(strVariable);
                }
                strVariable = "thr_type_" + strLine;
                if ((request.getParameter(strVariable) != null) && (NumberUtil.isNumeric(request.getParameter(strVariable)))) {
                    thr_type = Integer.parseInt(request.getParameter(strVariable));
                    if (thr_type == 0) {
                        thr_bonus = thr_bonus * (-1) / 100;
                    }
                }
                strVariable = "thr_thresholdOutOf_" + strLine;
                if (request.getParameter(strVariable) != null && NumberUtil.isNumeric(request.getParameter(strVariable))) {
                    thr_thresholdOutOf = Integer.parseInt(request.getParameter(strVariable));
                }
                strVariable = "thr_thresholdChances_" + strLine;
                if (request.getParameter(strVariable) != null && NumberUtil.isNumeric(request.getParameter(strVariable))) {
                    thr_thresholdChances = Integer.parseInt(request.getParameter(strVariable));
                }

                vecPromoThresholds = Promotion.GetPromoThresholds(promotion.getPromoId());
                for (int i = 0; i <= vecPromoThresholds.size(); i++) {
                    if (i == (Integer.valueOf(strLine) - 1)) {
                        PromThreshold promThreshold = null;
                        promThreshold = (PromThreshold) vecPromoThresholds.get(i);
                        thr_consolation = promThreshold.getConsAwardTypeID();
                        thr_award = promThreshold.getAwardTypeID();
                    }
                }
                iReturn = Promotion.UpdateThreshold(promotion.getPromoId(), Integer.parseInt(strPrThresholdID), thr_min_amount, thr_bonus, thr_add_receipt, thr_award, thr_consolation, SessionData, strInstance, thr_thresholdChances, thr_thresholdOutOf);
                if (iReturn != 0) {
                    strError = Languages.getString("jsp.admin.tools.editpromotions.pterror", SessionData.getLanguage());
                }
            }
        } else if (sAction.equals("addth")) {
            if (NumberUtil.isNumeric(strPrThresholdID) && NumberUtil.isNumeric(strLine)) {
                //Update threshold
                String strVariable = "thr_min_amount_" + strLine;
                if (request.getParameter(strVariable) != null) {
                    thr_min_amount = Float.parseFloat(NumberUtil.formatAmount(request.getParameter(strVariable)));
                }
                strVariable = "thr_bonus_" + strLine;
                if (request.getParameter(strVariable) != null) {
                    thr_bonus = Float.parseFloat(NumberUtil.formatAmount(request.getParameter(strVariable)));
                }
                strVariable = "thr_add_receipt_" + strLine;
                if (request.getParameter(strVariable) != null) {
                    thr_add_receipt = request.getParameter(strVariable);
                }

                strVariable = "thr_type_" + strLine;
                if ((request.getParameter(strVariable) != null) && (NumberUtil.isNumeric(request.getParameter(strVariable)))) {
                    thr_type = Integer.parseInt(request.getParameter(strVariable));
                    if (thr_type == 0) {
                        thr_bonus = thr_bonus * (-1) / 100;
                    }
                }
                strVariable = "thr_thresholdOutOf_" + strLine;
                if ((request.getParameter(strVariable) != null) && NumberUtil.isNumeric(request.getParameter(strVariable))) {
                    thr_thresholdOutOf = Integer.parseInt(request.getParameter(strVariable));
                } else {
                    thr_thresholdOutOf = 0;
                }
                strVariable = "thr_thresholdChances_" + strLine;
                if ((request.getParameter(strVariable) != null) && NumberUtil.isNumeric(request.getParameter(strVariable))) {
                    thr_thresholdChances = Integer.valueOf(request.getParameter(strVariable));
                } else {
                    thr_thresholdChances = 0;
                }

                iReturn = Promotion.InsertThreshold(promotion.getPromoId(), thr_min_amount, thr_bonus, thr_add_receipt, 0, 0, SessionData, strInstance, thr_thresholdChances, thr_thresholdOutOf);
                if (iReturn != 0) {
                    strError = Languages.getString("jsp.admin.tools.editpromotions.pterror", SessionData.getLanguage());
                }
            }
        }
        // To see if the tree exists
        entityExists = Promotion.checkEntityConfiguration(Integer.parseInt(sPromoID));

        if (treeVal == "" || sAction.equals("entitySave") || sAction.equals("save") || sAction.equals("addth") || sAction.equals("deleteth") || sAction.equals("updateth") || sAction.equals("addacc") || sAction.equals("remacc")) {

            treeResult = Promotion.getTree(Integer.valueOf(sPromoID), SessionData);
            sCheckedNodes = Promotion.getCheckedNodes(Integer.valueOf(sPromoID));
        }

        if (treeResult == "") {
            treeResult = "null";
        }
        if (sCheckedList == "") {
            sCheckedList = "null";
        }
        if (sCheckedNodes == "") {
            sCheckedNodes = "null";
        }
    }
    ArrayList<TerminalTypePojo> selectedTerminalTypesPromo = null;
    ArrayList<TerminalTypePojo> terminalTypes = new ArrayList<TerminalTypePojo>();
    terminalTypes = Promotion.GetTerminalTypes();

    if (!sPromoID.equals("")) {

        String userIDSession = SessionData.getUser().getUsername();
        String instance = DebisysConfigListener.getInstance(application);
        String current = request.getParameter("currentIdNews");
        String terminalTypesPromoIds[] = request.getParameterValues("terminalTypesPromoIds");
        Promotion.UpdateTerminalTypesByPromo(promotion.getPromoIdNew(), terminalTypesPromoIds, current, userIDSession, instance, sPromoID);

        selectedTerminalTypesPromo = Promotion.GetTerminalTypesByPromo(promotion.getPromoIdNew());
        Vector vecTemp = Promotion.GetMerchants(promotion.getPromoId());
        vecTemp = Promotion.GetAccounts(promotion.getPromoId());
        vecAuthAccounts = (Vector) vecTemp.elementAt(1);
        vecPromoThresholds = Promotion.GetPromoThresholds(promotion.getPromoId());
        if (vecISOs.size() == 0) {
            if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
                vecReps = Promotion.GetCompanies(promotion.getISO().getIsoID(), 1);
            } else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {
                vecAgents = Promotion.GetCompanies(promotion.getISO().getIsoID(), 4);
            }
        }
        vecPromoProducts = Promotion.GetProducts(promotion.getPromoId());

    }

    if (request.getParameter("isosID") != null) {
        sISOID = request.getParameter("isosID");
    } else if (promotion != null) {
        if (NumberUtil.isNumeric(Integer.toString(promotion.getPromoId()))) {
            sISOID = Long.toString(promotion.getISO().getIsoID());
        }
    }

    if (request.getParameter("agentsID") != null) {
        sAgentID = request.getParameter("agentsID");
        if (NumberUtil.isNumeric(sAgentID)) {
            vecSubagents = Promotion.GetCompanies(Long.parseLong(sAgentID), 5);
        }
    }

    if (request.getParameter("subsID") != null) {
        sSubagentID = request.getParameter("subsID");
        if (NumberUtil.isNumeric(sSubagentID)) {
            vecReps = Promotion.GetCompanies(Long.parseLong(sSubagentID), 1);
        }
    }

    if ((NumberUtil.isNumeric(sISOID)) && (NumberUtil.isNumeric(sCarrierID))) {
        vecProducts = Product.GetProductsByCarrier(Long.parseLong(sISOID), Long.parseLong(sCarrierID));
    } else if (NumberUtil.isNumeric(sISOID)) {
        vecProducts = Product.GetProductList(Long.parseLong(sISOID));
    } else if (NumberUtil.isNumeric(sCarrierID)) {
        vecProducts = Product.GetProductsByCarrier(-1, Long.parseLong(sCarrierID));
    }

    if (request.getParameter("prsID") != null) {
        sProductID = request.getParameter("prsID");
    } else if (promotion != null) {
        if (NumberUtil.isNumeric(Integer.toString(promotion.getPromoId()))) {
            sProductID = Long.toString(promotion.getProduct().getProductID());
        }
    }

    if (request.getParameterValues("pids1") != null) {
        vecPromoProducts = Promotion.GetProducts(promotion.getPromoId());
    }
    
    Calendar cal = promotion.getStartDateCal();    
    int yearUIControl  = cal.get( Calendar.YEAR );
    int monthUIControl = cal.get( Calendar.MONTH ) + 1;
    int dayUIControl   = cal.get( Calendar.DATE );
    int hourUIControl  = promotion.gethourOffset(); 
    int minUIControl   = promotion.getMinStart();
    
    Calendar calEnd = promotion.getEndDateCal();    
    int yearUIControlEnd  = calEnd.get( Calendar.YEAR );
    int monthUIControlEnd = calEnd.get( Calendar.MONTH ) + 1;
    int dayUIControlEnd   = calEnd.get( Calendar.DATE );
    int hourUIControlEnd  = promotion.gethourOffsetEnd();
    int minUIControlEnd   = promotion.getMinEnd();
    
    String languageFlag = SessionData.getUser().getLanguageCode(); 
    	
    String daysUIControl = "";
    String daysMinUIControl = "";
          
    ArrayList<BaseDaysMonths> days = CalendarBussines.getCalendarObjectsByType(CalendarBussines.CALENDAR_OBJECTS_TYPE.DAYS, languageFlag);
    for( BaseDaysMonths day :days)
    {
       daysUIControl = daysUIControl +"\""+ day.getName()+"\",";
       daysMinUIControl = daysMinUIControl +"\""+ day.getShortName()+"\",";
    }    
    
    String path = request.getContextPath();
%>
<%@ include file="/includes/header.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 

    <head> 
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
        <script type="text/javascript" src="includes/_lib/jquery.min.js"></script>    
        <script type="text/javascript" src="includes/jquery.jstree.js"></script>
        <script type="text/javascript" src="includes/_lib/jquery.cookie.js"></script>
        <script type="text/javascript" src="includes/_lib/jquery.hotkeys.js"></script>
        <link type="text/css" rel="stylesheet" href="includes/_docs/syntax/!style.css"/>
        <link type="text/css" rel="stylesheet" href="includes/_docs/!style.css"/>
        <script type="text/javascript" src="includes/_docs/syntax/!script.js"></script>	
        <script type="text/javascript" src="includes/json2.js"></script>
        
        <!--script type="text/javascript" src="<%=path%>/js/jquery.js"></script-->  
        <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script> 
        <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
        <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
        <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
        <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/> 
        <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>
              
        <script language="javascript">

            function validateForm()
            {

            <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
                if (document.getElementById('agentsID').options[document.getElementById('agentsID').selectedIndex].text === "All")
                {
                    document.getElementById('subsID').disabled = true;
                    document.getElementById('repsID').disabled = true;
                    document.getElementById('merchantsID').disabled = true;
                    document.getElementById('sitesID').disabled = true;
                }
                if (document.getElementById('subsID').options[document.getElementById('subsID').selectedIndex].text === "All")
                {
                    document.getElementById('repsID').disabled = true;
                    document.getElementById('merchantsID').disabled = true;
                    document.getElementById('sitesID').disabled = true;
                }
            <%}%>
                if (document.getElementById('repsID').options[document.getElementById('repsID').selectedIndex].text === "All")
                {
                    document.getElementById('merchantsID').disabled = true;
                    document.getElementById('sitesID').disabled = true;
                }
                if (document.getElementById('merchantsID').options[document.getElementById('merchantsID').selectedIndex].text === "All")
                {
                    document.getElementById('sitesID').disabled = true;
                }

            }

            function deleteNode()
            {
                var rootId = document.getElementById('rootId').value;
                var agree = confirm("<%=Languages.getString("jsp.admin.tools.managepromotions.deleteEntityConfirm", SessionData.getLanguage())%>");
                if (agree)
                {
            <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
                    if ($('#demo1').jstree('get_selected').attr("id") === "1-a-" + rootId)
                    {
                        $('#demo1').empty();
                        document.getElementById('treeVal').value = "";
                        createTree();

                    } else
                    {
                        $("#demo1").jstree('delete_node', $('#demo1').jstree('get_selected'));

                        var json = $.jstree._reference('#demo1').get_json(-1);
                        var jsonString = JSON.stringify(json);
                        document.getElementById('treeVal').value = jsonString;

                    }
            <%} else {%>
                    if ($('#demo1').jstree('get_selected').attr("id") === "1-r-" + rootId)
                    {
                        $('#demo1').empty();
                        document.getElementById('treeVal').value = "";
                        createTree();

                    } else
                    {
                        $("#demo1").jstree('delete_node', $('#demo1').jstree('get_selected'));

                        var json = $.jstree._reference('#demo1').get_json(-1);
                        var jsonString = JSON.stringify(json);
                        document.getElementById('treeVal').value = jsonString;

                    }
            <%}%>

                } else
                {
                    return false;
                }

            }
            $(document).ready(function () {


                var checked_nodes = [];
                $('#demo1').live('click', function (event) {
                    event.preventDefault();
                    checked_nodes.push($('#demo1').jstree('get_selected').attr('id'));

                });
                
                var rType = "<%=promotion.getRecurrenceType()%>";
                var rTypePattern = "<%=promotion.getRecurrencePattern()%>";                
                $('#DaysInNONE').hide();   
                $('#DaysInWeek').hide();
                $('#DaysInMonth').hide();  
                if (rTypePattern !== "null"){                    
                    var patternValues = rTypePattern.split(',');                    
                    for( i=0; i<patternValues.length; i++ ){                        
                        if ( rType=== "<%=Promotion.RECURRENCE_PATTERN.WEEKLY%>" ){                    
                            $("#DaysInWeek tr td input[id="+patternValues[i]+"]").each(function(){
                                $(this).prop('checked', true);                               
                            });
                            $('#DaysInWeek').show();
                            $('#DaysInMonth').hide();
                        } else if ( rType=== "<%=Promotion.RECURRENCE_PATTERN.MONTHLY %>" ){
                            $('#DaysInWeek').hide();
                            $('#DaysInMonth').show();
                            $("#DaysInMonth  tr td input[id="+patternValues[i]+"]").each(function(){
                                $(this).prop('checked', true);
                            });                    
                        }   
                    }
                }
                
                
                
                $("#txtConcatenatedPromoInterval").keydown(function (event) {
                    if (event.shiftKey) {
                        event.preventDefault();
                    }

                    if (event.keyCode === 46 || event.keyCode === 8) {
                    }
                    else {
                        if (event.keyCode < 95) {
                            if (event.keyCode < 48 || event.keyCode > 57) {
                                event.preventDefault();
                            }
                        }
                        else {
                            if (event.keyCode < 96 || event.keyCode > 105) {
                                event.preventDefault();
                            }
                        }
                    }
                });                

                $('#chkIsConcatenatedPromo').click(function() {
                    if(this.checked){                                      
                        if ($("#txtConcatenatedPromoInterval").attr('disabled')) {
                            $("#txtConcatenatedPromoInterval").removeAttr('disabled');
                        }
                    }else{
                        $("#txtConcatenatedPromoInterval").attr({'disabled': 'disabled'});
                        $("#txtConcatenatedPromoInterval").val("0");
                    }
                });
                
                
                
                $("#recurrenceType").val(rType);
                             
                
                $('.btnOpenCalendarStart').live('click', function (event) {
                    $('input[name="startDate"]').click();
                });
                $('.btnOpenCalendarEnd').live('click', function (event) {
                    $('input[name="endDate"]').click();
                });
                  
                  
                $('input[name="startDate"]').daterangepicker({
                    "showDropdowns": true,
                    "showWeekNumbers": false,
                    "singleDatePicker": true,
                    "autoApply": false,
                    "timePicker": true,
                    "timePicker24Hour": false,
                    "startDate": "<%=monthUIControl%>/<%=dayUIControl%>/<%=yearUIControl%> <%=hourUIControl%>:<%=minUIControl%>",
                    "endDate": "<%=monthUIControlEnd%>/<%=dayUIControlEnd%>/<%=yearUIControlEnd%> <%=hourUIControlEnd%>:<%=minUIControlEnd%>",
                    "locale": {
                        "format": "MM/DD/YYYY h:mm A",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>',
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment()                    
                }, function (start, end, label) {
                
                }
                );
                
                
                
                $('input[name="endDate"]').daterangepicker({
                    "showDropdowns": true,
                    "showWeekNumbers": false,
                    "singleDatePicker": true,
                    "timePicker": true,
                    "timePicker24Hour": false,
                    "startDate": "<%=monthUIControlEnd%>/<%=dayUIControlEnd%>/<%=yearUIControlEnd%> <%=hourUIControlEnd%>:<%=minUIControlEnd%>",
                    "locale": {
                        "format": "MM/DD/YYYY h:mm A",
                        "separator": " - ",
                        "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                        "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                        "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                        "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                        "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                        "daysOfWeek": [
                            '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>',
                        ],
                        "monthNames": [
                            '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                            '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                        ],
                        "firstDay": 1
                    },
                    endDate: moment()                    
                }, function (start, end, label) {
                   
                });                             
                                
                    
                $("#recurrenceType").change(function() {
                    var recurrenceType = $( this ).val(); 
                    $("#recurrencTypesTR").css("border", "1px solid #bec3e4");
                    $("#recurrencTypesTR").css("padding-left", "65px");
                    $("#recurrencTypesTR").css("padding-top", "15px");
                    $("#recurrencTypesTR").css("padding-bottom", "15px");
                    if ( recurrenceType === "<%=Promotion.RECURRENCE_PATTERN.WEEKLY%>"){
                        $('#DaysInNONE').hide();                        
                        $('#DaysInWeek').show();
                        $('#DaysInMonth').hide();                        
                    } else if( recurrenceType === "<%=Promotion.RECURRENCE_PATTERN.MONTHLY%>"){
                        $('#DaysInNONE').hide();   
                        $('#DaysInWeek').hide();
                        $('#DaysInMonth').show(); 
                        $("#recurrencTypesTR").css("padding-left", "115px");
                    } else{
                        $('#DaysInWeek').hide();
                        $('#DaysInMonth').hide(); 
                        $('#DaysInNONE').hide();    
                        $("#recurrencTypesTR").css("border", "");
                        $("#recurrencTypesTR").css("padding-left", "");
                    }                                       
                });
            });
  
            
            
            function generateTree()
            {
                document.getElementById('demo1').style.visibility = 'visible';
                document.getElementById('demo1').style.display = 'block';
                $(function () {
                    $("#demo1").jstree({
                        "json_data": {
                            "data":<%=treeResult%>
                        },
                        "themes": {
                            "theme": "classic",
                            "dots": false,
                            "icons": false
                        },
                        "plugins": ["themes", "json_data", "crrm", "checkbox", "ui"], rules: {deletable: "all", clickable: "false"},
                        "core": {"initially_open": ["root.id"]}

                    });


                });

                var showList = [];
                showList = [<%=sCheckedNodes%>];
                $("#demo1").bind("load_node.jstree", function (event, data) {
                    for (var i = 0, len = showList.length; i < len; i++)
                    {
                        $("#demo1").jstree('check_node', showList[i]);
                    }

                });



            }

            function submitForm()
            {
                document.getElementById('PageSubmitted').value = '1';
                // select all the values
                var products = document.getElementById('pids1');
                var prodList = "";
                if (products.length > 0)
                {
                    for (var i = 0; i < products.options.length; i++)
                    {
                        products.options[i].selected = true;
                        prodList = prodList + products.options[i].value + ":" + products.options[i].text + ";";
                    }
                    document.getElementById('selectedProdList').value = prodList;
                }
                if ($.jstree._reference('#demo1') !== null)
                {
                    var json = $.jstree._reference('#demo1').get_json(-1);
                    var jsonString = JSON.stringify(json);
                    document.getElementById('treeVal').value = jsonString;
                }
                var createCheckList = [];
                $("#demo1").find('.jstree-checked').each(function () {
                    createCheckList.push('"#' + this.id + '"');
                });
                document.getElementById('createCheckList').value = createCheckList;
                document.forms[0].submit();
            }



            function createTree()
            {
                var root = document.getElementById('treeRoot').value;
                var rootId = document.getElementById('rootId').value;
                var result = "";
                var treeExists = false;
                document.getElementById('demo1').style.visibility = 'visible';
                document.getElementById('demo1').style.display = 'block';
                if ((document.getElementById('treeVal').value) !== '')
                {
                    result = document.getElementById('treeVal').value;
                    treeExists = true;
                } else
                {
                    result = "[{'data':'" + root + "','attr':{'id':'" + rootId + "-i-" + rootId + "','type':'ISO'},'children':[] }] ";
                }
                var jData;
                eval('jData = ' + result);


                $(function () {
                    $("#demo1").jstree({
                        "json_data": {
                            "data": jData
                        },
                        "themes": {
                            "theme": "classic",
                            "dots": false,
                            "icons": false
                        },
                        "plugins": ["themes", "json_data", "crrm", "checkbox", "ui"], rules: {deletable: "all", clickable: "false"}
                        ,
                        //"checkbox" : { "override_ui" : true  } ,
                        "core": {"initially_open": ['"#' + rootId + '-i-' + rootId + '"']}

                    });


                });

                var showList = [];
                showList = [<%=sCreateCheckList%>];
                $("#demo1").bind("load_node.jstree", function (event, data) {
                    for (var i = 0, len = showList.length; i < len; i++)
                    {
                        $("#demo1").jstree('check_node', showList[i]);
                    }

                });


            }

            function addNode()
            {
                var obj = document.getElementById('agentsID');
                var selAgentVal = obj.options[obj.selectedIndex].text.replace("'", "\'");
                var selAgentId = obj.options[obj.selectedIndex].value;
                var root = document.getElementById('treeRoot').value;
                var rootId = document.getElementById('rootId').value;
                if ($.jstree._reference('#demo1').get_text('"#' + selAgentId + '-a-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selAgentVal, "attr": {"id": selAgentId + "-a-" + rootId, "type": "Agent"}},
                            function () { }, true);
                    if (selAgentId !== 1)
                    {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-a-' + rootId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Agents", "attr": {"id": "1-a-" + rootId, "type": "Agent"}},
                                    function () { }, true);
                        }
                    }


                }
                $("#demo1").bind("select_node.jstree", function (e, data) { });
            }

            function appendTree()
            {
                var root = document.getElementById('treeRoot').value;
                var rootId = document.getElementById('rootId').value;
                var obj = document.getElementById('subsID');
                var selSubAgentVal = obj.options[obj.selectedIndex].text.replace("'", "\'");
                var selSubAgentId = obj.options[obj.selectedIndex].value;
                var selAgent = document.getElementById('agentsID');
                var selAgentId = selAgent.options[selAgent.selectedIndex].value;
                var selAgentVal = selAgent.options[selAgent.selectedIndex].text.replace("'", "\'");
                if ($.jstree._reference('#demo1').get_text('"#' + selAgentId + '-a-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selAgentVal, "attr": {"id": selAgentId + "-a-" + rootId, "type": "Agent"}},
                            function () { }, true);
                }
                if (selAgentId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-a-' + rootId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Agents", "attr": {"id": "1-a-" + rootId, "type": "Agent"}},
                                function () { }, true);
                    }
                }

                if ($.jstree._reference('#demo1').get_text('"#' + selSubAgentId + '-sa-' + selAgentId + '"') === false)
                {

                    $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": selSubAgentVal, "attr": {"id": selSubAgentId + "-sa-" + selAgentId, "type": "SubAgent"}},
                            function () { }, true);

                    if (selSubAgentId !== 1)
                    {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-sa-' + selAgentId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": "All Subagents", "attr": {"id": "1-sa-" + selAgentId, "type": "SubAgent"}},
                                    function () { }, true);
                        }

                    }

                }

                $("#demo1").bind("select_node.jstree", function (e, data) { });


            }


            function addRepToTree()
            {
                var rootId = document.getElementById('rootId').value;
                var repObj = document.getElementById('repsID');
                var selRepVal = repObj.options[repObj.selectedIndex].text.replace("'", "\'");
                var selRepId = repObj.options[repObj.selectedIndex].value;

            <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
                var obj = document.getElementById('subsID');
                var selSubAgentVal = obj.options[obj.selectedIndex].text.replace("'", "\'");
                var selSubAgentId = obj.options[obj.selectedIndex].value;
                var selAgent = document.getElementById('agentsID');
                var selAgentId = selAgent.options[selAgent.selectedIndex].value;
                var selAgentVal = selAgent.options[selAgent.selectedIndex].text.replace("'", "\'");
                if ($.jstree._reference('#demo1').get_text('"#' + selAgentId + '-a-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selAgentVal, "attr": {"id": selAgentId + "-a-" + rootId, "type": "Agent"}},
                            function () { }, true);
                }
                if (selAgentId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-a-' + rootId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Agents", "attr": {"id": "1-a-" + rootId, "type": "Agent"}},
                                function () { }, true);
                    }
                }

                if ($.jstree._reference('#demo1').get_text('"#' + selSubAgentId + '-sa-' + selAgentId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": selSubAgentVal, "attr": {"id": selSubAgentId + "-sa-" + selAgentId, "type": "SubAgent"}},
                            function () { }, true);
                }
                if (selSubAgentId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-sa-' + selAgentId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": "All Subagents", "attr": {"id": "1-sa-" + selAgentId, "type": "SubAgent"}},
                                function () { }, true);
                    }

                }
                if ($.jstree._reference('#demo1').get_text('"#' + selRepId + '-r-' + selSubAgentId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selSubAgentId + '-sa-' + selAgentId + '"', "inside", {"data": selRepVal, "attr": {"id": selRepId + "-r-" + selSubAgentId, "type": "Rep"}},
                            function () { }, true);
                    if (selRepId !== 1) {

                        if ($.jstree._reference('#demo1').get_text('"#' + '1-r-' + selSubAgentId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selSubAgentId + '-sa-' + selAgentId + '"', "inside", {"data": "All Reps", "attr": {"id": "1-r-" + selSubAgentId, "type": "Rep"}},
                                    function () { }, true);
                        }
                    }


                }
            <%} else {%>
                if ($.jstree._reference('#demo1').get_text('"#' + selRepId + '-r-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selRepVal, "attr": {"id": selRepId + "-r-" + rootId, "type": "Rep"}},
                            function () { }, true);
                    if (selRepId !== 1)
                    {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-r-' + rootId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Reps", "attr": {"id": "1-r-" + rootId, "type": "Rep"}},
                                    function () { }, true);
                        }
                    }


                }
            <%}%>
            }


            function addMerchantToTree()
            {
                var rootId = document.getElementById('rootId').value;
                var repObj = document.getElementById('repsID');
                var selRepVal = repObj.options[repObj.selectedIndex].text.replace("'", "\'");
                var selRepId = repObj.options[repObj.selectedIndex].value;
                var merchantObj = document.getElementById('merchantsID');
                var selMerVal = merchantObj.options[merchantObj.selectedIndex].text.replace("'", "\'");
                var selMerId = merchantObj.options[merchantObj.selectedIndex].value;

            <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
                var obj = document.getElementById('subsID');
                var selSubAgentVal = obj.options[obj.selectedIndex].text.replace("'", "\'");
                var selSubAgentId = obj.options[obj.selectedIndex].value;
                var selAgent = document.getElementById('agentsID');
                var selAgentId = selAgent.options[selAgent.selectedIndex].value;
                var selAgentVal = selAgent.options[selAgent.selectedIndex].text.replace("'", "\'");
                if ($.jstree._reference('#demo1').get_text('"#' + selAgentId + '-a-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selAgentVal, "attr": {"id": selAgentId + "-a-" + rootId, "type": "Agent"}},
                            function () { }, true);
                }
                if (selAgentId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-a-' + rootId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Agents", "attr": {"id": "1-a-" + rootId, "type": "Agent"}},
                                function () { }, true);
                    }
                }

                if ($.jstree._reference('#demo1').get_text('"#' + selSubAgentId + '-sa-' + selAgentId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": selSubAgentVal, "attr": {"id": selSubAgentId + "-sa-" + selAgentId, "type": "SubAgent"}},
                            function () { }, true);
                }
                if (selSubAgentId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-sa-' + selAgentId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": "All Subagents", "attr": {"id": "1-sa-" + selAgentId, "type": "SubAgent"}},
                                function () { }, true);
                    }

                }
                if ($.jstree._reference('#demo1').get_text('"#' + selRepId + '-r-' + selSubAgentId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selSubAgentId + '-sa-' + selAgentId + '"', "inside", {"data": selRepVal, "attr": {"id": selRepId + "-r-" + selSubAgentId, "type": "Rep"}},
                            function () { }, true);
                    if (selRepId !== 1) {

                        if ($.jstree._reference('#demo1').get_text('"#' + '1-r-' + selSubAgentId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selSubAgentId + '-sa-' + selAgentId + '"', "inside", {"data": "All Reps", "attr": {"id": "1-r-" + selSubAgentId, "type": "Rep"}},
                                    function () { }, true);
                        }
                    }


                }
                if ($.jstree._reference('#demo1').get_text('"#' + selMerId + '-m-' + selRepId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selRepId + '-r-' + selSubAgentId + '"', "inside", {"data": selMerVal, "attr": {"id": selMerId + "-m-" + selRepId, "type": "Merchant"}},
                            function () { }, true);
                    if (selMerId !== 1)
                    {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-m-' + selRepId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selRepId + '-r-' + selSubAgentId + '"', "inside", {"data": "All Merchants", "attr": {"id": "1-m-" + selRepId, "type": "Merchant"}},
                                    function () { }, true);
                        }
                    }



                }
            <%} else {%>
                if ($.jstree._reference('#demo1').get_text('"#' + selRepId + '-r-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selRepVal, "attr": {"id": selRepId + "-r-" + rootId, "type": "Rep"}},
                            function () { }, true);
                }
                if (selRepId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-r-' + rootId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Reps", "attr": {"id": "1-r-" + rootId, "type": "Rep"}},
                                function () { }, true);
                    }
                }
                if ($.jstree._reference('#demo1').get_text('"#' + selMerId + '-m-' + selRepId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selRepId + '-r-' + rootId + '"', "inside", {"data": selMerVal, "attr": {"id": selMerId + "-m-" + selRepId, "type": "Merchant"}},
                            function () { }, true);
                    if (selMerId !== 1)
                    {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-m-' + selRepId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selRepId + '-r-' + rootId + '"', "inside", {"data": "All Merchants", "attr": {"id": "1-m-" + selRepId, "type": "Merchant"}},
                                    function () { }, true);
                        }
                    }



                }

            <%}%>
            }

            function addSiteToTree()
            {
                var rootId = document.getElementById('rootId').value;
                var siteObj = document.getElementById('sitesID');
                var selSiteVal = siteObj.options[siteObj.selectedIndex].text.replace("'", "\'");
                var selSiteId = siteObj.options[siteObj.selectedIndex].value;
                var merchantObj = document.getElementById('merchantsID');
                var selMerVal = merchantObj.options[merchantObj.selectedIndex].text.replace("'", "\'");
                var selMerId = merchantObj.options[merchantObj.selectedIndex].value;
                var repObj = document.getElementById('repsID');
                var selRepVal = repObj.options[repObj.selectedIndex].text.replace("'", "\'");
                ;
                var selRepId = repObj.options[repObj.selectedIndex].value;
            <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
                var obj = document.getElementById('subsID');
                var selSubAgentVal = obj.options[obj.selectedIndex].text.replace("'", "\'");
                var selSubAgentId = obj.options[obj.selectedIndex].value;
                var selAgent = document.getElementById('agentsID');
                var selAgentId = selAgent.options[selAgent.selectedIndex].value;
                var selAgentVal = selAgent.options[selAgent.selectedIndex].text.replace("'", "\'");
                if ($.jstree._reference('#demo1').get_text('"#' + selAgentId + '-a-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selAgentVal, "attr": {"id": selAgentId + "-a-" + rootId, "type": "Agent"}},
                            function () { }, true);
                }
                if (selAgentId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-a-' + rootId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Agents", "attr": {"id": "1-a-" + rootId, "type": "Agent"}},
                                function () { }, true);
                    }
                }

                if ($.jstree._reference('#demo1').get_text('"#' + selSubAgentId + '-sa-' + selAgentId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": selSubAgentVal, "attr": {"id": selSubAgentId + "-sa-" + selAgentId, "type": "SubAgent"}},
                            function () { }, true);
                }
                if (selSubAgentId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-sa-' + selAgentId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + selAgentId + '-a-' + rootId + '"', "inside", {"data": "All Subagents", "attr": {"id": "1-sa-" + selAgentId, "type": "SubAgent"}},
                                function () { }, true);
                    }

                }
                if ($.jstree._reference('#demo1').get_text('"#' + selRepId + '-r-' + selSubAgentId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selSubAgentId + '-sa-' + selAgentId + '"', "inside", {"data": selRepVal, "attr": {"id": selRepId + "-r-" + selSubAgentId, "type": "Rep"}},
                            function () { }, true);
                    if (selRepId !== 1) {

                        if ($.jstree._reference('#demo1').get_text('"#' + '1-r-' + selSubAgentId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selSubAgentId + '-sa-' + selAgentId + '"', "inside", {"data": "All Reps", "attr": {"id": "1-r-" + selSubAgentId, "type": "Rep"}},
                                    function () { }, true);
                        }
                    }


                }
                if ($.jstree._reference('#demo1').get_text('"#' + selMerId + '-m-' + selRepId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selRepId + '-r-' + selSubAgentId + '"', "inside", {"data": selMerVal, "attr": {"id": selMerId + "-m-" + selRepId, "type": "Merchant"}},
                            function () { }, true);
                    if (selMerId !== 1)
                    {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-m-' + selRepId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selRepId + '-r-' + selSubAgentId + '"', "inside", {"data": "All Merchants", "attr": {"id": "1-m-" + selRepId, "type": "Merchant"}},
                                    function () { }, true);
                        }
                    }



                }
                if ($.jstree._reference('#demo1').get_text('"#' + selSiteId + '-s-' + selMerId + '"') === false)
                {

                    $("#demo1").jstree("create", '"#' + selMerId + '-m-' + selRepId + '"', "inside", {"data": selSiteVal, "attr": {"id": selSiteId + "-s-" + selMerId, "type": "Site"}},
                            function () { }, true);

                    if (selSiteId !== 1) {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-s-' + selMerId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selMerId + '-m-' + selRepId + '"', "inside", {"data": "All Sites", "attr": {"id": "1-s-" + selMerId, "type": "Site"}},
                                    function () { }, true);
                        }
                    }


                }

            <%} else {%>
                if ($.jstree._reference('#demo1').get_text('"#' + selRepId + '-r-' + rootId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": selRepVal, "attr": {"id": selRepId + "-r-" + rootId, "type": "Rep"}},
                            function () { }, true);
                }
                if (selRepId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-r-' + rootId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + rootId + '-i-' + rootId + '"', "inside", {"data": "All Reps", "attr": {"id": "1-r-" + rootId, "type": "Rep"}},
                                function () { }, true);
                    }
                }
                if ($.jstree._reference('#demo1').get_text('"#' + selMerId + '-m-' + selRepId + '"') === false)
                {
                    $("#demo1").jstree("create", '"#' + selRepId + '-r-' + rootId + '"', "inside", {"data": selMerVal, "attr": {"id": selMerId + "-m-" + selRepId, "type": "Merchant"}},
                            function () { }, true);
                }
                if (selMerId !== 1)
                {
                    if ($.jstree._reference('#demo1').get_text('"#' + '1-m-' + selRepId + '"') === false)
                    {
                        $("#demo1").jstree("create", '"#' + selRepId + '-r-' + rootId + '"', "inside", {"data": "All Merchants", "attr": {"id": "1-m-" + selRepId, "type": "Merchant"}},
                                function () { }, true);
                    }
                }

                if ($.jstree._reference('#demo1').get_text('"#' + selSiteId + '-s-' + selMerId + '"') === false)
                {

                    $("#demo1").jstree("create", '"#' + selMerId + '-m-' + selRepId + '"', "inside", {"data": selSiteVal, "attr": {"id": selSiteId + "-s-" + selMerId, "type": "Site"}},
                            function () { }, true);

                    if (selSiteId !== 1) {
                        if ($.jstree._reference('#demo1').get_text('"#' + '1-s-' + selMerId + '"') === false)
                        {
                            $("#demo1").jstree("create", '"#' + selMerId + '-m-' + selRepId + '"', "inside", {"data": "All Sites", "attr": {"id": "1-s-" + selMerId, "type": "Site"}},
                                    function () { }, true);
                        }
                    }


                }





            <%}%>

            }



            function saveTree()
            {
                document.getElementById('lblAction').value = 'entitySave';
                var json = $.jstree._reference('#demo1').get_json(-1);
                var jsonString = JSON.stringify(json);
                document.getElementById('treeVal').value = jsonString;


                var checked_ids = [];
                var unchecked_ids = [];
                var checkList_ids = [];
                var partialChecked_ids = [];
                $("#demo1").find('.jstree-checked').each(function () {
                    checked_ids.push(this.id + "-p");
                    checkList_ids.push('"#' + this.id + '"');

                });

                $("#demo1").find('.jstree-undetermined').each(function () {
                    partialChecked_ids.push(this.id + "-pr");

                });

                $("#demo1").find('.jstree-unchecked').each(function () {
                    unchecked_ids.push(this.id + "-d");

                });

                document.getElementById('checkedVal').value = checked_ids;
                document.getElementById('uncheckedVal').value = unchecked_ids;
                document.getElementById('checkedList').value = checkList_ids;
                document.getElementById('partialCheckedVal').value = partialChecked_ids;

                document.getElementById('PageSubmitted').value = '1';
                // select all the values
                var products = document.getElementById('pids1');
                var prodList = "";
                if (products.length > 0)
                {
                    for (var i = 0; i < products.options.length; i++)
                    {
                        products.options[i].selected = true;
                        prodList = prodList + products.options[i].value + ":" + products.options[i].text + ";";
                    }
                    document.getElementById('selectedProdList').value = prodList;
                }

                document.forms[0].submit();
            }

            function doPost(line, action, recid) {
                document.getElementById('lblAction').value = action;
                document.getElementById('lblLine').value = line;
                document.getElementById('prtrID').value = recid;
                document.forms[0].submit();
            }

            var NS4 = (navigator.appName === "Netscape" && parseInt(navigator.appVersion) < 5);

            function addOption(theSel, theText, theValue)
            {
                var newOpt = new Option(theText, theValue);
                var selLength = theSel.length;
                theSel.options[selLength] = newOpt;

            }

            function deleteOption(theSel, theIndex)
            {
                var selLength = theSel.length;
                if (selLength > 0)
                {
                    theSel.options[theIndex] = null;
                }
            }

            function savePromo()
            {

                document.getElementById('lblAction').value = 'save';
                var selectedProducts = document.getElementById('pids1');

                if (document.getElementById('isEntityExist').value !== "true")
                {
                    alert('<%=Languages.getString("jsp.admin.tools.editpromotions.entityMessage", SessionData.getLanguage())%>');
                    return false;
                }
                if (selectedProducts.options.length === 0)
                {
                    alert('<%=Languages.getString("jsp.admin.tools.editpromotions.productMessage", SessionData.getLanguage())%>');
                    return false;
                } else
                {
                    for (var i = 0; i < selectedProducts.options.length; i++)
                    {
                        selectedProducts.options[i].selected = true;
                    }
                    document.forms[0].submit();
                }
            }
            function moveOptions(theSelFrom, theSelTo, check)
            {

                var selLength = theSelFrom.length;
                var selectedText = new Array();
                var selectedValues = new Array();
                var selectedCount = 0;

                var i;
                var found = false;

                // Find the selected Options in reverse order
                // and delete them from the 'from' Select.
                for (i = selLength - 1; i >= 0; i--)
                {
                    if (theSelFrom.options[i].selected)
                    {
                        selectedText[selectedCount] = theSelFrom.options[i].text;
                        selectedValues[selectedCount] = theSelFrom.options[i].value;
                        if (!check)
                        {
                            deleteOption(theSelFrom, i);
                        }

                        selectedCount++;
                    }
                }

                // Add the selected text/values in reverse order.
                // This will add the Options to the 'to' Select
                // in the same order as they were in the 'from' Select.
                if (check)
                {
                    var allProductsFound = false;

                    for (i = selectedCount - 1; i >= 0; i--)
                    {
                        if (selectedValues[i] === 0)
                        {
                            allProductsFound = true;
                            for (var m = theSelTo.length - 1; m >= 0; m--)
                            {
                                deleteOption(theSelTo, m);
                            }
                            addOption(theSelTo, selectedText[i], selectedValues[i]);
                            break;
                        }
                    }
                    if (!allProductsFound) {
                        for (i = selectedCount - 1; i >= 0; i--)
                        {
                            for (k = 0; k < theSelTo.length; k++)
                            {
                                if (theSelTo.options[k].value === selectedValues[i] || theSelTo.options[k].value === 0)
                                {
                                    found = true;
                                    break;
                                }
                            }

                            if (!found) {
                                addOption(theSelTo, selectedText[i], selectedValues[i]);
                            }

                        }
                    }

                }

                if (NS4)
                    history.go(0);
            }




        </script>

        <script type="text/javascript" src="includes/json2.js"></script>


    </head>
    <body onload="validateForm();">
        <table border="0" cellpadding="0" cellspacing="0" width="750">
            <tr>
                <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
                <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.editpromotions.edittitle", SessionData.getLanguage()).toUpperCase()%></td>
                <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
            </tr>
            <tr>
                <td colspan="3"  bgcolor="#FFFFFF">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                        <tr class="errorText"><td><%=strError%></td></tr>
                        <tr>
                            <td class="errorText" align="right"><%
                                if (promotion.getStartDate().equalsIgnoreCase(promotion.getEndDate())) {
                                    out.print(Languages.getString("jsp.admin.tools.editpromotions.status4", SessionData.getLanguage()));
                                } else if (promotion.getStatus() == 1) {
                                    out.print(Languages.getString("jsp.admin.tools.editpromotions.status1", SessionData.getLanguage()));
                                } else if (promotion.getStatus() == 2) {
                                    out.print(Languages.getString("jsp.admin.tools.editpromotions.status2", SessionData.getLanguage()));
                                } else if (promotion.getStatus() == 3) {
                                    out.print(Languages.getString("jsp.admin.tools.editpromotions.status3", SessionData.getLanguage()));
                                }%></td></tr>
                        <tr>
                            <td class="main">
                                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>	    	<td class="formArea2">
                                            <br>
                                            <form id="mainform" name="mainform" method="post" action="admin/tools/editpromotion.jsp#treeName" >
                                                <table width="100%">
                                                    <tr><td class=formAreaTitle2><%=Languages.getString("jsp.admin.tools.editpromotions.generalsettings", SessionData.getLanguage())%></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <input id="repId" type="hidden" name="repId" value="<%=strRepID%>">
                                                            <input id="carrierId" type="hidden" name="carrierId" value="<%=sCarrierID%>">
                                                            <input id="lblShowReport" type="hidden" name="lblShowReport" value="<%=strShowReport%>">
                                                            <input id="btnSubmit" type="button" value="<%=Languages.getString("jsp.admin.tools.editpromotions.save", SessionData.getLanguage())%>" ONCLICK="savePromo();">
                                                            <input id="btnSubmit1" type="button" value="<%=Languages.getString("jsp.admin.tools.editpromotions.cancel", SessionData.getLanguage())%>" ONCLICK="<%="javascript:location.href='/support/admin/tools/managepromos.jsp" + "?repId=" + strRepID + "&lblShowReport=" + strShowReport + "&carrierId=" + sCarrierID + "';"%>">
                                                            <%
                                                                if (SessionData.getLanguage().equals("spanish")) {%>
                                                            <a href="admin/help/promo_live_spanish.jsp" target="_blank">
                                                                <img src="images/help.gif" width="18" height="18" border="0" alt=""<%=Languages.getString("jsp.admin.tools.editpromotions.help", SessionData.getLanguage())%>"" />
                                                            </a>
                                                            <% } else {%>
                                                            <a href="admin/help/promo_live_english.jsp" target="_blank">
                                                                <img src="images/help.gif" width="18" height="18" border="0" alt=""<%=Languages.getString("jsp.admin.tools.editpromotions.help", SessionData.getLanguage())%>"" />
                                                            </a>
                                                            <% }%>	
                                                            <input id="treeRoot" type="hidden" name="treeRoot" value="<%=rootIso%>">
                                                            <input id="rootId" type="hidden" name="rootId" value="<%=rootId%>">
                                                            <input name="PageSubmitted" id="PageSubmitted" type="hidden" value="0"/>
                                                            <input name='selectedProdList' id='selectedProdList' type='hidden' value=''/>
                                                            <input name='subPromoName' id='subPromoName' type='hidden' value=''/>
                                                            <input name='treeVal' id='treeVal' type='hidden' value='<%=treeVal%>'/>
                                                            <input name='checkedVal' id='checkedVal' type='hidden' value=''/>
                                                            <input name='uncheckedVal' id='uncheckedVal' type='hidden' value=''/>
                                                            <input name='checkedList' id='checkedList' type='hidden' value='<%=sCheckedList%>'/>
                                                            <input name='partialCheckedVal' id='partialCheckedVal' type='hidden' value=''/>
                                                            <input name='isTreeFirstTime' id='isTreeFirstTime' type='hidden' value=''/>
                                                            <input name='createCheckList' id='createCheckList' type='hidden' value='<%=sCreateCheckList%>'/>
                                                            <input name='isEntityExist' id='isEntityExist' type='hidden' value='<%=entityExists%>'/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="formAreaTitle2">
                                                            <table class="formArea2">
                                                                <tr class="main"><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.promoid", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td><%=sPromoID%><input id="promoID" type="hidden" name="promoID" value=<%=sPromoID%>></td></tr>
                                                                <tr class="main" ><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.promoname", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td><input class="plain" id="promoname" name="promoname" <%if (!pageSubmittedVal.equalsIgnoreCase("1")) {%> value="<%=strPromoName.equals("") ? promotion.getDescription() : strPromoName%>" <% } else {%> value="<%= strPromoName%>" <% }%> size="50" maxlength="100"></td></tr>
                                                                <tr class="main"><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.promostatus", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td><%=promotion.getActive() == 0 ? Languages.getString("jsp.admin.tools.editpromotions.disabled", SessionData.getLanguage()) : Languages.getString("jsp.admin.tools.editpromotions.enabled", SessionData.getLanguage())%>
                                                                    </td></tr>
                                                                <br>	
                                                                <%Iterator it = null;
                                                                    if ((vecISOs != null) && (vecISOs.size() > 0)) {%>  
                                                                <tr class="main">
                                                                    <td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.isoname", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>					
                                                                    <td>
                                                                        <select id="isosID" name="isosID" onchange="javascript:document.forms[0].submit();">
                                                                            <option value=""><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_isos", SessionData.getLanguage())%></option>
                                                                            <%
                                                                                it = vecISOs.iterator();
                                                                                while (it.hasNext()) {
                                                                                    ISO isoTemp = null;
                                                                                    isoTemp = (ISO) it.next();
                                                                                    if (!sISOID.equals(String.valueOf(isoTemp.getIsoID()))) {
                                                                                        out.println("<option value=\"" + isoTemp.getIsoID() + "\">"
                                                                                                + HTMLEncoder.encode(isoTemp.getIsoName()) + "</option>");
                                                                                    } else {
                                                                                        out.println("<option value=\"" + isoTemp.getIsoID() + "\" selected>"
                                                                                                + HTMLEncoder.encode(isoTemp.getIsoName()) + "</option>");
                                                                                        iIsoType = isoTemp.getIsoType();
                                                                                        if (iIsoType == 2) {
                                                                                            vecReps = Promotion.GetCompanies(isoTemp.getIsoID(), 1);
                                                                                            //vecRepsTest = (Vector) vecReps.clone();
                                                                                        } else {
                                                                                            vecAgents = Promotion.GetCompanies(isoTemp.getIsoID(), 4);
                                                                                            //vecAgentsTest = (Vector) vecAgents.clone();
                                                                                        }
                                                                                    }
                                                                                }
                                                                                vecISOs.clear();
                                                                            %>
                                                                        </select>					
                                                                    </td>
                                                                </tr>  	
                                                                <%}%>
                                                                <tr class="main">
                                                                    <td><%=Languages.getString("jsp.admin.tools.editpromotions.productname", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                                <tr class="main">
                                                                    <td><%=Languages.getString("jsp.admin.tools.editpromotions.availableProduct", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>	
                                                                    <td><%=Languages.getString("jsp.admin.tools.editpromotions.chosenProduct", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>				
                                                                </tr>
                                                                <tr class="main">
                                                                    <td>
                                                                        <select name="pids" id="pids" size="10" multiple>
                                                                            <option value="0"><%=Languages.getString("jsp.admin.tools.managepromotions.all_products", SessionData.getLanguage())%>  (0)</option>											  
                                                                            <%
                                                                                it = vecProducts.iterator();
                                                                                while (it.hasNext()) {
                                                                                    Product prTemp = null;
                                                                                    prTemp = (Product) it.next();
                                                                                    out.println("<option value=\"" + prTemp.getProductID() + "\">"
                                                                                            + HTMLEncoder.encode(prTemp.getProductDes() + "  (" + prTemp.getProductID() + ")") + "</option>");
                                                                                }
                                                                            %>
                                                                        </select>					
                                                                    </td>
                                                                    <td align="center" valign="middle">
                                                                        <input type="button" value="Add"
                                                                               onclick="moveOptions(this.form.pids, this.form.pids1, true);" /><br />
                                                                        <input type="button" value="Remove"
                                                                               onclick="moveOptions(this.form.pids1, this.form.pids, false);" />
                                                                    </td>

                                                                    <td>
                                                                        <select id="pids1" name="pids1" size="10" multiple>
                                                                            <% it = vecPromoProducts.iterator();

                                                                                if (pageSubmittedVal.equalsIgnoreCase("1")) {
                                                                                    String prodList[] = selectedProdList.split(";");
                                                                                    if (selectedProdList != "") {
                                                                                        for (int i = 0; i < prodList.length; i++) {
                                                                                            String prodInfo[] = prodList[i].split(":");
                                                                                            out.println("<option value=\"" + prodInfo[0].toString() + "\">"
                                                                                                    + HTMLEncoder.encode(prodInfo[1].toString()) + "</option>");

                                                                                        }

                                                                                    }

                                                                                } else {
                                                                                    while (it.hasNext()) {
                                                                                        Vector vecTemp = null;
                                                                                        vecTemp = (Vector) it.next();
                                                                                        out.println("<option value=\"" + vecTemp.get(0).toString() + "\">"
                                                                                                + HTMLEncoder.encode(vecTemp.get(1).toString() + "  (" + vecTemp.get(0).toString() + ")") + "</option>");
                                                                                        //selectedProdList = selectedProdList + vecTemp.get(0).toString() + ":" + vecTemp.get(1).toString().trim() + ";"; 						
                                                                                    }
                                                                                }
                                                                            %>
                                                                        </select>					
                                                                    </td>
                                                                </tr>
                                                                <tr class="main" >
                                                                    <td colspan="3">*<%=Languages.getString("jsp.admin.reports.transactions.products.instructions", SessionData.getLanguage())%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    
                                                                </tr>
                                                                <tr class="main" ><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.datapromo", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td><input class="plain" id="promodata" name="promodata" value="<%=strData.equals("") ? ((promotion.getData() != null) ? promotion.getData() : "") : strData%>" size="50" maxlength="100"></td></tr>		
                                                                <tr class="main" ><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.adtext", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td><input class="plain" id="promoadtext" name="promoadtext" value="<%=strAdText.equals("") ? ((promotion.getAdText() != null) ? promotion.getAdText() : "") : strAdText%>" size="50" maxlength="100"></td></tr>									  	
                                                                <tr><td>&nbsp;&nbsp;</td></tr>	
                                                                
                                                                <%
                                                                String recurrenceLabel = Languages.getString("jsp.admin.tools.editpromotions.recurrenceType", SessionData.getLanguage());
                                                                String NoneLbl = Languages.getString("jsp.admin.recurrenceType.None", SessionData.getLanguage());
                                                                String Weeklyl = Languages.getString("jsp.admin.recurrenceType.Weekly", SessionData.getLanguage());
                                                                String MonthlyLbl = Languages.getString("jsp.admin.recurrenceType.Monthly", SessionData.getLanguage());
                                                                
                                                                %>
                                                                <tr class="main">
                                                                    <td nowrap widht="95%"><%=recurrenceLabel%>: </td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td>
                                                                        <select id="recurrenceType" name="recurrenceType" >
                                                                            <option value="NONE"><%=NoneLbl%></option>
                                                                            <option value="WEEKLY"><%=Weeklyl%></option>
                                                                            <option value="MONTHLY"><%=MonthlyLbl%></option>
                                                                        </select>                                                                        
                                                                    </td>                                                                    
                                                                </tr>    
                                                                    
                                                                <tr class="main" >          
                
                <td colspan="3" id="recurrencTypesTR" style="border: 1px solid #bec3e4; padding-left: 65px;padding-top: 15px;padding-bottom: 15px">
    <%
    String tdStyle = "style='border: 1px solid #bec3e4; padding-left: 10px;padding-top: 5px;padding-bottom: 5px;padding-right: 10px'";
    %>
                    <table id="DaysInWeek" name="DaysInWeek" >
        <tr>
            <td <%=tdStyle%> ><input type="checkbox" name="dayW" id="<%=Calendar.MONDAY%>" value="<%=Calendar.MONDAY%>"><%=Languages.getString("reports.dayName.Mo", SessionData.getLanguage())%></input> </td>
            <td <%=tdStyle%>><input type="checkbox" name="dayW" id="<%=Calendar.TUESDAY%>" value="<%=Calendar.TUESDAY%>"><%=Languages.getString("reports.dayName.Tu", SessionData.getLanguage())%></input></td>
            <td <%=tdStyle%>><input type="checkbox" name="dayW" id="<%=Calendar.WEDNESDAY%>" value="<%=Calendar.WEDNESDAY %>"><%=Languages.getString("reports.dayName.We", SessionData.getLanguage())%></input></td>
            <td <%=tdStyle%>><input type="checkbox" name="dayW" id="<%=Calendar.THURSDAY%>" value="<%=Calendar.THURSDAY %>"><%=Languages.getString("reports.dayName.Th", SessionData.getLanguage())%></input></td>
            <td <%=tdStyle%>><input type="checkbox" name="dayW" id="<%=Calendar.FRIDAY%>" value="<%=Calendar.FRIDAY %>"><%=Languages.getString("reports.dayName.Fr", SessionData.getLanguage())%></input></td>
            <td <%=tdStyle%>><input type="checkbox" name="dayW" id="<%=Calendar.SATURDAY%>" value="<%=Calendar.SATURDAY%>"><%=Languages.getString("reports.dayName.Sa", SessionData.getLanguage())%></input></td>
            <td <%=tdStyle%>><input type="checkbox" name="dayW" id="<%=Calendar.SUNDAY%>" value="<%=Calendar.SUNDAY%>"><%=Languages.getString("reports.dayName.Su", SessionData.getLanguage())%></input></td>
        </tr>        
    </table>    
    <table id="DaysInMonth" name="DaysInMonth" >
            <%
            int dayNumber = 1;
            for(int j=0; j<=4;j++){  
                out.println("<tr>");
                for(int k=0; k<=7 && dayNumber<32; k++){
                %>                    
                <td <%=tdStyle%> ><input type="checkbox" name="dayM" id="<%=dayNumber%>" value="<%=dayNumber%>"><%=dayNumber%></input></td>                                            
                <%                 
                    dayNumber++;
                }
                out.println("</tr>");   
            }%>
                
    </table>
    <table id="DaysInNONE" >
        <tr>
            <td></td>            
        </tr>        
    </table>                                                                     
                                                                    </td>                                                                                                                                        
                                                                </tr>  
                                                                
                                                                <tr class="main">
                                                                    <td nowrap widht="95%"><%=Languages.getString("jsp.admin.start_date", SessionData.getLanguage())%>: </td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td>
                <!--input class="plain" id="startDate" name="startDate" value="<%=strStartDate.equals("") ? promotion.getStartDate() : strStartDate%>" size="20">
                <a href="javascript:void(0)" onclick="if (self.gfPop)
                        gfPop.fStartPop(document.mainform.startDate, document.mainform.endDate);
                    return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
                </a>     
                <select id="starttime" name="starttime">
                    <% 	for (int i = 0; i <= 23; i++) {
                            if (((promotion.gethourOffset() == i) && (strStartTime.equals(""))) || (strStartTime.equals(Integer.toString(i)))) {
                                out.println("<option value=\"" + i + "\" selected>"
                                        + Integer.toString(i) + ":00" + "</option>");
                            } else {
                                out.println("<option value=\"" + i + "\">"
                                        + Integer.toString(i) + ":00" + "</option>");
                            }
                        }
                    %>                                                                                                                                                         
                </select -->
                <div class="form-group">                                                                                                                          
                        <div class="input-group input-group-sm">                                                                        
                            <span class="input-group-addon btnOpenCalendarStart"  id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                            <input type="text" class="form-control" name="startDate" />                                                           
                        </div>                                                                                                                                                                                                                                             
                </div>
                
                                                                        
                                                                            
                                                                    </td>
                                                                <tr class="main">   
                                                                    <td nowrap widht="95%"><%=Languages.getString("jsp.admin.end_date", SessionData.getLanguage())%>: </td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td>
                                                                        <!--input class="plain" id="endDate" name="endDate" value="<%=strEndDate.equals("") ? promotion.getEndDate() : strEndDate%>" size="20"><a href="javascript:void(0)" onclick="if (self.gfPop)
                                                                                gfPop.fEndPop(document.mainform.startDate, document.mainform.endDate);
                                                                            return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
                                                                        <select id="endtime" name="endtime">
                                                                            <% 	for (int i = 0; i <= 23; i++) {
                                                                                    if (((promotion.gethourOffsetEnd() == i) && (strEndTime.equals(""))) || (strEndTime.equals(Integer.toString(i)))) {
                                                                                        out.println("<option value=\"" + i + "\" selected>"
                                                                                                + Integer.toString(i) + ":00" + "</option>");
                                                                                    } else {
                                                                                        out.println("<option value=\"" + i + "\">"
                                                                                                + Integer.toString(i) + ":00" + "</option>");
                                                                                    }
                                                                                }
                                                                            %>
                                                                        <select-->
                <div class="form-group">                                                                                                                          
                        <div class="input-group input-group-sm">                                                                        
                            <span class="input-group-addon btnOpenCalendarEnd" id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                            <input type="text" class="form-control" name="endDate" />                                                           
                        </div>                                                                                                                                                                                                                                             
                </div>
                               
                
                                                                    </td>											    
                                                                </tr>	
                                                                <tr class="main"><td></td><td></td><td><%=Languages.getString("jsp.admin.tools.editpromotions.servertime", SessionData.getLanguage()) + ":&nbsp;&nbsp;" + Promotion.GetServerDate()%></td></tr>
                                                                <tr><td>&nbsp;&nbsp;</td></tr>
                                                                <tr class="main">   
                                                                    <td nowrap width="95%"><%=Languages.getString("jsp.admin.tools.editpromotions.minBal", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td><input class="plain" id="minBal" name="minBal"  value="<%=strMinBalance.equals("") ? Double.toString(promotion.getMinBalRequired()) : strMinBalance%>" size="10" maxlength="100">
                                                                    </td>
                                                                </tr>

                                                                <tr class="main">   
                                                                    <td nowrap width="95%"><%=Languages.getString("jsp.admin.tools.editpromotions.lastDayMonth", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>&nbsp;&nbsp;</td>
                                                                    <td><input class="plain" id="lastDayMonth" name="lastDayMonth" value="<%=strLastDayMonth.equals("") ? Integer.toString(promotion.getLastDayMonth()) : strLastDayMonth%>" size="3" maxlength="5">
                                                                    </td>
                                                                </tr>
                                                                <tr class="main">   
                                                                    <td nowrap width="33%"><input type="checkbox" name="chkIsConcatenatedPromo" id="chkIsConcatenatedPromo" <%if ((promotion.isConcatenatedPromo()) || (isConcatenatedPromo)) {out.println("checked='checked'");}%>>
                                                                            <%=Languages.getString("jsp.admin.tools.editpromotions.isConcatenatedPromo", SessionData.getLanguage()) + "&nbsp;&nbsp;"%></td>                                                                    
                                                                    <td nowrap width="34%"><%=Languages.getString("jsp.admin.tools.editpromotions.concatenatedPromoInterval", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td nowrap width="33%"><input class="plain" type="text" id="txtConcatenatedPromoInterval" name="txtConcatenatedPromoInterval" <%if((!promotion.isConcatenatedPromo()) && (!isConcatenatedPromo)){out.println("disabled");}else{out.println("");}%> value="<%if(concatenatedPromoInterval == 0) {concatenatedPromoInterval = promotion.getConcatenatedPromoInterval();} out.println(String.valueOf(concatenatedPromoInterval / 60000));%>" maxlength="4" size="3">
                                                                    </td>
                                                                </tr>
                                                            </table>		
                                                        </td>
                                                    </tr>													
                                                </table>
                                                <table width="100%">
                                                    <tr class="main"><br></tr>
                                                    <tr><td class="formAreaTitle2"><%=Languages.getString("jsp.admin.tools.editpromotions.qualifying", SessionData.getLanguage())%></td></tr>
                                                    <tr>
                                                        <td class="formArea2">
                                                            <table>
                                                                <tr class="main"><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.chances", SessionData.getLanguage())%>
                                                                        <input class="plain" id="chances" name="chances" value="<%=strChances.equals("") ? Integer.toString(promotion.getChances()) : strChances%>" size="3" maxlength="5">
                                                                        <%="&nbsp;" + Languages.getString("jsp.admin.tools.editpromotions.outof", SessionData.getLanguage()) + "&nbsp;"%>
                                                                        <input class="plain" id="outof" name="outof" value="<%=strOutof.equals("") ? Integer.toString(promotion.getOutOf()) : strOutof%>" size="3" maxlength="5">
                                                                    </td></tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="formArea2">
                                                            <table>
                                                                <tr class="main">
                                                                    <td nowrap>
                                                                        <input type="checkbox" id="qrCodeOnly" name="qrCodeOnly" value="true" <%if ((promotion.isQrCodeOnly()) || (qrCodeOnly)) {
                                                                                out.println("checked='checked'");
                                                                            }%>> <%=Languages.getString("jsp.admin.tools.editpromotions.qrCodeOnly", SessionData.getLanguage())%><br>
                                                                    </td>
                                                                    <td>

                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                </table>
                                                <table width="100%" id="authorizationTable">
                                                    <%
                                                        String sessionLanguage = SessionData.getUser().getLanguageCode();
                                                        HashMap<String, ResourceReport> propertiesReport = ResourceReport.findResourcesByReportHash(ResourceReport.RESOURCE_PROMOTION_ALLOW_TERMINAL_TYPE, sessionLanguage);
                                                        String toSelect = "_*To select/unselect only certain terminal types, hold the <CTRL> key and click on the terminal type you would like to select/unselect.";
                                                        String titleAllowTerminalTypes = "_PROMOTION - TERMINAL TYPES AUTHORIZATIONS";
                                                        String warnningAllowTerminalTypes = "_*This promo only applies to entities that have the following terminal types.";

                                                        if (propertiesReport.get("toSelect") != null) {
                                                            toSelect = propertiesReport.get("toSelect").getMessage();
                                                        }
                                                        if (propertiesReport.get("titleAllowTerminalTypes") != null) {
                                                            titleAllowTerminalTypes = propertiesReport.get("titleAllowTerminalTypes").getMessage();
                                                        }
                                                        if (propertiesReport.get("warnningAllowTerminalTypes") != null) {
                                                            warnningAllowTerminalTypes = propertiesReport.get("warnningAllowTerminalTypes").getMessage();
                                                        }
                                                    %>   
                                                    <tr>
                                                        <td class="formAreaTitle2"><%=Languages.getString("jsp.admin.tools.editpromotions.authorized", SessionData.getLanguage())%></td>
                                                        <td class="formAreaTitle2"><%=titleAllowTerminalTypes%></td>
                                                    </tr> 
                                                    <tr class="main">
                                                        <td valign="top" nowrap>
                                                            <table id="entitiesTable">
                                                                <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
                                                                <tr class="main">
                                                                    <td><%=Languages.getString("jsp.admin.tools.editpromotions.agentname", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td><select id="agentsID" name="agentsID" onchange="submitForm();">
                                                                            <option value="1"><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_agents", SessionData.getLanguage())%></option>
                                                                            <%
                                                                                it = vecAgents.iterator();
                                                                                while (it.hasNext()) {
                                                                                    Company compTemp = null;
                                                                                    compTemp = (Company) it.next();
                                                                                    if (!sAgentID.equals(String.valueOf(compTemp.getCompanyID()))) {
                                                                                        out.println("<option value=\"" + compTemp.getCompanyID() + "\">"
                                                                                                + HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
                                                                                    } else {
                                                                                        out.println("<option value=\"" + compTemp.getCompanyID() + "\" selected>"
                                                                                                + HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
                                                                                    }
                                                                                }
                                                                                vecAgents.clear();
                                                                            %>
                                                                        </select></td>
                                                                    <td><input id="btnAddAgent" type="Button" value="Add Agent" onClick="addNode()"></td>
                                                                <tr class="main">
                                                                    <td><%=Languages.getString("jsp.admin.tools.editpromotions.subagentname", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td><select id="subsID" name="subsID" onchange="submitForm();">
                                                                            <option value="1"><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_subagents", SessionData.getLanguage())%></option>
                                                                            <%
                                                                                it = vecSubagents.iterator();
                                                                                while (it.hasNext()) {
                                                                                    Company compTemp = null;
                                                                                    compTemp = (Company) it.next();
                                                                                    if (!sSubagentID.equals(String.valueOf(compTemp.getCompanyID()))) {
                                                                                        out.println("<option value=\"" + compTemp.getCompanyID() + "\">"
                                                                                                + HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
                                                                                    } else {
                                                                                        out.println("<option value=\"" + compTemp.getCompanyID() + "\" selected>"
                                                                                                + HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
                                                                                    }
                                                                                }
                                                                                vecSubagents.clear();
                                                                            %>
                                                                        </select>
                                                                    </td>			
                                                                    <td><input id="btnAddSubagent" type="button" value="Add SubAgent" onClick="appendTree()"></td>
                                                                    <!-- <td><input id="btnAddSubagent" type="button" value="Add SubAgent" onClick="updateTree('Subagent')"></td>-->				
                                                                </tr>			
                                                                <%} // End if (iIsoType == 3) %>						
                                                                <tr class="main">
                                                                    <td ><%=Languages.getString("jsp.admin.tools.editpromotions.repname", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td>
                                                                        <select id="repsID" name="repsID" onchange="submitForm();">
                                                                            <option value="1"><%=Languages.getString("jsp.admin.tools.editpromotions.view_all_reps", SessionData.getLanguage())%></option>
                                                                            <%
                                                                                it = vecReps.iterator();
                                                                                while (it.hasNext()) {
                                                                                    Company compTemp = null;
                                                                                    compTemp = (Company) it.next();
                                                                                    if (!sRepID.equals(String.valueOf(compTemp.getCompanyID()))) {
                                                                                        out.println("<option value=\"" + compTemp.getCompanyID() + "\">"
                                                                                                + HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
                                                                                    } else {
                                                                                        out.println("<option value=\"" + compTemp.getCompanyID() + "\" selected>"
                                                                                                + HTMLEncoder.encode(compTemp.getCompanyName()) + "</option>");
                                                                                    }
                                                                                }
                                                                                vecReps.clear();
                                                                            %>
                                                                        </select>

                                                                    <td><input id="btnAddRep" type="Button" value="Add Rep" ONCLICK="addRepToTree()"></td> 
                                                                    <!--<td><input id="btnAddRep" type="Button" value="Add Rep" ONCLICK="updateTree('Rep')"></td> -->

                                                                </tr>
                                                                <tr class="main">
                                                                    <td><%=Languages.getString("jsp.admin.tools.editpromotions.merchantname", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td><select id="merchantsID" name="merchantsID" onchange="submitForm();">
                                                                            <option value="1" <%=sMerchantID.equals("1") ? "selected" : ""%>> <%=Languages.getString("jsp.admin.tools.editpromotions.view_all_merchants", SessionData.getLanguage())%></option>
                                                                            <%
                                                                                it = vecMerchants.iterator();
                                                                                while (it.hasNext()) {
                                                                                    Merchant merTemp = null;
                                                                                    merTemp = (Merchant) it.next();
                                                                                    if (!sMerchantID.equals(String.valueOf(merTemp.getMerchantID()))) {
                                                                                        out.println("<option value=\"" + merTemp.getMerchantID() + "\">"
                                                                                                + HTMLEncoder.encode(merTemp.getMerchantName()) + "</option>");
                                                                                    } else {
                                                                                        out.println("<option value=\"" + merTemp.getMerchantID() + "\" selected>"
                                                                                                + HTMLEncoder.encode(merTemp.getMerchantName()) + "</option>");
                                                                                    }
                                                                                }
                                                                                vecMerchants.clear();
                                                                            %>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <input id="lblAction" type="hidden" name="lblAction" value="">
                                                                        <input id="btnAddMerchant" type="Button" value="Add Merchant" ONCLICK="addMerchantToTree()"> 
                                                                    </td>
                                                                </tr>
                                                                <tr class="main">
                                                                    <td><%=Languages.getString("jsp.admin.tools.editpromotions.sitename", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                                    <td><select id="sitesID" name="sitesID">
                                                                            <option value="1" <%=sSiteID.equals("1") ? "selected" : ""%>> <%=Languages.getString("jsp.admin.tools.editpromotions.view_all_sites", SessionData.getLanguage())%></option>
                                                                            <%
                                                                                it = vecSites.iterator();
                                                                                while (it.hasNext()) {
                                                                                    int siteTemp = (Integer) it.next();
                                                                                    if (!sSiteID.equals(String.valueOf(siteTemp))) {
                                                                                        out.println("<option value=\"" + siteTemp + "\">"
                                                                                                + HTMLEncoder.encode(String.valueOf(siteTemp)) + "</option>");
                                                                                    } else {
                                                                                        out.println("<option value=\"" + siteTemp + "\" selected>"
                                                                                                + HTMLEncoder.encode(String.valueOf(siteTemp)) + "</option>");
                                                                                    }
                                                                                }
                                                                                vecSites.clear();
                                                                            %>
                                                                        </select>
                                                                    </td>
                                                                    <td>
                                                                        <input id="btnAddSite" type="Button" value="Add Site" ONCLICK="addSiteToTree()">
                                                                        <!--<input id="btnAddSite" type="Button" value="Add Site" ONCLICK="updateTree('Site')">  -->
                                                                    </td>

                                                                </tr>	
                                                                <tr><td align="center"><input id="btnSaveTree" type="button" value="<%=Languages.getString("jsp.admin.tools.editPromo.saveEntity", SessionData.getLanguage())%>" onClick="saveTree()"></td><td><input id="btnDeleteNode" type="button" value="<%=Languages.getString("jsp.admin.tools.editPromo.deleteEntity", SessionData.getLanguage())%>" onClick="deleteNode()">
                                                                    </td>
                                                                </tr>			
                                                    </tr>

                                                    </tr>

                                                </table>					
                                        </td>
                                        <td valign="top" nowrap>

                                            <table>
                                                <%
                                                    boolean showWarnningAllowTerminalTypes = false;
                                                    StringBuilder optionsTerminalTypes = new StringBuilder();
                                                    StringBuilder currentIdNews = new StringBuilder();
                                                    if (selectedTerminalTypesPromo != null) {

                                                        for (TerminalTypePojo tt : selectedTerminalTypesPromo) {
                                                            showWarnningAllowTerminalTypes = true;
                                                            String terminalTypeIdNew = tt.getIdNewTerminalTypeId();
                                                            String terminalDesc = tt.getDescripton();
                                                            currentIdNews.append(tt.getIdNew() + "|" + tt.getIdNewTerminalTypeId()).append(",");

                                                            optionsTerminalTypes.append("<option selected id='" + terminalTypeIdNew + "' value='" + terminalTypeIdNew + "'>" + terminalDesc + "</option>");
                                                        }
                                                    }
                                                    for (TerminalTypePojo tt : terminalTypes) {
                                                        String terminalTypeId = tt.getId();
                                                        String terminalDesc = tt.getDescripton();
                                                        String terminalTypeIdNew = tt.getIdNewTerminalTypeId();
                                                        boolean showTerminalTypePromo = true;

                                                        if (selectedTerminalTypesPromo != null) {
                                                            for (BasicPojo stt : selectedTerminalTypesPromo) {
                                                                String sterminalTypeId = stt.getId();
                                                                if (sterminalTypeId.equals(terminalTypeId)) {
                                                                    showTerminalTypePromo = false;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        if (showTerminalTypePromo) {
                                                            optionsTerminalTypes.append("<option id='" + terminalTypeIdNew + "' value='" + terminalTypeIdNew + "'>" + terminalDesc + "</option>");
                                                        }
                                                    }
                                                    if (showWarnningAllowTerminalTypes) {
                                                %>
                                                <tr>                                                                    
                                                    <td colspan="2">
                                                        <%=warnningAllowTerminalTypes%>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td  style="font-size: 8px">
                                                        <select id="terminalTypesPromoIds" name="terminalTypesPromoIds" multiple size="20" class="terminalTypes-small">
                                                            <%=optionsTerminalTypes.toString()%>
                                                        </select>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" value="<%=currentIdNews%>" name="currentIdNews" >                                                                        
                                                    </td>                                                                    
                                                </tr>
                                                <tr>                                                                    
                                                    <td colspan="2">
                                                        <%=toSelect%>
                                                    </td>
                                                </tr>

                                            </table>  
                                        </td>
                                    </tr>
                                </table>
                                <a name="treeName"></a> 
                                <div style=display:none id="demo1"></div>	
                                <table>
                                    <tr class="main" >
                                        <td><table>
                                                <tr>
                                                    <td class="main" nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.addaccount", SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
                                                    <td><input class="plain" id="accountid" name="accountid" value="" size="15" style="width: 150px">
                                                        <input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.tools.editpromotions.addacc", SessionData.getLanguage())%>" ONCLICK="document.getElementById('lblAction').value = 'addacc';"></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="main" wrap><%=Languages.getString("jsp.admin.tools.editpromotions.warning", SessionData.getLanguage())%></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table></td>
                                    </tr>

                                    <tr>
                                        <td class="formArea2" >
                                            <table>
                                                <tr class="main">
                                                    <td valign="top" nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.accounts", SessionData.getLanguage()) + ":"%></td>
                                                    <td>
                                                        <select name="accids" id="accids" size="4" style="width: 150px" multiple>
                                                            <%
                                                                it = vecAuthAccounts.iterator();
                                                                while (it.hasNext()) {
                                                                    String accTemp = (String) it.next();
                                                                    out.println("<option value=" + accTemp + ">" + accTemp + "</option>");
                                                                }
                                                            %>
                                                        </select>									
                                                    </td>
                                                    <td valign="top">
                                                        <input id="btnSubmit" type="submit" value="<%=Languages.getString("jsp.admin.tools.editpromotions.removeacc", SessionData.getLanguage())%>" ONCLICK="document.getElementById('lblAction').value = 'remacc';">
                                                    </td>									

                                                </tr>
                                            </table>
                                            <table width="100%">	
                                                <tr class="main"><br></tr>					
                                                <tr><td class="formAreaTitle2"><%=Languages.getString("jsp.admin.tools.editpromotions.thresholds", SessionData.getLanguage())%></td></tr> 
                                                <tr>
                                                    <td class="formArea2" >
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="formArea2" >
                                                                    <table width="100%" cellspacing="1" cellpadding="1" border="0">     
                                                                        <tr>
                                                                            <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptmintransamount", SessionData.getLanguage()).toUpperCase()%></td>
                                                                            <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.pttype", SessionData.getLanguage()).toUpperCase()%></td>
                                                                            <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptvalue", SessionData.getLanguage()).toUpperCase()%></td>
                                                                            <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptadtext", SessionData.getLanguage()).toUpperCase()%></td>
                                                                            <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptthresholdChances", SessionData.getLanguage()).toUpperCase()%>
                                                                            <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptthresholdOut", SessionData.getLanguage()).toUpperCase()%>
                                                                            <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptoptions", SessionData.getLanguage()).toUpperCase()%>
                                                                                <input id="lblLine" type="hidden" name="lblLine" value="">
                                                                                <input id="prtrID" type="hidden" name="prtrID" value=""></td>
                                                                        </tr> 
                                                                        <%
                                                                            it = vecPromoThresholds.iterator();
                                                                            int intEvenOdd = 1;
                                                                            int icount = 0;
                                                                            while (it.hasNext()) {
                                                                                icount++;
                                                                                PromThreshold promTemp = (PromThreshold) it.next();
                                                                                out.println("<tr class=row" + intEvenOdd + ">");
                                                                                out.println("<td align=left> " + "$" + "<input type=\"textbox\" align=\"right\" class=\"plain\" name=\"thr_min_amount" + "_" + Integer.toString(icount) + "\" value=\"" + promTemp.getMinAmount() + "\" size=\"7\" maxlength=\"10\">" + "</td>");
                                                                                out.println("<td align=left><select id=\"thr_type" + "_" + Integer.toString(icount) + "\"" + "class=\"plain\" name=\"thr_type" + "_" + Integer.toString(icount) + "\"" + "enabled=\"true\" value=\"");
                                                                                out.println("thr_type" + "\">");
                                                                                out.println("<option value=\"0\" ");
                                                                                if (promTemp.getBonusAmount() < 0) {
                                                                                    out.print("selected ");
                                                                                }
                                                                                out.print(">" + Languages.getString("jsp.admin.tools.editpromotions.ptpercentage", SessionData.getLanguage()) + "</option>");
                                                                                out.print("<option value=\"1\" ");
                                                                                if (promTemp.getBonusAmount() >= 0) {
                                                                                    out.print("selected ");
                                                                                }
                                                                                out.print(">" + Languages.getString("jsp.admin.tools.editpromotions.pttvalue", SessionData.getLanguage()) + "</option></select></td>");
                                                                                out.println("<td align=left> ");
                                                                                if (promTemp.getBonusAmount() >= 0) {
                                                                                    out.println("$");
                                                                                }
                                                                                out.println("<input align=right class=\"plain\" name=\"thr_bonus" + "_" + Integer.toString(icount) + "\" value=\"" + ((promTemp.getBonusAmount() < 0) ? NumberUtil.formatAmount(Float.toString(promTemp.getBonusAmount() * -100)) : promTemp.getBonus()) + "\" size=\"7\" maxlength=\"10\">");
                                                                                if (promTemp.getBonusAmount() < 0) {
                                                                                    out.println("%");
                                                                                }
                                                                                out.println("</td>");
                                                                                out.println("<td align=left> " + "<input class=\"plain\" name=\"thr_add_receipt" + "_" + Integer.toString(icount) + "\" value='" + (promTemp.getAddReceipt() == null ? "" : promTemp.getAddReceipt()) + "' size=\"50\" maxlength=\"1000\">" + "</td>");
                                                                                out.println("<td align=left> " + "<input class=\"plain\" name=\"thr_thresholdChances" + "_" + Integer.toString(icount) + "\" value=\"" + (promTemp.getThreshChances() == null ? "0" : promTemp.getThreshChances()) + "\" size=\"3\" maxlength=\"5\">" + "</td>");
                                                                                out.println("<td align=left> " + "<input class=\"plain\" name=\"thr_thresholdOutOf" + "_" + Integer.toString(icount) + "\" value=\"" + (promTemp.getThreshOutOf() == null ? "0" : promTemp.getThreshOutOf()) + "\" size=\"3\" maxlength=\"5\">" + "</td>");

                                                                                // Pending actions                                        
                                                                                out.println("<td align=left nowrap> ");
                                                                                out.println("<table width='100%'>");
                                                                                out.println("<tr>" + "<td>" + "<a href=\"javascript:doPost(" + Integer.toString(icount) + ",'updateth'," + Integer.toString(promTemp.getRecID()) + ")" + ";\"><img border=0 src='images/icon_edit.gif'width=\"15\" height=\"15\" /></a>" + "</td>");
                                                                                out.println("<td>" + "<a href=\"javascript:doPost(" + Integer.toString(icount) + ",'deleteth'," + Integer.toString(promTemp.getRecID()) + ");\"><img border=0 src='images/icon_delete.gif' width=\"15\" height=\"15\" /></a>" + "</td>" + "</tr>");
                                                                                if (bManagePromoDetails) {
                                                                                    out.println("<tr><td colspan='2'><a id=\"detail" + Integer.toString(icount) + "\" " + "href=admin/tools/addPromoDetail.jsp?lblSource=edit&promoID=" + sPromoID + "&promoThresholdId=" + promTemp.getRecID() + "&repId=" + strRepID + "&carrierId=" + sCarrierID + ">" + Languages.getString("jsp.admin.tools.editpromotions.addPromoDetailLink", SessionData.getLanguage()) + "</a></td></tr>");

                                                                                }
                                                                                out.println("</table>");
                                                                                out.println("</td>");
                                                                                out.println("</tr>");

                                                                                if (intEvenOdd == 1) {
                                                                                    intEvenOdd = 2;
                                                                                } else {
                                                                                    intEvenOdd = 1;
                                                                                }

                                                                            }
                                                                            icount++;
                                                                            PromThreshold promTemp = new PromThreshold();
                                                                            out.println("<tr class=row" + intEvenOdd + ">");
                                                                            out.println("<td align=left> " + "$" + "<input type=\"textbox\" align=\"right\" class=\"plain\" name=\"thr_min_amount" + "_" + Integer.toString(icount) + "\" value=\"" + promTemp.getMinAmount() + "\" size=\"7\" maxlength=\"10\">" + "</td>");
                                                                            out.println("<td align=left><select id=\"thr_type" + "_" + Integer.toString(icount) + "\"" + "class=\"plain\" name=\"thr_type" + "_" + Integer.toString(icount) + "\"" + "enabled=\"true\" value=\"");
                                                                            out.println("thr_type" + "\">");
                                                                            out.println("<option value=\"0\" ");
                                                                            if (promTemp.getBonusAmount() < 0) {
                                                                                out.print("selected ");
                                                                            }
                                                                            out.print(">" + Languages.getString("jsp.admin.tools.editpromotions.ptpercentage", SessionData.getLanguage()) + "</option>");
                                                                            out.print("<option value=\"1\" ");
                                                                            if (promTemp.getBonusAmount() >= 0) {
                                                                                out.print("selected ");
                                                                            }
                                                                            out.print(">" + Languages.getString("jsp.admin.tools.editpromotions.pttvalue", SessionData.getLanguage()) + "</option></select></td>");
                                                                            out.println("<td align=left> ");
                                                                            if (promTemp.getBonusAmount() >= 0) {
                                                                                out.println("$");
                                                                            }
                                                                            out.println("<input align=right class=\"plain\" name=\"thr_bonus" + "_" + Integer.toString(icount) + "\" value=\"" + ((promTemp.getBonusAmount() < 0) ? Float.toString(promTemp.getBonusAmount() * -100) : promTemp.getBonus()) + "\" size=\"7\" maxlength=\"10\">");
                                                                            if (promTemp.getBonusAmount() < 0) {
                                                                                out.println("%");
                                                                            }
                                                                            out.println("</td>");
                                                                            out.println("<td align=left> " + "<input class=\"plain\" name=\"thr_add_receipt" + "_" + Integer.toString(icount) + "\" value=\"" + (promTemp.getAddReceipt() == null ? "" : promTemp.getAddReceipt()) + "\" size=\"50\" maxlength=\"1000\">" + "</td>");
                                                                            out.println("<td align=left> " + "<input class=\"plain\" name=\"thr_thresholdChances" + "_" + Integer.toString(icount) + "\" value=\"" + (promTemp.getThreshChances() == null ? "0" : promTemp.getThreshChances()) + "\" size=\"3\" maxlength=\"5\">" + "</td>");
                                                                            out.println("<td align=left> " + "<input class=\"plain\" name=\"thr_thresholdOutOf" + "_" + Integer.toString(icount) + "\" value=\"" + (promTemp.getThreshOutOf() == null ? "0" : promTemp.getThreshOutOf()) + "\" size=\"3\" maxlength=\"5\">" + "</td>");

                                                                            // Pending actions                                        
                                                                            out.println("<td align=left nowrap> ");
                                                                            out.println("<table>");
                                                                            out.println("<tr>" + "<td>" + "<a href=\"javascript:doPost(" + Integer.toString(icount) + ",'addth'," + Integer.toString(promTemp.getRecID()) + ")" + ";\">" + Languages.getString("jsp.admin.tools.editpromotions.tadd", SessionData.getLanguage()) + "</a>" + "</td>" + "</tr>");
                                                                            out.println("</table>");
                                                                            out.println("</td>");
                                                                            out.println("</tr>");

                                                                            vecPromoThresholds.clear();
                                                                        %>                								     
                                                                    </table>								
                                                                </td>
                                                            </tr>														
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            </form>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    <iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
    </iframe>
    <%@ include file="/includes/footer.jsp" %>
    <%if (treeVal != "" && treeResult.equals("null")) {%>
    <script language="javascript">
        createTree();
    </script>
    <%} else if (!treeResult.equals("null")) {%>
    <script language="javascript">
        generateTree();
    </script>
    <%} else {%>
    <script language="javascript">
        createTree();
    </script>
    <%}%>

</html>
