<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page contentType="text/html" import="com.debisys.tools.*,com.debisys.customers.Merchant,com.debisys.tools.paymentsbulk.*,com.debisys.utils.*" %>
<%@page import="javax.mail.internet.InternetAddress,javax.mail.MessagingException,com.debisys.utils.ValidateEmail"%>
  
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
  int section=9;
  int section_page=8;
 %>
  <%@ include file="/includes/security.jsp" %>
  <%@ include file="/includes/header.jsp" %> 


  <html>
  <head><title></title>
    <link href="default.css" type="text/css" rel="stylesheet">
    <script type="text/javascript">
      function validateFile()
      {
        var nameFile = document.getElementById("txt_filename");
        var nameFilesplit = nameFile.value.split("\\");
        var nameF = nameFilesplit[nameFilesplit.length-1];
        nameF= nameF.toLowerCase();
        
        //nameF.startWith("CC")
        //alert(nameF.length+" "+nameF);
        //&& nameF.substring(0,2)=="cc" && nameF.length==19
        if (nameF.indexOf(".txt") > 0 )
        { 
          return true;
        }
        else
        {
          alert("<%=Languages.getString("jsp.admin.tools.general_validate_file",SessionData.getLanguage())%>");
          return false;
        } 
      }   
      function pushValues(){
      
        var bank = document.getElementById("bank");
        var nofirst = document.getElementById("noFirstTime");
        var first = document.getElementById("firstPayment");
        var emailC = document.getElementById("emailCC");
        
        var form_bulk_payment = document.getElementById("form_bulk_payment");
        form_bulk_payment.action="admin/tools/paymentbulk.jsp?bank="+bank.value+"&first="+first.value+"&nofirst="+nofirst.value+"&emailCC="+emailC.value;
        
      } 
    </script>
  </head>
  <body> 

<table border="1" width ="200px" class="main" bgcolor="#FFFFFF" cellpadding="20">
 <tr>
 	
 	<th align="center" style="FONT-SIZE: 8pt" colspan="7">
 		
 		<%=Languages.getString("jsp.includes.menu.paymentbulk",SessionData.getLanguage())%>
 		
 	</th>
 	
 </tr>
 <tr>
  <td align="center" class="formArea2">
    <table>
      <!-- 
      <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule1",SessionData.getLanguage())%></td></tr>
      <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule2",SessionData.getLanguage())%></td></tr>
      <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule3",SessionData.getLanguage())%></td></tr>
      <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule4",SessionData.getLanguage())%></td></tr>
      <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule5",SessionData.getLanguage())%></td></tr>
	  <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule6",SessionData.getLanguage())%></td></tr>
      <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule7",SessionData.getLanguage())%></td></tr>
       -->
      <tr><td class="main"><%=Languages.getString("jsp.admin.tools.general_format_rule0",SessionData.getLanguage())%></td></tr> 
    </table>
  
  </td>
 </tr>
   <%
    String message="";
    String combosTypes="";
    
	String contentType = request.getContentType();
	String file = "";
	String saveFile = "";
	ArrayList<Payment> payments = null;
    PaymentBulk pPaymentBulk = new PaymentBulk();
  
    String strRefId = SessionData.getProperty("ref_id");
    
    StringBuffer emails= new StringBuffer();
    ValidateEmail val = new ValidateEmail();
    
		
    if (request.getMethod().equalsIgnoreCase("post"))
    {
            boolean failEmails=false;
            String emailCC = request.getParameter("emailCC");	
            String[] emailsTarget = emailCC.split("\\;");
            for(String cc : emailsTarget)
		    {
			    try
			    {
			        InternetAddress address = new InternetAddress(cc);
			        val.setStrEmailAddress(cc);
		            boolean validation = val.isEmailValid();
	              
	                if (!validation)
	                {
	                  failEmails=true;
	                  pPaymentBulk.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.iso_fail_email",SessionData.getLanguage())+" : "+cc); 
	                }
	                else
	                {
	                  emails.append(cc+";");
	                }			              
			        
			    }
			    catch(MessagingException mex)
			    {
			        failEmails=true;
			        pPaymentBulk.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.iso_fail_email",SessionData.getLanguage())+" : "+cc);
			    }
			}
		    if (!failEmails)
		    {   
                try
			    {
			                  String remoteHost = request.getRemoteHost();
			                  String localAddr =  request.getLocalAddr();
			                  String contextPath = request.getContextPath();
			                  String sDeploymentType = DebisysConfigListener.getDeploymentType(application);
			    			  String sCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
			                  String mailHost = DebisysConfigListener.getMailHost(application);
			                  String instance = DebisysConfigListener.getInstance(application);
			                  String workingDir  = DebisysConfigListener.getWorkingDir(application);
			                  String languaje =Languages.getCodeLanguage();
			                  	                  										  	
			                  		                  	                  
			                  if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) 
			                  { 
			                    payments = pPaymentBulk.processPaymentBulk(request,workingDir,SessionData,sDeploymentType,sCustomConfigType,strRefId,mailHost,instance,emails.toString(),languaje);
			                  }
			                 
			     }
			     catch (Exception e)
			     {
			        message= e.getMessage();
			        pPaymentBulk.addErrorLog(message);
			     } 		     	            
           }
            
     }
     else
     {
          try 
		  {
		      //find the ISO'S emails in reps table , column email
		      String email = SessionData.getUser().getISOEmail(SessionData);
		      if (email.length()>0)
		      {
		         email = email.replaceAll(";",",");
		         String[] emailsISO = email.split("\\,");
		         
		         for(String cc : emailsISO)
		         {
			           try
			           {
			              InternetAddress address = new InternetAddress(cc);
			              emails.append(cc+";");
			           }
			           catch(MessagingException mex)
			           {
			             //pPaymentBulk.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.iso_fail_email",SessionData.getLanguage())+" : "+cc);
			           }			           
		         }		         
		      }
		      else
		      {
			      pPaymentBulk.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.iso_no_email",SessionData.getLanguage()));
			  }
		  } 
		  catch (Exception ex){
		    pPaymentBulk.addErrorLog(Languages.getString("java.com.debisys.tools.paymentsbulk.iso_email_fail",SessionData.getLanguage()));
		  }		  
	     
    }
     
     ArrayList<Base> arrayBanks = pPaymentBulk.findBanksByIso(strRefId);
     ArrayList<Base> arrayPaymentsTypes = pPaymentBulk.findPaymentsTypes();
     
     %>
       
        <tr>
			<td align="left" colspan="7">
				  
				  <FORM id="form_bulk_payment" action="admin/tools/paymentbulk.jsp" ENCTYPE="multipart/form-data" METHOD="POST">
				    <table>
				     <tr>
				       <td class="main"><%=Languages.getString("jsp.includes.menu.paymentbulk.browse_file",SessionData.getLanguage())%></td>
				     </tr>
				    <tr>				      
				      <td><INPUT type="FILE" name="txt_filename" id="txt_filename" size="50" ></td>
				      <td><INPUT TYPE="SUBMIT" value="<%=Languages.getString("jsp.admin.tools.loadbutton",SessionData.getLanguage())%>"  onclick="pushValues(); return validateFile();"></td>
				    </tr>
				    
				    <tr>
				      <td colspan="2">
				         <table>
				           <tr>
						      <td class="main">
						        <%=Languages.getString("jsp.includes.menu.paymentbulk.email",SessionData.getLanguage())%> 						        
						      </td>
						      <td class="main">
						         <textarea id="emailCC" rows="3" cols="45" ><%=emails.toString()%></textarea>
						         <br/>
						         <font style="font-weight: bold">
						           <%=Languages.getString("jsp.merchant.requestrebuil.text15",SessionData.getLanguage())%>
						         </font>
						         <br/>						         						        						        
						      </td>
						   </tr>
				           <tr>
				              <td  class="main"><%=Languages.getString("jsp.includes.menu.paymentbulk.select_bank",SessionData.getLanguage())%></td>
						      <td colspan="2" >
						        <select id="bank" >
						           <% for(Base bank : arrayBanks)
						           {				              
						              %>
						               <option value="<%=bank.getId()%>"><%=bank.getDescription()%></option>
						              <% 
						           }
						           %>
						        </select>
						      </td>
				           </tr>
				           <tr>
						      <td class="main"><%=Languages.getString("jsp.includes.menu.paymentbulk.first_time",SessionData.getLanguage())%></td>
						      <td class="main" colspan="2">
						        <select id="firstPayment">
						          <% for(Base paymentsTypes : arrayPaymentsTypes)
						             {	
						              int type = Integer.parseInt(paymentsTypes.getId());
						              if ( type < 3)
						              {			              
						              %>
						               <option value="<%=paymentsTypes.getId()%>*<%=paymentsTypes.getDescription()%>"><%=paymentsTypes.getDescription()%></option>
						              <%
						              }  
						             }
						          %>
						        </select>
						      </td>
						    </tr>
						    <tr>
						      <td class="main"><%=Languages.getString("jsp.includes.menu.paymentbulk.no_first_time",SessionData.getLanguage())%></td>
						      <td colspan="2">
						        <select id="noFirstTime" >
						          <% for(Base paymentsTypes : arrayPaymentsTypes)
						             {	
						               int type = Integer.parseInt(paymentsTypes.getId());
						               if ( type > 2 )
						               {			              
						          %>
						              <option value="<%=paymentsTypes.getId()%>*<%=paymentsTypes.getDescription()%>"><%=paymentsTypes.getDescription()%></option>
						          <% 
						               }
						             }
						          %>
						        </select>
						      </td>
						    </tr>				         
				         </table>				       
				      </td>
				    </tr>		     
				   </table>
				  </FORM>   		
		
			</td>
		
		</tr>
		<% if (payments!=null){ %>
        <tr>
          <td class="formArea" with="100%">
            <table  cellpadding="1" cellspacing="1" style="width: 1200px">
              <tr>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.store",SessionData.getLanguage()).toUpperCase()%></td>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.control_no",SessionData.getLanguage()).toUpperCase()%></td>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.merchant",SessionData.getLanguage()).toUpperCase()%></td>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.ticket",SessionData.getLanguage()).toUpperCase()%></td>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.payment_date",SessionData.getLanguage()).toUpperCase()%></td>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.amount",SessionData.getLanguage()).toUpperCase()%></td>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.payment_type",SessionData.getLanguage()).toUpperCase()%></td>
                <td class="rowhead2"><%=Languages.getString("jsp.includes.menu.paymentbulk.status",SessionData.getLanguage()).toUpperCase()%></td>
              </tr>              
              <%int i=1;
                for(Payment pay : payments){%>
                  <tr class="row<%=i%>">
                    <td class="main"><%=pay.getNoTienda()%></td>
                    <td class="main"><%=pay.getNoMembresia()%></td>
                    <td class="main"><%=pay.getMerchantId()%></td>
                    <td class="main"><%=pay.getTicket().substring(0,pay.getTicket().length()-4)%></td>
                    <td class="main"><%=pay.getDatePayment().toString()%></td>
                    <td class="main"><%=pay.getUnidadesCompra()%></td>
                    <td class="main"><%=pay.getDescPaymentType()%></td>
                    <td class="main">
                      <table class="formArea">
                        <tr><td><%=pay.getObs()%></td></tr>                      
                      <%for(String error : pay.getErrorLog()){
                         %>
                          <tr><td><%=error%></td></tr>
                         <% 
                        } 
                        if (pay.getErrorInsertDescription()!=null){
                          %>
                            <tr><td><%=pay.getErrorInsertDescription()%></td></tr>  
                          <% 
                        }
                        %>
                         
                      </table>
                    </td>
                  </tr>
              <%
               if (i==1)
                i=2;
               else
                i=1;
               }%>            
            </table>           
          </td>        
        </tr>
        <%}
        if (pPaymentBulk.getErrorsProcess().size()>0)
        {
          int i=0;
          for(String desc : pPaymentBulk.getErrorsProcess())
          {
           %>
            <tr class="row<%=i%>"><td class="main"><%=desc%></td></tr>            
           <%
           if (i==1)
             i=2;
           else
             i=1;
          }          
        }
        %>
  
</table>
			  
  </body>
  </html>
  

