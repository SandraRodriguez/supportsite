<%@ page import="javax.mail.Session"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="PhoneRewards" class="com.debisys.tools.PhoneRewards" scope="request"/>
<%
  	int section=11;
  	int section_page=7;
  	int result = -1;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
	if(request.getParameter("type") != null)
	{
		if(request.getParameter("type").equals("UPDATE"))
		{
			PhoneRewards.updatePercentage(request,SessionData);
		}
		else if(request.getParameter("type").equals("Reset"))
		{
			PhoneRewards.resetaccountid(request,SessionData);
		}
	}
%>

<SCRIPT LANGUAGE="JavaScript">
			function disableEnterKey(e)
			{
			     var key;
			
			     if(window.event)
			          key = window.event.keyCode;     //IE
			     else
			          key = e.which;     //firefox
			
			     if(key == 13)
			          return false;
			     else
			          return true;
			}

            function resetphonenumber()
            {
            	 if(document.getElementById('resetid').value.length == 0 || document.getElementById('confid').value.length == 0)
						{
							alert('<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg6",SessionData.getLanguage())%>');
							return;
						}
		         else if( document.getElementById('resetid').value != document.getElementById('confid').value )
						{
							alert('<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg7",SessionData.getLanguage())%>');
							return;
						}
		         else if( !IsNumeric(document.getElementById('resetid').value) && !IsNumeric(document.getElementById('confid').value) )
						{
							alert('<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg8",SessionData.getLanguage())%>');
							var textBox = document.getElementById('resetid');
	 						textBox.value = "";
							var textBox = document.getElementById('confid');
	 						textBox.value = "";
							return;
						}
				
				if(document.getElementById('resetid').value.length < 8){
					var textBox = document.getElementById('resetid');
	 						textBox.value = "1284" + textBox.value;
					var textBox = document.getElementById('confid');
	 						textBox.value = "1284" + textBox.value;
	 						}
	 			else if(document.getElementById('resetid').value.length > 7 && document.getElementById('resetid').value.length < 11){
					var textBox = document.getElementById('resetid');
	 						textBox.value = "1" + textBox.value;	
	 						var textBox = document.getElementById('confid');
	 						textBox.value = "1" + textBox.value;
	 			}
				//check pass, confirm message	
				var message="<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsConfirm",SessionData.getLanguage())%>"+ document.getElementById('resetid').value +" <%=Languages.getString("jsp.admin.tools.index.PhoneRewardsConfirm2",SessionData.getLanguage())%>" ;
				if(confirm(message))
				{
					document.PhoneRewardsResetForm.submit();
				}
            }  
             function savePercentage()
            {
           		 if(document.getElementById('percentage').value.length == 0)
						{
							alert('<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg9",SessionData.getLanguage())%>');
							return;
						}
		         else if( !IsNumeric(document.getElementById('percentage').value) )
						{
							alert('<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg10",SessionData.getLanguage())%>');
							var textBox = document.getElementById('percentage');
	 						textBox.value = "";
							return;
						}
		         else if( verifyrange(document.getElementById('percentage').value) )
						{
							alert('<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg11",SessionData.getLanguage())%>');
							var textBox = document.getElementById('percentage');
	 						textBox.value = "";
	 						return;
						}
				
				document.PhoneRewardsForm.submit();
			
            }  
            
            function IsNumeric(sText)
			{
			   var ValidChars = "0123456789.";
			   var IsNumber=true;
			   var Char;
			   for (i = 0; i < sText.length && IsNumber == true; i++) 
			      { 
			      Char = sText.charAt(i); 
			      if (ValidChars.indexOf(Char) == -1) 
			         {
			         IsNumber = false;
			         }
			      }
			   return IsNumber;
			   
			   }
			   
            function verifyrange(sText){
				if (sText<1 || sText >100)
				{
					return true;
				}
				return false
				}

			   
			     
        </SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle"><b><%=Languages.getString("jsp.admin.tools.bonusphonerewards.title",SessionData.getLanguage()).toUpperCase()%></b></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
 </tr>
 <tr>
 	<td colspan="3">
   	<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
 	    <tr>
 		      <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
  		    <td align="center" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main">
     			<form name="PhoneRewardsForm" action="admin/tools/bonusPhoneRewardsPromo.jsp">
				<input type="hidden" name="type" value="UPDATE" />
                 <table class="main" >
                 <tr>
                  <td><%=Languages.getString("jsp.admin.tools.bonusphonerewards.percentage",SessionData.getLanguage())%>:</td>
                  <td><input name="percentage" id="percentage" type="text" size='30' onKeyPress="return disableEnterKey(event)" value ="<%=PhoneRewards.getPercentage()%>" /> </td>
                 </tr>
                 <tr>
                 <td><INPUT TYPE="button" id="percentagebutton" VALUE="<%=Languages.getString("jsp.admin.tools.bonusphonerewards.savebutton",SessionData.getLanguage())%>" ONCLICK="savePercentage()">
                 </td></tr>
                 </table>
                 </form>
     				    </td>
     		        <td width="18">&nbsp;</td>
        			</tr>
        			   <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main">
     			<form name="PhoneRewardsResetForm" action="admin/tools/bonusPhoneRewardsPromo.jsp">
				<input type="hidden" name="type" value="<%=Languages.getString("jsp.admin.tools.bonusphonerewards.resetbutton",SessionData.getLanguage())%>" />
                 <table class="main" >
                 <tr>
                  <td><%=Languages.getString("jsp.admin.tools.bonusphonerewards.reset",SessionData.getLanguage())%>:</td>
                  <td><input name="resetid" id="resetid" type="text" size='30' value="" /> </td>
                 </tr>
                 <tr>
                  <td><%=Languages.getString("jsp.admin.tools.bonusphonerewards.resetconfirm",SessionData.getLanguage())%>:</td>
                  <td><input name="confid" id="confid" type="text" size='30' value="" /> </td>
                 </tr>
                 <tr>
                 <td><INPUT TYPE="button" id="resetbutton" VALUE="Reset" ONCLICK="resetphonenumber()">
                 </td></tr>
                 </table>
                 </form>
     				    </td>
     		        <td width="18">&nbsp;</td>
        			</tr>
        			  <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main" style="color:red">
 <% if( PhoneRewards.getPhoneRewardsErrorCode()!=null ){%>
 <% if( PhoneRewards.getPhoneRewardsErrorCode().equals("0") ){%>
 <%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg1",SessionData.getLanguage())%><%=PhoneRewards.getPhoneRewardsAmount()%> <%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg2",SessionData.getLanguage())%>
 <% } else  if(PhoneRewards.getPhoneRewardsErrorCode().equals("1") ){%>
 						<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg3",SessionData.getLanguage())%> 
 <% } else  if(PhoneRewards.getPhoneRewardsErrorCode().equals("2") ){%>
 						<%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg4",SessionData.getLanguage())%> 
<% }} %> 							
 <% if( PhoneRewards.getPercentageSet()!=null ){%>
  <%=PhoneRewards.getPercentageSet()%>% <%=Languages.getString("jsp.admin.tools.index.PhoneRewardsMsg5",SessionData.getLanguage())%> 
 <% } %> 	
     				    </td>
     		        <td width="18">&nbsp;</td>
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>