<%@ page language="java" import="java.util.*,
                 com.debisys.languages.Languages"
                 pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to set the bonuses and topup thresholds
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="PhoneRewards" class="com.debisys.tools.PhoneRewards" scope="request"/>
<%
  	int section=11;
  	int section_page=6;
%>
<%@ include file="/includes/security.jsp" %>
 
<%
	Vector vecSearchResults = PhoneRewards.getProviders();
%>

<script>  
	function submitform()
	{
	  document.carrierSelectForm.submit();
	}
	
	function showID(id)
	{
		alert(id);
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" background="images/top_blue.gif">
	<tr>
		<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.providerID",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
		<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.provider",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	              				
	</tr>
	<form name=carrierSelectForm action="admin/tools/bonusPhoneRewardsPromo.jsp">
	<input type=hidden name=type value=UPDATE />
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		out.println(
		"<tr class=row" + intEvenOdd + ">" +
         	"<td align=center>" + vecTemp.get(0) + "</td>" +
         	"<td>" + vecTemp.get(1) + "</td>" +
		"</tr>"
		);
          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            							
	</form>
</table>