<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to add a bonus and topup threshold
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="PhoneRewards" class="com.debisys.tools.PhoneRewards" scope="request"/>
<%
  	int section=11;
  	int section_page=5;
	Vector vecTemp = null;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 

<table width="99%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td>	
<script>  
	function submitform()
	{
		// Check that everything is OK to be entered. No missing fields.
		if(document.getElementById('newProvider') == null)
		{
			alert('Please select a provider.');
			return;
		}
		else if(document.getElementById('newProvider').value.length == 0)
		{
			alert('Please select a provider.');
			return;
		}
		
  		document.PhoneRewardsSMSForm.submit();
	}
</script>
<%
	if(request.getParameter("type") != null)
	{
		if(request.getParameter("type").equals("UPDATE"))
		{
			String oldProvider = request.getParameter("oldProvider");
			Vector vecSearchResults = PhoneRewards.getBonusThresholdSMS(oldProvider, SessionData);
			Iterator it = vecSearchResults.iterator();
			vecTemp = (Vector) it.next();
%>
			<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
				<tr>
					<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
					<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.updateSMS.title",SessionData.getLanguage()).toUpperCase()%></td>
					<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
				</tr>
				<tr>
					<td colspan="3"  bgcolor="#FFFFFF">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
							<tr>
								<td class=formArea2>
									<table align=center>
										<tr>
											<td colspan="3">					
												<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
													<tr>
														<td class="main">
															<br>
															<table width="100%" cellspacing="1" cellpadding="2">
																<form name=PhoneRewardsSMSForm action="admin/tools/bonusPhoneRewardsPromoSMS.jsp">
																<input type=hidden name=type value=UPDATE />
																<tr>
																	<td class=rowhead2 valign=top><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.newProvider",SessionData.getLanguage()).toUpperCase()%></td>
<%
			if(request.getParameter("oldProvider") != null && request.getParameter("newProvider") != null)
			{
				out.println(
"																	<td><table><tr><td align=left>" + request.getParameter("newProvider") + "</td>" + 
"																	<td><a href=\"admin/tools/PRselectProvider.jsp?type=UPDATE&oldProvider=" + request.getParameter("oldProvider") + "\">(Change)</a></td></tr></table></td>" + 
"																	<input type=hidden id=newProvider name=newProvider value=" + request.getParameter("newProvider") + " />" + 
"																	<input type=hidden id=oldProvider name=oldProvider value=" + request.getParameter("oldProvider") + " />"
				);			
			}
			else
			{
				out.println(
"																	<td><table><tr><td align=left>" + vecTemp.get(0) + "</td>" + 
"																	<td><a href=\"admin/tools/PRselectProvider.jsp?type=UPDATE&oldProvider=" + vecTemp.get(0) + "\">(Change)</a></td></tr></table>" + 
"																	<input type=hidden id=newProvider name=newProvider value=" + vecTemp.get(0) + " />" + 
"																	<input type=hidden id=oldProvider name=oldProvider value=" + vecTemp.get(0) + " />"
				);
			}
%>														
																</tr>
																<tr>				
																</tr>	
																<tr>													
																	<td class=rowhead2 valign=top><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.newMessage",SessionData.getLanguage()).toUpperCase()%></td>  
																	<td align=left>
<%
			out.println(
"																		<textarea id=msg name=msg rows=5 cols=40>" + vecTemp.get(1) + "</textarea>"
			);
%>														
																	</td>
																</tr>
																</form>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table align=center>
										<tr>
											<td>
												<table align=center>
													<tr>
														<td>
															<input type="button" name="button" value="<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.update_bonusThresholdSMS",SessionData.getLanguage())%>" onClick=submitform();>
															<form action="admin/tools/bonusPhoneRewardsPromoSMS.jsp">
																<input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.cancelInsert",SessionData.getLanguage())%>">
															</form>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
<%			
		}
		else if(request.getParameter("type").equals("INSERT"))
		{
%>
			<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
				<tr>
					<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
					<td background="images/top_blue.gif" width="100%" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.PhoneRewardsToolsSMS.addSMS.title",SessionData.getLanguage())%></td>
					<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
				</tr>
				<tr>
					<td colspan="3"  bgcolor="#FFFFFF">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
							<tr>
								<td class=formArea>
									<table align=center>
										<tr>
											<td colspan="3">					
												<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
													<tr>
														<td class="main">
															<br>
															<table width="100%" cellspacing="1" cellpadding="2">
																<form name=PhoneRewardsSMSForm action="admin/tools/bonusPhoneRewardsPromoSMS.jsp">
																<input type=hidden name=type value=INSERT />
																<tr>
																	<td class=rowhead2 valign=top><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.newProvider",SessionData.getLanguage())%></td>
<%
			if(request.getParameter("newProvider") == null)
			{
%>
																	<td>																	
																		<a href="admin/tools/PRselectProvider.jsp?type=INSERT">Select Provider</a>
																	</td>
<%
			}
			else if(request.getParameter("newProvider") != null)
			{
				out.println(
"																	<td><table><tr><td align=left>" + request.getParameter("newProvider") + "</td>" + 
"																	<td><a href=\"admin/tools/PRselectProvider.jsp?type=INSERT&newProvider=" + request.getParameter("newProvider") + "\">(Change)</a></td></tr></table>" + 
"																	<input type=hidden id=newProvider name=newProvider value=" + request.getParameter("newProvider") + " />"
				);			
			}
%>			
																</tr>	
																<tr>													
																	<td class=rowhead2 valign=top><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.newMessage",SessionData.getLanguage())%></td>  
																	<td align=left>
																		<textarea id=msg name=msg rows=5 cols=40></textarea>
																	</td>
																</tr>
																</form>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table align=center>
										<tr>
											<td>
												<table align=center>
													<tr>
														<td>
															<input type="button" name="button" value="<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.add_bonusThresholdSMS",SessionData.getLanguage())%>" onClick="submitform();">
															<form action="admin/tools/bonusPhoneRewardsPromoSMS.jsp">
																<input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.cancelInsert",SessionData.getLanguage())%>">
															</form>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
<%
		}
	}
%>
<%@ include file="/includes/footer.jsp" %>