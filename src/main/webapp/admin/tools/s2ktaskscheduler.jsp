<%@ page language="java" import="java.util.*, com.debisys.languages.*,
						com.debisys.customers.Rep,com.debisys.tools.s2ktaskhandler" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
  	int section=9;
  	int section_page=14;
  	int result = -1;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
<% 
	int deleteresponse = -1;
	String strRefId = SessionData.getProperty("ref_id");
	String isoId = Rep.getIsoID(strRefId, strAccessLevel);
	String action = "";
	boolean deletefailed = false;
	if ( request.getParameter("action") != null && request.getParameter("jobid")!=null )
{
action = request.getParameter("action");
if(action.equals("delete") )
{
	 String jobid =request.getParameter("jobid"); 
	 deleteresponse = new s2ktaskhandler().deletetask(jobid);
	 if(deleteresponse!=0){
	 deletefailed = true;
	 }
	 else
	 	new s2ktaskhandler().deletemerchants(jobid);
}
}
%>

<![if !IE]> 
<link rel="stylesheet" type="text/css" href="includes/s2k/not-ie-s2ktaskhandler.css" />
<![endif]> 
<!--[if IE]> 
<link rel="stylesheet" type="text/css" href="includes/s2k/s2ktaskhandler.css" />
<![endif]--> 


<script type="text/javascript" src="includes/s2k/s2ktaskscheduler.js" ></script>
<script type="text/javascript" >

function getstatus() {
 xmlHttp=GetXmlHttpObject();
			if (xmlHttp===null)
			 {
				 alert ("Browser does not support HTTP Request");
				 return ;
			 }
			var temp = document.getElementById("filtercombobox").options[document.getElementById("filtercombobox").selectedIndex];
			var url="includes/s2k/s2kattfunctions.jsp";
				url = url + "?action=getstatus&v=" +
				Math.random()*Math.random() +"&t=" + 
				Math.random()*Math.random()
				+ "&isoid="+ "<%=isoId%>" +
			    "&filter=" + temp.value;
			xmlHttp.onreadystatechange=updatestatus ;
			xmlHttp.open("GET",url,true);
			xmlHttp.send(null);
 }
var alertTimerId = 0;
function loading(){
alertTimerId = setInterval ( 'getstatus()', '<%=application.getAttribute("System2000TaskListRefresh").toString()%>' );
}


 function updatestatus ()
{
 if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
 { 
  try {  

	 var response =  xmlHttp.responseText ; 
	 
	 if(response.indexOf("ERROR")>=0){
				alert("Unknown database error, Please contact customer support.");
	 }else if(response.length > 0)
	 	{
	 		 var item = response.split(';');
             var table = document.getElementById("dataTable1");  
             var rowCount = table.rows.length;  
             for(var i=rowCount; i>1; i--) {  
                 var row = table.rows[i-1];  
				var jj = row.cells[0];
				 var dis = 'none';     
			for(var f=0; f<item.length ; f++){
			    var javascriptString = new String(item[f]);
				var sub = javascriptString.split(',');	
				
				if( (jj.textContent || jj.innerText) 
				 === sub[0].toString() ){
					dis = 'block';
					dis = 'table-row';
					row.cells[2].innerText = choose(sub[1]);
				}
			}
			row.style.display = dis;
                							}  
          }
      }catch(e) {  
      alert(e);}           
 }  
}
function choose(dbvalue){
	var temp= "<%=Languages.getString("jsp.admin.tools.s2k.default",SessionData.getLanguage())%>";
	if(dbvalue==0){
		temp = "<%=Languages.getString("jsp.admin.tools.s2k.response0",SessionData.getLanguage())%>";
	}
	else if(dbvalue==1){
		temp = "<%=Languages.getString("jsp.admin.tools.s2k.response1",SessionData.getLanguage())%>";
	} 
	else if(dbvalue==2){
		temp = "<%=Languages.getString("jsp.admin.tools.s2k.response2",SessionData.getLanguage())%>";
	} 
	else if(dbvalue==3){
		temp = "<%=Languages.getString("jsp.admin.tools.s2k.response3",SessionData.getLanguage())%>";
	} 
	else if(dbvalue==4){
		temp = "<%=Languages.getString("jsp.admin.tools.s2k.response4",SessionData.getLanguage())%>";
	}
	
	return temp;	
}			
// for Mozilla browsers

if (document.addEventListener) {
  document.addEventListener("DOMContentLoaded", loading, false);
}



										
</script>
<script defer src="includes/s2k/ie_onload.js" type="text/javascript"></script>

<%
		Vector vecSearchResults = new s2ktaskhandler().gets2klist(isoId,strAccessLevel);
%>
<table   border="0" cellpadding="0" cellspacing="0" width="805"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.s2k.task.title1",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width=100% align=center>
				<tr>
					<td class=formArea2>
						<table>
							<tr>
								<td colspan="4">
								<noscript>
								   <%
								   out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>"); 
								   out.println("<li>" + Languages.getString("jsp.s2k.errornoscript",SessionData.getLanguage())); 
									  out.println("</font></td></tr></table>"); 
								   %>
								</noscript>
								<% if(deletefailed){%>
								<font face="Arial, Helvetica, sans-serif"><% 
								  out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>"); 
								  if(deleteresponse==-2)
								  	out.println("<li>" + Languages.getString("jsp.s2k.errordelete",SessionData.getLanguage())); 
								  else
								  	out.println("<li>" + Languages.getString("jsp.s2k.dberrordelete",SessionData.getLanguage())); 
								  	
								  out.println("</font></td></tr></table>"); 
								%></font>
								<%} %>					
									<table border="0" cellpadding="2" cellspacing="0" width=100% align="center">
										<tr>
											<td rowspan="2" valign="middle" width="30%" align="center" class="main">
											<%=Languages.getString("jsp.admin.tools.s2k.action",SessionData.getLanguage())%><br/>	
									<input type="hidden" id="accesslevel" name="accesslevel" value="<%=strAccessLevel%>"/>	
									<input type="hidden" id="isoid" name="isoid" value="<%=isoId%>"/>					
									<input id="addbutton"  name="addbutton" type="button"  onclick="add();" value="<%=Languages.getString("jsp.admin.tools.s2k.add",SessionData.getLanguage())%>"/><br/>
									<input disabled id="editbutton"  name="editbutton" type="button" onclick="edit();" value="<%=Languages.getString("jsp.admin.tools.s2k.edit",SessionData.getLanguage())%>"/><br/>
									<input disabled id="deletebutton" name="deletebutton" type="button" onclick="del();" value="<%=Languages.getString("jsp.admin.tools.s2k.delete",SessionData.getLanguage())%>"/>
											</td>
											<td class="main"><br>
											<table ><tr>
													<td class="main" align="left"><%=Languages.getString("jsp.admin.tools.s2k.task.title2",SessionData.getLanguage())%></td>
													<td align="right" style="float:right;" ><select style="float:right;" id="filtercombobox" name="filtercombobox" onchange="getstatus();" >
													<option value="<%=Languages.getString("jsp.admin.tools.s2k.response6",SessionData.getLanguage()) %>" selected>
													<%=Languages.getString("jsp.admin.tools.s2k.response5",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response6",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response7",SessionData.getLanguage()) %>
													</option>
													<option value="<%=Languages.getString("jsp.admin.tools.s2k.response0",SessionData.getLanguage()) %>">
													<%=Languages.getString("jsp.admin.tools.s2k.response5",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response0",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response7",SessionData.getLanguage()) %>
													</option>
													<option value="<%=Languages.getString("jsp.admin.tools.s2k.response1",SessionData.getLanguage()) %>">
													<%=Languages.getString("jsp.admin.tools.s2k.response5",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response1",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response7",SessionData.getLanguage()) %>
													</option>
													<option value="<%=Languages.getString("jsp.admin.tools.s2k.response2",SessionData.getLanguage()) %>">
													<%=Languages.getString("jsp.admin.tools.s2k.response5",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response2",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response7",SessionData.getLanguage()) %>
													</option>
													<option value="<%=Languages.getString("jsp.admin.tools.s2k.response3",SessionData.getLanguage()) %>">
													<%=Languages.getString("jsp.admin.tools.s2k.response5",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response2",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response7",SessionData.getLanguage()) %>  <%=Languages.getString("jsp.admin.tools.s2k.response32",SessionData.getLanguage()) %>
													</option>
													<option value="<%=Languages.getString("jsp.admin.tools.s2k.response4",SessionData.getLanguage()) %>">
													<%=Languages.getString("jsp.admin.tools.s2k.response5",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response4",SessionData.getLanguage()) %> <%=Languages.getString("jsp.admin.tools.s2k.response7",SessionData.getLanguage()) %>
													</option>
													</select></td></tr>
											</table>
													<div style="height:302px; overflow-y: auto;">
												<table  id="dataTable1" width="400" style = "border-collapse:collapse; margin:0; padding:0; border:none;">
													
													<tr>
													    <th class=rowhead2 width="40%"><%=Languages.getString("jsp.admin.tools.s2k.task.title4",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
														<th class=rowhead2 width="40%"><%=Languages.getString("jsp.admin.tools.s2k.task.title6",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>     
														<th class=rowhead2 width="20%"><%=Languages.getString("jsp.admin.tools.s2k.task.title5",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>   
													</tr>

<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();%><%
															String status;
															int dbvalue = Integer.parseInt(vecTemp.get(2).toString());
															String dvbalue2 ="";
															if(dbvalue==0){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response0",SessionData.getLanguage());
															}
															else if(dbvalue==1){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response1",SessionData.getLanguage());
															} 
															else if(dbvalue==2){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response2",SessionData.getLanguage());
															} 
															else if(dbvalue==3){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response3",SessionData.getLanguage());
															} 
															else if(dbvalue==4){
															dvbalue2 = Languages.getString("jsp.admin.tools.s2k.response4",SessionData.getLanguage());
															}
															else
																dvbalue2 = Languages.getString("jsp.admin.tools.s2k.default",SessionData.getLanguage());
															 %>
														    <tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"   class="row<%=intEvenOdd%>" style="display:block;display:table-row;" onclick="selected(this,'<%=intEvenOdd%>')" onmouseover="if(really_over(this)) {change(this,'<%=intEvenOdd%>');}" onmouseout="if(really_out(this)) {back(this,'<%=intEvenOdd%>')}" >
															<td id ="id" value ="<%=vecTemp.get(0).toString()%>" valign=top><%=vecTemp.get(0).toString().trim()%></td>
															<td id ="date" valign=top><%=vecTemp.get(1)%></td>
															<td id ="status" value="<%=String.valueOf(dbvalue)%>" valign=top><%=dvbalue2%></td>
															</tr>
<%          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            				
									</table></div>
								</td>
							</tr>
							<tr>
							<td class="main">
							<%=Languages.getString("jsp.admin.tools.s2k.task.title3",SessionData.getLanguage())%><br/>
							<div style="height:150px; overflow-y: auto;">
												<table  id="dataTable2"  cellspacing="1" cellpadding="2" width=100% style = "table-layout: fixed;">
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  >
														<th class=rowhead2 width="50%"><%=Languages.getString("jsp.admin.tools.s2k.task.title7",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
														<th class=rowhead2 width="50%"><%=Languages.getString("jsp.admin.tools.s2k.task.title8",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>    
													</tr>	
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  class="row1"><td  id="startdate" ><%=Languages.getString("jsp.admin.tools.s2k.att1",SessionData.getLanguage())%></td><td></td></tr>
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  class="row2"><td  id="edndate"><%=Languages.getString("jsp.admin.tools.s2k.att2",SessionData.getLanguage())%></td><td></td></tr>
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  class="row1"><td  id="companycode"><%=Languages.getString("jsp.admin.tools.s2k.addedit.companycode",SessionData.getLanguage())%></td><td></td></tr>
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  class="row2"><td  id="location"><%=Languages.getString("jsp.admin.tools.s2k.addedit.location",SessionData.getLanguage())%></td><td></td></tr>
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  class="row1"><td  id="shipto"><%=Languages.getString("jsp.admin.tools.s2k.addedit.shipto",SessionData.getLanguage())%></td><td></td></tr>
													
									</table></div>
							</td>
							</tr>
							</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>