<%-- 
    Document   : BanksDepositTypes
    Created on : Jan 25, 2017, 5:00:51 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.depositConfiguration.DepositTypesConf"%>
<%@page import="com.debisys.reports.banks.Banks"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesVo"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesConf"%>
<%@page import="com.debisys.tools.depositConfiguration.DepositTypesVo"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script language="JavaScript" src="/support/includes/primeui/primeui-2.0-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/banksDepositTypes.js"></script>


<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/primeui/primeui-2.0-min.js" language="JavaScript" charset="utf-8"></script>

<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
    #spanWarningMessages, #spanWarningMessagesProducts{
        font-size: 17px;
        color: green;
    }

    .windowsFloatCharging {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }


</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#div_form').css("display", "none");
    });
    
    function verifySelectAllDepositTypes(){
        var depositTypesSelected = $("#selectDepositTypes").val();
        if(depositTypesSelected == '0'){
            $('#selectDepositTypes option').prop('selected', true); // Select All elements to send
            $("#selectDepositTypes option:first").prop('selected', false);
        }
        
        
    }
</script>


<%
    String actionEvent = "insertBanksDeposit";
    String msg = request.getParameter("msg");
    msg = (msg == null) ? "" : msg;

    String action = request.getParameter("action");
    String bankId = request.getParameter("bankId");

    BanksDepositTypesVo dtEdit = null;

    if (action != null && action.trim().equals("edit")) {
        dtEdit = BanksDepositTypesConf.getBanksDepositTypesVoByBankId(bankId);
        actionEvent = "editDeposit";
    }
    else if (action != null && action.trim().equals("insertBanksDeposit")) {
        BanksDepositTypesVo banksDepositTypes = new BanksDepositTypesVo();
        banksDepositTypes.setBanksId(request.getParameter("selectBanks"));
        String[] selectDepositTypes = request.getParameterValues("selectDepositTypes");
        BanksDepositTypesConf.insertBanksDepositTypes(banksDepositTypes, selectDepositTypes);
    }
    else if (action != null && action.trim().equals("editDeposit")) {
        BanksDepositTypesVo banksDepositTypes = new BanksDepositTypesVo();
        banksDepositTypes.setId(request.getParameter("idBanksDepositType"));
        banksDepositTypes.setBanksId(request.getParameter("selectBanks"));
        String[] selectDepositTypes = request.getParameterValues("selectDepositTypes");
        BanksDepositTypesConf.updateBanksDepositsTypes(banksDepositTypes, selectDepositTypes);
    }
    else if (action != null && action.trim().equals("removeBanksDepositType")) {
        //TODO: verificar que no haya una dependencia antes de eliminar
        BanksDepositTypesConf.deleteBanksDepositTypesByBankId(bankId);
    }

    List<BanksDepositTypesVo> banksDepositTypesVoList = BanksDepositTypesConf.getBanksDepositTypesListByBanks();
    

%>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="windowsFloatCharging">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>    

<table border="0" cellpadding="0" cellspacing="0" width="1200" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <%=Languages.getString("jsp.admin.tools.banksDepositTypes.title", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">

            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr align="center">
                    <td>
                        <span id="spanWarningMessages"><%=msg%></span>
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        <div id="div_form_banks" style="<%=(action != null && action.equals("edit") ? "" : "display:none;")%>width:60%;height:100%;">
                            <fieldset class="field_set">
                                <legend></legend>
                                <form action="admin/tools/depositTypes/banksDepositTypes.jsp" method="POST" onsubmit="return validateForm('<%=Languages.getString("jsp.admin.tools.banksDepositTypes.duplicateMessage", SessionData.getLanguage())%>');">
                                    <table width="100%" border="0" cellspacing="1" align="center" height="40">
                                        <tr>
                                            <td><%=Languages.getString("jsp.admin.tools.banksDepositTypes.banksList", SessionData.getLanguage())%></td>
                                            <td><%=Languages.getString("jsp.admin.tools.banksDepositTypes.depositTypes", SessionData.getLanguage())%></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select name="selectBanks" id = "selectBanks">
                                                    <option value="-1"></option>
                                                    <%for (Banks bank : Banks.findBanks()) {%>  
                                                    <option value="<%=bank.getId()%>" <%=(dtEdit != null && dtEdit.getBanksId().equalsIgnoreCase(""+bank.getId()))?"selected":""%> > <%=bank.getName()%></option>
                                                    <%}%>
                                                </select>

                                            </td>
                                            <td><select id="selectDepositTypes" name="selectDepositTypes" size="8" multiple="" onchange="verifySelectAllDepositTypes()">
                                                    <option value="0">All</option>
                                                    <%for (DepositTypesVo depositType : DepositTypesConf.getDepositTypesList()) {%>  
                                                    <option value="<%=depositType.getId()%>" <%=(dtEdit != null && dtEdit.existDepositTypeId(depositType.getId()))?"selected":""%>>
                                                        <%=(SessionData.getLanguage().equalsIgnoreCase("0") || SessionData.getLanguage().equalsIgnoreCase("english"))?depositType.getDescriptionEnglish():depositType.getDescriptionSpanish()%>
                                                    </option>
                                                    <%}%>
                                                </select>
                                                (<%=Languages.getString("jsp.admin.tools.banksDepositTypes.controlClickMessage", SessionData.getLanguage())%>)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="hidden" name="action" id="action" value="<%=actionEvent%>" />
                                                <input type="hidden" name="idBanksDepositType" id="idBanksDepositType" value="<%=bankId%>" />
                                                <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%>" onclick="hideForm();">
                                                <input type="submit" value="<%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%>" />
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </fieldset>
                        </div>
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.banksDepositTypes.newBanksdepositTypes", SessionData.getLanguage())%>" onclick="newDepositType();">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="80%" border="0" cellspacing="1" align="center" height="40">
                            <tr class="rowhead2">
                                <td><%=Languages.getString("jsp.admin.tools.banksDepositTypes.banksList", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.banksDepositTypes.depositTypes", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.genericLabel.options", SessionData.getLanguage())%></td>
                            </tr>
                            <%
                                int index = 0;
                                for (BanksDepositTypesVo bankDepositType : banksDepositTypesVoList) {
                                    index++;
                            %>
                            <tr class="row<%=(index % 2 == 0) ? "1" : "2"%>">
                                <td><%=bankDepositType.getBankName()%></td>
                                <td><%
                                    List<DepositTypesVo> namesList = bankDepositType.getDepositTypeList();
                                    for (DepositTypesVo name : namesList) {
                                        if(SessionData.getLanguage().equalsIgnoreCase("spanish")){
                                            out.print(name.getDescriptionSpanish() + ", ");
                                        }
                                        else{
                                            out.print(name.getDescriptionEnglish() + ", ");
                                        }   
                                    }
                                %>
                                    
                                </td>
                                <td align="center"><input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.edit", SessionData.getLanguage())%>" onclick="editBanksDepositType('<%=bankDepositType.getBanksId()%>','<%=Languages.getString("jsp.admin.tools.banksDepositTypes.canNotEdit", SessionData.getLanguage())%>');">
                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%>" onclick="deleteBanksDepositType('<%=bankDepositType.getBanksId()%>', '<%=Languages.getString("jsp.admin.genericLabel.removeItemQuestion", SessionData.getLanguage())%> (<%=bankDepositType.getBankName()%>)','<%=Languages.getString("jsp.admin.genericLabel.canNotDelete", SessionData.getLanguage())%>');"></td>
                            </tr>
                            <%}%>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

<script LANGUAGE="JavaScript">


</script>

<%@ include file="/includes/footer.jsp" %>