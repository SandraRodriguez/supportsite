<%-- 
    Document   : depositTypesList
    Created on : Jan 24, 2017, 4:39:05 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.depositConfiguration.DepositExternalTypeVo"%>
<%@page import="com.debisys.tools.depositConfiguration.DepositTypesVo"%>
<%@page import="com.debisys.tools.depositConfiguration.DepositTypesConf"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script language="JavaScript" src="/support/includes/primeui/primeui-2.0-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/depositTypes.js"></script>
<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
    #spanWarningMessages, #spanWarningMessagesProducts{
        font-size: 17px;
        color: green;
    }
    #spanErrorMessages{
        font-size: 17px;
        color: red;
    }
    .windowsFloatCharging {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }


</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#div_form').css("display", "none");
    });
</script>


<%
    String cssSpanMessages = "spanWarningMessages";
    
    String actionEvent = "insertDeposit";
    String msg = request.getParameter("msg");
    msg = (msg == null) ? "" : msg;

    String action = request.getParameter("action");
    String id = request.getParameter("id");

    DepositTypesVo dtEdit = null;

    if (action != null && action.trim().equals("edit")) {
        dtEdit = DepositTypesConf.getDepositTypesById(id);
        actionEvent = "editDeposit";
    }
    else if (action != null && action.trim().equals("insertDeposit") && request.getParameter("depositCode").length()>0) {
        DepositTypesVo depositTypesVo = new DepositTypesVo();        
        depositTypesVo.setDescriptionEnglish(request.getParameter("descriptionEng"));
        depositTypesVo.setDescriptionSpanish(request.getParameter("descriptionSpa"));
        depositTypesVo.setExternalCode(request.getParameter("externalCode"));
        depositTypesVo.setDepositCode(request.getParameter("depositCode"));        
        if ( request.getParameter("transFerAccount")!=null && request.getParameter("transFerAccount").equals("on") ){
            depositTypesVo.setHasTransferAccount("1");
        } else{
            depositTypesVo.setHasTransferAccount("0");
        }
        //boolean duplicate = DepositTypesConf.isDuplicateDepositType(depositTypesVo.getDescriptionEnglish(), 
        //                                                            depositTypesVo.getDescriptionSpanish());        
        //if ( !duplicate ){
        DepositTypesConf.insertDepositTypes(depositTypesVo);
        //} else{        
        //}
        String errorCodeDT = depositTypesVo.getErrorCode();
        if (errorCodeDT!=null){
            msg = Languages.getString("jsp.admin.tools.depositTypes."+errorCodeDT, SessionData.getLanguage());
            cssSpanMessages = "spanErrorMessages";
            action = "edit";
            dtEdit = depositTypesVo;
        }
    }
    else if (action != null && action.trim().equals("editDeposit") && request.getParameter("depositCode").length()>0) {
        DepositTypesVo depositTypesVo = new DepositTypesVo();
        depositTypesVo.setId(request.getParameter("idDepositType"));
        depositTypesVo.setDescriptionEnglish(request.getParameter("descriptionEng"));
        depositTypesVo.setDescriptionSpanish(request.getParameter("descriptionSpa"));
        depositTypesVo.setExternalCode(request.getParameter("externalCode"));
        depositTypesVo.setDepositCode(request.getParameter("depositCode"));   
        if ( request.getParameter("transFerAccount")!=null && request.getParameter("transFerAccount").equals("on") ){
            depositTypesVo.setHasTransferAccount("1");
        } else{
            depositTypesVo.setHasTransferAccount("0");
        }
        DepositTypesConf.updateDepositTypes(depositTypesVo);
        String errorCodeDT = depositTypesVo.getErrorCode();
        if (errorCodeDT!=null){
            msg = Languages.getString("jsp.admin.tools.depositTypes."+errorCodeDT, SessionData.getLanguage());
            cssSpanMessages = "spanErrorMessages";
            action = "edit";
            dtEdit = depositTypesVo;
        }
    }
    else if(action != null && action.trim().equals("removeDepositType")){
        DepositTypesConf.deleteDepositTypes(id);
    }

    List<DepositTypesVo> depositTypesConfList = DepositTypesConf.getDepositTypesList();
    List<DepositExternalTypeVo> depositExternalTypesConfList =DepositTypesConf.getDepositExternalTypesList();
    StringBuilder comboExternal = new StringBuilder();
    for(DepositExternalTypeVo external : depositExternalTypesConfList ){
        if ( dtEdit != null && dtEdit.getExternalCode()!=null && dtEdit.getExternalCode().equals(external.getId()) ){            
            comboExternal.append("<option selected value='"+ external.getId() +"' >"+external.getDescription()+"</option>");
        } else{
            comboExternal.append("<option value='"+ external.getId() +"' >"+external.getDescription()+"</option>");
        }
    }

%>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="windowsFloatCharging">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>    

<table border="0" cellpadding="0" cellspacing="0" width="1200" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <%=Languages.getString("jsp.admin.tools.depositTypes.title", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">

            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr align="center">
                    <td>
                        <span id="<%=cssSpanMessages%>"><%=msg%></span>
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        <div id="div_form" style="<%=(action != null && action.equals("edit")?"":"display:none;")%>width:90%;height:100%;">
                            <fieldset class="field_set">
                                <legend></legend>
                                <form action="admin/tools/depositTypes/depositTypesList.jsp" method="POST">
                                    <table width="100%" border="0" cellspacing="10" align="center" height="40">
                                        <tr align="center">
                                            <td><%=Languages.getString("jsp.admin.tools.depositTypes.depositCode", SessionData.getLanguage())%></td>
                                            <td><%=Languages.getString("jsp.admin.tools.depositTypes.descriptionEng", SessionData.getLanguage())%></td>
                                            <td><%=Languages.getString("jsp.admin.tools.depositTypes.descriptionSpa", SessionData.getLanguage())%></td>
                                            <td><%=Languages.getString("jsp.admin.tools.depositTypes.catalogPaymentWay", SessionData.getLanguage())%></td>                                            
                                            <td><%=Languages.getString("jsp.admin.tools.depositTypes.captureTransAccount", SessionData.getLanguage())%></td>
                                        </tr>
                                        <tr align="center">
                                            <td><input size="15" type="text" name="depositCode" id="depositCode" value="<%=(dtEdit != null) ? dtEdit.getDepositCode(): ""%>" maxlength="70"></td>
                                            <td><input size="35" type="text" name="descriptionEng" id="descriptionEng" value="<%=(dtEdit != null) ? dtEdit.getDescriptionEnglish() : ""%>" maxlength="70"></td>
                                            <td><input size="35"type="text" name="descriptionSpa" id="descriptionSpa" value="<%=(dtEdit != null) ? dtEdit.getDescriptionSpanish() : ""%>" maxlength="70"></td>
                                            <td><select id="externalCode" name="externalCode"><%=comboExternal.toString()%></select> </td>                                            
                                            <% 
                                            String checketTrAccountControl = "";    
                                            if ( dtEdit !=null && dtEdit.getHasTransferAccount().equals("1")){
                                                checketTrAccountControl = "checked";
                                            }
                                            %>
                                            <td><input type="checkbox" name="transFerAccount" id="transFerAccount" <%=checketTrAccountControl%> ></td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <input type="hidden" name="action" id="action" value="<%=actionEvent%>" />
                                                <input type="hidden" name="idDepositType" id="idDepositType" value="<%=id%>" />
                                                <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%>" onclick="hideForm();">
                                                <input type="submit" value="<%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%>" />                                                
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </fieldset>
                        </div>
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.depositTypes.newDeportType", SessionData.getLanguage())%>" onclick="newDepositType();">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="80%" border="0" cellspacing="1" align="center" height="40">
                            <tr class="rowhead2">
                                <td><%=Languages.getString("jsp.admin.tools.depositTypes.depositCode", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.depositTypes.descriptionEng", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.depositTypes.descriptionSpa", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.depositTypes.PaymentWay", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.depositTypes.captureTransAccount", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.genericLabel.options", SessionData.getLanguage())%></td>                                                                
                            </tr>
                            <%
                                int index = 0;
                                for (DepositTypesVo depositType : depositTypesConfList) {
                                    index++;
                                    String externalCode = depositType.getExternalCodeDescription();
                                    if (externalCode == null){
                                        externalCode = "N/A";
                                    }
                                    String transAcc="";
                                    if ( depositType.getHasTransferAccount().equals("1")){
                                        transAcc = "checked";
                                    }
                            %>
                            <tr class="row<%=(index % 2 == 0) ? "1" : "2"%>">
                                <td><%=depositType.getDepositCode()%></td>
                                <td><%=depositType.getDescriptionEnglish()%></td>
                                <td><%=depositType.getDescriptionSpanish()%></td>
                                <td><%=externalCode%></td>
                                <td align="center"><input type="checkbox" <%=transAcc%> disabled ></td>
                                <td align="center">
                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.edit", SessionData.getLanguage())%>" onclick="editDepositType('<%=depositType.getId()%>');">
                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%>" onclick="deleteDepositType('<%=depositType.getId()%>', '<%=Languages.getString("jsp.admin.genericLabel.removeItemQuestion", SessionData.getLanguage())%> (<%=depositType.getDescriptionEnglish()%>)','<%=Languages.getString("jsp.admin.genericLabel.canNotDelete", SessionData.getLanguage())%>');">                                   
                                </td>                                
                            </tr>
                            <%}%>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

<script LANGUAGE="JavaScript">


</script>

<%@ include file="/includes/footer.jsp" %>
