<%-- 
    Document   : paymentWizardList
    Created on : Feb 9, 2017, 11:35:03 AM
    Author     : dgarzon
--%>


<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardVo"%>
<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardConfiguration"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script language="JavaScript" src="/support/includes/primeui/primeui-2.0-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/paymentWizard.js"></script>


<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>


<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
    #spanWarningMessages, #spanWarningMessagesProducts{
        font-size: 17px;
        color: green;
    }

    .windowsFloatCharging {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }


</style>


<%
    String msg = request.getParameter("msg");
    msg = (msg == null) ? "" : msg;

    String paymentWizardId = request.getParameter("action");

    String contentType = request.getContentType();
    if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
        Map<String, String> map = PaymentWizardConfiguration.uploadImage(request);

    }

    List<PaymentWizardVo> paymentWizardList = PaymentWizardConfiguration.getPaymentWizardList();

%>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="windowsFloatCharging">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>    

<table border="0" cellpadding="0" cellspacing="0" width="1200" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <%=Languages.getString("jsp.admin.tools.paymentWizard.title", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">

            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr align="center">
                    <td>
                        <span id="spanWarningMessages"><%=msg%></span>
                    </td>
                </tr>

                <tr align="center">
                    <td>
                        <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.paymentWizard.newRegister", SessionData.getLanguage())%>" onclick="newPaymentWizard();">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="80%" border="0" cellspacing="1" align="center" height="40">
                            <tr class="rowhead2">
                                <td><%=Languages.getString("jsp.admin.tools.paymentWizard.flowDescription", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.tools.paymentWizard.bank", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.isEnabled", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.genericLabel.options", SessionData.getLanguage())%></td>
                            </tr>
                            <%
                                int index = 0;
                                for (PaymentWizardVo paymentWizard : paymentWizardList) {
                                    index++;
                            %>
                            <tr class="row<%=(index % 2 == 0) ? "1" : "2"%>">
                                <td><%=paymentWizard.getDescription()%></td>
                                <td><%=paymentWizard.getBanksDepositTypes().getBankName()%></td>
                                <td><%=paymentWizard.isEnabled()%></td>
                                <td align="center">
                                    <input type="button" name="buttonEdit" value="<%=Languages.getString("jsp.admin.genericLabel.edit", SessionData.getLanguage())%>" onclick="editPaymentWizard('<%=paymentWizard.getId()%>');">
                                    <input type="button" name="buttonDelete" value="<%=Languages.getString("jsp.admin.genericLabel.Delete", SessionData.getLanguage())%>" onclick="deletePaymentWizard('<%=paymentWizard.getId()%>', '<%=Languages.getString("jsp.admin.genericLabel.removeItemQuestion", SessionData.getLanguage())%> (<%=paymentWizard.getDescription()%>)', '<%=Languages.getString("jsp.admin.genericLabel.canNotDelete", SessionData.getLanguage())%>');">
                                    <input type="button" name="buttonSelectImg" value="<%=Languages.getString("jsp.admin.tools.paymentWizard.selectImage", SessionData.getLanguage())%>" onclick="selectImage('<%=paymentWizard.getId()%>');">
                                </td>
                            </tr>
                            <%}%>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

<!-- Popup dialog -->
<div id="dialogUploadImage" style="display:none;">

    <form ENCTYPE="multipart/form-data" METHOD="POST" onsubmit="return validateFileToUpload();">

        <script>
            function validateFileToUpload() {

                var fileName = document.getElementById("txt_filename");
                if (fileName === null || fileName.value === '') {
                    return false;
                }

                var nameFilesplit = fileName.value.split("\\");
                var nameF = nameFilesplit[nameFilesplit.length - 1];
                nameF = nameF.toLowerCase();

                var file = fileName.files[0];
                if (file.size > 65536) {
                    alert('<%=Languages.getString("jsp.admin.tools.paymentWizard.invalidImage", SessionData.getLanguage())%>');
                    return false;
                }
                if (nameF.indexOf(".jpg") > 0 || nameF.indexOf(".JPG") > 0 ||
                        nameF.indexOf(".png") > 0 || nameF.indexOf(".PNG") > 0 ||
                        nameF.indexOf(".jpeg") > 0 || nameF.indexOf(".JPEG") > 0) {
                    return true;
                } else {
                    $("span").text('<%=Languages.getString("jsp.admin.tools.paymentWizard.invalidImage", SessionData.getLanguage())%>').show().fadeOut(10000);
                    alert('<%=Languages.getString("jsp.admin.tools.paymentWizard.invalidImage", SessionData.getLanguage())%> (*.jpg, *.png)');
                    return false;
                }
            }
        </script>

        <table width="100%" border="0" cellspacing="10" align="center" height="40">
            <tr>
                <td width="40%" style="vertical-align: top;">
                    <fieldset class="field_set">
                        <legend><%=Languages.getString("jsp.admin.tools.paymentWizard.configurationHelpField", SessionData.getLanguage())%> 
                        (<%=Languages.getString("jsp.admin.tools.paymentWizard.sizeImageMessage", SessionData.getLanguage())%>)
                        </legend>
                        <table width="100%" border="0" cellspacing="10" align="center" height="40">

                            <!-- imageId -->
                            <tr>
                                <td style="width: 20%"><%=Languages.getString("jsp.admin.tools.paymentWizard.helpImage", SessionData.getLanguage())%></td>
                                <td style="width: 80%">
                                    <input type="hidden" name="paymentWizardId" id="paymentWizardId" value="" />
                                    <input type="file" name="txt_filename" id="txt_filename" size="100" accept="image/*">
                                    <input type="submit" value="<%=Languages.getString("jsp.admin.tools.loadbutton", SessionData.getLanguage())%>">
                                </td>
                            </tr>
                        </table>
                    </fieldset>

                    <div id ="fieldsetImage" name ="fieldsetImage" style="display:none;">
                        <fieldset class="field_set">
                            <legend><%=Languages.getString("jsp.admin.tools.paymentWizard.currentImage", SessionData.getLanguage())%></legend>
                            <table width="100%" border="0" cellspacing="10" align="center" height="40">

                                <tr>
                                    <td style="width: 100%" colspan="2">
                                        <img id="imagenPreview" src="" />
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </td>
            </tr> 
        </table>
    </form>
</div>

<script LANGUAGE="JavaScript">

</script>

<%@ include file="/includes/footer.jsp" %>
