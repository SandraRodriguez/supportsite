<%-- 
    Document   : paymentWizardAddEdit
    Created on : Feb 9, 2017, 3:18:39 PM
    Author     : dgarzon
--%>


<%@page import="com.debisys.tools.depositConfiguration.DepositTypesVo"%>
<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardConfiguration"%>
<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardDetailVo"%>
<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardVo"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesVo"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesConf"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="js/paymentWizard.js"></script>

<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/primeui/primeui-2.0-min.js" language="JavaScript" charset="utf-8"></script>
<script type="text/javascript" src="js/paymentWizard.js"></script>


<script type="text/javascript">

    $(function () {
        $("#spinnerP").spinner({min: 0});
        $(".spinner").spinner({min: 0});
        $("#spinnerP").spinner({max: 80});
        $(".spinner").spinner({max: 80});
        spinnerNumeric();

        $("#spinnerP").on("spinstop", function () {
            erasePrefixValue();
            resizeInputType();
        });
    });

    window.onload = function () {
        var myInput = document.getElementById('fieldPrefix');
        myInput.onpaste = function (e) {
            e.preventDefault();
        }
    }



</script>


<%
    String strRefId = SessionData.getProperty("ref_id");
    String iso_id = SessionData.getProperty("iso_id");
    String action = request.getParameter("action");
    String textWarning = "";
    String textWarningProducts = "";
    int indexProducts = 0;
    String actionValue = "insertPaymentWizard";
    boolean enabled = true;
    int maxLength = 15;
    int minLength = 0;
    String fieldLabel = "";
    String fieldType = "";
    String fieldPrefix = "";
    String helpText = "";
    String imageId = "";

    String id = request.getParameter("id");
    PaymentWizardVo pw = new PaymentWizardVo();
    List<DepositTypesVo> depositTypesList = new ArrayList<DepositTypesVo>();
    
    if (action != null && action.trim().equals("edit")) {
        pw = PaymentWizardConfiguration.getPaymentWizardById(request.getParameter("id"));
        minLength = pw.getListDetail().get(0).getFieldMinLength();
        maxLength = pw.getListDetail().get(0).getFieldLength();
        actionValue = "editPaymentWizard";
        fieldLabel = pw.getListDetail().get(0).getLabel();
        fieldType = pw.getListDetail().get(0).getFieldType();
        fieldPrefix = pw.getListDetail().get(0).getFieldPrefix();
        helpText = pw.getListDetail().get(0).getHelpText();
        if (pw.getListDetail().get(0).getImageId() != null) {
            imageId = String.valueOf(pw.getListDetail().get(0).getImageId());
        }

        depositTypesList = pw.getBanksDepositTypes().getDepositTypeList();

    }

    List<BanksDepositTypesVo> banksDepositTypesList = BanksDepositTypesConf.getBanksDepositTypesListByBanks();


%>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="windowsFloatCharging">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>    


<table border="0" cellpadding="0" cellspacing="0" width="1200" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <%=Languages.getString("jsp.admin.tools.paymentWizard.title", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">


            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>


                <tr>
                    <td>

                        <form id="formUploadFile" class="reportForm" method="post" action="admin/tools/depositTypes/depositTypesProcessDB.jsp" onsubmit="return validateFormPaymentWizard('<%=Languages.getString("jsp.admin.tools.paymentWizard.duplicateMessage", SessionData.getLanguage())%>')">
                            <table width="100%" border="0" cellspacing="10" align="center" height="40">

                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="10" align="center" height="40">
                                            <tr>
                                                <td align="left"><%=Languages.getString("jsp.admin.rateplans.referralAgent.isEnabled", SessionData.getLanguage())%></td>
                                                <td><input type="checkbox" name="enabledWizard" id="enabledWizard"  <%=(enabled) ? " checked=\"checked\"" : ""%> ></td>
                                            </tr>
                                            <tr>
                                                <td align="left"><%=Languages.getString("jsp.admin.tools.paymentWizard.flowDescription", SessionData.getLanguage())%></td>
                                                <td><input type="text" id="description" name="description" value="<%=pw.getDescription()%>" size="40" maxlength="80"/></td>
                                            </tr>
                                            <tr >
                                                <td align="left"><%=Languages.getString("jsp.admin.tools.paymentWizard.bank", SessionData.getLanguage())%></td>
                                                <td>                                        
                                                    <select name="banksDepositTypesList" id="banksDepositTypesList" onchange="changeDepositTypes()" style="width: 260px;">
                                                        <option value="-1"></option>
                                                        <%for (BanksDepositTypesVo ra : banksDepositTypesList) {%>
                                                        <option value="<%=ra.getBanksId()%>" <%=(pw.getBanksDepositTypes() != null && ra.getBanksId().equalsIgnoreCase(pw.getBanksDepositTypes().getBanksId())) ? "selected" : ""%>><%=ra.getBankName()%></option>
                                                        <%}%>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr >
                                                <td align="left"><%=Languages.getString("jsp.admin.tools.paymentWizard.depositType", SessionData.getLanguage())%></td>
                                                <td>                                        
                                                    <select name="depositTypeList" id = "depositTypeList" style="width: 260px;">
                                                        <option value="-1"></option>
                                                        <%for (DepositTypesVo ra : depositTypesList) {%>
                                                        <option value="<%=ra.getId()%>" <%=(pw.getBanksDepositTypes() != null && ra.getId().equalsIgnoreCase(pw.getBanksDepositTypes().getDepositTypeSelected())) ? "selected" : ""%>><%=(SessionData.getLanguage().equalsIgnoreCase("spanish")) ? ra.getDescriptionSpanish() : ra.getDescriptionEnglish()%></option>

                                                        <%}%>
                                                    </select>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="1"> <span id="spanWarningMessages"><%=textWarning%></span></td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td>
                                        <table width="100%" border="0" cellspacing="10" align="center" height="40">

                                            <tr>
                                                <td width="40%" style="vertical-align: top;" rowspan="5">
                                                    <fieldset class="field_set">
                                                        <legend><%=Languages.getString("jsp.admin.tools.paymentWizard.configurationReferenceField", SessionData.getLanguage())%></legend>
                                                        <table width="100%" border="0" cellspacing="10" align="center" height="40">

                                                            <!-- Label -->
                                                            <tr>
                                                                <td style="width: 20%"><%=Languages.getString("jsp.admin.tools.paymentWizard.label", SessionData.getLanguage())%></td>
                                                                <td style="width: 80%">
                                                                    <input type="text" name="fieldLabel" id="fieldLabel" value="<%=fieldLabel%>" size="30" maxlength="50" onkeyup=""/>
                                                                </td>
                                                            </tr>

                                                            <!-- Min Length -->
                                                            <tr>
                                                                <td style="width: 20%"><%=Languages.getString("jsp.admin.tools.paymentWizard.Minlength", SessionData.getLanguage())%></td>
                                                                <td style="width: 80%">
                                                                    <input id="minLengthPayment" name="minLengthPayment" type="text" value="<%=minLength%>" class="spinner"/>
                                                                </td>
                                                            </tr>
                                                            
                                                            <!-- fieldLength -->
                                                            <tr>
                                                                <td style="width: 20%"><%=Languages.getString("jsp.admin.tools.paymentWizard.length", SessionData.getLanguage())%></td>
                                                                <td style="width: 80%">
                                                                    <input id="spinnerP" name="spinnerP" type="text" value="<%=maxLength%>" class="spinner"/>
                                                                </td>
                                                            </tr>

                                                            <!-- fieldType -->
                                                            <tr>
                                                                <td style="width: 20%"><%=Languages.getString("jsp.admin.tools.paymentWizard.fieldType", SessionData.getLanguage())%></td>
                                                                <td style="width: 80%">
                                                                    <input type="radio" onchange="erasePrefixValue();" name="fieldType" value="AN" <%=(fieldType.equals("") || fieldType.equalsIgnoreCase("AN")) ? "checked" : ""%>><%=Languages.getString("jsp.admin.tools.paymentWizard.alphanumeric", SessionData.getLanguage())%> 
                                                                    <input type="radio" onchange="erasePrefixValue();" name="fieldType" value="A"  <%=(fieldType.equals("A")) ? "checked" : ""%>><%=Languages.getString("jsp.admin.tools.paymentWizard.alphabetical", SessionData.getLanguage())%>
                                                                    <input type="radio" onchange="erasePrefixValue();" name="fieldType" value="N" <%=(fieldType.equals("N")) ? "checked" : ""%>><%=Languages.getString("jsp.admin.tools.paymentWizard.numeric", SessionData.getLanguage())%>
                                                                    <input type="radio" onchange="erasePrefixValue();" name="fieldType" value="F" <%=(fieldType.equals("F")) ? "checked" : ""%>><%=Languages.getString("jsp.admin.tools.paymentWizard.allCharacters", SessionData.getLanguage())%>
                                                                </td>
                                                            </tr>

                                                            <!-- fieldPrefix -->
                                                            <tr>
                                                                <td style="width: 20%"><%=Languages.getString("jsp.admin.tools.paymentWizard.prefix", SessionData.getLanguage())%></td>
                                                                <td style="width: 80%">
                                                                    <input type="text" name="fieldPrefix" id="fieldPrefix" value="<%=fieldPrefix%>" size="30" maxlength="<%=maxLength%>" onkeypress="return validateInputType(event);"/>
                                                                </td>
                                                            </tr>

                                                            <!-- helpText -->
                                                            <tr>
                                                                <td style="width: 20%"><%=Languages.getString("jsp.admin.tools.paymentWizard.helpText", SessionData.getLanguage())%></td>
                                                                <td style="width: 80%">
                                                                    <input type="text" name="helpText" id="helpText" value="<%=helpText%>" size="30" maxlength="800" onkeyup="" />
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </fieldset>
                                                </td> 
                                            </tr>

                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" align="center"> <input type="button" name="button" value="<%=Languages.getString("jsp.admin.genericLabel.cancel", SessionData.getLanguage())%>" onclick="goToPaymentWizard();">
                                        <input type="hidden" name="imageId" id="imageId" value="<%=imageId%>" />
                                        <input type="hidden" name="action" id="action" value="<%=actionValue%>" />
                                        <input type="hidden" name="idPaymentWizard" id="idPaymentWizard" value="<%=id%>" />
                                        <input type="submit" name="submit" id="submit" value="<%=Languages.getString("jsp.admin.genericLabel.Save", SessionData.getLanguage())%>" align="center" />
                                    </td>

                                </tr>



                            </table>    
                        </form>
                    </td>
                </tr>


                <tr>
                    <td>
                        <form id="formSimInventory" class="reportForm" method="post" action="admin/tools/depositTypes/depositTypesProcessDB.jsp" onsubmit="return validateFormPaymentWizard()">

                            <table width="100%" border="0" cellspacing="10" align="center" height="40">

                            </table>


                        </form>
                    </td>
                </tr>




            </table>
        </td>

    </tr>
</table>



<%@ include file="/includes/footer.jsp" %>
