<%-- 
    Document   : depositTypesProcessDB
    Created on : Feb 9, 2017, 9:34:23 AM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardConfiguration"%>
<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardDetailVo"%>
<%@page import="com.debisys.tools.depositConfiguration.PaymentWizardVo"%>
<%@page import="com.debisys.tools.depositConfiguration.DepositTypesVo"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesVo"%>
<%@page import="com.debisys.tools.depositConfiguration.BanksDepositTypesConf"%>
<%@page import="com.debisys.tools.depositConfiguration.DepositTypesConf"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String action = request.getParameter("action");
    
    if (action.equals("canDeleteDepositType")) {
        String depositTypeId = request.getParameter("depositTypeId");
        boolean exist = DepositTypesConf.canDeleteDepositType(depositTypeId);
        out.println(exist);
    }
    
    else if (action.equalsIgnoreCase("verifyDuplicateDepositType")){
        String descriptionEng = request.getParameter("descriptionEng");
        String descriptionSpa = request.getParameter("descriptionSpa");        
        boolean duplicate = DepositTypesConf.isDuplicateDepositType(descriptionEng, descriptionSpa);        
        out.println(duplicate);
    }
    
    else if(action.equalsIgnoreCase("verifyDuplicatePaymentWizard")){
        String bankSelected = request.getParameter("bankSelected");
        String depositTypeSelected = request.getParameter("depositTypeSelected");
        boolean duplicate = DepositTypesConf.isDuplicateDepositType(bankSelected, depositTypeSelected);
        out.println(duplicate);
    }
    
    else if (action.equals("canDeleteBankDepositType")) {
        String bankId = request.getParameter("bankId");
        boolean exist = BanksDepositTypesConf.canDeleteBanksDepositType(bankId);
        out.println(exist);
    }
    
    else if(action.equalsIgnoreCase("isBankDepositTypeDuplicate")){
        String idPaymentWizard = request.getParameter("idPaymentWizard");
        String selectedBank = request.getParameter("selectedBank");
        String depositTypeSelected = request.getParameter("depositTypeSelected");
        boolean existData = BanksDepositTypesConf.isBankDepositTypeDuplicate(idPaymentWizard, selectedBank,depositTypeSelected);
        out.println(existData);
    }

    else if (action.equals("getDepositTypes")) {
        String bankId = request.getParameter("bankId");
        BanksDepositTypesVo banksDepositTypesVo = BanksDepositTypesConf.getBanksDepositTypesVoByBankId(bankId);
        for (int i = 0; i < banksDepositTypesVo.getDepositTypeList().size(); i++) {
            DepositTypesVo depositType = banksDepositTypesVo.getDepositTypeList().get(i);
            if (SessionData.getLanguage().equalsIgnoreCase("spanish")) {
                out.println(depositType.getId() + "|" + depositType.getDescriptionSpanish());
            }
            else {
                out.println(depositType.getId() + "|" + depositType.getDescriptionEnglish());
            }
        }
    }
    
    //Payment wizard
    else if (action.equals("insertPaymentWizard") || action.equalsIgnoreCase("editPaymentWizard")) {
        String enabledWizard = request.getParameter("enabledWizard");
        String description = request.getParameter("description");
        String bankId = request.getParameter("banksDepositTypesList");
        String depositTypeId = request.getParameter("depositTypeList");
        String fieldLabel = request.getParameter("fieldLabel");
        String spinnerP = request.getParameter("spinnerP");
        String minLengthPayment = request.getParameter("minLengthPayment");
        String fieldType = request.getParameter("fieldType");
        String fieldPrefix = request.getParameter("fieldPrefix");
        String helpText = request.getParameter("helpText");
        
        PaymentWizardVo pw = new PaymentWizardVo();
        pw.setDescription(description);
        pw.setEnabled((enabledWizard == null)?false:true);
        pw.setBanksDepositTypes(BanksDepositTypesConf.getBanksDepositTypesByBankIdAndDepositType(bankId, depositTypeId));
        
        PaymentWizardDetailVo detailVo = new PaymentWizardDetailVo();
        detailVo.setLabel(fieldLabel);
        detailVo.setFieldMinLength(Integer.parseInt(minLengthPayment));
        detailVo.setFieldLength(Integer.parseInt(spinnerP));
        detailVo.setFieldType(fieldType);
        detailVo.setFieldPrefix(fieldPrefix);
        detailVo.setHelpText(helpText);
        detailVo.setImageId((request.getParameter("imageId").equals(""))?null: Long.parseLong(request.getParameter("imageId")));
        pw.addPaymentWizardDetail(detailVo);
        if(action.equals("insertPaymentWizard")){
            PaymentWizardConfiguration.paymentWizardInsert(pw);
        }
        else{
            pw.setId(request.getParameter("idPaymentWizard"));
            PaymentWizardConfiguration.paymentWizardUpdate(pw);
        }
        
        response.sendRedirect("paymentWizardList.jsp##");
        
    }
    
    else if(action.equalsIgnoreCase("removePaymentWizard")){
        PaymentWizardConfiguration.deletePaymentWizard(request.getParameter("id"));
        response.sendRedirect("paymentWizardList.jsp##");
    }
    
    else if (action.equalsIgnoreCase("getImageBytesByPaymentWizardId")){
        String imgBytes = PaymentWizardConfiguration.getImageBytes(request.getParameter("paymentWizardId"));
        out.print(imgBytes);
    }
    


%>