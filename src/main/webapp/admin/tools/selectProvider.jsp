<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to set the bonuses and topup thresholds
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="BonusThreshold" class="com.debisys.tools.BonusThreshold" scope="request"/>
<%
  	int section=11;
  	int section_page=6;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
<table width="99%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td>
<%
	Vector vecSearchResults = BonusThreshold.getProviders();
%>
<script>  
	function submitform()
	{
	  document.bonusPromoSMSForm.submit();
	}
</script>
<%
	if(request.getParameter("type") != null)
	{
		if(request.getParameter("type").equals("UPDATE"))
		{
			String oldProvider = request.getParameter("oldProvider");
			String msgName = request.getParameter("msgName");
%>
<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.bonusPromoToolsSMS.selectProvider",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
				<tr>
					<td class=formArea2>
						<table>
							<tr>
								<td colspan="2">					
									<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
										<tr>
											<td class="main"><br>
												<table width="100%" cellspacing="1" cellpadding="2">
													<tr>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.providerID",SessionData.getLanguage())%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.provider",SessionData.getLanguage())%>&nbsp;</th>	              				
													</tr>
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		out.println(
"													<tr class=row" + intEvenOdd + ">" +
"														<td align=center><a href=\"admin/tools/updateBonusPromoSMS.jsp?type=UPDATE&newProvider=" + vecTemp.get(0) + "&oldProvider=" + oldProvider + "&msgName=" + msgName +"\">" + vecTemp.get(0) + "</a></td>" +
"														<td>" + vecTemp.get(1) + "</td>" +
"													</tr>"
		);
          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
		}
		else if(request.getParameter("type").equals("INSERT"))
		{
%>
<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="100%" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.bonusPromoToolsSMS.selectProvider",SessionData.getLanguage())%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
				<tr>
					<td class=formArea>
						<table>
							<tr>
								<td colspan="2">					
									<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
										<tr>
											<td class="main"><br>
												<table width="100%" cellspacing="1" cellpadding="2">
													<tr>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.providerID",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.title.provider",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	              				
													</tr>
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
		out.println(
"													<tr class=row" + intEvenOdd + ">" +
"														<td align=center><a href=\"admin/tools/updateBonusPromoSMS.jsp?type=INSERT&newProvider=" + vecTemp.get(0) + "\">" + vecTemp.get(0) + "</a></td>" +
"														<td>" + vecTemp.get(1) + "</td>" +
"													</tr>"
		);
          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%
		}
	}
%>
<%@ include file="/includes/footer.jsp" %>