<%@ page import="com.debisys.tools.ErrorManagement,java.util.HashMap,java.util.Vector,java.util.Iterator,java.util.Hashtable,java.net.URLEncoder,com.debisys.utils.HTMLEncoder,com.debisys.utils.StringUtil" %>
<%
	int section=9;
	int section_page=22;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="ErrorManagement" class="com.debisys.tools.ErrorManagement" scope="request"/>
<jsp:setProperty name="ErrorManagement" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	String providerId = "";
	String errorCode = "";
	String errorMessage = "";
	Hashtable<String, String>  addErrors = null;
	if (request.getParameter("submitted") != null)
	{
		try
		{
			if (request.getParameter("submitted").equals("y"))
			{
				providerId = request.getParameter("providerID");
				errorCode = request.getParameter("errorCode");
				errorMessage = request.getParameter("errorMessage");
				ErrorManagement.insertError(providerId, errorCode, errorMessage, SessionData);
				if (ErrorManagement.isError())
				{
					addErrors = ErrorManagement.getErrors();
					providerId = ErrorManagement.getProviderId();
					errorCode = ErrorManagement.getErrorCode();
					errorMessage =  ErrorManagement.getErrorMessage();
				}
				else
				{
					response.sendRedirect("/support/admin/tools/errorManagement.jsp");
					return;
				}
			}
		}catch (Exception e)
		{
			System.out.println("ERROR "+e);
		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<script  type="text/javascript">

	function insertAtCaret(areaId)
	{
		var txtarea = document.getElementById(areaId);
		var wList = document.getElementById("wildcards");
		var text = wList.options[wList.selectedIndex].value;
		var scrollPos = txtarea.scrollTop;
		var strPos = 0;
		var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? "ff" : (document.selection ? "ie" : false ) );
		if (br == "ie")
		{
			txtarea.focus();
			var range = document.selection.createRange();
			range.moveStart ('character', -txtarea.value.length);
			strPos = range.text.length;
		}
		else if (br == "ff")
		{
			strPos = txtarea.selectionStart;
		}
		var front = (txtarea.value).substring(0,strPos);
		var back = (txtarea.value).substring(strPos,txtarea.value.length);
		txtarea.value=front+text+back;
		strPos = strPos + text.length;
		if (br == "ie")
		{
			txtarea.focus();
			var range = document.selection.createRange();
			range.moveStart ('character', -txtarea.value.length);
			range.moveStart ('character', strPos);
			range.moveEnd ('character', 0);
			range.select();
		}
		else if (br == "ff")
		{
			txtarea.selectionStart = strPos;
			txtarea.selectionEnd = strPos;
			txtarea.focus();
		}
		txtarea.scrollTop = scrollPos;
	}

	$(function ()
	{
		$('#Buttonsubmit').click(function()
		{
			$('#formErrorCode').submit();
		});
	});
</script>
<script>
	function setProviderID()
	{
		document.getElementById('providerID').value = document.getElementById('ProvidersList').value;
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    	<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.errors.add.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
  			<form id="formErrorCode" name="formErrorCode" method="post" action="admin/tools/errorManagement_add.jsp">
  			<input type="hidden" id="providerID" name="providerID" value="<%=providerId%>">
				<%
					if (addErrors != null)
					{
						out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
						Enumeration enum1=addErrors.keys();
						while(enum1.hasMoreElements())
						{
							String strKey = enum1.nextElement().toString();
							String strError = (String) addErrors.get(strKey);
							out.println("<li>" + strError + "</li>");
						}
						out.println("</font></td></tr>");
					}
				%>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
	  				<tr>
	    				<td>
	   						<table border="0" width="100%" cellpadding="0" cellspacing="0">
	   							<tr>
	       							<td class="formArea2">
	        							<br>
	         							<table border=0>
	         								<tr class="main">
	            								<td valign=middle nowrap width="30%"><%=Languages.getString("jsp.admin.reports.transactions.provider.option_providers",SessionData.getLanguage())%></td>
	            								<td valign=middle nowrap width="3%"></td>
	            								<td valign=middle nowrap width="67%"><%=Languages.getString("jsp.admin.tools.provider_error_code",SessionData.getLanguage())%></td>
	            							</tr>
	            							<tr class="main">
	            								<td valign=top nowrap width="30%">
													<%
														Vector vecIsoProviderMapping = ErrorManagement.getProvidersByIso(SessionData);
														if (vecIsoProviderMapping.isEmpty())
														{
													%>
															<br>
															<font color=ff0000><b><%=Languages.getString("jsp.admin.reports.transactions.errors.provider_error",SessionData.getLanguage())%></b></font>
															<br><br>
													<%
														}
														else
														{
													%>
													    	<select name="ProvidersList" id="ProvidersList"  onchange="setProviderID();">
													<%
													 			String strSelectedP = "";
																Iterator it2 = vecIsoProviderMapping.iterator();
															  	while (it2.hasNext())
															  	{
															    	Vector vecTemp = null;
															    	vecTemp = (Vector) it2.next();
														          	if(!providerId.equals(""))
														  	    	{
														  	    		if(providerId.equals(vecTemp.get(0)))
														     	    	{
														         			strSelectedP = "selected";
														      		    }
														          		else
															          	{
														          			strSelectedP = "";
															          	}
														  	    	}
														          	else
														          	{
														          		strSelectedP = "";
														          		providerId = vecTemp.get(0).toString();
														          	}
															    	out.println("<option " + strSelectedP + " value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
															  	}
													%>
													        </select>
													        <%if (addErrors != null && addErrors.containsKey("provider")) out.print("<font color=ff0000>*</font>");%>
           	    		    	            				<script>
           	    		        	        					setProviderID();
                                               				</script>													        
													<%
														}
													%>
												</td>
												<td valign=middle nowrap width="3%"></td>
	             								<td valign=middle nowrap width="67%">
	             									<input type="text" name="errorCode" id="errorCode" size="20" maxlength="30" value="<%=HTMLEncoder.encode(StringUtil.toString(errorCode))%>">
	             									<%if (addErrors != null && addErrors.containsKey("errorCode")) out.print("<font color=ff0000>*</font>");%>
	             									<br>
	             									<br>
	             								</td>
	             							</tr>
						            		<tr class="main">
							            		<td valign=middle nowrap width="30%"><%=Languages.getString("jsp.admin.tools.wildcards",SessionData.getLanguage())%></td>
							            		<td valign=middle nowrap width="3%"></td>
							            		<td valign=middle nowrap width="67%"><%=Languages.getString("jsp.admin.tools.provider_error_message",SessionData.getLanguage())%></td>
											</tr>
											<tr class="main">
												<td valign=top nowrap width="30%">
													<%
														Vector vecWildCards = ErrorManagement.getWildcards();
														if (vecWildCards.isEmpty())
														{
													%>
															<br>
															<font color=ff0000><b><%=Languages.getString("jsp.admin.tools.errors.errors.wildcard_error",SessionData.getLanguage())%></b></font>
															<br><br>
													<%
														}
														else
														{
													%>
															<select name="wildcards" id="wildcards">
													<%
															Iterator itWc = vecWildCards.iterator();
														  	while (itWc.hasNext())
														  	{
																Vector vecTempWc = null;
																vecTempWc = (Vector) itWc.next();
																out.println("<option value=" + vecTempWc.get(0) +">" + vecTempWc.get(0) + " - (" + vecTempWc.get(1) + ")" + "</option>");
														  	}
													%>
												    		</select>
													<%
														}
													%>
													<br>
													<br>
													<input type="button" id="btnInsert" name="btnInsert" onclick="insertAtCaret('errorMessage');" value="<%=Languages.getString("jsp.admin.tools.add_wildcards",SessionData.getLanguage())%>">
												</td>
												<td valign=middle nowrap width="3%"></td>
												<td valign=middle nowrap width="67%">
													<textarea name="errorMessage" id="errorMessage" rows="10" cols="50" maxlength="140"><%=HTMLEncoder.encode(StringUtil.toString(errorMessage))%></textarea>
													<%if (addErrors != null && addErrors.containsKey("errorMessage")) out.print("<font color=ff0000>*</font>");%>
												</td>
						            		</tr>
						            		<tr class="main">
							            		<td valign=middle nowrap width="30%"></td>
							            		<td valign=middle nowrap width="3%"></td>
							            		<td valign=middle nowrap width="67%"><br>*<%=Languages.getString("jsp.admin.tools.edit.update_error.error_message_instructions",SessionData.getLanguage())%></td>
											</tr>
	            						</table>
	            						<br>
	            					</td>
	            				</tr>
	         				</table>
	            		</td>
	            	</tr>
	            </table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
	  				<tr>
	    				<td>
	   						<table border="0" width="100%" cellpadding="0" cellspacing="0" align=center >
	   							<tr>
	       							<td class="formArea2">
	        							<br>
	         							<table border=0 align=center>
	            							<tr class="formAreaTitle2" align=center>
	            								<td>
	            									<input type="hidden" name="submitted" value="y">
	            									<input type="button" name="Buttonsubmit" id="Buttonsubmit" value="<%=Languages.getString("jsp.admin.tools.edit.add_error",SessionData.getLanguage())%>">
	            								</td>
	             								<td nowrap valign="top" width="100">
														<input type=button value="<%=Languages.getString("jsp.admin.cancel",SessionData.getLanguage())%>" onclick="DoCancel();">
													<script>

														function DoCancel()
														{
															window.location="/support/admin/tools/errorManagement.jsp?";
														}
													</script>
	             								</td>
	             							</tr>
	            						</table>
	            						<br>
	            					</td>
	            				</tr>
	         				</table>
	            		</td>
	            	</tr>
	            </table>
            </form>
    	</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>