<%@ page language="java" import="java.util.*, com.debisys.utils.*" pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to set the bonuses and topup thresholds
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CrossBorderExchangeRate" class="com.debisys.tools.CrossBorderExchangeRate" scope="request"/>
<%
int section=9;
  	int section_page=12;
  	int result = -1;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 

<table width="99%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td>
<%
   boolean resultFound=false;
   boolean showAddButton = false;
   int insertRes=1;
   String additionalMessages = "";
    if(request.getParameter("type") != null)
    {
        if(request.getParameter("type").equals("INSERT"))
        {
                insertRes=CrossBorderExchangeRate.insertXborderExchangeRate(request, SessionData);			
        }
        if(request.getParameter("type").equals("DELETE"))
        {
                CrossBorderExchangeRate.deleteXborderExchangeRate(request, SessionData);

        }
        if(request.getParameter("type").equals("UPDATE"))
        {
                CrossBorderExchangeRate.updateXborderExchangeRate(request, SessionData);			
        }
    }
	
    Vector vecSearchResults = CrossBorderExchangeRate.getXborderExchangeRates(request, SessionData);

    if (vecSearchResults != null && vecSearchResults.size() > 0)
    {
        resultFound=true;        
    }
    Vector countryResults=CrossBorderExchangeRate.getISOCountry(request,SessionData);
    String iso= "";
    String country = "";
    String currency = "";
    String countryId = "";
        
    if ( countryResults.size() > 0 )
    {
        Iterator i=countryResults.iterator();
        Vector vecTemp1 = (Vector) i.next();
        iso=vecTemp1.get(0).toString();
        country=vecTemp1.get(1).toString();
        currency=vecTemp1.get(2).toString();
        countryId=vecTemp1.get(3).toString();
        showAddButton = true;
    }
    else
    {            
        additionalMessages = Languages.getString("jsp.includes.menu.xBorderExchangeRate.currency_rep_setup",SessionData.getLanguage());
    }
%>
<script>  
	function submitform()
	{
	  document.crossBorderExchangeRateForm.submit();
	}
	
	
	
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width=100% align=center>
				<th>Exchange Rate Routes from <%=country%> ( <%=iso%> )</th>
				<%if(resultFound){%>
					
				<tr>
					<td class=formArea2>
					<%if(insertRes==0){%>			
						<tr><td><font color=red><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.addError",SessionData.getLanguage()).toUpperCase()%></font></td></tr>
				<%}%>
						<table>
							<tr>
								<td colspan="4">					
									<table border="0" cellpadding="2" cellspacing="0" width=100% align="center">
										<tr>
											<td class="main"><br>
												<table cellspacing="1" cellpadding="2" width=100%>
													<tr>
														<th class=rowhead2>#</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.destinationCountry",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.product",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.originCurrency",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.destinationCurrency",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.rate",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.flatFee",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.percentageFee",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   	   
														<th class=rowhead2><%=Languages.getString("jsp.admin.tools.bonusPromoTools.edit",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2 style="text-align:center;"><%=Languages.getString("jsp.admin.tools.s2k.delete",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
													</tr>
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		Vector vecTemp = null;
		vecTemp = (Vector) it.next();
%>		
														<tr class=rowwrap<%=intEvenOdd%>>
															<td valign=top><%=intCounter%></td>
															<td valign=top id=destinationCountry<%=intCounter%> name=destinationCountry<%=intCounter%>><%=vecTemp.get(0)%></td>
															<td valign=top id=product<%=intCounter%> name=product<%=intCounter%>><%=vecTemp.get(1)%></td>
															<td valign=top id=originCurrency<%=intCounter%> name=originCurrency<%=intCounter%>><%=vecTemp.get(2)%></td>
															<td valign=top id=destinationCurrency<%=intCounter%> name=destinationCurrency<%=intCounter%>><%=vecTemp.get(3)%></td>
															<td valign=top id=rate<%=intCounter%> name=rate<%=intCounter%>><%=NumberUtil.formatExchangeRate(vecTemp.get(4).toString())%></td>
															<td valign=top id=flatFee<%=intCounter%> name=flatFee<%=intCounter%>><%=NumberUtil.formatAmount(vecTemp.get(5).toString())%></td>
															<td class=noclass style='width:500;text-wrap:hard-wrap' id=percentageFee<%=intCounter%> name=percentageFee<%=intCounter++%>><%=NumberUtil.formatAmount(vecTemp.get(6).toString())%></td>
															<!--<td valign=top><a href="admin/tools/updateXborderExchangeRate.jsp?id=<%=vecTemp.get(10).toString()%>&type=UPDATE"><img src="images/icon_edit.gif" border=0 alt="Edit this Route." title="Edit this Route." /></a></td>  -->
															<td valign=top><a href="javascript:void(0);" onclick="return UpdateRoute(<%=vecTemp.get(10).toString()%>);"><img src="images/icon_edit.gif" border=0 alt="Edit this Route." title="Edit this Route." /></a></td>
															<td valign=top><a href="javascript:void(0);" onclick="return DeleteRoute(<%=vecTemp.get(10).toString()%>);"><img src="images/icon_delete.gif" border=0 alt="Delete this route." title="Delete this route." /></a></td>
															<!--<td valign=top><a href="admin/tools/xBorderExchangeRate.jsp?id=<%=vecTemp.get(10).toString()%>&type=DELETE" onclick="return confirm('Are you sure you want to delete?')"><img src="images/icon_delete.gif" border=0 alt="Delete this route." title="Delete this route." /></a></td>-->
															
														</tr>
<%          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            							
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						
				<%}else{ %>
                                    <tr><td><font color=ff0000><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.routes.not_found",SessionData.getLanguage())%></font></td></tr>
                                    <tr><td><font color="red"><%=additionalMessages%></font></td></tr>
                                                
				<%}%>
				
                                <tr>
                                    <td>
                                       <% if ( showAddButton ){%>
                                        <form name=insertThresholdBonus action="admin/tools/updateXborderExchangeRate.jsp" method="post">
                                            <input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.add_xBorderExchangeRate",SessionData.getLanguage())%>">
                                            <input type=hidden name=type value=INSERT />
                                        </form>
                                       <% } %>
                                        <FORM ID="frmDelete" action="admin/tools/xBorderExchangeRate.jsp" METHOD="POST">
                                            <INPUT TYPE="hidden" NAME="type" VALUE="DELETE">
                                            <INPUT TYPE="hidden" ID="delId" NAME="delId" VALUE="">
                                        </FORM>
                                        <SCRIPT>
                                            function DeleteRoute(nID)
                                            {
                                                    var agree=confirm("Are you sure you want to delete?");
                                                    if (agree)
                                                    {
                                                            document.getElementById("delId").value = nID;
                                                            document.getElementById("frmDelete").submit();
                                                    }
                                                    else
                                                    {
                                                            return false ;
                                                    }


                                            }
                                        </SCRIPT>
                                        <FORM ID="frmEdit" action="admin/tools/updateXborderExchangeRate.jsp" METHOD="POST">
                                                <INPUT TYPE="hidden" NAME="type" VALUE="UPDATE">
                                                <INPUT TYPE="hidden" ID="editId" NAME="editId" VALUE="">
                                        </FORM>
                                        <SCRIPT>
                                        function UpdateRoute(nID)
                                        {
                                                document.getElementById("editId").value = nID;
                                                document.getElementById("frmEdit").submit();
                                        }
                                        </SCRIPT>
                                    </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>