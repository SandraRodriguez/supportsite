<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
// DBSY-568 SW
// Used to add a bonus and topup threshold
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CrossBorderExchangeRate" class="com.debisys.tools.CrossBorderExchangeRate" scope="request"/>
<jsp:useBean id="BonusThreshold" class="com.debisys.tools.BonusThreshold" scope="request"/>
<%
  	int section=9;
  	int section_page=12;
	Vector vecTemp = null;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
 
<head>
 <script type="text/javascript" src="/support/includes/ajax.js"></script>
 <script type="text/javascript" src="/support/includes/jsquery.js"></script>
<table width="99%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td>	
<script>  

	var decimalSupported=8;
	
	function roundNumber(number, decimals) 
	{ 
	var i = number.indexOf('.');
  	if (i < 0) return number + ".0000";
	var newnumber = new Number(number+'').toFixed(parseInt(decimals));
	return newnumber;
	}
	
	function submitform(check)
	{
		// Check that everything is OK to be entered. No missing fields.
		if(document.getElementById('rate').value.length === 0)
		{
			alert('Please enter an exchange rate greater than 0');
			return;
		}
		
		 if (isNaN(document.getElementById('rate').value))
			{
				alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
				document.getElementById('rate').focus();
				return (false);
		
			}
			else
			{
				if (document.getElementById('rate').value < 0)
				{
		
			    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
			    	document.getElementById('rate').focus();
			    	return (false);
				}
				else
				{
					document.getElementById('rate').value = roundNumber(document.getElementById('rate').value,decimalSupported);
				}
			}
			
			
			if(document.getElementById('flatFee').value.length === 0)
		{
			alert('Please enter a flat fee value greater than 0');
			return;
		}
		
		 if (isNaN(document.getElementById('flatFee').value))
			{
				alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
				document.getElementById('flatFee').focus();
				return (false);
		
			}
			else
			{
				if (document.getElementById('flatFee').value < 0)
				{
		
			    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
			    	document.getElementById('flatFee').focus();
			    	return (false);
				}
				else
				{
					document.getElementById('flatFee').value = roundNumber(document.getElementById('flatFee').value,decimalSupported);
				}
			}
			
			
		if(document.getElementById('percentageFee').value.length === 0)
		{
			alert('Please enter percentage fee between 0 and 100');
			return;
		}
		if (isNaN(document.getElementById('percentageFee').value))
    	{
    		alert('Please enter percentage fee between 0 and 100');
    		document.getElementById('percentageFee').focus();
    		return (false);
    	}
    	else{
		if(document.getElementById('percentageFee').value<0 || document.getElementById('percentageFee').value>100)
	       {
 	        	alert('Percentage Fee should be between 0 and 100');
   				c.focus();
   				return (false);
	       }
 	       else
 	       {
 	          document.getElementById('percentageFee').value=roundNumber(document.getElementById('percentageFee').value,decimalSupported);
 	       }
		}
	 if(check)
		{
		 document.getElementById('destinationCurrency').value=document.getElementById('ddlModel').options[document.getElementById('ddlModel').selectedIndex].value;
		}
  		document.crossBorderExchangeRateForm.submit();
	}
	
</script>
</head>
<%
	if(request.getParameter("type") != null)
	{
			Vector countryResults=CrossBorderExchangeRate.getISOCountry(request,SessionData);
			Iterator i=countryResults.iterator();
			vecTemp = (Vector) i.next();
			String iso=vecTemp.get(0).toString();
			String country=vecTemp.get(1).toString();
			String currency=vecTemp.get(2).toString();
			String countryId=vecTemp.get(3).toString();
		if(request.getParameter("type").equals("UPDATE"))
		{
			String id = request.getParameter("editId");
			Vector vecSearchResults = CrossBorderExchangeRate.getEditExchangeRate(id,SessionData);
			Iterator it = vecSearchResults.iterator();
			vecTemp = (Vector) it.next();
%>
			<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
				<tr>
					<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
					<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.updatexBorderExchangeRate.title",SessionData.getLanguage()).toUpperCase()%></td>
					<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
				</tr>
				<tr>
					<td colspan="3"  bgcolor="#FFFFFF">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
						<th>Exchange Rate Routes from <%=country%> ( <%=iso%> )</th>
							<tr>
								<td class=formArea>
									<table align=center>
										<tr>
											<td colspan="3">					
												<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
													<tr>
														<td class="main">
															<br>
															<table width="100%" cellspacing="1" cellpadding="2">
																<form name=crossBorderExchangeRateForm method="post" action="admin/tools/xBorderExchangeRate.jsp">
																<input type=hidden name=type value=UPDATE />
																<input type=hidden name=id value="<%=vecTemp.get(10)%>"/>
																<input type=hidden name=oldRate value="<%=vecTemp.get(4)%>" />
																<input type=hidden name=oldFlatFee value=" <%=vecTemp.get(5)%>" />
																<input type=hidden name=oldPercentageFee value=" <%=vecTemp.get(6)%>" />
	<tr>
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.destinationCountry",SessionData.getLanguage())%></td>
																	<td><input type="text" value="<%=vecTemp.get(0)%>" id="country" name="country" disabled></td>
																</tr>
																<tr>												
																	<td class="rowhead2" valign=top><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.originCurrency",SessionData.getLanguage())%></td> 
																	<td align="left">
																		<input type="text" name="originCurrency" value="<%=vecTemp.get(2)%>" id="originCurrency" disabled></td>
																</tr>	
																
																<tr>												
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.destinationCurrency",SessionData.getLanguage())%></td> 
																	 <td align="left">
																		<input type="text" name="destinationCurrency" value="<%=vecTemp.get(3)%>" id="destinationCurrency" disabled></td>
																	</td>
																</tr>	
																
																
															<tr>
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.product",SessionData.getLanguage())%></td>
																	<td align="left">
																		<input type="text" name="product" value="<%=vecTemp.get(1)%>" id="product" disabled></td>
																	</td>
																</tr>  	
																
																<tr>												
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.rate",SessionData.getLanguage())%></td> 
																	<td align="left">
																		<input type="text"  value="<%=vecTemp.get(4)%>" id="rate" name="rate" /> 
																		
																	</td>
																</tr>	
																<tr>													
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.flatFee",SessionData.getLanguage())%></td>  
																	<td align="left">
																		<input type="text" id="flatFee" value="<%=vecTemp.get(5)%>" name="flatFee" size=20 /> 
																		
																	</td>
																</tr>
																<tr>													
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.percentageFee",SessionData.getLanguage())%></td>  
																	<td align="left">
																		<input type="text" id="percentageFee" value="<%=vecTemp.get(6)%>" name="percentageFee" size=20"/>
																		
																	</td>
																</tr>
																<tr>													
																	<td>
																	</td>
																</tr>
																<tr>													
																	  
																	<td align="left" colspan="2">
																		<%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.note",SessionData.getLanguage())%>												
																	</td>
																</tr>
																</form>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table align="center">
										<tr>
											<td>
												<table align="center">
													<tr>
														<td>
															<input type="button" name="button" value="<%=Languages.getString("jsp.incudes.menu.updateButton",SessionData.getLanguage())%>" onClick=submitform(false);>
															<form action="admin/tools/xBorderExchangeRate.jsp">
																<input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.cancelInsert",SessionData.getLanguage())%>">
															</form>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
<%			
		}
		else if(request.getParameter("type").equals("INSERT"))
		{
			
%>
			<body onload="onCountryChange()">
			<table border="0" cellpadding="0" cellspacing="0" width="750"  background="images/top_blue.gif">
				<tr>
					<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
					<td background="images/top_blue.gif" width="100%" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.addxBorderExchangeRate.title",SessionData.getLanguage())%></td>
					<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
				</tr>
				<tr>
					<td colspan="3"  bgcolor="#FFFFFF">
						<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
						<th>Exchange Rate Routes from <%=country%> ( <%=iso%> )</th>
							<tr>
								<td class=formArea>
									<table align=center>
										<tr>
											<td colspan="3">					
												<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
												
													<tr>
														<td class="main">
															<br>
															<table width="100%" cellspacing="1" cellpadding="2">
																<form name=crossBorderExchangeRateForm method="post" action="admin/tools/xBorderExchangeRate.jsp">
																<input type=hidden name=type value=INSERT />
																<input type=hidden name=destinationCurrency id=destinationCurrency value=""/>
																<input type=hidden name=origCountry value="<%=countryId%>"/>
																<input type=hidden name=originCurrency value="<%=currency%>"/>
																<tr>
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.destinationCountry",SessionData.getLanguage())%></td>
																	<td><select name="xborderCountries" id="countries" onchange="onCountryChange()">
																	         
																	<%
																	  Vector vecCountryList = CrossBorderExchangeRate.getCountries(SessionData);
																	  Iterator it = vecCountryList.iterator();
																	  while (it.hasNext())
																	  {
																	    Vector vecCoun = null;
																	    vecCoun = (Vector) it.next();
																	    out.println("<option value=" + "'"+vecCoun.get(0)+"'" +">" + vecCoun.get(1) + "</option>");
																	  }
																	
																	%>
																	 </select>
																</tr>
																<tr>												
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.originCurrency",SessionData.getLanguage())%></td> 
																	<td align="left">
																		<input type="text" name="originCurrencyDis" value="<%=currency%>" id="originCurrencyDis" disabled>

																	</td>
																</tr>	
																
																<tr>												
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.destinationCurrency",SessionData.getLanguage())%></td> 
																	<td align="left">
																	 <select name="model" id="ddlModel" disabled>
																	  
																		<!--  <label for="destinationCurrency"></label>-->

																	</td>
																</tr>	
																
																
															<tr>
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.product",SessionData.getLanguage())%></td>
																	<td><select name="products" id="products" ">
																	         
																	<%
																	  Vector vecProductList = CrossBorderExchangeRate.getCrossborderProducts(SessionData);
																	   it = vecProductList.iterator();
																	  while (it.hasNext())
																	  {
																	    Vector vecProd = null;
																	    vecProd = (Vector) it.next();
																	    out.println("<option value=" +vecProd.get(0) +">" + vecProd.get(1) + "("+vecProd.get(0)+")</option>");
																	  }
																	
																	%>
																	 </select>
																</tr>  	
																
																<tr>												
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.rate",SessionData.getLanguage())%></td> 
																	<td align="left">
																		<input type="text"  id="rate" name="rate"/> 
																	</td>
																</tr>	
																<tr>													
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.flatFee",SessionData.getLanguage())%></td>  
																	<td align="left">
																		<input type="text" id="flatFee" name="flatFee" size="20" /> 
																	</td>
																</tr>
																<tr>													
																	<td class="rowhead2" valign="top"><%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.title.percentageFee",SessionData.getLanguage())%></td>  
																	<td align="left">
																		<input type="text" id="percentageFee" name="percentageFee" size="20"/> 
																	</td>
																</tr>
																<tr>													
																	<td>
																	</td>
																</tr>
																<tr>													
																	  
																	<td align=left colspan="2">
																	<%=Languages.getString("jsp.includes.menu.xBorderExchangeRate.note",SessionData.getLanguage())%>
																	</td>
																</tr>
																</form>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									<table align=center>
										<tr>
											<td>
												<table align=center>
													<tr>
														<td>
															<input type="button" name="button" value="<%=Languages.getString("jsp.incudes.menu.addButton",SessionData.getLanguage())%>" onClick=submitform(true);>
															<form action="admin/tools/xBorderExchangeRate.jsp">
																<input type="submit" name="submit" value="<%=Languages.getString("jsp.includes.menu.bonusPromoToolsSMS.cancelInsert",SessionData.getLanguage())%>">
															</form>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</body>
<%
		}
	}
%>
<%@ include file="/includes/footer.jsp" %>