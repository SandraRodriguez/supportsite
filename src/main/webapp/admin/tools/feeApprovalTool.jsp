<%@ page import="com.debisys.languages.Languages,
				 com.debisys.customers.Merchant,
				 com.debisys.reports.TransactionReport,
				 com.debisys.utils.StringUtil,
				 com.debisys.utils.DateUtil,
				 com.debisys.utils.NumberUtil,
				 java.util.Vector,
				 java.util.Iterator,
				 java.util.Enumeration,
				 java.net.URLEncoder,
				 java.util.*,
				 com.debisys.utils.HTMLEncoder,
				 com.debisys.customers.Rep,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.FeeReports" %>
<%@page import="com.debisys.ach.TransactionSearch"%>
<%
int section = 9;
int section_page = 25;
boolean bShowLabelLimitDays=false;
int limitDays = 0;
String startDate = "";
String endDate = "";
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<style>
	.toolTip {padding-right: 20px;background:url(images/help.gif) no-repeat right;color: #3366FF;cursor: help;position: relative; with: 18px; height: 18px; display: block; }
	.toolTipWrapper {width: 375px;position: absolute;top: -20px; color: #FFFFFF; padding-left:40px;font-weight: bold;font-size: 9pt; z-index: 100;}
	.toolTipTop {width: 175px;height: 30px;background: transparent;}
	.toolTipMid {padding: 8px 15px;background: #A1D40A url(images/bubbleMid.gif) repeat-x top;}
	.toolTipBtm {height: 13px;background: url(images/bubbleBtmLarge.gif) }
	
</style>
<script>
	$(function() {
		//$( document ).tooltip();
		$(document).ready(function(){
			$('.toolTip').hover(
				function() {
					this.tip = this.title;
					$(this).append('<div class="toolTipWrapper"><div class="toolTipTop"></div><div class="toolTipMid">'+this.tip+'</div><div class="toolTipBtm"></div></div>');
					this.title = "";
					this.width = $(this).width();
					$(this).find('.toolTipWrapper').css({left:this.width-22});
					$('.toolTipWrapper').fadeIn(300);
				},
				function() {
					$('.toolTipWrapper').fadeOut(100);
					$(this).children().remove();
					this.title = this.tip;
				}
			);
		});
	});
</script> 
<link href="default.css" type="text/css" rel="stylesheet">
<%
Vector vResults = new Vector();


if ( request.getParameter("frmApply") != null ){

	try{
		boolean bResultsProcessed = false;
		Enumeration enParams = request.getParameterNames();
%>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
<%
		int nRow = 1;

		while ( enParams.hasMoreElements() ){//Thru request params
			String sNextId = enParams.nextElement().toString();
			if ( sNextId.startsWith("chk_") ){//If user made a change in results
				boolean bPending = false;
				boolean bApply = false;
				boolean bDenied = false;

				if ( !bResultsProcessed ){
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
				<thead>
					<tr class="SectionTopBorder">
					
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.assessedDate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeProductName",SessionData.getLanguage()).toUpperCase())%>&nbsp;&nbsp;</td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeCriteria",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeRecurrence",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.assessedId",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.busName",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.billedId",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.busName2",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.chargeInS",SessionData.getLanguage()).toUpperCase())%>&nbsp;&nbsp;</td>
						<td class=rowhead2 align=center valign="top" title="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.pendingApproval",SessionData.getLanguage())%>"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.pendingApproval1",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top" title="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.approved",SessionData.getLanguage())%>"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.approved1",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top" title="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.denied",SessionData.getLanguage())%>"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.denied1",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.result",SessionData.getLanguage()).toUpperCase())%></td>
					</tr>
				</thead>
<%
					bResultsProcessed = true;
				}
				
				int nRequestNumber = Integer.parseInt(sNextId.replaceAll("chk_", ""));
				if ( request.getParameter(sNextId).equals("p") ){
					bPending = true;
				}
				else if ( request.getParameter(sNextId).equals("a") ){
					bApply = true;
				}
				else if ( request.getParameter(sNextId).equals("d") ){
					bDenied = true;
				}
%>
				<tr class="rowNowRap<%=nRow%>">
					
					<td align="left"><%=request.getParameter("assessed_date_" + nRequestNumber)%></td>
					<td align="left"><%=request.getParameter("fee_name_" + nRequestNumber)%></td>
					<td align="left"><%=request.getParameter("fee_criteria_type_id_" + nRequestNumber)%></td>
					<td align="left"><%=request.getParameter("fee_recurrence_type_id_" + nRequestNumber)%></td>
					<td align="right"><%=request.getParameter("assessed_entity_id_" + nRequestNumber)%></td>
					<td align="left"><%=request.getParameter("assessed_entity_id_name_" + nRequestNumber)%></td>
					<td align="right"><%=request.getParameter("billed_entity_id_" + nRequestNumber)%></td>
					<td nowrap><%=request.getParameter("assessed_entity_id_name_" + nRequestNumber)%></td>
					<td align="right"><%=NumberUtil.formatAmount(request.getParameter("charge_" + nRequestNumber))%></td>					
					
					<td align=center><%=(bPending)?Languages.getString("jsp.admin.reports.paymentrequest_search.yes",SessionData.getLanguage()):Languages.getString("jsp.admin.reports.paymentrequest_search.no",SessionData.getLanguage())%></td>
					<td align=center><%=(bApply)?Languages.getString("jsp.admin.reports.paymentrequest_search.yes",SessionData.getLanguage()):Languages.getString("jsp.admin.reports.paymentrequest_search.no",SessionData.getLanguage())%></td>
					<td align=center><%=(bDenied)?Languages.getString("jsp.admin.reports.paymentrequest_search.yes",SessionData.getLanguage()):Languages.getString("jsp.admin.reports.paymentrequest_search.no",SessionData.getLanguage())%></td>
<%
				if ( bPending ){
%>
					<td align=center><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.pending",SessionData.getLanguage())%></td>
<%
				}
				else if ( bApply ){
					int errors = 0;
					try{
						errors = FeeReports.updateFeeTransactionStatus(1, nRequestNumber);
					}
					catch (Exception ex){
						errors = 1;
					}
					if (errors == 0 ){
%>
						<td align=center><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.approved",SessionData.getLanguage())%></td>
<%
					}
					else{//Else failed
%>					
					<td>
						<%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%><br>
					</td>
<%
					}
				}
				else if ( bDenied ){
				
					int errors = 0;
					try{
						errors = FeeReports.updateFeeTransactionStatus(2, nRequestNumber);
					}
					catch (Exception ex){
						errors = 1;
					}	
					if (errors == 0 ){
%>
						<td align=center><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.denied",SessionData.getLanguage())%></td>
<%
					}
					else{
%>
						<td align=center><%=Languages.getString("jsp.admin.reports.pinreturn_search.failed",SessionData.getLanguage())%></td>
<%
					}
				}
%>
				</tr>
<%
				nRow = (nRow == 1)?2:1;
			}
		}
		if ( bResultsProcessed ){
%>
			</table>
		</td>
<%
		}
		else{
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class=main><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.noitemschanged",SessionData.getLanguage())%></td></tr>
			</table>
		</td>
<%
		}
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan=3 align=center>
			<form method="post" action="admin/tools/feeApprovalTool.jsp">
				<input type=submit class="plain" value="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.backtosearch",SessionData.getLanguage())%>">
			</form>
		</td>
	</tr>
</table>
<%
	}
	catch (Exception e){
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b>ERROR IN PAGE</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class=main><%e.printStackTrace();%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}
}

else if ( request.getParameter("download") != null ){

	int selectEntityType = 0;
	int nStatusId = 0;
	String[] entitiesId = new String[]{""};
	String productIdSelected = "";
	if ( request.getParameter("statusId") != null ){
		nStatusId = Integer.parseInt(request.getParameter("statusId"));
	}
	if ( request.getParameter("productIdSelected") != null ){
		productIdSelected = request.getParameter("productIdSelected");
	}

	if ( request.getParameter("selectEntityType") != null){
		selectEntityType = Integer.parseInt(request.getParameter("selectEntityType"));
	}
	
	if ( request.getParameterValues("entityIds") != null ){
		entitiesId = request.getParameterValues("entityIds");
	}
	
	startDate = request.getParameter("txtStartDate");
	endDate = request.getParameter("txtEndDate");
	if(startDate != null && endDate != null &&!startDate.trim().equals("") && !endDate.trim().equals("")){
		limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays("feeApprovalTool", application);
		
	   	int daysBetween = DateUtil.getDaysBetween(endDate,startDate );
	   	if ( daysBetween > limitDays){
			startDate = DateUtil.addSubtractDays(endDate,-limitDays);
			bShowLabelLimitDays = true;
	   	}
   	}
		
	String sURL = FeeReports.downloadFeeManagementReport(startDate, endDate, nStatusId, productIdSelected, selectEntityType, entitiesId, SessionData,application);
	response.sendRedirect(sURL);
    return;
}

else if ( request.getParameter("defStatusId") != null ){
	try{
		int selectEntityType = 0;
		int nStatusId =0;
		String[] entitiesId = new String[]{""};
		String productIdSelected = "";

		if ( request.getParameter("statusId") != null ){
			nStatusId = Integer.parseInt(request.getParameter("statusId"));
		}
		if ( request.getParameter("productIdSelected") != null ){
			productIdSelected = request.getParameter("productIdSelected");
		}

		if ( request.getParameter("selectEntityType") != null){
			selectEntityType = Integer.parseInt(request.getParameter("selectEntityType"));
		}
		
		if ( request.getParameterValues("entityIds") != null ){
			entitiesId = request.getParameterValues("entityIds");
		}
		
		startDate = request.getParameter("txtStartDate");
		endDate = request.getParameter("txtEndDate");
		if(!startDate.trim().equals("") && !endDate.trim().equals("")){
			limitDays = com.debisys.utils.DbUtil.getReportIntervalLimitDays("feeApprovalTool", application);
			
		   	int daysBetween = DateUtil.getDaysBetween(endDate,startDate );
		   	if ( daysBetween > limitDays){
				startDate = DateUtil.addSubtractDays(endDate,-limitDays);
				bShowLabelLimitDays = true;
		   	}
	   	}
	   	
	   	%>
		<FORM ACTION="admin/tools/feeApprovalTool.jsp" METHOD="post" ONSUBMIT="document.getElementById('btnSubmitDownload').disabled = true;">
			<INPUT TYPE="hidden" NAME="download" VALUE="Y">
			<input type="hidden" name="statusId" value="<%=nStatusId%>">
			<input type="hidden" name="productIdSelected" value="<%=productIdSelected%>">
			<input type="hidden" name="selectEntityType" value="<%=selectEntityType%>">
			<input type="hidden" name="txtStartDate" value="<%=startDate%>">
			<input type="hidden" name="txtEndDate" value="<%=endDate%>">
			<%
			for(int i=0;i<entitiesId.length;i++){%>
				<input type="hidden" name ="entityIds" value="<%=entitiesId[i]%>">
			<%}%>
			<INPUT ID="btnSubmitDownload" TYPE="submit" VALUE="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.download",SessionData.getLanguage())%>" >
		</FORM>
		<%	
		vResults = FeeReports.getFeeManagementReport(startDate, endDate, nStatusId, productIdSelected, selectEntityType, entitiesId, SessionData);
		
	}
	catch (Exception e)
	{
%>
	<table border="0" cellpadding="0" cellspacing="0" width="60%">
		<tr>
			<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b>ERROR IN PAGE</b></td>
			<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
				<table width="100%" border="0" cellspacing="0" cellpadding="2">
					<tr><td class=main><%=Languages.getString("jsp.admin.reports.paymentrequest_search.checklog",SessionData.getLanguage())%></td></tr>
				</table>
			</td>
		</tr>
		<tr><td><br><br></td></tr>
	</table>
<%
	}
%>

<script>
function checkAll(status){
	var currentCheck = '';

	<%
	for ( int i = 0; i < vResults.size(); i++ ){
		Vector vTemp = (Vector)vResults.get(i);%>
		currentCheck = document.getElementsByName('chk_<%=vTemp.get(0)%>');
		
		if(status == "p"){
			currentCheck[0].checked = "true";			
		}
		else if(status == "a"){
			currentCheck[1].checked = "true";
		}
		else if(status == "d"){
			currentCheck[2].checked = "true";
		}
	<%}%>
}
	


</script>

<link href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script src="includes/sortROCRows.js" type="text/javascript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
<%
	if ( vResults.size() > 0 ){//If there are results
		boolean bShowOptions = false;
		bShowOptions = ((Vector)vResults.get(0)).get(13).toString().equals("0");
	    
		if ( bShowOptions ){
%>
		<form method="post" action="admin/tools/feeApprovalTool.jsp" onsubmit="return confirm('<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.confirmForm",SessionData.getLanguage())%>');">
<%
		}
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class=main><%
						if (bShowLabelLimitDays){
							out.println(" <span class=main>" + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  startDate + 
                          	" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + endDate + "</span>");
	                	%>
	                	
		                	<br/>
		                	<Font style="color: red">
		                	* <%=Languages.getString("jsp.admin.customers.noteHistory1",SessionData.getLanguage())%>
		                                                <%=" "+limitDays+" "%>
		                                                <%=Languages.getString("jsp.admin.customers.noteHistory2",SessionData.getLanguage())%>
		                    </Font>                            
	                	<%                 	
						}
						
						 %></td></tr>
			</table>
			<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1" style="display:inline;" >
				<thead>
					<tr  class="SectionTopBorder">
						<td class=rowhead2>
						</td>
				<%
						if ( bShowOptions )
						{
				%>
						<td class=rowhead2 align=center valign="top" title="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.pendingApproval",SessionData.getLanguage())%>"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.pendingApproval1",SessionData.getLanguage()).toUpperCase())%>
							<input type="radio" name="check_all" value="p" onclick="checkAll('p');">
						</td>
						<td class=rowhead2 align=center valign="top" title="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.approved",SessionData.getLanguage())%>"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.approved1",SessionData.getLanguage()).toUpperCase())%>
							<input type="radio" name="check_all" value="a" onclick="checkAll('a');">
						</td>
						<td class=rowhead2 align=center valign="top" title="<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.denied",SessionData.getLanguage())%>"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.denied1",SessionData.getLanguage()).toUpperCase())%>
							<input type="radio" name="check_all" value="d" onclick="checkAll('d');">
						</td>
				<%
						}
				%>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.details",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.assessedDate",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeProductName",SessionData.getLanguage()).toUpperCase())%>&nbsp;&nbsp;</td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeCriteria",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeRecurrence",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.assessedId",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.busName",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.billedId",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.busName2",SessionData.getLanguage()).toUpperCase())%></td>
						<td class=rowhead2 align=center valign="top"><%=StringUtil.formatTableCellTitle(Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.chargeInS",SessionData.getLanguage()).toUpperCase())%>&nbsp;&nbsp;</td>
					</tr>
				</thead>
<%
		int nRow = 1;
		for ( int i = 0; i < vResults.size(); i++ ){
			Vector vTemp = (Vector)vResults.get(i);
%>
				<tr class="rowNowRap<%=nRow%>">
					<td><%=(i + 1)%></td>
	<%
			if ( bShowOptions ){
	%>
						<td align="center">
						
							<input type=hidden name="assessed_date_<%=vTemp.get(0)%>" value="<%=DateUtil.formatDateTime((java.sql.Timestamp)vTemp.get(1))%>">
							<input type=hidden name="fee_name_<%=vTemp.get(0)%>" value="<%=vTemp.get(3)%>">
							<input type=hidden name="fee_criteria_type_id_<%=vTemp.get(0)%>" value="<%=vTemp.get(4)%>">
							<input type=hidden name="fee_recurrence_type_id_<%=vTemp.get(0)%>" value="<%=vTemp.get(5)%>">
							<input type=hidden name="assessed_entity_id_<%=vTemp.get(0)%>" value="<%=vTemp.get(6)%>">
							<input type=hidden name="assessed_entity_id_name_<%=vTemp.get(0)%>" value="<%=vTemp.get(7)%>">
							<input type=hidden name="billed_entity_id_<%=vTemp.get(0)%>" value="<%=vTemp.get(8)%>">
							<input type=hidden name="assessed_entity_id_name_<%=vTemp.get(0)%>" value="<%=vTemp.get(9)%>">
							<input type=hidden name="charge_<%=vTemp.get(0)%>" value="<%=vTemp.get(10)%>">
							
							<input type=hidden name="register_<%=vTemp.get(0)%>" value="<%=vTemp.get(0)%>">
							<input type=radio name="chk_<%=vTemp.get(0)%>" value="p" checked >
						</td>
						<td align=center><input type=radio name="chk_<%=vTemp.get(0)%>" value="a" ></td>
						<td align=center><input type=radio name="chk_<%=vTemp.get(0)%>" value="d" ></td>
	<%
			}
	%>				
					<td>
					
					<span class="toolTip" title=" <%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.detalleTransaction",SessionData.getLanguage())%><br/><br/> 
					<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.notes",SessionData.getLanguage())%>:<br/><%=vTemp.get(11)%><br/><br/>
					<%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.feeAssessmentNote",SessionData.getLanguage())%>:<br/> <%=vTemp.get(12)%>">
					</span>
					
					</td>
					
					<td align="left" nowrap><%=DateUtil.formatDateTime((java.sql.Timestamp)vTemp.get(1))%></td><!-- assessed_date -->
					<td align="left" nowrap><%=vTemp.get(3)%></td><!-- fee_name product-->
					<td align="left" nowrap><%=vTemp.get(4)%></td><!-- fee_criteria_type_id -->
					<td align="left" nowrap><%=vTemp.get(5)%></td><!-- fee_recurrence_type_id -->
					<td align="right" nowrap><%=vTemp.get(6)%></td><!-- assessed_entity_id -->
					<td align="left" nowrap style="size: 100px"><%=vTemp.get(7)%></td><!-- assessed_entity_id name -->
					<td align="right" nowrap><%=vTemp.get(8)%></td><!-- billed_entity_id -->
					<td align="left" nowrap><%=vTemp.get(9)%></td><!-- billed_entity_id name -->
					<td align="right" nowrap>$<%=NumberUtil.formatAmount(vTemp.get(10).toString())%></td><!-- charge -->
					
					
				</tr>
<%
			nRow = (nRow == 1)?2:1;
		}//End of for
%>
			</table>
			<script type="text/javascript">
<%
		if ( bShowOptions ){
%>
				//var stT1 = new SortROCRows(document.getElementById("t1"), ["None", "None", "None", "None", "DateTime","CaseInsensitiveString","CaseInsensitiveString", "CaseInsensitiveString","Number", "CaseInsensitiveString", "Number","CaseInsensitiveString", "Number","None"], 0, false, false);
<%
		}
		else{
%>
				var stT1 = new SortROCRows(document.getElementById("t1"), ["None","None", "DateTime","CaseInsensitiveString","CaseInsensitiveString", "CaseInsensitiveString","Number", "CaseInsensitiveString", "Number","CaseInsensitiveString", "Number"], 0, false, false,"rowNowRap1","rowNowRap2");
<%
		}
%>
			</script>
		</td>
<%
		if ( bShowOptions ){
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan="3" align="center">
			<table>
				<tr>
					<td>
						<input type="submit" id="btnSubmit" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.applychanges",SessionData.getLanguage())%>">
					</td>
					<td>&nbsp;&nbsp;&nbsp;</td>
					<td>
						<input type="reset" id="btnReset" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.resetchanges",SessionData.getLanguage())%>">
					</td>
				</tr>
			</table>
		</td>
		<input type="hidden" name="frmApply" value="y">
		</form>
<%
		}
	}//End of if there are results
	else{//Else show the message of no results
%>
		<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
				<tr><td class=main><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.noresults",SessionData.getLanguage())%></td></tr>
			</table>
		</td>

<%
	}//End of else show the message of no results
%>
	</tr>
	<tr><td><br><br></td></tr>
	<tr>
		<td colspan=3 align=center>
			<form method="post" action="admin/tools/feeApprovalTool.jsp">
				<input type=submit class="plain" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.backtosearch",SessionData.getLanguage())%>">
			</form>
		</td>
	</tr>
</table>
<%
}//End of if a search was made
else
{//Else we need to show the search page
%>
<script>
  var nSUBMITTEDID = 0;
  function ToggleDateFilters(chkAllOpen)
  {
    if ( chkAllOpen.checked )
    {
      document.getElementById('txtStartDate').readOnly = true;
      document.getElementById('txtEndDate').readOnly = true;
      document.getElementById('txtStartDate').value = '';
      document.getElementById('txtEndDate').value = '';
      document.getElementById('linkStartDate').onclick = new Function('return true;');
      document.getElementById('linkEndDate').onclick = new Function('return true;');
      document.getElementById('ddlStatus').disabled = true;
      document.getElementById('ddlStatus').value = nSUBMITTEDID;
    }
    else
    {
      document.getElementById('txtStartDate').readOnly = false;
      document.getElementById('txtEndDate').readOnly = false;
      document.getElementById('linkStartDate').onclick = new Function(document.getElementById('linkStartDate').getAttribute("_onclick"));//_onclick
      document.getElementById('linkEndDate').onclick = new Function(document.getElementById('linkEndDate').getAttribute("_onclick"));
      document.getElementById('ddlStatus').disabled = false;
    }
  }//End of function ToggleDateFilters
  
  
  function FilterRequestNumber(txtRequestNumber)
  {
    var sText = txtRequestNumber.value;
    var sResult = "";
    for ( i = 0; i < sText.length; i++ )
    {
      if ( (sText.charCodeAt(i) >= 48) && (sText.charCodeAt(i) <= 57) )
      {
        sResult += sText.charAt(i);
      }
    }
    if ( sText != sResult )
    {
      txtRequestNumber.value = sResult;
    }
  }//End of function FilterRequestNumber
  
  function ValidateForm()
  {
    var sMessage = '';
    if ( !document.getElementById('chkAllOpen').checked )
    {
      if ( document.getElementById('txtStartDate').value.length == 0 )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.paymentrequest_search.emptystartdate",SessionData.getLanguage())%>\n';
      }
      else if ( !document.getElementById('txtStartDate').value.match('^(0?[1-9]|1[012])\/(0?[1-9]|1[0-9]|2[0-9]|3[01])\/(19|20)[0-9][0-9]$') )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.paymentrequest_search.badstartdate",SessionData.getLanguage())%>\n';
      }

      if ( document.getElementById('txtEndDate').value.length == 0 )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.paymentrequest_search.emptyenddate",SessionData.getLanguage())%>\n';
      }
      else if ( !document.getElementById('txtEndDate').value.match('^(0?[1-9]|1[012])\/(0?[1-9]|1[0-9]|2[0-9]|3[01])\/(19|20)[0-9][0-9]$') )
      {
        sMessage += '<%=Languages.getString("jsp.admin.reports.paymentrequest_search.badenddate",SessionData.getLanguage())%>\n';
      }

      if ( sMessage.length > 0 )
      {
        alert(sMessage);
        return false;
      }
    }
    document.getElementById('btnSearch').disabled = true;
    return true;
  }//End of function ValidateForm
  
  function onChangeActor(){
  	  var ctlLevel = $("#selectEntityType").val();
	  var actorVal = "";
	  var repList =  $("#entityIds");
	  repList.empty();
		//alert('temp');
	  actorVal = <%=SessionData.getProperty("ref_id")%>;
	  
	  
	  //$.getJSON("/support/includes/ajax.jsp?class=com.debisys.customers.Merchant&method=getMerchantsByActorAjax&value=" + ctlLevel + "_" + actorVal + "_<%=strDistChainType%>&rnd=" + Math.random(),
	    $.getJSON("/support/includes/ajax.jsp?class=com.debisys.customers.Rep&method=getEntityListByIsoAjax&value=" + ctlLevel + "_" + actorVal + "_<%=strDistChainType%>&rnd=" + Math.random(),
	          function(data)
	          {
	            var option = $("<option>");
	            option.val("");
	            option.text("<%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%>");
	            option.appendTo(repList);
	            
	            $.each(data.items, function(i, item)
	            {
	              var option = $("<option>");
	              option.val(item.rep_id);
	              item.businessname = unescape(URLDecode(item.businessname));
	              item.businessname = item.businessname.replace(/\+/g, " ");
	              option.text(item.businessname);
	              option.appendTo(repList);
	              
	            });
	            repList.val("<%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%>");
	          });
	  //terminalType_onclick(document.getElementById("terminalType"));
}

function URLDecode(utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;
 
    while ( i < utftext.length ) {
 
      c = utftext.charCodeAt(i);
 
      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }
      else if((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i+1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }
      else {
        c2 = utftext.charCodeAt(i+1);
        c3 = utftext.charCodeAt(i+2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }
 
    }
 
    return string;
  }
  
</script>
<form name="mainform" method="post" action="admin/tools/feeApprovalTool.jsp" onsubmit="return ValidateForm();">
<table border="0" cellpadding="0" cellspacing="0" width="60%">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000"><b><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
	</tr>
	<tr>
		<td colspan=3>
			<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
				<tr>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
					<td align="left" valign="top" bgcolor="#FFFFFF">
						<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td>&nbsp;</td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
								<td>&nbsp;</td>
							    <td >
							    	<input type="text" readonly=true maxlength=10 class="plain" name="txtStartDate" id="txtStartDate" value="" size="12">
							    	<a id="linkStartDate" href="javascript:void(0)" 
							    		_onclick="if(self.gfPop)gfPop.fStartPop(document.getElementById('txtStartDate'),document.getElementById('txtEndDate'));return false;" HIDEFOCUS>
							    		<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
							    	</a>
							    </td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<input type="text" readonly=true maxlength=10 class="plain" name="txtEndDate" id="txtEndDate" value="" size="12">
							    		<a id="linkEndDate" href="javascript:void(0)" 
							    			_onclick="if(self.gfPop)gfPop.fEndPop(document.getElementById('txtStartDate'),document.getElementById('txtEndDate'));return false;" HIDEFOCUS>
							    			<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
							    		</a>
							    </td>
								<td></td>
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.allopenrequests",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td><input type=checkbox class="plain" name="allopen" id="chkAllOpen" checked onclick="ToggleDateFilters(this);"></td>
								<td></td>
								
							</tr>
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.status",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<select name="statusId" id="ddlStatus" disabled  style="width: 300px">
							    	
							    		<option selected value="0"><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.statusAwaitingApproval",SessionData.getLanguage())%></option>
							    		<option value="1"><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.statusApproved",SessionData.getLanguage())%></option>
							    		<option value="2"><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.statusDenied",SessionData.getLanguage())%></option>

									</select>
									<% int nSUBMITTEDID = 0;%>
									<input type=hidden name=defStatusId value=<%=nSUBMITTEDID%>>
									<script>nSUBMITTEDID = <%=nSUBMITTEDID%>;</script>
								</td>
								<td></td>
							</tr>
							
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.product",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<select name="productIdSelected" id="productIdSelected"  style="width: 300px">
							    		<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
										<%
									    Vector vec = FeeReports.getFeeProductsList(SessionData);
									    Iterator it0 = vec.iterator();
									    while (it0.hasNext()){
									      Vector vTemp0 = null;
									      vTemp0 = (Vector) it0.next();
									      out.println("<option value=" + vTemp0.get(0) +">" + vTemp0.get(1) + "</option>");
									    }
										%>
									</select>
								</td>
								<td></td>
							</tr>
							
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.selectEntity",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td></td>
								<td></td>
							</tr>
							
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.entityType",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<select name="selectEntityType" id="selectEntityType" onchange="onChangeActor();"  style="width: 300px">
							    	<%if(strAccessLevel.equals(DebisysConstants.ISO) && !strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){ %>
							    		<option selected value="<%=DebisysConstants.AGENT%>"><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.agent",SessionData.getLanguage())%></option>
							    		<option value="<%=DebisysConstants.SUBAGENT%>"><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.subAgent",SessionData.getLanguage())%></option>
							    	<%} %>
							    		<option value="<%=DebisysConstants.REP%>"><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.rep",SessionData.getLanguage())%></option>
										<option value="<%=DebisysConstants.MERCHANT%>"><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.merchant",SessionData.getLanguage())%></option>
									</select>
								</td>
								<td></td>
							</tr>
							
							<tr>
								<td></td>
							    <td class="main" nowrap><%=Languages.getString("jsp.admin.reports.tool.fee.feeApprovalTool.select",SessionData.getLanguage())%>:</td>
								<td></td>
							    <td>
							    	<select id="entityIds" name="entityIds" size=10 multiple>
										<option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
										<%
									    Vector v = TransactionSearch.getAgentList(SessionData, "");
									    Iterator it = v.iterator();
									    while (it.hasNext()){
									      Vector vTemp = null;
									      vTemp = (Vector) it.next();
									      out.println("<option value=" + vTemp.get(0) +">" + vTemp.get(1) + "</option>");
									    }
									%>
									</select>
								</td>
								<td></td>
							</tr>
							
							<tr><td><br></td></tr>
							<tr>
								<td colspan=5 align=center><input type=submit id="btnSearch" class="plain" value="<%=Languages.getString("jsp.admin.reports.paymentrequest_search.search",SessionData.getLanguage())%>"></td>
							</tr>
							<tr><td><br></td></tr>
						</table>
					</td>
					<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
				</tr>
				<tr>
					<td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	            </tr>
			</table>
		</td>
	</tr>
</table>
								<script type="text/javascript">
									ToggleDateFilters(document.getElementById('chkAllOpen'));
								</script>
</form>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%
}//End of else we need to show the search page
%>
<%@ include file="/includes/footer.jsp" %>