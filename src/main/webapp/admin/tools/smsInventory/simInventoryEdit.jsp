<%-- 
    Document   : simInventoryFindEdit
    Created on : Nov 10, 2016, 1:22:52 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.simInventory.BatchesInfo"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 9;
%>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>

<script src="js/simInventory.js" type="text/javascript"></script>
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}

    .titles{
        font-weight: bold;
    }

</style>


<script type="text/javascript">

    $(function () {
        $(document).ready(function () {
            $("#dialog-message").hide();
        });
    });

    

</script>

<%    String batchId = request.getParameter("batchId");
    String individualSim = request.getParameter("individualSim");
    String simProvider = request.getParameter("simProviderList");
    String simType = request.getParameter("simTypeList");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");

    String searchType = request.getParameter("searchType");

    List<BatchesInfo> batchesList = null;
    if (searchType != null && searchType.equals("ALL")) {
        batchesList = SimInventory.getBatchesListAllStatus(SessionData.getProperty("iso_id"), false);
    }
    else {
        batchesList = SimInventory.getBatchesList(batchId, individualSim, simProvider, simType, SessionData.getProperty("iso_id"), startDate, endDate, false, true);
    }
%>




<table border="0" cellpadding="0" cellspacing="0" width="1250" >
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.simInventoryEdit", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>

    <tr>
        <td>
            <div id="dialog-message" title="SIMs" >
                <p>
                    <span class="spanTextBatchId" style="float:left; margin:0 7px 50px 0;"></span>
                </p>
                <p id="pDialogMessage"></p>
            </div>
        </td>
    </tr>
    <tr>
    <br>

    <td colspan="3" class="formArea2" align="center">

        <table width="100%" cellspacing="1" cellpadding="1" border="1" >
            <tr>
                <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.batchId", SessionData.getLanguage())%></td>
                <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.quantity", SessionData.getLanguage())%></td>
                <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.loadDate", SessionData.getLanguage())%></td>
                <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.action", SessionData.getLanguage())%></td>
            </tr>

            <%
                for (int i = 0; i < batchesList.size(); i++) {
                    BatchesInfo info = batchesList.get(i);
            %>
            <tr>
                <td align="center"><%=info.getBatchId()%></td>
                <td align="center"><%=info.getQuantity()%></td>
                <td align="center"><%=info.getLoadDatetime()%></td>

                <td align="center"><button id="buttonEdit" class="buttonEdit" onclick="editBatch('<%=info.getId()%>','<%=info.getBatchId()%>');"><%=Languages.getString("jsp.admin.tools.smsInventory.editButton", SessionData.getLanguage())%></button></td>

            </tr>
            <%
                }
            %>
        </table>

        <button id="buttonAssing" onclick="goAssingInventory('?editOption=Y')"><%=Languages.getString("jsp.admin.tools.smsInventory.goBack", SessionData.getLanguage())%></button>
    </td>
</tr>
</table>



<%@ include file="/includes/footer.jsp" %>