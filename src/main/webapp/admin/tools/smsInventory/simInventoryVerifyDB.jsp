

<%@page import="java.util.Iterator"%>
<%@page import="java.util.Vector"%>
<%@page import="com.debisys.tools.simInventory.isoDomain.IsoDomainConfiguration"%>
<%@page import="com.debisys.tools.simInventory.isoDomain.IsoDomainVo"%>
<%@page import="com.debisys.tools.simInventory.SimProductPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.BatchesInfo"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String action = request.getParameter("action");

    if (action.equals("verifySims")) {
        String[] simsArray = request.getParameterValues("sims[]");
        SimInventory simInventory = new SimInventory();
        List<String> duplicateList = simInventory.verifySimsListDuplicate(simsArray);
        for (String sim : duplicateList) {
            out.println(sim);
        }
    }

    else if (action.equals("verifySimsToEdit")) {
        String[] simsArray = request.getParameterValues("sims[]");
        String batchId = request.getParameter("simBatchId");
        SimInventory simInventory = new SimInventory();
        List<String> duplicateList = simInventory.verifySimsListDuplicateEdit(simsArray, batchId);
        for (String sim : duplicateList) {
            out.println(sim);
        }
    }

    else if (action.equals("getMerchantDba")) {
        String merchantId = request.getParameter("merchantId");
        String merchantDba = SimInventory.getMerchantDBAById(merchantId, SessionData.getProperty("iso_id"));
        out.println(merchantDba);
    }

    else if (action.equals("getSimsList")) {
        String batchId = request.getParameter("batchId");
        List<String> simNumbers = SimInventory.getSimNumberListsByBatchIdWithStatus(batchId);
        for (String sim : simNumbers) {
            out.println(sim);
        }
    }

    else if (action.equals("verifyBatchSelected")) {
        String batchId = request.getParameter("batchId");
        String isoSelected = request.getParameter("isoSelected");
        SimInventory simInventory = new SimInventory();
        boolean exist = simInventory.verifyBatch(batchId, isoSelected);
        out.println(exist);
    }
    else if (action.equals("verifyIsoIdSelected")) {
        String isoIdSelected = request.getParameter("isoIdSelected");
        SimInventory simInventory = new SimInventory();
        boolean exist = simInventory.isValidIsoId(isoIdSelected);
        out.println(exist);
    }
    
    
    else if(action.equals("getBatchesByIsoId")){
        String isoOriginSelected = request.getParameter("isoOriginSelected");
        List<BatchesInfo> batchesList = SimInventory.getBatchesListAllStatus(isoOriginSelected, true);
        for(BatchesInfo bInfo : batchesList){
            out.println(bInfo.getBatchId()+"-["+bInfo.getLoadDatetime()+"]");
        }
    }
    
    else if(action.equals("getProductsByCarrierIdNew")){
        String carrierIdnew = request.getParameter("carrierIdnew");
        List<SimProductPojo> list = SimProviderDao.getSimCarrierProductsList("'"+carrierIdnew+"'");
        for (SimProductPojo p : list) {
            out.println(p.getProductId()+"|"+p.getDescription());
        }
    }
    

    else if (action.equals("insertIsoDomain")) {
        String userId = request.getParameter("user");
        IsoDomainVo isoDomainDB = IsoDomainConfiguration.getIsoDomainsByDomain(userId);
        if (isoDomainDB != null) {
            out.println(false);
        }
        else {
            out.println(true);
        }
    }

    else if (action.equals("edit")) { // Edit IsoDomain
        String id = request.getParameter("id");
        String userId = request.getParameter("user");
        IsoDomainVo isoDomainDB = IsoDomainConfiguration.getIsoDomainsByDomain(userId);
        if (isoDomainDB != null && !isoDomainDB.getId().equalsIgnoreCase(id)) {
            out.println(false);
        }
        else {
            out.println(true);
        }
    }

    else if (action.equals("getSiteIdsByIso")) {// for IsoDomain
        String isoId = request.getParameter("isoId");
        Vector list = IsoDomainConfiguration.getSiteIdsListByIso(isoId);
        Iterator itIL = list.iterator();
        while (itIL.hasNext()) {
            Vector vecTempIL = (Vector) itIL.next();
            out.println(vecTempIL.get(0) + "|(" + vecTempIL.get(0)+") "+vecTempIL.get(1));
        }
    }
    
    else if (action.equals("getSiteIdsByMerchantId")) {// for IsoDomain
        String isoId = request.getParameter("merchantId");
        Vector list = IsoDomainConfiguration.getSiteIdsListByMerchantId(isoId);
        Iterator itIL = list.iterator();
        while (itIL.hasNext()) {
            Vector vecTempIL = (Vector) itIL.next();
            out.println(vecTempIL.get(0) + "|(" + vecTempIL.get(0)+") "+vecTempIL.get(1));
        }
    }
%>

