<%-- 
    Document   : simInventoryMoveBatchProcess
    Created on : Mar 10, 2017, 1:15:24 PM
    Author     : dgarzon
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.debisys.tools.simInventory.ResponseInsertBatch"%>
<%@page import="com.debisys.tools.simInventory.SimTypesDao"%>
<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script src="js/simInventory.js" type="text/javascript"></script>
<style type="text/css">
    #infoBasic{
        font-size: 16px;
    }
    #infoBatch{
        font-size: 20px;
    }
    infoBatch
</style>

<%
    String isoOriginSelected = request.getParameter("isoOriginSelected");
    String batchSelected = request.getParameter("batchSelected");
    String isoDestinationSelected = request.getParameter("isoDestinationSelected");
    
    

    SimInventory inventory = new SimInventory();
    
    isoOriginSelected = inventory.getFieldIdFromAutoComplete(isoOriginSelected);
    batchSelected = inventory.getFieldIdFromAutoComplete(batchSelected);
    isoDestinationSelected = inventory.getFieldIdFromAutoComplete(isoDestinationSelected);
    
    int newBatchId = inventory.moveSimBatchToIso(isoOriginSelected, batchSelected, isoDestinationSelected);
    int countMoved = inventory.countSimsByIso(newBatchId, isoDestinationSelected);
    int countNOTMoved = inventory.countSimsByIso(Integer.parseInt(batchSelected), isoOriginSelected);
    int countNOTMovedMastedId = inventory.countSimsByMasterNotNull(Integer.parseInt(batchSelected),isoOriginSelected);
    int totalNotMoved = (countNOTMoved+countNOTMovedMastedId);
    
    /*Thread.sleep(8000);
    String newBatchId = "888888";
    int countMoved = 100;
    int countNOTMoved = 5;*/

%>

<script type="text/javascript">
    $(function () {
        $(document).ready(function () {
            $(".dataSimHidden").hide();
        });
    });

    function PrintReport()
    {
        var divBanner = document.getElementById("divPrintBanner");
        var divMenu = document.getElementById("divPrintMenu");
        var mainmenu = document.getElementById("mainmenu");
        var dataSimHidden = document.getElementById("dataSimHidden");
        var buttonAssing = document.getElementById("buttonAssing");
        var buttonPrint = document.getElementById("buttonPrint");
        var buttonExit = document.getElementById("buttonExit");

        if (divBanner != null) {
            divBanner.style.display = "none";
        }
        if (divMenu != null) {
            divMenu.style.display = "none";
        }
        buttonAssing.style.display = "none";
        buttonPrint.style.display = "none";
        buttonExit.style.display = "none";
        mainmenu.style.visibility = "hidden";
        dataSimHidden.style.display = "inline";

        window.print();
        dataSimHidden.style.display = "none";
        if (divMenu != null)
        {
            divMenu.style.display = "inline";
        }
        if (divBanner != null)
        {
            divBanner.style.display = "inline";
        }

        mainmenu.style.visibility = "visible";
        buttonAssing.style.display = "inline";
        buttonPrint.style.display = "inline";
        buttonExit.style.display = "inline";

    }

</script>

<table border="0" cellpadding="0" cellspacing="0" width="1250">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.simInventoryManagement", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" class="formArea2">

            <table width="70%" cellspacing="5" cellpadding="15" border="0" align="center">
                <tr>
                    <td align="center">
                        <p id="infoBasic">

                            <%=Languages.getString("jsp.admin.tools.smsInventory.batchMovedInfo", SessionData.getLanguage())%>
                            <BR>
                            <%=String.format(Languages.getString("jsp.admin.tools.smsInventory.batchMovedInfo2", SessionData.getLanguage()), countMoved, isoOriginSelected, isoDestinationSelected)%>
                            <BR>
                            
                            <%=String.format(Languages.getString("jsp.admin.tools.smsInventory.batchNOTMovedInfo", SessionData.getLanguage()), totalNotMoved)%>
                            
                            <BR>

                        </p>
                        <p id="infoBatch">
                            BATCH ID : <%=newBatchId%>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="dataSimHidden" id="dataSimHidden">
                            
                        </div>
                    </td>

                </tr>
                <tr>
                    <td align="center">
                        <button id="buttonAssing" onclick="goAssingInventory('')"><%=Languages.getString("jsp.admin.tools.smsInventory.assingInventory", SessionData.getLanguage())%></button>
                        <input type="button" id="buttonPrint" value="<%=Languages.getString("jsp.admin.reports.Print_This_Page", SessionData.getLanguage())%>" onclick="PrintReport();">
                        <button id="buttonExit" onclick="goToTools()"><%=Languages.getString("jsp.admin.tools.smsInventory.buttonExit", SessionData.getLanguage())%></button>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>
