<%@page import="java.util.Date"%>
<%@page import="com.debisys.tools.simInventory.BatchesInfo"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>


<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>

<script src="js/simInventory.js" type="text/javascript"></script>
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}

    select {
        width : 180px;
    }
    #spanWarningMessages{
        font-size: 16px;
        color: red;
    }

</style>

<script type="text/javascript">
    
    $(function () {
        $(document).ready(function () {
            $("#dialog-message").hide();
        });
        
        
        $("#options_5").change(function (e) {
            var entity = <%=DebisysConstants.MERCHANT%>
            var merchantId = document.getElementById("options_" + entity).value;
            $('#spanWarningMessages').text('');
            $('#merchantId').val('');
            $('#defaultSiteId2').val('0');
            showDialogDefaultSiteId(merchantId);
        });
        
        $("#comboDefaultSiteId").change(function (e) {
            $('#spanWarningMessages').text('<%=Languages.getString("jsp.admin.tools.smsInventory.defaultSiteId", SessionData.getLanguage())%> '+ $("#comboDefaultSiteId option:selected").text());
            $('#defaultSiteId1').val($("#comboDefaultSiteId").val());
            $('#defaultSiteId2').val($("#comboDefaultSiteId").val());
            $('#spanWarningMessages').css('color', 'green');
            $("#dialog-message").dialog("close");
        });
    });

    function validateForm1() {
        var merchantId = document.getElementById("merchantId").value;
        var dd = $("#startDate").val();
        return processValidate(merchantId);
    }

    function validateForm2() {
        var entity = <%=DebisysConstants.MERCHANT%>
        var merchantId = document.getElementById("options_" + entity).value;
        $("#merchantIdForm2").val(merchantId);
        var dd = $("#startDate").val();
        $("#startDateForm2").val(dd);
        return processValidate(merchantId);
    }

    function processValidate(merchantId) {

        if (merchantId === '') {
            return false;
        }

        if (isNumber(merchantId) === false) {
            $('#spanWarningMessages').css('color', 'red');
            $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.smsInventory.merchantOnlyNumbers", SessionData.getLanguage())%> ").show().fadeOut(7000);
            return false;
        }
        var passedValidation = false;
        $.ajax({
            type: "POST",
            async: false,
            data: {merchantId: merchantId, action: 'getMerchantDba'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var resp = String($.trim(data));

                if (resp === "null") {
                    passedValidation = false;
                    $('#spanWarningMessages').css('color', 'red');
                    $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.smsInventory.merchantNOTFound", SessionData.getLanguage())%> ").show();
                }
                else {
                    if (confirm("<%=Languages.getString("jsp.admin.tools.smsInventory.merchantFound", SessionData.getLanguage())%>" + resp.toUpperCase())) {
                        passedValidation = true;
                    } else {
                        passedValidation = false;
                    }
                }
            }
        });
        return passedValidation;
    }
    
    function selectSiteId(){
        $("#options_5").val('');
        $('#spanWarningMessages').text('');
        $('#defaultSiteId1').val('0');
        $('#defaultSiteId2').val('0');
        var merchantId = $('#merchantId').val();
        showDialogDefaultSiteId(merchantId);
    }
    
    function merchantIdOnBlurAction(){
        $("#options_5").val('');
        $('#spanWarningMessages').text('');
        $('#defaultSiteId1').val('0');
        $('#defaultSiteId2').val('0');
    }
    
    function showDialogDefaultSiteId(merchantId){
        
        if (merchantId === '') {
            return false;
        }

        if (isNumber(merchantId) === false) {
            $('#spanWarningMessages').css('color', 'red');
            $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.smsInventory.merchantOnlyNumbers", SessionData.getLanguage())%> ").show();
            return false;
        }
        $.ajax({
            type: "POST",
            async: false,
            data: {merchantId: merchantId, action: 'getMerchantDba'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var resp = String($.trim(data));
                if (resp === "null") {
                    $('#spanWarningMessages').css('color', 'red');
                    $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.smsInventory.merchantNOTFound", SessionData.getLanguage())%> ").show();
                }
                else {
                    changeSiteIdSelectByMerchantId(merchantId, resp);
                }
            }
        });
    }

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    
</script>

<%

    String batchId = request.getParameter("batchId");
    String textFileWarning = "";
    String strRefId = SessionData.getProperty("ref_id");
%>


<table border="0" cellpadding="0" cellspacing="0" width="950" >
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.assingInventory", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
    <br>
    <td colspan="3" class="formArea2">
        <form id="mainform" name="mainform" class="reportForm" name="mainform" method="post" action="admin/tools/smsInventory/inventoryAssignment.jsp" onsubmit="return validateForm1();">
            <table width="80%"  border="0" class="formArea2" cellpadding="10" cellspacing="10">
                <tr>
                    <td><%=Languages.getString("jsp.admin.tools.smsInventory.merchantIdSearch", SessionData.getLanguage())%></td>
                    <td><input type="text" name="merchantId" id="merchantId" value="" size="35" maxlength="30" onblur="merchantIdOnBlurAction()"/></td>
                    <td><a onclick="selectSiteId()" href="javascript:void(0);"><%=Languages.getString("jsp.admin.tools.smsInventory.selectDefaultSiteId", SessionData.getLanguage())%></a></td>
                </tr>

                <tr>

                    <td nowrap><%=Languages.getString("jsp.admin.tools.smsInventory.shippingDate", SessionData.getLanguage())%>:</td> 
                    <td>

                        <input class="plain" name="startDate" id="startDate" value="" size="12" onfocus="linkdate.focus();">
                        <a name="linkdate" href="javascript:void(0)" onclick="if (self.gfPop)
                                    gfPop.fPopCalendar(document.mainform.startDate);
                                return false;" HIDEFOCUS>
                            <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
                        </a>
                    </td>
                    <td colspan="2"><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonSearch", SessionData.getLanguage())%> / <%=Languages.getString("jsp.admin.tools.smsInventory.assingInventory", SessionData.getLanguage())%>"/></td>
                </tr>

            </table>
            <input type="hidden" name="action" value="assignMerchantForm1" />
            <input type="hidden" name="manual" id="manual" value="manual" />
            <input type="hidden" name="batchId" value="<%=batchId%>" />
            <input type="hidden" name="defaultSiteId1" id="defaultSiteId1" value="0" />

        </form>

        <span id="spanWarningMessages"><%=textFileWarning%></span>
    </td>
</tr>



<tr>

    <td colspan="3" class="formArea2">

        <form id="mainform2" name="mainform2" class="reportForm2" name="mainform2" method="post" action="admin/tools/smsInventory/inventoryAssignment.jsp" onsubmit="return validateForm2();">
            <table width="100%"  border="0" class="formArea2">
                <tr>
                    <td>
                        <div id="entityCombos">
                            <jsp:include page="/admin/customers/merchants/entity_combo_selector.jsp" >
                                <jsp:param value="<%=strRefId%>" name="strRefId" />
                                <jsp:param value="<%=strAccessLevel%>" name="strAccessLevel" />
                                <jsp:param value="<%=strDistChainType%>" name="strDistChainType" />                                            
                            </jsp:include>                                                
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td align="center">
                        <input type="hidden" name="action" value="assignMerchantForm2" />
                        <input type="hidden" name="startDateForm2" id="startDateForm2" value="" />
                        <input type="hidden" name="merchantIdForm2" id="merchantIdForm2" value="" />
                        <input type="hidden" name="batchId" value="<%=batchId%>" />
                        <input type="hidden" name="defaultSiteId2" id="defaultSiteId2" value="0" />
                        <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.assingInventory", SessionData.getLanguage())%>"/>
                        <button id="buttonBack" type="button" onclick="goToTools()"><%=Languages.getString("jsp.admin.tools.smsInventory.cancel", SessionData.getLanguage())%></button>
                    </td>
                </tr>
            </table>

        </form>
    </td>
    
    <!-- DIV SIM DEFAULT SITE ID -->
    <div id="dialog-message" title="Site Id" >
        <p>
            <span class="spanTextBatchId" style="float:left; margin:0 7px 50px 0;"></span>
        </p>
        <select name="comboDefaultSiteId" id="comboDefaultSiteId" tabindex="11" style="width: 450px;"> 
        </select>
        <p id="pDialogMessage"></p>
    </div>

</tr>

</table>

<iframe width=132 height=142 name="gToday:contrast:agenda.js"
        id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm"
        scrolling="no" frameborder="0"
        style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>


<%@ include file="/includes/footer.jsp" %>