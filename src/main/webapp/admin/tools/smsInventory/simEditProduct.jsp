<%@page import="com.debisys.tools.simInventory.SimEditProduct"%>
<%@page import="com.debisys.tools.simInventory.SimProductPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.SimTypesDao"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 42;
    String path = request.getContextPath();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>
    <link href="css/font-awesome/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="css/primeui-2.0-min.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="/support/includes/jquery/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script language="JavaScript" src="/support/includes/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
    <script language="JavaScript" src="/support/includes/primeui/primeui-3.0.2-min.js" type="text/javascript" charset="utf-8"></script>
    
    <link href="/support/css/multiple-select.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="/support/js/multiple-select.js" type="text/javascript" charset="utf-8"></script>
    
    <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script> 
    <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
    <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
    <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/> 
    <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>
            
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}
    
    .w300 {
        width: 300px;
    }
    .hidden-column{
        display: none;
    }
</style>
<%
    String simNumber = Languages.getString("jsp.admin.reports.sim.simNumber", SessionData.getLanguage());
    String productId = Languages.getString("jsp.admin.reports.sim.ProductId", SessionData.getLanguage());
    String productName = Languages.getString("jsp.admin.reports.sim.ProductName", SessionData.getLanguage());
    
    String validationOnlyDigits =  Languages.getString("jsp.admin.tools.smsInventory.validation.onlyDigits", SessionData.getLanguage()); 
    String completeSimNumber =  Languages.getString("jsp.admin.tools.smsInventory.validation.completeSimNumber", SessionData.getLanguage()); 
    String firstNDigits =  Languages.getString("jsp.admin.tools.smsInventory.validation.firstNDigits", SessionData.getLanguage());
    
    String expiredStart =  Languages.getString("jsp.admin.tools.smsInventory.startDate", SessionData.getLanguage());
    String expiredEnd =  Languages.getString("jsp.admin.tools.smsInventory.expiredDate", SessionData.getLanguage());
    //jsp.admin.tools.smsInventory.endDate

%>
<script type="text/javascript">
     
var uri = '/support/admin/tools/smsInventory/SimEditProductDB.jsp';
var dialog;
var dataSearchSims;
var momentStart = moment();
var momentEnd= moment().add(96, 'days');

var startExpiredMoment=null;
var endExpiredMoment=null;

    $(function() {
        var simCarriers = "";
        var carriersCount = 0;
        var carriersSel = 0;
        
        var simProducts = "";
        var productsCount = 0;
        var productsSel = 0;
            
        initDateControl();
        $( "#tdExpiredDateControl" ).hide();
        $( "#tdExpiredDate" ).hide();
        
        $( "#batchId" ).on( "blur", function() { 
            if ($("#batchId").val().length>0 && !$.isNumeric( $("#batchId").val()) ){
                $('#batchId').val("");
                messageValidation();
                $('#batchId').focus(100);
            }   
        });
        
        $('#simProducts').multipleSelect({
            onOpen: function() {                         },
            onClose: function() {                        },
            onCheckAll: function() {
                simProducts="-1";
            },
            onUncheckAll: function() {                   },
            onFocus: function() {                        },
            onBlur: function() {                         },
            onOptgroupClick: function(view) {            },
            onClick: function(view) {
                
            }
        });
        
        $('#simProviderList').multipleSelect({
            onOpen: function() {                         },
            onClose: function() {                        },
            onCheckAll: function() {
                $('#individualSim').val("");
                $('#batchId').val("");
            },
            onUncheckAll: function() {                   },
            onFocus: function() {                        },
            onBlur: function() {                         },
            onOptgroupClick: function(view) {            },
            onClick: function(view) {
                $('#individualSim').val("");
                $('#batchId').val("");
            }
        });
        
        $( "#expiredDate" ).on( "keydown", function() { 
            return false;
        });
        
        $( "#filterSims" ).on( "blur", function() {
            if ($.isNumeric( $("#filterSims").val()) ){
                $('#individualSim').val(""); 
            } else if ( $("#filterSims").val().length>0) {
                messageValidation();
                $('#filterSims').focus(100);
            }
        });
        
        $( "#individualSim" ).on( "blur", function() { 
            if ($.isNumeric( $("#individualSim").val()) ){
                $('#batchId').val("");
                $('#filterSims').val("");
                $('#simProviderList').multipleSelect('uncheckAll');
                $('#simProducts').multipleSelect('uncheckAll'); 
                $('#assigToNewSimProduct').hide();
            } else if ( $("#individualSim").val().length>0) {
                messageValidation();
                $('#individualSim').val("");
                $('#individualSim').focus(100);
            }            
        }); 
               
        
        $( "#simProviderList" ).change(function() {
            carriersCount = 0;
            carriersSel = 0;
            simCarriers = "";
            $("#simProviderList option").each(function() {
                carriersCount++;                    
            });                                
            $("#simProviderList option:selected").each(function() {
                carriersSel++;
                simCarriers += $(this).val() + ",";
            });
        });
        $( "#simProducts" ).change(function() {
            productsCount = 0;
            productsSel = 0;
            simProducts = "";
            $("#simProducts option").each(function() {
                productsCount++;                    
            });                                
            $("#simProducts option:selected").each(function() {
                productsSel++;
                simProducts += $(this).val() + ",";
            });
        });    
            
        dialog = $( "#dialog-form" ).dialog({modal: true});   
        
        $( "#dialog-form" ).dialog( "close" );  
        
        $( "#assigToNewSimProduct" ).button().on( "click", function() {
            if (dataSearchSims.expired){
                $("#dialog-form" ).dialog({ title: "<%=Languages.getString("jsp.admin.tools.smsInventory.titleExpired", SessionData.getLanguage())%>" });           
                $("#tdStart").show();
                $("#tdEnd").show();
                $("#tdInstructions").show();
                $("#tdInstructions1").show(); 
                $("#tdInstructions2").show();
                $("#tdOptional").show();                
                dialog = $( "#dialog-form" ).dialog({            
                    autoOpen: false,
                    height: 450,
                    width: 750,
                    modal: true,
                    buttons: [
                    {text: "<%=Languages.getString("jsp.admin.tools.smsInventory.deleteButton", SessionData.getLanguage())%>",
                        click: function() {
                            var finalDataUpdateFilter = $.extend(dataSearchSims, {"action": "deleteSims"});                                                            
                            $('#spinnerInfo').addClass("fa fa-refresh fa-spin");
                            $.ajax({
                                type:"GET",
                                data: finalDataUpdateFilter,
                                cache: false,
                                url: uri,
                                dataType: "json",
                                context: this,
                                success:function(response){                        
                                    if (response>0){                            
                                        setTimeout(function(){                    
                                            $("#searchSims" ).click();       
                                          }, 800);
                                        dialog.dialog( "close" );
                                    }
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");                        
                                },
                                error:function(err){
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                                    console.log(err);
                                }
                            });
                        }
                    },
                    {text: "<%=Languages.getString("jsp.admin.tools.smsInventory.cancelButton", SessionData.getLanguage())%>",
                        click: function() {
                            dialog.dialog( "close" ); 
                        }
                    },
                    {text: "<%=Languages.getString("jsp.admin.tools.smsInventory.changeButton", SessionData.getLanguage())%>",
                        click: function() {                            
                            var finalDataUpdateFilter;
                            var newProduct = $("#newSimProducts").val();                                  
                            if (newProduct==='-1'){
                                finalDataUpdateFilter = $.extend(dataSearchSims, {"action": "alive", 
                                    "startDate": $('#startDate').val(), 
                                    "endDate": $('#endDate').val()}); 
                            } else{
                                var selectedProduct = newProduct.split("*");
                                var carrierId = selectedProduct[1];
                                var productId = selectedProduct[0];
                                finalDataUpdateFilter = $.extend(dataSearchSims, {"action": "alive", 
                                    "carrierId": carrierId, 
                                    "productId": productId,
                                    "startDate": $('#startDate').val(), 
                                    "endDate": $('#endDate').val()}); 
                            }                               
                            $('#spinnerInfo').addClass("fa fa-refresh fa-spin");
                            $.ajax({
                                type:"GET",
                                data: finalDataUpdateFilter,
                                url: uri,
                                cache: false,
                                dataType: "json",
                                context: this,
                                success:function(response){                        
                                    if (response>0){                            
                                        setTimeout(function(){                    
                                            $("#searchSims" ).click();       
                                          }, 800);
                                        dialog.dialog( "close" );
                                    }
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");                        
                                },
                                error:function(err){
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                                    console.log(err);
                                }
                            });                            
                       }
                    }
                    ],
                    close: function() {
                        dialog.dialog( "close" );              
                    }
                });
                
            } else{
                $("#dialog-form" ).dialog({ title: "<%=Languages.getString("jsp.admin.tools.smsInventory.changeSimProducts", SessionData.getLanguage())%>" });
                $("#tdStart").hide();
                $("#tdEnd").hide();
                $("#tdInstructions").hide();
                $("#tdInstructions1").hide();
                $("#tdInstructions2").hide();
                $("#tdOptional").hide();
                dialog = $( "#dialog-form" ).dialog({            
                    autoOpen: false,
                    height: 250,
                    width: 750,
                    modal: true,
                    buttons: [
                    {text: "<%=Languages.getString("jsp.admin.tools.smsInventory.cancelButton", SessionData.getLanguage())%>",
                        click: function() {
                            dialog.dialog( "close" ); 
                        }
                    },
                    {text: "<%=Languages.getString("jsp.admin.tools.smsInventory.changeButton", SessionData.getLanguage())%>",
                        click: function() {
                            var doChange = false;
                            var newProduct = $("#newSimProducts").val();                            
                            if (newProduct==='-1'){                        
                                $("#changeInfo").fadeIn(1000);
                                $("#changeInfo").text("Please choose a product!");
                                $("#changeInfo").fadeOut(4000);
                                return;
                            } else{
                                doChange = true;
                            }                                                           
                            if (doChange){
                                var selectedProduct = newProduct.split("*");
                                var carrierId = selectedProduct[1];
                                var productId = selectedProduct[0];
                                var finalDataUpdateFilter;               
                                finalDataUpdateFilter = $.extend(dataSearchSims, {"action": "changeSims", 
                                    "carrierId": carrierId, 
                                    "productId": productId
                                });                                  
                                $('#spinnerInfo').addClass("fa fa-refresh fa-spin");
                                $.ajax({
                                    type:"GET",
                                    data: finalDataUpdateFilter,
                                    url: uri,
                                    cache: false,
                                    dataType: "json",
                                    context: this,
                                    success:function(response){                        
                                        if (response>0){                            
                                            setTimeout(function(){                    
                                                $("#searchSims" ).click();       
                                              }, 800);
                                            dialog.dialog( "close" );
                                        }
                                        $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");                        
                                    },
                                    error:function(err){
                                        $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                                        console.log(err);
                                    }
                                });
                            }
                       }
                    }
                    ],
                    close: function() {
                        dialog.dialog( "close" );              
                    }
                });
            }
            
            dialog.dialog( "open" );             
        });    
        
        $( "#expired" ).on( "change", function() {        
            $( "#assigToNewSimProduct" ).hide();
            if ( $("#expired").is(':checked') ){
                $( "#tdExpiredDateControl" ).show();
                $( "#tdExpiredDate" ).show();
            } else{
                $( "#tdExpiredDateControl" ).hide();
                $( "#tdExpiredDate" ).hide();
            }
        });    
                
        $( "#clearResults" ).button().on( "click", function() {
            $("#batchId").prop("disabled", false);
            $("#individualSim").prop("disabled", false);
            $("#simProviderList").multipleSelect("enable");
            $("#simProducts").multipleSelect("enable");
            $("#searchProductsByCarriers").prop("disabled", false);
        });
                
        $( "#searchProductsByCarriers" ).button().on( "click", function() {
            let combo = $( "#simProducts" );
            combo.empty(); 
            var data = {"action": "products"};
            if (carriersCount===carriersSel){
                data= $.extend(data, {"allCarriers": true} );
                $('#simProviderList').multipleSelect('checkAll');
            }else{
                if (simCarriers.length===0){
                    $('#simProviderList').multipleSelect('checkAll');
                }                
                data= $.extend(data, {"carriers": simCarriers} );
            }            
            $('#spinnerInfo').addClass("fa fa-refresh fa-spin");                        
            $.ajax({
                type: "GET",
                async: false,
                data: data,
                cache: false,
                url: uri,
                success: function (data) { 
                    $.each(data, function (key, entry) {               
                        combo.append($('<option selected></option>').attr('value', entry.productId).text(entry.description));
                    });                    
                    setTimeout(function(){
                        $('#spinnerInfo').removeClass("fa fa-refresh fa-spin"); 
                        $('#simProducts').multipleSelect({
                                isOpen: true,
                                keepOpen: true
                                });
                        $('#simProducts').multipleSelect('focus');        
                      }, 500);
                }
            });
            
        });
                
        $( "#searchSims" ).button().on( "click", function() {            
            dataSearchSims = {"action": "sims"};  
            if ( $("#expired").is(':checked') ){
                dataSearchSims = $.extend(dataSearchSims,{ "expired": $("#expired").is(':checked')});  
                if (startExpiredMoment!==null && endExpiredMoment!==null){
                    dataSearchSims= $.extend(dataSearchSims, {"startExpiredMoment": startExpiredMoment, "endExpiredMoment": endExpiredMoment} );                
                } 
            }
            if ($("#filterSims").val().length>0){
                if ($("#filterSims").val().length<5){
                    return;
                } else{
                    dataSearchSims= $.extend(dataSearchSims, {"searchSims": $("#filterSims").val()} );
                }
            }
                    
            if ($.isNumeric( $("#batchId").val()) ){
                dataSearchSims = $.extend(dataSearchSims, {"batchId": $("#batchId").val()});
            } else if ( $("#individualSim").val().length>0 ){
                dataSearchSims = $.extend(dataSearchSims, {"individualSim": $("#individualSim").val() } );
            } else{   
                debugger;
                if ( productsCount!==productsSel || carriersCount!==carriersSel ){
                    if (simProducts.length>0){
                        dataSearchSims= $.extend(dataSearchSims, {"skus": simProducts} );                        
                    } else if (simCarriers.length>0 && simProducts.length===0){
                        if (carriersCount===carriersSel){
                            dataSearchSims= $.extend(dataSearchSims, {"allCarriers": "-1"} );
                        }else{
                            dataSearchSims= $.extend(dataSearchSims, {"carriers": simCarriers} );
                        }                    
                    } 
                }
            }            
            //if ( Object.keys(dataSearchSims).length > 2 ){                               
                fillSimsDataTable(dataSearchSims);
            //}
        });        
    });
       
       
    function fillSimsDataTable(dataValues){
        
        var totalRecords = 0;
        var finalDataCount = $.extend(dataValues, {action: 'count'});
        $.ajax({
            type: "GET",
            async: false,
            data: finalDataCount,
            cache: false,
            url: uri,
            success: function (data) {                        
                totalRecords = data.trim();
                if (totalRecords>0){
                    var columnsR;
                    var dataTableName;
                    if ($("#expired").is(':checked')){
                        dataTableName= "simsResultExpired";
                        $("#simsResultExpired").show();
                        $("#simsResult").hide();
                        columnsR = [
                        {field:'simNumber', headerText: '<%=simNumber%>', sortable:true},
                        {field:'simProductId', headerText: '<%=productId%>', sortable:true, headerClass:'header-tbl'},
                        {field:'simProductDescription', sortable:true, headerText: '<%=productName%>' },
                        {field:'startDateTime', sortable:false, headerText: '<%=expiredStart%>',
                                content: function(row){return formatField(row,'startDateTime');}},
                        {field:'endDateTime', sortable:false, headerText: '<%=expiredEnd%>',
                                content: function(row){return formatField(row,'endDateTime');}}];                        
                    } else{
                        dataTableName= "simsResult";
                        $("#simsResultExpired").hide();
                        $("#simsResult").show();
                        columnsR = [
                        {field:'simNumber', headerText: '<%=simNumber%>', sortable:true},
                        {field:'simProductId', headerText: '<%=productId%>', sortable:true, headerClass:'header-tbl'},
                        {field:'simProductDescription', sortable:true, headerText: '<%=productName%>' }];
                    }          
                    
                    var info = totalRecords + " <%=Languages.getString("jsp.admin.results_found", SessionData.getLanguage())%> ";
                    $('#reportInfo').text(info);
                    $('#'+dataTableName).puidatatable({
                        lazy:true,
                        paginator: {
                            rows: 15,
                            pageLinks:10,
                            totalRecords:totalRecords
                        },
                        columns: columnsR,
                        sortField:'simNumber',
                        sortOrder:'1',
                        datasource: function(callback, ui){
                            $('#spinnerInfo').addClass("fa fa-refresh fa-spin");
                            var start = ui.first + 1;
                            var sortField = ui.sortField;
                            var sortOrder = ui.sortOrder === 1 ? 'ASC' : 'DESC';
                            var rows = this.options.paginator.rows;
                            var data = {
                                "action": "sims", "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows": (start+rows)                    
                            };
                            var finalDataFilter = $.extend(dataValues, data);                                                        
                            $.ajax({
                                type:"GET",
                                data:finalDataFilter,
                                cache: false,
                                url:uri,
                                dataType:"json",
                                context:this,
                                success:function(response){
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                                    callback.call(this, response);
                                    $(".colorized").parent("td").parent("tr").addClass('textRed');                                    
                                },
                                error:function(err){
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                                    console.log(err);
                                }
                            });
                        }
                    }); 
                    $('#tdResults').show();
                    $('#assigToNewSimProduct').show();
                    $("#reportInfo").fadeIn(1000);                    
                } else{  
                    $("#reportInfo").fadeIn(1000);
                    $("#reportInfo").text("No records!!!");
                    $("#reportInfo").fadeOut(3000);
                    $('#tdResults').hide();
                    $('#assigToNewSimProduct').hide();
                }
            }
        });         
    
    }
        
    function initDateControl(){
        momentStart = moment();
        momentEnd= moment().add(96, 'days');
        var days = momentEnd.diff(momentStart, 'days');
        $("#pDiffDays").text(days);
        $('input[name="startDate"]').daterangepicker({
            "minDate": momentStart,
            "showDropdowns": true,
            "showWeekNumbers": false,
            "singleDatePicker": true,
            "autoApply": true,
            "timePicker": false,
            "timePicker24Hour": false,
            "startDate": momentStart,
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                "daysOfWeek": [
                    '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                ],
                "monthNames": [
                    '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {
            momentStart=start;
            var days = momentEnd.diff(momentStart, 'days');
            $("#pDiffDays").text(days);
        }
        );
        $('input[name="endDate"]').daterangepicker({
            "minDate": momentEnd,
            "showDropdowns": true,
            "showWeekNumbers": false,
            "singleDatePicker": true,
            "autoApply": true,
            "timePicker": false,
            "timePicker24Hour": false,
            "startDate": momentEnd, 
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                "daysOfWeek": [
                    '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                ],
                "monthNames": [
                    '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                ],
                "firstDay": 1
            }            
            }, function (start, end, label) {
                momentEnd=start;
                var days = momentEnd.diff(momentStart, 'days');
                $("#pDiffDays").text(days);
            }
        );  
        
        $('input[name="expiredDate"]').daterangepicker({
            "autoUpdateInput": false,
            "maxDate": moment(),
            "showDropdowns": true,
            "showWeekNumbers": false,
            "singleDatePicker": false,
            "autoApply": false,
            "timePicker": false,
            "timePicker24Hour": false,
            "showCustomRangeLabel": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()]                
            },
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                "daysOfWeek": [
                    '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                ],
                "monthNames": [
                    '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                ],
                "firstDay": 1
            }            
            }, function (start, end, label) { }
        );
        
        $('input[name="expiredDate"]').on('apply.daterangepicker', function(ev, picker) {
            startExpiredMoment = picker.startDate.format('MM/DD/YYYY');
            endExpiredMoment = picker.endDate.format('MM/DD/YYYY');
            $(this).val(startExpiredMoment + ' - ' + endExpiredMoment);            
        });
        $('input[name="expiredDate"]').on('cancel.daterangepicker', function(ev, picker) {
           startExpiredMoment=null;
           endExpiredMoment=null;
           $(this).val('');
        });
        
    }
    
    function complete() {
        $("#reportInfo").text("Empty search");
    }
    
    function messageValidation() {
        $("#messagesValidation").fadeIn(1000);
        $("#messagesValidation").text("<%=validationOnlyDigits%>");
        $("#messagesValidation").fadeOut(3000);  
    }
    
    function formatField(data, fieldName){ 
        var dataInfo="";        
        if (fieldName==='startDateTime'){
            dataInfo = data[fieldName];
            dataInfo= dataInfo.substring(0, 10);
        } else if (fieldName==='endDateTime'){
            dataInfo = data[fieldName];
            dataInfo= dataInfo.substring(0, 10);            
        } 
        return dataInfo;
    }
        
</script>
                                    
<table border="0" cellpadding="0" cellspacing="0" width="1250">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.simInventory.productEdit", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif">
            <div id="dialog-form" >
                <table cellspacing="5" cellpadding="5" border="0">
                    <tr>
                    <td style="color:red;" colspan="4">
                            <p id="changeInfo" name="changeInfo"></p>
                        </td>
                    </tr>
                    <tr style="height: 50px">
                        <td><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%>:</td>
                        <td> 
                            <select name="newSimProducts" id="newSimProducts">
                                <option value="-1" selected></option>
                                <%                        
                                List<SimProductPojo> listProducts = SimEditProduct.getSimProductsList(null);
                                for (SimProductPojo product : listProducts) {
                                    out.println("<option value=" + product.getProductId() + "*"+product.getCarrierIdNew()+">" + product.getDescription()+ " ("+product.getProductId()+")</option>");
                                }
                                
    String editSimMessage1 = Languages.getString("jsp.admin.tools.smsInventory.editSimMessage1", SessionData.getLanguage());
    String editSimMessage2 = Languages.getString("jsp.admin.tools.smsInventory.editSimMessage2", SessionData.getLanguage());
    String startDate = Languages.getString("jsp.admin.tools.smsInventory.startDate", SessionData.getLanguage());
    String endDate = Languages.getString("jsp.admin.tools.smsInventory.endDate", SessionData.getLanguage());
    String editSimMessage3 = Languages.getString("jsp.admin.tools.smsInventory.editSimMessage3", SessionData.getLanguage());
    String optionalMessage = Languages.getString("jsp.admin.tools.smsInventory.optionalMessage", SessionData.getLanguage());
                                
                                %>
                            </select>
                        </td>
                        <td id="tdOptional" name="tdOptional"><%=optionalMessage%></td>
                    </tr>
                    <tr style="height: 50px" id="tdInstructions" name="tdInstructions">
                        <td colspan="4"><%=editSimMessage1%></td>                        
                    </tr>
                    <tr style="height: 50px" id="tdInstructions1" name="tdInstructions1">
                        <td colspan="4"><%=editSimMessage2%></td>                        
                    </tr>
                    <tr style="height: 50px" id="tdStart" name="tdStart">
                        <td><%=startDate%></td>
                        <td>
                            <div class="form-group">                                                                                                                          
                                <div class="input-group input-group-sm">                                                                        
                                    <span class="input-group-addon btnOpenCalendarStart"  id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                    <input type="text" class="form-control" id="startDate" name="startDate" />                                                           
                                </div>                                                                                                                                                                                                                                             
                            </div>
                        </td>
                    </tr>
                    <tr style="height: 50px" id="tdEnd" name="tdEnd">
                        <td><%=endDate%></td>
                        <td>
                            <div class="form-group">                                                                                                                          
                                <div class="input-group input-group-sm">                                                                        
                                    <span class="input-group-addon btnOpenCalendarEnd"  id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                    <input type="text" class="form-control" id="endDate" name="endDate" />                                                           
                                </div>                                                                                                                                                                                                                                             
                            </div>
                        </td>
                        <td><!--p id="pDiffDays" name="pDiffDays"></p-->
                            </td>
                    </tr>
                    <tr style="height: 50px" id="tdInstructions2" name="tdInstructions2">
                        <td colspan="4"><%=editSimMessage3%></td>                        
                    </tr>
                </table>                               
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3" class="formArea2">
            <%
            String expiredSimCheck = Languages.getString("jsp.admin.tools.smsInventory.expiredSimCheck", SessionData.getLanguage());
            String filterSims = Languages.getString("jsp.admin.tools.smsInventory.filterSims", SessionData.getLanguage());

            %>
                <table width="100%" cellspacing="15" cellpadding="15" border="0" >
                    <tr>
                        <td>
                            <p style="margin: 3px;color:red" id="messagesValidation" name="messagesValidation"></p>
                        </td>
                    </tr>    
                    <tr style="height: 30px">
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.batchId", SessionData.getLanguage())%></td>  
                        <td><input type="text" name="batchId" id="batchId" value="" size="38" maxlength="20" /></td>

                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.individualSim", SessionData.getLanguage())%>
                            <br/>
                            <span style="font-size: 10px;">(<%=completeSimNumber%>)</span>
                        </td>  
                        <td><input type="text" name="individualSim" id="individualSim" value="" size="38" maxlength="20"/></td>                        
                    </tr>
                    <tr style="height: 30px">
                        <td class="main"><%=expiredSimCheck%></td>
                        <td><input type="checkbox" id="expired" name="expired" /></td>
                        <td class="main" id="tdExpiredDate" name="tdExpiredDate"><%=Languages.getString("jsp.admin.tools.smsInventory.expiredDate", SessionData.getLanguage())%></td>
                        <td id="tdExpiredDateControl" name="tdExpiredDateControl">
                            <div class="form-group">                                                                                                                          
                                <div class="input-group input-group-sm">                                                                        
                                    <span class="input-group-addon btnOpenCalendarEnd"  id="sizing-addon3"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                    <input type="text" class="form-control" id="expiredDate" name="expiredDate" />                                                           
                                </div>                                                                                                                                                                                                                                             
                            </div>
                        </td>
                    </tr>
                    <tr style="height: 30px">
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simProviderList" id="simProviderList" class="w300" multiple="multiple">
                                <%
                                    List<SimProviderPojo> simProviderList = SimProviderDao.getSimCarrierList();
                                    for (SimProviderPojo currentSimProvider : simProviderList) {
                                        out.println("<option value=" + currentSimProvider.getId() + ">" + currentSimProvider.getName() + "</option>");
                                    }
                                %>
                            </select>
                            <input type="button" class="submitClass" id="searchProductsByCarriers" value="Get products"/>  
                        </td>                                                 
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simProducts" id="simProducts" tabindex="11" class="w300" multiple="multiple"></select>                            
                        </td>                        
                    </tr>
                    <tr style="height: 30px">
                        <td class="main" colspan="1"></td>
                        <td colspan="1"></td>
                        <td class="main"><%=filterSims%>
                            <br/>
                            <span style="font-size: 10px;">(<%=firstNDigits%>)</span>
                        </td>
                        <td>
                            <input type="text" id="filterSims" name="filterSims" size="38" maxlength="20"/>  
                            <input type="button" class="submitClass" id="searchSims" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonSearch", SessionData.getLanguage())%>"/>                            
                            <input type="button" id="assigToNewSimProduct" name="assigToNewSimProduct" value="Change"/> 
                        </td>                        
                    </tr>                                       
                    <tr>
                        <td colspan="3">
                            <p style="margin: 3px;" id="reportInfo"></p>
                        </td>
                    </tr>                                           
                    <tr>
                        <td olspan="3">
                            <p style="margin: 3px;" id="spinnerInfo">
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                            </p>
                        </td>
                    </tr>
                </table>
            
        </td>
    </tr>
    <tr>
        <td colspan="3" style="overflow: scroll; width:100%;" id="tdResults" name="tdResults">
            <div id="simsResult" name="simsResult" style="margin-top:5px; width: 100%"></div>
            <div id="simsResultExpired" name="simsResultExpired" style="margin-top:5px; width: 100%"></div>
        </td>
    </tr>
</table>


                            
<%@ include file="/includes/footer.jsp" %>
