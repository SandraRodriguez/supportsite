<%@page import="com.debisys.tools.simInventory.ResponseInsertBatch"%>
<%@page import="com.debisys.tools.simInventory.SimTypesDao"%>
<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script src="js/simInventory.js" type="text/javascript"></script>
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}

    #infoBasic{
        font-size: 16px;
    }
    #infoBatch{
        font-size: 20px;
    }
    infoBatch
</style>

<%    
    String[] simsProviderArray = request.getParameterValues("simsCarrierHidden");
    String[] simsTypeArray = request.getParameterValues("simsTypeHidden");
    String[] simsProductArray = request.getParameterValues("simsProductHidden");
    String[] carrierForProductHiddenArray = request.getParameterValues("carrierForProductHidden");
    String[] simsArray = request.getParameterValues("sims");
    String[] simsId = request.getParameterValues("simsId");
    
    String batchSaveOption = request.getParameter("batchSaveOption");
    String batchSelected = request.getParameter("batchSelected");
    String simBatchMode = request.getParameter("simBatchMode");
    String batchCode = request.getParameter("batchCode");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");
    
    if(batchSaveOption != null && batchSaveOption.equalsIgnoreCase("newBatch")){
        batchSelected = "-1";
    }

    SimInventory inventory = new SimInventory();
    ResponseInsertBatch responseBatch = null;
    if(simBatchMode == null || !simBatchMode.trim().equalsIgnoreCase("edit")){
        responseBatch = inventory.insertSimBatches(batchSelected, simsTypeArray, simsProviderArray, 
                simsProductArray, carrierForProductHiddenArray, SessionData.getProperty("iso_id"), simsArray, startDate, endDate);
    }
    else if(simBatchMode != null && simBatchMode.trim().equalsIgnoreCase("edit")){
        responseBatch = inventory.updateSims(batchCode, simsTypeArray, simsProviderArray, simsArray, simsId, simsProductArray);
    }

%>

<script type="text/javascript">
    $(function () {
        $(document).ready(function () {
            $(".dataSimHidden").hide();
        });
    });

    function PrintReport()
    {
        var divBanner = document.getElementById("divPrintBanner");
        var divMenu = document.getElementById("divPrintMenu");
        var mainmenu = document.getElementById("mainmenu");
        var dataSimHidden = document.getElementById("dataSimHidden");
        var buttonAssing = document.getElementById("buttonAssing");
        var buttonPrint = document.getElementById("buttonPrint");
        var buttonExit = document.getElementById("buttonExit");
        
        if (divBanner != null){
            divBanner.style.display = "none";
        }
        if (divMenu != null){
            divMenu.style.display = "none";
        }
        buttonAssing.style.display="none";
        buttonPrint.style.display="none";
        buttonExit.style.display="none";
        mainmenu.style.visibility="hidden";
        dataSimHidden.style.display = "inline";
        
        window.print();
        dataSimHidden.style.display = "none";
        if (divMenu != null)
        {
            divMenu.style.display = "inline";
        }
        if (divBanner != null)
        {
            divBanner.style.display = "inline";
        }
        
        mainmenu.style.visibility="visible";
        buttonAssing.style.display="inline";
        buttonPrint.style.display="inline";
        buttonExit.style.display="inline";
        
    }

</script>

<table border="0" cellpadding="0" cellspacing="0" width="1250">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.simInventoryManagement", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" class="formArea2">

            <table width="70%" cellspacing="5" cellpadding="15" border="0" align="center">
                <tr>
                    <td align="center">
                        <p id="infoBasic">
                            <%
                            if(simBatchMode == null){%>
                                <%=String.format(Languages.getString("jsp.admin.tools.smsInventory.batchCreated", SessionData.getLanguage()), responseBatch.getArrSimsInserted().size())%>
                            <%}
                            else{%>
                                <%=String.format(Languages.getString("jsp.admin.tools.smsInventory.batchUpdated", SessionData.getLanguage()), responseBatch.getArrSimsInserted().size())%>
                            <%}%>
                            <BR>
                            
                        </p>
                        <p id="infoBatch">
                            BATCH ID : <%=responseBatch.getBatchId()%>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="dataSimHidden" id="dataSimHidden">
                            <table width="100%" cellspacing="0" cellpadding="0" border="1" align="center">
                                <tr>
                                    <% int countRow = 1;
                                        for (int i = 0; i < responseBatch.getArrSimsInserted().size(); i++) {
                                            if (countRow % 5 == 1) {
                                    %><tr><%
                                                    }
                                    %><td align="center"><%=responseBatch.getArrSimsInserted().get(i)%></td>

                                    <%if (countRow % 5 == 0) {
                                    %></tr><%
                                            }
                                            countRow++;
                                        }
                                    %>

                            </table>
                        </div>
                    </td>

                </tr>
                <tr>
                    <td align="center">
                        <button id="buttonAssing" onclick="goAssingInventory('')"><%=Languages.getString("jsp.admin.tools.smsInventory.assingInventory", SessionData.getLanguage())%></button>
                        <input type="button" id="buttonPrint" value="<%=Languages.getString("jsp.admin.reports.Print_This_Page",SessionData.getLanguage())%>" onclick="PrintReport();">
                        <button id="buttonExit" onclick="goToTools()"><%=Languages.getString("jsp.admin.tools.smsInventory.buttonExit", SessionData.getLanguage())%></button>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>