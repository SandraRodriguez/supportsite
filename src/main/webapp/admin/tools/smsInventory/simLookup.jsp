<%@page import="com.debisys.tools.simInventory.SimStatusPojo"%>
<%@page import="com.debisys.tools.simInventory.SimLookup"%>
<%@page import="com.debisys.tools.simInventory.SimEditProduct"%>
<%@page import="com.debisys.tools.simInventory.SimProductPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.SimTypesDao"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 41;
    String path = request.getContextPath();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>
    <link href="css/font-awesome/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link href="css/primeui-2.0-min.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="/support/includes/primeui/primeui-3.0.2-min.js" type="text/javascript" charset="utf-8"></script>
    <script language="JavaScript" src="/support/includes/jquery/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script language="JavaScript" src="/support/includes/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
    <script language="JavaScript" src="/support/includes/primeui/primeui-3.0.2-min.js" type="text/javascript" charset="utf-8"></script>
    
    <link href="/support/css/multiple-select.css" type="text/css" rel="stylesheet">
    <link href="/support/css/simLookup.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="/support/js/multiple-select.js" type="text/javascript" charset="utf-8"></script>
    
    <script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script> 
    <script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
    <script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
    <link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
    <link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/> 
    <link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>
        
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}    
    .w300 {
        width: 300px;
    }    
</style>
<%
    String simNumber = Languages.getString("jsp.admin.reports.sim.simNumber", SessionData.getLanguage());
    String productId = Languages.getString("jsp.admin.reports.sim.ProductId", SessionData.getLanguage());
    String productName = Languages.getString("jsp.admin.reports.sim.ProductName", SessionData.getLanguage());
    String merchantId = Languages.getString("jsp.admin.reports.sim.merchantId", SessionData.getLanguage());
    String merchantName = Languages.getString("jsp.admin.reports.sim.merchantName", SessionData.getLanguage());
    String isoId = Languages.getString("jsp.admin.reports.sim.isoId", SessionData.getLanguage());
    String isoName = Languages.getString("jsp.admin.reports.sim.isoName", SessionData.getLanguage());
    String creationDateBatch = Languages.getString("jsp.admin.reports.sim.batchFileLoadDate", SessionData.getLanguage());
    String batchId = Languages.getString("jsp.admin.reports.sim.batchid", SessionData.getLanguage());
    String statusDescription = Languages.getString("jsp.admin.reports.sim.status", SessionData.getLanguage()); 
    String validationOnlyDigits =  Languages.getString("jsp.admin.tools.smsInventory.validation.onlyDigits", SessionData.getLanguage()); 
    String completeSimNumber =  Languages.getString("jsp.admin.tools.smsInventory.validation.completeSimNumber", SessionData.getLanguage()); 
    String firstNDigits =  Languages.getString("jsp.admin.tools.smsInventory.validation.firstNDigits", SessionData.getLanguage()); 
%>
<script type="text/javascript">
     
var uri = '/support/admin/tools/smsInventory/SimLookupDB.jsp';
var dataSearchSims;
var momentStart = moment();
var momentEnd= moment().add(96, 'days');
    
    $(function() {
        var simCarriers = "";
        var carriersCount = 0;
        var carriersSel = 0;        
        var simProducts = "";
        var productsCount = 0;
        var productsSel = 0;
        var simStatus="";
            
        initDateControl();
        
        $("#tdResults").hide();
        $('#simProducts').multipleSelect();
        $('#simStatusList').multipleSelect();
        
        $('#simProviderList').multipleSelect({
            onOpen: function() {                         },
            onClose: function() {                        },
            onCheckAll: function() {
                $('#individualSim').val("");
                //$('#batchId').val("");
            },
            onUncheckAll: function() {                   },
            onFocus: function() {                        },
            onBlur: function() {                         },
            onOptgroupClick: function(view) {            },
            onClick: function(view) {
                $('#individualSim').val("");
                //$('#batchId').val("");
            }
        });
        
        $( "#batchId" ).on( "blur", function() { 
            if ($("#batchId").val().length>0 && !$.isNumeric( $("#batchId").val()) ){
                $('#batchId').val("");
                messageValidation();
                $('#batchId').focus(100);
            }   
        });
        $( "#filterSims" ).on( "blur", function() {
            if ($.isNumeric( $("#filterSims").val()) ){
                $('#individualSim').val(""); 
            } else if ( $("#filterSims").val().length>0) {
                messageValidation();
                $('#filterSims').focus(100);
            }
        });
        
        $( "#individualSim" ).on( "blur", function() { 
            if ($.isNumeric( $("#individualSim").val()) ){
                $('#batchId').val("");
                $('#filterSims').val("");
                $('#simProviderList').multipleSelect('uncheckAll');
                $('#simProducts').multipleSelect('uncheckAll');                
            } else if ( $("#individualSim").val().length>0) {
                messageValidation();
                $('#individualSim').val("");
                $('#individualSim').focus(100);
            }            
        });                
        
        $( "#simProviderList" ).change(function() {
            carriersCount = 0;
            carriersSel = 0;
            simCarriers = "";
            $("#simProviderList option").each(function() {
                carriersCount++;                    
            });                                
            $("#simProviderList option:selected").each(function() {
                carriersSel++;
                simCarriers += $(this).val() + ",";
            });
        });
        
        $( "#simProducts" ).change(function() {
            productsCount = 0;
            productsSel = 0;
            simProducts = "";
            $("#simProducts option").each(function() {
                productsCount++;                    
            });                                
            $("#simProducts option:selected").each(function() {
                productsSel++;
                simProducts += $(this).val() + ",";
            });
        });    
        
        $( "#simStatusList" ).change(function() {    
            simStatus = "";
            $("#simStatusList option:selected").each(function() {                
                simStatus += $(this).val() + ",";
            });
        });     
                
        $( "#searchProductsByCarriers" ).button().on( "click", function() {
            let combo = $( "#simProducts" );
            combo.empty();
            var data = {"action": "products"};
            if (carriersCount===carriersSel){
                data= $.extend(data, {"allCarriers": true} );
                $('#simProviderList').multipleSelect('checkAll');
            }else{
                if (simCarriers.length===0){
                    $('#simProviderList').multipleSelect('checkAll');
                }                
                data= $.extend(data, {"carriers": simCarriers} );
            }            
            $('#spinnerInfo').addClass("fa fa-refresh fa-spin");                        
            $.ajax({
                type: "GET",
                async: false,
                data: data,
                url: uri,
                success: function (data) { 
                    $.each(data, function (key, entry) {               
                        combo.append($('<option selected></option>').attr('value', entry.productId).text(entry.description));
                    });                    
                    setTimeout(function(){                    
                        $('#simProducts').multipleSelect({
                                isOpen: true,
                                keepOpen: true
                                });
                        $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");        
                        $('#simProducts').multipleSelect('focus');        
                      }, 500);
                }
            });
            
        });
        
        $( "#downloadSims" ).button().on( "click", function() { 
            $.ajax({
                type:"GET",
                data: $.extend(dataSearchSims, {"action": "download"} ),
                url:uri,
                dataType:"json",
                context:this,
                success:function(response){
                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                    $(location).attr('href',response);                    
                },
                error:function(err){
                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                    console.log(err);
                }
            });
        });
        
        $( "#searchSims" ).button().on( "click", function() { 
            dataSearchSims = {"action": "sims"};                        
            if ($("#filterSims").val().length>0){
                if ($("#filterSims").val().length<5){
                    alert('Please enter minimun 5 digits!');
                    return;
                } else{
                    dataSearchSims= $.extend(dataSearchSims, {"searchSims": $("#filterSims").val()} );
                }
            }                         
            if (simStatus.length>0){
                dataSearchSims = $.extend(dataSearchSims, {"statusSims": simStatus});
            }            
            if ($.isNumeric( $("#batchId").val()) ){
                dataSearchSims = $.extend(dataSearchSims, {"batchId": $("#batchId").val()});
            } 
            if ( $("#individualSim").val().length>0 ){
                dataSearchSims = $.extend(dataSearchSims, {"individualSim": $("#individualSim").val() } );
            } else{                      
                if (simProducts.length>0){
                    dataSearchSims= $.extend(dataSearchSims, {"skus": simProducts} );                        
                } else if (simCarriers.length>0 && simProducts.length===0){
                    if (carriersCount===carriersSel){
                        dataSearchSims= $.extend(dataSearchSims, {"allCarriers": "-1"} );
                    }else{
                        dataSearchSims= $.extend(dataSearchSims, {"carriers": simCarriers} );
                    }                    
                }
            }            
            if ( Object.keys(dataSearchSims).length > 1 ){                               
                fillSimsDataTable(dataSearchSims);
            } else{
                $('#tdResults').hide();
            }
        });        
    });
       
       
    function fillSimsDataTable(dataValues){        
        var totalRecords = 0;
        var finalDataCount = $.extend(dataValues, {action: 'count'});        
        $.ajax({
            type: "GET",
            async: false,
            data: finalDataCount,
            url: uri,
            success: function (data) {                        
                totalRecords = data.trim();
                if (totalRecords>0){
                    var info = totalRecords + " <%=Languages.getString("jsp.admin.results_found", SessionData.getLanguage())%> ";
                    $('#reportInfo').text(info);
                    $('#simsResult').puidatatable({
                        lazy:true,
                        paginator: {
                            rows: 15,
                            pageLinks:10,
                            totalRecords:totalRecords
                        },                        
                        columns: [
                            {field:'simNumber', bodyClass:'bodyClassData', headerStyle:'width:200px',  sortable:true, headerText: '<%=simNumber%>'},
                            {field:'statusCode', bodyClass:'bodyClassData', headerStyle:'width:150px', sortable:true, headerText: '<%=statusDescription%>', 
                                content: function(row){return formatStatus(row.statusCode);}},                           
                            {field:'simProductId', bodyClass:'bodyClassData', headerStyle:'width:120px', headerText: '<%=productId%>', sortable:true, headerClass:'header-tbl'},
                            {field:'simProductDescription', bodyClass:'bodyClassData', headerStyle:'width:200px', sortable:true, headerText: '<%=productName%>',
                                content: function(row){return formatByFieldName(row,'simProductDescription');}},                            
                            {field:'merchantName', bodyClass:'bodyClassData', sortable:true, headerStyle:'width:200px', headerText: '<%=merchantName%>',
                                content: function(row){return formatByFieldName(row,'merchantName');}},
                            {field:'merchantId', bodyClass:'bodyClassData', sortable:true, headerStyle:'width:140px', headerText: '<%=merchantId%>' },
                            {field:'isoName', bodyClass:'bodyClassData', sortable:true, headerStyle:'width:200px', headerText: '<%=isoName%>' },
                            {field:'isoId', bodyClass:'bodyClassData', sortable:true, headerStyle:'width:140px', headerText: '<%=isoId%>' },                                                        
                            {field:'batchId', bodyClass:'bodyClassData', sortable:true, headerStyle:'width:120px', headerText: '<%=batchId%>' },
                            {field:'creationDateBatch', bodyClass:'bodyClassData', headerStyle:'width:140px', sortable:true, headerText: '<%=creationDateBatch%>',
                                content: function(row){return formatByFieldName(row,'creationDateBatch');}}                            
                        ],                        
                        sortField:'simNumber',
                        sortOrder:'1',
                        datasource: function(callback, ui){
                            $('#spinnerInfo').addClass("fa fa-refresh fa-spin");
                            var start = ui.first + 1;
                            var sortField = ui.sortField;
                            var sortOrder = ui.sortOrder === 1 ? 'ASC' : 'DESC';
                            var rows = this.options.paginator.rows;
                            var data = {
                                "action": "sims", "start":start, "sortField": sortField, "sortOrder":sortOrder, "rows": (start+rows)                    
                            };
                            var finalDataFilter = $.extend(dataValues, data);
                            $.ajax({
                                type:"GET",
                                data:finalDataFilter,
                                url:uri,
                                dataType:"json",
                                context:this,
                                success:function(response){
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                                    callback.call(this, response);
                                    $('#tdResults').show();
                                    $(".colorized").parent("td").parent("tr").addClass('textRed');                                    
                                },
                                error:function(err){
                                    $('#spinnerInfo').removeClass("fa fa-refresh fa-spin");
                                    console.log(err);
                                }
                            });
                        },
                        resizableColumns: true,
                        columnResizeMode: 'expand'
                    });                                         
                    $("#reportInfo").fadeIn(1000);                    
                } else{  
                    $("#reportInfo").fadeIn(1000);
                    $("#reportInfo").text("No records!!!");
                    $("#reportInfo").fadeOut(3000);
                    $('#tdResults').hide();
                    
                }
            }
        });         
    
    }
    
    function formatStatus(rowInfo){
        console.log(rowInfo);
        var data = rowInfo.split("*");
        var statusDesc = data[1];
        if ('<%=SessionData.getLanguage()%>'==='english'){
            statusDesc = data[0];
        }
        return statusDesc;
    }
    
    function formatByFieldName(data, fieldName){
        var dataInfo = data[fieldName];
        if (dataInfo){
            dataInfo = dataInfo.trim();
        }
        if (fieldName==='merchantName'){
            
        } else if (fieldName==='simProductDescription'){
            if (dataInfo.length>30){
                var skuName = dataInfo.substring(0, 20);
                //dataInfo = "<a href='#' title='"+dataInfo+"'>"+skuName+"...</a>";
                dataInfo = "<a title='"+dataInfo+"' >"+skuName+"...</a>";
            }
        } else if (fieldName==='creationDateBatch'){ 
            dataInfo = dataInfo.substring(0, 16);
        }
        return dataInfo;
    }
    
    
    
    function initDateControl(){
        var days = momentEnd.diff(momentStart, 'days');
        $("#pDiffDays").text(days);
        $('input[name="startDate"]').daterangepicker({
            "minDate": momentStart,
            "showDropdowns": true,
            "showWeekNumbers": false,
            "singleDatePicker": true,
            "autoApply": true,
            "timePicker": false,
            "timePicker24Hour": false,
            "startDate": momentStart,
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                "daysOfWeek": [
                    '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                ],
                "monthNames": [
                    '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {
            momentStart=start;
            var days = momentEnd.diff(momentStart, 'days');
            $("#pDiffDays").text(days);
        }
        );
        $('input[name="endDate"]').daterangepicker({
            "minDate": momentStart,
            "showDropdowns": true,
            "showWeekNumbers": false,
            "singleDatePicker": true,
            "autoApply": true,
            "timePicker": false,
            "timePicker24Hour": false,
            "startDate": momentStart, 
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                "daysOfWeek": [
                    '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                ],
                "monthNames": [
                    '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                ],
                "firstDay": 1
            }            
            }, function (start, end, label) {
                momentEnd=start;
                var days = momentEnd.diff(momentStart, 'days');
                $("#pDiffDays").text(days);
            });  
    }
    
    function messageValidation() {
        $("#messagesValidation").fadeIn(1000);
        $("#messagesValidation").text("<%=validationOnlyDigits%>");
        $("#messagesValidation").fadeOut(3000);  
    }
</script>
                                    
<table border="0" cellpadding="10" cellspacing="10" width="100%" style="padding: 5px;">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.simInventory.simLookup", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" class="formArea2">
            
                <table width="100%" cellspacing="5" cellpadding="5" border="0" >
                    <tr>
                        <td>
                            <p style="margin: 3px;color:red" id="messagesValidation" name="messagesValidation"></p>
                        </td>
                    </tr>    
                    <tr style="height: 50px">
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.batchId", SessionData.getLanguage())%></td>  
                        <td><input type="text" name="batchId" id="batchId" value="" size="38" maxlength="20"/></td>

                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.individualSim", SessionData.getLanguage())%>
                        <br/>
                        <span style="font-size: 10px;">(<%=completeSimNumber%>)</span>                        
                        </td>  
                        <td><input type="text" name="individualSim" id="individualSim" value="" size="38" maxlength="20"/></td>
                    </tr>
                    <tr style="height: 50px">
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simProviderList" id="simProviderList" class="w300" multiple="multiple">
                                <%
                                    List<SimProviderPojo> simProviderList = SimProviderDao.getSimCarrierList();
                                    for (SimProviderPojo currentSimProvider : simProviderList) {
                                        out.println("<option value=" + currentSimProvider.getId() + ">" + currentSimProvider.getName() + "</option>");
                                    }
                                    String getProducts = Languages.getString("jsp.admin.reports.sim.getProducts", SessionData.getLanguage());

                                %>
                            </select>
                            <input type="button" class="submitClass" id="searchProductsByCarriers" value="<%=getProducts%>"/>  
                        </td>                                                
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simProducts" id="simProducts" tabindex="11" class="w300" multiple="multiple"></select>                            
                        </td>                                     
                    </tr>
                    <tr style="height: 50px">
                        <td class="main"><%=statusDescription%></td>
                        <td>
                            <select name="simStatusList" id="simStatusList" class="w300" multiple="multiple">
                                <%
                                    List<SimStatusPojo> simsStaatusList = SimLookup.findSimsStatus();
                                    for (SimStatusPojo status : simsStaatusList) {
                                        out.println("<option value=" + status.getId() + ">" + status.getDescriptionEng() + "</option>");
                                    }
                                %>
                            </select>                            
                        </td>
                        <%                            
                            String filterSims = Languages.getString("jsp.admin.reports.sim.filterSims", SessionData.getLanguage());
                        %>
                        <td class="main"><%=filterSims%> <br/>
                            <span style="font-size: 10px;">(<%=firstNDigits%>)</span>
                        </td>
                        <td>
                            <input type="text" id="filterSims" name="filterSims" size="38" maxlength="20"/>  
                            <input type="button" class="submitClass" id="searchSims" name="searchSims" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonSearch", SessionData.getLanguage())%>"/>                            
                            
                        </td>                        
                    </tr>                                       
                    <tr>
                        <td>
                            <p style="margin: 3px;" id="reportInfo"></p>
                        </td>
                        <td>
                            <p style="margin: 3px;" id="spinnerInfo">
                                <i id="spinner" style="font-size: 20px; margin-left: 10px;"></i>
                            </p>                                                                               
                            
                        </td>
                    </tr>
                    
                </table>            
        </td>
    </tr>
    <tr>
        <td colspan="3" id="tdResults" name="tdResults" style="overflow: scroll; width:100%;" >
            <input type="button" class="submitClass" id="downloadSims" value="Download"/>                                                        
            <div id="simsResult" name="simsResult" style="padding: 5px;" ></div>
        </td>
    </tr>    
</table>
                            
<%@ include file="/includes/footer.jsp" %>