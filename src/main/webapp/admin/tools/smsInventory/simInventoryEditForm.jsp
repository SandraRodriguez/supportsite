<%-- 
    Document   : simInventoryEditProcess
    Created on : Nov 10, 2016, 1:58:22 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.simInventory.SimProductPojo"%>
<%@page import="com.debisys.tools.simInventory.SimTypesDao"%>
<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.SimPojo"%>
<%@page import="java.util.Date"%>
<%@page import="com.debisys.tools.simInventory.BatchesInfo"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>


<%
    int section = 9;
    int section_page = 9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>

<script src="js/simInventory.js" type="text/javascript"></script>
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}

    select {
        width : 180px;
    }
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }

    /* Popups of Sim Carrier select*/
    #popUpDivCarriers{
        width:200px;
        height:100px;
        position:absolute;
        top:50%;
        left:50%;
        margin:-50px 0 0 -100px; /* [-(height/2)px 0 0 -(width/2)px] */
        display:none;
        background-color: rgba(134, 174, 40, 0.8);
    }
    #simProviderList{
        z-index: 1000;
        position: absolute;
        top: 50%;
        left: 10px;
    }

    /* Popups of Sim Type select*/
    #popUpDivTypes{
        width:250px;
        height:100px;
        position:absolute;
        top:50%;
        left:50%;
        margin:-50px 0 0 -100px; /* [-(height/2)px 0 0 -(width/2)px] */
        display:none;
        background-color: rgba(134, 174, 40, 0.8);
    }
    #simTypeList{
        z-index: 1000;
        position: absolute;
        top: 50%;
        left: 15px;
    }
    
    /* Popups of Sim Product select*/
    #popUpDivProducts{
        width:350px;
        height:100px;
        position:absolute;
        top:50%;
        left:50%;
        margin:-50px 0 0 -100px; /* [-(height/2)px 0 0 -(width/2)px] */
        display:none;
        background-color: rgba(134, 174, 40, 0.8);
    }
    #simProductList{
        z-index: 1000;
        position: absolute;
        top: 50%;
        left: 15px;
    }


</style>

<%
    List<SimProviderPojo> simProviderList = SimProviderDao.getSimCarrierList();
    List<SimTypesPojo> simTypesList = SimTypesDao.getSimTypesList();
    String batchId = request.getParameter("batchId");
    String batchCode = request.getParameter("batchCode");
    List<SimPojo> simsPojoList = SimInventory.getSimNumberListsObjectsByBatchId(batchId);
    
    List<SimProductPojo> simCarrierProducts = new ArrayList<SimProductPojo>();
    if(simProviderList != null && !simProviderList.isEmpty()){
        simCarrierProducts = SimProviderDao.getSimCarrierProductsList("'"+simProviderList.get(0).getId()+"'");
    }

%>

<script type="text/javascript">

    var count = <%=simsPojoList.size()%>;
    var idRowSelected = '';
    var currentCarrierCache = '';
    $(document).ready(function () {

        $("#dialog-message").hide();

        var wrapper = $('.field_wrapper'); //Input field wrapper
        // Edit Carrier buttons action  ***********************************************
        $(wrapper).on('click', '.edit_carrier_button', function (e) {
            idRowSelected = $(this).attr('id');
            var arrayTemp = idRowSelected.split("_");
            var position = $(this).position();
            $("#popUpDivCarriers").css('left', position.left);
            $("#popUpDivCarriers").css('top', position.top);
            $("#simProviderList").val($("#simsCarrierHidden_" + arrayTemp[1]).val());
            $(document).ready(function () {
                $("#popUpDivCarriers").show();
            });
        });

        $("#simProviderList").change(function (e) {
            var arrayTemp = idRowSelected.split("_");
            var index = arrayTemp[1];
            $('#simsCarrier_' + index).val($("#simProviderList option:selected").text());
            $('#simsCarrierHidden_' + index).val($("#simProviderList").val());
            $("#popUpDivCarriers").hide();
            
            if(currentCarrierCache !== $("#simProviderList").val()){
                $('#simsProduct_'+arrayTemp[1]).val('');
                $('#simsProductHidden_'+arrayTemp[1]).val('');
                $('#carrierForProductHidden_'+arrayTemp[1]).val($("#simProviderList").val());
                updateDynamicProductList($("#simProviderList").val());
            }
        });


        // EDIT SIM TYPES BUTTON ACTION ***********************************************
        $(wrapper).on('click', '.edit_type_button', function (e) {
            idRowSelected = $(this).attr('id');
            var arrayTemp = idRowSelected.split("_");
            var position = $(this).position();
            $("#popUpDivTypes").css('left', position.left);
            $("#popUpDivTypes").css('top', position.top);
            $("#simTypeList").val($("#simsTypeHidden_" + arrayTemp[1]).val());
            $(document).ready(function () {
                $("#popUpDivTypes").show();
            });
        });

        $("#simTypeList").change(function (e) {
            var arrayTemp = idRowSelected.split("_");
            var index = arrayTemp[1];
            //alert(idRowSelected+" - "+index);
            $('#simsType_' + index).val($("#simTypeList option:selected").text());
            $('#simsTypeHidden_' + index).val($("#simTypeList").val());
            $("#popUpDivTypes").hide();
        });
        
        // EDIT SIM PRODUCTS BUTTON ACTION ***********************************************
        $(wrapper).on('click', '.edit_product_action', function (e) {
            idRowSelected = $(this).attr('id');
            var arrayTemp = idRowSelected.split("_");
            var position = $(this).position();
            $("#popUpDivProducts").css('left', position.left);
            $("#popUpDivProducts").css('top', position.top);
            
            if(currentCarrierCache !== $("#simsCarrierHidden_" + arrayTemp[1]).val()){
                updateDynamicProductList($("#simsCarrierHidden_" + arrayTemp[1]).val());
            }
            
            $("#simProductList").val($("#simsProductHidden_" + arrayTemp[1]).val());
            $(document).ready(function () {
                $("#popUpDivProducts").show();
            });
        });

        $("#simProductList").change(function (e) {
            var arrayTemp = idRowSelected.split("_");
            var index = arrayTemp[1];
            $('#simsProduct_' + index).val($("#simProductList option:selected").text());
            $('#simsProductHidden_' + index).val($("#simProductList").val());
            $("#popUpDivProducts").hide();
        });
    });
    
    function updateDynamicProductList(currentCarrier){
        currentCarrierCache = currentCarrier;
        $('#simProductList').html('');
        $.ajax({
            type: "POST",
            async: false,
            data: {carrierIdnew: currentCarrier, action: 'getProductsByCarrierIdNew'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data)).split("\n");
                $('#simProductList').append('<option value=""></option>');
                for (var i = 0; i < array_data.length; i++) {
                    if (array_data[i] !== '') {
                        var productData = String($.trim(array_data[i])).split("|");
                        $('#simProductList').append('<option value="' + productData[0] + '">' + productData[1] + '</option>');
                    }
                }
            }
        });
    }
    
    function updateCarrierProducts(){
        var carrierIdnew = $("#simProviderGenericList").val().trim();
        $('#simProductGenericList').html('');
        $('#simProductGenericList').append('<option value=""></option>');
        $.ajax({
            type: "POST",
            async: false,
            data: {carrierIdnew: carrierIdnew, action: 'getProductsByCarrierIdNew'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data)).split("\n");
                for (var i = 0; i < array_data.length; i++) {
                    if (array_data[i] !== '') {
                        var productData = String($.trim(array_data[i])).split("|");
                        $('#simProductGenericList').append('<option value="' + productData[0] + '">' + productData[1] + '</option>');
                    }
                }
            }
        });
    }

    function validateForm2() {

        var i, j, elemA = 0, totalDuplicates = 0;

        //VERIFY SIM LIST
        var simsArray = document.getElementsByName("sims");
        for (i = 0; i < simsArray.length; i++) {
            for (j = 0; j < simsArray.length; j++) {
                if (simsArray[i].value === simsArray[j].value) {
                    elemA++;
                }
            }
            simsArray[i].style.backgroundColor = "white";
            if (elemA > 1) {
                totalDuplicates++;
                simsArray[i].style.backgroundColor = "red";
            }
            elemA = 0;
        }

        var wantToSave = true;
        if (totalDuplicates > 0) {
            $("span").text("<%=Languages.getString("jsp.admin.tools.smsInventory.errorDuplicateSims", SessionData.getLanguage())%> ").show().fadeOut(10000);
            $("#submit").prop("disabled", false);
            document.getElementById('div_show_charge').style.display = "none";
            wantToSave = false;
        }


        if (wantToSave) { // verify in DB
            var strValues = [];
            for (i = 0; i < simsArray.length; i++) {
                strValues[i] = simsArray[i].value;
            }
            if (strValues != null && strValues.length > 0 && strValues != '') {
                var passedValidation = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    data: {sims: strValues, action: 'verifySimsToEdit', simBatchId: '<%=batchId%>'},
                    url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
                    success: function (data) {
                        var array_data = String($.trim(data)).split("\n");
                        if (array_data[0] !== 'NOTHING') {
                            var totalDuplicatesDB = 0;
                            for (i = 0; i < array_data.length; i++) {
                                for (j = 0; j < simsArray.length; j++) {
                                    if ($.trim(array_data[i]) === $.trim(simsArray[j].value) || $.trim(simsArray[j].value) === '') {
                                        simsArray[j].style.backgroundColor = "red";
                                        totalDuplicatesDB++;
                                    }
                                }
                            }
                            if (totalDuplicatesDB === simsArray.length) {
                                $("span").text("<%=Languages.getString("jsp.admin.tools.smsInventory.cannotInsertSims", SessionData.getLanguage())%> ").show().fadeOut(10000);
                                $("#submit").prop("disabled", false);
                                document.getElementById('div_show_charge').style.display = "none";
                                passedValidation = false;
                            }
                            else if (totalDuplicatesDB > 0) {
                                $("span").text("<%=Languages.getString("jsp.admin.tools.smsInventory.errorDuplicateSimsSaved", SessionData.getLanguage())%> ").show().fadeOut(10000);

                                $("#submit").prop("disabled", false);
                                document.getElementById('div_show_charge').style.display = "none";
                                passedValidation = false;

                            }
                        }
                        else {
                            passedValidation = true;
                        }

                    }
                });
                $("#submit").prop("disabled", false);
                document.getElementById('div_show_charge').style.display = "none";
                return passedValidation;
            }
            $("#submit").prop("disabled", false);
            document.getElementById('div_show_charge').style.display = "none";
            return false;
        }
        else {
            $("#submit").prop("disabled", false);
            document.getElementById('div_show_charge').style.display = "none";
            return wantToSave;
        }
    }

    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }


</script>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="ventana_flotante">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>

<table border="0" cellpadding="0" cellspacing="0" width="1600" >
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.simInventoryEdit", SessionData.getLanguage()).toUpperCase()%> [<%=batchCode%>]</td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>

    <tr>

        <td colspan="3" class="formArea2">

            <span id="spanWarningMessages"></span>

            <form id="mainform2" name="mainform2" class="reportForm2" name="mainform2" method="post" action="admin/tools/smsInventory/simInventoryManagementConfirm.jsp" onsubmit="return validateForm2();">

                <div id= "field_wrapper" class="field_wrapper">

                    <table width="100%"  border="0" class="formArea2">
                        <tr>
                            <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                            <td>
                                <select name="simProviderGenericList" id="simProviderGenericList" tabindex="11" onchange="updateCarrierProducts();">
                                    <%
                                        for (SimProviderPojo currentSimProvider : simProviderList) {
                                            out.println("<option value=\"" + currentSimProvider.getId() + "\">" + currentSimProvider.getName() + "</option>");
                                        }
                                    %>
                                </select>
                            </td>
                            <td width="70%">
                                <input type="checkbox" id="checkCarrierAll" onchange="checkAllCarriers(this, '<%=Languages.getString("jsp.admin.tools.smsInventory.confirmThisAction", SessionData.getLanguage())%>')"><%=Languages.getString("jsp.admin.tools.smsInventory.applyToAll", SessionData.getLanguage())%>
                            </td>
                        </tr>
                        <tr>
                            <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simType", SessionData.getLanguage())%></td>
                            <td>
                                <select name="simTypeGenericList" id="simTypeGenericList" tabindex="11"> 
                                    <%
                                        String language = Languages.getLanguage();
                                        for (SimTypesPojo currentSimType : simTypesList) {
                                            out.println("<option value=\"" + currentSimType.getId() + "\" >" + ((language.equals("english")) ? currentSimType.getDescriptionEng() : currentSimType.getDescriptionEng()) + "</option>");
                                        }
                                    %>
                                </select>
                            </td>
                            <td width="70%">
                                <input type="checkbox" id="checkTypeAll" onchange="checkAllTypes(this, '<%=Languages.getString("jsp.admin.tools.smsInventory.confirmThisAction", SessionData.getLanguage())%>')"><%=Languages.getString("jsp.admin.tools.smsInventory.applyToAll", SessionData.getLanguage())%>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>
                            <td>
                                <select name="simProductGenericList" id="simProductGenericList" tabindex="11"> 
                                    <%
                                        out.println("<option value=''></option>");
                                        for (SimProductPojo currentSimProduct : simCarrierProducts) {
                                            out.println("<option value='" + currentSimProduct.getProductId() + "' >" + currentSimProduct.getDescription()+ "</option>");
                                        }
                                    %>
                                </select>
                            </td>
                            <td width="70%">
                                <input type="checkbox" id="checkProductAll" onchange="checkAllProducts(this, '<%=Languages.getString("jsp.admin.tools.smsInventory.msgUpdateAllSimProducts", SessionData.getLanguage())%>')"><%=Languages.getString("jsp.admin.tools.smsInventory.applyToAll", SessionData.getLanguage())%>
                            </td>
                        </tr>
                    </table>

                    <div id="left">
                        <table width="80%" cellspacing="10" cellpadding="15" border="0" >
                            <tr><td><font color="#FF4500"><%=Languages.getString("jsp.admin.tools.smsInventory.messageCanNotBeChanged", SessionData.getLanguage())%></font></td></tr>
                        </table>
                    </div>
                    <div id="left">
                        <table width="10%" cellspacing="10" cellpadding="15" border="0" >

                        </table>
                    </div>

                    <table width="85%"  border="0" class="formArea2">

                        <tr>
                            <td class="titles" width="25%" align="center">Sim</td>
                            <td class="titles" width="20%" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                            <td class="titles" width="20%" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.simType", SessionData.getLanguage())%></td>
                            <td class="titles" width="25%"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>

                            <%if (simsPojoList.size() > 1) {%>
                            <td align="center"> - </td>
                            <td class="titles" width="25%" align="center">Sim</td>
                            <td class="titles" width="20%" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                            <td class="titles" width="20%" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.simType", SessionData.getLanguage())%></td>
                            <td class="titles" width="25%"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>
                            <%}%>
                        </tr>

                        <%
                            for (int i = 0; i < simsPojoList.size(); i++) {
                                SimPojo sim = simsPojoList.get(i);
                        %>
                        <%if (i % 2 == 0) {%>
                        <tr><%}%>
                            <td align="left">

                                <%if (sim.getStatusCode().equalsIgnoreCase("R") || sim.getStatusCode().equalsIgnoreCase("AS")) {%>
                                <input type="text" name="sims" value="<%=sim.getSimNumber()%>" id="sim_0" size="25" maxlength="20"/>
                                <input type="hidden" name="simsId" id="simIdHidden_<%=i%>" value="<%=sim.getId()%>" />
                                <%}
                                else {%>
                                <font color="#FF4500"><%=sim.getSimNumber()%></font>
                                <%}%>

                            </td>
                            <td align="left">
                                <%if (sim.getStatusCode().equalsIgnoreCase("R") || sim.getStatusCode().equalsIgnoreCase("AS")) {%>
                                <input type="text" name="simsCarrier" class="edit_carrier_button" value="<%=sim.getSimProviderName()%>" id="simsCarrier_<%=i%>" size="15" maxlength="15" readonly/>
                                <input type="hidden" name="simsCarrierHidden" id="simsCarrierHidden_<%=i%>" value="<%=sim.getSimProviderId()%>" />
                                <%}
                                else {%>
                                <font color="#FF4500"><%=sim.getSimProviderName()%></font>
                                <%}%>
                            </td>
                            <td align="left">
                                <%if (sim.getStatusCode().equalsIgnoreCase("R") || sim.getStatusCode().equalsIgnoreCase("AS")) {%>
                                <input type="text" name="simsType" class="edit_type_button" value="<%=sim.getSimTypeDescription()%>" id="simsType_<%=i%>" size="25" maxlength="20" readonly/>
                                <input type="hidden" name="simsTypeHidden" id="simsTypeHidden_<%=i%>" value="<%=sim.getSimTypeId()%>" />
                                <%}
                                else {%>
                                <font color="#FF4500"><%=sim.getSimTypeDescription()%></font>
                                <%}%>
                            </td>
                            
                            <td align="left">
                                <%if (sim.getStatusCode().equalsIgnoreCase("R") || sim.getStatusCode().equalsIgnoreCase("AS")) {%>
                                <input type="text" name="simsProduct" class="edit_product_action" value="<%=sim.getSimProductDescription()%>" id="simsProduct_<%=i%>" size="25" maxlength="20" readonly/>
                                <input type="hidden" name="simsProductHidden" id="simsProductHidden_<%=i%>" value="<%=sim.getSimProductId()%>" />
                                <%}
                                else {%>
                                <font color="#FF4500"><%=sim.getSimProductDescription()%></font>
                                <%}%>
                            </td>

                            <td align="center"> - </td>                                



                            <%if (i % 2 != 0) {%>
                        </tr>

                        <%
                                }
                            }
                        %>
                    </table>

                    <table width="100%" cellspacing="10" cellpadding="15" border="0" >
                        <tr>
                            <td align="center">
                                <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonVerifyAndProcess", SessionData.getLanguage())%>"/>
                                <button id="buttonExit" type="button" onclick="goToTools()"><%=Languages.getString("jsp.admin.tools.smsInventory.cancel", SessionData.getLanguage())%></button>
                                <input type="hidden" name="simBatchMode" id="simBatchMode" value="edit" />
                                <input type="hidden" name="simBatchId" id="simBatchId" value="<%=batchId%>" />
                                <input type="hidden" name="batchCode" id="batchCode" value="<%=batchCode%>" />
                            </td>
                        </tr>
                    </table>

                </div>

                <!-- DIV CARRIERS -->
                <div id="popUpDivCarriers">
                    <a href="javascript:closePopupCarriers();" class="close_popup_carriers" style="float:right"><img src="images/icn-redx1.gif"/></a>
                    <p style="font-weight: bold;font-size:120%;">SELECT SIM CARRIER</p>
                    <select name="simProviderList" id="simProviderList" tabindex="11">
                        <%
                            for (SimProviderPojo currentSimProvider : simProviderList) {
                                out.println("<option value=\"" + currentSimProvider.getId() + "\" >" + currentSimProvider.getName() + "</option>");
                            }
                        %>
                    </select>
                </div>

                <!-- DIV SIM TYPES -->
                <div id="popUpDivTypes">
                    <a href="javascript:closePopupTypes();" class="close_popup_types" title="Remove field" style="float:right"><img src="images/icn-redx1.gif"/></a>
                    <p style="font-weight: bold;font-size:120%;">SELECT SIM TYPE</p>
                    <select name="simTypeList" id="simTypeList" tabindex="11"> 
                        <%
                            for (SimTypesPojo currentSimType : simTypesList) {
                                out.println("<option value=\"" + currentSimType.getId() + "\" >" + ((language.equals("english")) ? currentSimType.getDescriptionEng() : currentSimType.getDescriptionEng()) + "</option>");
                            }
                        %>
                    </select>
                </div>
                    
                <!-- DIV SIM PRODUCTS -->
                <div id="popUpDivProducts">
                    <a href="javascript:closePopupProducts();" class="close_popup_types" title="Close" style="float:right"><img src="images/icn-redx1.gif"/></a>
                    <p style="font-weight: bold;font-size:120%;">SELECT SIM PRODUCT</p>
                    <select name="simProductList" id="simProductList" tabindex="11"> 
                        <%
                            out.println("<option value='0' ></option>");
                            for (SimProductPojo currentSimProduct : simCarrierProducts) {
                                out.println("<option value='" + currentSimProduct.getProductId() + "' >" + currentSimProduct.getDescription()+ "</option>");
                            }
                        %>
                    </select>
                </div>

            </form>
        </td>

    </tr>

</table>


<%@ include file="/includes/footer.jsp" %>