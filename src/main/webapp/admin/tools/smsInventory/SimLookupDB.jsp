    <%@page import="com.debisys.languages.Languages"%>
<%@page import="com.debisys.reports.summarys.DownloadsSummary"%>
<%@page import="java.util.Map"%>
<%@page import="com.debisys.tools.simInventory.SimEditProduct"%>
<%-- 
    Document   : SimEditProductDB
    Created on : Nov 23, 2018, 3:07:06 PM
    Author     : nmartinez
--%>
<%@page import="com.debisys.utils.QueryReportData"%>
<%@page import="com.debisys.tools.simInventory.SimPojo"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.debisys.utils.JSONObject"%>
<%@page import="com.debisys.utils.JSONArray"%>
<%@page import="com.debisys.tools.simInventory.SimLookup"%>
<%@page import="com.debisys.tools.simInventory.SimProductPojo"%>
<%@page import="java.util.List"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
    
    String action = request.getParameter("action");
    QueryReportData reporInfo = new QueryReportData();
    if (request.getParameter("batchId")!=null){
        reporInfo.setBatchId(request.getParameter("batchId"));
    } else if (request.getParameter("individualSim")!=null){
        reporInfo.setIndividualSim(request.getParameter("individualSim"));
    } else if (request.getParameter("carriers")!=null){
        reporInfo.setCarriers(request.getParameter("carriers"));
    } else if (request.getParameter("allCarriers")!=null){
        reporInfo.setCarriers("ALL");
    } else if (request.getParameter("skus")!=null){
        reporInfo.setProducts(request.getParameter("skus"));
    }
    if (request.getParameter("searchSims")!=null){//
        reporInfo.setSimInfo(request.getParameter("searchSims"));
    }
    
    if (request.getParameter("statusSims")!=null){
        reporInfo.setStatus(request.getParameter("statusSims").toString());
    }
    
    if (action.equals("count") || action.equals("sims")) {        
        
        String start = null;
        String end = null;
        String sortField = null;
        String sortOrder = null;
        if (action.equals("sims")){
            start = request.getParameter("start");
            end = request.getParameter("rows");
            sortField = request.getParameter("sortField");
            sortOrder = request.getParameter("sortOrder");
            reporInfo.setInfoReportData(sortField, sortOrder, start, end);   
        }        
        if (action.equals("sims")){
            List<SimPojo> list = SimLookup.getSims(reporInfo);
            String json = new Gson().toJson(list);
            out.print(json);
        } else if (action.equals("count")) {
            Integer countRows = SimLookup.getCountSims(reporInfo);
            out.print(countRows);
        }
        
    } else if (action.equals("products")){
        
        List<SimProductPojo> listProducts = null;
        if (request.getParameter("allCarriers")!=null){
            listProducts = SimEditProduct.getSimProductsList(null);
        } else{
            String carriers = request.getParameter("carriers");
            listProducts = SimEditProduct.getSimProductsList(carriers);
        }
        response.setContentType("application/json");
        String json = new Gson().toJson(listProducts);
        out.print(json);
        
    } else if (action.equals("newProducts")){
        response.setContentType("application/json");
        List<SimProductPojo> listProducts = SimEditProduct.getSimProductsList(null);
        String json = new Gson().toJson(listProducts);
        out.print(json);        
    } else if(action.equals("download")){
        String simNumber = Languages.getString("jsp.admin.reports.sim.simNumber", SessionData.getLanguage());
        String productId = Languages.getString("jsp.admin.reports.sim.ProductId", SessionData.getLanguage());
        String productName = Languages.getString("jsp.admin.reports.sim.ProductName", SessionData.getLanguage());
        String merchantId = Languages.getString("jsp.admin.reports.sim.merchantId", SessionData.getLanguage());
        String merchantName = Languages.getString("jsp.admin.reports.sim.merchantName", SessionData.getLanguage());
        String isoId = Languages.getString("jsp.admin.reports.sim.isoId", SessionData.getLanguage());
        String isoName = Languages.getString("jsp.admin.reports.sim.isoName", SessionData.getLanguage());
        String creationDateBatch = Languages.getString("jsp.admin.reports.sim.batchFileLoadDate", SessionData.getLanguage());
        String batchId = Languages.getString("jsp.admin.reports.sim.batchid", SessionData.getLanguage());
        String statusDescription = Languages.getString("jsp.admin.reports.sim.status", SessionData.getLanguage());
        String titles = simNumber +","+ statusDescription+","+productId+","+productName+","+merchantName+","+merchantId+","+
                isoName+","+isoId+","+batchId+","+creationDateBatch+"\n";
                
        String strUrlLocation = SimLookup.generateCsvReport(reporInfo, application, SessionData, titles );
        String json = new Gson().toJson(strUrlLocation);
        out.print(json);        
    }

%>