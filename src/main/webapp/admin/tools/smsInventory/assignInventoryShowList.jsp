<%@page import="com.debisys.tools.simInventory.BatchesInfo"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 9;
%>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>

<script src="js/simInventory.js" type="text/javascript"></script>
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}

    .titles{
        font-weight: bold;
    }

</style>


<script type="text/javascript">

    $(function () {
        $(document).ready(function () {
            $("#dialog-message").hide();
        });
    });

    function showSims(id, batchId) {

        $.ajax({
            type: "POST",
            async: false,
            data: {batchId: id, action: 'getSimsList'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data)).split("\n");
                if (array_data[0] !== 'NOTHING') {

                    var text = "";
                    text += "<tr class=\"rowhead2\"><td class=\"titles\" align=\"center\">SIM NUMBER</td><td class=\"titles\" align=\"center\" >SIM CARRIER</td><td class=\"titles\" align=\"center\">SIM TYPE</td><td class=\"titles\" align=\"center\">SIM PRODUCT</td><td class=\"titles\" align=\"center\">STATUS</td></tr>";
                    if (array_data.length >= 1) {
                        var countRow = 1;
                        var arrTemp;
                        for (var i = 0; i < array_data.length; i++) {

                            arrTemp = array_data[i].split(",");
                            if(countRow % 2 === 0){
                                text += "<tr class=\"row1\">";
                            }
                            else{
                                text += "<tr class=\"row2\">";
                            }
                            
                            text += "<td>" + arrTemp[0] + "</td>";
                            text += "<td>" + arrTemp[1] + "</td>";
                            text += "<td>" + arrTemp[2] + "</td>";
                            text += "<td>" + arrTemp[3] + "</td>";
                            text += "<td>" + (($.trim(arrTemp[4]) === 'A')? 'Activated':'Available') + "</td>";
                            text += "</tr>";
                            countRow++;
                        }
                    }
                    if (text.length == 0) {
                        text = "<tr><td>There are not SIMs.</td></tr>";
                    }

                    text = "<table border=\"0\" cellspacing=\"4\" cellpadding=\"4\"> " + text + "</table>";
                    $("#pDialogMessage").html(text);

                    var opt = {
                        autoOpen: false,
                        modal: true,
                        width: 900,
                        height: 650,
                        title: 'Batch Id: ' + batchId,
                        html: text
                    };

                    $(document).ready(function () {
                        $("#dialog-message").dialog(opt).dialog("open");
                    });


                }
                else {
                    alert("There are not data");
                }

            }
        });
    }

</script>

<%    String batchId = request.getParameter("batchId");
    String individualSim = request.getParameter("individualSim");
    String simProvider = request.getParameter("simProviderList");
    String simType = request.getParameter("simTypeList");
    String startDate = request.getParameter("startDate");
    String endDate = request.getParameter("endDate");

    String searchType = request.getParameter("searchType");

    List<BatchesInfo> batchesList = null;
    if (searchType != null && searchType.equals("ALL")) {
        batchesList = SimInventory.getBatchesList(SessionData.getProperty("iso_id"), false);
    }
    else {
        batchesList = SimInventory.getBatchesList(batchId, individualSim, simProvider, simType, SessionData.getProperty("iso_id"), startDate, endDate, false, false);
    }
%>




<table border="0" cellpadding="0" cellspacing="0" width="1250" >
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.simInventoryManagement", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>

    <tr>
        <td>
            <div id="dialog-message" title="SIMs" >
                <p>
                    <span class="spanTextBatchId" style="float:left; margin:0 7px 50px 0;"></span>
                </p>
                <p id="pDialogMessage"></p>
            </div>
        </td>
    </tr>
    <tr>
        <br>

        <td colspan="3" class="formArea2" align="center">

            <table width="100%" cellspacing="1" cellpadding="1" border="0" >
                <tr class="rowhead2">
                    <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.batchId", SessionData.getLanguage())%></td>
                    <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.quantity", SessionData.getLanguage())%></td>
                    <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.loadDate", SessionData.getLanguage())%></td>
                    <td class="titles" align="center"><%=Languages.getString("jsp.admin.tools.smsInventory.action", SessionData.getLanguage())%></td>
                </tr>

                <%
                    for (int i = 0; i < batchesList.size(); i++) {
                        BatchesInfo info = batchesList.get(i);
                %>
                    <tr class="row<%=(i % 2 == 0) ? "1" : "2"%>">
                    <td align="center"><%=info.getBatchId()%></td>
                    <td align="center"><a onclick="showSims('<%=info.getId()%>', '<%=info.getBatchId()%>')" href="javascript:void(0);"><%=info.getQuantity()%></a></td>
                    <td align="center"><%=info.getLoadDatetime()%></td>

                    <td align="center"><button id="buttonAssign" class="buttonAssign" onclick="assignBatch('<%=info.getId()%>');"><%=Languages.getString("jsp.admin.tools.smsInventory.assingInventory", SessionData.getLanguage())%></button></td>

                </tr>
                <%
                    }
                %>
            </table>

            <button id="buttonAssing" onclick="goAssingInventory('')"><%=Languages.getString("jsp.admin.tools.smsInventory.goBack", SessionData.getLanguage())%></button>
        </td>
    </tr>
</table>



<%@ include file="/includes/footer.jsp" %>