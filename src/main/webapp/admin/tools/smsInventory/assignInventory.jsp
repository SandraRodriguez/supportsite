<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.SimTypesDao"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 43;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%@page import="java.text.SimpleDateFormat"%>

<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}
</style>

<script type="text/javascript">

    
    $(document).ready(function () {
        $('#buttonSelectAll').click(function () {
            $('#searchType').val("ALL");
            $('#mainform').submit();
        });
    });
    
    function validateBatchId(){
        
        if($("#batchId").val().trim() !== ''){
            
            $("#individualSim").prop("disabled", true);
            $("#simProviderList").prop("disabled", true);
            $("#simTypeList").prop("disabled", true);
            $("#startDate").prop("disabled", true);
            $("#endDate").prop("disabled", true);
            $("#startDate").val("");
            $("#endDate").val("");
        }
        else{
            
            $("#individualSim").prop("disabled", false);
            $("#simProviderList").prop("disabled", false);
            $("#simTypeList").prop("disabled", false);
            $("#startDate").prop("disabled", false);
            $("#endDate").prop("disabled", false);
        }
    }
    
    function validateForm(){
        var start = $("#startDate").val().trim();
        var end = $("#endDate").val().trim();
        if((start !== '' && end === '')||(start === '' && end !== '')){
            alert('Please Select Start and End Date');
            return false;
        }
        return true;
    }
    
    
</script>

<%    SimpleDateFormat defaultDateFromatter = new SimpleDateFormat("MM/dd/yyyy");
    SessionData.setProperty("start_date", defaultDateFromatter.format(Calendar.getInstance().getTime()));
    SessionData.setProperty("end_date", defaultDateFromatter.format(Calendar.getInstance().getTime()));
    String editOption = request.getParameter("editOption");
    
    String urlProcessForm = "admin/tools/smsInventory/assignInventoryShowList.jsp";
    if(editOption != null && editOption.equalsIgnoreCase("Y")){
        urlProcessForm = "admin/tools/smsInventory/simInventoryEdit.jsp";
    }
%>


<table border="0" cellpadding="0" cellspacing="0" width="1250">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.assingInventory", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" class="formArea2">
            <form id="mainform" name="mainform" class="reportForm" name="mainform" method="post" action="<%=urlProcessForm%>" onsubmit="return validateForm();">
                <table width="70%" cellspacing="5" cellpadding="15" border="0" >
                    <tr>
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.batchId", SessionData.getLanguage())%></td>  
                        <td><input type="text" name="batchId" id="batchId" value="" size="25" maxlength="20"  onChange="return validateBatchId();"/></td>

                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.individualSim", SessionData.getLanguage())%></td>  
                        <td><input type="text" name="individualSim" id="individualSim" value="" size="25" maxlength="20"/></td>
                    </tr>
                    <tr>
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simProviderList" id="simProviderList" tabindex="11">
                                <%
                                    List<SimProviderPojo> simProviderList = SimProviderDao.getSimCarrierList();
                                    out.println("<option value=\"0\"></option>");
                                    for (SimProviderPojo currentSimProvider : simProviderList) {
                                        out.println("<option value=" + currentSimProvider.getId() + ">" + currentSimProvider.getName() + "</option>");
                                    }
                                %>
                            </select>
                        </td>
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simType", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simTypeList" id="simTypeList" tabindex="11"> 
                                <%

                                    String language = Languages.getLanguage();
                                    List<SimTypesPojo> simTypesList = SimTypesDao.getSimTypesList();
                                    out.println("<option value=\"0\"></option>");
                                    for (SimTypesPojo currentSimType : simTypesList) {
                                        out.println("<option value=" + currentSimType.getId() + ">" + ((language.equals("english")) ? currentSimType.getDescriptionEng() : currentSimType.getDescriptionEng()) + "</option>");
                                    }
                                %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.loadDate", SessionData.getLanguage())%></td>
                        <td><input class="plain" id="startDate" name="startDate"value=""size="12">
                            <a href="javascript:void(0)" onclick="if (self.gfPop)
                                                gfPop.fStartPop(document.mainform.startDate, document.mainform.endDate);
                                            return false;" HIDEFOCUS>
                                <img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""> </a>
                        </td>

                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.loadDateEnd", SessionData.getLanguage())%></td>
                        <td>
                            <input class="plain" id="endDate" name="endDate"
                                   value=""
                                   size="12">
                            <a href="javascript:void(0)"
                               onclick="if (self.gfPop)
                                           gfPop.fEndPop(document.mainform.startDate, document.mainform.endDate);
                                       return false;"
                               HIDEFOCUS><img name="popcal" align="absmiddle"
                                           src="admin/calendar/calbtn.png" width="34" height="22"
                                           border="0" alt=""> </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <input type="hidden" name="searchType" id="searchType" value="FILTERS" />
                            <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonSearch", SessionData.getLanguage())%>"/>
                            <button id="buttonSelectAll"><%=Languages.getString("jsp.admin.tools.smsInventory.buttonSelectAll", SessionData.getLanguage())%></button>
                        </td>
                    </tr>

                </table>
            </form>
        </td>
    </tr>
</table>

<iframe width=132 height=142 name="gToday:contrast:agenda.js"
        id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm"
        scrolling="no" frameborder="0"
        style="visibility: visible; z-index: 999; position: absolute; left: -500px; top: 0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>