<%-- 
    Document   : simInventoryMoveBatch
    Created on : Mar 9, 2017, 11:48:28 AM
    Author     : dgarzon
--%>

<%@page import="com.debisys.tools.simInventory.IsoPojo"%>
<%@page import="com.debisys.tools.simInventory.BatchesInfo"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         java.util.Iterator,
         java.util.Hashtable,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener" %>
<%
    int section = 9;
    int section_page = 44;
%>


<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script src="js/simInventory.js" type="text/javascript"></script>

<style type="text/css">
    .ventana_flotante {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }
</style>

<%
    String valuesIsos = "";
    List<IsoPojo> isoList = SimInventory.getIsosList();
    for (IsoPojo iso : isoList) {
        valuesIsos += "\"" + iso.getRep_id() + "-[" + iso.getBusinessname().trim() + "]\",";
    }
    if (valuesIsos.length() > 1) {
        valuesIsos = valuesIsos.substring(0, valuesIsos.length() - 1);
    }

%>

<script type="text/javascript">

    $(document).ready(function () {

        //Combo Isos Origin
        var availableTags = [<%=valuesIsos%>];
        $("#isoOriginSelected").autocomplete({
            source: availableTags,
            select: function (a, b) {
                var currentIsoOrigen = b.item.value;
                var arrayData = currentIsoOrigen.split("-[");
                updateBatchesList(arrayData[0]);
            }
        });

        var availableBatch = [''];
        $("#batchSelected").autocomplete({
            source: availableBatch
        });

        //Combo Isos Destination
        var availableTags2 = [<%=valuesIsos%>];
        $("#isoDestinationSelected").autocomplete({
            source: availableTags2
        });
    });

    function updateBatchesList(isoOriginSelected) {
        if (isoOriginSelected === undefined) {
            return;
        }
        document.getElementById('div_show_charge').style.display = "block";
        $('#batchSelected').val('');
        var valuesBatch = [];
        $.ajax({
            type: "POST",
            async: false,
            data: {isoOriginSelected: isoOriginSelected, action: 'getBatchesByIsoId'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                valuesBatch = String($.trim(data)).split("\n");
            }
        });
        $("#batchSelected").autocomplete({
            source: valuesBatch
        });
        document.getElementById('div_show_charge').style.display = "none";
        var batchesSize = valuesBatch.length;
        if (valuesBatch[0] === '') {
            batchesSize = 0;
        }

        $('#spanQuantityBatches').text('<%=Languages.getString("jsp.admin.tools.smsInventory.quantityBatchesInfo", SessionData.getLanguage())%> ' + batchesSize);
    }

    function validateFormMoveSimBatch() {

        $("#submit").prop("disabled", true);
        clearFields();
        var hasErrors = false;
        hasErrors = verifyFields();
        if (hasErrors) {
            $("#submit").prop("disabled", false);
            return false;
        }
        else {
            hasErrors = existIsoId("isoOriginSelected");
            hasErrors = hasErrors && existBatchId();
            hasErrors = hasErrors && existIsoId("isoDestinationSelected");
            return hasErrors;
        }

    }

    function existBatchId() {
        var validBatch = true;
        var isoSelected = $("#isoOriginSelected").val().trim();
        var batchSelected = $("#batchSelected").val().trim();
        $.ajax({
            type: "POST",
            async: false,
            data: {batchId: batchSelected, isoSelected: isoSelected, action: 'verifyBatchSelected'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data));
                if (array_data === 'false') {
                    validBatch = false;
                    $("#submit").prop("disabled", false);
                    $("#batchSelected").css("border", "1px solid red");
                    $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.smsInventory.invalidBatchId", SessionData.getLanguage())%> ").show().fadeOut(10000);
                    //document.getElementById('div_show_charge').style.display = "none";
                }
            }
        });
        return validBatch;
    }

    function existIsoId(isoIdItem) {
        var validIso = true;
        var isoSelected = '';

        if (isoIdItem === 'isoOriginSelected') {
            isoSelected = $("#isoOriginSelected").val().trim();
        }
        else {
            isoSelected = $("#isoDestinationSelected").val().trim();
        }
        $.ajax({
            type: "POST",
            async: false,
            data: {isoIdSelected: isoSelected, action: 'verifyIsoIdSelected'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                var array_data = String($.trim(data));
                if (array_data === 'false') {

                    $("#submit").prop("disabled", false);
                    $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.smsInventory.invalidIsoId", SessionData.getLanguage())%> ").show().fadeOut(10000);
                    if (isoIdItem === 'isoOriginSelected') {
                        $("#isoOriginSelected").css("border", "1px solid red");
                    }
                    else {
                        $("#isoDestinationSelected").css("border", "1px solid red");
                    }
                    validIso = false;
                }
            }
        });
        return validIso;
    }

    function clearFields() {
        $("#isoOriginSelected").css("border", "");
        $("#batchSelected").css("border", "");
        $("#isoDestinationSelected").css("border", "");
    }

    function verifyFields() {

        var hasErrors = false;

        var isoOriginSelected = $("#isoOriginSelected").val().trim();
        var batchSelected = $("#batchSelected").val().trim();
        var isoDestinationSelected = $("#isoDestinationSelected").val().trim();

        if (isoOriginSelected === '') {
            $("#isoOriginSelected").css("border", "1px solid red");
            hasErrors = true;
        }
        if (batchSelected === '') {
            $("#batchSelected").css("border", "1px solid red");
            hasErrors = true;
        }
        if (isoDestinationSelected === '') {
            $("#isoDestinationSelected").css("border", "1px solid red");
            hasErrors = true;
        }
        if (isoOriginSelected !== '' && isoDestinationSelected !== '' && isoOriginSelected === isoDestinationSelected) {
            $("#isoOriginSelected").css("border", "1px solid red");
            $("#isoDestinationSelected").css("border", "1px solid red");
            alert("<%=Languages.getString("jsp.admin.tools.smsInventory.selectDifferentIsos", SessionData.getLanguage())%> ");
        }
        return hasErrors;
    }




</script>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="ventana_flotante">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>


<table border="0" cellpadding="0" cellspacing="0" width="1250" >
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.simInventoryEdit", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>

    <tr>

        <td colspan="3">

            <fieldset>

                <form id="formMoveBatch" class="formMoveBatch" name="formMoveBatch" method="post" action="admin/tools/smsInventory/simInventoryMoveBatchProcess.jsp" onsubmit="return validateFormMoveSimBatch();">

                    <table width="100%" cellspacing="20" cellpadding="5" border="0" >
                        <tr>
                            <td>
                                <%=Languages.getString("jsp.admin.tools.smsInventory.selectIsoOrigen", SessionData.getLanguage())%>
                            </td>
                            <td >
                                <input type="text" value="" name="isoOriginSelected" id="isoOriginSelected" size="60" maxlength="60"  onclick="updateBatchesList();"/>
                            </td>
                            <td></td>

                        </tr>
                        <tr>
                            <td width="20%">
                                <%=Languages.getString("jsp.admin.tools.smsInventory.selectBatch", SessionData.getLanguage())%>
                            </td>
                            <td width="30%">
                                <input type="text" value="" name="batchSelected" id="batchSelected" size="60" maxlength="60"/>
                                <br>
                                <span id="spanBatchInfo"><%=Languages.getString("jsp.admin.tools.smsInventory.selectBatchInfo", SessionData.getLanguage())%></span>
                            </td>
                            <td width="50%">
                                <span id="spanQuantityBatches"><%=Languages.getString("jsp.admin.tools.smsInventory.quantityBatchesInfo", SessionData.getLanguage())%> 0</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%=Languages.getString("jsp.admin.tools.smsInventory.selectIsoDestination", SessionData.getLanguage())%>
                            </td>
                            <td>
                                <input type="text" value="" name="isoDestinationSelected" id="isoDestinationSelected" size="60" maxlength="60"/>
                            </td>
                            <td>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2" align="center">
                                <span id="spanWarningMessages" style="color:red"></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input type="submit" name="submit" id="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonMoveBatch", SessionData.getLanguage())%>" align="center" />
                            </td>
                        </tr>
                    </table>
                </form>
            </fieldset>
        </td>
    </tr>

</table>
</form>

<%@ include file="/includes/footer.jsp" %>