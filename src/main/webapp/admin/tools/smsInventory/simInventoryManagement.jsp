<%@page import="com.debisys.tools.simInventory.SimProductPojo"%>
<%@page import="com.debisys.tools.simInventory.BatchesInfo"%>
<%@page import="com.debisys.tools.simInventory.SimUploadLine"%>
<%@page import="com.debisys.tools.simInventory.SimInventoryRequestMultipart"%>
<%@page import="com.debisys.tools.simInventory.SimProviderDao"%>
<%@page import="com.debisys.tools.simInventory.SimTypesDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.debisys.tools.simInventory.SimTypesPojo"%>
<%@page import="com.debisys.tools.simInventory.SimProviderPojo"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<%@ page import="java.util.Vector,
         java.util.List,
         com.debisys.users.User,
         com.debisys.utils.*,
         com.debisys.utils.DebisysConfigListener,
         com.debisys.properties.RepProperty, java.text.DecimalFormat" %>
<%
    int section = 9;
    int section_page = 46;
    String path = request.getContextPath();
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Rep" class="com.debisys.customers.Rep" scope="request" />
<jsp:setProperty name="Rep" property="*" />
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script src="js/simInventory.js" type="text/javascript"></script>

<script type="text/javascript" src="<%=path%>/js/bootstrap.js"></script> 
<script type="text/javascript" src="<%=path%>/js/moment.min.js"></script>         
<script type="text/javascript" src="<%=path%>/js/daterangepicker.js"></script> 
<link href="<%=path%>/css/daterangepicker.css" rel="stylesheet" type="text/css"/>
<!--link href="<%=path%>/css/themes/bootstrap.css" rel="stylesheet" type="text/css"/--> 
<link href="<%=path%>/glyphicons/glyphicons.min.css" rel="stylesheet" type="text/css"/>
    
<style type="text/css">
    .reportForm table{border:1;width:100%;font-family:Arial, Helvetica, sans-serif;;color:#000000;font-size:13px;}
    .reportForm td{padding:40px; height:48;vertical-align:text-top;}

    input[type="text"]{height:20px; vertical-align:top;}
    .field_wrapper div{ margin-bottom:40px;}
    .add_button{ margin-top:10px; margin-left:10px;vertical-align: text-bottom;}
    .remove_button{ margin-top:10px; margin-left:10px;vertical-align: text-bottom;}



    #field_wrapper{
        width: 100%;
    }
    #left{
        float:left;
        width:750px;
        margin: 5px 10px;
        text-align:left;
        color:#fff;
        /* background-color: #bfb; */
    }
    .clear{
        clear:both;
    }

    #pSimNumber{
        color:green;
        font-size: 20px;
    }

    #divPleaseNote{
        font-size: 15px;
        background-color: yellow;
        size: 200px;
    }
    #legendIndividualRecords{
        font-size: 15px;
    }
    #spanWarningMessages{
        font-size: 20px;
        color: red;
    }

    .ventana_flotante {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }

    /* Popups of Sim Carrier select*/
    #popUpDivCarriers{
        width:200px;
        height:100px;
        position:absolute;
        top:50%;
        left:50%;
        margin:-50px 0 0 -100px; /* [-(height/2)px 0 0 -(width/2)px] */
        display:none;
        background-color: rgba(134, 174, 40, 0.8);
    }
    #simProviderList{
        z-index: 1000;
        position: absolute;
        top: 50%;
        left: 50px;
    }

    /* Popups of Sim Type select*/
    #popUpDivTypes{
        width:250px;
        height:100px;
        position:absolute;
        top:50%;
        left:50%;
        margin:-50px 0 0 -100px; /* [-(height/2)px 0 0 -(width/2)px] */
        display:none;
        background-color: rgba(134, 174, 40, 0.8);
    }
    #simTypeList{
        z-index: 1000;
        position: absolute;
        top: 50%;
        left: 15px;
    }
    
    /* Popups of Sim Product select*/
    #popUpDivProducts{
        width:350px;
        height:100px;
        position:absolute;
        top:50%;
        left:50%;
        margin:-50px 0 0 -100px; /* [-(height/2)px 0 0 -(width/2)px] */
        display:none;
        background-color: rgba(134, 174, 40, 0.8);
    }
    #simProductList{
        z-index: 1000;
        position: absolute;
        top: 50%;
        left: 15px;
    }


</style>


<%
    int xValue = 1;
    int countValue = 0;
    String divSims = "";
    

    List<SimProviderPojo> simProviderList = SimProviderDao.getSimCarrierList();
    List<SimTypesPojo> simTypesList = SimTypesDao.getSimTypesList();
    List<BatchesInfo> batchesList = SimInventory.getBatchesList(SessionData.getProperty("iso_id"), true);
    
    List<SimProductPojo> simCarrierProducts = new ArrayList();
    if(simProviderList != null && !simProviderList.isEmpty()){
        simCarrierProducts = SimProviderDao.getSimCarrierProductsList("'"+simProviderList.get(0).getId()+"'");
    }

    String valuesBatch = "";
    for (BatchesInfo batch : batchesList) {
        valuesBatch += "\"" + batch.getBatchId() + "-[" + batch.getLoadDatetime() + "]\",";
    }
    if (valuesBatch.length() > 1) {
        valuesBatch = valuesBatch.substring(0, valuesBatch.length() - 1);
    }

    int maxItems = DebisysConfigListener.getSIMInventoryLoadMaxSims(application);
    String textFileWarning = "";
    String simProvider = "";
    String simType = "";
    String simProduct = "";
    String contentType = request.getContentType();
    if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
        SimInventory simInventory = new SimInventory();
        SimInventoryRequestMultipart multipartResponse = simInventory.analizeInputStream(request, SessionData);

        if (multipartResponse.getListReaded() == null || multipartResponse.getListReaded().isEmpty()) {
            countValue = 1;
            textFileWarning = Languages.getString("jsp.admin.tools.smsInventory.warnFileIsEmpty", SessionData.getLanguage());
        }
        else if (multipartResponse.getListReaded().size() > maxItems) {
            countValue = 1;
            textFileWarning = String.format(Languages.getString("jsp.admin.tools.smsInventory.warnFileHasMuchRows", SessionData.getLanguage()), multipartResponse.getListReaded().size(), maxItems);
        }
        else {

            simProvider = multipartResponse.getSimCarrierId();
            simType = multipartResponse.getSimTypeId();
            simProduct = multipartResponse.getSimProduct();
            if (simProvider == null) {
                simProvider = "";
            }
            if (simType == null) {
                simType = "";
            }
            if (simProduct == null) {
                simProduct = "";
            }

            for (SimUploadLine row : multipartResponse.getListReaded()) {

                divSims += "<div id=\"left\"><input type=\"text\" name=\"sims\" id=\"sim_" + countValue + "\" value=\"" + row.getSimNumber() + "\" size=\"20\" maxlength=\"20\"/> ";

                divSims += "<input type=\"text\" name=\"simsCarrier\" class=\"edit_carrier_button\" value=\"" + row.getSimCarrierDescription() + "\" id=\"simsCarrier_" + countValue + "\" size=\"15\" maxlength=\"15\" readonly/> ";
                divSims += "<input type=\"hidden\" name=\"simsCarrierHidden\" id=\"simsCarrierHidden_" + countValue + "\" value=\"" + row.getSimCarrierId() + "\" /> ";

                divSims += "<input type=\"text\" name=\"simsType\" class=\"edit_type_button\" value=\"" + row.getSimTypeDescription() + "\" id=\"simsType_" + countValue + "\" size=\"20\" maxlength=\"20\" readonly/> ";
                divSims += "<input type=\"hidden\" name=\"simsTypeHidden\" id=\"simsTypeHidden_" + countValue + "\" value=\"" + row.getSimTypeId() + "\" /> ";
                
                divSims += "<input type=\"text\" name=\"simsProduct\" class=\"edit_product_action\" value=\"" + row.getSimProductDescription() + "\" id=\"simsProduct_" + countValue + "\" size=\"20\" maxlength=\"20\" readonly/> ";
                divSims += "<input type=\"hidden\" name=\"simsProductHidden\" id=\"simsProductHidden_" + countValue + "\" value=\"" + row.getSimProductId() + "\" /> ";
                divSims += "<input type=\"hidden\" name=\"carrierForProductHidden\" id=\"carrierForProductHidden_" + countValue + "\" value=\"" + row.getSimCurrentCarrierForProductId() + "\" /> ";

                if (countValue > 0) {
                    divSims += "<a href=\"javascript:void(0);\" class=\"remove_button\" title=\"Remove field\"><img src=\"images/details_close.png\"/></a> ";
                }
                divSims += " </div>";
                countValue++;
            }
        }
        xValue = countValue;
    }
    else {
        countValue = 1;
        xValue = 1;
    }
%>


<script type="text/javascript">
    var idRowSelected = '';
    var count = 0;
    var currentCarrierCache = '';
    $(document).ready(function () {
        initDateControl();
        fillBatches();

        $("#dialog-message").hide();
        $('#dialog-message-view-products').hide();

        // INIT VALUES
        if (<%=divSims.trim().isEmpty()%>) {
            $("#simsCarrier_0").val($("#simProviderGenericList option:selected").text());
            $("#simsCarrierHidden_0").val($("#simProviderGenericList").val());
            $("#simsType_0").val($("#simTypeList option:selected").text());
            $("#simsTypeHidden_0").val($("#simTypeList").val());
            
            $("#simsProduct_0").val($("#simProductList option:selected").text());
            $("#simsProductHidden_0").val($("#simProductList").val());
            $("#carrierForProductHidden_0").val($("#simProviderGenericList").val());
        }

        var maxField = <%=maxItems%>; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var x = <%=xValue%>; //Initial field counter is 1
        count = <%=countValue%>;
        $(addButton).click(function () { //Once add button is clicked
            var fieldHTML = '<div id="left"><input type="text" name="sims" id="sim_' + count + '" value="" size="25" maxlength="20"/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="images/details_close.png"/></a></div>'; //New input field html 
            if (x < maxField) { //Check maximum number of input fields
                $("#pSimNumber").text("<%=Languages.getString("jsp.admin.tools.smsInventory.simNumberCount", SessionData.getLanguage())%> " + x + " / " + maxField);
                x++; //Increment field counter
                count++;
                $("span").text("");
                $("#fieldCurrentCount").value = x;
                $(wrapper).append(fieldHTML); // Add field html
            }
            else {
                $("span").text("<%=Languages.getString("jsp.admin.tools.smsInventory.warnNoAddMoreSims", SessionData.getLanguage())%> ").show().fadeOut(10000);
            }
        });
        $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            $("span").text("");
            $("#fieldCurrentCount").value = x;
            $("#pSimNumber").text("<%=Languages.getString("jsp.admin.tools.smsInventory.simNumberCount", SessionData.getLanguage())%> " + x + " / " + maxField);
        });

        // Edit Carrier buttons action  ***********************************************
        $(wrapper).on('click', '.edit_carrier_button', function (e) {
            idRowSelected = $(this).attr('id');
            var arrayTemp = idRowSelected.split("_");
            var position = $(this).position();
            $("#popUpDivCarriers").css('left', position.left);
            $("#popUpDivCarriers").css('top', position.top);            
            $("#simProviderList").val($("#simsCarrierHidden_" + arrayTemp[1]).val());
            $(document).ready(function () {
                $("#popUpDivCarriers").show();
            });
        });

        $("#simProviderList").change(function (e) {
            var arrayTemp = idRowSelected.split("_");
            var index = arrayTemp[1];
            $('#simsCarrier_' + index).val($("#simProviderList option:selected").text());
            $('#simsCarrierHidden_' + index).val($("#simProviderList").val());
            $("#popUpDivCarriers").hide();
            
            
            $('#simsProduct_'+arrayTemp[1]).val('');
            $('#simsProductHidden_'+arrayTemp[1]).val('');
            $('#carrierForProductHidden_'+arrayTemp[1]).val($("#simProviderList").val());
            if(currentCarrierCache !== $("#simProviderList").val()){
                updateDynamicProductList($("#simProviderList").val());
            }
            
        });


        // EDIT SIM TYPES BUTTON ACTION ***********************************************
        $(wrapper).on('click', '.edit_type_button', function (e) {
            idRowSelected = $(this).attr('id');
            var arrayTemp = idRowSelected.split("_");
            var position = $(this).position();
            $("#popUpDivTypes").css('left', position.left);
            $("#popUpDivTypes").css('top', position.top);
            $("#simTypeList").val($("#simsTypeHidden_" + arrayTemp[1]).val());
            $(document).ready(function () {
                $("#popUpDivTypes").show();
            });
        });

        $("#simTypeList").change(function (e) {
            var arrayTemp = idRowSelected.split("_");
            var index = arrayTemp[1];
            $('#simsType_' + index).val($("#simTypeList option:selected").text());
            $('#simsTypeHidden_' + index).val($("#simTypeList").val());
            $("#popUpDivTypes").hide();
        });
        
        // EDIT SIM PRODUCTS BUTTON ACTION ***********************************************
        $(wrapper).on('click', '.edit_product_action', function (e) {
            idRowSelected = $(this).attr('id');
            var arrayTemp = idRowSelected.split("_");
            var position = $(this).position();
            $("#popUpDivProducts").css('left', position.left);
            $("#popUpDivProducts").css('top', position.top);
            
            if(currentCarrierCache !== $("#simsCarrierHidden_" + arrayTemp[1]).val()){
                updateDynamicProductList($("#simsCarrierHidden_" + arrayTemp[1]).val());
            }
            
            $("#simProductList").val($("#simsProductHidden_" + arrayTemp[1]).val());
            $(document).ready(function () {
                $("#popUpDivProducts").show();
            });
        });

        $("#simProductList").change(function (e) {
            var arrayTemp = idRowSelected.split("_");
            var index = arrayTemp[1];
            $('#simsProduct_' + index).val($("#simProductList option:selected").text());
            $('#simsProductHidden_' + index).val($("#simProductList").val());
            $("#popUpDivProducts").hide();
        });


        $(window).keydown(function (event) {
            if (event.keyCode === 13) {

                var carrierGenericLabel = $("#simProviderGenericList option:selected").text();
                var carrierGenericValue = $("#simProviderGenericList").val();

                var typeGenericLabel = $("#simTypeGenericList option:selected").text();
                var typeGenericValue = $("#simTypeGenericList").val();
                
                var productGenericLabel = $("#simProductGenericList option:selected").text();
                var productGenericValue = $("#simProductGenericList").val();
                if(productGenericValue === null || productGenericValue === '0'){
                    productGenericValue = '';
                }

                var fieldHTML = '<div id="left"><input type="text" name="sims" id=sim_' + count + ' value="" size="20" maxlength="20" /> ';

                fieldHTML += '<input type="text" name="simsCarrier" class="edit_carrier_button" value="' + carrierGenericLabel + '" id="simsCarrier_' + count + '" size="15" maxlength="15" readonly/> ';
                fieldHTML += '<input type="hidden" name="simsCarrierHidden" id="simsCarrierHidden_' + count + '" value="' + carrierGenericValue + '" /> ';

                fieldHTML += '<input type="text" name="simsType" class="edit_type_button" value="' + typeGenericLabel + '" id="simsType_' + count + '" size="20" maxlength="20" readonly/> ';
                fieldHTML += '<input type="hidden" name="simsTypeHidden" id="simsTypeHidden_' + count + '" value="' + typeGenericValue + '" /> ';
                
                fieldHTML += '<input type="text" name="simsProduct" class="edit_product_action" value="' + productGenericLabel + '" id="simsProduct_' + count + '" size="20" maxlength="20" readonly/> ';
                fieldHTML += '<input type="hidden" name="simsProductHidden" id="simsProductHidden_' + count + '" value="' + productGenericValue + '" /> ';
                fieldHTML += '<input type="hidden" name="carrierForProductHidden" id="carrierForProductHidden_' + count + '" value="' + carrierGenericValue + '" /> ';

                fieldHTML += '<a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="images/details_close.png"/></a></div>'; //New input field html
                if (x < maxField) { //Check maximum number of input fields
                    x++; //Increment field counter
                    count++;

                    $("#pSimNumber").text("<%=Languages.getString("jsp.admin.tools.smsInventory.simNumberCount", SessionData.getLanguage())%> " + x + " / " + maxField);
                    $(wrapper).append(fieldHTML); // Add field html
                    $('#sim_' + (count - 1)).focus();
                    $("span").text("");
                    $("#fieldCurrentCount").value = x;

                }
                else {
                    $("span").text("<%=Languages.getString("jsp.admin.tools.smsInventory.warnNoAddMoreSims", SessionData.getLanguage())%> ").show().fadeOut(10000);
                }
                event.preventDefault();
                return false;
            }
        });
    });

    function validateForm() {
        $("#submit").prop("disabled", true);
        document.getElementById('div_show_charge').style.display = "block";

        //VERIFY BATCHID
        var validBatch = validateBatchId();
        if (!validBatch) {
            return validBatch;
        }


        var i, j, elemA = 0, totalDuplicates = 0;

        //VERIFY SIM LIST
        var simsArray = document.getElementsByName("sims");
        for (i = 0; i < simsArray.length; i++) {
            for (j = 0; j < simsArray.length; j++) {
                if (simsArray[i].value === simsArray[j].value) {
                    elemA++;
                }
            }
            simsArray[i].style.backgroundColor = "white";
            if (elemA > 1) {
                totalDuplicates++;
                simsArray[i].style.backgroundColor = "red";
            }
            elemA = 0;
        }
        
        //VERIFY SIM CARRIER LIST AND  SIM TYPE LIST
        var carrierListCountErrors = 0;
        var typeListCountErrors = 0;
        var carriersHiddenArray = document.getElementsByName("simsCarrierHidden");
        var carriersArray = document.getElementsByName("simsCarrier");
        var typesHiddenArray = document.getElementsByName("simsTypeHidden");
        var typesArray = document.getElementsByName("simsType");

        for (i = 0; i < carriersHiddenArray.length; i++) {
            carriersArray[i].style.backgroundColor = "white";
            typesArray[i].style.backgroundColor = "white";
            if (carriersHiddenArray[i].value === 'null') {
                carrierListCountErrors++;
                carriersArray[i].style.backgroundColor = "red";
            }
            if (typesHiddenArray[i].value === 'null') {
                typeListCountErrors++;
                typesArray[i].style.backgroundColor = "red";
            }
        }


        var wantToSave = true;
        if (totalDuplicates > 0) {
            if (confirm("<%=Languages.getString("jsp.admin.tools.smsInventory.duplicateConfirmDialog", SessionData.getLanguage())%>")) {
                wantToSave = true;
            } else {
                $("#submit").prop("disabled", false);
                document.getElementById('div_show_charge').style.display = "none";
                wantToSave = false;
            }
        }

        if (carrierListCountErrors > 0 || typeListCountErrors > 0) {
            $("#submit").prop("disabled", false);
            document.getElementById('div_show_charge').style.display = "none";
            wantToSave = false;
        }
        
        // Verifing SIM products
        if(wantToSave){
            var hasProductErrors = false;
            
            var simsProductArray = document.getElementsByName("simsProduct");
            var carrierForProductHiddenArray = document.getElementsByName("carrierForProductHidden");
            for (i = 0; i < carrierForProductHiddenArray.length; i++) {

                simsProductArray[i].style.backgroundColor = "white";

                if(carrierForProductHiddenArray[i].value === 'ERROR_NOT_FOUND'){
                    simsProductArray[i].style.backgroundColor = "orange";
                    hasProductErrors = true;
                }
                else if(simsProductArray[i].value !=='' && carrierForProductHiddenArray[i].value !== $('#simsCarrierHidden_'+i).val()){
                    simsProductArray[i].style.backgroundColor = "orange";
                    hasProductErrors = true;
                }
            }

            if (hasProductErrors) {
                if (confirm("<%=Languages.getString("jsp.admin.tools.smsInventory.msgInvalidSIMProducts", SessionData.getLanguage())%>")) {
                    wantToSave = true;
                } else {
                    $("#submit").prop("disabled", false);
                    document.getElementById('div_show_charge').style.display = "none";
                    wantToSave = false;
                }
            }
        }


        if (wantToSave) { // verify in DB
            var strValues = [];
            for (i = 0; i < simsArray.length; i++) {
                strValues[i] = simsArray[i].value;
            }
            if (strValues != null && strValues.length > 0 && strValues != '') {
                var passedValidation = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    data: {sims: strValues, action: 'verifySims'},
                    url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
                    success: function (data) {
                        var array_data = String($.trim(data)).split("\n");
                        if (array_data[0] !== 'NOTHING') {
                            var totalDuplicatesDB = 0;
                            for (i = 0; i < array_data.length; i++) {
                                for (j = 0; j < simsArray.length; j++) {
                                    if ($.trim(array_data[i]) === $.trim(simsArray[j].value) || $.trim(simsArray[j].value) === '') {
                                        simsArray[j].style.backgroundColor = "red";
                                        totalDuplicatesDB++;
                                    }
                                }
                            }
                            if (totalDuplicatesDB === simsArray.length) {
                                $("span").text("<%=Languages.getString("jsp.admin.tools.smsInventory.cannotInsertSims", SessionData.getLanguage())%> ").show().fadeOut(10000);
                                $("#submit").prop("disabled", false);
                                document.getElementById('div_show_charge').style.display = "none";
                                passedValidation = false;
                            }
                            else if (totalDuplicatesDB > 0) {
                                $("span").text("<%=Languages.getString("jsp.admin.tools.smsInventory.duplicateSmsInDB", SessionData.getLanguage())%> ").show().fadeOut(10000);
                                if (confirm("<%=Languages.getString("jsp.admin.tools.smsInventory.duplicateSmsInDB", SessionData.getLanguage())%>")) {
                                    passedValidation = true;
                                } else {
                                    $("#submit").prop("disabled", false);
                                    document.getElementById('div_show_charge').style.display = "none";
                                    passedValidation = false;
                                }
                            }
                        }
                        else {
                            passedValidation = true;
                        }

                    }
                });
                $("#submit").prop("disabled", false);
                document.getElementById('div_show_charge').style.display = "none";
                return passedValidation;
            }
            $("#submit").prop("disabled", false);
            document.getElementById('div_show_charge').style.display = "none";
            return false;
        }
        else {
            $("#submit").prop("disabled", false);
            document.getElementById('div_show_charge').style.display = "none";
            return wantToSave;
        }

    }

    function validateBatchId() {

        var validBatch = true;
        $("#batchSelected").css("border", "");
        var radioOp = $('input[name="batchSaveOption"]:checked', '#formSimInventory').val();
        if (radioOp === 'oldBatch') {
            // VERIFY IF CURRENT batchSelected IS VALID
            var batchSelected = $("#batchSelected").val().trim();
            $.ajax({
                type: "POST",
                async: false,
                data: {batchId: batchSelected, action: 'verifyBatchSelected'},
                url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
                success: function (data) {
                    var array_data = String($.trim(data));
                    if (array_data === 'false') {
                        validBatch = false;
                        $("#submit").prop("disabled", false);
                        $("#batchSelected").css("border", "1px solid red");
                        $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.tools.smsInventory.invalidBatchId", SessionData.getLanguage())%> ").show().fadeOut(10000);
                        document.getElementById('div_show_charge').style.display = "none";
                    }
                }
            });
        }
        return validBatch;
    }

    function validateFile() {

        var simProvider = document.getElementById("simProviderList").value;
        var simType = document.getElementById("simTypeList").value;
        var simProduct = document.getElementById("simProductList").value;
        $("#simProvider").val(simProvider);
        $("#simType").val(simType);
        $("#simProduct").val(simProduct);

        var nameFile = document.getElementById("txt_filename");
        var nameFilesplit = nameFile.value.split("\\");
        var nameF = nameFilesplit[nameFilesplit.length - 1];
        nameF = nameF.toLowerCase();
        if (nameF.indexOf(".csv") > 0) {
            return true;
        } else {
            $("span").text("<%=Languages.getString("jsp.admin.tools.general_validate_file", SessionData.getLanguage())%> ").show().fadeOut(10000);
            return false;
        }
    }

    function fillBatches() {

        $("#radioNew").prop("checked", true);
        $("#batchSelected").prop("disabled", true);

        var availableTags = [<%=valuesBatch%>];
        $("#batchSelected").autocomplete({
            source: availableTags
        });
    }
    
    function viewDataOptions() {
        var text = "";
        var text2 = "";
        var countRows = 1;
        text += "<tr class=\"rowhead2\"> <td class=\"titles\" align=\"center\"><b>SIM Carriers available</b></td></tr>";

    <%
        for (SimProviderPojo currentSimProvider : simProviderList) {%>
            if(countRows % 2 === 0){
                text += "<tr class=\"row1\">";
            }
            else{
                text += "<tr class=\"row2\">";
            }
            text += "<td><%=currentSimProvider.getName().trim()%></td>";
            text += "</tr>";
            countRows++;
        <%}%>

        text = "<table border=\"0\" cellspacing=\"3\" cellpadding=\"3\"> " + text + "</table>";

        text2 += "<tr class=\"rowhead2\"><td class=\"titles\" align=\"center\"><b>SIM Carriers available</b></td></tr>";

    <%
        for (SimTypesPojo currentSimType : simTypesList) {%>
            if(countRows % 2 === 0){
                text2 += "<tr class=\"row1\">";
            }
            else{
                text2 += "<tr class=\"row2\">";
            }
            text2 += "<td><%=currentSimType.getDescriptionEng().trim()%></td>";
            text2 += "</tr>";
            countRows++;
        <%}%>

        text2 = "<table border=\"0\" cellspacing=\"3\" cellpadding=\"3\"> " + text2 + "</table>";
        $("#pDialogMessage").html(text + '<br>' + text2);

        var opt = {
            autoOpen: false,
            modal: true,
            width: 400,
            height: 550,
            title: 'Data Options',
            html: text
        };

        $(document).ready(function () {
            $("#dialog-message").dialog(opt).dialog("open");
        });
    }
    
    
    
    
    function viewProductsOptions() {
        var text = "";
        text += "<tr class=\"rowhead2\"> <td class=\"titles\" align=\"center\"><b>SIM Carriers available</b></td></tr>";
        var array_data = getAjaxDynamicProductList($('#simCarrierForProducts').val());
        var countRows = 1;
        for (var i = 0; i < array_data.length; i++) {
            if (array_data[i] !== '') {
                var productData = String($.trim(array_data[i])).split("|");
                $('#simProductList').append('<option value="' + productData[0] + '">' + productData[1] + '</option>');
                if(countRows % 2 === 0){
                    text += "<tr class=\"row1\">";
                }
                else{
                    text += "<tr class=\"row2\">";
                }
                text += "<td>"+productData[1]+"</td>";
                text += "</tr>";
                countRows++;
            }
        }
        
        text = "<table border=\"0\" cellspacing=\"3\" cellpadding=\"3\"> " + text + "</table>";

        var opt = {
            autoOpen: false,
            modal: true,
            width: 600,
            height: 550,
            title: 'Sim Carrier - products',
            html: text
        };

        $(document).ready(function () {
            $("#dialog-message-view-products").dialog(opt).dialog("open");
        });
    }
    
    function updateCarrierProducts(){
        var carrierIdnew = $("#simProviderGenericList").val().trim();
        $('#simProductGenericList').html('');
        
        var array_data = getAjaxDynamicProductList(carrierIdnew);
        $('#simProductGenericList').append('<option value=""></option>');
        for (var i = 0; i < array_data.length; i++) {
            if (array_data[i] !== '') {
                var productData = String($.trim(array_data[i])).split("|");
                $('#simProductGenericList').append('<option value="' + productData[0] + '">' + productData[1] + '</option>');
            }
        }
    }
    
    function updateCarrierProductsForView(){
        var carrierIdnew = $("#simCarrierForProducts").val().trim();
        $('#pDialogTableProducts').html('');
        var text = '';
        var countRows = 0;
        var array_data = getAjaxDynamicProductList(carrierIdnew);
        text += "<tr class=\"rowhead2\"> <td class=\"titles\" align=\"center\"><b>SIM Products available</b></td></tr>";
        for (var i = 0; i < array_data.length; i++) {
            if (array_data[i] !== '') {
                var productData = String($.trim(array_data[i])).split("|");
                if(countRows % 2 === 0){
                    text += "<tr class=\"row1\">";
                }
                else{
                    text += "<tr class=\"row2\">";
                }
                text += "<td>"+productData[1]+"</td>";
                text += "</tr>";
                countRows++;
            }
        }
        text = "<table border=\"0\" cellspacing=\"3\" cellpadding=\"3\"> " + text + "</table>";
        $("#pDialogTableProducts").html( '<br>' + text);
    }
        
    
    
    function updateDynamicProductList(currentCarrier){
        currentCarrierCache = currentCarrier;
        $('#simProductList').html('');
        var array_data = getAjaxDynamicProductList(currentCarrier);
        $('#simProductList').append('<option value=""></option>');
        for (var i = 0; i < array_data.length; i++) {
            if (array_data[i] !== '') {
                var productData = String($.trim(array_data[i])).split("|");
                $('#simProductList').append('<option value="' + productData[0] + '">' + productData[1] + '</option>');
            }
        }
    }
    
    function getAjaxDynamicProductList(currentCarrier){
        var arrayData = [];
        $.ajax({
            type: "POST",
            async: false,
            data: {carrierIdnew: currentCarrier, action: 'getProductsByCarrierIdNew'},
            url: "admin/tools/smsInventory/simInventoryVerifyDB.jsp",
            success: function (data) {
                arrayData = String($.trim(data)).split("\n");
            }
        });
        return arrayData;
    }

    var momentStart = moment();
    var momentEnd= moment().add(96, 'days');
    
    function initDateControl(){
        var days = momentEnd.diff(momentStart, 'days');
        $("#pDiffDays").text(days);
        $('input[name="startDate"]').daterangepicker({
            "minDate": momentStart,
            "showDropdowns": true,
            "showWeekNumbers": false,
            "singleDatePicker": true,
            "autoApply": true,
            "timePicker": false,
            "timePicker24Hour": false,
            "startDate": momentStart,
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                "daysOfWeek": [
                    '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                ],
                "monthNames": [
                    '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                ],
                "firstDay": 1
            }
        }, function (start, end, label) {
            momentStart=start;
            var days = momentEnd.diff(momentStart, 'days');
            $("#pDiffDays").text(days);
        }
        );
        $('input[name="endDate"]').daterangepicker({
            "minDate": momentStart,
            "showDropdowns": true,
            "showWeekNumbers": false,
            "singleDatePicker": true,
            "autoApply": true,
            "timePicker": false,
            "timePicker24Hour": false,
            "startDate": momentEnd, 
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": '<%=Languages.getString("reports.apply", SessionData.getLanguage())%>',
                "cancelLabel": '<%=Languages.getString("reports.cancel", SessionData.getLanguage())%>',
                "fromLabel": '<%=Languages.getString("reports.from", SessionData.getLanguage())%>',
                "toLabel": '<%=Languages.getString("reports.to", SessionData.getLanguage())%>',
                "customRangeLabel": '<%=Languages.getString("reports.custom", SessionData.getLanguage())%>',
                "daysOfWeek": [
                    '<%=Languages.getString("reports.day.Su", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Mo", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Tu", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.We", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Th", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Fr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.day.Sa", SessionData.getLanguage())%>'
                ],
                "monthNames": [
                    '<%=Languages.getString("reports.monthName.Jan", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Feb", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Mar", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Apr", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.May", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jun", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Jul", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Aug", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Sep", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Oct", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Nov", SessionData.getLanguage())%>',
                    '<%=Languages.getString("reports.monthName.Dec", SessionData.getLanguage())%>'
                ],
                "firstDay": 1
            }            
            }, function (start, end, label) {
                momentEnd=start;
                var days = momentEnd.diff(momentStart, 'days');
                $("#pDiffDays").text(days);
            });  
    }
    
</script>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="ventana_flotante">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>

<table border="0" cellpadding="0" cellspacing="0" width="1800">
    <tr>
        <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.smsInventory.simInventoryManagement", SessionData.getLanguage()).toUpperCase()%></td>
        <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colspan="3" class="formArea2">

            <div id="divPleaseNote"><%=Languages.getString("jsp.admin.tools.smsInventory.pleaseNote", SessionData.getLanguage())%></div>



            <form ACTION="" ENCTYPE="multipart/form-data" METHOD=POST >
                <table>
                    <tr>
                        <td>
                            <%=Languages.getString("jsp.admin.tools.smsInventory.uploadFromFile", SessionData.getLanguage())%>
                            <input type="file" name="txt_filename" id="txt_filename" size="100">
                            <input type="hidden" name="simProvider" id="simProvider" value="" />
                            <input type="hidden" name="simType" id="simType" value="" />
                            <input type="hidden" name="simProduct" id="simProduct" value="" />
                            <input type="submit" value="<%=Languages.getString("jsp.admin.tools.loadbutton", SessionData.getLanguage())%>" onclick="return validateFile();">
                        </td>
                        <td>
                            <fieldset>
                                <legend id="legendIndividualRecords">Options</legend>
                                <a onclick="viewDataFormat()" href="javascript:void(0);"><%=Languages.getString("jsp.admin.tools.smsInventory.ViewFileFormat", SessionData.getLanguage())%></a><br>
                                <a onclick="viewDataOptions()" href="javascript:void(0);"><%=Languages.getString("jsp.admin.tools.smsInventory.ViewDataAvailable", SessionData.getLanguage())%></a><br>
                                <a onclick="viewProductsOptions()" href="javascript:void(0);"><%=Languages.getString("jsp.admin.tools.smsInventory.viewCarrierProducts", SessionData.getLanguage())%></a><br>
                                <a onclick="downloadCSV(this)" href="javascript:void(0);"><%=Languages.getString("jsp.admin.tools.smsInventory.DownloadTemplateCSVFile", SessionData.getLanguage())%></a><br>  
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <br>
            </form>

            <form id="formSimInventory" name="formSimInventory" class="reportForm" name="mainform" method="post" action="admin/tools/smsInventory/simInventoryManagementConfirm.jsp" onsubmit="return validateForm();">

                <table cellspacing="15" cellpadding="5" border="0" >
                    <tr>  
                        <td colspan='3'><%=Languages.getString("jsp.admin.tools.smsInventory.webActivationMessage1", SessionData.getLanguage())%></td>
                    </tr>
                    <tr>
                        <td colspan='3'><%=Languages.getString("jsp.admin.tools.smsInventory.webActivationMessage2", SessionData.getLanguage())%></td>
                    </tr>
                    <tr>
                        <td>Start Date<br/><input type="text" class="form-control" id="startDate" name="startDate" /></td>
                        <td>End Date<br/><input type="text" class="form-control" id="endDate" name="endDate" /></td> 
                    </tr>
                    <tr>
                        <td><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simProviderGenericList" id="simProviderGenericList" tabindex="11" onchange="updateCarrierProducts();">
                                <%
                                    for (SimProviderPojo currentSimProvider : simProviderList) {
                                        out.println("<option value=" + currentSimProvider.getId() + " " + (simProvider.equalsIgnoreCase(currentSimProvider.getId()) ? " SELECTED " : "") + ">" + currentSimProvider.getName() + "</option>");
                                    }
                                %>
                            </select>
                        </td>
                        <td><input type="checkbox" id="checkCarrierAll" onchange="checkAllCarriers(this, '<%=Languages.getString("jsp.admin.tools.smsInventory.confirmThisAction", SessionData.getLanguage())%>')"><%=Languages.getString("jsp.admin.tools.smsInventory.applyToAll", SessionData.getLanguage())%></td>
                        
                    </tr>
                    <tr>
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simType", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simTypeGenericList" id="simTypeGenericList" tabindex="11"> 
                                <%
                                    String language = Languages.getLanguage();
                                    for (SimTypesPojo currentSimType : simTypesList) {
                                        out.println("<option value=" + currentSimType.getId() + " " + (simType.equalsIgnoreCase(currentSimType.getId()) ? " SELECTED " : "") + ">" + ((language.equals("english")) ? currentSimType.getDescriptionEng() : currentSimType.getDescriptionEng()) + "</option>");
                                    }
                                %>
                            </select>
                        </td>
                        <td>
                            <input type="checkbox" id="checkTypeAll" onchange="checkAllTypes(this, '<%=Languages.getString("jsp.admin.tools.smsInventory.confirmThisAction", SessionData.getLanguage())%>')"><%=Languages.getString("jsp.admin.tools.smsInventory.applyToAll", SessionData.getLanguage())%>
                        </td>                        
                    </tr>
                    
                    <tr>
                        <td class="main"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>
                        <td>
                            <select name="simProductGenericList" id="simProductGenericList" tabindex="11"> 
                                <%
                                    out.println("<option value=''></option>");
                                    for (SimProductPojo currentSimProduct : simCarrierProducts) {
                                        out.println("<option value='" + currentSimProduct.getProductId() + "' " + (simProduct.equalsIgnoreCase(""+currentSimProduct.getProductId()) ? " SELECTED " : "") + ">" + currentSimProduct.getDescription()+ "</option>");
                                    }
                                %>
                            </select>
                        </td>
                        <td>
                            <input type="checkbox" id="checkProductAll" onchange="checkAllProducts(this, '<%=Languages.getString("jsp.admin.tools.smsInventory.msgUpdateAllSimProducts", SessionData.getLanguage())%>')"><%=Languages.getString("jsp.admin.tools.smsInventory.applyToAll", SessionData.getLanguage())%>
                        </td>
                    </tr>

                    <tr>
                        <td><input type="radio" name="batchSaveOption" id="radioNew" value="newBatch" onchange="onchangeRadioNew();"><%=Languages.getString("jsp.admin.tools.smsInventory.createNewBatch", SessionData.getLanguage())%></td>
                        <td><input type="radio" name="batchSaveOption" id="radioOld" value="oldBatch" onchange="onchangeRadioOld();" ><%=Languages.getString("jsp.admin.tools.smsInventory.selectOldBatch", SessionData.getLanguage())%></td>
                        <td><input type="text" class="select_batch_button" value="" name="batchSelected" id="batchSelected" size="40" maxlength="40"/></td>
                    </tr>
                                    
                </table>

                <fieldset>
                    <legend id="legendIndividualRecords"><%=Languages.getString("jsp.admin.tools.smsInventory.infoIndividualRecords", SessionData.getLanguage())%></legend>
                    <p id="pSimNumber">
                        <font color="green">
                        <%=Languages.getString("jsp.admin.tools.smsInventory.simNumberCount", SessionData.getLanguage()) + " " + xValue + " / " + maxItems%>
                        </font>
                    </p>

                    <span id="spanWarningMessages"><%=textFileWarning%></span>

                    <!--<div class="buttonAdd"> 
                        <a href="javascript:void(0);" class="add_button" title="Add field"><img src="images/details_open.png"/></a>
                    </div>-->

                    <div id= "field_wrapper" class="field_wrapper">

                        <div id="left">
                            <table cellspacing="5" cellpadding="5" border="0" >
                                <tr>
                                    <td class="titles" width="20%">Sim</td>
                                    <td class="titles" width="20%"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                                    <td class="titles" width="20%"><%=Languages.getString("jsp.admin.tools.smsInventory.simType", SessionData.getLanguage())%></td>
                                    <td class="titles" width="30%"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>
                                </tr>
                            </table>
                        </div>
                        <div id="left">
                            <table width="10%" cellspacing="10" cellpadding="15" border="0" >
                                <tr>
                                    <td class="titles" width="20%">Sim</td>
                                    <td class="titles" width="20%"><%=Languages.getString("jsp.admin.tools.smsInventory.simCarrier", SessionData.getLanguage())%></td>
                                    <td class="titles" width="20%"><%=Languages.getString("jsp.admin.tools.smsInventory.simType", SessionData.getLanguage())%></td>
                                    <td class="titles" width="30%"><%=Languages.getString("jsp.admin.tools.smsInventory.simProduct", SessionData.getLanguage())%></td>
                                </tr>
                            </table>
                        </div>


                        <%if (divSims.trim().isEmpty()) {%>
                        <div id="left">
                            <input type="text" name="sims" value="" id="sim_0" size="20" maxlength="20"/>

                            <input type="text" name="simsCarrier" class="edit_carrier_button" value="" id="simsCarrier_0" size="15" maxlength="15" readonly/>
                            <input type="hidden" name="simsCarrierHidden" id="simsCarrierHidden_0" value="" />

                            <input type="text" name="simsType" class="edit_type_button" value="" id="simsType_0" size="20" maxlength="20" readonly/>
                            <input type="hidden" name="simsTypeHidden" id="simsTypeHidden_0" value="" />
                            
                            <input type="text" name="simsProduct" class="edit_product_action" value="" id="simsProduct_0" size="20" maxlength="20" readonly/>
                            <input type="hidden" name="simsProductHidden" id="simsProductHidden_0" value="" />
                            <input type="hidden" name="carrierForProductHidden" id="carrierForProductHidden_0" value="" />

                        </div>
                        <%}
                        else {%>
                        <%=divSims%>
                        <%}%>
                    </div>
                    <div class="clear"></div>

                </fieldset>
                                    
                <table width="80%" cellspacing="10" cellpadding="15" border="0" >
                    <tr>
                        <td align="center">
                            <input type="submit" name="submit" id="submit" value="<%=Languages.getString("jsp.admin.tools.smsInventory.buttonVerifyAndProcess", SessionData.getLanguage())%>" align="center" />
                            <button id="buttonExit" type="button" onclick="goToTools()"><%=Languages.getString("jsp.admin.tools.smsInventory.cancel", SessionData.getLanguage())%></button>
                        </td>
                    </tr>
                </table>

                <!-- DIV CARRIERS -->
                <div id="popUpDivCarriers">
                    <a href="javascript:closePopupCarriers();" class="close_popup_carriers" style="float:right"><img src="images/icn-redx1.gif"/></a>
                    <p style="font-weight: bold;font-size:120%;">SELECT SIM CARRIER</p>
                    <select name="simProviderList" id="simProviderList" tabindex="11">
                        <%
                            for (SimProviderPojo currentSimProvider : simProviderList) {
                                out.println("<option value=" + currentSimProvider.getId() + " " + (simProvider.equalsIgnoreCase(currentSimProvider.getId()) ? " SELECTED " : "") + ">" + currentSimProvider.getName() + "</option>");
                            }
                        %>
                    </select>
                </div>

                <!-- DIV SIM TYPES -->
                <div id="popUpDivTypes">
                    <a href="javascript:closePopupTypes();" class="close_popup_types" title="Remove field" style="float:right"><img src="images/icn-redx1.gif"/></a>
                    <p style="font-weight: bold;font-size:120%;">SELECT SIM TYPE</p>
                    <select name="simTypeList" id="simTypeList" tabindex="11"> 
                        <%
                            for (SimTypesPojo currentSimType : simTypesList) {
                                out.println("<option value=" + currentSimType.getId() + " " + (simType.equalsIgnoreCase(currentSimType.getId()) ? " SELECTED " : "") + ">" + ((language.equals("english")) ? currentSimType.getDescriptionEng() : currentSimType.getDescriptionEng()) + "</option>");
                            }
                        %>
                    </select>
                </div>
                    
                <!-- DIV SIM PRODUCTS -->
                <div id="popUpDivProducts">
                    <a href="javascript:closePopupProducts();" class="close_popup_types" title="Close" style="float:right"><img src="images/icn-redx1.gif"/></a>
                    <p style="font-weight: bold;font-size:120%;">SELECT SIM PRODUCT</p>
                    <select name="simProductList" id="simProductList" tabindex="11"> 
                        <%
                            out.println("<option value=''></option>");
                            for (SimProductPojo currentSimProduct : simCarrierProducts) {
                                out.println("<option value='" + currentSimProduct.getProductId() + "' " + (simProduct.equalsIgnoreCase(""+currentSimProduct.getProductId()) ? " SELECTED " : "") + ">" + currentSimProduct.getDescription()+ "</option>");
                            }
                        %>
                    </select>
                </div>

                <div id="dialog-message" title="Data Format CSV File" >
                    <p>
                        <span class="spanTextBatchId" style="float:left; margin:0 7px 50px 0;"></span>
                    </p>
                    <p id="pDialogMessage"></p>
                </div>
                    
                <!-- DIV Carriers products -->
                <div id="dialog-message-view-products" title="Site Id" >
                    <p>
                        <select name="simCarrierForProducts" id="simCarrierForProducts" tabindex="11" onchange="updateCarrierProductsForView();">
                        <%
                            for (SimProviderPojo currentSimProvider : simProviderList) {
                                out.println("<option value=" + currentSimProvider.getId() + ">" + currentSimProvider.getName() + "</option>");
                            }
                        %>
                        </select>
                    </p>
                    
                    <p id="pDialogTableProducts"></p>
                </div>

            </form>
        </td>
    </tr>
</table>

<%@ include file="/includes/footer.jsp" %>