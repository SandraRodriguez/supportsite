
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    int section=4;
    int section_page=63;
%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<jsp:useBean id="simReport" class="com.debisys.reports.SimReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%
    //Some code to execute

    if (request.getParameter("submitted") != null && request.getParameter("submitted").equalsIgnoreCase("y")){

        String downLoadUrl = simReport.generateCsvInventoryReport(application, SessionData);

        response.sendRedirect(downLoadUrl);
        return;

    }


    String reportLegend = Languages.getString("jsp.admin.sim.inventory.report.legend",SessionData.getLanguage());
    String btnDownload = Languages.getString("jsp.admin.sim.inventory.report.button.download",SessionData.getLanguage());

%>


<%@ include file="/includes/header.jsp" %>


<table id="tb1" border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">

    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="formAreaTitle" align="center" height="20" style="border-left: 1px solid #62891E; border-right: 1px solid #62891E;">
                        <%=Languages.getString("jsp.admin.sim.inventory.report.title",SessionData.getLanguage()).toUpperCase()%>
                    </td>
                </tr>
            </table>
        </td>

    </tr>

    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" style="height: 25px;"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF"><%=reportLegend%></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" style="height: 25px;"></td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" align="center">
            <form name="inventoryReportForm" id="inventoryReportForm" method="post" action="/support/admin/tools/smsInventory/inventoryReporting.jsp" onsubmit="document.getElementById('btnSubmit').disabled = true;">
                <input type="hidden" name="submitted" value="y">
                <input id="btnSubmit" type="submit" class="main" value="<%=btnDownload%>"/>
            </form>
        </td>
    </tr>
    <tr>
        <td colspan="3"  bgcolor="#FFFFFF" style="height: 25px"></td>
    </tr>
</table>


<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>