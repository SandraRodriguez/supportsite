    <%-- 
    Document   : SimEditProductDB
    Created on : Nov 23, 2018, 3:07:06 PM
    Author     : nmartinez
--%>
<%@page import="com.debisys.utils.QueryReportData"%>
<%@page import="com.debisys.tools.simInventory.SimPojo"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.debisys.utils.JSONObject"%>
<%@page import="com.debisys.utils.JSONArray"%>
<%@page import="com.debisys.tools.simInventory.SimEditProduct"%>
<%@page import="com.debisys.tools.simInventory.SimProductPojo"%>
<%@page import="java.util.List"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
    String action = request.getParameter("action");
    QueryReportData reporInfo = new QueryReportData();
    if (request.getParameter("batchId")!=null){
        reporInfo.setBatchId(request.getParameter("batchId"));
    } else if (request.getParameter("individualSim")!=null){
        reporInfo.setIndividualSim(request.getParameter("individualSim"));
    } else if (request.getParameter("carriers")!=null){
        reporInfo.setCarriers(request.getParameter("carriers"));
    } else if (request.getParameter("allCarriers")!=null){
        reporInfo.setCarriers("ALL");
    } else if (request.getParameter("skus")!=null){
        reporInfo.setProducts(request.getParameter("skus"));
    }
    if (request.getParameter("searchSims")!=null){//
        reporInfo.setSimInfo(request.getParameter("searchSims"));
    }
    if (request.getParameter("expired")!=null && request.getParameter("expired").equals("true")){
        reporInfo.setExpired(Boolean.TRUE);
        if (request.getParameter("startExpiredMoment")!=null && request.getParameter("endExpiredMoment")!=null)
        reporInfo.setStartExpiredMoment(request.getParameter("startExpiredMoment"));
        reporInfo.setEndExpiredMoment(request.getParameter("endExpiredMoment"));
    } else{
        reporInfo.setExpired(Boolean.FALSE);
    }
    if (action.equals("count") || action.equals("sims")) {        
        
        String start = null;
        String end = null;
        String sortField = null;
        String sortOrder = null;
        if (action.equals("sims")){
            start = request.getParameter("start");
            end = request.getParameter("rows");
            sortField = request.getParameter("sortField");
            sortOrder = request.getParameter("sortOrder");
            reporInfo.setInfoReportData(sortField, sortOrder, start, end);   
        }        
        if (action.equals("sims")){
            List<SimPojo> list = SimEditProduct.getSims(reporInfo);
            String json = new Gson().toJson(list);
            out.print(json);
        } else if (action.equals("count")) {
            Integer countRows = SimEditProduct.getCountSims(reporInfo);
            out.print(countRows);
        }
        
    } else if (action.equals("products")){
        
        List<SimProductPojo> listProducts = null;
        if (request.getParameter("allCarriers")!=null){
            listProducts = SimEditProduct.getSimProductsList(null);
        } else{
            String carriers = request.getParameter("carriers");
            listProducts = SimEditProduct.getSimProductsList(carriers);
        }
        response.setContentType("application/json");
        String json = new Gson().toJson(listProducts);
        out.print(json);
        
    } else if (action.equals("newProducts")){
        response.setContentType("application/json");
        List<SimProductPojo> listProducts = SimEditProduct.getSimProductsList(null);
        String json = new Gson().toJson(listProducts);
        out.print(json);
        
    } else if (action.equals("changeSims")){
        reporInfo.setNewCarrier(request.getParameter("carrierId"));
        reporInfo.setNewProduct(request.getParameter("productId"));
        Integer rows = SimEditProduct.changeProductAndCarrier(reporInfo);
        out.print(rows);
    } else if (action.equals("alive")){
        reporInfo.setNewCarrier(request.getParameter("carrierId"));
        reporInfo.setNewProduct(request.getParameter("productId"));
        reporInfo.setStartDate(request.getParameter("startDate"));
        reporInfo.setEndDate(request.getParameter("endDate"));
        Integer rows = SimEditProduct.changeProductAndCarrierAndStatus(reporInfo);
        out.print(rows);
    } else if (action.equals("deleteSims")){ //
        Integer rows = SimEditProduct.deleteSims(reporInfo);
        out.print(rows);
    }

%>