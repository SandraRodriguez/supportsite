<%@page import="com.debisys.utils.HTMLEncoder, com.debisys.presentation.ToolGroup"%>
<%
int section=9;
int section_page=9;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
ToolGroup t = ToolGroup.createTransactionsGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
%>
<style>
reportGroupTable {
	BORDER-RIGHT: #7B9EBD 1px solid; BORDER-TOP: #7B9EBD 1px solid; FONT-SIZE: 9pt; BORDER-LEFT: #7B9EBD 1px solid; WIDTH: 100%; COLOR: #7B9EBD; BORDER-BOTTOM: #7B9EBD 1px solid; FONT-FAMILY: Tahoma; BORDER-COLLAPSE: collapse
}
.reportGroupTable TD {
	PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; VERTICAL-ALIGN: text-top; CURSOR: pointer; PADDING-TOP: 4px; HEIGHT: 8px
}
.reportGroupTable TH {
	PADDING-RIGHT: 4px; PADDING-LEFT: 4px; FONT-WEIGHT: 700; PADDING-BOTTOM: 4px; PADDING-TOP: 4px; HEIGHT: 8px
}
.reportGroupTable A {
	FONT-SIZE: 9pt; TEXT-DECORATION: none
}
.reportGroupTable A:link {
	FONT-SIZE: 9pt; TEXT-DECORATION: none
}
.reportGroupTable A:visited {
	FONT-SIZE: 9pt; TEXT-DECORATION: none
}
.reportGroupTable A:active {
	FONT-SIZE: 9pt; TEXT-DECORATION: none
}
.reportGroupTable A:hover {
	FONT-SIZE: 9pt; TEXT-DECORATION: none
}
.reportGroupTable .sublevel {
	DISPLAY: none
}
.reportGroupTable .sublevel LI:hover {
	BACKGROUND-COLOR: #ffffff
}
.reportGroupTable .sublevel LI.group-open {
	LIST-STYLE-IMAGE: url(images/group-open-icon.png); BACKGROUND-COLOR: #ffffff;padding-left:18px;
}
.reportGroupTable .sublevel LI.group-closed {
	LIST-STYLE-IMAGE: url(images/group-closed-icon.png);padding-left:18px;
}
.reportGroupTable .sublevel LI.report {
	LIST-STYLE-IMAGE: url(images/report-icon.png);	padding-left:18px;
}
.reportGroupTable .sublevel group-label {
	LIST-STYLE-IMAGE: url(images/group-open-icon.png); BACKGROUND-COLOR: #ffffff;padding-left:18px;
}
.main2{
	FONT-SIZE: 10pt;
	FONT-FAMILY: "Trebuchet MS", Arial, Helvetica, sans-serif;
	color: 628514;
	;padding-left:0px;
}
</style>

<script type="text/javascript" src="/support/includes/jquery.js"></script>

<script>
	$(function(){
		$(".group-label").click(function(){
			if( $('ul', $(this).parent()).is(':visible')) {
				$('ul', $(this).parent()).hide();
				$(this).parent().removeClass('group-open').addClass('group-closed');
			}else{
				//$("ul.sublevel", this).hide();
				$(this).parent().removeClass('group-closed').addClass('group-open').children().show().parents().show();
			}
		});
		$(".sublevel[name=main]").show();
                
                
	});
    function managementDocumentsFlowTool(){   
        var pageURL = escape($(location).attr("href"));        
        $.ajax({
            type: "POST",
            async: false,
            data: {action: ''},
            url: "admin/tools/toolLogic.jsp?urlReturn="+pageURL,
            success: function (data) {                
                if (data.response){                    
                    var url = data.url;
                    var token = data.token;
                    var openUrl = url+'/?token='+token;
                    debugger;
                    window.open(openUrl);                    
                }
            }
        });
        return false;
    }
</script>
<table cellSpacing=0 cellPadding=0 width=750 background=images/top_blue.gif border=0>
	<tr>
		<td width=23 height=20><IMG height=20 src="images/top_left_blue.gif" width=18></td>
		<td width="4000" class=formAreaTitle><%=Languages.getString("jsp.includes.menu.tools",SessionData.getLanguage()).toUpperCase()%></td>
		<td height=20><IMG src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
            <td colSpan=3>
                    <table cellSpacing=0 cellPadding=0 width="100%" bgColor=#7B9EBD border=0>
                            <tr>
                                <td width=1 bgColor=#7B9EBD><img src="images/trans.gif" width=1></td>
                                <td vAlign=top align=middle bgColor=#ffffff>
                                    <table width="100%" border=0 align=center cellPadding=2 cellSpacing=0 class="fondoceldas">
                                        <tr>
                                            <td class=main align="left">
                                                <table class=reportGroupTable>
                                                    <tr>
                                                        <th align=left>
                                                            <img height=22 src="images/analysis_cube.png" width=22 border=0>
                                                            <span class="main">
                                                                <%=Languages.getString("jsp.includes.menu.tools",SessionData.getLanguage()).toUpperCase() %>
                                                            </span>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <ul class=sublevel name="main">
                                                                <% t.showItem(out); %>
                                                            </lu>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                            </tr>
                            <tr>
                                <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
                            </tr>
                    </table>
            </td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>