<%@ page language="java" import="java.util.*, com.debisys.languages.*,com.debisys.customers.Rep" pageEncoding="ISO-8859-1"%>
<%@ page import="com.debisys.tools.dto.S2kStockCode" %>
<%
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="s2k" class="com.debisys.tools.s2k" scope="request"/>
<%
	
  	int section=9;
  	int section_page=13;
  	int result = -1;
%>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %> 
<% 
    
	String strRefId = SessionData.getProperty("ref_id");
	String isoId = Rep.getIsoID(strRefId, strAccessLevel);
	if(isoId == null){
	 isoId = strRefId;
	}
%>
<style>
.tablebutton {
align:center;
text-align: center;
}
input.btnhov {
  color:#050;
  font: bold 84% 'trebuchet ms',helvetica,sans-serif;
  background-color:#ffaaaaff;
  border: 2px solid;
  border-color: #7b9ebd;
  filter:progid:DXImageTransform.Microsoft.Gradient
  (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffaaaaff');
}
input.btn {
  color:#050;
  font: bold 84% 'trebuchet ms',helvetica,sans-serif;
  background-color:#ffaaaaff;
  border: 2px solid;
  border-color: #005eb2;
  filter:progid:DXImageTransform.Microsoft.Gradient
  (GradientType=0,StartColorStr='#ffffffff',EndColorStr='#ffaaaaff');
}
</style>
<link rel="stylesheet" type="text/css" href="includes/s2k/jquery.autocomplete.css" />
	<!-- <script src="includes/s2k/jsapi"></script> -->  
	<!-- <script>  
		google.load("jquery", "1");
	</script> -->
<script type="text/javascript" src="includes/s2k/jquery.autocomplete.js"></script>
<script type="text/javascript" src="includes/s2k/s2kstockcodesfunctions.js"></script>

<table   width="99%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td>
<%
	List<S2kStockCode> vecSearchResults = s2k.gets2kstockcode(isoId);
%>
<table border="0" cellpadding="0" cellspacing="0" width="800"  background="images/top_blue.gif">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.s2k.task.title111",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3"  bgcolor="#FFFFFF">
			<noscript>
								   <%
								   out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>"); 
								   out.println("<li>" + Languages.getString("jsp.s2k.errornoscript",SessionData.getLanguage())); 
									  out.println("</font></td></tr></table>"); 
								   %>
			</noscript>
			<table border="0" cellpadding="0" cellspacing="0" width=100% align=center>
				<tr>
					<td class=formArea2>
						<table>
							<tr>
								<td colspan="4">					
									<table border="0" cellpadding="2" cellspacing="0" width=100% align="center">
										<tr>
											<td class="main"><br>
													<div style="height: 372px; overflow-y: auto;">
												<table  id="dataTable"  cellspacing="1" cellpadding="2" width=100% style = "table-layout: fixed;">
													<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  >
														<th class=rowhead2 width="10%"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.SKU",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>	   
														<th class=rowhead2 width="45%"><%=Languages.getString("jsp.admin.report.transactions.products.merchants.Product",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>   	
														<th class=rowhead2 width="25%"><%=Languages.getString("jsp.admin.tools.s2k.stockcode",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2 width="25%"><%=Languages.getString("jsp.admin.tools.s2k.excludeTax",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2 width="10%" style="text-align:center;"><%=Languages.getString("jsp.admin.tools.s2k.edit",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
														<th class=rowhead2 width="10%" style="text-align:center;"><%=Languages.getString("jsp.admin.tools.s2k.delete",SessionData.getLanguage()).toUpperCase()%>&nbsp;</th>
													</tr>
<%
	int intCounter = 1;
	int intEvenOdd = 1;
	Iterator<S2kStockCode> it = vecSearchResults.iterator();
	
	while (it.hasNext())
	{
		S2kStockCode stock = (S2kStockCode) it.next();
%>		
														<tr onselectstart="return false;" style="cursor:default;"  onmousedown="return false;"  class=rowwrap<%=intEvenOdd%>>
															<td valign=top><%=stock.getProductId()%></td>
															<td valign=top><%=stock.getDescription()%></td>
															<td valign=top><%=stock.getStockCode()%></td>
															<td valign=top><%=stock.isExcludeTax() ? Languages.getString("jsp.admin.tools.s2k.excludeTax.yes",SessionData.getLanguage()) : "no"%></td>
															<td valign=top align=center><img src="images/icon_edit.gif"    onclick="editstockcode('<%=isoId%>','<%=stock.getProductId()%>','<%=stock.getStockCode()%>', '<%=stock.isExcludeTax() %>')"  border=0 alt="Edit" title="Edit" /></td>
															<td valign=top align=center><img src="images/icon_delete.gif"  onclick="deletestockcode('<%=isoId%>','<%=stock.getProductId()%>','<%=stock.getStockCode()%>')"  border=0 alt="Delete" title="Delete" /></td>
														</tr>
<%          
		// Used to make the rows alternation in background color
    	if (intEvenOdd == 1)
      		intEvenOdd = 2;
    	else
      		intEvenOdd = 1;
	}
	vecSearchResults.clear();
%>            				
									</table></div>
								</td>
							</tr></table>
							<table cellspacing="1" cellpadding="2" width=100% height ="30px" style = "table-layout: fixed;">							<tr>
								<td class=rowhead2> 				
									<%=Languages.getString("jsp.admin.report.transactions.products.merchants.SKU",SessionData.getLanguage())%>:				
									<input type="text" name ="pid" id="pid" maxlength="20" onkeypress="doClick('addbutton',event);" /> 
									<script>
									$("#pid").autocomplete("includes/s2k/getsku.jsp");
									</script>
									<%=Languages.getString("jsp.admin.tools.s2k.stockcode",SessionData.getLanguage())%>:				
									<input type="text" name ="sc"  maxlength="20" id="sc" value="" onkeypress="doClick('addbutton',event);"/>
									<%=Languages.getString("jsp.admin.tools.s2k.excludeTax",SessionData.getLanguage())%>:
									<input type="checkbox" name ="excludeTax" id="excludeTax" onclick="clickExcludeTax()"/>
									<input type="hidden" name="ISOID" id= "ISOID" value="<%=isoId%>" />
									<input id="addbutton" name="addbutton" onmouseover="hov(this,'btnhov');" onmouseout="hov(this,'btn');"  type="button" style="" onclick="addRow(<%=isoId%>)" value="<%=Languages.getString("jsp.admin.tools.s2k.add",SessionData.getLanguage())%>"/>
									<input id="editbutton" name="editbutton" type="hidden" value="<%=Languages.getString("jsp.admin.tools.s2k.save",SessionData.getLanguage())%>"/>
								    <input id="add" name="add" type="hidden" value=" <%=Languages.getString("jsp.admin.tools.s2k.add",SessionData.getLanguage())%> "/>
								    <input id="excludeTaxText" name="excludeTaxText" type="hidden" value="no"/>
									<input id="cancelbutton" name="cancelbutton" onmouseover="hov(this,'btnhov')" onmouseout="hov(this,'btn')"  type="button" onclick="cancel();" value="<%=Languages.getString("jsp.admin.tools.s2k.cancel",SessionData.getLanguage())%>"/>
<script type="text/javascript">
//hide button onload								
var cancel_button = document.getElementById('cancelbutton');
  cancel_button.style.visibility = 'hidden'; 
</script>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<%@ include file="/includes/footer.jsp" %>