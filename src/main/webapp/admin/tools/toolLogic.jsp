<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="com.debisys.tools.TokenService"%>
<%@page import="com.google.gson.JsonObject"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
    response.setContentType("application/json");    
    String urlReturn = request.getParameter("urlReturn");
    JsonObject jsonResponse = new JsonObject();       
    String entityId = SessionData.getUser().getRefId();
    String language = SessionData.getLanguage();
    Boolean hasAdminPerm =SessionData.checkPermission(DebisysConstants.PERM_ALLOW_ADMIN_DOCUMENT_MANAGEMENT);
    jsonResponse = TokenService.buildToken(entityId, language, hasAdminPerm, urlReturn);
    out.print(jsonResponse);
%>
