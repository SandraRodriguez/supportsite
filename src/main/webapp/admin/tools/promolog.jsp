<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.*,
                 java.util.Hashtable,
                 com.debisys.promotions.*" %>
<%
int section=9;
int section_page=17;
int iPromoID = -1;
String strProductID = "";
String strISOID = "";
String strRepID = "";
String strCarrierID = "";
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>

<%
  	Vector vecLogRegistries = new Vector();

	if ((request.getParameter("promoID") != null)&&(NumberUtil.isNumeric(request.getParameter("promoID"))) ){
		iPromoID = Integer.parseInt(request.getParameter("promoID"));
		vecLogRegistries = Promotion.GetPromoLog(iPromoID);
	}
	
	if (request.getParameter("prsID") != null) {
		strProductID = request.getParameter("prsID");
	}
	
	if (request.getParameter("isosID") != null) {
		strISOID = request.getParameter("isosID");
	}
	
	if (request.getParameter("repId") != null) {
		strRepID = request.getParameter("repId");
	}

	if (request.getParameter("carrierId") != null) {
		strCarrierID = request.getParameter("carrierId");
	}	

%>
<%@ include file="/includes/header.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/bootstrap.min.css">
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/paginathing.js"></script>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/Promolog.js"></script>

<script language="javascript">



</script>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr><td><br/></td>
  	<td align="right"><input id="btnSubmit1" type="button" value="<%=Languages.getString("jsp.admin.tools.editpromotions.cancel",SessionData.getLanguage())%>" ONCLICK="<%="javascript:location.href='/support/admin/tools/managepromos.jsp" + "?repId=" + strRepID + "&prsID=" + strProductID + "&isosID=" + strISOID + "&carrierId=" + strCarrierID + "';"%>">
  	</td></tr>
	<tr><td><br/></td></tr>
	<tr>
	    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.promolog.title",SessionData.getLanguage()).toUpperCase() +"  "+Languages.getString("jsp.admin.tools.promolog.title_promotionid",SessionData.getLanguage()).toUpperCase()+"  "+ Integer.toString(iPromoID)%></td>
	    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr><td><br/></td></tr>
  	 	
  	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
  			<form method="get" action="admin/tools/promolog.jsp">
<%
if (vecLogRegistries != null && vecLogRegistries.size() > 0)
{
%>
			<table id="promotionsTableSummary" width="100%" cellspacing="1" cellpadding="1" border="0" class="table table-bordered">
                            <thead>
			     <tr>
			        <th class=rowhead2><%=Languages.getString("jsp.admin.tools.promolog.logentry",SessionData.getLanguage()).toUpperCase()%></th>
			     	<th class=rowhead2><%=Languages.getString("jsp.admin.tools.promolog.datetime",SessionData.getLanguage()).toUpperCase()%></th>
			      	<th class=rowhead2 ><%=Languages.getString("jsp.admin.tools.promolog.user",SessionData.getLanguage()).toUpperCase()%></th>
			     	<th class=rowhead2 ><%=Languages.getString("jsp.admin.tools.promolog.changes",SessionData.getLanguage()).toUpperCase()+"<br/>"%></th>
			     </tr> 
                            </thead>
                            <tbody>
<%
                  Collections.reverse(vecLogRegistries);
                  Iterator it = vecLogRegistries.iterator();
                  int intEvenOdd = 1;
                  int icount=0;
                  while (it.hasNext())
                  {
                    icount++;
                    PromotionLog promTemp = (PromotionLog) it.next();
                    out.println("<tr class=row" + intEvenOdd +">");
                    out.println("<td align=left class=main> " + HTMLEncoder.encode(Integer.toString(promTemp.getRegistryID())) + "</td>");
                    out.println("<td align=left class=main> " + HTMLEncoder.encode(promTemp.getDateTime()) + "</td>");
                    out.println("<td align=left class=main> " + HTMLEncoder.encode(promTemp.getUserName()) + "<br/></td>");
                    // Changes
                    String changes = "";
                    if ((promTemp.getChangeName().equals("Promo Registry"))&&promTemp.getOldValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line1",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line2",SessionData.getLanguage()); 
                    } else if ((promTemp.getChangeName().equals("Promo Registry"))&&promTemp.getNewValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line3",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line4",SessionData.getLanguage());  
                    } else if (promTemp.getTableName().equals("PROMO_INFO")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line5",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line6",SessionData.getLanguage()) + promTemp.getFieldChanged() + Languages.getString("jsp.admin.tools.promolog.line7",SessionData.getLanguage()) + promTemp.getOldValue() + Languages.getString("jsp.admin.tools.promolog.line8",SessionData.getLanguage()) + promTemp.getNewValue() + "'";
                    } else if ((promTemp.getChangeName().equals("Threshold Registry"))&&promTemp.getOldValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line9",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line10",SessionData.getLanguage()) + Float.toString(promTemp.getMinTransaction()) + "'"; 
                    } else if ((promTemp.getChangeName().equals("Threshold Registry"))&&promTemp.getNewValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line11",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line12",SessionData.getLanguage()) + Float.toString(promTemp.getMinTransaction()) + "'"; 
                    } else if (promTemp.getTableName().equals("PROMO_THRESHOLDS")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line13",SessionData.getLanguage()) + Float.toString(promTemp.getMinTransaction()) + Languages.getString("jsp.admin.tools.promolog.line14",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line15",SessionData.getLanguage()) + promTemp.getFieldChanged() + Languages.getString("jsp.admin.tools.promolog.line16",SessionData.getLanguage()) + promTemp.getOldValue() + Languages.getString("jsp.admin.tools.promolog.line17",SessionData.getLanguage()) + promTemp.getNewValue() + "'";
                    } else if ((promTemp.getChangeName().equals("Account Number Registry"))&&promTemp.getOldValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line18",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line19",SessionData.getLanguage()) + promTemp.getAccountNumber() + Languages.getString("jsp.admin.tools.promolog.line20",SessionData.getLanguage()) + Integer.toString(promTemp.getAccountTest()) + "'"; 
                    } else if ((promTemp.getChangeName().equals("Account Number Registry"))&&promTemp.getNewValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line21",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line22",SessionData.getLanguage()) + promTemp.getAccountNumber() + Languages.getString("jsp.admin.tools.promolog.line23",SessionData.getLanguage()) + Integer.toString(promTemp.getAccountTest()) + "'"; 
                    } else if (promTemp.getTableName().equals("PROMO_ACCOUNTNUMBER")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line24",SessionData.getLanguage()) + promTemp.getAccountNumber() + Languages.getString("jsp.admin.tools.promolog.line25",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line26",SessionData.getLanguage()) + promTemp.getFieldChanged() + Languages.getString("jsp.admin.tools.promolog.line27",SessionData.getLanguage()) + promTemp.getOldValue() + Languages.getString("jsp.admin.tools.promolog.line28",SessionData.getLanguage()) + promTemp.getNewValue() + "'";
                    } else if ((promTemp.getChangeName().equals("Merchant Registry"))&&promTemp.getOldValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line29",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line30",SessionData.getLanguage()) + Long.toString(promTemp.getMerchantID()) + Languages.getString("jsp.admin.tools.promolog.line31",SessionData.getLanguage())  + Integer.toString(promTemp.getMerchantTest()) + Languages.getString("jsp.admin.tools.promolog.line32",SessionData.getLanguage())  + Integer.toString(promTemp.getAuthMerchant()) + Languages.getString("jsp.admin.tools.promolog.line33",SessionData.getLanguage())  + Integer.toString(promTemp.getAllMerchant()) + "'"; 
                    } else if ((promTemp.getChangeName().equals("Merchant Registry"))&&promTemp.getNewValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line34",SessionData.getLanguage())  + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line35",SessionData.getLanguage())  + Long.toString(promTemp.getMerchantID()) + Languages.getString("jsp.admin.tools.promolog.line36",SessionData.getLanguage())  + Integer.toString(promTemp.getMerchantTest()) + Languages.getString("jsp.admin.tools.promolog.line37",SessionData.getLanguage())  + Integer.toString(promTemp.getAuthMerchant()) + Languages.getString("jsp.admin.tools.promolog.line38",SessionData.getLanguage())  + Integer.toString(promTemp.getAllMerchant()) + "'"; 
                    } else if (promTemp.getTableName().equals("PROMO_ACCOUNTNUMBER")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line39",SessionData.getLanguage())  + promTemp.getAccountNumber() + Languages.getString("jsp.admin.tools.promolog.line40",SessionData.getLanguage())  + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line41",SessionData.getLanguage()) + promTemp.getFieldChanged() + Languages.getString("jsp.admin.tools.promolog.line42",SessionData.getLanguage()) + promTemp.getOldValue() + Languages.getString("jsp.admin.tools.promolog.line43",SessionData.getLanguage()) + promTemp.getNewValue() + "'";
                    } else if ((promTemp.getChangeName().equals("Site Registry"))&&promTemp.getOldValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line44",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line45",SessionData.getLanguage()) + Integer.toString(promTemp.getSiteID()) + Languages.getString("jsp.admin.tools.promolog.line46",SessionData.getLanguage()) + Integer.toString(promTemp.getSiteTest()) + Languages.getString("jsp.admin.tools.promolog.line47",SessionData.getLanguage()) + Integer.toString(promTemp.getAuthSite()) + Languages.getString("jsp.admin.tools.promolog.line48",SessionData.getLanguage()) + Integer.toString(promTemp.getAllSite()) + "'"; 
                    } else if ((promTemp.getChangeName().equals("Site Registry"))&&promTemp.getNewValue().equals("")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line49",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line50",SessionData.getLanguage()) + Integer.toString(promTemp.getSiteID()) + Languages.getString("jsp.admin.tools.promolog.line51",SessionData.getLanguage()) + Integer.toString(promTemp.getSiteTest()) + Languages.getString("jsp.admin.tools.promolog.line52",SessionData.getLanguage()) + Integer.toString(promTemp.getAuthSite()) + Languages.getString("jsp.admin.tools.promolog.line53",SessionData.getLanguage()) + Integer.toString(promTemp.getAllSite()) + "'"; 
                    } else if (promTemp.getTableName().equals("PROMO_SITE")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line54",SessionData.getLanguage()) + promTemp.getAccountNumber() + Languages.getString("jsp.admin.tools.promolog.line55",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line56",SessionData.getLanguage()) + promTemp.getFieldChanged() + Languages.getString("jsp.admin.tools.promolog.line57",SessionData.getLanguage()) + promTemp.getOldValue() + Languages.getString("jsp.admin.tools.promolog.line58",SessionData.getLanguage()) + promTemp.getNewValue() + "'";
                    }  else if (promTemp.getTableName().equals("PROMO_PRODUCT")) {
                    	changes = Languages.getString("jsp.admin.tools.promolog.line5",SessionData.getLanguage()) + promTemp.getPromoName() + Languages.getString("jsp.admin.tools.promolog.line6",SessionData.getLanguage()) + promTemp.getFieldChanged() + Languages.getString("jsp.admin.tools.promolog.line7",SessionData.getLanguage()) + promTemp.getOldValue() + Languages.getString("jsp.admin.tools.promolog.line8",SessionData.getLanguage()) + promTemp.getNewValue() + "'";
                    }  
                    out.println("<td align=left wrap left class=main > " + HTMLEncoder.encode(changes) + "<br/></td></tr>");
                    
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecLogRegistries.clear();
            %>                
		</table>
	<%
	} else {/*(vecLogRegistries != null && vecLogRegistries.size() > 0)*/ %>
 			<table width="100%" cellspacing="1" cellpadding="1" border="0">     
			     <tr>
			        <td class=main><%=Languages.getString("jsp.admin.tools.promolog.noresults",SessionData.getLanguage())%></td>
			     </tr> 
			    </table>           
            
	<%
	} /*(vecLogRegistries != null && vecLogRegistries.size() > 0)*/ %>            					
           </form>
          </td>
      </tr>
    </table>


<%@ include file="/includes/footer.jsp" %>
