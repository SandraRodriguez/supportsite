<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@page contentType="text/html" import="com.debisys.tools.*,com.debisys.customers.Merchant,com.debisys.terminals.Terminal" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
int section=9;
int section_page=7;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<style>
	.toolTip {padding-right: 20px;background: url(images/help.gif) no-repeat right;color: #3366FF;cursor: help;position: relative; with: 18px; height: 18px;}
	.toolTipWrapper {width: 175px;position: absolute;top: 20px;display: none;color: #FFF;font-weight: bold;font-size: 9pt; z-index: 100}
	.toolTipTop {width: 175px;height: 30px;background: url(images/bubbleTop.gif) no-repeat;}
	.toolTipMid {padding: 8px 15px;background: #A1D40A url(images/bubbleMid.gif) repeat-x top;}
	.toolTipBtm {height: 13px;background: url(images/bubbleBtm.gif) no-repeat;}
	ol#instructions {
	}
	ol#instructions > li {
		margin-top: 1.5em;
	}

	ol#recommends {
		list-style: none outside none;
		/*font-family: monospace;*/
	}
	ol#recommends > li {
		margin-top: 1.5em;
	}
	ol#recommends > li > span{
		display: inline-block;
		margin-left: 3em;
		margin-top: -1.5em;
	}
	ol#recommends {
		counter-reset: itemnr;
	} 
	ol#recommends > li:before{ 
		content: "(" counter(itemnr, lower-alpha) ")"; 
		counter-increment: itemnr; 
		display: marker;
	}
	ol#recommends > li > span{
		padding: 5px;
		display: inline-block;
	}
</style>
<script>
	$(function() {
		//$( document ).tooltip();
		$(document).ready(function(){
			$('.toolTip').hover(
				function() {
					this.tip = this.title;
					$(this).append('<div class="toolTipWrapper"><div class="toolTipTop"></div><div class="toolTipMid">'+this.tip+'</div><div class="toolTipBtm"></div></div>');
					this.title = "";
					this.width = $(this).width();
					$(this).find('.toolTipWrapper').css({left:this.width-22});
					$('.toolTipWrapper').fadeIn(300);
				},
				function() {
					$('.toolTipWrapper').fadeOut(100);
					$(this).children().remove();
					this.title = this.tip;
				}
			);
		});
	});
</script> 
<link href="default.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
	function validateFile(){
		var nameFile = document.getElementById("xls_filename");
		var nameFilesplit = nameFile.value.split("\\");
		var nameF = nameFilesplit[nameFilesplit.length-1];
		nameF= nameF.toLowerCase();
		if (nameF.indexOf(".xls") > 0){ 
			return true;
		}else{
			alert("<%=Languages.getString("jsp.admin.tools.general_validate_file",SessionData.getLanguage())%>");
			return false;
		}
	}
</script>
<table border="1" width ="960" class="main" bgcolor="#FFFFFF" cellpadding="20">
	<tr>
		<th align="left"  colspan="7"><%=Languages.getString("jsp.admin.reports.analysis.TitleMerchantBulkImport",SessionData.getLanguage())%></th>
	</tr>
	<tr>
		<td align="left" colspan="7" class="main">
			<p><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInstP1",SessionData.getLanguage())%></p>
			<p><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInstP2",SessionData.getLanguage())%></p>
			<ol id="instructions">
				<li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst1",SessionData.getLanguage())%>
<%
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
					<a href="documents/Inter.xls"><%=Languages.getString("jsp.admin.tools.format_bulk",SessionData.getLanguage())%></a>
<%
}else{
%>
					<a href="documents/Domestic.xls"><%=Languages.getString("jsp.admin.tools.format_bulk",SessionData.getLanguage())%></a>
<%
}
%>
				</li>
				<li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst3",SessionData.getLanguage())%>
				<p>
					<ol id="recommends">
						<li><span><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInstA",SessionData.getLanguage())%></span></li>
						<li><span><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInstB",SessionData.getLanguage())%></span></li>
<%
if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ 
%>
						<li><span><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInstC",SessionData.getLanguage())%></span></li>
<%
}else{
%>
						<li><span><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInstC_international",SessionData.getLanguage())%></span></li>
<%
}
%>
						<li><span><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInstD",SessionData.getLanguage())%></span></li>
					</ol>
				</p>
				<li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst3",SessionData.getLanguage())%></li>
				<li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst4",SessionData.getLanguage())%></li>
				<li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst5",SessionData.getLanguage())%></li>
				<li><%=Languages.getString("jsp.admin.reports.analysis.bulkImportInst6",SessionData.getLanguage())%></li>
			</ol>
		</td>
	</tr>
</table>
<br>
<br>
<table border="1" width = 1200 class="main" bgcolor="#FFFFFF" cellpadding="20">
	<tr>
		<th align="left" colspan="7">
			<form ACTION="" ENCTYPE="multipart/form-data" METHOD=POST >
				<p><%=Languages.getString("jsp.admin.tools.title1",SessionData.getLanguage())%></p>
				<input type="file" name="xls_filename" id="xls_filename" size="100"><br>
				<input type="submit" value="<%=Languages.getString("jsp.admin.tools.loadbutton",SessionData.getLanguage())%>" onclick="return validateFile();">
			</form>
		</th>
	</tr>
</table>
<br>
<br>
<%
ArrayList<MerchantBulk> arrayMerchant = new ArrayList<MerchantBulk>();
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String message="";
String combosTypes="";
String contentType = request.getContentType();
String file = "";
String saveFile = "";
try{
	String remoteHost = request.getRemoteHost();
	String localAddr =  request.getLocalAddr();
	String contextPath = request.getContextPath();
	String mailHost = DebisysConfigListener.getMailHost(application);
	String userName = SessionData.getProperty("username");
	String companyName = SessionData.getProperty("company_name");
	String workingDir  = DebisysConfigListener.getWorkingDir(application);
	//InputStream inp = application.getResourceAsStream(filename);
	BulkMerchants objBulkMerchants = new BulkMerchants(SessionData,application,customConfigType,remoteHost,localAddr,contextPath,mailHost,companyName,userName);
	combosTypes = objBulkMerchants.getComboTypes();
	if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
		arrayMerchant = objBulkMerchants.analizeInputStream(request,workingDir,SessionData,application);  
	}
	message = objBulkMerchants.getMessagesError().toString();
	//errorHashtable
}catch (Exception e){
	message="JSP Error";
}
%>
<table border="1" class="main" bgcolor="#FFFFFF">
	<thead>
		<tr><th align="left" colspan="7"><%=Languages.getString("jsp.admin.reports.analysis.message_info_bulk",SessionData.getLanguage())%></th></tr>
		<tr><td colspan="7"><%=combosTypes%></td></tr>
<% 
if (customConfigType.equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){
%>
		<tr>
			<th align="left"  colspan="7">
				* <%=Languages.getString("jsp.admin.reports.analysis.invoice",SessionData.getLanguage())%>
				<br /><br />
			</th>
		</tr>
<% 
}
%>
		<tr><th align="left" class="rowred1" colspan="7"><%=message%></th></tr>
		<tr class=rowhead2>
			<th>#</th>
			<th><%=Languages.getString("jsp.admin.tools.messages",SessionData.getLanguage()).toUpperCase()%></th>
			<th><%=Languages.getString("jsp.admin.ach.summary.rep",SessionData.getLanguage()).toUpperCase()%></th>
			<th><%=Languages.getString("jsp.admin.ach.summary.merchant",SessionData.getLanguage()).toUpperCase()%></th>
			<th><%=Languages.getString("jsp.admin.customers.merchants_edit.business_name",SessionData.getLanguage()).toUpperCase()%></th>
			<th><%=Languages.getString("jsp.admin.customers.merchants_edit.dba",SessionData.getLanguage()).toUpperCase()%></th>
			<th><%=Languages.getString("jsp.admin.customers.merchants_edit.terminal_information",SessionData.getLanguage()).toUpperCase()%></th>
		</tr>
	</thead>
	<tbody>
<%
int irowId=1;
int irowCss=1;
for(MerchantBulk objMerchant : arrayMerchant){
	String tableErrorsMerchant = objMerchant.getTableErrorValidation();
	String repid = objMerchant.getRepId();
	String merchantId = objMerchant.getMerchantId();
	String businessName = objMerchant.getBusinessName();
	String dba = objMerchant.getDba();
%>
		<tr class="row<%=irowCss%>">
			<td><%=irowId%></td>
			<td align="left" ><%=tableErrorsMerchant%>
			<td><%=repid%></td>
			<td><%=merchantId%></td>
			<td><%=businessName%></td>
			<td><%=dba%></td> 
			<td>
<%
	if ( objMerchant.getArrayTerminals().size() > 0 ){
%>
				<table class="main" border="1">
					<tr>
						<th class=rowhead2><%=Languages.getString("jsp.admin.customers.merchants_info.site_id",SessionData.getLanguage()).toUpperCase()%></th>
						<!--  
						<th class=rowhead><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pc_term_username",SessionData.getLanguage()).toUpperCase()%></th>
						<th class=rowhead><%=Languages.getString("jsp.admin.customers.merchants_add_terminal.pc_term_password",SessionData.getLanguage()).toUpperCase()%></th>
						-->
						<th class=rowhead2><%=Languages.getString("jsp.admin.tools.messages",SessionData.getLanguage()).toUpperCase()%></th>
					</tr>
<%
		for(TerminalBulk objTerminal : objMerchant.getArrayTerminals()){
			String terminalType = objTerminal.getTerminalDesc();
			String tableErrorsTerminal = (objTerminal.getTableErrorValidation().length()==0?"--":objTerminal.getTableErrorValidation());
			String siteID = objTerminal.getSiteId();		                                    
			//String terminalUser= (objTerminal.getTerminalUser()==null?"--":objTerminal.getTerminalUser());
			//String terminalPass= (objTerminal.getTerminalPassword()==null?"--":objTerminal.getTerminalPassword());
			//String pcTerminalUser= (objTerminal.getPcTermUsername()==null?"--":objTerminal.getPcTermUsername());
			//String pcTerminalPass= (objTerminal.getPcTermPassword()==null?"--":objTerminal.getPcTermPassword());
			if (objTerminal.getProcessOK()==0){
				//cannot create the terminal
			}else if (objTerminal.getProcessOK()==2){
				//create terminal OK
				//sent email notification OK
			}else if (objTerminal.getProcessOK()==3){
				//create terminal OK
				//Fail email notification 
			}
%>
					<tr>
						<td><%=siteID%> <%=terminalType%></td>
						<td align="left" class="rowred1"><%=tableErrorsTerminal%></td>
					</tr>
<%
		}
%>
				</table>
<%
	}
%>
			</td>
		</tr>
<%
	irowId++;
	if (irowCss==1)
		irowCss=2;
	else
		irowCss=1;
}
%>
	</tbody>
</table>
<br/>
<br/>
<br/>
<br/>