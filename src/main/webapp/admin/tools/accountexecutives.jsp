<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>

<%@page contentType="text/html" import="com.debisys.tools.*,com.debisys.customers.Merchant,com.debisys.terminals.Terminal" %>
  
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
  int section=9;
  int section_page=11;
 %>
  <%@ include file="/includes/security.jsp" %>
  <%@ include file="/includes/header.jsp" %> 

 <link rel="stylesheet" type="text/css" href="/support/css/displaytagex.css">
  <SCRIPT LANGUAGE="JavaScript">
    function sendUpdate(isChecked,id)
    {
    	var typeSelect = "0";
    	if (isChecked)
		{
			typeSelect="1";
		}
		$('#divMessage').load('admin/tools/accountexecutive_ajax.jsp?actionTask=updaterow&id='+id+'&status='+typeSelect+'&Random=' + Math.random());
    }
    function selectChk(isChecked)
    {    	
    	var typeSelect = "0";
		if (isChecked)
		{
			typeSelect="1";
		}
		//alert(typeSelect);				
		$('#divMessage').load('admin/tools/accountexecutive_ajax.jsp?actionTask='+typeSelect+'&Random=' + Math.random());
    }
  </SCRIPT>
	
<table border="1" cellpadding="0" cellspacing="0" width="600" height="104">
<tr>
	
	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">
		<b>&nbsp;&nbsp;&nbsp;&nbsp;<%=Languages.getString("jsp.admin.reports.tools.accountexecutives.title",SessionData.getLanguage()).toUpperCase()%></b>
		
	</td>
	
</tr>
<tr>
		
  <td colspan="0" height="10">
    
    <table cellpadding="0" cellspacing="0" width="818" height="139">
     <tr>
       <td width="1%" align="center" colspan="0" bgcolor="#FFFFFF"  rowspan="0">
       
       <% 
			String name = "";
			String statusExecutive = "";
			String id = "";
			
			String textButtonUpdateSave = Languages.getString("jsp.admin.reports.tools.accountexecutives.newexecutive",SessionData.getLanguage());
			
			String typeActionButton="n";
			
			String typeAction = request.getParameter("typeAction");
			String idActor = SessionData.getUser().getRefId();
			boolean updateNewFlag=false;
			String accountLabel = Languages.getString("jsp.admin.reports.tools.accountexecutives.label",SessionData.getLanguage());
			String statusLabel  = Languages.getString("jsp.admin.reports.tools.accountexecutives.enable",SessionData.getLanguage());
			String descriptionStatus="";
			String messageAction="";
				
			if (typeAction!=null)
			{
				if (typeAction.equals("n") && request.getMethod().equals("POST"))
				{	
					boolean status=true;
					name = request.getParameter("nameExecutive");
					//insert new  executive 
					if (name!=null && name.length()>2){ 
						AccountExecutive.addExecutive(name,status,Long.parseLong(idActor));
					}
					typeAction="";	
					typeActionButton="";
					name="";
				}
				else if (typeAction.equals("e"))
				{
					//set up executive fields in the html controls		
					typeActionButton="u";
					textButtonUpdateSave = Languages.getString("jsp.admin.reports.tools.accountexecutives.update",SessionData.getLanguage());
					id = request.getParameter("id");
					AccountExecutive account = AccountExecutive.findExecutiveById(Integer.parseInt(id),SessionData);
					name = account.getNameExecutive();
					updateNewFlag=true;
				}
				else if ( typeAction.equals("u") && request.getMethod().equals("POST") )
				{
					//update executive
					name = request.getParameter("nameExecutive");
					id = request.getParameter("id");
					if (name!=null && name.length()>2)
					{ 
						messageAction = AccountExecutive.updateExecutive(name,Integer.parseInt(id),SessionData);
					}
					name="";
				}
			}
			
			ArrayList<AccountExecutive> executives = AccountExecutive.findExecutivesByActor(Long.parseLong(idActor),request,false, SessionData);
			if (executives.size()>0 || updateNewFlag)
			{
				messageAction = "";
				request.setAttribute("accountExecutives",executives);
				
				ArrayList<String> statusAll = (ArrayList<String>)session.getAttribute("toolAccountExecutives");
				session.removeAttribute("toolAccountExecutives");
				if (statusAll!=null)
				{
					StringBuilder messagesMerchants = new StringBuilder();
					messagesMerchants.append(Languages.getString("jsp.admin.reports.tools.accountexecutives.message",SessionData.getLanguage())+" <br/>");
					for(String messageRow : statusAll )
					{
						messagesMerchants.append(messageRow+" <br/>");
					}
					messagesMerchants.append(" <br/>");
					messageAction = messagesMerchants.toString();
				}				
			
				String strchecked="";
				if (request.getParameter("actionTask")!=null && request.getParameter("actionTask").equals("1"))
				{
					strchecked="checked='checked'";
				}
			}
			%>
			  <form id="formExe" name="formExe" action="admin/tools/accountexecutives.jsp" method="post"> 
				<table  border="0" class="sort-table" cellpadding="0" cellspacing="0"  >
				    <thead>		
				    	<tr align="center" class="main" >
					    	<td><%=accountLabel%></td>
					    	<td>  </td>
					    </tr>
					    <tr height="50px" class="main">
				  			<td>
				  				<input type="text" id="nameExecutive" name="nameExecutive" value="<%=name%>"  maxlength="100" size="50">
				  				<input type="hidden" id="typeAction" name="typeAction" value="<%=typeActionButton%>">
				  				<input type="hidden" id="id" name="id" value="<%=id%>">
				  			</td>
				  			<td>  </td>
				  			<td>
				  				<input type="submit" value="<%=textButtonUpdateSave%>"/>
				  			</td>	
						</tr>
						<tr class="main">
					  	 <td style="color:red">
					  	 	<div id="divMessage"><%=messageAction%></div>					  	 
					  	 </td>
					  	</tr>
					  </thead>
					  <tbody>
					  	 <tr class="main">
						 	<td>
						 	 <input id="chkSelect" type="button" value="<%=Languages.getString("jsp.admin.reports.tools.accountexecutives.selectAll",SessionData.getLanguage())%>" onclick="selectChk(1);"     />
						 	 <input id="chkSelect" type="button" value="<%=Languages.getString("jsp.admin.reports.tools.accountexecutives.unselectAll",SessionData.getLanguage())%>" onclick="selectChk(0);"  />
						 	</td>						 	
						 </tr>
						 <tr style="width: 850px">
						      <td colspan="1" >
						         <display:table export="false"  name="accountExecutives" id="row" class="dataTable" cellspacing="1"  >
						            <display:column autolink="true" style="width: 69px" property="nameExecutiveLink" title="<%=accountLabel%>" sortable="true" class="customer" headerClass="customer"  />					              
						            <display:column property="enableCheckBox" title="<%=statusLabel%>" sortable="true" class="customer" headerClass="customer"  />
						         </display:table>
							</td>     
						 </tr>						 
						</tbody>
					 </table>
					 </form>
					 
				  
     </tr>
    </table>  

  </td>
		
</tr>



</table>

<%@ include file="/includes/footer.jsp"%>
  

