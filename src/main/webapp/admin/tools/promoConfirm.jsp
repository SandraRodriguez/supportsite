<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.*,
                 java.util.Hashtable,
                 com.debisys.promotions.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<%
int section=9;
int section_page=17;
int iPromoID = -1;
String strProductID = "";
String strISOID = "";
String strRepID = "";
String strCarrierID = "";
Promotion promotion = null;
String sAction="";
String strInstance = "";
String sPromoID="";
String treeResult="null";
String sCheckedNodes="null";

%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Properties" class="com.debisys.properties.Properties" scope="request" />
<%@ include file="/includes/security.jsp" %>

<%
Vector vecPromoThresholds = new Vector();

	Vector countryResults=Promotion.getISO(request,SessionData);
	Iterator iterator=countryResults.iterator();
	Vector vecTemp1 = (Vector) iterator.next();
	String rootIso=vecTemp1.get(0).toString();
	Vector agentEntities=null;
	Vector subAgentEntities=null;
	Vector repEntities=null;
	Vector merchantEntities=null;
	Vector siteEntities=null;
	
	String promoID=request.getParameter("promoID");
	
	String instance = DebisysConfigListener.getInstance(this.getServletContext());	
    Vector vecPromoProducts=null;
	if ((promoID!= null)&&(NumberUtil.isNumeric(promoID)) ){
		iPromoID = Integer.parseInt(promoID);
		sPromoID=promoID;
		promotion= Promotion.GetPromotionInfo(iPromoID);
		vecPromoProducts=Promotion.GetProducts(iPromoID);
		treeResult=Promotion.getTree(iPromoID,SessionData);
	 	sCheckedNodes=Promotion.getCheckedNodes(iPromoID);
		 vecPromoThresholds = Promotion.GetPromoThresholds(iPromoID);
  		
	}
	if (request.getParameter("lblAction") != null) {
  		sAction = request.getParameter("lblAction");
  	}
	/*if (request.getParameter("submitted") != null){
		if (request.getParameter("submitted").equals("y")){*/
		if(sAction.equals("save"))
		{
		    iPromoID=Integer.parseInt(promoID);
		   	Promotion.ChangeStatus(iPromoID, SessionData, instance);
                        promotion= Promotion.GetPromotionInfo(iPromoID);
			String mailHost = DebisysConfigListener.getMailHost(application);
			String mailUser= Properties.getPropertieByName(instance,"customerServiceMail");
                        String  recurrenceValue = request.getParameter("recurrenceValue");
			Promotion.sendEmail(mailHost,promotion,SessionData.getUser().getUsername(),application,mailUser, recurrenceValue);
		}
	if (request.getParameter("prsID") != null) {
		strProductID = request.getParameter("prsID");
	}
	
	if (request.getParameter("isosID") != null) {
		strISOID = request.getParameter("isosID");
	}
	
	if (request.getParameter("repId") != null) {
		strRepID = request.getParameter("repId");
	}

	if (request.getParameter("carrierId") != null) {
		strCarrierID = request.getParameter("carrierId");
	}	
	

%>
<%@ include file="/includes/header.jsp" %>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="includes/_lib/jquery.min.js"></script>    
<script type="text/javascript" src="includes/jquery.jstree.js"></script>
<script type="text/javascript" src="includes/_lib/jquery.cookie.js"></script>
	<script type="text/javascript" src="includes/_lib/jquery.hotkeys.js"></script>
	<link type="text/css" rel="stylesheet" href="includes/_docs/syntax/!style.css"/>
	<link type="text/css" rel="stylesheet" href="includes/_docs/!style.css"/>
	<script type="text/javascript" src="includes/_docs/syntax/!script.js"></script>	
	<script type="text/javascript" src="includes/json2.js"></script>
<script language="javascript">
    
    $(document).ready(function () {
        
        var rType = "<%=promotion.getRecurrenceType()%>";
        var rTypePattern = "<%=promotion.getRecurrencePattern()%>";                
        $('#DaysInNONE').hide();   
        $('#DaysInWeek').hide();
        $('#DaysInMonth').hide();  
        if (rTypePattern !== "null"){                    
            var patternValues = rTypePattern.split(',');                    
            for( i=0; i<patternValues.length; i++ ){                        
                if ( rType=== "<%=Promotion.RECURRENCE_PATTERN.WEEKLY%>" ){                    
                    $("#DaysInWeek tr td input[id="+patternValues[i]+"]").each(function(){
                        $(this).prop('checked', true);                               
                    });
                    $('#DaysInWeek').show();
                    $('#DaysInMonth').hide();
                } else if ( rType=== "<%=Promotion.RECURRENCE_PATTERN.MONTHLY %>" ){
                    $('#DaysInWeek').hide();
                    $('#DaysInMonth').show();
                    $("#DaysInMonth  tr td input[id="+patternValues[i]+"]").each(function(){
                        $(this).prop('checked', true);
                    });                    
                }   
            }
        }        
        //$("#RecurrenceValuesTR tr td input:checkbox").prop('disabled', true );
        var recurrenceValue = $("#RecurrenceValuesTR").html();
        debugger;
        $('#recurrenceValue').val(recurrenceValue);
        $("#RecurrenceValuesTR tr td input:checkbox").prop('disabled', true );
    });
    
function save()
{
    var recurrenceValue = $("#RecurrenceValuesTR").html();
    $('#recurrenceValue').val(recurrenceValue);
	document.getElementById('lblAction').value='save';
	document.getElementById('promoID').value="<%=sPromoID%>";        
	document.forms[0].submit();
}

function generateTree()
{
	document.getElementById('demo1').style.visibility='visible';
	document.getElementById('demo1').style.display='block';
		$(function () { 
			        $("#demo1").jstree({  
			            "json_data" : { 
			                "data" :<%=treeResult%>
			             },
			             "themes" : {
	            "theme" : "classic",
	            "dots" : false,
            "icons" : false
	        },
			            "plugins" : [ "themes", "json_data", "crrm","checkbox","ui"],rules : { deletable : "all",clickable : "false"    },
											"core" : { "initially_open" : [ "root.id" ]	}
			           
			        }); 
			        
			        
			});
			
			 var showList=[];
    	showList=[<%=sCheckedNodes%>];
	 $("#demo1").bind("load_node.jstree", function (event,data) {
	  		for (var i=0, len=showList.length; i < len; i++) 
	  		{ 
	  			$("#demo1").jstree('check_node', showList[i]); 
	 		 } 
	 		 
	 	});
	 	

	
}
</script>
<body onload="generateTree();">
<table border="0" cellpadding="0" cellspacing="0" width="750" id="tablePromotionInfo">
	<tr><td><br/></td></tr>
	<tr><td><br/></td></tr>
	<tr>
		 <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	    <td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.tools.promoConfiguration.title",SessionData.getLanguage()).toUpperCase() +"  "+Languages.getString("jsp.admin.tools.promolog.title_promotionid",SessionData.getLanguage()).toUpperCase()+"  "+ Integer.toString(iPromoID)%></td>
	    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr><td><br/></td></tr>
  	 	
  	<tr>
  	  	<td colspan="3"  bgcolor="#FFFFFF">
  			<form method="post" action="admin/tools/promoConfirm.jsp">
			 <input id="lblAction" type="hidden" name="lblAction" value="">
			 <input id="promoID" type="hidden" name="promoID" value="<%=sPromoID%>">
			 <input id="prsID" type="hidden" name="prsID" value="<%=strProductID%>">
			 <input id="isosID" type="hidden" name="isosID" value="<%=strISOID%>">
                        <input id="repId" type="hidden" name="repId" value="<%=strRepID%>">
                        <input id="carrierId" type="hidden" name="carrierId" value="<%=strCarrierID%>">
                        <input id="recurrenceValue" type="hidden" name="recurrenceValue" value="">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>  
			     <tr>
			        <td class="formAreaTitle2">
								<table width="100%" class="formArea2">
									<tr class="main"><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.promoid",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td><td>&nbsp;&nbsp;</td>
									<td><%=iPromoID%></td></tr> 
									<tr class="main" ><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.promoname",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td></td><td>&nbsp;&nbsp;</td>
									<td><%=promotion.getDescription()%></td></tr> 
									<tr class="main" ><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.productname",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td></td><td>&nbsp;&nbsp;</td>
									
									<td><%if(vecPromoProducts.size()>1){%><select id="pids1" name="pids1" size="5" multiple><%}else{%><select id="pids1" name="pids1"><%}%>
											<%Iterator it = null; 
											it = vecPromoProducts.iterator();
												while (it.hasNext()) {
														Vector vecTemp = null;
										                    vecTemp = (Vector) it.next();
										                     	out.println("<option value=\""+vecTemp.get(0).toString()+"\">" +
																	HTMLEncoder.encode(vecTemp.get(1).toString()+"  (" + vecTemp.get(0).toString() + ")") + "</option>");
					
													}
                                                                                                            
													%>
											</select></td></tr> 
									<tr class="main"><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.promostatus",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
									<td>&nbsp;&nbsp;</td>
									<td><%=promotion.getActive() == 0?Languages.getString("jsp.admin.tools.editpromotions.disabled",SessionData.getLanguage()):Languages.getString("jsp.admin.tools.editpromotions.enabled",SessionData.getLanguage()) %>
			    					</td></tr>
			    					<tr class="main" ><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.datapromo",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
									<td>&nbsp;&nbsp;</td>
									<td><%=promotion.getData()%></td></tr>                                                                        
									<tr class="main" ><td nowrap><%=Languages.getString("jsp.admin.tools.editpromotions.adtext",SessionData.getLanguage()) + ":&nbsp;&nbsp;"%></td>
									<td>&nbsp;&nbsp;</td>
									<td><%=promotion.getAdText()%></td></tr>									  	
                                                                        
                                                                <%
                                                                String recurrenceLabel = Languages.getString("jsp.admin.tools.editpromotions.recurrenceType", SessionData.getLanguage());
                                                                String recurrenceValue = "";
                                                                String NoneLbl = Languages.getString("jsp.admin.recurrenceType.None", SessionData.getLanguage());
                                                                String Weeklyl = Languages.getString("jsp.admin.recurrenceType.Weekly", SessionData.getLanguage());
                                                                String MonthlyLbl = Languages.getString("jsp.admin.recurrenceType.Monthly", SessionData.getLanguage());
                                                                boolean showRecurrenceTr = false;
                                                                String recurrence="";
                                                                
                                                                if ( promotion.getRecurrenceType().equals(Promotion.RECURRENCE_PATTERN.NONE)){
                                                                    recurrenceValue = NoneLbl;
                                                                } else if ( promotion.getRecurrenceType().equals(Promotion.RECURRENCE_PATTERN.WEEKLY) ){
                                                                    recurrenceValue = Weeklyl;
                                                                    showRecurrenceTr = true;
                                                                    if ( promotion.getRecurrencePattern() != null){
                                                                        recurrence = promotion.getRecurrencePattern();            
                                                                    }
                                                                } else if ( promotion.getRecurrenceType().equals(Promotion.RECURRENCE_PATTERN.MONTHLY) ){
                                                                    recurrenceValue = MonthlyLbl;
                                                                    showRecurrenceTr = true;
                                                                    if ( promotion.getRecurrencePattern() != null){
                                                                        recurrence = promotion.getRecurrencePattern();            
                                                                    }
                                                                }
                                                                %>
                                                                        <tr class="main" ><td nowrap><%=recurrenceLabel + ":&nbsp;&nbsp;"%></td>
                                                                            <td>&nbsp;&nbsp;</td>
                                                                            <td><%=recurrenceValue%></td>
                                                                        </tr>
                                                                <%if ( showRecurrenceTr ){%>        
                                                                        <tr class="main" id="RecurrenceValuesTR" name="RecurrenceValuesTR">
                                                                            <td>&nbsp;&nbsp;</td>                                                                          
                                                                            <td>&nbsp;&nbsp;</td>                                                                      
                                                                        <td id="recurrencTypesTR" style="border: 1px solid #bec3e4; padding-left: 65px;padding-top: 15px;padding-bottom: 15px">
    <%
    
    String tdStyle = "style='border: 1px solid #bec3e4; padding-left: 10px;padding-top: 5px;padding-bottom: 5px;padding-right: 10px'";
    if ( promotion.getRecurrenceType().equals(Promotion.RECURRENCE_PATTERN.WEEKLY) ){
        String[] days = recurrence.split(",");
        List<Integer> list = new ArrayList<Integer>();
        for(String selectedDay : days){
            list.add(Integer.parseInt(selectedDay));
        }        

    %>
        <table id="DaysInWeek" name="DaysInWeek" >
            <tr>
                <% if ( list.contains(Calendar.MONDAY) ){%>
            <td <%=tdStyle%>><input type="checkbox" checked name="dayW" id="<%=Calendar.MONDAY%>" value="<%=Calendar.MONDAY%>"><%=Languages.getString("reports.dayName.Mo", SessionData.getLanguage())%></input> </td>
                <%}
                 if ( list.contains(Calendar.TUESDAY) ){%>
                <td <%=tdStyle%>><input type="checkbox" checked name="dayW" id="<%=Calendar.TUESDAY%>" value="<%=Calendar.TUESDAY%>"><%=Languages.getString("reports.dayName.Tu", SessionData.getLanguage())%></input></td>
                <%}
                 if ( list.contains(Calendar.WEDNESDAY) ){%>
                <td <%=tdStyle%>><input type="checkbox" checked name="dayW" id="<%=Calendar.WEDNESDAY%>" value="<%=Calendar.WEDNESDAY %>"><%=Languages.getString("reports.dayName.We", SessionData.getLanguage())%></input></td>
                <%}
                 if ( list.contains(Calendar.THURSDAY) ){%>
                <td <%=tdStyle%>><input type="checkbox" checked name="dayW" id="<%=Calendar.THURSDAY%>" value="<%=Calendar.THURSDAY %>"><%=Languages.getString("reports.dayName.Th", SessionData.getLanguage())%></input></td>
                <%}
                 if ( list.contains(Calendar.FRIDAY) ){%>
                <td <%=tdStyle%>><input type="checkbox" checked name="dayW" id="<%=Calendar.FRIDAY%>" value="<%=Calendar.FRIDAY %>"><%=Languages.getString("reports.dayName.Fr", SessionData.getLanguage())%></input></td>
                <%}
                 if ( list.contains(Calendar.SATURDAY) ){%>
                <td <%=tdStyle%>><input type="checkbox" checked name="dayW" id="<%=Calendar.SATURDAY%>" value="<%=Calendar.SATURDAY%>"><%=Languages.getString("reports.dayName.Sa", SessionData.getLanguage())%></input></td>
                <%}
                 if ( list.contains(Calendar.SUNDAY) ){%>
                <td <%=tdStyle%>><input type="checkbox" checked name="dayW" id="<%=Calendar.SUNDAY%>" value="<%=Calendar.SUNDAY%>"><%=Languages.getString("reports.dayName.Su", SessionData.getLanguage())%></input></td>
                <%}%>
                
            </tr>        
        </table>    
        
    <%}
    if ( promotion.getRecurrenceType().equals(Promotion.RECURRENCE_PATTERN.MONTHLY) ){
        %>
        <table id="DaysInMonth" name="DaysInMonth" >
        <%
                int dayNumber = 1;
                String[] days = recurrence.split(","); 
                for(int j=0; j<=4;j++){  
                    out.println("<tr>");
                    for(int k=0; k<=7 && dayNumber<32; k++){                                            
                        boolean isDay = false;
                        for(String selectedDay : days){
                            if (selectedDay.equals(String.valueOf(dayNumber))){
                                isDay = true;
                            }
                        }                        
                        if (isDay){
                    %>                    
                    <td <%=tdStyle%> ><input type="checkbox" checked name="dayM" id="<%=dayNumber%>" value="<%=dayNumber%>"><%=dayNumber%></input></td>                                            
                    <%             
                        } else{
                            %>                    
                                <td <%=tdStyle%> ><input type="checkbox" name="dayM" id="<%=dayNumber%>" value="<%=dayNumber%>"><%=dayNumber%></input></td>                                            
                            <%
                        }
                        dayNumber++;
                    }
                    out.println("</tr>");   
                }
%>
        </table>
        <%
    }%>
                                                                                    
    </td>
    </tr>
    <%}%>                                                                        
									<tr class="main">
									    <td nowrap widht="95%"><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>: </td>
									    <td>&nbsp;&nbsp;</td>
                                                                            <%
                                                    java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");
                                                    formatter.setLenient(false);
                                                    Calendar clStartDate = promotion.getStartDateCal();
                                                    clStartDate.add(Calendar.HOUR, promotion.gethourOffset());
                                                    clStartDate.add(Calendar.MINUTE, promotion.getMinStart());
                                                    String dtStartDate = formatter.format(clStartDate.getTime());
                                                                                                        
                                                    Calendar clEndDate = promotion.getEndDateCal();
                                                    clEndDate.add(Calendar.HOUR, promotion.gethourOffsetEnd());
                                                    clEndDate.add(Calendar.MINUTE, promotion.getMinEnd());
                                                    String dtEndDate = formatter.format(clEndDate.getTime());
                                                    
                                                                            %>
									    <td><%=dtStartDate%></td>
									<tr class="main">   
									    <td nowrap widht="95%"><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td>
									    <td>&nbsp;&nbsp;</td>
									    <td><%=dtEndDate%></td>											    
									</tr>
									<tr class="main">   
									    <td> </td>
									    <td>&nbsp;&nbsp;</td>
									    <td></td>											    
									</tr>
									<tr class="main">
										<td nowrap><%=Languages.getString("jsp.admin.tools.promoConfiguration.Entity",SessionData.getLanguage())%>:</td>
									    <td>&nbsp;&nbsp;</td>
									    <td id="demo1"></td>											    
									</tr>
							     </table> 
							     </td>
							     </tr>          
	          </table>
	          
	          
						<table width="100%">	
						<tr class="main"><br></tr>					
						<tr><td class="formAreaTitle2"><%=Languages.getString("jsp.admin.tools.editpromotions.thresholds",SessionData.getLanguage())%></td></tr> 
						<tr>
							<td class="formArea2" >
								<table width="100%">
									<tr>
										<td class="formArea2" >
											<table width="100%" cellspacing="1" cellpadding="1" border="0">     
											     <tr>
											        <td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptmintransamount",SessionData.getLanguage()).toUpperCase()%></td>
											     	<td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptvalue",SessionData.getLanguage()).toUpperCase()%></td>
											     	<td class=rowhead2 ><%=Languages.getString("jsp.admin.tools.editpromotions.ptadtext",SessionData.getLanguage()).toUpperCase()%></td>
											      </tr> 
												<%
								                  it = vecPromoThresholds.iterator();
								                  int intEvenOdd = 1;
								                  int icount=0;
								                  while (it.hasNext()) {
									                   	icount++;
									                   	PromThreshold promTemp = (PromThreshold) it.next();
									                   	out.println("<tr class=row" + intEvenOdd +">");
									                   	out.println("<td align=left>" + "$" +  promTemp.getMinAmount() + "</td>");
									                   	out.println("<td align=left>");
									                   	if (promTemp.getBonusAmount()>=0) {
									                    	out.println("$"+promTemp.getBonus());
									                    }
									                   else {
									                    	out.println(NumberUtil.formatAmount(Float.toString(promTemp.getBonusAmount()*-100))+"%");
									                    }
									               		out.println("</td>");
									                    out.println("<td >" +promTemp.getAddReceipt() +"</td>");
									                    out.println("</tr>");
									                    
									                    if (intEvenOdd == 1) {
									                      intEvenOdd = 2;
									                    } else {
									                      intEvenOdd = 1;
									                    }
									
									                }
									                icount++;

									               
								            	%>                								     
										     	</table>
										</td>
										</tr>
									</table>
										</td>
										</tr>
										
									<tr class="main">
									  
										<td align="right">
										<%if(sAction.equals("save")){ %>
											<input id="btnSubmit1" type="button" value="<%=Languages.getString("jsp.admin.tools.promoConfiguration.button",SessionData.getLanguage())%>" ONCLICK="<%="javascript:location.href='/support/admin/tools/managepromos.jsp" + "?repId=" + strRepID + "&prsID=" + strProductID + "&isosID=" + strISOID + "&carrierId=" + strCarrierID + "';"%>">
										</td>
										<%}else{%>
										<input id="btnSubmit" type="button" value="<%=Languages.getString("jsp.admin.tools.promoConfiguration.Confirm",SessionData.getLanguage())%>" ONCLICK="save();">&nbsp;&nbsp;
										<input id="btnSubmit1" type="button" value="<%=Languages.getString("jsp.admin.tools.editpromotions.cancel",SessionData.getLanguage())%>" ONCLICK="<%="javascript:location.href='/support/admin/tools/managepromos.jsp" + "?repId=" + strRepID + "&prsID=" + strProductID + "&isosID=" + strISOID + "&carrierId=" + strCarrierID + "';"%>">										
										<%}%>
										
									</tr>	
									</table>
									
			         					
           </form>
          </td>
      </tr>
    </table>
    <%if(sAction.equals("save")){ %>
<script language="javascript">
alert("Email has been sent successfuly");
</script>
<%}%>
<%@ include file="/includes/footer.jsp" %>
