<%@page import="com.debisys.utils.*,
				java.util.*,java.util.ArrayList,
				com.debisys.users.User,
				com.debisys.languages.Languages,
				com.debisys.customers.Merchant,
				com.debisys.terminals.Terminal,
				com.debisys.pincache.Pincache,
				com.debisys.pincache.PcAssignment" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
int section = 9;
int section_page = 24;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<link href="css/jquery.dataTables.css" type="text/css" rel="stylesheet">
<style>
	#add-template-form label, #add-product-form label, #edit-product-form label{font-weight: bold;}
	.pinBox {border-spacing:0; border-collapse:collapse; }
	.pinBox tr td, .pinBox tr th { border: 1px solid black; padding: 2px; }
</style>
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/support/js/manage_pins.js" type="text/javascript"></script>
<script>
	saveButtonText = "<%=Languages.getString("jsp.admin.pincache.save",SessionData.getLanguage())%>";
	cancelButtonText = "<%=Languages.getString("jsp.admin.pincache.cancel",SessionData.getLanguage())%>";
	okButtonText = "<%=Languages.getString("jsp.admin.pincache.ok",SessionData.getLanguage())%>";
	differentError = "<%=Languages.getString("jsp.admin.pincache.differentstatuserror",SessionData.getLanguage())%>";
	changeStatustext = "<%= Languages.getString("jsp.admin.pincache.changestatus",SessionData.getLanguage()) %>";
	filterText = "<%=Languages.getString("jsp.admin.pincache.report.filter",SessionData.getLanguage())%>";
</script>
<%
String site_id= request.getParameter("site_id");
if(site_id == null){
	site_id = "";
}

ArrayList<PcAssignment> pins = null;
Merchant mer = null;
Terminal t = null;
if(!site_id.equals("")){
	t = new Terminal();
	t.setSiteId(site_id);
	String merchant_id = t.getMerchantId(t.getSiteId());
	if(merchant_id != null && !merchant_id.equals("")){
		mer = new Merchant();
		mer.setMerchantId(merchant_id);
		mer.getMerchant(SessionData,application);
		t.setMerchantId(mer.getMerchantId());
		t.load(SessionData, application);
		
		//m.setPinCacheEnabled(true);
		//t.setTerminalId(t.getTerminalIdBySite(t.getSiteId()));
		pins = PcAssignment.getPINsBySiteId(site_id);
	}
}
%>
<br>
<h4><%=Languages.getString("jsp.admin.pincache.tool",SessionData.getLanguage())%></h4>
<div class="ui-widget" id="searchcriteria">
	<div class="ui-widget-header ui-corner-top"><strong>&nbsp;</strong></div>
	<div class="ui-widget-content ui-corner-bottom">
		<p>
			<form action="/support/admin/pincache/manage_pins.jsp" method="get">
				<label for="site_id"><%=Languages.getString("jsp.admin.pincache.reportsync.header1",SessionData.getLanguage())%></label>
				<input name="site_id" type="text" value="<%=site_id%>" class="text ui-widget-content ui-corner-all" >
				<br>
				<div style="text-align: right">
					<button id="searchBySiteIdBtn"><%=Languages.getString("jsp.admin.pincache.search",SessionData.getLanguage())%></button>
				</div>
			</form>
		</p>
	</div>
</div>
<br />
<br />
<div class="productBox">
<%
if(!site_id.equals("")){
	if (t != null && mer != null && Terminal.IsTerminalInMBList(application, t.getTerminalType()) && mer.isPinCacheEnabled()) {
		if(pins != null && !pins.isEmpty()){
%>
		<label for="rateplan_id"><%=Languages.getString("jsp.admin.pincache.totalpines",SessionData.getLanguage())%>:</label>
		<span><%=pins.size()%></span><br>						
		<div>
			<button class="selectAllStatus2"><%= Languages.getString("jsp.admin.pincache.selectAllStatus2",SessionData.getLanguage()) %></button>
			<button class="selectAllStatus5"><%= Languages.getString("jsp.admin.pincache.selectAllStatus5",SessionData.getLanguage()) %></button>
			<button class="selectAllStatus8"><%= Languages.getString("jsp.admin.pincache.selectAllStatus8",SessionData.getLanguage()) %></button>
			<button class="changeStatusSelected"><%= Languages.getString("jsp.admin.pincache.changestatusselected",SessionData.getLanguage()) %></button>
		</div>
		<table id="pinTable">
			<thead>
				<tr>
					<th><input type="checkbox" id="allpins" name="allpins"></th>
					<th><%=Languages.getString("jsp.admin.pincache.reportpins.details.header1",SessionData.getLanguage())%></th>
					<th><%=Languages.getString("jsp.admin.pincache.reportpins.details.header2",SessionData.getLanguage())%></th>
					<th><%=Languages.getString("jsp.admin.pincache.reportpins.details.header3",SessionData.getLanguage())%></th>
					<th><%=Languages.getString("jsp.admin.pincache.reportpins.details.header4",SessionData.getLanguage())%></th>
					<th><%=Languages.getString("jsp.admin.pincache.reportpins.details.header5",SessionData.getLanguage())%></th>
					<th><%=Languages.getString("jsp.admin.pincache.reportpins.details.header6",SessionData.getLanguage())%></th>
					<th><%=Languages.getString("jsp.admin.pincache.reportpins.details.header7",SessionData.getLanguage())%></th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
<%
			for(PcAssignment p: pins){
%>					
				<tr data-pcasid="<%=p.getId()%>" data-status="<%=p.getPinStatusId()%>">
					<td><input type="checkbox" id="pin_<%=p.getId()%>" name="pin_<%=p.getId()%>"></td>
					<td><%=p.getDescription()%></td>
					<td><%=p.getSequence_no()%></td>
					<td><%=p.getControl_no()%></td>
					<td><%=p.getCarrier_control_no()%></td>
					<td align="right"><%=Pincache.formatCurrency(String.valueOf(p.getAmount()))%></td>
					<td><%=p.getPin()%></td>
					<td><%=Languages.getString("jsp.admin.pincache.status"+p.getPinStatusId(),SessionData.getLanguage())%></td>
					<td>
<%
				if(p.getPinStatusId() == 2 || p.getPinStatusId() == 5 || p.getPinStatusId() == 8){
%>
						<button class="changeStatus"><%= Languages.getString("jsp.admin.pincache.changestatus",SessionData.getLanguage()) %></button>
<%
				}
%>
					</td>
				</tr>
			
<%
			}
%>
			</tbody>
		</table>
		<div style="clear: both; float: none;"></div>
		<div>
			<button class="selectAllStatus2"><%= Languages.getString("jsp.admin.pincache.selectAllStatus2",SessionData.getLanguage()) %></button>
			<button class="selectAllStatus5"><%= Languages.getString("jsp.admin.pincache.selectAllStatus5",SessionData.getLanguage()) %></button>
			<button class="selectAllStatus8"><%= Languages.getString("jsp.admin.pincache.selectAllStatus8",SessionData.getLanguage()) %></button>
			<button class="changeStatusSelected"><%= Languages.getString("jsp.admin.pincache.changestatusselected",SessionData.getLanguage()) %></button>
		</div>
		<div id="changeDialog">
			<label id="noteTxtLbl"><%= Languages.getString("jsp.admin.pincache.changereason",SessionData.getLanguage()) %></label><br />
			<textarea id="noteTxt" name="noteTxt" style="width:100%"></textarea>
			<br /><hr><br />
			<button id="pinToStatus5"><%= Languages.getString("jsp.admin.pincache.toStatus5",SessionData.getLanguage()) %></button>
			<br /><br />
			<button id="pinToStatus3"><%= Languages.getString("jsp.admin.pincache.toStatus3",SessionData.getLanguage()) %></button>
		</div>
		<div id="customMessage"></div>
<%
		}else{
			%>
			<div class="warning"><%=Languages.getString("jsp.admin.pincache.noassignments",SessionData.getLanguage())%></div>
	<%
		}
	}else{
%>
		<div class="warning"><%=Languages.getString("jsp.admin.pincache.noaccess",SessionData.getLanguage())%></div>
<%
	
	}
}else{
%>
		<div class="warning"><%=Languages.getString("jsp.admin.pincache.noassignments",SessionData.getLanguage())%></div>
<%
}
%>
</div>