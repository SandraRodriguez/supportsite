<%@page import="com.debisys.utils.*,
				java.util.*,java.util.ArrayList,
				com.debisys.users.User,
				com.debisys.languages.Languages,
				com.debisys.pincache.Pincache,
				com.debisys.pincache.PcAssignmentTemplate,
				com.debisys.pincache.PcAssignmentTemplateProduct" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%
int section = 9;
int section_page = 23;
%>
<%@ include file="/includes/security.jsp" %>
<%
String template_id= request.getParameter("template_id");
PcAssignmentTemplate t = PcAssignmentTemplate.getById(template_id);
%>
<style>
#add-template-form label, #add-product-form label, #edit-product-form label{
	font-weight: bold;
}
</style>
<h3><%=t.getName()%></h3>
<div class="templateBox">
	<h4><%=t.getDescription()%></h4>
	<p>
		<label for="rateplan_id"><%=Languages.getString("jsp.admin.pincache.totalpines",SessionData.getLanguage())%>:</label>
		<span><%=t.getTotalPines()%></span><br>						
<%
if(t.getProducts() != null && !t.getProducts().isEmpty()){
%>
		<table class="productBox">
			<tr>
				<th><%=Languages.getString("jsp.admin.pincache.product",SessionData.getLanguage())%></th>
				<th><%=Languages.getString("jsp.admin.pincache.quantity",SessionData.getLanguage())%></th>
			</tr>
<%
	for(PcAssignmentTemplateProduct p: t.getProducts()){
%>							
			<tr>
				<td id="<%=p.getTemplate_id()%>-<%=p.getProduct_id()%>-description"><%=p.getDescription()%></td>
				<td align="right" id="<%=p.getTemplate_id()%>-<%=p.getProduct_id()%>-quantity"><%=p.getQuantity()%></td>
			</tr>
<%
	}
%>
		</table>
<%
}else{
%>
		<div class="warning"><%=Languages.getString("jsp.admin.pincache.noproducts",SessionData.getLanguage())%></div>
<%
}
%>
	</p>
	<a id="managetemplates" href="admin/pincache/template.jsp"><%=Languages.getString("jsp.admin.pincache.templates", SessionData.getLanguage()) %></a>
	<script>
		$(document).ready(function(){
			$('#managetemplates').button();
		});
	</script>
</div>