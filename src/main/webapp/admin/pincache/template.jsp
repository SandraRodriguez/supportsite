<%@page import="com.debisys.utils.*,
				com.debisys.terminals.Terminal,
				com.debisys.properties.Properties,
				java.util.*,java.util.ArrayList,
				com.debisys.terminalTracking.*,
				com.debisys.users.User,com.debisys.terminals.JavaHandSet,
				com.debisys.pincache.Pincache,
				com.debisys.pincache.PcAssignmentTemplate,
				com.debisys.pincache.PcAssignmentTemplateProduct,
				com.debisys.terminals.Terminal,
				com.debisys.terminals.CarriersMobile" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<%
int section = 9;
int section_page = 23;
%>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%
Pincache.processRequest(SessionData, request, response);
ArrayList<String> errors = (ArrayList<String>)request.getAttribute("errors");
ArrayList<PcAssignmentTemplate> templates = (ArrayList<PcAssignmentTemplate>)request.getAttribute("templates");
Vector<Vector<String>> reps = (Vector<Vector<String>>)request.getAttribute("reps");
%>
<style>
#add-template-form label, #add-product-form label, #edit-product-form label, .templateBox label{
	font-weight: bold;
}
</style>
<script src="includes/jquery.numeric.js" type="text/javascript"></script>
<script>
var saveButtonText = '<%=Languages.getString("jsp.admin.pincache.save",SessionData.getLanguage())%>';
var cancelButtonText = '<%=Languages.getString("jsp.admin.pincache.cancel",SessionData.getLanguage())%>';
var okButtonText = '<%=Languages.getString("jsp.admin.pincache.ok",SessionData.getLanguage())%>';
</script>
<script src="js/pincache.js" type="text/javascript"></script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.pincache.templates",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
</table>
<%
if(errors != null && !errors.isEmpty()){
	for(String error:errors){
%>
	<div class="error"><%=error%></div>
<%
	}
}
%>
<p align="right">
<%
if(templates != null && !templates.isEmpty()){
%>
	<button id="collapse-all-templates"><%=Languages.getString("jsp.admin.pincache.collapseall",SessionData.getLanguage())%></button>
	<button id="expand-all-templates"><%=Languages.getString("jsp.admin.pincache.expandall",SessionData.getLanguage())%></button>
<%
}
%>

	<button id="add-template"><%=Languages.getString("jsp.admin.pincache.createtemplate",SessionData.getLanguage())%></button>
</p>
<div class="accordion">
<%
if(templates != null && !templates.isEmpty()){
	for(PcAssignmentTemplate t: templates){
%>
				<h3><a href="#"><%=t.getName()%></a></h3>
				<div class="templateBox">
					<div class="actions">
						<button class="delete-template" data-templateid="<%=t.getId()%>"><%=Languages.getString("jsp.admin.pincache.deletetemplate",SessionData.getLanguage())%></button>
						<button class="add-product" data-templateid="<%=t.getId()%>" data-rateplanid="<%=t.getRatePlanId()%>"><%=Languages.getString("jsp.admin.pincache.addproduct",SessionData.getLanguage())%></button>
					</div>
					<label><%=Languages.getString("jsp.admin.pincache.description",SessionData.getLanguage())%></label>
					<span><%=t.getDescription()%></span>
					<br>
					<label><%=Languages.getString("jsp.admin.pincache.rateplan",SessionData.getLanguage())%></label>
					<span><%=t.getRatePlanName()%></span>
					<br>
					<label for="rateplan_id"><%=Languages.getString("jsp.admin.pincache.totalpines",SessionData.getLanguage())%>:</label>
					<span class="totalPines"><%=t.getTotalPines()%></span><br>
<%
		if(t.getProducts() != null && !t.getProducts().isEmpty()){
%>
					<table class="productBox">
						<tr>
							<th><%=Languages.getString("jsp.admin.pincache.product",SessionData.getLanguage())%></th>
							<th><%=Languages.getString("jsp.admin.pincache.quantity",SessionData.getLanguage())%></th>
							<th>&nbsp;</th>
						</tr>
<%
				for(PcAssignmentTemplateProduct p: t.getProducts()){
%>							
						<tr>
							<td id="<%=p.getTemplate_id()%>-<%=p.getProduct_id()%>-description"><%=p.getDescription()%></td>
							<td id="<%=p.getTemplate_id()%>-<%=p.getProduct_id()%>-quantity"><%=p.getQuantity()%></td>
							<td align="right">
								<button class="edit-product" data-templateid="<%=p.getTemplate_id()%>" data-productid="<%=p.getProduct_id()%>">
									<%=Languages.getString("jsp.admin.pincache.editproduct",SessionData.getLanguage())%>
								</button>
								<button class="delete-product" data-templateid="<%=p.getTemplate_id()%>" data-productid="<%=p.getProduct_id()%>">
									<%=Languages.getString("jsp.admin.pincache.deleteproduct",SessionData.getLanguage())%>
								</button>
							</td>
						</tr>
<%
				}
%>
					</table>
<%
			}else{
%>
					<div class="warning"><%=Languages.getString("jsp.admin.pincache.noproducts",SessionData.getLanguage())%></div>
<%
			}
%>
				</div>
<%
		}
	}else{
%>
			<div class="warning"><%=Languages.getString("jsp.admin.pincache.notemplates",SessionData.getLanguage())%></div>
<%
	}
%>
</div>
<div id="add-template-form" title="<%=Languages.getString("jsp.admin.pincache.newtemplate",SessionData.getLanguage())%>">
	<p class="validateTips"></p>
	<form id="template-form" method="post" action="admin/pincache/template.jsp">
		<input type="hidden" name="action" value="save_template" />
		<label for="rateplan_id"><%=Languages.getString("jsp.admin.pincache.rep",SessionData.getLanguage())%></label>
		<br>
		<select id="representative_id" name="representative_id" class="select ui-widget-content ui-corner-all">
			<option value=""></option>
<%
	for(Vector<String> r: reps){
%>
				<option value="<%=r.get(0)%>"><%=r.get(1)%></option>
<%
	}
%>
		</select>
		<br>
		<label for="rateplan_id"><%=Languages.getString("jsp.admin.pincache.rateplan",SessionData.getLanguage())%></label>
		<br>
		<select id="rateplan_id" name="rateplan_id" class="select ui-widget-content ui-corner-all"></select>
		<br>
		<label for="name"><%=Languages.getString("jsp.admin.pincache.name",SessionData.getLanguage())%></label>
		<br>
		<input type="text" maxlength="100" name="name" class="text ui-widget-content ui-corner-all" />
		<br>
		<label for="description"><%=Languages.getString("jsp.admin.pincache.description",SessionData.getLanguage())%></label>
		<br>
		<textarea maxlength="500" name="description" class="text ui-widget-content ui-corner-all"></textarea>
	</form>
</div>
<div id="add-product-form" title="<%=Languages.getString("jsp.admin.pincache.newproduct",SessionData.getLanguage())%>">
	<p class="validateTips"></p>
	<form id="a-product-form" method="post" action="admin/pincache/template.jsp">
		<input type="hidden" name="action" value="save_product" />
		<input type="hidden" id="a-template_id" name="template_id" value="" />
		<label for="product_id"><%=Languages.getString("jsp.admin.pincache.product",SessionData.getLanguage())%></label>
		<br>
		<select id="a-product_id" name="product_id" class="select ui-widget-content ui-corner-all"></select>
		<br>
		<label for="quantity"><%=Languages.getString("jsp.admin.pincache.quantity",SessionData.getLanguage())%></label>
		<br>
		<input id="a-quantity" maxlength="3" name="quantity"/>
		<br>
	</form>
</div>
<div id="edit-product-form" title="<%=Languages.getString("jsp.admin.pincache.editproduct",SessionData.getLanguage())%>">
	<p class="validateTips"></p>
	<form id="e-product-form" method="post" action="admin/pincache/template.jsp">
		<input type="hidden" name="action" value="edit_product" />
		<input type="hidden" id="e-template_id" name="template_id" value="" />
		<label for="product_id"><%=Languages.getString("jsp.admin.pincache.product",SessionData.getLanguage())%></label>
		<br>
		<input type="hidden" id="e-product_id" name="product_id" value="" />
		<span id="e-product_id_label"></span>
		<br>
		<label for="quantity"><%=Languages.getString("jsp.admin.pincache.quantity",SessionData.getLanguage())%></label>
		<br>
		<input id="e-quantity" maxlength="3" name="quantity"/>
		<br>
	</form>
</div>
<div id="confirmDialog">
	<div class="warning">
		<%=Languages.getString("jsp.admin.pincache.confirmremove",SessionData.getLanguage())%>
	</div>
</div>
<div id="customMessage"></div>
<%@ include file="/includes/footer.jsp" %>
