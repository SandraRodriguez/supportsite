<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.languages.Languages" %>
<%
int section=8;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String strUrlParams = "?startDate=" + URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8");
String strAction = request.getParameter("action");

if (request.getParameter("action") != null)
{

  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  if (TransactionSearch.validateDateRange(SessionData))
  {
    SessionData.setProperty("start_date", request.getParameter("startDate"));
    SessionData.setProperty("end_date", request.getParameter("endDate"));

    if (strAction == null) strAction="";

    if (strAction.equals("iso"))
    {
      vecSearchResults = TransactionSearch.getTransactions(intPage, intPageSize, SessionData,1);
      strUrlParams = strUrlParams + "&action=iso";
    }
    else if (strAction.equals("agent"))
    {
      vecSearchResults = TransactionSearch.getTransactions(intPage, intPageSize, SessionData,2);
      strUrlParams = strUrlParams + "&action=agent";
    }
    else if (strAction.equals("subagent"))
    {
      vecSearchResults = TransactionSearch.getTransactions(intPage, intPageSize, SessionData,3);
      strUrlParams = strUrlParams + "&action=subagent";
    }
    else if (strAction.equals("rep"))
    {
      vecSearchResults = TransactionSearch.getTransactions(intPage, intPageSize, SessionData,4);
      strUrlParams = strUrlParams + "&action=rep";
    }
    else if (strAction.equals("merchant"))
    {
      vecSearchResults = TransactionSearch.getTransactions(intPage, intPageSize, SessionData,5);
      strUrlParams = strUrlParams + "&action=merchant";
    }

    intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize) + 1;
      if ((intPageCount * intPageSize) + 1 >= intRecordCount)
      {
        intPageCount++;
      }
    }
  }
  else
  {
   searchErrors = TransactionSearch.getErrors();
  }


}
%>
<html>
  <head>
    <link href="../../default.css" type="text/css" rel="stylesheet">
    <title>Debisys</title>
</head>
<body bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" width="700" background="../../images/top_blue.gif">
	<tr>
    <td width="18" height="20" align="left"><img src="../../images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.ach.ach_transactions.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20" align="right"><img src="../../images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td class="main">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2" class="main">
<table width=400>
<%
if (searchErrors != null)
{
  out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
Enumeration enum1=searchErrors.keys();
while(enum1.hasMoreElements())
{
  String strKey = enum1.nextElement().toString();
  String strError = (String) searchErrors.get(strKey);
  out.println("<li>" + strError);
}

  out.println("</font></td></tr>");
}
%>
</table>
               </td>
              </tr>
            </table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
              if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals(""))
                {
                   out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount-1)},SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
              if (intPage > 1)
              {
                out.println("<a href=\"ach_transactions.jsp" + strUrlParams +"&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\"ach_transactions.jsp" + strUrlParams +"&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }
              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"ach_transactions.jsp" + strUrlParams + "&page=" + i + "\">" + i + "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"ach_transactions.jsp" + strUrlParams + "&page=" + (intPage+1) + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
                out.println("<a href=\"ach_transactions.jsp" + strUrlParams + "&page=" + (intPageCount-1) + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
              }

              %>
              </td>
            </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="2">
            <tr>
              <td class=rowhead2>#</td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.date",SessionData.getLanguage()).toUpperCase()%></td>
              <%
              if (strAction.equals("merchant"))
              {
                out.println("<td class=rowhead2>"+Languages.getString("jsp.admin.merchant",SessionData.getLanguage()).toUpperCase()+"</td>");
              }
              else if (strAction.equals("agent"))
              {
                out.println("<td class=rowhead2>"+Languages.getString("jsp.admin.agent",SessionData.getLanguage()).toUpperCase()+"</td>");
              }
              else if (strAction.equals("subagent"))
              {
                out.println("<td class=rowhead2>"+Languages.getString("jsp.admin.subagent",SessionData.getLanguage()).toUpperCase()+"</td>");
              }
              else if (strAction.equals("rep"))
              {
                out.println("<td class=rowhead2>"+Languages.getString("jsp.admin.rep",SessionData.getLanguage()).toUpperCase()+"</td>");
              }

              %>
              <td class=rowhead2><%=Languages.getString("jsp.admin.description",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.type",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2><%=Languages.getString("jsp.admin.amount",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);
                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td>" + vecTemp.get(1)+ "</td>");
                                if (!strAction.equals("iso"))
                                {
                                  out.println("<td>"+vecTemp.get(8)+"</td>");
                                }
                    out.println("<td>" + vecTemp.get(2) + "</td>" +
                                "<td>" + vecTemp.get(3) + "</td>" +
                                "<td>" + vecTemp.get(4) + "</td>" +
                        "</tr>");
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>

<%
}
else if (intRecordCount==0 && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.ach.ach_transactions.not_found",SessionData.getLanguage())+"</font>");
}
%>

</td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
