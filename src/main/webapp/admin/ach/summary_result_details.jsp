<%@ page import="java.net.*,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 java.io.*, 
                 java.text.*,
                 com.debisys.languages.Languages,
                 com.debisys.utils.NumberUtil,
                 com.debisys.ach.TransactionSearch" %>

<%
	int section=8;
	int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%
//Vector    vecTransactionType = new Vector();
Hashtable<String, Vector<Vector<String>>> htSearchResults = new Hashtable<String, Vector<Vector<String>>>();
String strDownload = ""; 
String strMerchantID = ""; 
String strStartDate = ""; 
String strEndDate = ""; 
String statementDate = "";
String strAmount = ""; 
String strDescription = "";
String strEntityName = "";
String strTotalACH = "";
String strTotalAmount = "";
String strTotalAmountPos = "";
String strTotalAmountNeg = "";
String strStartDateNow = ""; 
String strTransType = "";
String strViewtype = "";
double dblTotalACH = 0;
double dblTotalAmount = 0;
double dblTotalAmountPos = 0;
double dblTotalAmountNeg = 0;
String strIsoName = TransactionSearch.getIsoName(SessionData);
String strHrefTemp = request.getRequestURL().toString();
String strBaseHref = strHrefTemp.substring(0,strHrefTemp.indexOf("/support/") + 9);

	try
    {
    	if (request.getParameter("DownloadPage") != null)
    	{
    		strDownload = request.getParameter("MerchantID");
    	    response.setContentType("application/x-download");
    	    response.setHeader("Content-Disposition", "attachment; filename=ACHStatement.html");
    	}
    	if (request.getParameter("MerchantID") != null)
    	{
    		strMerchantID = request.getParameter("MerchantID");
    	}    	
        if (request.getParameter("startDate") != null) 
        {
           strStartDate = request.getParameter("startDate");
        }
        if (request.getParameter("endDate") != null) 
        {
           strEndDate = request.getParameter("endDate");
        }
    	if (request.getParameter("statementDate") != null) 
    	{
    	   statementDate = request.getParameter("statementDate");
    	}    	
    	if (request.getParameter("Amount") != null)
    	{
    		strAmount = request.getParameter("Amount");
    	}
    	if (request.getParameter("Description") != null)
    	{
    		strDescription = request.getParameter("Description");
    	}
    	if (request.getParameter("EntityName") != null)
    	{
    		strEntityName = request.getParameter("EntityName");
    	}    	
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        Calendar currentDate = Calendar.getInstance();
        strStartDateNow = dateFormat.format(currentDate.getTime());

	}
	catch (Exception e){}  
%>
<HTML>
	<HEAD>
		<BASE HREF="<%=strBaseHref%>">
		<LINK HREF="default.css" TYPE="text/css" REL="stylesheet">
		<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    	<TITLE><%=Languages.getString("jsp.admin.ach.summary.result.details.title",SessionData.getLanguage())%></TITLE>
	</HEAD>
	<BODY BGCOLOR="#ffffff">
	<table border="0" cellpadding="0" cellspacing="0" width="750" align="left">
	<tr>
		<td background="images/top_blue.gif" width="5000" class="formAreaTitle"><b><%=Languages.getString("jsp.admin.ach.summary.result.details.title",SessionData.getLanguage()).toUpperCase()%></b></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td><br></td>
	</tr>
	<tr>
		<td colspan="3" class="main"><b><%=Languages.getString("jsp.admin.ach.summary.result.details.header_date",SessionData.getLanguage()) + " "%></b><%=strStartDateNow%></td>
	</tr>
	<tr>
		<td><br></td>
	</tr>			
	<tr>
		<td colspan="3" align="center" style="font-family: Verdana, Arial, Helvetica, sans-serif;font-size:12pt;"><b><%=strIsoName%></b></td>
	</tr>
	<tr>
		<td><br></td>
	</tr>	
	<tr>
		<td colspan="3" class="main">
			<b><%=Languages.getString("jsp.admin.ach.summary.result.details.dear",SessionData.getLanguage())+ " " + strEntityName%></b>
		</td>
	</tr>
	<tr>
		<td><br></td>
	</tr>		
	<tr>
		<td colspan="3" class="main">
			<%=Languages.getString("jsp.admin.ach.summary.result.details.header",SessionData.getLanguage()) + " " + strAmount + " " + Languages.getString("jsp.admin.ach.summary.result.details.header_for",SessionData.getLanguage()) + " " + strDescription%>
		</td>
	</tr>
	<tr>
		<td><br></td>
	</tr>	

<%
	try
	{
	   htSearchResults = TransactionSearch.getReportDetailACH(strMerchantID, strDescription, statementDate, strStartDate, strEndDate); 
	}
	catch (Exception e){}  
	if (htSearchResults != null && !htSearchResults.isEmpty())
	{
	   for (String key : htSearchResults.keySet()) 
	   {
	       String[] keySplit = key.split("_");
	       strViewtype = keySplit[0];
	       strTransType = keySplit[1];	       	       
%>
    <tr>
        <td colspan="2" class="main">
            <u>
                <%
                    if((strViewtype.equals("11"))||(strViewtype.equals("12")))
                    {
                        out.println(Languages.getString("jsp.admin.ach.summary.result.details.details_title",SessionData.getLanguage()));
                    }
                    else
                    {
                        out.println(strTransType);
                    }
                %>
            </u>
        </td>
    </tr>
	<tr>
        <td colspan="3">
            <table border="0" cellpadding="2" cellspacing="0" width="5000" align="center">
            <tr>                        
                <td class="main"><br>
                <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
                <table cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t<%=key%>">
                <thead>
                <tr class="SectionTopBorder">
                    <td class=rowhead2>#</td>
                    <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.date_time",SessionData.getLanguage()).toUpperCase()%></td>
                    <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.name",SessionData.getLanguage()).toUpperCase()%></td>
                    <%
                        if (! (strViewtype.equals("11") || strViewtype.equals("12")) )
                        {
                    %>                     
                    <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.transactionId",SessionData.getLanguage()).toUpperCase()%></td>
                    <%
                    	}
                     %>
                    <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.sku",SessionData.getLanguage()).toUpperCase()%></td>
                    <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.product_desc",SessionData.getLanguage()).toUpperCase()%></td>                             
                    <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.amount",SessionData.getLanguage()).toUpperCase()%></td>
                    <%
                        if (! (strViewtype.equals("11") || strViewtype.equals("12")) )
                        {
                    %>  
                        <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.discount",SessionData.getLanguage()).toUpperCase()%></td>
                        <td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.details.grid.net",SessionData.getLanguage()).toUpperCase()%></td>                          
                    <% 
                        }
                    %>                          
                </tr>
                </thead>
<%
            Vector<Vector<String>> entriesPerViewType = htSearchResults.get(key);
            int intCounter = 1;
            int intEvenOdd = 1;
   		    for (Vector<String> vecTemp : entriesPerViewType)
   		    {	   		
   		       String formattedAmt = NumberUtil.formatCurrency(vecTemp.get(6), true);
   		       String formattedACHAmt = NumberUtil.formatCurrency(vecTemp.get(8), true);
   		       String formattedDiscount = NumberUtil.formatPercentage(vecTemp.get(7));
   	   		   if((strViewtype.equals("11"))||(strViewtype.equals("12")))
		   	   {
           		   out.println("<tr class=row" + intEvenOdd +">" +
            		"<td>" + intCounter++ + "</td>" +
            		"<td>" + vecTemp.get(2)+ "</td>" +
            		"<td>" + vecTemp.get(3) + "</td>" +
            		"<td>" + vecTemp.get(4) + "</td>" +
            		"<td>" + vecTemp.get(5) + "</td>" +
            		"<td>" + formattedACHAmt + "</td>" +
           			"</tr>");                    	   			
		   	    }
                else
                {
                    out.println("<tr class=row" + intEvenOdd +">" +
              		    "<td>" + intCounter++ + "</td>" +
              		    "<td>" + vecTemp.get(2)+ "</td>" +
              		    "<td>" + vecTemp.get(3) + "</td>" +
              		    "<td>" + vecTemp.get(12) + "</td>" +
              		    "<td>" + vecTemp.get(4) + "</td>" +
              		    "<td>" + vecTemp.get(5) + "</td>" +
              		    "<td>" + formattedAmt + "</td>" +
              		    "<td>" + formattedDiscount + "</td>" +
              		    "<td>" + formattedACHAmt + "</td>" +
             			"</tr>");
		   	     }
	      		if (intEvenOdd == 1)
	      		{
	 	      		intEvenOdd = 2;
	   		    }	
	      		else
	     		{
	        		intEvenOdd = 1;
	     		}
	      		dblTotalACH = dblTotalACH + java.lang.Double.parseDouble(vecTemp.get(8).toString()); 
	      		double dblTotalAmountTemp = java.lang.Double.parseDouble(vecTemp.get(6).toString()); 
	      		dblTotalAmount += dblTotalAmountTemp; 
	      		if(dblTotalAmountTemp >= 0)
	      		{
	      			dblTotalAmountPos += java.lang.Double.parseDouble(vecTemp.get(6).toString()); 
	      		}
	      		else
	      		{
	      			dblTotalAmountNeg += java.lang.Double.parseDouble(vecTemp.get(6).toString()); 
	      		} 
   		   }
   		       %>                                          
                      </table>  
                <br>
                <br>
                <SCRIPT type="text/javascript">
                    <!--
                            var sTable = new SortROC(document.getElementById("t<%=key%>"),
                        ["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"],0,false,false);
                        -->
                </SCRIPT>                       
                </td>
            </tr>
<%
		    strTotalACH = NumberUtil.formatCurrency(Double.toString(dblTotalACH), true);
		    strTotalAmount = NumberUtil.formatCurrency(Double.toString(dblTotalAmount), true);
		    strTotalAmountPos = NumberUtil.formatCurrency(Double.toString(dblTotalAmountPos));
		    strTotalAmountNeg = NumberUtil.formatCurrency(Double.toString(dblTotalAmountNeg), true);
		    // must reset for every viewtype/transtype group totals
		    dblTotalACH = 0.0D;
		    dblTotalAmount = 0.0D;
		    dblTotalAmountPos = 0.0D;
		    dblTotalAmountNeg = 0.0D;
		    if((strViewtype.equals("11"))||(strViewtype.equals("12")))
		    {
%>      
                    <tr>
                        <td class="main">
                            *<b><%=Languages.getString("jsp.admin.ach.summary.result.details.total_amount",SessionData.getLanguage())+ " " + strTotalACH%></b>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="main">
                            <%=Languages.getString("jsp.admin.ach.summary.result.details.negative_info",SessionData.getLanguage())%>
                        </td>                       
                    </tr>                   
<% 
		    }
		    else
		    {
%>      
                    <tr>
                        <td class="main">
                            <b><%=Languages.getString("jsp.admin.ach.summary.result.details.total_transaction",SessionData.getLanguage())+ " " + strTotalAmount%></b>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="main">
                            <%=Languages.getString("jsp.admin.ach.summary.result.details.total_debit",SessionData.getLanguage())+ " " + strTotalAmountPos%>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="main">
                            <%=Languages.getString("jsp.admin.ach.summary.result.details.total_credit",SessionData.getLanguage())+ " " + strTotalAmountNeg%>
                        </td>                       
                    </tr>                                       
                    <tr>
                        <td><br><br></td>
                    </tr>
                    <tr>
                        <td class="main">
                            *<b><%=Languages.getString("jsp.admin.ach.summary.result.details.total_net",SessionData.getLanguage())+ " " + strTotalACH%></b>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="main">
                            <%=Languages.getString("jsp.admin.ach.summary.result.details.negative_info",SessionData.getLanguage())%>
                        </td>                       
                    </tr>       
<%
	        }
	        
	    } // end for all entries per  viewtype/transtype 
%>				
				</table>         			
		</td>
	</tr>					
<% 
	}
%>	
	<tr>
		<td><br><br></td>
	</tr>					
	<tr>
		<td class="main" colspan="3">
			<%=Languages.getString("jsp.admin.ach.summary.result.details.footer1",SessionData.getLanguage())%>
		</td>
	</tr>
	<tr>
		<td><br><br></td>
	</tr>	
	<tr>
		<td class="main" colspan="3">
			<%=Languages.getString("jsp.admin.ach.summary.result.details.footer2",SessionData.getLanguage())%>
		</td>
	</tr>
	<tr>
		<td class="main" colspan="3">
			<%=Languages.getString("jsp.admin.ach.summary.result.details.footer3",SessionData.getLanguage())%>
		</td>
	</tr>
	<tr>
		<td><br><br></td>
	</tr>		
	<tr>
	<td>
		


<% 
if(strDownload.equals(""))
{
%>
	<div id="divPrintButton">
		<script>
		function PrintReport()
		{
			var divButton = document.getElementById("divPrintButton");
			divButton.style.display = "none";
			window.print();
			divButton.style.display = "inline";
		}
		function DownloadReport()
		{
			var sURL = "/support/admin/ach/summary_result_details.jsp?MerchantID=<%=strMerchantID%>&statementDate=<%=statementDate%>&Amount=<%=strAmount%>&Description=<%=strDescription%>&EntityName=" + encodeURIComponent("<%=strEntityName%>") + "&DownloadPage=Download&startDate=<%=strStartDate%>&endDate=<%=strEndDate%>";
	  		var sOptions = "left=" + (screen.width - (screen.width * 0.8))/2 + ",top=" + (screen.height - (screen.height * 0.4))/2 + ",width=" + (screen.width * 0.8) + ",height=" + (screen.height * 0.5) + ",location=no,menubar=no,resizable=yes,scrollbars=yes";
  			var w = window.open(sURL, "ACHTransactionDetail", sOptions, true);
  			w.focus();
		}		
		</script>
		<input type=button value="<%=Languages.getString("jsp.admin.ach.summary.result.details.print",SessionData.getLanguage())%>" onclick="PrintReport();">
		<input type=button value="<%=Languages.getString("jsp.admin.ach.summary.result.details.download",SessionData.getLanguage())%>" onclick="DownloadReport();">
	</div>
<% 
}
%>	
</td>
	</tr>		
</table>
</BODY>
</HTML>

