<%@ page import="java.net.URLEncoder,  
                 com.debisys.utils.HTMLEncoder,
                 java.util.*, 
                 com.debisys.ach.TransactionSearch, com.debisys.utils.DateUtil, com.debisys.utils.NumberUtil" %>

<%
	/*int section=15;
	int section_page=1;*/
	
	int section=8;
	
	int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>
<%
Vector<Vector<String>>    vecSearchResults = new Vector<Vector<String>>();
String strIsoID = "";
String strAgentID = "";
String strSubAgentID = "";
String strRepID = "";
String strMerchantID = "";
String strStartDate = "";
String strEndDate = "";
String strErrorDate = "";
//for response only
String strStatementDate = "";

if (request.getParameter("search") != null)
{
	try
    {
    	if (request.getParameter("IsoID") != null)
    	{
    		strIsoID = request.getParameter("IsoID");
    	}
    	if (request.getParameter("AgentID") != null)
    	{
    		strAgentID = request.getParameter("AgentID");
    	}
    	if (request.getParameter("SubAgentID") != null)
    	{
    		strSubAgentID = request.getParameter("SubAgentID");
    	}
    	if (request.getParameter("RepID") != null)
    	{
    		strRepID = request.getParameter("RepID");
    	}
    	if (request.getParameter("MerchantID") != null)
    	{
    		strMerchantID = request.getParameter("MerchantID");
    	}
    	if (request.getParameter("StartDate") != null)
    	{
    		strStartDate = request.getParameter("StartDate");
    	}
    	if (request.getParameter("EndDate") != null)
    	{
    		strEndDate = request.getParameter("EndDate");
    	}
    	strErrorDate = TransactionSearch.validateDateRange(strStartDate,strEndDate,SessionData);
    	
    	if(strErrorDate.equals(""))
    	{
			vecSearchResults = TransactionSearch.getReportACHBilling(SessionData, strIsoID, strAgentID, strSubAgentID, strRepID, strMerchantID, strStartDate, strEndDate);
    	}
	}
	catch (Exception e){}  
}	    
%>
<SCRIPT LANGUAGE="JavaScript">
	function openViewDetail(sMerchantID, sStatementDate, sAmount, sDescription, sEntityName, sStartDate, sEndDate, paymentBatchEntityId, paymentGrouping)
	{
		var sURL = "/support/admin/achBilling/summary_result_details.jsp?MerchantID=" + sMerchantID + "&statementDate=" +  sStatementDate + "&Amount=" +  sAmount + "&Description=" + sDescription + "&EntityName=" + encodeURIComponent(sEntityName) + "&startDate=" + sStartDate + "&endDate=" + sEndDate + "&paymentBatchEntityId=" + paymentBatchEntityId + "&paymentGrouping=" + paymentGrouping;
	  	var sOptions = "left=" + (screen.width - (screen.width * 0.8))/2 + ",top=" + (screen.height - (screen.height * 0.4))/2 + ",width=" + (screen.width * 0.8) + ",height=" + (screen.height * 0.5) + ",location=no,menubar=no,resizable=yes,scrollbars=yes";
  		var w = window.open(sURL, "ACHTransactionDetail", sOptions, true);
  		w.focus();
	}
</SCRIPT>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>

            <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
              <TBODY>
              <TR>
                <TD align=left width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=images/top_blue.gif width="3000">&nbsp;<B><%=Languages.getString("jsp.admin.ach.summary.title",SessionData.getLanguage()).toUpperCase()%></B></TD>
                <TD align=right width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_right_blue.gif" width=18></TD></TR>
                  <TR>
                <TD colSpan=3>
                  <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
                  bgColor=#fffcdf class="backceldas">
                    <TBODY>
                    <TR class="backceldas">
                      <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top align=middle bgColor=#ffffff>
                        <TABLE width="100%" border=0 
                        align=center cellPadding=2 cellSpacing=0 class="backceldas">
                          <TBODY>
                          <TR>
                            <TD width=18>&nbsp;</TD>
                            <TD class="main"><BR><BR>

<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>					
					<table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
					<tr>
						<td class="main"><br>
				        <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
      	      			<thead>
      	      			<tr class="SectionTopBorder">
              			<td class=rowhead2>#</td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.statement_date",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.company_type",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.company_name",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.description_period",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.ach.summary.result.amount",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.ach_status",SessionData.getLanguage()).toUpperCase()%></td>
              			<td class=rowhead2><%=Languages.getString("jsp.admin.ach.summary.result.statement_status",SessionData.getLanguage()).toUpperCase()%></td>
            			</tr>
		            	</thead>
		            	<%
		            		int intCounter = 1;
		               		Iterator<Vector<String>> it = vecSearchResults.iterator();
		               		int intEvenOdd = 1;
		               		while (it.hasNext())
		               		{
		            	   		Vector<String> vecTemp = null;
                    	   		vecTemp = it.next();
                    	   		String strLink = "";
                    	   		String formattedAmt = NumberUtil.formatCurrency(vecTemp.get(4), true);
						        strLink = "<a href=\"javascript:void(0);\" onclick=\"openViewDetail('" + vecTemp.get(5) + "','" + vecTemp.get(0) + "','" + formattedAmt + "','" + vecTemp.get(8) + "','" + vecTemp.get(2).replaceAll("'","") + "','" + vecTemp.get(6) + "','" + vecTemp.get(7) + "','" + vecTemp.get(11) + "','" + vecTemp.get(12) + "');\"><span class=\"showLink\"><span class=\"showLink\">" + vecTemp.get(8) + "</span></span></a>";
                           		out.println("<tr class=row" + intEvenOdd +">");
                           		out.println("<td>" + intCounter++ + "</td>");
                           		out.println("<td>" + DateUtil.formatDateNoTime_DB(vecTemp.get(0))+"</td>");
                           		out.println("<td>" + vecTemp.get(1) + "</td>");
                           		out.println("<td>" + vecTemp.get(2) + "</td>");
                           		out.println("<td>" + strLink + "</td>");
                           		out.println("<td align=\"right\">" + formattedAmt + "</td>");
                           		out.println("<td>" + vecTemp.get(9)+ "</td>");
                           		out.println("<td>" + vecTemp.get(10)+ "</td>");
                           		out.println("</tr>");
                           		if (intEvenOdd == 1)
                           		{
                      	      		intEvenOdd = 2;
                    	   		}	
                           		else
                          		{
                             		intEvenOdd = 1;
                          		}
	                  		}
                  	  		vecSearchResults.clear();
		            	%> 		            	
                        </table>
						<br>
						<br>
						<SCRIPT type="text/javascript">
  							var stT1 = new SortROC(document.getElementById("t1"),
  							["Number","CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "Number","CaseInsensitiveString"],0,false,false);
						</SCRIPT>	
						</td>
					   	    	        	            
        	        </tr>	
        	        </table>
        	       										
<%
}
else
{
	if(strErrorDate.equals(""))
	{
	 	out.println("<td class=main align=left><br><br><font color=ff0000>"  + Languages.getString("jsp.admin.ach.summary.not_found",SessionData.getLanguage()) + "</font><br></td>");
	}
	else
	{
		out.println("<td class=main align=left><br><br><font color=ff0000>"  + strErrorDate + "</font><br><br></td>");
	}
}
%>							
                            
                            
                   </td>
                   </tr>
                   <tr>
                        <td width="18">&nbsp;<a href="admin/achBilling/summary.jsp"><%=Languages.getString("jsp.admin.ach.summary.continue",SessionData.getLanguage())%></a></td>
                   </tr>     
                    
                    </table>
                        <div align=right class="backceldas"><font size="1"></div>
                </td>
                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	         </tr>
	         <tr>
		            <td height="1" colspan="3" bgcolor="#003082"><img src="images/trans.gif" height="1"></td>
            </tr>
            </table>
        </td>
    </tr>
</table>

<%@ include file="/includes/footer.jsp"%>
