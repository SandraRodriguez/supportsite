<%@ 
	page import="
                 java.util.*,
                 com.debisys.utils.DebisysConfigListener" 
%>
<%
	/*int section=15;
	int section_page=1;*/
	int section=16;
	
	int section_page=1;
%>
	<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
	<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
	<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ 
	include file="/includes/security.jsp" 
%>
<%
	String strIsoID = "-2";
	String strAgentID = "-2";
	String strSubAgentID = "-2";
	String strRepID = "-2";
	String strMerchantID = "-2";
	String strStartDate = "";
	String strEndDate = "";	
	int intACHRange = DebisysConfigListener.getACHRange(application) - 1;
	
	if (request.getParameter("submitted") != null)
	{
		try
	    {
		    if (request.getParameter("submitted").equals("y"))
    		{
		    	if (request.getParameter("IsoID") != null)
		    	{
		    		strIsoID = request.getParameter("IsoID");
		    	}
		    	if (request.getParameter("AgentID") != null)
		    	{
		    		strAgentID = request.getParameter("AgentID");
		    	}
		    	if (request.getParameter("SubAgentID") != null)
		    	{
		    		strSubAgentID = request.getParameter("SubAgentID");
		    	}
		    	if (request.getParameter("RepID") != null)
		    	{
		    		strRepID = request.getParameter("RepID");
		    	}
		    	if (request.getParameter("MerchantID") != null)
		    	{
		    		strMerchantID = request.getParameter("MerchantID");
		    	}
		    	if (request.getParameter("StartDate") != null)
		    	{
		    		strStartDate = request.getParameter("StartDate");
		    	}
		    	if (request.getParameter("EndDate") != null)
		    	{
		    		strEndDate = request.getParameter("EndDate");
		    	}		    	
		    }
  		}
		catch (Exception e){}  
	}		    		
%>
<%@ 
	include file="/includes/header.jsp" 
%>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.frmMain.showRep.disabled = true;
  document.frmMain.showRep.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    	<td width="18" height="20" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	    <td class="formAreaTitle" align="left" width="3000"><%=Languages.getString("jsp.admin.ach.summary.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20" align="right"><img src="images/top_right_blue.gif"></td>
		</tr>
	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
			  <tr>
			    <td>
	    			<form name="frmMain" id="frmMain" method="post" action="/support/admin/achBilling/summary.jsp" onSubmit="scroll();">
	    			<input type="hidden" id="submitted" name="submitted" value="y">	
					<input type="hidden" id="IsoID" name="IsoID" value="<%=strIsoID%>">
					<input type="hidden" id="AgentID" name="AgentID" value="<%=strAgentID%>">							
					<input type="hidden" id="SubAgentID" name="SubAgentID" value="<%=strSubAgentID%>">							
					<input type="hidden" id="RepID" name="RepID" value="<%=strRepID%>">
					<input type="hidden" id="MerchantID" name="MerchantID" value="<%=strMerchantID%>">	
					<input type="hidden" id="StartDate" name="StartDate" value="<%=strStartDate%>">							
					<input type="hidden" id="EndDate" name="EndDate" value="<%=strEndDate%>">							
											
      					<table border="0" width="100%" cellpadding="0" cellspacing="0">
     						<tr>
	        					<td class="formArea2">
	          						<table width="300">
               							<tr>
               								<td valign="top" nowrap="nowrap">
												<table width="400">
	                            		    		<% 
	                            		    		   	if (strAccessLevel.equals(DebisysConstants.ISO))
		                            		    	    {
	                            		    		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.iso",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">
        	            	            						<select id="isoList" name="isoList" onchange="getAgents();">
            	    	                    	    				<%
	            	    	                    	    				String strSelectedISO = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
	            	    	                    	    				if(strIsoID != "")
	                        	    	                    	    	{
           	        	    				    	                		if(strIsoID.equals(SessionData.getProperty("ref_id")))
               		    	                				    	    	{
           	        	    				    	                			strSelectedISO = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedISO = "";
			                	                	                		}
	                        	    	                    	    	}  
	                		                	                		else
	                		                	                		{
	                		                	                			strSelectedISO = "";
	                	        	        	                		}        	    	    	                    	    				
	            	    	                    	    				out.println("<option " + strSelectedISO + " value=" +  SessionData.getProperty("ref_id") +">" + SessionData.getProperty("company_name") + "</option>");
	                    	        	    	     				%>                                	
	    	                	        	        			</select>
            	    		    	            				<SCRIPT>
            	    		        	        					function getAgents()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;
                                                    	    			document.frmMain.submit();
                                                     				}
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		<% 
	                            		    		    }
	                            		    		   	if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT)))
		                            		    	    {
                            		    		   			if(strAccessLevel.equals(DebisysConstants.AGENT))
                            		    		   			{    	                    	    	    				
                           		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.agent",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	                            		    		   		
        	            	            						<select  id="agentList" name="agentList" onchange="getSubAgents();">
            	    	                    	    				<%
	            	    	                    	    				String strSelectedAgent = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
	            	    	                    	    				if(strAgentID != "")
	                        	    	                    	    	{
           	        	    				    	                		if(strAgentID.equals(SessionData.getProperty("ref_id")))
               		    	                				    	    	{
           	        	    				    	                			strSelectedAgent = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedAgent = "";
			                	                	                		}
	                        	    	                    	    	}  
	                		                	                		else
	                		                	                		{
	                		                	                			strSelectedAgent = "";
	                	        	        	                		}        	    	    	                    	    				
	            	    	                    	    				out.println("<option " + strSelectedAgent + " value=" +  SessionData.getProperty("ref_id") +">" + SessionData.getProperty("company_name") + "</option>");
	                    	        	    	     				%>                                	
	    	                	        	        			</select>
	    	                	        	        			</td>
	    	                	        	        			</tr>
    	                    	    	    				<% 
	                            		    		   			}
	                            		    		   			else
	                            		    		   			{
	                            		    		   				if((strAccessLevel.equals(DebisysConstants.ISO))&&(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)))
	                            		    		   				{
	                            		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.agent",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="agentList" name="agentList" onchange="getSubAgents();">
				            	    	                    	    <%
            					    	                    	    	Vector vecAgentList = TransactionSearch.getAgentList(SessionData, strIsoID);
    	    	        	    				                        Iterator itAL = vecAgentList.iterator();
            	    	                    					    	String strSelectedAL = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
			                	                	                	if(strAgentID != "")
            					    	                    	    	{
           	        	    				    	                		if(strAgentID.equals("-1"))
               		    	                				    	    	{
           	        	    				    	                			strSelectedAL = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedAL = "";
			                	                	                		}
			                	    	                    	    	} 
			    	    	                	                		else
        					                	                		{
			    	    	                	                			strSelectedAL = "";
        	                	            				    		} 
        	    	    	                    	    				out.println("<option " + strSelectedAL + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
				            	    	                    	    	while (itAL.hasNext())
	        					                	                    {
                	            				    	                	Vector vecTempAL = null;
                	            				    	                	vecTempAL = (Vector) itAL.next();
				                	                	                	if(strAgentID != "")
                					    	                    	    	{
               	        	    				    	                		if(strAgentID.equals(vecTempAL.get(0)))
                   		    	                				    	    	{
               	        	    				    	                			strSelectedAL = "selected";
				               	    	    	                    	    	}
           	    					                	                		else
				       	        	                	                		{
           	    					                	                			strSelectedAL = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedAL = "";
            	                	            				    		}                	                	                	
				                        	                		    	out.println("<option " + strSelectedAL + " value=" + vecTempAL.get(0) +">" + vecTempAL.get(1) + "</option>");
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
    	                    	    	    				<% 
	                            		    		   				}
	                            		    		   			}
	                            		    		   		%>	                            		    		   			    	                	        	        			
            	    		    	            				<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
            		                            		    	    {
            	    		    	            					%>
            	    		        	        					function getSubAgents()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
            	    		    	            					<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.AGENT))
            		                            		    	    {
            	    		    	            					%>  
            	    		    	            					function getSubAgents()
                                        	            			{
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;	                                        	            			
                                                    	    			document.frmMain.submit();
                                                     				}
            	    		    	            					<%
            		                            		    	    }
            	    		    	            					%>                                                   				
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		<% 
	                            		    		    }
        	                	        			%>  
	   	    	                	        		<% 
	                            		    		   	if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))||(strAccessLevel.equals(DebisysConstants.SUBAGENT)))
		                            		    	    {
                            		    		   			if(strAccessLevel.equals(DebisysConstants.SUBAGENT))
                            		    		   			{    	                    	    	    				
                           		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.subagent",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	                            		    		   		
        	            	            						<select id="subAgentList" name="subAgentList" onchange="getReps();">
            	    	                    	    				<%
	            	    	                    	    				String strSelectedSubAgent = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
	            	    	                    	    				if(strSubAgentID != "")
	                        	    	                    	    	{
           	        	    				    	                		if(strSubAgentID.equals(SessionData.getProperty("ref_id")))
               		    	                				    	    	{
           	        	    				    	                			strSelectedSubAgent = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedSubAgent = "";
			                	                	                		}
	                        	    	                    	    	}  
	                		                	                		else
	                		                	                		{
	                		                	                			strSelectedSubAgent = "";
	                	        	        	                		}        	    	    	                    	    				
	            	    	                    	    				out.println("<option " + strSelectedSubAgent + " value=" +  SessionData.getProperty("ref_id") +">" + SessionData.getProperty("company_name") + "</option>");
	                    	        	    	     				%>                                	
	    	                	        	        			</select>
	    	                	        	        			</td>
	    	                	        	        			</tr>
    	                    	    	    				<% 
	                            		    		   			}
	                            		    		   			else
	                            		    		   			{
	                            		    		   				if(((strAccessLevel.equals(DebisysConstants.ISO))&&(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)))||strAccessLevel.equals(DebisysConstants.AGENT))
	                            		    		   				{
	                            		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.subagent",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="subAgentList" name="subAgentList" onchange="getReps();">
				            	    	                    	    <%
            					    	                    	    	Vector vecSubAgentList = TransactionSearch.getSubAgentList(SessionData, strAgentID);
    	    	        	    				                        Iterator itSAL = vecSubAgentList.iterator();
            	    	                    					    	String strSelectedSAL = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
			                	                	                	if(strSubAgentID != "")
            					    	                    	    	{
           	        	    				    	                		if(strSubAgentID.equals("-1"))
               		    	                				    	    	{
           	        	    				    	                			strSelectedSAL = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedSAL = "";
			                	                	                		}
			                	    	                    	    	} 
			    	    	                	                		else
        					                	                		{
			    	    	                	                			strSelectedSAL = "";
        	                	            				    		} 
        	    	    	                    	    				out.println("<option " + strSelectedSAL + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
				            	    	                    	    	while (itSAL.hasNext())
	        					                	                    {
                	            				    	                	Vector vecTempSAL = null;
                	            				    	                	vecTempSAL = (Vector) itSAL.next();
				                	                	                	if(strSubAgentID != "")
                					    	                    	    	{
               	        	    				    	                		if(strSubAgentID.equals(vecTempSAL.get(0)))
                   		    	                				    	    	{
               	        	    				    	                			strSelectedSAL = "selected";
				               	    	    	                    	    	}
           	    					                	                		else
				       	        	                	                		{
           	    					                	                			strSelectedSAL = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedSAL = "";
            	                	            				    		}                	                	                	
				                        	                		    	out.println("<option " + strSelectedSAL + " value=" + vecTempSAL.get(0) +">" + vecTempSAL.get(1) + "</option>");
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
    	                    	    	    				<% 
	                            		    		   				}
	                            		    		   			}
	                            		    		   		%>	                            		    		   			    	                	        	        			
            	    		    	            				<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
            		                            		    	    {
            	    		    	            					%>            	    		    	            				
            	    		        	        					function getReps()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
            	    		    	            					<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.AGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		        	        					function getReps()
                                        	            			{
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				} 
            	    		    	            					<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.SUBAGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		    	            					function getReps()
                                        	            			{
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%
            		                            		    	    }
            	    		    	            					%>
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		<% 
	                            		    		    }
        	                	        			%>        	                	        			        	                	        			      	                	        			
	   	    	                	        		<% 
	   	    	                	        		if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))||(strAccessLevel.equals(DebisysConstants.SUBAGENT))||(strAccessLevel.equals(DebisysConstants.REP)))
		                            		    	    {
                            		    		   			if(strAccessLevel.equals(DebisysConstants.REP))
                            		    		   			{    	                    	    	    				
                           		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.rep",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	                            		    		   		
        	            	            						<select id="repList" name="repList" onchange="getMerchants();">
            	    	                    	    				<%
	            	    	                    	    				String strSelectedRep = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
	            	    	                    	    				if(strRepID != "")
	                        	    	                    	    	{
           	        	    				    	                		if(strRepID.equals(SessionData.getProperty("ref_id")))
               		    	                				    	    	{
           	        	    				    	                			strSelectedRep = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedRep = "";
			                	                	                		}
	                        	    	                    	    	}  
	                		                	                		else
	                		                	                		{
	                		                	                			strSelectedRep = "";
	                	        	        	                		}        	    	    	                    	    				
	            	    	                    	    				out.println("<option " + strSelectedRep + " value=" +  SessionData.getProperty("ref_id") +">" + SessionData.getProperty("company_name") + "</option>");
	                    	        	    	     				%>                                	
	    	                	        	        			</select>
	    	                	        	        			</td>
	    	                	        	        			</tr>
    	                    	    	    				<% 
	                            		    		   			}
	                            		    		   			else
	                            		    		   			{
	                            		    		   				if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))||(strAccessLevel.equals(DebisysConstants.SUBAGENT)))
	                            		    		   				{
	                            		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.rep",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="repList" name="repList" onchange="getMerchants();">
				            	    	                    	    <%
            					    	                    	    	Vector vecRepList = null;
				            	    	                    	    	if(strAccessLevel.equals(DebisysConstants.ISO))
				            	    	                    	    	{
				            	    	                    	    		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
				            	    	                    	    		{
				            	    	                    	    			vecRepList = TransactionSearch.getRepList(SessionData, strSubAgentID);
				            	    	                    	    		}
				            	    	                    	    		else
				            	    	                    	    		{
				            	    	                    	    			vecRepList = TransactionSearch.getRepList(SessionData, strIsoID);
				            	    	                    	    		}
				            	    	                    	    	}
				            	    	                    	    	else
			            	    	                    	    		{
				            	    	                    	    		vecRepList = TransactionSearch.getRepList(SessionData, strSubAgentID);
			            	    	                    	    		}
    	    	        	    				                        Iterator itRL = vecRepList.iterator();
            	    	                    					    	String strSelectedRL = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
			                	                	                	if(strRepID != "")
            					    	                    	    	{
           	        	    				    	                		if(strRepID.equals("-1"))
               		    	                				    	    	{
           	        	    				    	                			strSelectedRL = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedRL = "";
			                	                	                		}
			                	    	                    	    	} 
			    	    	                	                		else
        					                	                		{
			    	    	                	                			strSelectedRL = "";
        	                	            				    		} 
        	    	    	                    	    				out.println("<option " + strSelectedRL + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
				            	    	                    	    	while (itRL.hasNext())
	        					                	                    {
                	            				    	                	Vector vecTempRL = null;
                	            				    	                	vecTempRL = (Vector) itRL.next();
				                	                	                	if(strRepID != "")
                					    	                    	    	{
               	        	    				    	                		if(strRepID.equals(vecTempRL.get(0)))
                   		    	                				    	    	{
               	        	    				    	                			strSelectedRL = "selected";
				               	    	    	                    	    	}
           	    					                	                		else
				       	        	                	                		{
           	    					                	                			strSelectedRL = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedRL = "";
            	                	            				    		}                	                	                	
				                        	                		    	out.println("<option " + strSelectedRL + " value=" + vecTempRL.get(0) +">" + vecTempRL.get(1) + "</option>");
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
    	                    	    	    				<% 
	                            		    		   				}
	                            		    		   			}
	                            		    		   		%>	                            		    		   			    	                	        	        			
            	    		    	            				<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
            		                            		    	    {
            	                            		    		   		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            	                            		    		   		{
            	    		    	            					%>            	    		    	            				
            	    		        	        					function getMerchants()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
            	    		    	            					<%
            	                            		    		   		}
            	                            		    		   		else
            	                            		    		   		{
            	                            		    		   	%>
            	    		        	        					function getMerchants()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}            	                            		    		   	
            	                            		    		   	<%
            	                            		    		   		}
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.AGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		        	        					function getMerchants()
                                        	            			{
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				} 
            	    		    	            					<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.SUBAGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		    	            					function getMerchants()
                                        	            			{
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%            	    		    	            					
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.REP))
            		                            		    	    {
            	    		    	            					%> 
            	    		    	            					function getMerchants()
                                        	            			{
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%
            		                            		    	    }
            	    		    	            					%>
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		<% 
	                            		    		    }
        	                	        			%>  
	   	    	                	        		<% 
	   	    	                	        		if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))||(strAccessLevel.equals(DebisysConstants.SUBAGENT))||(strAccessLevel.equals(DebisysConstants.REP))||(strAccessLevel.equals(DebisysConstants.MERCHANT)))
		                            		    	    {
                            		    		   			if(strAccessLevel.equals(DebisysConstants.MERCHANT))
                            		    		   			{    	                    	    	    				
                           		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.merchant",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	                            		    		   		
        	            	            						<select id="merchantList" name="merchantList" onchange="saveInfo();">
            	    	                    	    				<%
	            	    	                    	    				String strSelectedMerchant = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
	            	    	                    	    				if(strRepID != "")
	                        	    	                    	    	{
           	        	    				    	                		if(strMerchantID.equals(SessionData.getProperty("ref_id")))
               		    	                				    	    	{
           	        	    				    	                			strSelectedMerchant = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedMerchant = "";
			                	                	                		}
	                        	    	                    	    	}  
	                		                	                		else
	                		                	                		{
	                		                	                			strSelectedMerchant = "";
	                	        	        	                		}        	    	    	                    	    				
	            	    	                    	    				out.println("<option " + strSelectedMerchant + " value=" +  SessionData.getProperty("ref_id") +">" + SessionData.getProperty("company_name") + "</option>");
	                    	        	    	     				%>                                	
	    	                	        	        			</select>
	    	                	        	        			</td>
	    	                	        	        			</tr>
    	                    	    	    				<% 
	                            		    		   			}
	                            		    		   			else
	                            		    		   			{
	                            		    		   				if((strAccessLevel.equals(DebisysConstants.ISO))||(strAccessLevel.equals(DebisysConstants.AGENT))||(strAccessLevel.equals(DebisysConstants.SUBAGENT))||(strAccessLevel.equals(DebisysConstants.REP)))
	                            		    		   				{
	                            		    		   		%>
	                            		    			<tr class="main">
	                            		    				<td>
	                            		    					<%=Languages.getString("jsp.admin.ach.summary.merchant",SessionData.getLanguage())%>
	                            		    				</td>
    	                    	    	    				<td colspan="3">	 	                            		    		   		
        	            	            						<select id="merchantList" name="merchantList" onchange="saveInfo();">
				            	    	                    	    <%
            					    	                    	    	Vector vecMerchantList = TransactionSearch.getMerchantList(SessionData, strRepID);
    	    	        	    				                        Iterator itML = vecMerchantList.iterator();
            	    	                    					    	String strSelectedML = "";
        	    	    	                    	    				out.println("<option value=" + "-2" +">" + Languages.getString("jsp.admin.ach.summary.none",SessionData.getLanguage()) + "</option>");
			                	                	                	if(strMerchantID != "")
            					    	                    	    	{
           	        	    				    	                		if(strMerchantID.equals("-1"))
               		    	                				    	    	{
           	        	    				    	                			strSelectedML = "selected";
			               	    	    	                    	    	}
       	    					                	                		else
			       	        	                	                		{
       	    					                	                			strSelectedML = "";
			                	                	                		}
			                	    	                    	    	} 
			    	    	                	                		else
        					                	                		{
			    	    	                	                			strSelectedML = "";
        	                	            				    		} 
        	    	    	                    	    				out.println("<option " + strSelectedML + " value=" + "-1" +">" + Languages.getString("jsp.admin.ach.summary.all",SessionData.getLanguage()) + "</option>");
				            	    	                    	    	while (itML.hasNext())
	        					                	                    {
                	            				    	                	Vector vecTempML = null;
                	            				    	                	vecTempML = (Vector) itML.next();
				                	                	                	if(strMerchantID != "")
                					    	                    	    	{
               	        	    				    	                		if(strMerchantID.equals(vecTempML.get(0)))
                   		    	                				    	    	{
               	        	    				    	                			strSelectedML = "selected";
				               	    	    	                    	    	}
           	    					                	                		else
				       	        	                	                		{
           	    					                	                			strSelectedML = "";
				                	                	                		}
				                	    	                    	    	} 
				    	    	                	                		else
	        					                	                		{
				    	    	                	                			strSelectedML = "";
            	                	            				    		}                	                	                	
				                        	                		    	out.println("<option " + strSelectedML + " value=" + vecTempML.get(0) +">" + vecTempML.get(1) + "</option>");
                				            	        	    		}
	                            					    	     	%>                                 	
	    	                	        	        			</select>	                            		    		   		
    	                    	    	    				<% 
	                            		    		   				}
	                            		    		   			}
	                            		    		   		%>	                            		    		   			    	                	        	        			
            	    		    	            				<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
            		                            		    	    {
            	                            		    		   		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            	                            		    		   		{
            	    		    	            					%>            	    		    	            				
            	    		        	        					function saveInfo()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
            	    		    	            					<%
            		                            		    	    	}
            	                            		    		   		else
            	                            		    		   		{
            	                            		    		   	%>
            	    		        	        					function saveInfo()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}            	                            		    		   	
            	                            		    		   	<%
            	                            		    		   		}
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.AGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		        	        					function saveInfo()
                                        	            			{
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				} 
            	    		    	            					<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.SUBAGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		    	            					function saveInfo()
                                        	            			{
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%            	    		    	            					
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.REP))
            		                            		    	    {
            	    		    	            					%> 
            	    		    	            					function saveInfo()
                                        	            			{
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
	                                        	            			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.MERCHANT))
            		                            		    	    {
            	    		    	            					%>            	    		    	            				
            	    		    	            					function saveInfo()
                                        	            			{
	                                        	            			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%
            		                            		    	    }
            	    		    	            					%>
                                                				</SCRIPT>	    	                        	        
	    	    	                	        			</td>
	    	    	                	        		</tr>
	   	    	                	        		<% 
	                            		    		    }
        	                	        			%>        	                	        			        	                	        			      	                	        			        	                	        			        	                	        			      	                	        			        	                	        			      	                	        			        	                	        			
			              							<tr class="main">
            			   								<td nowrap="nowrap">
										               		<%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:
							        			       	</td>
							        			       	<td colspan="3">&nbsp;</td>
			               							</tr>    	    	                	        		
    	    	                	        		<tr class="main">													
			    										<td nowrap="nowrap">
			    											<%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>: 
			    										</td>
			    										<td>
			    											<input class="plain" id="txtStartDate" name="txtStartDate" value="<%=strStartDate%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.frmMain.txtStartDate,document.frmMain.txtEndDate);return false;" HIDEFOCUS="HIDEFOCUS"><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
			    										</td>
    													<td nowrap="nowrap">
    														<%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: 
    													</td>
    													<td>
    														<input class="plain" id="txtEndDate" name="txtEndDate" value="<%=strEndDate%>" size="12"><a href="javascript:void(0)" onclick="setDate();" HIDEFOCUS="HIDEFOCUS"><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a>
			    											<SCRIPT>
																function PadLeft(sValue, sPad, nLength) 
																{
																	sValue = sValue.toString();
																  	while ( sValue.length < nLength )
																    sValue = sPad + sValue;
																	return sValue;
																}			    											
			    												function setDate()
                                        	            		{
  																	if(!document.getElementById("txtStartDate").value.length == 0) 
  																	{
  																		var dtStart = Date.parse(document.getElementById("txtStartDate").value);
  																		dtStart = new Date(dtStart);
  																		var sStartYear = dtStart.getFullYear();
  																		var sStartMonth = PadLeft(dtStart.getMonth() + 1, "0", 2);
  																		var sStartDay = PadLeft(dtStart.getDate(), "0", 2);
  																		dtStart.setDate(dtStart.getDate() + <%=intACHRange%>);
  																		var sEndYear = dtStart.getFullYear();
  																		var sEndMonth = PadLeft(dtStart.getMonth() + 1, "0", 2);
  																		var sEndDay = PadLeft(dtStart.getDate(), "0", 2);
 																		if(self.gfPop)
																			gfPop.fPopCalendar(document.frmMain.txtEndDate,[[sStartYear,sStartMonth,sStartDay],[sEndYear,sEndMonth,sEndDay],[sStartYear,sStartMonth]]);
																	}
																	else
																	{
																		if(self.gfPop)
																			gfPop.fEndPop(document.frmMain.txtStartDate,document.frmMain.txtEndDate);
																	}	                                        	            		
	                                        	            		return false;
                                        	            		}
			    											</SCRIPT>    														
    													</td>
													</tr>
													<tr class="main">
														<td colspan="4" nowrap="nowrap">
															* <%=Languages.getString("jsp.admin.ach.summary.instructions",SessionData.getLanguage())%>
														</td>
													</tr>
													<tr class="main">
    													<td colspan="4">
      														<input type="hidden" name="search" id="search" value="y">
									         				<input type="submit" name="showRep" id="showRep" value="<%=Languages.getString("jsp.admin.ach.summary.submit",SessionData.getLanguage())%>" onclick="showReport();">
			            	    		                	<SCRIPT>
            	    		    	            					<%
            	                            		    		   	if(strAccessLevel.equals(DebisysConstants.ISO))
            		                            		    	    {
            	                            		    		   		if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            	                            		    		   		{
            	    		    	            					%>            	    		    	            				
            	    		        	        					function showReport()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.action = '/support/admin/achBilling/summary_result.jsp';
                                                    	    			document.frmMain.submit();
                                                     				}
            	    		    	            					<%
            		                            		    	    	}
            	                            		    		   		else
            	                            		    		   		{
            	                            		    		   	%>
            	    		        	        					function showReport()
                                        	            			{
	                                        	            			document.getElementById('IsoID').value = document.getElementById('isoList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.action = '/support/admin/achBilling/summary_result.jsp';
                                                    	    			document.frmMain.submit();
                                                     				}            	                            		    		   	
            	                            		    		   	<%
            	                            		    		   		}
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.AGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		        	        					function showReport()
                                        	            			{
	                                        	            			document.getElementById('AgentID').value = document.getElementById('agentList').value ;
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.action = '/support/admin/achBilling/summary_result.jsp';
                                                    	    			document.frmMain.submit();
                                                     				} 
            	    		    	            					<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.SUBAGENT))
            		                            		    	    {
            	    		    	            					%> 
            	    		    	            					function showReport()
                                        	            			{
	                                        	            			document.getElementById('SubAgentID').value = document.getElementById('subAgentList').value ;
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
                                                    	    			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;                                                    	    			
                                                    	    			document.frmMain.action = '/support/admin/achBilling/summary_result.jsp';
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%            	    		    	            					
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.REP))
            		                            		    	    {
            	    		    	            					%> 
            	    		    	            					function showReport()
                                        	            			{
	                                        	            			document.getElementById('RepID').value = document.getElementById('repList').value ;
	                                        	            			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;
                                                    	    			document.frmMain.action = '/support/admin/achBilling/summary_result.jsp';
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%
            		                            		    	    }
            	                            		    		   	else if(strAccessLevel.equals(DebisysConstants.MERCHANT))
            		                            		    	    {
            	    		    	            					%>
            	    		    	            					function showReport()
                                        	            			{
	                                        	            			document.getElementById('MerchantID').value = document.getElementById('merchantList').value ;
	                                        	            			document.getElementById('StartDate').value = document.getElementById('txtStartDate').value;
																		document.getElementById('EndDate').value = document.getElementById('txtEndDate').value;
                                                    	    			document.frmMain.action = '/support/admin/achBilling/summary_result.jsp';
                                                    	    			document.frmMain.submit();
                                                     				}
                                                     				<%
            		                            		    	    }
            	    		    	            					%>
                                                			</SCRIPT>
    													</td>
													</tr>
												</table>
		               						</td>
              							</tr>
            						</table>
            					</td>
            				</tr>	
            			</table>
            		 </form>
            	  </td>
               </tr>	
           </table>
		</td>
	</tr>
</table>
	<iframe width="132" height="142" name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
	</iframe>
<%@ 
	include file="/includes/footer.jsp" 
%>