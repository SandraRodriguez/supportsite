<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>

<%
int section = 7;
int section_page = 4;
%>

<%@ include file="/includes/security.jsp"%>
<%@ include file="/includes/header.jsp"%>

<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="2000">
			&nbsp;<%=Languages.getString("jsp.includes.menu.rateplan_manage",SessionData.getLanguage()).toUpperCase()%>
		</td>
		<td background="images/top_blue.gif" width="1%" align="right">
			<img src="images/top_right_blue.gif" width="18" height="20">
		</td>
	</tr>
	<tr><td colspan="3" class="formArea2" align="center">
		<div id="divTemplateList" class="main"></div>
	</td></tr>
</table>
<script>
function DoRefreshList()
{
	var sParams = "";

	if ( document.getElementById("txtPageNumber") != null )
	{
		sParams += "&txtSort=" + $("#txtSort").val();
		sParams += "&txtSortDirection=" + $("#txtSortDirection").val();
		sParams += "&ddlFilterType=" + $("#ddlFilterType").val();
		sParams += "&txtFilterID=" + escape($("#txtFilterID").val());
		sParams += "&txtFilterName=" + escape($("#txtFilterName").val());
		sParams += "&ddlFilterLevel=" + $("#ddlFilterLevel").val();
		sParams += "&txtPageNumber=" + $("#txtPageNumber").val();
		sParams += "&ddlPageSize=" + $("#ddlPageSize").val();
	}

	$('#divTemplateList').html("<%=Languages.getString("jsp.admin.rateplans.TemplateListWait",SessionData.getLanguage())%>");
	$('#divTemplateList').load('admin/rateplans/rateplans_list.jsp?Random=' + Math.random() + sParams);
}

function RenderSortIcon(sField)
{
	var sSort, imgPath;

	if (sField != "")
	{
		$("#txtSort").val(sField);
		if ( $("#txtSortDirection").val() == "A" )
		{
			$("#txtSortDirection").val("D");
		}
		else
		{
			$("#txtSortDirection").val("A");
		}
	}
	sSort = $("#txtSort").val();

	if ( $("#txtSortDirection").val() == "A" )
	{
		imgPath = "<img border=\"0\" src=\"images/up.png\">";
	}
	else
	{
		imgPath = "<img border=\"0\" src=\"images/down.png\">";
	}

	$("#divType").html("");
	$("#divID").html("");
	$("#divName").html("");
	$("#divLevel").html("");
	$("#div" + sSort).html(imgPath);

	if (sField != "")
	{
		DoRefreshList();
	}
}
DoRefreshList();
</script>
<%@ include file="/includes/footer.jsp"%>
