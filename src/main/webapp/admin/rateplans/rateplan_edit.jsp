<%@page import="com.debisys.rateplans.RatePlan"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.debisys.utils.DateUtil"%>
<%@page import="com.debisys.dateformat.DateFormat"%>
<%
int section = 7;
int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request" />
<jsp:setProperty name="RatePlan" property="*" />
<%@ include file="/includes/security.jsp"%>

<%@ include file="/includes/header.jsp"%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<table border="0" cellpadding="0" cellspacing="0" width="530">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000">
			&nbsp;
<%
Vector<String> vPlan = new Vector<String>();
Vector<String> vBasePlan;
if ( request.getParameter("base").length() > 0 )
{
	vBasePlan = RatePlan.getRatePlanById(request.getParameter("base"));
}
else
{
	vBasePlan = new Vector<String>();
	vBasePlan.add(0, "0");//OwnerID
	vBasePlan.add(1, "");
	vBasePlan.add(2, "");
	vBasePlan.add(3, "[EMIDA]");//Name
	vBasePlan.add(4, "");
	vBasePlan.add(5, "1");//IsTemplate
	vBasePlan.add(6, "ISO");//TargetEntityType
	vBasePlan.add(7, "");
	vBasePlan.add(8, "");
	vBasePlan.add(9, "");
}
boolean bIsEditable = true;
boolean bHasInheritance = false;
boolean bIsOwner = true;
boolean bHasChildren = false;
boolean bHasTerminals = false;

String sRefId = null;
String sAccessLevel = null;
String sChainLevel = null;
String sFullName;
String sBreakName;
String sDuplicateName = "";

if ( SessionData.getPropertyObj("CurrentRatePlanActor") != null )
{
	Vector<String> vTmp = (Vector<String>)SessionData.getPropertyObj("CurrentRatePlanActor");
	sRefId = vTmp.get(0);
	sAccessLevel = vTmp.get(1);
	sChainLevel = vTmp.get(2);
}
else
{
	sRefId = SessionData.getProperty("ref_id");
	sAccessLevel = strAccessLevel;
	sChainLevel = strDistChainType;
	Vector<String> vTmp = new Vector<String>();
	vTmp.add(sRefId);
	vTmp.add(sAccessLevel);
	vTmp.add(sChainLevel);
	SessionData.setPropertyObj("CurrentRatePlanActor", vTmp);
}

if ( request.getParameter("id").equals("0") )
{
	out.print(Languages.getString("jsp.admin.rateplans.CreateTemplateFrom2",SessionData.getLanguage()));
	if ( request.getParameter("dup") != null )
	{
		vPlan = RatePlan.getRatePlanById(request.getParameter("dup"));
		sDuplicateName = vPlan.get(3);
		vPlan.set(3, "");
		vPlan.set(4, "");
	}
	else
	{
		vPlan.add(0, "");
		vPlan.add(1, "");
		vPlan.add(2, "");
		vPlan.add(3, "");
		vPlan.add(4, "");
		vPlan.add(5, (sAccessLevel.equals(DebisysConstants.REP))?"0":"1");//IsTemplate
		vPlan.add(6, (sAccessLevel.equals(DebisysConstants.REP))?"MERCHANT":"REP");//TargetEntityType
		vPlan.add(7, "");
		vPlan.add(8, "");
		vPlan.add(9, "");
	}

	sFullName = sBreakName = vBasePlan.get(3);
	if ( sBreakName.length() > 100 )
	{
		sBreakName = sBreakName.substring(0, 100) + "...";
	}
	//out.print(" [" + sBreakName + "]");

	if ( sFullName.lastIndexOf("_") > -1 )
	{
		sFullName = sFullName.substring(0, sFullName.lastIndexOf("_"));
	}
}
else
{
	out.print(Languages.getString("jsp.admin.rateplans.EditTemplate",SessionData.getLanguage()));
	vPlan = RatePlan.getRatePlanById(request.getParameter("id"));
	sFullName = sBreakName = vPlan.get(3);
	if ( sBreakName.length() > 100 )
	{
		sBreakName = sBreakName.substring(0, 100) + "...";
	}
	//out.print(" [" + sBreakName + "]");
	bIsEditable = false;
	bHasInheritance = !(vPlan.get(0).equals(sRefId)) || Integer.parseInt(vPlan.get(7).concat(vPlan.get(8))) > 0;

	String sCarrier = null;
	if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
	{
		sCarrier = SessionData.getUser().getRefId();
	}
	Vector<Vector<String>> vAssignments = RatePlan.getRatePlanAssignments(request.getParameter("id"), sAccessLevel, sRefId, false, sCarrier);

	if ( vAssignments.size() > 0 )
	{
		bHasInheritance = bHasInheritance || !vAssignments.get(vAssignments.size() - 1).get(2).equals("0");
		bHasTerminals = !vAssignments.get(vAssignments.size() - 1).get(2).equals("0");
	}
	bIsOwner = vPlan.get(0).equals(sRefId);
	bHasChildren = Integer.parseInt(vPlan.get(7)) > 0;
	bHasTerminals |= Integer.parseInt(vPlan.get(8)) > 0;
}
%>
		</td>
		<td background="images/top_blue.gif" width="1%" align="right">
			<img src="images/top_right_blue.gif" width="18" height="20">
		</td>
	</tr>
	<tr><td colspan="3" class="formArea" align="center">
<%
if ( !bIsEditable )
{
%>
		<div style="text-align:right;">
			<div id="divExpandName" style="display:inline;"><a href="javascript:" onclick="$('#tName').css('display', 'inline');$('#divCollapseName').css('display', 'inline');$('#divExpandName').css('display', 'none');"><%=Languages.getString("jsp.admin.rateplans.ExpandPanel",SessionData.getLanguage())%></a>&nbsp;</div>
			<div id="divCollapseName" style="display:none;"><a href="javascript:" onclick="$('#tName').css('display', 'none');$('#divExpandName').css('display', 'inline');$('#divCollapseName').css('display', 'none');"><%=Languages.getString("jsp.admin.rateplans.CollapsePanel",SessionData.getLanguage())%></a>&nbsp;</div>
		</div><br/>
<%
}
%>
		<table id="tName" class="main" align="left" style="text-align:left;display:<%=(request.getParameter("id").equals("0"))?"inline":"none"%>;margin:5px;">
<%
String sAux, sShowName = "";
if ( request.getParameter("dup") != null )
{
	sAux = sDuplicateName;
}
else
{
	sAux = vBasePlan.get(3);
}

while ( sAux.length() > 40 )
{
	sShowName += sAux.substring(0, 40) + "<br/>";
	sAux = sAux.substring(40);
}
sShowName += sAux;
%>
			<tr>
				<td nowrap>&nbsp;<%=((request.getParameter("dup") != null)?Languages.getString("jsp.admin.rateplans.SiblingTemplate",SessionData.getLanguage()):Languages.getString("jsp.admin.rateplans.ParentTemplate",SessionData.getLanguage()))%></td>
				<td><b><%=sShowName%></b></td>
			</tr>
			<tr><td>&nbsp;</td></tr>
<%
if ( bIsEditable )
{
%>
			<tr>
				<td>&nbsp;<%=Languages.getString("jsp.admin.rateplans.Prefix",SessionData.getLanguage())%></td>
				<td><div id="divPrefixName" style="font-weight:bold;"></div></td>
			</tr>
<%
}
%>
			<tr>
				<td>&nbsp;<%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
				<td>
					&nbsp;<input type="text" id="txtName" name="txtName" maxlength="200" style="width:600px" onkeyup="TriggerValidateName();" onfocus="this.select();" value="<%=vPlan.get(3)%>" <%=(!bIsEditable)?"readonly":""%>><br/>
					<div id="divCheckName" style="display:none;"><img src="images/ajax-loading.gif" border="0"><%=Languages.getString("jsp.admin.rateplans.ValidatingName",SessionData.getLanguage())%></div>
				</td>
			</tr>
			<tr>
				<td>&nbsp;<%=Languages.getString("jsp.admin.rateplans.Description",SessionData.getLanguage())%></td>
				<td>
					&nbsp;<textarea id="txtDesc" name="txtDesc" rows="5" style="width:600px" onkeydown="ValidateDescription(this);" <%=(!bIsEditable)?"readonly":""%>><%=vPlan.get(4)%></textarea><br/>&nbsp;<i><%=Languages.getString("jsp.admin.rateplans.CaptionPlanDescription",SessionData.getLanguage())%></i>
				</td>
			</tr>
			<tr>
				<td>&nbsp;<%=Languages.getString("jsp.admin.rateplans.Type",SessionData.getLanguage())%></td>
				<td>&nbsp;
					<label><input type="radio" id="rbtnTemplate" name="PlanType" value="1" onclick="SetTemplateType(1);" <%=vPlan.get(5).equals("1")?"checked":""%> <%=(strAccessLevel.equals(DebisysConstants.REP) || !bIsEditable || (request.getParameter("dup") != null))?"disabled":""%>><%=Languages.getString("jsp.admin.rateplans.TypeTemplate",SessionData.getLanguage())%></label>
					<label><input type="radio" id="rbtnPlan" name="PlanType" value="0" onclick="SetTemplateType(0);" <%=vPlan.get(5).equals("0")?"checked":""%> <%=(!bIsEditable || (request.getParameter("dup") != null))?"disabled":""%>><%=Languages.getString("jsp.admin.rateplans.TypePlan",SessionData.getLanguage())%></label>
				</td>
			</tr>
			<tr>
				<td>&nbsp;<%=Languages.getString("jsp.admin.rateplans.AssignmentLevel",SessionData.getLanguage())%></td>
				<td>
					&nbsp;<select id="ddlTarget" <%=(!bIsEditable || (request.getParameter("dup") != null))?"disabled":""%> onchange="SetPlanName();"></select><br/>
					&nbsp;<div id="divLevelZero" style="display:none;"><img src='images/riskSite.jpg' style="vertical-align:middle;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.LevelHasZeroRates",SessionData.getLanguage())%></div>
				</td>
			</tr>
<%
if ( bIsEditable )
{
%>
			<tr>
				<td colspan="2" align="center"><br/>
					<input type="button" id="btnSaveName" value="<%=Languages.getString("jsp.admin.rateplans.SaveName",SessionData.getLanguage())%>" onclick="DoSaveName();return false;">
					&nbsp;&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.rateplans.Cancel",SessionData.getLanguage())%>" onclick="location.href = location.href.replace('rateplan_edit.jsp', 'rateplans.jsp');">
				</td>
			</tr>
<%
}
%>
		</table>
<%
if ( !bIsEditable )
{
%>
		<br/><form action="admin/rateplans/rateplans.jsp"><input type="submit" value="<%=Languages.getString("jsp.admin.rateplans.ReturnToManage",SessionData.getLanguage()) + Languages.getString("jsp.includes.menu.rateplan_manage",SessionData.getLanguage())%>"></form><br/><br/>
<%
}
%>
	</td></tr>
</table>
<script>
var nTimerID, bIsNameValid = null;

function SetTemplateType(nType)
{
	$('#ddlTarget').children().remove();
	if ( nType == 0 )
	{
		
		$('#ddlTarget').append('<option value="MERCHANT" <%if (vPlan.get(6).equals("MERCHANT")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.merchants",SessionData.getLanguage())%></option>'); 
	}
	else
	{
<%
if (vBasePlan.get(6).equals("ISO"))
{//ISO
	if ( vBasePlan.get(0).equals("0") )
	{
%>
		$('#ddlTarget').append('<option value="ISO"><%=Languages.getString("jsp.includes.menu.iso",SessionData.getLanguage())%></option>'); 
<%
	}
	else if (sChainLevel.equals(DebisysConstants.DIST_CHAIN_5_LEVEL) ||
		( (SessionData.getUser().isIntranetUser() && 
		 SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
		)
	)
	{//ISO 5 LEVELS
%>
		$('#ddlTarget').append('<option value="AGENT" <%if (vPlan.get(6).equals("AGENT")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.agents",SessionData.getLanguage())%></option>'); 
		$('#ddlTarget').append('<option value="SUBAGENT" <%if (vPlan.get(6).equals("SUBAGENT")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%></option>'); 
		$('#ddlTarget').append('<option value="REP" <%if (vPlan.get(6).equals("REP")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>'); 
<%
	}
	else
	{//ISO 3 LEVELS
%>
		$('#ddlTarget').append('<option value="REP" <%if (vPlan.get(6).equals("REP")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>'); 
<%
	}
}
else if (vBasePlan.get(6).equals("AGENT"))
{//AGENT
%>
		$('#ddlTarget').append('<option value="SUBAGENT" <%if (vPlan.get(6).equals("SUBAGENT")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%></option>'); 
		$('#ddlTarget').append('<option value="REP" <%if (vPlan.get(6).equals("REP")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>'); 
<%
}
else if (vBasePlan.get(6).equals("SUBAGENT"))
{//SUBAGENT
%>
		$('#ddlTarget').append('<option value="REP" <%if (vPlan.get(6).equals("REP")) {%>selected<%}%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>'); 
<%
}
else if (vBasePlan.get(6).equals("REP"))
{//REP
%>
		document.getElementById('rbtnTemplate').disabled = true;
		document.getElementById('rbtnPlan').checked = true;
		document.getElementById('rbtnPlan').onclick();
<%
}
%>
	}

<%
if ( bIsEditable )
{
%>
	SetPlanName();
<%
}
%>
}

function SetPlanName()
{
	var sFullName = "<%=sFullName.replaceAll("\"", "")%>";

<%
	Vector<Double> vMinRates = RatePlan.getRatePlanMinimumRates(request.getParameter("base"));
%>
	if ( $('#ddlTarget').val() == "AGENT" )
	{
		$('#divPrefixName').html(sFullName + "_MA");
		$('#divLevelZero').css('display', '<%=(vMinRates.get(1).equals(0.0)?"inline":"none")%>');
	}
	else if ( $('#ddlTarget').val() == "SUBAGENT" )
	{
		$('#divPrefixName').html(sFullName + "_SA");
		$('#divLevelZero').css('display', '<%=(vMinRates.get(2).equals(0.0)?"inline":"none")%>');
	}
	else if ( $('#ddlTarget').val() == "REP" )
	{
		$('#divPrefixName').html(sFullName + "_R");
		$('#divLevelZero').css('display', '<%=(vMinRates.get(3).equals(0.0)?"inline":"none")%>');
	}
	else if ( $('#ddlTarget').val() == "MERCHANT" )
	{
		$('#divPrefixName').html(sFullName.replace("(T)", "(RP)"));
		$('#divLevelZero').css('display', '<%=(vMinRates.get(4).equals(0.0)?"inline":"none")%>');
	}
	else
	{
		$('#divPrefixName').html(sFullName);
	}
	ValidateName();
}

function TriggerValidateName()
{
	window.clearTimeout(nTimerID);
	nTimerID = window.setTimeout("ValidateName();", 500);
}

function ValidateName()
{
	var ctl = document.getElementById("txtName");

	ctl.value = ctl.value.replace(/_/g, "");
	if ( ctl.value != "" )
	{
		$("#divCheckName").html("<img src='images/ajax-loading.gif' border='0' style='vertical-align:middle;'><%=Languages.getString("jsp.admin.rateplans.ValidatingName",SessionData.getLanguage())%>");
		$("#divCheckName").css('display', 'block');
		$.post('admin/rateplans/rateplan_ajax.jsp?action=checkName&id=<%=request.getParameter("id")%>&data=' + escape($('#divPrefixName').html() + "_" + ctl.value) + '&Random=' + Math.random(), ProcessCheckName);
	}
	else
	{
		bIsNameValid = false;
	}
}

function ValidateDescription(ctl)
{
	if ( ctl.value.length > 255 )
	{
		ctl.value = ctl.value.substring(0, 254);
	}
}

function ProcessCheckName(sData, sStatus)
{
	var ctl = document.getElementById("txtName");

	if ( sStatus == "success" )
	{
		if (sData.match("-VALID-") == "-VALID-")
		{
			bIsNameValid = true;
			$("#divCheckName").html("<img src='images/approved-icon.png' border='0' style='vertical-align:middle;'><%=Languages.getString("jsp.admin.rateplans.NameValid",SessionData.getLanguage())%>");
		}
		else
		{
			bIsNameValid = false;
			$("#divCheckName").html("<img src='images/icn-redx1.gif' border='0' style='vertical-align:middle;'><%=Languages.getString("jsp.admin.rateplans.NameAlreadyUsed",SessionData.getLanguage())%>");
			ctl.focus();
		}
	}
	else
	{
		alert("<%=Languages.getString("jsp.admin.rateplans.NameNotValidated",SessionData.getLanguage())%>");
	}
}

function DoSaveName()
{
	var ctlName = document.getElementById("txtName");
	var ctlDesc = document.getElementById("txtDesc");
	var ctlTarget = document.getElementById("ddlTarget");
	var nType = document.getElementById("rbtnTemplate").checked?1:0;
	var sDuplicate = "";

	ctlName.value = ctlName.value.replace(/_/g, "");
<%
if ( request.getParameter("dup") != null )
{
	out.println("	sDuplicate = \"&dup=" + request.getParameter("dup") + "\";");
}
%>

	if ( (bIsNameValid || bIsNameValid == null) && (ctlName.value != "") )
	{
		$("#divCheckName").html("<%=Languages.getString("jsp.admin.rateplans.SavingData",SessionData.getLanguage())%>");
		$("#divCheckName").css('display', 'block');
		$.post('admin/rateplans/rateplan_ajax.jsp?action=saveName&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>&name=' + escape($('#divPrefixName').html() + "_" + ctlName.value) + '&desc=' + escape(ctlDesc.value) + '&target=' + ctlTarget.value + '&type=' + nType + sDuplicate + '&Random=' + Math.random(), ProcessSaveName);
	}
	else
	{
		if ( ctlName.value == "" )
		{
			alert("<%=Languages.getString("jsp.admin.rateplans.EnterName",SessionData.getLanguage())%>");
			return false;
		}
		alert("<%=Languages.getString("jsp.admin.rateplans.NameAlreadyUsed2",SessionData.getLanguage())%>");
		$("#txtName").focus();
	}

	return false;
}

function ProcessSaveName(sData, sStatus)
{
	$("#divCheckName").css('display', 'none');
	if ( sStatus == "success" )
	{
		if (sData.match("-VALID-") == "-VALID-")
		{
			if (<%=request.getParameter("id")%> == 0)
			{
				location.href = location.href.replace('id=0', 'id=' + sData.split(":")[1]);
			}
		}
		else
		{
			alert("<%=Languages.getString("jsp.admin.rateplans.NameNotValidated",SessionData.getLanguage())%>");
		}
	}
	else
	{
		alert("<%=Languages.getString("jsp.admin.rateplans.NameNotValidated",SessionData.getLanguage())%>");
	}
}

SetTemplateType(document.getElementById('rbtnTemplate').checked?1:0);
</script>
<%
if ( bIsEditable )
{
%>
<script>
$(document).ready( function() 
{
	window.setTimeout("$('#txtName').focus();", 1000);
} );
</script>
<%
}
else
{
%>
<br/>
<div id="divRatePlanProductsSearch"></div><br/>
<div id="divRatePlanProducts"></div>
<script>
$(document).ready( function() 
{
<%
	if ( !bHasInheritance )
	{
%>
	$("#divRatePlanProductsSearch").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
	$('#divRatePlanProductsSearch').load('admin/rateplans/rateplan_ajax.jsp?action=getSearchProducts&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>&Random=' + Math.random());
<%
	}
	else
	{
		String sMsg = "";
		if ( !bIsOwner )
		{
			sMsg = Languages.getString("jsp.admin.rateplans.IsNotOwner",SessionData.getLanguage());
		}
		else
		{
			if ( bHasChildren )
			{
				sMsg = Languages.getString("jsp.admin.rateplans.PlanNotEditable",SessionData.getLanguage());
			}
			else
			{
				sMsg = Languages.getString("jsp.admin.rateplans.PlanHasTerminals",SessionData.getLanguage());
			}
			sMsg += "<br/>" + Languages.getString("jsp.admin.rateplans.CanEditEnabled",SessionData.getLanguage());
		}
%>
	$("#divRatePlanProductsSearch").html("<img src='images/risk-icon.png' style='vertical-align:middle;'>&nbsp;<span class='main'><%=sMsg%></span>");
<%
	}
%>        
	$("#divRatePlanProducts").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");        
	$('#divRatePlanProducts').load('admin/rateplans/rateplan_ajax.jsp?action=getProducts&id=<%=request.getParameter("id")%>&base=<%=(request.getParameter("base").equalsIgnoreCase("") ? "0" : request.getParameter("base"))%>&Random=' + Math.random());                        
} );

function RenderSortIcon(sField)
{
	var sSort, imgPath;

	if (sField !== "")
	{
		$("#txtSort").val(sField);
		if ( $("#txtSortDirection").val() == "A" )
		{
			$("#txtSortDirection").val("D");
		}
		else
		{
			$("#txtSortDirection").val("A");
		}
	}
	sSort = $("#txtSort").val();

	if ( $("#txtSortDirection").val() == "A" )
	{
		imgPath = "<img border=\"0\" src=\"images/up.png\">";
	}
	else
	{
		imgPath = "<img border=\"0\" src=\"images/down.png\">";
	}

	$("#divSKU").html("");
	$("#divName").html("");
	$("#divAmount").html("");
	$("#div" + sSort).html(imgPath);

	if (sField !== "")
	{            
		DoSave();            
	}
}
</script>
<%
}
%>
<%@ include file="/includes/footer.jsp"%>
