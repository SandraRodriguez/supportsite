<%@ page import="java.util.Vector,
                 java.util.Iterator,
                 com.debisys.utils.HTMLEncoder,
                 java.util.Hashtable" %>
<%
int section=7;
int section_page=1;
boolean hide_edit=true;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Hashtable hashTSorders = new Hashtable();


	if(request.getParameter("search") != null)
	{
		hide_edit=false;
	}

	if (request.getParameter("update_tsorder") != null)
	{
  		try
  		{
 			int i = 1;
  			while (request.getParameter("isopr_" + String.valueOf(i)) != null)
	 		{
	 			String pr = request.getParameter("isopr_" + String.valueOf(i));
	 			String ts = request.getParameter("isots_" + String.valueOf(i));
	 			String cr = request.getParameter("isocached_" + String.valueOf(i));
	 			String ratetable_id = request.getParameter("isoratep_" + String.valueOf(i));
    			Vector vecIsoInfo = new Vector();
	          	vecIsoInfo.add(0,pr);
          		vecIsoInfo.add(1,ts);
	          	vecIsoInfo.add(2,ratetable_id);
				if (cr != null)
				{
	          		vecIsoInfo.add(3,"1");
	          	}
	          	else
	          	{
	          		vecIsoInfo.add(3,"0");
	          	}
	          	if (!hashTSorders.containsKey(i))
	          	{
	              	hashTSorders.put(i, vecIsoInfo);
	          	}  
	           
	 			i++;
	 		}
	
	 		RatePlan.updateTSOrder(SessionData,hashTSorders);
	   	}
	  	catch (Exception e)
	  	{
	   		RatePlan.WriteDebug(e.toString());
	  	}
	}
 %>

<%

  Vector vecSearchResults = new Vector();
  Vector vecRatePlans = new Vector();

  vecRatePlans = RatePlan.getIsoRatePlans(SessionData);
  if (request.getParameter("search") != null)
  {
    if (request.getParameter("search").equals("y"))
    {
        vecSearchResults = RatePlan.getIsoProducts(SessionData);
    }
  }

%>
<%@ include file="/includes/header.jsp" %>

<script language="javascript">
	function validate(c)
    {
    	if (isNaN(c.value))
    	{
    		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
    		c.focus();
    		return (false);

    	}
    	else
    	{
    		if (c.value < 0)
    		{

		    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
		    	c.focus();
		    	return (false);
			}
		}
    }
    function GetRatePlanID()
    {
	    var dropdownIndex = document.getElementById('ratePlanId').selectedIndex;
	    alert(dropDownIndex);
		var dropdownValue = document.getElementById('ratePlanId')[dropdownIndex].value;
		return dropdownValue;
	}
</script>

<table border="0" cellpadding="0" cellspacing="0" width="750" class="formArea">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.rateplans.index.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
  <tr>
  <td>
<%
if (vecRatePlans != null && vecRatePlans.size() > 0)
{
  %>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
    	<tr>
	    	<td class="main">
          		<%=Languages.getString("jsp.admin.rateplans.index.instructions",SessionData.getLanguage())%>
       			<br>
            	<form name="downloadform" method="post" action="admin/rateplans/download_rateplans.jsp">
	             	<input type="hidden" name="Download" value="Y">
	             	<input type="hidden" name="RatePlanId" value="<%=RatePlan.getRatePlanId()%>">	
				</form>
				<form name="rateplan" method="post" action="admin/rateplans/index.jsp" >
					<table width="300">
						<tr class="main">
							<td nowrap="nowrap" valign="top">
								<%=Languages.getString("jsp.admin.rateplans.index.select_rateplan",SessionData.getLanguage())%>:
								
							</td>
							<br/>
							<td>
								<select id="ratePlanId" name="ratePlanId">
									<option value=""><%=Languages.getString("jsp.admin.rateplans.index.view_all",SessionData.getLanguage())%></option>
								<%
									Iterator it = vecRatePlans.iterator();
									while (it.hasNext())
									{
										Vector vecTemp = null;
										vecTemp = (Vector) it.next();
										if (!vecTemp.get(0).toString().equals(RatePlan.getRatePlanId()))
										{
											out.println("<option value=\""+vecTemp.get(0).toString()+"\">" +
												HTMLEncoder.encode(vecTemp.get(1).toString()) + "["+vecTemp.get(2).toString()+"]" +
												"</option>");
										}
										else
										{
											out.println("<option value=\""+vecTemp.get(0).toString()+"\" selected>" +
												HTMLEncoder.encode(vecTemp.get(1).toString()) + "["+vecTemp.get(2).toString()+"]" +
												"</option>");
										}
									}
									vecRatePlans.clear();
								%>
								</select>
							</td>
							<td valign="top">
								<input type="hidden" name="search" value="y">
								<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.rateplans.index.view_rateplan",SessionData.getLanguage())%>">
							</td>
							<td valign="top">
							<%
								if (!hide_edit)
								{
									out.println("<input type=\"submit\" name=\"update_tsorder\" value=\"" + Languages.getString("jsp.admin.rateplans.index.update_tsorder",SessionData.getLanguage())+ "\">");
								}
								else
								{
									out.println("<input type=\"submit\" disabled=\"true\" name=\"update_tsorder\" value=\"" + Languages.getString("jsp.admin.rateplans.index.update_tsorder",SessionData.getLanguage())+ "\">");
								}
							%>
							</td>
							<%
								if(!hide_edit){
									out.println("<td><input type=button name=submit value=" + Languages.getString("jsp.admin.rateplans.download",SessionData.getLanguage()) + " onClick=\"document.downloadform.submit()\"></td>");
								}else{
									out.println("<td><input type=button disabled=\"true\" name=submit value=" + Languages.getString("jsp.admin.rateplans.download",SessionData.getLanguage()) + " onClick=\"document.downloadform.submit()\"></td>");
								}
							%>
							<td>
								<a style="font-weight:bold;" href="admin/rateplans/assig_rateplans.jsp"><nobr><%=Languages.getString("jsp.admin.rateplans.assign.button",SessionData.getLanguage())%> </nobr></a>
							</td>
						</tr>
					</table>
<%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <table width="100%" cellspacing="1" cellpadding="1" border="0">
            <tr>
                <td class="rowhead2"></td>
            	<td class="rowhead2"><%=Languages.getString("jsp.admin.rateplans.index.rateplan",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class="rowhead2"><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class="rowhead2"><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class="rowhead2" ><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%></td>
              <% if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
            	<td class="rowhead2" ><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class="rowhead2"><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%></td>
              <%}%>
              <td class="rowhead2"><%=Languages.getString("jsp.admin.rep_percent",SessionData.getLanguage()).toUpperCase()%></td>
              <td class="rowhead2" ><%=Languages.getString("jsp.admin.merchant_percent",SessionData.getLanguage()).toUpperCase()%></td>
              <% //if (SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION)){ %>
              <td class="rowhead2" ><%=Languages.getString("jsp.admin.rateplans.tsorder",SessionData.getLanguage()).toUpperCase()%></td>
              <%//} %>
              <td class="rowhead2"><%=Languages.getString("jsp.admin.rateplans.cachedreceipts",SessionData.getLanguage()).toUpperCase()%></td>

            </tr>
            <%
                  it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  int icount=0;
                  while (it.hasNext())
                  {
                    icount++;  
                    Vector vecTemp = null; 
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + icount + "</td>" );
                    out.println("<td> ");
                    out.println(HTMLEncoder.encode(vecTemp.get(0).toString())); 
                    
                    out.println("</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(1).toString()) + "</td>" +
                                "<td align=left>" + vecTemp.get(2) + "</td>" +
                                "<td align=right>" + vecTemp.get(3) + "%</td>");

                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                    {
                       out.println("<td align=right>" + vecTemp.get(4) + "%</td>" +
                                            "<td align=right>" + vecTemp.get(5) + "%</td>");
                    }
                    out.println("<td align=right>" + vecTemp.get(6) + "%</td>" +
                                "<td align=right>"); 
                    out.println(vecTemp.get(7) + "%</td>" );
					
					if (SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION)) 
					{
					  out.println("<td align=center>");
					  out.println("<input type=text name=\"isots_" + icount + "\" value=\"" + vecTemp.get(8) + "\" size=3 maxlength=5 onBlur=\"return validate(this);\"></td>");
					}
					else{
					  out.println("<td align=center>");
					  out.println("<input disabled type=text  value=\"" + vecTemp.get(8) + "\" size=3 maxlength=5 onclick=\"return false;\">");
					  out.println("<input type=hidden name=\"isots_" + icount + "\" value=\"" + vecTemp.get(8) + "\">");
					  out.println("</td>");
					}
                    out.println("<td align=center>");
                    String strTemp = vecTemp.get(10).equals("1")?"checked":"";
					out.println("<input type=\"checkbox\" name=\"isocached_" + icount + "\" value=\"isocached_" + icount + "\"" + strTemp + ">");
                    out.println("</td>");
                    out.println("<td align=right>  <input type=hidden name=\"isopr_" + icount + "\" value=\"" + vecTemp.get(2) + "\">"); 
                    out.println("<input type=hidden name=\"isoratep_" + icount + "\" value=\"" + vecTemp.get(9) + "\">");
                    out.println("</td>");
                    out.println("</tr>");
                    
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>                
            </table>
           </form>

<%
}
else
{
 if (request.getParameter("search") != null)
 {
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.rateplans.index.error1",SessionData.getLanguage())+"</font>");
 }
}
  }
  else
  {
    out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.rateplans.index.error2",SessionData.getLanguage())+"</font>");
  }
%>
          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>