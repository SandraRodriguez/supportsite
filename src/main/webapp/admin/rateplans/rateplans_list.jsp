<%@page import="com.debisys.rateplans.RatePlan"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.debisys.utils.NumberUtil"%>
<%@page import="com.debisys.users.SessionData"%>
<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.utils.DateUtil"%>
<%@page import="com.debisys.dateformat.DateFormat"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request" />

<%
response.setCharacterEncoding("iso-8859-1");

int section = 7;
int section_page = 4;

String sRefId = null;
String sAccessLevel = null;
String sChainLevel = null;
String strAccessLevel = SessionData.getProperty("access_level");
String strDistChainType = SessionData.getProperty("dist_chain_type");

String sPageNumber, sPageSize, sSort, sSortDirection, sFilterType, sFilterId, sFilterName, sFilterLevel;
int nTotalRows, nPageCount;

sPageNumber = request.getParameter("txtPageNumber");
sPageSize = request.getParameter("ddlPageSize");
sSort = request.getParameter("txtSort");
sSortDirection = request.getParameter("txtSortDirection");
sFilterType = request.getParameter("ddlFilterType");
sFilterId = request.getParameter("txtFilterID");
sFilterName = request.getParameter("txtFilterName");
sFilterLevel = request.getParameter("ddlFilterLevel");

SessionData.removeProperty("RatePlan_RecentAdded");
if ( sPageNumber == null ) sPageNumber = "1";
if ( sPageSize  == null ) sPageSize = "50";
if ( sSort == null ) sSort = "Type";
if ( sSortDirection == null ) sSortDirection = "D";
if ( sFilterType == null ) sFilterType = "";
if ( sFilterId == null ) sFilterId = "";
if ( sFilterName == null ) sFilterName = "";
if ( sFilterLevel == null ) sFilterLevel = "";

if ( SessionData.getPropertyObj("CurrentRatePlanActor") != null )
{
	Vector<String> vTmp = (Vector<String>)SessionData.getPropertyObj("CurrentRatePlanActor");
	sRefId = vTmp.get(0);
	sAccessLevel = vTmp.get(1);
	sChainLevel = vTmp.get(2);

	if ( !sRefId.equals(SessionData.getProperty("ref_id")) )
	{//If we are browsing someone's else hierarchy
		Hashtable<String, String> htLevels = new Hashtable<String, String>();
		htLevels.put(DebisysConstants.ISO, "ISO");
		htLevels.put(DebisysConstants.AGENT, "AGENT");
		htLevels.put(DebisysConstants.SUBAGENT, "SUBAGENT");
		htLevels.put(DebisysConstants.REP, "REP");

		Hashtable<String, Vector<String>> htActors = (Hashtable<String, Vector<String>>)SessionData.getPropertyObj("RatePlanActors");
		Vector<String> vItem = htActors.get(sRefId);
		String sBreadCrumb = "&nbsp;&gt;&gt;&nbsp;<b>[" + htLevels.get(vItem.get(1)) + "]&nbsp;" + vItem.get(0) + "</b><br/><br/>";
		String sLastActorId = null;
		if ( vItem.get(1).equals(DebisysConstants.ISO) )
		{
			sBreadCrumb = "&nbsp;&gt;&gt;&nbsp;<b><a href=\"javascript:DoBrowse(" + SessionData.getProperty("ref_id") + ", " + strAccessLevel + ", " + strDistChainType + ", 0)\">[EMIDA]&nbsp;</a></b>" + sBreadCrumb;
		}
		else
		{
			do
			{
				sLastActorId = vItem.get(3);
				vItem = htActors.get(sLastActorId);
				if ( vItem == null )
				{
					sBreadCrumb = "&nbsp;&gt;&gt;&nbsp;<b><a href=\"javascript:DoBrowse(" + SessionData.getProperty("ref_id") + ", " + strAccessLevel + ", " + strDistChainType + ", 0)\">[" + htLevels.get(strAccessLevel) + "]&nbsp;" + SessionData.getProperty("company_name") + "</a></b>" + sBreadCrumb;
					break;
				}
				sBreadCrumb = "&nbsp;&gt;&gt;&nbsp;<b><a href=\"javascript:DoBrowse(" + sLastActorId + ", " + vItem.get(1) + ", " + vItem.get(2) + ", 0)\">[" + htLevels.get(vItem.get(1)) + "]&nbsp;" + vItem.get(0) + "</a></b>" + sBreadCrumb;
				if ( sLastActorId.equals(vItem.get(3)) && ((htActors.get("") != null) || SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) ))
				{
					sBreadCrumb = "&nbsp;&gt;&gt;&nbsp;<b><a href=\"javascript:DoBrowse(" + SessionData.getProperty("ref_id") + ", " + strAccessLevel + ", " + strDistChainType + ", 0)\">[EMIDA]&nbsp;</a></b>" + sBreadCrumb;
				}
			}
			while ( !sLastActorId.equals(vItem.get(3)) );
		}
		out.print("<div style=\"width:100%;text-align:left;\">" + Languages.getString("jsp.admin.rateplans.UseBreadCrumb",SessionData.getLanguage()) + "<br/>");
		out.print(sBreadCrumb + "</div>");
	}
}
else
{
	sRefId = SessionData.getProperty("ref_id");
	sAccessLevel = strAccessLevel;
	sChainLevel = strDistChainType;
	Vector<String> vTmp = new Vector<String>();
	vTmp.add(sRefId);
	vTmp.add(sAccessLevel);
	vTmp.add(sChainLevel);
	SessionData.setPropertyObj("CurrentRatePlanActor", vTmp);
}

String sISODefault = request.getSession().getServletContext().getAttribute("iso_default").toString();
Vector<Vector<String>> vData = null;
if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
{
	vData = RatePlan.getNewRatePlansByActor(sISODefault, Integer.parseInt(sPageNumber), Integer.parseInt(sPageSize), sSort, sSortDirection, sFilterType, sFilterId, sFilterName, sFilterLevel, SessionData.getUser().getRefId());
}
else
{
	String sCarrier = null;
	if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
	{
		sCarrier = SessionData.getUser().getRefId();
	}
	vData = RatePlan.getNewRatePlansByActor(sRefId, Integer.parseInt(sPageNumber), Integer.parseInt(sPageSize), sSort, sSortDirection, sFilterType, sFilterId, sFilterName, sFilterLevel, sCarrier);
}
%>
<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;">
	<tr class="main">
		<td align="left" class="main"><%=Languages.getString("jsp.admin.rateplans.FilterBy",SessionData.getLanguage())%>:</td>
<%
if ( !vData.get(0).get(0).equals("0") )
{
%>
		<td align="right" colspan="2"><form action="admin/rateplans/rateplans_list_export.jsp" target="_blank"><input type="submit" value="<%=Languages.getString("jsp.admin.rateplans.ClickExportExcel",SessionData.getLanguage())%>"></form></td>
<%
}
%>
	</tr>
	<tr class="main">
		<td align="left" valign="bottom" nowrap="nowrap" class="main">
			<%=Languages.getString("jsp.admin.rateplans.Type",SessionData.getLanguage())%>&nbsp;<select id="ddlFilterType" name="ddlFilterType" style="vertical-align:middle;" onchange="$('#txtPageNumber').val('1');DoRefreshList();"><option value=""><%=Languages.getString("jsp.admin.rateplans.All",SessionData.getLanguage())%></option><option value="1" <%=(sFilterType.equals("1")?"selected":"")%>><%=Languages.getString("jsp.admin.rateplans.TypeTemplate",SessionData.getLanguage())%></option><option value="0" <%=(sFilterType.equals("0")?"selected":"")%>><%=Languages.getString("jsp.admin.rateplans.TypePlan",SessionData.getLanguage())%></option></select>&nbsp;
			<%=Languages.getString("jsp.admin.rateplans.ID",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterID" name="txtFilterID" maxlength="50" size="5" style="vertical-align:middle;" value="<%=sFilterId%>">&nbsp;
			<%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterName" name="txtFilterName" maxlength="200" size="30" style="vertical-align:middle;" value="<%=sFilterName%>">&nbsp;
			<%=Languages.getString("jsp.admin.rateplans.Level",SessionData.getLanguage())%>&nbsp;<select id="ddlFilterLevel" name="ddlFilterLevel" style="vertical-align:middle;" onchange="$('#txtPageNumber').val('1');DoRefreshList();">
			<option value=""><%=Languages.getString("jsp.admin.rateplans.All",SessionData.getLanguage())%></option>
			<option value="ISO" <%=(sFilterLevel.equals("ISO")?"selected":"")%>>ISO</option>
			<option value="AGENT" <%=(sFilterLevel.equals("AGENT")?"selected":"")%>>AGENT</option>
			<option value="SUBAGENT" <%=(sFilterLevel.equals("SUBAGENT")?"selected":"")%>>SUBAGENT</option>
			<option value="REP" <%=(sFilterLevel.equals("REP")?"selected":"")%>>REP</option>
			<option value="MERCHANT" <%=(sFilterLevel.equals("MERCHANT")?"selected":"")%>>MERCHANT</option>
			</select>
			<input type="image" src="images/greenarrow.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickFilterResults",SessionData.getLanguage())%>" onclick="$('#txtPageNumber').val('1');DoRefreshList();">
			<input type="image" src="images/reddelete.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickClearResults",SessionData.getLanguage())%>" onclick="$('#ddlFilterType').val('');$('#txtFilterID').val('');$('#txtFilterName').val('');$('#ddlFilterLevel').val('');$('#txtPageNumber').val('1');DoRefreshList();">
		</td>
		<td align="center" valign="bottom" class="main">
			<table cellspacing="-1" cellpadding="-1"><tr class="main">
<%
if ( !vData.get(0).get(0).equals("0") )
{
	int nPageNumber = Integer.parseInt(sPageNumber);
	int nPageSize = Integer.parseInt(sPageSize);
	nTotalRows = Integer.parseInt(vData.get(0).get(0));
	nPageCount = nTotalRows / nPageSize;
	if ((nPageCount * nPageSize) < nTotalRows)
	{
		nPageCount++;
	}

	if ( nPageCount > 1 && nPageNumber > 1 )
	{
%>
				<td class="main"><a href="javascript:" onclick="$('#txtPageNumber').val('1');DoRefreshList();"><img border="0" src="images/pagefirst.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></a></td>
<%
	}
	else
	{
%>
				<td class="main"><img border="0" src="images/pagefirstdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></td>
<%
	}

	if (nPageNumber > 1)
	{
%>
				<td class="main"><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber - 1)%>');DoRefreshList();"><img border="0" src="images/pageprev.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></a></td>
<%
	}
	else
	{
%>
				<td class="main"><img border="0" src="images/pageprevdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></td>
<%
	}
%>
				<td nowrap="nowrap" class="main">&nbsp;<%=SessionData.getString("jsp.admin.rateplans.ShowingPageOf", new Object[]{new Integer(nPageNumber), new Integer(nPageCount)})%>&nbsp;</td>
<%
	if (nPageNumber < nPageCount)
	{
%>
				<td class="main"><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber + 1)%>');DoRefreshList();"><img border="0" src="images/pagenext.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></a></td>
<%
	}
	else
	{
%>
				<td class="main"><img border="0" src="images/pagenextdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></td>
<%
	}

	if ( nPageCount > 1 && nPageNumber < nPageCount )
	{
%>
				<td class="main"><a href="javascript:" onclick="$('#txtPageNumber').val('<%=nPageCount%>');DoRefreshList();"><img border="0" src="images/pagelast.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></a></td>
<%
	}
	else
	{
%>
				<td class="main"><img border="0" src="images/pagelastdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></td>
<%
	}
}
%>
			</tr></table>
		</td>
		<td align="right" valign="bottom" nowrap="nowrap" class="main">
			<%=Languages.getString("jsp.admin.rateplans.PageSize",SessionData.getLanguage())%>&nbsp;<select id="ddlPageSize" name="ddlPageSize" style="vertical-align:middle;" onchange="$('#txtPageNumber').val('1');DoRefreshList();"><option value="10" <%=(sPageSize.equals("10")?"selected":"")%>>10</option><option value="20" <%=(sPageSize.equals("20")?"selected":"")%>>20</option><option value="50" <%=(sPageSize.equals("50")?"selected":"")%>>50</option></select>
		</td>
	</tr>
</table>
<input type="hidden" id="txtPageNumber" name="txtPageNumber" value="<%=sPageNumber%>" class="main">
<input type="hidden" id="txtSort" name="txtSort" value="<%=sSort%>" class="main">
<input type="hidden" id="txtSortDirection" name="txtSortDirection" value="<%=sSortDirection%>" class="main">
<%
if ( !vData.get(0).get(0).equals("0") )
{
%>
<table width="100%" cellspacing="1" cellpadding="1" border="0">
	<tr class="main"><td align="center" colspan="15"><small><%=Languages.getString("jsp.admin.rateplans.ClickHeadersSort",SessionData.getLanguage())%></small></td></tr>
	<tr class="SectionTopBorder">
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><a href="javascript:" onclick="RenderSortIcon('Type');" class="rowhead2"><%=SessionData.getString("jsp.admin.rateplans.Type").toUpperCase()%></a><br/><div id="divType"></div></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><a href="javascript:" onclick="RenderSortIcon('ID');"><%=SessionData.getString("jsp.admin.rateplans.ID").toUpperCase()%></a><br/><div id="divID"></div></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;width:250px;" nowrap="nowrap"><a href="javascript:" onclick="RenderSortIcon('Name');"><u><%=SessionData.getString("jsp.admin.rateplans.Name").toUpperCase()%></u></a><br/><div id="divName"></div></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.InheritsFrom").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><a href="javascript:" onclick="RenderSortIcon('Level');"><u><%=SessionData.getString("jsp.admin.rateplans.Level").toUpperCase()%></u></a><br/><div id="divLevel"></div></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=SessionData.getString("jsp.admin.rateplans.CreatedBy").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=SessionData.getString("jsp.admin.rateplans.LastUpdate").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=SessionData.getString("jsp.admin.rateplans.AmountInheritance").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.AmountAssigned").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=SessionData.getString("jsp.admin.rateplans.AmountTerminals").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.CreateTemplateFrom").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.Duplicate").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.Assign").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.Edit").toUpperCase()%></td>
		<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.Delete").toUpperCase()%></td>
	</tr>
<%
	int nCounter = 1;
	Iterator<Vector<String>> it = vData.iterator();
	Hashtable<String, String> htLevels = new Hashtable<String, String>();
	htLevels.put("ISO", DebisysConstants.ISO);
	htLevels.put("AGENT", DebisysConstants.AGENT);
	htLevels.put("SUBAGENT", DebisysConstants.SUBAGENT);
	htLevels.put("REP", DebisysConstants.REP);
	htLevels.put("MERCHANT", DebisysConstants.MERCHANT);

	boolean bAllowShowAllLevels = false;//Used when login as SuperUser of ISO-3 level and needs to see both Agent and SubAgent levels
	if (SessionData.getProperty("ref_id").equals(sISODefault) &&
		( (SessionData.getUser().isIntranetUser() && 
		SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
		) ||
		(SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()))
	)
	{
		bAllowShowAllLevels = true;
	}

	while ( it.hasNext() )
	{
		Vector<String> vTemp = it.next();
		if ( nCounter == 1 )
		{
			nCounter++;
			continue;//We must skip this iteration since the first row is the count of total records
		}

		String sNameTmp = "";
		while ( vTemp.get(3).length() > 40 )
		{
			sNameTmp += vTemp.get(3).substring(0, 40) + "<br/>";
			vTemp.set(3, vTemp.get(3).substring(40));
		}
		sNameTmp += vTemp.get(3);


		if ( !bAllowShowAllLevels && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && ( vTemp.get(4).equals("AGENT") || vTemp.get(4).equals("SUBAGENT") ) )
		{
			continue;
		}
%>
	<tr class="RatePlanRow<%=vTemp.get(9)%><%=(nCounter++ % 2)%>">
		<td class="main">&nbsp;<%=vTemp.get(9).equals("1")?Languages.getString("jsp.admin.rateplans.TypeTemplate",SessionData.getLanguage()):Languages.getString("jsp.admin.rateplans.TypePlan",SessionData.getLanguage())%>&nbsp;</td>
		<td class="main">&nbsp;<%=vTemp.get(0)%>&nbsp;</td>
		<td class="main"><%=sNameTmp%></td>
		<td class="main">&nbsp;<%=(vTemp.get(1).equals(""))?"[EMIDA]":vTemp.get(1)%>&nbsp;</td>
		<td align="center" class="main">&nbsp;<%=vTemp.get(4)%>&nbsp;</td>
		<td align="center" nowrap="nowrap" class="main">
<%
		if ( vTemp.get(5).length() > 10 )
		{
			out.print("<span label=\"" + vTemp.get(5).replaceAll("\"", "'") + "\">[" + vTemp.get(5).substring(0, 10) + "]</span>");
		}
		else
		{
			out.print("[" + vTemp.get(5) + "]");
		}
		out.print("&nbsp;" + vTemp.get(6) + "<br/>&nbsp;" + Languages.getString("jsp.admin.rateplans.onDate",SessionData.getLanguage()) + "&nbsp;" + DateUtil.formatDateNoTime(vTemp.get(7), DateFormat.getDateNoTimeFormat()) + "&nbsp;");
%>
		</td>
		<td align="center" nowrap="nowrap" class="main">&nbsp;<%=vTemp.get(8)%>&nbsp;</td>
<%
		if ( vTemp.get(2).equals(sRefId) )//If I'm the Owner of this item
		{
			if ( vTemp.get(9).equals("1") )//If is a template
			{
				if ( vTemp.get(10).equals("0") )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\"><b><a href=\"javascript:ShowHierarchy(" + vTemp.get(0) + ");\">" + vTemp.get(10) + "&gt;&gt;</a></b></td>");
				}

				Vector<Vector<String>> vAssignments = new Vector<Vector<String>>();
				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					)
				)
				{
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), htLevels.get(vTemp.get(4)), "", true, null);
				}
				else
				{
					String sCarrier = null;
					if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
					{
						sCarrier = SessionData.getUser().getRefId();
					}
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), sAccessLevel, sRefId, false, sCarrier);
				}

				if ( vAssignments.size() == 0 )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\"><a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&level=" + htLevels.get(vTemp.get(4)) + "&search=y&newrp=&filter=\">&nbsp;&nbsp;<b>" + vAssignments.get(0).get(1) + "</b>&nbsp;&nbsp;</a></td>");
				}
				out.println("<td align=\"center\">-</td>");
				out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=0&base=" + vTemp.get(0) + "\"><img src=\"images/greenadd.png\" border=\"0\"></a></td>");
				out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=0&base=" + vTemp.get(1) + "&dup=" + vTemp.get(0) + "\"><img src=\"images/bluecopy.png\" border=\"0\"></a></td>");

				String sSearch;
				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					)
				)
				{
					sSearch = "";
				}
				else
				{
					sSearch = "&search=y";
				}
				out.println("<td align=\"center\"><a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&level=" + htLevels.get(vTemp.get(4)) + sSearch + "&newrp=\"><img src=\"images/greenarrow.png\" border=\"0\"></a></td>");
				out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=" + vTemp.get(0) + "&base=" + vTemp.get(1) + "\"><img src=\"images/yellowedit.png\" border=\"0\"></a></td>");
				if ( Integer.parseInt(vTemp.get(10)) > 0 )
				{
					out.println("<td align=\"center\"><img src=\"images/graydelete.png\"></td>");
				}
				else
				{
					out.println("<td align=\"center\"><a href=\"javascript:\" onclick=\"DoDeletePlan(" + vTemp.get(0) + ");\"><img src=\"images/reddelete.png\" border=\"0\"></a></td>");
				}
			}
			else//Else is a RatePlan
			{
				out.println("<td align=\"center\">-</td>");
				boolean bHasEnabledMerchants = false;
				Vector<Vector<String>> vAssignments = new Vector<Vector<String>>();
				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					)
				)
				{
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), "", "", false, null);
				}
				else
				{
					String sCarrier = null;
					if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
					{
						sCarrier = SessionData.getUser().getRefId();
					}
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), sAccessLevel, sRefId, false, sCarrier);
				}

				if ( vAssignments.size() == 0 )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\">");
					Iterator<Vector<String>> itCounts = vAssignments.iterator();
					while ( itCounts.hasNext() )
					{
						Vector<String> vItem = itCounts.next();
						out.println("<a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&level=" + htLevels.get(vItem.get(0)) + "&search=y&newrp=&filter=&isPlan=\">" + vItem.get(0) + "&nbsp;<b>" + vItem.get(1) + "</b></a><br/>");
						vItem = null;
					}
					itCounts = null;
					bHasEnabledMerchants = !vAssignments.get(vAssignments.size() - 1).get(2).equals("0");
					out.println("</td>");
				}

				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					)
				)
				{
					vTemp.set(11, vTemp.get(12));
				}

				if ( vTemp.get(11).equals("0") )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\"><a href=\"admin/reports/analysis/merchant_detail_siteId.jsp?ratePlanId=" + vTemp.get(0) + "&mids=ALL&tmids=ALL&searchMerchants=\" target=\"_blank\">&nbsp;&nbsp;<b>" + vTemp.get(11) + "</b>&nbsp;&nbsp;</a></td>");
				}

				out.println("<td align=\"center\"><img src=\"images/grayadd.png\"></td>");
				out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=0&base=" + vTemp.get(1) + "&dup=" + vTemp.get(0) + "\"><img src=\"images/bluecopy.png\" border=\"0\"></a></td>");
				if ( sAccessLevel.equals(DebisysConstants.REP) )
				{
					out.println("<td align=\"center\"><img src=\"images/grayarrow.png\"></td>");
				}
				else
				{
					out.println("<td align=\"center\"><a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&newrp=1&isPlan=\"><img src=\"images/greenarrow.png\" border=\"0\"></a></td>");
				}
				out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=" + vTemp.get(0) + "&base=" + vTemp.get(1) + "\"><img src=\"images/yellowedit.png\" border=\"0\"></a></td>");
				if ( Long.parseLong(vTemp.get(10).concat(vTemp.get(11))) > 0 || bHasEnabledMerchants )
				{
					out.println("<td align=\"center\"><img src=\"images/graydelete.png\"></td>");
				}
				else
				{
					out.println("<td align=\"center\"><a href=\"javascript:\" onclick=\"DoDeletePlan(" + vTemp.get(0) + ");\"><img src=\"images/reddelete.png\" border=\"0\"></a></td>");
				}
			}
		}
		else//If this item was assigned from someone above me
		{
			if ( vTemp.get(9).equals("1") )//If is a template
			{
				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					) || (SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()))
				)
				{
					if ( vTemp.get(10).equals("0") )
					{
						out.println("<td align=\"center\">0</td>");
					}
					else
					{
						out.println("<td align=\"center\"><b><a href=\"javascript:ShowHierarchy(" + vTemp.get(0) + ");\">" + vTemp.get(10) + "&gt;&gt;</a></b></td>");
					}

					String sCarrier = null;
					if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
					{
						sCarrier = SessionData.getUser().getRefId();
					}
					Vector<Vector<String>> vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), DebisysConstants.ISO, "", true, sCarrier);

					if ( vAssignments.size() == 0 )
					{
						out.println("<td align=\"center\">0</td>");
					}
					else
					{
						out.println("<td align=\"center\"><a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&level=" + htLevels.get("ISO") + "&search=y&newrp=&filter=\">&nbsp;&nbsp;<b>" + vAssignments.get(0).get(1) + "</b>&nbsp;&nbsp;</a></td>");
					}
					out.println("<td align=\"center\">-</td>");
					if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
					{
						out.println("<td align=\"center\"><img src=\"images/grayadd.png\"></td>");
						out.println("<td align=\"center\"><img src=\"images/graycopy.png\"></td>");
					}
					else
					{
						out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=0&base=" + vTemp.get(0) + "\"><img src=\"images/greenadd.png\" border=\"0\"></a></td>");
						out.println("<td align=\"center\"><img src=\"images/graycopy.png\"></td>");
					}
					out.println("<td align=\"center\"><a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&level=" + htLevels.get("ISO") + "&search=y&newrp=\"><img src=\"images/greenarrow.png\" border=\"0\"></a></td>");
				}
				else
				{
					out.println("<td align=\"center\">-</td>");
					out.println("<td align=\"center\">-</td>");
					out.println("<td align=\"center\">-</td>");
					out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=0&base=" + vTemp.get(0) + "\"><img src=\"images/greenadd.png\" border=\"0\"></a></td>");
					out.println("<td align=\"center\"><img src=\"images/graycopy.png\"></td>");
					out.println("<td align=\"center\"><img src=\"images/grayarrow.png\"></td>");
				}
				out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=" + vTemp.get(0) + "&base=" + vTemp.get(1) + "\"><img src=\"images/yellowedit.png\" border=\"0\"></a></td>");
				out.println("<td align=\"center\"><img src=\"images/graydelete.png\"></td>");
			}
			else//Else is a RatePlan
			{
				out.println("<td align=\"center\">-</td>");

				String sCarrier = null;
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
				{
					sCarrier = SessionData.getUser().getRefId();
				}
				Vector<Vector<String>> vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), sAccessLevel, sRefId, vTemp.get(9).equals("1"), sCarrier);

				if ( vAssignments.size() == 0 )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\">");
					Iterator<Vector<String>> itCounts = vAssignments.iterator();
					while ( itCounts.hasNext() )
					{
						Vector<String> vItem = itCounts.next();
						out.println("<a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&level=" + htLevels.get(vItem.get(0)) + "&search=y&newrp=&filter=&isPlan=\">" + vItem.get(0) + "&nbsp;<b>" + vItem.get(1) + "</b></a><br/>");
						vItem = null;
					}
					itCounts = null;
					out.println("</td>");
				}
				out.println("<td align=\"center\">-</td>");
				out.println("<td align=\"center\"><img src=\"images/grayadd.png\"></td>");
				out.println("<td align=\"center\"><img src=\"images/graycopy.png\"></td>");
				if ( sAccessLevel.equals(DebisysConstants.REP) )
				{
					out.println("<td align=\"center\"><img src=\"images/grayarrow.png\"></td>");
				}
				else
				{
					out.println("<td align=\"center\"><a href=\"admin/rateplans/assig_rateplans.jsp?ratePlanId=1_" + vTemp.get(0) + "&newrp=1&isPlan=\"><img src=\"images/greenarrow.png\" border=\"0\"></a></td>");
				}
				out.println("<td align=\"center\"><a href=\"admin/rateplans/rateplan_edit.jsp?id=" + vTemp.get(0) + "&base=" + vTemp.get(1) + "\"><img src=\"images/yellowedit.png\" border=\"0\"></a></td>");
				out.println("<td align=\"center\"><img src=\"images/graydelete.png\"></td>");
			}
		}
%>
	</tr>
<%
		vTemp = null;
	}
	it = null;
%>
</table>
<script>

	function DoDeletePlan(sID)
	{
		if ( confirm("<%=Languages.getString("jsp.admin.rateplans.DeletePlanWarning",SessionData.getLanguage())%>") )
		{
			$.post('admin/rateplans/rateplan_ajax.jsp?action=deletePlan&id=' + sID + '&Random=' + Math.random(), ProcessDeletePlan);
		}
	}

	function ProcessDeletePlan(sData, sStatus)
	{
		if ( sStatus == "success" )
		{
			if (sData.match("-VALID-") == "-VALID-")
			{
				//alert("<%=Languages.getString("jsp.admin.rateplans.DeleteSuccess",SessionData.getLanguage())%>");
				if ( location.href.indexOf("?") > 0 )
				{
					location.href = location.href + '&Random=' + Math.random();
				}
				else
				{
					location.href = location.href + '?Random=' + Math.random();
				}
			}
			else
			{
				alert("<%=Languages.getString("jsp.admin.rateplans.DeleteFailed",SessionData.getLanguage())%>");
			}
		}
		else
		{
			alert("<%=Languages.getString("jsp.admin.rateplans.DeleteFailed",SessionData.getLanguage())%>");
		}
	}

	function ShowHierarchy(RatePlanID)
	{
		window.open("/support/admin/rateplans/rateplanhierarchy.jsp?id=" + RatePlanID, "RatePlanHierarchy", "left=" + ((screen.width - 800) / 2) + ", top=" + ((screen.height - 480) / 2) + ", width=800, height=480, menubar=0, toolbar=0, resizable=1, scrollbars=1");
	}

	var nBrowsePlanID = 0;
	function DoBrowse(nActor, nType, nChainLevel, nPlanID)
	{
		nBrowsePlanID = nPlanID;
		$.post('admin/rateplans/rateplan_ajax.jsp?action=browseActor&actor=' + nActor + '&type=' + nType + '&chain=' + nChainLevel + '&Random=' + Math.random(), ProcessBrowse);
	}

	function ProcessBrowse(sData, sStatus)
	{
		if ( sStatus == "success" )
		{
			if (sData.match("-VALID-") == "-VALID-")
			{
				if ( nBrowsePlanID == 0 )
				{
					nBrowsePlanID = "";
				}
				$("#txtFilterID").val(nBrowsePlanID);
				DoRefreshList();
			}
			else
			{
				alert("<%=Languages.getString("jsp.admin.rateplans.BrowseFailed",SessionData.getLanguage())%>");
			}
		}
		else
		{
			alert("<%=Languages.getString("jsp.admin.rateplans.BrowseFailed",SessionData.getLanguage())%>");
		}
	}
	RenderSortIcon("");
</script>
<%
}
else
{
	out.println("<br class=main/>" + Languages.getString("jsp.admin.rateplans.NoTemplatesFound",SessionData.getLanguage()) + "<br/><br/>");
}
%>
