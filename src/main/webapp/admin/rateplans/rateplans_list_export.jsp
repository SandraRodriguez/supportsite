<%@page import="com.debisys.rateplans.RatePlan"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.debisys.utils.NumberUtil"%>
<%@page import="com.debisys.users.SessionData"%>
<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.utils.DateUtil"%>
<%@page import="com.debisys.dateformat.DateFormat"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request" />

<%
response.addHeader("content-disposition", "filename=Templates_Plans.xls");
response.setCharacterEncoding("iso-8859-1");
response.setContentType("application/vnd.ms-excel");

String sRefId = null;
String sAccessLevel = null;
String sChainLevel = null;
String strDistChainType = SessionData.getProperty("dist_chain_type");

Vector<String> vTmp = (Vector<String>)SessionData.getPropertyObj("CurrentRatePlanActor");
if ( vTmp == null )
{
	return;
}
sRefId = vTmp.get(0);
sAccessLevel = vTmp.get(1);
sChainLevel = vTmp.get(2);

String sISODefault = request.getSession().getServletContext().getAttribute("iso_default").toString();
Vector<Vector<String>> vData = null;
if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
{
	vData = RatePlan.getNewRatePlansByActor(sISODefault, 1, Integer.MAX_VALUE, "Type", "D", "", "", "", "", SessionData.getUser().getRefId());
}
else
{
	vData = RatePlan.getNewRatePlansByActor(sRefId, 1, Integer.MAX_VALUE, "Type", "D", "", "", "", "", null);
}

%>
<table>
	<tr style="color:#deeef3;background-color:#005eb2;">
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Type",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.ID",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.InheritsFrom",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Level",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.CreatedBy",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.LastUpdate",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.AmountInheritance",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.AmountAssigned",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.AmountTerminals",SessionData.getLanguage())%></td>
	</tr>
<%
	int nCounter = 1;
	Iterator<Vector<String>> it = vData.iterator();
	Hashtable<String, String> htLevels = new Hashtable<String, String>();
	htLevels.put("ISO", DebisysConstants.ISO);
	htLevels.put("AGENT", DebisysConstants.AGENT);
	htLevels.put("SUBAGENT", DebisysConstants.SUBAGENT);
	htLevels.put("REP", DebisysConstants.REP);
	htLevels.put("MERCHANT", DebisysConstants.MERCHANT);

	boolean bAllowShowAllLevels = false;//Used when login as SuperUser of ISO-3 level and needs to see both Agent and SubAgent levels
	if (SessionData.getProperty("ref_id").equals(sISODefault) &&
		( (SessionData.getUser().isIntranetUser() && 
		SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
		) ||
		(SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()))
	)
	{
		bAllowShowAllLevels = true;
	}

	while ( it.hasNext() )
	{
		Vector<String> vTemp = it.next();
		if ( nCounter == 1 )
		{
			nCounter++;
			continue;//We must skip this iteration since the first row is the count of total records
		}

		String sNameTmp = "";
		while ( vTemp.get(3).length() > 40 )
		{
			sNameTmp += vTemp.get(3).substring(0, 40) + "<br/>";
			vTemp.set(3, vTemp.get(3).substring(40));
		}
		sNameTmp += vTemp.get(3);


		if ( !bAllowShowAllLevels && strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL) && ( vTemp.get(4).equals("AGENT") || vTemp.get(4).equals("SUBAGENT") ) )
		{
			continue;
		}

		if ( vTemp.get(9).equals("1") )
		{
			if ( (nCounter % 2) == 0 )
			{
				out.println("<tr style=\"background-color:#dddddd;\">");
			}
			else
			{
				out.println("<tr style=\"background-color:#ffffff;\">");
			}
		}
		else
		{
			if ( (nCounter % 2) == 0 )
			{
				out.println("<tr style=\"background-color:#c0cbf4;\">");
			}
			else
			{
				out.println("<tr style=\"background-color:#d9dcf4;\">");
			}
		}
%>
		<td><%=vTemp.get(9).equals("1")?Languages.getString("jsp.admin.rateplans.TypeTemplate",SessionData.getLanguage()):Languages.getString("jsp.admin.rateplans.TypePlan",SessionData.getLanguage())%></td>
		<td><%=vTemp.get(0)%></td>
		<td><%=sNameTmp%></td>
		<td><%=(vTemp.get(1).equals(""))?"[EMIDA]":vTemp.get(1)%></td>
		<td><%=vTemp.get(4)%></td>
		<td>
<%
		if ( vTemp.get(5).length() > 10 )
		{
			out.print("<span label=\"" + vTemp.get(5).replaceAll("\"", "'") + "\">[" + vTemp.get(5).substring(0, 10) + "]</span>");
		}
		else
		{
			out.print("[" + vTemp.get(5) + "]");
		}
		out.print("&nbsp;" + vTemp.get(6) + "<br/>&nbsp;" + Languages.getString("jsp.admin.rateplans.onDate",SessionData.getLanguage()) + "&nbsp;" + DateUtil.formatDateNoTime(vTemp.get(7), DateFormat.getDateNoTimeFormat()) + "&nbsp;");
%>
		</td>
		<td><%=vTemp.get(8)%></td>
<%
		if ( vTemp.get(2).equals(sRefId) )//If I'm the Owner of this item
		{
			if ( vTemp.get(9).equals("1") )//If is a template
			{
				if ( vTemp.get(10).equals("0") )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\">" + vTemp.get(10) + "</td>");
				}

				Vector<Vector<String>> vAssignments = new Vector<Vector<String>>();
				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					)
				)
				{
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), htLevels.get(vTemp.get(4)), "", true, null);
				}
				else
				{
					String sCarrier = null;
					if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
					{
						sCarrier = SessionData.getUser().getRefId();
					}
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), sAccessLevel, sRefId, false, sCarrier);
				}

				if ( vAssignments.size() == 0 )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\">" + vAssignments.get(0).get(1) + "</td>");
				}
				out.println("<td align=\"center\">-</td>");
			}
			else//Else is a RatePlan
			{
				out.println("<td align=\"center\">-</td>");
				boolean bHasEnabledMerchants = false;
				Vector<Vector<String>> vAssignments = new Vector<Vector<String>>();
				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					)
				)
				{
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), "", "", false, null);
				}
				else
				{
					String sCarrier = null;
					if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
					{
						sCarrier = SessionData.getUser().getRefId();
					}
					vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), sAccessLevel, sRefId, false, sCarrier);
				}

				if ( vAssignments.size() == 0 )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\">");
					Iterator<Vector<String>> itCounts = vAssignments.iterator();
					while ( itCounts.hasNext() )
					{
						Vector<String> vItem = itCounts.next();
						out.println(vItem.get(0) + "&nbsp;<b>" + vItem.get(1) + "</b><br/>");
						vItem = null;
					}
					itCounts = null;
					bHasEnabledMerchants = !vAssignments.get(vAssignments.size() - 1).get(2).equals("0");
					out.println("</td>");
				}

				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					)
				)
				{
					if ( vTemp.get(12).equals("0") )
					{
						out.println("<td align=\"center\">0</td>");
					}
					else
					{
					out.println("<td align=\"center\">" + vTemp.get(12) + "</td>");
					}
				}
				else
				{
					if ( vTemp.get(11).equals("0") )
					{
						out.println("<td align=\"center\">0</td>");
					}
					else
					{
					out.println("<td align=\"center\">" + vTemp.get(11) + "</td>");
					}
				}
			}
		}
		else//If this item was assigned from someone above me
		{
			if ( vTemp.get(9).equals("1") )//If is a template
			{
				if (SessionData.getProperty("ref_id").equals(sISODefault) &&
					( (SessionData.getUser().isIntranetUser() && 
					SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
					) || (SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()))
				)
				{
					if ( vTemp.get(10).equals("0") )
					{
						out.println("<td align=\"center\">0</td>");
					}
					else
					{
						out.println("<td align=\"center\">" + vTemp.get(10) + "</td>");
					}

					String sCarrier = null;
					if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
					{
						sCarrier = SessionData.getUser().getRefId();
					}
					Vector<Vector<String>> vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), DebisysConstants.ISO, "", true, sCarrier);

					if ( vAssignments.size() == 0 )
					{
						out.println("<td align=\"center\">0</td>");
					}
					else
					{
						out.println("<td align=\"center\">" + vAssignments.get(0).get(1) + "</td>");
					}
					out.println("<td align=\"center\">-</td>");
				}
				else
				{
					out.println("<td align=\"center\">-</td>");
					out.println("<td align=\"center\">-</td>");
					out.println("<td align=\"center\">-</td>");
				}
			}
			else//Else is a RatePlan
			{
				out.println("<td align=\"center\">-</td>");

				String sCarrier = null;
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
				{
					sCarrier = SessionData.getUser().getRefId();
				}
				Vector<Vector<String>> vAssignments = RatePlan.getRatePlanAssignments(vTemp.get(0), sAccessLevel, sRefId, vTemp.get(9).equals("1"), sCarrier);

				if ( vAssignments.size() == 0 )
				{
					out.println("<td align=\"center\">0</td>");
				}
				else
				{
					out.println("<td align=\"center\">");
					Iterator<Vector<String>> itCounts = vAssignments.iterator();
					while ( itCounts.hasNext() )
					{
						Vector<String> vItem = itCounts.next();
						out.println(vItem.get(0) + "&nbsp;<b>" + vItem.get(1) + "</b><br/>");
						vItem = null;
					}
					itCounts = null;
					out.println("</td>");
				}
				out.println("<td align=\"center\">-</td>");
			}
		}
		out.println("</tr>");
		vTemp = null;
		nCounter++;
	}
	it = null;
%>
</table>
