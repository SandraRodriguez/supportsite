<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<%
int section=7;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
  String strMessage = request.getParameter("message");
  if (strMessage == null)
  {
    strMessage="";
  }


  Vector vecRatePlans = new Vector();

  vecRatePlans = RatePlan.getIsoRepRatePlans(SessionData);

%>
<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    	<td width="18" height="20" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	    <td class="formAreaTitle" align="left" width="2000"><%=Languages.getString("jsp.admin.rateplans.rep_rates.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20" align="right"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
				<tr>
			    	<td >
    				    

<%
if (strMessage.equals("1"))
{
  out.println("<tr bgcolor=ffffff><td align=left class=main><img src=images/information.png border=0 valign=middle><font color=000000>"+Languages.getString("jsp.admin.rateplans.rep_rates.success",SessionData.getLanguage())+"</td></tr>");
}
%>       
<%

if (vecRatePlans != null && vecRatePlans.size() > 0)
{
  %>

            <%=Languages.getString("jsp.admin.rateplans.rep_rates.instructions",SessionData.getLanguage())%>
            <table cellspacing="1" cellpadding="0" border="0" align="left">
            <tr>
              <td class=rowhead2 nowrap>#</td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.rateplans.index.rateplan",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.rateplans.rep_rates.delete",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
                  Iterator it = vecRatePlans.iterator();
                  int intEvenOdd = 1;
                  int counter=1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + counter++ + "</td>" +
                                "<td nowrap><a href=admin/rateplans/edit_rep_rate.jsp?repRatePlanId=" + vecTemp.get(0).toString() + ">" + HTMLEncoder.encode(vecTemp.get(1).toString())+ "</a></td>" +
                                "<td><a href=admin/rateplans/delete_rep_rate.jsp?repRatePlanId=" + vecTemp.get(0).toString() + ">"+Languages.getString("jsp.admin.rateplans.rep_rates.delete",SessionData.getLanguage())+"</a></td></tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecRatePlans.clear();
            %>

            

<%

  }
  else
  {
    %>
     	<tr>
	        <td class="main">
          <br><br><font color=ff0000><%=Languages.getString("jsp.admin.rateplans.rep_rates.error1",SessionData.getLanguage())%></font><br><br><br>
          </td>
      </tr>

      <%
  }
%>     <br/>
		<tr>
	        <td><br/>
                       <form method="get" action="/support/admin/rateplans/add_rep_rate1.jsp">
             <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.rateplans.rep_rates.submit",SessionData.getLanguage())%>">
             </form>          </td>
      </tr>
                 
                 </table>
                 



            	  </td>
               </tr>	
           </table>
		</td>
	</tr>
</table>






<%@ include file="/includes/footer.jsp" %>