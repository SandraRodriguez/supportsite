<%@ page import="com.debisys.rateplans.RatePlan,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%
	if(request.getParameter("Download") != null)
	{
		String sURL = "";
		sURL = RatePlan.downloadRatePlans(SessionData, application, request.getParameter("RatePlanId"));
		response.sendRedirect(sURL);
	}
%>