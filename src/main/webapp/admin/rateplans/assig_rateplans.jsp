<%@ page import="com.debisys.customers.CustomerSearch,
                 java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.rateplans.RatePlan"%>
<%@page import="com.debisys.utils.StringUtil"%>
<%
int section=7;
int section_page=3;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>

<%@ include file="/includes/security.jsp"%>
<%
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String level = request.getParameter("level");

if (request.getParameter("assign") != null && request.getParameter("data") != null  && level != null)
{
	String[] sRatePlanData = request.getParameter("assign").split("_");
	long assign = Long.parseLong(sRatePlanData[1]);
	String data = request.getParameter("data").replaceAll(",0", "");
	String unch = request.getParameter("unch");
	if (assign > 0 && level.length() > 0)
	{
		String result = RatePlan.assignRatePlan(SessionData, application, assign, data, unch, level, sRatePlanData[0].equals("1"));
		out.print(result);
	}
}
else
{
%>
<%@ include file="/includes/header.jsp"%>
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<%
String sAux;
String sRefId = null;
String sAccessLevel = null;
String sChainLevel = null;
if ( SessionData.getPropertyObj("CurrentRatePlanActor") != null )
{
	Vector<String> vTmp = (Vector<String>)SessionData.getPropertyObj("CurrentRatePlanActor");
	sRefId = vTmp.get(0);
	sAccessLevel = vTmp.get(1);
	sChainLevel = vTmp.get(2);
}
else
{
	sRefId = SessionData.getProperty("ref_id");
	sAccessLevel = strAccessLevel;
	sChainLevel = strDistChainType;
	Vector<String> vTmp = new Vector<String>();
	vTmp.add(sRefId);
	vTmp.add(sAccessLevel);
	vTmp.add(sChainLevel);
	SessionData.setPropertyObj("CurrentRatePlanActor", vTmp);
}
%>
<script>
	$(document).ready(function (){
		$(':checkbox').click(function(){$('#savechanges').show();});
		$(':checkbox').change(function(){$('#savechanges').show();});
	});
	function saveRatePlans(){
		var opts;
		$('#divResults').css('display', 'none');
		opts = document.getElementsByName('rep_rateplan');
		if (opts && opts.length > 0)
		{
			var ch = '', unch = '';
			for (var i=0; i < opts.length; i++)
			{
				if (opts[i].checked && opts[i].getAttribute('originalValue') == 0)
				{
					ch += opts[i].value + ',';
				}
				else if (!opts[i].checked && opts[i].getAttribute('originalValue') == 1)
				{
					unch += opts[i].value + ',';
				}
			}
			var ts = '&rand=' + Math.round(new Date().getTime() / 1000) + Math.random();
			var url =  'admin/rateplans/assig_rateplans.jsp?assign=<%=RatePlan.getRatePlanId()%>&level=<%=level%>&data=' + ch + '0&unch=' + unch + '0' + ts + '&newrp=';
			$('#divResults').css('display', 'block');
			$('#divResults').html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
			$.post(url, handleResponse);
		}
	}
	function handleResponse(responseText, textStatus, XMLHttpRequest)
	{
		if (responseText.match("OK") == "OK")
		{
			$("#savechanges").hide();
			alert("<%=Languages.getString("jsp.admin.rateplans.updateOK",SessionData.getLanguage())%>");
			document.getElementById("entityform").submit();
		}
		else
		{
			alert("Error!");
		}
	}
	function showHelp(){
		var p = window.open("", "helpwindow", "width=400,height=210,scrollbars=yes");
		p.document.write('<head><title><%=Languages.getString("jsp.admin.rateplans.instructions",SessionData.getLanguage())%></title></head>');
		p.document.write("<%=Languages.getString("jsp.admin.rateplans.helpassign",SessionData.getLanguage())%>");
	}
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
<%
	String isNewRatePlanModel = null;//When null we return both old and new Rate Plans
	if ( (SessionData.getUser().isIntranetUser() && 
		 SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
		 || (SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()))
		)
	{
		isNewRatePlanModel = "1";//Return only New Model-Rate Plans
	}
	else if ( !SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS) )
	{
		isNewRatePlanModel = "0";//Return only old Model-Rate Plans
	}

	level = request.getParameter("level");
	if (level == null || level.equals("") || level.equals("null"))
	{
		level = SessionData.getProperty("default_level");
	}

	if (level != null && !level.equals("") && !level.equals("null"))
	{
		Vector<Vector<Vector<String>>> vecRatePlans = null;
		//"newrp" is used when this page is called from the new RatePlans page, this in order to disable both the RatePlan and the Actor lists
		if ( request.getParameter("newrp") != null && request.getParameter("newrp").equals("1") )
		{
			if ( request.getParameter("isPlan") != null )
			{
				sAux = sRefId;
				String sCarrier = null;
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = application.getAttribute("iso_default").toString();
					sCarrier = sAux;
				}
				vecRatePlans = RatePlan.getISORatePlansG(sRefId, isNewRatePlanModel, DebisysConstants.MERCHANT, sCarrier);
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = sAux;
				}
				sCarrier = null;
				sAux = null;
			}
			else
			{
				sAux = sRefId;
				String sCarrier = null;
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = application.getAttribute("iso_default").toString();
					sCarrier = sAux;
				}
				vecRatePlans = RatePlan.getISORatePlansG(sRefId, isNewRatePlanModel, strAccessLevel, sCarrier);
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = sAux;
				}
				sCarrier = null;
				sAux = null;
			}
		}
		else
		{
			if ( request.getParameter("isPlan") != null )
			{
				sAux = sRefId;
				String sCarrier = null;
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = application.getAttribute("iso_default").toString();
					sCarrier = sAux;
				}
				vecRatePlans = RatePlan.getISORatePlansG(sRefId, isNewRatePlanModel, DebisysConstants.MERCHANT, sCarrier);
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = sAux;
				}
				sCarrier = null;
				sAux = null;
			}
			else
			{
				sAux = sRefId;
				String sCarrier = null;
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = application.getAttribute("iso_default").toString();
					sCarrier = sAux;
				}
				vecRatePlans = RatePlan.getISORatePlansG(sRefId, isNewRatePlanModel, level, sCarrier);
				if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
				{
					sRefId = sAux;
				}
				sCarrier = null;
				sAux = null;
			}
		}
%>
	<tr background="images/top_blue.gif">
		<td width="18" height="20">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">
<%
		if ( request.getParameter("newrp") != null )
		{
			out.println("&nbsp;" + Languages.getString("jsp.admin.rateplans.AssignTemplatesPlans",SessionData.getLanguage()).toUpperCase() + " " + SessionData.getProperty("company_name").toUpperCase());
		}
		else
		{
			out.println("&nbsp;" + Languages.getString("jsp.admin.rateplans.assign.title",SessionData.getLanguage()).toUpperCase() + " " + SessionData.getProperty("company_name").toUpperCase());
		}
%>
		</td>
		<td width="12" height="20">
			<img src="images/top_right_blue.gif">
		</td>
	</tr>
	<tr>
		<td colspan="3" bgcolor="#FFFFFF">
			<form id="entityform" name="entityform" method="get" action="admin/rateplans/assig_rateplans.jsp">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
					<tr>
						<td class="formArea2">
							<br/>
							<table border="0">
								<tr class="main">
									<td nowrap="nowrap" valign="top" colspan="3">
										<a style="font-weight:bold;" href="javascript:showHelp();"><%=Languages.getString("jsp.admin.rateplans.instructions",SessionData.getLanguage())%></a>
									</td>
								</tr>
								<tr class="main">
									<td nowrap="nowrap" valign="top" colspan="3">
										<%=Languages.getString("jsp.admin.rateplans.selectrateplan",SessionData.getLanguage())%>
<%
											if (level.toString().equals(DebisysConstants.ISO))
											{
												out.print(Languages.getString("jsp.includes.menu.iso",SessionData.getLanguage()));
											}
											if (level.toString().equals(DebisysConstants.AGENT))
											{
												out.print(Languages.getString("jsp.includes.menu.agents",SessionData.getLanguage()));
											}
											if (level.toString().equals(DebisysConstants.SUBAGENT))
											{
												out.print(Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage()));
											}
											if (level.toString().equals(DebisysConstants.REP))
											{
												out.print(Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage()));
											}
											if (level.toString().equals(DebisysConstants.MERCHANT))
											{
												out.print(Languages.getString("jsp.includes.menu.merchants",SessionData.getLanguage()));
											}
%>
										:
									</td>
								</tr>
								<tr class="main">
									<td valign="top" nowrap="nowrap" colspan="1">
										<select id="ratePlanId" name="ratePlanId" <%=(request.getParameter("newrp") != null)?"disabled":""%>>
											<option value=""></option>
<%
										boolean hasItems = false;

										if ( vecRatePlans.get(0).size() > 0 )//New-model templates
										{
											hasItems = true;
											out.println("<optgroup label='" + Languages.getString("jsp.admin.rateplans.newmodel_templates",SessionData.getLanguage()) + "'>");
											Iterator<Vector<String>> it = vecRatePlans.get(0).iterator();
											while (it.hasNext())
											{
												Vector<String> vecTemp = it.next();
												String selected = "";
												if ( vecTemp.get(1).length() > 100 )
												{
													vecTemp.set(1, vecTemp.get(1).substring(0, 100) + "...");
												}
												if ( vecTemp.get(2).length() > 30 )
												{
													vecTemp.set(2, vecTemp.get(2).substring(0, 30) + "...");
												}
												if (StringUtil.toString(request.getParameter("ratePlanId")).equals("1_" + vecTemp.get(0)))
												{
													selected = "selected";
												}
												out.println("<option value='1_" + vecTemp.get(0) + "' "+ selected + ">" + HTMLEncoder.encode(vecTemp.get(1)) + "[" + vecTemp.get(2) + "]" + "</option>");
											}
											it = null;
											out.println("</optgroup>");
										}

										if ( vecRatePlans.get(1).size() > 0 )//New-model rate plans
										{
											hasItems = true;
											out.println("<optgroup label='" + Languages.getString("jsp.admin.rateplans.newmodel_plans",SessionData.getLanguage()) + "'>");
											Iterator<Vector<String>> it = vecRatePlans.get(1).iterator();
											while (it.hasNext())
											{
												Vector<String> vecTemp = it.next();
												String selected = "";
												if ( vecTemp.get(1).length() > 100 )
												{
													vecTemp.set(1, vecTemp.get(1).substring(0, 100) + "...");
												}
												if ( vecTemp.get(2).length() > 30 )
												{
													vecTemp.set(2, vecTemp.get(2).substring(0, 30) + "...");
												}
												if (StringUtil.toString(request.getParameter("ratePlanId")).equals("1_" + vecTemp.get(0)))
												{
													selected = "selected";
												}
												out.println("<option value='1_" + vecTemp.get(0) + "' "+ selected + ">" + HTMLEncoder.encode(vecTemp.get(1)) + "[" + vecTemp.get(2) + "]" + "</option>");
											}
											it = null;
											out.println("</optgroup>");
										}

										if ( (isNewRatePlanModel == null || isNewRatePlanModel.equals("0")) && vecRatePlans.get(2).size() > 0 )//Old-model rate plans
										{
											hasItems = true;
											if ( SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS) )
											{
												out.println("<optgroup label='" + Languages.getString("jsp.admin.rateplans.oldmodel",SessionData.getLanguage()) + "'>");
											}
											Iterator<Vector<String>> it = vecRatePlans.get(2).iterator();
											while (it.hasNext())
											{
												Vector<String> vecTemp = it.next();
												String selected = "";
												if ( vecTemp.get(1).length() > 100 )
												{
													vecTemp.set(1, vecTemp.get(1).substring(0, 100) + "...");
												}
												if ( vecTemp.get(2).length() > 30 )
												{
													vecTemp.set(2, vecTemp.get(2).substring(0, 30) + "...");
												}
												if (StringUtil.toString(request.getParameter("ratePlanId")).equals("0_" + vecTemp.get(0)))
												{
													selected = "selected";
												}
												out.println("<option value='0_" + vecTemp.get(0) + "' "+ selected + ">" + HTMLEncoder.encode(vecTemp.get(1)) + "[" + vecTemp.get(2) + "]" + "</option>");
											}
											it = null;
											if ( SessionData.checkPermission(DebisysConstants.PERM_MANAGE_NEW_RATEPLANS) )
											{
												out.println("</optgroup>");
											}
										}
										if ( !hasItems )
										{
											out.println("<option value='' selected>" + Languages.getString("jsp.admin.rateplans.noRatePlansForLevel",SessionData.getLanguage()) + "</option>");
										}
%>
										</select>
									</td>
									<td>
										<select name="level" onchange="NavigateLevel(this.value);" <%=((request.getParameter("newrp") != null) && request.getParameter("newrp").equals(""))?"disabled":""%>>
<%
											if ( isNewRatePlanModel != null && isNewRatePlanModel.equals("1") )
											{
%>
											<option value="<%=DebisysConstants.ISO%>" <%=(level.equals(DebisysConstants.ISO))?"selected":""%>><%=Languages.getString("jsp.includes.menu.iso",SessionData.getLanguage())%></option>
											<option value="<%=DebisysConstants.AGENT%>" <%=(level.equals(DebisysConstants.AGENT))?"selected":""%>><%=Languages.getString("jsp.includes.menu.agents",SessionData.getLanguage())%></option>
											<option value="<%=DebisysConstants.SUBAGENT%>" <%=(level.equals(DebisysConstants.SUBAGENT))?"selected":""%>><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%></option>
											<option value="<%=DebisysConstants.REP%>" <%=(level.equals(DebisysConstants.REP))?"selected":""%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>
<%
											}
											else if (sAccessLevel.equals(DebisysConstants.ISO))
											{//ISO
												if (sChainLevel.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
												{//ISO 5 LEVELS
%>
											<option value="<%=DebisysConstants.AGENT%>" <%=(level.equals(DebisysConstants.AGENT))?"selected":""%>><%=Languages.getString("jsp.includes.menu.agents",SessionData.getLanguage())%></option>
											<option value="<%=DebisysConstants.SUBAGENT%>" <%=(level.equals(DebisysConstants.SUBAGENT))?"selected":""%>><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%></option>
											<option value="<%=DebisysConstants.REP%>" <%=(level.equals(DebisysConstants.REP))?"selected":""%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>
<%
												}
												else
												{//ISO 3 LEVELS
%>
											<option value="<%=DebisysConstants.REP%>" <%=(level.equals(DebisysConstants.REP))?"selected":""%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>
<%
												}
											}
											else if (sAccessLevel.equals(DebisysConstants.AGENT))
											{//AGENT
%>
											<option value="<%=DebisysConstants.SUBAGENT%>" <%=(level.equals(DebisysConstants.SUBAGENT))?"selected":""%>><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%></option>
											<option value="<%=DebisysConstants.REP%>" <%=(level.equals(DebisysConstants.REP))?"selected":""%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>
<%
											}
											else if (sAccessLevel.equals(DebisysConstants.SUBAGENT))
											{//SUBAGENT
%>
											<option value="<%=DebisysConstants.REP%>" <%=(level.equals(DebisysConstants.REP))?"selected":""%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>
<%
											}
%>
										</select>
										<script>
										function NavigateLevel(sValue)
										{
											if ( location.href.indexOf('?') > 0 )
											{
												var vParams = location.href.substring(location.href.indexOf('?') + 1, location.href.length).split('&');
												var i, s;
												for ( i = 0; i < vParams.length; i++ )
												{
													var vItem = vParams[i].split('=');
													if ( vItem[0] == "level" )
													{
														vItem[1] = sValue;
														vParams[i] = vItem.join('=');
														s = location.href.substring(0, location.href.indexOf('?') + 1) + vParams.join('&');
														if ( location.href.indexOf('isPlan=') == -1 )
														{
															location.href = s.replace("&search=y", "");
														}
														else
														{
															location.href = s;
														}
														return;
													}
												}
												location.href += "&level=" + sValue;
											}
											else
											{
												location.href += "?level=" + sValue;
											}
										}
										</script>
									</td>
									<td valign="top" align="left">
<%
	if ( request.getParameter("newrp") != null )
	{
%>
										<input type="hidden" name="ratePlanId" value="<%=request.getParameter("ratePlanId")%>">
<%
		if ( request.getParameter("level") != null )
		{
%>
										<input type="hidden" name="level" value="<%=request.getParameter("level")%>">
<%
		}
		if ( request.getParameter("isPlan") != null )
		{
%>
										<input type="hidden" name="isPlan" value="<%=request.getParameter("isPlan")%>">
<%
		}
%>
										<input type="hidden" name="newrp" value="<%=request.getParameter("newrp")%>">
<%
	}
%>
										<input type="hidden" name="search" value="y">
										<input type="hidden" name="rand" value="<%=Math.random()%>">
<%
	if ( request.getParameter("filter") == null )
	{
%>
										<input type="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
<%
	}
%>
									</td>
									<td valign="top" align="left">
										<input type="button" id="savechanges" style="display:none;" value="<%=Languages.getString("jsp.admin.savechanges",SessionData.getLanguage())%>" onclick="saveRatePlans();">
										<span id="resp" style="display:none;"></span>
									</td>
								</tr>
								<tr class="main">
									<td colspan="3">
<%
	if ( !level.equals(DebisysConstants.ISO) &&
		request.getParameter("filter") == null &&
		( (SessionData.getUser().isIntranetUser() && 
		 SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_NEW_RATEPLANS))
		 || (SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()))
		)
	)
	{
		String sSupLevel = "";
		out.println(Languages.getString("jsp.admin.rateplans.FilterBy",SessionData.getLanguage()));
		if ( level.equals(DebisysConstants.AGENT) )
		{
			sSupLevel = DebisysConstants.ISO;
		out.print(Languages.getString("jsp.includes.menu.iso",SessionData.getLanguage()));
		}
		else if ( level.equals(DebisysConstants.SUBAGENT) )
		{
			sSupLevel = DebisysConstants.AGENT;
		out.print(Languages.getString("jsp.includes.menu.agents",SessionData.getLanguage()));
		}
		else if ( level.equals(DebisysConstants.REP) )
		{
			sSupLevel = DebisysConstants.SUBAGENT;
		out.print(Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage()) + "/" + Languages.getString("jsp.includes.menu.iso",SessionData.getLanguage()) + "(3)");
		}
		else if ( level.equals(DebisysConstants.MERCHANT) )
		{
			sSupLevel = DebisysConstants.REP;
		out.print(Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage()));
		}
%>
										&nbsp;<select name="ddlActorFilter">
<%
		RatePlan.setRatePlanId("0");
		String sSelected = StringUtil.toString(request.getParameter("ddlActorFilter"));
		String sCarrier2 = null;
		if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
		{
			sCarrier2 = SessionData.getUser().getRefId();
		}
		Vector<Vector<String>> vActorFilter = RatePlan.getIsoReps(SessionData, sSupLevel, true, "", sCarrier2);

		if ( sSupLevel.equals(DebisysConstants.SUBAGENT) && vActorFilter.size() > 0 )
		{
			out.println("<optgroup label=\"" + Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage()) + "\">");
		}
		Iterator<Vector<String>> itActorFilter = vActorFilter.iterator();
		while ( itActorFilter.hasNext() )
		{
			Vector<String> vTmp = itActorFilter.next();
			out.println("<option value=\"" + vTmp.get(1) + "\" " + (vTmp.get(1).equals(sSelected)?"selected":"") + ">" + vTmp.get(0) + "</option>");
			vTmp = null;
		}
		itActorFilter = null;

		//When showing Reps we need to filter by both SubAgents and ISOs of 3 levels
		if ( sSupLevel.equals(DebisysConstants.SUBAGENT) )
		{
			if ( vActorFilter.size() > 0 )
			{
				out.println("</optgroup>");
			}

			String sCarrier = null;
			if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
			{
				sCarrier = SessionData.getUser().getRefId();
			}
			vActorFilter = RatePlan.getIsoReps(SessionData, "ISO3", true, "", sCarrier);

			if ( vActorFilter.size() > 0 )
			{
				out.println("<optgroup label=\"" + Languages.getString("jsp.includes.menu.iso",SessionData.getLanguage()) + " (3)\">");
			}
			itActorFilter = vActorFilter.iterator();
			while ( itActorFilter.hasNext() )
			{
				Vector<String> vTmp = itActorFilter.next();
				out.println("<option value=\"" + vTmp.get(1) + "\" " + (vTmp.get(1).equals(sSelected)?"selected":"") + ">" + vTmp.get(0) + "</option>");
				vTmp = null;
			}
			itActorFilter = null;
			if ( vActorFilter.size() > 0 )
			{
				out.println("</optgroup>");
			}
		}
		vActorFilter = null;
%>
										</select>
<%
	}
%>
									</td>
								</tr>
								<tr>
									<td colspan="3">
<%
	if ( request.getParameter("newrp") != null )
	{
%>
										</form><form action="admin/rateplans/rateplans.jsp"><input type="submit" value="<%=Languages.getString("jsp.admin.rateplans.ReturnToManage",SessionData.getLanguage()) + Languages.getString("jsp.includes.menu.rateplan_manage",SessionData.getLanguage())%>">
<%
	}
%>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
<%
	Vector vecSearchResults = null;
	if (request.getParameter("search") != null)
	{
		if ( request.getParameter("search").equals("y") && !request.getParameter("ratePlanId").equals(""))
		{
			String[] sRatePlanData = request.getParameter("ratePlanId").split("_");
			RatePlan.setRatePlanId(sRatePlanData[1]);

			String sCarrier = null;
			if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) && sRefId.equals(SessionData.getUser().getRefId()) )
			{
				sCarrier = SessionData.getUser().getRefId();
			}
			vecSearchResults = RatePlan.getIsoReps(SessionData, level, sRatePlanData[0].equals("1"), StringUtil.toString(request.getParameter("ddlActorFilter")), sCarrier);
		}
	}
	
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
		%>
		<tr>
			<td colspan="3">
				<div id="divResults" style="<%=(vecSearchResults.size() > 30)?"height:650px;overflow:auto;display:block":"display:block"%>">
				<table border="0" cellpadding="1" cellspacing="1" width="100%">
					<tr>
						<td class="rowhead2" style="text-align:center;vertical-align:top;">#</td>
						<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.businessName",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.repId",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" style="text-align:center;vertical-align:top;">
							<%=(request.getParameter("filter") == null)?Languages.getString("jsp.admin.rateplans.assigned_unassigned",SessionData.getLanguage()).toUpperCase():Languages.getString("jsp.admin.rateplans.AlreadyAssigned",SessionData.getLanguage()).toUpperCase()%><br/>
<%
	if ( request.getParameter("filter") == null )
	{
%>
							<input type="checkbox" onclick="ToggleReps(this.checked);">
<%
	}
%>
						</td>
					</tr>
<%
		Iterator it = vecSearchResults.iterator();
		int i = 1;
		while (it.hasNext())
		{
			Vector vecTemp = (Vector) it.next();
			if ( (request.getParameter("filter") != null) && vecTemp.get(2).equals("0") )
			{
				continue;
			}
			String selected ="";
%>
					<tr class="row<%=((i%2)+1)%>">
						<td><%=i++%></td>
						<td nowrap="nowrap"><%=vecTemp.get(0)%></td>
						<td nowrap="nowrap"><%=vecTemp.get(1)%></td>
						<td align="center" valign="middle">
<%
			if ( request.getParameter("filter") == null )
			{
				if ( vecTemp.get(3).equals("0") )
				{
%>
							<input type="checkbox" id="rep_rateplan" name="rep_rateplan" value="<%=vecTemp.get(1)%>" <%=(vecTemp.get(2).equals("1"))?"checked":""%> originalValue=<%=(vecTemp.get(2).equals("1"))?1:0%>>
<%
				}
				else
				{
%>
							<b><%=Languages.getString("jsp.admin.rateplans.AlreadyAssigned",SessionData.getLanguage())%></b>&nbsp;<img src="images/questionmark.png" title="<%=Languages.getString("jsp.admin.rateplans.AlreadyAssignedToolTip",SessionData.getLanguage())%>">
<%
				}
			}
			else
			{
%>
							<input type="checkbox" checked="checked" disabled="disabled">
<%
			}
%>
						</td>
					</tr>
	
	<%		
		}
		vecSearchResults.clear();
	%>
				</table>
				<script>
				function ToggleReps(bValue)
				{
					var i = 0;
					for ( i; i < document.getElementsByName('rep_rateplan').length; i++ )
					{
						document.getElementsByName('rep_rateplan')[i].checked = bValue;
					}
				}
				</script>
				</div>
			</td>
		</tr>
	<%
		}
	}//End of if (level != null && !level.equals("") && !level.equals("null"))
	else
	{
%>
	<tr><td colspan="3"><%=Languages.getString("jsp.admin.rateplans.selectalevel",SessionData.getLanguage())%></td></tr>
<%
	}
%>
</table>
<%@ include file="/includes/footer.jsp"%>
<%
}//End of else of if (request.getParameter("assign") != null && request.getParameter("data") != null  && level != null)
%>
