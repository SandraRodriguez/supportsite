<%@ page import="java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 java.util.Hashtable" %>
<%
int section=7;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
  Hashtable hashRatePlanErrors = new Hashtable();
  Hashtable hashRepRates = new Hashtable();
  Vector vecSearchResults = new Vector();
  Vector vecCurrentProducts = new Vector();
  Iterator it_1;

  //all available products
  vecSearchResults = RatePlan.getAvailableRepRatePlanProducts(SessionData);


  if (request.getParameter("submitted") != null)
  {
    try
    {
      if (request.getParameter("submitted").equals("y"))
      {
        String[] productIds = request.getParameterValues("productId");
        if (productIds != null)
        {
          String strProductId = "";
          for(int i=0; i<productIds.length;i++)
          {
            strProductId = productIds[i];
            if (strProductId !=null && !strProductId.equals(""))
            {
              if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
              {
                if (request.getParameter("iso_" + strProductId) != null
                    &&request.getParameter("agent_" + strProductId) != null
                    &&request.getParameter("subagent_" + strProductId) != null)
                {
                  Vector vecRepRates = new Vector();
                  vecRepRates.add(0,request.getParameter("iso_" + strProductId));
                  vecRepRates.add(1,request.getParameter("agent_" + strProductId));
                  vecRepRates.add(2,request.getParameter("subagent_" + strProductId));
                  if (!hashRepRates.containsKey(strProductId))
                  {
                    hashRepRates.put(strProductId, vecRepRates);
                  }

                }
              }
              else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
              {
                if (request.getParameter("iso_" + strProductId) != null)
                {
                  Vector vecRepRates = new Vector();
                  vecRepRates.add(0,request.getParameter("iso_" + strProductId));
                  if (!hashRepRates.containsKey(strProductId))
                  {
                    hashRepRates.put(strProductId, vecRepRates);
                  }
                }
              }
            }

          }

        }
        //do this to bypass the rateplan name check
        RatePlan.setRatePlanName("a");
        if(RatePlan.validateRepRatePlan(SessionData, hashRepRates))
        {
          RatePlan.updateRepRatePlanProducts(SessionData, hashRepRates);
          response.sendRedirect("/support/admin/rateplans/edit_rep_rate.jsp?repRatePlanId="+RatePlan.getRepRatePlanId()+"&message=1");
        }
        else
        {
          hashRatePlanErrors = RatePlan.getErrors();
        }

      }

    }
    catch (Exception e)
    {
     e.printStackTrace();
    }
  }
  else
  {
      vecCurrentProducts = RatePlan.getRepRatePlan(SessionData);
      //convert vector to has for easy lookup
      it_1 = vecCurrentProducts.iterator();
      while (it_1.hasNext())
      {
        Vector vecTemp = null;
        vecTemp = (Vector) it_1.next();
        String strProductId = vecTemp.get(1).toString();
        if (!hashRepRates.containsKey(strProductId))
        {
          hashRepRates.put(strProductId, vecTemp);
        }
      }
  }




%>
<%@ include file="/includes/header.jsp" %>

 <TABLE cellSpacing=0 cellPadding=0 width=750px border=0>
   <TBODY>
   <TR>
     <TD align=left width="1%" 
     background=images/top_blue.gif><IMG height=20 
       src="images/top_left_blue.gif" width=18></TD>
     <TD class=formAreaTitle 
     background=images/top_blue.gif width="3000">&nbsp;<B><%=Languages.getString("jsp.admin.rateplans.edit_products.title",SessionData.getLanguage()).toUpperCase()%></B></TD>
     <TD align=right width="1%" 
     background=images/top_blue.gif><IMG height=20 
       src="images/top_right_blue.gif" width=18></TD></TR>
       <TR>
     <TD colSpan=3>
       <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
       bgColor=#fffcdf class="backceldas">
         <TBODY>
         <TR class="backceldas">
           <TD width=1 bgColor=#003082><IMG 
             src="images/trans.gif" width=1></TD>
           <TD vAlign=top align=middle bgColor=#ffffff>
             <TABLE width="100%" border=0 
             align=center cellPadding=2 cellSpacing=0 class="backceldas">
               <TBODY>
               <TR>
                 <TD width=18>&nbsp;</TD>
                 <TD class=main colspan="3"><BR>
                 	       
          <%=Languages.getString("jsp.admin.rateplans.edit_products.instructions",SessionData.getLanguage())%><br>
<%
if (hashRatePlanErrors != null && hashRatePlanErrors.size() > 0)
{
  out.println("<font color=ff0000>"+Languages.getString("jsp.index.error_input",SessionData.getLanguage())+"</font>");
  if (hashRatePlanErrors.containsKey("general"))
  {
  out.print("<font color=ff0000>"+hashRatePlanErrors.get("general").toString()+"</font>");
  }

}

if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
<script language="javascript">

    function calculate(c)
    {
    	if (isNaN(c.value)== false)
    	{
    		if (c.value >=0)
    		{
    		var strFieldName = c.name;
    		var aryFieldName = strFieldName.split("_");
    		var strLevel     = aryFieldName[0];
    		var strProductId = aryFieldName[1];
    		var tmpTotalRate = eval("document.products.total_" + strProductId +".value;");
    		var tmpIsoRate   = eval("document.products.iso_" + strProductId +".value;");
    		<%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
          var tmpAgentRate = eval("document.products.agent_" + strProductId +".value;");
    		  var tmpSubagentRate = eval("document.products.subagent_" + strProductId +".value;");
    		  var intAgentRate = 0;
    		  var intSubagentRate = 0;
        <%}%>
    		var intTotalRate = 0;
    		var intIsoRate   = 0;



    		if (isNaN(tmpTotalRate) == false)
    		{
    			intTotalRate = tmpTotalRate - 0;
    		}
    		if (isNaN(tmpIsoRate) == false)
    		{
    			intIsoRate = tmpIsoRate - 0;
    		}
        <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
    		  if (isNaN(tmpAgentRate) == false)
    		  {
    			  intAgentRate = tmpAgentRate - 0;
    		  }
    		  if (isNaN(tmpSubagentRate) == false)
    		  {
    			  intSubagentRate = tmpSubagentRate - 0;
    		  }
          var intTotalRemaining = intTotalRate - (intIsoRate+intAgentRate+intSubagentRate);
        <%}
        else
        {
        %>
          var intTotalRemaining = intTotalRate - intIsoRate;
        <% }%>


    		if (isNaN(intTotalRemaining)==false)
    		{
   	    		var fieldName = "remaining_" + strProductId;

   	    		document.products[fieldName].value = formatAmount(intTotalRemaining);

   	    		if((intTotalRemaining < 0) || intTotalRemaining > intTotalRate)
   	    		{
   	    			document.products[fieldName].style.color = '#ff0000';
   	    		}
   	    		else
   	    		{
   	    			document.products[fieldName].style.color = '#33FF33';
   	    		}

   	    }
        }
    	}
    }

    function validate(c)
    {
    	if (isNaN(c.value))
    	{
    		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
    		c.focus();
    		return (false);

    	}
    	else
    	{
    		if (c.value < 0)
    		{

		    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
		    	c.focus();
		    	return (false);
			}
			else
			{
				c.value = formatAmount(c.value);
			}
		}
    }


	function formatAmount(n)
	{
  		var s = "" + Math.round(n * 100) / 100
  		var i = s.indexOf('.')
  		if (i < 0) return s + ".00"
  		var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3)
  		if (i + 2 == s.length) t += "0"
  		return t
	}                
        
        $(document).ready(function() {
            $('.dsall').hide(0);
            $('.sall').click(function() {                
                $('input:checkbox').prop('checked', true);                
                $('.dsall').show(0);
                $('.sall').hide(0);
            });            
            $('.dsall').click(function() {                
                $('input:checkbox').prop('checked', false);                
                $('.sall').show(0);
                $('.dsall').hide(0);
            });                        
        });

</script>

            <form name="products" method="post" action="admin/rateplans/edit_products.jsp">                
            <input class="sall" type=button value="<%=Languages.getString("jsp.tools.searchmerchant.selectAll",SessionData.getLanguage())%>">
            <input class="dsall" type=button value="<%=Languages.getString("jsp.tools.searchmerchant.UnselectAll",SessionData.getLanguage())%>">
            <input type=submit name=submit value="<%=Languages.getString("jsp.admin.rateplans.edit_products.submit",SessionData.getLanguage())%>"><br>            
            <input type="hidden" name="repRatePlanId" value="<%=RatePlan.getRepRatePlanId()%>">
            <input type="hidden" name="ratePlanId" value="<%=RatePlan.getRatePlanId()%>">
            <input type="hidden" name="submitted" value="y">
            <table width="450" cellspacing="1" cellpadding="1" border="0">
            <tr>
              <td class=rowhead2 width=10>&nbsp;</td>
              <td class=rowhead2 width=100 nowrap><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage())%></td>
            	<td class=rowhead2 width=100><%=Languages.getString("jsp.admin.product",SessionData.getLanguage())%></td>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.buy_rate",SessionData.getLanguage())%></td>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage())%></td>
              <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage())%></td>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage())%></td>
              <%}%>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.unassigned_commission",SessionData.getLanguage())%></td>
            </tr>

            <%
                  it_1 = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  String strProductId="";
                  double dblTotalRate = 0;
                  double dblIsoRate = 0;
                  double dblAgentRate = 0;
                  double dblSubAgentRate = 0;
                  double dblRemainingRate = 0;
                  int intTabIndex=1;
                  while (it_1.hasNext())
                  {
                    String selected = "";
                    Vector vecTemp = null;

                    vecTemp = (Vector) it_1.next();
                    strProductId = vecTemp.get(1).toString();
                    if (hashRepRates.containsKey(strProductId))
                    {
                      if (request.getParameter("submitted") == null)
                      {
                        vecTemp = (Vector) hashRepRates.get(strProductId);
                      }
                      selected="checked";
                    }


                    dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
                    if (request.getParameter("submitted") != null
                        && request.getParameter("submitted").equals("y"))
                    {
                      if (request.getParameter("iso_" + strProductId) != null)
                      {
                        if (NumberUtil.isNumeric(request.getParameter("iso_" + strProductId)))
                        {
                          dblIsoRate = Double.parseDouble(request.getParameter("iso_" + strProductId));
                        }
                        else
                        {
                          dblIsoRate =0;
                        }
                      }

                      if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                      {
                        if (request.getParameter("agent_" + strProductId) != null)
                        {
                          if (NumberUtil.isNumeric(request.getParameter("agent_" + strProductId)))
                          {
                            dblAgentRate = Double.parseDouble(request.getParameter("agent_" + strProductId));
                          }
                          else
                          {
                            dblAgentRate =0;
                          }
                        }
                        if (request.getParameter("subagent_" + strProductId) != null)
                        {
                          if (NumberUtil.isNumeric(request.getParameter("subagent_" + strProductId)))
                          {
                            dblSubAgentRate = Double.parseDouble(request.getParameter("subagent_" + strProductId));
                          }
                          else
                          {
                            dblSubAgentRate =0;
                          }
                        }

                      }
                    }
                    else
                    {
                      dblIsoRate = Double.parseDouble(vecTemp.get(3).toString());
                      dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
                      dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
                    }

                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                    {
                      dblRemainingRate = dblTotalRate - (dblIsoRate+dblAgentRate+dblSubAgentRate);
                    }
                    else
                    {
                      dblRemainingRate = dblTotalRate - dblIsoRate;
                    }

                    out.print("<tr class=row" + intEvenOdd +">" +                                
                                "<td><input class='checkboxall' type=checkbox name=productId value=" + strProductId + " " + selected + "></td>" +
                                "<td>" +strProductId+ "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
                                "<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"total_"+strProductId+"\" value=\""+NumberUtil.formatAmount(Double.toString(dblTotalRate))+"\" readonly size=3></td>" +
                                "<td align=left><input type=text name=\"iso_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblIsoRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\" tabIndex=\""+ intTabIndex++ +"\">");
                    if (hashRatePlanErrors != null
                        && hashRatePlanErrors.containsKey("iso_" + strProductId))
                    {
                      out.print("<font color=ff0000>*</font>");
                    }
                    out.print("</td>");
                                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                                {
                                  out.print("<td align=left><input type=text name=\"agent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblAgentRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\" tabIndex=\""+ intTabIndex++ +" \">");

                                  if (hashRatePlanErrors != null
                                      && hashRatePlanErrors.containsKey("agent_" + strProductId))
                                  {
                                    out.print("<font color=ff0000>*</font>");
                                  }
                                  out.print("</td>");
                                  out.print("<td align=left><input type=text name=\"subagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\" tabIndex=\"" + intTabIndex++ + "\">");
                                  if (hashRatePlanErrors != null
                                      && hashRatePlanErrors.containsKey("subagent_" + strProductId))
                                  {
                                    out.print("<font color=ff0000>*</font>");
                                  }
                                  out.print("</td>");

                                }

                                 out.print("<td align=left><input style=\"color:#");

                                  if((dblRemainingRate < 0) || dblRemainingRate > dblTotalRate)
                                  {
                                    out.print("ff0000");
                                  }
                                  else
                                  {
                                    out.print("0000FF");
                                  }
                                 out.println(";background:#C0C0C0;\" type=text name=\"remaining_"+strProductId+"\" value=\""+NumberUtil.formatAmount(Double.toString(dblRemainingRate))+"\" readonly size=3>");
                                  if (hashRatePlanErrors != null
                                      && hashRatePlanErrors.containsKey("remaining_" + strProductId))
                                  {
                                    out.print("<font color=ff0000>*</font>");
                                  }
                                  out.print("</td></tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
                  out.println("</td></tr></table><input type=submit name=submit value=\""+Languages.getString("jsp.admin.rateplans.edit_products.submit",SessionData.getLanguage())+"\"><br></form>");

}
else
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.rateplans.edit_products.error1",SessionData.getLanguage())+"</font>");
}
%>
                 
                 
                                  </table>
                 
                </td>
                   <td width="18">&nbsp;</td>
                   
               </tr>
               </table>
                     <div align=right class="backceldas"><font size="1"></div>
             </td>

             <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
       </tr>
       <tr>
           <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
         </tr>
         </table>
        </td>
    </tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>