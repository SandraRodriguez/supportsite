<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.StringUtil,
                 java.util.Hashtable" %>
<%
int section=7;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Iterator it;
String strMessage = request.getParameter("message");
if (strMessage == null)
{
  strMessage="";
}

  Vector vecSearchResults = new Vector();
  Hashtable hashRatePlanErrors = new Hashtable();
  Hashtable hashRepRates = new Hashtable();
  if (request.getParameter("submitted") != null)
  {
    try
    {
      if (request.getParameter("submitted").equals("y"))
      {
        if (request.getParameter("editProducts") != null)
        {
          response.sendRedirect("edit_products.jsp?repRatePlanId=" + RatePlan.getRepRatePlanId() + "&ratePlanId=" + RatePlan.getRatePlanId());
          return;
        }

        String[] productIds = request.getParameterValues("productId");
        if (productIds != null)
        {

          String strProductId = "";
          for(int i=0; i<productIds.length;i++)
          {
            strProductId = productIds[i];
            if (strProductId !=null && !strProductId.equals(""))
            {
              if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
              {
                if (request.getParameter("iso_" + strProductId) != null
                    &&request.getParameter("agent_" + strProductId) != null
                    &&request.getParameter("subagent_" + strProductId) != null)
                {
                  Vector vecRepRates = new Vector();
                  vecRepRates.add(0,request.getParameter("iso_" + strProductId));
                  vecRepRates.add(1,request.getParameter("agent_" + strProductId));
                  vecRepRates.add(2,request.getParameter("subagent_" + strProductId));
                  if (!hashRepRates.containsKey(strProductId))
                  {
                    hashRepRates.put(strProductId, vecRepRates);
                  }
                }
              }
              else if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
              {
                if (request.getParameter("iso_" + strProductId) != null)
                {
                  Vector vecRepRates = new Vector();
                  vecRepRates.add(0,request.getParameter("iso_" + strProductId));
                  if (!hashRepRates.containsKey(strProductId))
                  {
                    hashRepRates.put(strProductId, vecRepRates);
                  }
                }
              }
            }

          }

        }

        if(RatePlan.validateRepRatePlan(SessionData, hashRepRates))
        {
          RatePlan.updateRepRatePlan(SessionData, hashRepRates);
          response.sendRedirect("/support/admin/rateplans/rep_rates.jsp?message=2");
        }
        else
        {
          hashRatePlanErrors = RatePlan.getErrors();
          vecSearchResults = RatePlan.getRepRatePlan(SessionData);
          RatePlan.loadRatePlanReps(SessionData);

        }

      }

    }
    catch (Exception e)
    {
    }
  }
  else
  {
      vecSearchResults = RatePlan.getRepRatePlan(SessionData);
      RatePlan.loadRatePlanReps(SessionData);
  }
%>
<%@ include file="/includes/header.jsp" %>


<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.rateplans.edit_rep_rate.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
<%
if (strMessage.equals("1"))
{
  out.println("<tr bgcolor=ffffff><td align=center class=main><img src=images/information.png border=0 valign=middle><font color=000000>"+Languages.getString("jsp.admin.rateplans.edit_rep_rate.success",SessionData.getLanguage())+"</td></tr>");
}
%>
  <tr>
    <td class="main">

      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
<br>

<script language="javascript">

    function calculate(c)
    {
    	if (isNaN(c.value)== false)
    	{
    		if (c.value >=0)
    		{
    		var strFieldName = c.name;
    		var aryFieldName = strFieldName.split("_");
    		var strLevel     = aryFieldName[0];
    		var strProductId = aryFieldName[1];
    		var tmpTotalRate = eval("document.products.total_" + strProductId +".value;");
    		var tmpIsoRate   = eval("document.products.iso_" + strProductId +".value;");
    		<%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
          var tmpAgentRate = eval("document.products.agent_" + strProductId +".value;");
    		  var tmpSubagentRate = eval("document.products.subagent_" + strProductId +".value;");
    		  var intAgentRate = 0;
    		  var intSubagentRate = 0;
        <%}%>
    		var intTotalRate = 0;
    		var intIsoRate   = 0;



    		if (isNaN(tmpTotalRate) == false)
    		{
    			intTotalRate = tmpTotalRate - 0;
    		}
    		if (isNaN(tmpIsoRate) == false)
    		{
    			intIsoRate = tmpIsoRate - 0;
    		}
        <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
    		  if (isNaN(tmpAgentRate) == false)
    		  {
    			  intAgentRate = tmpAgentRate - 0;
    		  }
    		  if (isNaN(tmpSubagentRate) == false)
    		  {
    			  intSubagentRate = tmpSubagentRate - 0;
    		  }
          var intTotalRemaining = intTotalRate - (intIsoRate+intAgentRate+intSubagentRate);
        <%}
        else
        {
        %>
          var intTotalRemaining = intTotalRate - intIsoRate;
        <% }%>


    		if (isNaN(intTotalRemaining)==false)
    		{
   	    		var fieldName = "remaining_" + strProductId;

   	    		document.products[fieldName].value = formatAmount(intTotalRemaining);

   	    		if((intTotalRemaining < 0) || intTotalRemaining > intTotalRate)
   	    		{
   	    			document.products[fieldName].style.color = '#ff0000';
   	    		}
   	    		else
   	    		{
   	    			document.products[fieldName].style.color = '#33FF33';
   	    		}

   	    }
        }
    	}
    }

    function validate(c)
    {
    	if (isNaN(c.value))
    	{
    		alert('<%=Languages.getString("jsp.admin.error2",SessionData.getLanguage())%>');
    		c.focus();
    		return (false);

    	}
    	else
    	{
    		if (c.value < 0)
    		{

		    	alert('<%=Languages.getString("jsp.admin.error3",SessionData.getLanguage())%>');
		    	c.focus();
		    	return (false);
			}
			else
			{
				c.value = formatAmount(c.value);
			}
		}
    }


	function formatAmount(n)
	{
  		var s = "" + Math.round(n * 100) / 100
  		var i = s.indexOf('.')
  		if (i < 0) return s + ".00"
  		var t = s.substring(0, i + 1) + s.substring(i + 1, i + 3)
  		if (i + 2 == s.length) t += "0"
  		return t
	}

</script>

            <form name="products" method="post" action="admin/rateplans/edit_rep_rate.jsp">
            <input type="hidden" name="ratePlanId" value="<%=RatePlan.getRatePlanId()%>">
            <input type="hidden" name="repRatePlanId" value="<%=RatePlan.getRepRatePlanId()%>">
            <input type="hidden" name="submitted" value="y">
            <table width="575" cellspacing="1" cellpadding="1" border="0">
            <tr class=main>
              <td align=left colspan=7>
              <%
                out.println(Languages.getString("jsp.admin.rateplans.edit_rep_rate.instructions",SessionData.getLanguage()) + "<br>");
                if (hashRatePlanErrors != null && hashRatePlanErrors.size() > 0)
                {
                  out.println("<font color=ff0000>"+Languages.getString("jsp.index.error_input",SessionData.getLanguage())+"</font>");
                }
              %>
              <br>
              </td>
            </tr>
            <tr class=main>
              <td align=left colspan=7>
                  <%=Languages.getString("jsp.admin.rateplans.edit_rep_rate.rateplan_name",SessionData.getLanguage())%>:<input type="text" name="ratePlanName" value="<%=RatePlan.getRatePlanName()%>" size="30" maxlength="50"><%if (hashRatePlanErrors != null && hashRatePlanErrors.containsKey("ratePlanName")) out.print("<font color=ff0000>*</font>");%>
              </td>
            </tr>
            <tr>
            	<td class=rowhead2 width=100><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2 width=100><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.buy_rate",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.iso_percent",SessionData.getLanguage()).toUpperCase()%></td>
              <%if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)) {%>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.agent_percent",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.subagent_percent",SessionData.getLanguage()).toUpperCase()%></td>
              <%}%>
              <td class=rowhead2 width=75 align=center><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.unassigned_commission",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{

                  it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  String strProductId="";
                  double dblTotalRate = 0;
                  double dblIsoRate = 0;
                  double dblAgentRate = 0;
                  double dblSubAgentRate = 0;
                  double dblRemainingRate = 0;
                  int intTabIndex=1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    strProductId = vecTemp.get(1).toString();
                    dblTotalRate = Double.parseDouble(vecTemp.get(2).toString());
                    if (request.getParameter("submitted") != null
                        && request.getParameter("submitted").equals("y"))
                    {
                      if (request.getParameter("iso_" + strProductId) != null)
                      {
                        if (NumberUtil.isNumeric(request.getParameter("iso_" + strProductId)))
                        {
                          dblIsoRate = Double.parseDouble(request.getParameter("iso_" + strProductId));
                        }
                        else
                        {
                          dblIsoRate =0;
                        }
                      }
                      if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                      {
                        if (request.getParameter("agent_" + strProductId) != null)
                        {
                          if (NumberUtil.isNumeric(request.getParameter("agent_" + strProductId)))
                          {
                            dblAgentRate = Double.parseDouble(request.getParameter("agent_" + strProductId));
                          }
                          else
                          {
                            dblAgentRate =0;
                          }
                        }
                        if (request.getParameter("subagent_" + strProductId) != null)
                        {
                          if (NumberUtil.isNumeric(request.getParameter("subagent_" + strProductId)))
                          {
                            dblSubAgentRate = Double.parseDouble(request.getParameter("subagent_" + strProductId));
                          }
                          else
                          {
                            dblSubAgentRate =0;
                          }
                        }

                      }
                    }
                    else
                    {
                      dblIsoRate = Double.parseDouble(vecTemp.get(3).toString());
                      dblAgentRate = Double.parseDouble(vecTemp.get(4).toString());
                      dblSubAgentRate = Double.parseDouble(vecTemp.get(5).toString());
                    }

                    if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                    {
                      dblRemainingRate = dblTotalRate - (dblIsoRate+dblAgentRate+dblSubAgentRate);
                    }
                    else
                    {
                      dblRemainingRate = dblTotalRate - dblIsoRate;
                    }


                    out.print("<tr class=row" + intEvenOdd +">" +
                                "<td><input type=hidden name=productId value=" + strProductId + ">" +strProductId+ "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
                                "<td align=left><input style=\"color:#0000FF;background:#C0C0C0;\" type=text name=\"total_"+strProductId+"\" value=\""+NumberUtil.formatAmount(Double.toString(dblTotalRate))+"\" readonly size=3></td>" +
                                "<td align=left><input type=text name=\"iso_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblIsoRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\" tabIndex=\""+ intTabIndex++ +"\">");
                    if (hashRatePlanErrors != null
                        && hashRatePlanErrors.containsKey("iso_" + strProductId))
                    {
                      out.print("<font color=ff0000>*</font>");
                    }
                    out.print("</td>");
                                if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                                {
                                  out.print("<td align=left><input type=text name=\"agent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblAgentRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\" tabIndex=\""+ intTabIndex++ +" \">");

                                  if (hashRatePlanErrors != null
                                      && hashRatePlanErrors.containsKey("agent_" + strProductId))
                                  {
                                    out.print("<font color=ff0000>*</font>");
                                  }
                                  out.print("</td>");
                                  out.print("<td align=left><input type=text name=\"subagent_" + strProductId + "\" value=\"" + NumberUtil.formatAmount(Double.toString(dblSubAgentRate)) + "\" size=3 maxlength=5 onKeyUp=\"calculate(this);\" onBlur=\"return validate(this);\" tabIndex=\"" + intTabIndex++ + "\">");
                                  if (hashRatePlanErrors != null
                                      && hashRatePlanErrors.containsKey("subagent_" + strProductId))
                                  {
                                    out.print("<font color=ff0000>*</font>");
                                  }
                                  out.print("</td>");

                                }

                                 out.print("<td align=left><input style=\"color:#");

                                  if((dblRemainingRate < 0) || dblRemainingRate > dblTotalRate)
                                  {
                                    out.print("ff0000");
                                  }
                                  else
                                  {
                                    out.print("0000FF");
                                  }
                                 out.println(";background:#C0C0C0;\" type=text name=\"remaining_"+strProductId+"\" value=\""+NumberUtil.formatAmount(Double.toString(dblRemainingRate))+"\" readonly size=3>");
                                  if (hashRatePlanErrors != null
                                      && hashRatePlanErrors.containsKey("remaining_" + strProductId))
                                  {
                                    out.print("<font color=ff0000>*</font>");
                                  }
                                  out.print("</td></tr>");

                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
}
else
{

 out.println("<tr class=row1><td align=left colspan=7>"+Languages.getString("jsp.admin.rateplans.edit_rep_rate.error1",SessionData.getLanguage())+"</td></tr>");
}

            %>
            <tr class=main>
            <td colspan=7 align=left>
              <input type=submit name=editProducts value="<%=Languages.getString("jsp.admin.rateplans.edit_rep_rate.add_remove_products",SessionData.getLanguage())%>">
            </td>
            </tr>
            <tr class=main>
              <td colspan=7 align=left>
              <br><br>
                <%=Languages.getString("jsp.admin.rateplans.add_rep_rate.assign_reps",SessionData.getLanguage())%>
                <br>
                <select name="repIds" size="15" multiple>
               <%
                 Vector vecRepList = RatePlan.getRepList(SessionData);
                 String repIds = "|" + StringUtil.arrayToString(RatePlan.getRepIds(), "|") + "|";
                 String repId = "";
                 it = vecRepList.iterator();
                 if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
                 {
                   while (it.hasNext())
                   {
                     Vector vecTemp = null;
                     vecTemp = (Vector) it.next();
                     repId = vecTemp.get(0).toString();
                     out.println("<option value=\"" + repId + "\" ");
                     if (repIds.indexOf("|" + repId + "|") != -1)
                     {
                       out.println("selected");
                     }
                     out.println(">" + vecTemp.get(2) + "->" + vecTemp.get(3) +"->"+ vecTemp.get(1) + "</option>");
                   }
                 }
                 else
                 {
                   while (it.hasNext())
                   {
                     Vector vecTemp = null;
                     vecTemp = (Vector) it.next();
                     repId = vecTemp.get(0).toString();
                     out.println("<option value=\"" + repId + "\" ");
                     if (repIds.indexOf("|" + repId + "|")!= -1)
                     {
                       out.println("selected");
                     }
                     out.println(">" + vecTemp.get(1) + "</option>");
                   }
                 }

               %>
               </select>
              </td>
            </tr>
            <tr class=main>
              <td colspan=7 align=left><input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.rateplans.edit_rep_rate.update",SessionData.getLanguage())%>"></td>
            </tr>

            </table>

          </td>
      </tr>
    </table>
    </form>
</td>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>