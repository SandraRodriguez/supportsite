<%@page import="com.debisys.rateplans.RatePlan"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Vector,java.util.ArrayList,java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.debisys.utils.NumberUtil"%>
<%@page import="com.debisys.users.SessionData"%>
<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="com.debisys.utils.StringUtil"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request" />
<%
	response.setCharacterEncoding("iso-8859-1");

	int section = 7;
	int section_page = 4;

	Vector<String> vTmp = (Vector<String>)SessionData.getPropertyObj("CurrentRatePlanActor");
	String sRefId = vTmp.get(0);
	String sAccessLevel = vTmp.get(1);
	//String sChainLevel = vTmp.get(2);

	if ( request.getParameter("action").equals("checkName") )
	{
		out.print(RatePlan.checkRatePlanName(request.getParameter("id"), request.getParameter("data")));
	}
	else if ( request.getParameter("action").equals("saveName") )
	{
		if ( request.getParameter("id").equals("0") )
		{
			int nResult = 0;
			if ( request.getParameter("dup") != null )
			{
				nResult = RatePlan.addRatePlan(SessionData, application, request.getParameter("base"), request.getParameter("name"), request.getParameter("desc"), request.getParameter("target"), request.getParameter("type"), request.getParameter("dup"));
			}
			else
			{
				nResult = RatePlan.addRatePlan(SessionData, application, request.getParameter("base"), request.getParameter("name"), request.getParameter("desc"), request.getParameter("target"), request.getParameter("type"), "0");
			}

			if ( nResult > 0 )
			{
				out.print("-VALID-:" + nResult);
			}
			else
			{
				out.print("-ERROR-");
			}
		}
		else
		{
			if ( RatePlan.saveRatePlan(SessionData, request.getParameter("id"), request.getParameter("name"), request.getParameter("desc")) )
			{
				out.print("-VALID-");
			}
			else
			{
				out.print("-ERROR-");
			}
		}
	}
	else if ( request.getParameter("action").equals("deletePlan") )
	{
		if ( RatePlan.deleteRatePlan(request.getParameter("id"), SessionData, application) )
		{
			out.print("-VALID-");
		}
		else
		{
			out.print("-ERROR-");
		}
	}
	else if ( request.getParameter("action").equals("browseActor") )
	{
		Vector<String> vT = new Vector<String>();
		vT.add(request.getParameter("actor"));
		vT.add(request.getParameter("type"));
		vT.add(request.getParameter("chain"));
		SessionData.setPropertyObj("CurrentRatePlanActor", vT);
		out.print("-VALID-");
	}
	else if ( request.getParameter("action").equals("getSearchProducts") )
	{
		String sCarrier = null;
		if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
		{
			sCarrier = SessionData.getUser().getRefId();
		}
		Vector<Vector<String>> vSearch = RatePlan.getRatePlanProductsSearch(request.getParameter("id"), request.getParameter("base"), sCarrier);
		if ( vSearch.size() > 0 )
		{
%>
<table border="0" cellpadding="0" cellspacing="0" width="600">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%=Languages.getString("jsp.admin.rateplans.ProductsFromParent",SessionData.getLanguage()).toUpperCase()%></td>
		<td background="images/top_blue.gif" width="1%" align="right">
			<img src="images/top_right_blue.gif" width="18" height="20">
		</td>
	</tr>
	<tr><td colspan="3" class="formArea2" align="center">
		<form>
<%
			if (vSearch.size() > 20)
			{
%>
			<div style="text-align:right;">
				<div id="divExpandSearch" style="display:none;"><a href="javascript:" onclick="$('#divInnerResults').css('display', 'block');$('#divCollapseSearch').css('display', 'inline');$('#divExpandSearch').css('display', 'none');"><%=Languages.getString("jsp.admin.rateplans.ExpandPanel2",SessionData.getLanguage())%></a>&nbsp;</div>
				<div id="divCollapseSearch" style="display:inline;"><a href="javascript:" onclick="$('#divInnerResults').css('display', 'none');$('#divExpandSearch').css('display', 'inline');$('#divCollapseSearch').css('display', 'none');"><%=Languages.getString("jsp.admin.rateplans.CollapsePanel",SessionData.getLanguage())%></a>&nbsp;</div>
			</div><br/>
<%
			}
%>
			<table><tr><td><div id="divInnerResults" style="<%=(vSearch.size() > 20)?"height:300px;overflow:auto;display:block;":"display:block;"%>">
				<table class="main">
					<tr class="SectionTopBorder">
						<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.Amount",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
<%
			if ( SessionData.getUser().isIntranetUser() )
			{
%>
						<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.Provider",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
<%
			}
%>
						<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.Carrier",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
						<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<label for="chkSearchProduct"><%=Languages.getString("jsp.admin.rateplans.Select_Deselect",SessionData.getLanguage()).toUpperCase()%></label>&nbsp;<br/><input type="checkbox" id="chkSearchProduct" onclick="ToggleSearch(this.checked);"></td>
					</tr>
<%
			String sSearchScript = "";
			int nCounter = 1;
			Iterator<Vector<String>> it = vSearch.iterator();
			while ( it.hasNext() )
			{
				Vector<String> vItem = it.next();
				sSearchScript += "SearchItems[" + (nCounter - 1) + "] = " + vItem.get(0) + ";";
%>
						<tr class="RatePlanRow1<%=(nCounter++ % 2)%>">
							<td>&nbsp;<%=vItem.get(0)%>&nbsp;</td>
							<td nowrap="nowrap">&nbsp;<%=vItem.get(1)%>&nbsp;</td>
							<td align="right">&nbsp;<%=NumberUtil.formatCurrency(vItem.get(2))%>&nbsp;</td>
<%
			if ( SessionData.getUser().isIntranetUser() )
			{
%>
							<td align="center" nowrap="nowrap">&nbsp;<%=vItem.get(3)%>&nbsp;</td>
<%
			}
%>
							<td align="center" nowrap="nowrap">&nbsp;<%=vItem.get(4)%>&nbsp;</td>
							<td align="center"><input type="checkbox" id="chkSearch_<%=vItem.get(0)%>"></td>
						</tr>
<%
				vItem = null;
			}
			it = null;
%>
				</table>
			</div></td></tr></table>
			<br/><input type="submit" value="<%=Languages.getString("jsp.admin.rateplans.AddToRatePlan",SessionData.getLanguage())%>" onclick="DoAdd();return false;">
		</form>
		<script>
		var SearchItems = new Array();
		<%=sSearchScript%>

		function ToggleSearch(bValue)
		{
			var i;
			for ( i = 0; i < SearchItems.length; i++ )
			{
				document.getElementById('chkSearch_' + SearchItems[i]).checked = bValue;
			}
		}

		function DoAdd()
		{
			var i, sProductList;
			sProductList = "";

			for ( i = 0; i < SearchItems.length; i++ )
			{
				sProductList += document.getElementById('chkSearch_' + SearchItems[i]).checked?SearchItems[i] + ",":"";
			}

			if ( sProductList == "" )
			{
				alert("<%=Languages.getString("jsp.admin.rateplans.SelectAProduct",SessionData.getLanguage())%>");
				return;
			}

			$("#divRatePlanProducts").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
			$.post('admin/rateplans/rateplan_ajax.jsp?action=addProducts&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>&base=<%=request.getParameter("base")%>&data=' + sProductList + '&Random=' + Math.random(), ProcessAdd);
		}

		function ProcessAdd(sData, sStatus)
		{
			if ( sStatus == "success" )
			{
				$("#divRatePlanProducts").html(sData);
				$("#divRatePlanProductsSearch").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
				$('#divRatePlanProductsSearch').load('admin/rateplans/rateplan_ajax.jsp?action=getSearchProducts&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>&Random=' + Math.random());
			}
			else
			{
				alert("<%=Languages.getString("jsp.admin.rateplans.AddFailed",SessionData.getLanguage())%>");
			}
		}
		</script>
	</td></tr>
</table>
<%
		}
		else
		{
			out.println("<img src='images/information.png' style=\"vertical-align:middle;\">&nbsp;<span class='main'>" + Languages.getString("jsp.admin.rateplans.NoSearchProducts",SessionData.getLanguage()) + "</span>");
		}
	}
	else if ( request.getParameter("action").equals("getProducts") 
			|| request.getParameter("action").equals("addProducts") 
			|| request.getParameter("action").equals("removeProducts") 
			|| request.getParameter("action").equals("saveProducts") )
	{
		String sPageNumber, sPageSize, sSort, sSortDirection, sFilterId, sFilterName;
		int nTotalRows, nPageCount;
		
		sPageNumber = request.getParameter("txtPageNumber");
		sPageSize = request.getParameter("ddlPageSize");
		sSort = request.getParameter("txtSort");
		sSortDirection = request.getParameter("txtSortDirection");
		sFilterId = request.getParameter("txtFilterID");
		sFilterName = request.getParameter("txtFilterName");
		
		if ( sPageNumber == null ) sPageNumber = "1";
		if ( sPageSize  == null ) sPageSize = "50";
		if ( sSort == null ) sSort = "SKU";
		if ( sSortDirection == null ) sSortDirection = "A";
		if ( sFilterId == null ) sFilterId = "";
		if ( sFilterName == null ) sFilterName = "";

		Vector<String> vPlan = RatePlan.getRatePlanById(request.getParameter("id"));
		boolean bIsEditable = (Long.parseLong(vPlan.get(7).concat(vPlan.get(8))) == 0);

		String sCarrier = null;
		if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
		{
			sCarrier = SessionData.getUser().getRefId();
		}
		Vector<Vector<String>> vAssignments = RatePlan.getRatePlanAssignments(request.getParameter("id"), sAccessLevel, sRefId, false, sCarrier);

		if ( vAssignments.size() > 0 )
		{
			bIsEditable = bIsEditable && (vAssignments.get(vAssignments.size() - 1)).get(2).equals("0");
		}
		bIsEditable = bIsEditable && (vPlan.get(0).equals(sRefId));

		if ( request.getParameter("action").equals("addProducts") )
		{
			RatePlan.addRatePlanProducts(SessionData, application, request.getParameter("id"), request.getParameter("base"), request.getParameter("data"));
			SessionData.setProperty("RatePlan_RecentAdded", request.getParameter("data"));
		}
		else if ( request.getParameter("action").equals("removeProducts") )
		{
			RatePlan.deleteRatePlanProducts(SessionData, application, request.getParameter("id"), request.getParameter("data"));
		}
		else if ( request.getParameter("action").equals("saveProducts") )
		{
			RatePlan.saveRatePlanProducts(request.getParameter("id"), request.getParameter("data"), SessionData, application);
		}

		Vector<Vector<String>> vProducts = RatePlan.getRatePlanProducts(request.getParameter("id"), request.getParameter("base"), Integer.parseInt(sPageNumber), Integer.parseInt(sPageSize), sSort, sSortDirection, sFilterId, sFilterName, sCarrier);
		boolean bHideCell = false;
		if ( !vProducts.get(0).get(0).equals("0") || (sFilterId.length() > 0 || sFilterName.length() > 0) )
		{
			if ( SessionData.getProperty("RatePlan_RecentAdded") != null && !SessionData.getProperty("RatePlan_RecentAdded").equals("") )
			{
				out.println("<img src='images/information.png' style=\"vertical-align:middle;\">&nbsp;<span class='main'>" + Languages.getString("jsp.admin.rateplans.RecentProductsAdded",SessionData.getLanguage()) + "</span><br/>");
			}
			if ( !StringUtil.toString(request.getParameter("data")).equals("") )
			{
%>
		<div id="divMessage" class="main">
			<br/><img id="imgMessage" src="images/information.png" style="vertical-align:middle;">
			&nbsp;<span id="spMessage"><%=((vPlan.get(5).equals("0"))?Languages.getString("jsp.admin.rateplans.SaveSuccessP",SessionData.getLanguage()):Languages.getString("jsp.admin.rateplans.SaveSuccessT",SessionData.getLanguage()))%></span>&nbsp;
			<img id="imgMessage" src="images/information.png" style="vertical-align:middle;">
		</div><br/>
		<script>
		window.setTimeout("location.href = location.href + ((location.href.charAt(location.href.length - 1) == '#')?'':'#');", 500);
		</script>
<%
			}

			String sBreakName = vPlan.get(3);
			if ( sBreakName.length() > 100 )
			{
				sBreakName = sBreakName.substring(0, 100) + "...";
			}
%>
<table border="0" cellpadding="-1" cellspacing="-1" width="300">
	<tr>
		<td background="images/top_blue.gif" width="1%" align="left">
			<img src="images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%=Languages.getString("jsp.admin.rateplans.EditRates",SessionData.getLanguage()).toUpperCase() + " [" + sBreakName.toUpperCase() + "]"%></td>
		<td background="images/top_blue.gif" width="1%" align="right">
			<img src="images/top_right_blue.gif" width="18" height="20">
		</td>
	</tr>
	<tr><td colspan="3" class="formArea2" align="center">
		<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;">
			<tr class="main">
				<td align="left"><%=Languages.getString("jsp.admin.rateplans.FilterBy",SessionData.getLanguage())%>:</td>
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
%>
				<td align="right" colspan="2"><form action="admin/rateplans/rateplan_edit_export.jsp" target="_blank" method="post"><input type="hidden" name="id" value="<%=request.getParameter("id")%>"><input type="hidden" name="base" value="<%=request.getParameter("base")%>"><input type="submit" value="<%=Languages.getString("jsp.admin.rateplans.ClickExportExcel",SessionData.getLanguage())%>"></form></td>
<%
			}
%>
			</tr>
			<tr class="main">
				<td align="left" valign="bottom" nowrap="nowrap">
					<%=Languages.getString("jsp.admin.rateplans.ID",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterID" name="txtFilterID" maxlength="50" size="5" style="vertical-align:middle;" value="<%=sFilterId%>">&nbsp;
					<%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterName" name="txtFilterName" maxlength="200" size="30" style="vertical-align:middle;" value="<%=sFilterName%>">&nbsp;
					<input type="image" src="images/greenarrow.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickFilterResults",SessionData.getLanguage())%>" onclick="$('#txtPageNumber').val('1');DoSave();">
					<input type="image" src="images/reddelete.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickClearResults",SessionData.getLanguage())%>" onclick="$('#txtFilterID').val('');$('#txtFilterName').val('');$('#txtPageNumber').val('1');">
				</td>
				<td align="center" valign="bottom">
					<table cellspacing="-1" cellpadding="-1"><tr class="main">
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
				int nPageNumber = Integer.parseInt(sPageNumber);
				int nPageSize = Integer.parseInt(sPageSize);
				nTotalRows = Integer.parseInt(vProducts.get(0).get(0));
				nPageCount = nTotalRows / nPageSize;
				if ((nPageCount * nPageSize) < nTotalRows)
				{
					nPageCount++;
				}

				if ( nPageCount > 1 && nPageNumber > 1 )
				{
%>
						<td><a href="javascript:" onclick="$('#txtPageNumber').val('1');DoSave();"><img border="0" src="images/pagefirst.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
						<td><img border="0" src="images/pagefirstdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></td>
<%
				}

				if (nPageNumber > 1)
				{
%>
						<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber - 1)%>');DoSave();"><img border="0" src="images/pageprev.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
						<td><img border="0" src="images/pageprevdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></td>
<%
				}
%>
						<td nowrap="nowrap">&nbsp;<%=Languages.getString("jsp.admin.rateplans.ShowingPageOf", new Object[]{nPageNumber, nPageCount},SessionData.getLanguage())%>&nbsp;</td>
<%
				if (nPageNumber < nPageCount)
				{
%>
						<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber + 1)%>');DoSave();"><img border="0" src="images/pagenext.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
						<td><img border="0" src="images/pagenextdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></td>
<%
				}

				if ( nPageCount > 1 && nPageNumber < nPageCount )
				{
%>
						<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=nPageCount%>');DoSave();"><img border="0" src="images/pagelast.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
						<td><img border="0" src="images/pagelastdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></td>
<%
				}
			}
%>
					</tr></table>
				</td>
				<td align="right" valign="bottom" nowrap="nowrap">
					<%=Languages.getString("jsp.admin.rateplans.PageSize",SessionData.getLanguage())%>&nbsp;<select id="ddlPageSize" name="ddlPageSize" style="vertical-align:middle;" onchange="$('#txtPageNumber').val('1');DoSave();"><option value="10" <%=(sPageSize.equals("10")?"selected":"")%>>10</option><option value="20" <%=(sPageSize.equals("20")?"selected":"")%>>20</option><option value="50" <%=(sPageSize.equals("50")?"selected":"")%>>50</option></select>
				</td>
			</tr>
		</table>
		<input type="hidden" id="txtPageNumber" name="txtPageNumber" value="<%=sPageNumber%>">
		<input type="hidden" id="txtSort" name="txtSort" value="<%=sSort%>">
		<input type="hidden" id="txtSortDirection" name="txtSortDirection" value="<%=sSortDirection%>">
	</td></tr>
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
%>
	<tr><td colspan="3" class="formArea2" align="center">
		<form>
			<table class="main">
				<tr class="main"><td align="center" colspan="12"><small><%=Languages.getString("jsp.admin.rateplans.ClickHeadersSort",SessionData.getLanguage())%></small></td></tr>
				<tr class="SectionTopBorder">
					<td class="rowhead2" style="text-align:center;vertical-align:top;"><a href="javascript:" onclick="RenderSortIcon('SKU');"><u><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage()).toUpperCase()%></u></a><br/><div id="divSKU"></div></td>
					<td class="rowhead2" style="text-align:center;vertical-align:top;"><a href="javascript:" onclick="RenderSortIcon('Name');"><u><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage()).toUpperCase()%></u></a><br/><div id="divName"></div></td>
					<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<a href="javascript:" onclick="RenderSortIcon('Amount');"><u><%=Languages.getString("jsp.admin.rateplans.Amount",SessionData.getLanguage()).toUpperCase()%></u></a>&nbsp;<br/><div id="divAmount"></div></td>
<%
			if ( SessionData.getUser().isIntranetUser() )
			{
%>
					<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.Provider",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
<%
			}
%>
					<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.Carrier",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
					<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.RateType",SessionData.getLanguage()).toUpperCase().replaceAll("&NBSP;", "&nbsp;")%>&nbsp;</td>
					<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.TotalRate",SessionData.getLanguage()).toUpperCase().replaceAll("&NBSP;", "&nbsp;")%></td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
					<td class="rowhead2" style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><%=SessionData.getString("jsp.admin.rateplans.ISORate").toUpperCase().replaceAll("&NBSP;", "&nbsp;")%></td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
					<td class="rowhead2" style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><%=SessionData.getString("jsp.admin.rateplans.AgentRate").toUpperCase().replaceAll("&NBSP;", "&nbsp;")%></td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
					<td class="rowhead2" style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><%=SessionData.getString("jsp.admin.rateplans.SubAgentRate").toUpperCase().replaceAll("&NBSP;", "&nbsp;")%></td>
					<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.RepRate").toUpperCase().replaceAll("&NBSP;", "&nbsp;")%></td>
					<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.MerchantRate").toUpperCase().replaceAll("&NBSP;", "&nbsp;")%></td>
<%
			if ( vPlan.get(5).equals("0") )//0 = RatePlan, 1 = Template
			{
				if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) )
				{
					out.println("<td class=\"rowhead2\" style=\"text-align:center;vertical-align:top;\">" + Languages.getString("jsp.admin.rateplans.TopSellersOrder",SessionData.getLanguage()).toUpperCase() + "</td>");
				}
				//if ( SessionData.checkPermission(DebisysConstants.PERM_EDIT_CACHED_RECEIPTS) )
				//{
					out.println("<td class=\"rowhead2\" style=\"text-align:center;vertical-align:top;\">" + Languages.getString("jsp.admin.rateplans.CachedReceipts",SessionData.getLanguage()).toUpperCase() + "</td>");
					out.println("<td class=\"rowhead2\" style=\"text-align:center;vertical-align:top;\">" + Languages.getString("jsp.admin.customers.merchants_add_terminal.term_cachereceiptsType",SessionData.getLanguage()).toUpperCase() + "</td>");
				//}
				
			}
%>
					<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<label for="chkEnableProduct"><%=Languages.getString("jsp.admin.rateplans.Enabled",SessionData.getLanguage()).toUpperCase()%></label>&nbsp;<br/><input type="checkbox" id="chkEnableProduct" style="<%=(!bIsEditable && !vPlan.get(0).equals(sRefId))?"display:none":""%>" onclick="ToggleEnable(this.checked);" <%=(!bIsEditable && !vPlan.get(0).equals(sRefId))?"disabled":""%>></td>
					<td class="rowhead2" style="text-align:center;vertical-align:top;<%=(!bIsEditable)?"display:none":""%>">&nbsp;<label for="chkSelectProduct"><%=Languages.getString("jsp.admin.rateplans.Delete",SessionData.getLanguage()).toUpperCase()%></label>&nbsp;<br/><input type="checkbox" id="chkSelectProduct" onclick="ToggleProducts(this.checked);"></td>
				</tr>
<%
			List<com.debisys.utils.Base> listTypes = com.debisys.terminals.Terminal.getReceiptTypes( SessionData.getUser().getLanguageCode() );
			String sProductScript = "";
			int nCounter = 1;
			Iterator<Vector<String>> it = vProducts.iterator();
			boolean bHighlightColumn = false;
			
			while ( it.hasNext() )
			{
				Vector<String> vItem = it.next();
				if ( nCounter == 1 )
				{
					nCounter++;
					continue;//We must skip this iteration since the first row is the count of total records
				}

				sProductScript += "ProductItems[" + (nCounter - 2) + "] = " + vItem.get(0) + ";";

				if ( vItem.get(15).equals("1") )
				{
					//vItem.get(17) is the Fixed_Fee
					vItem.set(5, Double.toString((Double.parseDouble(vItem.get(5)) * Double.parseDouble(vItem.get(17))) / 100));
					vItem.set(6, Double.toString((Double.parseDouble(vItem.get(6)) * Double.parseDouble(vItem.get(17))) / 100));
					vItem.set(7, Double.toString((Double.parseDouble(vItem.get(7)) * Double.parseDouble(vItem.get(17))) / 100));
					vItem.set(8, Double.toString((Double.parseDouble(vItem.get(8)) * Double.parseDouble(vItem.get(17))) / 100));
					vItem.set(9, Double.toString((Double.parseDouble(vItem.get(9)) * Double.parseDouble(vItem.get(17))) / 100));
					vItem.set(10, Double.toString((Double.parseDouble(vItem.get(10)) * Double.parseDouble(vItem.get(17))) / 100));
				}

				if ( SessionData.getProperty("RatePlan_RecentAdded") != null )
				{
					if ( String.valueOf("," + SessionData.getProperty("RatePlan_RecentAdded") + ",").indexOf("," + vItem.get(0) + ",") > -1 )
					{
						out.println("<tr class=\"main\" style=\"background-color:#C0C0FF;\">");
					}
					else
					{
						out.println("<tr class=\"RatePlanRow1" + (nCounter++ % 2) + "\">");
					}
				}
				else
				{
%>
				<tr class="RatePlanRow1<%=(nCounter++ % 2)%>">
<%
				}
%>
					<td>
						&nbsp;<%=vItem.get(0)%>&nbsp;
						<input type="hidden" id="SumRates_<%=vItem.get(0)%>" value="<%=vItem.get(10)%>">
						<input type="hidden" id="FixedFee_<%=vItem.get(0)%>" value="<%=vItem.get(17)%>">
						<input type="hidden" id="Original_<%=vItem.get(0)%>" value="<%=vItem.get(5)%>,<%=vItem.get(6)%>,<%=vItem.get(7)%>,<%=vItem.get(8)%>,<%=vItem.get(9)%>,<%=vItem.get(11)%>,<%=vItem.get(13)%>,<%=vItem.get(14)%>,<%=vItem.get(15)%>,<%=vItem.get(18)%>">
<%
			if ( vPlan.get(5).equals("0") )//0 = RatePlan, 1 = Template
			{
				if ( !SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) )
				{
					out.println("<input type=\"text\" id=\"TopSeller_" + vItem.get(0) + "\" style=\"display:none;\" value=\"" + vItem.get(13) + "\">");
				}
				if ( !SessionData.checkPermission(DebisysConstants.PERM_EDIT_CACHED_RECEIPTS) )
				{
					out.println("<input type=\"checkbox\" id=\"chkCachedR_" + vItem.get(0) + "\" style=\"display:none;\">");
				}
			}
			else
			{
				out.println("<input type=\"text\" id=\"TopSeller_" + vItem.get(0) + "\" style=\"display:none;\" value=\"" + vItem.get(13) + "\">");
				out.println("<input type=\"checkbox\" id=\"chkCachedR_" + vItem.get(0) + "\" style=\"display:none;\">");
				out.println("<input type=\"text\" id=\"chkCachedRType_" + vItem.get(0) + "\" style=\"display:none;\">");
			}
%>
					</td>
					<td nowrap="nowrap">&nbsp;<%=vItem.get(1)%>&nbsp;</td>
					<td align="right">&nbsp;<%=NumberUtil.formatCurrency(vItem.get(2))%>&nbsp;</td>
<%
			if ( SessionData.getUser().isIntranetUser() )
			{
%>
					<td align="center" nowrap="nowrap">&nbsp;<%=vItem.get(3)%>&nbsp;</td>
<%
			}
%>
					<td align="center" nowrap="nowrap">&nbsp;<%=vItem.get(4)%>&nbsp;</td>
					<td align="center"><%=(vItem.get(15).equals("1")?"$":"%")%></td>
<%
			Double dTotalRate = new Double(vItem.get(10));
			if ( sAccessLevel.equals(DebisysConstants.AGENT) )
			{
				dTotalRate -= new Double(vItem.get(5));
			}
			if ( sAccessLevel.equals(DebisysConstants.SUBAGENT) )
			{
				dTotalRate -= new Double(vItem.get(5));
				dTotalRate -= new Double(vItem.get(6));
			}
			if ( sAccessLevel.equals(DebisysConstants.REP) )
			{
				dTotalRate -= new Double(vItem.get(5));
				dTotalRate -= new Double(vItem.get(6));
				dTotalRate -= new Double(vItem.get(7));
			}
%>
					<td align="center"><%=NumberUtil.formatAmount(Double.toString(dTotalRate))%>
                                            <%
                                            if(vItem.get(19).equals("0")){
                                                %>
                                                <img id="imgMessage" src="images/risk-icon.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ProductRatesNotEditable",SessionData.getLanguage())%>">
                                                <%
                                            }
                                            %>
                                        </td>
<%
				bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER));
				bHighlightColumn = sAccessLevel.equals(DebisysConstants.ISO);
%>
					<td align="center" valign="bottom" id="tdISO_<%=vItem.get(0)%>" style="<%=bHideCell?"display:none":""%><%=bHighlightColumn?"background-color:#80FF80":""%>">
						&nbsp;<input type="text" id="ISO_<%=vItem.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount(vItem.get(5))%>" onkeyup="ProcessRate(this, false);" onblur="ProcessRate(this, true);HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=(bHighlightColumn || !bIsEditable || vItem.get(12).equals("1") || vItem.get(19).equals("0"))?" readonly":""%>>&nbsp;
					</td>
<%
				bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
				bHighlightColumn = sAccessLevel.equals(DebisysConstants.AGENT);
%>
					<td align="center" valign="bottom" id="tdAgent_<%=vItem.get(0)%>" style="<%=bHideCell?"display:none":""%><%=bHighlightColumn?"background-color:#80FF80":""%>">
						&nbsp;<input type="text" id="Agent_<%=vItem.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount(vItem.get(6))%>" onkeyup="ProcessRate(this, false);" onblur="ProcessRate(this, true);HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=(bHighlightColumn || !bIsEditable || vItem.get(12).equals("1") || vItem.get(19).equals("0"))?" readonly":""%>>&nbsp;
					</td>
<%
				bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
				bHighlightColumn = sAccessLevel.equals(DebisysConstants.SUBAGENT);
%>
					<td align="center" valign="bottom" id="tdSubAgent_<%=vItem.get(0)%>" style="<%=bHideCell?"display:none":""%><%=bHighlightColumn?"background-color:#80FF80":""%>">
						&nbsp;<input type="text" id="SubAgent_<%=vItem.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount(vItem.get(7))%>" onkeyup="ProcessRate(this, false);" onblur="ProcessRate(this, true);HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=(bHighlightColumn || !bIsEditable || vItem.get(12).equals("1") || vItem.get(19).equals("0"))?" readonly":""%>>&nbsp;
					</td>
<%
				bHighlightColumn = sAccessLevel.equals(DebisysConstants.REP);
%>
					<td align="center" valign="bottom" id="tdRep_<%=vItem.get(0)%>" style="<%=bHighlightColumn?"background-color:#80FF80":""%>">
						&nbsp;<input type="text" id="Rep_<%=vItem.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount(vItem.get(8))%>" onkeyup="ProcessRate(this, false);" onblur="ProcessRate(this, true);HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=(bHighlightColumn || !bIsEditable || vItem.get(12).equals("1") || vItem.get(19).equals("0"))?" readonly":""%>>&nbsp;
					</td>
					<td align="center" valign="bottom" id="tdMerchant_<%=vItem.get(0)%>">
						&nbsp;<input type="text" id="Merchant_<%=vItem.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount(vItem.get(9))%>" onkeyup="ProcessRate(this, false);" onblur="ProcessRate(this, true);HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=(!bIsEditable || vItem.get(12).equals("1") || vItem.get(19).equals("0") )?" readonly":""%>>&nbsp;
					</td>
<%
			if ( vPlan.get(5).equals("0") )//0 = RatePlan, 1 = Template
			{
				if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) )
				{					
%>
					  
					<td align="center" valign="bottom">
						&nbsp;<input type="text" id="TopSeller_<%=vItem.get(0)%>" style="text-align:right;" size="3" maxlength="3" value="<%=vItem.get(13)%>" onblur="ValidateTopSeller(this);" 
							<%=(vItem.get(12).equals("1"))?" readonly":""%>>&nbsp;
					</td>
					
<%					
				}
				String cacheReceiptType = vItem.get(18).toString();
				String disableControlsCache = "disabled=\"disabled\"";
				if ( SessionData.checkPermission(DebisysConstants.PERM_EDIT_CACHED_RECEIPTS) )
				{
					disableControlsCache = "";
				}
%>
					<td align="center"><input type="checkbox" id="chkCachedR_<%=vItem.get(0)%>" <%=(vItem.get(14).equals("1"))?"checked":""%> <%=disableControlsCache%> ></td>
                                        <td align="center">
						<select id="chkCachedRType_<%=vItem.get(0)%>" <%=disableControlsCache%>>
							<%for(com.debisys.utils.Base receiptTypes : listTypes)
							  {	
								  	String receTypeId = receiptTypes.getId();
								  	String receDescr  = receiptTypes.getDescription();
								  	String receiptTypeSelected="";
								  	if ( cacheReceiptType.equals(receTypeId) )
								  		receiptTypeSelected = "selected";
									%>
									<option value="<%=receTypeId%>" <%=receiptTypeSelected%> ><%=receDescr%></option>
									<%
							  }
							%>
						</select>
					</td>
<%
								
			}			
%>
					<td align="center"><input type="checkbox" id="Enable_<%=vItem.get(0)%>" <%=(vItem.get(11).equals("1"))?"checked":""%> <%=((!bIsEditable && !vPlan.get(0).equals(sRefId)) || vItem.get(16).equals("0"))?"disabled":""%>></td>
					<td align="center" style="<%=(!bIsEditable)?"display:none":""%>"><input type="checkbox" id="chkSelect_<%=vItem.get(0)%>"></td>
				</tr>
<%
				vItem = null;
			}
			it = null;
%>
			</table>
<%
			if ( bIsEditable )
			{
%>
			<br/><input type="submit" id="btnDoSave" value="<%=Languages.getString("jsp.admin.rateplans.SaveChanges",SessionData.getLanguage())%>" onclick="DoSave();return false;">
			&nbsp;&nbsp;&nbsp;<input type="button" id="btnDoRemove" value="<%=Languages.getString("jsp.admin.rateplans.RemoveProducts",SessionData.getLanguage())%>" onclick="DoRemove();">
<%
			}
			else if ( vPlan.get(0).equals(sRefId) )
			{
%>
			<br/><input type="submit" id="btnDoSave" value="<%=Languages.getString("jsp.admin.rateplans.SaveChanges",SessionData.getLanguage())%>" onclick="DoSave();return false;">
			<input type="button" id="btnDoRemove" style="display:none;" value="" onclick="">
<%
			}
			else
			{
%>
			<br/><div style="display:none;"><input type="submit" id="btnDoSave" value="" onclick="return false;">
			<input type="button" id="btnDoRemove" style="display:none;" value="" onclick=""></div>
<%
			}
%>
		</form>
		<script>
		var ProductItems = new Array();
		var sAccessLevel = <%=sAccessLevel%>;
		<%=sProductScript%>

		function ToggleProducts(bValue)
		{
			var i;
			for ( i = 0; i < ProductItems.length; i++ )
			{
				document.getElementById('chkSelect_' + ProductItems[i]).checked = bValue;
			}
		}

		function ToggleEnable(bValue)
		{
			var i;
			for ( i = 0; i < ProductItems.length; i++ )
			{
				if ( !document.getElementById('Enable_' + ProductItems[i]).disabled )
				{
					document.getElementById('Enable_' + ProductItems[i]).checked = bValue;
				}
			}
		}

		function ValidateTopSeller(ctl)
		{
			if ( ctl.value != "" )
			{
				if (isNaN(ctl.value))
				{
					ctl.value = "";
				}
				else
				{
					ctl.value = Math.abs(parseInt(ctl.value));
				}
			}
		}

		function DoRemove()
		{
			var i, sProductList;
			sProductList = "";

			for ( i = 0; i < ProductItems.length; i++ )
			{
				sProductList += document.getElementById('chkSelect_' + ProductItems[i]).checked?ProductItems[i] + ",":"";
			}

			if ( sProductList == "" )
			{
				alert("<%=Languages.getString("jsp.admin.rateplans.SelectAProduct",SessionData.getLanguage())%>");
				return;
			}

			if (confirm("<%=Languages.getString("jsp.admin.rateplans.ConfirmRemoveProducts",SessionData.getLanguage())%>"))
			{
				document.getElementById("btnDoSave").disabled = true;
				document.getElementById("btnDoRemove").disabled = true;
				$.post('admin/rateplans/rateplan_ajax.jsp?action=removeProducts&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>&data=' + sProductList + '&Random=' + Math.random(), ProcessRemove);
			}
		}

		function ProcessRemove(sData, sStatus)
		{
			if ( sStatus == "success" )
			{
				$("#divRatePlanProducts").html(sData);
				$("#divRatePlanProductsSearch").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
				$('#divRatePlanProductsSearch').load('admin/rateplans/rateplan_ajax.jsp?action=getSearchProducts&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>&Random=' + Math.random());
			}
			else
			{
				alert("<%=Languages.getString("jsp.admin.rateplans.DeleteFailed",SessionData.getLanguage())%>");
				document.getElementById("btnDoSave").disabled = false;
				document.getElementById("btnDoRemove").disabled = false;
			}
		}

		function HighlightRow(ctl, bActive)
		{
			var nID = ctl.id.split("_")[1];

			if ( bActive )
			{
				if ( ProcessRate(ctl, false) == "OK" )
				{
					document.getElementById("tdISO_" + nID).style.backgroundColor = "#4040FF";
					document.getElementById("tdAgent_" + nID).style.backgroundColor = "#4040FF";
					document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#4040FF";
					document.getElementById("tdRep_" + nID).style.backgroundColor = "#4040FF";
					document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#4040FF";
				}
			}
			else
			{
				if ( ProcessRate(ctl, true) == "OK" )
				{
					document.getElementById("tdISO_" + nID).style.backgroundColor = "";
					document.getElementById("tdAgent_" + nID).style.backgroundColor = "";
					document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "";
					document.getElementById("tdRep_" + nID).style.backgroundColor = "";
					document.getElementById("tdMerchant_" + nID).style.backgroundColor = "";
					if (sAccessLevel == <%=DebisysConstants.ISO%>)
					{
						document.getElementById("tdISO_" + nID).style.backgroundColor = "#80FF80";
					}
					else if (sAccessLevel == <%=DebisysConstants.AGENT%>)
					{
						document.getElementById("tdAgent_" + nID).style.backgroundColor = "#80FF80";
					}
					else if (sAccessLevel == <%=DebisysConstants.SUBAGENT%>)
					{
						document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#80FF80";
					}
					else if (sAccessLevel == <%=DebisysConstants.REP%>)
					{
						document.getElementById("tdRep_" + nID).style.backgroundColor = "#80FF80";
					}
				}
			}
		}

		function ProcessRate(ctl, bFormatText)
		{
			var nID = ctl.id.split("_")[1];
			var nValue = parseFloat(ctl.value);
			var nISO, nAgent, nSubAgent, nRep, nMerchant;
			var nSumRates = 0, nDifference;

			if ( isNaN(nValue) )
			{
				document.getElementById("tdISO_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdAgent_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdRep_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#C05858";
				return "INVALID_VALUE";
			}

			nValue = Math.abs(nValue);
			if ( bFormatText )
			{
				ctl.value = nValue.toFixed(2);
			}
			nISO = parseFloat(document.getElementById("ISO_" + nID).value);
			nAgent = parseFloat(document.getElementById("Agent_" + nID).value);
			nSubAgent = parseFloat(document.getElementById("SubAgent_" + nID).value);
			nRep = parseFloat(document.getElementById("Rep_" + nID).value);
			nMerchant = parseFloat(document.getElementById("Merchant_" + nID).value);

			if ( isNaN(nISO) || isNaN(nAgent) || isNaN(nSubAgent) || isNaN(nRep) || isNaN(nMerchant) )
			{
				document.getElementById("tdISO_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdAgent_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdRep_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#C05858";
				return "INVALID_SUM";
			}

			nISO = Math.abs(nISO);
			nAgent = Math.abs(nAgent);
			nSubAgent = Math.abs(nSubAgent);
			nRep = Math.abs(nRep);
			nMerchant = Math.abs(nMerchant);
			if ( bFormatText )
			{
				document.getElementById("ISO_" + nID).value = nISO.toFixed(2);
				document.getElementById("Agent_" + nID).value = nAgent.toFixed(2);
				document.getElementById("SubAgent_" + nID).value = nSubAgent.toFixed(2);
				document.getElementById("Rep_" + nID).value = nRep.toFixed(2);
				document.getElementById("Merchant_" + nID).value = nMerchant.toFixed(2);
			}
			nSumRates = nISO + nAgent + nSubAgent + nRep + nMerchant;

			if ( sAccessLevel == <%=DebisysConstants.ISO%> )
			{
				nSumRates -= nISO;
				nDifference = parseFloat(document.getElementById("SumRates_" + nID).value) - nSumRates;
				document.getElementById("ISO_" + nID).value = nDifference.toFixed(2);
			}
			else if ( sAccessLevel == <%=DebisysConstants.AGENT%> )
			{
				nSumRates -= nAgent;
				nDifference = parseFloat(document.getElementById("SumRates_" + nID).value) - nSumRates;
				document.getElementById("Agent_" + nID).value = nDifference.toFixed(2);
			}
			else if ( sAccessLevel == <%=DebisysConstants.SUBAGENT%> )
			{
				nSumRates -= nSubAgent;
				nDifference = parseFloat(document.getElementById("SumRates_" + nID).value) - nSumRates;
				document.getElementById("SubAgent_" + nID).value = nDifference.toFixed(2);
			}
			else if ( sAccessLevel == <%=DebisysConstants.REP%> )
			{
				nSumRates -= nRep;
				nDifference = parseFloat(document.getElementById("SumRates_" + nID).value) - nSumRates;
				document.getElementById("Rep_" + nID).value = nDifference.toFixed(2);
			}

			if (nDifference < 0)
			{
				document.getElementById("tdISO_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdAgent_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdRep_" + nID).style.backgroundColor = "#C05858";
				document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#C05858";
				return "EXCEED_SUM";
			}
			else
			{
				document.getElementById("tdISO_" + nID).style.backgroundColor = "#4040FF";
				document.getElementById("tdAgent_" + nID).style.backgroundColor = "#4040FF";
				document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#4040FF";
				document.getElementById("tdRep_" + nID).style.backgroundColor = "#4040FF";
				document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#4040FF";
				return "OK";
			}
		}

		function GetProductChanges(nID)
		{
			var vItemData, nISO, nAgent, nSubAgent, nRep, nMerchant, nEnable, nTopSellers, nCachedReceipt, nUseMoneyRates, nFixedFee, sItemData, nCachedReceiptType;

			vItemData = document.getElementById('Original_' + nID).value.split(",");
			nISO = parseFloat(document.getElementById("ISO_" + nID).value);
			nAgent = parseFloat(document.getElementById("Agent_" + nID).value);
			nSubAgent = parseFloat(document.getElementById("SubAgent_" + nID).value);
			nRep = parseFloat(document.getElementById("Rep_" + nID).value);
			nMerchant = parseFloat(document.getElementById("Merchant_" + nID).value);
			nEnable = document.getElementById("Enable_" + nID).checked?1:0;
			nTopSellers = document.getElementById("TopSeller_" + nID).value;
			nCachedReceipt = document.getElementById("chkCachedR_" + nID).checked?1:0;
			nCachedReceiptType = document.getElementById("chkCachedRType_" + nID).value;
			sItemData = nID + "," + nISO + "," + nAgent + "," + nSubAgent + "," + nRep + "," + nMerchant + "," + nEnable + "," + nTopSellers + "," + nCachedReceipt+ "," + nCachedReceiptType;
			if ( nISO != parseFloat(vItemData[0]) ||
				nAgent != parseFloat(vItemData[1]) ||
				nSubAgent != parseFloat(vItemData[2]) ||
				nRep != parseFloat(vItemData[3]) ||
				nMerchant != parseFloat(vItemData[4]) ||
				nEnable != vItemData[5] ||
				nTopSellers != vItemData[6] ||
				nCachedReceipt != vItemData[7] ||
				nCachedReceiptType != vItemData[8] )
			{
				if ( vItemData[8] == "1" )
				{
					nFixedFee = parseFloat(document.getElementById("FixedFee_" + nID).value);
					nISO = parseFloat((nISO * 100) / nFixedFee).toFixed(2);
					nAgent = parseFloat((nAgent * 100) / nFixedFee).toFixed(2);
					nSubAgent = parseFloat((nSubAgent * 100) / nFixedFee).toFixed(2);
					nRep = parseFloat((nRep * 100) / nFixedFee).toFixed(2);
					nMerchant = parseFloat((nMerchant * 100) / nFixedFee).toFixed(2);
					sItemData = nID + "," + nISO + "," + nAgent + "," + nSubAgent + "," + nRep + "," + nMerchant + "," + nEnable + "," + nTopSellers + "," + nCachedReceipt+ "," + nCachedReceiptType;
				}
				return sItemData + "|";
			}
			return "";
		}

		function DoSave()
		{
		
			var i, bExceedRates, sSaveData, sParams = "";

			bExceedRates = false;
			sSaveData = "";
			for ( i = 0; i < ProductItems.length; i++ )
			{
				validationResult = ProcessRate(document.getElementById('ISO_' + ProductItems[i]), true);
				<%if (SessionData.getUser().isIntranetUser()
                                            && SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
							DebisysConstants.INTRANET_PERMISSION_ALLOW_EXCEED_RATES)){%>
								if ( validationResult == "EXCEED_SUM" )
								{
									validationResult = "OK";
								}
							<%}%>							
				switch (validationResult)
				{
					case "INVALID_VALUE":
					case "INVALID_SUM":
						alert("<%=Languages.getString("jsp.admin.rateplans.ProductErrors",SessionData.getLanguage())%>");
						return;
						break;
					case "EXCEED_SUM":
						alert("<%=Languages.getString("jsp.admin.rateplans.SumExceeded",SessionData.getLanguage())%>");
						return;
						break;
					case "OK":
						sSaveData += GetProductChanges(ProductItems[i]);
						HighlightRow(document.getElementById('ISO_' + ProductItems[i]), false);
						break;
				}
			}	
			sParams += "&txtSort=" + $("#txtSort").val();
			sParams += "&txtSortDirection=" + $("#txtSortDirection").val();
			sParams += "&txtFilterID=" + escape($("#txtFilterID").val());
			sParams += "&txtFilterName=" + escape($("#txtFilterName").val());
			sParams += "&txtPageNumber=" + $("#txtPageNumber").val();
			sParams += "&ddlPageSize=" + $("#ddlPageSize").val();
			document.getElementById("btnDoSave").disabled = true;
			document.getElementById("btnDoRemove").disabled = true;			
                        debugger;
                        //$.post('admin/rateplans/rateplan_ajax.jsp?action=saveProducts&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>&data=' + sSaveData + sParams + '&Random=' + Math.random(), 
                        //    ProcessSaveResult);
                        
			$.post("admin/rateplans/rateplan_ajax.jsp", 
                            { 
                                action: "saveProducts", 
                                id: <%=request.getParameter("id")%>,
                                base: <%=request.getParameter("base")%>,
                                data: sSaveData,
                                txtSort: $("#txtSort").val(),
                                txtSortDirection: $("#txtSortDirection").val(),
                                txtFilterID: escape($("#txtFilterID").val()),
                                txtFilterName: escape($("#txtFilterName").val()),
                                txtPageNumber: $("#txtPageNumber").val(),
                                ddlPageSize: $("#ddlPageSize").val(),
                                Random: Math.random()
                            },
                        ProcessSaveResult);
                        
                        //action=saveProducts&id=<%=request.getParameter("id")%>&base=<%=request.getParameter("base")%>
                        //&data=' + sSaveData + sParams + '&Random=' + Math.random()
		}

		function ProcessSaveResult(sData, sStatus)
		{
			if ( sStatus == "success" )
			{
				$("#divRatePlanProducts").html("<span class='main'><%=Languages.getString("jsp.admin.rateplans.pleasewait",SessionData.getLanguage())%></span>");
				$("#divRatePlanProducts").html(sData);
			}
			else
			{
				alert("<%=((vPlan.get(5).equals("0"))?Languages.getString("jsp.admin.rateplans.SaveFailedP",SessionData.getLanguage()):Languages.getString("jsp.admin.rateplans.SaveFailedT",SessionData.getLanguage()))%>");
				document.getElementById("btnDoSave").disabled = false;
				document.getElementById("btnDoRemove").disabled = false;
			}
		}

		RenderSortIcon("");
		</script>
	</td></tr>
<%
			}
%>
</table>
<%
		}
		else
		{
			out.println("<img src='images/risk-icon.png'>&nbsp;<span class='main'>" + Languages.getString("jsp.admin.rateplans.NoProductsAssigned",SessionData.getLanguage()) + "</span>");
		}
	}
%>
