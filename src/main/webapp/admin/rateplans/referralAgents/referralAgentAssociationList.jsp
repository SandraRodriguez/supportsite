<%-- 
    Document   : referralAgentAssociationList
    Created on : Nov 12, 2015, 4:57:22 PM
    Author     : dgarzon
--%>

<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsAssociationPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentConf"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 7;
    int section_page = 6;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script language="JavaScript" src="/support/includes/primeui/primeui-2.0-min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/referralAgent.js"></script>
<style type="text/css">
    .ui-widget{
        font-size: 96%;
    }
    ul.errors{
        border: 1px solid #cd0a0a;
        background-color: #fff7f4;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    ul.errors li{
        padding: 2px;
        color : #cd0a0a;
    }
    div.errors{
        border:1px solid #cd0a0a;
        padding: 5px;
        line-height: 16px;
        background-color: #fff7f4;
        color: #cd0a0a;
    }
    input.new-error{
        border: 1px solid #cd0a0a;
    }
    label.new-error{
        color : #cd0a0a;
    }
    #spanWarningMessages, #spanWarningMessagesProducts{
        font-size: 17px;
        color: green;
    }
    
    .windowsFloatCharging {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.2;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }

    
</style>

<script type="text/javascript">
    
    function deleteReferralAgent(id, description) {
        
        showWindowsCharge();

        if (confirm("<%=Languages.getString("jsp.admin.rateplans.referralAgent.removeItem", SessionData.getLanguage())%> \""+description+"\"")) {
            if (isInternetExplorer()) {
                window.location.href = "referralAgentProcess.jsp?action=removeAssociation&id=" + id + "";
            }
            else {
                window.location.href = "admin/rateplans/referralAgents/referralAgentProcess.jsp?action=removeAssociation&id=" + id + "";
            }
        }
        else{
            hideWindowsCharge();
        }
    }
    
</script>


<%    String strRefId = SessionData.getProperty("ref_id");
    String iso_id = SessionData.getProperty("iso_id");
    
    String msg = request.getParameter("msg");
    msg = (msg == null)?"":msg;
    ReferralAgentConf referralAgentConf = new ReferralAgentConf();
    List<ReferralAgentsAssociationPojo> raAssociationList = referralAgentConf.getReferralAgentAssociationList();


%>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="windowsFloatCharging">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>    

<table border="0" cellpadding="0" cellspacing="0" width="1200" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentConfiguration", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">

            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr align="center">
                    <td>
                        <span id="spanWarningMessages"><%=msg%></span>
                    </td>
                </tr>
                <tr align="center">
                    <td>
                        <input type="button" name="button" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.newAssociation", SessionData.getLanguage())%>" onclick="newAssociation();">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="80%" border="0" cellspacing="1" align="center" height="40">
                            <tr class="rowhead2">
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentGeneralDescription", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentId", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentName", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentCountIsos", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentCountProducts", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.isEnabled", SessionData.getLanguage())%></td>
                                <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentOptions", SessionData.getLanguage())%></td>
                            </tr>
                            <%
                            int index = 0;
                            for (ReferralAgentsAssociationPojo raAssociation : raAssociationList) {
                                index++;
                            %>
                            <tr class="row<%=(index%2==0)?"1":"2"%>">
                                <td><%=raAssociation.getDescription()%></td>
                                <td><%=raAssociation.getReferralAgentId()%></td>
                                <td><%=raAssociation.getReferralAgentName()%></td>
                                <td align="center"><%=raAssociation.getCountIsos()%></td>
                                <td align="center"><%=raAssociation.getCountProducts()%></td>
                                <td align="center"><%=raAssociation.isEnabled()%></td>
                                <td align="center"><input type="button" name="button" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.buttonEdit", SessionData.getLanguage())%>" onclick="editReferralAgent('<%=raAssociation.getId()%>', '<%=raAssociation.getDescription()%>');">
                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.buttonDelete", SessionData.getLanguage())%>" onclick="deleteReferralAgent('<%=raAssociation.getId()%>', '<%=raAssociation.getDescription()%>');"></td>
                            </tr>
                            <%}%>
                        </table>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

<script LANGUAGE="JavaScript">


</script>

<%@ include file="/includes/footer.jsp" %>