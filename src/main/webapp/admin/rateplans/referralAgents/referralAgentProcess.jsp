<%-- 
    Document   : referralAgentDB
    Created on : Nov 19, 2015, 11:45:30 AM
    Author     : dgarzon
--%>


<%@page import="com.debisys.utils.DebisysConfigListener"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsProductsPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsIsosPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsAssociationPojo"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.debisys.rateplans.referralAgent.ProductPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentConf"%>
<%@page import="java.util.List"%>
<%@page import="com.debisys.tools.simInventory.SimInventory"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />

<%
    String action = request.getParameter("action");
    
    if(action.equals("getProductsFilter")){
        String productFilter = request.getParameter("productFilter");
        List<ProductPojo> productList = ReferralAgentConf.getProductsList(productFilter);
        for (ProductPojo p : productList) {
            out.println(p.getId()+"|"+p.getDescription()+"|"+p.getProviderName());
        }
    }
    else if(action.equals("getProductsByProvider")){
        String providerId = request.getParameter("providerId");
        List<ProductPojo> productList = ReferralAgentConf.getProductsListByProvider(Integer.parseInt(providerId));
        for (ProductPojo p : productList) {
            out.println(p.getId()+"|"+p.getDescription()+"|"+p.getProviderName());
        }
    }
    
    else if(action.equals("verifyData")){
        
        String idAssociation = request.getParameter("idAssociation");
        String referralAgent = request.getParameter("referralAgentId");
        String[] raIsos = request.getParameterValues("isoList[]");
        String[] raProducts = request.getParameterValues("productsArray[]");
        
        List<String> listData = ReferralAgentConf.verifyAssociationData(idAssociation, referralAgent, raIsos, raProducts);
        for(String current: listData){
            out.println(current);
        }
        
    }
    
    else if(action.equals("insertReferralAgent") || action.equals("editReferralAgent")){
        
        String idAssociation = request.getParameter("idAssociation");
        String isEnabled = request.getParameter("isEnabled");
        String shortDescription = request.getParameter("shortDescription");
        String referralAgent = request.getParameter("referralAgentId");
        String[] raIsosArray = request.getParameterValues("isosList[]");
        String[] raProductsArray = request.getParameterValues("productsArray[]");
        
        List<String> listIsos = Arrays.asList(raIsosArray);
        List<String> listProducts = Arrays.asList(raProductsArray);
        
        ReferralAgentsAssociationPojo raAssociation = new ReferralAgentsAssociationPojo();
        raAssociation.setDescription(shortDescription);
        raAssociation.setReferralAgentId(new BigDecimal(referralAgent));
        raAssociation.addIsoToList(listIsos);
        raAssociation.addProductToList(listProducts);
        
        raAssociation.setEnabled((isEnabled.trim().equals("1"))?true:false);
        
        if(action.equals("insertReferralAgent")){
            ReferralAgentConf.insertReferralAgents(SessionData, application, raAssociation);
        }
        else if (action.equals("editReferralAgent")){
            raAssociation.setId(idAssociation);
            ReferralAgentConf.updateReferralAgents(SessionData, application, raAssociation);
        }
        
        response.sendRedirect("referralAgentAssociationList.jsp##");
    }
    
    else if(action.equals("removeAssociation")){
        String idAssociation = request.getParameter("id");
        ReferralAgentConf.deleteRaAssociations(SessionData, application, idAssociation);
        response.sendRedirect("referralAgentAssociationList.jsp##");
    }
    
%>




