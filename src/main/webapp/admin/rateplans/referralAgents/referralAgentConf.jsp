<%-- 
    Document   : referralAgentAssociationList
    Created on : Nov 17, 2015, 4:57:22 PM
    Author     : dgarzon
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.debisys.rateplans.referralAgent.ProvidersPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ProductPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsProductsPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.IsosPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsIsosPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentsAssociationPojo"%>
<%@page import="com.debisys.rateplans.referralAgent.ReferralAgentConf"%>
<%@ page import="java.net.URLEncoder,
         com.debisys.utils.HTMLEncoder,
         java.util.*,
         com.debisys.reports.TransactionReport" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
    int section = 7;
    int section_page = 6;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script type="text/javascript" src="js/referralAgent.js"></script>

<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jquery.searchabledropdown-1.0.8.min.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/primeui/primeui-2.0-min.js" language="JavaScript" charset="utf-8"></script>

<style type="text/css">
    
    .toolTip {padding-right: 20px;background: url(images/help.gif) no-repeat right;color: #3366FF;cursor: help;position: relative; with: 18px; height: 18px;}
	.toolTipWrapper {width: 175px;position: absolute;top: 20px;display: none;color: #FFF;font-weight: bold;font-size: 9pt; z-index: 100}
	.toolTipTop {width: 175px;height: 30px;background: url(images/bubbleTop.gif) no-repeat;}
	.toolTipMid {padding: 8px 15px;background: #A1D40A url(images/bubbleMid.gif) repeat-x top;}
	.toolTipBtm {height: 13px;background: url(images/bubbleBtm.gif) no-repeat;}
    
    #spanWarningMessages, #spanWarningMessagesProducts{
        font-size: 17px;
        color: red;
    }
    #spanInfo{
        font-size: 20px;
        color: #088A08;
    }

    #selectAllIsos{
        width:300px;   
    }
    #selectAllIsos option{
        width:300px;   
    }
    
    #productsTableSelected {
        border-collapse: collapse;
        border: 1px solid #E8E8E8;
    }
    #productsTableSelected td {
        border: 1px solid #E8E8E8;
    }
    .field_set{
        border-color:#AFD855;
        border-style: ridge;
    }
    
    .windowsFloatCharging {
        position:absolute;
        margin-left:auto;
        margin-right:auto;
        background-color: #6E6E6E;
        opacity: 0.4;
        filter: alpha(opacity=20); /* For IE8 and earlier */
        text-align: center;
    }
   
</style>

<script type="text/javascript">
    
    
    function addIsos() {
        var currentId = $("#selectAllIsos").val();
        var txt = $("#selectAllIsos option:selected").text();

        if (currentId === '-1') {
            return;
        }
        var currentSeletedValues = $.map($('#referralAgentIsos option'), function (e) {
            return e.value;
        });
        for (var j = 0; j < currentSeletedValues.length; j++) {
            if (currentId === currentSeletedValues[j]) {
                $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.isoExist", SessionData.getLanguage())%> ").show().fadeOut(10000);
                return;
            }
        }
        $("#referralAgentIsos").append("<option value=\"" + currentId + "\">" + txt + "</option>");
    }

    function changeProviders() {
        var providerSelected = $('#selectProviders').val();
        if (providerSelected === '-1') {
            return;
        }
        $("#productsTable").empty();
        $.ajax({
            type: "POST",
            async: false,
            data: {action: 'getProductsByProvider', providerId: providerSelected},
            url: "admin/rateplans/referralAgents/referralAgentProcess.jsp",
            success: function (data) {
                processProductInfo(data);
            }
        });
        openDialog('dialogProducts', '<%=Languages.getString("jsp.admin.rateplans.referralAgent.productList", SessionData.getLanguage())%> > Provider ' + providerSelected);
    }

    function addProducts() {
        // table tr td input[type="checkbox" checked]    input:checked
        var searchIDs = $('.checkBoxProducts:checked').map(function () {
            return $(this).val();
        });

        var generalRatePopup = $('#generalRatePopup').val();
        if (generalRatePopup === '') {
            generalRatePopup = '0';
        }

        var newRow = '';
        for (var i = 0; i < searchIDs.length; i++) {
            var productDescription = $("#hidden_" + searchIDs[i]).val();
            var productProviderName = $("#hiddenProvider_" + searchIDs[i]).val();
            var currentRate = $('#rate_' + searchIDs[i]).val(); // look if this product already exist in the current table in order to not add it.
            if ((typeof currentRate === "undefined") ) {
                if ((searchIDs[i] !== '0')) {
                    newRow = '';
                    newRow += '<tr id="trId_'+searchIDs[i]+'">';
                    newRow += '<td align="center"><input type="checkbox" class="checkBoxProductsToDelete" value="'+searchIDs[i]+'" /><a href=\"javascript:void(0);\" class=\"remove_button\" title=\"Remove product\"><img src=\"images/details_close.png\"/></a></td>';
                    newRow += '<td>' + searchIDs[i] + ' <input type="hidden" class="hiddenProducts" id="hiddenProducts_' + searchIDs[i] + '" value="' + searchIDs[i] + '"></td>';
                    newRow += '<td style="width: 50%">' + productDescription + '</td>';
                    newRow += '<td style="width: 50%">' + productProviderName + '</td>';
                    newRow += '<td class="main" align="center" ><input type="text" class="numeric" id="rate_' + searchIDs[i] + '" value="' + generalRatePopup + '" size="10" maxlength="10"/>%</td>';
                    newRow += '</tr>';
                    $('#productsTableSelected').append(newRow);
                }
            }
        }
        setNumericKey();
        if (searchIDs.length === 0) {
            $("#spanWarningMessagesProducts").text("<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.noProductsSelected", SessionData.getLanguage())%> ").show().fadeOut(10000);
        }
        $('#dialogProducts').dialog("close");
    }
    
    function searchProductEnter(event) {
        if(event.keyCode == 13){
            searchProduct();
        }
    }
    
    function searchProduct() {
        var productFilter = $("#productFilter").val();
        if (productFilter.trim() == '') {
            $("#productFilter").css("border", "1px solid red");
            return false;
        }
        else {
            $("#productFilter").css('border', '');
        }

        $("#productsTable").empty();
        $.ajax({
            type: "POST",
            async: false,
            data: {action: 'getProductsFilter', productFilter: productFilter},
            url: "admin/rateplans/referralAgents/referralAgentProcess.jsp",
            success: function (data) {
                processProductInfo(data);
            }
        });

        openDialog('dialogProducts', productFilter);
    }

    function processProductInfo(data) {
        var array_prod = String($.trim(data)).split("\n");
        $('#productsTable').append('<tr class="rowhead2"><td><input type="checkbox" id="checkAll" value="0" onclick="selectAllproducts();"/></td><td><%=Languages.getString("jsp.admin.rateplans.referralAgent.productId", SessionData.getLanguage())%></td><td>\n\
    <%=Languages.getString("jsp.admin.rateplans.referralAgent.productDescription", SessionData.getLanguage())%></td></tr>');
        for (var i = 0; i < array_prod.length; i++) {
            if (array_prod[i] !== '') {
                var merchantData = String($.trim(array_prod[i])).split("|");
                $('#productsTable').append('<tr class="row' + ((i % 2 === 0) ? '1' : '2') + '"><td><input type="checkbox" class="checkBoxProducts" value="' + merchantData[0] + '" /></td><td>'
                        + merchantData[0] + '</td><td>' + merchantData[1] + '<input type="hidden" id="hidden_' + merchantData[0] + '" value="' + merchantData[1] + '"><input type="hidden" id="hiddenProvider_' + merchantData[0] + '" value="' + merchantData[2] + '"></td></tr>');
            }
        }
    }

    function openDialog(dialogId, msg) {

        $('#' + dialogId).dialog({
            title: msg,
            maxWidth: 700,
            maxHeight: 500,
            width: 700,
            height: 500,
            modal: true
        });
    }


    function selectAllproducts() {
        // select all products in popup dialog
        $('#checkAll').change(function () {
            $('#productsTable tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
    }
    
    function selectAllproductsToDelete(){
        $('#checkAllProductsToDeleted').change(function () {
            $('#productsTableSelected tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
        });
    }
    
    function removeSelectedProducts(){
        
        var productIdsToDelete = $('.checkBoxProductsToDelete:checked').map(function () {
            return $(this).val();
        });
        
        for (var i = 0; i < productIdsToDelete.length; i++) {
            $('table#productsTableSelected tr#trId_'+productIdsToDelete[i]).remove();
        }
        $('#checkAllProductsToDeleted').prop('checked',false);
    }

    function changeAllRates(event) {
        //event.keyCode >= 48
        if(event.keyCode == 13){
            var generalRate = String($.trim($('#genericRate').val()));
            if(generalRate !== ''){
                $('#genericRate').blur();
                var msg = '<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.confirmChangeRates", SessionData.getLanguage())%>'.replace("NUM", generalRate);
                if (confirm(msg)) {
                    $('#productsTableSelected tr td input[type="text"]').prop('value', generalRate);
                }
            }
        }
    }

    function verifyAndSave() {
        
        showWindowsCharge();
        $( "#submit" ).prop( "disabled", true );
        removeRedBorder();
        var hasErrors = false;

        if ($('#shortDescription').val() == '') {
            $("#shortDescription").css("border", "1px solid red");
            hasErrors = true;
        }

        if ($('#referralAgentList').val() == '-1') {
            $("#referralAgentList").css("border", "1px solid red");
            hasErrors = true;
        }


        var selectIsos = $.map($('#referralAgentIsos option'), function (e) {
            return e.value;
        });
        if (selectIsos.length == 0) {
            $("#referralAgentIsos").css("border", "1px solid red");
            hasErrors = true;
        }

        // tables ratesvalues
        var rateArray = [];
        $('#productsTableSelected tr td input[type="text"]').each(function () {
            rateArray.push(this.value);
        });
        if (rateArray.length <= 1) {
            $("#generalProductsTable").css("border", "1px solid red");
            hasErrors = true;
        }

        if (hasErrors) {
            hideWindowsCharge();
            $( "#submit" ).prop( "disabled", false );
            $("#spanWarningMessages").text("<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.redFieldsAreMandatory", SessionData.getLanguage())%> ").show().fadeOut(15000);
            return false;
        }

        hasErrors = isErrorGeneralData(selectIsos);


        if (hasErrors) {
            hideWindowsCharge();
            $( "#submit" ).prop( "disabled", false );
            return false;
        }

        saveAssociation(selectIsos);
        $('#referralAgentIsos option').prop('selected', true); // Select All elements to send


    }

    function isErrorGeneralData(isoList) {

        var referralAgentId = $('#referralAgentList').val();
        var idAssociation = $('#idAssociation').val();
        var productsArray = [];
        $('.hiddenProducts').each(function () {
            productsArray.push(this.value);
        });

        var existErrors = false;
        $("#errorTable").empty();
        $.ajax({
            type: "POST",
            async: false,
            data: {action: 'verifyData', idAssociation: idAssociation, referralAgentId: referralAgentId, isoList: isoList, productsArray: productsArray},
            url: "admin/rateplans/referralAgents/referralAgentProcess.jsp",
            success: function (data) {
                var array_data = String($.trim(data)).split("\n");
                $('#errorTable').append('<tr class="rowhead2"><td>ISO</td><td>Product</td></tr>');
                for (var i = 0; i < array_data.length; i++) {
                    if (array_data[i] !== '') {
                        var parseData = String($.trim(array_data[i])).split(",");
                        $('#errorTable').append('<tr class="row' + ((i % 2 === 0) ? '1' : '2') + '"><td>' + parseData[0] + '</td><td>' + parseData[1] + '</td></tr>');
                    }
                }
                if (array_data.length === 1 && array_data[0] === '') {
                    existErrors = false;
                }
                else if (array_data.length > 0) {
                    openDialog('dialogErrors', '<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.validationError", SessionData.getLanguage())%>');
                    existErrors = true;
                }

            }
        });
        return existErrors;
    }

    function saveAssociation(isoList) {

        var shortDescription = $('#shortDescription').val();
        var action = $('#action').val();
        var referralAgentId = $('#referralAgentList').val();
        var idAssociation = $('#idAssociation').val();
        var productsArray = [];
        $('.hiddenProducts').each(function () {
            productsArray.push(this.value);
        });
        
        var isEnabled = "0";
        if($("#enabledAssociation").prop('checked') === true){
            isEnabled = "1";
        }        

        productsArray = getRates(productsArray);
        $.ajax({
            type: "POST",
            async: false,
            data: {action: action, isEnabled: isEnabled, shortDescription: shortDescription, idAssociation: idAssociation, referralAgentId: referralAgentId, isosList: isoList, productsArray: productsArray},
            url: "admin/rateplans/referralAgents/referralAgentProcess.jsp",
            success: function (data) {
                if(action === 'insertReferralAgent'){
                    goBack('<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.insertSuccess", SessionData.getLanguage())%>');
                }
                else{
                    goBack('<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.updateSuccess", SessionData.getLanguage())%>');
                }
            }
        });
    }

    function getRates(productsArray) {
        for (var i = 0; i < productsArray.length; i++) {
            productsArray[i] = productsArray[i]+'_'+$('#rate_'+productsArray[i]).val();
        }
        return productsArray;
    }

    function removeRedBorder() {
        $("#generalProductsTable").css('border', '');
        $("#shortDescription").css('border', '');
        $("#referralAgentList").css('border', '');
        $("#referralAgentIsos").css('border', '');
        $("#productsTableSelected").css('border', '');
    }


</script>


<%
    String strRefId = SessionData.getProperty("ref_id");
    String iso_id = SessionData.getProperty("iso_id");
    String action = request.getParameter("action");
    String textWarning = "";
    String textWarningProducts = "";
    int indexProducts = 0;
    String actionValue = "insertReferralAgent";
    boolean enabled = true;

    String id = request.getParameter("id");
    ReferralAgentConf referralAgentConf = new ReferralAgentConf();
    ReferralAgentsAssociationPojo currentAssociation = new ReferralAgentsAssociationPojo();
    List<ReferralAgentsIsosPojo> isosListCurrent = new ArrayList<ReferralAgentsIsosPojo>();
    List<ReferralAgentsProductsPojo> productListCurrent = new ArrayList<ReferralAgentsProductsPojo>();
    if (action != null && action.trim().equals("edit")) {
        currentAssociation = referralAgentConf.getReferralAgentAssociation(id);
        isosListCurrent = currentAssociation.getIsosList();
        productListCurrent = currentAssociation.getProductsList();
        actionValue = "editReferralAgent";
        enabled = currentAssociation.isEnabled();
    }

    List<ReferralAgentsPojo> referralAgentList = referralAgentConf.getReferralAgentList();
    List<IsosPojo> allIsosList = referralAgentConf.getIsosList();
    List<ProvidersPojo> allProviderList = referralAgentConf.getProvidersList();


%>

<div id="div_show_charge" name="div_show_charge" style="display:none;width:100%;height:100%;" class="windowsFloatCharging">
    <img src="support/../images/loading.gif" style="width:120px;height:120px;">
</div>    


<table border="0" cellpadding="0" cellspacing="0" width="1200" style="margin: 5px">
    <tr style="background-color: #84AE27; height: 20px">
        <td style="padding-left:10px; border-left: 1px solid #62891E; border-right: 1px solid #62891E;" class="formAreaTitle">
            <%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentConfiguration", SessionData.getLanguage())%>
        </td>
    </tr>
    <tr>
        <td bgcolor="#FFFFFF" class="formArea2" style="padding: 5px;">


            <table id="formContentArea" border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="10" align="center" height="40">
                            <tr>
                                <td align="right"><%=Languages.getString("jsp.admin.rateplans.referralAgent.isEnabled", SessionData.getLanguage())%></td>
                                <td><input type="checkbox" name="enabledAssociation" id="enabledAssociation"  <%=(enabled)?"checked":""%> ></td>
                            </tr>
                            <tr>
                                <td align="right"><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgentGeneralDescription", SessionData.getLanguage())%></td>
                                <td><input type="text" id="shortDescription" value="<%=currentAssociation.getDescription()%>" size="40" maxlength="50"/></td>
                            </tr>
                            <tr >
                                <td align="right"><%=Languages.getString("jsp.admin.rateplans.referralAgent.referralAgent", SessionData.getLanguage())%></td>
                                <td>                                        
                                    <select name="referralAgentList" id = "referralAgentList">
                                        <option value="-1"></option>
                                        <%for (ReferralAgentsPojo ra : referralAgentList) {%>
                                        <option value="<%=ra.getRepId()%>" <%=(ra.getRepId().toString().equalsIgnoreCase(currentAssociation.getReferralAgentId().toString())) ? "selected" : ""%>><%=ra.getBusinessName()%></option>
                                        <%}%>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1" align="right"> <input type="button" name="button" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.back", SessionData.getLanguage())%>" onclick="goBack('');"></td>
                                <td colspan="1">
                                    <input type="hidden" name="action" id="action" value="<%=actionValue%>" />
                                    <input type="hidden" name="idAssociation" id="idAssociation" value="<%=id%>" />
                                    <input type="button" name="submit" id="submit" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.saveChanges", SessionData.getLanguage())%>" align="center" onclick="verifyAndSave()" />
                                </td>

                            </tr>
                            <tr>
                                <td colspan="1"> <span id="spanWarningMessages"><%=textWarning%></span></td>
                                <td colspan="1"> <span id="spanWarningMessagesProducts"><%=textWarningProducts%></span></td>
                            </tr>
                            <tr>
                                <td width="40%" style="vertical-align: top;">
                                    <fieldset class="field_set">
                                        <legend><%=Languages.getString("jsp.admin.rateplans.referralAgent.isoList", SessionData.getLanguage())%></legend>
                                        <table width="100%" border="0" cellspacing="1" align="center" height="40">
                                            <tr>
                                                <td style="width: 20%"><%=Languages.getString("jsp.admin.rateplans.referralAgent.search", SessionData.getLanguage())%></td>
                                                <td style="width: 80%">
                                                    <select name="selectAllIsos" id = "selectAllIsos">
                                                        <option value="-1"></option>
                                                        <%for (IsosPojo iso : allIsosList) {%>  
                                                        <option value="<%=iso.getIsoId()%>"><%=iso.getIsoId() + " - " + iso.getBusinesName()%></option>
                                                        <%}%>
                                                    </select>
                                                </td>
                                                <td>
                                                    <span class="toolTip" title="<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.msgSelectIsos", SessionData.getLanguage())%>"></span>
                                                </td>
                                                <td>
                                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.add", SessionData.getLanguage())%>" onclick="addIsos();">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <select name="referralAgentIsos" id = "referralAgentIsos" size="15" multiple="" style="width: 100%">
                                                        <%for (ReferralAgentsIsosPojo referralAgentIso : isosListCurrent) {%>  
                                                        <option value="<%=referralAgentIso.getIsoId()%>"><%=referralAgentIso.getIsoId() + " - " + referralAgentIso.getBusinessName()%></option>
                                                        <%}%>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr align="center">
                                                <td colspan="2" >
                                                    <input type="button" name="button" value="<%=Languages.getString("jsp.admin.tools.stockLevels.buttonRemoveSelectItems", SessionData.getLanguage())%>" onclick="removeSelectItems();">
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                                <td width="60%">
                                    <fieldset class="field_set">
                                        <legend><%=Languages.getString("jsp.admin.rateplans.referralAgent.productList", SessionData.getLanguage())%></legend>
                                        <table id="generalProductsTable" width="100%" border="0" cellspacing="1" align="center" >
                                            <tr>
                                                <td style="width: 20%" align="center"><%=Languages.getString("jsp.admin.rateplans.referralAgent.filter", SessionData.getLanguage())%></td>
                                                <td style="width: 50%" align="center">
                                                    <input type="text" name="productFilter" id="productFilter" value="" size="30" maxlength="30" onkeyup="searchProductEnter(event)"/>
                                                </td>
                                                <td style="width: 20%">
                                                    <input type="button" name="buttonSearch" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.search", SessionData.getLanguage())%>" onclick="searchProduct();">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%" align="center"><%=Languages.getString("jsp.admin.rateplans.referralAgent.provider", SessionData.getLanguage())%></td>
                                                <td style="width: 50%" align="center">
                                                    <select name="selectProviders" id = "selectProviders" style="width: 55%" onchange="changeProviders()">
                                                        <option value="-1"></option>
                                                        <%for (ProvidersPojo provider : allProviderList) {%>  
                                                        <option value="<%=provider.getProvider_id()%>"><%=provider.getProvider_id() + " - " + provider.getName()%></option>
                                                        <%}%>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" align="right"><span class="toolTip" align="center" title="<%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.msgChangeAllRates", SessionData.getLanguage())%>"></span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <!-- PRODUCTS TABLE -->
                                                    <table id="productsTableSelected" class= "productsTableSelected" border="1" width="100%" cellspacing="1" align="center">
                                                        <tr class="rowhead2">
                                                            <td>
                                                                <input type="checkbox" id="checkAllProductsToDeleted" value="0" onclick="selectAllproductsToDelete();"/>
                                                                <br>
                                                                <input type="button" id="deleteOnly" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.buttonRemoveAll", SessionData.getLanguage())%>" onclick="removeSelectedProducts();">
                                                            </td>
                                                            <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.productId", SessionData.getLanguage())%></td>
                                                            <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.productDescription", SessionData.getLanguage())%></td>
                                                            <td><%=Languages.getString("jsp.admin.rateplans.referralAgent.providerColumn", SessionData.getLanguage())%></td>
                                                            <td>
                                                                <%=Languages.getString("jsp.admin.rateplans.referralAgent.rates", SessionData.getLanguage())%>
                                                                <input type="text" class="numeric" id="genericRate" name="genericRate" value="" size="10" maxlength="10" onkeyup="changeAllRates(event)" /> %
                                                            </td>
                                                        </tr>
                                                        <%
                                                            String linkRemove = "<a href=\"javascript:void(0);\" class=\"remove_button\" title=\"Remove product\"><img src=\"images/details_close.png\"/></a>";
                                                            for (ReferralAgentsProductsPojo raProduct : productListCurrent) {
                                                                indexProducts++;
                                                        %>
                                                        <tr class="roww<%=(indexProducts % 2 == 0) ? "1" : "2"%>" id="trId_<%=raProduct.getProductId()%>">
                                                            <td align="center">
                                                                <input type="checkbox" class="checkBoxProductsToDelete" value="<%=raProduct.getProductId()%>" />
                                                                <%=linkRemove%>
                                                            </td>
                                                            <td><%=raProduct.getProductId()%> <input type="hidden" class="hiddenProducts" id="hiddenProducts_<%=raProduct.getProductId()%>" value="<%=raProduct.getProductId()%>"></td>
                                                            <td style="width: 50%"><%=raProduct.getProductDescription()%></td>
                                                            <td style="width: 10%"><%=raProduct.getProviderName()%></td>
                                                            <td class="main" align="center" >
                                                                <input type="text" class="numeric" id="rate_<%=raProduct.getProductId()%>" value="<%=raProduct.getRate()%>" size="10" maxlength="10"/>%
                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>
                                    </fieldset>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>

    </tr>
</table>

<!-- Popup dialog -->
<div id="dialogProducts" style="display:none;">
    <input type="button" name="buttonAddProduct" value="<%=Languages.getString("jsp.admin.rateplans.referralAgent.add", SessionData.getLanguage())%>" onclick="addProducts();" align="center">
    <%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.generalRateForTheseProducts", SessionData.getLanguage())%>
    <input type="text" class="numeric" id="generalRatePopup" value="0" size="10" maxlength="10"/>
    <table id="productsTable" class= "productsTable" width="100%" border="0" cellspacing="1" align="center" >
        <!-- Products filtered-->
    </table>
</div>

<div id="dialogErrors" style="display:none;">
    <span id="spanErrorMessages"><%=Languages.getString("jsp.admin.rateplans.referralAgent.warn.validationData", SessionData.getLanguage())%></span>
    <table id="errorTable" class= "errorTable" width="100%" border="0" cellspacing="1" align="center" >
        <!-- Isos vs products Error validation-->
    </table>
</div>




<script>


</script>

<%@ include file="/includes/footer.jsp" %>