<%@page import="com.debisys.rateplans.RatePlan"%>
<%@page import="com.debisys.users.SessionData"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.utils.DebisysConstants"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.debisys.utils.JSONObject"%>
<%@page import="com.debisys.rateplans.RatePlanWorker"%>
<%@page import="com.debisys.rateplans.RatePlanWorker.Actions"%>
<%@page import="com.debisys.utils.NumberUtil"%>
<%@page import="com.debisys.customers.Rep"%>
<%@page import="com.debisys.utils.StringUtil"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request" />
<%
	response.setCharacterEncoding("iso-8859-1");

	int section = 7;
	int section_page = 5;

	Hashtable<String, String> htLevels = new Hashtable<String, String>();
	htLevels.put(DebisysConstants.ISO, "ISO");
	htLevels.put(DebisysConstants.AGENT, "AGENT");
	htLevels.put(DebisysConstants.SUBAGENT, "SUBAGENT");
	htLevels.put(DebisysConstants.REP, "REP");

	Vector<Vector<String>> vData = null;
	if ( request.getParameter("action").equals("getHierarchy") )
	{
		int nLevel = Integer.parseInt(request.getParameter("level"));
		if ( !request.getParameter("id").equals("0") )//If we need to retrieve the hierarchy of a specific plan
		{
			String sCarrier = null;
			if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
			{
				sCarrier = SessionData.getUser().getRefId();
			}
			vData = RatePlan.getRatePlanChildrenHierarchy("0", request.getParameter("id"), SessionData.getProperty("RatePlan_ProductFilter"), sCarrier);
			if ( vData.size() > 0 )
			{
				Vector<HashMap> vMap = new Vector<HashMap>();

				String sPadding = "";
				for (int i = 0; i < nLevel; i++)
				{
					sPadding += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				}

				Iterator<Vector<String>> itData = vData.iterator();
				while ( itData.hasNext() )
				{
					Vector<String> vTemp = itData.next();

					String sPlusMinus = "<img src=\"/support/images/nodebox.png\" style=\"vertical-align:middle;\">&nbsp;&nbsp;";
					if ( !vTemp.get(8).equals("0") )
					{
						sPlusMinus = "<a id=\"aOpen_" + vTemp.get(0) + "\" href=\"javascript:OpenNode(" + vTemp.get(0) + ", " + (nLevel + 1) + ");\" style=\"display:inline;\"><img src=\"/support/images/plusbox.png\" style=\"vertical-align:middle;\" border=\"0\"></a>";
						sPlusMinus += "<a id=\"aClose_" + vTemp.get(0) + "\" href=\"javascript:CloseNode(" + vTemp.get(0) + ");\" style=\"display:none;\"><img src=\"/support/images/minusbox.png\" style=\"vertical-align:middle;\" border=\"0\"></a>&nbsp;&nbsp;";
					}

					String sNameTmp = "";
					boolean bNeedsPadding = false;
					while (vTemp.get(1).length() > 40)
					{
						sNameTmp += bNeedsPadding?sPadding + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;":"";
						sNameTmp += vTemp.get(1).substring(0, 40) + "<br/>";
						vTemp.set(1, vTemp.get(1).substring(40));
						bNeedsPadding = true;
					}
					sNameTmp += bNeedsPadding?sPadding + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;":"";
					sNameTmp += vTemp.get(1);

					String sItemData = "";
					sItemData += "<td style=\"text-align:left;\" nowrap>" + sPadding + sPlusMinus + "[" + vTemp.get(0) + "]&nbsp;" + sNameTmp + "</td>";
					sItemData += "<td>&nbsp;" + (vTemp.get(4).equals("1") ? Languages.getString("jsp.admin.rateplans.TypeTemplate",SessionData.getLanguage()) : Languages.getString("jsp.admin.rateplans.TypePlan",SessionData.getLanguage())) + "&nbsp;</td>";
					sItemData += "<td align=\"center\">&nbsp;" + vTemp.get(3) + "&nbsp;</td>";
					sItemData += "<td nowrap>&nbsp;" + htLevels.get(vTemp.get(7)) + "&nbsp;-&nbsp;(" + vTemp.get(5) + ")&nbsp;" + vTemp.get(6) + "&nbsp;</td>";
					sItemData += "<td align=\"center\">";
					sItemData += "<input id=\"chk_" + vTemp.get(0) + "\" type=\"checkbox\" " + (request.getParameter("checked").equals("1")?"checked disabled":"") + " onclick=\"ToggleChecks(this.id.replace('chk_', ''), this.checked);\" onchange=\"ToggleChecks(this.id.replace('chk_', ''), this.checked);\">";
					sItemData += "</td>";

					HashMap<String, String> m = new HashMap<String, String>();
					m.put("id", vTemp.get(0));
					m.put("level", Integer.toString(nLevel));
					m.put("data", sItemData);
					vMap.add(m);
					vTemp = null;
				}
				itData = null;

				JSONObject jo = new JSONObject();
				jo.put("items", vMap);
				out.print(jo.toString());
				jo = null;
			}
		}//End of If we need to retrieve the hierarchy of a specific plan
		else
		{//Else we need to retrieve all the first level children nodes of the logged-in user
			String sOwnerID, sAccessLevel;
			sAccessLevel = SessionData.getProperty("access_level");
			if ((SessionData.getUser().isIntranetUser() && 
				SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
				|| SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
				)
			{
				sOwnerID = "0";
			}
			else
			{
				sOwnerID = SessionData.getProperty("ref_id");
			}

			String sCarrier = null;
			if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
			{
				sCarrier = SessionData.getUser().getRefId();
			}
			SessionData.setProperty("RatePlan_ProductFilter", request.getParameter("filter"));
			vData = RatePlan.getRatePlanChildrenHierarchy(sOwnerID, "0", request.getParameter("filter"), sCarrier);
			if ( vData.size() > 0 )
			{
				String sNextStep = "";
				String sPrevStep = "";
				if ( request.getParameter("op").equals("ADD") )
				{
					sNextStep = "DoShowAddProducts();";
					sPrevStep = "DoReturnToMenu();";
				}
				else if ( request.getParameter("op").equals("REMOVE") )
				{
					sNextStep = "DoShowRemoveProducts();";
					sPrevStep = "DoReturnToMenu();";
				}
				else if ( request.getParameter("op").equals("UPDATE") )
				{
					sNextStep = "DoConfirmUpdateRates();";
					sPrevStep = "DoReturnToUpdateProducts();";
				}
%>
	<input type="button" value="&lt;&lt; <%=Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage())%>" onclick="<%=sPrevStep%>">&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.rateplans.Next",SessionData.getLanguage())%> &gt;&gt;" onclick="<%=sNextStep%>"><br/><br/>
	<table id="tabPlans" width="100%" cellspacing="1" cellpadding="1" border="0">
		<tr class="SectionTopBorder">
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.Type",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.Level",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=Languages.getString("jsp.admin.rateplans.CreatedBy",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap">
<%
				out.println("&nbsp;<label for=\"chkSelectPlan\">" + Languages.getString("jsp.admin.rateplans.Select_Deselect",SessionData.getLanguage()) + "</label>&nbsp;<br/><input type=\"checkbox\" id=\"chkSelectPlan\" onclick=\"ToggleSelect(this.checked);\">");
%>
			</td>
		</tr>
<%
				Iterator<Vector<String>> itData = vData.iterator();
				String sSelectScript = "";
				int nCounter = 1;
	
				while ( itData.hasNext() )
				{
					Vector<String> vTemp = itData.next();
					sSelectScript += "SelectItems[" + (nCounter++ - 1) + "] = " + vTemp.get(0) + ";";

					String sNameTmp = "";
					boolean bNeedsPadding = false;
					while (vTemp.get(1).length() > 40)
					{
						sNameTmp += bNeedsPadding?"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;":"";
						sNameTmp += vTemp.get(1).substring(0, 40) + "<br/>";
						vTemp.set(1, vTemp.get(1).substring(40));
						bNeedsPadding = true;
					}
					sNameTmp += bNeedsPadding?"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;":"";
					sNameTmp += vTemp.get(1);

					String sPlusMinus = "<img src=\"/support/images/nodebox.png\" style=\"vertical-align:middle;\">&nbsp;&nbsp;";
					if ( !vTemp.get(8).equals("0") )
					{
						sPlusMinus = "<a id=\"aOpen_" + vTemp.get(0) + "\" href=\"javascript:OpenNode(" + vTemp.get(0) + ", 1);\" style=\"display:inline;\"><img src=\"/support/images/plusbox.png\" style=\"vertical-align:middle;\" border=\"0\"></a>";
						sPlusMinus += "<a id=\"aClose_" + vTemp.get(0) + "\" href=\"javascript:CloseNode(" + vTemp.get(0) + ");\" style=\"display:none;\"><img src=\"/support/images/minusbox.png\" style=\"vertical-align:middle;\" border=\"0\"></a>&nbsp;&nbsp;";
					}
%>
		<tr id="tr_<%=vTemp.get(0)%>" class="Hierarchy0">
			<td style="text-align:left;" nowrap="nowrap"><%=sPlusMinus%>[<%=vTemp.get(0)%>]&nbsp;<%=sNameTmp%></td>
			<td>&nbsp;<%=(vTemp.get(4).equals("1") ? Languages.getString("jsp.admin.rateplans.TypeTemplate",SessionData.getLanguage()) : Languages.getString("jsp.admin.rateplans.TypePlan",SessionData.getLanguage()))%>&nbsp;</td>
			<td align="center">&nbsp;<%=vTemp.get(3)%>&nbsp;</td>
<%
					if (vTemp.get(6).equals("EMIDA"))
					{
						out.println("<td nowrap>EMIDA</td>");
					}
					else
					{
						out.println("<td nowrap>&nbsp;" + htLevels.get(vTemp.get(7)) + "&nbsp;-&nbsp;(" + vTemp.get(5) + ")&nbsp;" + vTemp.get(6) + "&nbsp;</td>");
					}
%>
			<td align="center">
				<input id="chk_<%=vTemp.get(0)%>" type="checkbox" onclick="ToggleChecks(this.id.replace('chk_', ''), this.checked);" onchange="ToggleChecks(this.id.replace('chk_', ''), this.checked);">
			</td>
		</tr>
<%
					vTemp = null;
				}
				itData = null;
%>
	</table>
	<script>
	var SelectItems = new Array();
	<%=sSelectScript%>
	var vPlansChildren = {};
	</script>
<%
				if ( request.getParameter("op").equals("UPDATE") )
				{
					boolean bHideCell;
					SessionData.setProperty("RatePlan_SelectedPlans", request.getParameter("plans"));
%>
	<br/><hr/>
	<span style="width:100%;text-align:left;"><img src='images/information.png' style="vertical-align:middle;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.EnterRates",SessionData.getLanguage())%></span><br/><br/>
	<table id="tabRates" width="100%" cellspacing="1" cellpadding="1" border="0">
		<tr class="SectionTopBorder">
			<td></td>
<%
			if ((SessionData.getUser().isIntranetUser() && 
				SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
				|| SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
				)
			{
				out.println("<td class=\"rowhead2\" style=\"text-align:center;vertical-align:top;\">" + Languages.getString("jsp.admin.rateplans.EmidaRate",SessionData.getLanguage()) + "</td>");
			}
			else
			{
				out.println("<td style=\"display:none;\">" + Languages.getString("jsp.admin.rateplans.EmidaRate",SessionData.getLanguage()) + "</td>");
			}
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td class="rowhead2" style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>">&nbsp;<%=Languages.getString("jsp.admin.rateplans.ISORate",SessionData.getLanguage())%>&nbsp;</td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td class="rowhead2" style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>">&nbsp;<%=Languages.getString("jsp.admin.rateplans.AgentRate",SessionData.getLanguage())%>&nbsp;</td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td class="rowhead2" style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>">&nbsp;<%=Languages.getString("jsp.admin.rateplans.SubAgentRate",SessionData.getLanguage())%>&nbsp;</td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.RepRate",SessionData.getLanguage())%>&nbsp;</td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.MerchantRate",SessionData.getLanguage())%>&nbsp;</td>
		</tr>
		<tr class="row1">
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.ChangeToApply",SessionData.getLanguage())%></td>
<%
			if ((SessionData.getUser().isIntranetUser() && 
				SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
				|| SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
				)
			{
%>
				<td style="text-align:center;vertical-align:top;">
					<input type="text" id="Emida" style="text-align:right;" size="3" maxlength="6" value="0" readonly>
				</td>
<%
			}
			else
			{
%>
				<td style="display:none;"><input type="text" id="Emida" value="0" readonly></td>
<%
			}
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><input type="text" id="ISO" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateRate(this);" onfocus="this.select();"></td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><input type="text" id="Agent" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateRate(this);" onfocus="this.select();"></td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><input type="text" id="SubAgent" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateRate(this);" onfocus="this.select();"></td>
			<td style="text-align:center;vertical-align:top;"><input type="text" id="Rep" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateRate(this);" onfocus="this.select();"></td>
			<td style="text-align:center;vertical-align:top;"><input type="text" id="Merchant" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateRate(this);" onfocus="this.select();"></td>
		</tr>
		<tr class="row2">
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.MinimumValue",SessionData.getLanguage())%></td>
<%
			if ((SessionData.getUser().isIntranetUser() && 
				SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
				|| SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
				)
			{
				out.println("<td></td>");
			}
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><input type="text" id="ISOMin" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateMin(this);" onfocus="this.select();"></td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><input type="text" id="AgentMin" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateMin(this);" onfocus="this.select();"></td>
<%
			bHideCell = !(sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER));
%>
			<td style="text-align:center;vertical-align:top;<%=bHideCell?"display:none":""%>"><input type="text" id="SubAgentMin" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateMin(this);" onfocus="this.select();"></td>
			<td style="text-align:center;vertical-align:top;"><input type="text" id="RepMin" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateMin(this);" onfocus="this.select();"></td>
			<td style="text-align:center;vertical-align:top;"><input type="text" id="MerchantMin" style="text-align:right;" size="3" maxlength="6" value="<%=NumberUtil.formatAmount("0")%>" onblur="ValidateMin(this);" onfocus="this.select();"></td>
		</tr>
	</table>
<%
				}
			}
			else
			{
				out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoTemplatesAvailable",SessionData.getLanguage()));
			}
		}//End of Else we need to retrieve all the first level children nodes of the logged-in user
	}//End of if ( request.getParameter("action").equals("getHierarchy") )
	else if ( request.getParameter("action").equals("getAddProducts") )
	{
		if ( StringUtil.toString(request.getParameter("plans")).length() > 0 )
		{
			SessionData.setProperty("RatePlan_SelectedPlans", request.getParameter("plans"));
		}

		if ( RatePlan.countRootRatePlans(SessionData.getProperty("RatePlan_SelectedPlans")) )
		{//If there are Root Rate Plans, then we need to show the search box and show all the platform products
%>
			<input type="button" value="&lt;&lt; <%=Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage())%>" onclick="DoReturnToAddPlans();">&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.rateplans.Next",SessionData.getLanguage())%> &gt;&gt;" onclick="DoConfirmAddProducts();"><br/><br/>
			<hr/>
			<div style="text-align:left;">
			<img src='images/information.png' style="vertical-align:middle;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.SearchPlatform",SessionData.getLanguage())%><br/><br/>
			<b><%=Languages.getString("jsp.admin.rateplans.SearchProducts",SessionData.getLanguage())%></b><br/>
			<form>
			<table>
				<tr class="main"><td><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage())%></td><td>&nbsp;<input type="text" id="txtSKU" maxlength="10" size="20"></td></tr>
				<tr class="main"><td><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td><td>&nbsp;<input type="text" id="txtName" maxlength="50" size="20"></td></tr>
				<tr class="main"><td colspan="2" align="center"><input type="submit" value="<%=Languages.getString("jsp.admin.rateplans.Search",SessionData.getLanguage())%>" onclick="DoSearchProducts();return false;"></td></tr>
			</table>
			</form>
			<script>
			$("#txtSKU").focus();
			function DoSearchProducts()
			{
				if ( ($("#txtSKU").val() == "") && ($("#txtName").val() == "") )
				{
					alert("<%=Languages.getString("jsp.admin.rateplans.SearchValidation",SessionData.getLanguage())%>");
					$("#txtSKU").focus();
					return false;
				}
				$('#divSearchResults').load('admin/rateplans/globalupdate_ajax.jsp?action=getSearchProducts&sku=' + escape($("#txtSKU").val()) + '&name=' + escape($("#txtName").val()) + '&CopyCheck=' + ($('#chkCopy').is(':checked')?1:0) + '&Random=' + Math.random());
				return false;
			}
			</script>
			</div><br/>
			<div id="divSearchResults">
			</div>
			<hr/><br/>
			<div style="width:100%;text-align:left;">
				<img src='images/information.png' style="vertical-align:middle;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.CopyProductMessage",SessionData.getLanguage())%><br/><br/>
				<label><input type="checkbox" id="chkCopy" onclick="ToggleCopyProduct(this)">&nbsp;<%=Languages.getString("jsp.admin.rateplans.UseCopyProduct",SessionData.getLanguage())%></label><br/><br/>
			</div>
			<div id="divSearchCopy" style="display:none;">
				<div style="text-align:left;">
				<b><%=Languages.getString("jsp.admin.rateplans.SearchCopyProduct",SessionData.getLanguage())%></b><br/>
				<form>
				<table>
					<tr class="main"><td><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage())%></td><td>&nbsp;<input type="text" id="txtCopySKU" maxlength="10" size="20"></td></tr>
					<tr class="main"><td><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td><td>&nbsp;<input type="text" id="txtCopyName" maxlength="50" size="20"></td></tr>
					<tr class="main"><td colspan="2" align="center"><input type="submit" value="<%=Languages.getString("jsp.admin.rateplans.Search",SessionData.getLanguage())%>" onclick="DoSearchCopyProducts();return false;"></td></tr>
				</table>
				</form>
				<script>
				function DoSearchCopyProducts()
				{
					if ( ($("#txtCopySKU").val() == "") && ($("#txtCopyName").val() == "") )
					{
						alert("<%=Languages.getString("jsp.admin.rateplans.SearchValidation",SessionData.getLanguage())%>");
						$("#txtCopySKU").focus();
						return false;
					}
					$('#divSearchCopyResults').load('admin/rateplans/globalupdate_ajax.jsp?action=getSearchCopyProducts&sku=' + escape($("#txtCopySKU").val()) + '&name=' + escape($("#txtCopyName").val()) + '&Random=' + Math.random());
					return false;
				}
				</script>
				</div><br/>
				<div id="divSearchCopyResults">
				</div>
			</div>
			<script>
			function ToggleCopyProduct(ctlCopy)
			{
				var i;
			
				if ( (typeof SelectProductItems) == "undefined" )
				{
					alert("<%=Languages.getString("jsp.admin.rateplans.UseCopyValidation",SessionData.getLanguage())%>");
					ctlCopy.checked = false;
					return;
				}

				for ( i = 0; i < SelectProductItems.length; i++ )
				{
					if ( document.getElementById('chkP_' + SelectProductItems[i]) == null )
					{
						alert("<%=Languages.getString("jsp.admin.rateplans.UseCopyValidation",SessionData.getLanguage())%>");
						ctlCopy.checked = false;
						return;
					}
					$("#tdISO_" + SelectProductItems[i]).css("display", ctlCopy.checked?"none":"");
					$("#tdAgent_" + SelectProductItems[i]).css("display", ctlCopy.checked?"none":"");
					$("#tdSubAgent_" + SelectProductItems[i]).css("display", ctlCopy.checked?"none":"");
					$("#tdRep_" + SelectProductItems[i]).css("display", ctlCopy.checked?"none":"");
					$("#tdMerchant_" + SelectProductItems[i]).css("display", ctlCopy.checked?"none":"");
					$("#tdRef_" + SelectProductItems[i]).css("display", ctlCopy.checked?"none":"");
					if ( ctlCopy.checked )
					{
						document.getElementById("ISO_" + SelectProductItems[i]).value = 0;
						document.getElementById("Agent_" + SelectProductItems[i]).value = 0;
						document.getElementById("SubAgent_" + SelectProductItems[i]).value = 0;
						document.getElementById("Rep_" + SelectProductItems[i]).value = 0;
						document.getElementById("Merchant_" + SelectProductItems[i]).value = 0;
						document.getElementById("Ref_" + SelectProductItems[i]).value = 0;
					}
				}
				$("#tdISO").css("display", ctlCopy.checked?"none":"");
				$("#tdAgent").css("display", ctlCopy.checked?"none":"");
				$("#tdSubAgent").css("display", ctlCopy.checked?"none":"");
				$("#tdRep").css("display", ctlCopy.checked?"none":"");
				$("#tdMerchant").css("display", ctlCopy.checked?"none":"");
				$("#tdRef").css("display", ctlCopy.checked?"none":"");
				$("#divSearchCopy").css("display", ctlCopy.checked?"":"none");
			}
			</script>
<%
		}
		else
		{//Else we are editing children Rate Plans and then we need to show the products available from the selected parents
			String sPageNumber, sPageSize, sSort, sSortDirection, sFilterId, sFilterName, sOwnerID;
			int nTotalRows, nPageCount;

			sPageNumber = request.getParameter("txtPageNumber");
			sPageSize = request.getParameter("ddlPageSize");
			sSort = request.getParameter("txtSort");
			sSortDirection = request.getParameter("txtSortDirection");
			sFilterId = request.getParameter("txtFilterID");
			sFilterName = request.getParameter("txtFilterName");

			if ( sPageNumber == null ) sPageNumber = "1";
			if ( sPageSize  == null ) sPageSize = "50";
			if ( sSort == null ) sSort = "SKU";
			if ( sSortDirection == null ) sSortDirection = "A";
			if ( sFilterId == null ) sFilterId = "";
			if ( sFilterName == null ) sFilterName = "";

			String sCarrier = null;
			if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
			{
				sCarrier = SessionData.getUser().getRefId();
			}
			Vector<Vector<String>> vProducts = RatePlan.getRatePlanParentUnionProducts(SessionData.getProperty("RatePlan_SelectedPlans"), Integer.parseInt(sPageNumber), Integer.parseInt(sPageSize), sSort, sSortDirection, sFilterId, sFilterName, sCarrier);
			if ( !vProducts.get(0).get(0).equals("0") || (sFilterId.length() > 0 || sFilterName.length() > 0) )
			{
%>
	<input type="button" value="&lt;&lt; <%=Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage())%>" onclick="DoReturnToAddPlans();">&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.rateplans.Next",SessionData.getLanguage())%> &gt;&gt;" onclick="DoConfirmAddProducts();"><br/><br/>
	<hr width="80%">
	<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;"><tr><td align="center">
		<img src='images/information.png' style="vertical-align:middle;">&nbsp;<span class='main'><%=Languages.getString("jsp.admin.rateplans.OnlyCheckedProductsInList",SessionData.getLanguage())%></span><br/>
	</td></tr></table>
	<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;">
		<tr class="main">
			<td align="left"><%=Languages.getString("jsp.admin.rateplans.FilterBy",SessionData.getLanguage())%>:</td>
		</tr>
		<tr class="main">
			<td align="left" valign="bottom" nowrap="nowrap">
				<%=Languages.getString("jsp.admin.rateplans.ID",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterID" name="txtFilterID" maxlength="50" size="5" style="vertical-align:middle;" value="<%=sFilterId%>">&nbsp;
				<%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterName" name="txtFilterName" maxlength="200" size="30" style="vertical-align:middle;" value="<%=sFilterName%>">&nbsp;
				<input type="image" src="images/greenarrow.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickFilterResults",SessionData.getLanguage())%>" onclick="$('#txtPageNumber').val('1');DoRefreshList();">
				<input type="image" src="images/reddelete.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickClearResults",SessionData.getLanguage())%>" onclick="$('#txtFilterID').val('');$('#txtFilterName').val('');$('#txtPageNumber').val('1');DoRefreshList();">
			</td>
			<td align="center" valign="bottom">
				<table cellspacing="-1" cellpadding="-1"><tr class="main">
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
				int nPageNumber = Integer.parseInt(sPageNumber);
				int nPageSize = Integer.parseInt(sPageSize);
				nTotalRows = Integer.parseInt(vProducts.get(0).get(0));
				nPageCount = nTotalRows / nPageSize;
				if ((nPageCount * nPageSize) < nTotalRows)
				{
					nPageCount++;
				}

				if ( nPageCount > 1 && nPageNumber > 1 )
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('1');DoRefreshList();"><img border="0" src="images/pagefirst.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagefirstdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></td>
<%
				}

				if (nPageNumber > 1)
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber - 1)%>');DoRefreshList();"><img border="0" src="images/pageprev.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pageprevdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></td>
<%
				}
%>
					<td nowrap="nowrap">&nbsp;<%=Languages.getString("jsp.admin.rateplans.ShowingPageOf", new Object[]{nPageNumber, nPageCount},SessionData.getLanguage())%>&nbsp;</td>
<%
				if (nPageNumber < nPageCount)
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber + 1)%>');DoRefreshList();"><img border="0" src="images/pagenext.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagenextdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></td>
<%
				}

				if ( nPageCount > 1 && nPageNumber < nPageCount )
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=nPageCount%>');DoRefreshList();"><img border="0" src="images/pagelast.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagelastdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></td>
<%
				}
			}
%>
				</tr></table>
			</td>
			<td align="right" valign="bottom" nowrap="nowrap">
				<%=Languages.getString("jsp.admin.rateplans.PageSize",SessionData.getLanguage())%>&nbsp;<select id="ddlPageSize" name="ddlPageSize" style="vertical-align:middle;" onchange="$('#txtPageNumber').val('1');DoRefreshList();"><option value="10" <%=(sPageSize.equals("10")?"selected":"")%>>10</option><option value="20" <%=(sPageSize.equals("20")?"selected":"")%>>20</option><option value="50" <%=(sPageSize.equals("50")?"selected":"")%>>50</option></select>
			</td>
		</tr>
	</table>
	<input type="hidden" id="txtPageNumber" name="txtPageNumber" value="<%=sPageNumber%>">
	<input type="hidden" id="txtSort" name="txtSort" value="<%=sSort%>">
	<input type="hidden" id="txtSortDirection" name="txtSortDirection" value="<%=sSortDirection%>">
<%
				if ( !vProducts.get(0).get(0).equals("0") )
				{
%>
	<table id="tabProducts" width="100%" cellspacing="1" cellpadding="1" border="0">
		<tr class="SectionTopBorder">
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap">
				<label for="chkSelectProduct"><%=Languages.getString("jsp.admin.rateplans.Select_Deselect",SessionData.getLanguage())%></label><br/><input type="checkbox" id="chkSelectProduct" onclick="ToggleSelectProducts(this.checked);">
			</td>
		</tr>
<%
					Iterator<Vector<String>> itProducts = vProducts.iterator();
					String sSelectScript = "";
					int nCounter = 1;

					while ( itProducts.hasNext() )
					{
						Vector<String> vItem = itProducts.next();
						if ( nCounter == 1 )
						{
							nCounter++;
							continue;//First record skipped because is the Total Rows count
						}
						sSelectScript += "SelectProductItems[" + (nCounter++ - 2/*2 because we substracted the first item and the inline increment*/) + "] = " + vItem.get(0) + ";";
%>
		<tr class="row<%=((nCounter % 2) + 1)%>">
			<td nowrap="nowrap">&nbsp;<%=vItem.get(0)%>&nbsp;</td>
			<td nowrap="nowrap">&nbsp;<%=vItem.get(1)%>&nbsp;</td>
			<td align="center"><input id="chkP_<%=vItem.get(0)%>" type="checkbox"></td>
		</tr>
<%
						vItem = null;
					}
					itProducts = null;
%>
	</table>
	<script>
	var SelectProductItems = new Array();
	<%=sSelectScript%>
	</script>
<%
				}
				else
				{
					out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
				}
			}
			else
			{
				out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
				out.println("<input type=\"button\" value=\"&lt;&lt; " + Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage()) + "\" onclick=\"DoReturnToAddPlans();\"><br/><br/>");
			}
			vProducts = null;
		}
	}//End of else if ( request.getParameter("action").equals("getAddProducts") )
	else if ( request.getParameter("action").equals("getRemoveProducts") )
	{
		String sPageNumber, sPageSize, sSort, sSortDirection, sFilterId, sFilterName, sOwnerID;
		int nTotalRows, nPageCount;

		sPageNumber = request.getParameter("txtPageNumber");
		sPageSize = request.getParameter("ddlPageSize");
		sSort = request.getParameter("txtSort");
		sSortDirection = request.getParameter("txtSortDirection");
		sFilterId = request.getParameter("txtFilterID");
		sFilterName = request.getParameter("txtFilterName");

		if ( sPageNumber == null ) sPageNumber = "1";
		if ( sPageSize  == null ) sPageSize = "50";
		if ( sSort == null ) sSort = "SKU";
		if ( sSortDirection == null ) sSortDirection = "A";
		if ( sFilterId == null ) sFilterId = "";
		if ( sFilterName == null ) sFilterName = "";

		if ( StringUtil.toString(request.getParameter("plans")).length() > 0 )
		{
			SessionData.setProperty("RatePlan_SelectedPlans", request.getParameter("plans"));
		}

		String sCarrier = null;
		if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
		{
			sCarrier = SessionData.getUser().getRefId();
		}
		Vector<Vector<String>> vProducts = RatePlan.getRatePlanUnionProducts(SessionData.getProperty("RatePlan_SelectedPlans"), Integer.parseInt(sPageNumber), Integer.parseInt(sPageSize), sSort, sSortDirection, sFilterId, sFilterName, sCarrier);
		if ( !vProducts.get(0).get(0).equals("0") || (sFilterId.length() > 0 || sFilterName.length() > 0) )
		{
%>
	<input type="button" value="&lt;&lt; <%=Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage())%>" onclick="DoReturnToRemovePlans();">&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.rateplans.Next",SessionData.getLanguage())%> &gt;&gt;" onclick="DoConfirmRemoveProducts();"><br/><br/>
	<hr width="80%">
	<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;"><tr><td align="center">
		<img src='images/information.png' style="vertical-align:middle;">&nbsp;<span class='main'><%=Languages.getString("jsp.admin.rateplans.OnlyCheckedProductsInList",SessionData.getLanguage())%></span><br/>
	</td></tr></table>
	<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;">
		<tr class="main">
			<td align="left"><%=Languages.getString("jsp.admin.rateplans.FilterBy",SessionData.getLanguage())%>:</td>
		</tr>
		<tr class="main">
			<td align="left" valign="bottom" nowrap="nowrap">
				<%=Languages.getString("jsp.admin.rateplans.ID",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterID" name="txtFilterID" maxlength="50" size="5" style="vertical-align:middle;" value="<%=sFilterId%>">&nbsp;
				<%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterName" name="txtFilterName" maxlength="200" size="30" style="vertical-align:middle;" value="<%=sFilterName%>">&nbsp;
				<input type="image" src="images/greenarrow.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickFilterResults",SessionData.getLanguage())%>" onclick="$('#txtPageNumber').val('1');DoRefreshList();">
				<input type="image" src="images/reddelete.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickClearResults",SessionData.getLanguage())%>" onclick="$('#txtFilterID').val('');$('#txtFilterName').val('');$('#txtPageNumber').val('1');DoRefreshList();">
			</td>
			<td align="center" valign="bottom">
				<table cellspacing="-1" cellpadding="-1"><tr class="main">
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
				int nPageNumber = Integer.parseInt(sPageNumber);
				int nPageSize = Integer.parseInt(sPageSize);
				nTotalRows = Integer.parseInt(vProducts.get(0).get(0));
				nPageCount = nTotalRows / nPageSize;
				if ((nPageCount * nPageSize) < nTotalRows)
				{
					nPageCount++;
				}

				if ( nPageCount > 1 && nPageNumber > 1 )
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('1');DoRefreshList();"><img border="0" src="images/pagefirst.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagefirstdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></td>
<%
				}

				if (nPageNumber > 1)
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber - 1)%>');DoRefreshList();"><img border="0" src="images/pageprev.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pageprevdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></td>
<%
				}
%>
					<td nowrap="nowrap">&nbsp;<%=Languages.getString("jsp.admin.rateplans.ShowingPageOf", new Object[]{nPageNumber, nPageCount},SessionData.getLanguage())%>&nbsp;</td>
<%
				if (nPageNumber < nPageCount)
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber + 1)%>');DoRefreshList();"><img border="0" src="images/pagenext.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagenextdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></td>
<%
				}

				if ( nPageCount > 1 && nPageNumber < nPageCount )
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=nPageCount%>');DoRefreshList();"><img border="0" src="images/pagelast.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagelastdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></td>
<%
				}
			}
%>
				</tr></table>
			</td>
			<td align="right" valign="bottom" nowrap="nowrap">
				<%=Languages.getString("jsp.admin.rateplans.PageSize",SessionData.getLanguage())%>&nbsp;<select id="ddlPageSize" name="ddlPageSize" style="vertical-align:middle;" onchange="$('#txtPageNumber').val('1');DoRefreshList();"><option value="10" <%=(sPageSize.equals("10")?"selected":"")%>>10</option><option value="20" <%=(sPageSize.equals("20")?"selected":"")%>>20</option><option value="50" <%=(sPageSize.equals("50")?"selected":"")%>>50</option></select>
			</td>
		</tr>
	</table>
	<input type="hidden" id="txtPageNumber" name="txtPageNumber" value="<%=sPageNumber%>">
	<input type="hidden" id="txtSort" name="txtSort" value="<%=sSort%>">
	<input type="hidden" id="txtSortDirection" name="txtSortDirection" value="<%=sSortDirection%>">
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
%>
	<table id="tabProducts" width="100%" cellspacing="1" cellpadding="1" border="0">
		<tr class="SectionTopBorder">
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap">
				<label for="chkSelectProduct"><%=Languages.getString("jsp.admin.rateplans.Select_Deselect",SessionData.getLanguage())%></label><br/><input type="checkbox" id="chkSelectProduct" onclick="ToggleSelectProducts(this.checked);">
			</td>
		</tr>
<%
			Iterator<Vector<String>> itProducts = vProducts.iterator();
			String sSelectScript = "";
			int nCounter = 1;

			while ( itProducts.hasNext() )
			{
				Vector<String> vTmp = itProducts.next();
				if ( nCounter == 1 )
				{
					nCounter++;
					continue;//First record skipped because is the Total Rows count
				}
				sSelectScript += "SelectProductItems[" + (nCounter++ - 2/*2 because we substracted the first item and the inline increment*/) + "] = " + vTmp.get(0) + ";";
%>
		<tr class="row<%=((nCounter % 2) + 1)%>">
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(0)%>&nbsp;</td>
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(1)%>&nbsp;</td>
			<td align="center"><input id="chkP_<%=vTmp.get(0)%>" type="checkbox"></td>
		</tr>
<%
				vTmp = null;
			}
			itProducts = null;
%>
	</table>
	<script>
	var SelectProductItems = new Array();
	<%=sSelectScript%>
	</script>
<%
			}
			else
			{
				out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
			}
		}
		else
		{
			out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
			out.println("<input type=\"button\" value=\"&lt;&lt; " + Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage()) + "\" onclick=\"DoReturnToRemovePlans();\"><br/><br/>");
		}
		vProducts = null;
	}//End of else if ( request.getParameter("action").equals("getRemoveProducts") )
	else if ( request.getParameter("action").equals("getUpdateProducts") )
	{
		String sPageNumber, sPageSize, sSort, sSortDirection, sFilterId, sFilterName, sOwnerID;
		int nTotalRows, nPageCount;
		
		sPageNumber = request.getParameter("txtPageNumber");
		sPageSize = request.getParameter("ddlPageSize");
		sSort = request.getParameter("txtSort");
		sSortDirection = request.getParameter("txtSortDirection");
		sFilterId = request.getParameter("txtFilterID");
		sFilterName = request.getParameter("txtFilterName");
		
		if ( sPageNumber == null ) sPageNumber = "1";
		if ( sPageSize  == null ) sPageSize = "50";
		if ( sSort == null ) sSort = "SKU";
		if ( sSortDirection == null ) sSortDirection = "A";
		if ( sFilterId == null ) sFilterId = "";
		if ( sFilterName == null ) sFilterName = "";

		if (
			(SessionData.getUser().isIntranetUser() && 
			SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))
			|| SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)
			)
		{
			sOwnerID = "0";
		}
		else
		{
			sOwnerID = SessionData.getProperty("ref_id");
		}

		String sCarrier = null;
		if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
		{
			sCarrier = SessionData.getUser().getRefId();
		}
		Vector<Vector<String>> vProducts = RatePlan.getRatePlanPlatformUnionProducts(sOwnerID, Integer.parseInt(sPageNumber), Integer.parseInt(sPageSize), sSort, sSortDirection, sFilterId, sFilterName, sCarrier);
		if ( !vProducts.get(0).get(0).equals("0") || (sFilterId.length() > 0 || sFilterName.length() > 0) )
		{
%>
	<input type="button" value="&lt;&lt; <%=Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage())%>" onclick="DoReturnToMenu();">&nbsp;&nbsp;<input type="button" value="<%=Languages.getString("jsp.admin.rateplans.Next",SessionData.getLanguage())%> &gt;&gt;" onclick="DoShowUpdatePlans();"><br/><br/>
	<hr width="80%">
	<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;"><tr><td align="center">
		<img src='images/information.png' style="vertical-align:middle;">&nbsp;<span class='main'><%=Languages.getString("jsp.admin.rateplans.OnlyCheckedProductsInList",SessionData.getLanguage())%></span><br/>
	</td></tr></table>
	<table width="100%" cellspacing="-1" cellpadding="-1" style="padding:5px;">
		<tr class="main">
			<td align="left"><%=Languages.getString("jsp.admin.rateplans.FilterBy",SessionData.getLanguage())%>:</td>
		</tr>
		<tr class="main">
			<td align="left" valign="bottom" nowrap="nowrap">
				<%=Languages.getString("jsp.admin.rateplans.ID",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterID" name="txtFilterID" maxlength="50" size="5" style="vertical-align:middle;" value="<%=sFilterId%>">&nbsp;
				<%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%>&nbsp;<input type="text" id="txtFilterName" name="txtFilterName" maxlength="200" size="30" style="vertical-align:middle;" value="<%=sFilterName%>">&nbsp;
				<input type="image" src="images/greenarrow.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickFilterResults",SessionData.getLanguage())%>" onclick="$('#txtPageNumber').val('1');DoRefreshList();">
				<input type="image" src="images/reddelete.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ClickClearResults",SessionData.getLanguage())%>" onclick="$('#txtFilterID').val('');$('#txtFilterName').val('');$('#txtPageNumber').val('1');DoRefreshList();">
			</td>
			<td align="center" valign="bottom">
				<table cellspacing="-1" cellpadding="-1"><tr class="main">
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
				int nPageNumber = Integer.parseInt(sPageNumber);
				int nPageSize = Integer.parseInt(sPageSize);
				nTotalRows = Integer.parseInt(vProducts.get(0).get(0));
				nPageCount = nTotalRows / nPageSize;
				if ((nPageCount * nPageSize) < nTotalRows)
				{
					nPageCount++;
				}

				if ( nPageCount > 1 && nPageNumber > 1 )
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('1');DoRefreshList();"><img border="0" src="images/pagefirst.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagefirstdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToFirstPage",SessionData.getLanguage())%>"></td>
<%
				}

				if (nPageNumber > 1)
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber - 1)%>');DoRefreshList();"><img border="0" src="images/pageprev.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pageprevdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToPrevPage",SessionData.getLanguage())%>"></td>
<%
				}
%>
					<td nowrap="nowrap">&nbsp;<%=Languages.getString("jsp.admin.rateplans.ShowingPageOf", new Object[]{nPageNumber, nPageCount},SessionData.getLanguage())%>&nbsp;</td>
<%
				if (nPageNumber < nPageCount)
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=(nPageNumber + 1)%>');DoRefreshList();"><img border="0" src="images/pagenext.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagenextdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToNextPage",SessionData.getLanguage())%>"></td>
<%
				}

				if ( nPageCount > 1 && nPageNumber < nPageCount )
				{
%>
					<td><a href="javascript:" onclick="$('#txtPageNumber').val('<%=nPageCount%>');DoRefreshList();"><img border="0" src="images/pagelast.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></a></td>
<%
				}
				else
				{
%>
					<td><img border="0" src="images/pagelastdisabled.png" style="margin:1px;padding:0px;" title="<%=Languages.getString("jsp.admin.rateplans.GoToLastPage",SessionData.getLanguage())%>"></td>
<%
				}
			}
%>
				</tr></table>
			</td>
			<td align="right" valign="bottom" nowrap="nowrap">
				<%=Languages.getString("jsp.admin.rateplans.PageSize",SessionData.getLanguage())%>&nbsp;<select id="ddlPageSize" name="ddlPageSize" style="vertical-align:middle;" onchange="$('#txtPageNumber').val('1');DoRefreshList();"><option value="10" <%=(sPageSize.equals("10")?"selected":"")%>>10</option><option value="20" <%=(sPageSize.equals("20")?"selected":"")%>>20</option><option value="50" <%=(sPageSize.equals("50")?"selected":"")%>>50</option></select>
			</td>
		</tr>
	</table>
	<input type="hidden" id="txtPageNumber" name="txtPageNumber" value="<%=sPageNumber%>">
	<input type="hidden" id="txtSort" name="txtSort" value="<%=sSort%>">
	<input type="hidden" id="txtSortDirection" name="txtSortDirection" value="<%=sSortDirection%>">
<%
			if ( !vProducts.get(0).get(0).equals("0") )
			{
%>
	<table id="tabProducts" width="100%" cellspacing="1" cellpadding="1" border="0">
		<tr class="SectionTopBorder">
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap">
				<label for="chkSelectProduct"><%=Languages.getString("jsp.admin.rateplans.Select_Deselect",SessionData.getLanguage())%></label><br/><input type="checkbox" id="chkSelectProduct" onclick="ToggleSelectProducts(this.checked);">
			</td>
		</tr>
<%
			Iterator<Vector<String>> itProducts = vProducts.iterator();
			String sSelectScript = "";
			int nCounter = 1;

			while ( itProducts.hasNext() )
			{
				Vector<String> vTmp = itProducts.next();
				if ( nCounter == 1 )
				{
					nCounter++;
					continue;//First record skipped because is the Total Rows count
				}
				sSelectScript += "SelectProductItems[" + (nCounter++ - 2/*2 because we substracted the first item and the inline increment*/) + "] = " + vTmp.get(0) + ";";
%>
		<tr class="row<%=((nCounter % 2) + 1)%>">
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(0)%>&nbsp;</td>
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(1)%>&nbsp;</td>
                        <td align="center"><input id="chkP_<%=vTmp.get(0)%>" type="checkbox" <%=(vTmp.get(2).equals("0"))?"disabled=\"disabled\"":""%>>
                            <%
                                if (vTmp.get(2).equals("0")) {
                            %>
                            <img id="imgMessage" src="images/risk-icon.png" style="vertical-align:middle;" title="<%=Languages.getString("jsp.admin.rateplans.ProductRatesNotEditable", SessionData.getLanguage())%>">
                            <%
                                }
                            %>
                        </td>
		</tr>
<%
				vTmp = null;
			}
			itProducts = null;
%>
	</table>
	<script>
	var SelectProductItems = new Array();
	<%=sSelectScript%>
	</script>
<%
			}
			else
			{
				out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
			}
		}
		else
		{
			out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
			out.println("<input type=\"button\" value=\"&lt;&lt; " + Languages.getString("jsp.admin.rateplans.Back",SessionData.getLanguage()) + "\" onclick=\"DoReturnToMenu();\"><br/><br/>");
		}
		vProducts = null;
	}//End of else if ( request.getParameter("action").equals("getUpdateProducts") )
	else if ( request.getParameter("action").equals("getSearchProducts") )
	{
		String sCarrier = null;
		if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
		{
			sCarrier = SessionData.getUser().getRefId();
		}
		Vector<Vector<String>> vProducts = RatePlan.getSearchProducts(request.getParameter("sku"), request.getParameter("name"), sCarrier);
		boolean bShowRates = request.getParameter("CopyCheck").equals("0");
		if ( vProducts.size() > 0 )
		{
%>
	<br/><img src='images/information.png' style="vertical-align:middle;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.SearchPerformance",SessionData.getLanguage())%><br/><br/>
	<div <%=(vProducts.size() > 20)?"style=\"height:500px;overflow:auto;\"":""%>>
	<table id="tabProducts" width="100%" cellspacing="1" cellpadding="1" border="0">
		<tr class="SectionTopBorder">
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.SKU")%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=SessionData.getString("jsp.admin.rateplans.Name")%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=SessionData.getString("jsp.admin.rateplans.Amount")%>&nbsp;</td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=SessionData.getString("jsp.admin.rateplans.BuyRate")%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=SessionData.getString("jsp.admin.rateplans.RateType")%>&nbsp;</td>
			<td id="tdISO" class="rowhead2" style="text-align:center;vertical-align:top;<%=bShowRates?"":"display:none;"%>"><%=SessionData.getString("jsp.admin.rateplans.ISORate")%></td>
			<td id="tdAgent" class="rowhead2" style="text-align:center;vertical-align:top;<%=bShowRates?"":"display:none;"%>"><%=SessionData.getString("jsp.admin.rateplans.AgentRate")%></td>
			<td id="tdSubAgent" class="rowhead2" style="text-align:center;vertical-align:top;<%=bShowRates?"":"display:none;"%>"><%=SessionData.getString("jsp.admin.rateplans.SubAgentRate")%></td>
			<td id="tdRep" class="rowhead2" style="text-align:center;vertical-align:top;<%=bShowRates?"":"display:none;"%>"><%=SessionData.getString("jsp.admin.rateplans.RepRate")%></td>
			<td id="tdMerchant" class="rowhead2" style="text-align:center;vertical-align:top;<%=bShowRates?"":"display:none;"%>"><%=SessionData.getString("jsp.admin.rateplans.MerchantRate")%></td>
			<td id="tdRef" class="rowhead2" style="text-align:center;vertical-align:top;<%=bShowRates?"":"display:none;"%>"><%=SessionData.getString("jsp.admin.rateplans.RefRate")%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap">
				&nbsp;<label for="chkSelectProduct"><%=Languages.getString("jsp.admin.rateplans.Select_Deselect",SessionData.getLanguage())%></label>&nbsp;<br/><input type="checkbox" id="chkSelectProduct" onclick="ToggleSelectProducts(this.checked);">
			</td>
		</tr>
<%
			Iterator<Vector<String>> itProducts = vProducts.iterator();
			String sSelectScript = "";
			int nCounter = 1;

			while ( itProducts.hasNext() )
			{
				Vector<String> vTmp = itProducts.next();
				boolean bNotManagedByRatePlan = vTmp.get(4).equals("1");
				sSelectScript += "SelectProductItems[" + (nCounter++ - 1) + "] = " + vTmp.get(0) + ";";

				if ( vTmp.get(6).equals("1") )
				{
					//vTmp.get(7) is the Fixed_Fee
					vTmp.set(3, vTmp.get(7));
					vTmp.set(5, NumberUtil.formatAmount(Double.toString((Double.parseDouble(vTmp.get(5)) * Double.parseDouble(vTmp.get(7))) / 100)));
				}
%>
		<tr class="row<%=((nCounter % 2) + 1)%>">
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(0)%>&nbsp;</td>
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(1)%>&nbsp;</td>
			<td align="right" nowrap="nowrap">&nbsp;<%=vTmp.get(2)%>&nbsp;</td>
			<td align="right" nowrap="nowrap">
				&nbsp;<%=vTmp.get(3)%>&nbsp;
				<input type="hidden" id="Buy_<%=vTmp.get(0)%>" value="<%=vTmp.get(3)%>">
				<input type="hidden" id="UseMoneyRates_<%=vTmp.get(0)%>" value="<%=vTmp.get(6)%>">
			</td>
			<td align="center"><%=(vTmp.get(6).equals("1")?"$":"%")%></td>
			<td id="tdISO_<%=vTmp.get(0)%>" align="center" valign="bottom" nowrap="nowrap" style="<%=bShowRates?"":"display:none;"%>">&nbsp;
				<input type="text" id="ISO_<%=vTmp.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=vTmp.get(5)%>" onkeyup="ProcessRate(this, false);" onblur="HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=bNotManagedByRatePlan?"disabled":""%>>
			&nbsp;</td>
			<td id="tdAgent_<%=vTmp.get(0)%>" align="center" valign="bottom" nowrap="nowrap" style="<%=bShowRates?"":"display:none;"%>">&nbsp;
				<input type="text" id="Agent_<%=vTmp.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount("0")%>" onkeyup="ProcessRate(this, false);" onblur="HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=bNotManagedByRatePlan?"disabled":""%>>
			&nbsp;</td>
			<td id="tdSubAgent_<%=vTmp.get(0)%>" align="center" valign="bottom" nowrap="nowrap" style="<%=bShowRates?"":"display:none;"%>">&nbsp;
				<input type="text" id="SubAgent_<%=vTmp.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount("0")%>" onkeyup="ProcessRate(this, false);" onblur="HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=bNotManagedByRatePlan?"disabled":""%>>
			&nbsp;</td>
			<td id="tdRep_<%=vTmp.get(0)%>" align="center" valign="bottom" nowrap="nowrap" style="<%=bShowRates?"":"display:none;"%>">&nbsp;
				<input type="text" id="Rep_<%=vTmp.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount("0")%>" onkeyup="ProcessRate(this, false);" onblur="HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=bNotManagedByRatePlan?"disabled":""%>>
			&nbsp;</td>
			<td id="tdMerchant_<%=vTmp.get(0)%>" align="center" valign="bottom" nowrap="nowrap" style="<%=bShowRates?"":"display:none;"%>">&nbsp;
				<input type="text" id="Merchant_<%=vTmp.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount("0")%>" onkeyup="ProcessRate(this, false);" onblur="HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" <%=bNotManagedByRatePlan?"disabled":""%>>
			&nbsp;</td>
			<td id="tdRef_<%=vTmp.get(0)%>" align="center" valign="bottom" nowrap="nowrap" style="<%=bShowRates?"":"display:none;"%>">&nbsp;
				<input type="text" id="Ref_<%=vTmp.get(0)%>" style="text-align:right;" size="3" maxlength="5" value="<%=NumberUtil.formatAmount("0")%>" onkeyup="ProcessRate(this, false);" onblur="HighlightRow(this, false);" onfocus="HighlightRow(this, true);this.select();" disabled="disabled">&nbsp;
<%
				if ( bNotManagedByRatePlan )
				{
					out.print("<img border=\"0\" src=\"images/icnTransaction1.gif\">");
				}
				else
				{
					out.print("<a href=\"javascript:\" onclick=\"ToggleRefList(" + vTmp.get(0) + ", true);\"><img border=\"0\" src=\"images/icnTransaction1.gif\"></a>");
				}
%>
				<input type="hidden" id="RefID_<%=vTmp.get(0)%>" value="">
				<div style="position:relative;left:-100px;">
					<div id="divRef_<%=vTmp.get(0)%>" style="position:absolute;display:none;border:4px solid navy;border-collapse:collapse;background-color:#D0D0D0;"></div>
				</div>
			&nbsp;</td>
			<td align="center"><input id="chkP_<%=vTmp.get(0)%>" type="checkbox"></td>
		</tr>
<%
				vTmp = null;
			}
			itProducts = null;
%>
	</table>
	</div>
	<script>
	var SelectProductItems = new Array();
	<%=sSelectScript%>

	function ShowInitialRatesStatus()
	{
		var i;
		var sRefsList = "";

		sRefsList += "<select id='DDL_ID' size='5' onchange='SetSelectedRefAgent(this);' onblur='ToggleRefList(ITEM_ID, false);'>";
		sRefsList += "<option value=''>--NONE--</option>";
<%
			Iterator<Vector<String>> itRefAgents = Rep.getReferralAgents().iterator();
			while ( itRefAgents.hasNext() )
			{
				Vector<String> vRefItem = itRefAgents.next();
				out.println("sRefsList += \"<option value='" + vRefItem.get(0) + "'>" + vRefItem.get(1).replaceAll("\"", "'") + "</option>\";");
				vRefItem = null;
			}
			itRefAgents = null;
%>
		sRefsList += "</select></br><i><%=Languages.getString("jsp.admin.rateplans.ClickOut",SessionData.getLanguage())%></i>";

		for ( i = 0; i < SelectProductItems.length; i++ )
		{
			ProcessRate(document.getElementById('ISO_' + SelectProductItems[i]), false);
			$("#divRef_" + SelectProductItems[i]).html(sRefsList.replace("DDL_ID", "ddlRef_" + SelectProductItems[i]).replace("ITEM_ID", SelectProductItems[i]));
			document.getElementById("divRef_" + SelectProductItems[i]).parentNode.style.display = 'none';
		}
	}
	ShowInitialRatesStatus();
	</script>
<%
		}
		else
		{
			out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
		}
		vProducts = null;
	}//End of else if ( request.getParameter("action").equals("getSearchProducts") )
	else if ( request.getParameter("action").equals("getSearchCopyProducts") )
	{
		String sCarrier = null;
		if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
		{
			sCarrier = SessionData.getUser().getRefId();
		}
		Vector<Vector<String>> vProducts = RatePlan.getSearchCopyProducts(request.getParameter("sku"), request.getParameter("name"), SessionData.getProperty("RatePlan_SelectedPlans"), sCarrier);
		if ( vProducts.size() > 0 )
		{
%>
	<br/><img src='images/information.png' style="vertical-align:middle;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.SearchCopyPerformance",SessionData.getLanguage())%><br/><br/>
	<div <%=(vProducts.size() > 20)?"style=\"height:500px;overflow:auto;\"":""%>>
	<table id="tabCopyProducts" width="100%" cellspacing="1" cellpadding="1" border="0">
		<tr class="SectionTopBorder">
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;">&nbsp;<%=Languages.getString("jsp.admin.rateplans.Amount",SessionData.getLanguage())%>&nbsp;</td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.BuyRate",SessionData.getLanguage())%></td>
			<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap="nowrap">&nbsp;<%=Languages.getString("jsp.admin.rateplans.SelectOne",SessionData.getLanguage())%>&nbsp;</td>
		</tr>
<%
			Iterator<Vector<String>> itProducts = vProducts.iterator();
			String sSelectCopyScript = "";
			boolean bIsSelected = false;
			int nCounter = 1;

			while ( itProducts.hasNext() )
			{
				Vector<String> vTmp = itProducts.next();
				sSelectCopyScript += "SelectCopyProductItems[" + (nCounter++ - 1) + "] = " + vTmp.get(0) + ";";
%>
		<tr class="row<%=((nCounter % 2) + 1)%>">
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(0)%>&nbsp;</td>
			<td nowrap="nowrap">&nbsp;<%=vTmp.get(1)%>&nbsp;</td>
			<td align="right" nowrap="nowrap">&nbsp;<%=vTmp.get(2)%>&nbsp;</td>
			<td align="right" nowrap="nowrap">&nbsp;<%=vTmp.get(3)%>&nbsp;</td>
			<td align="center"><input id="rbtnP_<%=vTmp.get(0)%>" type="radio" name="CheckCopyProduct" <%=bIsSelected?"":"checked"%>></td>
		</tr>
<%
				bIsSelected = true;
				vTmp = null;
			}
			itProducts = null;
%>
	</table>
	<script>
	var SelectCopyProductItems = new Array();
	<%=sSelectCopyScript%>
	</script>
	</div>
<%
		}
		else
		{
			out.println("<img src='images/risk-icon.png'>&nbsp;" + Languages.getString("jsp.admin.rateplans.NoProducts",SessionData.getLanguage()) + "<br/><br/>");
		}
		vProducts = null;
	}//End of else if ( request.getParameter("action").equals("getSearchCopyProducts") )
	else if ( request.getParameter("action").equals("doConfirmAddProducts") )
	{
		if ( RatePlan.checkRatePlanWorkTableStatus(request.getSession().getId()) > 0 )
		{
			out.print(Languages.getString("jsp.admin.rateplans.GlobalUpdateInProcess",SessionData.getLanguage()));
		}
		else
		{
			String sAccessLevel = SessionData.getProperty("access_level");

			SessionData.setProperty("RatePlan_SelectedProducts", request.getParameter("products"));
			SessionData.setProperty("RatePlan_EnableProducts", request.getParameter("Enable"));
			SessionData.setProperty("RatePlan_CopyProduct", request.getParameter("Copy"));
			SessionData.setProperty("RatePlan_ProcessBoth", request.getParameter("ProcessBoth"));
			RatePlan.generateRatePlanWorkHierarchy(request.getSession().getId(), SessionData.getProperty("RatePlan_SelectedPlans"), request.getParameter("products"), request.getParameter("Enable"), request.getParameter("Copy"));
			out.print(RatePlan.generateRatePlanGlobalOperation(request.getSession().getId(), "ADD", request.getParameter("Enable"), request.getParameter("Copy"), sAccessLevel, request.getParameter("ProcessBoth"), SessionData));
		}
	}//End of else if ( request.getParameter("action").equals("doConfirmAddProducts") )
	else if ( request.getParameter("action").equals("doConfirmRemoveProducts") )
	{
		if ( RatePlan.checkRatePlanWorkTableStatus(request.getSession().getId()) > 0 )
		{
			out.print(Languages.getString("jsp.admin.rateplans.GlobalUpdateInProcess",SessionData.getLanguage()));
		}
		else
		{
			String sAccessLevel = SessionData.getProperty("access_level");

			SessionData.setProperty("RatePlan_SelectedProducts", request.getParameter("products"));
			RatePlan.generateRatePlanWorkHierarchy(request.getSession().getId(), SessionData.getProperty("RatePlan_SelectedPlans"), request.getParameter("products"), "1", "");
			out.print(RatePlan.generateRatePlanGlobalOperation(request.getSession().getId(), "REMOVE", null, null, sAccessLevel, null, SessionData));
		}
	}//End of else if ( request.getParameter("action").equals("doConfirmRemoveProducts") )
	else if ( request.getParameter("action").equals("doConfirmUpdateRates") )
	{
		if ( RatePlan.checkRatePlanWorkTableStatus(request.getSession().getId()) > 0 )
		{
			out.print(Languages.getString("jsp.admin.rateplans.GlobalUpdateInProcess",SessionData.getLanguage()));
		}
		else
		{
			String sAccessLevel = SessionData.getProperty("access_level");

			SessionData.setProperty("RatePlan_SelectedPlans", request.getParameter("plans"));
			SessionData.setProperty("RatePlan_Rates", request.getParameter("Rates"));
			SessionData.setProperty("RatePlan_MinValues", request.getParameter("Mins"));
			RatePlan.generateRatePlanWorkHierarchy(request.getSession().getId(), request.getParameter("plans"), SessionData.getProperty("RatePlan_ProductFilter"), "1", "");
			out.print(RatePlan.generateRatePlanGlobalOperation(request.getSession().getId(), "UPDATE", request.getParameter("Rates"), request.getParameter("Mins"), sAccessLevel, null, SessionData));
		}
	}//End of else if ( request.getParameter("action").equals("doConfirmRemoveProducts") )
	else if ( request.getParameter("action").equals("doAddProducts") )
	{
		int nPlansCount;
		nPlansCount = RatePlan.checkRatePlanWorkTableStatus(request.getSession().getId());
		if ( nPlansCount > 0 )
		{
			nPlansCount = -1;
		}
		else
		{
			nPlansCount = RatePlan.generateRatePlanWorkHierarchy(request.getSession().getId(), SessionData.getProperty("RatePlan_SelectedPlans"), SessionData.getProperty("RatePlan_SelectedProducts"), SessionData.getProperty("RatePlan_EnableProducts"), SessionData.getProperty("RatePlan_CopyProduct"));
			(new Thread((new RatePlanWorker(request.getSession().getId(), RatePlanWorker.Actions.ADD, SessionData, Integer.parseInt(SessionData.getProperty("RatePlan_ProcessBoth")),(SessionData.getUser().isIntranetUser()
					&& SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
							DebisysConstants.INTRANET_PERMISSION_ALLOW_EXCEED_RATES)))), request.getSession().getId())).start();
		}
		out.print("-VALID-:" + nPlansCount);
	}//End of else if ( request.getParameter("action").equals("doAddProducts") )
	else if ( request.getParameter("action").equals("doRemoveProducts") )
	{
		int nPlansCount;
		nPlansCount = RatePlan.checkRatePlanWorkTableStatus(request.getSession().getId());
		if ( nPlansCount > 0 )
		{
			nPlansCount = -1;
		}
		else
		{
			nPlansCount = RatePlan.generateRatePlanWorkHierarchy(request.getSession().getId(), SessionData.getProperty("RatePlan_SelectedPlans"), SessionData.getProperty("RatePlan_SelectedProducts"), "1", "");
			(new Thread((new RatePlanWorker(request.getSession().getId(), RatePlanWorker.Actions.REMOVE, SessionData, 2,(SessionData.getUser().isIntranetUser()
					&& SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
							DebisysConstants.INTRANET_PERMISSION_ALLOW_EXCEED_RATES)))), request.getSession().getId())).start();
		}
		out.print("-VALID-:" + nPlansCount);
	}//End of else if ( request.getParameter("action").equals("doRemoveProducts") )
	else if ( request.getParameter("action").equals("doUpdateRates") )
	{
		int nPlansCount;
		nPlansCount = RatePlan.checkRatePlanWorkTableStatus(request.getSession().getId());
		if ( nPlansCount > 0 )
		{
			nPlansCount = -1;
		}
		else
		{
			String sAccessLevel = SessionData.getProperty("access_level");

			//First call is to retrieve the items to be processed because we need to insert additinal rows in the WorkTable to process the UpdateRates
			RatePlan.generateRatePlanWorkHierarchy(request.getSession().getId(), SessionData.getProperty("RatePlan_SelectedPlans"), SessionData.getProperty("RatePlan_ProductFilter"), "1", "");
			RatePlan.generateUpdateRatesWorkTable(request.getSession().getId(), SessionData.getProperty("RatePlan_Rates"), SessionData.getProperty("RatePlan_MinValues"), sAccessLevel, SessionData);
			//Second call is the one that will be used to perform the operation
			nPlansCount = RatePlan.generateRatePlanWorkHierarchy(request.getSession().getId(), SessionData.getProperty("RatePlan_SelectedPlans"), SessionData.getProperty("RatePlan_ProductFilter"), "1", "");
			(new Thread((new RatePlanWorker(request.getSession().getId(), RatePlanWorker.Actions.UPDATE, SessionData, 2, (SessionData.getUser().isIntranetUser()
					&& SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
							DebisysConstants.INTRANET_PERMISSION_ALLOW_EXCEED_RATES)))), request.getSession().getId())).start();
		}
		out.print("-VALID-:" + nPlansCount);
	}//End of else if ( request.getParameter("action").equals("doRemoveProducts") )
	else if ( request.getParameter("action").equals("checkStatus") )
	{
		out.print("-VALID-:" + RatePlan.checkRatePlanWorkTableStatus(request.getSession().getId()));
	}
%>
