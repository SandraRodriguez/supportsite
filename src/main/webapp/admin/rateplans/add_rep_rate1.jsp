<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil" %>
<%
int section=7;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%

  Vector vecSearchResults = new Vector();
  Vector vecRatePlans = new Vector();

  vecRatePlans = RatePlan.getIsoRatePlans(SessionData);
  if (request.getParameter("search") != null)
  {
    vecSearchResults = RatePlan.getQualifyingISOProducts(SessionData);
  }
%>
<%@ include file="/includes/header.jsp" %>
<script language="javascript">
 var submitted = false;
 var arySelectedIndexes = new Array();
 var aryLen;
 var checkflag = "false";

    function s(c,o)
    {
        if (c.checked) {
          glow(c);
        }
        else {
          unglow(c,o);
        }
    }

    function checkAll(field)
    {
      var x = 1;
      if (checkflag == "false")
      {
        for (i = 0; i < field.length; i++)
        {
          field[i].checked = true;
          glow(field[i]);
        }
        checkflag = "true";
        return "<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.uncheck_all",SessionData.getLanguage())%>";
      }
      else
      {
        for (i = 0; i < field.length; i++)
        {
          field[i].checked = false;
          unglow(field[i],x++);
          if (x > 2) x = 1;
        }
        checkflag = "false";
        return "<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.check_all",SessionData.getLanguage())%>";
      }
}

    function check(c)
    {
        c.checked = true;
        glow(c);
    }



    function glow(c)
    {
        var tr = null;
        if (c.parentNode && c.parentNode.parentNode) {
            tr = c.parentNode.parentNode;
        }
        else {
            if (c.parentElement && c.parentElement.parentElement) {
              tr = c.parentElement.parentElement;
            }
        }
        if (tr) {
          tr.className = "glow";
        }
    }


    function unglow(c,o)
    {
        var tr = null;
        if (c.parentNode && c.parentNode.parentNode) {
            tr = c.parentNode.parentNode;
        }
        else
           { if (c.parentElement && c.parentElement.parentElement) {
               tr = c.parentElement.parentElement;
             }
           }
        if (tr) {
          tr.className = "row" + o;
        }
    }

 function already_been_processed(c)
 {  var n;
    var a = arySelectedIndexes;
    for (n=0;n<aryLen;n+=2)
     { if ( ( c>= a[n] ) && (c <= a[n+1] ) && (a[n] > 0)   )
         return true;
     }
    return false;
 }

 function invalidate_range(c)
 {  var n;
    var a = arySelectedIndexes;
    for (n=0;n<aryLen;n+=2)
     { if ( (a[n] > 0) && ( c>= a[n] ) && (c <= a[n+1] ) )
         a[n]=-1;n[n+1]=-1;
     }
 }


 function checkRanges(f)
 { var i,n,x,z;
   var range_begin=0;
   var l = f.elements.length;
   var a = arySelectedIndexes;

   for (z=0;i<l; z++)
     if (f.elements[z].type == "checkbox")
       break;

   //set up the ranges
   for (i=0;i<l; i++)
   {
        if (  f.elements[i].checked==true )
        {  if (!already_been_processed(i))
            if (range_begin > 0)
            {
                x = arySelectedIndexes.length;
                arySelectedIndexes[x] = range_begin;
                arySelectedIndexes[x+1] = i;
                aryLen = x+1;
                for (n=range_begin;n<=i;n++)
                  check(f.elements[n]);
                range_begin = 0;
            }
              else
              { range_begin = i; }
        } else //not checked
          { invalidate_range(i);
          }
        pct = parseInt(i / l * 100);
      if (pct % 10 == 0)
        window.status = 'working (' + pct + '% done)';

    }
    window.status = "Done!";
 }


</script>


 <TABLE cellSpacing=0 cellPadding=0 width=750px border=0>
   <TBODY>
   <TR>
     <TD align=left width="1%" 
     background=images/top_blue.gif><IMG height=20 
       src="images/top_left_blue.gif" width=18></TD>
     <TD class=formAreaTitle 
     background=images/top_blue.gif width="3000">&nbsp;<B><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.title",SessionData.getLanguage()).toUpperCase()%></B></TD>
     <TD align=right width="1%" 
     background=images/top_blue.gif><IMG height=20 
       src="images/top_right_blue.gif" width=18></TD></TR>
       <TR>
     <TD colSpan=3>
       <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
       bgColor=#fffcdf class="backceldas">
         <TBODY>
         <TR class="backceldas">
           <TD width=1 bgColor=#003082><IMG 
             src="images/trans.gif" width=1></TD>
           <TD vAlign=top align=middle bgColor=#ffffff>
             <TABLE width="100%" border=0 
             align=center cellPadding=2 cellSpacing=0 class="backceldas">
               <TBODY>
               <TR>
                 <TD width=18>&nbsp;</TD>
                 <TD class=main colspan="3"><BR>
<%
if (vecRatePlans != null && vecRatePlans.size() > 0)
{
  %>
		     <table border="0" width="100%" cellpadding="0" cellspacing="0">
		     	<tr>
			        <td class="formArea2">
		
		  <table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="33%">
					 <table border="0" width="100%" cellspacing="0" cellpadding="0">
					 <tr>
						<td width="50%" align="right"><img src="images/bullet.gif" border="0" alt=""></td>
						<td width="50%"><img src="images/pixel_silver.gif" border="0" alt="" width="100%" height="1"></td>
					 </tr>
					 </table>
		     </td>
		       <td width="33%"><img src="images/pixel_silver.gif" border="0" alt="" width="100%" height="1"></td>
		       <td width="33%">
		       	  <table border="0" width="100%" cellspacing="0" cellpadding="0">
		           <tr>
		             <td width="50%"><img src="images/pixel_silver.gif" border="0" alt="" width="100%" height="1"></td>
		             <td width="50%"><img src="images/pixel_silver.gif" border="0" alt="" width="1" height="5"></td>
		           </tr>
		           </table>
		       </td>
		    </tr>
		    <tr>
		       <td align="center" width="33%" class="BarCurrent"><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.step1",SessionData.getLanguage())%></td>
		       <td align="center" width="33%" class="BarTo"><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.step2",SessionData.getLanguage())%></td>
		       <td align="center" width="33%" class="BarTo"><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.step3",SessionData.getLanguage())%></td>
		    </tr>
		</table>
		<br>
		            <%=Languages.getString("jsp.admin.rateplans.add_rep_rate.instructions1",SessionData.getLanguage())%><br>
	          <table width="300">
            <form name="rateplan" method="get" action="admin/rateplans/add_rep_rate1.jsp" >
              <tr class="main">
               <td nowrap valign="top"><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.select",SessionData.getLanguage())%>:</td>
               <td>
               <select name="ratePlanId">
               <%
                  Iterator it = vecRatePlans.iterator();
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    if (!vecTemp.get(0).toString().equals(RatePlan.getRatePlanId()))
                    {
                    out.println("<option value=\""+vecTemp.get(0).toString()+"\">" +
                                HTMLEncoder.encode(vecTemp.get(1).toString()) + "["+vecTemp.get(2).toString()+"]" +
                                "</option>");
                    }
                    else
                    {
                    out.println("<option value=\""+vecTemp.get(0).toString()+"\" selected>" +
                                HTMLEncoder.encode(vecTemp.get(1).toString()) + "["+vecTemp.get(2).toString()+"]" +
                                "</option>");

                    }
                  }
                  vecRatePlans.clear();


               %>
               </select>
               </td>
               <td valign="top">
                  <input type="hidden" name="search" value="y">
                  <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.submit1",SessionData.getLanguage())%>">
               </td>
               </tr>
              </form>
            </table>
            
            <%
if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>
            <form name="products" method="post" action="admin/rateplans/add_rep_rate2.jsp">
            <%=Languages.getString("jsp.admin.rateplans.add_rep_rate.instructions2",SessionData.getLanguage())%><br><br>
            <input type="hidden" name="ratePlanId" value="<%=RatePlan.getRatePlanId()%>">
            <input type="button" value="<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.check_all",SessionData.getLanguage())%>" onClick="this.value=checkAll(this.form.productId)">
            <input type="button" title="<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.select_between",SessionData.getLanguage())%>" name="range" value="<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.ranges",SessionData.getLanguage())%>" onClick="checkRanges(document.products)">
            <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.submit_next",SessionData.getLanguage())%>">
            <table width="450" cellspacing="1" cellpadding="1" border="0">
            <tr>
            	<td class=rowhead2 width=30 align=center>&nbsp;</td>
            	<td class=rowhead2 width=300><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
            	<td class=rowhead2 width=60><%=Languages.getString("jsp.admin.product_id",SessionData.getLanguage()).toUpperCase()%></td>
              <td class=rowhead2 width=60><%=Languages.getString("jsp.admin.rateplans.add_rep_rate.buy_rate",SessionData.getLanguage()).toUpperCase()%></td>
            </tr>
            <%
                  it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td><input onClick=\"s(this," +intEvenOdd+ ")\" type=checkbox name=productId value=" + vecTemp.get(1).toString() + "></td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(0).toString()) + "</td>" +
                                "<td>" + vecTemp.get(1).toString() + "</td>" +
                                "<td>" + NumberUtil.formatAmount(vecTemp.get(2).toString()) + "%</td></tr>");
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            <tr><td nowrap colspan="3">
            <input type="button" value="<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.check_all",SessionData.getLanguage())%>" onClick="this.value=checkAll(this.form.productId)">
            <input type="button" title="<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.select_between",SessionData.getLanguage())%>" name="range" value="<%=Languages.getString("jsp.admin.rateplans.add_rep_rate.ranges",SessionData.getLanguage())%>" onClick="checkRanges(document.products)">
            <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.submit_next",SessionData.getLanguage())%>">
            </table>

<%
}
else
{
 if (request.getParameter("search") != null)
 {
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.rateplans.add_rep_rate.error1",SessionData.getLanguage())+"</font>");
 }
}
}   else
  {
    out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.rateplans.add_rep_rate.error2",SessionData.getLanguage())+"</font>");
  }
%>                 
                 </table>
                 
                </td>
                   <td width="18">&nbsp;</td>
                   
               </tr>
               </table>
                     <div align=right class="backceldas"><font size="1"></div>
             </td>

             <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
       </tr>
       <tr>
           <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
         </tr>
         </table>
        </td>
    </tr>
</table>


</td>
</tr>
</table>
</td>
</tr>
</table>
</tr>
</table>
</td>
</tr>
</table>
                 


<%@ include file="/includes/footer.jsp" %>