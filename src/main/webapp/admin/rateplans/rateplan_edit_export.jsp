<%@page import="com.debisys.rateplans.RatePlan"%>
<%@page import="com.debisys.languages.Languages"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.debisys.utils.NumberUtil"%>
<%@page import="com.debisys.users.SessionData"%>
<%@page import="com.debisys.utils.DebisysConstants"%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request" />
<%
	response.addHeader("content-disposition", "filename=Products.xls");
	response.setCharacterEncoding("iso-8859-1");
	response.setContentType("application/vnd.ms-excel");

	Vector<String> vTmp = (Vector<String>)SessionData.getPropertyObj("CurrentRatePlanActor");
	if ( vTmp == null )
	{
		return;
	}
	String sRefId = vTmp.get(0);
	String sAccessLevel = vTmp.get(1);
	//String sChainLevel = vTmp.get(2);

	String sPageNumber, sPageSize, sSort, sSortDirection, sFilterId, sFilterName;
	int nTotalRows, nPageCount;

	Vector<String> vPlan = RatePlan.getRatePlanById(request.getParameter("id"));
	String sCarrier = null;
	if ( SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER) )
	{
		sCarrier = SessionData.getUser().getRefId();
	}
	Vector<Vector<String>> vProducts = RatePlan.getRatePlanProducts(request.getParameter("id"), request.getParameter("base"), 1, Integer.MAX_VALUE, "SKU", "A", "", "", sCarrier);
%>
<table>
	<tr style="color:#deeef3;background-color:#005eb2;">
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.SKU",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Amount",SessionData.getLanguage())%></td>
<%
			if ( SessionData.getUser().isIntranetUser() )
			{
%>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Provider",SessionData.getLanguage())%></td>
<%
			}
%>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Carrier",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.RateType",SessionData.getLanguage())%></td>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.TotalRate",SessionData.getLanguage())%></td>
<%
	if ( sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER) )
	{
		out.println("<td align=\"center\">&nbsp;" + Languages.getString("jsp.admin.rateplans.ISORate",SessionData.getLanguage()) + "&nbsp;</td>");
	}
	if ( sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER) )
	{
		out.println("<td align=\"center\">&nbsp;" + Languages.getString("jsp.admin.rateplans.AgentRate",SessionData.getLanguage()) + "&nbsp;</td>");
	}
	if ( sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER) )
	{
		out.println("<td align=\"center\">&nbsp;" + Languages.getString("jsp.admin.rateplans.SubAgentRate",SessionData.getLanguage()) + "&nbsp;</td>");
	}
%>
		<td align="center">&nbsp;<%=Languages.getString("jsp.admin.rateplans.RepRate",SessionData.getLanguage())%>&nbsp;</td>
		<td align="center">&nbsp;<%=Languages.getString("jsp.admin.rateplans.MerchantRate",SessionData.getLanguage())%>&nbsp;</td>
<%
	if ( vPlan.get(5).equals("0") )//0 = RatePlan, 1 = Template
	{
		if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) )
		{
			out.println("<td align=\"center\">" + Languages.getString("jsp.admin.rateplans.TopSellersOrder",SessionData.getLanguage()) + "</td>");
		}
		if ( SessionData.checkPermission(DebisysConstants.PERM_EDIT_CACHED_RECEIPTS) )
		{
			out.println("<td align=\"center\">" + Languages.getString("jsp.admin.rateplans.CachedReceipts",SessionData.getLanguage()) + "</td>");
		}
	}
%>
		<td align="center"><%=Languages.getString("jsp.admin.rateplans.Enabled",SessionData.getLanguage())%></td>
	</tr>
<%
	int nCounter = 1;
	Iterator<Vector<String>> it = vProducts.iterator();
	while ( it.hasNext() )
	{
		Vector<String> vItem = it.next();
		if ( nCounter == 1 )
		{
			nCounter++;
			continue;//We must skip this iteration since the first row is the count of total records
		}

		if ( vItem.get(15).equals("1") )
		{
			//vItem.get(17) is the Fixed_Fee
			vItem.set(5, Double.toString((Double.parseDouble(vItem.get(5)) * Double.parseDouble(vItem.get(17))) / 100));
			vItem.set(6, Double.toString((Double.parseDouble(vItem.get(6)) * Double.parseDouble(vItem.get(17))) / 100));
			vItem.set(7, Double.toString((Double.parseDouble(vItem.get(7)) * Double.parseDouble(vItem.get(17))) / 100));
			vItem.set(8, Double.toString((Double.parseDouble(vItem.get(8)) * Double.parseDouble(vItem.get(17))) / 100));
			vItem.set(9, Double.toString((Double.parseDouble(vItem.get(9)) * Double.parseDouble(vItem.get(17))) / 100));
			vItem.set(10, Double.toString((Double.parseDouble(vItem.get(10)) * Double.parseDouble(vItem.get(17))) / 100));
		}
%>
	<tr style="background-color:<%=(( (nCounter % 2) == 0 )?"#dddddd":"#ffffff")%>;">
		<td><%=vItem.get(0)%></td>
		<td><%=vItem.get(1)%></td>
		<td><%=NumberUtil.formatCurrency(vItem.get(2))%></td>
<%
			if ( SessionData.getUser().isIntranetUser() )
			{
%>
		<td><%=vItem.get(3)%></td>
<%
			}
%>
		<td><%=vItem.get(4)%></td>
		<td><%=(vItem.get(15).equals("1")?"$":"%")%></td>
<%
			Double dTotalRate = new Double(vItem.get(10));
			if ( sAccessLevel.equals(DebisysConstants.AGENT) )
			{
				dTotalRate -= new Double(vItem.get(5));
			}
			if ( sAccessLevel.equals(DebisysConstants.SUBAGENT) )
			{
				dTotalRate -= new Double(vItem.get(5));
				dTotalRate -= new Double(vItem.get(6));
			}
			if ( sAccessLevel.equals(DebisysConstants.REP) )
			{
				dTotalRate -= new Double(vItem.get(5));
				dTotalRate -= new Double(vItem.get(6));
				dTotalRate -= new Double(vItem.get(7));
			}
%>
		<td><%=NumberUtil.formatAmount(Double.toString(dTotalRate))%></td>
<%
		if ( sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.CARRIER) )
		{
			out.println("<td>" + NumberUtil.formatAmount(vItem.get(5)) + "</td>");
		}
		if ( sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.CARRIER) )
		{
			out.println("<td>" + NumberUtil.formatAmount(vItem.get(6)) + "</td>");
		}
		if ( sAccessLevel.equals(DebisysConstants.ISO) || sAccessLevel.equals(DebisysConstants.AGENT) || sAccessLevel.equals(DebisysConstants.SUBAGENT) || sAccessLevel.equals(DebisysConstants.CARRIER) )
		{
			out.println("<td>" + NumberUtil.formatAmount(vItem.get(7)) + "</td>");
		}
%>
		<td><%=NumberUtil.formatAmount(vItem.get(8))%></td>
		<td><%=NumberUtil.formatAmount(vItem.get(9))%></td>
<%
		if ( vPlan.get(5).equals("0") )//0 = RatePlan, 1 = Template
		{
			if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLED_TOP_SELLERS_EDITION) )
			{
				out.println("<td>" + vItem.get(13) + "</td>");
			}
			if ( SessionData.checkPermission(DebisysConstants.PERM_EDIT_CACHED_RECEIPTS) )
			{
				out.println("<td>" + ((vItem.get(14).equals("1"))?"YES":"NO") + "</td>");
			}
		}
%>
		<td><%=(vItem.get(11).equals("1"))?"YES":"NO"%></td>
	</tr>
<%
		vItem = null;
		nCounter++;
	}
	it = null;
%>
</table>
