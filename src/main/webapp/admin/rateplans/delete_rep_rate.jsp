<%@ page import="com.debisys.rateplans.RatePlan,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.NumberUtil,
                 com.debisys.utils.StringUtil,
                 java.util.Hashtable" %>
<%
int section=7;
int section_page=2;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:setProperty name="RatePlan" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
  if (request.getParameter("submit1") != null || request.getParameter("submit2") != null)
  {
    try
    {
      if (request.getParameter("submit1") != null && !request.getParameter("submit1").equals(""))
      {

        RatePlan.deleteRepRatePlan(SessionData);
        response.sendRedirect("/support/admin/rateplans/rep_rates.jsp");

      }
      else
      {
        response.sendRedirect("/support/admin/rateplans/rep_rates.jsp");
      }

    }
    catch (Exception e)
    {
    }
  }
%>
<%@ include file="/includes/header.jsp" %>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.rateplans.delete_rep_rate.title",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td class="main">

      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
<br>


            <form name="products" method="post" action="admin/rateplans/delete_rep_rate.jsp">
            <input type="hidden" name="repRatePlanId" value="<%=RatePlan.getRepRatePlanId()%>">
            <input type="hidden" name="submitted" value="y">
            <table width="575" cellspacing="1" cellpadding="1" border="0">
            <tr class=main>
              <td align=left colspan=7>
                <font color=ff0000><%=Languages.getString("jsp.admin.rateplans.delete_rep_rate.instructions",SessionData.getLanguage())%><br></font>
              <br>
              </td>
            </tr>
            <tr class=main>
              <td colspan=7 align=left><input type="submit" name="submit1" value="<%=Languages.getString("jsp.admin.rateplans.delete_rep_rate.yes",SessionData.getLanguage())%>">&nbsp;<input type="submit" name="submit2" value="<%=Languages.getString("jsp.admin.rateplans.delete_rep_rate.no",SessionData.getLanguage())%>"></td>
            </tr>

            </table>

          </td>
      </tr>
    </table>
    </form>
</td>
</tr>
</table>
</td>
</tr>
</table>

<%@ include file="/includes/footer.jsp" %>