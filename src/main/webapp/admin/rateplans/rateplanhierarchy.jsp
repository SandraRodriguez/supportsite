<%@page import="java.util.Vector"%>
<%@page import="java.util.Hashtable"%>
<%@page import="com.debisys.languages.Languages"%>
<%
int section = 7;
int section_page = 4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request" />
<jsp:setProperty name="RatePlan" property="*" />
<%@ include file="/includes/security.jsp"%>
<%
Hashtable<String, Vector<Vector<String>>> htData = RatePlan.getRatePlanHierarchy(SessionData, request.getParameter("id"));
%>
<html>
<head>
	<!-- <title>Debisys</title> -->
	<link href="/support/default.css" type="text/css" rel="stylesheet">
	<LINK href="/support/includes/sortROC.css" type="text/css" rel="StyleSheet" />
	<script type="text/javascript" src="/support/includes/jquery.js"></script>
</head>
<body><div style="text-align:center;">
<table id="tabData" border="0" cellpadding="0" cellspacing="0" width="650">
	<tr>
		<td background="/support/images/top_blue.gif" width="1%" align="left">
			<img src="/support/images/top_left_blue.gif" width="18" height="20">
		</td>
		<td background="/support/images/top_blue.gif" class="formAreaTitle" width="3000">
<%
	{
		String sName = htData.get("0").get(0).get(1);
		if ( sName.length() > 100 )
		{
			sName = sName.substring(0, 100) + "...";
		}
%>
			&nbsp;<%=Languages.getString("jsp.admin.rateplans.HierarchyFor",SessionData.getLanguage()).toUpperCase() + "[" + sName.toUpperCase() + "]"%>
<%
	}
%>
		</td>
		<td background="/support/images/top_blue.gif" width="1%" align="right">
			<img src="/support/images/top_right_blue.gif" width="18" height="20">
		</td>
	</tr>
	<tr><td colspan="3" class="formArea2">
<%
if ( htData.size() > 1 )
{
%>
		<table width="100%" cellspacing="1" cellpadding="1" border="0">
			<tr class="SectionTopBorder">
				<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap><%=Languages.getString("jsp.admin.rateplans.Name",SessionData.getLanguage()).toUpperCase()%></td>
				<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.Type",SessionData.getLanguage()).toUpperCase()%></td>
				<td class="rowhead2" style="text-align:center;vertical-align:top;"><%=Languages.getString("jsp.admin.rateplans.Level",SessionData.getLanguage()).toUpperCase()%></td>
				<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap><%=Languages.getString("jsp.admin.rateplans.CreatedBy",SessionData.getLanguage()).toUpperCase()%></td>
				<td class="rowhead2" style="text-align:center;vertical-align:top;" nowrap>&nbsp;<%=Languages.getString("jsp.admin.rateplans.BrowseHierarchy",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>
			</tr>
<%
	out.print(RatePlan.RenderHierarchyItem(((Vector<String>)SessionData.getPropertyObj("CurrentRatePlanActor")).get(0), htData, htData.get("0"), 0,SessionData));
%>
		</table>
<%
}
else
{
	out.println("<br/>" + Languages.getString("jsp.admin.rateplans.NoTemplatesAvailable",SessionData.getLanguage()) + "<br/><br/>");
}
%>
	</td></tr>
</table>
<script>
function DoBrowse(nActor, nType, nChainLevel, nPlanID)
{
	window.opener.DoBrowse(nActor, nType, nChainLevel, nPlanID);
	self.close();
}

$(document).ready( function() 
{
	var oTable = document.getElementById("tabData");
	var nMaxHeight = oTable.scrollHeight + 100;
	if ( nMaxHeight > (screen.height * 0.9) )
	{
		nMaxHeight = (screen.height * 0.9);
	}
	self.window.resizeTo(oTable.scrollWidth + 50, nMaxHeight);
	self.window.moveTo((screen.width - (oTable.scrollWidth + 50)) / 2, (screen.height - nMaxHeight) / 2);
});
</script>
</div></body>
</html>
