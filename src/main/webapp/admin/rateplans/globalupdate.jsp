<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    int section = 7;
    int section_page = 5;
%>
<%@ include file="/includes/security.jsp"%>

<%@ include file="/includes/header.jsp"%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />


<table border="0" cellpadding="0" cellspacing="0" width="300px" background="images/top_blue.gif">
    <TR>
        <td width="18" height="20" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
        <td class="formAreaTitle" align="left" width="3000"><%=SessionData.getString("jsp.admin.rateplans.GlobalUpdate").toUpperCase()%></td>
        <td width="12" height="20" align="right"><img src="images/top_right_blue.gif"></td>
    </TR>
    <TR>
        <td colspan="3"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                <TR>
                    <TD class="main">

                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="formArea2" align="center">
                                    <div id="divMenu" class="main" align="left" style="text-align:left;">
                                        <%=SessionData.getString("jsp.admin.rateplans.GlobalUpdateSelectAction")%><br />
                                        <br />
                                        <label>
                                            <input class="main" type="radio" id="rbtnAdd" name="GlobalAction" checked="checked"><%=SessionData.getString("jsp.admin.rateplans.AddProducts")%></label>
                                        <br />
                                        <br />
                                        <label>
                                            <input class="main" type="radio" id="rbtnRemove" name="GlobalAction"><%=SessionData.getString("jsp.admin.rateplans.RemoveProducts")%></label>
                                        <br />
                                        <br />
                                        <label>
                                            <input class="main" type="radio" id="rbtnEdit" name="GlobalAction"><%=SessionData.getString("jsp.admin.rateplans.EditRates")%></label>
                                        <br />
                                        <br />

                                        <%
                                            if ((SessionData.getUser().isIntranetUser()
                                                    && SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1, DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES))) {%>

                                        <label>
                                            <input class="main" type="radio" id="rbtnReferralAgent" name="GlobalAction"><%=SessionData.getString("jsp.admin.rateplans.referralAgent.referralAgentConfiguration")%></label>
                                        <br/><br/>

                                        <%}%>


                                        <input type="button" value="<%=SessionData.getString("jsp.admin.rateplans.Next")%>" onclick="DoSelectOperation();">
                                    </div>
                                    <div id="divAdd" style="margin: 5px; display: none; text-align: center; width: 99%;" class="main">
                                        <span style="width: 100%; text-align: left;" class="main"><%=SessionData.getString("jsp.admin.rateplans.GlobalUpdateSelectPlans")%></span>
                                        <br />
                                        <br />
                                        <div id="divInnerAdd">
                                        </div>
                                    </div>
                                    <div id="divRemove" style="margin: 5px; display: none; text-align: center; width: 99%;" class="main">
                                        <span style="width: 100%; text-align: left;" class="main"><%=SessionData.getString("jsp.admin.rateplans.GlobalUpdateSelectPlans")%></span>
                                        <br />
                                        <br />
                                        <div id="divInnerRemove">
                                        </div>
                                    </div>
                                    <div id="divUpdate" style="margin: 5px; display: none; text-align: center; width: 99%;" class="main">
                                        <span style="width: 100%; text-align: left;" class="main"><%=SessionData.getString("jsp.admin.rateplans.GlobalUpdateSelectPlans")%></span>
                                        <br />
                                        <br />
                                        <div id="divInnerUpdate">
                                        </div>
                                    </div>
                                    <div id="divAddProducts" style="margin: 5px; display: none; text-align: center; width: 99%;" class="main">
                                        <span style="width: 100%; text-align: left;" class="main"> <%=SessionData.getString("jsp.admin.rateplans.GlobalUpdateSelectProducts")%></span>
                                        <br /><br />
                                        <div style="text-align: left;width:500px;">
                                            <label class="main">
                                                <input type="checkbox" id="chkEnable" checked="on">&nbsp;<%=SessionData.getString("jsp.admin.rateplans.AddEnabled")%>
                                            </label><br />
                                            <label class="main">
                                                <input type="radio" id="rbtnProcessBoth" name="rbtnProcessOption" checked="checked">&nbsp;<%=SessionData.getString("jsp.admin.rateplans.ProcessBoth")%>
                                            </label><br/>
                                            <label class="main">
                                                <input type="radio" id="rbtnProcessTemplates" name="rbtnProcessOption">&nbsp;<%=SessionData.getString("jsp.admin.rateplans.ProcessTemplates")%>
                                            </label></div> <br />
                                        <br />
                                        <div id="divInnerAddProducts">
                                        </div>
                                    </div>
                                    <div id="divRemoveProducts" style="margin: 5px; display: none; text-align: center; width: 99%;" class="main">
                                        <span style="width: 100%; text-align: left;" class="main"><%=SessionData.getString("jsp.admin.rateplans.GlobalUpdateSelectProducts")%></span>
                                        <br />
                                        <br />
                                        <div id="divInnerRemoveProducts">
                                        </div>
                                    </div>
                                    <div id="divUpdateProducts" style="margin: 5px; display: none; text-align: center; width: 99%;" class="main">
                                        <span style="width: 100%; text-align: left;" class="main"><%=SessionData.getString("jsp.admin.rateplans.GlobalUpdateSelectProducts")%></span>
                                        <br />
                                        <br />
                                        <div id="divInnerUpdateProducts">
                                        </div>
                                    </div>
                                    <div id="divResult" style="margin: 5px; display: none; text-align: center; width: 99%;" class="main">
                                    </div>

                                </td>
                            </tr>

                        </table>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>


<script>
    <%/*This function shows the next screen depending on what operation the user selected*/%>
    function DoSelectOperation()
    {
        $('#divMenu').css('display', 'none');
        if ($('#rbtnAdd').is(':checked'))
        {
            $('#divAdd').css('display', 'block');
            $('#divInnerAdd').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerAdd').load('admin/rateplans/globalupdate_ajax.jsp?action=getHierarchy&op=ADD&id=0&level=0&filter=&Random=' + Math.random());
        }
        else if ($('#rbtnRemove').is(':checked'))
        {
            $('#divRemove').css('display', 'block');
            $('#divInnerRemove').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerRemove').load('admin/rateplans/globalupdate_ajax.jsp?action=getHierarchy&op=REMOVE&id=0&level=0&filter=&Random=' + Math.random());
        }
        else if ($('#rbtnEdit').is(':checked'))
        {
            $('#divUpdateProducts').css('display', 'block');
            $('#divInnerUpdateProducts').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerUpdateProducts').load('admin/rateplans/globalupdate_ajax.jsp?action=getUpdateProducts&Random=' + Math.random());
        }
        else if ($('#rbtnReferralAgent').is(':checked'))
        {
            
            if (isInternetExplorer()) {
                window.location.href = "referralAgents/referralAgentAssociationList.jsp";
            }
            else {
                window.location.href = "admin/rateplans/referralAgents/referralAgentAssociationList.jsp";
            }
        }
    }

    function isInternetExplorer() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {      // If Internet Explorer, return version number
            return true;
        }
        else {                 // If another browser, return 0
            return false;
        }
    }

    <%/*This function allows the user to return to the operation selection screen*/%>
    function DoReturnToMenu()
    {
        $('#divMenu').css('display', 'block');
        if ($('#rbtnAdd').is(':checked'))
        {
            $('#divAdd').css('display', 'none');
            $('#divInnerAdd').html("");
        }
        else if ($('#rbtnRemove').is(':checked'))
        {
            $('#divRemove').css('display', 'none');
            $('#divInnerRemove').html("");
        }
        else if ($('#rbtnEdit').is(':checked'))
        {
            $('#divUpdateProducts').css('display', 'none');
            $('#divInnerUpdateProducts').html("");
        }
    }

    <%/*This function checks or unchecks recursively the RatePlans*/%>
    function ToggleSelect(bValue)
    {
        var i;
        for (i = 0; i < SelectItems.length; i++)
        {
            ToggleChecks(SelectItems[i], bValue);
            document.getElementById('chk_' + SelectItems[i]).checked = bValue;
        }
    }

    <%/*This function checks or unchecks the Products*/%>
    function ToggleSelectProducts(bValue)
    {
        var i;
        for (i = 0; i < SelectProductItems.length; i++) {

            var chkP = document.getElementById('chkP_' + SelectProductItems[i]);
            if (!chkP.disabled) {
                chkP.checked = bValue;
            }
        }
    }

    <%/*This function checks or unchecks recursively a specific node of the RatePlan hierarchy*/%>
    function ToggleChecks(nID, bValue)
    {
        if (vPlansChildren[nID] != null)
        {
            var i;
            for (i = 0; i < vPlansChildren[nID].length; i++)
            {
                ToggleChecks(vPlansChildren[nID][i], bValue);
                document.getElementById('chk_' + vPlansChildren[nID][i]).checked = bValue;
                document.getElementById('chk_' + vPlansChildren[nID][i]).disabled = bValue;
            }
        }
    }

    <%/*This function opens a RatePlan node in the shown hierarchy*/%>
    function OpenNode(nID, nLevel)
    {
        if (vPlansChildren[nID] == null)
        {
            $.getJSON("/support/admin/rateplans/globalupdate_ajax.jsp?action=getHierarchy&id=" + nID + "&level=" + nLevel + "&checked=" + (document.getElementById("chk_" + nID).checked ? 1 : 0) + "&Random=" + Math.random(),
                    function (data)
                    {
                        var ctlTable = document.getElementById("tabPlans");
                        var nIndex;
                        var nCount = 1;
                        var vArray = new Array();

                        for (i = 0; i < ctlTable.rows.length; i++)
                        {
                            if (ctlTable.rows[i].id == ("tr_" + nID))
                            {
                                nIndex = i;
                                break;
                            }
                        }

                        $.each(data.items,
                                function (i, item)
                                {
                                    vArray[vArray.length] = item.id;
                                    var oRow = ctlTable.insertRow(nIndex + nCount++);
                                    oRow.id = "tr_" + item.id;
                                    oRow.className = "Hierarchy" + item.level;
                                    $("#" + oRow.id).html(item.data);
                                }
                        );
                        vPlansChildren[nID] = vArray;
                    }
            );
        }
        else
        {
            for (i = 0; i < vPlansChildren[nID].length; i++)
            {
                $("#tr_" + vPlansChildren[nID][i]).css("display", "");<%/*inline caused a display error in FF*/%>
            }
        }
        $("#aOpen_" + nID).css("display", "none");
        $("#aClose_" + nID).css("display", "inline");
    }

    <%/*This function closes a RatePlan node in the shown hierarchy*/%>
    function CloseNode(nID)
    {
        var nChildID;
        var i;

        for (i = 0; i < vPlansChildren[nID].length; i++)
        {
            nChildID = vPlansChildren[nID][i];
            if (vPlansChildren[nChildID] != null)
            {
                CloseNode(nChildID);
            }
            $("#tr_" + nChildID).css("display", "none");
        }
        $("#aOpen_" + nID).css("display", "inline");
        $("#aClose_" + nID).css("display", "none");
    }

    <%/*This function shows the screen of Product selection when adding products*/%>
    function DoShowAddProducts()
    {
        var sItems = "";
        var vItems = $("[type=checkbox]");
        var i;

        for (i = 0; i < vItems.length; i++)
        {
            if (vItems[i].checked && (vItems[i].id.indexOf("chk_") > -1))
            {
                sItems += vItems[i].id.replace("chk_", "") + ",";
            }
        }
        if (sItems.length > 0)
        {
            $('#divAdd').css('display', 'none');
            $('#divAddProducts').css('display', 'block');
            $('#divInnerAddProducts').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerAddProducts').load('admin/rateplans/globalupdate_ajax.jsp?action=getAddProducts&plans=' + sItems + '0&Random=' + Math.random());
        }
        else
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.SelectAPlan", SessionData.getLanguage())%>");
        }
    }

    <%/*This function shows the screen of Product selection when removing products*/%>
    function DoShowRemoveProducts()
    {
        var sItems = "";
        var vItems = $("[type=checkbox]");
        var i;

        for (i = 0; i < vItems.length; i++)
        {
            if (vItems[i].checked && (vItems[i].id.indexOf("chk_") > -1))
            {
                sItems += vItems[i].id.replace("chk_", "") + ",";
            }
        }
        if (sItems.length > 0)
        {
            $('#divRemove').css('display', 'none');
            $('#divRemoveProducts').css('display', 'block');
            $('#divInnerRemoveProducts').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerRemoveProducts').load('admin/rateplans/globalupdate_ajax.jsp?action=getRemoveProducts&plans=' + sItems + '0&Random=' + Math.random());
        }
        else
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.SelectAPlan", SessionData.getLanguage())%>");
        }
    }

    <%/*This function shows the screen of Plan selection when updating products*/%>
    function DoShowUpdatePlans()
    {
        var sItems = "";
        var i;

        for (i = 0; i < SelectProductItems.length; i++)
        {
            if (document.getElementById('chkP_' + SelectProductItems[i]).checked)
            {
                sItems += SelectProductItems[i] + ",";
            }
        }

        if (sItems.length > 0)
        {
            $('#divUpdateProducts').css('display', 'none');
            $('#divUpdate').css('display', 'block');
            $('#divInnerUpdate').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerUpdate').load('admin/rateplans/globalupdate_ajax.jsp?action=getHierarchy&op=UPDATE&id=0&level=0&filter=' + sItems + '0&Random=' + Math.random());
        }
        else
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.SelectAProduct", SessionData.getLanguage())%>");
        }
    }

    <%/*This function allows the user to return to the RatePlan selection screen when adding products*/%>
    function DoReturnToAddPlans()
    {
        $('#divAdd').css('display', 'block');
        $('#divAddProducts').css('display', 'none');
        $('#divInnerAddProducts').html('');
    }

    <%/*This function allows the user to return to the RatePlan selection screen when removing products*/%>
    function DoReturnToRemovePlans()
    {
        $('#divRemove').css('display', 'block');
        $('#divRemoveProducts').css('display', 'none');
        $('#divInnerRemoveProducts').html('');
    }

    <%/*This function allows the user to return to the Products selection screen when updating rates*/%>
    function DoReturnToUpdateProducts()
    {
        $('#divUpdateProducts').css('display', 'block');
        $('#divUpdate').css('display', 'none');
        $('#divInnerUpdate').html('');
    }

    <%/*This function shows the summary of changes before doing the Product Addition*/%>
    function DoConfirmAddProducts()
    {
        var sItems = "";
        var sCopyProduct = "";
        var i, s, sText;
        var bLowerSum = false;
        var bExceedRates = false;

        if ((typeof SelectProductItems) != "undefined")
        {
            for (i = 0; i < SelectProductItems.length; i++)
            {
                if (document.getElementById('chkP_' + SelectProductItems[i]) == null)
                {
                    break;
                }

                if (document.getElementById('chkP_' + SelectProductItems[i]).checked)
                {
                    if (document.getElementById('ISO_' + SelectProductItems[i]) == null)
                    {
                        sItems += SelectProductItems[i] + ",";
                    }
                    else
                    {
                        switch (ProcessRate(document.getElementById('ISO_' + SelectProductItems[i]), true))
                        {
                            case "INVALID_VALUE":
                            case "INVALID_SUM":
                                alert("<%=Languages.getString("jsp.admin.rateplans.ProductErrors", SessionData.getLanguage())%>");
                                return;
                                break;
                            case "EXCEED_SUM":
                                bExceedRates = true;
                                sItems += GetProductChanges(SelectProductItems[i]);
                                HighlightRow(document.getElementById('ISO_' + SelectProductItems[i]), false);
                                break;
                            case "LOWER_SUM":
                                bLowerSum = true;
                                sItems += GetProductChanges(SelectProductItems[i]);
                                HighlightRow(document.getElementById('ISO_' + SelectProductItems[i]), false);
                                break;
                            case "OK":
                                sItems += GetProductChanges(SelectProductItems[i]);
                                HighlightRow(document.getElementById('ISO_' + SelectProductItems[i]), false);
                                break;
                        }
                    }
                }
            }
        }

        if (sItems.length > 0)
        {
            if ($('#chkCopy').is(':checked'))
            {
                if (document.getElementsByName("CheckCopyProduct").length == 0)
                {
                    alert("<%=Languages.getString("jsp.admin.rateplans.UseCopySelectProduct", SessionData.getLanguage())%>");
                    return;
                }

                for (i = 0; i < SelectCopyProductItems.length; i++)
                {
                    if (document.getElementById('rbtnP_' + SelectCopyProductItems[i]).checked)
                    {
                        sCopyProduct = SelectCopyProductItems[i];
                    }
                }
            }
            else
            {
                if (bExceedRates)
                {
    <%if (SessionData.getUser().isIntranetUser()
                && SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
                        DebisysConstants.INTRANET_PERMISSION_ALLOW_EXCEED_RATES)) {%>
                    sText = "<%=Languages.getString("jsp.admin.rateplans.SumExceedsRates", SessionData.getLanguage())%>";
                    if (bLowerSum)
                    {
                        sText += "\n\n<%=Languages.getString("jsp.admin.rateplans.SumLowerAddition", SessionData.getLanguage())%>";
                    }
                    sText += "\n\n>> <%=Languages.getString("jsp.admin.rateplans.SumAdditionConfirm", SessionData.getLanguage())%> <<";
                    if (!confirm(sText))
                    {
                        return;
                    }
    <%} else {%>
                    alert("<%=Languages.getString("jsp.admin.rateplans.SumExceeded", SessionData.getLanguage())%>");
                    return;
    <%}%>
                }
                else if (bLowerSum)
                {
                    if (!confirm("<%=Languages.getString("jsp.admin.rateplans.SumLower", SessionData.getLanguage())%>"))
                    {
                        return;
                    }
                }
            }
            $('#divAddProducts').css('display', 'none');
            $('#divResult').css('display', 'block');
            s = "<%=Languages.getString("jsp.admin.rateplans.GlobalUpdateWait", SessionData.getLanguage())%>";
            $('#divResult').html(s);
            //$('#divResult').load('admin/rateplans/globalupdate_ajax.jsp?action=doConfirmAddProducts&products=' + sItems + '0&Enable=' + (($('#chkEnable').is(':checked')) ? 1 : 0) + '&Copy=' + sCopyProduct + '&ProcessBoth=' + (($('#rbtnProcessBoth').is(':checked')) ? 2 : 0) + '&Random=' + Math.random());
            $( "#divResult" ).load( "admin/rateplans/globalupdate_ajax.jsp", 
                { 
                    action: "doConfirmAddProducts",
                    products: sItems+"0",
                    Enable: (($('#chkEnable').is(':checked')) ? 1 : 0),
                    Copy: sCopyProduct,
                    ProcessBoth: (($('#rbtnProcessBoth').is(':checked')) ? 2 : 0),
                    Random : Math.random()
                }
            );
        }
        else
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.SelectAProduct", SessionData.getLanguage())%>");
        }
    }

    <%/*This function shows the summary of changes before doing the Product Removal*/%>
    function DoConfirmRemoveProducts()
    {
        var sItems = "";
        var i, s;

        for (i = 0; i < SelectProductItems.length; i++)
        {
            if (document.getElementById('chkP_' + SelectProductItems[i]).checked)
            {
                sItems += SelectProductItems[i] + ",";
            }
        }

        if (sItems.length > 0)
        {
            $('#divRemoveProducts').css('display', 'none');
            $('#divResult').css('display', 'block');
            s = "<%=Languages.getString("jsp.admin.rateplans.GlobalUpdateWait", SessionData.getLanguage())%>";
            $('#divResult').html(s);
            $('#divResult').load('admin/rateplans/globalupdate_ajax.jsp?action=doConfirmRemoveProducts&products=' + sItems + '&Random=' + Math.random());
        }
        else
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.SelectAProduct", SessionData.getLanguage())%>");
        }
    }

    <%/*This function shows the summary of changes before doing the Global Update Rate*/%>
    function DoConfirmUpdateRates()
    {
        var sItems = "";
        var vItems = $("[type=checkbox]");
        var i, s;
        var nEmida, nISO, nAgent, nSubAgent, nRep, nMerchant;
        var nSumRates, nMinValues;

        for (i = 0; i < vItems.length; i++)
        {
            if (vItems[i].checked && (vItems[i].id.indexOf("chk_") > -1))
            {
                sItems += vItems[i].id.replace("chk_", "") + ",";
            }
        }

        nEmida = parseFloat(document.getElementById("Emida").value);
        nISO = parseFloat(document.getElementById("ISO").value);
        nAgent = parseFloat(document.getElementById("Agent").value);
        nSubAgent = parseFloat(document.getElementById("SubAgent").value);
        nRep = parseFloat(document.getElementById("Rep").value);
        nMerchant = parseFloat(document.getElementById("Merchant").value);

        nSumRates = nEmida + nISO + nAgent + nSubAgent + nRep + nMerchant;
        if (nSumRates != 0)
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.EnterRatesValidation", SessionData.getLanguage())%>");
            return;
        }
        nSumRates = nISO + "," + nAgent + "," + nSubAgent + "," + nRep + "," + nMerchant;

        nISO = parseFloat(document.getElementById("ISOMin").value);
        nAgent = parseFloat(document.getElementById("AgentMin").value);
        nSubAgent = parseFloat(document.getElementById("SubAgentMin").value);
        nRep = parseFloat(document.getElementById("RepMin").value);
        nMerchant = parseFloat(document.getElementById("MerchantMin").value);
        nMinValues = nISO + "," + nAgent + "," + nSubAgent + "," + nRep + "," + nMerchant;

        if (sItems.length > 0)
        {
            $('#divUpdate').css('display', 'none');
            $('#divResult').css('display', 'block');
            s = "<%=Languages.getString("jsp.admin.rateplans.GlobalUpdateWait", SessionData.getLanguage())%>";
            $('#divResult').html(s);
            $('#divResult').load('admin/rateplans/globalupdate_ajax.jsp?action=doConfirmUpdateRates&plans=' + sItems + '0&Rates=' + nSumRates + '&Mins=' + nMinValues + '&Random=' + Math.random());
        }
        else
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.SelectAPlan", SessionData.getLanguage())%>");
        }
    }

    <%/*This function allows the user to return to the screen of selecting products for Addition*/%>
    function DoReturnToAddProducts()
    {
        $('#divAddProducts').css('display', 'block');
        $('#divResult').css('display', 'none');
        $('#divResult').html('');
    }

    <%/*This function allows the user to return to the screen of selecting products for Removal*/%>
    function DoReturnToRemoveProducts()
    {
        $('#divRemoveProducts').css('display', 'block');
        $('#divResult').css('display', 'none');
        $('#divResult').html('');
    }

    <%/*This function allows the user to return to the Plans selection screen when updating rates*/%>
    function DoReturnToUpdatePlans()
    {
        $('#divUpdate').css('display', 'block');
        $('#divResult').css('display', 'none');
        $('#divResult').html('');
    }

    <%/*This function is the last step before invoking the Product Addition*/%>
    function DoAddProducts()
    {
        if (confirm("<%=Languages.getString("jsp.admin.rateplans.ConfirmGlobalOperation", SessionData.getLanguage())%>"))
        {
            $('#btnFinish').attr("disabled", true);
            $('#btnBack').attr("disabled", true);
            $.post('admin/rateplans/globalupdate_ajax.jsp?action=doAddProducts&Random=' + Math.random(), ProcessOperationResult);
        }
    }

    <%/*This function is the last step before invoking the Product Removal*/%>
    function DoRemoveProducts()
    {
        if (confirm("<%=Languages.getString("jsp.admin.rateplans.ConfirmGlobalOperation", SessionData.getLanguage())%>"))
        {
            $('#btnFinish').attr("disabled", true);
            $('#btnBack').attr("disabled", true);
            $.post('admin/rateplans/globalupdate_ajax.jsp?action=doRemoveProducts&Random=' + Math.random(), ProcessOperationResult);
        }
    }

    <%/*This function is the last step before invoking the Update Rates*/%>
    function DoUpdateRates()
    {
        if (confirm("<%=Languages.getString("jsp.admin.rateplans.ConfirmGlobalOperation", SessionData.getLanguage())%>"))
        {
            $('#btnFinish').attr("disabled", true);
            $('#btnBack').attr("disabled", true);
            $.post('admin/rateplans/globalupdate_ajax.jsp?action=doUpdateRates&Random=' + Math.random(), ProcessOperationResult);
        }
    }

    <%/*This function calls itself each second to show the advance of the Global Update Operation*/%>
    function ProcessOperationResult(sData, sStatus)
    {
        if (sStatus == "success")
        {
            if (sData.match("-VALID-") == "-VALID-")
            {
                if (sData.split(":")[1] > 0)
                {
                    var s = "<%=Languages.getString("jsp.admin.rateplans.GlobalUpdatePending", SessionData.getLanguage())%>";
                    s = s.replace("_COUNT_", sData.split(":")[1]);
                    $('#divResult').html(s);
                    window.setTimeout("$.post('admin/rateplans/globalupdate_ajax.jsp?action=checkStatus&Random=' + Math.random(), ProcessOperationResult);", 1000);
                }
                else if (sData.split(":")[1] < 0)
                {
                    $('#divResult').html("<%=Languages.getString("jsp.admin.rateplans.GlobalUpdateInProcess", SessionData.getLanguage())%>");
                }
                else
                {
                    alert("<%=Languages.getString("jsp.admin.rateplans.GlobalUpdateFinished", SessionData.getLanguage())%>");
                    location.href = location.href;
                }
            }
            else
            {
                alert("<%=Languages.getString("jsp.admin.rateplans.GlobalUpdateError", SessionData.getLanguage())%>");
                $('#btnFinish').attr("disabled", false);
                $('#btnBack').attr("disabled", false);
            }
        }
        else
        {
            alert("<%=Languages.getString("jsp.admin.rateplans.GlobalUpdateError", SessionData.getLanguage())%>");
            $('#btnFinish').attr("disabled", false);
            $('#btnBack').attr("disabled", false);
        }
    }

    <%/*This function highlights the row of rates being edited, if there's an error in the rates the highlighted color is red*/%>
    function HighlightRow(ctl, bActive)
    {
        var nID = ctl.id.split("_")[1];

        if (bActive)
        {
            /*
             if ( ProcessRate(ctl, false) == "OK" )
             {
             document.getElementById("tdISO_" + nID).style.backgroundColor = "#4040FF";
             document.getElementById("tdAgent_" + nID).style.backgroundColor = "#4040FF";
             document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#4040FF";
             document.getElementById("tdRep_" + nID).style.backgroundColor = "#4040FF";
             document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#4040FF";
             document.getElementById("tdRef_" + nID).style.backgroundColor = "#4040FF";
             }
             */
        }
        else
        {
            if (ProcessRate(ctl, true) == "OK")
            {
                document.getElementById("tdISO_" + nID).style.backgroundColor = "";
                document.getElementById("tdAgent_" + nID).style.backgroundColor = "";
                document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "";
                document.getElementById("tdRep_" + nID).style.backgroundColor = "";
                document.getElementById("tdMerchant_" + nID).style.backgroundColor = "";
                document.getElementById("tdRef_" + nID).style.backgroundColor = "";
            }
        }
    }

    <%/*This function analyze the rates being edited and do the proper math to adjust the entered values*/%>
    function ProcessRate(ctl, bFormatText)
    {
        var nID = ctl.id.split("_")[1];
        var nValue = parseFloat(ctl.value);
        var nISO, nAgent, nSubAgent, nRep, nMerchant, nRef;
        var nSumRates = 0, nDifference;

        if (isNaN(nValue))
        {
            $("#tdISO_" + nID).css("background-color", "#C05858");
            $("#tdAgent_" + nID).css("background-color", "#C05858");
            $("#tdSubAgent_" + nID).css("background-color", "#C05858");
            $("#tdRep_" + nID).css("background-color", "#C05858");
            $("#tdMerchant_" + nID).css("background-color", "#C05858");
            $("#tdRef_" + nID).css("background-color", "#C05858");
            document.getElementById("chkP_" + nID).checked = false;
            return "INVALID_VALUE";
        }

        nValue = Math.abs(nValue);
        if (bFormatText)
        {
            ctl.value = nValue.toFixed(2);
        }
        nISO = parseFloat(document.getElementById("ISO_" + nID).value);
        nAgent = parseFloat(document.getElementById("Agent_" + nID).value);
        nSubAgent = parseFloat(document.getElementById("SubAgent_" + nID).value);
        nRep = parseFloat(document.getElementById("Rep_" + nID).value);
        nMerchant = parseFloat(document.getElementById("Merchant_" + nID).value);
        nRef = parseFloat(document.getElementById("Ref_" + nID).value);

        if (isNaN(nISO) || isNaN(nAgent) || isNaN(nSubAgent) || isNaN(nRep) || isNaN(nMerchant) || isNaN(nRef))
        {
            $("#tdISO_" + nID).css("background-color", "#C05858");
            $("#tdAgent_" + nID).css("background-color", "#C05858");
            $("#tdSubAgent_" + nID).css("background-color", "#C05858");
            $("#tdRep_" + nID).css("background-color", "#C05858");
            $("#tdMerchant_" + nID).css("background-color", "#C05858");
            $("#tdRef_" + nID).css("background-color", "#C05858");
            document.getElementById("chkP_" + nID).checked = false;
            return "INVALID_SUM";
        }

        nISO = Math.abs(nISO);
        nAgent = Math.abs(nAgent);
        nSubAgent = Math.abs(nSubAgent);
        nRep = Math.abs(nRep);
        nMerchant = Math.abs(nMerchant);
        nRef = Math.abs(nRef);
        if (bFormatText)
        {
            document.getElementById("ISO_" + nID).value = nISO.toFixed(2);
            document.getElementById("Agent_" + nID).value = nAgent.toFixed(2);
            document.getElementById("SubAgent_" + nID).value = nSubAgent.toFixed(2);
            document.getElementById("Rep_" + nID).value = nRep.toFixed(2);
            document.getElementById("Merchant_" + nID).value = nMerchant.toFixed(2);
            document.getElementById("Ref_" + nID).value = nRef.toFixed(2);
        }
        nSumRates = nISO + nAgent + nSubAgent + nRep + nMerchant + nRef;

        if (parseFloat(nSumRates) > parseFloat(document.getElementById("Buy_" + nID).value))
        {
            document.getElementById("tdISO_" + nID).style.backgroundColor = "#C05858";
            document.getElementById("tdAgent_" + nID).style.backgroundColor = "#C05858";
            document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#C05858";
            document.getElementById("tdRep_" + nID).style.backgroundColor = "#C05858";
            document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#C05858";
            document.getElementById("tdRef_" + nID).style.backgroundColor = "#C05858";
            document.getElementById("chkP_" + nID).checked = true;
            return "EXCEED_SUM";
        }
        else if (parseFloat(nSumRates) < parseFloat(document.getElementById("Buy_" + nID).value))
        {
            document.getElementById("tdISO_" + nID).style.backgroundColor = "#FFFFAA";
            document.getElementById("tdAgent_" + nID).style.backgroundColor = "#FFFFAA";
            document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "#FFFFAA";
            document.getElementById("tdRep_" + nID).style.backgroundColor = "#FFFFAA";
            document.getElementById("tdMerchant_" + nID).style.backgroundColor = "#FFFFAA";
            document.getElementById("tdRef_" + nID).style.backgroundColor = "#FFFFAA";
            document.getElementById("chkP_" + nID).checked = true;
            return "LOWER_SUM";
        }
        else
        {
            document.getElementById("tdISO_" + nID).style.backgroundColor = "";
            document.getElementById("tdAgent_" + nID).style.backgroundColor = "";
            document.getElementById("tdSubAgent_" + nID).style.backgroundColor = "";
            document.getElementById("tdRep_" + nID).style.backgroundColor = "";
            document.getElementById("tdMerchant_" + nID).style.backgroundColor = "";
            document.getElementById("tdRef_" + nID).style.backgroundColor = "";
            document.getElementById("chkP_" + nID).checked = true;
            return "OK";
        }
    }

    <%/*This function summarizes the rates for a specific product before doing the GlobalAdding*/%>
    function GetProductChanges(nID)
    {
        var nISO, nAgent, nSubAgent, nRep, nMerchant, nRef, nRefID, nUseMoneyRates, nBuyRate, sItemData;

        nISO = parseFloat(document.getElementById("ISO_" + nID).value);
        nAgent = parseFloat(document.getElementById("Agent_" + nID).value);
        nSubAgent = parseFloat(document.getElementById("SubAgent_" + nID).value);
        nRep = parseFloat(document.getElementById("Rep_" + nID).value);
        nMerchant = parseFloat(document.getElementById("Merchant_" + nID).value);
        nRef = parseFloat(document.getElementById("Ref_" + nID).value);
        nRefID = document.getElementById("RefID_" + nID).value;
        nUseMoneyRates = document.getElementById("UseMoneyRates_" + nID).value;
        sItemData = nID + "," + nISO + "," + nAgent + "," + nSubAgent + "," + nRep + "," + nMerchant + "," + nRef + "," + nRefID + ",0|";

        if (nUseMoneyRates == "1")
        {
            nBuyRate = parseFloat(document.getElementById("Buy_" + nID).value);
            nISO = parseFloat((nISO * 100) / nBuyRate).toFixed(2);
            nAgent = parseFloat((nAgent * 100) / nBuyRate).toFixed(2);
            nSubAgent = parseFloat((nSubAgent * 100) / nBuyRate).toFixed(2);
            nRep = parseFloat((nRep * 100) / nBuyRate).toFixed(2);
            nMerchant = parseFloat((nMerchant * 100) / nBuyRate).toFixed(2);
            nRef = parseFloat((nRef * 100) / nBuyRate).toFixed(2);
            sItemData = nID + "," + nISO + "," + nAgent + "," + nSubAgent + "," + nRep + "," + nMerchant + "," + nRef + "," + nRefID + ",0|";
        }

        return sItemData;
    }

    function ToggleRefList(nID, bShow)
    {
        var divRefList = document.getElementById("divRef_" + nID);
        var ddlRefList = document.getElementById("ddlRef_" + nID);
        divRefList.style.display = bShow ? "inline" : "none";
        divRefList.parentNode.style.display = bShow ? "inline" : "none";
        if (bShow)
        {
            ddlRefList.value = document.getElementById("RefID_" + nID).value;
            ddlRefList.focus();
        }
    }

    function SetSelectedRefAgent(ctl)
    {
        var nID = ctl.id.split("_")[1];

        document.getElementById("RefID_" + nID).value = ctl.value;
        if (ctl.value == "")
        {
            document.getElementById("Ref_" + nID).value = "0.00";
            document.getElementById("Ref_" + nID).disabled = true;
            ProcessRate(document.getElementById("Ref_" + nID), true);
        }
        else
        {
            document.getElementById("Ref_" + nID).disabled = false;
        }
        ToggleRefList(nID, false);
    }

    <%/*This function validates the rates entered by the user*/%>
    function ValidateRate(ctl)
    {
        var nValue = parseFloat(ctl.value);
        var nEmida, nISO, nAgent, nSubAgent, nRep, nMerchant;

        if (isNaN(nValue))
        {
            nValue = parseFloat("0");
        }

        nValue = nValue.toFixed(2);
        ctl.value = nValue;

        nISO = parseFloat(document.getElementById("ISO").value);
        nAgent = parseFloat(document.getElementById("Agent").value);
        nSubAgent = parseFloat(document.getElementById("SubAgent").value);
        nRep = parseFloat(document.getElementById("Rep").value);
        nMerchant = parseFloat(document.getElementById("Merchant").value);

        nEmida = 0 - parseFloat(nISO + nAgent + nSubAgent + nRep + nMerchant);
    <%if (SessionData.getUser().isIntranetUser()
                && SessionData.getUser().hasIntranetUserPermission(SessionData.getUser().getIntranetUserId(), 1,
                        DebisysConstants.INTRANET_PERMISSION_MANAGE_GLOBAL_UPDATES)
                || SessionData.getUser().getAccessLevel().equals(DebisysConstants.CARRIER)) {
            out.println("document.getElementById(\"Emida\").value = nEmida.toFixed(2);");
        }%>
    }

    <%/*This function validates the Minimum values entered by the user*/%>
    function ValidateMin(ctl)
    {
        var nValue = parseFloat(ctl.value);

        if (isNaN(nValue))
        {
            nValue = parseFloat("0");
        }

        nValue = Math.abs(nValue);
        nValue = nValue.toFixed(2);
        ctl.value = nValue;
    }

    <%/*This function performs the retrieval of results when paging, filtering or sorting*/%>
    function DoRefreshList()
    {
        var sParams = "";

        sParams += "&txtSort=" + $("#txtSort").val();
        sParams += "&txtSortDirection=" + $("#txtSortDirection").val();
        sParams += "&txtFilterID=" + escape($("#txtFilterID").val());
        sParams += "&txtFilterName=" + escape($("#txtFilterName").val());
        sParams += "&txtPageNumber=" + $("#txtPageNumber").val();
        sParams += "&ddlPageSize=" + $("#ddlPageSize").val();

        if ($('#rbtnAdd').is(':checked'))
        {
            $('#divInnerAddProducts').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerAddProducts').load('admin/rateplans/globalupdate_ajax.jsp?action=getAddProducts&plans=&Random=' + Math.random() + sParams);
        }
        else if ($('#rbtnRemove').is(':checked'))
        {
            $('#divInnerRemoveProducts').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerRemoveProducts').load('admin/rateplans/globalupdate_ajax.jsp?action=getRemoveProducts&plans=&Random=' + Math.random() + sParams);
        }
        else if ($('#rbtnEdit').is(':checked'))
        {
            $('#divInnerUpdateProducts').html("<%=Languages.getString("jsp.admin.rateplans.pleasewait", SessionData.getLanguage())%>");
            $('#divInnerUpdateProducts').load('admin/rateplans/globalupdate_ajax.jsp?action=getUpdateProducts&Random=' + Math.random() + sParams);
        }
    }
</script>
<%@ include file="/includes/footer.jsp"%>
