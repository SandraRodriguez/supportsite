<%@ page import="java.util.Hashtable,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.utils.DebisysConfigListener,
                 com.debisys.languages.Languages,
                 java.util.Vector, java.util.Iterator,
                 java.util.Random
                 "%>
<jsp:useBean id="User" class="com.debisys.users.User" scope="request"/>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:setProperty name="User" property="*"/>
<script>
<%
	String langua="";
	String pathContp = request.getContextPath();
	String newloc= pathContp+"/servlet/Languages?lan=";
	String newloc_1=pathContp+"/servlet/Languages?lan=";
	String newloc_2="&page="+request.getContextPath() + request.getServletPath();
%>
function changeLanguage(obj){
	if(obj.value=="spanish"){
<%langua="spanish";
		newloc+=langua+newloc_2;
		System.out.println("newloc_spanish" + newloc);
	%>

		window.location = "<%=newloc%>";
	}else if(obj.value=="english"){
<%langua="english";
		newloc_1+=langua+newloc_2;
		System.out.println("newloc_english" + newloc_1);
	%>

		window.location = "<%=newloc%>";
	}
}
</script>
<%
if(SessionData.getUser()==null){
	if(SessionData.getLanguage().equals("1")){
		SessionData.setLanguage(application.getAttribute("language").toString());
	}
}
	
if ( SessionData.isLoggedIn() ){
	//out.println("this user is already logged.");
	response.sendRedirect("/support/admin/index.jsp");
	return;
}
		
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
int section = 1;
int section_page = 1;
//String s = request.getServerPort() + " " + request.getLocalPort() + " " + request.getRemotePort();
Hashtable userErrors = (Hashtable)request.getSession().getAttribute("userErrors");

if (userErrors != null){
	System.out.println("errors jsp");
}else{
	request.getSession().removeAttribute("userErrors");
}

String strDistChainType = "";
String strAccessLevel = "";
String strUrl = request.getParameter("url");
	

int sq1; 
int sq2;
// select questions
Random r = new Random();
sq1 = r.nextInt(4);
do{
	sq2 = r.nextInt(4);
}while(sq2 == sq1);
%>
<%@ include file="/includes/header.jsp" %>
<script>
	$(document).ready(function(){
		$('#answerform').submit(function(){
			var ok = true;
			$('#msgs').html('');
			if($('#username').val() == ''){
				$('#msgs').append('<div class="error"><%=Languages.getString("jsp.answers.username_error",SessionData.getLanguage())%></div>')
				ok = false;
			}
			if($('#answer1').val() == ''){
				$('#msgs').append('<div class="error"><%=Languages.getString("jsp.answers.answer1_error",SessionData.getLanguage())%></div>')
				ok = false;
			}
			if($('#answer2').val() == ''){
				$('#msgs').append('<div class="error"><%=Languages.getString("jsp.answers.answer2_error",SessionData.getLanguage())%></div>')
				ok = false;
			}
			return ok;
		});
	});
</script>
<table cellSpacing="0" cellPadding="0" width="600" border="0">
	<tbody>
		<tr>
			<td align="left" width="1%" background="images/top_blue.gif">
				<img height="20" src="images/top_left_blue.gif" width="18">
			</td>
			<td class="formAreaTitle" background="images/top_blue.gif" width="2000">
				&nbsp;
				<b class="formAreaTitle"><%=Languages.getString("jsp.answers.forgot_password",SessionData.getLanguage()).toUpperCase()%></b>
			</td>
			<td align="right" width="1%" background="images/top_blue.gif">
				<img height="20" src="images/top_right_blue.gif" width="18">
			</td>
		</tr>
		<tr>
			<td bgColor="#5F8716" colSpan="3">
				<table cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tbody>
						<tr>
							<td>
								<form id="answerform" name="answerform" action="<%=path%>/servlet/SecurityLoginExtern" method="post">
									<input type="hidden" value="y" name="submit_answers">
<%
	if (strUrl != null && !strUrl.equals("") && !strUrl.equalsIgnoreCase("null")){
		out.print("<input type=\"hidden\" name=\"url\" value=\"" +strUrl+ "\">");
	}
%>
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tbody>
											<tr>
												<td class="formArea2">
													<div class="mainpad">
														<br/>
														<%=Languages.getString("jsp.answers.instructions",SessionData.getLanguage())%>
													</div>
													<br/>
													<div class="rowred2" style="padding-left:18px;" id="msgs">
<%
	if (userErrors != null){
%>
														<%=(userErrors.containsKey("login")?userErrors.get("login"):Languages.getString("jsp.answers.doesnt_match",SessionData.getLanguage()))%>
														<%=(userErrors.containsKey("noanswers")?userErrors.get("noanswers"):"")%>
<%
	}
%>
													</div>
													<table width="100%">
														<tbody>
															<tr class="main">
																<td noWrap="nowrap" style="padding-left:18px;width:60%;">
																	<%=Languages.getString("jsp.answers.username",SessionData.getLanguage())%>:
																</td>
																<td style="width:40%;s">
																	<input maxLength="64" style="width:90%;" id="username" name="username" value="<%=User.getUsername()%>"/>
																	&nbsp;&nbsp;&nbsp;
																</td>
															</tr>
															<tr class="main">
																<td colspan="100%">
																	&nbsp;
																</td>
															</tr>
															<tr class="main">
																<td style="padding-left:18px;">
																	<%=Languages.getString("jsp.answers.security_question" + sq1, SessionData.getLanguage())%>:
																</td>
																<td>
																	<input type="hidden" name="sq1" value="<%=sq1%>"/>
																	<input type="text" maxLength="100" style="width:90%;" id="answer1" name="answer1"/>
																</td>
															</tr>
															<tr class="main">
																<td style="padding-left:18px;">
																	<%=Languages.getString("jsp.answers.security_question" + sq2, SessionData.getLanguage())%>:
																</td>
																<td>
																	<input type="hidden" name="sq2" value="<%=sq2%>"/>
																	<input type="text" maxLength="64" style="width:90%;" id="answer2" name="answer2"/>
																</td>
															</tr>
														</tbody>
													</table>
													<table width="100%">
														<tbody>
															<tr class="main">
																<td align="center">
																	<input type="submit" value="<%=Languages.getString("jsp.answers.submitanswers",SessionData.getLanguage())%>" name="submit">
																</td>
															</tr>
														</tbody>
													</table>
													<br><br>
												</td>
											</tr>
										</tbody>
									</table>
								</form>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<%@ include file="/includes/footer.jsp" %>
