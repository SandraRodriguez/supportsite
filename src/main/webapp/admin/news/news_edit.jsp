<%@ page language="java" import="
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.presentation.NewsComponent" pageEncoding="ISO-8859-1"%>
<%
int section=9;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="News" class="com.debisys.news.News" scope="request"/>
<jsp:setProperty name="News" property="*"/>
<%@include file="/includes/security.jsp" %>
<%
NewsComponent nc = new NewsComponent(News);
String action = "" + request.getParameter("action");
String message = request.getParameter("message");
String messageType = request.getParameter("message");
nc.loadFromRequest(request, response, application,SessionData);
if(action.equals("save")){
	if(nc.save()){
		message = "News saved successfully.";
		messageType = "1";
	}else{
		message = "Error saving news";
		messageType = "2";
	}
	response.sendRedirect("news_edit.jsp?action=edit&newsId="+ News.getNewsId() + "&messageType=" + messageType + "&message=" + HTMLEncoder.encode(message));
}else{
	nc.loadFromDB();
	if(action.equals("duplicate")){
		nc.setValue("newsId", "");
	}
}
%>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<form name="mainform" method="post" action="admin/news/news_edit.jsp">
<input type="hidden" name="action" value="save">
<%
if(message != null){
%>
			<p style="color:<%=(messageType.equals("1")? "green":"red")%>"><%=message %></p>
<%
}
%>
	<table border="0" cellpadding="0" cellspacing="0" width="750">
		<tr>
			<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;NEWS SETTINGS</td>
			<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
		</tr>
		<tr>
			<td colspan="3" class="formArea2">
				<p style="color:white">
					&lt;&lt; <a href="admin/news/news_list.jsp"><%=Languages.getString("jsp.admin.news.manage",SessionData.getLanguage())%></a>
				</p>
				<table class="main details-table">
					<% nc.showFields(out,SessionData); %>
					<tr>
						<td colspan="3" align="center"><input type="submit" name="save" value="Save News"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>