<%@ page language="java" import="java.util.*,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.presentation.NewsComponent" pageEncoding="ISO-8859-1"%>
<%
int section=4;
int section_page=10;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@include file="/includes/security.jsp" %>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<jsp:useBean id="News" class="com.debisys.news.News" scope="request"/>
<jsp:setProperty name="News" property="*"/>
<%
Vector<Vector<String>> vecNews = News.getNews();
%>

            <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
              <TBODY>
              <TR>
                <TD align="left" width="1%" 
                  background="images/top_blue.gif"><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class="formAreaTitle" 
                  background="images/top_blue.gif" width="3000">&nbsp;<B class="formAreaTitle">BANNERS</B></TD>
                <TD align="right" width="1%" 
                  background="images/top_blue.gif"><IMG height="20" 
                  src="images/top_right_blue.gif" width="18"></TD></TR>
              <TR>
			  
			  <TD bgColor=#ffffff colSpan=3>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TBODY>
                    <TR>
                    
                                          <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1></TD>
             <TD>

 <%
String message = request.getParameter("message");
if(message != null){
%>
			<p style="color:red"><%=message %></p>
<%
}
%>
			<p style="color:white">
				&lt;&lt; <a href="admin/brandings/branding_options.jsp"><%=Languages.getString("jsp.admin.brandings.tool",SessionData.getLanguage())%></a>
			</p>
			<form action="admin/news/news_edit.jsp?action=add" method="post">
				<table class="">
					<tr>
					<td width="90%"></td>
						<td colspan="2" align="left"><input type="submit" value="Add News"></td>
					</tr>
				</table>
			</form>
			<table width="100%">
				<tr>
					<th class="rowhead2">TITLE</th>
					<th class="rowhead2">BODY</th>
					<th class="rowhead2">ENTER DATE</th>
					<th class="rowhead2">END DATE</th>
					<th class="rowhead2">ACTIONS</th>
				</tr>
				<%
					if (vecNews != null && vecNews.size() > 0)
						{
							Iterator<Vector<String>> it = vecNews.iterator();
							int i = 0;
							while (it.hasNext())
							{
								Vector<String> vecTemp = (Vector<String>) it.next();
								%>
									<tr class="row<%=(++i)%>">
										<td><%=vecTemp.get(1)%></td>
										<td><%=vecTemp.get(2)%></td>
										<td><%=HTMLEncoder.encode(vecTemp.get(3))%></td>
										<td><%=HTMLEncoder.encode(vecTemp.get(4))%></td>
										<td align="center">
											<a href="admin/news/news_edit.jsp?action=edit&newsId=<%=vecTemp.get(0)%>">Edit</a> &nbsp;
										</td>
									</tr>
								<%
								i %= 2; 
							}
							vecNews.clear();
						}
				%>		

				</table>

                </td>
                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	         </tr>
	        
	         
	         <tr>
		            <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
            </tr>
             </TBODY>
            </table>
        </td>
    </tr>
</table>

