<%@ page language="java" import="
                 com.debisys.utils.HTMLEncoder,
                 org.apache.commons.fileupload.servlet.ServletFileUpload,
                 com.debisys.presentation.BannerComponent" pageEncoding="ISO-8859-1"%>
<%
int section=9;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Banner" class="com.debisys.images.Banner" scope="request"/>
<jsp:setProperty name="Banner" property="*"/>
<%@include file="/includes/security.jsp" %>
<%
//String bannerId = "" + request.getParameter("bannerId");
//String iso_id = request.getParameter("iso_id");
//boolean save = (("" + request.getParameter("save")).equals("Save Banner"));

BannerComponent bc = new BannerComponent(Banner);
String action;
if (ServletFileUpload.isMultipartContent(request)) {
	bc.loadFromRequest(request, response, application, SessionData);
	if(Banner.getBannerId() == 0){
		String bid = bc.getValue("bannerId");
		if(bid != null && !bid.equals("")){
			Banner.setBannerId(Long.parseLong(bid));
		}
	}
	action = "" + bc.getValue("action");
}else{
	action = "" + request.getParameter("action");
}

String message = request.getParameter("message");
String messageType = request.getParameter("messageType");

if(action.equals("save")){
	if(bc.save()){
		message = "Banner saved successfully.";
		messageType = "1";
	}else{
		message = "Error saving Banner";
		messageType = "2";
	}
	response.sendRedirect("banner_edit.jsp?action=edit&bannerId="+ Banner.getBannerId() + "&messageType=" + messageType + "&message=" + HTMLEncoder.encode(message));
} else if (action.equals("delete")){   
    if ( bc.delete() ){
        message = "Banner delete successfully.";
        messageType = "1";
    } else{
        message = "Banner cannot delete.";
        messageType = "2";
    }
    response.sendRedirect("banners_list.jsp?messageType=" + messageType + "&message=" + HTMLEncoder.encode(message));
}
else{
	bc.loadFromDB();
	if(action.equals("duplicate")){
		bc.setValue("bannerId", "");
	}
}
%>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>
<script>
    $(document).ready(function () {
        
        $("#selectAll").bind("click", function (e) {
            
            var textSelect = $("#controlSelectAll").html();
            var isCheckedMaster = ($(this).is(':checked'));
            if (isCheckedMaster){
                $("#controlSelectAll").html("Unselect all Iso's");                
            } else{
                $("#controlSelectAll").html("Select all Iso's");                                
            }   
            
            $( "[type=checkbox]" ).each(function(index) {
                var isChecked = ($(this).is(':checked'));
                //var value = $( this )[0].value;
                if ( $(this)[0].name==="RepId"){
                    console.log(index+" === " +isChecked);
                    if ( isCheckedMaster ){
                        $(this).prop( "checked", true );
                    }
                    else{
                        $(this).prop( "checked", false );
                    }
                }
            });
        
        });        
        
        
    });
    
</script>
<form name="mainform" method="post" action="admin/banners/banner_edit.jsp" enctype="multipart/form-data">
	<input type="hidden" name="action" value="save">
	<table border="0" cellpadding="0" cellspacing="0" width="750">
		<tr>
			<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;BANNER SETTINGS</td>
			<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
		</tr>
		<tr>
			<td colspan="3" class="formArea2">
				<p style="color:white">
					&lt;&lt; <a href="admin/banners/banners_list.jsp"><%=Languages.getString("jsp.admin.banners.manage",SessionData.getLanguage())%></a>
				</p>
<%
if(message != null){
%>
			<h2 style="color:<%=(messageType.equals("1")? "green":"red")%>"><%=message %></h2>
<%
}
%>
				<table class="main details-table">
					<% bc.showFields(out, SessionData); %>
					<tr>
						<td colspan="1" align="center"><input type="submit" name="save" value="Save Banner"></td>
                                                <td colspan="3" align="center">
                                                    <span id="controlSelectAll">Select All</span>
                                                    <input type="checkbox" id="selectAll" name="selectAll" value="Select All"></td>
                                                
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
