<%@ page language="java" import="java.util.*,
         com.debisys.utils.HTMLEncoder,
         com.debisys.presentation.BrandingComponent" pageEncoding="ISO-8859-1"%>
<%
    int section = 9;
    int section_page = 18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@include file="/includes/security.jsp" %>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<jsp:useBean id="Banner" class="com.debisys.images.Banner" scope="request"/>
<jsp:setProperty name="Banner" property="*"/>
<%
    Vector<Vector<String>> vecBanners = Banner.getBanners();
%>
<script >

    $(document).ready(function () {

        $("#updateButtonBanners").bind("click", function (e) {
            //iterate
            var bannersToupdate = "";
            $( ".prioEntity" ).each(function( index ) {
                var value = $( this )[0].value;
                if ( value>0 || value.length===0 ){
                    if (value.length===0){
                        value="0";                        
                    }
                    var id = $( this )[0].id;                    
                    bannersToupdate = bannersToupdate + id + "|"+value+"-";
                }      
            });
            bannersToupdate = "param="+bannersToupdate;           
            var url = "admin/banners/bannerUtil.jsp";            
            $.ajax({
                type: "POST",
                url: url,
                data: bannersToupdate,
                success: function (response) {
                    $("#messageH2").html("Priority change sucessfully!!");
                    
                }, error: function (error) {
                    console.log(error);               
                }
            });
            
        });

    });

    function validateKeypress(validChars) {
        var keyChar = String.fromCharCode(event.which || event.keyCode);        
        var pattern = new RegExp(validChars, "i");
        return pattern.test(keyChar) ? keyChar : false;

    }
</script>
<TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
    <TBODY>
        <TR>
        <TD align="left" width="1%" 
            background="images/top_blue.gif"><IMG height=20 
                                              src="images/top_left_blue.gif" width=18></TD>
        <TD class="formAreaTitle" 
            background="images/top_blue.gif" width="3000">&nbsp;<B class="formAreaTitle">BANNERS</B></TD>
        <TD align="right" width="1%" 
            background="images/top_blue.gif"><IMG height="20" 
                                              src="images/top_right_blue.gif" width="18"></TD></TR>
        <TR>

        <TD bgColor=#ffffff colSpan=3>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                <TBODY>
                    <TR>

                    <TD width=1 bgColor=#003082><IMG 
                            src="images/trans.gif" width=1></TD>
                    <TD>
                        <p style="color:white">
                            &lt;&lt; <a href="admin/brandings/branding_options.jsp"><%=Languages.getString("jsp.admin.brandings.tool", SessionData.getLanguage())%></a>
                        </p>

                        <%
                            
                            String messageType = request.getParameter("messageType");
                            String styleMessage = "color:red";
                            if ( messageType != null && messageType.equals("1")){
                                styleMessage = "color:green";
                            }
                            String message = request.getParameter("message");
                            if (message == null) {
                                message = "";
                            }
                        %>
                        <h2 id="messageH2" style="<%=styleMessage%>"><%=message%></h2>
                        <%
                            
                        %>
                        <form action="admin/banners/banner_edit.jsp?action=add" method="post">
                            <table >
                                <tr>
                                <td width="90%"></td>
                                <td colspan="4" align="left"><input type="submit" value="Add Banner"></td>
                                </tr>
                            </table>
                        </form>
                        <table>
                            <tr>
                            <td colspan="8" align="right"><input type="button" id="updateButtonBanners" value="Update"></td>
                            </tr>
                            <tr>
                            <th class="rowhead2">DESCRIPTION</th>
                            <th class="rowhead2">LINK</th>
                            <th class="rowhead2">IMAGE FILE</th>
                            <th class="rowhead2">DISPLAYTIME</th>
                            <th class="rowhead2">ENTER DATE</th>
                            <th class="rowhead2">END DATE</th>
                            <th class="rowhead2">PRIORITY</th>
                            <th class="rowhead2">ACTIONS</th>
                            </tr>
                            <%
                                if (vecBanners != null && vecBanners.size() > 0) {
                                    Iterator<Vector<String>> it = vecBanners.iterator();
                                    int i = 0;
                                    while (it.hasNext()) {
                                        Vector<String> vecTemp = (Vector<String>) it.next();
                                        String prio = vecTemp.get(7);
                                        if (prio.equals("0")){
                                            prio="";
                                        }
                            %>
                            <tr class="row<%=(++i)%>">
                            <td><%=HTMLEncoder.encode(vecTemp.get(5))%></td>
                            <td><%=HTMLEncoder.encode(vecTemp.get(4))%></td>
                            <td><%=vecTemp.get(6)%></td>
                            <td><%=vecTemp.get(1)%></td>
                            <td><%=vecTemp.get(2)%></td>
                            <td><%=vecTemp.get(3)%></td>
                            <td><input id="<%=vecTemp.get(0)%>" class="prioEntity" type="text" size="5" maxlength="5" value="<%=prio%>"
                                       onkeypress="return validateKeypress('[0-9]');"/>                                                                                    
                            </td>
                            <td align="center">
                                <a href="admin/banners/banner_edit.jsp?action=edit&bannerId=<%=vecTemp.get(0)%>">Edit</a> &nbsp;
                                <a href="admin/banners/banner_edit.jsp?action=delete&bannerId=<%=vecTemp.get(0)%>">Delete</a> &nbsp;
                            </td>
                            </tr>
                            <%
                                        i %= 2;
                                    }
                                    vecBanners.clear();
                                }
                            %>

                        </table>

                    </td>
                    <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
                    </tr>


                    <tr>
                    <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
                    </tr>
                </TBODY>
            </table>
        </td>
        </tr>
</table>

