<%@ page import="java.util.Hashtable,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.utils.DebisysConfigListener,
				com.debisys.properties.Properties,
                 java.net.URLEncoder,
                 com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:setProperty name="User" property="*"/>
<%
int section = 1;
int section_page = 1;
String strDistChainType = "";
String strAccessLevel = "";
String instance = DebisysConfigListener.getInstance(application);
//Languages.getString("jsp.index.username",SessionData.getLanguage())
%>
<html>
	<head>
		<title>Debisys</title>
		
	</head>
	<body bgcolor="#ffffff"><br>
		<table cellspacing="0" width="100%" cellpadding="0" border="0">
			<tr>
				<td valign="top">
					<table  align="left" width="99%" cellpadding="5" cellspacing="5" border="0"  class="ControllerStyleClass" >
						<tr align="left">
							<td valign="top" class="ContentStyleClass" style="background-color:#ffffff">
								<br>
								<table border="0" cellpadding="0" cellspacing="0" width="765">
									<tr>
										<td background="top_blue.gif" width="1%" align="left"><img src="top_left_blue.gif" width="18" height="20"></td>
										<td background="top_blue.gif" width="1%" align="right"><img src="top_right_blue.gif" width="18" height="20"></td>
									</tr>
									<tr>
										<td colspan="3" class="formArea" style="font-size:15px">
											<h1><br></h1><h1>PC Terminal Certificate Enrollment</h1>
											<p>
												Welcome to the PC Terminal certificate enrollment page!
												<ul>
													<li>In order to install your digital certificate, you must have the following on your terminal:</li>
														
														<ol style="border:1px solid black; padding:30px;margin: 20px; width: 500px">
															<strong>Required:<br><br></strong>
															
															<li>Windows XP Service Pack 2. If you do not have Service Pack 2 installed on your computer, please upgrade your system before you attempt to install the certificate. (approximate install time 20-30 minutes)</li><br>
															<li>Microsoft .Net Framework 2. If you do not have Microsoft Framework 2, and you attempt to install your certificate, the certificate installer will notify you that Framework 2 is needed and will immediately download Framework 2 for you.  (approximate download time 5-10 minutes)</li>
														</ol>
														</ul>
														Once your computer is equipped with the above required items, you are ready to install your digital certificate security.<br>
													<ul>
													<li>Please download your certificate installer here: <a style="font-weight:bold; font-size:14px" href="<%=Properties.getPropertyValue(instance, "global","downloadCertUrlserver")%>">Download Cert Installer</a>.</li>
													</ul>
													<br>After your download is finished please follow the instructions below:<br><br>
													<ol>
													<li>Please click on the Certificate Installer Icon of the file downloaded to begin the installation process.</li><br>
													
													<center><img src="emida_cert_inst1.png"><br><br></center>
													<li>After you click on the icon, you will be taken to the Certificate Setup Wizard (see image below).</li><br><br>
													<center><img src="emida_cert_inst2.png"></center><br><br>
													<li>Click the <strong>&quot;Next&quot;</strong> button and then the <strong>&quot;Finish&quot;</strong> button. You will then be asked to enter your PC terminal username and password. See image below.</li><br><br>
													<center><img src="emida_cert_inst3.png"></center><br><br>
													<li>That will complete the installation process. You will see a message notifying you the installation is complete and a PC Terminal Icon has been placed on your desktop (see image below).<br><br>
													Click on the PCterminal icon on your desktop to go immediately to your PC Terminal.<br><br> </li><br>
													<center><img src="emida_cert_inst4.png"></center><br><br>
												</ol>
											</p>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<script src=" https://ssl.google-analytics.com/urchin.js" type="text/javascript"></script>
		<script type="text/javascript">
			_uacct = "UA-742998-2";
			urchinTracker();
		</script>
	</body>
</html>