<html>
	<head>
		<title>Debisys</title>
		
	</head>
	<body bgcolor="#ffffff"><br>
		<table cellspacing="0" width="100%" cellpadding="0" border="0">
			<tr>
				<td valign="top">
					<table  align="left" width="99%" cellpadding="5" cellspacing="5" border="0"  class="ControllerStyleClass" >
						<tr align="left">
							<td valign="top" class="ContentStyleClass" style="background-color:#ffffff">
								<br>
								<table border="0" cellpadding="0" cellspacing="0" width="765">
									<tr>
										<td background="top_blue_1.gif" width="1%" align="left"><img src="top_left_blue.gif" width="18" height="20"></td>
										<td background="top_blue_1.gif" width="1%" align="right"><img src="top_right_blue.gif" width="18" height="20"></td>
									</tr>
									<tr>
										<td colspan="3" class="formArea" style="font-size:15px">
											<h1><br></h1><h1>PC Terminal - Descarga de Certificados</h1>
											<p>
												Bienvenido a la pagina de descarga de certificados del PCterminal!
												<ul>
													<li>
													Para instalar su certificado digital usted debe tener lo siguiente en su computador:
													</li>
														
														<ol style="border:1px solid black; padding:30px;margin: 20px; width: 500px">
															<strong>Requerimientos m�nimos:<br><br></strong>
															
															<li>Windows XP Service Pack 2. Si no tiene Service Pack 2 instalado en su computador por favor actualice su sistema antes de intentar instalar su certificado. (Tiempo de instalaci�n aproximado 20-30 minutos) </li><br>
															<li>Microsoft .Net Framework 2. Si usted no tiene Microsoft Framework 2, e intenta instalar su certificado, el instalador de certificados le notificar� que Framework 2 es necesario e inmediatamente intentar� hacer la instalaci�n por usted.  (Tiempo aproximado de descarga 5-10 minutos)</li>
														</ol>
														</ul>
														Una vez que su computador est� equipado con estos requerimientos usted estar� listo para instalar sus certificados de seguridad.<br>
													<ul>
													<li>Por favor descargue su instalador de certificados haciendo clic aqu�: <a style="font-weight:bold; font-size:14px" href="http://certs.emidacol.net:8080//certinstaller//EmidaCertificateInstaller.exe">Descargar Instalador de Certificados</a>.</li>
													</ul>
													<br>Cuando su descarga haya terminado por favor siga las instrucciones:<br><br>
													<ol>
													<li>Inicie el programa CertificateInstaller haciendo doble clic en el �cono del programa descargado o simplemente corriendo el ejecutable cuando termine de descargar.</li><br>
													
													<center><img src="Logo.PNG"><br><br></center>
													<li>Tras iniciar el programa usted recibir� la guia de instalaci�n por pasos (ver imagen abajo). Presione en el bot�n <strong>&quot;Next&quot;</strong> </li><br><br>
													<center><img src="Instalador_EC_2.PNG"></center><br><br>
													<li>Siga los pasos para que el proceso se ejecute y para terminar presione el bot�n  <strong>&quot;Finish&quot;</strong>.</li><br><br>
													<center><img src="Instalador_EC_3.PNG"></center><br><br>
													<li>Un mensaje aparecer� en pantalla para empezar con la instalaci�n correspondiente. Presione el bot�n <strong>&quot;OK&quot;</strong> </li><br><br>
													<center><img src="Instalador_EC_4.PNG"></center><br><br>
													<li>Suministre el nombre de usuario y contrase�a asignado por su Distribuidor. Al finalizar presione el bot�n <strong>&quot;Solicitar certificado cliente&quot;</strong></li><br><br>
													<center><img src="Instalador_EC_5.PNG"></center><br><br>
													<li>Con esto completar� el proceso de instalaci�n. Ver� el mensage notificando que la instalaci�n ha terminado exitosamente. El icono de acceso a PC Terminal aparecer� en su escritorio (ver imagen a continuaci�n).<br><br>
													<center><img src="Instalador_EC_6.PNG"></center><br><br>
													Haga doble clic en el icono de PCterminal en el escritorio para empezar a utilizar su terminal.<br><br> </li><br>
													<center><img src="Instalador_EC_7.PNG"></center><br><br>
												</ol>
											</p>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<script src=" https://ssl.google-analytics.com/urchin.js" type="text/javascript"></script>
		<script type="text/javascript">
			_uacct = "UA-742998-2";
			urchinTracker();
		</script>
	</body>
</html>