<%@ page import="java.util.Hashtable,
                 com.debisys.utils.DebisysConstants,
                 com.debisys.utils.DebisysConfigListener,
				com.debisys.properties.Properties,
                 java.net.URLEncoder,
                 com.debisys.languages.Languages"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:setProperty name="User" property="*"/>
<%
int section = 1;
int section_page = 1;
String strDistChainType = "";
String strAccessLevel = "";
String instance = DebisysConfigListener.getInstance(application);
//Languages.getString("jsp.index.username",SessionData.getLanguage())
%>
<html>
	<head>
		<title>Debisys</title>
		
	</head>
	<body bgcolor="#ffffff"><br>
		<table cellspacing="0" width="100%" cellpadding="0" border="0">
			<tr>
				<td valign="top">
					<table  align="left" width="99%" cellpadding="5" cellspacing="5" border="0"  class="ControllerStyleClass" >
						<tr align="left">
							<td valign="top" class="ContentStyleClass" style="background-color:#ffffff">
								<br>
								<table border="0" cellpadding="0" cellspacing="0" width="765">
									<tr>
										<td background="top_blue.gif" width="1%" align="left"><img src="top_left_blue.gif" width="18" height="20"></td>
										<td background="top_blue.gif" width="1%" align="right"><img src="top_right_blue.gif" width="18" height="20"></td>
									</tr>
									<tr>
										<td colspan="3" class="formArea" style="font-size:15px">
											<h1><br></h1><h1>PC Terminal - Descarga de Certificados</h1>
											<p>
												Bienvenido a la pagina de descarga de certificados del PCterminal!
												<ul>
													<li>
													Para instalar su certificado digital usted debe tener lo siguiente en su computador:
													</li>
														
														<ol style="border:1px solid black; padding:30px;margin: 20px; width: 500px">
															<strong>Requerimientos m�nimos:<br><br></strong>
															
															<li>Windows XP Service Pack 2. Si no tiene Service Pack 2 instalado en su computador por favor actualice su sistema antes de intentar instalar su certificado. (Tiempo de instalaci�n aproximado 20-30 minutos) </li><br>
															<li>Microsoft .Net Framework 2. Si usted no tiene Microsoft Framework 2, e intenta instalar su certificado, el instalador de certificados le notificar� que Framework 2 es necesario e inmediatamente intentar� hacer la instalaci�n por usted.  (Tiempo aproximado de descarga 5-10 minutos)</li>
														</ol>
														</ul>
														Una vez que su computador este equipado con estos requerimientos usted estar� listo para instalar sus certificados de seguridad.<br>
													<ul>
													<li>Por favor descargue su instalador de certificados haciendo click aqu�: <a style="font-weight:bold; font-size:14px" href="<%=Properties.getPropertyValue(instance, "global","downloadCertUrlserver")%>">Descargar Instalador de Certificados</a>.</li>
													</ul>
													<br>Cuando su descarga se halla terminado por favor siga las instrucciones:<br><br>
													<ol>
													<li>Inicie el programa EmidaCertificateInstaller haciendo doble clic en el icono del programa descargado, esto iniciar� la instalaci�n.</li><br>
													
													<center><img src="emida_cert_inst1.png"><br><br></center>
													<li>Tras iniciar el programa usted recibir� a la guia de instalaci�n por pasos (ver imagen abajo).</li><br><br>
													<center><img src="emida_cert_inst2.png"></center><br><br>
													<li>Haga Clic en el bot�n <strong>&quot;Siguiente&quot;</strong> y a continuaci�n Clic en el bot�n  <strong>&quot;Finalizar&quot;</strong>. Un mensaje aparecer� en pantalla pidiendo que ingrese su nombre de usuario y contrase�a. Ver imagen. Haga clic en el boton Instale su Certificado de Cliente</li><br><br>
													<center><img src="emida_cert_inst3.png"></center><br><br>
													<li>Con esto completar� el proceso de instalaci�n. Ver� el mensage notificando que la instalaci�n ha terminado exitosamente. El icono de acceso a PC Terminal aparecer� en su escritorio (ver imagen a continuaci�n).<br><br>
													Haga doble clic en el icono de PCterminal en el escritorio para empezar a utilizar su terminal.<br><br> </li><br>
													<center><img src="emida_cert_inst4.png"></center><br><br>
												</ol>
											</p>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<script src=" https://ssl.google-analytics.com/urchin.js" type="text/javascript"></script>
		<script type="text/javascript">
			_uacct = "UA-742998-2";
			urchinTracker();
		</script>
	</body>
</html>