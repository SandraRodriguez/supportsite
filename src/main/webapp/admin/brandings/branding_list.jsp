<%@ page language="java" import="java.util.*,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.presentation.BrandingComponent" pageEncoding="ISO-8859-1"%>
<%
int section=9;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@include file="/includes/security.jsp" %>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<jsp:useBean id="Branding" class="com.debisys.brandings.Branding" scope="request"/>
<jsp:setProperty name="Branding" property="*"/>

 <script>
     
        
        function createNewBranding(){
            var brandingName = $("#newBrandingName").val();
            if ( brandingName.length===0){
                $("#newBrandingName").focus();                           
            } else{
                $.post("admin/brandings/utilBranding.jsp", 
                    { 
                        action: "createEmptyBranding", 
                        name: brandingName,                                
                        Random: Math.random()
                    },
                    function( data ) {
                        var result = data.trim();
                        if (result === "0"){
                            var url = "admin/brandings/branding_edit.jsp?action=edit&customConfigId="+brandingName;
                            $("#brandingFormList").attr("action", url);
                            $("#brandingFormList").submit();
                        } else if (result === "2"){
                            alert("The Name already exist!");
                            $("#newBrandingName").focus();
                        }
                    }
                );                
            }                
        }
    </script>     

<%
Vector<Vector<String>> vecBrandings = Branding.getBrandings(SessionData);
%>

            <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
              <TBODY>
              <TR>
                <TD align=left width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=images/top_blue.gif width="3000">&nbsp;<B><%=Languages.getString("jsp.admin.brandings.tool",SessionData.getLanguage()).toUpperCase()%></B></TD>
                <TD align=right width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_right_blue.gif" width=18></TD></TR>
                  <TR>
                <TD colSpan=3>
                  <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
                  bgColor=#fffcdf class="backceldas">
                    <TBODY>
                    <TR class="backceldas">
                      <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top >
                      			<p style="color:white">
				&lt;&lt; <a href="admin/brandings/branding_options.jsp"><%=Languages.getString("jsp.admin.brandings.tool",SessionData.getLanguage())%></a>
			</p>
			<%
String message = request.getParameter("message");
if(message != null){
%>
			<p style="color:red"><%=message %></p>
<%
}
%>
			<form id="brandingFormList" name="brandingFormList" action="admin/brandings/branding_edit.jsp?action=add" method="post">
				<table class="">
					<tr>
                                            <td width="90%"></td>
                                            <td colspan="1" align="left">
                                                    <input type="button" value="Add Branding" onclick="createNewBranding();" >
                                            </td>
                                            <td colspan="1" align="left">
                                                <input type="text"  size="60" id="newBrandingName" name="newBrandingName" 
                                                       placeholder="PUT HERE THE NEW BRANDING NAME">                                                    
                                            </td>
					</tr>
				</table>
			</form>
			<table class="list-table main">
				<tr>
					<th class="rowhead2">Name</th>
					<th class="rowhead2">Actions</th>
				</tr>
				<%
				if (vecBrandings != null && vecBrandings.size() > 0)
				{
					Iterator<Vector<String>> it = vecBrandings.iterator();
					int i = 0;
					while (it.hasNext())
					{
						Vector<String> vecTemp = (Vector<String>) it.next();
						%>
							<tr class="row<%=(++i)%>">
								<td><%=HTMLEncoder.encode(vecTemp.get(1))%></td>
								<td align="center">
									<a href="admin/brandings/branding_edit.jsp?action=edit&customConfigId=<%=vecTemp.get(0)%>">Edit</a> &nbsp;
									<a href="admin/brandings/branding_assign.jsp?customConfigId=<%=vecTemp.get(0)%>">Assign</a>
								</td>
							</tr>
						<%
						i %= 2; 
					}
					vecBrandings.clear();
				}
				%>		
			</table>
                </td>
                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	         </tr>
	         <tr>
		            <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
            </tr>
            </table>
        </td>
    </tr>
</table>
    
