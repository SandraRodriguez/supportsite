<%@ page import="com.debisys.customers.CustomerSearch,
                 java.util.HashMap,
                 java.util.Vector,
                 java.util.Iterator,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.brandings.Branding"%>
<%
int section=13;
int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="Branding" class="com.debisys.brandings.Branding" scope="request"/>
<jsp:setProperty name="Branding" property="*"/>

<%@ include file="/includes/security.jsp"%>
<%
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;
String level = request.getParameter("level");
if(request.getParameter("assign") != null && request.getParameter("data") != null  && level != null){
	String assign = request.getParameter("assign");
	String data = "" + request.getParameter("data");
	Branding.setCustomConfigId(assign);
	if(assign != null && level.length() > 0){
		String result = Branding.assignBranding(SessionData, data, level);
		%><%=result%><%
	}
	
}else{
Vector vecBrandings = Branding.getISOBrandings(SessionData);
%>
<%@ include file="/includes/header.jsp"%>
<script type="text/javascript" src="/support/includes/jquery.js"></script>
<script>
	$(document).ready(function (){
		$(':checkbox').click(function(){$('#savechanges').show();});
		$(':checkbox').change(function(){$('#savechanges').show();});
	});
	function saveBrandings(){
		var opts = document.getElementsByName('rep_branding');
		if(opts && opts.length > 0){
			var ch = '', unch = '';
			for(var i=0; i<opts.length; i++){
				if(opts[i].checked){
					if(ch != ''){
						ch += '|';
					}
					ch += opts[i].value;
				}
			}
			var ts = '&rand=' + Math.round(new Date().getTime() / 1000) + Math.random();
			var custom = '<%=Branding.getCustomConfigId()%>';
			custom = escape(custom);
			var url =  'admin/brandings/assig_brandings.jsp'; 
                        //?assign='+custom+'&level=<%=level%>&data=|'+ ch +'|'+ ts;	
			$("#resp").load(url, 
                            { 
                                assign: custom, 
                                level: <%=level%>,
                                data: '|'+ ch +'|',
                                rand: $("#txtSort").val(),                                
                                Random: Math.round(new Date().getTime() / 1000) + Math.random()
                            },
                            handleResponse);
		}
	}
	function handleResponse(responseText, textStatus, XMLHttpRequest){
            var responseseCodes = responseText.match("OK");
            if( responseseCodes[0] === "OK"){
                $("#savechanges").hide();
            }else{
                alert("Error!");
            }
	}
	function showHelp(){
		var p = window.open("", "helpwindow", "width=400,height=210,scrollbars=yes");
		p.document.write('<head><title><%=Languages.getString("jsp.admin.brandings.instructions",SessionData.getLanguage())%></title></head>');
		p.document.write("<%=Languages.getString("jsp.admin.brandings.helpassign",SessionData.getLanguage())%>");
	}
function selectAllProducts(bVal)
{ 
	var node_list = document.getElementsByName('rep_branding');
	var items = node_list.length;
	for (var i = 0; i < items; i++) 
	{
		var node = node_list[i];
		if (node.getAttribute('type') == 'checkbox'  && node.getAttribute('name') != 'masterdelele') 
		{  
			var uname = node.getAttribute('name');
			if ( uname.indexOf("rep_branding") != -1)
			{
				node.checked  = bVal;
			}
		}
	}
}
function deSelectMaster()
{
	var node = document.getElementById('masterasign');
	node.checked  = false;
}
function selectMaster(bVal)
{
	var master = document.getElementById('masterasign');
	var allSellected = true;
	if (!bVal){
		master.checked  = false;
	}else{
		var node_list = document.getElementsByName('rep_branding');
		var items = node_list.length;
		for (var i = 0; i < items; i++) 
		{
			var node = node_list[i];
			if (!node.checked){
				allSellected = false;
			}
		}
		if(allSellected){
			master.checked  = true;
		}
	}
}
</script>

 <TABLE cellSpacing=0 cellPadding=0 width=750px border=0>
    <TBODY>
    <%
level = request.getParameter("level");
if(level == null || level.equals("") || level.equals("null")){
	level = SessionData.getProperty("default_level");
}
if(level != null && !level.equals("") && !level.equals("null")){
%>
   <TR>
     <TD align=left width="1%" 
     background=images/top_blue.gif><IMG height=20 
       src="images/top_left_blue.gif" width=18></TD>
     <TD class=formAreaTitle 
     background=images/top_blue.gif width="4000">&nbsp;<B><%=Languages.getString("jsp.admin.brandings.assign.title",SessionData.getLanguage()).toUpperCase()+ " " + SessionData.getProperty("company_name").toUpperCase()%></B></TD>
     <TD align=right width="1%" 
     background=images/top_blue.gif><IMG height=20 
       src="images/top_right_blue.gif" width=18></TD></TR>
       <TR>
     <TD colSpan=3>
       <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
       bgColor=#fffcdf class="backceldas">
         <TBODY>
         <TR class="backceldas">
           <TD width=1 bgColor=#003082><IMG 
             src="images/trans.gif" width=1></TD>
           <TD vAlign=top align=middle bgColor=#ffffff>
             <TABLE width="100%" border=0 
             align=center cellPadding=2 cellSpacing=0 class="backceldas">
               <TBODY>
               <TR>
                 <TD width=18 bgColor=#ffffff>&nbsp;</TD>
                 <TD class=main colspan="3"><BR>
                 
			<form name="entityform" method="get" action="admin/brandings/assig_brandings.jsp">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
					<tr>
						<td class="formArea2">
							<br>
							<table border="0">
								<tr class="main">
									<td nowrap="nowrap" valign="top" colspan="3">
										<a style="font-weight:bold;" href="javascript:showHelp();"><%=Languages.getString("jsp.admin.brandings.instructions",SessionData.getLanguage())%></a>
									</td>
								</tr>
								<tr class="main">
									<td nowrap="nowrap" valign="top" colspan="3">
										<%=Languages.getString("jsp.admin.brandings.selectbranding",SessionData.getLanguage())%> 
										<%
											if(level.toString().equals(DebisysConstants.ISO)){
												%><%=Languages.getString("jsp.includes.menu.iso",SessionData.getLanguage())%><%
											}
											if(level.toString().equals(DebisysConstants.AGENT)){
												%><%=Languages.getString("jsp.includes.menu.agents",SessionData.getLanguage())%><%
											}
											if(level.toString().equals(DebisysConstants.SUBAGENT)){
												%><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%><%
											}
											if(level.toString().equals(DebisysConstants.REP)){
												%><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%><%
											}
										%>
										:
									</td>
								</tr>
								<tr class="main">
									<td valign="top" nowrap="nowrap" colspan="1">
										<select id="customConfigId" name="customConfigId">
											<option value=""></option>
										<%
										if (vecBrandings != null && vecBrandings.size() > 0)
										{
											Iterator it = vecBrandings.iterator();
											while (it.hasNext())
											{
												Vector vecTemp = (Vector) it.next();
												String selected ="";
												if (vecTemp.get(0).toString().equals(Branding.getCustomConfigId())){
													selected = "selected";
												}
												out.println("<option value=\""+vecTemp.get(0).toString()+"\" "+ selected + ">" + HTMLEncoder.encode(vecTemp.get(1).toString()) + "</option>");
											}
											vecBrandings.clear();
										}
										%>
										</select>
									</td>
									<td>
										<select name="level">
											<%
											String sel = "";
											if (strAccessLevel.equals(DebisysConstants.ISO)){//ISO
												if (strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL)){//ISO 5 LEVELS
													if(level.equals(DebisysConstants.AGENT)){sel = "selected";}else{sel="";};
													%><option value="<%=DebisysConstants.AGENT%>" <%=sel%>><%=Languages.getString("jsp.includes.menu.agents",SessionData.getLanguage())%></option><%
													if(level.equals(DebisysConstants.SUBAGENT)){sel = "selected";}else{sel="";};
													%><option value="<%=DebisysConstants.SUBAGENT%>" <%=sel%>><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%></option><%
												}
											}else if (strAccessLevel.equals(DebisysConstants.AGENT)){//AGENT
												if(level.equals(DebisysConstants.SUBAGENT)){sel = "selected";}else{sel="";};
												%><option value="<%=DebisysConstants.SUBAGENT%>" <%=sel%>><%=Languages.getString("jsp.includes.menu.sub_agents",SessionData.getLanguage())%></option><%
											}
											if(level.equals(DebisysConstants.REP)){sel = "selected";}else{sel="";};
											%><option value="<%=DebisysConstants.REP%>" <%=sel%>><%=Languages.getString("jsp.includes.menu.representatives",SessionData.getLanguage())%></option>
										</select>
									</td>
									<td valign="top" align="left">
										<input type="hidden" name="search" value="y">
										<input type="hidden" name="rand" value="<%=Math.random()%>">
										<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
									</td>
									<td valign="top" align="left">
										<input type="button" id="savechanges" style="display:none;" value="<%=Languages.getString("jsp.admin.savechanges",SessionData.getLanguage())%>" onclick="saveBrandings();">
										<span id="resp" style="display:none;"></span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
			<table>
			
<%
	Vector vecSearchResults = null;
	if (request.getParameter("search") != null){
		if (request.getParameter("search").equals("y")){
			vecSearchResults = Branding.getIsoReps(SessionData, level);
		}
	}

	
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
		%>
		<tr>
			<td colspan="3">
				<table border="0" cellpadding="1" cellspacing="1" width="100%">
					<tr>
						<td class="rowhead2" width="20"></td>
						<td class="rowhead2" width="200"><%=Languages.getString("jsp.admin.brandings.grid.businessname",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" width="100"><%=Languages.getString("jsp.admin.brandings.grid.repId",SessionData.getLanguage()).toUpperCase()%></td>
						<td class="rowhead2" width="100" align="center"><input type="checkbox" id="masterasign" name="masterasign" onclick="selectAllProducts(this.checked)" checked="checked"><%=Languages.getString("jsp.admin.brandings.grid.selectall",SessionData.getLanguage()).toUpperCase()%></td>
					</tr>
	<%
		Boolean allSelected = true;
		Iterator it = vecSearchResults.iterator();
		int i = 0;
		while (it.hasNext())
		{
			i++;
			Vector vecTemp = (Vector) it.next();
			String selected ="";
	%>
					<tr class="row<%=((i%2)+1)%>">
						<td width="20"><%=i%></td>
						<td nowrap="nowrap" style="width:200;"><%=vecTemp.get(0)%></td>
						<td nowrap="nowrap" style="width:150;"><%=vecTemp.get(1)%></td>
						<td align="center" style="width:50; max-width:50;height:24px;">
							<input id="rep_branding" name="rep_branding" value="<%=vecTemp.get(1)%>"<%
							if(vecTemp.get(2).equals("1")){
								%>checked="checked" <%
							}else{
								allSelected = false;
							}
							%> type="checkbox" onClick="selectMaster(this.checked);">
						</td>
					</tr>
	
	<%		
		}
		vecSearchResults.clear();
		if (!allSelected){
	%>

					<script>
						deSelectMaster();
					</script>
	<%	} 
	%>

				</table>
			</td>
		</tr>
	<%
	}
}else{
	%><tr><td colspan="3"><%=Languages.getString("jsp.admin.brandings.selectalevel",SessionData.getLanguage())%></td></tr><%
}
%>
</table>
			
                 
                 
                 
                                                   </table>
                 
                </td>
                   <td width="18" bgColor=#ffffff>&nbsp;</td>
                   
               </tr>
               </table>
                     <div align=right class="backceldas"><font size="1"></div>
             </td>

             <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
       </tr>
       <tr>
           <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
         </tr>
         </table>
        </td>
    </tr>
</table>


 <%
}%>                
                 
                 


<%@ include file="/includes/footer.jsp"%>
