<%@ page language="java" import="java.util.*,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.presentation.BrandingComponent" pageEncoding="ISO-8859-1"%>
<%
int section=9;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Branding" class="com.debisys.brandings.Branding" scope="request"/>
<jsp:setProperty name="Branding" property="*"/>
<%@include file="/includes/security.jsp" %>
<%
String iso_id = request.getParameter("iso_id");
boolean assign = (("" + request.getParameter("assign")).equals("1"));
if(iso_id != null && !iso_id.equals("")){
	if(Branding.saveAssignment(iso_id, assign)){
		%>ok<%
	}else{
		%>error<%
	}
}else{
%>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<%
Vector<Vector<String>> vecBrandings = Branding.getAllISOs(SessionData);
%>
<script>
	$(document).ready(function (){
		$("input[type=checkbox]").click(function(){
			var url = "admin/brandings/branding_assign.jsp?customConfigId=<%=Branding.getCustomConfigId()%>&assign="+ ($(this).is(':checked')?1:0) + "&iso_id=" + $(this).val() + "&s=" + (new Date().getTime()) + Math.random();
			$.post(url, function(data) {
				data = jQuery.trim(data);
				if(data != "ok"){
					alert("Error trying to assign ISO to branding");
				}
			});
		});
	});
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;ASSING ISOS TO BRANDING</td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
	<tr>
		<td colspan="3" class="formArea2">
			<p style="color:white">
				&lt;&lt; <a href="admin/brandings/branding_list.jsp"><%=Languages.getString("jsp.admin.brandings.manage",SessionData.getLanguage())%></a>
			</p>
			<p>
				** <%=Languages.getString("jsp.admin.brandings.tip1",SessionData.getLanguage())%>
			</p>
<%
String message = request.getParameter("message");
String messageType = request.getParameter("messageType");
if(message != null){
%>
			<p style="color:<%=(messageType.equals("1")? "green":"red")%>"><%=message %></p>
<%
}
%>
			<table class="list-table main">
				<tr>
					<th>ID</th>
					<th>Business Name</th>
					<th>Assigned to <%=Branding.getCustomConfigId()%></th>
				</tr>
				<%
				if (vecBrandings != null && vecBrandings.size() > 0)
				{
					Iterator<Vector<String>> it = vecBrandings.iterator();
					int i = 0;
					while (it.hasNext())
					{
						Vector<String> vecTemp = (Vector<String>) it.next();
						String selected ="";
						if (vecTemp.get(0).toString().equals(Branding.getCustomConfigId())){
							selected = "selected"; 
						}
						out.println("<tr class=\"row"+ (++i) +"\"><td>" + vecTemp.get(0) + "</td><td>" + HTMLEncoder.encode(vecTemp.get(1)) + "</td>");
						out.println("<td><input type=\"checkbox\" value=\""+ vecTemp.get(0) +"\" "+ (vecTemp.get(2).equals("true")?"checked=\"checked\"":"") +"/></td></tr>");
						i %= 2; 
					}
					vecBrandings.clear();
				}
				%>		
			</table>
		</td>
	</tr>
</table>
<% } %>