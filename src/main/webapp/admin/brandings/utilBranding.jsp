<%@page import="com.debisys.brandings.Branding"%>
<%
    String responseMessage="-1";
    String action = request.getParameter("action");
       
    if (action.equalsIgnoreCase("createEmptyBranding")){
        String name = request.getParameter("name");
        com.debisys.brandings.Branding branding = new Branding();
        branding.setCustomConfigId(name.toUpperCase());
        branding.setName(name.toLowerCase());
        branding.setURL("");
        branding.setSecureURL("");            
        if (branding.existsBrading(name)){
            responseMessage="2";
        } else{
            branding.save();
            responseMessage="0";
        }                
    }    
    out.print(responseMessage);
%>
