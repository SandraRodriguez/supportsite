<%@ page language="java" import="
	com.debisys.utils.HTMLEncoder,
	com.debisys.presentation.BrandingComponent,
	org.apache.commons.fileupload.servlet.ServletFileUpload,
	com.debisys.properties.Properties" pageEncoding="ISO-8859-1"%>
<%
int section=9;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@include file="/includes/security.jsp" %>
<jsp:useBean id="Branding" class="com.debisys.brandings.Branding" scope="request"/>
<jsp:setProperty name="Branding" property="*"/>
<%
    String PCTERM2 = BrandingComponent.PCTERM2;
    String COMMONS = BrandingComponent.COMMONS;
    String PCTERM4 = BrandingComponent.PCTERM4;
    
    String labelPCTERM2 = Languages.getString("jsp.admin.brandings.label."+PCTERM2, SessionData.getLanguage());            
    String labelCOMMONS = Languages.getString("jsp.admin.brandings.label."+COMMONS, SessionData.getLanguage());            
    String labelPCTERM4 = Languages.getString("jsp.admin.brandings.label."+PCTERM4, SessionData.getLanguage());            
            
    BrandingComponent bp = new BrandingComponent(Branding);

    String instance = DebisysConfigListener.getInstance(application);
    bp.setValue("PCTermURL", Properties.getPropertieByName(instance, "branding.tool.pcterm.url"));
    bp.setValue("PCTermSecureURL", Properties.getPropertieByName(instance, "branding.tool.pcterm.secureurl"));
    String action;
    if (ServletFileUpload.isMultipartContent(request)) {
            bp.loadFromRequest(request, response, application,SessionData);
            if(Branding.getCustomConfigId().equals("")){
                    Branding.setCustomConfigId(bp.getValue("customConfigId"));
            }           
            action = "" + bp.getValue("action");
    }else{
            action = "" + request.getParameter("action");
    }
    String message = request.getParameter("message");
    String messageType = request.getParameter("message");
    
    String brandingGroup = bp.getValue("brandingGroup");
    if (brandingGroup == null){        
        brandingGroup = PCTERM2;        
    }
    bp.setValue("brandingGroup", brandingGroup);
    
    if(action.equals("save")){
            if(bp.save()){
                    message = "Branding saved successfully.";
                    messageType = "1";
                    response.sendRedirect("branding_edit.jsp?action=edit&customConfigId="+ Branding.getCustomConfigId() + "&messageType=" + messageType + "&message=" + HTMLEncoder.encode(message));
            }else{
                    message = "Error saving branding";
                    messageType = "2";
            }
    }else if(action.equals("edit")){
            bp.loadFromDB();
    }
    String customConfigId = bp.getValue("customConfigId");
    
    String BrandingSettings = Languages.getString("jsp.admin.brandings.BrandingSettings", SessionData.getLanguage()); 
    //#639200
    String styleActivePCTERM2 = "";
    String styleActiveCOMMONS = "";
    String styleActivePCTERM4 = "";
    
    if ( brandingGroup.equals(PCTERM2) ) {
        styleActivePCTERM2 = "background-color: #639200;";
    } else if ( brandingGroup.equals(COMMONS) ){
        styleActiveCOMMONS = "background-color: #639200;";
    } else if ( brandingGroup.equals(PCTERM4) ){
        styleActivePCTERM4 = "background-color: #639200;";
    }
%>
<%@include file="/includes/header.jsp" %>
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<form id="brandingFormList" name="brandingFormList" method="post" action="admin/brandings/branding_edit.jsp" enctype="multipart/form-data" class="formComponent">
<input type="hidden" name="action" id="action"  value="save">
	<table border="0" cellpadding="0" cellspacing="0" width="900px" >
		<tr>
			<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
			<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=BrandingSettings%> <%=customConfigId%></td>
			<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
		</tr>
		<tr>
			<td colspan="3">
			<p style="color:white" class="main details-table">
				&lt;&lt; <a href="admin/brandings/branding_list.jsp"><%=Languages.getString("jsp.admin.brandings.manage",SessionData.getLanguage())%></a>
			</p>
                        <%
                        if(message != null){
                        %>
                            <p style="color:<%=(messageType.equals("1")? "green":"red")%>"><%=message %></p>
                        <%
                        }
                        %>
                                        
    <style>
    /* Style the tab */
    div.tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    div.tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px 10px 10px;
        transition: 0.3s;
        background-color: #afd855;
        border-right: 1px solid #263010;
        color: white;
    }

    /* Change background color of buttons on hover */
    div.tab button:hover {
        background-color: #639200;
    }

    /* Create an active/current tablink class */
    .tablinks button.active {
        background-color: red;
        padding: 6px 12px 10px 10px;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px 10px 10px;
        border: 1px solid #ccc;
        border-top: #afd855;
        
    }
    
    .brandingFieldSet { 
        display: block;
        margin-left: 2px;
        margin-right: 2px;
        padding-top: 0.35em;
        padding-bottom: 0.625em;
        padding-left: 0.75em;
        padding-right: 0.75em;
        border:2px solid threedface;        
    }
    
    .brandingLegend {
        display: block;
        -webkit-padding-start: 2px;
        -webkit-padding-end: 2px;
        border-width: initial;
        border-style: none;
        border-color: initial;
        border-image: initial;
        font-size: 13px;
        width: 10%;
    }
    
    .brandingImgae {
        width: 40%;
    }
                                            
    </style>                                    
    
    <script>
    function changeStatusRequest(target) {
        $("#brandingGroup").val(target);
        $("#action").val("edit");
                
    }
    function changeStatusSave() {
        $("#action").val("save");
    }
    function openControl (){
        $("#controlColors").show(2);
    }    
    </script>     
                                        
    <div class="tab">
        <button  class="tablinks" onclick="changeStatusRequest('<%=PCTERM2%>')" style="<%=styleActivePCTERM2%>"><%=labelPCTERM2%></button>
        <button  class="tablinks" onclick="changeStatusRequest('<%=COMMONS%>')" style="<%=styleActiveCOMMONS%>"><%=labelCOMMONS%></button>
        <button  class="tablinks" onclick="changeStatusRequest('<%=PCTERM4%>')" style="<%=styleActivePCTERM4%>"><%=labelPCTERM4%></button>
    </div>        
    
          <table class="main details-table">            
            <tr>
                <td colspan="2" align="center">
                                      
                </td>
            </tr>
            <% 
            if ( brandingGroup.equals(PCTERM4)){
                String BaseColor    = bp.getValues("BaseColor");
                String ReverseColor = bp.getValues("ReverseColor");
                
                String saveButton  = Languages.getString("jsp.admin.reports.pinreturn_search.applychanges", SessionData.getLanguage()); 
                String resetButton = Languages.getString("jsp.admin.reports.pinreturn_search.resetchanges", SessionData.getLanguage()); 
                String cancelButton = Languages.getString("jsp.admin.reports.paymentrequest_search.cancel", SessionData.getLanguage()); 
                
                String themes = Languages.getString("jsp.admin.brandings.label.themes", SessionData.getLanguage()); 
                String themesGreen = Languages.getString("jsp.admin.brandings.label.themesGreen", SessionData.getLanguage()); 
                String themesPurple = Languages.getString("jsp.admin.brandings.label.themesPurple", SessionData.getLanguage()); 
                String themesRed = Languages.getString("jsp.admin.brandings.label.themesRed", SessionData.getLanguage()); 
                String themesYellow = Languages.getString("jsp.admin.brandings.label.themesYellow", SessionData.getLanguage()); 
                String lblBaseColor = Languages.getString("jsp.admin.branding.label.BaseColor", SessionData.getLanguage()); 
                String lblReverseColor = Languages.getString("jsp.admin.branding.label.ReverseColor", SessionData.getLanguage()); 
                String lblSeleColor = Languages.getString("jsp.admin.brandings.tool.selectionColor", SessionData.getLanguage()); 
                String lblEditColor = Languages.getString("jsp.admin.brandings.tool.editColors", SessionData.getLanguage()); 
             %>
             <tr>
                <td colspan="2" align="center">
                    <input type="button" onclick="openControl()" value="<%=lblEditColor%>"></input>
                    <div id="controlColors" name="controlColors" style="display: none; width: 120%;border: thin solid black">
                        <jsp:include page="/theme.jsp">
                            <jsp:param name="action" value="1"></jsp:param>
                            <jsp:param name="labelSave" value="<%=saveButton%>"></jsp:param>
                            <jsp:param name="labelCancel" value="<%=cancelButton%>"></jsp:param>
                            <jsp:param name="labelReset" value="<%=resetButton%>"></jsp:param>                            
                            <jsp:param name="BaseColor" value="<%=BaseColor%>"></jsp:param>
                            <jsp:param name="ReverseColor" value="<%=ReverseColor%>"></jsp:param>
                            <jsp:param name="themes" value="<%=themes%>"></jsp:param>
                            <jsp:param name="themesGreen" value="<%=themesGreen%>"></jsp:param>
                            <jsp:param name="themesPurple" value="<%=themesPurple%>"></jsp:param>
                            <jsp:param name="themesRed" value="<%=themesRed%>"></jsp:param>
                            <jsp:param name="themesYellow" value="<%=themesYellow%>"></jsp:param>                            
                            <jsp:param name="lblBaseColor" value="<%=lblBaseColor%>"></jsp:param>
                            <jsp:param name="lblReverseColor" value="<%=lblReverseColor%>"></jsp:param>
                            <jsp:param name="lblSeleColor" value="<%=lblSeleColor%>"></jsp:param>                            
                        </jsp:include>
                    </div>
                </td>
            </tr>
             
              <%
              bp.showFields(out,SessionData);     
            } else{
              bp.showFields(out,SessionData);   
            %>
            
            
            <%
            } 
            %>
          
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" name="save" value="Save Branding" onclick="changeStatusSave();">
                    
                </td>
            </tr>
            
          </table>        
                                               
					
				
			</td>
		</tr>
	</table>
</form>

	