<%@ page language="java" import="java.util.*,
                 com.debisys.utils.HTMLEncoder" pageEncoding="ISO-8859-1"%>
<%
int section=9;
int section_page=18;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@include file="/includes/security.jsp" %>
<%@include file="/includes/header.jsp" %>
<style>
	.branding-option-list li{
		padding: 6px;
	}
</style>
            <TABLE cellSpacing=0 cellPadding=0 width=750 border=0>
              <TBODY>
              <TR>
                <TD align=left width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=images/top_blue.gif width="3000">&nbsp;<B><%=Languages.getString("jsp.admin.brandings.tool",SessionData.getLanguage()).toUpperCase()%></B></TD>
                <TD align=right width="1%" 
                background=images/top_blue.gif><IMG height=20 
                  src="images/top_right_blue.gif" width=18></TD></TR>
                  <TR>
                <TD colSpan=3>
                  <TABLE width="100%" border=0 cellPadding=0 cellSpacing=0 
                  bgColor=#fffcdf class="backceldas">
                    <TBODY>
                    <TR class="backceldas">
                      <TD width=1 bgColor=#003082><IMG 
                        src="images/trans.gif" width=1></TD>
                      <TD vAlign=top >

<%
String message = request.getParameter("message");
String messageType = request.getParameter("messageType");
if(message != null){
%>
			<p style="color:<%=(messageType.equals("1")? "green":"red")%>"><%=message %></p>
<%
}
%>
			<ul class="main branding-option-list">
				<li><a href="admin/brandings/branding_list.jsp"><%=Languages.getString("jsp.admin.brandings.manage",SessionData.getLanguage())%></a></li>
				<li><a href="admin/banners/banners_list.jsp"><%=Languages.getString("jsp.admin.banners.manage",SessionData.getLanguage())%></a></li>
				<li><a href="admin/news/news_list.jsp"><%=Languages.getString("jsp.admin.news.manage",SessionData.getLanguage())%></a></li>
				<li><a href="admin/images/image_edit.jsp?action=add"><%=Languages.getString("jsp.admin.images.add",SessionData.getLanguage())%></a></li>
			</ul>

			
                </td>
                <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	         </tr>
	         <tr>
		            <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
            </tr>
            </table>
        </td>
    </tr>
</table>			