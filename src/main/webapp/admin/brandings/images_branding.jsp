<%--
    Created on : May 3, 2017
    Author     : Jorge Nicol�s Mart�nez Romero
--%>

<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<%
    String action = request.getParameter("action");
       
    if (action.equalsIgnoreCase("getImageBytesById")){
        String objectId = request.getParameter("objectId");
        com.debisys.images.Image img = new com.debisys.images.Image();
        img.setImageId(Long.parseLong(objectId));        
        img.load();
        String imgBytes = img.getData();
        out.print(imgBytes);
    }    
%>