<%@ page import="com.debisys.languages.Languages"%>
<%
int section=9;
int section_page=12;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp"%>

<html>
<head>
<link href="/support/default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="500">
 <TR>
                <TD align=left width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=/support/images/top_blue.gif width="3000">&nbsp;<B>PROMOTION CONFIGURATION - HELP</B></TD>
                <TD align=right width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_right_blue.gif" width=18></TD></TR>
                 <tr>
 	<td colspan="3">
   	<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
 	    <tr>
 		      <td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
  		    <td align="center" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main"></td>
							<table class=main border=0 width="100%">
							<tr><td colspan=2><font size=3><b>Following are the steps you follow to create a promo.</td></tr>
							<tr>&nbsp;</tr>
							<tr><td colspan=2><font size=2>1. You add a promo by clicking Add Promotions button on the Manage Promo page, which will take you to the add/Edit promotions page.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>2. You can assign a name to the Promo.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>3. Default status of promo is inactive. Once created and saved, the status can be changed by going on the manage promo page and clicking the status Link.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>4. Status of Promo can be enabled/Disabled from the Manage Promo page by clicking the status links.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>5. There is a start date and end date associated with the promo. For testing a promo, the status should be Active and the start date should be in future.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>6. Promo can be associated either with a single product or multiple products or you can even choose All products. On the edit page of the promo you will see a list of all Available products and you can select the products and click Add button which will show the chosen products on the right side.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>7. Promo message that you want to see in your SMS can be entered for the field Promo Message. You can add some additional information regarding the promo in the Additional Data field.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>8. You can specify the chances i.e. probability of qualifying for a promo as 1 in 10 or 0 in 0 means everytime it would be considered to qualify for a promo.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>9. You would need to associate the promo with an entity. There will be options to choose Agents,Subagents,Reps,Merchants and sites. Each entity will be added in tree form showing the hierarchy. Each entity can be configured as either a Test entity or Live entity or  none.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							 <tr><td colspan=2><font size=2>10. When you say its a Test entity it means it will be only considered for the test period. When the entity is configured as Live means it will be considered when Promo is running. None means this entity needs to be excluded from both testing and Live. This option will be available at all levels starting Agent.We can specify All Agents as Live which means All the sites will be qualify for the Promo when its live by adding the All option in Agents dropdown and making the checkmark checked.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							 <tr><td colspan=2><font size=2>11. By default when the entity is added in a tree, it will be unchecked which means TEST mode. Once the user checks the checkbox, it means the entity is configured as Live. If the entity does not exist at all in the tree it means it is neither test nor live. If the site is unchecked means its in test but once the promo is live, the site needs to be checked explicitly to be considered for Promo.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							
							<tr><td colspan=2><font size=2>12. You can specify a minimum amount in the Promo Thresholds section, which means anything equal or above this amount will be qualified for Promo and anything under that amount is non- promo qualifier. </font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>13. You can also specify the bonus amount either in exact value or percentage per threshold. Eg. you can say for Transaction amount 5, Bonus amount is 1. Then second threshold set up of Transaction amount 10 has bonus amount 2, which means if top up is between 5 and 10, the bonus amount is 1 and then above 10 is $2. You also have additional receipt text for each Threshold. There are certain details that you could enter per threshold by clicking the Add Promo Details link.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>14. Once the promotion is saved, you can make it Enabled/Active by clicking the enable Link under status Header on the ManagePromotions page. On clicking the link you will be given a summary of the promo, with option to confirm or Cancel. </font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>15. If you are sure all is well, you can click Confirm which would make the promo Enabled and thus send email to Customer Service letting them know that a promo is enabled. You can click cancel to come back to Manage Promotions page. From there on any change in status either enable/Disable will give the summary and send an email.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
								</table>
							  </td>
							</tr>
							</table>
	     		        
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="/support/images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>
</body>
</html>



