<table class=main border=0 width="100%">
<tr><td colspan=2><font size=3><b><a name="1">1 Clientes</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.1 Comercios</b></font>
  <br><br>
    <b>Revisi�n/B�squeda</b>
    <ol>
      <li>Para reducir la lista de resultados, ingrese parte o la totalidad del DBA, ID Sitio, ID Comercio o Numero de Telefono en la barra de "Reducir Resultados" y presione el boton de buscar. Todos los comercios que cumplan con el criterio de busqueda seran presentados.
      <li>Los resultados pueden ser ordenados en forma ascendiente o descendiente para cada columna. <img src="/support/images/down.gif" border=0> los ordenara en forma ascendiente y <img src="/support/images/up.gif" border=0> en forma descendiente.
      <li>Para ver/editar la informacion de comercios presione en el DBA del comercio.
      <li>Para activar o desactivar comercios seleccione o deseleccione las cajas que aparecen en la columna "Desactivar?", y luego presione "Guardar Cambios" al final de la pagina.  Las comercios seleccionados indican localidades desactivadas y los deseleccionados una localidad activa.  El boton de Resetear pondra todas las cajas en sus valores originales antes de la ultima guardada.
      <li>Para ver una lista de transacciones de un comercio en particular, presione en <img src="/support/images/icon_dollar.gif" border=0> icono.
      <li>El resumen de total de localidades acitvas y desactivadas al final de la pagina indica el total de todos los comercios y no se ve afectado cuando la lista de comercios es reducida por filtros.
    </ol>
    <b>Editar Comercios</b>
    <ol>
      <li>Para agregar o eliminar un Codigo de Cajero presione el boton "Codigo Cajeros" para el terminal que desea editar.
    </ol>
    <b>Editando Codigos de Cajeros</b>
    <ol>
      <li>Para agregar un codigo de cajero ingrese un valor numerico en la barra de ingreso y presione "Enviar".
      <li>Para eliminar codigos de cajero seleccione las cajas al lado de los codigos que desea eliminar y presione "Enviar".
    </ol>
  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="3">2 Transacciones</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>2.1 Transacciones</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>La fecha de todas las transacciones estan en Horario del Pacifico PST.
    <li>Para ver un listado de transacciones de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para transacciones de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Para descargar las transacciones presione el boton de "Descargar Transacciones".
    </ol>
    <b>Descargando</b>
    <ol>
    <li>Todos los archivos descargados son archivos zip que contienen archivos .csv (delimitados por coma).
    </ol>
  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="4">3 Reportes</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.1 Resumen Transacciones por Comercio</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver reportes de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para reportes de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Puede filtrar los resultados de reportes por comercio. Para ver todos los comercios seleccione "TODOS". Para ver solo ciertos comercios, mantenga presionadas las teclas &lt;ctrl&gt; y seleccione los comercios que desea ver.
    <li>Los resultados pueden ser ordenados en forma ascendente o descendiente para cada columna.<img src="/support/images/down.gif" border=0> Ordenara los resultados en forma ascendente y<img src="/support/images/up.gif" border=0> los ordenara en forma descendiente.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.2 Resumen de Transacciones por Producto</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver reportes de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para reportes de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Puede filtrar los resultados del reporte por producto.  Para ver todos los productos seleccione "TODOS". Para ver solo ciertos productos mantenga presionadas las teclas &lt;ctrl&gt; y seleccione los productos que desea ver.
    <li>Los resultados pueden ser ordenados en forma ascendente o descendiente para cada columna.<img src="/support/images/down.gif" border=0> Ordenara los resultados en forma ascendente y<img src="/support/images/up.gif" border=0> los ordenara en forma descendiente.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td>
  <font color=ff0000>*La data disponible para reportes es solo de un periodo de 7 meses previos a la fecha de hoy.</font>
  <br>
  <font size=2><b>3.3 Consola Gerencial</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>Consola Gerencial es un resumen de 4 reportes para una rango de fechas predefinido.
      <ol>
        <li>Principales 10 Comercios - presenta los 10 comercios con mayor volumen de ventas del ultimo mes a partir a la fecha de hoy.
        <li>Principales 10 Productos - presenta los 10 productos mas vendidos por monto total de ventas del ultimo mes a partir de la fecha de hoy.
        <li>Volumen Semanal - transacciones totales del ultimo mes a partir de la fecha de hoy desglosados por incrementos semanales.
        <li>Volumen Mensual - transacciones totales de los ultimos 6 meses a partir de la fecha de hoy desglosados por incrementos mensuales.
      </ol>
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.4 Total Volumen Diario</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver un reporte de una fecha determinada ingrese la Fecha de Inicio del periodo que desea ver.
    <li>La Fecha de Inicio puede ser ingresada manualmente ene el formato o puede ser seleccionada usando el calendario.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El Reporte de Total Volumen Diario es un resumen de las transacciones diarias de 1 mes a partir de la Fecha de Inicio ingresada por el usuario.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.5 Total Volumen Semanal</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver un reporte de una fecha determinada ingrese la Fecha de Inicio del periodo que desea ver.
    <li>La Fecha de Inicio puede ser ingresada manualmente ene el formato o puede ser seleccionada usando el calendario.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El Reporte de Total Volumen Semanal es un resumen de las transacciones semanales por una periodo de 1 mes a partir de la fecha ingresada por el usuario.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.6 Total Volumen Mensual</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver un reporte de una fecha determinada ingrese la Fecha de Inicio del periodo que desea ver.
    <li>La Fecha de Inicio puede ser ingresada manualmente en el formato o puede ser seleccionada usando el calendario.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El reporte de Total Volumen Mensual es un resumen de las transacciones mensuales de un periodo de 6 meses a partir de la fecha ingresada por el usuario.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.7 Principales 10 Comercios</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver reportes de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para reportes de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El Reporte de 10 Comercios Principales es un reporte de los 10 comercios con mayor volumen de ventas para el rango de fecha ingresado por el usuario.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.8 Principales 10 Productos</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de los reportes estan en Hora del Pacifico PST.
    <li>Para ver reportes de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para reportes de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El Reporte de 10 Productos Principales es un reporte de los 10 productos mas vendidos por volumen de ventas para el rango de fecha ingresado por el usuario.
    </ol>
  </td>
</tr>
</table>
