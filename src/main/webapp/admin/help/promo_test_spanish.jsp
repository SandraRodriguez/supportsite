<%@ page import="com.debisys.languages.Languages"%>
<%
int section=9;
int section_page=12;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp"%>

<html>
<head>
<link href="/support/default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="500">
 <TR>
                <TD align=left width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=/support/images/top_blue.gif width="3000">&nbsp;<B>AYUDA PARA LA CONFIGURACI�N DE PROMOCIONES</B></TD>
                <TD align=right width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_right_blue.gif" width=18></TD></TR>

 <tr>
 	<td colspan="3">
   	<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
 	    <tr>
 		      <td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
  		    <td align="center" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center" class="formArea">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main"></td>
							<table class=main border=0 width="100%">
							<tr><td colspan=2><font size=3><b>CONFIGURACI�N DE AUTORIZACIONES EN MODO DE PRUEBA</td></tr>
							<tr>&nbsp;</tr>
							<tr><td colspan=2><font size=2>El cuadro siguiente explica las configuraciones necesarias para que una promoci�n funcione correctamente.</font></td></tr>
							<tr>
							  <td width=5>&nbsp;</td>
							  <td>
								<table>
									<tr class="rowhead2">
										<td>Configuraci�n Requerida</td>
										<td>N�meros de Acceso Autorizados</td>
										<td>Comercios Autorizados</td>
										<td>Tiendas Autorizadas</td>
										<td>Tiendas No Autorizadas</td>
									</tr>
									<tr class="row1">
										<td>Ninguna Autorizaci�n</td>
										<td>Ninguno</td>
										<td>Ninguno</td>
										<td>Ninguna</td>
										<td>Ninguna</td>
									</tr>		
									<tr class="row2">
										<td>Ninguna Autorizaci�n</td>
										<td>N�mero de Acceso de prueba</td>
										<td>Ninguno</td>
										<td>Ninguna</td>
										<td>Ninguna</td>
									</tr>		
									<tr class="row1">
										<td>Ninguna Autorizaci�n</td>
										<td>N�meros de Accesso de Prueba</td>
										<td>Todos</td>
										<td>Ninguna</td>
										<td>Todas</td>
									</tr>				
									<tr class="row2">
										<td>Ninguna Autorizaci�n</td>
										<td>N�meros de Acceso de Prueba</td>
										<td>Ninguno</td>
										<td>Ninguna</td>
										<td>Todas</td>
									</tr>				
									<tr class="row1">
										<td>Ninguna Autorizaci�n</td>
										<td>N�meros de Acceso de Prueba</td>
										<td>Ninguno</td>
										<td>Todas</td>
										<td>Ninguno</td>
									</tr>				
									<tr class="row2">
										<td>Autorizar todas las tiendas de un comercio para un n�mero de acceso de prueba</td>
										<td>N�mero de Acceso de Prueba</td>
										<td>Comercio A</td>
										<td>Ninguna</td>
										<td>Ninguna</td>
									</tr>				
									<tr class="row1">
										<td>Autorizar s�lo una o varias tiendas de todos los comercios para un n�mero de acceso de prueba</td>
										<td>N�mero de Acceso de Prueba</td>
										<td>Todos</td>
										<td>Tienda A, Tienda B, ...</td>
										<td>Todas</td>
									</tr>				
									<tr class="row2">
										<td>Autorizar solo una o varias tiendas de uno o varios comercios para un n�mero de acceso de prueba</td>
										<td>N�mero de Acceso de Prueba</td>
										<td>Comercio A, Comercio B, ...</td>
										<td>Tienda A, Tienda B, ...</td>
										<td>Todas</td>
									</tr>				
									<tr class="row1">
										<td>Autorizar todas las tiendas pero no autorizar una o varias tiendas para un numero de acceso de prueba</td>
										<td>N�mero de Acceso de Prueba</td>
										<td>Todos</td>
										<td>Todas</td>
										<td>Tienda A, Tienda B, ...</td>
									</tr>				
									<tr class="row2">
										<td>Autorizar una o varias tiendas pero no autorizar una o varias tiendas pertenecientes a uno o varios comercios para un n�mero de acceso de prueba</td>
										<td>N�mero de Acceso de Prueba</td>
										<td>Comercio A, Comercio B, ...</td>
										<td>Tienda A, Tienda B, ...</td>
										<td>Tienda C, Tienda D, ...</td>
									</tr>				
								</table>
							  </td>
							</tr>
							</table>
	     		        
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="/support/images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>
</body>
</html>



