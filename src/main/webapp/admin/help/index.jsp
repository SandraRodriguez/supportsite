<%@ page import="com.debisys.languages.Languages"%>
<%
int section=5;
int section_page=1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp"%>
<%
String label_lang="";
if(SessionData.getLanguage().equals("spanish")){
label_lang="AYUDA";
}
else{
label_lang="HELP";
}
 %>
<html>
<head>
<link href="/support/default.css" type="text/css" rel="stylesheet">
</head>
<body>
<TABLE cellSpacing=0 cellPadding=0 width=500 border=0>
  <TBODY>
  <TR>
    <TD align="left" width=18 height=20><IMG height=20 
      src="/support/images/top_left_blue.gif" width=18></TD>
    <TD align="left" class=formAreaTitle width="2000" 
      background=/support/images/top_blue.gif><B class="stilotexto"><%=label_lang%></B></TD>
    <TD align="right" width=12 height=20><IMG 
  src="/support/images/top_right_blue.gif"></TD></TR>
  <TR>
    <TD colSpan=3>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" bgColor=#fffcdf 
        border=0><TBODY>
        <TR>
          <TD width=1 bgColor=#003082><IMG src="/support/images/trans.gif" 
            width=1></TD>
          <TD vAlign=top align=middle bgColor=#ffffff>
            <TABLE width="100%" 
            border=0 align=center cellPadding=2 cellSpacing=0 class="fondoceldas">
              <TBODY>
              <TR>
                <TD width=18>&nbsp;</TD>
                <TD class=main>

                 <%
                   if (SessionData.getLanguage().equals("spanish"))
                   {
		// This is for Carrier User viewing only
		if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
%>
<%@ include file="carrier_user_spanish.jsp" %>
<%
		}
		else if (SessionData.getProperty("access_level").equals(DebisysConstants.ISO))
                       {
%>
<%@ include file="iso_spanish.jsp" %>
<%
                       }
                       else if (SessionData.getProperty("access_level").equals(DebisysConstants.REP)
                       ||SessionData.getProperty("access_level").equals(DebisysConstants.AGENT)
                       ||SessionData.getProperty("access_level").equals(DebisysConstants.SUBAGENT))
                       {
%>
<%@ include file="rep_spanish.jsp" %>
<%
                       }
                       else if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT))
                       {
%>
<%@ include file="merchant_spanish.jsp" %>
<%
                       }
                   }
                     else
                   {
		// This is for Carrier User viewing only
		if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_CARRIER_REPORTS)) {
%>
<%@ include file="carrier_user.jsp" %>
<%
		}
		else if (SessionData.getProperty("access_level").equals(DebisysConstants.ISO))
                       {
%>
<%@ include file="iso.jsp" %><%
                       }
                       else if (SessionData.getProperty("access_level").equals(DebisysConstants.REP)
                       ||SessionData.getProperty("access_level").equals(DebisysConstants.AGENT)
                       ||SessionData.getProperty("access_level").equals(DebisysConstants.SUBAGENT)
                       ||SessionData.getProperty("access_level").equals(DebisysConstants.CARRIER))
                       {
%>
<%@ include file="rep.jsp" %>
<%
                       }
                       else if (SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT))
                       {
%>
<%@ include file="merchant.jsp" %>
<%
                       }

                   }

%>

     				    </td>
     		        <td width="18">&nbsp;</td>
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="/support/images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>
</body>
</html>



