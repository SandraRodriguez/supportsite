<table class=main border=0 width="100%">
<tr><td colspan=2><font size=3><b><a name="1">1 Customers</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.1 Merchants</b></font>
  <br><br>
    <b>Viewing/Searching</b>
    <ol>
      <li>In order to narrow a list of results, enter part or all of a dba, site id, merchant id, or phone number in the "narrow results" input box and click the search button.  All merchants matching the criteria will be returned.
      <li>Results can be sorted in ascending or descending order for each column. <img src="/support/images/down.gif" border=0> will sort ascending and <img src="/support/images/up.gif" border=0> will sort descending.
      <li>To view/edit the merchants information click on the merchant's dba.
      <li>To enable or disable merchant locations check or uncheck the boxes under the "Disabled?" column, then click "Save Changes" at the bottom of the page.  Checked indicates a disabled location and unchecked indicates an active location.  The reset button will reset the checkboxes to their original values before the last save.
      <li>To view a list of transactions for a particular merchant, click on the <img src="/support/images/icon_dollar.gif" border=0> icon.
      <li>The summary of total active and disabled locations at the bottom of the page indicates a total of all merchants and are not affected when a list of merchants are narrowed or filtered down.
    </ol>
    <b>Editing Merchants</b>
    <ol>
      <li>To add or remove clerk codes click on "Clerk Codes" for the terminal you would like to edit.
    </ol>
    <b>Editing Clerk Codes</b>
    <ol>
      <li>To add a clerk code enter a numeric value in the input box and click "Submit".
      <li>To remove clerk codes check the boxes next to the clerk codes you would like to delete and click "Submit".
    </ol>

  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="3">2 Transactions</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>2.1 Transactions</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All transaction dates are in Pacific Standard Time.
    <li>To view a list of transactions for a certain date range enter a start and end date for the period you would like to view.  For the transactions in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>To download transactions click the "Download Transactions" button.
    </ol>
    <b>Downloading</b>
    <ol>
    <li>All downloaded files are zip files containing .csv (Comma Separated Values) files.
    </ol>

  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="4">3 Reports</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.1 Transaction Summary By Merchant</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view reports for a certain date range enter a start and end date for the period you would like to view.  For the reports in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>You may filter report results by merchant.  To view all merchants select "ALL".  To view only certain merchants, hold the &lt;ctrl&gt; key, and click the merchants you would like to view.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.gif" border=0> will sort ascending and <img src="/support/images/up.gif" border=0> will sort descending.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.2 Transaction Summary by Product</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view reports for a certain date range enter a start and end date for the period you would like to view.  For the reports in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>You may filter report results by product.  To view all products select "ALL".  To view only certain products, hold the &lt;ctrl&gt; key, and click the products you would like to view.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.gif" border=0> will sort ascending and <img src="/support/images/up.gif" border=0> will sort descending.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td>
  <font color=ff0000>*Report data is only available for a 7 month period from todays date.</font>
  <br>
  <font size=2><b>3.3 Management Dashboard</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>All charts support mouse overs for exact numeric values.
    <li>Management Dashboard is a summary of 4 reports for a predefined date range.
      <ol>
        <li>Top 10 Merchants - top 10 merchants for a 1 month period from the current date.
        <li>Top 10 Products - top 10 products for a 1 month period from the current date. The criteria used to determine the top 10 products is the total amount in sales and not the quantity or number of products sold.
        <li>Weekly Volume - transaction totals for a 1 month period from the current date broken down in weekly increments.
        <li>Monthly Volume - transaction totals for a 6 month period from the current date broken down in monthly increments.
      </ol>
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.4 Total Volume Daily</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view a report for a certain date enter a start date for the period you would like to view.
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The total volume daily report is the summary of transactions daily for a one month period starting from the start date entered by the user.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.5 Total Volume Weekly</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view a report for a certain date enter a start date for the period you would like to view.
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The total volume weekly report is the summary of transactions weekly for a one month period starting from the start date entered by the user.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.6 Total Volume Monthly</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view a report for a certain date enter a start date for the period you would like to view.
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The total volume monthly report is the summary of transactions monthly for a six month period starting from the start date entered by the user.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.7 Top 10 Merchants</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view reports for a certain date range enter a start and end date for the period you would like to view.  For the reports in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The top 10 merchants report is a the top 10 merchants for the date range entered by the user.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.8 Top 10 Products</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view reports for a certain date range enter a start and end date for the period you would like to view.  For the reports in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The top 10 products report is a the top 10 products for the date range entered by the user.  The criteria used to determine the top 10 products is the total amount in sales and not the quantity or number of products sold.
    </ol>
  </td>
</tr>

</table>




