<table class=main border=0 width="100%">
<tr><td colspan=2><font size=3><b><a name="4">1 Reports</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.1 Transaction Summary by Product</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view reports for a certain date range enter a start and end date for the period you would like to view.  For the reports in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>You may filter report results by rep.  To view all reps select "ALL".  To view only certain reps, hold the &lt;ctrl&gt; key, and click the reps you would like to view.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.png" border=0> will sort ascending and <img src="/support/images/up.png" border=0> will sort descending.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.2 Transaction Summary by Terminal Type</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view reports for a certain date range enter a start and end date for the period you would like to view.  For the reports in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>You may filter report results by merchant.  To view all merchants select "ALL".  To view only certain merchants, hold the &lt;ctrl&gt; key, and click the merchants you would like to view.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.png" border=0> will sort ascending and <img src="/support/images/up.png" border=0> will sort descending.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.3 Transaction Error Summary by Terminal Type</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view reports for a certain date range enter a start and end date for the period you would like to view.  For the reports in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>All end dates are always inclusive, any date entered in this field is considered for the entire day up until midnight PST.
    <li>You may filter report results by product.  To view all products select "ALL".  To view only certain products, hold the &lt;ctrl&gt; key, and click the products you would like to view.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.png" border=0> will sort ascending and <img src="/support/images/up.png" border=0> will sort descending.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.4 Total Volume Daily Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>All charts support mouse overs for exact numeric values.
    <li>Management Dashboard is a summary of 4 reports for a predefined date range.
      <ol>
        <li>Top 10 Merchants - top 10 merchants for a 1 month period from the current date.
        <li>Top 10 Products - top 10 products for a 1 month period from the current date. The criteria used to determine the top 10 products is the total amount in sales and not the quantity or number of products sold.
        <li>Weekly Volume - transaction totals for a 1 month period from the current date broken down in weekly increments.
        <li>Monthly Volume - transaction totals for a 6 month period from the current date broken down in monthly increments.
      </ol>
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.5 Total Volume Monthly Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view a report for a certain date enter a start date for the period you would like to view.    
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The total volume daily report is the summary of transactions daily for a one month period starting from the start date entered by the user.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.6 Carrier Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view a report for a certain date enter a start date for the period you would like to view.
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The total volume weekly report is the summary of transactions weekly for a one month period starting from the start date entered by the user.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.7 Roaming eTopUp (CrossBorder) Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in Pacific Standard Time.
    <li>To view a report for a certain date enter a start date for the period you would like to view.
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the date chooser.
    <li>The chart supports mouse overs for exact numeric values.
    <li>The total volume monthly report is the summary of transactions monthly for a six month period starting from the start date entered by the user.
    </ol>
  </td>
</tr>
</table>