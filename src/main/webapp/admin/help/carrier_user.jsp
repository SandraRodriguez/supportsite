<table class=main border=0 width="100%">
<tr><td colspan=2><font size=3><b><a name="4">1 Transactions Reports</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.1 Transaction Summary by Product</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in your local time zone (server time).
    <li>To view reports enter a start and end date or use the convenient calendar for the period you would like to view. For the reports in the same day, enter the same start and end date.
    <li>All date ranges are inclusive. The beginning date starts at midnight and the ending date is through 23:59:59 hours.
    <li>You may filter report results by products. To include all products, select the default "ALL". To include specific products, hold the &lt;ctrl&gt; key, and right-mouse click the products you need in the report.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.png" border=0> will sort ascending and <img src="/support/images/up.png" border=0> will sort descending.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.2 Transaction Summary by Terminal Type</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in your local time zone (server time).
    <li>To view reports enter a start and end date or use the convenient calendar for the period you would like to view. For the reports in the same day, enter the same start and end date.
    <li>All date ranges are inclusive. The beginning date starts at midnight and the ending date is through 23:59:59 hours.
    <li>You may filter report results by products. To include all products, select the default "ALL". To include specific products, hold the &lt;ctrl&gt; key, and right-mouse click the products you need in the report.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.png" border=0> will sort ascending and <img src="/support/images/up.png" border=0> will sort descending.
	</ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.3 Transaction Error Summary</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in your local time zone (server time).
    <li>To view reports enter a start and end date or use the convenient calendar for the period you would like to view. For the reports in the same day, enter the same start and end date.
    <li>All date ranges are inclusive. The beginning date starts at midnight and the ending date is through 23:59:59 hours.
    <li>You may filter report results by products. To include all products, select the default "ALL". To include specific products, hold the &lt;ctrl&gt; key, and right-mouse click the products you need in the report.
    <li>Report results can be sorted in ascending or descending order for each column. <img src="/support/images/down.png" border=0> will sort ascending and <img src="/support/images/up.png" border=0> will sort descending.
	</ol>
  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="4">2 Analysis Reports</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>2.1 Total Volume Daily Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in your local time zone (server time).
    <li>To view a report for a certain date enter a start date.    
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the convenient calendar.
    <li>Hover your mouse cursor to view exact numeric values.
    <li>The total volume daily report is the summary of transactions daily for a one month period starting from the start date entered by the user.
    </ol>
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>2.2 Total Volume Monthly Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in your local time zone (server time).
    <li>To view a report for a certain date enter a start date.    
    <li>Start date may be manually entered in "mm/dd/yyyy" format or can be selected using the convenient calendar.
    <li>Hover your mouse cursor to view exact numeric values.
    <li>The total volume monthly report is the summary of transactions monthly for a six month period starting from the start date entered by the user.
    </ol>
  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="4">3 Carrier Reports</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.1 Carrier Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in your local time zone (server time).
    <li>To view a report for a certain date range enter a start and end date for the period you would like to view.  For a report in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the convenient calendar.
    <li>All date ranges are inclusive. The beginning date starts at midnight and the ending date is through 23:59:59 hours.
    <li>One of the most powerful reports on the Emida system as it provides at a glance current versus previous month performance reporting on a range of important mobile service metrics including ARPU.
    </ol>
  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="4">4 Roaming Reports</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>4.1 Roaming eTopUp (CrossBorder) Report</b></font>
  <br><br>
    <b>Viewing</b>
    <ol>
    <li>All report dates are in your local time zone (server time).
    <li>To view a report for a certain date range enter a start and end date for the period you would like to view.  For a report in the same day, enter the same start and end date.
    <li>Date ranges may be manually entered in "mm/dd/yyyy" format or can be selected using the conventient calendar.
    <li>All date ranges are inclusive. The beginning date starts at midnight and the ending date is through 23:59:59 hours.
    <li>If your Emida system supports sending top-ups to another country, this report provides summary and detail on these "cross-border" top-ups.
    </ol>
  </td>
</tr>
</table>