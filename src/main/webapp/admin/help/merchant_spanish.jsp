<table class=main border=0 width="100%">
<tr><td colspan=2><font size=3><b><a name="1">1 Merchant Info</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>1.1 Informacion del COmercio</b></font>
  <br><br>
    <b>Informacion del Comercio</b>
    <ol>
      <li>Para agregar o eliminar Codigos de Cajeros presione el boton de "Codigos de Cajeros" para el terminal que desea editar.
    </ol>
    <b>Modificar Codigos de Cajeros</b>
    <ol>
      <li>Para agregar un codigo de cajero ingrese un valor numerico en la barra de ingreso y presione el boton de "Enviar".
      <li>Para eliminar un codigo de cajero seleccione las cajas junto al codigo de cajero que desea eliminar y presione el boton de "Enviar".
    </ol>
  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="3">2 Transacciones</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>2.1 Transacciones</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>La fecha de todas las transacciones estan en Horario del Pacifico PST.
    <li>Para ver un listado de transacciones de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para transacciones de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Para descargar las transacciones presione el boton de "Descargar Transacciones".
    </ol>
    <b>Descargando</b>
    <ol>
    <li>Todos los archivos descargados son archivos zip que contienen archivos .csv (delimitados por coma).
    </ol>
  </td>
</tr>
<tr><td colspan=2><font size=3><b><a name="4">3 Reportes</a></td></tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.1 Resumen de Transacciones por Producto</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver reportes de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para reportes de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Puede filtrar los resultados del reporte por producto.  Para ver todos los productos seleccione "TODOS". Para ver solo ciertos productos mantenga presionadas las teclas &lt;ctrl&gt; y seleccione los productos que desea ver.
    <li>Los resultados pueden ser ordenados en forma ascendente o descendiente para cada columna.<img src="/support/images/down.png" border=0> Ordenara los resultados en forma ascendente y<img src="/support/images/up.png" border=0> los ordenara en forma descendiente.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td>
  <font color=ff0000>*La data disponible para reportes es solo de un periodo de 7 meses previos a la fecha de hoy.</font>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.2 Total Volumen Diario</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver un reporte de una fecha determinada ingrese la Fecha de Inicio del periodo que desea ver.
    <li>La Fecha de Inicio puede ser ingresada manualmente ene el formato o puede ser seleccionada usando el calendario.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El Reporte de Total Volumen Diario es un resumen de las transacciones diarias de 1 mes a partir de la Fecha de Inicio ingresada por el usuario.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.3 Total Volumen Semanal</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver un reporte de una fecha determinada ingrese la Fecha de Inicio del periodo que desea ver.
    <li>La Fecha de Inicio puede ser ingresada manualmente ene el formato o puede ser seleccionada usando el calendario.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El Reporte de Total Volumen Semanal es un resumen de las transacciones semanales por una periodo de 1 mes a partir de la fecha ingresada por el usuario.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.4 Total Volume Mensual</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de reportes estan en Horario del Pacifico PST.
    <li>Para ver un reporte de una fecha determinada ingrese la Fecha de Inicio del periodo que desea ver.
    <li>La Fecha de Inicio puede ser ingresada manualmente en el formato o puede ser seleccionada usando el calendario.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El reporte de Total Volumen Mensual es un resumen de las transacciones mensuales de un periodo de 6 meses a partir de la fecha ingresada por el usuario.
    </ol>
  </td>
</tr>
<tr>
  <td width=5>&nbsp;</td>
  <td><font size=2><b>3.5 Principales 10 Productos</b></font>
  <br><br>
    <b>Revisi�n</b>
    <ol>
    <li>Todas las fechas de los reportes estan en Hora del Pacifico PST.
    <li>Para ver reportes de un rango de fechas determinado ingrese una Fecha de Inicio y una Fecha Final para el periodo que desea ver. Para reportes de un solo dia, ingrese la misma Fecha de Inicio y Fecha Final.
    <li>El rango de fechas puede ser ingresado manualmente bajo el formato "mm/dd/yyyy" format o seleccionando las fechas en los calendarios.
    <li>Todas las Fechas Finales son siempre inclusivas, cualquier fecha ingresada en este campo es considerada para el dia entero hasta medianoche Horario del Pacifico.
    <li>Puede pasar el mouse sobre las graficas para ver los valores numericos exactos.
    <li>El Reporte de 10 Productos Principales es un reporte de los 10 productos mas vendidos por volumen de ventas para el rango de fecha ingresado por el usuario.
    </ol>
  </td>
</tr>
</table>
