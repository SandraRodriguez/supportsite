<%@ page import="com.debisys.languages.Languages"%>
<%
int section=9;
int section_page=12;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp"%>

<html>
<head>
<link href="/support/default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="500">
 <TR>
                <TD align=left width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=/support/images/top_blue.gif width="3000">&nbsp;<B>CONFIGURACION DE PROMOCION - AYUDA</B></TD>
                <TD align=right width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_right_blue.gif" width=18></TD></TR>
                 <tr>
 	<td colspan="3">
   	<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
 	    <tr>
 		      <td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
  		    <td align="center" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main"></td>
							<table class=main border=0 width="100%">
							<tr><td colspan=2><font size=3><b>A continuaci�n se describen los pasos para crear una Promoci�n.</td></tr>
							<tr>&nbsp;</tr>
							<tr><td colspan=2><font size=2>1. Usted puede agregar una Promoci�n haciendo clic en el bot�n de "Agregar Promoci�n" en la p�gina de Administraci�n de Promociones, la cual le llevar� a la p�gina de Agregar/Editar promociones.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>2. Usted puede ahora asignar un nombre a la Promoci�n.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>3. El Estado por defecto de la Promoci�n es INACTIVA. Una vez la Promoci�n ha sido creada y salvada, el estado puede ser actualizado usando la p�gina de administrar promociones y haciendo clic en el enlace de Estados.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>4. El Estado de una Promoci�n puede ser HABILITADA o DESHABILITADA desee la p�gina de administrar promociones haciendo clic en el enlace de Estados.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>5. Hay una Fecha de Inicio y Fecha de Finalizaci�n asociada con la Promoci�n. De manera de probar la Promoci�n antes de que pase a producci�n, el Estado debe ser ACTIVA y la Fecha de Comienzo debe estar configurada en una fecha futura.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>6. Una Promoci�n puede ser asociada con uno o m�s productos, o inlcluso usted puede seleccionar TODOS los productos. En la p�gina de edici�n de Promoci�n usted ver� una lista con todos los productos disponibles de donde podr� hacer su elecci�n y luego hacer clic en el bot�n de "Agregar". Luego de esto, el o los productos seleccionados se mostrar�n en el lado derecho de la pantalla.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>7. En el campo Mensaje de Promoci�n usted puede insertar el mensaje que ustede desea ver como notificaci�n SMS. Usted puede agregar informacion adicional relacionada con la Promoci�n em el campo de Datos Adicionales.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>8. Usted puede especificar la probabilidad de calificar para una Promoci�n en la forma de 1 a 10, o si lo desea puede usar 0 en 0 que significa que siempre se clasificara para una Promoci�n.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>9. Usted necesitar� asociar una o mas entidades a la Promoci�n. Usted podr� ver opciones para configurar una Promoci�n a nivel de Agentes, Subagentes, Reps, Comercios y Terminales. Cada entidad ser� agregada in forma de arbol mostrando su jerarqu�a respectiva. Cada entidad puede ser configurada como Prueba o Producci�n, una entidad no presente en el arbol no ser� considerada ni como Prueba, ni como Producci�n.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>10. Cuando usted configura una Entidad como de Prueba significa que ser� considerada en la Promoci�n durante el periodo de pruebas. Cuando la entidad es configurada como producci�n significa que la misma ser� considerada s�lo durante el per�odo valido de producci�n de la Promoci�n. Una entidad no presente en el arbol no ser� considerada ni como Prueba, ni como Producci�n. Esta opci�n estar� disponible para todos los niveles de entidad comenzando por Agente. Usted puede especificar TODOS los Agentes como producci�n, lo cual significa que esta configurando todas las entidades debajo del ISO para que clasifiquen como producci�n si selecciona TODOS los Agentes y hace clic en el checkbox.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							 <tr><td colspan=2><font size=2>11. Cuando una entidad es agregada al arbol por defecto estar� en modo Prueba, la misma estar no sleccionada en el checkbox, lo que significa en Modo de Prueba. Una vez que el usuario hace clic en el checkbox y lo marca como seleccionado, significa que la entidad esta marcada como Producci�n. If the entity does not exist at all in the tree it means it is neither test nor live. Una entidad necesita estar en el arbol y estar explicitamente seleccionado para ser considerado como en modo Producci�n.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							
							<tr><td colspan=2><font size=2>12. Ustede puede especificar un monto m�nicmo para clasificar para la Promoci�n en la secci�n de Niveles de Promoci�n, which means anything equal or above this amount will be qualified for Promo and anything under that amount is non- promo qualifier. </font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>13. Usted tambi�n puede especificar el monto de bono a ser otorgado como parte de la Promoci�n como un valor fijo o un porcentaje del monto de la transacci�n por niveles de Promoci�n. Ejemplo: Usted puede decir que para un monto m�nimo de 5 el bono ser� 1. Luego en el segundo nivel usted puede decir que para montos de m�nimo 10 se tendr� un bono de 2, lo que significa que si una transaccion est� entre 5 y 10, el monto de bono sera 1, y por encima de 10 ser� de 2. Usted tambi�n puede especificar un cecibo de texto (mensaje) adicional por cada nivel. Hay detalles adicionales que aplican a solo ciertos proveedores que usted puede agregar haciendo clic en el link de Agregar Detalles de Promoci�n.</font></td></tr>							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>14. Una vez que la Promoci�n es salvada usted [puede activarla o desactivarla haciendo clic en el link de Estado en la p�gina de manejar promociones. Cuando haga clic para activar la promoci�n usted podr� ver un resumen de la promoci�n configurada, con la opci�n de cinfirmar o cancelar. </font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2><font size=2>15. Si usted est� seguro que todo est� bien, puede hacer clic en la opci�n de confirmar lo cual habilitar� la Promoci�n y enviar� un correo electr�nico autom�tico a Atenci�n al Cliente para dejarles saber que una nueva Promoci�n ha sido configurada. Usted tambi�n puede hacer clic en cancelar lo cual lo llevar� de regreso a la p�gina de manejar promociones. Desde all� cada vez que ustede active o reactive una Promoci�n existente le mostrar� el resumen de la promoci�n y se enviar� un correo electr�nico para Atenci�n al Cliente.</font></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
							<tr><td colspan=2></td></tr>
								</table>
							  </td>
							</tr>
							</table>
	     		        
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="/support/images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="/support/images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>
</body>
</html>



