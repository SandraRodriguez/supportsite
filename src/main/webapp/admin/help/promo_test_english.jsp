<%@ page import="com.debisys.languages.Languages"%>
<%
int section=9;
int section_page=12;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp"%>

<html>
<head>
<link href="/support/default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="500">
 <TR>
                <TD align=left width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_left_blue.gif" width=18></TD>
                <TD class=formAreaTitle 
                background=/support/images/top_blue.gif width="3000">&nbsp;<B>PROMOTION CONFIGURATION - HELP</B></TD>
                <TD align=right width="1%" 
                background=/support/images/top_blue.gif><IMG height=20 
                  src="/support/images/top_right_blue.gif" width=18></TD></TR> 

 <tr>
 	<td colspan="3">
   	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="formaArea2">
 	    <tr>
 		      <td width="1"><img src="/support/images/trans.gif" width="1"></td>
  		    <td align="center" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center" class="formArea">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main"></td>
						<table class=main border=0 width="100%">
						<tr><td colspan=2><font size=3><b>AUTHORIZATION CONFIGURATIONS FOR TEST MODE</td></tr>
						<tr>&nbsp;</tr>
						<tr><td colspan=2><font size=2>The matrix below explain the necessary configurations for a promotion to run.</font></td></tr>
						<tr>
						  <td width=5>&nbsp;</td>
						  <td>
							<table>
								<tr class="rowhead2">
									<td>Required Configuration</td>
									<td>Authorized Access Number</td>
									<td>Authorized Merchants</td>
									<td>Authorized Sites</td>
									<td>Non Authorized Sites</td>
								</tr>
								<tr class="row1">
									<td>None</td>
									<td>None</td>
									<td>None</td>
									<td>None</td>
									<td>None</td>
								</tr>		
								<tr class="row2">
									<td>None</td>
									<td>Test Access Numbers</td>
									<td>None</td>
									<td>None</td>
									<td>None</td>
								</tr>		
								<tr class="row1">
									<td>None</td>
									<td>Test Access Numbers</td>
									<td>All</td>
									<td>None</td>
									<td>All</td>
								</tr>				
								<tr class="row2">
									<td>None</td>
									<td>Test Access Numbers</td>
									<td>None</td>
									<td>None</td>
									<td>All</td>
								</tr>				
								<tr class="row1">
									<td>None</td>
									<td>Test Access Numbers</td>
									<td>None</td>
									<td>All</td>
									<td>None</td>
								</tr>				
								<tr class="row2">
									<td>Authorize all Sites from one merchant for one test access number</td>
									<td>Test Access Number</td>
									<td>Specific Merchant</td>
									<td>None</td>
									<td>None</td>
								</tr>				
								<tr class="row1">
									<td>Authorize just one or several sites from all merchants for one test access number</td>
									<td>Test Access Number</td>
									<td>All</td>
									<td>Site A, Site B, ...</td>
									<td>All</td>
								</tr>				
								<tr class="row2">
									<td>Authorize just one or several sites from one or several merchants for one test access number</td>
									<td>Test Access Number</td>
									<td>Merchant A, Merchant B, ...</td>
									<td>Site A, Site B, ...</td>
									<td>All</td>
								</tr>				
								<tr class="row1">
									<td>Authorize all sites but not authorize one or several sites for one test access number</td>
									<td>Test Access Number</td>
									<td>All</td>
									<td>All</td>
									<td>Site A, Site B, ...</td>
								</tr>				
								<tr class="row2">
									<td>Authorize one or several sites but not authorize one or several sites from one or several merchants for one test access number</td>
									<td>Test Access Number</td>
									<td>Merchant A, Merchant B, ...</td>
									<td>Site A, Site B, ...</td>
									<td>Site C, Site D, ...</td>
								</tr>				
							</table>
						  </td>
						</tr>
						</table>
	     		        <td width="18" bgcolor="#ffffff"></td>
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#ffffff"><img src="/support/images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#ffffff" colspan="3"><img src="/support/images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>
</body>
</html>



