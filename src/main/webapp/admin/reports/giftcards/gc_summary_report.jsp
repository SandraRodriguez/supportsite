<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page=23;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%

  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;

  if (request.getParameter("search") != null)
  {
    if (TransactionReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));

      String strMerchantIds[] = request.getParameterValues("mids");

      if (strMerchantIds != null)
      {
        TransactionReport.setMerchantIds(strMerchantIds);
      }
      vecSearchResults = TransactionReport.getGiftCardSummary(SessionData, application);
    }
    else
    {
      searchErrors = TransactionReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.title",SessionData.getLanguage()).toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main">

<%
                out.println(Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.title",SessionData.getLanguage()));
                if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                  out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) +
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
%>
              </td>
              <td class=main align=right valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>
              </td>
            </tr>
            <tr>
             <td>
             	<form method="post" action="admin/reports/giftcards/gc_export.jsp">
                     <input type="submit" value="<%=Languages.getString("jsp.admin.reports.analisys.download",SessionData.getLanguage())%>">
					 <input type="hidden" value="<%=request.getParameter("startDate")%>" name="start">                    
					 <input type="hidden" value="<%=request.getParameter("endDate")%>" name="end">
					 <input type="hidden" value="<%=TransactionReport.getMerchantIds()%>" name="ids">
					 <input type="hidden" value="ByMerchant" name="reportType">
                  </form> 
             </td>
            </tr>
            
                  
            
            
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr>
                <td class=rowhead2>#</td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.activation_qty",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.activation_total",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.recharge_qty",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.recharge_total",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.redemption_qty",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.redemption_total",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.total_qty",SessionData.getLanguage()).toUpperCase() %>
                </td>
              </tr>
            </thead>
            <tbody>
<%
            double   dblActivationAmountSum   = 0;
            double   dblRechargeAmountSum    = 0;
            double   dblRedemptionAmountSum   = 0;
            int      intActivationQtySum      = 0;
            int      intRechargeQtySum       = 0;
            int      intRedemptionQtySum      = 0;
            int      intTotalQtySum           = 0;
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;

            if (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
/*
          vecTemp.add(NumberUtil.formatAmount(rs.getString("activation_qty")));
          vecTemp.add(NumberUtil.formatAmount(rs.getString("activation_total")));
          vecTemp.add(NumberUtil.formatAmount(rs.getString("recharge_qty")));
          vecTemp.add(NumberUtil.formatAmount(rs.getString("recharge_total")));
          vecTemp.add(NumberUtil.formatAmount(rs.getString("redemption_qty")));
          vecTemp.add(NumberUtil.formatAmount(rs.getString("redemption_total")));
          vecTemp.add(Integer.toString((int) quantity));
*/
              //summary totals are the first row of the vector
              intActivationQtySum      = Integer.parseInt(vecTemp.get(0).toString());
              dblActivationAmountSum   = Double.parseDouble(vecTemp.get(1).toString());
              intRechargeQtySum       = Integer.parseInt(vecTemp.get(2).toString());
              dblRechargeAmountSum    = Double.parseDouble(vecTemp.get(3).toString());
              intRedemptionQtySum      = Integer.parseInt(vecTemp.get(4).toString());
              dblRedemptionAmountSum   = Double.parseDouble(vecTemp.get(5).toString());
              intTotalQtySum           = Integer.parseInt(vecTemp.get(6).toString());

              while (it.hasNext())
              {
                vecTemp = null;
                vecTemp = (Vector)it.next();

                int intActivationQty        = Integer.parseInt(vecTemp.get(2).toString());
                double dblActivationAmount  = Double.parseDouble(vecTemp.get(3).toString());
                int intRechargeQty         = Integer.parseInt(vecTemp.get(4).toString());
                double dblRechargeAmount   = Double.parseDouble(vecTemp.get(5).toString());
                int intRedemptionQty        = Integer.parseInt(vecTemp.get(6).toString());
                double dblRedemptionAmount  = Double.parseDouble(vecTemp.get(7).toString());
                int intTotalQty             = Integer.parseInt(vecTemp.get(8).toString());

                out.print("<tr class=row" + intEvenOdd + ">" +
                            "<td>" + intCounter++ + "</td>" +
                            "<td nowrap>" + vecTemp.get(0).toString() + "</td>" +
                            "<td>" + vecTemp.get(1) + "</td>" +
                            "<td align=\"right\">" + intActivationQty + "</td>" +
                            "<td align=\"right\">" + NumberUtil.formatCurrency(Double.toString(dblActivationAmount)) + "</td>" +
                            "<td align=\"right\">" + intRechargeQty + "</td>" +
                            "<td align=\"right\">" + NumberUtil.formatCurrency(Double.toString(dblRechargeAmount)) + "</td>" +
                            "<td align=\"right\">" + intRedemptionQty + "</td>" +
                            "<td align=\"right\">" + NumberUtil.formatCurrency(Double.toString(dblRedemptionAmount)) + "</td>" +
                            "<td align=\"right\">" + intTotalQty + "</td></tr>");

                if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
              }
            }

%>
            </tbody>
            <tfoot>
              <tr class=row<%= intEvenOdd %>>
                <td colspan=3 align=right>
                  <%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                </td>
                <td align=right><%= intActivationQtySum %></td>
                <td align=right><%= NumberUtil.formatCurrency(Double.toString(dblActivationAmountSum)) %>
                <td align=right><%= intRechargeQtySum %></td>
                <td align=right><%= NumberUtil.formatCurrency(Double.toString(dblRechargeAmountSum)) %>
                <td align=right><%= intRedemptionQtySum %></td>
                <td align=right><%= NumberUtil.formatCurrency(Double.toString(dblRedemptionAmountSum)) %>
                <td align=right><%= intTotalQtySum %></td>
              </tr>
            </tfoot>
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "Number", "Number"],0,false,false);
  -->
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
