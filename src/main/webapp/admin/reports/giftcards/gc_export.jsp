<%@ page import="java.util.*" %>
<%
int section=3;
int section_page=4;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>

<%
   Vector vecSearchResults = new Vector();
   String strRuta="";
   int reportNumber=0;
   
   String cardNo="";
   String ids = request.getParameter("ids");
   String start = request.getParameter("start");
   String end = request.getParameter("end");
   String reportType = request.getParameter("reportType");
   
   TransactionReport.setStartDate(start);
   TransactionReport.setEndDate(end);
		
   if ( reportType.equals("ByMerchant") )
   {
		TransactionReport.setMerchantId(ids);
		vecSearchResults = TransactionReport.getGiftCardSummary(SessionData, application);
		reportNumber=17;
   }
   else if ( reportType.equals("ByProduct") )
   {
		TransactionReport.setProductIds(ids);
		vecSearchResults = TransactionReport.getGiftCardProductSummary(SessionData);
   		reportNumber=18;	
   }
   else if ( reportType.equals("Deactivation"))
   {
        vecSearchResults = TransactionReport.getGiftCardDeactivations(1, 900000, SessionData);
   		reportNumber=19;   
   }
   else if ( reportType.equals("BySearch"))
   {
   		//in this case is just one merchant
   	    TransactionReport.setMerchantId(ids);
   	    cardNo = request.getParameter("cardNo");
   	    TransactionReport.setCardNo(cardNo);
   		reportNumber=20;   		
   		vecSearchResults = TransactionReport.getGiftCardTransactions(1, 900000, SessionData);   
   }
   
   System.out.println("start :"+start+" end:"+end+" ids:"+ids+" reportType:"+reportType+" cardNo:"+cardNo);
   
   strRuta = TransactionReport.downloadReportCVS(this.getServletContext(),vecSearchResults,reportNumber,SessionData);
  
   if ( strRuta.length() > 0)
   {
     response.sendRedirect(strRuta);    
   }
%>
