<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.customers.Merchant,
                 com.debisys.utils.*" %>
<%
int section=4;
int section_page=23;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>

<%
Vector vecSearchResults = new Vector();
Hashtable searchErrors = null;
int intRecordCount = 0;
int intPage = 1;
int intPageSize = 50;
int intPageCount = 1;

int intActivationQty        = 0;
double dblActivationAmount  = 0;
int intRechargeQty          = 0;
double dblRechargeAmount    = 0;
int intRedemptionQty        = 0;
double dblRedemptionAmount  = 0;
double dblTotalBalance      = 0;

boolean showDownload = false;



if (request.getParameter("search") != null)
{
  if (request.getParameter("page") != null)
  {
    try
    {
      intPage=Integer.parseInt(request.getParameter("page"));
    }
    catch(NumberFormatException ex)
    {
      intPage = 1;
    }
  }

  if (intPage < 1)
  {
    intPage=1;
  }

  if (TransactionReport.validateGiftCardReport(SessionData))
  {
    vecSearchResults = TransactionReport.getGiftCardTransactions(intPage, intPageSize, SessionData);
    showDownload = vecSearchResults != null && vecSearchResults.size() > 0;
    
    boolean download = (request.getParameter("download")!=null?true:false);
    download=false;
       	 
    Vector vecTemp = (Vector) vecSearchResults.get(0);
    intRecordCount = Integer.parseInt(vecTemp.get(0).toString());

    intActivationQty     = Integer.parseInt(vecTemp.get(1).toString());
    dblActivationAmount  = Double.parseDouble(vecTemp.get(2).toString());
    intRechargeQty       = Integer.parseInt(vecTemp.get(3).toString());
    dblRechargeAmount    = Double.parseDouble(vecTemp.get(4).toString());
    intRedemptionQty     = Integer.parseInt(vecTemp.get(5).toString());
    dblRedemptionAmount  = Double.parseDouble(vecTemp.get(6).toString());
    dblTotalBalance      = (dblActivationAmount + dblRechargeAmount) - dblRedemptionAmount;

    vecSearchResults.removeElementAt(0);
    if (intRecordCount>0)
    {
      intPageCount = (intRecordCount / intPageSize) + 1;
      if ((intPageCount * intPageSize) + 1 >= intRecordCount)
      {
        intPageCount++;
      }
    }
    
  }
  else
  {
   searchErrors = TransactionReport.getErrors();
  }
}

%>
<%@ include file="/includes/header.jsp" %>
<%if (showDownload)
{ %>
		    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
		    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%} %>

<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}
</SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
  <tr>
	  <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
	  <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%=Languages.getString("jsp.admin.reports.giftcards.gc_lookup",SessionData.getLanguage()).toUpperCase()%></td>
	  <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
  </tr>
  <tr>
  	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">

	    
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea">
          <%=Languages.getString("jsp.admin.reports.giftcards.gc_search.instructions",SessionData.getLanguage())%>
              <form name="mainform" method="post" action="admin/reports/giftcards/gc_search.jsp" onSubmit="scroll();">
	          <table width="1000" height="120">
              <tr class="main">
               <td nowrap valign="top"><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
               <td nowrap valign="top"><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>:</td>
<%
					if(!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
				%>
               <td nowrap valign="top"><%=Languages.getString("jsp.admin.reports.giftcards.gc_lookup_merchant",SessionData.getLanguage())%>:</td>
                    <%} %>
               <td nowrap valign="top"><%=Languages.getString("jsp.admin.reports.giftcards.gc_lookup_card_number",SessionData.getLanguage())%>:</td>
               <td>&nbsp;</td>
               </tr>
                <tr class="main">
                    <td nowrap><input class="plain" name="startDate" value="<%=TransactionReport.getStartDate()%>" size="10"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
                    <td nowrap><input class="plain" name="endDate" value="<%=TransactionReport.getEndDate()%>" size="10"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
					<%
					if(!strAccessLevel.equals(DebisysConstants.MERCHANT)) {
				%>
                    <td>

                        <select name="merchantId">
                          <option value=""><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
                          <%
                            Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
                            Iterator it = vecMerchantList.iterator();
                            while (it.hasNext())
                            {
                              Vector vecTemp = (Vector) it.next();
                              if (TransactionReport.getMerchantId() != null && TransactionReport.getMerchantId().equals(vecTemp.get(0)))
                              {
                              out.println("<option value=" + vecTemp.get(0) +" selected>" + vecTemp.get(1) + "</option>");
                              }
                              else
                              {
                              out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
                              }
                            }

                          %>
                        </select>
					
                    </td>
                <%} else { //Missing merchantId param value on next screen (self-redirect) when logged on as Merchant
                    out.println("<td><input type=\"hidden\" name=\"merchantId\" value=\"" + SessionData.getProperty("ref_id") + "\"></td>"); 
                  }
                %>
                    <td><input type=text name="cardNo" value="<%=TransactionReport.getCardNo()%>" size="20" maxlength="50"></td>
                    <td>                        
                       <input type="hidden" name="search" value="y">
                       <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.search",SessionData.getLanguage())%>">
                        
                    </td>                    
                </tr>
                </table>
				</form>
<%
if (searchErrors != null)
{
	out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
	Enumeration enum1=searchErrors.keys();
	while(enum1.hasMoreElements())
	{
	  String strKey = enum1.nextElement().toString();
	  String strError = (String) searchErrors.get(strKey);
	  out.println("<li>" + strError);
	}

  out.println("</font></td></tr></table>");
}

  if (vecSearchResults != null && vecSearchResults.size() > 0)
{
%>	    
            <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr><td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
              if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
                {
                   out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
                }
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount-1)},SessionData.getLanguage())%></td></tr>
            <tr>
              <td align=right class="main" nowrap>
              <%
                String strUrl = "admin/reports/giftcards/gc_search.jsp?search=y&merchantId=" + URLEncoder.encode(request.getParameter("merchantId"), "UTF-8") + "&cardNo=" + URLEncoder.encode(TransactionReport.getProductId(),"UTF-8") + "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8");

              if (intPage > 1)
              {
                out.println("<a href=\""+strUrl+"&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
                out.println("<a href=\""+strUrl+ "&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
              }

              int intLowerLimit = intPage - 12;
              int intUpperLimit = intPage + 12;

              if (intLowerLimit<1)
              {
                intLowerLimit=1;
                intUpperLimit = 25;
              }

              for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
              {
                if (i==intPage)
                {
                  out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
                }
                else
                {
                  out.println("<a href=\"" + strUrl + "&page=" + i + "\">" +i+ "</a>&nbsp;");
                }
              }

              if (intPage < (intPageCount-1))
              {
                out.println("<a href=\"" + strUrl + "&page=" + (intPage+1) + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
                out.println("<a href=\""+ strUrl + "&page=" + (intPageCount-1) + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
              }

              %>
              </td>
              
            </tr>
             <tr>
             <td>
             	<form id="exportForm" method="post" action="admin/reports/giftcards/gc_export.jsp">
                     <input type="submit" value="<%=Languages.getString("jsp.admin.reports.analisys.download",SessionData.getLanguage())%>">
					 <input type="hidden" value="<%=TransactionReport.getStartDate()%>" name="start">                    
					 <input type="hidden" value="<%=TransactionReport.getEndDate()%>" name="end">
					 <input type="hidden" value="<%=TransactionReport.getMerchantId()%>" name="ids">
					 <input type="hidden" value="<%=TransactionReport.getCardNo()%>" name="cardNo">					 
					 <input type="hidden" value="BySearch" name="reportType">
                  </form> 
             </td>
            </tr>
            </table>
            <b><%=Languages.getString("jsp.admin.reports.giftcards.gc_search.summary",SessionData.getLanguage())%></b>
            <table width="600" cellspacing="1" cellpadding="2">
            <tr>
                <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.total_qty",SessionData.getLanguage()).toUpperCase()%></td>
                <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.activation_qty",SessionData.getLanguage()).toUpperCase() %></td>
                <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.activation_total",SessionData.getLanguage()).toUpperCase() %></td>
                <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.recharge_qty",SessionData.getLanguage()).toUpperCase() %></td>
                <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.recharge_total",SessionData.getLanguage()).toUpperCase() %></td>
                <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.redemption_qty",SessionData.getLanguage()).toUpperCase() %></td>
                <td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.redemption_total",SessionData.getLanguage()).toUpperCase() %></td>
                <%
                out.print("</tr><tr class=row1>" +
                            "<td>" + intRecordCount + "</td>" +
                            "<td>" + intActivationQty + "</td>" +
                            "<td>" + NumberUtil.formatCurrency(Double.toString(dblActivationAmount)) + "</td>" +
                            "<td>" + intRechargeQty + "</td>" +
                            "<td>" + NumberUtil.formatCurrency(Double.toString(dblRechargeAmount)) + "</td>" +
                            "<td>" + intRedemptionQty + "</td>" +
                            "<td>" + NumberUtil.formatCurrency(Double.toString(dblRedemptionAmount)) + "</td>");
                out.print("</tr>");
                %>
            </table>
            <br>
            <b><%=Languages.getString("jsp.admin.reports.giftcards.gc_search.transaction_detail",SessionData.getLanguage())%></b>
            <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
	            <tr class="SectionTopBorder">
	              <td class=rowhead2>#</td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.city_county",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.amount",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.giftcards.gc_search.current_card_balance",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></td>
	              <td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_type",SessionData.getLanguage()).toUpperCase()%></td>
	            </tr>
            </thead>
            <%
                  int intCounter = 1;
                  //intCounter = intCounter - ((intPage-1)*intPageSize);

                  Iterator it = vecSearchResults.iterator();
                  int intEvenOdd = 1;
                  while (it.hasNext())
                  {
                    Vector vecTemp = null;
                    vecTemp = (Vector) it.next();
                    out.println("<tr class=row" + intEvenOdd +">" +
                                "<td>" + intCounter++ + "</td>" +
                                "<td>" + vecTemp.get(0)+ "</td>" +
                                "<td>" + vecTemp.get(1) + "</td>" +
                                "<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString())  + "</td>" +
                                "<td>" + vecTemp.get(3) + "</td>" +
                                "<td nowrap>" + vecTemp.get(4) + "</td>" +
                                "<td>" + vecTemp.get(5) + "</td>" +
                                "<td>" + vecTemp.get(6) + "</td>" +
                                "<td align=right>" + vecTemp.get(7) + "</td>" +
                                "<td align=right>" + vecTemp.get(8) + "</td>" +
                                "<td nowrap>" + vecTemp.get(9) + "</td>" +
                                "<td>" + vecTemp.get(10) + "</td>" +
                                "<td>" + vecTemp.get(11) + "</td>" +
                                "<td>" + vecTemp.get(12) + "</td>" +
                        "</tr>");
                    if (intEvenOdd == 1)
                    {
                      intEvenOdd = 2;
                    }
                    else
                    {
                      intEvenOdd = 1;
                    }

                  }
                  vecSearchResults.clear();
            %>
            </table>
<SCRIPT type="text/javascript">
<!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None","Number", "Number", "CaseInsensitiveString", "Number", "Date", "CaseInsensitiveString","String","Number","Number","String","Number","Number","String"],0,false,false);
-->            
</SCRIPT>
<%
}
else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
{
 out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
}
%>

</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>