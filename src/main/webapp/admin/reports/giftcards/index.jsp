<%
int section=4;
int section_page=23;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="750" background="images/top_blue.gif">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td class="formAreaTitle" width="3000"><b><%=Languages.getString("jsp.admin.reports.giftcards.gift_card_reports",SessionData.getLanguage())%></b></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
 </tr>
 <tr>
 	<td colspan="3">
		
		<%@ include file="../qcommReportsFragment.jsp" %>
		
   	<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
 	    <tr>
 		      <td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
  		    <td align="center" valign="top" bgcolor="#FFFFFF">
   			    <table border="0" cellpadding="2" cellspacing="0" width="100%" align="center">
    			    <tr>
    				    <td width="18">&nbsp;</td>
     				    <td class="main">
                    <table border="0" cellpadding="4" cellspacing="0" width="700" height="1">
                      <tr>
                        <td VALIGN="top" WIDTH="376" height="28">
                          <font face="Arial, Helvetica, sans-serif" color="#2C0973" style="font-size: 13px"><a href="admin/reports/giftcards/gc_search.jsp" style="text-decoration: none">
                          - <%=Languages.getString("jsp.admin.reports.giftcards.gc_lookup",SessionData.getLanguage())%></font>
                        </td>
                      </tr>
                      <tr>
                        <td VALIGN="top" WIDTH="376" height="28">
                            <font face="Arial, Helvetica, sans-serif" color="#2C0973" style="font-size: 13px"><a href="admin/reports/giftcards/gc_summary.jsp" style="text-decoration: none">
                          - <%=Languages.getString("jsp.admin.reports.giftcards.gc_summary_report.title",SessionData.getLanguage())%></font>
                        </td>
                      </tr>
                      <tr>
                        <td VALIGN="top" WIDTH="376" height="28">
                           <font face="Arial, Helvetica, sans-serif" color="#2C0973" style="font-size: 13px"><a href="admin/reports/giftcards/gc_product.jsp" style="text-decoration: none">
                          - <%=Languages.getString("jsp.admin.reports.giftcards.gc_product_summary.title",SessionData.getLanguage())%></font>
                        </td>
                      </tr>
                      <tr>
                        <td VALIGN="top" WIDTH="376" height="28">
                           <font face="Arial, Helvetica, sans-serif" color="#2C0973" style="font-size: 13px"><a href="admin/reports/giftcards/gc_deactivation.jsp" style="text-decoration: none">
                          - <%=Languages.getString("jsp.admin.reports.giftcards.gc_deactivation.title",SessionData.getLanguage())%></font>
                        </td>
                      </tr>



                    </table>
     				    </td>
     		        <td width="18">&nbsp;</td>
        			</tr>
       			</table>
    		</td>
    		<td width="1" bgcolor="#003082"><img src="images/trans.gif" width="1"></td>
	  </tr>
	  <tr>
		  <td height="1" bgcolor="#003082" colspan="3"><img src="images/trans.gif" height="1"></td>
	  </tr>
 	</table>
 </td>
</tr>
</table>


<%@ include file="/includes/footer.jsp" %>


