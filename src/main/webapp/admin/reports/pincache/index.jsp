<%@page import="com.debisys.utils.*,
				java.util.*,java.util.ArrayList, java.io.PrintWriter,
				com.debisys.pincache.Pincache,
				com.debisys.users.User,
				java.text.DateFormat, java.text.SimpleDateFormat,
				org.apache.poi.hssf.usermodel.HSSFWorkbook" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<%
int section = 9;
int section_page = 23;
%>
<%@ include file="/includes/security.jsp" %>
<%
//Pincache.processRequest(SessionData, request, response);
if (SessionData.isLoggedIn()){
	User u = SessionData.getUser();
	String action = request.getParameter("action");
	if(action != null && action.equals("download")){
		String type = request.getParameter("type");
		if(type.equals("xls")){
			response.setContentType("application/vnd.ms-excel");
			HSSFWorkbook workbook = Pincache.getReportXLS(u.getRefId(), SessionData.getLanguage(), strDistChainType, strAccessLevel);
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
			Date date = new Date();
			response.addHeader("Content-Disposition", "attachment; filename=CachedPinsReport-"+ dateFormat.format(date) +".xls");
			ServletOutputStream oo = response.getOutputStream();
			try {
				workbook.write(oo);
				oo.flush();
			}
			catch (Exception ioe) {}
			out.close();
			return;
		}else if(type.equals("csv")){
			StringBuilder results = Pincache.getReportCSV(u.getRefId(), SessionData.getLanguage(), strDistChainType, strAccessLevel);
			response.setContentType("application/text");
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
			Date date = new Date();
			response.addHeader("Content-Disposition", "attachment; filename=CachedPinsReport-"+ dateFormat.format(date) +".csv");
			PrintWriter pw = response.getWriter();
			pw.write(results.toString());
			pw.flush();
			pw.close();
		}
	}else if(action != null && action.equals("details")){
		ArrayList<String> headers = new ArrayList<String>();
		headers.add(Languages.getString("jsp.admin.pincache.details.header1", SessionData.getLanguage()));
		headers.add(Languages.getString("jsp.admin.pincache.details.header2", SessionData.getLanguage()));
		headers.add(Languages.getString("jsp.admin.pincache.details.header3", SessionData.getLanguage()));
		headers.add(Languages.getString("jsp.admin.pincache.details.header4", SessionData.getLanguage()));
		headers.add(Languages.getString("jsp.admin.pincache.details.header5", SessionData.getLanguage()));
		
		ArrayList<ArrayList<String>> data = Pincache.getPincacheDetails(request.getParameter("product_id"));
%>
			<div align="right"><table class="reportDetails">
				<thead>
					<tr>
<%
		for(String cell: headers){
%>
						<th><%=cell%></th>
<%
		}
%>
					</tr>
				</thead>
				<tbody>
<%
		for(ArrayList<String> row: data){
%>
					<tr>
						<td><%=row.get(0)%></td>
						<td><%=row.get(1)%></td>
						<td><%=row.get(2)%></td>
						<td><%=row.get(3)%></td>
						<td align="right"><%=row.get(4)%></td>
					</tr>
<%
		}
%>

				</tbody>
			</table></div>
<%
	}else{
		ArrayList<String> headers = Pincache.getReportHeaders(SessionData.getLanguage(), strDistChainType);
		ArrayList<ArrayList<String>> data = Pincache.getPincacheReport(u.getRefId(), strDistChainType, strAccessLevel);
		ArrayList<String> errors = new ArrayList<String>();
		if(action != null && action.equals("download")){
	
	
		}else{

%>
<%@ include file="/includes/header.jsp" %>
<style>
	.reportDetails{
		border: 1px solid black;
	}
</style>
<link href="css/jquery.dataTables.css" type="text/css" rel="stylesheet">
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
var cancelButtonText = '<%=Languages.getString("jsp.admin.pincache.cancel",SessionData.getLanguage())%>';
var okButtonText = '<%=Languages.getString("jsp.admin.pincache.ok",SessionData.getLanguage())%>';

function fnFormatDetails ( oTable, nTr, product_id){
	var aData = oTable.fnGetData( nTr );
	var sOut = '';
	$.ajax({
		url: '/support/admin/reports/pincache/index.jsp?action=details&product_id=' + product_id,
		async: false,
		success: function(data){
			sOut = data;
		}
	});
	return sOut;
}

$(document).ready(function() {
	$('.download-report').button({icons: {primary: 'ui-icon-circle-arrow-s'}}).click(function() {
		//$('#downloadDialog').dialog( "open" );
		window.open('/support/admin/reports/pincache/index.jsp?action=download&type=' + $(this).data('type'));
	});
	var oTable = $('#report').dataTable({
		"sPaginationType": "full_numbers",
		"oLanguage": {
			"sSearch": '<%=Languages.getString("jsp.admin.pincache.report.filter",SessionData.getLanguage())%>',
			"sFirst": "First page",
			"sLast": "Last page",
			"sNext": "Next page",
			"sPrevious": "Previous page",
			"sInfoFiltered": " - filtering from _MAX_ records"
		}		
	});
	$('.details-button').live('click', function () {
		var nTr = $(this).parents('tr')[0];
		if ( oTable.fnIsOpen(nTr) ){
			this.src = "/support/images/details_open.png";
			oTable.fnClose( nTr );
		}else{
			this.src = "/support/images/details_close.png";
			oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr, $(this).data('productid')), 'details' );
		}
	} );
	$("#report thead").addClass("ui-widget-header");
});
</script>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.pincache.report",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
</table>
<%
			if(errors != null && !errors.isEmpty()){
				for(String error:errors){
%>
	<div class="error"><%=error%></div>
<%
				}
			}
%>
<br>
<br>
<div>
<%
			if(data != null && !data.isEmpty()){
%>
			<div align="right">
				<button class="download-report" data-type="csv">
					<img src="images/doc_excel_csv.png" />
					<%=Languages.getString("jsp.admin.pincache.report.downloadCSV",SessionData.getLanguage())%>
				</button>
				<button class="download-report" data-type="xls">
					<img src="images/page_excel.png"/>
					<%=Languages.getString("jsp.admin.pincache.report.downloadXLS",SessionData.getLanguage())%>
				</button>
			</div>
			<br>
			<br>
			<table id="report" >
				<thead>
					<tr>
<%
				for(String cell: headers){
%>
						<th><%=cell%></th>
<%
				}
%>
					</tr>
				</thead>
				<tbody>
<%
				String s = "";
				for(ArrayList<String> row: data){
%>
					<tr>
<%
					for(int i = 1; i < row.size(); i++){
						if(i >= row.size() - 2){
							s = "align=\"right\"";
						}else{
							s = "";
						}
%>
						<td <%=s%>><%=row.get(i)%></td>
<%
					}
%>
						<td><img class="details-button" src="images/details_open.png" data-productid="<%=row.get(0)%>"></td>
					</tr>
<%
				}
%>

				</tbody>
			</table>
			<br>
			<br>
			<div align="right">
				<button class="download-report" data-type="csv">
					<img src="images/doc_excel_csv.png" />
					<%=Languages.getString("jsp.admin.pincache.report.downloadCSV",SessionData.getLanguage())%>
				</button>
				<button class="download-report" data-type="xls">
					<img src="images/page_excel.png"/>
					<%=Languages.getString("jsp.admin.pincache.report.downloadXLS",SessionData.getLanguage())%>
				</button>
			</div>
<%
			}else{
%>
			<div class="warning"><%=Languages.getString("jsp.admin.pincache.report.noresults",SessionData.getLanguage())%></div>
<%
			}
%>
</div>
<!-- <div id="downloadDialog">
	<div class="warning">
		<%=Languages.getString("jsp.admin.pincache.report.noresults",SessionData.getLanguage())%>
	</div>
</div> -->
<%@ include file="/includes/footer.jsp" %>
<%
		}
	}
}
%>
