<%@page import="com.debisys.utils.*,
				java.util.*,java.util.ArrayList, java.io.PrintWriter,
				com.debisys.pincache.PcTermlSync,
				com.debisys.users.User,
				java.text.DateFormat, java.text.SimpleDateFormat,
				org.apache.poi.hssf.usermodel.HSSFWorkbook" pageEncoding="ISO-8859-1"%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="Terminal" class="com.debisys.terminals.Terminal" scope="request"/>
<jsp:useBean id="RatePlan" class="com.debisys.rateplans.RatePlan" scope="request"/>
<jsp:useBean id="LogChanges" class="com.debisys.utils.LogChanges" scope="request" />
<%
int section = 9;
int section_page = 23;
%>
<%@ include file="/includes/security.jsp" %>
<%
//Pincache.processRequest(SessionData, request, response);
boolean showCriteria = true;
if (SessionData.isLoggedIn()){
	User u = SessionData.getUser();
	String siteId = request.getParameter("site_id");
	String sStartDate = request.getParameter("startdate");
	String sEndDate = request.getParameter("enddate");
	String dialogMode = request.getParameter("dialogMode");
	if(siteId == null){
		siteId = "";
	}
	if(dialogMode != null && dialogMode.equals("1")){
		showCriteria = false;
	}
	if(sStartDate == null || sEndDate == null){
		sStartDate = "";
		sEndDate = "";
	}
	ArrayList<String> headers = PcTermlSync.getReportHeaders(SessionData.getLanguage());
	PcTermlSync.setFilterSiteId(siteId);
	PcTermlSync.setFilterStartDate(sStartDate);
	PcTermlSync.setFilterEndDate(sEndDate);
	PcTermlSync.setFilterRepId(u.getRefId());
	ArrayList<ArrayList<String>> data = PcTermlSync.getSyncReport(strDistChainType, strAccessLevel);
	ArrayList<String> errors = new ArrayList<String>();
	if(showCriteria){
		%>
		<%@ include file="/includes/header.jsp" %>
		<%
	}
%>
<style>
	.reportDetails{border: 1px solid black;}
	#searchcriteria label{font-weight: bold; min-width: 200px; display: inline-block;}
</style>
<link href="css/jquery.dataTables.css" type="text/css" rel="stylesheet">
<script src="/support/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
var cancelButtonText = '<%=Languages.getString("jsp.admin.pincache.cancel",SessionData.getLanguage())%>';
var okButtonText = '<%=Languages.getString("jsp.admin.pincache.ok",SessionData.getLanguage())%>';

$(document).ready(function() {
	$('.download-report').button({icons: {primary: 'ui-icon-circle-arrow-s'}}).click(function() {
		//$('#downloadDialog').dialog( "open" );
		window.open('/support/admin/reports/pincache/index.jsp?action=download&type=' + $(this).data('type'));
	});
	var oTable = $('#report').dataTable({
		"sPaginationType": "full_numbers",
		"oLanguage": {
			"sSearch": '<%=Languages.getString("jsp.admin.pincache.report.filter",SessionData.getLanguage())%>',
			"sFirst": "First page",
			"sLast": "Last page",
			"sNext": "Next page",
			"sPrevious": "Previous page",
			"sInfoFiltered": " - filtering from _MAX_ records"
		}		
	});
	$('#report thead').addClass("ui-widget-header");
	$('.datepicker').datepicker();
	$('button').button();
});
</script>
<%
	if(showCriteria){
%>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
		<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
		<td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.pincache.reportsync",SessionData.getLanguage()).toUpperCase()%></td>
		<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
	</tr>
</table>
<%
		if(errors != null && !errors.isEmpty()){
			for(String error:errors){
%>
	<div class="error"><%=error%></div>
<%
			}
		}
%>
<br>
<div class="ui-widget" id="searchcriteria">
	<div class="ui-widget-header ui-corner-top"><strong>&nbsp;</strong></div>
	<div class="ui-widget-content ui-corner-bottom">
		<p>
			<form action="/support/admin/reports/pincache/sync.jsp" method="post">
				<label for="site_id"><%=Languages.getString("jsp.admin.pincache.reportsync.header1",SessionData.getLanguage())%></label>
				<input name="site_id" type="text" value="<%=siteId%>" class="text ui-widget-content ui-corner-all" >
				<br>
				<br>
				<label for="siteId"><%=Languages.getString("jsp.admin.pincache.reportsync.header2",SessionData.getLanguage())%></label>
				<div style="padding-left: 20px;">
					<label for="startdate"><%=Languages.getString("jsp.admin.pincache.reportsync.startDate",SessionData.getLanguage())%></label>
					<input name="startdate" type="text" value="<%=sStartDate%>" class="datepicker text ui-widget-content ui-corner-all"><br>
					<label for="enddate"><%=Languages.getString("jsp.admin.pincache.reportsync.endDate",SessionData.getLanguage())%></label>
					<input name="enddate" type="text" value="<%=sEndDate%>" class="datepicker text ui-widget-content ui-corner-all"><br>
				</div>
				<div style="text-align: right">
					<button><%=Languages.getString("jsp.admin.pincache.search",SessionData.getLanguage())%></button>
				</div>
			</form>
		</p>
	</div>
</div>
<%
	}
%>
<br>
<div>
<%
	if(data != null && !data.isEmpty()){
%>
			<table id="report" >
				<thead>
					<tr>
<%
		for(String cell: headers){
%>
						<th><%=cell%></th>
<%
		}
%>
					</tr>
				</thead>
				<tbody>
<%
		String s = "";
		for(ArrayList<String> row: data){
%>
					<tr>
<%
			for(int i = 0; i < row.size(); i++){
%>
						<td <%=s%>><%=row.get(i)%></td>
<%
			}
%>
					</tr>
<%
		}
%>

				</tbody>
			</table>
<%
	}else{
%>
			<div class="warning"><%=Languages.getString("jsp.admin.pincache.report.noresults",SessionData.getLanguage())%></div>
<%
	}
%>
</div>
<!-- <div id="downloadDialog">
	<div class="warning">
		<%=Languages.getString("jsp.admin.pincache.report.noresults",SessionData.getLanguage())%>
	</div>
</div> -->
<%
	if(showCriteria){
		%>
		<%@ include file="/includes/footer.jsp" %>
		<%
	}
}
%>
