<%@page import="java.text.DecimalFormat"%>
<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 4;
  int section_page = 21;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
    <jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
  <jsp:setProperty name="TransactionReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";

  if (request.getParameter("search") != null)
  {
   
     vecSearchResults = TransactionReport.getProviderExternalBalance(SessionData);

  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="420">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= Languages.getString("jsp.admin.reports.title51",SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.accountName",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.providerExternalBalance",SessionData.getLanguage()).toUpperCase() %>
                </td>  
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.providerExternalTimeServer",SessionData.getLanguage()).toUpperCase() %>
                </td>               
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.providerExternalTime",SessionData.getLanguage()).toUpperCase() %>
                </td>
              </tr>
            </thead>
<%
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
              
              double  totalsales=0;
              double totalpayments=0;
              
            while (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
              DecimalFormat df = new DecimalFormat("#");
              df.setMaximumFractionDigits(2);
              out.print("<tr class=row" + intEvenOdd + ">" + 
              "<td>" + intCounter++ + "</td>" + 
              "<td nowrap>" + vecTemp.get(0) + "</td>" + 
              "<td nowrap align=\"right\">" + vecTemp.get(3)+"&nbsp;"+ df.format(Double.parseDouble(vecTemp.get(1).toString())) + "</td>" + 
              "<td nowrap align=\"right\">" + vecTemp.get(4) + "</td>" +
              "<td nowrap align=\"right\">" + vecTemp.get(2) + "</td></tr>");
              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }
            }

%> 
          </table>
<%

        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
          out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.Provider_External_Balance_no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
            
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "Date", "Number", "Number", "Number", "Number"],0,false,false);
  -->
            
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
