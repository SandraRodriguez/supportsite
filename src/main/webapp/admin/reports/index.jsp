<%@page import="com.debisys.utils.HTMLEncoder, com.debisys.presentation.ReportGroup"%>
<%
    // Warning! If section or section_page has been modified,
    // Please adjust header.jsp accordingly
    int section = 4;
    int section_page = 1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<%    ReportGroup a = ReportGroup.createAnalysisGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup t = ReportGroup.createTransactionsGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup aa = ReportGroup.createCarrierGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup tt = ReportGroup.createRoamingGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup pc = ReportGroup.createPincacheGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup ach = ReportGroup.createACHGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup pinStock = ReportGroup.createPINStockGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup scheduleTool = ReportGroup.createScheduleToolGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup spiff = ReportGroup.createSpiffGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup balanceHistory = ReportGroup.createBalanceHistoryGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup internalR = ReportGroup.createInternalReportsGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    ReportGroup flowBusinessIntelligence = ReportGroup.createFlowBusinessIntelligence(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType);
    SessionData.removeProperty(DebisysConstants.SC_SESS_VAR_NAME);
%>
<table cellSpacing=0 cellPadding=0 width="750" background=images/top_blue.gif border=0>
    <tr>
        <td width=23 height=20><IMG height=20 src="images/top_left_blue.gif" width=18></td>
        <td width="2000" class=main><B class="formAreaTitle"><%=Languages.getString("jsp.admin.reports.reports", SessionData.getLanguage()).toUpperCase()%></B></td>
        <td height=20><IMG src="images/top_right_blue.gif"></td>
    </tr>
    <tr>
        <td colSpan=3>
            <table cellSpacing=0 cellPadding=0 width="100%" bgColor=#7B9EBD border=0>
                <tr>
                    <td width=1 bgColor=#7B9EBD>
                        <img src="images/trans.gif" width=1>
                    </td>
                    <td vAlign=top align=middle bgColor=#ffffff>
                        <table width="100%" border=0 align=center cellPadding=2 cellSpacing=0 class="fondoceldas">
                            <tr>
                                <td class=main align="left">
                                    <%@ include file="qcommReportsFragment.jsp" %>
                                    <%
                                        if (t.hasItems()) {
                                    %>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/transaction_cube.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.transactions", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main"><%t.showItem(out);%></ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22  src="images/analysis_cube.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.analysis", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% a.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>

                                    <%
    if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {%>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <TH align=left>
                                                <img height=22 src="images/carrier.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.carrier_user.carrier", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </TH>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% aa.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)) {%>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/roaming.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.includes.menu.roaming_etopups", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% tt.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                    <%
                                    // pincache
                                        if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.PERM_VIEW_CACHED_PINS_REPORTS)) {
                                    %>


                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/nodebox.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.pincache.title", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% pc.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>

                                    <%
                                        if (ach.hasItems()) {
                                    %>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/nodebox.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.ach.title.group", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% ach.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>		
                                    <%
                                    // pinstock
                                        if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.PERM_ENABLE_PIN_STOCK_REPORT)) {
                                    %>


                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/nodebox.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.pinstock.title.group", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% pinStock.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                    <%
                                    	boolean isInternational = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)
                                    		&& DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
                                        boolean permissionFlag = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_SCHEDULE_REPORTS) 
                                                && SessionData.checkPermission(DebisysConstants.PERM_VIEW_MANAGED_SCHEDULE_REPORTS);
                                        if (permissionFlag && isInternational) {
                                    %>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/nodebox.png" width=22 border=0>
                                                <span class="main">
                                                    Manage Schedule Reports
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% scheduleTool.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                    <%
                                        if (deploymentType.equals(DebisysConstants.DEPLOYMENT_DOMESTIC)) {
                                    %>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/nodebox.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.spiffReports.group.title", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% spiff.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                    <%
                                        if (SessionData.checkPermission(DebisysConstants.PERM_ALLOW_AUDIT_REPORT_MANAGEMENT)) {
                                    %>
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/nodebox.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.balanceAudit.group.title", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% balanceHistory.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>                                                                                                        
                                    <%
                                        }
                                    %>
                                    
                                    <!--janez-->
                                    <%
                                    if (internalR.hasItems() && SessionData.checkPermission(DebisysConstants.ISO) && 
                                            SessionData.checkPermission(DebisysConstants.PERM_VIEW_INTERNAL_REPORTS)) {
                                    %>                                    
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/carrier.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.tools.internal.report.title", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% internalR.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                    <!--janez-->
                                    <%
                                    if (SessionData.checkPermission(DebisysConstants.ISO) && SessionData.checkPermission(DebisysConstants.PERM_CWC_FLOW_STREETLIGHT_REPORTS)) {
                                    %>                                    
                                    <table class=reportGroupTable>
                                        <tr>
                                            <th align=left>
                                                <img height=22 src="images/carrier.png" width=22 border=0>
                                                <span class="main">
                                                    <%=Languages.getString("jsp.admin.reports.flowBusinessIntelligence.group.title", SessionData.getLanguage()).toUpperCase()%>
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ul class=sublevel name="main">
                                                    <span class="main">
                                                        <% flowBusinessIntelligence.showItem(out); %>
                                                    </span>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <%
                                        }
                                    %>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width=1 bgColor=#7B9EBD>
                        <img src="images/trans.gif" width=1>
                    </td>
                </tr>
                <tr>
                    <td bgColor=##7B9EBD colSpan=3 height=1>
                        <img height=1 src="images/trans.gif">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<%@ include file="/includes/footer.jsp" %>