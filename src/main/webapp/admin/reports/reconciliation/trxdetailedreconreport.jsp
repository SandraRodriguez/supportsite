<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.reports.TransactionReport,
                 com.debisys.reports.reconciliation.*" %>
                 
<%@page import="com.debisys.utils.TimeZone"%>

<jsp:useBean id="TransactionSearch" class="com.debisys.ach.TransactionSearch" scope="request"/>
<jsp:useBean id="ReconciliationWithBonus" class="com.debisys.reports.reconciliation.ReconciliationWithBonus" scope="request"/>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:setProperty name="ReconciliationWithBonus" property="*"/>

<%
int section=4;
int section_page=57;
String titleReport57  = Languages.getString("jsp.admin.reports.reconciliation.trxdetconreport",SessionData.getLanguage()).toUpperCase();
%>

<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<SCRIPT LANGUAGE="JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");

function GoToPage(nPage)
{
	document.getElementById('varPage').value = nPage;
	document.getElementById('submit').click();	
}
function GoToDownload()
{
	document.getElementById('varPage').value = "d";
		
}
function ChangeVarPage()
{
	document.getElementById('varPage').value = "s";
		
}
</SCRIPT>
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%= titleReport57 %></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form id="mainform" name="mainform" method="post" action="admin/reports/reconciliation/trxdetailedreconreport.jsp"  >
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
<%
	String showLabelValue = Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage());
	String downloadLabelValue = Languages.getString("jsp.admin.reports.reconciliation.withbonus.download",SessionData.getLanguage());
	
	//System.out.println("request.getMethod() "+request.getMethod());	
	boolean download = false;
	ArrayList<ReconciliationWithBonus> arrReconciliationWithBonus = null;
	int nPage = 1;
	int nPageCount = 1;
	int nRecordCount = 0;
	int nPageSize = 10;
	boolean showError = false;
	String strErrorMsg = "";
	
	if ( request.getMethod().equals("POST"))
	{
	    String pageRequest = request.getParameter("page");
		if ( pageRequest.equals("d") )
		{
			download = true;
		}
		else
		{
			try
			{
				nPage = Integer.parseInt(request.getParameter("page"));
				if (nPage < 1) { nPage = 1; }
			}
			catch (Exception ex)
			{
				nPage = 1;
			}
		}
 
		ReconciliationWithBonus.setSummaryOrDetailed(false);
		ReconciliationWithBonus.setSessionData(SessionData);
		ReconciliationWithBonus.setContext(application);
		ReconciliationWithBonus.setTitle(titleReport57);
		
		strErrorMsg = TransactionSearch.validateDateRange(ReconciliationWithBonus.getStartDate(),ReconciliationWithBonus.getEndDate(),SessionData);
		if(strErrorMsg.equals(""))
    	{	 
			if ( !download )
			{
			    ReconciliationWithBonus.setDownload(false);
			    ReconciliationWithBonus.setnPageNumber(nPage);
			    ReconciliationWithBonus.getReconciliationWithBonusReport();
				arrReconciliationWithBonus = ReconciliationWithBonus.getArrReport();
				nRecordCount = ReconciliationWithBonus.recordCountLastQuery();
				nPageSize = ReconciliationWithBonus.getnRecordsPerPage();
	    	    if (nRecordCount > 0)
				{
					nPageCount = (nRecordCount / nPageSize);
					if ((nPageCount * nPageSize) < nRecordCount)
					{
						nPageCount++;
					}
				}			
			}
			else
			{
				ReconciliationWithBonus.setDownload(true);
				ReconciliationWithBonus.getReconciliationWithBonusReportDownload();
				response.sendRedirect(ReconciliationWithBonus.getDownloadUrl());
			}	
		 }
		 else
		 {
		   showError = true;		
		 }	
	}
	else
	{
	    //*****************************************************
		//NOTHING BY NOW
		//*****************************************************
	}

    boolean showButtonDownloadAndTable= ( arrReconciliationWithBonus!=null && arrReconciliationWithBonus.size()>0 );
    
	Vector vTimeZoneData = null;
	if ( SessionData.getProperty("access_level").equals(DebisysConstants.MERCHANT) )
	{
		vTimeZoneData = TimeZone.getTimeZoneByMerchant(Long.parseLong(SessionData.getProperty("ref_id")));
	}
	else
	{
		vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
	}
%>
              <tr class="main"><td nowrap><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
              <tr class="main">
               <td nowrap valign="top"><%if( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO)){ %>
		         <%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%>:</td>
		         <%}else{ %>
		         <%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%>:</td>
		         <%}%><td>&nbsp;</td>
               </tr>
               <tr>
               <td valign="top" nowrap>
					<table>
					<tr class="main">
					    <td><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:<input class="plain" name="startDate" id="startDate" value="<%=ReconciliationWithBonus.getStartDate()%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
					</tr>
					<tr class="main">
					    <td><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: <input class="plain" name="endDate" id="endDate" value="<%=ReconciliationWithBonus.getEndDate()%>" size="12"><a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS><img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt=""></a></td>
					</tr>
					<%
					 if ( strAccessLevel.equals(DebisysConstants.ISO) && false )
					 {
					 %>
						<TR CLASS="main">
						  <TD VALIGN="top" NOWRAP><%=Languages.getString("jsp.admin.reports.transactions.merchants.option",SessionData.getLanguage())%></TD>
						  <TD VALIGN="top">
						    <SELECT NAME="merchantIds" SIZE=10 MULTIPLE>
						      <OPTION VALUE="" SELECTED><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></OPTION>
									<%
									    Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
									    Iterator it = vecMerchantList.iterator();
									    while (it.hasNext())
									    {
									      Vector vecTemp = null;
									      vecTemp = (Vector) it.next();
									      out.println("<OPTION VALUE=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</OPTION>");
									    }
									%>
						    </SELECT>
						    <BR>
						    *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%>
						  </TD>
						</TR>
					<%
					 }
					 if ( false)
					 {
					%>    
						<tr>
						    <td class=main valign=top nowrap><%=Languages.getString("jsp.admin.reports.transactions.products.option",SessionData.getLanguage())%></td>
						    <td class=main valign=top>
						        <select name="pids" size="10" multiple>
						          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
										<%
										  Vector vecProductList = TransactionReport.getProductList(SessionData, null);
										  Iterator it = vecProductList.iterator();
										  while (it.hasNext())
										  {
										    Vector vecTemp = null;
										    vecTemp = (Vector) it.next();
										    out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "("+vecTemp.get(0)+")</option>");
										  }
										
										%>
						        </select>
						        <br>
						        *<%=Languages.getString("jsp.admin.reports.transactions.products.instructions",SessionData.getLanguage())%>
						   </td>
						</tr>
					<%
					 } 
					%> 
					<tr>
					    <td class=main align="left" colspan="6">
					      <input type="hidden" id="varPage" name="page" value="1">
					      <input type="submit" id="submit" name="submit" onmouseover="javascript:ChangeVarPage()" value="<%=showLabelValue%>"  >
					      <%  					
						   if ( showButtonDownloadAndTable ) 
						   {
						   %>	
							<input type="submit" value="<%=downloadLabelValue%>" onclick="javascript:GoToDownload()">
						   <%  					
						   }
						   %>	
					    </td>
					</tr>
					<tr>
						<td class="main" colspan="6">
						* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
						</td>
					</tr>
					<%  					
				    if ( ReconciliationWithBonus.getWarnnigs() != null ) 
				    {
				    %>	
					 <tr>
						<td class="main" >
						* <%=ReconciliationWithBonus.getWarnnigs()%>
						</td>
					</tr>
				    <%  					
				    }
				    %>
					
				<%  	
					if ( showError )
					{
					  out.println("<tr><td class=main align=left><br><br><font color=ff0000>"  + strErrorMsg + "</font><br><br></td></tr>");
					}									
					if ( showButtonDownloadAndTable ) 
					{
				%>						
						<tr>				    	
							<td colspan="6" align=right class="main" nowrap>
							<%
							if (nPage > 1)
							{
							%>
								<a href="javascript:GoToPage(1)"><%=Languages.getString("jsp.admin.first",SessionData.getLanguage())%></a>
								<a href="javascript:GoToPage(<%=nPage - 1%>)">&lt;&lt;<%=Languages.getString("jsp.admin.previous",SessionData.getLanguage())%></a>
							<%
							}		
							int nLowerLimit = nPage - 12;
							int nUpperLimit = nPage + 12;					
							if (nLowerLimit < 1)
							{
								nLowerLimit = 1;
								nUpperLimit = 25;
							}		
							for (int i = nLowerLimit; i <= nUpperLimit && i <= nPageCount; i++)
							{
								if (i == nPage)
								{
									out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
								}
								else
								{
									%>
									<a href="javascript:GoToPage(<%=i%>)"><%=i%></a>
									<%
								}
							}		
							if (nPage <= (nPageCount - 1))
							{
								%>
								<a href="javascript:GoToPage(<%=nPage + 1%>)"><%=Languages.getString("jsp.admin.next",SessionData.getLanguage())%>&gt;&gt;</a>
								<a href="javascript:GoToPage(<%=nPageCount%>)"><%=Languages.getString("jsp.admin.last",SessionData.getLanguage())%></a>
								<%
							}		
							%>
						 </td>
					   </tr>
					   
					   <tr>
						 <td class="main" colspan=3>
							 <table cellspacing="2" cellpadding="1" id="t1">
	   	      	 				<tr class="SectionTopBorder">
				           		  <td class="rowhead2">#</td>
				           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.reconciliation.withbonus.date",SessionData.getLanguage()).toUpperCase()%></td>
				           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.reconciliation.withbonus.type",SessionData.getLanguage()).toUpperCase()%></td>
				           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.reconciliation.withbonus.value",SessionData.getLanguage()).toUpperCase()%></td>
				           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.reconciliation.withbonus.TrxNumber",SessionData.getLanguage()).toUpperCase()%></td>
				           		  <td class="rowhead2"><%=Languages.getString("jsp.admin.reports.reconciliation.withbonus.MerchantId",SessionData.getLanguage()).toUpperCase()%></td>
				           		 </tr>
				           		<%	
				           				           		
								for(ReconciliationWithBonus withBonus : arrReconciliationWithBonus)
								{
							    %>
									<tr>
										<td class="main" align="center" ><%= withBonus.getId() %></td>
										<td class="main" align="center"><%= withBonus.getDate()%></td>
										<td class="main" align="center"><%= withBonus.getTrxType()%></td>
										<td class="main" align="center"><%= withBonus.getAmnTrx()%></td>
										<td class="main" align="center"><%= withBonus.getTransactionId()%></td>
										<td class="main" align="center"><%= withBonus.getMerchantId()%></td>
									</tr>
							
							   <% 
							    }%>
		           	    	</table>
						</td>								
					   </tr>
					<%
					}
					%>
					</table>
               </td>
              </tr>
              </form>
            </table>

          </td>
      </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>
