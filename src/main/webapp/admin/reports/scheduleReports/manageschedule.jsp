<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.TimeZone,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.schedulereports.ScheduleReport,
                 com.debisys.schedulereports.ScheduleConfigurationJob,
                 com.debisys.schedulereports.SchedulerProperties,
                 com.debisys.schedulereports.AdminScheduleJob,
                 com.debisys.schedulereports.JobUI" %>
                 
<%@page import="com.jaspersoft.jasperserver.ws.scheduling.ReportSchedulerSoapBindingStub"%>
<%@page import="java.net.URL"%>
<%
int section=4;
int section_page=59;
//.toolTip {padding-right: 20px;background:url(images/help.gif) no-repeat right;color: #3366FF;cursor: help;position: relative; with: 18px; height: 18px; display: block; }
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<style>
	.toolTip {padding-right: 20px;background:url(images/help.gif) no-repeat left;color: #3366FF;cursor: help;position: relative; with: 18px; height: 18px; display: block; }
	.toolTipWrapper {width: 375px;position: absolute;top: -20px; color: #FFFFFF; padding-left:40px;font-weight: bold;font-size: 9pt; z-index: 100;}
	.toolTipTop {width: 175px;height: 30px;}
	.toolTipMid {padding: 8px 15px;background: #A1D40A url(images/bubbleMid.gif) repeat-x top;}
	.toolTipBtm {height: 1px;background: url(images/bubbleBtmLarge.gif) }	
</style>

<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>

<% 
	String userId = SessionData.getUser().getPasswordId();
	String title = Languages.getString("jsp.admin.reports.scheduleReport.manageSchedule",SessionData.getLanguage()).toUpperCase();
	
	
	String reportGroupLabel = Languages.getString("jsp.admin.reports.managescheduleteports.reportGroup", SessionData.getLanguage() );
	String creationDateLabel = Languages.getString("jsp.admin.reports.managescheduleteports.creationDate", SessionData.getLanguage() );
	String notificationEmailsLabel = Languages.getString("jsp.admin.reports.managescheduleteports.notificationEmails", SessionData.getLanguage() );
	String informationLabel = Languages.getString("jsp.admin.reports.managescheduleteports.information", SessionData.getLanguage() );
	String deleteLabel = Languages.getString("jsp.admin.reports.managescheduleteports.delete", SessionData.getLanguage() );
    String reportJob = Languages.getString("jsp.admin.reports.managescheduleteports.reportJob", SessionData.getLanguage() );

	String action = request.getParameter("action");
	String idReport = request.getParameter("idReport");
	if ( action != null && action.equals("d") && idReport != null )
	{
		AdminScheduleJob.deleteJobById( application, idReport );
	}
    ArrayList<JobUI> jobsList = AdminScheduleJob.listJobByUserId( SessionData, application, request, userId  );
%>
<SCRIPT LANGUAGE="JavaScript">
	
	$(function() {
		//$( document ).tooltip();
		$(document).ready(function(){
			$('.toolTip').hover(
				function() {
					this.tip = this.title;
					$(this).append('<div class="toolTipWrapper"><div class="toolTipTop"></div><div class="toolTipMid">'+this.tip+'</div><div class="toolTipBtm"></div></div>');
					this.title = "";
					this.width = $(this).width();
					$(this).find('.toolTipWrapper').css({left:this.width-22});
					$('.toolTipWrapper').fadeIn(300);
				},
				function() {
					$('.toolTipWrapper').fadeOut(100);
					$(this).children().remove();
					this.title = this.tip;
				}
			);
		});
	});


</SCRIPT>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=title%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
    				<td>
	    				
      					<table border="0" width="100%" cellpadding="0" cellspacing="0">
     						<tr>
	        					<td>
	          						<table width="100%" border="0">
	          							<thead>
	          								<tr class="rowhead2">
	          									<td>#</td>
	          									<td><%= reportGroupLabel %></td>
	          									<td><%= creationDateLabel %></td>
	          									<td><%= notificationEmailsLabel %></td>
	          									<td><%= informationLabel %></td>
	          									<td></td>
	          								</tr>
	          							</thead>
              							<% 
              							
              							for( JobUI info : jobsList)
       									{
       										long jobId = info.getId();
       										String creationDate = info.getCreationDate();
       										String errorSchedule = info.getErrorDescription();
       										String triggerDescription = info.getScheduleDescription();
       										String emails[] = info.getEmails();
       										StringBuilder emailBuff = new StringBuilder();
       										if ( emails != null )
       										{
	       										for(String email : emails)
	       										{
	       											emailBuff.append(email+" ");
	       										}
       										}
       										String groupReport = info.getGroupReport();
       									%>
       										<tr style="height: 30px">
       											<td>
	              								  <%= jobId %>
												</td>
	              								<td>
	              								  <%= groupReport %>
												</td>    
												<td>
	              								  <%= creationDate %>
												</td>
												<td>
	              								  <%= emailBuff %>
												</td> 
												<td >
	              								  <%= errorSchedule %>
	              								  <span class="toolTip" title="<%= reportJob %>: <%= jobId %> <br/><%= triggerDescription %>">
												  </span>
												</td>
												<td>
												  <a href="admin/reports/scheduleReports/manageschedule.jsp?action=d&idReport=<%= jobId %>">Delete</a>	              								  
												</td>               								
               								</tr>
       									<% 
										}
              							%>
            						</table>
          						</td>
      						</tr>
    					</table>
    					
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>

	
	
</script>
<script type="text/javascript">
			$(".onlyNumbers").numeric(false, function() { alert("Integers only"); this.value = ""; this.focus(); });
</script>
<%@ include file="/includes/footer.jsp" %>