<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 com.debisys.utils.TimeZone,
                 java.util.*,
                 com.debisys.utils.CalendarBussines,
                 com.debisys.customers.Merchant,
                 com.debisys.schedulereports.ScheduleReport,
                 com.debisys.schedulereports.ScheduleConfigurationJob,
                 com.debisys.schedulereports.SchedulerProperties,
                 com.debisys.schedulereports.RelativeOptions" %>
<%@page import="net.sf.jasperreports.engine.type.CalculationEnum"%>
<%@page import="com.debisys.utils.CalendarBussines.CALENDAR_OBJECTS_TYPE"%>
<%@page import="com.debisys.utils.BaseDaysMonths"%>
<%
	int section=4;
	int section_page=59;
	
	String workingDir   = DebisysConfigListener.getWorkingDir( application );
	String downloadPath = DebisysConfigListener.getDownloadPath( application );
	String instance 	= DebisysConfigListener.getInstance( application );
	
		
	boolean isSetProperties = false;
			
  	String param_ReportName 	   = com.debisys.schedulereports.ScheduleConfigurationJob.REPORT_NAME_PARAMETER;
  	String param_ReportDescription = com.debisys.schedulereports.ScheduleConfigurationJob.REPORT_DESCRIPTION_PARAMETER;
  	String param_StartReport       = com.debisys.schedulereports.ScheduleConfigurationJob.START_REPORT_PARAMETER;
  	String param_StartDateReport   = com.debisys.schedulereports.ScheduleConfigurationJob.START_DATE_REPORT_PARAMETER;
  	String param_scheduleTypeId    = com.debisys.schedulereports.ScheduleConfigurationJob.SCHEDULE_TYPE_ID_PARAMETER;
  	String param_DateReport        = com.debisys.schedulereports.ScheduleConfigurationJob.END_DATE_REPORT_PARAMETER;
  	String param_TimeZone    	   = com.debisys.schedulereports.ScheduleConfigurationJob.TIME_ZONE_REPORT_PARAMETER;
  	
  	//simple controls
  	String param_SimpleRepeatEveryUnit 	= com.debisys.schedulereports.ScheduleConfigurationJob.SIMPLE_REPEAT_EVERY_PARAMETER_UNIT;
  	String param_SimpleRadioControl 	= com.debisys.schedulereports.ScheduleConfigurationJob.SIMPLE_RADIO_CONTROL_PARAMETER;
  	String param_SimpleTimes 			= com.debisys.schedulereports.ScheduleConfigurationJob.SIMPLE_TIMES_PARAMETER;
  	String param_SimpleRepeatEveryValue = com.debisys.schedulereports.ScheduleConfigurationJob.SIMPLE_REPEAT_EVERY_VALUE_PARAMETER;
  	
  	
  	
  	//calendar control
  	String param_CalendarMonthEach = com.debisys.schedulereports.ScheduleConfigurationJob.CALENDAR_MONTH_EACH_PARAMETER;
  	String param_CalendarTriggerMonths = com.debisys.schedulereports.ScheduleConfigurationJob.CALENDAR_TRIGGER_MONTHS_PARAMETER;
  	String param_CalendarDaysEach = com.debisys.schedulereports.ScheduleConfigurationJob.CALENDAR_DAYS_EACH_PARAMETER;
  	String param_CalendarTriggerDays = com.debisys.schedulereports.ScheduleConfigurationJob.CALENDAR_TRIGGER_DAYS_PARAMETER;
  	String param_CalendarDaysNumber = com.debisys.schedulereports.ScheduleConfigurationJob.CALENDAR_DAYS_NUMBER_PARAMETER;
  	String param_CalendarTimeExecution = com.debisys.schedulereports.ScheduleConfigurationJob.CALENDAR_TIME_EXECUTIONR_PARAMETER;
  	String param_EmailsNotifications = com.debisys.schedulereports.ScheduleConfigurationJob.EMAILS_NOTIFICATIONS_PARAMETER;
  	String param_RelativeTypeId = com.debisys.schedulereports.ScheduleConfigurationJob.RELATIVE_TYPE_ID_PARAMETER;
  	String param_RelativeValue  = com.debisys.schedulereports.ScheduleConfigurationJob.RELATIVE_VALUE_PARAMETER;
  	String param_IncludingToday = com.debisys.schedulereports.ScheduleConfigurationJob.RELATIVE_INCLUDING_TODAY;
  	  	
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>

<link rel="stylesheet" media="screen" type="text/css" href="/support/css/colorpicker.css" />
<link rel="stylesheet" media="screen" type="text/css" href="/support/css/HtmlComponent.css" />
<script type="text/javascript" src="/support/includes/colorpicker.js"></script>
<script type="text/javascript" src="/support/includes/HtmlComponent.js"></script>
<link rel="stylesheet" type="text/css" href="/support/css/themes/base/jquery.ui.all.css" />
<script type="text/javascript" src="/support/includes/jquery-ui.js"></script>
<script type="text/javascript" src="/support/includes/jquery-ui-timepicker-addon.min.js"></script>

<% 
	String languageFlag = SessionData.getUser().getLanguageCode(); 
	ArrayList<BaseDaysMonths> months = CalendarBussines.getCalendarObjectsByType(CalendarBussines.CALENDAR_OBJECTS_TYPE.MONTHS, languageFlag);
	
	String monthsUIControl = "";
	String monthsMinUIControl = "";
	String daysUIControl = "";
	String daysMinUIControl = "";
	for( BaseDaysMonths month :months)
	{
       monthsUIControl = monthsUIControl +"'"+ month.getShortName()+"',";
       monthsMinUIControl = monthsMinUIControl +"'"+ month.getShortName()+"',";
    }    
    
	ArrayList<BaseDaysMonths> days = CalendarBussines.getCalendarObjectsByType(CalendarBussines.CALENDAR_OBJECTS_TYPE.DAYS, languageFlag);
    for( BaseDaysMonths day :days)
	{
       daysUIControl = daysUIControl +"\""+ day.getName()+"\",";
       daysMinUIControl = daysMinUIControl +"\""+ day.getShortName()+"\",";
    }
       
    String timeLabel = Languages.getString("jsp.admin.reports.scheduleReport.time",SessionData.getLanguage());
    String hourLabel = Languages.getString("jsp.admin.reports.managescheduleteports.unitHour",SessionData.getLanguage());
    String minuteLabel = Languages.getString("jsp.admin.reports.managescheduleteports.unitMinute",SessionData.getLanguage());
    String doneLabel = Languages.getString("jsp.admin.reports.scheduleReport.done",SessionData.getLanguage());
    String nextLabel = Languages.getString("jsp.admin.reports.scheduleReport.next",SessionData.getLanguage());
    String prevLabel = Languages.getString("jsp.admin.reports.scheduleReport.prev",SessionData.getLanguage());
    String nowLabel = Languages.getString("jsp.admin.reports.scheduleReport.now",SessionData.getLanguage());
    //
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, 1); // add 1 day  
    int yearUIControl = cal.get( Calendar.YEAR );
    int monthUIControl = cal.get( Calendar.MONTH );
    int dayUIControl = cal.get( Calendar.DATE );
    int hourUIControl = cal.get( Calendar.HOUR_OF_DAY );
    hourUIControl = 5;

	boolean showRelativeDate = true;
	
	String selectedCodeRelativeDate = "LastMonth";
	String titleReport = "";
	SchedulerProperties properties = SchedulerProperties.findPropertiesScheduler(instance);
	if ( properties != null)
	{
		isSetProperties = true;
		selectedCodeRelativeDate = properties.getCodeSelectedRelativeDate();
	}	
	
	ScheduleReport scheduleReport = (ScheduleReport) SessionData.getPropertyObj( DebisysConstants.SC_SESS_VAR_NAME );
	if (  scheduleReport != null  )
	{		    
	    if ( scheduleReport.getTitleName() != null )
	    {
	    	titleReport = Languages.getString( scheduleReport.getTitleName() ,SessionData.getLanguage());
	    	String additionalInfo = scheduleReport.getAdditionalData();
	    	if ( additionalInfo != null )
	    		titleReport += " - "+additionalInfo;
		}	
		
		if ( scheduleReport.getName().equals( DebisysConstants.SC_COLLECTIONS_REPORTS ) || 
				scheduleReport.getName().equals( DebisysConstants.SC_TRX_DAILY_LIABILITY_REPORT ) ||
					scheduleReport.getName().equals( DebisysConstants.SC_NON_TRANSAC_TERMINALS ) ||
						scheduleReport.getName().equals( DebisysConstants.SC_PIN_INVENTORY) ) 		
		{
			showRelativeDate = false;		
		}		
			
		if ( request.getParameter("process") != null && isSetProperties )
		{
			ScheduleConfigurationJob job = new ScheduleConfigurationJob();
			job.setProperties( properties );
			job.saveSchedule( SessionData, application, workingDir, request );
			//response.sendRedirect( job.getUrlBack() );
			//let know if the job can be schedule succesfully
		}
	}
	else
	{		
		//If the DebisysConstants.SC_SESS_VAR_NAME variable is not loaded, we redirect to main menu
		response.sendRedirect("/support/admin/index.jsp");
	}
	
	
	
	String startDate = SessionData.getProperty("start_date");
    String endDate   = SessionData.getProperty("end_date");
    String notificationEmail = SessionData.getUser().getEmail(); 
	
	String emailUser = SessionData.getUser().getEmail();
	String title = Languages.getString("jsp.admin.reports.scheduleReport.programmingReport",SessionData.getLanguage()).toUpperCase();
	String buttonTitle = Languages.getString("jsp.admin.reports.scheduleReport.programmingReport",SessionData.getLanguage());
	String reportName = Languages.getString("jsp.admin.reports.scheduleReport.reportName",SessionData.getLanguage());
	String description = Languages.getString("jsp.admin.reports.scheduleReport.description",SessionData.getLanguage());
	String startReport = Languages.getString("jsp.admin.reports.scheduleReport.startReport",SessionData.getLanguage());
	String immediately = Languages.getString("jsp.admin.reports.scheduleReport.immediately",SessionData.getLanguage());
	String at = Languages.getString("jsp.admin.reports.scheduleReport.at",SessionData.getLanguage());
	String timezone = Languages.getString("jsp.admin.reports.scheduleReport.timezone",SessionData.getLanguage());
	String periodicity = Languages.getString("jsp.admin.reports.scheduleReport.periodicity",SessionData.getLanguage());
	String required = Languages.getString("jsp.admin.reports.scheduleReport.required",SessionData.getLanguage());
	String indefinitely = Languages.getString("jsp.admin.reports.scheduleReport.indefinitely",SessionData.getLanguage());											   
	String times = Languages.getString("jsp.admin.reports.scheduleReport.times",SessionData.getLanguage());	
	String none = Languages.getString("jsp.admin.reports.scheduleReport.none",SessionData.getLanguage());
	String oneTime = Languages.getString("jsp.admin.reports.scheduleReport.onetime",SessionData.getLanguage());
	String recurring = Languages.getString("jsp.admin.reports.scheduleReport.recurring",SessionData.getLanguage());		
	String endReport = Languages.getString("jsp.admin.reports.scheduleReport.until",SessionData.getLanguage());
	String startSchedule = Languages.getString("jsp.admin.reports.scheduleReport.startSchedule",SessionData.getLanguage());
	String reportRangesTitle = Languages.getString("jsp.admin.reports.scheduleReport.reportRangesTitle",SessionData.getLanguage());
	String includinghDateTimeExecution = Languages.getString("jsp.admin.reports.scheduleReport.includinghDateTimeExecution",SessionData.getLanguage());
	String fixedPrev = Languages.getString("jsp.admin.reports.scheduleReport.fixedPrev",SessionData.getLanguage());
	String notificationEmailTitle = Languages.getString("jsp.admin.reports.scheduleReport.notificationEmail",SessionData.getLanguage());
	String toNotificationEmail = Languages.getString("jsp.admin.reports.scheduleReport.to",SessionData.getLanguage());
	String separateEmails = Languages.getString("jsp.admin.reports.scheduleReport.separateEmails",SessionData.getLanguage());
	String unSetPropertiesWarnning = Languages.getString("jsp.admin.reports.scheduleReport.unSetPropertiesWarnning",SessionData.getLanguage());
	String executionDatetime = Languages.getString("jsp.admin.reports.scheduleReport.executionDatetime",SessionData.getLanguage());
	String warnningSelect = Languages.getString("jsp.admin.reports.scheduleReport.warnningSelect",SessionData.getLanguage());
	//
	String eachMonth = Languages.getString("jsp.admin.reports.scheduleReport.eachMonth",SessionData.getLanguage());
	String selectedMonths = Languages.getString("jsp.admin.reports.scheduleReport.selectedMonths",SessionData.getLanguage());
	String eachDays = Languages.getString("jsp.admin.reports.scheduleReport.eachDays",SessionData.getLanguage());
	String weekDays = Languages.getString("jsp.admin.reports.scheduleReport.weekDays",SessionData.getLanguage());
	String selectedNumberDays = Languages.getString("jsp.admin.reports.scheduleReport.selectedNumberDays",SessionData.getLanguage());
	
	String letRelativeDates = Languages.getString("jsp.admin.reports.scheduleReport.letRelativeDates",SessionData.getLanguage());
	String missingRelatives = Languages.getString("jsp.admin.reports.scheduleReport.missingRelatives",SessionData.getLanguage());
		
	String reportTitleSchedule = Languages.getString("jsp.admin.reports.managescheduleteports.reporttitle",SessionData.getLanguage());	
	String untilControlrequired = Languages.getString("jsp.admin.reports.scheduleReport.untilControlRequired",SessionData.getLanguage());
	
	
	StringBuilder sb = new StringBuilder();	
	Iterator<Vector<String>> itList = TimeZone.getTimeZoneList().iterator();	
	while ( itList.hasNext() )
	{
		Vector vItem = itList.next();
		sb.append("<option value=\""+vItem.get(0).toString()+"\">"+vItem.get(2).toString()+" ["+vItem.get(1).toString()+"]</option>");
		vItem.clear();
		vItem = null;
	}
	itList = null;
			
		
	
%>


<script type="text/javascript" charset="utf-8">

	$(document).ready(function() 
	{			
		$("#<%= param_SimpleRepeatEveryValue %>").ForceNumericOnly();
		$("#<%= param_SimpleRepeatEveryUnit %>").ForceNumericOnly();
		$("#<%= param_RelativeValue %>").ForceNumericOnly();
		$("#<%= param_SimpleTimes %>").ForceNumericOnly();
		$("#<%= param_CalendarDaysNumber %>").ForceNumericOnly();
	
		$("#<%= param_StartDateReport %>").datetimepicker(
			{
				minDate: new Date(<%= yearUIControl %>, <%= monthUIControl %>, <%= dayUIControl %>, 0, 0),
				hourMin:  1,
				hourMax:  <%= hourUIControl %>,
				monthNames: [ <%= monthsUIControl %> ],
				dayNames: [ <%= daysUIControl %> ],
				dayNamesMin: [ <%= daysMinUIControl %> ],
				timeText: '<%= timeLabel %>',
				hourText: '<%= hourLabel %>',
				minuteText: '<%= minuteLabel %>',
				closeText: '<%= doneLabel %>',
				prevText: '<%= prevLabel %>',
				nextText: '<%= nextLabel %>',
				currentText: '<%= nowLabel %>'				
			}
			);
		
		//datetimepicker
		$("#<%= param_DateReport %>").datepicker(
			{
				minDate: new Date(<%= yearUIControl %>, <%= monthUIControl %>, <%= dayUIControl %>, 2, 6),
				currentText: '<%= nowLabel %>',
				monthNames: [ <%= monthsUIControl %> ],
				dayNames: [ <%= daysUIControl %> ],
				dayNamesMin: [ <%= daysMinUIControl %> ],
				timeText: '<%= timeLabel %>',
				hourText: '<%= hourLabel %>',
				minuteText: '<%= minuteLabel %>',
				closeText: '<%= doneLabel %>',
				prevText: '<%= prevLabel %>',
				nextText: '<%= nextLabel %>',
				timepicker: false								
			}
			);
		
 	
 	    $.timepicker.regional['current'] = {
			timeOnlyTitle: '<%= hourLabel %>:<%= minuteLabel %>',
			timeText: '<%= timeLabel %>',
			hourText: '<%= hourLabel %>',
			minuteText: '<%= minuteLabel %>',
			isRTL: false
		};
		$.timepicker.setDefaults($.timepicker.regional['current']);
				
		
		$('#<%= param_CalendarTimeExecution %>').timepicker({
			hourMin: 0,
			hourMax: 5
		});
		
		document.getElementById("divSimple").style.display="none";		
		document.getElementById("divRecurring").style.display="none";
		document.getElementById("tdendScheduleDateControls").style.display="none";		
	}
	);
</script>

<script type="text/javascript" charset="utf-8">

function validateForm(){ 
  document.mainform.submit.disabled = true;
}
 
function onChangeScheduleType( paramName )
{	
	var scheduleTypeId = document.getElementById( paramName ).value;
	if(scheduleTypeId == 1){
		document.getElementById("divSimple").style.display="inline";
		document.getElementById("divRecurring").style.display="none";	
		document.getElementById("tdendScheduleDateControls").style.display="inline";	
	}
	else if(scheduleTypeId == 2){
		document.getElementById("divSimple").style.display="none";
		document.getElementById("divRecurring").style.display="inline";	
		document.getElementById("tdendScheduleDateControls").style.display="inline";	
	}
	else
	{
		doNoneOptionTask();			
	}
}

function doNoneOptionTask()
{
	//none
	document.getElementById("divSimple").style.display="none";
	document.getElementById("divRecurring").style.display="none";
	document.getElementById("tdendScheduleDateControls").style.display="none";	
}

function relativeDatesControls(type)
{
	$("#<%=param_scheduleTypeId%>").attr("disabled",false);	
	if ( type=="1" )
	{
		document.getElementById("tdRelatives").style.display="inline";
	}
	else if ( type=="2" )
	{
		$("#relative").prop("checked", true)		
	}
	else if ( type=="3" )
	{
		$("#valueRelativeDates").val("");
		$("#valueRelativeDates").attr("disabled",true);
		doNoneOptionTask();		
		$('#<%=param_scheduleTypeId%>').val("<%= ScheduleConfigurationJob.SCHEDULE_TYPE_NONE %>");
		$('#<%=param_scheduleTypeId%>').attr("disabled",true);
	}
}

//control can be CAL,DAY
function unSelectOptions(control, flag)
{
	if ( control=="CAL")
	{
		//if (flag=="1")
		//	$("#theMonths option:selected").prop("selected", false);		
	}
	else
	{		
		if (flag=="1")
		{
			//$("#theWeekDays option:selected").prop("selected", false);
			$("#<%= param_CalendarDaysNumber %>").val("");
		}
		else if (flag=="0")
		{
			$("#<%= param_CalendarDaysNumber %>").val("");
		}
		else if (flag=="2")
		{
			//$("#theWeekDays option:selected").prop("selected", false);
			$("#<%= param_CalendarDaysNumber %>").focus();
		}
		else if (flag=="3")
		{
			//$("#theWeekDays option:selected").prop("selected", false);
			$("#<%= param_CalendarDaysEach %>").prop("checked", true);			
		}		
	}	
}
function letChecked(control)
{
	if ( control=="0")
		$("#calendarMonthSelected").prop("checked", true);
	else
		$("#calendarSelectedDays").prop("checked", true);
		  
}
function showControlStartDate()
{
	$("#<%= param_StartDateReport %>").datepicker("show");
	$("#startReportA").prop("checked", true);
}
function letEmptyStartDateControl()
{
	$("#<%= param_StartDateReport %>").val("");
}
function simpleRadioUnselect(action)
{
	if ( action==0 )
	{
		$("#<%= param_SimpleTimes %>").val("");
	}
	else if ( action==1 ) 
	{
		$("#<%= param_SimpleTimes %>").focus();
	}
	else
	{
		$("#simpleRadioTimes").prop("checked", true);
	}
}

function emailValidator()
{
	var emailsTmp="";
	var emailsList = $("#emails").val();
	//alert(emailsList);
	var lines = emailsList.split(',');
	//alert(lines);
	$.each(lines, function(key, line) 
	{
	    var result = validateEmail(line);
	    //alert(result);
	    if ( result )	
	    {
	    	if ( emailsTmp.length > 0 )
	    		emailsTmp += ","+ line;
	    	else	
	    		emailsTmp += line;
	    	//alert(emailsTmp);	
	 	}       
	});	
	if ( emailsTmp.length > 0 )
		$("#emails").val(emailsTmp);
	else	
		$("#emails").val("");
	//alert("end");
}

function validateEmail($email) 
{
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if( !emailReg.test( $email ) ) 
  {
    return false;
  }
  else
  {
    return true;
  }
}

function changeRelativeRange()
{
  var filterParams = $("#<%= param_RelativeTypeId %> option:selected").val();
  var parts = filterParams.split("-");
  
  var desc = parts[3];
  $("#td_DescriptionRelativeDates").text( desc );
  
  //alert( parts );
  if ( parts[2] == 1 )
   {									  //checked
  	
  	$("#<%= param_IncludingToday %>").attr("disabled",false);
  }
  else
  { 
  	$("#<%= param_IncludingToday %>").attr("checked", false ); 	
  	$("#<%= param_IncludingToday %>").attr("disabled",true);
  }
  
  if ( parts[1] == 1 )
  {
     $("#<%= param_RelativeValue %>").attr("disabled",false);
     $("#<%= param_RelativeValue %>").focus();
  }
  else
  {
  	 $("#<%= param_RelativeValue %>").val("");
     $("#<%= param_RelativeValue %>").attr("disabled",true);
  } 
}
jQuery.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
    	//alert(this.id);
    	if ( this.id == "<%= param_CalendarDaysNumber %>" )
    	{
    		$(this).keydown(function(e)
	        {
	        	//var valueT = $("#<%= param_CalendarDaysNumber %>").val();
	        	//alert(valueT);
	            var key = e.charCode || e.keyCode || 0;
	            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
	            // home, end, period, and numpad decimal
	            // 188 comma symbol
	            return (	            	
	            	key == 188 || 
	                key == 8 || 
	                key == 9 ||
	                key == 46 ||
	                key == 110 ||
	                key == 190 ||
	                (key >= 35 && key <= 40) ||
	                (key >= 48 && key <= 57) ||
	                (key >= 96 && key <= 105));
	        });
    	}
    	else
    	{
    		$(this).keydown(function(e)
	        {
	            var key = e.charCode || e.keyCode || 0;
	            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
	            // home, end, period, and numpad decimal
	            return (
	                key == 8 || 
	                key == 9 ||
	                key == 46 ||
	                key == 110 ||
	                key == 190 ||
	                (key >= 35 && key <= 40) ||
	                (key >= 48 && key <= 57) ||
	                (key >= 96 && key <= 105));
	        });
    	}	
        
    });
};

function validateRequiredNumber()
{	
	var val =  $("#<%= param_SimpleRepeatEveryValue %>").val();
	if ( val.length == 0 )
		$("#<%= param_SimpleRepeatEveryValue %>").val("2");
}
function showReportJobCreationId(data, type)
{
	$("#messagesZone").html(data);	 
	$("#messagesZone").fadeIn(300);
		
	if ( type == 1)
		$("#messagesZone").css("color","blue");
	else if ( type == 2 )
		$("#messagesZone").css("color","red");
								    
	$("#messagesZone").fadeOut(6000);
}
function validateUntilControl(data)
{
    var result = true;
	var typeSchedule = $("#parameter_scheduleTypeId").val();
	
	if ( typeSchedule != "0" )
	{
	  var parameter_endScheduleDate = $("#parameter_endScheduleDate").val();
	  //alert(parameter_endScheduleDate);
	  if ( parameter_endScheduleDate.length == 0 )
	  {
	  	 result = false;
		showReportJobCreationId("<%=untilControlrequired%>",2);
		$("#parameter_endScheduleDate").focus();
	  }
	}		 
	return result;
}				    
</script>

<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=title%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
  	
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
    				<td>
	    				<form name="mainform" method="post" action="admin/reports/scheduleReports/scheduleReports.jsp" onSubmit="validateForm();">
      					<table border="0"  cellpadding="0" cellspacing="0">
     						<tr>
	        					<td>
	          						<table width="100%" border="0">
	          							<tr>
											<td align="center" class="formAreaTitle2">
												<%= titleReport %> <%= reportTitleSchedule %>
												<div id="messagesZone"></div>
											</td>
										</tr>
              							<tr>
              								<td colspan="2" class="formArea2">
              									
												<table border="0" cellpadding="5" cellspacing="5" align="left">
														
														
													<tr align="left">
				               							<td valign="top" nowrap class="formAreaTitle2" >
				               								<fieldset>
																<legend style="font-weight:bold;"><%= startSchedule %></legend>
																<table>
					               									<tr align="left">
								               							<td colspan="2" nowrap>
							               									<input id="startReportI" type="radio" name="<%= param_StartReport %>" value="i" checked="" onclick="letEmptyStartDateControl()"><%=immediately%>
							               								</td>				               										               													               								
								               						</tr>
								               						<tr align="left" valign="top">
								               							<td nowrap>
								               								<input id="startReportA" type="radio"  onclick=" $('#startReportA').prop('checked',true); "  name="<%= param_StartReport %>" value="a" onclick="showControlStartDate();" ><%=at%>
								               								<div id="sd_divStartDate">
								               									<input onclick=" $('#startReportA').prop('checked',true); "   
								               										   name="<%= param_StartDateReport %>" id="<%= param_StartDateReport %>"
								               										   readonly= "readonly"   />
								               									<img idc="<%= param_StartDateReport %>" onclick=" $('#startReportA').prop('checked',true); " class="datetimeButton" src="/support/images/calendar.png" />
								               								</div>							               								
								               							</td>
							               							</tr>					
				               									</table>	
															</fieldset>
				               								
				               							</td>				               										               													               								
				               							<td valign="top" nowrap class="formAreaTitle2"  >
				               								<fieldset>
																<legend style="font-weight:bold;"><%=periodicity%></legend>
																<table>
																	<tr>
																		<td> 
								               								<select name="<%= param_scheduleTypeId %>" id="<%= param_scheduleTypeId %>" 
																	    			onchange="onChangeScheduleType('<%= param_scheduleTypeId %>');" >
																	    		<option selected value="<%= ScheduleConfigurationJob.SCHEDULE_TYPE_NONE %>"><%=none%></option>
																	    		<option value="<%= ScheduleConfigurationJob.SCHEDULE_TYPE_SIMPLE %>"><%=oneTime%></option>
																	    		<option value="<%= ScheduleConfigurationJob.SCHEDULE_TYPE_CALENDAR %>"><%=recurring%></option>
																			</select>
																	    </td>	
																    															    
																	    <td id="tdendScheduleDateControls" name="tdendScheduleDateControls" nowrap>
																	    	<%=endReport%>
								               								<input name="<%= param_DateReport %>" id="<%= param_DateReport %>"  
								               									   readonly= "readonly" />
								               								<img idc="<%= param_DateReport %>" class="datetimeButton" src="/support/images/calendar.png" id="imgendScheduleDate" />
								               							</td>	
																	
																	
																	</tr>
																</table>
															</fieldset>	
				               							</td>
				               						</tr>	
				               							
				               						<!-- 				               								               						
				               						<tr align="left">
				               							<td nowrap class="formAreaTitle2" ><%= startSchedule %></td>				               										               													               								
				               						</tr>			               						
				               						<tr align="left">
				               							<td valign="top" >
				               								<table cellpadding="5" cellspacing="5" align="left">
				               									
				               									<tbody>
				               									<tr align="left" valign="top">
							               							<td colspan="2" nowrap>
							               								<input id="startReportI" type="radio" name="<%= param_StartReport %>" value="i" checked="" onclick="letEmptyStartDateControl()"><%=immediately%>
							               							</td>																
							               						</tr>
							               						<tr align="left" valign="top">
							               							<td nowrap>
							               								<input id="startReportA" type="radio" name="<%= param_StartReport %>" value="a" onclick="showControlStartDate();" ><%=at%>
							               								<div id="sd_divStartDate">
							               									<input onclick=" $('#startReportA').prop('checked',true); "   
							               										   name="<%= param_StartDateReport %>" id="<%= param_StartDateReport %>"
							               										   readonly= "readonly"   />
							               									<img idc="<%= param_StartDateReport %>" class="datetimeButton" src="/support/images/calendar.png" />
							               								</div>							               								
							               							</td>							               							
							               							<td> 
							               								<%=periodicity%>
																    	<select name="<%= param_scheduleTypeId %>" id="<%= param_scheduleTypeId %>" 
																    			onchange="onChangeScheduleType('<%= param_scheduleTypeId %>');" >
																    		<option selected value="<%= ScheduleConfigurationJob.SCHEDULE_TYPE_NONE %>"><%=none%></option>
																    		<option value="<%= ScheduleConfigurationJob.SCHEDULE_TYPE_SIMPLE %>"><%=oneTime%></option>
																    		<option value="<%= ScheduleConfigurationJob.SCHEDULE_TYPE_CALENDAR %>"><%=recurring%></option>
																		</select>
																    </td>	
																    															    
																    <td id="tdendScheduleDateControls" name="tdendScheduleDateControls" nowrap>
																    	<%=endReport%>
							               								<input name="<%= param_DateReport %>" id="<%= param_DateReport %>"  
							               									   readonly= "readonly" />
							               								<img idc="<%= param_DateReport %>" class="datetimeButton" src="/support/images/calendar.png" id="imgendScheduleDate" />
							               							</td>							               													               													               													               													               								
							               						</tr>
							               						</tbody>
															</table>																												
				               							</td>
				               					   </tr>	
				               					   -->					               						
												</table>	
											</td>               								
               							</tr> 
										
									  
										<tr> 
											<td colspan="2">
												<div id= "divSimple" >
											    	<fieldset>
														<legend style="font-weight:bold;"><%=Languages.getString("jsp.admin.reports.scheduleReport.oneTimeOcurrense",SessionData.getLanguage())%></legend>
														<table border="0" cellpadding="0" cellspacing="0" align="left">
	
															<tr align="left">
																<td><%=Languages.getString("jsp.admin.reports.scheduleReport.recursEvery",SessionData.getLanguage())%>:
																	<input type="text" value="2"  onblur="validateRequiredNumber();" id="<%= param_SimpleRepeatEveryValue %>" name="<%= param_SimpleRepeatEveryValue %>"     /></td>
						               							<td> 
						               								<select name="<%= param_SimpleRepeatEveryUnit %>" id="<%= param_SimpleRepeatEveryUnit %>" >
													    				<!--  
													    				<option selected value="MINUTE"><%=Languages.getString("jsp.admin.reports.scheduleReport.eachMinutes",SessionData.getLanguage())%></option>
													    				<option value="HOUR"><%=Languages.getString("jsp.admin.reports.scheduleReport.eachHours",SessionData.getLanguage())%></option>
													    				-->
													    				<option value="DAY"><%=Languages.getString("jsp.admin.reports.scheduleReport.days",SessionData.getLanguage())%></option>
													    				<option value="WEEK"><%=Languages.getString("jsp.admin.reports.scheduleReport.eachWeeks",SessionData.getLanguage())%></option>
																	</select>
																	<%= required %>
						               							</td>						               														               								
						               						</tr>
						               						
						               						<!--  
						               						<tr align="left">
						               							<td nowrap >
						               								<table>
						               									<tr>
						               										<td nowrap>
						               											<input id="simpleRadioInde" type="radio" name="<%= param_SimpleRadioControl %>" value="i" checked="checked" onclick="simpleRadioUnselect(0);"/> 
						               										
						               										</td>
									               							<td nowrap><%=indefinitely%></td>      								
						               									</tr>
						               								</table>
						               							</td>					               													               								
						               						</tr>	
						               						<tr align="left">
						               							<td nowrap>
						               								<table>
						               									<tr>
						               										<td nowrap><input id="simpleRadioTimes" type="radio" name="<%= param_SimpleRadioControl %>" value="t" onclick="simpleRadioUnselect(1);" /> </td>
						               										<td nowrap><%=times%>:</td>						               													               								
						               										<td nowrap><input type="text" name="<%= param_SimpleTimes %>" id="<%= param_SimpleTimes %>" onclick="simpleRadioUnselect(2);"/> </td>					               																		               								
						               									</tr>
						               								</table>
						               							</td>
						               						</tr>
						               						-->
														</table>
													</fieldset>
											     </div>
											</td>
										</tr>
										
										<tr>
											<td colspan="2">
												<div id="divRecurring">
													<fieldset>
														<legend style="font-weight:bold;"><%=Languages.getString("jsp.admin.reports.scheduleReport.frequency",SessionData.getLanguage())%></legend>
														<table cellpadding="5" cellspacing="5" align="left">
															<tr>
																<td valign="baseline">
																	<fieldset>
																	<legend style="font-weight:bold;"><%=Languages.getString("jsp.admin.reports.scheduleReport.months",SessionData.getLanguage())%></legend>
																		<table cellpadding="0" cellspacing="0" align="left">
																		  	<tr>
																			  <td><input type="radio" name="<%= param_CalendarMonthEach %>" value="e" id="calendarMonthEach"  checked="checked" onclick="unSelectOptions('CAL','1');"/><%= eachMonth %></td>
																			</tr>
																		  	<tr>
																			 <td><input type="radio" name="<%= param_CalendarMonthEach %>" value="s" id="calendarMonthSelected" onclick="unSelectOptions('CAL','0');" /><%= selectedMonths %></td>
																			</tr>
																			<tr>
																				<td>
																					<select onclick="letChecked('0');" id="theMonths" name="<%= param_CalendarTriggerMonths %>" id="<%= param_CalendarTriggerMonths %>"  multiple="multiple">
                                                                
                                                                <% 
                                                                if ( months.size() > 0 )
                                                                {
                                                                 for( BaseDaysMonths base : months )
                                                                 {
																		%>
																		<option selected="selected" value="<%= base.getOrder() %>"><%= base.getShortName() %></option>
																		<% 
													 			 }
												 			    }
																else
																{
                                                                %>
                                                                   <option value="1">Jan</option>                                                                                
                                                                   <option value="2">Feb</option>                                                                                
                                                                   <option value="3">Mar</option>                                                                                
                                                                   <option value="4">Apr</option>                                                                                
                                                                   <option value="5">May</option>                                                                                
                                                                   <option value="6">Jun</option>                                                                                
                                                                   <option value="7">Jul</option>                                                                                
                                                                   <option value="8">Aug</option>                                                                                
                                                                   <option value="9">Sep</option>                                                                                
                                                                   <option value="10">Oct</option>                                                                                
                                                                   <option value="11">Nov</option>                                                                                
                                                                   <option value="12">Dec</option> 
                                                               <%}%>  
                                                                
                                                                                                                              
					                                                                 </select>
																				</td>
																			</tr>
																			<tr style="height: 55px">
																				<td>
																					<span style="font-size: 9;color: gray;"><%= warnningSelect %></span>																				 
																				</td>
																			</tr>																			
																		</table>
																	</fieldset>																																	
																</td>																
																
																<td valign="baseline" >
																	<fieldset>
																	<legend style="font-weight:bold;"><%=Languages.getString("jsp.admin.reports.scheduleReport.days",SessionData.getLanguage())%></legend>
																		<table cellpadding="0" cellspacing="0" align="left">
																		  	<tr align="left">
																				<td><input type="radio" name="<%= param_CalendarDaysEach %>" id="calendarEveryDay" value="e" checked="checked" onclick="unSelectOptions('DAY','1');" /><%= eachDays %></td>
																			</tr>
																			<tr align="left">
																				<td><input type="radio" name="<%= param_CalendarDaysEach %>" id="calendarSelectedDays" value="s" onclick="unSelectOptions('DAY','0');"/><%= weekDays %></td>
																			</tr>
																			<tr align="left">
																				<td>
																					<select id="theWeekDays" name="<%= param_CalendarTriggerDays %>" id="<%= param_CalendarTriggerDays %>"
																							 multiple="multiple" onclick="letChecked('1');">
																                                     
																    <% 
                                                                if ( days.size() > 0 )
                                                                {
                                                                  for( BaseDaysMonths base : days )
                                                                  {                                                                  	
																	%>
																	<option selected="selected" value="<%= base.getOrder() %>"><%= base.getShortName() %></option>
																	<%
																  }
																}
																else
																{
																%> 
				                                                                         <option value="2">Mon</option>
				                                                                         <option value="3">Tue</option>
				                                                                         <option value="4">Wed</option>
				                                                                         <option value="5">Thu</option>
				                                                                         <option value="6">Fri</option>
				                                                                         <option value="7">Sat</option>
				                                                                         <option value="1">Sun</option>
				                                                <%
																}
																%>                                                                      
				                                                                    </select>
																				</td>																				
																			</tr>
																			<tr style="height: 55px">
																				<td><input type="radio" name="<%= param_CalendarDaysEach %>"  id="<%= param_CalendarDaysEach %>" value="d" onclick="unSelectOptions('DAY','2');" /> 
																					<%= selectedNumberDays %>:<input id="<%= param_CalendarDaysNumber %>" name="<%= param_CalendarDaysNumber %>" type="text" onclick="unSelectOptions('DAY','3');" > 
																				</td>
																			</tr>
																		</table>
																	</fieldset>
																</td>
																
																<td valign="baseline" >
																	<fieldset>
																	<legend style="font-weight:bold;"><%= executionDatetime %></legend>
																		<table cellpadding="0" cellspacing="0" align="left">
																		  	<tr align="left">
																				<td><%= timeLabel %></td>
																			</tr>
																			<tr align="left">
																				<td>
																					<input type="text" name="<%= param_CalendarTimeExecution %>" id="<%= param_CalendarTimeExecution %>" value="01:00" readonly="readonly" />
																					<img idc="<%= param_CalendarTimeExecution %>" class="datetimeButton" src="/support/images/time.png" />
																				</td>
																			</tr>																			
																		</table>
																	</fieldset>
																</td>
															</tr>
															
														</table>
													</fieldset>
												</div>
											</td>
										</tr>
										
										
										
										
										
										
										<% 
										if ( showRelativeDate )
										{
										%>
										<tr style="height: 35px">
											<td align="center" class="formAreaTitle2"><%= reportRangesTitle %></td>
										</tr>										
										<tr align="left" >
	               							<td class="formArea2">
	               								<table>
		               								<tr align="center">
		               									<td colspan="3" align="center" id="td_DescriptionRelativeDates"></td>
		               								</tr>
							               			<% 													
													ArrayList<RelativeOptions> arrRelative = new ArrayList<RelativeOptions>();
													arrRelative = RelativeOptions.findRelativeOptions( languageFlag );
													if ( arrRelative.size() > 0 )
													{
													%>
	               									<tr>				               										
	               										<td><input type="radio" name="dateBehaviour" id="relative" value="relative" checked="checked" onclick="relativeDatesControls(1);">
	               											<%= letRelativeDates %>
	               										</td>
	               										<td id="tdRelatives" align="right" colspan="2" >
	               												               											
								
             											<select id="<%= param_RelativeTypeId %>" name="<%= param_RelativeTypeId %>" onchange="changeRelativeRange();"  >
             												
             												<% 
															for( RelativeOptions optionRel : arrRelative )
															{
																String id         = optionRel.getId();
																String capture    = optionRel.getCaptureData();
																String startFunc  = optionRel.getStartFunction();
																String endFunc    = optionRel.getEndFunction();
																String shortDesc  = optionRel.getShortDescrption();
																String longDesc   = optionRel.getLongDescription();
																String canInclude = optionRel.getCanIncludeCurrent();
																
																String selectedOption = "";
																if ( optionRel.getCode().equals( selectedCodeRelativeDate ) )
																{
																	selectedOption = "selected=\"selected\"";
																}
																%>
																<option   value="<%= id %>-<%= capture %>-<%= canInclude %>-<%= longDesc %>" <%= selectedOption %> ><%= shortDesc %></option>
																<% 
															} 
															%>
             												
             											 </select>
             											 <input disabled="disabled" type="text" id="<%= param_RelativeValue %>" size="4" name="<%= param_RelativeValue %>" onclick="relativeDatesControls(2);"  >
	               										 <input type="checkbox"  id="<%= param_IncludingToday %>" name="<%= param_IncludingToday %>" value="1"/><%= includinghDateTimeExecution %>
	               										</td>	               										
	               									</tr>
	               									<% 
													}
													else
													{
													%>
													<tr>
													 	<td colspan="2" style="color: red;"><%= missingRelatives %></td>
													</tr>
													<% 
													}
													%>
	               									<tr style="height: 65px">	
	               										<td colspan="2">
	               											<input type="radio" name="dateBehaviour" id="fixed" value="fixed" onclick="relativeDatesControls(3);"><%= fixedPrev %>
	               												<br/>(<%=startDate %> - <%=endDate %>)
	               										</td>              										
	               									</tr>	
	               								</table>	               								
	               							</td>						               									
				               			</tr>
				               			<% 
										}
										%>
				               			
				               			
										<tr style="height: 35px">
											<td align="center" class="formAreaTitle2"><%= notificationEmailTitle %></td>
										</tr>
										<tr>
											<td class="formArea2">
												<%= toNotificationEmail %>:<input id="<%= param_EmailsNotifications %>" name="<%= param_EmailsNotifications %>" type="text" size="90" onblur="emailValidator();" 
															value="<%= notificationEmail %>" /><br/>
												<span style="font-size: 9;color: gray;"><%= separateEmails %></span>
											</td>
										</tr>										
										<tr>
										   <td class=main colspan=2 align="center">
										     	
										     <%if ( isSetProperties )
										       {%>
										       
										       <input type="hidden" name="title" value="<%= titleReport %>">
										       	<input type="hidden" name="process" value="y">
										      	<input type="submit" name="submit" value="<%=buttonTitle%>" onclick="return validateUntilControl();">
										     <%} 
										       else
										       {
										       	%>
										      	<%= unSetPropertiesWarnning %>
										     <%}%>
										    </td>
										</tr>
            						</table>
          						</td>
      						</tr>
    					</table>
    					</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<%@ include file="/includes/footer.jsp" %>
