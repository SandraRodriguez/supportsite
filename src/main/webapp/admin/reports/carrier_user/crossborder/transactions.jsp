<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<%
	int section = 14;
	int section_page = 17;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CrossBorderReport" class="com.debisys.reports.CrossBorderReport" scope="request"/>
<jsp:setProperty name="CrossBorderReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector<Vector<String>> vecSearchResults = new Vector<Vector<String>>();
	Hashtable searchErrors = null;
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;

	if ( request.getParameter("download") != null )
	{
	  	String sURL = "";
	    CrossBorderReport.setStartDate(request.getParameter("startDate"));
	    CrossBorderReport.setEndDate(request.getParameter("endDate"));
	    SessionData.setProperty("start_date", request.getParameter("startDate"));
	    SessionData.setProperty("end_date", request.getParameter("endDate"));
		sURL = CrossBorderReport.downloadCarrierUserCrossBorderDetails(application, SessionData);
    	response.sendRedirect(sURL);
	  	return;
	}
	
	if (request.getParameter("search") != null)
	{
  		if (request.getParameter("page") != null)
  		{
    		try
    		{
      			intPage = Integer.parseInt(request.getParameter("page"));
    		}
    		catch(NumberFormatException ex)
    		{
      			intPage = 1;
    		}	
  		}

  		if (intPage < 1)
  		{
    		intPage = 1;
  		}

  		if (CrossBorderReport.validateDateRange(SessionData))
  		{
	    	CrossBorderReport.setStartDate(request.getParameter("startDate"));
	    	CrossBorderReport.setEndDate(request.getParameter("endDate"));
	    	SessionData.setProperty("start_date", request.getParameter("startDate"));
	    	SessionData.setProperty("end_date", request.getParameter("endDate"));
    		vecSearchResults = CrossBorderReport.getCarrierUserCrossBorderDetails(intPage, intPageSize, SessionData);
    		
    		// This is the record count
    		Vector<String> vecCount = vecSearchResults.get(0);
    		
    		intRecordCount = Integer.parseInt(vecCount.get(0).toString());
    		vecSearchResults.removeElementAt(0);
    
    		if (intRecordCount>0)
    		{
      			intPageCount = (intRecordCount / intPageSize);
      	
      			if ((intPageCount * intPageSize) < intRecordCount)
      			{
        			intPageCount++;
      			}
    		}
  		}
  		else
  		{
   			searchErrors = CrossBorderReport.getErrors();
  		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.crossborder.transactions",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
	       				<table width="100%" border="0" cellspacing="0" cellpadding="2">
	       					<tr>
	       						<td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
		if (!CrossBorderReport.getStartDate().equals("") && !CrossBorderReport.getEndDate().equals(""))
		{
			out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(CrossBorderReport.getStartDate()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(CrossBorderReport.getEndDate()));
		}
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
	       						</td>
	       					</tr>
	       					<tr>
	       						<td align=right class="main" nowrap>
<%
		if (intPage > 1)
		{
			out.println("<a href=\"admin/reports/carrier_user/crossborder/transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&sourceCountry=" + request.getParameter("sourceCountry") + "&targetCountry=" + request.getParameter("targetCountry") + "&startDate=" + URLEncoder.encode(CrossBorderReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(CrossBorderReport.getEndDate(), "UTF-8") + "&page=1\">First</a>&nbsp;");
			out.println("<a href=\"admin/reports/carrier_user/crossborder/transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&sourceCountry=" + request.getParameter("sourceCountry") + "&targetCountry=" + request.getParameter("targetCountry") + "&startDate=" + URLEncoder.encode(CrossBorderReport.getStartDate(), "UTF-8") +  "&endDate=" + URLEncoder.encode(CrossBorderReport.getEndDate(), "UTF-8") + "&page=" + (intPage-1) + "\">&lt;&lt;Prev</a>&nbsp;");
		}

		int intLowerLimit = intPage - 12;
		int intUpperLimit = intPage + 12;

		if (intLowerLimit<1)
		{
			intLowerLimit=1;
			intUpperLimit = 25;
		}

		for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
		{
			if (i==intPage)
			{
				out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
			}
			else
			{
				out.println("<a href=\"admin/reports/carrier_user/crossborder/transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&sourceCountry=" + request.getParameter("sourceCountry") + "&targetCountry=" + request.getParameter("targetCountry") + "&startDate=" + URLEncoder.encode(CrossBorderReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(CrossBorderReport.getEndDate(), "UTF-8") + "&page=" + i + "\">" +i+ "</a>&nbsp;");
			}
		}

		if (intPage <= (intPageCount-1))
		{
			out.println("<a href=\"admin/reports/carrier_user/crossborder/transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&sourceCountry=" + request.getParameter("sourceCountry") + "&targetCountry=" + request.getParameter("targetCountry") + "&startDate=" + URLEncoder.encode(CrossBorderReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(CrossBorderReport.getEndDate(), "UTF-8") + "&page=" + (intPage+1) + "\">Next&gt;&gt;</a>&nbsp;");
			out.println("<a href=\"admin/reports/carrier_user/crossborder/transactions.jsp?search=" + URLEncoder.encode(request.getParameter("search"), "UTF-8") + "&sourceCountry=" + request.getParameter("sourceCountry") + "&targetCountry=" + request.getParameter("targetCountry") + "&startDate=" + URLEncoder.encode(CrossBorderReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(CrossBorderReport.getEndDate(), "UTF-8") + "&page=" + (intPageCount) + "\">Last</a>");
		}
%>
	      						</td>
	      					</tr>
	      					<tr>
	       						<td align=left class="main" nowrap>
		              				<form method=post action="admin/reports/carrier_user/crossborder/transactions.jsp">
			              				<input type="hidden" name="startDate" value="<%=request.getParameter("startDate")%>">
			              				<input type="hidden" name="endDate" value="<%=request.getParameter("endDate")%>">
			              				<input type="hidden" name="sourceCountry" value="<%=request.getParameter("sourceCountry")%>">
			              				<input type="hidden" name="targetCountry" value="<%=request.getParameter("targetCountry")%>">
			              				<input type="hidden" name="search" value="y">
			              				<input type="hidden" name="download" value="y">
			              				<input type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
	              					</form>
	      						</td>
	       					</tr>
	       				</table>
	       				<table width="100%" cellspacing="1" cellpadding="2">
	       					<tr>
	       						<td class=rowhead2>#</td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
<%
		if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
%>
	  							<td class=rowhead2><%=Languages.getString("jsp.admin.reports.phys_state",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>               
								<td class=rowhead2><%=Languages.getString("jsp.admin.reports.city_county",SessionData.getLanguage()).toUpperCase()%></td>
	            				<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
	            				<td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference",SessionData.getLanguage()).toUpperCase()%></td>
	            				<td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.amount",SessionData.getLanguage()).toUpperCase()%></td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase()%></td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
	       						<td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></td>
<%
		if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
%>
								<td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>                 
            				</tr>
<%
		int intCounter = 1;
					
		String sPhysState = "", sAuthorizationNo = "";

		Iterator<Vector<String>> it = vecSearchResults.iterator();
		int intEvenOdd = 1;
                  
		while (it.hasNext())
		{
			Vector<String> vecTemp = null;
			vecTemp = (Vector<String>) it.next();
                    
			if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
				sPhysState = "<td nowrap>" + vecTemp.get(14) + "</td>";
			}
			else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				sAuthorizationNo = "<td nowrap>" + vecTemp.get(13) + "</td>";
			}  
                    
			out.println("<tr class=row" + intEvenOdd +">" +
				"<td nowrap>" + intCounter++ + "</td>" +
				"<td nowrap>" + vecTemp.get(0)+ "</td>" +
				"<td nowrap>" + vecTemp.get(1) + "</td>" +
				"<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2))  + "</td>" +
				"<td nowrap>" + vecTemp.get(3) + "</td>" +
				sPhysState +                                                             
				"<td nowrap>" + vecTemp.get(4) + "</td>" +
				"<td nowrap>" + vecTemp.get(5) + "</td>" +
				"<td nowrap>" + vecTemp.get(6) + "</td>" +
				"<td nowrap>" + vecTemp.get(7) + "</td>" +
				"<td nowrap>" + vecTemp.get(8) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(9) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>" +
				"<td width=200 nowrap align=\"right\">" + vecTemp.get(11) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(12) + "</td>" +
				sAuthorizationNo +
				"</tr>");
                
			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}
		
		out.println("</table>");
		
		vecSearchResults.clear();
	}
	else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
	{
%>
						<table width="100%">
				           	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>