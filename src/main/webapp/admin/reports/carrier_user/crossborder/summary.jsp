<%@ page import="java.net.URLEncoder,
                 java.util.*,
                 com.debisys.utils.*" %>
<%
	int section = 14;
	int section_page = 16;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="CrossBorderReport" class="com.debisys.reports.CrossBorderReport" scope="request"/>
<jsp:setProperty name="CrossBorderReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector<Vector<String>> vecCurrent = new Vector<Vector<String>>();
	Hashtable searchErrors = null;

	if ( request.getParameter("download") != null )
	{
	  	String sURL = "";
    	SessionData.setProperty("start_date", request.getParameter("startDate"));
    	SessionData.setProperty("end_date", request.getParameter("endDate"));
  	  	sURL = CrossBorderReport.downloadCarrierUserRoamingETopUps(application, SessionData);
	  	response.sendRedirect(sURL);
	  	return;
	}
	
	if (request.getParameter("search") != null)
	{
  		if (CrossBorderReport.validateDateRange(SessionData))
  		{
    		CrossBorderReport.setStartDate(request.getParameter("startDate"));
    		CrossBorderReport.setEndDate(request.getParameter("endDate"));
    		SessionData.setProperty("start_date", request.getParameter("startDate"));
    		SessionData.setProperty("end_date", request.getParameter("endDate"));
    		vecCurrent = CrossBorderReport.getCarrierUserCrossBorderSummary(SessionData);
  		}
  		else
  		{
   			searchErrors = CrossBorderReport.getErrors();
  		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.carrier_user.roaming.roaming_etopup.titlereport",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
		          		<table width="300">
	              	 		<tr>
	               				<td valign="top" nowrap>
									<table width=400>
<%
	if (searchErrors != null)
	{
  		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1=searchErrors.keys();

		while(enum1.hasMoreElements())
		{
  			String strKey = enum1.nextElement().toString();
  			String strError = (String) searchErrors.get(strKey);
  			out.println("<li>" + strError);
		}
  		out.println("</font></td></tr>");
	}
%>
									</table>
			               		</td>
			              	</tr>
			            </table>
<%
	if (vecCurrent != null && vecCurrent.size() > 0)
	{
%>
						<table>
		            		<tr>
		            			<td>
		            				<table>
						              	<tr>
							                <td>
								                <form action="admin/reports/carrier_user/crossborder/summary.jsp" method=post onsubmit="document.getElementById('btnSubmit').disabled = true;">
								                	<input type=hidden name=startDate value="<%=request.getParameter("startDate")%>">
								                	<input type=hidden name=endDate value="<%=request.getParameter("endDate")%>">
								                	<input type=hidden name=download value="Y">
								                	<input id=btnSubmit type=submit value="<%=Languages.getString("jsp.admin.reports.carrier_user.roaming.roaming_etopup.download",SessionData.getLanguage())%>">
								                </form>
							                </td>
						              	</tr>
						            </table>
								</td>
							</tr>
						</table>
						<table width="100%" cellspacing="1" cellpadding="2">
		            		<tr>
		              			<td colspan=8 class=rowhead2 align="center">Cross Border Summary Report from <%=request.getParameter("startDate")+" to "+request.getParameter("endDate")%></td>
			            	</tr>
			            	<tr>
				              	<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.reports.description",SessionData.getLanguage()).toUpperCase()%></td>
				              	<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.reports.crossborder.summary.qty",SessionData.getLanguage()).toUpperCase()%></td>
				              	<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.reports.crossborder.summary.total_source",SessionData.getLanguage()).toUpperCase()%><font color=ff0000>*</font></td>
				              	<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.reports.crossborder.summary.avg_trans",SessionData.getLanguage()).toUpperCase()%></td>
				              	<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.reports.crossborder.summary.unique_customers",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.reports.crossborder.summary.avg_user",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2 align="center"><%=Languages.getString("jsp.admin.reports.crossborder.summary.total_target",SessionData.getLanguage()).toUpperCase()%><font color=ff0000>**</font></td>
			            	</tr>

<%
		Iterator<Vector<String>> it = vecCurrent.iterator();
		String prevCountry = "";
		//store sums
		double currTransCountSum = 0;
		double currSourceTotalSum = 0;
		double currTargetTotalSum = 0;
		double currUniqueCustomersSum = 0;

		int intEvenOdd = 1;
		while(it.hasNext())
		{
			Vector<String> vecTemp = (Vector<String>)it.next();

            if (prevCountry == "" || !prevCountry.equals(vecTemp.get(0)))
            {
%>
		            		<tr>
		              			<td class=row2 align=left colspan=8><b><%=vecTemp.get(0)%></b></td>
		            		</tr>
<%
				prevCountry = vecTemp.get(0);
			}
			else
            {
              	prevCountry = vecTemp.get(0);
            }

            double currTransCount = Double.parseDouble(vecTemp.get(2));
            double currSourceTotal = Double.parseDouble(vecTemp.get(3));
            double currTargetTotal = Double.parseDouble(vecTemp.get(5));
            double currUniqueCustomers = Double.parseDouble(vecTemp.get(7));

            double avgPerTrans = currSourceTotal/currTransCount;
            double avgPerUser  = currSourceTotal/currUniqueCustomers;

            //add to running total
            currTransCountSum = currTransCountSum + currTransCount;
            currSourceTotalSum = currSourceTotalSum + currSourceTotal;
            currTargetTotalSum = currTargetTotalSum + currTargetTotal;
            currUniqueCustomersSum = currUniqueCustomersSum + currUniqueCustomers;

            out.println("<tr class=row" + intEvenOdd + ">" +
                "<td nowrap>&nbsp;&nbsp;&nbsp;<b>" + vecTemp.get(1) + "</b></td>" +
                "<td align=right>" + vecTemp.get(2) + "</td>" +
                "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(3)) + " " + vecTemp.get(4) + "</td>" +
                "<td align=right>" + NumberUtil.formatCurrency(Double.toString(avgPerTrans))+ " " + vecTemp.get(4) + "</td>" +
                "<td align=right>" + vecTemp.get(7) + "</td>" +
                "<td align=right>" + NumberUtil.formatCurrency(Double.toString(avgPerUser)) + " " + vecTemp.get(4) + "</td>" +
                "<td align=right>" + NumberUtil.formatCurrency(vecTemp.get(5)) + " " + vecTemp.get(6) + "</td>" +
                "</tr>");

			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}

		out.println("<tr class=row" + intEvenOdd + ">" +
			"<td nowrap><b>Totals</b></td>" +
			"<td align=right>" + (int)currTransCountSum + "</td>" +
			"<td align=right>" + NumberUtil.formatCurrency(Double.toString(currSourceTotalSum)) + "</td>" +
			"<td align=right>&nbsp;</td>" +
			"<td align=right>" + (int)currUniqueCustomersSum + "</td>" +
			"<td align=right>&nbsp;</td>" +
			"<td align=right>&nbsp;</td>" +
			"</tr>");

		out.println("<tr class=row2><td nowrap colspan=8><font color=ff0000>" + Languages.getString("jsp.admin.reports.crossborder.summary.instructions",SessionData.getLanguage()) + "</font></td></tr>");

		out.println("</table>");

		vecCurrent.clear();
	}
	else if (vecCurrent.size()==0 && request.getParameter("search") != null && searchErrors == null)
	{
%>
						<table width="100%">
				           	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>