<%@page import="com.debisys.utils.HTMLEncoder, com.debisys.presentation.CarrierReportGroup"%>
<%
	int section=14;
	int section_page=14;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<%@ include file="/includes/security.jsp" %>
<LINK REL=StyleSheet HREF="/css/CollapsibleGroupPanel.css" TYPE="text/css"></LINK>
<script type="text/javascript" src="/includes/jquery.js"></script>
<script type="text/javascript" src="/includes/CollapsibleGroupPanel.js"></script>
<%@ include file="/includes/header.jsp" %>
<%
CarrierReportGroup a = CarrierReportGroup.createAnalysisGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType,customConfigType,4);
CarrierReportGroup t = CarrierReportGroup.createTransactionsGroup(SessionData, application, strAccessLevel, strDistChainType, deploymentType, customConfigType,4);
%>
<table border="0" cellpadding="0" cellspacing="0" width="75%">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.carrier_user.reports",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
						<table width="100%" border="0" bgcolor="#FFFCDF" cellpadding="0" cellspacing="0">
							<tr class="backceldas">
								<td width="1"><img src="images/trans.gif" width="1"></td>
								<td align="left" valign="top" bgcolor="#FFFFFF">
									<table border="0" cellpadding="2" cellspacing="0" width="100%" align="left">
										<tr>
											<td width="18">&nbsp;</td>
				     			    		<td class="main">
												<br>		
				        						<table BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="700" height="1">
				          						<tr>
												<TD class=main align="left">
												<%
													if ( t.hasItems() )
													{
												%>
												                              <TABLE class=reportGroupTable>
												                                <TBODY>
												                                <TR>
												                                <TH align=left><IMG height=22 
												                                src="images/transaction_cube.png" 
												                                width=22 border=0><span class="main">
												                                <%=Languages.getString("jsp.admin.reports.transactions",SessionData.getLanguage()).toUpperCase()%>
												                                </SPAN>
												                                </TH></TR>
												                                <TR>
												                                <TD>
												                                <UL class=sublevel name="main">
												                                <%t.showItem(out);%>
												                                </UL></TD></TR>
												                                </TBODY></TABLE>
												<%
													}
												%>
												                                <TABLE class=reportGroupTable>
												                                <TBODY>
												                                <TR>
												                                <TH align=left><IMG height=22 
												                                src="images/analysis_cube.png" 
												                                width=22 border=0> <span class="main">
												                                <%=Languages.getString("jsp.admin.reports.analysis",SessionData.getLanguage()).toUpperCase() %></TH></TR>
												                                <TR>
												                                <TD>
												                                <UL class=sublevel name="main">
												                                <% a.showItem(out); %>
												                                </SPAN>
												                                </LI></UL></TD></TR>
												                                </TBODY></TABLE>
												                                
																					
													     				    	</td>
					          					</tr>
					          					</table>
					                  			<br>
					                  			<br>
					     			    	</td>
					     		       		<td width="18">&nbsp;</td>
					        			</tr>
						      		</table>
						      	</td>
					   			<td width="1"><img src="images/trans.gif" width="1">
					    		</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>