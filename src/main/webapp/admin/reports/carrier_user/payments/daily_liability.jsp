<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.customers.Merchant,
                 com.debisys.users.SessionData" %>
<%int section=14;int section_page=48;%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<%@ include file="/includes/security.jsp" %>
<%@ include file="/includes/header.jsp" %>
<script type="text/JavaScript">
var count = 0
var iProcessMsg = new Array("< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> > ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ","< <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> < ","# <%=Languages.getString("jsp.admin.processing",SessionData.getLanguage())%> # ");
//these variables needs to be declared before linking XmlGetEntities.js
var Isolevel	 	= <%=strAccessLevel.equals(DebisysConstants.ISO)%>;
var Agentlevel 		= <%=strAccessLevel.equals(DebisysConstants.AGENT)%>;
var SubAgentlevel 	= <%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>;
var RepLevel 		= <%=strAccessLevel.equals(DebisysConstants.REP)%>;
var ref_id 			= "<%=SessionData.getProperty("ref_id")%>"; 
var LangAssign 		= "<%=Languages.getString("jsp.admin.transactions.manageinventory.inventory_assignment.all",SessionData.getLanguage())%>";
var validation2 	= <%=SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>;
function scroll()
{
  document.mainform.submit.disabled = true;
  document.mainform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll()', 150);
}
function scroll2()
{
  document.downloadform.submit.disabled = true;
  document.downloadform.submit.value = iProcessMsg[count];
  count++
  if (count = iProcessMsg.length) count = 0
  setTimeout('scroll2()', 150);
}
</script>
<body onload="loadXmlGetEntities();">
<table border="0" cellpadding="0" cellspacing="0" width="750">
	<tr>
    <td width="18" height="20"><img src="images/top_left_blue.gif" width="18" height="20"></td>
    <td background="images/top_blue.gif" width="2000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title38",SessionData.getLanguage()).toUpperCase()%></td>
    <td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  </tr>
  <tr>
  	<td colspan="3"  bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  <tr>
    <td>
	    <form name="mainform" method="post" action="admin/reports/carrier_user/payments/daily_liability_report.jsp" onSubmit="scroll();" id="daily_form">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
     	<tr>
	        <td class="formArea2">
	          <table width="300">
               <tr>
               <td valign="top" nowrap>
<table width=400>
<%if(!strAccessLevel.equals(DebisysConstants.MERCHANT)) {%>
<tr>
    <td class=main nowrap></td>
    <td class=main valign=top>
<% if(strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {%>
    			<br /> <br />
		<%=Languages.getString("jsp.admin.reports.transactions.daily_liability.agent.select",SessionData.getLanguage())%>
		<br />
		<SELECT NAME="AgentTypes" onchange="importAgentXML()">
		 </SELECT>
<%}if((strAccessLevel.equals(DebisysConstants.ISO)|| strAccessLevel.equals(DebisysConstants.AGENT)) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {%>
				<br /> <br />
		<%=Languages.getString("jsp.admin.reports.transactions.daily_liability.sub_agent.select",SessionData.getLanguage())%>
		<br />
		<SELECT NAME="SubAgentTypes" onchange ="importSubAgentXML()">
		 </SELECT>
<%}if(strAccessLevel.equals(DebisysConstants.ISO)|| strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {%>		 
		  		<br /> <br />
		<%=Languages.getString("jsp.admin.reports.transactions.daily_liability.rep.select",SessionData.getLanguage())%>
		<br />
		<SELECT NAME="RepTypes" onchange="importRepXML()">
		 </SELECT>
<%}%>
		 <br /> <br />
		 <%=Languages.getString("jsp.admin.reports.transactions.daily_liability.merchant.select",SessionData.getLanguage())%>
        <br>
        <select name="mids" size="10" style='width:250px;' multiple>
          <option value="" selected><%=Languages.getString("jsp.admin.reports.all",SessionData.getLanguage())%></option>
          <%
          if(strAccessLevel.equals(DebisysConstants.REP)){
          	Vector vecMerchantList = Merchant.getMerchantListReports(SessionData);
  			Iterator it = vecMerchantList.iterator();
  			while (it.hasNext())
  			{
    			Vector vecTemp = null;
    			vecTemp = (Vector) it.next();
    			out.println("<option value=" + vecTemp.get(0) +">" + vecTemp.get(1) + "</option>");
  			}
          }
           %>
        </select>	
        <br>
        *<%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions",SessionData.getLanguage())%><br/>
        <%=Languages.getString("jsp.admin.reports.transactions.merchants.instructions2",SessionData.getLanguage())%>
</td>
</tr>
<%} else {%>
<input type="hidden" name="search" value="y">
	<script language="javascript" type="text/javascript" >
	var form = document.getElementById('daily_form');
	form.submit();
	</script>
	<%}%>
<tr>
    <td class=main colspan=2 align=center>
      <input type="hidden" name="search" value="y">
      <input type="hidden" name="AllValues">
      <input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.reports.show_report",SessionData.getLanguage())%>">
    </td>
</tr>

</table>
               </td>
              </tr>
            </table>

          </td>
      </tr>
    </table>
    </form>
</td>
</tr>
</table>
</td>
</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>