<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
  int section      = 14;
  int section_page = 49;
  boolean NoCarrierProductAttached = false;
%>
  <jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
  <jsp:useBean id="CarrierReport" class="com.debisys.reports.CarrierReport" scope="request" />
  <jsp:setProperty name="CarrierReport" property="*" />
  <%@ include file="/includes/security.jsp" %>
<%
  Vector    vecSearchResults = new Vector();
  Hashtable searchErrors     = null;
  String    sortString       = "";

  if ( request.getParameter("download") != null )
  {
  	  String sURL = "";
  	  CarrierReport.setMerchantIds(request.getParameter("merchantIds"));
      sURL = CarrierReport.downloadDailyLiability(application, SessionData);
  	  response.sendRedirect(sURL);
  	  return;
  }
  if (request.getParameter("search") != null)
  {
      String strMerchantIds[] = request.getParameterValues("mids");

	  //Alfred A./DBSY-1082 - Modified this portion to always take merchant params for later limiting.
      if (strMerchantIds != null){
      	if(strMerchantIds[0] != ""){
        CarrierReport.setMerchantIds(strMerchantIds);
        }else{
        	String vals = request.getParameter("AllValues");
      		String merchantIds[] = vals.split(":");
      		CarrierReport.setMerchantIds(merchantIds);
      }
      }
     
    String carrierUserProds = SessionData.getProperty("carrierUserProds"); 
      if(carrierUserProds!=null && !"".equals(carrierUserProds))
			vecSearchResults = CarrierReport.getDailyLiability(SessionData, false);
	  else
	  	NoCarrierProductAttached = true;
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  if (vecSearchResults != null && vecSearchResults.size() > 0)
  {
%>
    <LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
    <SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  }
%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">
        &nbsp;
        <%= Languages.getString("jsp.admin.reports.title38",SessionData.getLanguage()).toUpperCase() %>
      </td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td class="main">
                <font color="ff0000"><!-- <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.avg_sales_last_30_days",SessionData.getLanguage()) %> --><br/>
                <%= Languages.getString("jsp.admin.reports.transactions.merchants.instructions2",SessionData.getLanguage()) %>
                </font>
              </td>
             </tr>
             <tr>
              <td class=main align=left valign=bottom>
                <%= Languages.getString("jsp.admin.reports.click_to_sort",SessionData.getLanguage()) %>                
              </td>
            </tr>
            <tr>
            	<td colspan=2>
            <table>
              <tr><td>&nbsp;</td></tr>
              <tr>
                <td>
                <FORM ACTION="admin/reports/carrier_user/payments/daily_liability_report.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
                	<INPUT TYPE=hidden NAME=search VALUE="<%=request.getParameter("search")%>">
                	<INPUT TYPE=hidden NAME=merchantIds VALUE="<%=CarrierReport.getMerchantIds()%>">
                	<INPUT TYPE=hidden NAME=download VALUE="Y">
                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                </FORM>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                </td>
              </tr>
            </table>
            	</td>
            </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                <td class=rowhead2>
                  #
                </td>
<%
if(strAccessLevel.equals(DebisysConstants.ISO) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
%>
				<td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.agent",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
} if((strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO)) && !SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)) {
%>
				<td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.subagent",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
} if(strAccessLevel.equals(DebisysConstants.AGENT) || strAccessLevel.equals(DebisysConstants.ISO) || strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
%>
				<td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.rep",SessionData.getLanguage()).toUpperCase() %>
                </td>
<%
}
%>     
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.merchant_name",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.contact_phone_number",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.remaining_days",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.available_credit",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.credit_limit",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.sales_since_last_adj",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.avg_sales_per_day",SessionData.getLanguage()).toUpperCase() %>
                </td>             
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.transactions.daily_liability_report.lastpurchase",SessionData.getLanguage()).toUpperCase() %>
                </td>
                        
              </tr>
            </thead>
<%
            Iterator it                       = vecSearchResults.iterator();
            int      intEvenOdd               = 1;
            int      intCounter               = 1;
              
            //Totals
            double dblRemainingDaysTotal = 0;
            double dblAvailableCredits = 0;
            double dblCreditLimit = 0;
            double dblSalesSinceLastAdj = 0;
            double dblAverageSalesPerDay = 0;
            while (it.hasNext())
            {
              Vector vecTemp = null;
              vecTemp = (Vector)it.next();
              	if (strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
              out.print("<tr class=row" + intEvenOdd + ">" + 
            		  "<td>" + intCounter++ + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(0) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(1) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(2) + "</td>" +
            		  "<td nowrap>" + vecTemp.get(3) + "</td>" +
            		  "<td nowrap align=right>" + vecTemp.get(4) + "</td>" + 
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>");
            				out.print("<td nowrap>" + vecTemp.get(9) + "</td>");
				}else if (strAccessLevel.equals(DebisysConstants.ISO)){
					out.print("<tr class=row" + intEvenOdd + ">" + 
            		  "<td>" + intCounter++ + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(0) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(1) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(2) + "</td>" +
            		  "<td nowrap>" + vecTemp.get(3) + "</td>" +
            		  "<td nowrap>" + vecTemp.get(4) + "</td>" +
            		  "<td nowrap>" + vecTemp.get(5) + "</td>" +
            		  "<td nowrap align=right>" + vecTemp.get(6) + "</td>" + 
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(10).toString()) + "</td>");
 							out.print("<td nowrap>" + vecTemp.get(11) + "</td>");
				}else if(strAccessLevel.equals(DebisysConstants.AGENT)){
					out.print("<tr class=row" + intEvenOdd + ">" + 
            		  "<td>" + intCounter++ + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(0) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(1) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(2) + "</td>" +
            		  "<td nowrap>" + vecTemp.get(3) + "</td>" +
            		  "<td nowrap>" + vecTemp.get(4) + "</td>" +
            		  "<td nowrap align=right>" + vecTemp.get(5) + "</td>" + 
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(9).toString()) + "</td>");
            				out.print("<td nowrap>" + vecTemp.get(10) + "</td>");
				}else if(strAccessLevel.equals(DebisysConstants.SUBAGENT) ){
					out.print("<tr class=row" + intEvenOdd + ">" + 
            		  "<td>" + intCounter++ + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(0) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(1) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(2) + "</td>" +
            		  "<td nowrap>" + vecTemp.get(3) + "</td>" +
            		  "<td nowrap align=right>" + vecTemp.get(4) + "</td>" + 
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(8).toString()) + "</td>");
            				out.print("<td nowrap>" + vecTemp.get(9) + "</td>");
					}else{
					out.print("<tr class=row" + intEvenOdd + ">" + 
            		  "<td>" + intCounter++ + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(0) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(1) + "</td>" + 
            		  "<td nowrap>" + vecTemp.get(2) + "</td>" +
            		  "<td nowrap align=right>" + vecTemp.get(3) + "</td>" + 
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(4).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(5).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(6).toString()) + "</td>" +
            		  "<td nowrap align=right>" + NumberUtil.formatCurrency(vecTemp.get(7).toString()) + "</td>");
            				out.print("<td nowrap>" + vecTemp.get(8) + "</td>");
				}
              out.println("</tr>");

              if (intEvenOdd == 1)
              {
                intEvenOdd = 2;
              }
              else
              {
                intEvenOdd = 1;
              }
            	int startCounter = -1;
            	//to get the totals
            	if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
            		startCounter = 4;
            	}else if (strAccessLevel.equals(DebisysConstants.ISO)){
            		startCounter = 6;
            	}else if (strAccessLevel.equals(DebisysConstants.AGENT)){
            		startCounter = 5;
            	}else if (strAccessLevel.equals(DebisysConstants.SUBAGENT)){
            		startCounter = 4;
            	}else{
            		startCounter = 3;
            	}
            
            	dblRemainingDaysTotal += Double.parseDouble(vecTemp.get(startCounter).toString());
            	dblAvailableCredits += Double.parseDouble(vecTemp.get(startCounter+1).toString());
            	dblCreditLimit += Double.parseDouble(vecTemp.get(startCounter+2).toString());
            	dblSalesSinceLastAdj += Double.parseDouble(vecTemp.get(startCounter+3).toString());
            	dblAverageSalesPerDay += Double.parseDouble(vecTemp.get(startCounter+4).toString());            	
            	
            }
%> 
			<tfoot>
			<tr class=row<%=intEvenOdd%>>
<%
if(strAccessLevel.equals(DebisysConstants.ISO) && SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)){
%>
				<td colspan=5 align=right>
<%
}else if(strAccessLevel.equals(DebisysConstants.ISO)) {
%>
				<td colspan=7 align=right>
<%
}else if(strAccessLevel.equals(DebisysConstants.AGENT)) {
%>
				<td colspan=6 align=right>
<%
}else if(strAccessLevel.equals(DebisysConstants.SUBAGENT)) {
%>
				<td colspan=5 align=right>
<%
}else {
%>
				<td colspan=4 align=right>
<%
}
%>
				<%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
				<td align=right><%=dblRemainingDaysTotal%></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblAvailableCredits)) %></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblCreditLimit)) %></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblSalesSinceLastAdj)) %></td>
				<td align=right><%=NumberUtil.formatCurrency(Double.toString(dblAverageSalesPerDay))%></td>
			</tr>
			</tfoot>          
          </table>
<%
        }
        else
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
           if(NoCarrierProductAttached)
                out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage()) + 
                                    "</font>");
           else
                out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
        if (vecSearchResults != null && vecSearchResults.size() > 0)
        {
%>
          <SCRIPT type="text/javascript">
            
                    //<!--
   var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number"],0,false,false);
  if(<%=strAccessLevel.equals(DebisysConstants.ISO)&& SessionData.getProperty("dist_chain_type").equals(DebisysConstants.DIST_CHAIN_3_LEVEL)%>){
	stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number","DateTime"],0,false,false); 
  }else if(<%=strAccessLevel.equals(DebisysConstants.ISO)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime"],0,false,false);
		
  }else if(<%=strAccessLevel.equals(DebisysConstants.AGENT)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime"],0,false,false);
  }else if(<%=strAccessLevel.equals(DebisysConstants.SUBAGENT)%>){
			stT1 = new SortROC(document.getElementById("t1"),
		  	["None","CaseInsensitiveString", "Number", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number", "Number", "DateTime"],0,false,false);
  }
  //-->
            
          </SCRIPT>
<%
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
