<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.languages.*,
                 com.debisys.utils.HTMLEncoder" %>
<%
	int section=14;
	int section_page=19;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector vecSearchResults = new Vector();
	Hashtable searchErrors = null;
	int intRecordCount = 0;
	String queryType = null;

  	if (request.getParameter("queryType") != null) 
  	{
	    queryType = request.getParameter("queryType");
  	}

  	if (TransactionReport.validateDateRange(SessionData))
  	{
    	SessionData.setProperty("start_date", request.getParameter("startDate"));
    	SessionData.setProperty("end_date", request.getParameter("endDate"));
    	vecSearchResults = TransactionReport.getCarrierUserTransactionErrorDetailByTypes(1, 100000, SessionData, true, queryType);
    	intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    	vecSearchResults.removeElementAt(0);
  	}
  	else
  	{
	    searchErrors = TransactionReport.getErrors();
  	}
%>
<html>
<head>
    <link href="../../../../default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
	  	<td width="18" height="20" align=left><img src="../../../../images/top_left_blue.gif"></td>
	  	<td background="../../../../images/top_blue.gif" class="formAreaTitle" align=left width="3000">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.transaction_error_details.title",SessionData.getLanguage()).toUpperCase()%></td>
	  	<td width="12" height="20" align=right><img src="../../../../images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
            	<tr>
            		<td class="main"><%=intRecordCount%> result(s) found<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
            %>.
            		</td>
            	</tr>
			</table>
            <table width="100%" cellspacing="1" cellpadding="2">
            	<thead>
      	      	<tr>
              		<td class=rowhead2>#</td>
              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.transaction_id",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.amount",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.clerk_id",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.product_id",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.phone",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.type",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.host",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.port",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.duration",SessionData.getLanguage()).toUpperCase()%></td>
              		<td class=rowhead2 nowrap><%=Languages.getString("jsp.admin.reports.result",SessionData.getLanguage()).toUpperCase()%></td>
            	</tr>
				</thead>
<%
		int intCounter = 1;
        Iterator it = vecSearchResults.iterator();
		int intEvenOdd = 1;
                  
		while (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();
			out.println("<tr class=row" + intEvenOdd +">" +
				"<td nowrap>" + intCounter++ + "</td>" +
				"<td nowrap>" + vecTemp.get(0)+ "</td>" +
				"<td nowrap>" + vecTemp.get(1) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(2) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(3) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(4) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(5) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(6) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(7) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(8) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(9) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(10) + "</td>" +
				"<td nowrap align=\"right\">" + vecTemp.get(11) + "</td>" +
				"</tr>");

			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}
		vecSearchResults.clear();
%>
			</table>
            <table id="tblClose" style="display:block;width:100%;">
				<tr>
					<td align=center><input type=button value="<%=Languages.getString("jsp.admin.reports.closewindow",SessionData.getLanguage())%>" onclick="window.close();/*window.setTimeout('', 1000);*/">
					</td>
				</tr>
            </table>
<%
	}
	else
	{
 		out.println("<br><br><font color=ff0000>"+Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())+"</font>");
	}
%>
		</td>
	</tr>
</table>
<script>
document.getElementById("tblClose").style.display = "none";
window.print();
document.getElementById("tblClose").style.display = "block";
</script>
</body>
</html>