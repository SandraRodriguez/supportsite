<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.*,
                 com.debisys.customers.Merchant,
                 java.util.*" %>
<%@page import="com.debisys.tools.MarkedCollection"%>
<%@page import="com.debisys.utils.TimeZone"%>
<%
	int section = 14;
	int section_page = 34;
	boolean downloadbar = false;
	boolean maxdownloadbar = false;
	int maxdownloadset = -1 ;
  boolean NoCarrierProductAttached = false;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionSearch" property="*"/>

<%@ include file="/includes/security.jsp" %>

<%
	Vector<Vector<String>> vecSearchResults = new Vector<Vector<String>>();
	Hashtable<String, Vector<String>> successfulTransData = new Hashtable<String, Vector<String>>();
	Hashtable searchErrors = null;
	
	
	String strImageDown = "<img src=\"images/down.png\" height=11 width=11 border=0>";
	String strImageUp = "<img src=\"images/up.png\" height=11 width=11 border=0>";
	
	
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;
	
	// Pulling these out for easier scanning
	boolean customConfigTypeIsMexico = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO);
	boolean customConfigTypeIsDefault = DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);

	boolean deployTypeIsIntl = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL);
	boolean deployTypeIsDomestic = DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC);
	
	boolean invoiceNumberEnabled = com.debisys.users.User.isInvoiceNumberEnabled(SessionData, application);

	boolean hasPermViewPin = SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN);
	
	boolean accessLevelIsISO = strAccessLevel.equals(DebisysConstants.ISO);
	
	if (request.getParameter("search") != null)
	{
  		if (request.getParameter("page") != null)
  		{
    		try
    		{
      			intPage=Integer.parseInt(request.getParameter("page"));
    		}
    		catch(NumberFormatException ex)
    		{
      			intPage = 1;
    		}
  		}

  		if (intPage < 1)
  		{
    		intPage = 1;
  		}

  		if (TransactionSearch.validateDateRange(SessionData))
  		{
    		if (request.getParameter("invoiceID") != null)
    		{
    			TransactionSearch.setInvoiceNo(request.getParameter("invoiceID").toString());
    		}
    
    		String carrierUserProds = SessionData.getProperty("carrierUserProds");
    		
    		SessionData.setProperty("start_date", request.getParameter("startDate"));
    		SessionData.setProperty("end_date", request.getParameter("endDate"));
    		
    		if(carrierUserProds!=null && !"".equals(carrierUserProds)){
    			intRecordCount = TransactionSearch.carrierUserSearchGetCount(intPage, intPageSize, SessionData, application);
				vecSearchResults = TransactionSearch.carrierUserSearch(intPage, intPageSize, SessionData, application);
			}
			else
      	 		NoCarrierProductAttached = true;
    		
    		// You've got problems if this happened
    		if(vecSearchResults == null)
    		{
    			// do something to indicate an error has occurred.
    		}
    			
    	    if (intRecordCount>0)
    	    
    	    {
    	      	intPageCount = (intRecordCount / intPageSize);
    	      	if ((intPageCount * intPageSize) < intRecordCount)
    	      	{
    	        	intPageCount++;
    	      	}
    	    }
	  	}
	  	else
	  	{
	   		searchErrors = TransactionSearch.getErrors();
	  	}
	}
%>
<%@ include file="/includes/header.jsp" %>
<script>
function showHideRow(id){ 
	if (document.getElementById){ 
		obj = document.getElementById(id); 
		if (obj.style.display == "none"){ 
			obj.style.display = ""; 
		} else { 
			obj.style.display = "none"; 
		} 	
	} 
} 
</script>
<%
  	if (vecSearchResults != null && vecSearchResults.size() > 0)
  	{
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  	}
%>
<table border="0" cellpadding="0" cellspacing="0" width="1200">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.transactions.transactions.title",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
  		<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
	        		<td class="formArea2">
	    				<form name="mainform" method="post" action="admin/reports/carrier_user/transactions/transactions.jsp">
      					<table width="300">
<%
	if(SessionData.getProperty("carrierUserProds") == null || SessionData.getProperty("carrierUserProds").equals(""))
	{
%>
							<tr class=main>
								<td colspan=2><font color="red">NOTE: This Carrier User does not have any products allowed for report viewing.</font></td>
							</tr>
<%		
	}

	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
							<tr class="main">
  								<td nowrap colspan="2">
  									<%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%>
  									<br/>
  									<br/>
  								</td>
  							</tr>  
 							<tr class="main">
   								<td nowrap valign="top">
<%
	if(customConfigTypeIsMexico)
	{ 
%>
									<%=Languages.getString("jsp.admin.select_date_range",SessionData.getLanguage())%> :
<%
	}
	else
	{ 
%>
									<%=Languages.getString("jsp.admin.select_date_range_international",SessionData.getLanguage())%> :
<%
	}
%>
								</td>
         						<td>&nbsp;</td>
        					</tr>           
               				<tr>
               					<td valign="top" nowrap>
									<table width=400>
<%
	if (searchErrors != null)
	{
  		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1=searchErrors.keys();

		while(enum1.hasMoreElements())
		{
  			String strKey = enum1.nextElement().toString();
  			String strError = (String) searchErrors.get(strKey);
  			out.println("<li>" + strError);
		}
  		
		out.println("</font></td></tr>");
	}
%>
										<tr>
											<td class="main" colspan=2>
												* <%=Languages.getString("jsp.admin.reports.general",SessionData.getLanguage())%>
											</td>
										</tr>
										<tr class="main">
										    <td nowrap><%=Languages.getString("jsp.admin.start_date",SessionData.getLanguage())%>:</td>
										    <td>
										    	<input class="plain" id="startDate" name="startDate" value="<%=SessionData.getProperty("start_date")%>" size="12" />
										    	<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fStartPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS>
										    		<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
										    	</a>
										   	</td>
										</tr>
										<tr class="main">
								    		<td nowrap><%=Languages.getString("jsp.admin.end_date",SessionData.getLanguage())%>: </td>
								    		<td> 
								    			<input class="plain" id="endDate" name="endDate" value="<%=SessionData.getProperty("end_date")%>" size="12" />
								    			<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fEndPop(document.mainform.startDate,document.mainform.endDate);return false;" HIDEFOCUS>
								    				<img name="popcal" align="absmiddle" src="admin/calendar/calbtn.png" width="34" height="22" border="0" alt="">
								    			</a>
								    		</td>
										</tr>
										<tr class="main">
											<td nowrap><%=Languages.getString("jsp.admin.reports.transaction_id",SessionData.getLanguage())%>: </td>
											<td>
												<input name="transactionID" id="transactionID" value="<%=TransactionSearch.getTransactionID()%>" />
											</td> 
										</tr>
<% 
	if(invoiceNumberEnabled && deployTypeIsIntl && customConfigTypeIsDefault)
	{
%>
										<tr class="main">
										    <td nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>: </td>
										    <td>
										    	<input name="invoiceID" id="invoiceID" value="<%=TransactionSearch.getInvoiceNo()%>" />
										    </td> 
										</tr>
<%
	} 
	else
	{ 
%>
										<tr style="display:none;" class="main">
										    <td style="display:none;" nowrap><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>: </td>
										    <td>
										    	<input name="invoiceID" id="invoiceID" style="display:none;"  value="<%=TransactionSearch.getInvoiceNo()%>" />
										    </td> 
										</tr>
<%
	} 
%>
										<tr class="main">
											<td nowrap><%=Languages.getString("jsp.admin.reports.pinreturn_search.pin",SessionData.getLanguage())%>: </td>
											<td>
												<input name="PINNumber" id="PINNumber" value="<%=TransactionSearch.getPINNumber()%>" />
											</td> 
										</tr>
										<tr class="main">
											<td nowrap><%=Languages.getString("jsp.admin.amount",SessionData.getLanguage())%>: </td>
											<td>
												<input name="rechargeAmt" id="rechargeAmt" value="<%=TransactionSearch.getRechargeAmt()%>" />
											</td>
										</tr>
										<tr class="main">
											<td nowrap><%=Languages.getString("jsp.admin.report.carrier_user.transactions.trans_summ_report.merchant_criteria",SessionData.getLanguage())%>: </td>
											<td>
												<input name="criteria" id="criteria" value="<%=TransactionSearch.getCriteria()%>" />
											</td>
										</tr>
										<tr>
											<td class="main" colspan=2>
												(<%=Languages.getString("jsp.admin.customers.merchants.search_instructions",SessionData.getLanguage())%>)
											</td>
										</tr>
										<tr class="main">
											<td nowrap><%=Languages.getString("jsp.admin.report.carrier_user.transactions.trans_summ_report.transaction_result",SessionData.getLanguage())%>: </td>
											<td>
												<select name="transactionResult" id="transactionResult">
													<option <%=((TransactionSearch.getTransactionResult().equals("ALL")) ? "selected" : "") %> value="ALL"><%=Languages.getString("jsp.admin.report.carrier_user.transactions.trans_summ_report.all",SessionData.getLanguage())%></option>
													<option <%=((TransactionSearch.getTransactionResult().equals("SUCCESS")) ? "selected" : "") %> value="SUCCESS"><%=Languages.getString("jsp.admin.report.carrier_user.transactions.trans_summ_report.successful",SessionData.getLanguage())%></option>
													<option <%=((TransactionSearch.getTransactionResult().equals("FAIL")) ? "selected" : "") %> value="FAIL"><%=Languages.getString("jsp.admin.report.carrier_user.transactions.trans_summ_report.failed",SessionData.getLanguage())%></option>
												</select>
											</td>
										</tr>
										<tr>    
									    	<td class=main>
									      		<input type="hidden" name="search" value="y" />
									      		<input type="submit" name="submit" value="<%=Languages.getString("jsp.admin.transactions.show_transactions",SessionData.getLanguage())%>" />
									      		</form>
									    	</td>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
									    	<td align=left class="main" nowrap>
                								<form name="downloadform" method=post action="admin/reports/carrier_user/transactions/download_transactions.jsp" onSubmit="scroll2('false');">	
			              							<input type="hidden" id="startDate" name="startDate" value="<%=TransactionSearch.getStartDate()%>">
									              	<input type="hidden" id="endDate" name="endDate" value="<%=TransactionSearch.getEndDate()%>">
									              	<input type="hidden" id="criteria" name="criteria" value="<%=TransactionSearch.getCriteria()%>">              
									              	<input type="hidden" id="transactionID" name="transactionID" value="<%=TransactionSearch.getTransactionID()%>">       
									              	<input type="hidden" id="invoiceID" name="invoiceID" value="<%=TransactionSearch.getInvoiceNo()%>">
									              	<input type="hidden" id="PINNumber" name="PINNumber" value="<%=TransactionSearch.getPINNumber()%>">
									              	<input type="hidden" id="rechargeAmt" name="rechargeAmt" value="<%=TransactionSearch.getRechargeAmt()%>">
									              	<input type="hidden" id="transactionResult" name="transactionResult" value="<%=TransactionSearch.getTransactionResult()%>">
									              	
									              	<input type="hidden" id="recordcount" name="recordcount" value="<%=intRecordCount%>">									              	
									              	<input type="hidden" id="page" name="page" value="<%=intPage%>">
									              	<input type="hidden" id="section_page" name="section_page" value="<%=section_page%>">
									              	<input type="hidden" id="download" name="download" value="y">
              										<input type=submit name=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
              									</form>
              									<br/>
              								</td>
<%
	}
%>
										</tr>
									</table>
								</td>
							</tr>
						</table>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
           				<table width="100%" border="0" cellspacing="0" cellpadding="2">
            				<tr>
            					<td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%><%
		if (!TransactionSearch.getStartDate().equals("") && !TransactionSearch.getEndDate().equals(""))
		{
			out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) +" "+ HTMLEncoder.encode(TransactionSearch.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionSearch.getEndDateFormatted()));
		}
            %>. <%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount)},SessionData.getLanguage())%>
            					</td>
            				</tr>
            				<tr>
              					
            				</tr>
            				<tr>
              					<td align=right class="main" nowrap>
<%
		StringBuffer pagingLink = new StringBuffer();
		pagingLink.append("<a href=\"admin/reports/carrier_user/transactions/transactions.jsp?search=");
		pagingLink.append(URLEncoder.encode(request.getParameter("search"), "UTF-8"));

		pagingLink.append("&startDate=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getStartDate(), "UTF-8"));
		pagingLink.append("&endDate=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getEndDate(), "UTF-8"));
		pagingLink.append("&transactionID=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getTransactionID(), "UTF-8"));
		pagingLink.append("&invoiceID=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getInvoiceNo(), "UTF-8"));
		pagingLink.append("&PINNumber=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getPINNumber(), "UTF-8"));
		pagingLink.append("&rechargeAmt=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getRechargeAmt(), "UTF-8"));
		pagingLink.append("&criteria=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getCriteria(), "UTF-8"));
		pagingLink.append("&transactionResult=");
		pagingLink.append(URLEncoder.encode(TransactionSearch.getTransactionResult(), "UTF-8"));
		
		if (intPage > 1)
		{
			out.println(pagingLink.toString() + "&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
			out.println(pagingLink.toString() + "&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
		}
              
		int intLowerLimit = intPage - 12;
		int intUpperLimit = intPage + 12;

		if (intLowerLimit<1)
		{
			intLowerLimit=1;
			intUpperLimit = 25;
		}

		for(int i = intLowerLimit; i <= intUpperLimit && i <= intPageCount; i++)
		{
			if (i==intPage)
			{
				out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
			}
			else
	    	{
				out.println(pagingLink.toString() + "&page=" + i + "\">" + i + "</a>&nbsp;");
			}
		}

		if (intPage <= (intPageCount-1))
		{
			out.println(pagingLink.toString() + "&page=" + (intPage+1) + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
			out.println(pagingLink.toString() + "&page=" + (intPageCount) + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
		}
%>
              					</td>
            				</tr>
            			</table>
            			<table>
							<tr>
						    	<td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%>
								</td>
							</tr>	
						</table>
						<style type="text/css">
							table.noWrapTable td {
								white-space:nowrap;
							}
						</style>
<%
		String transactionID = TransactionSearch.getTransactionID();
		transactionID = (transactionID.equals("") ? "" : "&transactionID=" + transactionID);
		String invoiceID = TransactionSearch.getInvoiceNo();
		invoiceID = (invoiceID.equals("") ? "" : "&invoiceID=" + invoiceID);
		String PINNumber = TransactionSearch.getPINNumber();
		PINNumber = (PINNumber.equals("") ? "" : "&PINNumber=" + PINNumber);
		String rechargeAmt = TransactionSearch.getRechargeAmt();
		rechargeAmt = (rechargeAmt.equals("") ? "" : "&rechargeAmt=" + rechargeAmt);
		String criteria = TransactionSearch.getCriteria();
		criteria = (criteria.equals("") ? "" : "&criteria=" + criteria);
		String transactionResult = TransactionSearch.getTransactionResult();
		transactionResult = (transactionResult.equals("") ? "" : "&transactionResult=" + transactionResult);
	
		String strSortURL = "admin/reports/carrier_user/transactions/transactions.jsp?search=y&startDate=" + TransactionSearch.getStartDate() + "&endDate=" + TransactionSearch.getEndDate() + transactionID + invoiceID + PINNumber + rechargeAmt + criteria + transactionResult;
%>						
						<table width="100%" cellspacing="1" cellpadding="2" class="noWrapTable">
							<tr>
					        	<td class=rowhead2 valign=bottom>#</td>
					            <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=1&sort=2"><%=strImageUp%></a></td>
					            <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=2&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=2&sort=2"><%=strImageUp%></a></td>
					            <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=3&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=3&sort=2"><%=strImageUp%></a></td>
					            <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=4&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=4&sort=2"><%=strImageUp%></a></td>
					            <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
              					<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=6&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=6&sort=2"><%=strImageUp%></a></td>
					            <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.phone",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=7&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=7&sort=2"><%=strImageUp%></a></td>
					            <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=8&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=8&sort=2"><%=strImageUp%></a></td>
            					<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.transaction_type",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=9&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=9&sort=2"><%=strImageUp%></a></td>
								<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_type",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=10&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=10&sort=2"><%=strImageUp%></a></td>
              					<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.host",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=11&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=11&sort=2"><%=strImageUp%></a></td>
              					<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.duration",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=12&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=12&sort=2"><%=strImageUp%></a></td>
              					<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.result",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=13&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=13&sort=2"><%=strImageUp%></a></td>
              					<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.provider_response",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=14&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=14&sort=2"><%=strImageUp%></a></td>
<% 
		if(invoiceNumberEnabled && deployTypeIsIntl && customConfigTypeIsDefault)
		{
%>
								<td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.invoiceno",SessionData.getLanguage())%>&nbsp;<a href="<%=strSortURL%>&col=15&sort=1"><%=strImageDown%></a><a href="<%=strSortURL%>&col=15&sort=2"><%=strImageUp%></a></td>
<%
		}
%>
            				</tr>
<%
		int intCounter = 1;
		int intEvenOdd = 1; 

		Iterator<Vector<String>> it = vecSearchResults.iterator();                 
  
		// Create all of the headers for successful transactions. Speed things up a bit	
		StringBuffer successHeaders = new StringBuffer();
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.city",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.county",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.product",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()) + "</td>");
		successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.transaction_type",SessionData.getLanguage()) + "</td>");
		
		if(hasPermViewPin && accessLevelIsISO && deployTypeIsDomestic) 
		{
			successHeaders.append("<td class=rowhead2>" + Languages.getString("jsp.admin.reports.pin_number",SessionData.getLanguage()) + "</td>");	
		}
		
		while (it.hasNext())
		{
			Vector<String> vecTemp = null;
			vecTemp = it.next();
			
			out.print("<tr class=row" + intEvenOdd + ">");
			out.print("<td>" + intCounter + "</td>");
			// Datetime
			out.print("<td>" + vecTemp.get(0)+ "</td>");
			// Transaction ID
			// The 12th vector specifies the transaction result
			// On a click, show the web_transactions data
			if(vecTemp.get(12).equals("0"))
				out.print("<td><a href=\"javascript:showHideRow('transrow" + intCounter + "');\">" + vecTemp.get(1) + "</a></td>");
			else
				out.print("<td>" + vecTemp.get(1) + "</td>");
			// Recharge value
			out.print("<td>" + vecTemp.get(2) + "</td>");
			// DBA
			out.print("<td>" + vecTemp.get(3) + "</td>");
			// Clerk 
			out.print("<td>" + vecTemp.get(4) + "</td>");
			// Product ID
			out.print("<td>" + vecTemp.get(5) + "</td>");
			// Phone/Reference #
			out.print("<td>" + vecTemp.get(6) + "</td>");
			// Site ID
			out.print("<td>" + vecTemp.get(7) + "</td>");
			// Transaction type/Transaction type description
			out.print("<td>" + vecTemp.get(8) + "</td>");
			// Terminal Type
			out.print("<td>" + vecTemp.get(9) + "</td>");
			// Host application type
			out.print("<td>" + vecTemp.get(10) + "</td>");
			// Transaction duration
			out.print("<td>" + vecTemp.get(11) + "</td>");
			// Result/Result Description
			out.print("<td>" + vecTemp.get(13) + "</td>");
			// Provider Response
			out.print("<td>" + vecTemp.get(14) + "</td>");
			
			// Invoice number
			if(invoiceNumberEnabled && deployTypeIsIntl && customConfigTypeIsDefault)
			{
				out.print("<td>" + vecTemp.get(15) + "</td>");
			}
			
			out.print("</tr>");
			out.println("");
			
			// Show more info if the transaction is successful
    		// Looks like for certain transaction types, there is no web_transaction record. For example, Type 1/Host Report
    		// Also, not always is there web_transactions info in the DB for successful transactions
			if(vecTemp.get(12).equals("0") && vecTemp.size() > 16)
			{
				out.print("<tr>");
				out.print("<td colspan=2>");
				out.print("</td>");
				out.print("<td colspan=14>");
				out.print("<div id=\"transrow" + intCounter + "\" style=\"display:none;\" ><table cellspacing=1 cellpadding=2>");
				out.print("<tr>");
				out.print(successHeaders.toString());
				out.print("</tr>");
				out.print("<tr class=row1>");
				// Transaction ID
				out.print("<td>" + vecTemp.get(1) + "</td>");
				// Site ID
				out.print("<td >" + vecTemp.get(7) + "</td>");
				// DBA
				out.print("<td>" + vecTemp.get(3) + "</td>");
				// Merchant ID
				out.print("<td>" + vecTemp.get(16) + "</td>");
				// Date
				out.print("<td>" + vecTemp.get(0) + "</td>");
				// City
				out.print("<td>" + vecTemp.get(17) + "</td>");
				// County
				out.print("<td>" + vecTemp.get(18) + "</td>");
				// Clerk
				out.print("<td>" + vecTemp.get(19) + "</td>");
				// Ref No
				out.print("<td>" + vecTemp.get(25) + "</td>");
				// Recharge Value
				out.print("<td>" + vecTemp.get(2) + "</td>");
				// Bonus Amt
				out.print("<td>" + vecTemp.get(20) + "</td>");
				// Total Recharge
				out.print("<td>" + vecTemp.get(26) + "</td>");
				// Balance
				out.print("<td>" + vecTemp.get(21) + "</td>");
				// Product ID
				out.print("<td>" + vecTemp.get(5) + "</td>");
				// Control #
				out.print("<td>" + vecTemp.get(22) + "</td>");
				// Transaction Type
				out.print("<td>" + vecTemp.get(23) + "</td>");
				
				// PIN
				if(hasPermViewPin && accessLevelIsISO && deployTypeIsDomestic) 
				{
					if(vecTemp.get(24) != null)
						out.print("<td>" + vecTemp.get(24) + "</td>");
					else
						out.print("<td></td>");
				}
				
				out.print("</tr>");
				out.print("</table></div>");
				out.print("</td>");
				out.print("</tr>");
				out.println("");
			}
			else if(vecTemp.get(12).equals("0") && vecTemp.size() <= 16)
			{
				// Warn the user that this is a successful transaction but no web_transactions info was avavilable
				out.print("<tr class=rowred2>");
				out.print("<td colspan=2>");
				out.print("</td>");
				out.print("<td colspan=14>");
				out.print("<div id=\"transrow" + intCounter + "\" style=\"display:none;\" >");
				out.print("No additional information available for this transaction");
				out.print("</div>");
				out.print("</td>");
				out.print("</tr>");
				out.println("");
			}
			
			if (intEvenOdd == 1)
              	intEvenOdd = 2;
            else
              	intEvenOdd = 1;
			
			intCounter = intCounter + 1;
    	}
        
		vecSearchResults.clear();
		vecSearchResults = null;
%>
						</table>
<%
	}
	else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
	{	
%>
						<table>
							<tr>
							<% if(NoCarrierProductAttached){ %>
								<td><br><br><font color=ff0000><%=Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage())%></font></td>				
							<% }else{ %>
								<td><br><br><font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font></td>									
							<% } %>
							</tr>
						</table>
<%
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%@ include file="/includes/footer.jsp" %>