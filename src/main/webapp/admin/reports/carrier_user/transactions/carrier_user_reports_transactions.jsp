<tr>
	<td VALIGN="middle" WIDTH="376" height="22">
		<span style="font-weight: 700"><font face="Arial, Helvetica, sans-serif" style="font-size: 13px" color="#2C0973">
		<img border="0" src="images/transaction_cube.png" width="22" height="22"><%=Languages.getString("jsp.admin.reports.carrier_user.transactions",SessionData.getLanguage())%> </font></span>
	</td>
</tr>
<tr>
	<td VALIGN="top" WIDTH="376" height="28">
		<font color="#2C0973" face="Arial, Helvetica, sans-serif" style="font-size: 13px">
		<a href="admin/reports/carrier_user/transactions/products.jsp" style="text-decoration: none">
        - <%=Languages.getString("jsp.admin.reports.carrier_user.transactions.trans_summ_by_prod",SessionData.getLanguage())%></a></font>
	</td>
</tr>
<tr>
	<td VALIGN="top" WIDTH="376" height="28">
		<font color="#2C0973" face="Arial, Helvetica, sans-serif" style="font-size: 13px">
		<a href="admin/reports/carrier_user/transactions/terminals.jsp" style="text-decoration: none">
        - <%=Languages.getString("jsp.admin.reports.carrier_user.transactions.trans_summ_by_term_type",SessionData.getLanguage())%></a></font>
	</td>
</tr>
<tr>
	<td VALIGN="top" WIDTH="376" height="28">
		<font color="#2C0973" face="Arial, Helvetica, sans-serif" style="font-size: 13px">
		<a href="admin/reports/carrier_user/transactions/transaction_error.jsp?filter=merchants" style="text-decoration: none">
        - <%=Languages.getString("jsp.admin.reports.carrier_user.transactions.trans_err_summ_report",SessionData.getLanguage())%></a></font>
	</td>
</tr>
<tr>
	<td VALIGN="top" WIDTH="376" height="28">
		<font color="#2C0973" face="Arial, Helvetica, sans-serif" style="font-size: 13px">
		<a href="admin/reports/carrier_user/transactions/transactions.jsp" style="text-decoration: none">
        - <%=Languages.getString("jsp.admin.reports.carrier_user.transactions.trans_summ_report",SessionData.getLanguage())%></a></font>
	</td>
</tr>