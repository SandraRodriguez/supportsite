<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
	int section      = 14;
	int section_page = 32;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
  	Vector    vecSearchResults = new Vector();
  	Hashtable searchErrors     = null;
  	String    sortString       = "";

  	if ( request.getParameter("Download") != null )
  	{
	  	TransactionReport.setMerchantIds(request.getParameter("MerchantIDs"));
	  	String sURL = TransactionReport.downloadCarrierUserTransactionErrorSummaryByMerchant(application, SessionData);
	  	response.sendRedirect(sURL);
	  	return;
  	}
  
  	if (request.getParameter("search") != null)
  	{
    	if (TransactionReport.validateDateRange(SessionData))
    	{
      		SessionData.setProperty("start_date", request.getParameter("startDate"));
      		SessionData.setProperty("end_date", request.getParameter("endDate"));
	
      		String strMerchantIds[] = request.getParameterValues("mids");
	
      		if (strMerchantIds != null)
      		{
        		TransactionReport.setMerchantIds(strMerchantIds);
      		}

	  		//DJC R24 added to allow a new filter by product for this report
	  		String strProductIds[] = request.getParameterValues("pids");
	
      		if (strProductIds != null)
      		{
        		TransactionReport.setProductIds(strProductIds);
      		}
      		//DJC R24

      		vecSearchResults = TransactionReport.getCarrierUserTransactionErrorSummaryByMerchants(SessionData);
    	}
    	else
    	{
      		searchErrors = TransactionReport.getErrors();
    	}
  	}
%>
<%@ include file="/includes/header.jsp" %>
<%
  	if (vecSearchResults != null && vecSearchResults.size() > 0)
  	{
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  	}
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title33",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
		          		<table width="300">
	              	 		<tr>
	               				<td valign="top" nowrap>
									<table width=400>
<%
	if (searchErrors != null)
	{
		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
			"jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

		Enumeration enum1 = searchErrors.keys();

		while (enum1.hasMoreElements())
		{
			String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
		}

		out.println("</font></td></tr>");
	}
%>
									</table>
								</td>
							</tr>
						</table>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
			            	<tr>
			              		<td class="main">
		                			<%= Languages.getString("jsp.admin.error_summary_report",SessionData.getLanguage()) %>
<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
				" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
%>
			                		<br>
			                		<font color="#ff0000"><%= Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage()) %></font>
			            			<table>	
			              				<tr>
			                				<td>
								                <FORM ACTION="admin/reports/carrier_user/transactions/transaction_error_summary_by_merchants.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
								                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
								                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
								                	<INPUT TYPE=hidden NAME=MerchantIDs VALUE="<%=TransactionReport.getMerchantIds()%>">
								                	<INPUT TYPE=hidden NAME=Download VALUE="Y">
								                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
								                </FORM>
							                </td>
							                <td>
								                <FORM ACTION="admin/reports/carrier_user/transactions/print_transaction_error_summary_by_merchants.jsp" METHOD=post TARGET=blank>
								                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
								                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
								                	<INPUT TYPE=hidden NAME=MerchantIDs VALUE="<%=TransactionReport.getMerchantIds()%>">
								                	<INPUT ID=btnPrint TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.carrier_user.print",SessionData.getLanguage())%>">
								                </FORM>
					               	 		</td>
					              		</tr>
				            		</table>
			              		</td>
			            	</tr>
			          	</table>
			          	<table>
							<tr>
								<td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
			                </tr>
			          	</table>
			          	<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
			            	<thead>
			              	<tr class="SectionTopBorder">
			                	<td class=rowhead2 width="4%">#</td>
			                	<td class=rowhead2 nowrap width="35%">
			                  		<%= Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="20%">
			                  		<%= Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="6%">
			                  		<%= Languages.getString("jsp.admin.reports.success",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="16%">
			                  		<%= Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="6%">
			                  		<%= Languages.getString("jsp.admin.reports.errors",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="13%">
			                  		<%= Languages.getString("jsp.admin.reports.error_percent",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			              	</tr>
			            	</thead>
<%
		double   dblTotalSalesSum         = 0;
		double   dblErrorCountSum         = 0;
		double   dblErrorPercentageSum    = 0;
		int      intTotalQtySum           = 0;
		Iterator it                       = vecSearchResults.iterator();
		int      intEvenOdd               = 1;
		int      intCounter               = 1;

		if (it.hasNext())
		{
			Vector vecTemp = null;

			vecTemp = (Vector)it.next();
			dblTotalSalesSum = Double.parseDouble(vecTemp.get(1).toString());
			intTotalQtySum = Integer.parseInt(vecTemp.get(0).toString());
			dblErrorCountSum = Double.parseDouble(vecTemp.get(2).toString());
			dblErrorPercentageSum = Double.parseDouble(vecTemp.get(3).toString());

			while (it.hasNext())
			{
				vecTemp = null;
                vecTemp = (Vector)it.next();

                int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
                double dblTotalSales         = Double.parseDouble(vecTemp.get(3).toString());
                
                out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + "<td nowrap>" + vecTemp.get(
                        0) + "</td>" + "<td>" + vecTemp.get(1) + "</td>" + "<td>" + intTotalQty + "</td>" + 
                        "<td align=right><a href=\"admin/transactions/merchants_transactions.jsp?startDate=" + URLEncoder.encode(
                        TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), 
                        "UTF-8") + "&search=y&merchantId=" + vecTemp.get(1) + "&report=y&printbtn=\" target=\"_blank\">" + 
                        NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</a></td>");
              
                out.print("<td align=right><a href=\"admin/reports/carrier_user/transactions/transaction_error_details.jsp?startDate=" 
                          + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(
                          TransactionReport.getEndDate(), "UTF-8") + "&merchantId=" + vecTemp.get(1) + 
                          "&printbtn=&resultCodes=" + TransactionReport.getResultCodes() + "\" target=\"_blank\">" + 
                          vecTemp.get(4).toString() + "</a></td>" + "<td align=right>" + vecTemp.get(5).toString() + "%</td></tr>");
                
				if (intEvenOdd == 1)
                {
                  	intEvenOdd = 2;
                }
                else
                {
                  	intEvenOdd = 1;
                }
			}
		}
%>
			            	<tfoot>
			              	<tr class=row<%= intEvenOdd %>>
			                	<td colspan=3 align=right>
			                  		<%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
			                	</td>
			                	<td align=left>
			                  		<%= intTotalQtySum %>
			                	</td>
			                	<td align=right>
			                  		<%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
			                	</td>
<%
		out.println("<td align=right><a href=\"admin/reports/carrier_user/transactions/transaction_error_summary_by_types.jsp?startDate=" + URLEncoder.encode(
			TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + 
			"&merchantIds=" + TransactionReport.getMerchantIds() + 
            "&resultCodes=" + TransactionReport.getResultCodes() + "&search=y\" target=\"_blank\">" + 
            Integer.toString((int)dblErrorCountSum) + "</a>" +
            "<td align=right>" +
            NumberUtil.formatAmount(Double.toString(dblErrorPercentageSum))+ 
            "%</td>"); 
%>
			              	</tr>
			            	</tfoot>
			          	</table>
<%
	}
	else
	{
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
%>
						<table width="100%">
			               	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
        }
	}
        
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
<SCRIPT type="text/javascript">
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "CaseInsensitiveString", "Number", "Number", "Number", "Number", "Number"],0,false,false);
  -->
</SCRIPT>
<%
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>