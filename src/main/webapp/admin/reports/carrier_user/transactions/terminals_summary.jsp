<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
  	int section = 14;
  	int section_page = 27;
  boolean NoCarrierProductAttached = false;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
  	Vector vecSearchResults = new Vector();
  	Hashtable searchErrors = null;
  	String sortString = "";
  	boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns

  	if ( request.getParameter("download") != null )
  	{
  	  	String sURL = "";
      	SessionData.setProperty("start_date", request.getParameter("startDate"));
      	SessionData.setProperty("end_date", request.getParameter("endDate"));
      	TransactionReport.setTerminalTypes(request.getParameter("terminalTypes"));
      	TransactionReport.setProductIds(request.getParameter("products"));
      	TransactionReport.setMerchantIds(request.getParameter("merchants"));
	  	TransactionReport.setMinAmount(request.getParameter("minAmount"));
	  	TransactionReport.setMaxAmount(request.getParameter("maxAmount"));
      	
	  	if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
              (request.getParameter("chkUseTaxValue") != null))
		{
			bUseTaxValue = true;
			sURL = TransactionReport.downloadCarrierUserTerminalSummary(application, SessionData, true);
		}
		else
		{
			sURL = TransactionReport.downloadCarrierUserTerminalSummary(application, SessionData, false);
		}
		response.sendRedirect(sURL);
		return;
	}
	
  	if (request.getParameter("search") != null)
	{
	    if (TransactionReport.validateDateRange(SessionData))
	    {
      		SessionData.setProperty("start_date", request.getParameter("startDate"));
      		SessionData.setProperty("end_date", request.getParameter("endDate"));

      		String strTerminalTypes[] = request.getParameterValues("terminals");
      		String strProducts[] = request.getParameterValues("pids");
      		String strMerchants[] = request.getParameterValues("merchantIds");
	
	  		String carrierUserProds = SessionData.getProperty("carrierUserProds");
	  		
      		if (strTerminalTypes != null)
      		{
        		TransactionReport.setTerminalTypes(strTerminalTypes);
      		}
	      	if (strProducts != null)
	      	{
	        	TransactionReport.setProductIds(strProducts);
	      	}
	      	if (strMerchants != null)
	      	{
		        TransactionReport.setMerchantIds(strMerchants);
	      	}
	      	if ( request.getParameter("txtMinAmount") != null )
	      	{
	    	  	TransactionReport.setMinAmount(request.getParameter("txtMinAmount"));
	      	}
	      	if ( request.getParameter("txtMaxAmount") != null )
	      	{
	    	  	TransactionReport.setMaxAmount(request.getParameter("txtMaxAmount"));
			}
      
	      	if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
				(request.getParameter("chkUseTaxValue") != null))
	      	{
		        bUseTaxValue = true;
	        	
				  if(carrierUserProds!=null && !"".equals(carrierUserProds))
				  		vecSearchResults = TransactionReport.getCarrierUserTerminalSummaryMx(SessionData, application);
				  else
			      	 	NoCarrierProductAttached = true;
	      	}
	      	else
	      	{
	      		
				  if(carrierUserProds!=null && !"".equals(carrierUserProds))
				 	 	vecSearchResults = TransactionReport.getCarrierUserTerminalSummary(SessionData,application);
  				  else
      	 				NoCarrierProductAttached = true;
	    	}
    	}
    	else
    	{
      		searchErrors = TransactionReport.getErrors();
    	}
  	}
%>
<%@ include file="/includes/header.jsp" %>
<%
  	if (vecSearchResults != null && vecSearchResults.size() > 0)
  	{
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  	}
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%= Languages.getString("jsp.admin.reports.title36",SessionData.getLanguage()).toUpperCase() %></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
		          		<table width="300">
	              	 		<tr>
	               				<td valign="top" nowrap>
									<table width=400>
<%
	if (searchErrors != null)
	{
		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString("jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

		Enumeration enum1 = searchErrors.keys();

		while (enum1.hasMoreElements())
		{
			String strKey   = enum1.nextElement().toString();
			String strError = (String)searchErrors.get(strKey);
			out.println("<li>" + strError);
		}

		out.println("</font></td></tr>");
	}
%>
									</table>
								</td>
							</tr>
						</table>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
			            	<tr>
			              		<td class="main">
			                		<%= Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_type_summary_report",SessionData.getLanguage()) %>
<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
				" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
%>
									<br>
<%
		if (!TransactionReport.getMinAmount().equals("") && !TransactionReport.getMaxAmount().equals(""))
		{
			out.println(Languages.getString("jsp.admin.ach.summary.amount",SessionData.getLanguage()) + " " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getMinAmount()) + 
				" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getMaxAmount()));
		}
%>
			                		<font color="#ff0000"><%= Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage()) %></font>
			              		</td>
			            	</tr>
			            	<tr>
			              		<td align=left class="main" nowrap>
					              	<form method=post action="admin/reports/carrier_user/transactions/terminals_summary.jsp">
						              	<input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>">
						              	<input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>">
						              	<input type="hidden" name="terminalTypes" value="<%=TransactionReport.getTerminalTypes()%>">
						              	<input type="hidden" name="products" value="<%=TransactionReport.getProductIds()%>">
						              	<input type="hidden" name="merchants" value="<%=TransactionReport.getMerchantIds()%>">
						              	<input type="hidden" name="minAmount" value="<%=TransactionReport.getMinAmount()%>">
						              	<input type="hidden" name="maxAmount" value="<%=TransactionReport.getMaxAmount()%>">
					                	<input type="hidden" type=chkUseTaxValue type="<%=request.getParameter("chkUseTaxValue")%>">
						              	<input type="hidden" name="download" value="y">
						              	<input type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
					              	</form>
			              		</td>
			            	</tr>
			          	</table>
			          	<table>
							<tr>
								<td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
			                </tr>
			          	</table>
			          	<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
			            	<thead>
			              	<tr class="SectionTopBorder">
			                	<td class=rowhead2>#</td>
<%
		if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
		{
			//If we are not in EC
%>
			                	<td class=rowhead2 nowrap>
			                  		<%= Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_type",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
<%
		}//End of if we are not in EC
%>
								<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.transactions.terminals_summary.terminal_description",SessionData.getLanguage()).toUpperCase() %></td>
			                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.RegisteredTerminals",SessionData.getLanguage()).toUpperCase() %></td>
			                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.ActiveTerminals",SessionData.getLanguage()).toUpperCase() %></td>
			                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase() %></td>
			                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase() %></td>
<%
		if ( bUseTaxValue )
		{
%>
								<TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%></TD>
<%
		}
   
		if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
%>
			      				<TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.netAmount",SessionData.getLanguage()).toUpperCase()%></TD>
			       				<TD CLASS="rowhead2" NOWRAP><%=Languages.getString("jsp.admin.reports.taxAmount",SessionData.getLanguage()).toUpperCase()%></TD>
<%
		}
%>
			                	<td class=rowhead2 nowrap><%= Languages.getString("jsp.admin.reports.avg_per_trans",SessionData.getLanguage()).toUpperCase() %></td>
			              	</tr>
			            	</thead>
<%
		double   dblTotalSalesSum         = 0;
		double   dblVATTotalSalesSum      = 0;
		double  dblTaxSum 				  = 0;
		int      intTotalQtySum           = 0;
		Iterator it                       = vecSearchResults.iterator();
		int      intEvenOdd               = 1;
		int      intCounter               = 1;
		int      nRegTerminalCount        = 0;
		int      nActiveTerminalCount     = 0;

		Vector vecTemp = null;
            
		if (it.hasNext())
		{
			vecTemp = (Vector)it.next();
			dblTotalSalesSum = Double.parseDouble(vecTemp.get(0).toString());
            
			if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
			{
				dblVATTotalSalesSum = Double.parseDouble(vecTemp.get(1).toString());
			}
              
			if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
			{
				dblTaxSum=dblTotalSalesSum-dblVATTotalSalesSum;
			}
		} 
              
		while (it.hasNext())
		{
			vecTemp = null;
			vecTemp = (Vector)it.next();

			int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
			double dblTotalSales         = Double.parseDouble(vecTemp.get(3).toString());
			double dblVATTotalSales      = 0;
			double dbTemp =  dblTotalSales + intTotalQty;
			double dblTaxAmount=0;
			double averageAmount         = 0;
              
			if(dbTemp > 0)
			{
				averageAmount         = dblTotalSales/intTotalQty;
			}
            
			String mxUseTaxValue         = "";
			String sTerminalTypeID       = "";
			String sTerminalTypeCounts   = "";
			intTotalQtySum = intTotalQtySum + intTotalQty;

			if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
			{
				dblVATTotalSales = Double.parseDouble(vecTemp.get(6).toString());
				mxUseTaxValue = "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue");
			}
              
			if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
			{
				dblTaxAmount=dblTotalSales-dblVATTotalSales;
			}
			
			if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
			{
				//If we are not in EC
				sTerminalTypeID = "<td nowrap>" + vecTemp.get(0) + "</td>";
			}//End of if we are not in EC
				
			sTerminalTypeCounts = "<td align=right nowrap>" + vecTemp.get(4) + "</td><td align=right nowrap>" + vecTemp.get(5) + "</td>";
			nRegTerminalCount += Integer.parseInt(vecTemp.get(4).toString());
			nActiveTerminalCount += Integer.parseInt(vecTemp.get(5).toString());
			String ultTrans = "";
			Double dblAvgAmn = Double.parseDouble("0");

			if(intTotalQty > 0)
			{
				ultTrans = NumberUtil.formatCurrency(Double.toString(dblTotalSales));
                dblAvgAmn = averageAmount;
			}
			else
			{
				ultTrans = NumberUtil.formatCurrency(Double.toString(dblTotalSales));
				dblAvgAmn = dblTotalSales;
			}

			out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + sTerminalTypeID +
				"<td>" + vecTemp.get(1) + "</td>" + sTerminalTypeCounts + "<td align=right>" + intTotalQty + "</td>" + 
				"<td align=right>" + ultTrans + "</td>" + 
				((bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))?"<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATTotalSales)) + "</td>":""));
                      
			if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
			{
				out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAmount)) + "</td>");
			}

			out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblAvgAmn))  + "</td>");

			out.println("</tr>");

			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}
            
		double dbTempSum =  intTotalQtySum + intTotalQtySum;
		double averageAmountTot =0;

		if(dbTempSum > 0)
		{
			averageAmountTot = dblTotalSalesSum/intTotalQtySum;
		}
%> 
							<tfoot>
			              	<tr class=row<%= intEvenOdd %>>
<%
		String sTerminalTypeID = "2";
	
		if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
		{
			//If we are not in EC
			sTerminalTypeID = "3";
		}//End of if we are not in EC
%>
			                	<td colspan=<%=sTerminalTypeID%> align=right><%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:</td>
			                	<td align=right><%=nRegTerminalCount%></td>
			                	<td align=right><%=nActiveTerminalCount%></td>
			                	<td align=right><%= intTotalQtySum %></td>
			                	<td align=right><%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %></td>
<%
		if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) )
		{
%>
                				<TD ALIGN="right"><%=NumberUtil.formatCurrency(Double.toString(dblVATTotalSalesSum))%></TD>
<%
		}
                
		if ( SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
		{
%>
                				<TD ALIGN="right"><%=NumberUtil.formatCurrency(Double.toString(dblTaxSum))%></TD>
<%
		}
%>
			                	<td align=right><%= NumberUtil.formatCurrency(Double.toString(averageAmountTot)) %></td>
			              	</tr>
			            	</tfoot>
			        	</table>
<%
	}
	else 
	{
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
        if(NoCarrierProductAttached){
             %>
						<table width="100%">
			               	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%	}
else {
%>
						<table width="100%">
			               	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		}

        }
	}
	        
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
		String sTerminalTypeID = "";
		String sTerminalTypeCounts = "";
	
		if (!(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
		{
			//If we are not in EC
			sTerminalTypeID = "\"Number\",";
		}//End of if we are not in EC

		sTerminalTypeCounts = "\"Number\", \"Number\",";
%>
<SCRIPT type="text/javascript">
            
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", <%=sTerminalTypeID%> "CaseInsensitiveString", <%=sTerminalTypeCounts%> "Number", "Number", "Number"<%=((bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS)&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL)  && DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))?", \"Number\"":"")%>],0,false,false);
  -->
            
</SCRIPT>
<%
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>