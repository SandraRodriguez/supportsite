<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.*,
                 com.debisys.languages.*,
                 java.util.*" %>
<%
	int section = 14;
	int section_page = 21;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
  	Vector    vecSearchResults = new Vector();
  	Hashtable searchErrors     = null;
  	String    sortString       = "";

	SessionData.setProperty("start_date", request.getParameter("startDate"));
	SessionData.setProperty("end_date", request.getParameter("endDate"));
	TransactionReport.setMerchantIds(request.getParameter("MerchantIDs"));
	vecSearchResults = TransactionReport.getCarrierUserTransactionErrorSummaryByMerchants(SessionData);
%>
<html>
<head>
    <link href="../../../../default.css" type="text/css" rel="stylesheet">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
    <tr>
      	<td width="18" height="20" align="left"><img src="../../../../images/top_left_blue.gif"></td>
      	<td background="../../../../images/top_blue.gif" class="formAreaTitle" width="3000">&nbsp;<%=Languages.getString("jsp.admin.reports.title33",SessionData.getLanguage()).toUpperCase()%></td>
      	<td width="12" height="20" align="right"><img src="../../../../images/top_right_blue.gif"></td>
    </tr>
    <tr>
      	<td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
			<table width="100%" border="0" cellspacing="0" cellpadding="2">
            	<tr>
              		<td class="main">
                		<%= Languages.getString("jsp.admin.error_summary_report",SessionData.getLanguage()) %>
<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
				" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
%>
                		<br>
                		<br>
                		<font color="#ff0000">
                  		<%= Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage()) %>
                		</font>
              		</td>
            	</tr>
          	</table>
          	<table width="100%" cellspacing="1" cellpadding="1">
            	<thead>
              	<tr>
                	<td class=rowhead width="4%">#</td>
                	<td class=rowhead nowrap width="35%">
                  		<%= Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()) %>
                	</td>
                	<td class=rowhead nowrap width="20%">
                  		<%= Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()) %>
                	</td>
                	<td class=rowhead nowrap width="6%">
                  		<%= Languages.getString("jsp.admin.reports.success",SessionData.getLanguage()) %>
                	</td>
                	<td class=rowhead nowrap width="16%">
                  		<%= Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()) %>
                	</td>
                	<td class=rowhead nowrap width="6%">
                  		<%= Languages.getString("jsp.admin.reports.errors",SessionData.getLanguage()) %>
                	</td>
                	<td class=rowhead nowrap width="13%">
                  		<%= Languages.getString("jsp.admin.reports.error_percent",SessionData.getLanguage()) %>
                	</td>	
              	</tr>
				</thead>
<%
		double   dblTotalSalesSum         = 0;
		double   dblErrorCountSum         = 0;
		double   dblErrorPercentageSum    = 0;
		int      intTotalQtySum           = 0;
		Iterator it                       = vecSearchResults.iterator();
		int      intEvenOdd               = 1;
		int      intCounter               = 1;

		if (it.hasNext())
		{
			Vector vecTemp = null;

			vecTemp = (Vector)it.next();
			dblTotalSalesSum = Double.parseDouble(vecTemp.get(1).toString());
			intTotalQtySum = Integer.parseInt(vecTemp.get(0).toString());
			dblErrorCountSum = Double.parseDouble(vecTemp.get(2).toString());
			dblErrorPercentageSum = Double.parseDouble(vecTemp.get(3).toString());

			while (it.hasNext())
			{
				vecTemp = null;
                vecTemp = (Vector)it.next();

                int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
                double dblTotalSales         = Double.parseDouble(vecTemp.get(3).toString());
                
                out.print("<tr class=row" + intEvenOdd + ">" + "<td>" + intCounter++ + "</td>" + "<td nowrap>" + vecTemp.get(
					0) + "</td>" + "<td>" + vecTemp.get(1) + "</td>" + "<td>" + intTotalQty + "</td>" + 
					"<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</td>");
              
                out.print("<td align=right>" + vecTemp.get(4).toString() + "</td>" + "<td align=right>" + vecTemp.get(5).toString() + "%</td></tr>");
                
				if (intEvenOdd == 1)
                {
                  	intEvenOdd = 2;
                }
                else
                {
                  	intEvenOdd = 1;
                }
			}
		}
%>
				<tfoot>
              	<tr class=row<%= intEvenOdd %>>
                	<td colspan=3 align=right>
                  		<%= Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage()) %>:
                	</td>
                	<td align=left>
                  		<%= intTotalQtySum %>
                	</td>
                	<td align=right>
                  		<%= NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum)) %>
                	</td>
<%           
		out.println("<td align=right>" + Integer.toString((int)dblErrorCountSum) + "</td>" + 
				"<td align=right>" + NumberUtil.formatAmount(Double.toString(dblErrorPercentageSum)) + "%</td>"); 
%>
              	</tr>
            	</tfoot>
          	</table>
            <table id="tblClose" style="display:none;width:100%;">
              	<tr>
              		<td align=center>
              			<input type=button value="<%=Languages.getString("jsp.admin.reports.closewindow",SessionData.getLanguage())%>" onclick="window.close();">
              		</td>
              	</tr>
            </table>
<%
	}
%>
		</td>
	</tr>
</table>
<script>
document.getElementById("tblClose").style.display = "none";
window.print();
document.getElementById("tblClose").style.display = "block";
</script>
</body>
</html>