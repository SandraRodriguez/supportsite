<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%@page import="com.debisys.utils.TimeZone"%>
<%
  int section      = 14;
  int section_page = 37;
  boolean NoCarrierProductAttached = false;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="CarrierReport" class="com.debisys.reports.CarrierReport" scope="request"/>
<jsp:setProperty name="CarrierReport" property="*"/>
  <%@ include file="/includes/security.jsp" %>
<%

  String[][]    vecSearchResults = new String[24][2];
  Hashtable searchErrors     = null;
  String    sortString       = "";
  
  if (request.getParameter("search") != null)
  {
    if (CarrierReport.validateDateRange(SessionData))
    {
      SessionData.setProperty("start_date", request.getParameter("startDate"));
      SessionData.setProperty("end_date", request.getParameter("endDate"));
	  CarrierReport.setStartDate(request.getParameter("startDate"));
      CarrierReport.setEndDate(request.getParameter("endDate"));
      String strProducts[] = request.getParameterValues("productids");
      String strMerchants[] = request.getParameterValues("merchantids");
      
      CarrierReport.setMerchantIds(strMerchants);
      CarrierReport.setProductIds(strProducts);
      String carrierUserProds = SessionData.getProperty("carrierUserProds");
	  if(carrierUserProds!=null && !"".equals(carrierUserProds))
			vecSearchResults = CarrierReport.getCarrierUserHourlySummary(SessionData, application);
      	else
      	 	NoCarrierProductAttached = true;
    }
    else
    {
      searchErrors = CarrierReport.getErrors();
    }
  }
%>
  <%@ include file="/includes/header.jsp" %>
<%
  boolean vecSearchResultsIsNotEmpty = false;
  
  if (vecSearchResults != null && searchErrors==null && !NoCarrierProductAttached){
  //check if there is any trx returned
  for(int i=0;i<24;i++){
  	if(vecSearchResults[i][1]!="0")
  		vecSearchResultsIsNotEmpty = true;
  }
  }
  	
  

%>
  <table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
      <td background="images/top_blue.gif" width="1%" align="left"><img src="images/top_left_blue.gif" width="18" height="20"></td>
      <td background="images/top_blue.gif" class="formAreaTitle" width="2000">&nbsp;<%= Languages.getString("jsp.admin.carrier_user.reports.title4",SessionData.getLanguage()).toUpperCase() %></td>
      <td background="images/top_blue.gif" width="1%" align="right"><img src="images/top_right_blue.gif" width="18" height="20"></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#FFFFFF" class="formArea2">
<%
        if (searchErrors != null)
        {
          out.println("<table width=400><tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
                  "jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

          Enumeration enum1 = searchErrors.keys();

          while (enum1.hasMoreElements())
          {
            String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
          }

          out.println("</font></td></tr></table>");
        }

        
  if (vecSearchResultsIsNotEmpty)
        {
%>
          <table width="100%" border="0" cellspacing="0" cellpadding="2">          
<%
	Vector vTimeZoneData = TimeZone.getTimeZoneByRep(Long.parseLong(SessionData.getProperty("ref_id")));
%>
            <tr class="main"><td nowrap colspan="2"><%=Languages.getString("jsp.admin.timezone.reportNote",SessionData.getLanguage())%>:&nbsp;<%=vTimeZoneData.get(1) + " [" + vTimeZoneData.get(2) + "]"%><br/><br/></td></tr>
            <tr>
            
              <td class="main">

<%
                out.println(Languages.getString("jsp.admin.index.carrier_title22",SessionData.getLanguage()));
                if (!CarrierReport.getStartDate().equals("") && !CarrierReport.getEndDate().equals(""))
                {
                  out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(CarrierReport.getStartDateFormatted()) + 
                          " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(CarrierReport.getEndDateFormatted()));
                }
%>
              </td>
            </tr>
          </table>
          <table>
              <tr>
                  <td class="formAreaTitle2" align="left" width="720"><%=Languages.getString("jsp.admin.index.carrier_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
              </tr>
              <tr>
              <% //insert daily hour by start and end date here %>
              </tr>
          </table>
          <table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
            <thead>
              <tr class="SectionTopBorder">
                
                <td class=rowhead2 nowrap>
                  <!-- Reserved for the hourly range where no header is needed -->
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.carrier.user.numoftrx",SessionData.getLanguage()).toUpperCase()  %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.carrier.user.totaltrx",SessionData.getLanguage()).toUpperCase() %>
                </td>
                <td class=rowhead2 nowrap>
                  <%= Languages.getString("jsp.admin.reports.carrier.user.avgtrx",SessionData.getLanguage()).toUpperCase() %>
                </td>
              </tr>
            </thead>
<%
            int      intEvenOdd               = 1;
            int      hourlycount              = 0; // first column is divided into 23 hour intervals
            
            while (hourlycount<24)
            {
            	String addzero="0";
            	if(hourlycount>9)
            		addzero = "";
                out.print("<tr class=row" + intEvenOdd + ">" 
                  			+ "<td align=\"center\">" + addzero + hourlycount +":00:00 - " + addzero + hourlycount +":59:59" + "</td>" 
                  			+ "<td align=\"center\">" + vecSearchResults[hourlycount][0]+ "</td>"); //number of trx
                if(vecSearchResults[hourlycount][0]!="0"){
                out.print("<td align=\"center\">" + vecSearchResults[hourlycount][1] + "</td>" + // trx amount
                "<td align=\"center\">" + NumberUtil.formatAmount( // avg trx amount
                String.valueOf( Double.parseDouble(vecSearchResults[hourlycount][1])/Double.parseDouble(vecSearchResults[hourlycount][0]) ) )+
                "</td>" );
                }
                else
                 out.print("<td align=\"center\">" + "0.00" + "</td>" +"<td align=\"center\">" + "0.00" + "</td>") ;
                
                if (intEvenOdd == 1)
                {
                  intEvenOdd = 2;
                }
                else
                {
                  intEvenOdd = 1;
                }
                hourlycount++;
            }
              out.print("</tr>");
%>
           
	 </table>
<%
        }
        else
        if ( !vecSearchResultsIsNotEmpty && request.getParameter("search") != null && searchErrors == null)
        {
           if(NoCarrierProductAttached)
                 out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage()) + "</font>");
           else
                 out.println("<br><br><font color=ff0000>" + Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage()) + "</font>");
        }
%>
      </td>
    </tr>
  </table>
  <%@ include file="/includes/footer.jsp" %>
