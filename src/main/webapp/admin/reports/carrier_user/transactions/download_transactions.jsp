<%@ page import="java.util.*, com.debisys.utils.*" %>
<% session.removeAttribute("task"); %>
<jsp:useBean id="task" scope="session"
    class="com.debisys.utils.TaskBean"/>
<%
	int section = 14;
	int section_page = 35;
	// use/don't use the download bar
	boolean downloadBar = false;
	// set a download cap, -1 means no cap?
	int maxDownload = -1;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionSearch" class="com.debisys.transactions.TransactionSearch" scope="request"/>
<jsp:setProperty name="TransactionSearch" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	String positionMarkers = null;
	int intRecordCount = 0;
	int intSectionPage = 0;
	int intPageSize = 50;
	int recordcount = 0;

	if (request.getParameter("download") != null)
	{
  		if (request.getParameter("section_page") != null)
  		{
    		try
    		{
      			intSectionPage = Integer.parseInt(request.getParameter("section_page"));
    		}
    		catch(NumberFormatException ex)
    		{
     			response.sendRedirect(request.getHeader("referer"));
     			return;
    		}
  		}  		
    
    	if (request.getParameter("recordcount") != null)
        	recordcount = Integer.valueOf(request.getParameter("recordcount"));
    	
    	if(request.getParameter("invoiceID") != null)
    		TransactionSearch.setInvoiceNo(request.getParameter("invoiceID"));
    	
  		if (TransactionSearch.validateDateRange(SessionData))
  		{
			String strCustomConfigType = DebisysConfigListener.getCustomConfigType(application);
     	 	
			String strUrlLocation = TransactionSearch.downloadCarrierUserTrans(SessionData, application);

			response.sendRedirect(strUrlLocation);
  		}
  		else
  		{
     		response.sendRedirect(request.getHeader("referer"));
     		return;
  		}
	}
%>