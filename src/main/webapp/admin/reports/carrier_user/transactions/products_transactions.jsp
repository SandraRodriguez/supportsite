<%@ page import="java.util.*,
                 java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder" %>
<%
	int section=14;
	int section_page=25;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:useBean id="NumberUtil" class="com.debisys.utils.NumberUtil" scope="page"/>
<jsp:useBean id="DateUtil" class="com.debisys.utils.DateUtil" scope="page"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector vecSearchResults = new Vector();
	Hashtable searchErrors = null;
	int intRecordCount = 0;
	int intPage = 1;
	int intPageSize = 50;
	int intPageCount = 1;

	if ( request.getParameter("Download") != null )
	{
	  	String sURL = "";
    	
	  	if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) 
			&& (request.getParameter("chkUseTaxValue") != null))
       	{
    	  	sURL = TransactionReport.downloadProductDetails(application, SessionData, true);
       	}
       	else
       	{
       	  	sURL = TransactionReport.downloadProductDetails(application, SessionData, false);
       	}
	  	response.sendRedirect(sURL);
	  	return;
	}
	
	if (request.getParameter("search") != null)
	{
  		if (request.getParameter("page") != null)
  		{
    		try
    		{
      			intPage=Integer.parseInt(request.getParameter("page"));
    		}
    		catch(NumberFormatException ex)
    		{
      			intPage = 1;
    		}
  		}

  		if (intPage < 1)
  		{
    		intPage=1;
  		}

  		if (TransactionReport.validateDateRange(SessionData))
  		{		
    		SessionData.setProperty("start_date", request.getParameter("startDate"));
    		SessionData.setProperty("end_date", request.getParameter("endDate"));
    		String strMerchantIds[] = request.getParameterValues("merchantIds");
    
    		if (strMerchantIds != null)
    		{
      			TransactionReport.setMerchantIds(strMerchantIds);
    		}

    		if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
    		{
      			SessionData.setProperty("deploymentType", DebisysConfigListener.getDeploymentType(application));
    		}
    
	    	if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
    		{
      			SessionData.setProperty("customConfigType", DebisysConfigListener.getCustomConfigType(application));
    		}

    		if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) 
    				&& (request.getParameter("chkUseTaxValue") != null))
    		{
      			vecSearchResults = TransactionReport.getProductTransactionsMx(intPage, intPageSize, SessionData, application, true);
    		}
    		else
    		{
    			vecSearchResults = TransactionReport.getProductTransactions(intPage, intPageSize, SessionData, application,  true);
    		}
    		intRecordCount = Integer.parseInt(vecSearchResults.get(0).toString());
    		vecSearchResults.removeElementAt(0);
    
    		if (intRecordCount>0)
    		{
      			intPageCount = (intRecordCount / intPageSize) + 1;
      			
      			if ((intPageCount * intPageSize) + 1 >= intRecordCount)
      			{
        			intPageCount++;
      			}
    		}
  		}
  		else
  		{
   			searchErrors = TransactionReport.getErrors();
  		}
	}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title29",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
		          		<table width="300">
	              	 		<tr>
	               				<td valign="top" nowrap>
									<table width=400>
<%
	if (searchErrors != null)
	{
  		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1=searchErrors.keys();

		while(enum1.hasMoreElements())
		{
  			String strKey = enum1.nextElement().toString();
  			String strError = (String) searchErrors.get(strKey);
  			out.println("<li>" + strError);
		}

  		out.println("</font></td></tr>");
	}
%>
									</table>
								</td>
							</tr>
						</table>
<%
  	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
		String baseURL = "admin/reports/carrier_user/transactions/products_transactions.jsp?search=" 
			+ URLEncoder.encode(request.getParameter("search"), "UTF-8") 
			+ "&productId=" + TransactionReport.getProductId() 
			+ "&startDate=" + URLEncoder.encode(TransactionReport.getStartDate(), "UTF-8") 
			+  "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8")
			+  "&merchantIds=" + URLEncoder.encode(TransactionReport.getMerchantIds(), "UTF-8");
	
		if ( request.getParameter("chkUseTaxValue") != null )
		{
			baseURL += "&chkUseTaxValue=" + request.getParameter("chkUseTaxValue");
		}
%>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
	            			<tr>
	            				<td class="main"><%=intRecordCount + " " + Languages.getString("jsp.admin.results_found",SessionData.getLanguage()) + " "%>
<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}%>.&nbsp;<%=Languages.getString("jsp.admin.displaying", new Object[]{ Integer.toString(intPage),  Integer.toString(intPageCount-1)},SessionData.getLanguage())%>
								</td>
							</tr>
				            <tr>
				            	<td colspan=2>
				            		<table>
				             	 		<tr>
				                			<td>
								                <FORM ACTION="admin/reports/carrier_user/transactions/products_transactions.jsp" METHOD=post ONSUBMIT="document.getElementById('btnSubmit').disabled = true;">
								                	<INPUT TYPE=hidden NAME=startDate VALUE="<%=request.getParameter("startDate")%>">
								                	<INPUT TYPE=hidden NAME=endDate VALUE="<%=request.getParameter("endDate")%>">
								                	<INPUT TYPE=hidden NAME=chkUseTaxValue VALUE="<%=request.getParameter("chkUseTaxValue")%>">
								                	<INPUT TYPE=hidden NAME=search VALUE="<%=request.getParameter("search")%>">
								                	<INPUT TYPE=hidden NAME=productId VALUE="<%=request.getParameter("productId")%>">
								                	<INPUT TYPE=hidden NAME=merchantIds VALUE="<%=request.getParameter("merchantIds")%>">
								                	<INPUT TYPE=hidden NAME=Download VALUE="Y">
								                	<INPUT ID=btnSubmit TYPE=submit VALUE="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
								                </FORM>
				                			</td>
				                			<td>&nbsp;&nbsp;&nbsp;</td>
				                			<td></td>
				              			</tr>
				            		</table>
				            	</td>
				            </tr>            
				            <tr>
				            </tr>
						</table>
			          	<table>
							<tr>
								<td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
				              	<td align=right class="main" nowrap>
<%
		if (intPage > 1)
		{
			out.println("<a href=\"" + baseURL + "&page=1\">"+Languages.getString("jsp.admin.first",SessionData.getLanguage())+"</a>&nbsp;");
			out.println("<a href=\"" + baseURL + "&page=" + (intPage-1) + "\">&lt;&lt;"+Languages.getString("jsp.admin.previous",SessionData.getLanguage())+"</a>&nbsp;");
		}

		int intLowerLimit = intPage - 12;
		int intUpperLimit = intPage + 12;

		if (intLowerLimit<1)
		{
			intLowerLimit=1;
			intUpperLimit = 25;
		}

		for(int i = intLowerLimit; i <= intUpperLimit && i < intPageCount; i++)
		{
			if (i==intPage)
			{
				out.println("<font color=#ff0000>" + i + "</font>&nbsp;");
			}
			else
			{
				out.println("<a href=\"" + baseURL + "&page=" + i + "\">" +i+ "</a>&nbsp;");
			}
		}

		if (intPage < (intPageCount-1))
		{
			out.println("<a href=\"" + baseURL + "&page=" + (intPage+1) + "\">" +Languages.getString("jsp.admin.next",SessionData.getLanguage())+ "&gt;&gt;</a>&nbsp;");
			out.println("<a href=\"" + baseURL + "&page=" + (intPageCount-1) + "\">" +Languages.getString("jsp.admin.last",SessionData.getLanguage())+ "</a>");
		}
%>
								</td>
			                </tr>
			          	</table>
						<table width="100%" cellspacing="1" cellpadding="2">
				            <tr>
				              	<td class=rowhead2>#</td>
				              	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.tran_no",SessionData.getLanguage()).toUpperCase()%></td>
				              	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.term_no",SessionData.getLanguage()).toUpperCase()%></td>
				              	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.dba",SessionData.getLanguage()).toUpperCase()%></td>
				              	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.merchant_id",SessionData.getLanguage()).toUpperCase()%></td>
				              	<td class=rowhead2><%=Languages.getString("jsp.admin.reports.date",SessionData.getLanguage()).toUpperCase()%></td>
<%
		if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
%>
  			  					<td class=rowhead2><%=Languages.getString("jsp.admin.reports.phys_state",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.city_county",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.clerk",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.reference",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.ref_no",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.amount",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.balance",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.product",SessionData.getLanguage()).toUpperCase()%></td>
			              		<td class=rowhead2><%=Languages.getString("jsp.admin.reports.control_no",SessionData.getLanguage()).toUpperCase()%></td>
<%
		if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
%>
  			  					<td class=rowhead2><%=Languages.getString("jsp.admin.reports.vendor_id",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>
<%
		if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
%>
								<td class=rowhead2><%=Languages.getString("jsp.admin.reports.pin_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>
<% 
		if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
			&& strAccessLevel.equals(DebisysConstants.ISO)
			&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
		{
%>
								<td class=rowhead2><%=Languages.getString("jsp.admin.reports.ach_date",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>                
<%
		if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
		{
%>
  			  					<td class=rowhead2><%=Languages.getString("jsp.admin.reports.authorization_number",SessionData.getLanguage()).toUpperCase()%></td>
<%
		}
%>
            				</tr>
<%
		int intCounter = 1;

		Iterator it = vecSearchResults.iterator();
		int intEvenOdd = 1;
		
		while (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector) it.next();

			String sViewPIN = "", sPhysState = "", sAuthorizationNo = "", sVendorID = "";

			if (SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN) )
	        {
				//if the trans_type=2 which is a pin sale
				String strCurrencySymbol = NumberUtil.getCurrencySymbol();
                      
				if ( vecTemp.get(13).toString().equals("2") && Double.parseDouble(vecTemp.get(9).toString().replaceAll(strCurrencySymbol, "")) > 0)
				{
					sViewPIN = "<a href=\"javascript:void(0);\" onclick=\"openViewPIN('" + vecTemp.get(0).toString() + "');\"><span class=\"showLink\">" + vecTemp.get(12).toString() + "</span></a>";
				}
				else
				{
					sViewPIN = vecTemp.get(12).toString();
				}
			}
			else
			{
				sViewPIN = vecTemp.get(12).toString();
			}
			
			if (DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{
				sPhysState = "<td nowrap>" + vecTemp.get(15).toString() + "</td>";
						
				if ( vecTemp.get(19).toString().equals("154"))
				{
					sVendorID = "<td nowrap>" + vecTemp.get(18).toString() + "</td>";
				}
				else
				{
					sVendorID = "<td nowrap>N/A</td>";
				}
			}
			else if (DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO))
			{
				sAuthorizationNo = "<td nowrap>" + vecTemp.get(14).toString() + "</td>";
			}

			out.println("<tr class=row" + intEvenOdd +">" +
				"<td>" + intCounter++ + "</td>" +
				"<td>" + vecTemp.get(0)+ "</td>" +
				"<td>" + vecTemp.get(1) + "</td>" +
				"<td nowrap>" + HTMLEncoder.encode(vecTemp.get(2).toString())  + "</td>" +
				"<td>" + vecTemp.get(3) + "</td>" +
				"<td nowrap>" + vecTemp.get(4) + "</td>" +
				sPhysState +
				//city
				"<td nowrap>" + vecTemp.get(5) + "</td>" +
				//clerk
				"<td nowrap>" + vecTemp.get(6) + "</td>" +
				"<td>" + vecTemp.get(7) + "</td>" +
				"<td>" + vecTemp.get(8) + "</td>" +
				//amount
				"<td nowrap>" + vecTemp.get(9) + "</td>" +
				//bal
				"<td nowrap>" + vecTemp.get(10) + "</td>" +
				"<td nowrap width=200>" + vecTemp.get(11) + "</td>" +
				"<td>" + sViewPIN + "</td>" +
				sVendorID
				);
				
			if(SessionData.checkPermission(DebisysConstants.PERM_VIEWPIN)
				&& strAccessLevel.equals(DebisysConstants.ISO)
				&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{					
				out.println("<td>" + vecTemp.get(16) + "</td>");
			}
					
			if(SessionData.checkPermission(DebisysConstants.PERM_VIEW_ACH_DATE)
				&& strAccessLevel.equals(DebisysConstants.ISO)
				&& DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC))
			{					
				out.println("<td>" + vecTemp.get(17) + "</td>");
			}		 
			out.println(sAuthorizationNo + "</tr>");
                    
			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}
		vecSearchResults.clear();
%>
						</table>

<%
	}
	else if (intRecordCount==0 && request.getParameter("search") != null && searchErrors == null)
	{
%>
						<table width="100%">
				           	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js" src="admin/calendar/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>

<%@ include file="/includes/footer.jsp" %>