<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
	int section      = 14;
	int section_page = 33;
  boolean NoCarrierProductAttached = false;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session" />
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request" />
<jsp:setProperty name="TransactionReport" property="*" />
<%@ include file="/includes/security.jsp" %>
<%
  	Vector    vecSearchResults = new Vector();
  	Hashtable searchErrors     = null;
  
  	int intErrorsSum = 0;
  
  	if ( request.getParameter("download") != null )
	{
	  	String sURL = "";
      	SessionData.setProperty("start_date", request.getParameter("startDate"));
      	SessionData.setProperty("end_date", request.getParameter("endDate"));
		TransactionReport.setMerchantIds(request.getParameter("merchant_ids"));
		TransactionReport.setResultCodes(request.getParameter("resultCodes"));
      	sURL = TransactionReport.downloadCarrierUserTransactionErrorSummaryByType(application, SessionData);

      	response.sendRedirect(sURL);
	  	return;
	}
  	
	if (request.getParameter("search") != null)
  	{
    	if (TransactionReport.validateDateRange(SessionData))
    	{
      		SessionData.setProperty("start_date", request.getParameter("startDate"));
      		SessionData.setProperty("end_date", request.getParameter("endDate"));
	
      		String strResultCodes[] = request.getParameterValues("errors");
      
      		if (strResultCodes != null)
      		{
        		TransactionReport.setResultCodes(strResultCodes);  
      		}
      
       		//DJC R24 added to allow a new filter by product for this report
	  		String strProductIds[] = request.getParameterValues("pids");

            String carrierUserProds = SessionData.getProperty("carrierUserProds");
            
      		if (strProductIds != null)
      		{
        		TransactionReport.setProductIds(strProductIds);
      		}
      		//DJC R24
			if(carrierUserProds!=null && !"".equals(carrierUserProds))
				vecSearchResults = TransactionReport.getCarrierUserTransactionErrorSummaryByTypes(SessionData);
			else
      	 		NoCarrierProductAttached = true;
      	}
    	else
    	{
      		searchErrors = TransactionReport.getErrors();
    	}
  	}
%>
<%@ include file="/includes/header.jsp" %>
<%  
  	if (vecSearchResults != null && vecSearchResults.size() > 0)
  	{
%>
<LINK href="includes/sortROC.css" type="text/css" rel="StyleSheet" />
<SCRIPT SRC="includes/sortROC.js" type="text/javascript"></SCRIPT>
<%
  	}
%>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%= Languages.getString("jsp.admin.reports.title31",SessionData.getLanguage()).toUpperCase() %></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
	  	<td colspan="3"  bgcolor="#FFFFFF">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
  				<tr>
					<td class="formArea2">
		          		<table width="300">
	              	 		<tr>
	               				<td valign="top" nowrap>
									<table width=400>
<%
	if (searchErrors != null)
	{
		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>" + Languages.getString(
			"jsp.admin.error1",SessionData.getLanguage()) + ":<br>");

		Enumeration enum1 = searchErrors.keys();

		while (enum1.hasMoreElements())
		{
			String strKey   = enum1.nextElement().toString();
            String strError = (String)searchErrors.get(strKey);

            out.println("<li>" + strError);
		}

		out.println("</font></td></tr>");
	}
%>
									</table>
								</td>
							</tr>
						</table>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
			            	<tr>
			              		<td class="main">
			                 		<%= Languages.getString("jsp.admin.error_summary_report",SessionData.getLanguage()) %>
<%
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + 
				" " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
%>
			                		<br>
			            			<table>
						              	<tr>
						                	<td>
							                  	<form method=post action="admin/reports/carrier_user/transactions/transaction_error_summary_by_types.jsp">
								                    <input type="hidden" name="startDate" value="<%=TransactionReport.getStartDate()%>">
								                    <input type="hidden" name="endDate" value="<%=TransactionReport.getEndDate()%>">
								                    <input type="hidden" name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">
								                    <input type="hidden" name="resultCodes" value="<%=TransactionReport.getResultCodes()%>">
						              				<input type="hidden" name="download" value="y">
								                    <input type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
							                  	</form>
							                </td>
						                	<td>
							                  	<form target=blank method=post action="admin/reports/carrier_user/transactions/print_transaction_error_summary_by_types.jsp">
								                    <input type=hidden name="startDate" value="<%=TransactionReport.getStartDate()%>">
								                    <input type=hidden name="endDate" value="<%=TransactionReport.getEndDate()%>">
								                    <input type=hidden name="merchant_ids" value="<%=TransactionReport.getMerchantIds()%>">
								                    <input type=hidden name="resultCodes" value="<%=TransactionReport.getResultCodes()%>">
								                    <input type=submit value="<%=Languages.getString("jsp.admin.reports.carrier_user.print",SessionData.getLanguage())%>">
							                  	</form>
						                	</td>
						              	</tr>
						            </table>
			              		</td>
			            	</tr>
			          	</table>
			          	<table width="100%" cellspacing="1" cellpadding="1" border="0" class="sort-table" id="t1">
			            	<thead>
			              	<tr class="SectionTopBorder">
			                	<td class=rowhead2 width="5%">#</td>
			                	<td class=rowhead2 nowrap width="40%">
			                  		<%= Languages.getString("jsp.admin.reports.error_type",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="17%">
			                  		<%= Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="20%">
			                  		<%= Languages.getString("jsp.admin.reports.total_errors",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			                	<td class=rowhead2 nowrap width="18%">
			                  		<%= Languages.getString("jsp.admin.reports.error_percent",SessionData.getLanguage()).toUpperCase() %>
			                	</td>
			              	</tr>
			            	</thead>
<%
		Iterator it                       = vecSearchResults.iterator();
		int      intEvenOdd               = 1;
		int      intCounter               = 1;
            
		if (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector)it.next();
			intErrorsSum = Integer.parseInt(vecTemp.get(0).toString());
		}
              
		while (it.hasNext())
		{
			Vector vecTemp = null;
			vecTemp = (Vector)it.next();
			int    intTotalQty           = Integer.parseInt(vecTemp.get(2).toString());
                
			int errors = Integer.parseInt(vecTemp.get(2).toString());
			double errors_percent = 0;
              
			if (intErrorsSum == 0)
			{
				errors_percent = 100;
			}
			else
			{
				errors_percent = ((double)errors/intErrorsSum) * 100;
			} 
              
			// conditional filter that modifies detail query for void errors cases
			String appendToURL = "";
              
			if (vecTemp.get(0).toString().equalsIgnoreCase("VOID ERRORS")) 
			{
				appendToURL += "&queryType=voidErrors";
			}
              
			out.print("<tr class=row" + intEvenOdd + ">" + 
				"<td nowrap>" + intCounter++ + "</td>" + 
              	"<td nowrap>" + vecTemp.get(1) + "</td>" + 
              	"<td nowrap><a href=\"admin/reports/carrier_user/transactions/transaction_error_detail_by_types.jsp?startDate=" + URLEncoder.encode(
              	TransactionReport.getStartDate(), "UTF-8") + "&endDate=" + URLEncoder.encode(TransactionReport.getEndDate(), "UTF-8") + 
              	"&resultCode=" + vecTemp.get(0).toString() + "&merchantIds=" + TransactionReport.getMerchantIds() + appendToURL 
              	+ "\" target=\"_blank\">" + vecTemp.get(2) + "</td>" +
              	"<td nowrap>" + intErrorsSum + "</td>" + 
              	"<td nowrap align=\"right\">" + NumberUtil.formatAmount(Double.toString(errors_percent)) + "%</td></tr>");
                    
			if (intEvenOdd == 1)
			{
				intEvenOdd = 2;
			}
			else
			{
				intEvenOdd = 1;
			}
		}            
%>            
          				</table>
<%
	}
	else {
        if (vecSearchResults.size() == 0 && request.getParameter("search") != null && searchErrors == null)
        {
%>
						<table width="100%">
			               	<tr>
				               	<td nowrap>
									<table width=400>
							            <tr>
							            	<td class="main">
							            	<% if(NoCarrierProductAttached){ %>
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage())%></font>
							            	<% }else{%>
							            		<font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
							            	<% }%>
							            	</td>
							            </tr>
									</table>
								</td>
							</tr>
						</table>
<%		
        }
	}
     
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
          <SCRIPT type="text/javascript">
            
                    <!--
  var stT1 = new SortROC(document.getElementById("t1"),
  ["None", "CaseInsensitiveString", "Number", "Number", "Number"],0,false,false);
  -->
            
          </SCRIPT>
<%
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>