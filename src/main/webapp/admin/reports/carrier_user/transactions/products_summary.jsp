<%@ page import="java.net.URLEncoder,
                 com.debisys.utils.HTMLEncoder,
                 java.util.*,
                 com.debisys.utils.NumberUtil" %>
<%
    int section=14;
    int section_page=24;
    boolean NoCarrierProductAttached = false;
%>
<jsp:useBean id="SessionData" class="com.debisys.users.SessionData" scope="session"/>
<jsp:useBean id="TransactionReport" class="com.debisys.reports.TransactionReport" scope="request"/>
<jsp:setProperty name="TransactionReport" property="*"/>
<%@ include file="/includes/security.jsp" %>
<%
	Vector vecSearchResults = new Vector();
	Hashtable searchErrors = null;
	boolean bUseTaxValue = false;//Indicates if we are in the Mexico implementation and the user want to see the taxes in columns
        
        boolean showInfoMultiSource = SessionData.checkPermission(DebisysConstants.PERM_SHOW_MULTISOURCE_INFO);
        /*        
        boolean IspermSSInterDefault = SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                        DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                            DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT);
        */ 
	if ( request.getParameter("Download") != null )
	{
            String sURL = "";
            TransactionReport.setProductIds(request.getParameter("_productIds"));
            TransactionReport.setMerchantIds(request.getParameter("_merchantIds"));
    	
            if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
                    !(request.getParameter("chkUseTaxValue").equals("null") ) )
            {
                bUseTaxValue = true;
                sURL = TransactionReport.downloadCarrierUserProductSummary(application, SessionData, true);
            }
            else
            {
                sURL = TransactionReport.downloadCarrierUserProductSummary(application, SessionData, false);
            }
            response.sendRedirect(sURL);
            return;
	}

	if (request.getParameter("search") != null)
	{
            if (TransactionReport.validateDateRange(SessionData))
            {
                String carrierUserProds = SessionData.getProperty("carrierUserProds");
                SessionData.setProperty("start_date", request.getParameter("startDate"));
                SessionData.setProperty("end_date", request.getParameter("endDate"));
                String strProductIds[] = request.getParameterValues("pids");

                if (strProductIds != null)
                {
                    TransactionReport.setProductIds(strProductIds);
                }	
                String strMerchantIds[] = request.getParameterValues("merchantIds");

                if (strMerchantIds != null)
                {
                    TransactionReport.setMerchantIds(strMerchantIds);
                }

                if ( DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_DOMESTIC) )
                {
                    SessionData.setProperty("deploymentType", DebisysConfigListener.getDeploymentType(application));
                }

                if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) )
                {
                    SessionData.setProperty("customConfigType", DebisysConfigListener.getCustomConfigType(application));
                }

                if ( DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_MEXICO) &&
                        (request.getParameter("chkUseTaxValue") != null ))
                {
                    bUseTaxValue = true;
                    if(carrierUserProds!=null && !"".equals(carrierUserProds))
                        vecSearchResults = TransactionReport.getCarrierUserProductSummaryMx(SessionData, application);
                    else
                        NoCarrierProductAttached = true;
                }
                else
                {
                    if(carrierUserProds!=null && !"".equals(carrierUserProds))
                        vecSearchResults = TransactionReport.getCarrierUserProductSummary(SessionData, application);
                    else
                        NoCarrierProductAttached = true;
                }
            }
            else
            {
                searchErrors = TransactionReport.getErrors();
            }
	}
%>
<%@ include file="/includes/header.jsp" %>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
	<tr>
    	<td width="18" height="20"><img src="images/top_left_blue.gif"></td>
    	<td background="images/top_blue.gif" width="3000" class="formAreaTitle">&nbsp;<%=Languages.getString("jsp.admin.reports.title5",SessionData.getLanguage()).toUpperCase()%></td>
    	<td width="12" height="20"><img src="images/top_right_blue.gif"></td>
  	</tr>
  	<tr>
            <td colspan="3"  bgcolor="#FFFFFF">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align=center>
                    <tr>
                        <td class="formArea2">
                        <table width="300">
                            <tr>
                                <td valign="top" nowrap>
                                    <table width=400>
<%
	if (searchErrors != null)
	{
  		out.println("<tr class=main><td align=left colspan=3><font color=ff0000>"+Languages.getString("jsp.admin.error1",SessionData.getLanguage())+":<br>");
		Enumeration enum1=searchErrors.keys();

		while(enum1.hasMoreElements())
		{
  			String strKey = enum1.nextElement().toString();
  			String strError = (String) searchErrors.get(strKey);
  			out.println("<li>" + strError);
		}

  		out.println("</font></td></tr>");
	}
%>
                                    </table>
                                </td>
                            </tr>
                        </table>
<%
	if (vecSearchResults != null && vecSearchResults.size() > 0)
	{
%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                            <tr>
                                <td class="main">
<%
		out.println(Languages.getString("jsp.admin.reports.transaction.product_summary",SessionData.getLanguage()));
	
		if (!TransactionReport.getStartDate().equals("") && !TransactionReport.getEndDate().equals(""))
		{
			out.println(" " + Languages.getString("jsp.admin.from",SessionData.getLanguage()) + " " +  HTMLEncoder.encode(TransactionReport.getStartDateFormatted()) + " " + Languages.getString("jsp.admin.to",SessionData.getLanguage()) + " " + HTMLEncoder.encode(TransactionReport.getEndDateFormatted()));
		}
%>
                                        <br><font color="#ff0000"><%=Languages.getString("jsp.admin.reports.test_trans",SessionData.getLanguage())%></font>
                                </td>
                            </tr>
                            <tr>
                                <td colspan=2>
                                    <table>
                                        <tr>
                                            <td>
                                                <form action="admin/reports/carrier_user/transactions/products_summary.jsp" method=post onsubmit="document.getElementById('btnSubmit').disabled = true;">
                                                    <input type=hidden name=startDate value="<%=request.getParameter("startDate")%>">
                                                    <input type=hidden name=endDate value="<%=request.getParameter("endDate")%>">
                                                    <input type=hidden name=chkUseTaxValue value="<%=request.getParameter("chkUseTaxValue")%>">
                                                    <input type=hidden name=_productIds value="<%=TransactionReport.getProductIds()%>">
                                                    <input type=hidden name=_merchantIds value="<%=TransactionReport.getMerchantIds()%>">
                                                    <input type=hidden name=Download value="Y">
                                                    <input id=btnSubmit type=submit value="<%=Languages.getString("jsp.admin.reports.transactions.transactions.download",SessionData.getLanguage())%>">
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td class="formAreaTitle" align="left" width="720"><%=Languages.getString("jsp.admin.index.company_name",SessionData.getLanguage())%>: <%=SessionData.getProperty("company_name")%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="1" cellpadding="2">
                            <tr>
<%
    String strSortURL = "admin/reports/carrier_user/transactions/products_summary.jsp?search=y&startDate=" 
            + TransactionReport.getStartDate() 
            + "&endDate=" + TransactionReport.getEndDate() 
            + "&pids=" + TransactionReport.getProductIds() 
            + ((bUseTaxValue)?"&chkUseTaxValue=" 
            + request.getParameter("chkUseTaxValue"):"") 
            + ((request.getParameterValues("merchantIds") != null)?"&merchantIds=" 
            + request.getParameterValues("merchantIds")[0]:"");
%>
                                <td class=rowhead2 valign=bottom>#</td>
                                <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.transactions.products_summary.product_name",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=1&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=1&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.id",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=2&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=2&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.qty",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=3&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=3&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=4&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=4&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
<%
		if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                        DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
%>
                                <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.bonus",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=17&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=17&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
                                <td class=rowhead2 valign=bottom><%=Languages.getString("jsp.admin.reports.total_recharge",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=18&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=18&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
<%
		}
                
		if ( bUseTaxValue )
		{
%>
                                <td class="rowhead2" valign="bottom"><%=Languages.getString("jsp.admin.reports.total",SessionData.getLanguage()).toUpperCase()%><%=Languages.getString("jsp.admin.reports.minusvat",SessionData.getLanguage()).toUpperCase()%>&nbsp;<a href="<%=strSortURL%>&col=11&sort=1"><img src="images/down.png" height=11 width=11 border=0></a><a href="<%=strSortURL%>&col=11&sort=2"><img src="images/up.png" height=11 width=11 border=0></a></td>
<%
		}
		
                if ( showInfoMultiSource )
                {        
                %>
                                <td class=rowhead2 nowrap><%=Languages.getString("jsp.tools.dtu2536.MultiSource",SessionData.getLanguage()).toUpperCase()%>&nbsp;</td>                
                <%
                }
                %>
                        </tr>
                <%
    double dblTotalSalesSum = 0;
    double dblTotalBonus = 0;
    double dblTotalRechargeSum = 0;
    double dblMerchantCommissionSum = 0;
    double dblRepCommissionSum = 0;
    double dblSubAgentCommissionSum = 0;
    double dblAgentCommissionSum = 0;
    double dblISOCommissionSum = 0;
    double dblVATTotalSalesSum = 0;
    double dblVATMerchantCommissionSum = 0;
    double dblVATRepCommissionSum = 0;
    double dblVATSubAgentCommissionSum = 0;
    double dblVATAgentCommissionSum = 0;
    double dblVATISOCommissionSum = 0;
    double dblAdjAmountSum = 0;
    double dblChannelPercentageSum = 0; // 2008-11-12.FB. New column added
    double dblTotalTaxAmountSum=0;
    int intTotalQtySum = 0;
    Iterator it = vecSearchResults.iterator();
    int intEvenOdd = 1;
    int intCounter = 1;
        
    while (it.hasNext())
    {
        Vector vecTemp = null;
        vecTemp = (Vector) it.next();
        int intTotalQty = Integer.parseInt(vecTemp.get(2).toString());
        double dblTotalSales = Double.parseDouble(vecTemp.get(3).toString());
        double dblMerchantCommission = Double.parseDouble(vecTemp.get(4).toString());
        double dblRepCommission = Double.parseDouble(vecTemp.get(5).toString());
        double dblSubAgentCommission = Double.parseDouble(vecTemp.get(6).toString());
        double dblAgentCommission = Double.parseDouble(vecTemp.get(7).toString());
        double dblISOCommission = Double.parseDouble(vecTemp.get(8).toString());
        double dblAdjAmount = Double.parseDouble(vecTemp.get(9).toString());
        double dblVATTotalSales = 0;
        double dblVATMerchantCommission = 0;
        double dblVATRepCommission = 0;
        double dblVATSubAgentCommission = 0;
        double dblVATAgentCommission = 0;
        double dblVATISOCommission = 0;
        double dblBonus = 0;
        double dblTotalRecharge = 0;
        double dblTaxAmount=0;

        // 2008-11-11 FB. New column added for Jim
        double dblChannelPercentage = 0;
            
        if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS)) //iso only and with permission
        {
            if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
            {
                dblChannelPercentage = (dblISOCommission + dblRepCommission + dblMerchantCommission) /  dblTotalSales * 100;
            }
            else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
            {
                dblChannelPercentage = (dblISOCommission + dblAgentCommission + dblSubAgentCommission + dblRepCommission + dblMerchantCommission) / dblTotalSales * 100;
            }
        }
	
        if ( bUseTaxValue )
        {
            dblVATTotalSales = Double.parseDouble(vecTemp.get(10).toString());
            dblVATMerchantCommission = Double.parseDouble(vecTemp.get(11).toString());
            dblVATRepCommission = Double.parseDouble(vecTemp.get(12).toString());
            dblVATSubAgentCommission = Double.parseDouble(vecTemp.get(13).toString());
            dblVATAgentCommission = Double.parseDouble(vecTemp.get(14).toString());
            dblVATISOCommission = Double.parseDouble(vecTemp.get(15).toString());
            dblBonus = Double.parseDouble(vecTemp.get(16).toString());
            dblTotalRecharge = Double.parseDouble(vecTemp.get(17).toString());
        } 
        else 
        {
            dblBonus = Double.parseDouble(vecTemp.get(10).toString());
            dblTotalRecharge = Double.parseDouble(vecTemp.get(11).toString()); 
        }
	/*        
        if( IspermSSInterDefault )
        {
            dblTaxAmount=dblTotalSales-dblVATTotalSales;
        }
	*/
        intTotalQtySum = intTotalQtySum + intTotalQty;
        dblTotalSalesSum = dblTotalSalesSum + dblTotalSales;
        dblTotalBonus += dblBonus;
        dblTotalRechargeSum += dblTotalRecharge;
        dblMerchantCommissionSum=dblMerchantCommissionSum + dblMerchantCommission;
        dblRepCommissionSum = dblRepCommissionSum + dblRepCommission;
        dblSubAgentCommissionSum = dblSubAgentCommissionSum + dblSubAgentCommission;
        dblAgentCommissionSum = dblAgentCommissionSum + dblAgentCommission;
        dblISOCommissionSum = dblISOCommissionSum + dblISOCommission;
        dblAdjAmountSum = dblAdjAmountSum + dblAdjAmount;
	
        String sMerchantIds = "";

        if ( request.getParameterValues("merchantIds") != null )
        {
            sMerchantIds = "&merchantIds=" + TransactionReport.getMerchantIds();
        }
	
        if ( bUseTaxValue ) //|| (IspermSSInterDefault)
        {
            /*
            if( IspermSSInterDefault )
            {
                dblTotalTaxAmountSum+=dblTaxAmount;
            }
            */
            dblVATTotalSalesSum += dblVATTotalSales;
            dblVATMerchantCommissionSum += dblVATMerchantCommission;
            dblVATRepCommissionSum += dblVATRepCommission;
            dblVATSubAgentCommissionSum += dblVATSubAgentCommission;
            dblVATAgentCommissionSum += dblVATAgentCommission;
            dblVATISOCommissionSum += dblVATISOCommission;
            out.println("<tr class=row" + intEvenOdd +">" +
                    "<td>" + intCounter++ + "</td>" +
                    "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                    "<td>" + vecTemp.get(1) + "</td>" +
                    "<td>" + intTotalQty + "</td>" +
                    "<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</td>");

            if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                    DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
            {
                out.println("<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" +
                        "<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblTotalRecharge)) + "</td>");
            }

            out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblVATTotalSales)) + "</td>");
            /*
            if( IspermSSInterDefault )
            {
                out.println("<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTaxAmount)) + "</td>");
            }
            */
        }
        else 
        {
                out.println("<tr class=row" + intEvenOdd +">" +
                        "<td>" + intCounter++ + "</td>" +
                        "<td nowrap>" + vecTemp.get(0)+ "</td>" +
                        "<td>" + vecTemp.get(1) + "</td>" +
                        "<td>" + intTotalQty + "</td>" +
                        "<td align=right>" + NumberUtil.formatCurrency(Double.toString(dblTotalSales)) + "</td>");
	            
                if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                        DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
                {
                    out.println("<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblBonus)) + "</td>" +
                            "<td align=right>"+NumberUtil.formatCurrency(Double.toString(dblTotalRecharge)) + "</td>");
                }
        }
	
        // 2008-11-11 FB. New column added for Jim
        if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
        {
            out.println("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblChannelPercentage))  + "</td>");
        }
        String markMultiSource = "";
        if ( showInfoMultiSource )
        {       
            if ( bUseTaxValue )
            {
                if ( vecTemp.get(18)=="1" )
                {
                   markMultiSource = Languages.getString("jsp.tools.dtu2536.MultiSourceFlag",SessionData.getLanguage()).toUpperCase();     
                }
                //out.println("<td align=right>" + markMultiSource  + "</td>");
            }
            else if ( vecTemp.get(12)=="1" )
            {
               markMultiSource = Languages.getString("jsp.tools.dtu2536.MultiSourceFlag",SessionData.getLanguage()).toUpperCase();     
            }
            out.println("<td align=right>" + markMultiSource  + "</td>");
        }
        
        
        out.println("</tr>");
	
        if (intEvenOdd == 1)
        {
            intEvenOdd = 2;
        }
        else
        {
            intEvenOdd = 1;
        }
    }

    vecSearchResults.clear();
%>
                <tr class=row<%=intEvenOdd%>>
                    <td colspan=3 align=right><%=Languages.getString("jsp.admin.reports.totals",SessionData.getLanguage())%>:</td>
                            <td align=left><%=intTotalQtySum%></td>
                    <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalSalesSum))%></td>
<%
		if(DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                        DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)) 
		{
%>
                    <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalBonus))%></td>
                    <td align=right><%=NumberUtil.formatCurrency(Double.toString(dblTotalRechargeSum))%></td>
	
<%
		}
	
		if ( bUseTaxValue || (SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                        DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                            DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT)))
		{
%>
                    <td align=right><%= NumberUtil.formatCurrency(Double.toString(dblVATTotalSalesSum)) %></td>
<%
		}
	
		if(SessionData.checkPermission(DebisysConstants.PERM_ENABLE_TAX_CALCULATION_IN_SS_REPORTS) && 
                            DebisysConfigListener.getDeploymentType(application).equals(DebisysConstants.DEPLOYMENT_INTERNATIONAL) && 
                                DebisysConfigListener.getCustomConfigType(application).equals(DebisysConstants.CUSTOM_CONFIG_TYPE_DEFAULT))
		{
%>
                    <td align=right><%= NumberUtil.formatCurrency(Double.toString(dblTotalTaxAmountSum)) %></td>
<%
		}
		
		// 2008-11-11 FB. New column added for Jim
		if (SessionData.checkPermission(DebisysConstants.PERM_MANAGE_SUPPORT_ISOS))
		{	
			if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_3_LEVEL))
			{
				dblChannelPercentageSum = (dblISOCommissionSum + dblRepCommissionSum + dblMerchantCommissionSum) /  dblTotalSalesSum * 100;
			}
			else if(strDistChainType.equals(DebisysConstants.DIST_CHAIN_5_LEVEL))
			{
		 		dblChannelPercentageSum = (dblISOCommissionSum + dblAgentCommissionSum + dblSubAgentCommissionSum + dblRepCommissionSum + dblMerchantCommissionSum) / dblTotalSalesSum * 100;
			}
	
	   		out.println("<td align=right>" + NumberUtil.formatAmount(Double.toString(dblChannelPercentageSum))  + "</td>");
		}
                if ( showInfoMultiSource )
                {
                    out.println("<td></td>");
                }
                out.println("</tr></table>");
	}
         else if (vecSearchResults.size()==0 && request.getParameter("search") != null && searchErrors == null)
	{
%>
            <table width="100%">
                <tr>
                    <td nowrap>
                        <table width=400>
                            <tr>
                                <td class="main">
                            <%	if(NoCarrierProductAttached){ %>
                                        <font color=ff0000><%=Languages.getString("jsp.admin.no_carriersattached_found",SessionData.getLanguage())%></font>
                            <% }else{ %>	
                                        <font color=ff0000><%=Languages.getString("jsp.admin.no_records_found",SessionData.getLanguage())%></font>
                            <% } %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
<%
	}
%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/includes/footer.jsp" %>